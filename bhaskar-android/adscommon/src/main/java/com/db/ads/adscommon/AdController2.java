package com.db.ads.adscommon;

import android.content.Context;

import com.bhaskar.appscommon.tracking.util.Systr;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;

import java.lang.ref.WeakReference;
import java.util.HashMap;

public class AdController2 {

    private static final int AD_SIZE_320_50 = 1;
    private static final int AD_SIZE_300_250 = 2;

//    public static final String AD_UNIT_Photo_Gallery_LISTING_TOP = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_Photo_Listing_Top_300x250";
//    public static final String AD_UNIT_Photo_Gallery_LISTING_MID = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_Photo_Listing_Mid_300x250";
//    public static final String AD_UNIT_Photo_Gallery_LISTING_BOTTOM = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_Photo_Listing_Bottom_300x250";
//
//    public static final String AD_UNIT_News_Breif_LISTING_TOP = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_Brief_Top_300x250";
//    public static final String AD_UNIT_News_Breif_LISTING_MID = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_Brief_Mid_300x250";
//    public static final String AD_UNIT_News_Breif_LISTING_BOTTOM = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_Brief_Bottom_300x250";

    public interface onAdNotifyListener {
        void onNotifyAd();
    }

    private static AdController2 nativeListAdController;
    private static Context mContext;

    private AdController2() {
    }

    class DataInfo {
        String unitId;
        int adSize;

        WeakReference<onAdNotifyListener> listener;
        PublisherAdView adView;

        @Override
        public boolean equals(Object obj) {
            return this.unitId.equals(((DataInfo) obj).unitId);
        }
    }

    private HashMap<String, DataInfo> viewHashMap = new HashMap<>();

    public static AdController2 getInstance(Context context) {
        mContext = context;
        if (nativeListAdController == null) {
            nativeListAdController = new AdController2();
        }

        return nativeListAdController;
    }

    public synchronized void destroy() {
        viewHashMap.clear();
    }

    public synchronized PublisherAdView getAd(String unitId, WeakReference<onAdNotifyListener> listener) {
        Systr.println("ADS : banner GET ad view");

        if (viewHashMap.containsKey(unitId)) {
            DataInfo info = viewHashMap.get(unitId);
            return info.adView;
        } else {
            showBannerInPhotoListing(unitId, listener);
        }

        return null;
    }

    private synchronized void showBannerInPhotoListing(String unitId, WeakReference<onAdNotifyListener> listener) {
        DataInfo dataInfo = new DataInfo();
        dataInfo.unitId = unitId;
        dataInfo.adSize = AD_SIZE_300_250;
        dataInfo.listener = listener;

        showBannerAd(mContext, dataInfo);
    }

    private synchronized void showBannerAd(Context context, final DataInfo dataInfo) {
        Systr.println("ADS : Banner Called : AdUnit : " + dataInfo.unitId + ", AdSize : " + dataInfo.adSize);

        final PublisherAdView adView = new PublisherAdView(context);
        adView.setAdUnitId(dataInfo.unitId);

        switch (dataInfo.adSize) {

            case AD_SIZE_300_250:
                adView.setAdSizes(AdSize.MEDIUM_RECTANGLE, new AdSize(336, 280), new AdSize(250, 250), new AdSize(200, 200));
                break;

            case AD_SIZE_320_50:
            default:
                adView.setAdSizes(AdSize.BANNER);
                break;
        }

        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Systr.println("ADS : Loaded : AdUnit : " + dataInfo.unitId + ", AdSize : " + dataInfo.adSize);
                dataInfo.adView = adView;
                viewHashMap.put(dataInfo.unitId, dataInfo);

                try {
                    if (dataInfo.listener != null) {
                        dataInfo.listener.get().onNotifyAd();
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onAdFailedToLoad(int i) {
                Systr.println("ADS : Failed : AdUnit : " + dataInfo.unitId + ", AdSize : " + dataInfo.adSize + ", Error Code : " + i);
            }
        });

        try {

            PublisherAdRequest.Builder builder = new PublisherAdRequest.Builder();
//        builder.addTestDevice("B3EEABB8EE11C2BE770B684D95219ECB");

            Systr.println("ADS : Banner Custom Params : AdUnit : " + dataInfo.unitId + ", GDPR : " + AdsConstants.gdpr_blocked_boolean);
            builder.addCustomTargeting("gdpr_blocked", AdsConstants.gdpr_blocked_boolean ? "true" : "false");
//        if (AdsConstants.gdpr_blocked_boolean) {
//            Bundle bundle = new Bundle();
//            bundle.putString("npa", "1");
//            builder.addNetworkExtrasBundle(AdMobAdapter.class, bundle);
//        }

            adView.loadAd(builder.build());
        } catch (Exception e) {
        }
    }
}
