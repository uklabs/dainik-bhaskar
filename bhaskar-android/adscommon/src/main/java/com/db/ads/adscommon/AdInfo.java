package com.db.ads.adscommon;

import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

public class AdInfo {

    public String adUnitId;
    public int position;
    public int type;

    public PublisherAdView bannerAdView;
    public int catType;
}
