package com.db.ads.adscommon;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;

public class AdsConstants {

    public static final int AD_TYPE_BANNER = 1;
    public static final int AD_TYPE_NATIVE = 2;
    public static final int AD_TYPE_NATIVE_HORIZONTAL_PHOTO = 3;
    public static final int AD_TYPE_NATIVE_HORIZONTAL_VIDEO = 4;

    public static final int ACTIVE = 1;
    public static final int DE_ACTIVE = 0;

    public static final int VIDEO_AD_NETWORK_NONE = 0;
    public static final int VIDEO_AD_NETWORK_IMA = 1;
    public static final int VIDEO_AD_NETWORK_FACEBOOK = 4;

    static String versionName = BuildConfig.VERSION_NAME;
    static String osVersion = "0";
    static String is_gdpr_blocked = "false";
    static String dpiString = "DENSITY_UNKNOWN";
    static int total_session_counted = 0;

    public static final String app_version = "app_version";
    public static final String vtpc = "vtpc";
    public static final String gdpr_blocked = "gdpr_blocked";
    public static boolean gdpr_blocked_boolean = false;
    public static boolean is_personalized = false;
    public static final String auto_paly = "auto_play";
    public static String provider_code = "provider_code";
    public static String vwallbrandid = "vwallbrandid";
    public static String session_counted = "session_counted";

    public static String getVersionNameAlreadyCalculated() {
        return versionName;
    }

    public static void setSessionCount(int setSessionCounted) {
        AdsConstants.total_session_counted = setSessionCounted;
    }

    public static String getVersionName(Context context, String versionName, boolean isBlocked, boolean isPersonalized) {
        try {
            AdsConstants.versionName = versionName;
            osVersion = Build.VERSION.RELEASE;
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            dpiString = "" + getDpiString(metrics.densityDpi);
            is_gdpr_blocked = "" + isBlocked;
            gdpr_blocked_boolean = isBlocked;
            is_personalized = isPersonalized;
        } catch (Exception e) {
        }
        return versionName;
    }

    public static String getDpiString(int unit) {
        String dpiString;

        switch (unit) {
            case DisplayMetrics.DENSITY_LOW:
                dpiString = "ldpi";
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                dpiString = "mdpi";
                break;
            case DisplayMetrics.DENSITY_HIGH:
                dpiString = "hdpi";
                break;
            case DisplayMetrics.DENSITY_XHIGH:
            case DisplayMetrics.DENSITY_260:
            case DisplayMetrics.DENSITY_280:
            case DisplayMetrics.DENSITY_300:
                dpiString = "xhdpi";
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
            case DisplayMetrics.DENSITY_340:
            case DisplayMetrics.DENSITY_360:
            case DisplayMetrics.DENSITY_400:
            case DisplayMetrics.DENSITY_420:
                dpiString = "xxhdpi";
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
            case DisplayMetrics.DENSITY_560:
                dpiString = "xxxhdpi";
                break;
            case DisplayMetrics.DENSITY_TV:
                dpiString = "tvdpi";
                break;
            default:
                dpiString = "DENSITY_" + unit;
        }

        return dpiString;
    }

    public static int getIntFromString(String text) {
        int value = 0;
        try {
            text = text.trim();
            value = Integer.valueOf(text);
        } catch (Exception e) {
        }
        return value;

    }

    public static String getContentUrl(String link) {
        if (link != null && link.contains("?ref") && link.indexOf("?ref") > 0) {
            link = link.substring(0, link.indexOf("?ref"));
            return link;
        }
        return null;
    }
}
