package com.db.ads.adscommon;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.doubleclick.PublisherAdView;

public class BannerAdViewHolder extends RecyclerView.ViewHolder {

    private RelativeLayout adLayout;

    public BannerAdViewHolder(View view) {
        super(view);

        adLayout = view.findViewById(R.id.adRelativeLayout);
    }

    public void addAds(PublisherAdView adView) {
        try {
            if (adLayout != null) {
                adLayout.removeAllViews();

                if (adView != null) {
                    ViewParent parent = adView.getParent();
                    ViewGroup pr = (ViewGroup) parent;
                    if (pr != null)
                        pr.removeView(adView);
                    adView.setFocusable(false);
                }

                adLayout.addView(adView);
                adLayout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
        }
    }

    public void addAdsUrl(View adView) {
        try {
            if (adLayout != null) {
                adLayout.removeAllViews();

                if (adView != null) {
                    ViewParent parent = adView.getParent();
                    ViewGroup pr = (ViewGroup) parent;
                    if (pr != null)
                        pr.removeView(adView);
                    adView.setFocusable(false);
                }

                adLayout.addView(adView);
                adLayout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
        }
    }
}
