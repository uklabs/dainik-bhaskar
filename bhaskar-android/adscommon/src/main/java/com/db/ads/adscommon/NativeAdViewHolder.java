package com.db.ads.adscommon;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;

public class NativeAdViewHolder extends RecyclerView.ViewHolder {

    public RelativeLayout adLayout;
    public CardView adCardLayout;

    public NativeAdViewHolder(View view) {
        super(view);

        adLayout = view.findViewById(R.id.adRelativeLayout);
        adCardLayout = view.findViewById(R.id.adCardView);
    }

    public void addAds(View adView) {
        try {
            if (adLayout != null) {

                if (adView != null) {
                    ViewParent parent = adView.getParent();
                    ViewGroup pr = (ViewGroup) parent;
                    if (pr != null)
                        pr.removeView(adView);
                    adView.setFocusable(false);
                }

                adLayout.removeAllViews();
                adLayout.addView(adView);
                adLayout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
        }
    }
}
