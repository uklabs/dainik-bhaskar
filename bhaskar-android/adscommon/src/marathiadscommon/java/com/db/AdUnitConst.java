package com.db;

public class AdUnitConst {

    public static final String AD_UNIT_Exit_Popup = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_300x250_Exit";
    public static final String AD_UNIT_Article_ATF = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_AS_TOP_320x50";
    public static final String AD_UNIT_Article_BTF = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_AS_BOTTOM_300x250";
    public static final String AD_UNIT_All_Listing_ATF = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_AL_TOP_320x50";
    public static final String AD_UNIT_Home_Listing_ATF = "/28928716/DIVYAMARATHI_APP/Home/DM_APP_AOS_HP_Top_320x50";
    public static final String AD_UNIT_Home_Listing_BTF = "/28928716/DIVYAMARATHI_APP/Home/DM_APP_AOS_HP_Bottom_300x250";

    public static final String AD_UNIT_News_Breif_LISTING_TOP = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Brief_Top_300x250";
    public static final String AD_UNIT_News_Breif_LISTING_MID = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Brief_Mid_300x250";
    public static final String AD_UNIT_News_Breif_LISTING_BOTTOM = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Brief_Bottom_300x250";

    /*******/
    public static final String AD_UNIT_Photo_Gallery_LISTING_TOP = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Photo_Listing_Top_300x250";
    public static final String AD_UNIT_Photo_Gallery_LISTING_MID = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Photo_Listing_Mid_300x250";
    public static final String AD_UNIT_Photo_Gallery_LISTING_BOTTOM = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Photo_Listing_Bottom_300x250";

    public static final String AD_UNIT_Photo_Gallery_BTF = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Photo_Gallery_320x50";
    public static final String AD_UNIT_VIDEO_GALLERY = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Video_Gallery_320x50";


    public static final String AD_UNIT_Article_BTF_50 = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_AS_BOTTOM_320x50";


    /********/

    public static final String AD_UNIT_NATIVE_LISTING_TOP = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Native_Top";
    public static final String AD_UNIT_NATIVE_LISTING_MID = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Native_Mid";
    public static final String AD_UNIT_NATIVE_LISTING_BOTTOM = "/28928716/DIVYAMARATHI_APP/ROS/DM_APP_AOS_ROS_Native_Bottom";

    public static final String AD_UNIT_NATIVE_HOME_LISTING_TOP = "/28928716/DIVYAMARATHI_APP/Home/DM_APP_AOS_HP_Native_Top";
    public static final String AD_UNIT_NATIVE_HOME_LISTING_MID = "/28928716/DIVYAMARATHI_APP/Home/DM_APP_AOS_HP_Native_Mid";
    public static final String AD_UNIT_NATIVE_HOME_LISTING_BOTTOM = "/28928716/DIVYAMARATHI_APP/Home/DM_APP_AOS_HP_Native_Bottom";
}
