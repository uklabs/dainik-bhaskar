# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Android_Dev\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#
-dontoptimize
-keep public class * extends android.preference.Preference

-keep public class * extends android.app.Fragment
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

-dontwarn com.google.android.material.**
-keep class com.google.android.material.** { *; }

-dontwarn androidx.** -keep class androidx.** { *; }
-keep interface androidx.** { *; }
-keep class androidx.appcompat.widget.** { *; }
-keep public interface androidx.room.** { *; }
-keep interface androidx.room.** { *; }
-keep public class androidx.room.** { *; }
-keep class androidx.room.** { *; }

# Support-v4
-dontwarn android.support.v4.**
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }

# Support-v7
-dontwarn android.support.v7.**
-keep class android.support.v7.internal.** { *; }
-keep interface android.support.v7.internal.** { *; }
-keep class android.support.v7.widget.RoundRectDrawable { *; }

# Design library
-dontwarn android.support.design.**
-keep class android.support.design.** { *; }
-keep interface android.support.design.** { *; }
-keep public class android.support.design.R$* { *; }

-keep class com.google.android.gms.common.api.GoogleApiClient { public *; }
-keep class com.google.android.gms.common.api.GoogleApiClient$* {public *;}
-keep class com.google.android.gms.location.LocationServices {public *;}
-keep class com.google.android.gms.location.FusedLocationProviderApi {public *;}
-keep class com.google.android.gms.location.ActivityRecognition {public *;}
-keep class com.google.android.gms.location.ActivityRecognitionApi {public *;}
-keep class com.google.android.gms.location.ActivityRecognitionResult {public *;}
-keep class com.google.android.gms.location.DetectedActivity {public *;}

#gms exception handling
-keep class com.google.android.gms.internal.** { *; }
-dontwarn com.google.android.gms.internal.zzhu

#-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient{public *;}
#-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info{public *;}

# AppsFlyer
#-dontwarn com.appsflyer.AFKeystoreWrapper
#-dontwarn com.appsflyer.**

# ComScore
-keep class com.comscore.** { *; }
-dontwarn com.comscore.**

# okhttp
-dontwarn com.squareup.okhttp.**
-dontwarn okio.**


# Otto Bus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @com.squareup.otto.Subscribe public *;
    @com.squareup.otto.Produce public *;
}

# Facebook
-keep class com.facebook.** { *; }
-keepattributes Signature
-keep class com.classes.extending.facebook.** { *; }


# Facebook
-dontwarn com.facebook.ads.internal.**
-keep class com.facebook.ads.**

# Apache
-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient

# Volley
-dontwarn com.android.volley.toolbox.**
-keep class com.android.volley.** { *; }
-keep interface com.android.volley.** { *; }

# Lint Resources
-keep public class com.google.android.gms.* { public *; }
-dontwarn com.google.android.gms.**

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepattributes SourceFile,LineNumberTable,InnerClasses
#-dontwarn com.inmobi.**
#-keep class com.inmobi.** { *; }
-keep public class com.google.android.gms.**
-dontwarn com.google.android.gms.**
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient{
     public *;
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info{
     public *;
}

# Moat / Inmobi Anaytics
-keep class com.moat.** {*;}
-dontwarn com.moat.**

# AVID
-keep class com.integralads.avid.library.* {*;}
-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}
-keep class com.google.ads.mediation.admob.AdMobAdapter {
    *;
}
-keep class com.google.ads.mediation.AdUrlAdapter {
    *;
}

# InMobi SDK
-keepattributes SourceFile,LineNumberTable
#-keep class com.inmobi.** { *; }
#-keep public class com.google.android.gms.**
#-dontwarn com.google.android.gms.**
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient{
     public *;
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info{
     public *;
}
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep class com.bumptech.glide.GeneratedAppGlideModuleImpl
-keep class com.bumptech.glide.integration.okhttp.OkHttpGlideModule
-dontwarn com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool
-dontwarn com.bumptech.glide.load.resource.bitmap.Downsampler
-dontwarn com.bumptech.glide.load.resource.bitmap.HardwareConfigState
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

# AVID classes
-keep class com.integralads.avid.library.* {*;}
-keep class com.google.ads.mediation.admob.AdMobAdapter {*;}
#-keep class com.google.ads.mediation.inmobi.**
#-dontwarn com.google.ads.mediation.inmobi.**
-keep class com.google.ads.mediation.AdUrlAdapter {*;}

# Search view
-keep class android.support.v7.widget.SearchView { *; }


# IMA SDK
-keep class * extends java.util.ListResourceBundle { protected Object[][] getContents();}
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable { public static final *** NULL;}
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * { @com.google.android.gms.common.annotation.KeepName *;}
-keepnames class * implements android.os.Parcelable { public static final ** CREATOR;}
-keep class com.google.** { *; }
-keep interface com.google.** { *; }

#Google IMA
#-keep class com.google.ads.interactivemedia.v3.api.** { *;}
#-keep interface com.google.ads.interactivemedia.** { *; }
#-dontwarn com.google.ads.interactivemedia.v3.api.**

-keep class org.nexage.sourcekit.** {
public protected private *;
}
-keep class javax.xml.xpath.** {
public protected private *;
}
-keep class org.w3c.dom.** {
public protected private *;
}

#apche
-dontwarn com.shaded.fasterxml.**
-dontwarn org.apache.**
-dontwarn org.shaded.apache.**
-keepnames class com.shaded.fasterxml.jackson.** { *; }
-keepnames class org.shaded.apache.**


#Vmax Progaured rules
-dontwarn com.google.ads.**
#If Rewarded Interstitial Ad Format not integrated
-dontwarn com.google.firebase.**
#If AdMob not integrated
-dontwarn com.google.android.gms.**

#-keep public class com.vmax.android.ads.api.VmaxAdView {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.api.VmaxSdk {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.api.VmaxAdSize {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.api.ViewMandatoryListener {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.api.BitmapSampler {
#    public <fields>;
#    public <methods>;
#}
#-keep public class com.vmax.android.ads.api.AdContainer {
#    public <fields>;
#    public <methods>;
#}
#-keep public class com.vmax.android.ads.api.ImageLoader {
#    public <fields>;
#    public <methods>;
#}
#-keep public class com.vmax.android.ads.api.NativeImageDownload {
#    public <fields>;
#    public <methods>;
#}
#-keep public class com.vmax.android.ads.api.NativeImageDownloadListener {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.api.VmaxAdPartner {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.api.VmaxAdReward {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.common.VmaxAdListener {
#     <fields>;
#     <methods>;
#}
#
#-keep public class com.vmax.android.ads.common.User {
#     public <fields>;
#     public <methods>;
#}
#
#-keep public class com.vmax.android.ads.exception.** {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.exception.VmaxAdError {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.mediation.** {
#    public <fields>;
#    public <methods>;
#}
#
#-keep class com.vmax.android.ads.mediation.partners.** {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.mediation.partners.VmaxAdPlayer {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.nativeads.** {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.nativeHelper.** {
#     public <fields>;
#     public <methods>;
#}
#
#-keep public class com.vmax.android.ads.nativeview.** {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.util.** {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.util.CountryAttributes {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.util.CountryNames {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.util.Utility {
#    <fields>;
#    <methods>;
#}
#
#-keep public class com.vmax.android.ads.vast.** {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public class com.vmax.android.ads.webview.** {
#    public <fields>;
#    public <methods>;
#}
#
#-keep public enum com.vmax.android.ads.api.VmaxAdView$AdState {
#  <fields>;
#  public static **[] values();
#  public static ** valueOf(java.lang.String);
#}

-keep public class com.google.ads.** {
    public <fields>;
    public <methods>;
}

-keep public class com.google.android.gms.** {
    <fields>;
    <methods>;
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keep,allowshrinking @com.google.android.gms.common.annotation.KeepName class *


#inmobi
-keepattributes SourceFile,LineNumberTable

#Flurry Ad SDK Settings
-keep class com.flurry.**{ *; }
-dontwarn com.flurry**
-keepattributes *Annotation*,EnclosingMethod
-keepclasseswithmembers class * {
 public <init>(android.content.Context, android.util.AttributeSet, int);
}

#Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-dontwarn com.bumptech.glide.load.resource.bitmap.VideoDecoder
#-keep class com.bumptech.glide.integration.okhttp.OkHttpGlideModule
#-keep class com.bumptech.glide.integration.okhttp3.OkHttpGlideModule
#-keep class com.bumptech.glide.integration.okhttp3.**
#-keep class com.bumptech.glide.integration.okhttp.**
#-dontwarn com.bumptech.glide.integration.okhttp3.**
#-dontwarn com.bumptech.glide.integration.okhttp.**
#-keep class com.bumptech.glide.annotation.compiler.**
#-dontwarn com.bumptech.glide.annotation.compiler.**

#Okhttp
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
-dontwarn org.conscrypt.**
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

#Retrofit
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}
-dontwarn rx.**
-dontwarn retrofit.appengine.UrlFetchClient
-dontnote retrofit2.Platform
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
-dontwarn retrofit2.Platform$Java8
-keepattributes Signature
-keepattributes Exceptions
-dontwarn okio.**
-dontwarn javax.annotation.**
-keepattributes Signature
-keepattributes *Annotation*
-dontwarn sun.misc.**
-keep class com.google.gson.stream.** { *; }
-keep class com.google.gson.examples.android.model.** { *; }
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

-keep class com.wang.avi.** { *; }
-keep class com.wang.avi.indicators.** { *; }

-keep class ch.qos.** { *; }
-keep class org.slf4j.** { *; }
-dontwarn ch.qos.logback.core.net.*
-dontwarn org.slf4j.**

-keepclassmembers class * extends org.jbox2d.dynamics.contacts.Contact{
    public <init>(...);
}

##---------------Begin: proguard configuration for Parceler library ----------

-keep interface org.parceler.Parcel
-keep @org.parceler.Parcel class * { *; }
-keep class **$$Parcelable { *; }

# Butter Knife
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

# Rx Bus
-keepclassmembers class ** {
    @com.hwangjr.rxbus.annotation.Subscribe public *;
    @com.hwangjr.rxbus.annotation.Produce public *;
}

-keepclassmembers,allowshrinking,allowobfuscation class com.android.volley.NetworkDispatcher {
    void processRequest();
}
-keepclassmembers,allowshrinking,allowobfuscation class com.android.volley.CacheDispatcher {
    void processRequest();
}


##---------------Begin: proguard configuration for Parceler library ----------

-keep interface org.parceler.Parcel
-keep @org.parceler.Parcel class * { *; }
-keep class **$$Parcelable { *; }

##---------------End: proguard configuration for Parceler library ----------

-ignorewarnings -keep class com.db.offers.megaoffers.model.** {
  public protected private *;
}
-ignorewarnings -keep public class com.db.offers.megaoffers.model.** {
  public protected private *;
}
-ignorewarnings -keep class com.db.offers.megaoffers.sqldb.tables.** {
  public protected private *;
}
-ignorewarnings -keep public class com.db.offers.megaoffers.sqldb.tables.** {
  public protected private *;
}

# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep public class * implements com.google.gson.TypeAdapterFactory
-keep public class * implements com.google.gson.JsonSerializer
-keep public class * implements com.google.gson.JsonDeserializer


-ignorewarnings -keep class com.igalata.bubblepicker.** {
  public protected private *;
}

-ignorewarnings -keep class com.db.offers.common.rxbus.event.** {
  public protected private *;
}
-ignorewarnings -keep public class com.db.offers.common.rxbus.event.** {
  public protected private *;
}
-ignorewarnings -keep class com.db.offers.tambola.model.** {
  public protected private *;
}
-ignorewarnings -keep public class com.db.offers.tambola.model.** {
  public protected private *;
}
-ignorewarnings -keep class com.db.database.** {
  public protected private *;
}
-ignorewarnings -keep public class com.db.database.** {
  public protected private *;
}
-ignorewarnings -keep class com.db.data.models.** {
  public protected private *;
}
-ignorewarnings -keep public class com.db.data.models.** {
  public protected private *;
}
-ignorewarnings -keep public interface com.db.database.** { *; }
-ignorewarnings -keep interface com.db.database.** { *; }
-ignorewarnings -keep public class com.db.database.** { *; }
-ignorewarnings -keep class com.db.database.** { *; }
-ignorewarnings -keep public class com.db.data.models.** { *; }
-ignorewarnings -keep class com.db.data.models.** { *; }
-ignorewarnings -keep public interface com.db.data.models.** { *; }
-ignorewarnings -keep interface com.db.data.models.** { *; }

# Application classes that will be serialized/deserialized over Gson
-ignorewarnings -keep public class * implements com.db.offers.common.GsonProguardMarker
-ignorewarnings -keep class * implements com.db.offers.common.GsonProguardMarker
-ignorewarnings -keep public class * extends com.db.offers.common.GsonProguardMarker
-ignorewarnings -keep class * extends com.db.offers.common.GsonProguardMarker
-ignorewarnings -keep public interface * extends com.db.offers.common.GsonProguardMarker
-ignorewarnings -keep interface * extends com.db.offers.common.GsonProguardMarker


# Retain service method parameters when optimizing.
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}

# Ignore annotation used for build tooling.
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

# Ignore JSR 305 annotations for embedding nullability information.
-dontwarn javax.annotation.**

# Guarded by a NoClassDefFoundError try/catch and only used when on the classpath.
-dontwarn kotlin.Unit

# Top-level functions that can only be used by RxSupport.
-dontwarn retrofit.*

# Top-level functions that can only be used by RxSupport.
-dontwarn retrofit.RxSupport.*

# Top-level functions that can only be used by RxSupport.
-dontwarn rx.*

# Top-level functions that can only be used by appengine.
-dontwarn retrofit.appengine.*

# Top-level functions that can only be used by appengine.
-dontwarn com.google.appengine.*

-dontwarn android.arch.util.paging.CountedDataSource
-dontwarn android.arch.persistence.room.paging.LimitOffsetDataSource

-keepclassmembers class * extends java.lang.Enum {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep public enum com.db.home.fantasyMenu.**{
    *;
}