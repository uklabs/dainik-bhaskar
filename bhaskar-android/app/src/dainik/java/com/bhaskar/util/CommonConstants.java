package com.bhaskar.util;

public class CommonConstants {
    public static final String CHANNEL_ID = "521";
    public static final String BHASKAR_APP_ID = "2";

    public static String CONTENT_URL_VALUE = "https://www.bhaskar.com/";
    public static final String TRACKING_PREFIX = "DB";
    public static final String FEED_BACK_EMAIL_ID = "dainikapp@dainikbhaskar.com";

    public static final String EVENT_LABEL = "db";

    public static final String CHANNEL_NEWS = "521";
    public static final String CHANNEL_BUSINESS = "1463";
    public static final String CHANNEL_RELIGION = "3322";

    public static final String APP_SHARE_URL = "https://dainikbhaskar.page.link/applink";

    //Ads Publisher App ID
    public static final String ADMOB_APP_ID = "ca-app-pub-3985955077200088~8965948255";
    public static final String DFP_PUBLISHER_ID_FOR_CONSENT = "28928716";

    /*app widget & sticky api feed url*/
    public static String APP_WIDGET_STICKY_FEED_URL = "https://appfeedlight.bhaskar.com/appFeedV3/WidgetNews/521/521/1/";
    public static final String USER_AGENT = "DainikBhaskarAndroid";

    public static String rashifalTabTitles[] = new String[]{"दैनिक", "साप्ताहिक", "मासिक", "वार्षिक"};
    public static String rashifalTabTitlesEnglish[] = new String[]{"Daily", "Weekly", "Monthly", "Yearly"};
}
