package com.bhaskar.util;


import com.db.InitApplication;

public class AppUrls {

    private static final String LOGIN_DEFAULT_URL = "https://appfeedlight.bhaskar.com/";
    public static final String BASE_URL = (InitApplication.getInstance() == null) ? LOGIN_DEFAULT_URL : InitApplication.getInstance().getLoginBaseUrl(LOGIN_DEFAULT_URL);

    public static final String DEFAULT_FEED_BASE_URL = "https://appfeedlight.bhaskar.com/appFeedV3/";
    public static final String FEED_BASE_URL = (InitApplication.getInstance() == null) ? DEFAULT_FEED_BASE_URL : InitApplication.getInstance().getBaseUrl(DEFAULT_FEED_BASE_URL);

    public static final String DEFAULT_LOG_FEED_BASE_URL = "https://appfeedlight.bhaskar.com/appFeedV3/";
    public static final String LOG_FEED_BASE_URL = (InitApplication.getInstance() == null) ? DEFAULT_LOG_FEED_BASE_URL : InitApplication.getInstance().getLogFeedUrl(DEFAULT_LOG_FEED_BASE_URL);

    //public static final String LIVE_TV_BASEURL="http://staging.yupptv.in/";
    public static final String LIVE_TV_BASEURL="https://www.yupptv.in/";

    // Tracking Urls
    public static final String DOMAIN = "http://android.divyabhaskar.co.in/";
    public static final String LOCATION_URL = BASE_URL + "CommonAPI/location/";
    public static final String TOGGLE_NOTIFY_URL = BASE_URL + "webapiV3/toggle_notification/";
    public static final String SESSION_URL = BASE_URL + "CommonAPI/locationsession/";
    public final static String PUSH_IMAGE_PREFIX = BASE_URL + "appFeedV3/NewsImg/";
    public final static String PUSH_VIDEO_PREFIX = BASE_URL + "appFeedV3/Dbvideos/Item/";

    public static final String WISDOM_URL = "https://realtime.bhaskar.com/article_app_data.php";
    public static final String WISDOM_NOTIFICATION_URL = "https://realtime.bhaskar.com/app_notification.php";
    public static final String WISDOM_APP_SOURCE_URL = "https://realtime.bhaskar.com/app_source_tracking.php";

    public static final String RASHIFAL_DETAIL_URL = "Numerology/3776/";

    public static final String DEFAULT_VIDEO_ACTION = "videoaction/";
    public static final String DEFAULT_VIDEO_PROVIDER = "ProviderVideos/960/";
    public static final String DEFAULT_DIVYA_VIDEO_REC_URL = "GuruvaniRecVideo/960/";

    public static final String VIDEO_RECOMMENDATION_URL = "VideoRecomendation/";

    public static String AUTO_START_URL = "https://i10.dainikbhaskar.com/images/appfeed/autostart/auto_start_divya.gif";
    public static String AUTO_START_2_URL = "https://i10.dainikbhaskar.com/images/appfeed/autostart/auto_start_divya_2.gif";
}
