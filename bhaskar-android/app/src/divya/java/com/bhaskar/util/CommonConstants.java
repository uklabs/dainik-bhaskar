package com.bhaskar.util;

public class CommonConstants {
    public static final String CHANNEL_ID = "960";
    public static final String BHASKAR_APP_ID = "4";

    public static String CONTENT_URL_VALUE = "https://www.divyabhaskar.co.in/";
    public static final String TRACKING_PREFIX = "DVB";
    public static final String FEED_BACK_EMAIL_ID = "divyaapp@dainikbhaskar.com";

    public static final String EVENT_LABEL = "dvb";

    public static final String CHANNEL_NEWS = "960";
    public static final String CHANNEL_BUSINESS = "4371";
    public static final String CHANNEL_RELIGION = "3776";

    public static final String APP_SHARE_URL = "https://divyabhaskar.page.link/applink";

    //Ads Publisher App ID
    public static final String ADMOB_APP_ID = "ca-app-pub-3985955077200088~8965948255";
    public static final String DFP_PUBLISHER_ID_FOR_CONSENT = "131958808";

    /*app widget & sticky api feed url*/
    public static String APP_WIDGET_STICKY_FEED_URL = "https://appfeedlight.bhaskar.com/appFeedV3/WidgetNews/960/0/1/";
    public static final String USER_AGENT = "DivyaBhaskarAndroid";

    public static String rashifalTabTitles[] = new String[]{"દૈનિક", "સાપ્તાહિક", "માસિક", "વાર્ષિક"};
    public static String rashifalTabTitlesEnglish[] = new String[]{"Daily", "Weekly", "Monthly", "Yearly"};
}
