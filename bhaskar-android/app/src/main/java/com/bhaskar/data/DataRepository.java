package com.bhaskar.data;

import androidx.lifecycle.MutableLiveData;

import com.bhaskar.data.api.ApiClient;
import com.bhaskar.data.api.ApiHelper;
import com.bhaskar.data.model.MeraPageCategoryResponse;
import com.bhaskar.data.model.NewsResponse;
import com.bhaskar.data.model.PhotoResponse;
import com.db.data.models.PhotoDetailInfo;
import com.db.divya_new.articlePage.ArticleCommonInfo;
import com.db.divya_new.data.CatHome2;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataRepository {
    private static final String TAG = DataRepository.class.getName();
    private static DataRepository dataRepository;
    private ApiHelper apiHelper;

    public static DataRepository getInstance() {
        if (dataRepository == null) {
            dataRepository = new DataRepository();
        }
        return dataRepository;
    }

    private DataRepository() {
        apiHelper = ApiClient.createService(ApiHelper.class);

    }

    /**
     * For home page news list
     *
     * @param url
     * @param mutableLiveData
     * @return
     */
    public MutableLiveData<List<CatHome2>> getHomeNews(String url, MutableLiveData<List<CatHome2>> mutableLiveData) {
        apiHelper.getHomeNewsList(url).enqueue(new Callback<List<CatHome2>>() {
            @Override
            public void onResponse(Call<List<CatHome2>> call,
                                   Response<List<CatHome2>> response) {
                if (response.isSuccessful()) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<CatHome2>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    /**
     * For Mera page Interest categories
     *
     * @param url
     * @param mutableLiveData
     * @return
     */
    public MutableLiveData<MeraPageCategoryResponse> getInterestCategories(String url, MutableLiveData<MeraPageCategoryResponse> mutableLiveData) {
        apiHelper.getMeraPageInterestCategories(url).enqueue(new Callback<MeraPageCategoryResponse>() {
            @Override
            public void onResponse(Call<MeraPageCategoryResponse> call,
                                   Response<MeraPageCategoryResponse> response) {
                if (response.isSuccessful()) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<MeraPageCategoryResponse> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    /**
     * For Mera page News
     *
     * @param url
     * @param mutableLiveData
     * @return
     */
    public MutableLiveData<NewsResponse> getMeraPageNews(String url, MutableLiveData<NewsResponse> mutableLiveData) {
        apiHelper.getNews(url).enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call,
                                   Response<NewsResponse> response) {
                if (response.isSuccessful()) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }


    /**
     * For News List
     *
     * @param url
     * @param mutableLiveData
     * @return
     */
    public MutableLiveData<NewsResponse> getNewsList(String url, MutableLiveData<NewsResponse> mutableLiveData) {
        apiHelper.getNews(url).enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call,
                                   Response<NewsResponse> response) {
                if (response.isSuccessful()) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

    /**
     * For Photo Gallary List
     *
     * @param url
     * @param mutableLiveData
     * @return
     */
    public MutableLiveData<PhotoResponse> getPhotoGallaryList(String url, MutableLiveData<PhotoResponse> mutableLiveData) {
        apiHelper.getPhotos(url).enqueue(new Callback<PhotoResponse>() {
            @Override
            public void onResponse(Call<PhotoResponse> call,
                                   Response<PhotoResponse> response) {
                if (response.isSuccessful()) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<PhotoResponse> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }
    /**
     * For Photo Gallary List
     *
     * @param url
     * @param mutableLiveData
     * @return
     */
    public MutableLiveData<List<PhotoDetailInfo>> getPhotoDetailGallaryList(String url, MutableLiveData<List<PhotoDetailInfo>> mutableLiveData) {
        apiHelper.getPhotosDetailList(url).enqueue(new Callback<List<PhotoDetailInfo>>() {
            @Override
            public void onResponse(Call<List<PhotoDetailInfo>> call,
                                   Response<List<PhotoDetailInfo>> response) {
                if (response.isSuccessful()) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<PhotoDetailInfo>> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }


    /**
     * For Photo Gallary List
     *
     * @param url
     * @param mutableLiveData
     * @return
     */
    public MutableLiveData<ArticleCommonInfo> getPhotoDetailGallaryDetail(String url, MutableLiveData<ArticleCommonInfo> mutableLiveData) {
        apiHelper.getPhotosDetail(url).enqueue(new Callback<ArticleCommonInfo>() {
            @Override
            public void onResponse(Call<ArticleCommonInfo> call,
                                   Response<ArticleCommonInfo> response) {
                if (response.isSuccessful()) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ArticleCommonInfo> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });
        return mutableLiveData;
    }

}
