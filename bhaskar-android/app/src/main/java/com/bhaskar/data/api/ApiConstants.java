package com.bhaskar.data.api;

public interface ApiConstants {
    String APP_FEED_BASE_URL = "https://appfeedlight.bhaskar.com/appFeedV3/";

    //URLS
    String MERA_PAGE_CATEGORY_URL= "MyPageCategory/%s/";

    String MERA_PAGE_NEWS_URL= "MyPage/%s/";

}
