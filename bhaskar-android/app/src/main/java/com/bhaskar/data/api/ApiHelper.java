package com.bhaskar.data.api;

import com.bhaskar.data.model.MeraPageCategoryResponse;
import com.bhaskar.data.model.NewsResponse;
import com.bhaskar.data.model.PhotoResponse;
import com.db.audioBulletin.model.NewsBulletinListResponse;
import com.db.audioBulletin.model.NewsBulletinResponse;
import com.db.data.models.PhotoDetailInfo;
import com.db.divya_new.articlePage.ArticleCommonInfo;
import com.db.divya_new.data.CatHome2;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiHelper {

    @GET
    Call<List<CatHome2>> getHomeNewsList(@Url String url);

    @GET
    Call<MeraPageCategoryResponse> getMeraPageInterestCategories(@Url String url);

    @GET
    Call<NewsResponse> getNews(@Url String url);

    @GET
    Call<PhotoResponse> getPhotos(@Url String url);

    @GET
    Call<List<PhotoDetailInfo>> getPhotosDetailList(@Url String url);

    @GET
    Call<ArticleCommonInfo> getPhotosDetail(@Url String url);

    @GET
    Call<NewsBulletinListResponse> getNewsBulletinList(@Url String url);

    @GET
    Call<NewsBulletinResponse> getNewsBulletinDetail(@Url String url);
}
