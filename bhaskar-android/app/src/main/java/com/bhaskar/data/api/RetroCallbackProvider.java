package com.bhaskar.data.api;

import android.util.Log;

import androidx.annotation.NonNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RetroCallbackProvider<T> implements Callback<T> {

    public abstract void onSuccess(Call<T> call, @NonNull T response);

    public abstract void onFailed(Call<T> call, Throwable t);

    private final String TAG = "RetroCallbackProvider";

    private Object tag;

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public Object getTag() {
        return tag;
    }

    @Override
    public final void onResponse(Call<T> call, @NonNull Response<T> response) {
        try {
            T body = response.body();
            if (body != null) {
                Log.d(TAG, response + "");
                onSuccess(call, body);
            } else {
                onFailed(call, new NullPointerException());
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }

    @Override
    public final void onFailure(Call<T> call, Throwable t) {
        try {
            Log.e(TAG, "onFailure: " + call.request().url());
            onFailed(call, t);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }
}
