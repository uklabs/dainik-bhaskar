package com.bhaskar.data.local.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.db.data.models.BrandInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.PrimeBrandInfo;
import com.db.database.BottomNavInfo;
import com.db.database.BottomNavInfoDao;
import com.db.database.BrandInfoDao;
import com.db.database.CategoryInfoDao;
import com.db.database.ClickInfo;
import com.db.database.ClickInfoDao;
import com.db.database.Converters;
import com.db.database.HeaderNavInfo;
import com.db.database.HeaderNavInfoDao;
import com.db.database.PrimeBrandInfoDao;
import com.db.database.TabInfo;
import com.db.database.TabInfoDao;

import javax.inject.Inject;

@Database(entities = {CategoryInfo.class, BrandInfo.class, PrimeBrandInfo.class, ClickInfo.class, TabInfo.class, HeaderNavInfo.class, BottomNavInfo.class}, version = 10, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract CategoryInfoDao categoryInfoDao();

    public abstract BrandInfoDao brandInfoDao();

    public abstract PrimeBrandInfoDao primeBrandInfoDao();

    public abstract ClickInfoDao clickInfoDao();

    public abstract TabInfoDao tabInfoDao();

    public abstract HeaderNavInfoDao headerNavInfoDao();

    public abstract BottomNavInfoDao bottomNavInfoDao();
}