package com.bhaskar.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class LoginPreferences {

    public interface Keys {
        String CONSENT_ACCEPT = "consent_accept";
        String CURRENT_USER = "currentUser";
        String GOOGLE_WEB_CLIENT = "google_web_client";
        String MOBILE = "mobile";
        String DB_ID = "db_id";
        String DEVICE_ID = "device_id";
        String APP_ID = "app_ID";
        String IS_FORGOT = "isForgot";
        String IS_FACEBOOK_LOGIN_DISABLED = "facebook_login_disabled";
    }

    public static final String NAME = "Login_Preference";
    private static LoginPreferences instance = null;
    private SharedPreferences preferences = null;

    private LoginPreferences(Context ctx) {
        if (ctx != null) {
            preferences = ctx.getApplicationContext().getSharedPreferences(NAME, Context.MODE_PRIVATE);
        }
    }

    public static LoginPreferences getInstance(Context ctx) {
        if (instance == null && ctx !=null) {
            instance = new LoginPreferences(ctx);
        }
        return instance;
    }

    public void setIntValue(String key, int value) {
        if (preferences != null)
            preferences.edit().putInt(key, value).commit();
    }

    public int getIntValue(String key, int defaultValue) {
        if (preferences != null)
            return preferences.getInt(key, defaultValue);
        else
            return defaultValue;
    }

    public void setStringValue(String key, String value) {
        if (preferences != null)
            preferences.edit().putString(key, value).commit();
    }

    public String getStringValue(String key, String defaultValue) {
        if (preferences != null)
            return preferences.getString(key, defaultValue);
        else
            return defaultValue;
    }

    public void setBooleanValue(String key, Boolean value) {
        if (preferences != null)
            preferences.edit().putBoolean(key, value).commit();
    }

    public Boolean getBooleanValue(String key, Boolean defaultValue) {
        if (preferences != null)
            return preferences.getBoolean(key, defaultValue);
        else
            return defaultValue;
    }


    public synchronized void saveDataIntoPreferences(Object dataObject, String key) {
        try {
            Gson gson = new Gson();
            String json = gson.toJson(dataObject);
            SharedPreferences.Editor prefsEditor = preferences.edit();
            prefsEditor.putString(key, json);
            prefsEditor.commit();
        } catch (Exception ignored) {

        }

    }

    /**
     * Return the dataObject of type typeOfT class if successfull. if fail to get data from SharedPreferences then returns null
     */
    public synchronized <T> T getDataFromPreferences(String key, Type typeOfT) {
        T dataObject = null;
        try {
            Gson gson = new Gson();
            String jsonString = preferences.getString(key, "");
            dataObject = gson.fromJson(jsonString, typeOfT);
        } catch (Exception ignored) {
        }

        return dataObject;
    }

}
