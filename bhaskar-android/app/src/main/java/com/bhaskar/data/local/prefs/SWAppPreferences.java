package com.bhaskar.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class SWAppPreferences {

    public interface Keys {
        String OFFER = "Offer";
        String OFFER_DISABLED = "Offer_DISABLED_V2";
        String OFFER_TERMS_DISABLED = "OFFER_TERMS_DISABLED_%s";
        String OFFER_VERSION = "Offer_VERSION";
        String CURRENT_OFFER_VERSION = "Current_Offer_Version";
        String REFF_SENT = "REFF_SENT";
        String SHOW_COUPON_ENTRY_POINT = "showCoupon";
    }
    private static final String NAME = "SW_Preference";
    private static SWAppPreferences instance = null;
    private SharedPreferences preferences = null;

    private SWAppPreferences(Context ctx) {
        if (ctx != null) {
            preferences = ctx.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        }
    }

    public static SWAppPreferences getInstance(Context ctx) {
        if (instance == null) {
            instance = new SWAppPreferences(ctx);
        }
        return instance;
    }

    public void setBooleanValue(String key, Boolean value) {
        if (preferences != null)
            preferences.edit().putBoolean(key, value).commit();
    }

    public Boolean getBooleanValue(String key, Boolean defaultValue) {
        if (preferences != null)
            return preferences.getBoolean(key, defaultValue);
        else
            return defaultValue;
    }
    public String getStringValue(String key, String defaultValue) {
        if (preferences != null)
            return preferences.getString(key, defaultValue);
        else
            return defaultValue;
    }
    public void setStringValue(String key, String value) {
        if (preferences != null)
            preferences.edit().putString(key, value).commit();
    }




}
