package com.bhaskar.data.model

import com.bhaskar.util.GsonProguardMarker
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseResponse : GsonProguardMarker {
    @SerializedName("code")
    @Expose
    var code: Int = 0
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("time")
    @Expose
    var time: String? = null

}