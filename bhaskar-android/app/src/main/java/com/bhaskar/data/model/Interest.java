package com.bhaskar.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Interest implements Serializable {

    @SerializedName("cat_id")
    @Expose
    private int id;
    @SerializedName("cat_name")
    @Expose
    private String name;

    @SerializedName("color_code")
    @Expose
    private String color;

    private boolean isSelected;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Interest{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", is_selected=" + isSelected +
                '}';
    }
}