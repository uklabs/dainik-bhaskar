package com.bhaskar.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MeraPageCategoryResponse {

    @SerializedName("feed")
    @Expose
    private String feed;
    @SerializedName("data")
    @Expose
    private List<Interest> data = null;

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }

    public List<Interest> getData() {
        return data;
    }

    public void setData(List<Interest> data) {
        this.data = data;
    }
}
