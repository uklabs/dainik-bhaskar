package com.bhaskar.data.model;

import com.db.data.models.NewsListInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class NewsResponse {

    @SerializedName("count")
    @Expose
    private String count= "0";
    @SerializedName("date")
    @Expose
    private String date = "";
    @SerializedName("flicker_active")
    @Expose
    private String flickerActive = "";
    @SerializedName("web_banner")
    @Expose
    private JSONArray webBanner;
    @SerializedName("feed")
    @Expose
    private List<NewsListInfo> feed = new CopyOnWriteArrayList<>();

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<NewsListInfo> getFeed() {
        return feed;
    }

    public void setFeed(List<NewsListInfo> feed) {
        this.feed = feed;
    }

    public String getFlickerActive() {
        return flickerActive;
    }

    public void setFlickerActive(String flickerActive) {
        this.flickerActive = flickerActive;
    }

    public JSONArray getWebBanner() {
        return webBanner;
    }

    public void setWebBanner(JSONArray webBanner) {
        this.webBanner = webBanner;
    }
}
