package com.bhaskar.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Offer implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("displayname")
    @Expose
    public String displayname;
    @SerializedName("banner_image")
    @Expose
    public String bannerImage;
    @SerializedName("dailog_banner_img")
    @Expose
    public String dailog_banner_img;
    @SerializedName("dailog_action")
    @Expose
    public String dailog_action;
    @SerializedName("show_dailog")
    @Expose
    public String showDialog;
    @SerializedName("web_url")
    @Expose
    public String webUrl;
    @SerializedName("dailog_tnc")
    @Expose
    public String dialogTnc;
    @SerializedName("tnc")
    @Expose
    public String tnc;
    @SerializedName("faq")
    @Expose
    public String faq;
    @SerializedName("htp")
    @Expose
    public String htp;
    @SerializedName("brand_id")
    @Expose
    public String brandId;
    @SerializedName("action")
    @Expose
    public String action;
    @SerializedName("ga_event_label")
    @Expose
    public String gaEventLabel;
    @SerializedName("ga_screen")
    @Expose
    public String gaScreen;
    @SerializedName("ga_article")
    @Expose
    public String gaArticle;
    @SerializedName("extra_values")
    @Expose
    public String extraValues;
    @SerializedName("datetime")
    @Expose
    public String datetime;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("offer_expire")
    @Expose
    public String offerExpire = "";



}
