package com.bhaskar.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProfileInfo implements Serializable {

    @SerializedName("isUserLoggedIn")
    public boolean isUserLoggedIn = false;

    @SerializedName("uID")
    public String uID;

    @SerializedName("uuID")
    public String uuID;

    @SerializedName("loginType")
    public String loginType;

    @SerializedName("fbAuth")
    public String fbAuth;

    @SerializedName("googleAuth")
    public String googleAuth;

    @SerializedName("mobile")
    public String mobile;

    @SerializedName("country")
    public String country;

    @SerializedName("state")
    public String state;

    @SerializedName("city")
    public String city;

    @SerializedName("zipcode")
    public String zipcode;

    @SerializedName("countryCode")
    public String countryCode = "91";

    @SerializedName("password")
    public String password;

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("gender")
    public String gender = "";

    @SerializedName("profilePic")
    public String profilePic;

    @SerializedName("dob")
    public String dob;

    public ProfileInfo(ProfileInfo mProfileInfo) {
        this.isUserLoggedIn = mProfileInfo.isUserLoggedIn;
        this.uID = mProfileInfo.uID;
        this.uuID = mProfileInfo.uuID;
        this.loginType = mProfileInfo.loginType;
        this.fbAuth = mProfileInfo.fbAuth;
        this.googleAuth = mProfileInfo.googleAuth;
        this.mobile = mProfileInfo.mobile;
        this.country = mProfileInfo.country;
        this.state = mProfileInfo.state;
        this.city = mProfileInfo.city;
        this.zipcode = mProfileInfo.zipcode;
        this.countryCode = mProfileInfo.countryCode;
        this.password = mProfileInfo.password;
        this.name = mProfileInfo.name;
        this.email = mProfileInfo.email;
        this.gender = mProfileInfo.gender;
        this.profilePic = mProfileInfo.profilePic;
        this.dob = mProfileInfo.dob;
    }

    public ProfileInfo() {

    }
}
