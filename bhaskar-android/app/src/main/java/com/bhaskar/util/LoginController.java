package com.bhaskar.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import androidx.annotation.IntDef;
import androidx.annotation.StringDef;

import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.R;
import com.google.android.gms.analytics.Tracker;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;

public class LoginController {
    public static final int LOGIN_POPUP_REQUEST_CODE = 1267;
    // login types
    private final static String TAG = LoginController.class.getSimpleName();
    // variable declaration
    //Static Properties
    private static LoginController _LoginController;
    //Instance Properties
    private static ProfileInfo mCurrentUser = null;
    private String googleWebClient;
    private Context context;
    private Tracker tracker;

    /**
     * private constructor make it to be Singleton class
     */
    private LoginController() {

    }

    public static HashMap<String, Object> getUserDataMap() {
        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();

        HashMap<String, Object> userMap = new HashMap<>();
        if (profileInfo != null) {
            userMap.put("email", profileInfo.email);
            userMap.put("mobile", profileInfo.mobile);
            userMap.put("isUserLoggedIn", profileInfo.isUserLoggedIn);
        }
        return userMap;
    }

    /**
     * method to create a threadsafe singleton class instance
     *
     * @return a thread safe singleton object of model manager
     */
    public static synchronized LoginController loginController() {
        if (_LoginController == null) {
            _LoginController = new LoginController();
        }
        return _LoginController;
    }


    public void getLoginIntent(Activity context, String dbID, String deviceID, int requestCode) {
        if (TextUtils.isEmpty(googleWebClient))
            googleWebClient = LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.GOOGLE_WEB_CLIENT, String.class);

        ModuleUtilAll.Companion.getInstance(context).launchModule(ModuleUtilAll.LOGIN_PAGE, context, requestCode);

//        return LoginManagerActivity.getIntent(context, googleWebClient, dbID, deviceID);
    }

    public void showProfile(Activity activity) {
        if (LoginController.loginController().isUserLoggedIn()) {
//            try {
//                Intent intent = new Intent(activity, Class.forName("com.bhaskar.loginmanager.ProfileManagerActivity"));
//                activity.startActivityForResult(intent, 101);
//            } catch (ClassNotFoundException ignore) {
//            }
//            activity.startActivityForResult(ProfileManagerActivity.getIntent(this), 101);

            ModuleUtilAll.Companion.getInstance(activity).launchModule(ModuleUtilAll.LOGIN_PROFILE, activity, 101);

        } else {
            getLoginIntent(activity, TrackingData.getDBId(activity), TrackingData.getDeviceId(activity), 101);
        }
    }

    /**
     * to initialize the singleton object
     */

    public void initAppFeedBaseURL(String baseURL) {
//        LoginUrls.LOGIN_FEED_BASE_URL = baseURL;
    }

    public void setFacebookLoginCheck(boolean isDisabled) {
        LoginPreferences.getInstance(context).setBooleanValue(LoginPreferences.Keys.IS_FACEBOOK_LOGIN_DISABLED, isDisabled);
    }

    public void initializeModelManager(Context context, String gWebClient, Tracker tracker) {
        this.context = context;
        mCurrentUser = LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        this.googleWebClient = (TextUtils.isEmpty(gWebClient) ? context.getString(R.string.default_web_client_id_static) : gWebClient);
        LoginPreferences.getInstance(context).saveDataIntoPreferences(this.googleWebClient, LoginPreferences.Keys.GOOGLE_WEB_CLIENT);
        this.tracker = tracker;
    }

    /**
     * getter and setter method for current user
     *
     * @return {@link ProfileInfo} if user already logged in, null otherwise
     */
    public synchronized ProfileInfo getCurrentUser() {
        mCurrentUser = LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        return mCurrentUser;
    }

    public synchronized void setCurrentUser(ProfileInfo o) {
        mCurrentUser = o;
        archiveCurrentUser();
    }

    public boolean isUserLoggedIn() {
        mCurrentUser = LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        return (mCurrentUser != null && mCurrentUser.isUserLoggedIn);
    }

    /**
     * Stores {@link ProfileInfo} to the share preferences and synchronize sharedpreferece
     */
    private synchronized void archiveCurrentUser() {
        LoginPreferences.getInstance(context).saveDataIntoPreferences(mCurrentUser, LoginPreferences.Keys.CURRENT_USER);
    }

    private Tracker getDefaultTracker() {
        return tracker;
    }

    public String getLoginType(ProfileInfo profileInfo) {
        if (profileInfo == null) {
            return AppFlyerConst.GALabel.MOBILE;
        } else if (profileInfo.loginType.equalsIgnoreCase(LoginType.FB)) {
            return AppFlyerConst.GALabel.FB;
        } else if (profileInfo.loginType.equalsIgnoreCase(LoginType.GOOGLE)) {
            return AppFlyerConst.GALabel.GOOGLE;
        }
        return AppFlyerConst.GALabel.MOBILE;
    }

    public void sendTrackingForLogin(String status, String type) {
        switch (status) {
            case AppFlyerConst.GAAction.REGISTER:
                switch (type) {
                    case AppFlyerConst.GALabel.FB:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.REGISTER, AppFlyerConst.GALabel.FB, "");
                        break;
                    case AppFlyerConst.GALabel.GOOGLE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.REGISTER, AppFlyerConst.GALabel.GOOGLE, "");
                        break;
                    case AppFlyerConst.GALabel.MOBILE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.REGISTER, AppFlyerConst.GALabel.MOBILE, "");
                        break;
                    case AppFlyerConst.GALabel.UPDATE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.REGISTER, AppFlyerConst.GALabel.UPDATE, "");
                        break;
                    case AppFlyerConst.GALabel.OTP:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.REGISTER, AppFlyerConst.GALabel.OTP, "");
                        break;
                }
                break;

            case AppFlyerConst.GAAction.LOGIN:
                switch (type) {
                    case AppFlyerConst.GALabel.FB:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.LOGIN, AppFlyerConst.GALabel.FB, "");
                        break;
                    case AppFlyerConst.GALabel.GOOGLE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.LOGIN, AppFlyerConst.GALabel.GOOGLE, "");
                        break;
                    case AppFlyerConst.GALabel.MOBILE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.LOGIN, AppFlyerConst.GALabel.MOBILE, "");
                        break;
                    case AppFlyerConst.GALabel.UPDATE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.LOGIN, AppFlyerConst.GALabel.UPDATE, "");
                        break;
                    case AppFlyerConst.GALabel.OTP:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.LOGIN, AppFlyerConst.GALabel.OTP, "");
                        break;
                }
                break;
            case AppFlyerConst.GAAction.LOGOUT:
                switch (type) {
                    case AppFlyerConst.GALabel.FB:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.LOGOUT, AppFlyerConst.GALabel.FB, "");
                        break;
                    case AppFlyerConst.GALabel.GOOGLE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.LOGOUT, AppFlyerConst.GALabel.GOOGLE, "");
                        break;
                    case AppFlyerConst.GALabel.MOBILE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.LOGOUT, AppFlyerConst.GALabel.MOBILE, "");
                        break;
                    case AppFlyerConst.GALabel.UPDATE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.LOGOUT, AppFlyerConst.GALabel.UPDATE, "");
                        break;
                    case AppFlyerConst.GALabel.OTP:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.LOGOUT, AppFlyerConst.GALabel.OTP, "");
                        break;
                }
                break;
            case AppFlyerConst.GAAction.SUCCESS:
                switch (type) {
                    case AppFlyerConst.GALabel.FB:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.FB, "");
                        break;
                    case AppFlyerConst.GALabel.GOOGLE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.GOOGLE, "");
                        break;
                    case AppFlyerConst.GALabel.MOBILE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.MOBILE, "");
                        break;
                    case AppFlyerConst.GALabel.UPDATE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.UPDATE, "");
                        break;
                    case AppFlyerConst.GALabel.OTP:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.OTP, "");
                        break;
                }
                break;
            case AppFlyerConst.GAAction.FAIL:
                switch (type) {
                    case AppFlyerConst.GALabel.FB:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.FB, "");
                        break;
                    case AppFlyerConst.GALabel.GOOGLE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.GOOGLE, "");
                        break;
                    case AppFlyerConst.GALabel.MOBILE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.MOBILE, "");
                        break;
                    case AppFlyerConst.GALabel.UPDATE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.UPDATE, "");
                        break;
                    case AppFlyerConst.GALabel.OTP:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.OTP, "");
                        break;
                }
                break;
            case AppFlyerConst.GAAction.SKIP:
                switch (type) {
                    case AppFlyerConst.GALabel.FB:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.FB, "");
                        break;
                    case AppFlyerConst.GALabel.GOOGLE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.GOOGLE, "");
                        break;
                    case AppFlyerConst.GALabel.MOBILE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.MOBILE, "");
                        break;
                    case AppFlyerConst.GALabel.UPDATE:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.UPDATE, "");
                        break;
                    case AppFlyerConst.GALabel.OTP:
                        Tracking.trackGAEvent(context, getDefaultTracker(), AppFlyerConst.GACategory.ACCOUNT, AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.OTP, "");
                        break;
                }
                break;


        }
    }

    public void sendGAScreen(Context context, String gaScreen) {
        Tracking.trackGAScreen(context, getDefaultTracker(), gaScreen, "", "", "");
    }


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ResultType.SUCCESS, ResultType.SKIP, ResultType.FAIL})
    public @interface ResultType {
        int SUCCESS = 1;
        int SKIP = 2;
        int FAIL = 3;
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({LoginType.FB, LoginType.GOOGLE, LoginType.MANUAL})
    public @interface LoginType {
        String FB = "F";
        String GOOGLE = "G";
        String MANUAL = "M";
    }


}
