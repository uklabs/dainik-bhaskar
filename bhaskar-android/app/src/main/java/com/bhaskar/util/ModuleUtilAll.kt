package com.bhaskar.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.bhaskar.appscommon.tracking.TrackingData
import com.bhaskar.data.local.prefs.LoginPreferences
import com.bhaskar.data.local.prefs.SWAppPreferences
import com.bhaskar.data.model.Offer
import com.bhaskar.view.ui.module.ModuleDownloadActivity
import com.db.util.AppPreferences
import com.db.util.QuickPreferences
import com.db.util.Systr
import com.google.android.play.core.splitinstall.SplitInstallManager
import com.google.android.play.core.splitinstall.SplitInstallManagerFactory

class ModuleUtilAll private constructor(context: Context) {

    private val splitInstallManager: SplitInstallManager = SplitInstallManagerFactory.create(context)

    companion object {

        const val EPAPER_SPLASH = 1
        const val SHARE_WIN_PAGE = 2
        const val COMMENT_PAGE = 3
        const val LOGIN_PAGE = 4
        const val LOGIN_PROFILE = 5

        private const val EPAPER = "epaper";
        private const val SHARE_WIN = "sharewin";
        private const val COMMENT = "commentsdk";
        private const val LOGIN = "loginmanager";

        private var instance: ModuleUtilAll? = null

        fun getInstance(context: Context): ModuleUtilAll? {
            synchronized(ModuleUtilAll::class.java) {
                if (instance == null) {
                    instance = ModuleUtilAll(context)
                }
            }

            return instance
        }
    }

    private fun moduleName(type: Int): String {
        var moduleName = "";
        when (type) {
            EPAPER_SPLASH -> moduleName = EPAPER
            SHARE_WIN_PAGE -> moduleName = SHARE_WIN
            COMMENT_PAGE -> moduleName = COMMENT
            LOGIN_PAGE -> moduleName = LOGIN
            LOGIN_PROFILE -> moduleName = LOGIN
        }

        return moduleName
    }

    fun launchModule(type: Int, context: Context) {
        val name = moduleName(type)
        if (splitInstallManager.installedModules.contains(name)) {
            Systr.println("MODULE : already installed : $name")
            openModule(type, context)
        } else {
            Systr.println("MODULE : download start : $name")
            downloadModule(type, name, context)
        }
    }

    fun launchCommentModule(type: Int, context: Context, storyUrl: String, storyId: String, title: String, color: String) {
        val name = moduleName(type)
        if (splitInstallManager.installedModules.contains(name)) {
            Systr.println("MODULE : already installed : $name")
            commentOpen(context, storyUrl, storyId, title, color)
        } else {
            Systr.println("MODULE : download start : $name")
            val intent = downloadModuleIntent(type, name, 0, context)
            intent.putExtra("story_url", storyUrl)
            intent.putExtra("story_id", storyId)
            intent.putExtra("story_title", title)
            intent.putExtra("color", color)
            context.startActivity(intent)
        }
    }

    fun launchShareWinModule(type: Int, activity: Activity, requestCode: Int, offer: Offer, shouldShowCoupon: Boolean) {
        val name = moduleName(type)
        if (splitInstallManager.installedModules.contains(name)) {
            Systr.println("MODULE : already installed : $name")
            shareWinOpen(activity, requestCode, offer, shouldShowCoupon)
        } else {
            Systr.println("MODULE : download start : $name")
            val intent = downloadModuleIntent(type, name, 0, activity)
            intent.putExtra(SWAppPreferences.Keys.OFFER, offer)
            intent.putExtra(SWAppPreferences.Keys.SHOW_COUPON_ENTRY_POINT, shouldShowCoupon)
            activity.startActivity(intent)
        }
    }

    fun launchModule(type: Int, activity: Activity, requestCode: Int) {
        val name = moduleName(type)
        if (splitInstallManager.installedModules.contains(name)) {
            Systr.println("MODULE : already installed : $name")
            openModule(type, activity, requestCode)
        } else {
            Systr.println("MODULE : download start : $name")
            downloadModule(type, name, activity, requestCode)
        }
    }

    private fun downloadModule(type: Int, name: String, context: Context) {
        val intent = downloadModuleIntent(type, name, 0, context)
        context.startActivity(intent)
    }

    private fun downloadModule(type: Int, name: String, activity: Activity, requestCode: Int) {
        val intent = downloadModuleIntent(type, name, requestCode, activity)
        activity.startActivityForResult(intent, requestCode)
    }

    private fun downloadModuleIntent(type: Int, name: String, requestCode: Int, context: Context): Intent {
        val list = ArrayList<String>()
        list.add(name)
        if (name.equals(COMMENT) && !splitInstallManager.installedModules.contains(LOGIN)) {
            list.add(LOGIN)
        }

        var title = ""
        var desc = ""
        when (name) {
            EPAPER -> {
                title = "DB-Epaper"
                desc = "Get ready, we are arranging E-paper for you!"
            }
            COMMENT -> {
                title = "Comments"
                desc = "Get ready, we are arranging Comments for you!"
            }
            LOGIN -> {
                title = "Login"
                desc = "Get ready, we are arranging Login for you!"
            }
            SHARE_WIN -> {
                title = "Share Win"
                desc = "Get ready, we are arranging Share Win for you!"
            }
        }

        return ModuleDownloadActivity.newIntent(context, title, desc, list, type, requestCode)
    }

    fun openModule(type: Int, activity: Activity, requestCode: Int) {
        when (type) {
            EPAPER_SPLASH ->
                epaperOpen(activity)
            COMMENT_PAGE ->
                commentOpen(activity, "", "", "", "")
            LOGIN_PAGE ->
                loginOpen(activity, requestCode)
            LOGIN_PROFILE ->
                loginProfileOpen(activity, requestCode)
            SHARE_WIN_PAGE ->
                shareWinOpen(activity, requestCode, Offer(), false)
        }
    }

    fun openModule(type: Int, context: Context) {
        when (type) {
            EPAPER_SPLASH ->
                epaperOpen(context)
            COMMENT_PAGE ->
                commentOpen(context, "", "", "", "")
        }
    }

    private fun epaperOpen(context: Context) {
        try {
            val intent = Intent(context, Class.forName("com.bhaskar.epaper.ui.EpaperMainActivity"))
            context.startActivity(intent)
        } catch (e: Exception) {
        }
    }

    private fun shareWinOpen(activity: Activity, requestCode: Int, offer: Offer, shouldShowCoupon: Boolean) {
        try {
            val intent = Intent(activity, Class.forName("com.bhaskar.sharewin.dashboard.SWDashboardActivity"))
            intent.putExtra(SWAppPreferences.Keys.OFFER, offer)
            intent.putExtra(SWAppPreferences.Keys.SHOW_COUPON_ENTRY_POINT, shouldShowCoupon)
            activity.startActivityForResult(intent, requestCode)
        } catch (e: Exception) {
        }
    }

    private fun commentOpen(context: Context, storyUrl: String, storyId: String, title: String, color: String) {
        try {
            val host = AppPreferences.getInstance(context).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN)
            val isLogin = LoginController.loginController().isUserLoggedIn

            val intent = Intent(context, Class.forName("com.bhaskar.comment.ui.DivyaCommentActivity"))
            intent.putExtra("is_login", isLogin)
            intent.putExtra("host", host)
            intent.putExtra("channel_slno", CommonConstants.CHANNEL_ID)
            intent.putExtra("loadMoreCount", 10)
            intent.putExtra("story_url", storyUrl)
            intent.putExtra("story_id", storyId)
            intent.putExtra("story_title", title)
            intent.putExtra("color", color)
            context.startActivity(intent)
        } catch (e: Exception) {
        }
    }

    private fun loginOpen(activity: Activity, requestCode: Int) {
        try {
            val googleWebClient = LoginPreferences.getInstance(activity)!!.getDataFromPreferences<String>(LoginPreferences.Keys.GOOGLE_WEB_CLIENT, String::class.java)
            val dbID = TrackingData.getDBId(activity)
            val deviceID = TrackingData.getDeviceId(activity)

            val intent = Intent(activity, Class.forName("com.bhaskar.login.LoginManagerActivity"))
            intent.putExtra("googleTokenId", googleWebClient)
            intent.putExtra("dbID", dbID)
            intent.putExtra("deviceID", deviceID)
            activity.startActivityForResult(intent, requestCode)
        } catch (e: Exception) {
        }
    }

    private fun loginProfileOpen(activity: Activity, requestCode: Int) {
        try {
            val intent = Intent(activity, Class.forName("com.bhaskar.login.ProfileManagerActivity"))
            activity.startActivityForResult(intent, requestCode)
        } catch (e: Exception) {
        }
    }
}
