package com.bhaskar.util;

import android.app.Activity;
import android.content.Context;

import com.bhaskar.data.model.Offer;
import com.google.android.gms.analytics.Tracker;

public class ShareWinController {
    private final static String TAG = ShareWinController.class.getSimpleName();
    private static ShareWinController _ShareWinController;
    private Context context;
    private String dbID;
    private String deviceID;
    private String appId;
    private Tracker tracking;

    /**
     * method to create a threadsafe singleton class instance
     *
     * @return a thread safe singleton object of model manager
     */
    public static synchronized ShareWinController shareWinController() {
        if (_ShareWinController == null) {
            _ShareWinController = new ShareWinController();
        }
        return _ShareWinController;
    }

    public Tracker getDefaultTracker() {
        return tracking;
    }

    /**
     * to initialize the singleton object
     */


    public void initializeController(Context context, String dbID, String deviceID, String appId) {
        this.context = context;
        this.dbID = dbID;
        this.deviceID = deviceID;
        this.appId = appId;
    }


    public void getDashboardIntent(Activity activity, Offer offer, boolean shouldShowCoupon, Tracker defaultTracker) {
        this.tracking = defaultTracker;
//        return SWDashboardActivity.getIntent(context, offer, shouldShowCoupon);
//        try {
//            return new Intent(context, Class.forName("com.bhaskar.sharewin.dashboard.SWDashboardActivity"));
//        } catch (ClassNotFoundException e) {
//        }
        ModuleUtilAll.Companion.getInstance(context).launchShareWinModule(ModuleUtilAll.SHARE_WIN_PAGE, activity, 999, offer, shouldShowCoupon);
    }

    public String getDBId() {
        return dbID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public String getAppId() {
        return appId;
    }
}
