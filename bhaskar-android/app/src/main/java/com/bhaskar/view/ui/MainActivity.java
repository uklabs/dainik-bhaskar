package com.bhaskar.view.ui;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.local.prefs.SWAppPreferences;
import com.bhaskar.data.model.Offer;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.Action;
import com.bhaskar.util.AppUrls;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.util.LoginController;
import com.bhaskar.util.ShareWinController;
import com.bhaskar.view.ui.myPage.MeraPageNewsFragment;
import com.bhaskar.view.ui.news.NewsListFragment;
import com.db.InitApplication;
import com.db.data.models.CategoryInfo;
import com.db.data.models.RelatedArticleInfo;
import com.db.data.models.fantasyMenu.MenuItem;
import com.db.data.source.server.BackgroundRequest;
import com.db.database.BottomNavInfo;
import com.db.database.DatabaseClient;
import com.db.database.HeaderNavInfo;
import com.db.divya_new.articlePage.ArticleActionListener;
import com.db.divya_new.articlePage.ArticleCommonInfo;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.dharamDarshan.Songs.SongService;
import com.db.divya_new.offers.OffersDialogFragment;
import com.db.divya_new.offers.OffersTermsFragment;
import com.db.home.ActivityUtil;
import com.db.home.BottomNavGridAdapter;
import com.db.home.NewsBriefListFragment2;
import com.db.home.NewsCategoryFragment;
import com.db.home.NewsFragment;
import com.db.home.PrimeFragmentNew;
import com.db.home.WapV2Fragment;
import com.db.home.fantasyMenu.FantasyFragment;
import com.db.homebanner.BannerViewHolder;
import com.db.inapp_message.InAppMessageClickListener;
import com.db.listeners.OnCategoryClickListener;
import com.db.listeners.OnDBVideoClickListener;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.listeners.OnRelatedArticleClickListener;
import com.db.main.BaseAppCompatActivity;
import com.db.news.PrefferedCityNewsListFragment;
import com.db.news.SearchActivity;
import com.db.receivers.ConnectivityReceiver;
import com.db.search.SearchFragmentDialog;
import com.db.util.AppPreferences;
import com.db.util.AppUpdateUtils;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.DbSharedPref;
import com.db.util.FragmentUtil;
import com.db.util.ImageUtil;
import com.db.util.JobUtil;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.db.util.ViewBottomBehavior;
import com.db.views.BoundedLinearLayout;
import com.google.android.gms.analytics.Tracker;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.inappmessaging.FirebaseInAppMessaging;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.view.View.GONE;

public class MainActivity extends BaseAppCompatActivity implements OnCategoryClickListener, OnDBVideoClickListener, OnFeedFetchFromServerListener, SearchFragmentDialog.SearchItemListener, FantasyFragment.OnActionListener, OnRelatedArticleClickListener, ArticleActionListener {

    /*Request Codes*/
    public static final int REQUEST_CODE_HOME_FEED_CUSTOMIZE = 1010;
    public static final int REQUEST_CODE_CALL_FOR_FOOTER_TAB = 12121;

    /*Bottom Navigation Variables*/
    private static final int MAX_NAVIGATION_ITEMS = 5;
    public static final int NEWS_FRAGMENT = 1;
    private static final int PRIME_FRAGMENT = 2;
    private static final int MORE_FRAGMENT = 3;

    public static int currentFragmentIndex = -1;
    public static int currentBottomPosition = -1;

    public static boolean tabReClicked = false;

    private static ConnectivityReceiver connectivityReceiver;
    public final int LOGIN_REQUEST_CODE = 445;

    /*Native component*/
    public RelativeLayout progressBarLayout;
    boolean isTabVisible = false;
    private SearchFragmentDialog searchFragmentDialog;

    @SuppressLint("HandlerLeak")
    private String actionBarText;
    private ArrayList<BottomNavInfo> bottomNavInfosMain;
    private ArrayList<BottomNavInfo> bottomNavInfosMore;
    private int selectedTab;
    private CategoryInfo categoryInfo = null;
    /*Fragment instance*/
    private Fragment fragment = null;
    private TextView titleTextView;
    private RelativeLayout imageTitleLayout;
    private AppBarLayout.LayoutParams appBarLayoutParams;
    private ImageView searchItem, refreshItem;
    private LinearLayout dynamicBrandLayout;
    private DrawerLayout drawer;
    private View actionBar;
    private View moreLayoutDivider;

    private RecyclerView moreBottomRV;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ImageView ivNavIcon;
    private List<Offer> offersList;
    private ProfileInfo mProfileInfo;

    private List<HeaderNavInfo> headerNavList;
    private ViewBottomBehavior viewBottomBehavior;
    public static int IN_APP_UPDATE_REQUEST_CODE = 9090;

    @SuppressLint({"RestrictedApi", "InflateParams"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_home);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        // Tool Bar
        tabLayout = findViewById(R.id.tab_layout);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.notify_star);
        setSupportActionBar(toolbar);

        viewBottomBehavior = new ViewBottomBehavior(this);
        appBarLayoutParams = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        actionBar = getLayoutInflater().inflate(R.layout.action_bar_view, null);
        ivNavIcon = actionBar.findViewById(R.id.menu_icon);
        ivNavIcon.setColorFilter(ContextCompat.getColor(this, R.color.nav_menu_color));
        View viewById = actionBar.findViewById(R.id.nav_icon);
        viewById.setOnClickListener(v -> {
            if (drawer != null)
                if (drawer.isDrawerOpen(GravityCompat.START))
                    drawer.closeDrawer(GravityCompat.START);
                else {
                    drawer.openDrawer(GravityCompat.START);
                }
        });

        /*dynamic brands*/
        handleActionbarIcons();

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, Gravity.START);
        try {
            ActionBar supportActionBar = getSupportActionBar();
            assert supportActionBar != null;
            supportActionBar.setCustomView(actionBar, params);
            supportActionBar.setDisplayShowCustomEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setShowHideAnimationEnabled(true);
            supportActionBar.setHomeButtonEnabled(true);
        } catch (Exception ignored) {
        }

        // :: ActiveUser ga Client Id
        new GetGAClientId().execute();

        // :: /*Drawer initialize*/
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        DrawerLayout.LayoutParams layoutParams = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        layoutParams.width = displayMetrics.widthPixels;
        navigationView.setLayoutParams(layoutParams);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void setDrawerArrowDrawable(@NonNull DrawerArrowDrawable drawable) {
                super.setDrawerArrowDrawable(drawable);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                if (newState != DrawerLayout.STATE_IDLE && !drawer.isDrawerVisible(GravityCompat.START)) {
                    String campaign = AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                    String gAaction = AppFlyerConst.GALabel.DVB_CLICK;
                    Tracking.trackGAEvent(MainActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.MENU, gAaction, AppFlyerConst.GALabel.HAMBERGER, campaign);

                    CleverTapDB.getInstance(MainActivity.this).cleverTapTrackEvent(MainActivity.this, CTConstant.EVENT_NAME.SIDE_MENU_CLICKS, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.ICON, LoginController.getUserDataMap());
                }
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(false);

        // :: /*initializeViews*/
        initializeViews();

        // :: /*initializeBottomNavigationView*/
        initializeBottomNavigationView();

        JsonParser.getInstance().parseBannerV2(MainActivity.this);
        // :: /*get Extras from Intent*/
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (savedInstanceState != null) {
                currentFragmentIndex = savedInstanceState.getInt(Constants.KeyPair.KEY_FRAGMENT_VALUE);
                currentBottomPosition = savedInstanceState.getInt(Constants.HomeRecreateVariables.BOTTOM_POS);
                isTabVisible = savedInstanceState.getBoolean(Constants.HomeRecreateVariables.IS_TAB_VISIBLE);
                actionBarText = savedInstanceState.getString(Constants.HomeRecreateVariables.ACTION_BAR_TEXT);
                if (isTabVisible) {
                    appBarLayoutParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
                    findViewById(R.id.tab_layout).setVisibility(View.VISIBLE);
                    titleTextView.setVisibility(GONE);
                    imageTitleLayout.setVisibility(View.VISIBLE);
                } else {
                    appBarLayoutParams.setScrollFlags(0);
                    findViewById(R.id.tab_layout).setVisibility(GONE);
                    imageTitleLayout.setVisibility(GONE);
                    titleTextView.setVisibility(View.VISIBLE);
                    titleTextView.setText(actionBarText);
                }
            } else {
                afterGettingIntentData(intent);
            }
        } else {
            afterGettingIntentData(intent);
        }


        /*Receiver for Internet Available*/
        connectivityReceiver = new ConnectivityReceiver();

        /*Offers Check*/
        if (JsonParser.getInstance().isOfferActionAvailable(MainActivity.this)) {
            //init share win controller
            ShareWinController.shareWinController().initializeController(MainActivity.this, TrackingData.getDBId(MainActivity.this), TrackingData.getDeviceId(MainActivity.this), CommonConstants.BHASKAR_APP_ID);
            fetchLatestOffers();
        }
        /*Register connection status listener*/
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectivityReceiver, intentFilter);
        titleTextView.setTextColor(AppUtils.fetchAttributeColor(this, R.attr.AppLogoIconColor));

        /*get value from preference*/
        boolean isAppLaunchedFirstTime = AppPreferences.getInstance(MainActivity.this).getBooleanValue(QuickPreferences.IS_APP_LAUNCED_FISRT_TIME, true);

        /*init fragment*/

        if (isAppLaunchedFirstTime) {
            getAppInstallationTime();
        }

        /*fetch latest Css */
        BackgroundRequest.fetchLatestCSS(MainActivity.this);

        AppUpdateUtils.getAppUpdateUtils(MainActivity.this).initAppUpdate();

        // Tracking : set GA tracker
        ((InitApplication) getApplication()).setTrackerValuesForGA();

        /*GDPR Tracking*/
        sendGdprTracking();

        if (!AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.RECOMMENDATION_CONSENT_SENT, false)) {
            boolean recommendationConsentAccepted = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.RECOMMENDATION_CONSENT, false);
            boolean isBlockedCountry = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
            BackgroundRequest.recommendationConsent(this, isBlockedCountry ? (recommendationConsentAccepted ? "1" : "0") : "1", false);
        }

        mProfileInfo = LoginPreferences.getInstance(this).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);


        if (AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.AdPref.EXIT_BANNER_TOGGLE, false)) {
            AdController.loadExitPopupAd(this);
        }

        /*In App Notification*/
        new Handler().postDelayed(this::startInAppMessaging, 500);

        initializeNavigationDrawer();
        setCheckForNotificationEnablePopup();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("core_event"));

        DbSharedPref.Companion.setPromoWidgetVisibilityStatus(dbApp, true);
    }

    private void setCheckForNotificationEnablePopup() {

        if (JobUtil.getNotificationIssueDevice(this) && !AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.NOTIFICATION_AUTOSTART_POPUP_SHOW, false)) {
            long notificationReceiveTime = AppPreferences.getInstance(this).getLongValue(QuickPreferences.NOTIFICATION_RECEIVE_TIME_MILLIS, 0L);
            if (notificationReceiveTime == 0) {
                notificationReceiveTime = System.currentTimeMillis();
                AppPreferences.getInstance(this).setLongValue(QuickPreferences.NOTIFICATION_RECEIVE_TIME_MILLIS, notificationReceiveTime);
            }

            if (System.currentTimeMillis() > (notificationReceiveTime + 259200000L)) {
                AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.NOTIFICATION_AUTOSTART_POPUP_SHOW, true);
                showAutoStartEnablePopup();
            }
        }
    }

    private void showAutoStartEnablePopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.layout_auto_start_popup, null);
        builder.setView(view);

        final AlertDialog alert = builder.create();

        view.findViewById(R.id.ok_tv).setOnClickListener(view1 -> {
            JobUtil.autoStart(MainActivity.this);
            alert.dismiss();
        });

        view.findViewById(R.id.cancel_tv).setOnClickListener(view12 -> alert.dismiss());

        alert.show();
    }

    private void handleActionbarIcons() {
        dynamicBrandLayout = actionBar.findViewById(R.id.dynamic_brand_layout);

        refreshItem = actionBar.findViewById(R.id.refresh_item);
        refreshItem.setOnClickListener(v -> {
            try {
                if (getSupportFragmentManager().findFragmentById(R.id.frame_layout) instanceof NewsBriefListFragment2) {
                    NewsBriefListFragment2 fragment = (NewsBriefListFragment2) getSupportFragmentManager().findFragmentById(R.id.frame_layout);
                    assert fragment != null;
                    fragment.refreshItems();

                    CleverTapDB.getInstance(this).cleverTapTrackEvent(this, CTConstant.EVENT_NAME.HOME_TOP_RIGHT_ICONS_CLICK, CTConstant.PROP_NAME.ICON_NAME, CTConstant.PROP_VALUE.REFRESH, LoginController.getUserDataMap());
                }
            } catch (Exception ignored) {
            }
        });

        searchItem = actionBar.findViewById(R.id.search_item);
        searchItem.setColorFilter(ContextCompat.getColor(this, R.color.nav_menu_color), PorterDuff.Mode.SRC_IN);
        searchItem.setOnClickListener(v -> {
            try {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                searchFragmentDialog = SearchFragmentDialog.newInstance();
                transaction.addToBackStack("searchdialog");
                searchFragmentDialog.show(transaction, "searchdialog");

                CleverTapDB.getInstance(this).cleverTapTrackEvent(this, CTConstant.EVENT_NAME.HOME_TOP_RIGHT_ICONS_CLICK, CTConstant.PROP_NAME.ICON_NAME, CTConstant.PROP_VALUE.SEARCH, LoginController.getUserDataMap());
            } catch (Exception ignored) {
            }
        });

        // dynamic tool bar items
        headerNavList = JsonParser.getInstance().getHeaderNavList(this);
        for (int i = 0; i < headerNavList.size(); i++) {
            if (i == 0) {
                ImageView dynamicBrand1 = actionBar.findViewById(R.id.dynamic_brand_1);
                ImageUtil.setImage(MainActivity.this, headerNavList.get(i).icon, dynamicBrand1, R.drawable.menu_header_placeholder);
                int finalI = i;
                dynamicBrand1.setOnClickListener(v -> handleDynamicBrand(finalI));
                dynamicBrand1.setVisibility(View.VISIBLE);
            } else if (i == 1) {
                ImageView dynamicBrand2 = actionBar.findViewById(R.id.dynamic_brand_2);
                ImageUtil.setImage(MainActivity.this, headerNavList.get(i).icon, dynamicBrand2, R.drawable.menu_header_placeholder);
                int finalI = i;
                dynamicBrand2.setOnClickListener(v -> handleDynamicBrand(finalI));
                dynamicBrand2.setVisibility(View.VISIBLE);
            } else if (i == 2) {
                return;
            }
        }
        setDynamicBransLayoutVisibility();
    }

    private void initializeViews() {
        titleTextView = actionBar.findViewById(R.id.header_text_view);
        ImageView titleImageView = actionBar.findViewById(R.id.header_image_view);
        imageTitleLayout = actionBar.findViewById(R.id.image_title_layout);
        String homeLogo = AppPreferences.getInstance(this).getStringValue(QuickPreferences.HomeLogo.HOME_LOGO_URL, "");
        if (AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.HomeLogo.IS_HOME_LOGO_ACTIVE, false) && !TextUtils.isEmpty(homeLogo)) {
            ImageUtil.setImage(MainActivity.this, homeLogo, titleImageView, 0, R.drawable.ic_toolbar_logo);
        } else {
            titleImageView.setImageResource(R.drawable.ic_toolbar_logo);
        }

        progressBarLayout = findViewById(R.id.progress_bar_layout);
        moreBottomRV = findViewById(R.id.more_layout);
        moreLayoutDivider = findViewById(R.id.more_layout_divider);
    }

    TabLayout bottomTabLayout;

    private void initializeBottomNavigationView() {
        /* Bottom Navigation */
        ArrayList<TabLayout.Tab> bottomNavigationItems = new ArrayList<>(MAX_NAVIGATION_ITEMS);
        bottomNavInfosMain = new ArrayList<>(MAX_NAVIGATION_ITEMS);

        // init Bottom view
        bottomTabLayout = findViewById(R.id.bottom_navigation);
        bottomTabLayout.removeAllTabs();
        /*Dynamic Bottom Item*/
        boolean isHomeAdded = false;
        List<BottomNavInfo> bottomNavList = JsonParser.getInstance().getBottomNavList(this);
        if (bottomNavList != null && bottomNavList.size() > 0) {
            int dynamicBrandPos = 0;
            for (int indx = 0; indx < bottomNavList.size(); indx++) {
                if (dynamicBrandPos == (isHomeAdded ? MAX_NAVIGATION_ITEMS : (MAX_NAVIGATION_ITEMS - 1))) {
                    break;
                }
                BottomNavInfo brandNavItem = bottomNavList.get(indx);

                if (brandNavItem.action.equalsIgnoreCase(Action.CategoryAction.CAT_MORE_NAV)) {
                    List<BottomNavInfo> bottomNavMoreList = JsonParser.getInstance().getBottomNavMoreList(this);
                    if (bottomNavMoreList.size() > 1) {

                        bottomNavInfosMain.add(dynamicBrandPos, brandNavItem);
                        dynamicBrandPos++;

                        // add bottom more items
                        initializeBottomNavMore(bottomNavMoreList);

                    } else if (bottomNavMoreList.size() == 1) {
                        BottomNavInfo brandNavMoreItem = bottomNavMoreList.get(0);
                        bottomNavInfosMain.add(dynamicBrandPos, brandNavMoreItem);
                        dynamicBrandPos++;
                    }
                } else {
                    if (brandNavItem.action.equalsIgnoreCase(Action.CategoryAction.CAT_HOME_NAV)) {
                        isHomeAdded = true;
                    }
                    bottomNavInfosMain.add(dynamicBrandPos, brandNavItem);
                    dynamicBrandPos++;
                }
            }
        }

        if (!isHomeAdded) {
            BottomNavInfo brandNavItem = new BottomNavInfo();
            brandNavItem.action = Action.CategoryAction.CAT_HOME_NAV;
            brandNavItem.id = "810";
            brandNavItem.name = getResources().getString(R.string.home);
            brandNavItem.icon = "https://i10.dainikbhaskar.com/images/appfeed/nav_icon/Home-1.png";
            brandNavItem.iconActive = "https://i10.dainikbhaskar.com/images/appfeed/nav_icon/Home-2.png";
            bottomNavInfosMain.add(0, brandNavItem);
        } else {
            for (int i = 0; i < bottomNavInfosMain.size(); i++) {
                if (i != 0 && bottomNavInfosMain.get(i).action.equalsIgnoreCase(Action.CategoryAction.CAT_HOME_NAV)) {
                    BottomNavInfo homeBottomNav = new BottomNavInfo(bottomNavInfosMain.get(i));
                    bottomNavInfosMain.remove(homeBottomNav);
                    bottomNavInfosMain.add(0, homeBottomNav);
                }
            }
        }

        for (int index = 0; index < bottomNavInfosMain.size(); index++) {
            bottomNavigationItems.add(createTabIcons(bottomNavInfosMain.get(index), index));
        }

        if (bottomNavigationItems.size() < 2) {
            AppPreferences.getInstance(MainActivity.this).setBooleanValue(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE, true);
            findViewById(R.id.bottom_bar_layout).setVisibility(GONE);
        } else {
            AppPreferences.getInstance(MainActivity.this).setBooleanValue(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE, AppPreferences.getInstance(MainActivity.this).getBooleanValue(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE_SERVER, false));
        }

        bottomTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = bottomNavigationItems.indexOf(tab);
                updateTabUI(tab, bottomNavInfosMain.get(position), true);
                if (!isIndexSelected) {
                    onMainBottomTabClick(position);
                } else {
                    isIndexSelected = false;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int position = bottomNavigationItems.indexOf(tab);
                updateTabUI(tab, bottomNavInfosMain.get(position), false);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int position = bottomNavigationItems.indexOf(tab);
                updateTabUI(tab, bottomNavInfosMain.get(position), true);
                if (!isIndexSelected) {
                    onMainBottomTabClick(position);
                } else {
                    isIndexSelected = false;
                }
            }
        });
    }


    private TabLayout.Tab createTabIcons(@NonNull BottomNavInfo menuItem, int index) {
        bottomTabLayout.addTab(bottomTabLayout.newTab());
        LinearLayout layout = (LinearLayout) LayoutInflater.from(MainActivity.this).inflate(R.layout.custom_bottom_bar, null);
        TextView tabOne = layout.findViewById(R.id.tab);
        tabOne.setText(menuItem.name);
        ImageView tabImg = layout.findViewById(R.id.tab_img);
        ImageUtil.setImage(MainActivity.this, menuItem.icon, tabImg, 0);
        TabLayout.Tab tab = bottomTabLayout.getTabAt(index);
        if (null != tab) {
            tab.setTag(menuItem);
            tab.setCustomView(layout);
        }
        return tab;
    }

    private void initializeBottomNavMore(List<BottomNavInfo> bottomNavMoreList) {
        bottomNavInfosMore = new ArrayList<>();
        /*Dynamic Bottom Item*/
        if (bottomNavMoreList != null && bottomNavMoreList.size() > 0) {
            int dynamicBrandPos = 0;
            for (int indx = 0; indx < bottomNavMoreList.size(); indx++) {
                BottomNavInfo brandNavItem = bottomNavMoreList.get(indx);

                if (!brandNavItem.id.equalsIgnoreCase("578")) {
                    bottomNavInfosMore.add(dynamicBrandPos, brandNavItem);
                    dynamicBrandPos++;
                }
            }
        }

        if (bottomNavInfosMore.size() > 0) {
            if (bottomNavInfosMore.size() <= 5) {
                moreBottomRV.setLayoutManager(new GridLayoutManager(MainActivity.this, bottomNavInfosMore.size()));
                moreBottomRV.setAdapter(new BottomNavGridAdapter(bottomNavInfosMore, this::onMoreBottomTabClick, BottomNavGridAdapter.GRID_TYPE));
            } else {
                moreBottomRV.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false));
                moreBottomRV.setAdapter(new BottomNavGridAdapter(bottomNavInfosMore, this::onMoreBottomTabClick, BottomNavGridAdapter.HORIZONTAL_TYPE));
            }
        } else {
            moreBottomRV.setVisibility(View.INVISIBLE);
            moreLayoutDivider.setVisibility(View.INVISIBLE);
        }
    }

    boolean isIndexSelected = false;

    private void setItemIndexActiveState(int currentBottomPosition) {
        try {
            isIndexSelected = true;
            Objects.requireNonNull(bottomTabLayout.getTabAt(currentBottomPosition)).select();
        } catch (Exception ignored) {

        }
    }

    private void updateTabUI(@NonNull TabLayout.Tab tab, BottomNavInfo bottomNavInfo, boolean isActive) {
        try {
            int tabTextColor = (isActive) ? ContextCompat.getColor(MainActivity.this, R.color.divya_theme_color) : AppUtils.getThemeColor(MainActivity.this, "#FF777777");
            String imageUrl = (isActive) ? bottomNavInfo.iconActive : bottomNavInfo.icon;
            View customView = tab.getCustomView();
            if (null != customView) {
                TextView tabText = customView.findViewById(R.id.tab);
                ImageView tabImg = customView.findViewById(R.id.tab_img);
                tabText.setTextColor(tabTextColor);
                if (null != tabImg) {
                    ImageUtil.setImage(MainActivity.this, imageUrl, tabImg, 0);
                }
            }
        } catch (Exception ignored) {
        }
    }

    private void onMainBottomTabClick(int position) {
        int id = getIDForTab1(position);
        if (position < 0 || position >= bottomNavInfosMain.size())
            return;

        BottomNavInfo bottomNavInfo = bottomNavInfosMain.get(position);

        // tracking on click events, **please put on upon side
        if (id != MORE_FRAGMENT && moreBottomRV.getVisibility() == View.VISIBLE) {
            moreBottomRV.setVisibility(View.INVISIBLE);
            moreLayoutDivider.setVisibility(View.INVISIBLE);
        }

        if (progressBarLayout != null)
            progressBarLayout.setVisibility(GONE);

        String temp = AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL_TEMP, CommonConstants.EVENT_LABEL);
        if (!TextUtils.isEmpty(temp)) {
            AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL, temp);
            AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL_TEMP, "");
        }

        String tempDomain = AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.WISDOM_DOMAIN_TEMP, "");
        if (!TextUtils.isEmpty(tempDomain)) {
            AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.WISDOM_DOMAIN, tempDomain);
            AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.WISDOM_DOMAIN_TEMP, "");
        }
        CategoryInfo naviBrandInfo = JsonParser.getInstance().getCategoryInfoByIdForBottom(MainActivity.this, bottomNavInfo.id);
        if (naviBrandInfo != null) {
            tracking(id, naviBrandInfo.gaEventLabel, naviBrandInfo.action, naviBrandInfo.displayName, false);
        }
        switch (id) {
            case NEWS_FRAGMENT:
                currentBottomPosition = position;
                saveBottomPosition(currentBottomPosition);
                if (currentFragmentIndex != NEWS_FRAGMENT || fragment instanceof NewsCategoryFragment) {
                    addFragment(NEWS_FRAGMENT, position, null, false, false, AppFlyerConst.PrefixSection.HOME);
                } else {
                    if (fragment != null && fragment instanceof NewsFragment) {
                        ((NewsFragment) fragment).moveToZeroIndex();
                    }
                }
                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME);
                break;

            case PRIME_FRAGMENT:
                currentBottomPosition = position;
                saveBottomPosition(currentBottomPosition);
                if (currentFragmentIndex != PRIME_FRAGMENT || fragment instanceof NewsCategoryFragment) {
                    addFragment(PRIME_FRAGMENT, position, null, false, false, "");
                }
                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.BRAND);
                break;

            case MORE_FRAGMENT:
                currentBottomPosition = position;
                saveBottomPosition(position);
                if (moreBottomRV.getVisibility() == View.VISIBLE) {
                    setItemIndexActiveState(currentBottomPosition);
                    moreBottomRV.setVisibility(View.INVISIBLE);
                    moreLayoutDivider.setVisibility(View.INVISIBLE);
                } else {
                    moreBottomRV.setVisibility(View.VISIBLE);
                    moreLayoutDivider.setVisibility(View.VISIBLE);
                }
                break;

            default:
                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.WISDOM_DOMAIN_TEMP, AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN));
                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);

                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL_TEMP, AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL, CommonConstants.EVENT_LABEL));
                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL, CommonConstants.EVENT_LABEL);

                if (!bottomNavInfosMain.isEmpty()) {
                    if (Action.OUTSIDE_OPEN_CATEGORY_ACTIONS.contains(bottomNavInfo.action)) {
                        new Handler().postDelayed(() -> {
                            AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.NAV);
                            setItemIndexActiveState(currentBottomPosition);
                            ActivityUtil.openCategoryActionOutside(MainActivity.this, naviBrandInfo);
                        }, 200);
                        return;
                    } else {
                        if (currentFragmentIndex != Integer.parseInt(bottomNavInfo.id)) {
                            AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.NAV);
                            currentBottomPosition = position;
                            saveBottomPosition(currentBottomPosition);
                            addFragment(Integer.parseInt(bottomNavInfo.id), position, null, false, false, "");
                        }
                    }
                }
                break;
        }
    }

    private void onMoreBottomTabClick(int position) {
        int id = getIDForTab2(position);
        if (position < 0 || position >= bottomNavInfosMore.size())
            return;

        BottomNavInfo bottomNavInfo = bottomNavInfosMore.get(position);

        if (moreBottomRV.getVisibility() == View.VISIBLE) {
            moreBottomRV.setVisibility(View.INVISIBLE);
            moreLayoutDivider.setVisibility(View.INVISIBLE);
        }

        if (progressBarLayout != null)
            progressBarLayout.setVisibility(GONE);

        String temp = AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL_TEMP, CommonConstants.EVENT_LABEL);
        if (!TextUtils.isEmpty(temp)) {
            AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL, temp);
            AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL_TEMP, "");
        }

        String tempDomain = AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.WISDOM_DOMAIN_TEMP, "");
        if (!TextUtils.isEmpty(tempDomain)) {
            AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.WISDOM_DOMAIN, tempDomain);
            AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.WISDOM_DOMAIN_TEMP, "");
        }

        CategoryInfo naviBrandInfo = JsonParser.getInstance().getCategoryInfoByIdForBottom(MainActivity.this, bottomNavInfo.id);
        if (naviBrandInfo != null) {
            tracking(id, naviBrandInfo.gaEventLabel, naviBrandInfo.action, naviBrandInfo.displayName, true);
        }

        switch (id) {
            case NEWS_FRAGMENT:
                currentFragmentIndex = -1;
                addFragment(NEWS_FRAGMENT, position, null, true, false, "");
                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME);
                break;

            case PRIME_FRAGMENT:
                currentFragmentIndex = -1;
                addFragment(PRIME_FRAGMENT, position, null, true, false, "");
                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.BRAND);
                break;

            case MORE_FRAGMENT:
                break;

            default:
                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.WISDOM_DOMAIN_TEMP, AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN));
                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);

                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL_TEMP, AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL, CommonConstants.EVENT_LABEL));
                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL, CommonConstants.EVENT_LABEL);

                AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.NAV);

                if (!bottomNavInfosMore.isEmpty()) {
                    if (Action.OUTSIDE_OPEN_CATEGORY_ACTIONS.contains(bottomNavInfo.action)) {
                        new Handler().postDelayed(() -> {
                            ActivityUtil.openCategoryActionOutside(MainActivity.this, naviBrandInfo);
                            setItemIndexActiveState(currentBottomPosition);
                        }, 200);
                        return;
                    } else {
                        addFragment(Integer.parseInt(bottomNavInfo.id), position, null, true, true, "");
                    }
                }
                break;
        }
    }

    private void saveBottomPosition(int position) {
        AppPreferences.getInstance(this).setIntValue("bottomPosition", position);
    }

    private int getIDForTab1(int position) {
        if (bottomNavInfosMain != null && !bottomNavInfosMain.isEmpty() && position >= 0 && position < bottomNavInfosMain.size()) {
            String action = bottomNavInfosMain.get(position).action;
            if (action.equalsIgnoreCase(Action.CategoryAction.CAT_HOME_NAV)) {
                return NEWS_FRAGMENT;
            } else if (action.equalsIgnoreCase(Action.CategoryAction.CAT_PRIME_NAV)) {
                return PRIME_FRAGMENT;
            } else if (action.equalsIgnoreCase(Action.CategoryAction.CAT_MORE_NAV)) {
                return MORE_FRAGMENT;
            } else {
                return Integer.parseInt(bottomNavInfosMain.get(position).id);
            }
        }
        return -1;
    }

    private int getIDForTab2(int position) {
        if (bottomNavInfosMore != null && !bottomNavInfosMore.isEmpty() && position >= 0 && position < bottomNavInfosMore.size()) {
            String action = bottomNavInfosMore.get(position).action;
            if (action.equalsIgnoreCase(Action.CategoryAction.CAT_HOME_NAV)) {
                return NEWS_FRAGMENT;
            } else if (action.equalsIgnoreCase(Action.CategoryAction.CAT_PRIME_NAV)) {
                return PRIME_FRAGMENT;
            } else if (action.equalsIgnoreCase(Action.CategoryAction.CAT_MORE_NAV)) {
                return MORE_FRAGMENT;
            } else {
                return Integer.parseInt(bottomNavInfosMore.get(position).id);
            }
        }
        return -1;
    }

    private void addFragment(final int id, final int position, CategoryInfo categoryInfo1, boolean moreOption, boolean isDynamic, String prefixSection) {

        boolean isMarginAllowed = AppPreferences.getInstance(MainActivity.this).getBooleanValue(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE, false);
        setBottomBehaviour(isMarginAllowed);
        appBarLayoutParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);

        switch (id) {
            case NEWS_FRAGMENT:
                isTabVisible = true;
                if (fragment != null && fragment instanceof NewsFragment && categoryInfo1 != null && ((NewsFragment) fragment).moveToSpecificTab(categoryInfo1)) {
                    return;
                }
                fragment = NewsFragment.getInstance(true, prefixSection);
                this.categoryInfo = null;
                imageTitleLayout.setVisibility(View.VISIBLE);
                titleTextView.setText("");
                titleTextView.setVisibility(GONE);
                refreshItem.setVisibility(GONE);
                searchItem.setVisibility(View.VISIBLE);
                setDynamicBransLayoutVisibility();
                overideThemeColor("");
                break;

            case PRIME_FRAGMENT:
                isTabVisible = false;
                PrimeFragmentNew primeFragment = PrimeFragmentNew.getInstance(!isMarginAllowed);
                primeFragment.setListener(new OnFeedFetchFromServerListener() {
                    @Override
                    public void onDataFetch(boolean flag) {
                    }

                    @Override
                    public void onDataFetch(boolean flag, String catId) {
                        CategoryInfo categoryInfo = JsonParser.getInstance().getCategoryInfoBasedOnId(MainActivity.this, catId);
                        if (Action.OUTSIDE_OPEN_CATEGORY_ACTIONS.contains(categoryInfo.action)) {
                            new Handler().postDelayed(() -> {
                                ActivityUtil.openCategoryActionOutside(MainActivity.this, categoryInfo);
                                setItemIndexActiveState(currentBottomPosition);
                            }, 200);
                            return;
                        } else {
                            addFragment(Integer.parseInt(categoryInfo.id), position, null, true, true, AppFlyerConst.PrefixSection.PRIME);
                        }

                    }

                    @Override
                    public void onDataFetch(boolean flag, int viewType) {
                    }

                    @Override
                    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {
                    }
                });
                fragment = primeFragment;
                imageTitleLayout.setVisibility(GONE);
                titleTextView.setText(R.string.prime);
                titleTextView.setVisibility(View.VISIBLE);
                refreshItem.setVisibility(GONE);
                searchItem.setVisibility(GONE);
                dynamicBrandLayout.setVisibility(GONE);
                overideThemeColor("");
                break;

            case MORE_FRAGMENT:
                isTabVisible = false;
                break;

            default:
                // Add NewsFragment in Case of other Brands
                CategoryInfo naviBrandInfo = JsonParser.getInstance().getCategoryInfoById(MainActivity.this, String.valueOf(id));
                if (naviBrandInfo != null) {
                    if (naviBrandInfo.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_WAP) || naviBrandInfo.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_WAP_USER)) {
                        fragment = WapV2Fragment.getInstance(naviBrandInfo.feedUrl, naviBrandInfo.gaScreen, false, naviBrandInfo.menuName, naviBrandInfo.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_WAP_USER));
                        titleTextView.setText(naviBrandInfo.menuName);
                        titleTextView.setVisibility(View.VISIBLE);
                        imageTitleLayout.setVisibility(GONE);
                        isTabVisible = false;

                    } else {

                        if (!naviBrandInfo.parentId.equalsIgnoreCase("0")) {
                            CategoryInfo parentCategory = JsonParser.getInstance().getCategoryInfoBasedOnId(MainActivity.this, naviBrandInfo.parentId);
                            isTabVisible = parentCategory.subMenu.size() > 1;
                            titleTextView.setText(parentCategory.menuName);
                        } else {
                            isTabVisible = naviBrandInfo.subMenu.size() > 1;
                            titleTextView.setText(naviBrandInfo.menuName);
                        }


                        titleTextView.setVisibility(View.VISIBLE);
                        imageTitleLayout.setVisibility(GONE);
                        dynamicBrandLayout.setVisibility(GONE);
                        searchItem.setVisibility(GONE);
                        refreshItem.setVisibility(GONE);
                        fragment = NewsCategoryFragment.getInstance(naviBrandInfo, prefixSection);
                        appBarLayoutParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);


                        Systr.println("Dynamic or not => " + isDynamic);
                        if (isDynamic) {
                            overideThemeColor(naviBrandInfo.color);
                        } else {
                            overideThemeColor("");
                        }
                    }
                }
        }
        if (!moreOption) {
            currentFragmentIndex = id;
            if (bottomTabLayout.getTabAt(position) != null)
                bottomTabLayout.getTabAt(position).select();
        }
        actionBarText = titleTextView.getText().toString();
        try {
            if (!isFinishing() && fragment != null) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, fragment);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        } catch (Exception ignored) {
        }

        final boolean finalIsTabVisible = isTabVisible;
        new Handler().postDelayed(() -> {
            if (finalIsTabVisible) {
                findViewById(R.id.tab_layout).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.tab_layout).setVisibility(GONE);
            }
        }, 100);
    }

    private void overideThemeColor(String color) {
        AppBarLayout appbar = findViewById(R.id.appbar);
        if (!TextUtils.isEmpty(color)) {
            ivNavIcon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.white));
            appbar.setBackgroundColor(AppUtils.getThemeColor(this, color));
            titleTextView.setTextColor(getResources().getColor(R.color.white));
            AppUtils.getInstance().setStatusBarColor(this, color);
            tabLayout.setTabTextColors(getResources().getColor(R.color.white), getResources().getColor(R.color.white));
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.white));
        } else {
            ivNavIcon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.theme_darkGray_navigationDrawerIconColor));
            appbar.setBackgroundColor(getResources().getColor(R.color.white));
            titleTextView.setTextColor(getResources().getColor(R.color.black));
            tabLayout.setTabTextColors(getResources().getColor(R.color.tab_deselect_text_color), getResources().getColor(R.color.divya_theme_color));
            int themeColor = AppUtils.getThemeColor(MainActivity.this, "");
            tabLayout.setSelectedTabIndicatorColor(themeColor);
        }
    }

    private void setBottomBehaviour(boolean isMarginAllowed) {
        LinearLayout yourView = findViewById(R.id.bottom_bar_layout);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) yourView.getLayoutParams();
        if (isMarginAllowed)
            params.setBehavior(viewBottomBehavior);
        else {
            params.setBehavior(null);
        }
        yourView.requestLayout();
    }

    private void startInAppMessaging() {
        InAppMessageClickListener inAppMessageClickListener = new InAppMessageClickListener(MainActivity.this);
        FirebaseInAppMessaging.getInstance().addClickListener(inAppMessageClickListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void launchOfferDialog() {
        if (offersList != null && !offersList.isEmpty()) {
            List<Offer> filterOffer = new ArrayList<>();
            for (Offer offer : offersList) {
                if (offer.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_OFFER_SHARE_WIN)
                        || offer.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_WAP)) {
                    if (!TextUtils.isEmpty(offer.showDialog) && offer.showDialog.equalsIgnoreCase("1") && offer.offerExpire.equalsIgnoreCase("0"))
                        filterOffer.add(offer);
                }
            }
            if (!filterOffer.isEmpty()) {
                try {
                    OffersDialogFragment offersDialogFragment = OffersDialogFragment.getInstance(filterOffer, (offer) -> {
                        String dialogAction = offer.dailog_action;
                        SWAppPreferences.getInstance(MainActivity.this).setBooleanValue(SWAppPreferences.Keys.OFFER_DISABLED, true);

                        if (dialogAction.equalsIgnoreCase("1")) {
                            //open offer
                            launchDashBoard(offer);
                        } else if (dialogAction.equalsIgnoreCase("2")) {
                            //open listing
                            /*Brand*/
                            CategoryInfo offerBrand = JsonParser.getInstance().getCategoryInfoBasedOnAction(MainActivity.this, Action.CategoryAction.CAT_ACTION_OFFER);
                            if (null != offerBrand) {
                                ActivityUtil.openCategoryAccordingToAction(MainActivity.this, offerBrand, "");
                            }
                        }
                    });
                    offersDialogFragment.show(getSupportFragmentManager(), OffersDialogFragment.class.getSimpleName());
                } catch (Exception ignore) {
                }
            }
        }
    }

    private void showTermsnCondition(Offer offer) {
        OffersTermsFragment offersTermsFragment = OffersTermsFragment.getInstance(offer, () -> {
            SWAppPreferences.getInstance(MainActivity.this).setBooleanValue(String.format(SWAppPreferences.Keys.OFFER_TERMS_DISABLED, offer.id), true);
            switch (offer.action) {
                case Action.CategoryAction.CAT_ACTION_OFFER_SHARE_WIN:
                    if (NetworkStatus.getInstance().isConnected(MainActivity.this)) {
                        AppUtils.getInstance().startOffer(MainActivity.this, offer, false);
                    } else {
                        AppUtils.getInstance().showCustomToast(MainActivity.this, getString(R.string.no_network_error));
                    }
                    break;
                case Action.CategoryAction.CAT_ACTION_WAP:
                    AppUtils.getInstance().startWapOffer(MainActivity.this, offer);
                    break;
                case Action.CategoryAction.CAT_ACTION_OFFER_EMPTY:
                    break;
            }
        });
        offersTermsFragment.show(getSupportFragmentManager(), OffersTermsFragment.class.getName());
    }

    private void launchDashBoard(Offer offer) {
        boolean isTermsDisabled = SWAppPreferences.getInstance(MainActivity.this).getBooleanValue(String.format(SWAppPreferences.Keys.OFFER_TERMS_DISABLED, offer.id), false);
        boolean isDialogTncEmpty = TextUtils.isEmpty(offer.dialogTnc);
        switch (offer.action) {
            case Action.CategoryAction.CAT_ACTION_OFFER_SHARE_WIN:
                if (!isTermsDisabled && !isDialogTncEmpty) {
                    showTermsnCondition(offer);
                } else {
                    if (NetworkStatus.getInstance().isConnected(MainActivity.this)) {
                        AppUtils.getInstance().startOffer(MainActivity.this, offer, false);
                    } else {
                        AppUtils.getInstance().showCustomToast(MainActivity.this, getString(R.string.no_network_error));
                    }
                }
                break;
            case Action.CategoryAction.CAT_ACTION_WAP:
                if (!isTermsDisabled && !isDialogTncEmpty) {
                    showTermsnCondition(offer);
                } else {
                    AppUtils.getInstance().startWapOffer(MainActivity.this, offer);
                }
                break;
            case Action.CategoryAction.CAT_ACTION_OFFER_EMPTY:
                if (!isTermsDisabled && !isDialogTncEmpty) {
                    showTermsnCondition(offer);
                }
        }
        Tracking.trackGAEvent(MainActivity.this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.OFFER, AppFlyerConst.Key.CLICK, offer.gaEventLabel, AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, ""));
    }


    private void postRefData(String offerID) {
        String dbId = TrackingData.getDBId(MainActivity.this);
        String deviceID = TrackingData.getDeviceId(MainActivity.this);
        String appId = CommonConstants.BHASKAR_APP_ID;
        String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, QuickPreferences.UTM_DIRECT);
        String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, QuickPreferences.UTM_DIRECT);

        boolean isRefSent = SWAppPreferences.getInstance(MainActivity.this).getBooleanValue(SWAppPreferences.Keys.REFF_SENT, false);
        if (source.equalsIgnoreCase("app_invite") && !TextUtils.isEmpty(medium) && !medium.equalsIgnoreCase("direct") && !isRefSent) {
            AppUtils.getInstance().postUserReferral(MainActivity.this, dbId, deviceID, medium, appId, offerID);
        }
    }


    private void fetchLatestOffers() {
        BackgroundRequest.fetchAvailableOffers(MainActivity.this, null, new OnFeedFetchFromServerListener() {
            @Override
            public void onDataFetch(boolean flag) {
            }

            @Override
            public void onDataFetch(boolean flag, String response) {
                if (flag) {
                    offersList = JsonParser.getInstance().getAvailableOffers(response);
                    getLatestOffersFromServer();
                }
            }

            @Override
            public void onDataFetch(boolean flag, int viewType) {
            }

            @Override
            public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {
            }
        });
    }

    private void getLatestOffersFromServer() {
        String baseAppVersion = AppPreferences.getInstance(MainActivity.this).getStringValue(Action.CategoryAction.CAT_OFFER_VERSION, "0");
        String currentVersion = SWAppPreferences.getInstance(MainActivity.this).getStringValue(SWAppPreferences.Keys.CURRENT_OFFER_VERSION, "0");
        boolean isOfferVersionUpgraded = Integer.parseInt(baseAppVersion) > Integer.parseInt(currentVersion);
        boolean offerDisabled = SWAppPreferences.getInstance(MainActivity.this).getBooleanValue(SWAppPreferences.Keys.OFFER_DISABLED, false);
        if (isOfferVersionUpgraded) {
            SWAppPreferences.getInstance(MainActivity.this).setBooleanValue(SWAppPreferences.Keys.OFFER_DISABLED, false);
            SWAppPreferences.getInstance(MainActivity.this).setStringValue(SWAppPreferences.Keys.CURRENT_OFFER_VERSION, baseAppVersion);
            launchOfferDialog();
        } else if (!offerDisabled)
            launchOfferDialog();

        String offerID = getEnabledOfferId(offersList);
        if (!TextUtils.isEmpty(offerID))
            postRefData(offerID);

    }

    private String getEnabledOfferId(List<Offer> offersList) {
        for (Offer offer : offersList) {
            if (offer.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_OFFER_SHARE_WIN) && offer.offerExpire.equalsIgnoreCase("0")) {
                return offer.id;
            }
        }
        return "";
    }

    private void sendGdprTracking() {
        if (!AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_SEND_TO_GA, false)) {
            boolean isBlockedCountry = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
            if (isBlockedCountry) {
                boolean adConsent = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.ADS_CONSENT, false);
                Tracking.trackGAEvent(this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.GDPR, AppFlyerConst.GAAction.ADS, adConsent ? AppFlyerConst.GALabel.YES : AppFlyerConst.GALabel.NO, "");

                boolean isFailVer = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_FAIL_VERSION, false);
                if (isFailVer) {
                    Tracking.trackGAEvent(this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.GDPR, AppFlyerConst.GAAction.VERSION_API, AppFlyerConst.GALabel.YES, "");
                }
            }
            AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.GdprConst.IS_SEND_TO_GA, true);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null) {
                final int unmaskedRequestCode = requestCode & 0x0000ffff;
                fragment.onRequestPermissionsResult(unmaskedRequestCode, permissions, grantResults);
            }
        }
    }


    private void closeNavigationDrawer() {
        drawer.closeDrawer(GravityCompat.START);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                HashMap<String, Object> dataMap = (HashMap<String, Object>) intent.getSerializableExtra("dataMap");
                boolean isBottom = (boolean) dataMap.get("isBottom");
                if (isBottom) {
                    int position = (int) dataMap.get("position");
                    boolean isMainBottom = (boolean) dataMap.get("isMainBottom");
                    if (isMainBottom) {
                        onMainBottomTabClick(position);
                    } else {
                        int bottomPosition = 0;
                        if (dataMap.containsKey("bottomPosition")) {
                            bottomPosition = (int) dataMap.get("bottomPosition");
                        }
                        onMoreBottomTabClick(position);
                        setItemIndexActiveState(bottomPosition);
                    }
                } else {
                    MenuItem menuItem = (MenuItem) dataMap.get("menuItem");
                    if (menuItem != null) {
                        onActionClick(menuItem);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };


    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        currentFragmentIndex = getIDForTab1(0);
        currentBottomPosition = 0;
        saveBottomPosition(currentBottomPosition);
        addFragment(currentFragmentIndex, currentBottomPosition, categoryInfo, false, false, AppFlyerConst.PrefixSection.HOME);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onActionBarIconUpdate(String action) {
    }

    @Override
    public void onBackPressed() {
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            closeNavigationDrawer();
        } else if (fragment instanceof NewsFragment && currentBottomPosition == 0) {
            if (AppPreferences.getInstance(MainActivity.this).getBooleanValue(QuickPreferences.LastTabPrefs.TOGGLING_LAST_TAB_ACTIVE, false)) {
                exitPopup();
            } else {
                if (selectedTab > 0) {
                    addFragment(currentFragmentIndex, currentBottomPosition, null, false, false, AppFlyerConst.PrefixSection.HOME);
                } else {
                    exitPopup();
                }
            }
        } else {
            if (fragment instanceof NewsCategoryFragment && getIDForTab1(currentBottomPosition) == PRIME_FRAGMENT) {
                bottomTabLayout.getTabAt(currentBottomPosition).select();
            } else {
                if (AppPreferences.getInstance(MainActivity.this).getBooleanValue(QuickPreferences.LastTabPrefs.TOGGLING_LAST_TAB_ACTIVE, false)) {
                    String lastOpenedTabId = AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.LAST_OPENED_CATEGORY, "");
                    if (!TextUtils.isEmpty(lastOpenedTabId)) {
                        if (JsonParser.getInstance().isCategoryAvailable(MainActivity.this, lastOpenedTabId)) {
                            categoryInfo = JsonParser.getInstance().getCategoryInfoBasedOnId(MainActivity.this, lastOpenedTabId);
                        }
                    }

                }
                int id = getIDForTab1(0);
                addFragment(id, 0, categoryInfo, false, false, AppFlyerConst.PrefixSection.HOME);
            }
        }
    }

    private void exitPopup() {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.app_exit_popup_layout, null);
        exitDialogBuilder.setView(dialogView);
        TextView btnYes = dialogView.findViewById(R.id.textyes);
        TextView btnNo = dialogView.findViewById(R.id.textno);

        try {
            BoundedLinearLayout adContainer = dialogView.findViewById(R.id.ad_layout);
            if (AdController.exitAdView != null) {
                if (AdController.exitAdView.getParent() != null) {
                    ViewGroup parent = (ViewGroup) AdController.exitAdView.getParent();
                    parent.removeAllViews();
                }
                adContainer.removeAllViews();
                adContainer.addView(AdController.exitAdView);
            }
        } catch (Exception ignored) {
        }

        final AlertDialog exitDialog = exitDialogBuilder.create();
        exitDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exitDialog.setCancelable(true);

        if (btnYes != null)
            btnYes.setOnClickListener(v -> {

                //clear resources
                JsonParser.cityListMap = null;

                Constants.singleSessionCategoryIdList.clear();
                exitDialog.dismiss();
                finish();
            });
        if (btnNo != null)
            btnNo.setOnClickListener(v -> exitDialog.dismiss());
        exitDialog.show();
    }

    void handleDynamicBrand(int index) {
        /*start mega offer*/
        CategoryInfo brandInfo = DatabaseClient.getInstance(this).getCategoryById(headerNavList.get(index).id);
        if (brandInfo != null) {
            ActivityUtil.openCategoryAccordingToAction(MainActivity.this, brandInfo, "");
            Tracking.trackGAEvent(this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.TOP_NAV, AppFlyerConst.Key.CLICK, brandInfo.gaEventLabel, AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, ""));
            CleverTapDB.getInstance(this).cleverTapTrackEvent(this, CTConstant.EVENT_NAME.HOME_TOP_RIGHT_ICONS_CLICK, CTConstant.PROP_NAME.ICON_NAME, brandInfo.displayName, LoginController.getUserDataMap());
        }
    }

    private void setDynamicBransLayoutVisibility() {
        if (headerNavList != null && headerNavList.size() > 0) {
            dynamicBrandLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDbVideoClick(int position) {
        selectedTab = position;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IN_APP_UPDATE_REQUEST_CODE) {
            if (resultCode == RESULT_CANCELED) {
                final Date lastShownDate = new Date();
                AppPreferences.getInstance(MainActivity.this).setIntValue(QuickPreferences.APP_UPDATE_SESSION_COUNT, 1);
                AppPreferences.getInstance(MainActivity.this).setLongValue(QuickPreferences.APP_UPDATE_POP_UP_SHOWN_DATE, lastShownDate.getTime());
            }
        } else if (requestCode == MainActivity.REQUEST_CODE_HOME_FEED_CUSTOMIZE) {
            if (resultCode == RESULT_OK) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
                if (fragment instanceof NewsFragment) {
                    ViewPager pager = ((NewsFragment) fragment).pager;
                    if (pager != null && pager.getAdapter() != null) {
                        NewsListFragment newsListFragment = (NewsListFragment) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                        newsListFragment.moveToTop();
                        newsListFragment.swipeRefreshLayout();
                    }
                }
            }
        } else if (requestCode == REQUEST_CODE_CALL_FOR_FOOTER_TAB && resultCode == RESULT_OK) {
            Fragment newsFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
            if (newsFragment instanceof NewsFragment) {
                newsFragment.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == LOGIN_REQUEST_CODE) {
            updateLoginView();
        } else {
            if (resultCode == RESULT_OK) {
                Fragment newsFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
                if (newsFragment instanceof NewsFragment) {
                    ViewPager pager = ((NewsFragment) newsFragment).pager;
                    if (pager.getAdapter() != null) {
                        if (pager.getAdapter().instantiateItem(pager, pager.getCurrentItem()) instanceof PrefferedCityNewsListFragment) {
                            PrefferedCityNewsListFragment fragment = (PrefferedCityNewsListFragment) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                            fragment.refreshFragment(null);
                        } else if (pager.getAdapter().instantiateItem(pager, pager.getCurrentItem()) instanceof MeraPageNewsFragment) {
                            MeraPageNewsFragment fragment = (MeraPageNewsFragment) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                            fragment.onActivityResult(requestCode, resultCode, data);
                        }
                    }
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void getAppInstallationTime() {
        Date mInstallDate = new Date();
        AppPreferences.getInstance(MainActivity.this).setLongValue(QuickPreferences.APP_INSTALL_DATE, mInstallDate.getTime());

        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        AppPreferences.getInstance(MainActivity.this).setStringValue(QuickPreferences.APP_INSTALL_DATE_IN_FORMAT, date);
        AppPreferences.getInstance(MainActivity.this).setBooleanValue(QuickPreferences.IS_APP_LAUNCED_FISRT_TIME, false);

        TrackingData.setAppInstallDate(MainActivity.this, date);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateLoginView();
    }

    private void updateLoginView() {
        mProfileInfo = LoginPreferences.getInstance(MainActivity.this).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        FantasyFragment fantasyFragment = (FantasyFragment) getSupportFragmentManager().findFragmentByTag(FantasyFragment.class.getName());
        if (fantasyFragment != null) {
            fantasyFragment.updateLoginView(mProfileInfo);
        }
    }

    @Override
    protected void onRestart() {
        initializeViews();
        super.onRestart();
    }

    private void initializeNavigationDrawer() {
        FragmentUtil.changeFragment(getSupportFragmentManager(), R.id.root_container, FantasyFragment.getInstance(), false, false);
    }

    private void tracking(int id, String gaEventLabel, String action, String displayName, boolean fromMore) {
        // Tracking
        String source = AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        String channel = CommonConstants.TRACKING_PREFIX;
        channel += "-";

        switch (id) {
            case NEWS_FRAGMENT:
                if (currentFragmentIndex != NEWS_FRAGMENT) {
                    Tracking.trackGAEvent(MainActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.Event.TAB, AppFlyerConst.GAAction.CLICK, gaEventLabel, campaign);
                    CleverTapDB.getInstance(MainActivity.this).cleverTapTrackEvent(MainActivity.this, CTConstant.EVENT_NAME.BOTTOM_BAR_CLICKS, CTConstant.PROP_NAME.CLICKON, fromMore ? "More_" + displayName : displayName, LoginController.getUserDataMap());
                }
                break;

            case PRIME_FRAGMENT:
                if (currentFragmentIndex != PRIME_FRAGMENT) {
                    Tracking.trackGAScreen(MainActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), channel + AppFlyerConst.GAScreen.BRAND, source, medium, campaign);
                    Tracking.trackGAEvent(MainActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.Event.TAB, AppFlyerConst.GAAction.CLICK, gaEventLabel, campaign);
                    CleverTapDB.getInstance(MainActivity.this).cleverTapTrackEvent(MainActivity.this, CTConstant.EVENT_NAME.BOTTOM_BAR_CLICKS, CTConstant.PROP_NAME.CLICKON, fromMore ? "More_" + displayName : displayName, LoginController.getUserDataMap());
                }
                break;

            case MORE_FRAGMENT:
                if (moreBottomRV.getVisibility() != View.VISIBLE) {
                    Tracking.trackGAEvent(MainActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.Event.TAB, AppFlyerConst.GAAction.CLICK, gaEventLabel, campaign);
                    CleverTapDB.getInstance(MainActivity.this).cleverTapTrackEvent(MainActivity.this, CTConstant.EVENT_NAME.BOTTOM_BAR_CLICKS, CTConstant.PROP_NAME.CLICKON, fromMore ? "More_" + displayName : displayName, LoginController.getUserDataMap());
                }
                break;

            default:
                if (Action.OUTSIDE_OPEN_CATEGORY_ACTIONS.contains(action)) {
                    Tracking.trackGAEvent(MainActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.Event.TAB, AppFlyerConst.GAAction.CLICK, gaEventLabel, campaign);
                    CleverTapDB.getInstance(MainActivity.this).cleverTapTrackEvent(MainActivity.this, CTConstant.EVENT_NAME.BOTTOM_BAR_CLICKS, CTConstant.PROP_NAME.CLICKON, fromMore ? "More_" + displayName : displayName, LoginController.getUserDataMap());
                } else {
                    if (currentFragmentIndex != id) {
                        Tracking.trackGAEvent(MainActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.Event.TAB, AppFlyerConst.GAAction.CLICK, gaEventLabel, campaign);
                        CleverTapDB.getInstance(MainActivity.this).cleverTapTrackEvent(MainActivity.this, CTConstant.EVENT_NAME.BOTTOM_BAR_CLICKS, CTConstant.PROP_NAME.CLICKON, fromMore ? "More_" + displayName : displayName, LoginController.getUserDataMap());
                    }
                }
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Save the user's current fragment state
        try {
            outState.putInt(Constants.KeyPair.KEY_FRAGMENT_VALUE, currentFragmentIndex);

            outState.putInt(Constants.HomeRecreateVariables.BOTTOM_POS, currentBottomPosition);
            outState.putBoolean(Constants.HomeRecreateVariables.IS_TAB_VISIBLE, isTabVisible);
            outState.putString(Constants.HomeRecreateVariables.ACTION_BAR_TEXT, actionBarText);
            super.onSaveInstanceState(outState);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            currentFragmentIndex = savedInstanceState.getInt(Constants.KeyPair.KEY_FRAGMENT_VALUE);
            currentBottomPosition = savedInstanceState.getInt(Constants.HomeRecreateVariables.BOTTOM_POS);
            isTabVisible = savedInstanceState.getBoolean(Constants.HomeRecreateVariables.IS_TAB_VISIBLE);
            actionBarText = savedInstanceState.getString(Constants.HomeRecreateVariables.ACTION_BAR_TEXT);

            if (isTabVisible) {
                findViewById(R.id.tab_layout).setVisibility(View.VISIBLE);
                titleTextView.setVisibility(GONE);
                imageTitleLayout.setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.tab_layout).setVisibility(GONE);
                imageTitleLayout.setVisibility(GONE);
                titleTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(actionBarText);
            }
        }
    }

    @Override
    protected void onStop() {
        AppUpdateUtils.getAppUpdateUtils(MainActivity.this).onUnregister();
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        try {
            unregisterReceiver(connectivityReceiver);
        } catch (Exception ignored) {
        }

        JsonParser.getInstance().appKill();
        currentFragmentIndex = -1;
        currentBottomPosition = -1;

        //stop song service
        stopSongService();
        Constants.IS_OVERLAY_PERMISSION_ASKED = false;
        DbSharedPref.Companion.setPromoWidgetVisibilityStatus(dbApp, true);
        super.onDestroy();
    }

    private void stopSongService() {
        try {
            Intent intent = new Intent(MainActivity.this, SongService.class);
            intent.putExtra("action", true);
            startService(intent);
        } catch (Exception ignored) {
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        boolean isFromBannerDynamicLink = intent.getBooleanExtra(Constants.KeyPair.KEY_IS_BANNER_DYNAMIC_LINK, false);
        if (isFromBannerDynamicLink) {
            handleDynamicLink(intent);
        } else {
            afterGettingIntentData(intent);
        }
    }

    private void handleDynamicLink(Intent intent) {
        String catIdMenuId = intent.getStringExtra(Constants.KeyPair.KEY_CATEGORY_ID);
        BannerViewHolder.bannerClickHandle(MainActivity.this, catIdMenuId);
    }

    private void afterGettingIntentData(Intent intent) {
        String categoryId = "";
        if (intent != null && intent.getExtras() != null) {
            categoryId = intent.getStringExtra(Constants.KeyPair.KEY_CATEGORY_ID);
        } else if (AppPreferences.getInstance(MainActivity.this).getBooleanValue(QuickPreferences.LastTabPrefs.TOGGLING_LAST_TAB_ACTIVE, false)) {
            categoryId = AppPreferences.getInstance(MainActivity.this).getStringValue(QuickPreferences.LAST_OPENED_CATEGORY, "");

        }
        if (!TextUtils.isEmpty(categoryId)) {
            if (JsonParser.getInstance().isCategoryAvailable(MainActivity.this, categoryId)) {
                categoryInfo = JsonParser.getInstance().getCategoryInfoBasedOnId(MainActivity.this, categoryId);
            }
        }
        if (categoryInfo != null) {
            boolean isOpened = ActivityUtil.openCategoryActionOutside(MainActivity.this, categoryInfo);
            if (!isOpened)
                addFragment(Integer.parseInt(categoryInfo.id), -1, null, true, true, AppFlyerConst.PrefixSection.HAMBERGER);
        }
    }


    @Override
    public void OnRelatedArticleClick(String storyId, String channelId, int indexPosition, String color, String headerName) {
        String gaArticle = AppFlyerConst.RecommendationValue.ARTICLE_VALUE;
        String gaScreen = AppFlyerConst.RecommendationValue.SCREEN_VALUE;
        startActivity(DivyaCommonArticleActivity.getIntent(MainActivity.this, storyId, "", gaArticle, gaScreen, categoryInfo.displayName, categoryInfo.gaEventLabel, categoryInfo.color));
    }

    @Override
    public void OnRelatedArticleClick(RelatedArticleInfo relatedArticleInfo) {

    }

    @Override
    public void onDataFetch(boolean flag) {
    }

    @Override
    public void onDataFetch(boolean flag, String sectionName) {
    }

    @Override
    public void onDataFetch(boolean flag, int viewType) {
    }

    @Override
    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {
        if (categoryInfo.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_TV)) {
            AppUtils.getInstance().callLiveTV(MainActivity.this, categoryInfo, MainActivity.this);
        }
    }

    @Override
    public void onItemClicked(String searchKeyword) {
        String gaArticle = AppFlyerConst.GAExtra.SEARCH_GA_ARTICLE;
        String gaEventLabel = AppFlyerConst.GAExtra.SEARCH_GA_ARTICLE;
        String gaScreen = AppFlyerConst.GAExtra.SEARCH_GA_SCREEN;
        Intent searchIntent = new Intent(MainActivity.this, SearchActivity.class);
        searchIntent.putExtra(Constants.KeyPair.KEY_KEYWORD, searchKeyword);
        searchIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        searchIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        searchIntent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
        searchIntent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, gaScreen);
        searchIntent.putExtra(Constants.KeyPair.KEY_IS_SEARCH, true);
        startActivity(searchIntent);
        if (searchFragmentDialog != null) {
            searchFragmentDialog.dismiss();
        }
    }

    @Override
    public void onActionClick(MenuItem menuItem) {
        if (menuItem != null) {
            if (menuItem.action.equals("profile")) {
                LoginController.loginController().getLoginIntent(MainActivity.this, TrackingData.getDBId(this), TrackingData.getDeviceId(this), LOGIN_REQUEST_CODE);
//                startActivityForResult(LoginController.loginController().getLoginIntent(MainActivity.this, TrackingData.getDBId(this), TrackingData.getDeviceId(this)), LOGIN_REQUEST_CODE);
                CleverTapDB.getInstance(MainActivity.this).cleverTapTrackEvent(MainActivity.this, CTConstant.EVENT_NAME.SIDE_MENU_CLICKS, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.PROFILE, LoginController.getUserDataMap());
            } else {
                CategoryInfo categoryInfo = JsonParser.getInstance().getCategoryInfoById(MainActivity.this, menuItem.id);
                if (categoryInfo != null) {
                    boolean isOpened = ActivityUtil.openCategoryActionOutside(MainActivity.this, categoryInfo);
                    if (!isOpened)
                        addFragment(Integer.parseInt(categoryInfo.id), -1, null, true, true, AppFlyerConst.PrefixSection.HAMBERGER);

                    CleverTapDB.getInstance(MainActivity.this).cleverTapTrackEvent(MainActivity.this, CTConstant.EVENT_NAME.SIDE_MENU_CLICKS, CTConstant.PROP_NAME.CLICKON, categoryInfo.displayName, LoginController.getUserDataMap());
                }
            }
        }
        closeNavigationDrawer();
    }

    @Override
    public void onArticleSuccess(ArticleCommonInfo commonInfo) {

    }

    @Override
    public void onShowingNextStory(boolean isAutoSwiped) {

    }

    @Override
    public void onShowingPreviousStory() {

    }

    @Override
    public boolean isViewPagerAnimationRunning() {
        return false;
    }


    @SuppressLint("StaticFieldLeak")
    private class GetGAClientId extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            Tracker gaTracker = ((InitApplication) getApplication()).getDefaultTracker();
            if (gaTracker != null)
                return gaTracker.get("&cid");
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Context context = MainActivity.this;
            if (s != null)
                AppPreferences.getInstance(context).setStringValue(QuickPreferences.ActiveUser.GA_CLIENT_ID, s);
        }
    }
}
