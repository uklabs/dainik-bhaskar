package com.bhaskar.view.ui.home;

import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bhaskar.R;
import com.bhaskar.util.Action;
import com.db.controller.WebController;
import com.db.data.models.NewsListInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.data.models.StoryListInfo;
import com.db.dbvideo.player.BusProvider;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.divya_new.data.CatHome2;
import com.db.divya_new.divyashree.HomeAdapter;
import com.db.listeners.FragmentLifecycle;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.OpenNewsLinkInAppV2;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;


public class HomeFragment extends Fragment implements FragmentLifecycle, OnMoveTopListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private HomeAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private String campaign;

    private int currentNetworkType;

    private int pageCount = 1;

    private int totalItemCount;
    private int lastVisibleItem;
    private int visibleThreshold = 2;
    private boolean isLoading = false;
    private boolean isLoadMoreEnabled = true;
    private WebView webViewBanner;
    private String feedUrl;

    public HomeFragment() {
    }

    public static HomeFragment newInstance(String feedUrl) {
        HomeFragment fragment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("feedurl", feedUrl);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        currentNetworkType = AppPreferences.getInstance(getContext()).getIntValue(QuickPreferences.FeedData.NETWORK_TYPE, NetworkStatus.NetworkType.NETWORK_NONE);
        adapter = new HomeAdapter(getActivity());

        Bundle arguments = getArguments();
        if (arguments != null) {
            feedUrl = arguments.getString("feedurl");
        }

        BusProvider.getInstance().register(this);
        loadData(false, 1);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));
        webViewBanner = view.findViewById(R.id.wv_homeBanner);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            isLoadMoreEnabled = true;
            VolleyNetworkSingleton.getInstance(getContext()).cancelRequest("HOMEPAGE");
            loadData(true, 1);
            addButtonTopWidget();
        });

        linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new SpacesDecoration(0, 0, 0, (int) getResources().getDimension(R.dimen.form_input_padding)));
        recyclerView.setAdapter(adapter);

        campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (currentNetworkType != NetworkStatus.NetworkType.NETWORK_WIFI && isLoadMoreEnabled) {
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            loadMoreData();
                        }
                    }
                }
            }
        });
        addButtonTopWidget();
        return view;
    }


    @Override
    public void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }


    private void loadData(boolean isRefresh, int pageCount) {
        switch (currentNetworkType) {
            case NetworkStatus.NetworkType.NETWORK_WIFI:
                Systr.println("HOME PAGE : network type : wifi, isRefresh : " + isRefresh);
                loadDataForWiFi(isRefresh);
                break;

            case NetworkStatus.NetworkType.NETWORK_4G:
                Systr.println("HOME PAGE : network type : 4g, isRefresh : " + isRefresh);
                loadDataFor4G(isRefresh, pageCount);
                break;

            default:
                Systr.println("HOME PAGE : network type : 3g or 2g, isRefresh : " + isRefresh);
                loadDataFor3G2G(isRefresh, pageCount);
                break;
        }
    }

    private void loadMoreData() {
        switch (currentNetworkType) {
            case NetworkStatus.NetworkType.NETWORK_4G:
                Systr.println("HOME PAGE : network type : 4g, load more called");
                getDataAccordingToPage(false, true, pageCount);
                break;

            case NetworkStatus.NetworkType.NETWORK_2G_3G:
                Systr.println("HOME PAGE : network type : 3g or 2g, load more called");
                getDataAccordingToPage(false, false, pageCount);
                break;
        }
    }

    private void loadDataForWiFi(boolean isRefresh) {
        if (!isRefresh) {
            try {
                String strPG1 = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.FeedData.HOME_PAGE_PG1, "");
                if (!TextUtils.isEmpty(strPG1)) {
                    parseJsonObjectFeedList(new JSONArray(strPG1), false, true, 1);
                }
            } catch (Exception ignore) {
            }
        }

        getDataOneTime();
    }

    private void loadDataFor4G(boolean isRefresh, int pageCount) {
        if (!isRefresh) {
            try {
                String strPG1 = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.FeedData.HOME_PAGE_PG1, "");
                if (!TextUtils.isEmpty(strPG1)) {
                    parseJsonObjectFeedList(new JSONArray(strPG1), false, true, pageCount);
                    pageCount = this.pageCount;
                }
            } catch (Exception ignore) {
            }
        }

        getDataAccordingToPage(isRefresh, true, pageCount);
    }

    private void loadDataFor3G2G(boolean isRefresh, int pageCount) {
        if (!isRefresh) {
            try {
                String strPG1 = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.FeedData.HOME_PAGE_PG1, "");
                if (!TextUtils.isEmpty(strPG1)) {
                    parseJsonObjectFeedList(new JSONArray(strPG1), false, true, pageCount);
                    pageCount = this.pageCount;
                }
            } catch (Exception ignore) {
            }
        }

        getDataAccordingToPage(isRefresh, false, pageCount);
    }

    private void getDataOneTime() {
//        String url = Urls.HOME_PAGE_URL + (Urls.IS_TESTING ? "?testing=1" : "");
        String url = Urls.APP_FEED_BASE_URL + feedUrl + (Urls.IS_TESTING ? "?testing=1" : "");
        getDataFromServer(url, true, false, false, 1);
    }

    private void getDataAccordingToPage(boolean isRefresh, boolean hitForNextPage, int pageCount) {
//        String url = Urls.HOME_PAGE_URL + "PG" + pageCount + "/" + (Urls.IS_TESTING ? "?testing=1" : "");
        String url = Urls.APP_FEED_BASE_URL + feedUrl + "PG" + pageCount + "/" + (Urls.IS_TESTING ? "?testing=1" : "");
        getDataFromServer(url, false, hitForNextPage, isRefresh, pageCount);
    }

    private void getDataFromServer(String url, boolean fullPage, boolean hitForNextPage, boolean clear, int pgCount) {
        Systr.println("HOME PAGE : hit on server : url : " + url + " , next page : " + hitForNextPage);
        isLoading = true;
        if (!fullPage && !clear) {
            adapter.addNullItem();
        }

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, response -> {
            Systr.println("HOME PAGE : GET DATA from server : " + response.toString());

            parseJsonObjectFeedList(response, fullPage, clear, pgCount);

            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }

            if (hitForNextPage) {
                Systr.println("HOME PAGE : hit for next page, from success");
                getDataAccordingToPage(false, false, pageCount);
            } else {
                isLoading = false;
            }

        }, error -> {
            Systr.println("HOME PAGE : ERROR from server : url : " + url);

            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }

            if (!fullPage && !clear) {
                adapter.removeNullItem();
            }

            if (hitForNextPage) {
                Systr.println("HOME PAGE : hit for next page, from error");
                getDataAccordingToPage(false, false, pgCount);
            } else {
                isLoading = false;
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag("HOMEPAGE");
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
    }


    private void addButtonTopWidget() {
        if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.ButtonTopWGTPref.IS_ACTIVE, false)) {
            String bannerUrl = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.ButtonTopWGTPref.BANNER_URL, "");
            WebController.getWebController().setWebViewSettings(getContext(), webViewBanner);
            new Handler().postDelayed(() -> {
                if (!TextUtils.isEmpty(bannerUrl)) {
                    webViewBanner.setVisibility(View.VISIBLE);
                    webViewBanner.loadUrl(bannerUrl);
                    webViewBanner.setVisibility(View.VISIBLE);
                } else {
                    webViewBanner.setVisibility(View.GONE);
                }
            }, 0);
            webViewBanner.setWebViewClient(new DbWebViewClient());
            webViewBanner.setWebChromeClient(new DbWebChromeClient());
        }
    }

    private class DbWebChromeClient extends android.webkit.WebChromeClient {

    }

    private class DbWebViewClient extends WebViewClient {

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            if (handler != null)
                handler.cancel();
        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains("bhaskar.com")) {
                return false;
            } else {
                OpenNewsLinkInAppV2 openNewsLinkInApp = new OpenNewsLinkInAppV2(getContext(), url, Constants.KeyPair.KEY_NEWS_UPDATE, 1);
                openNewsLinkInApp.openLink();
                AppLogs.printDebugLogs("webload url", url);
            }
            return true;
        }


        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            AppLogs.printDebugLogs("onPageFinished URL", url);
            if (url != null && url.contains("failure=1")) {
                webViewBanner.setVisibility(View.GONE);
            } else {
                webViewBanner.setVisibility(View.VISIBLE);
            }

            super.onPageFinished(view, url);
        }


    }

    @Override
    public void onPauseFragment() {
    }

    @Override
    public void onResumeFragment() {
        adapter.notifyDataSetChanged();
    }

    public void moveToTop() {
        if (recyclerView != null) {
            recyclerView.getLayoutManager().scrollToPosition(0);
        }
    }

    private void parseJsonObjectFeedList(JSONArray resultArray, boolean fullPage, boolean clear, int pgCount) {
        if (resultArray.length() == 0) {
            isLoadMoreEnabled = false;
            if (!fullPage) {
                adapter.removeNullItem();
            }
            adapter.addBlankItem();

            return;
        }

        pageCount = pgCount;

        ArrayList<CatHome2> list = new ArrayList<>();

        Gson gson = new Gson();
        for (int i = 0; i < resultArray.length(); i++) {
            try {
                JSONObject jsonObject = resultArray.getJSONObject(i);
                CatHome2 catHome = gson.fromJson(jsonObject.toString(), CatHome2.class);
                try {
                    catHome.list = getListAccordingToAction(gson, catHome.action, jsonObject.optJSONArray("list").toString());
                    if (!TextUtils.isEmpty(jsonObject.optString("flicker_active")))
                        catHome.flickerActive = jsonObject.optString("flicker_active").equalsIgnoreCase("1");
                } catch (Exception ignore) {
                }

                JSONArray jsonArray = jsonObject.optJSONArray("cat_sub_data");
                ArrayList<CatHome2> catSubDataList = new ArrayList<>();
                for (int j = 0; j < jsonArray.length(); j++) {
                    try {
                        JSONObject jsonObj = jsonArray.getJSONObject(j);
                        CatHome2 ch = gson.fromJson(jsonObj.toString(), CatHome2.class);
                        ch.list = getListAccordingToAction(gson, ch.action, jsonObj.optJSONArray("list").toString());
                        catSubDataList.add(ch);
                    } catch (Exception ignore) {
                    }
                }

                catHome.subDatalist = catSubDataList;
                if ((catHome.list != null && catHome.list.size() > 0) || (catHome.subDatalist.size() > 0) || (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_RASHIFAL_V2))) {
                    list.add(catHome);
                }

            } catch (Exception ignore) {
            }
        }

        if (clear) {
            adapter.clearAll();
        }

        if (fullPage) {
            CatHome2 ch = new CatHome2();
            ch.id = "BLANK";
            list.add(ch);

            adapter.addAllAndClear(list);

        } else {
            pageCount++;
            if (!clear) {
                adapter.removeNullItem();
            }
            adapter.addAll(list);
        }
    }

    private ArrayList<Object> getListAccordingToAction(Gson gson, String action, String json) {
        ArrayList<Object> list = new ArrayList<>();
        switch (action) {
            case Action.CategoryAction.CAT_ACTION_RAJYA:
            case Action.CategoryAction.CAT_ACTION_NEWS:
                list = new ArrayList<>(Arrays.asList(gson.fromJson(json, NewsListInfo[].class)));
                break;
            case Action.CategoryAction.CAT_ACTION_VIDEO_GALLERY_V3:
                list = new ArrayList<>(Arrays.asList(gson.fromJson(json, VideoInfo[].class)));
                break;
            case Action.CategoryAction.CAT_ACTION_DB_COLUMN:
            case Action.CategoryAction.CAT_ACTION_STORY_LIST:
            case Action.CategoryAction.CAT_ACTION_STORY_LIST_V2:
                list = new ArrayList<>(Arrays.asList(gson.fromJson(json, StoryListInfo[].class)));
                break;
            case Action.CategoryAction.CAT_ACTION_PHOTO_GALLERY:
                list = new ArrayList<>(Arrays.asList(gson.fromJson(json, PhotoListItemInfo[].class)));
                break;
            case Action.CategoryAction.CAT_ACTION_RASHIFAL_V2:
                break;
        }

        return list;
    }
}
