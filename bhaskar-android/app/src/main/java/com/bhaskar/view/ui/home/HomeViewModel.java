package com.bhaskar.view.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bhaskar.data.DataRepository;
import com.db.divya_new.data.CatHome2;

import org.json.JSONObject;

import java.util.List;

public class HomeViewModel extends ViewModel {
    private MutableLiveData<List<CatHome2>> mutableLiveData;
    private DataRepository dataRepository;

    public void init() {
        if (mutableLiveData != null) {
            return;
        }
        dataRepository = DataRepository.getInstance();
        mutableLiveData = new MutableLiveData<>();
    }

    public void fetchNews(String url) {
        mutableLiveData = dataRepository.getHomeNews(url,mutableLiveData);
    }


    public LiveData<List<CatHome2>> getNewsRepository() {
        return mutableLiveData;
    }

}
