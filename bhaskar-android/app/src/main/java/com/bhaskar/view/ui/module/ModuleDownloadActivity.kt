package com.bhaskar.view.ui.module

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bhaskar.util.ModuleUtilAll
import com.bhaskar.R
import com.db.util.Systr
import com.google.android.play.core.splitinstall.SplitInstallManager
import com.google.android.play.core.splitinstall.SplitInstallManagerFactory
import com.google.android.play.core.splitinstall.SplitInstallRequest
import com.google.android.play.core.splitinstall.SplitInstallStateUpdatedListener
import com.google.android.play.core.splitinstall.model.SplitInstallSessionStatus
import kotlinx.android.synthetic.main.activity_module_download.*
import kotlinx.android.synthetic.main.app_bar_layout.*

class ModuleDownloadActivity : AppCompatActivity() {

    private lateinit var splitInstallManager: SplitInstallManager
    private lateinit var moduleList: ArrayList<String>
    private var mySessionId = 0
    private var moduleType = 0
    private var requestCode = 0

    @SuppressLint("SwitchIntDef")
    private val stateListener = SplitInstallStateUpdatedListener { state ->
        Systr.println("MODULE : state listener : status : " + state.status() + ", session : " + state.sessionId() + ", mySession : $mySessionId")

        if (state.sessionId() == mySessionId) {
            // Read the status of the request to handle the state update.
            when (state.status()) {
                SplitInstallSessionStatus.DOWNLOADING -> {
                    val totalBytes = state.totalBytesToDownload()
                    val progress = state.bytesDownloaded()

                    val percentage = (progress * 100) / totalBytes
                    percent_tv.text = ("" + percentage)

                    Systr.println("MODULE : state listener : downloading : bytes : $totalBytes, progress : $progress")
                }
                SplitInstallSessionStatus.INSTALLED -> {
                    Systr.println("MODULE : state listener : installed")
                    launchModule()
                }
                SplitInstallSessionStatus.CANCELED -> {
                    Systr.println("MODULE : state listener : canceled")
                }
            }
        }
    }

    companion object {
        private const val MODULE_LIST = "module_list";
        private const val MODULE_TITLE = "module_title";
        private const val MODULE_DESC = "module_desc";
        private const val MODULE_TYPE = "module_type";
        private const val REQUEST_CODE = "request_code";

        fun newIntent(context: Context, title: String, desc: String, moduleName: ArrayList<String>, moduleType: Int, requestCode: Int): Intent {
            val intent = Intent(context, ModuleDownloadActivity::class.java);
            intent.putExtra(MODULE_TITLE, title)
            intent.putExtra(MODULE_DESC, desc)
            intent.putExtra(MODULE_LIST, moduleName)
            intent.putExtra(MODULE_TYPE, moduleType)
            intent.putExtra(REQUEST_CODE, requestCode)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_module_download)

        splitInstallManager = SplitInstallManagerFactory.create(this)

        moduleList = intent.getStringArrayListExtra(MODULE_LIST)
        val title = intent.getStringExtra(MODULE_TITLE)
        val desc = intent.getStringExtra(MODULE_DESC)
        moduleType = intent.getIntExtra(MODULE_TYPE, 0)
        requestCode = intent.getIntExtra(REQUEST_CODE, 0)

//        setSupportActionBar(toolbar)
//        toolbar.title = title
        desc_tv.text = desc

        startToDownload(moduleList)
    }

    private fun startToDownload(name: ArrayList<String>) {
        val requestBuilder = SplitInstallRequest.newBuilder()
        for (value: String in name)
            requestBuilder.addModule(value)
        val request = requestBuilder.build()

        splitInstallManager.startInstall(request)
                .addOnSuccessListener { sessionId ->
                    Systr.println("MODULE : onSuccess : " + sessionId!!)
                    mySessionId = sessionId
                }
                .addOnFailureListener { e ->
                    Systr.println("MODULE : onFailure : " + e.message)
                }
    }

    override fun onResume() {
        super.onResume()
        splitInstallManager.registerListener(stateListener)
    }

    override fun onPause() {
        super.onPause()
        splitInstallManager.unregisterListener(stateListener)
    }

    private fun launchModule() {
        ModuleUtilAll.getInstance(this)?.openModule(moduleType, this, requestCode)
        finish()
    }
}