package com.bhaskar.view.ui.myPage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.main.BaseAppCompatActivity;
import com.db.settings.NotificationSettingFragment;
import com.db.settings.QuickReadFragment;
import com.db.settings.VideoSettingFragment;
import com.db.util.Constants;
import com.db.util.FragmentUtil;
import com.db.util.ThemeUtil;

public class FragmentDetailActivity extends BaseAppCompatActivity {

    private CategoryInfo categoryInfo;
    private boolean isFromSettings;
    private String action;

    public static Intent getIntent(Context context, CategoryInfo categoryInfo, boolean isFromSettings, String action) {
        Intent intent = new Intent(context, FragmentDetailActivity.class);
        intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        intent.putExtra("isFromSettings", isFromSettings);
        intent.putExtra("action", action);

        return intent;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_mera_page_details);
        if (getIntent() != null) {
            categoryInfo = (CategoryInfo) getIntent().getSerializableExtra(Constants.KeyPair.KEY_CATEGORY_INFO);
            isFromSettings = getIntent().getBooleanExtra("isFromSettings", false);
            action = getIntent().getStringExtra("action");
        }

        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(v -> FragmentDetailActivity.this.finish());
        TextView tvTitle = findViewById(R.id.toolbar_title);
        tvTitle.setTextColor(Color.BLACK);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ica_back_black);
            actionBar.setTitle(null);
        }
        String title = "";
        switch (action) {
            case Constants.SettingAction.MERA_PAGE:
                title = getString(R.string.mera_page_title);
                FragmentUtil.changeFragment(getSupportFragmentManager(), R.id.root_container, MeraPageFragment.newInstance(isFromSettings), false, false);
                break;
            case Constants.SettingAction.NOTIFICATION:
                title = getString(R.string.title_notification);
                FragmentUtil.changeFragment(getSupportFragmentManager(), R.id.root_container, NotificationSettingFragment.getInstance(), false, false);
                break;
            case Constants.SettingAction.QUICK_READ:
                title = getString(R.string.title_quick_reads);
                FragmentUtil.changeFragment(getSupportFragmentManager(), R.id.root_container, QuickReadFragment.getInstance(), false, false);
                break;
            case Constants.SettingAction.VIDEO_SETTING:
                title = getString(R.string.title_video_settings);
                FragmentUtil.changeFragment(getSupportFragmentManager(), R.id.root_container, VideoSettingFragment.getInstance(), false, false);
                break;

        }

        tvTitle.setText(title);
    }

}
