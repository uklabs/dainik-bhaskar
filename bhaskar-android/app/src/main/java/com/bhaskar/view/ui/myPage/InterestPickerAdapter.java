package com.bhaskar.view.ui.myPage;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.bhaskar.data.model.Interest;
import com.db.util.AppUtils;
import com.igalata.bubblepicker.adapter.BubblePickerAdapter;
import com.igalata.bubblepicker.model.BubbleGradient;
import com.igalata.bubblepicker.model.PickerItem;

import java.util.ArrayList;
import java.util.List;

public class InterestPickerAdapter implements BubblePickerAdapter {

    private Context context;
    private List<Interest> interestList;
    private int txtColor;
    private float bubbleTextSize;

    /**
     * @param context
     * @param interestList
     */
    public InterestPickerAdapter(Context context, List<Interest> interestList) {
        this.context = context;
        this.interestList = interestList;
        txtColor = ContextCompat.getColor(context, android.R.color.white);
        bubbleTextSize = AppUtils.getInstance().convertDpToPixel(23,context);
    }

    @Override
    public int getTotalCount() {
        if (interestList == null) {
            return 0;
        } else {
            return interestList.size();
        }

    }

    @NonNull
    @Override
    public PickerItem getItem(int position) {

        Interest interest = interestList.get(position);

        PickerItem pickerItem = new PickerItem();

        pickerItem.setTitle(interest.getName());

        pickerItem.setGradient(new BubbleGradient(AppUtils.getThemeColor(context, interest.getColor()),
                AppUtils.getThemeColor(context, interest.getColor()), BubbleGradient.VERTICAL));


        pickerItem.setTextColor(txtColor);
        pickerItem.setSelected(interest.isSelected());
        pickerItem.setTextSize(bubbleTextSize);
        pickerItem.setOverlayAlpha(0.6f);

        return pickerItem;
    }

    Interest getInterest(String interestLabel) {
        for (Interest interest : interestList) {
            if (interest.getName().equals(interestLabel)) {
                return interest;
            }
        }
        return null;
    }

    public List<Interest> getSelectedInterestList() {
        List<Interest> interests = new ArrayList<>();
        for (Interest interest : interestList) {
            if (interest.isSelected())
                interests.add(interest);
        }
        return interests;
    }

    public String getInterestName(String interestLabel) {
        String name = "";
        for (Interest interest : interestList) {
            if (interest.getName().equals(interestLabel)) {
                name = interest.getName();
                break;
            }
        }
        return name;
    }

}
