package com.bhaskar.view.ui.myPage;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.data.api.ApiConstants;
import com.bhaskar.data.api.ApiHelper;
import com.bhaskar.data.model.Interest;
import com.bhaskar.util.LoginController;
import com.bhaskar.R;
import com.db.appintro.utils.DialogUtils;
import com.db.settings.SettingActivity;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.igalata.bubblepicker.BubblePickerListener;
import com.igalata.bubblepicker.model.PickerItem;
import com.igalata.bubblepicker.rendering.BubblePicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MeraPageFragment extends Fragment {
    private static final int BUBBLE_SIZE = 25;
    private FrameLayout mPickerContainer;
    private List<Interest> mList;
    private ProgressBar progressBar;
    private BubblePicker picker;
    private InterestPickerAdapter interestPickerAdapter;
    private ProgressBar interestSelectionBar;
    private boolean isFromSettings;
    private String selectedIds;
    private MeraPageFragmentViewModel meraPageFragmentViewModel;

    public static MeraPageFragment newInstance(boolean isFromSettings) {
        MeraPageFragment fragment = new MeraPageFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFromSettings", isFromSettings);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isFromSettings = getArguments().getBoolean("isFromSettings");
            mList = new ArrayList<>();
        }
        selectedIds = AppPreferences.getInstance(getContext()).getStringValue(Constants.KeyPair.KEY_SELECTED_MY_PAGE_IDS, "");
        meraPageFragmentViewModel = ViewModelProviders.of(this).get(MeraPageFragmentViewModel.class);
        meraPageFragmentViewModel.init();
        meraPageFragmentViewModel.getRepository().observe(this, newsResponse -> {
            showProgress(false);
            handleResponse(newsResponse.getData());
        });
    }

    private void handleResponse(List<Interest> data) {
        mList.clear();
        mList.addAll(data);
        List<String> selectedList = Arrays.asList(selectedIds.split(","));

        for (Interest interest : mList) {
            if (selectedList.contains(String.valueOf(interest.getId()))) {
                interest.setSelected(true);
            }
        }
        showInterests(mList);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mera_page, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        interestSelectionBar = view.findViewById(R.id.progressBarCount);
        mPickerContainer = view.findViewById(R.id.pickerContainer);
        TextView tvOk = view.findViewById(R.id.tv_ok);
        TextView tvReset = view.findViewById(R.id.tv_reset);
        View okView = view.findViewById(R.id.vCenterOK);
        okView.setOnClickListener(v -> onOk());
        tvReset.setOnClickListener(v -> onReset());

        interestSelectionBar.setMax(3);
        interestSelectionBar.setProgress(0);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showProgress(true);
        meraPageFragmentViewModel.getCategories(String.format(ApiConstants.MERA_PAGE_CATEGORY_URL,CommonConstants.CHANNEL_ID));
    }

    private void onReset() {
        interestSelectionBar.setProgress(0);
        for (Interest interest : mList) {
            interest.setSelected(false);
        }
        showInterests(mList);
    }

    private void onOk() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Interest interest : mList) {
            if (interest.isSelected()) {
                stringBuilder.append(interest.getId()).append(",");
//                nameList.append(interest.getName()).append(",");
            }
        }
        String selectedIds = stringBuilder.toString();
        if (!TextUtils.isEmpty(selectedIds) && interestPickerAdapter.getSelectedInterestList().size() >= 3) {
            AppPreferences.getInstance(getContext()).setStringValue(Constants.KeyPair.KEY_SELECTED_MY_PAGE_IDS, selectedIds.substring(0, selectedIds.length() - 1));
            if (isFromSettings) {
                finishActivity();
            } else {
                showAcknowlegeDialog();
            }

//            String names = nameList.substring(0, nameList.length() - 1);
            String ids = selectedIds.substring(0, selectedIds.length() - 1);
            CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.MERA_PAGE, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.SAVE, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.CATEGORIES_NAME, ids));
        } else {
            AppUtils.getInstance().showCustomToast(getContext(), "Please Select atleast 3 Categories to proceed.");
        }
    }

    private void showAcknowlegeDialog() {
        DialogUtils.showInfoDialog(getActivity(), getString(R.string.alert_title_success), getString(R.string.mera_page_dialog_sub_header), () -> {
            finishActivity();
        });
    }


    private void finishActivity() {
        Objects.requireNonNull(getActivity()).setResult(Activity.RESULT_OK);
        Objects.requireNonNull(getActivity()).finish();
    }

    private void setDefaultSelectedBubble(List<Interest> interestIds) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            interestSelectionBar.setProgress(interestIds.size(), true);
        } else {
            interestSelectionBar.setProgress(interestIds.size());
        }
    }



    private void showProgress(boolean show) {
        if (show) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void showInterests(final List<Interest> interestList) {
        try {
            if (picker != null) {
                mPickerContainer.removeView(picker);
            }
            picker = (BubblePicker) getLayoutInflater().inflate(R.layout.picker_bubble,
                    (ViewGroup) getView(), false);
            mPickerContainer.addView(picker, 0);
            picker.setBubbleSize(BUBBLE_SIZE);
            picker.setCenterImmediately(true);
            picker.setMaxSelectedCount(3);

            picker.setListener(new BubblePickerListener() {
                @Override
                public void onBubbleSelected(@NonNull PickerItem pickerItem) {
                    Interest interest = interestPickerAdapter.getInterest(pickerItem.getTitle());
                    interest.setSelected(true);
                    setDefaultSelectedBubble(interestPickerAdapter.getSelectedInterestList());
                }

                @Override
                public void onBubbleDeselected(@NonNull PickerItem pickerItem) {
                    Interest interest = interestPickerAdapter.getInterest(pickerItem.getTitle());
                    interest.setSelected(false);
                    setDefaultSelectedBubble(interestPickerAdapter.getSelectedInterestList());
                }
            });
            interestPickerAdapter = new InterestPickerAdapter(getContext(), interestList);
            picker.setAdapter(interestPickerAdapter);
            setDefaultSelectedBubble(interestPickerAdapter.getSelectedInterestList());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (isFromSettings && getActivity() instanceof SettingActivity)
            ((SettingActivity) Objects.requireNonNull(getActivity())).setHeaderTitle(getString(R.string.mera_page_title));
        if (picker != null) {
            picker.onResume();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (picker != null) {
            picker.onPause();
        }
    }
}
