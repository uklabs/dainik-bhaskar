package com.bhaskar.view.ui.myPage;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bhaskar.data.DataRepository;
import com.bhaskar.data.model.MeraPageCategoryResponse;
import com.db.divya_new.data.CatHome2;

import java.util.List;

public class MeraPageFragmentViewModel extends ViewModel {
    private MutableLiveData<MeraPageCategoryResponse> mutableLiveData;
    private DataRepository dataRepository;

    public void init() {
        if (mutableLiveData != null) {
            return;
        }
        dataRepository = DataRepository.getInstance();
        mutableLiveData = new MutableLiveData<>();
    }

    void getCategories(String url) {
        mutableLiveData = dataRepository.getInterestCategories(url,mutableLiveData);
    }


    LiveData<MeraPageCategoryResponse> getRepository() {
        return mutableLiveData;
    }

}
