package com.bhaskar.view.ui.myPage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.data.api.ApiConstants;
import com.bhaskar.data.api.ApiHelper;
import com.bhaskar.data.model.NewsResponse;
import com.db.InitApplication;
import com.db.ads.AdPlacementController;
import com.db.ads.NativeListAdController2;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.data.CatHomeData;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bhaskar.view.ui.myPage.MeraPageNewsAdapter.VIEW_TYPE_LOADING;

public class MeraPageNewsFragment extends Fragment implements NativeListAdController2.onAdNotifyListener {
    private boolean isLoading = false;
    private ArrayList<Object> mList;
    private String finalFeedURL;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    private MeraPageNewsAdapter mAdapter;
    private CategoryInfo categoryInfo;
    private int LOAD_MORE_COUNT = 1;
    private String selectedIds;
    private int totalItemCount;
    private int lastVisibleItem;
    private int visibleThreshold = 5;
    private int count;
    private View vEmptyView;
    private List<BannerInfo> bannerInfoList;
    private MeraPageNewsFragmentViewModel meraPageNewsFragmentViewModel;
    private boolean isClear;

    public static MeraPageNewsFragment newInstance(CategoryInfo categoryInfo) {
        MeraPageNewsFragment fragment = new MeraPageNewsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedIds = AppPreferences.getInstance(getContext()).getStringValue(Constants.KeyPair.KEY_SELECTED_MY_PAGE_IDS, "");
        mList = new ArrayList<>();
        if (getArguments() != null) {
            categoryInfo = (CategoryInfo) getArguments().getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        /*Banner*/
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);

        meraPageNewsFragmentViewModel = ViewModelProviders.of(this).get(MeraPageNewsFragmentViewModel.class);
        meraPageNewsFragmentViewModel.init();
        meraPageNewsFragmentViewModel.getRepository().observe(this, this::handleResponse);
    }

    private void handleResponse(NewsResponse response) {
        if(response!=null){
            isLoading = false;
            showProgress(false);
            swipeRefreshLayout.setRefreshing(false);
            try {
                count = Integer.parseInt(response.getCount());
                List<NewsListInfo> founderList = response.getFeed();
                int length = founderList.size();
                if (length > 0) {
                    if (LOAD_MORE_COUNT < 5) {
                        founderList = AdPlacementController.getInstance().addNewsListingNativeAds(founderList, getContext());
                    }
                    parseJsonResponse(founderList, isClear);
                } else {
                    removeLastItemFromTheList(mList);
                    if (LOAD_MORE_COUNT > 1) {
                        LOAD_MORE_COUNT--;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                AppUtils.getInstance().showCustomToast(getContext(), e.getMessage());
            }
        }else {
            showProgress(false);
            isLoading = false;
            swipeRefreshLayout.setRefreshing(false);
            AppUtils.getInstance().showCustomToast(getContext(), getResources().getString(R.string.no_network_error));
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mera_page_news_listing, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
            onRefresh(false);
        });
        recyclerView = view.findViewById(R.id.recycler_view);
        mAdapter = new MeraPageNewsAdapter(getContext(), mList, categoryInfo);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        setInfiniteScrolling(layoutManager);

        vEmptyView = view.findViewById(R.id.vEmptyView);
        TextView tvAddCat = view.findViewById(R.id.button_add_cat);
        tvAddCat.setOnClickListener(v -> {
            startActivityForResult(FragmentDetailActivity.getIntent(getContext(), categoryInfo, false, Constants.SettingAction.MERA_PAGE), 109);
        });
        return view;
    }

    private void onRefresh(boolean showprogress) {
        LOAD_MORE_COUNT = 1;
        selectedIds = AppPreferences.getInstance(getContext()).getStringValue(Constants.KeyPair.KEY_SELECTED_MY_PAGE_IDS, "");
        if (TextUtils.isEmpty(selectedIds)) {
            vEmptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
        } else {
            vEmptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            makeJsonObjectRequest(true, showprogress);
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (TextUtils.isEmpty(selectedIds)) {
            vEmptyView.setVisibility(View.VISIBLE);
        } else {
            vEmptyView.setVisibility(View.GONE);
            CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(categoryInfo.id);
            long currentTime = System.currentTimeMillis();
            if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && LOAD_MORE_COUNT <= 2) {
                parseOfflineData(catHomeData);
            } else if (!isLoading) {
                makeJsonObjectRequest(true, true);
            }
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            onRefresh(true);
        }
    }

    private void parseOfflineData(CatHomeData catHomeData) {
        List<NewsListInfo> newsListInfos = new ArrayList<>((Collection<? extends NewsListInfo>) catHomeData.getArrayList(CatHomeData.TYPE_MERA_PAGE));
        if (!newsListInfos.isEmpty()) {
            //passing the data to the adapter to show.
            mList.clear();
            mList.addAll(newsListInfos);
            mAdapter.notifyDataSetChanged();
        } else {
            makeJsonObjectRequest(true, true);
        }
    }

    private void setInfiniteScrolling(LinearLayoutManager linearLayoutManager) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        LOAD_MORE_COUNT++;
                        makeJsonObjectRequest(false, false);
                    }
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (mAdapter != null) {
                    int i = linearLayoutManager.findFirstVisibleItemPosition();
                    int j = linearLayoutManager.findLastVisibleItemPosition();
                    for (int k = i; k <= j; k++) {
                        NewsListInfo itemAtPosition = mAdapter.getItemAtPosition(k);
                        if (itemAtPosition != null && itemAtPosition.isAd) {
                            NativeListAdController2.getInstance(getContext()).getAdRequest(itemAtPosition.adUnit, itemAtPosition.adType, k, new WeakReference<NativeListAdController2.onAdNotifyListener>(MeraPageNewsFragment.this));
                        }
                    }
                }
            }
        });
    }


    private void showProgress(boolean show) {
        if (show) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void makeJsonObjectRequest(boolean isClear, boolean showProgress) {
        this.isClear = isClear;
        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            if (showProgress)
                showProgress(true);
            finalFeedURL = String.format(ApiConstants.MERA_PAGE_NEWS_URL, CommonConstants.CHANNEL_ID) + selectedIds + "/PG" + LOAD_MORE_COUNT + "/";
            isLoading = true;
            meraPageNewsFragmentViewModel.getMeraPageNews(finalFeedURL);
           /* JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        Systr.println(String.format(" Feed API %s response %s", finalFeedURL, response.toString()));
                        isLoading = false;
                        showProgress(false);
                        swipeRefreshLayout.setRefreshing(false);
                        try {
                            count = Integer.parseInt(response.optString("count"));
                            List<NewsListInfo> founderList = new ArrayList<>(Arrays.asList(new Gson().fromJson(response.getJSONArray("feed").toString(), NewsListInfo[].class)));
                            int length = founderList.size();
                            if (length > 0) {
                                if (LOAD_MORE_COUNT < 5) {
                                    founderList = AdPlacementController.getInstance().addNewsListingNativeAds(founderList, getContext());
                                }
                                parseJsonResponse(founderList, isClear);
                            } else {
                                removeLastItemFromTheList(mList);
                                if (LOAD_MORE_COUNT > 1) {
                                    LOAD_MORE_COUNT--;
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            AppUtils.getInstance().showCustomToast(getContext(), e.getMessage());
                        }
                    }, error -> {


            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);*/
        } else {
            if (isAdded() && getActivity() != null)
                AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
        }
    }

    private void parseJsonResponse(List<NewsListInfo> response, boolean isClear) {
        if (isClear) {
            mList.clear();
            mList.addAll(response);
            //save for offline
            if (LOAD_MORE_COUNT <= 1 && !TextUtils.isEmpty(categoryInfo.id)) {
                CatHomeData catHomeData = new CatHomeData(mList, count, CatHomeData.TYPE_MERA_PAGE, System.currentTimeMillis());
                DataParser.getInstance().addCatHomeDataHashMap(categoryInfo.id, catHomeData);
            }
            addAdsAndWidgetInCommonList();
            if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
                mList.add(0, AdController.showATFForAllListing(getContext(), mAdapter));
            }
            mList.add(VIEW_TYPE_LOADING);
        } else {
            removeLastItemFromTheList(mList);
            mList.addAll(response);
            mList.add(VIEW_TYPE_LOADING);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void removeLastItemFromTheList(List<Object> newsInfoList) {
        if (newsInfoList != null) {
            if (newsInfoList.size() > 0) {
                newsInfoList.remove(newsInfoList.size() - 1);
            }
        }
    }

    public void addAdsAndWidgetInCommonList() {
        for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
            if (!bannerInfoList.get(bannerIndex).isAdded) {
                int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catPos);
                if (bannerPos > -1 && mList.size() >= bannerPos) {
                    mList.add(bannerPos, new NewsListInfo(MeraPageNewsAdapter.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)));
                    bannerInfoList.get(bannerIndex).isAdded = true;
                }
            }
        }
    }

    @Override
    public void onNotifyAd() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }
}
