package com.bhaskar.view.ui.myPage;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bhaskar.data.DataRepository;
import com.bhaskar.data.model.MeraPageCategoryResponse;
import com.bhaskar.data.model.NewsResponse;

public class MeraPageNewsFragmentViewModel extends ViewModel {
    private MutableLiveData<NewsResponse> mutableLiveData;
    private DataRepository dataRepository;

    public void init() {
        if (mutableLiveData != null) {
            return;
        }
        dataRepository = DataRepository.getInstance();
        mutableLiveData = new MutableLiveData<>();
    }

    void getMeraPageNews(String url) {
        mutableLiveData = dataRepository.getMeraPageNews(url,mutableLiveData);
    }

    LiveData<NewsResponse> getRepository() {
        return mutableLiveData;
    }

}
