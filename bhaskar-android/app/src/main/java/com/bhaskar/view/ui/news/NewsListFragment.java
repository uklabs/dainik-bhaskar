package com.bhaskar.view.ui.news;

import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.model.NewsResponse;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.db.ads.AdPlacementController;
import com.db.ads.NativeListAdController2;
import com.db.data.database.CommonListTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.WebBannerInfo;
import com.db.dbvideo.player.BusProvider;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.home.CommonListAdapter;
import com.db.listeners.FragmentLifecycle;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NewsListFragment extends Fragment implements OnFeedFetchFromServerListener, OnMoveTopListener, NativeListAdController2.onAdNotifyListener, FragmentLifecycle {
    private final String TAG = AppConfig.BaseTag + "." + NewsListFragment.class.getSimpleName();

    //HpBig
    private InitApplication application;
    Handler handler;
    private ProgressBar progressBar;

    //lazy loading
    public boolean isLoading;
    private int LOAD_MORE_COUNT = 0;
    private int visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
    private int lastVisibleItem, totalItemCount;

    private ArrayList<NewsListInfo> newsInfoList;
    private CommonListAdapter commonListAdapter;
    private String finalFeedURL;
    private CategoryInfo categoryInfo;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Integer> trackingList = new ArrayList<>();
    private boolean isShowing = false;
    //widgets
    private boolean isStockActive;
    private boolean isStockAdded;

    private boolean isFlickerAdded;

    private int listIndex = 0;
    private List<BannerInfo> bannerInfoList;
    private LinearLayoutManager linearLayoutManager;

    private boolean isFlickerActive;
    private List<WebBannerInfo> webBannerInfoList;
    private NewsListFragmentViewModel newsListFragmentViewModel;

    public NewsListFragment() {
    }

    public static NewsListFragment newInstance(CategoryInfo categoryInfo, boolean flag, String parentDisplayName) {
        NewsListFragment fragmentFirst = new NewsListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putBoolean("flag", flag);
        bundle.putString("displayName", parentDisplayName);
        fragmentFirst.setArguments(bundle);
        return fragmentFirst;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.application = InitApplication.getInstance();
        isStockActive = AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.StockPrefs.STOCK_IS_ACTIVE, false);
        BusProvider.getInstance().register(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
        newsListFragmentViewModel = ViewModelProviders.of(this).get(NewsListFragmentViewModel.class);
        newsListFragmentViewModel.init();
        newsListFragmentViewModel.getRepository().observe(this, this::handleNewsResponse);
    }

    private void handleNewsResponse(NewsResponse newsResponse) {
        if (newsResponse != null) {
            if (!Constants.singleSessionCategoryIdList.contains(categoryInfo.id)) {
                Constants.singleSessionCategoryIdList.add(categoryInfo.id);
            }
            parseJsonObjectFeedList(newsResponse);
            if (LOAD_MORE_COUNT > 0) {
                removeLoadingFromBottom();
            }
        } else {
            hideProgress();
            if (LOAD_MORE_COUNT > 0) {
                removeLoadingFromBottom();
            }
            AppUtils.getInstance().showCustomToast(getContext(), getResources().getString(R.string.sorry_error_found_please_try_again_));
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_list_fragment_main, container, false);

        progressBar = rootView.findViewById(R.id.progress_bar);
        if (getActivity() != null) {
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
        recyclerView = rootView.findViewById(R.id.recycler_view);
        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        handler = new Handler();

        commonListAdapter = new CommonListAdapter(getActivity(), categoryInfo.gaScreen, categoryInfo.color, categoryInfo);
        recyclerView.setAdapter(commonListAdapter);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!NetworkStatus.getInstance().isConnected(getActivity()) || Constants.singleSessionCategoryIdList.contains(categoryInfo.id)) {
                    isTokenExpire();
                } else {
                    swipeRefreshLayout();
                }
            }
        }, Constants.DataBaseHit.DEFAULT_TIME_IS_TOKEN);


        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                swipeRefreshLayout();
            } else {
                hideProgress();
                if (isAdded() && getActivity() != null)
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));

            }
        });
        if (MainActivity.tabReClicked)
            recyclerView.scrollToPosition(0);
        return rootView;
    }

    /**
     * Call for refreshing the content by pull to refresh and event when internet has come
     */
    public void swipeRefreshLayout() {
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);

        if (newsInfoList == null) {
            newsInfoList = new ArrayList<>();
        } else {
            newsInfoList.clear();
        }

        listIndex = 0;
        LOAD_MORE_COUNT = 0;
        isFlickerAdded = false;
        isStockAdded = false;
        setApiUrl();
        makeJsonObjectRequest();
    }

    private void swipeRefreshLayoutWithOutApi() {
        LOAD_MORE_COUNT = 0;

        setApiUrl();
        if (!categoryInfo.feedUrl.endsWith("1/"))
            LOAD_MORE_COUNT++;
    }

    private void setApiUrl() {
        if (categoryInfo.feedUrl.endsWith("1/")) {
            finalFeedURL = categoryInfo.feedUrl + "PG1/";
        } else {
            finalFeedURL = categoryInfo.feedUrl + "PG" + (LOAD_MORE_COUNT + 1) + "/";
        }
    }


    public void moveToTop() {
        Systr.println("Move to top called");
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Rect scrollBounds = new Rect();
        this.recyclerView.getHitRect(scrollBounds);
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            LOAD_MORE_COUNT++;
                            onLoadMore();
                        }
                    }
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int i = linearLayoutManager.findFirstVisibleItemPosition();
                int j = linearLayoutManager.findLastVisibleItemPosition();
                for (int k = i; k <= j; k++) {
                    NewsListInfo itemAtPosition = commonListAdapter.getItemAtPosition(k);
                    if (itemAtPosition != null && itemAtPosition.isAd) {
                        NativeListAdController2.getInstance(getContext()).getAdRequest(itemAtPosition.adUnit, itemAtPosition.adType, k, new WeakReference<NativeListAdController2.onAdNotifyListener>(NewsListFragment.this));
                    }
                }
            }
        });
    }

    @Override
    public void onNotifyAd() {
        if (commonListAdapter != null) {
            commonListAdapter.notifyDataSetChanged();
        }
    }

    public void setFragmentShow() {
        isShowing = true;
        sendTracking();
    }

    private void sendTracking() {
        // Tracking
        if (isShowing) {
            if (categoryInfo != null && trackingList != null) {
                while (trackingList.size() > 0) {
                    int value = trackingList.get(0) + (categoryInfo.feedUrl.endsWith("1/") ? 1 : 0);
                    trackingList.remove(0);

                    if (value > 1) {
                        value = value - 1;
                        String ga_action = categoryInfo.displayName;
                        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_DEPTH, ga_action, AppFlyerConst.GALabel.PG + value, campaign);
                        Systr.println("GA Events : " + AppFlyerConst.GACategory.SCROLL_DEPTH + ga_action + AppFlyerConst.GALabel.PG + value + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));
                    }
                }
            }
        }
    }

    private void onLoadMore() {
        isLoading = true;
        recyclerView.post(() -> {
            if (commonListAdapter != null)
                commonListAdapter.addItem();
        });
        new Handler().postDelayed(() -> {
            finalFeedURL = AppUtils.getInstance().makeUrlForLoadMore(categoryInfo.feedUrl, LOAD_MORE_COUNT);
            makeJsonObjectRequest();
        }, 1000);
    }

    private void makeJsonObjectRequest() {
        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            if (!isLoading && swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            AppLogs.printDebugLogs("Feed URL", "== " + finalFeedURL);

            //make api call
            newsListFragmentViewModel.getNewsList(finalFeedURL);
        } else {
            if (isAdded() && getActivity() != null)
                AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));

            hideProgress();
        }
    }

    private void setOfflineCommonData() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                /**
                 * First show offline data
                 */
                try {
                    showProgressBar();
                    boolean isShowInternetMessage = true;
                    if (newsInfoList == null) {
                        newsInfoList = new ArrayList<>();
                    } else {
                        newsInfoList.clear();
                    }
                    CommonListTable commonListTable = (CommonListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CommonListTable.TABLE_NAME);

                    List<NewsListInfo> newsListInfos = commonListTable.getAllNewsListInfoListAccordingToCategory(categoryInfo.id, getContext());
                    for (NewsListInfo newsInfo : newsListInfos) {
                        ++listIndex;
                        newsInfo.listIndex = listIndex;
                    }
                    newsInfoList.addAll(newsListInfos);

                    addStockPhotoFlickerWidget();
//                  newsInfoList.addAll(commonListTable.getAllNewsListInfoListAccordingToCategory(categoryInfo.id, getContext()));
                    if (newsInfoList != null && newsInfoList.size() > 0) {
                        isShowInternetMessage = false;
                        addBanner();
                        addWebBanner();
                    }
                    commonListAdapter.setData(newsInfoList, categoryInfo.detailUrl);
                    commonListAdapter.notifyDataSetChanged();
                    hideProgress();
                    swipeRefreshLayoutWithOutApi();
                    checkInternetAndShowToast(isShowInternetMessage);
                } catch (Exception ex) {
                    hideProgress();
                    AppLogs.printErrorLogs(TAG, ex.toString());
                }

            }
        }, Constants.DataBaseHit.DEFAULT_TIME);

    }


    private void checkInternetAndShowToast(Boolean isShowInternetMessage) {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
//                isTokenExpire(isForSection);
            } else {
                if (isShowInternetMessage)
                    if (isAdded() && getActivity() != null)
                        AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }
    }

    private void isTokenExpire() {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                CommonListTable commonListTable = (CommonListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CommonListTable.TABLE_NAME);
                String dbEntryCreationTime = "";
                dbEntryCreationTime = commonListTable.getCommonListItemCreationTime(categoryInfo.id);
                if (dbEntryCreationTime != null && !dbEntryCreationTime.equalsIgnoreCase("")) {
                    long dbEntryCreationTimemilliSeconds = Long.parseLong(dbEntryCreationTime);
                    long currentTime = System.currentTimeMillis();
                    if ((currentTime - dbEntryCreationTimemilliSeconds) <= application.getServerTimeoutValue()) {
                        //token not Expire
                        hideProgress();
                        setOfflineCommonData();
                    } else {
                        //token Expire
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideProgress();
                                firstApiHit();
                            }
                        }, 500L);
                    }
                } else {
                    hideProgress();
                    firstApiHit();
                }
                AppLogs.printDebugLogs(TAG + "valueFromServer :", "" + application.getServerTimeoutValue());
                AppLogs.printDebugLogs(TAG + "dbEntryCreationTime :", dbEntryCreationTime);
            } else {
                hideProgress();
                setOfflineCommonData();
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }
    }

    private void firstApiHit() {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                AppLogs.printDebugLogs(TAG + "firstApiHit :", "firstApiHit");
                showProgressBar();
                swipeRefreshLayout();
            } else {
                hideProgress();
                if (isAdded() && getActivity() != null)
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            }

        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }

    }


    private void removeLoadingFromBottom() {
        if (commonListAdapter != null)
            commonListAdapter.removeItem();
    }


    private void parseJsonObjectFeedList(NewsResponse response) {
        boolean isReferesh = false;

        if (response.getFlickerActive() != null)
            isFlickerActive = response.getFlickerActive().equalsIgnoreCase("1");

        JSONArray webBannerArray = response.getWebBanner();

        if (webBannerInfoList == null) {
            webBannerInfoList = new ArrayList<>();
            if (webBannerArray != null)
                webBannerInfoList.addAll(Arrays.asList(new Gson().fromJson(webBannerArray.toString(), WebBannerInfo[].class)));
        }

        if (newsInfoList == null) {
            newsInfoList = new ArrayList<>();
        }

        if (response.getFeed() != null && response.getFeed().size() > 0) {
            if (LOAD_MORE_COUNT == 0) {
                newsInfoList.clear();
            } else {
                removeLoadingFromBottom();
                visibleThreshold += response.getFeed().size();
            }

            for (NewsListInfo newsInfo : response.getFeed()) {
                listIndex += 1;
                newsInfo.listIndex = listIndex;
            }

            // Inserting NewsList in mdb
            if (LOAD_MORE_COUNT == 0) {
                final CommonListTable commonListTable = (CommonListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CommonListTable.TABLE_NAME);
                commonListTable.deleteParticularNewsListCategory(categoryInfo.id, getContext());
                commonListTable.addCommonListItem(categoryInfo.id, newsInfoList, getContext());
                if (!categoryInfo.feedUrl.endsWith("1/"))
                    LOAD_MORE_COUNT++;
                isReferesh = true;
            }

            if (LOAD_MORE_COUNT < 5) {
                AdPlacementController.getInstance().addNewsListingNativeAds(response.getFeed(), getContext());
            }
            newsInfoList.addAll(response.getFeed());
            addBanner();

            addStockPhotoFlickerWidget();

            addWebBanner();

            if (isReferesh) {
                commonListAdapter.setData(newsInfoList, categoryInfo.detailUrl);
            } else {
                commonListAdapter.setLoadMoreData(response.getFeed());
            }

            commonListAdapter.notifyDataSetChanged();
        } else {
            if (LOAD_MORE_COUNT > 0) {
                LOAD_MORE_COUNT--;
                removeLoadingFromBottom();
            }
        }

        hideProgress();
        isLoading = false;
        // Tracking
        if (categoryInfo != null && response.getFeed().size() > 0) {
            trackingList.add(LOAD_MORE_COUNT);
            sendTracking();
        }
    }

    private void parseJsonObjectFeedList(JSONObject response) throws JSONException {
        boolean isReferesh = false;
        List<NewsListInfo> newsListInfos;
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);

        isFlickerActive = response.optString("flicker_active").equalsIgnoreCase("1");
        JSONArray webBannerArray = response.optJSONArray("web_banner");

        if (webBannerInfoList == null) {
            webBannerInfoList = new ArrayList<>();
            if (webBannerArray != null)
                webBannerInfoList.addAll(Arrays.asList(new Gson().fromJson(webBannerArray.toString(), WebBannerInfo[].class)));
        }

        if (newsInfoList == null) {
            newsInfoList = new ArrayList<>();
        }

        if (resultArray.length() > 0) {

            if (LOAD_MORE_COUNT == 0) {
                newsInfoList.clear();
            } else {
                removeLoadingFromBottom();
                visibleThreshold += resultArray.length();
            }

            newsListInfos = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), NewsListInfo[].class)));
            for (NewsListInfo newsInfo : newsListInfos) {
                listIndex += 1;
                newsInfo.listIndex = listIndex;
            }

            // Inserting NewsList in mdb
            if (LOAD_MORE_COUNT == 0) {
                final CommonListTable commonListTable = (CommonListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CommonListTable.TABLE_NAME);
                commonListTable.deleteParticularNewsListCategory(categoryInfo.id, getContext());
                commonListTable.addCommonListItem(categoryInfo.id, newsInfoList, getContext());
                if (!categoryInfo.feedUrl.endsWith("1/"))
                    LOAD_MORE_COUNT++;
                isReferesh = true;
            }

            if (LOAD_MORE_COUNT < 5) {
                newsListInfos = AdPlacementController.getInstance().addNewsListingNativeAds(newsListInfos, getContext());
            }
            newsInfoList.addAll(newsListInfos);
            addBanner();

            addStockPhotoFlickerWidget();

            addWebBanner();

            if (isReferesh) {
                commonListAdapter.setData(newsInfoList, categoryInfo.detailUrl);
            } else {
                commonListAdapter.setLoadMoreData(newsListInfos);
            }

            commonListAdapter.notifyDataSetChanged();
        } else {
            if (LOAD_MORE_COUNT > 0) {
                LOAD_MORE_COUNT--;
                removeLoadingFromBottom();
            }
        }

        hideProgress();
        isLoading = false;
        // Tracking
        if (categoryInfo != null && resultArray.length() > 0) {
            trackingList.add(LOAD_MORE_COUNT);
            sendTracking();
        }
    }


    private void addStockPhotoFlickerWidget() {
        if (isStockActive) {
            int posHome;
            String addedOn;
            addedOn = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.StockPrefs.STOCK_ADDED_ON_MENU, "");
            posHome = AppPreferences.getInstance(getContext()).getIntValue(QuickPreferences.StockPrefs.STOCK_MENU_POS, 0);
            JSONArray jsonElements;
            try {
                jsonElements = new JSONArray(addedOn);
                List<String> stringList = new ArrayList<>();
                for (int i = 0; i < jsonElements.length(); i++) {
                    stringList.add(jsonElements.getString(i));
                }
                if (stringList.contains(categoryInfo.id) && !isStockAdded) {
                    NewsListInfo listInfo = new NewsListInfo();
                    listInfo.isStockWidget = true;
                    isStockAdded = true;
                    newsInfoList.add(posHome, listInfo);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (isFlickerActive) {
            int posHome;
            String addedOn;
            posHome = AppPreferences.getInstance(getContext()).getIntValue(QuickPreferences.FlickerPrefs.FLICKER_MENU_POS, 0);
            addedOn = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_ADDED_ON_MENU, "");

            JSONArray jsonElements;
            try {
                jsonElements = new JSONArray(addedOn);
                List<String> stringList = new ArrayList<>();
                for (int i = 0; i < jsonElements.length(); i++) {
                    stringList.add(jsonElements.getString(i));
                }
                if (stringList.contains(categoryInfo.id) && !isFlickerAdded) {
                    NewsListInfo listInfo = new NewsListInfo();
                    listInfo.isFlicker = true;
                    isFlickerAdded = true;
                    newsInfoList.add(posHome, listInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public void addBanner() {
        for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
            int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catPos);
            if (!bannerInfoList.get(bannerIndex).isAdded && bannerPos > -1 && newsInfoList.size() >= bannerPos) {
                newsInfoList.add(bannerPos, new NewsListInfo(CommonListAdapter.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)));
                bannerInfoList.get(bannerIndex).isAdded = true;
            }
        }
    }

    public void addWebBanner() {
        for (int bannerIndex = 0; bannerIndex < webBannerInfoList.size(); bannerIndex++) {
            int bannerPos = Integer.parseInt(webBannerInfoList.get(bannerIndex).position);
            if (!webBannerInfoList.get(bannerIndex).isAdded && bannerPos > -1 && newsInfoList.size() >= bannerPos) {
                newsInfoList.add(bannerPos, new NewsListInfo(CommonListAdapter.VIEW_TYPE_WEB_BANNER, webBannerInfoList.get(bannerIndex)));
                webBannerInfoList.get(bannerIndex).isAdded = true;
            }
        }
    }

    @Override
    public void onDataFetch(boolean flag) {
    }

    @Override
    public void onDataFetch(boolean flag, String sectionName) {
    }

    @Override
    public void onDataFetch(boolean flag, int viewType) {

    }

    @Override
    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {

    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Subscribe
    public void NetworkConnectionStateChange(Boolean isConnected) {
        AppLogs.printDebugLogs(TAG + "newsList getMessage1", "" + isConnected);
        if (isConnected && isVisible()) {
            AppLogs.printDebugLogs(TAG + "ConnectionStateChange", "" + "" + isConnected);
            showProgressBar();
            swipeRefreshLayout();
//        } else {
            //Internet Not Available
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();
        isShowing = false;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (commonListAdapter != null) {
            commonListAdapter.setNotify();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
        newsInfoList = null;
        commonListAdapter = null;
        finalFeedURL = null;
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }

        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onPauseFragment() {
       /* if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.GA_SCROLL_TEST_ENABLE, false)) {
            int j = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            NewsListInfo itemAtPosition = commonListAdapter.getItemAtPosition(j);
            String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

            Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_TEST, AppFlyerConst.GAAction.LISTING, String.valueOf(itemAtPosition.listIndex), campaign);
        }*/
    }

    @Override
    public void onResumeFragment() {
        isShowing = true;
        sendTracking();
    }
}
