package com.bhaskar.view.ui.news;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bhaskar.data.DataRepository;
import com.bhaskar.data.model.NewsResponse;

public class NewsListFragmentViewModel extends ViewModel {
    private MutableLiveData<NewsResponse> mutableLiveData;
    private DataRepository dataRepository;

    public void init() {
        if (mutableLiveData != null) {
            return;
        }
        dataRepository = DataRepository.getInstance();
        mutableLiveData = new MutableLiveData<>();
    }

    void getNewsList(String url) {
        mutableLiveData = dataRepository.getNewsList(url,mutableLiveData);
    }

    LiveData<NewsResponse> getRepository() {
        return mutableLiveData;
    }

}
