package com.bhaskar.view.ui.photoGallery;


import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhaskar.R;
import com.bhaskar.util.TouchImageView;
import com.db.data.models.NewsPhotoInfo;
import com.db.listeners.OnDBVideoClickListener;
import com.bhaskar.util.sensor.CustomViewPager;
import com.bhaskar.util.sensor.GyroscopeObserver;
import com.bhaskar.util.sensor.PanoramaImageView;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.AppUtils;

public class PhotoArticleDetailFragment extends Fragment {
    private String TAG = "PhotoArticleDetailFragment";
    private int position, size;
    private ImageView swipeLeftImageView, swipeRightImageView;
    private OnDBVideoClickListener onDBVideoClickListener;
    private SensorManager mSensorManager;
    private GyroscopeObserver gyroscopeObserver;
    private TextView slugIntro, title, photo_count;
    private LinearLayout adViewRealtivelayout;
    private CustomViewPager viewpager;
    private NewsPhotoInfo newsPhotoInfo = null;

    public static PhotoArticleDetailFragment getInstance(int position, int size, NewsPhotoInfo newsPhotoInfo) {
        PhotoArticleDetailFragment photoArticleDetailFragment = new PhotoArticleDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_PHOTO_DETAIL, newsPhotoInfo);
        bundle.putInt("pos", position);
        bundle.putInt("length", size);
        photoArticleDetailFragment.setArguments(bundle);
        return photoArticleDetailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onDBVideoClickListener = ((OnDBVideoClickListener) getActivity());
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_gallery_detail, null);
        final ViewGroup transitionsContainer = getActivity().findViewById(R.id.transitions_container);
        adViewRealtivelayout = getActivity().findViewById(R.id.ads_view);
        viewpager = getActivity().findViewById(R.id.viewpager);
        slugIntro = rootView.findViewById(R.id.slug_intro);
        title = rootView.findViewById(R.id.title);
        photo_count = getActivity().findViewById(R.id.photo_count);

        Bundle bundle = getArguments();
        if (bundle != null) {
            newsPhotoInfo = (NewsPhotoInfo) bundle.getSerializable(Constants.KeyPair.KEY_PHOTO_DETAIL);
            position = bundle.getInt("pos");
            size = bundle.getInt("length");
        }
        gyroscopeObserver = new GyroscopeObserver();

        if (newsPhotoInfo == null)
            newsPhotoInfo = new NewsPhotoInfo();


        if (!TextUtils.isEmpty(newsPhotoInfo.title)) {
            title.setText(newsPhotoInfo.title);
            title.setVisibility(View.VISIBLE);
        } else {
            title.setVisibility(View.GONE);
        }
       /* if (!TextUtils.isEmpty(newsPhotoInfo.story)) {
            slugIntro.setVisibility(View.VISIBLE);
            slugIntro.setText(newsPhotoInfo.story);
        } else {
            slugIntro.setVisibility(View.GONE);
        }*/
        AppUtils.makeTextViewResizable(title, 3, "More", true);
        swipeLeftImageView = rootView.findViewById(R.id.left_swipe_arrow);
        swipeRightImageView = rootView.findViewById(R.id.right_swipe_arrow);
        setArrowSwipe();

        swipeLeftImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDBVideoClickListener.onDbVideoClick(position - 1);
            }
        });
        swipeRightImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDBVideoClickListener.onDbVideoClick(position + 1);
            }
        });
        final String imagePath = newsPhotoInfo.image;
        if (mSensorManager == null) {
            mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        }
        final PanoramaImageView panoramaImageView = rootView.findViewById(R.id.iv_photo_gallery_image);
        final TouchImageView touchImageView = rootView.findViewById(R.id.iv_photo_touch_image);

//        if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            panoramaImageView.setVisibility(View.VISIBLE);
//            touchImageView.setVisibility(View.GONE);
//            panoramaImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//            ImageUtil.setImage(getContext(), imagePath, panoramaImageView, R.drawable.water_mark_news_detail,R.drawable.water_mark_big_news_detail_error);
//        } else {
            panoramaImageView.setVisibility(View.GONE);
            touchImageView.setVisibility(View.VISIBLE);
            ImageUtil.setImage(getContext(), imagePath, touchImageView, R.drawable.water_mark_news_detail,R.drawable.water_mark_big_news_detail_error);
//        }

        /*if (!TextUtils.isEmpty(imagePath)) {


        } else {
            touchImageView.setImageResource(R.drawable.water_mark_news_detail);
        }
*/

        panoramaImageView.setGyroscopeObserver(gyroscopeObserver);

        panoramaImageView.setOnTouchListener(new View.OnTouchListener() {

            boolean mExpanded;
            private GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    mExpanded = !mExpanded;

                    return false;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        TransitionManager.beginDelayedTransition(transitionsContainer, new TransitionSet()
                                .addTransition(new ChangeBounds())
                                .addTransition(new ChangeImageTransform()));
                    }


                    if (!mExpanded) {
                        panoramaImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        viewpager.setPagingEnabled(true);
                        adViewRealtivelayout.setVisibility(View.VISIBLE);
                        photo_count.setVisibility(View.VISIBLE);
                        setArrowSwipe();
                        ViewGroup.LayoutParams params = panoramaImageView.getLayoutParams();
                        params.height = 600;
                        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                        if (!TextUtils.isEmpty(newsPhotoInfo.title)) {
                            title.setVisibility(View.VISIBLE);
                        } else {
                            title.setVisibility(View.GONE);
                        }
//                        if (!TextUtils.isEmpty(newsPhotoInfo.story)) {
//                            slugIntro.setVisibility(View.VISIBLE);
//                        } else {
//                            slugIntro.setVisibility(View.GONE);
//                        }
                        panoramaImageView.setLayoutParams(params);
                        gyroscopeObserver.unregister();
                    } else {
                        panoramaImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                        adViewRealtivelayout.setVisibility(View.GONE);
                        swipeLeftImageView.setVisibility(View.GONE);
                        swipeRightImageView.setVisibility(View.GONE);
                        slugIntro.setVisibility(View.GONE);
                        photo_count.setVisibility(View.GONE);
                        viewpager.setPagingEnabled(false);

                        ViewGroup.LayoutParams params = panoramaImageView.getLayoutParams();
                        params.height = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.MATCH_PARENT;
                        params.width = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.MATCH_PARENT;
                        panoramaImageView.setLayoutParams(params);
                        gyroscopeObserver.register(getActivity());

                    }
                    return false;
                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });
        touchImageView.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
            boolean mExpanded;

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {

                return false;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                mExpanded = !mExpanded;
                return false;
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                if (!mExpanded) {
                    viewpager.setPagingEnabled(true);

                    adViewRealtivelayout.setVisibility(View.VISIBLE);
                    setArrowSwipe();
                    adViewRealtivelayout.setEnabled(false);
                    if (!TextUtils.isEmpty(newsPhotoInfo.title)) {
                        title.setVisibility(View.VISIBLE);
                    } else {
                        title.setVisibility(View.GONE);
                    }
                   /* if (!TextUtils.isEmpty(newsPhotoInfo.story)) {
                        slugIntro.setVisibility(View.VISIBLE);
                    } else {
                        slugIntro.setVisibility(View.GONE);
                    }*/
                    photo_count.setVisibility(View.VISIBLE);
                    viewpager.setPagingEnabled(true);


                } else {
                    adViewRealtivelayout.setVisibility(View.GONE);
                    swipeLeftImageView.setVisibility(View.GONE);
                    swipeRightImageView.setVisibility(View.GONE);
                    title.setVisibility(View.GONE);
                    slugIntro.setVisibility(View.GONE);
                    photo_count.setVisibility(View.GONE);
                    viewpager.setPagingEnabled(false);

                }
                return false;
            }
        });
        return rootView;
    }


    private void setArrowSwipe() {
//        if (position == 0 && size == 1) {
//            swipeLeftImageView.setVisibility(View.GONE);
//            swipeRightImageView.setVisibility(View.GONE);
//        } else if (position == 0 && size - 1 > position) {
//            swipeRightImageView.setVisibility(View.VISIBLE);
//        } else if (position > 0 && size - 1 > position) {
//            swipeLeftImageView.setVisibility(View.VISIBLE);
//            swipeRightImageView.setVisibility(View.VISIBLE);
//        } else if (position > 0 && position == size - 1) {
//            swipeLeftImageView.setVisibility(View.VISIBLE);
//        }
    }

}
