package com.bhaskar.view.ui.photoGallery;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;

public class PhotoGallaryViewHolder extends RecyclerView.ViewHolder {
    ImageView ivPhotoGalleryImage;
    TextView tvPhotoGalleryTitle;
    public ImageView moreIV;

    PhotoGallaryViewHolder(View itemView) {
        super(itemView);
        ivPhotoGalleryImage = itemView.findViewById(R.id.iv_photo_gallery_image);
        tvPhotoGalleryTitle = itemView.findViewById(R.id.tv_photo_gallery_title);
        moreIV = itemView.findViewById(R.id.more_img);
        itemView.setTag(itemView);


    }
}
