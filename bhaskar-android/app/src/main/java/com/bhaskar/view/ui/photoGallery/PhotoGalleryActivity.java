package com.bhaskar.view.ui.photoGallery;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.AppUrls;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.util.LoginController;
import com.bhaskar.util.sensor.CustomViewPager;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.db.data.models.NewsDetailPhotoInfo;
import com.db.data.models.NewsPhotoInfo;
import com.db.data.models.PhotoDetailInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.divya_new.articlePage.ArticleCommonInfo;
import com.db.listeners.OnDBVideoClickListener;
import com.db.main.BaseAppCompatActivity;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class PhotoGalleryActivity extends BaseAppCompatActivity implements OnDBVideoClickListener {
    private CustomViewPager photoGalleryViewPager;
    private ArrayList<PhotoDetailInfo> galleryPhotoGalleryArrayList = new ArrayList<>();
    private ArrayList<NewsPhotoInfo> detailPhotoGalleryArrayList = new ArrayList<>();
    private PhotoPagerAdapter photoPagerAdapter;
    private short galleryAction = -1;
    private TextView photoCountTextView;
    private int slidePosition = 0;
    private Tracker tracker;
    private String gaArticle;
    private String gTrackUrl;
    private String gLinkUrl;
    private String trackUrl;
    private String title, imagePath, detailText;
    private String wisdomDomain, wisdomSubDomain;
    private ImageView shareButton;
    InitApplication initApplication;
    private ProgressBar progressBar;
    private String selectedImageUrl;
    private boolean isViaNotification;
    private PhotoGalleryActivityViewModel photoGalleryActivityViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initApplication = (InitApplication) getApplication();
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_photo_gallery);

        photoGalleryActivityViewModel = ViewModelProviders.of(this).get(PhotoGalleryActivityViewModel.class);
        photoGalleryActivityViewModel.init();
        photoGalleryActivityViewModel.getDetailRepo().observe(this, response -> {
            if (response != null) {
                progressBar.setVisibility(View.GONE);
                try {
                    parseJsonObjectFeedList(response);
                } catch (Exception e) {
                }

                registerPageListener();
            }
        });
        photoGalleryActivityViewModel.getListRepo().observe(this, response -> {
            if (response != null) {
                progressBar.setVisibility(View.GONE);
                galleryPhotoGalleryArrayList.addAll(response);
                afterParse();
            }
        });

        tracker = ((InitApplication) getApplication()).getDefaultTracker();

        ImageView closeButton = findViewById(R.id.photo_gallery_close_button);
        shareButton = findViewById(R.id.share_iv);
        progressBar = findViewById(R.id.progress_bar);
        closeButton.setOnClickListener(view -> finish());
        DrawableCompat.setTint(closeButton.getDrawable(), ContextCompat.getColor(this, R.color.white));

        photoCountTextView = findViewById(R.id.photo_count);
        photoGalleryViewPager = findViewById(R.id.viewpager);
//        photoGalleryViewPager.setPageTransformer(false, new TabletTransformer());
        photoGalleryViewPager.setPageTransformer(false, AppUtils.getParallaxPagerTransform(R.id.iv_photo_touch_image, 0, 0.5f));

        Intent bundle = getIntent();
        if (bundle != null) {

            /* Basic bundle extras*/
            isViaNotification = bundle.getBooleanExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, false);
            gaArticle = bundle.getStringExtra(Constants.KeyPair.KEY_GA_ARTICLE);
            gTrackUrl = bundle.getStringExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL);
            gLinkUrl = bundle.getStringExtra(Constants.KeyPair.KEY_LINK);
            trackUrl = bundle.getStringExtra(Constants.KeyPair.KEY_GA_TRACK_URL);
            galleryAction = bundle.getShortExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_MOVIE_REVIEW_PREVIEW);
            wisdomDomain = bundle.getStringExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN);
            wisdomSubDomain = bundle.getStringExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN);
            switch (galleryAction) {
                case Constants.PhotoGalleryActions.GALLERY_FOR_ORGINIAL_PHOTO_GALLERY:
                    galleryPhotoGalleryArrayList = new ArrayList<>();
                    PhotoListItemInfo photoListItemInfo = (PhotoListItemInfo) bundle.getSerializableExtra(Constants.KeyPair.KEY_LIST_INFO);
                    galleryPhotoGalleryArrayList = photoListItemInfo.photos;
                    slidePosition = bundle.getIntExtra("position", 0);
                    afterParse();
                    break;
                case Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS:
                    galleryPhotoGalleryArrayList = new ArrayList<>();
                    String storyId = bundle.getStringExtra(Constants.KeyPair.KEY_ID);
                    slidePosition = bundle.getIntExtra("position", 0);
                    makeJsonObjectRequest(storyId);
                    break;

                case Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY:
                    galleryPhotoGalleryArrayList = new ArrayList<>();
                    String detailUrl = bundle.getStringExtra(Constants.KeyPair.KEY_DETAIL_URL);
                    storyId = bundle.getStringExtra(Constants.KeyPair.KEY_ID);
                    slidePosition = bundle.getIntExtra("position", 0);
                    makeJsonArrayRequest(storyId, detailUrl);
                    break;

                case Constants.PhotoGalleryActions.GALLERY_FOR_DETAIL:
                    detailPhotoGalleryArrayList = new ArrayList<>();
                    NewsDetailPhotoInfo newsDetailInfo = (NewsDetailPhotoInfo) bundle.getSerializableExtra(Constants.KeyPair.KEY_NEWS_DETAIL);
                    slidePosition = bundle.getIntExtra(Constants.KeyPair.KEY_POSITION, 0);
                    if (detailPhotoGalleryArrayList == null) {
                        detailPhotoGalleryArrayList = new ArrayList<>();
                    }
                    detailPhotoGalleryArrayList = newsDetailInfo.photos;
                    if (detailPhotoGalleryArrayList != null && detailPhotoGalleryArrayList.size() > 0) {
                        shareButton(detailPhotoGalleryArrayList.get(0).isShare);
                    }
                    photoCountTextView.setText(getString(R.string.photo_gallery_image_count, slidePosition + 1, detailPhotoGalleryArrayList.size()));
                    photoPagerAdapter = new PhotoPagerAdapter(getSupportFragmentManager());
                    photoGalleryViewPager.setAdapter(photoPagerAdapter);
                    photoPagerAdapter.notifyDataSetChanged();
                    photoGalleryViewPager.setCurrentItem(slidePosition);

                    registerPageListener();
                    break;

                case Constants.PhotoGalleryActions.GALLERY_FOR_MOVIE_REVIEW_PREVIEW:
                    title = bundle.getStringExtra("title");
                    imagePath = bundle.getStringExtra("image");
                    detailText = bundle.getStringExtra("detail");
                    photoCountTextView.setText("");
                    photoPagerAdapter = new PhotoPagerAdapter(getSupportFragmentManager());
                    photoGalleryViewPager.setAdapter(photoPagerAdapter);
                    photoPagerAdapter.notifyDataSetChanged();
                    photoCountTextView.setVisibility(View.GONE);
                    registerPageListener();
                    break;
            }
        }

        shareButton.setOnClickListener(view -> {
            ImageUtil.shareImageAfterDownload(PhotoGalleryActivity.this, selectedImageUrl, getResources().getString(R.string.app_name));
            String campaign = AppPreferences.getInstance(PhotoGalleryActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            Tracking.trackGAEvent(PhotoGalleryActivity.this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.SHARE, gLinkUrl, campaign);
        });

        if (AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.AdPref.PHOTO_BANNER_TOGGLE, false)) {
            AdController.showPhotoGalleryBTF(this, findViewById(R.id.ads_view));
        }
    }

    @Override
    public void finish() {
        if (isViaNotification) {
            Intent intent = new Intent(PhotoGalleryActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        super.finish();
    }


    final ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            try {
                String gtu = "";
                String tu = "";
                if (galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY
                        || galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_ORGINIAL_PHOTO_GALLERY) {
                    if (galleryPhotoGalleryArrayList != null && galleryPhotoGalleryArrayList.size() > 0) {
                        gtu = (TextUtils.isEmpty(galleryPhotoGalleryArrayList.get(position).gTrackUrl) ? gTrackUrl : galleryPhotoGalleryArrayList.get(position).gTrackUrl);
                        tu = (TextUtils.isEmpty(galleryPhotoGalleryArrayList.get(position).trackUrl) ? trackUrl : galleryPhotoGalleryArrayList.get(position).trackUrl);
                        // tu = galleryPhotoGalleryArrayList.get(position).trackUrl;
                    }
                } else {
                    gtu = gTrackUrl;
                    tu = trackUrl;
                }

                // Tracking
                String value = gaArticle + "-" + gtu + "?slide=" + (position + 1);
                String source = AppPreferences.getInstance(PhotoGalleryActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
                String medium = AppPreferences.getInstance(PhotoGalleryActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                String campaign = AppPreferences.getInstance(PhotoGalleryActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAScreen(PhotoGalleryActivity.this, tracker, value, source, medium, campaign);

                if (!TextUtils.isEmpty(tu)) {
                    String domain = AppPreferences.getInstance(PhotoGalleryActivity.this).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
                    String url = (TextUtils.isEmpty(wisdomDomain) ? domain : wisdomDomain) + (TextUtils.isEmpty(wisdomSubDomain) ? AppUtils.GLOBAL_DISPLAY_NAME : wisdomSubDomain);
                    ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
                    String email = "";
                    String mobile = "";
                    if (profileInfo != null) {
                        email = profileInfo.email;
                        mobile = profileInfo.mobile;
                    }
                    Tracking.trackWisdomArticlePage(PhotoGalleryActivity.this, tu, url, (position + 1), email, mobile, CommonConstants.BHASKAR_APP_ID, -1, -1, 0, "");
                }

                if (galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS) {
                    photoCountTextView.setText(getString(R.string.photo_gallery_image_count, (position + 1), detailPhotoGalleryArrayList.size()));
                    selectedImageUrl = detailPhotoGalleryArrayList.get(position).image;
                } else if (galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_DETAIL) {
                    photoCountTextView.setText(getString(R.string.photo_gallery_image_count, (position + 1), detailPhotoGalleryArrayList.size()));
                    selectedImageUrl = detailPhotoGalleryArrayList.get(position).image;
                } else if (galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY
                        || galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_ORGINIAL_PHOTO_GALLERY) {
                    photoCountTextView.setText(getString(R.string.photo_gallery_image_count, (position + 1), galleryPhotoGalleryArrayList.size()));
                    selectedImageUrl = galleryPhotoGalleryArrayList.get(position).rssImage;
                } else {
                    photoCountTextView.setText("");
                }
            } catch (Exception e) {
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    private void registerPageListener() {
        photoGalleryViewPager.addOnPageChangeListener(onPageChangeListener);
        photoGalleryViewPager.post(() -> onPageChangeListener.onPageSelected(photoGalleryViewPager.getCurrentItem()));
    }

    private void makeJsonObjectRequest(String storyId) {
        progressBar.setVisibility(View.VISIBLE);
        String finalUrl = Urls.APP_FEED_BASE_URL + String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID) + storyId + "/";
        Systr.println("PHOTO GALLLERY : " + finalUrl);
        photoGalleryActivityViewModel.getPhotoGallaryDetail(finalUrl);
/*        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalUrl), null,
                response -> {

                }, error -> VolleyLog.d("NewsListFragment", "Error: " + error.getMessage())) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(PhotoGalleryActivity.this).addToRequestQueue(jsonArrayRequest);*/
    }

    private void parseJsonObjectFeedList(JSONObject response) {

        ArticleCommonInfo photoDetailInfo = new Gson().fromJson(response.toString(), ArticleCommonInfo.class);

        detailPhotoGalleryArrayList = new ArrayList<>();
        if (detailPhotoGalleryArrayList == null) {
            detailPhotoGalleryArrayList = new ArrayList<>();
        }
        detailPhotoGalleryArrayList = (photoDetailInfo.photosGallery == null || photoDetailInfo.photosGallery.size() == 0) ? photoDetailInfo.photos : photoDetailInfo.photosGallery;

        gTrackUrl = photoDetailInfo.gTrackUrl;
        gLinkUrl = photoDetailInfo.link;
        trackUrl = photoDetailInfo.trackUrl;
        setData();
    }
    private void parseJsonObjectFeedList(ArticleCommonInfo photoDetailInfo) {
        detailPhotoGalleryArrayList = new ArrayList<>();
        detailPhotoGalleryArrayList = (photoDetailInfo.photosGallery == null || photoDetailInfo.photosGallery.size() == 0) ? photoDetailInfo.photos : photoDetailInfo.photosGallery;

        gTrackUrl = photoDetailInfo.gTrackUrl;
        gLinkUrl = photoDetailInfo.link;
        trackUrl = photoDetailInfo.trackUrl;
        setData();
    }

    private void setData() {
        if (detailPhotoGalleryArrayList != null && detailPhotoGalleryArrayList.size() > 0) {
            shareButton(detailPhotoGalleryArrayList.get(0).isShare);
        }

        photoCountTextView.setText(getString(R.string.photo_gallery_image_count, slidePosition + 1, detailPhotoGalleryArrayList.size()));
        photoPagerAdapter = new PhotoPagerAdapter(getSupportFragmentManager());
        photoGalleryViewPager.setAdapter(photoPagerAdapter);
        photoPagerAdapter.notifyDataSetChanged();
        photoGalleryViewPager.setCurrentItem(slidePosition);
    }

    private void makeJsonArrayRequest(String storyId, String detailUrl) {
        progressBar.setVisibility(View.VISIBLE);
        String finalUrl = Urls.APP_FEED_BASE_URL + detailUrl + storyId + "/";
        photoGalleryActivityViewModel.getPhotoGallaryList(finalUrl);
        /*JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalUrl), null,
                response -> {
                    progressBar.setVisibility(View.GONE);
                    try {
                        if (!TextUtils.isEmpty(response.toString())) {
                            parseJsonArrayFeedList(response);
                        }
                    } catch (Exception e) {
                    }

//                    registerPageListener();
                }, error -> {
            VolleyLog.d("NewsListFragment", "Error: " + error.getMessage());

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(PhotoGalleryActivity.this).addToRequestQueue(jsonArrayRequest);*/
    }

    private void parseJsonArrayFeedList(JSONArray response) {

        galleryPhotoGalleryArrayList.addAll(Arrays.asList(new Gson().fromJson(response.toString(), PhotoDetailInfo[].class)));

        afterParse();
    }

    private void afterParse() {
        if (galleryPhotoGalleryArrayList != null && galleryPhotoGalleryArrayList.size() > 0) {
            shareButton(galleryPhotoGalleryArrayList.get(0).isShare);

            photoCountTextView.setText(getString(R.string.photo_gallery_image_count, slidePosition + 1, galleryPhotoGalleryArrayList.size()));

            photoPagerAdapter = new PhotoPagerAdapter(getSupportFragmentManager());
            photoGalleryViewPager.setAdapter(photoPagerAdapter);
            photoPagerAdapter.notifyDataSetChanged();
            photoGalleryViewPager.setCurrentItem(slidePosition);
            registerPageListener();
        }
    }

    private void shareButton(boolean isShare) {
        if (isShare) {
            shareButton.setVisibility(View.VISIBLE);
        } else {
            shareButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDbVideoClick(int position) {
        photoGalleryViewPager.setCurrentItem(position);
    }

    private class PhotoPagerAdapter extends FragmentStatePagerAdapter {

        private PhotoPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment galleryFragment;
            if (galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_DETAIL
                    || galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS) {
                galleryFragment = PhotoArticleDetailFragment.getInstance(position, detailPhotoGalleryArrayList.size(), detailPhotoGalleryArrayList.get(position));
            } else if (galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY
                    || galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_ORGINIAL_PHOTO_GALLERY) {
                galleryFragment = PhotoGalleryDetailFragment.getInstance(position, galleryPhotoGalleryArrayList.size(), galleryPhotoGalleryArrayList.get(position));
            } else {
                galleryFragment = PhotoGallerySingleImageFragment.getInstance(title, imagePath, detailText);
            }
            return galleryFragment;
        }

        @Override
        public int getCount() {
            if (galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_DETAIL
                    || galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS) {
                return (detailPhotoGalleryArrayList != null) ? detailPhotoGalleryArrayList.size() : 0;
            } else if (galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY
                    || galleryAction == Constants.PhotoGalleryActions.GALLERY_FOR_ORGINIAL_PHOTO_GALLERY) {
                return (galleryPhotoGalleryArrayList != null) ? galleryPhotoGalleryArrayList.size() : 0;
            } else {
                return 1;
            }
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
}
