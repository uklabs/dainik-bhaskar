package com.bhaskar.view.ui.photoGallery

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bhaskar.data.DataRepository
import com.db.data.models.PhotoDetailInfo
import com.db.divya_new.articlePage.ArticleCommonInfo

class PhotoGalleryActivityViewModel : ViewModel() {
    private var mutableLiveDataList: MutableLiveData<List<PhotoDetailInfo>>? = null
    private var mutableLiveDataDetail: MutableLiveData<ArticleCommonInfo>? = null
    private var dataRepository: DataRepository? = null
    fun init() {
        if (mutableLiveDataDetail != null && mutableLiveDataList != null) {
            return
        }
        dataRepository = DataRepository.getInstance()
        mutableLiveDataDetail = MutableLiveData()
        mutableLiveDataList = MutableLiveData()
    }

    fun getPhotoGallaryList(url: String?) {
        mutableLiveDataList = dataRepository!!.getPhotoDetailGallaryList(url, mutableLiveDataList)
    }

    fun getPhotoGallaryDetail(url: String?) {
        mutableLiveDataDetail = dataRepository!!.getPhotoDetailGallaryDetail(url, mutableLiveDataDetail)
    }

    val listRepo: LiveData<List<PhotoDetailInfo>>?
        get() = mutableLiveDataList

    val detailRepo: LiveData<ArticleCommonInfo>?
        get() = mutableLiveDataDetail
}