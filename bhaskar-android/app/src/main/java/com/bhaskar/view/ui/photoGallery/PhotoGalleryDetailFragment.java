package com.bhaskar.view.ui.photoGallery;


import android.content.Context;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhaskar.R;
import com.bhaskar.util.TouchImageView;
import com.db.data.models.PhotoDetailInfo;
import com.db.listeners.OnDBVideoClickListener;
import com.bhaskar.util.sensor.CustomViewPager;
import com.bhaskar.util.sensor.GyroscopeObserver;
import com.bhaskar.util.sensor.PanoramaImageView;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;


public class PhotoGalleryDetailFragment extends Fragment {
    private String TAG = "PhotoGalleryDetailFragment";
    private int position;
    private ImageView swipeLeftImageView, swipeRightImageView;
    private OnDBVideoClickListener onDBVideoClickListener;
    private TextView slug_intro, title, photo_count;
    private LinearLayout adViewRealtivelayout;
    private GyroscopeObserver gyroscopeObserver;
    private SensorManager mSensorManager;
    private CustomViewPager viewpager;

    public static PhotoGalleryDetailFragment getInstance(int position, int size, PhotoDetailInfo photoDetailInfo) {
        PhotoGalleryDetailFragment photoGalleryDetailFragment = new PhotoGalleryDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_PHOTO_DETAIL, photoDetailInfo);
        bundle.putInt("pos", position);
        bundle.putInt("length", size);
        photoGalleryDetailFragment.setArguments(bundle);
        return photoGalleryDetailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onDBVideoClickListener = ((OnDBVideoClickListener) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_gallery_detail, null);
        final ViewGroup transitionsContainer = getActivity().findViewById(R.id.transitions_container);
        viewpager = getActivity().findViewById(R.id.viewpager);

        adViewRealtivelayout = getActivity().findViewById(R.id.ads_view);
        slug_intro = rootView.findViewById(R.id.slug_intro);
        title = rootView.findViewById(R.id.title);
        photo_count = getActivity().findViewById(R.id.photo_count);
        gyroscopeObserver = new GyroscopeObserver();
        PhotoDetailInfo photoDetailInfo = null;
        Bundle bundle = getArguments();

        if (bundle != null) {
            photoDetailInfo = (PhotoDetailInfo) bundle.getSerializable(Constants.KeyPair.KEY_PHOTO_DETAIL);
            position = bundle.getInt("pos");
            int size = bundle.getInt("length");
        }
        if (photoDetailInfo == null) {
            photoDetailInfo = new PhotoDetailInfo();
        }

        if (TextUtils.isEmpty(photoDetailInfo.rssTitle)) {
            title.setVisibility(View.GONE);
        } else {
            title.setVisibility(View.VISIBLE);
            title.setText(photoDetailInfo.rssTitle);
        }
        if (TextUtils.isEmpty(photoDetailInfo.rssDesc)) {
            rootView.findViewById(R.id.slug_intro).setVisibility(View.GONE);
        } else {
            ((TextView) rootView.findViewById(R.id.slug_intro)).setText(photoDetailInfo.rssDesc);
        }
        AppUtils.makeTextViewResizable(rootView.findViewById(R.id.slug_intro), 3, "More", true);
        swipeLeftImageView = rootView.findViewById(R.id.left_swipe_arrow);
        swipeRightImageView = rootView.findViewById(R.id.right_swipe_arrow);
        setArrowSwipe();

        swipeLeftImageView.setOnClickListener(view -> onDBVideoClickListener.onDbVideoClick(position - 1));
        swipeRightImageView.setOnClickListener(view -> onDBVideoClickListener.onDbVideoClick(position + 1));

        final String imagePath = photoDetailInfo.rssImage;

        if (mSensorManager == null) {
            mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        }
        final PanoramaImageView panoramaImageView = rootView.findViewById(R.id.iv_photo_gallery_image);
        final TouchImageView touchImageView = rootView.findViewById(R.id.iv_photo_touch_image);
        panoramaImageView.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(imagePath)) {

//            if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                panoramaImageView.setVisibility(View.VISIBLE);
//                touchImageView.setVisibility(View.GONE);
//                panoramaImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//
//                ImageUtil.setImage(getContext(), imagePath, panoramaImageView, R.drawable.water_mark_news_detail);
//            } else {
                panoramaImageView.setVisibility(View.GONE);
                touchImageView.setVisibility(View.VISIBLE);

                ImageUtil.setImage(getContext(), imagePath, touchImageView, R.drawable.water_mark_news_detail);
//            }

        } else {
            touchImageView.setVisibility(View.VISIBLE);
            touchImageView.setImageResource(R.drawable.water_mark_news_detail);
        }


        panoramaImageView.setGyroscopeObserver(gyroscopeObserver);
       /* panoramaImageView.setOnClickListener(new View.OnClickListener() {

            boolean mExpanded;

            @Override
            public void onClick(View v) {


                mExpanded = com.bhaskar.view.ui.photoGallery!mExpanded;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    TransitionManager.beginDelayedTransition(transitionsContainer, new TransitionSet()
                            .addTransition(new ChangeBounds())
                            .addTransition(new ChangeImageTransform()));
                }


                if (!mExpanded) {

                    viewpager.setPagingEnabled(true);
                    panoramaImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    adViewRealtivelayout.setVisibility(View.VISIBLE);
                    photo_count.setVisibility(View.VISIBLE);
                    setArrowSwipe();
                    ViewGroup.LayoutParams params = panoramaImageView.getLayoutParams();
                    params.height =ViewGroup.LayoutParams.MATCH_PARENT;
                    params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    slug_intro.setVisibility(View.VISIBLE);
                    panoramaImageView.setLayoutParams(params);
                    gyroscopeObserver.unregister();
                    //imageView.setScaleType(!mExpanded ? ImageView.ScaleType.CENTER_CROP : ImageView.ScaleType.FIT_CENTER);

                } else {
                    panoramaImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    adViewRealtivelayout.setVisibility(View.GONE);
                    swipeLeftImageView.setVisibility(View.GONE);
                    swipeRightImageView.setVisibility(View.GONE);
                    slug_intro.setVisibility(View.GONE);
                    photo_count.setVisibility(View.GONE);
                    viewpager.setPagingEnabled(false);

                    ViewGroup.LayoutParams params = panoramaImageView.getLayoutParams();
                    params.height = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.MATCH_PARENT;
                    params.width = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.MATCH_PARENT;
                    panoramaImageView.setLayoutParams(params);
                    gyroscopeObserver.register(getActivity());

                }
            }
        });*/
        panoramaImageView.setOnTouchListener(new View.OnTouchListener() {

            boolean mExpanded;
            private GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    mExpanded = !mExpanded;

                    return false;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        TransitionManager.beginDelayedTransition(transitionsContainer, new TransitionSet()
                                .addTransition(new ChangeBounds())
                                .addTransition(new ChangeImageTransform()));
                    }


                    if (!mExpanded) {
                        panoramaImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        viewpager.setPagingEnabled(true);
                        adViewRealtivelayout.setVisibility(View.VISIBLE);
                        photo_count.setVisibility(View.VISIBLE);
                        setArrowSwipe();
                        ViewGroup.LayoutParams params = panoramaImageView.getLayoutParams();
                        params.height = 600;
                        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                        slug_intro.setVisibility(View.VISIBLE);
                        panoramaImageView.setLayoutParams(params);
                        gyroscopeObserver.unregister();

                        //imageView.setScaleType(!mExpanded ? ImageView.ScaleType.CENTER_CROP : ImageView.ScaleType.FIT_CENTER);

                    } else {
                        panoramaImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                        adViewRealtivelayout.setVisibility(View.GONE);
                        swipeLeftImageView.setVisibility(View.GONE);
                        swipeRightImageView.setVisibility(View.GONE);
                        slug_intro.setVisibility(View.GONE);
                        photo_count.setVisibility(View.GONE);
                        viewpager.setPagingEnabled(false);

                        ViewGroup.LayoutParams params = panoramaImageView.getLayoutParams();
                        params.height = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.MATCH_PARENT;
                        params.width = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.MATCH_PARENT;
                        panoramaImageView.setLayoutParams(params);
                        gyroscopeObserver.register(getActivity());
                        //  panoramaImageView.setScaleType(mExpanded ? ImageView.ScaleType.CENTER_CROP : ImageView.ScaleType.FIT_CENTER);

                    }
                    return false;
                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });
        touchImageView.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
            boolean mExpanded;

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {

                return false;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                mExpanded = !mExpanded;
                return false;
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {


                if (!mExpanded) {
                    viewpager.setPagingEnabled(true);
                    adViewRealtivelayout.setVisibility(View.VISIBLE);
                    setArrowSwipe();
                    adViewRealtivelayout.setEnabled(false);
                    title.setVisibility(View.VISIBLE);
                    slug_intro.setVisibility(View.VISIBLE);
                    photo_count.setVisibility(View.VISIBLE);

                } else {
                    viewpager.setPagingEnabled(false);
                    adViewRealtivelayout.setVisibility(View.GONE);
                    swipeLeftImageView.setVisibility(View.GONE);
                    swipeRightImageView.setVisibility(View.GONE);
                    title.setVisibility(View.GONE);
                    slug_intro.setVisibility(View.GONE);
                    photo_count.setVisibility(View.GONE);
                }
                return false;
            }
        });
        return rootView;
    }


    private void setArrowSwipe() {
//        if (position == 0 && size == 1) {
//            swipeLeftImageView.setVisibility(View.GONE);
//            swipeRightImageView.setVisibility(View.GONE);
//        } else if (position == 0 && size - 1 > position) {
//            swipeRightImageView.setVisibility(View.VISIBLE);
//        } else if (position > 0 && size - 1 > position) {
//            swipeLeftImageView.setVisibility(View.VISIBLE);
//            swipeRightImageView.setVisibility(View.VISIBLE);
//        } else if (position > 0 && position == size - 1) {
//            swipeLeftImageView.setVisibility(View.VISIBLE);
//        }
    }

}
