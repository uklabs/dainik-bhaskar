package com.bhaskar.view.ui.photoGallery;


import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController2;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.InitApplication;
import com.db.ads.BannerAdPlacementController;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.PhotoGalleryListTable;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.dbvideo.player.BusProvider;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.listeners.FragmentLifecycle;
import com.db.listeners.OnDBVideoClickListener;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class PhotoGalleryFragment extends Fragment implements OnDBVideoClickListener, OnMoveTopListener, FragmentLifecycle {
    private final String TAG = AppConfig.BaseTag + "." + PhotoGalleryFragment.class.getSimpleName();
    public boolean isLoading;
    //HpBig
    //Atf Ads
    InitApplication application;
    private CategoryInfo categoryInfo;
    private PhotoGalleryListAdapter photoGalleryListAdapter;
    private int LOAD_MORE_COUNT = 0;
    private int visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
    private int lastVisibleItem, totalItemCount;
    private String finalFeedURL = "";
    private LinearLayoutManager linearLayoutManager;
    RecyclerView photoGalleryRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<PhotoListItemInfo> photoListItemInfoArrayList = null;
    private ProgressBar progressBar;
    private ArrayList<Integer> trackingList = new ArrayList<>();
    private boolean isShowing = false;
    private List<BannerInfo> bannerInfoList;
    private PhotoGalleryFragmentViewModel photoGalleryFragmentViewModel;

    public PhotoGalleryFragment() {
    }


    public static PhotoGalleryFragment getInstance(CategoryInfo categoryInfo) {
        PhotoGalleryFragment photoGalleryFragment = new PhotoGalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        photoGalleryFragment.setArguments(bundle);
        return photoGalleryFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        /*Banner*/
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);


        photoGalleryFragmentViewModel = ViewModelProviders.of(this).get(PhotoGalleryFragmentViewModel.class);
        photoGalleryFragmentViewModel.init();
        photoGalleryFragmentViewModel.getRepository().observe(this, newsResponse -> {
            if (newsResponse != null) {
                handleResponse(newsResponse.getFeed());
            } else {
                hideProgress();
                if (LOAD_MORE_COUNT > 0) {
                    LOAD_MORE_COUNT--;
                    removeLoadingFromBottom();
                }
                AppUtils.getInstance().showCustomToast(getContext(), getResources().getString(R.string.sorry_error_found_please_try_again_));
            }
        });
    }

    private void handleResponse(List<PhotoListItemInfo> feed) {
        try {
            if (feed != null) {
                if (!Constants.singleSessionCategoryIdList.contains(categoryInfo.id)) {
                    Constants.singleSessionCategoryIdList.add(categoryInfo.id);
                }
                parseFeedList(feed);
            }
        } catch (Exception e) {
            hideProgress();
        }
    }

    private void parseFeedList(List<PhotoListItemInfo> feed) {
        if (photoListItemInfoArrayList == null) {
            photoListItemInfoArrayList = new ArrayList<>();
        }
        isLoading = false;
        if (feed.size() > 0) {
            if (LOAD_MORE_COUNT > 0) {
                visibleThreshold += feed.size();

                removeLoadingFromBottom();
            } else {
                photoListItemInfoArrayList.clear();
            }

            if (LOAD_MORE_COUNT == 0) {
                final PhotoGalleryListTable photoGalleryListTable = (PhotoGalleryListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(PhotoGalleryListTable.TABLE_NAME);
                photoGalleryListTable.deleteParticularPhotoGalleryListCategory(categoryInfo.id);
                photoGalleryListTable.addPhotoGalleryListItem(categoryInfo.id, photoListItemInfoArrayList);
            }
            if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.BANNER_LIST_TOGGLE, false)) {
                BannerAdPlacementController.getInstance().addPhotoListingBannerAds(feed);
            }
            photoListItemInfoArrayList.addAll(feed);

            /*Inserting  PhotoGallery in mdb*/
            if (LOAD_MORE_COUNT == 0) {
                if (!categoryInfo.feedUrl.endsWith("1/"))
                    LOAD_MORE_COUNT++;
                addAdsAndWidgetInCommonList();
            }
            // Tracking
            if (categoryInfo != null) {
                trackingList.add(LOAD_MORE_COUNT);
                sendTracking();
            }
        } else {
            if (LOAD_MORE_COUNT > 0) {
                LOAD_MORE_COUNT--;
                removeLoadingFromBottom();
            }
        }

        photoGalleryListAdapter.setData(photoListItemInfoArrayList, categoryInfo);
        photoGalleryListAdapter.notifyDataSetChanged();
        hideProgress();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_gallery, container, false);
        progressBar = rootView.findViewById(R.id.progress_bar);
        application = (InitApplication) getActivity().getApplication();
        progressBar = rootView.findViewById(R.id.progress_bar);
        photoGalleryRecyclerView = rootView.findViewById(R.id.db_videos_recycler_view);
        linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        photoGalleryRecyclerView.setLayoutManager(linearLayoutManager);

        swipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));

        photoGalleryListAdapter = new PhotoGalleryListAdapter(getContext(), this);
        photoGalleryRecyclerView.setAdapter(photoGalleryListAdapter);
        photoGalleryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            LOAD_MORE_COUNT++;
                            onLoadMore();
                        }
                    }
                }
            }
        });
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!NetworkStatus.getInstance().isConnected(getActivity()) || Constants.singleSessionCategoryIdList.contains(categoryInfo.id)) {
                    isTokenExpire();
                } else {
                    swipeRefreshLayout();
                }
            }
        }, Constants.DataBaseHit.DEFAULT_TIME_IS_TOKEN);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    swipeRefreshLayout();
                } else {
                    hideProgress();
                    if (isAdded() && getActivity() != null)
                        AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
                }
            }
        });
        return rootView;
    }

    private Rect scrollBounds;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        scrollBounds = new Rect();
        photoGalleryRecyclerView.getHitRect(scrollBounds);
    }

    private void swipeRefreshLayout() {
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
        visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
        LOAD_MORE_COUNT = 0;
        setApiUrl();
        makeJsonObjectRequest();
    }

    private void swipeRefreshLayoutWithoutApiHit() {
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
        visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
        LOAD_MORE_COUNT = 0;
        setApiUrl();
        if (!categoryInfo.feedUrl.endsWith("1/"))
            LOAD_MORE_COUNT++;

    }

    private void setApiUrl() {
        if (categoryInfo.feedUrl.endsWith("1/")) {
            finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG1/";
        } else {
            finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + (LOAD_MORE_COUNT + 1) + "/";
        }
    }

    private void showProgressBar() {
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        progressBar.setVisibility(View.VISIBLE);
    }

//    public void setFragmentShow() {
//        isShowing = true;
//        sendTracking();
//    }

    private void sendTracking() {
        // Tracking
        if (isShowing) {
            if (categoryInfo != null && trackingList != null) {
                while (trackingList.size() > 0) {
                    int value = trackingList.get(0);
                    trackingList.remove(0);
                    if (value > 1) {
                        value = value - 1;
                        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_DEPTH, categoryInfo.displayName, AppFlyerConst.GALabel.PG + value, campaign);
                        Systr.println("GA Events : " + AppFlyerConst.GACategory.SCROLL_DEPTH + categoryInfo.displayName + AppFlyerConst.GALabel.PG + value + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));
                    }
                }
            }
        }
    }

    private void onLoadMore() {
        isLoading = true;
//        photoGalleryRecyclerView.post(() -> photoGalleryListAdapter.addItem());

        finalFeedURL = AppUtils.getInstance().makeUrlForLoadMore(Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl, LOAD_MORE_COUNT);
        makeJsonObjectRequest();
    }

    private void isTokenExpire() {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                showProgressBar();
                PhotoGalleryListTable photoGalleryListTable = (PhotoGalleryListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(PhotoGalleryListTable.TABLE_NAME);

                String dbEntryCreationTime = photoGalleryListTable.getPhotoGallaryListItemCreationTime(categoryInfo.id);

                if (dbEntryCreationTime != null && !dbEntryCreationTime.equalsIgnoreCase("")) {
                    long dbEntryCreationTimemilliSeconds = Long.parseLong(dbEntryCreationTime);
                    long currentTime = System.currentTimeMillis();

                    if ((currentTime - dbEntryCreationTimemilliSeconds) <= application.getServerTimeoutValue()) {
                        // token not Expire
                        hideProgress();
                        setOfflinePhotoGalleryData();
                    } else {
                        // token Expire
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideProgress();
                                firstApiHit();
                            }
                        }, 500L);
                    }

                } else {
                    hideProgress();
                    firstApiHit();
                }
                AppLogs.printDebugLogs(TAG + "valueFromServer :", "" + application.getServerTimeoutValue());
                AppLogs.printDebugLogs(TAG + "dbEntryCreationTime :", dbEntryCreationTime);


            } else {
                hideProgress();
                setOfflinePhotoGalleryData();
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }

    }

    private void firstApiHit() {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                AppLogs.printDebugLogs(TAG + "firstApiHit :", "firstApiHit");
                showProgressBar();
                swipeRefreshLayout();
            } else {
                hideProgress();
                if (isAdded() && getActivity() != null)
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }

    }


    private void setOfflinePhotoGalleryData() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    showProgressBar();
                    boolean isShowInternetMessage = true;
                    if (photoListItemInfoArrayList == null) {
                        photoListItemInfoArrayList = new ArrayList<>();
                    } else {
                        photoListItemInfoArrayList.clear();
                    }

                    PhotoGalleryListTable photoGalleryListTable = (PhotoGalleryListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(PhotoGalleryListTable.TABLE_NAME);
                    photoListItemInfoArrayList.addAll(photoGalleryListTable.getAllPhotoGalleryInfoListAccordingToCategory(categoryInfo.id));

                    if (photoListItemInfoArrayList != null && photoListItemInfoArrayList.size() > 0) {
                        if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.BANNER_LIST_TOGGLE, false)) {
                            photoListItemInfoArrayList = BannerAdPlacementController.getInstance().addPhotoListingBannerAds(photoListItemInfoArrayList);
                        }
                        isShowInternetMessage = false;
                        addAdsAndWidgetInCommonList();
                    }
                    photoGalleryListAdapter.setData(photoListItemInfoArrayList, categoryInfo);
                    photoGalleryListAdapter.notifyDataSetChanged();
                    hideProgress();
                    swipeRefreshLayoutWithoutApiHit();
                    checkInternetAndShowToast(isShowInternetMessage);
                } catch (Exception ex) {
                    AppLogs.printErrorLogs(TAG, ex.toString());
                }

            }
        }, Constants.DataBaseHit.DEFAULT_TIME);

    }

    private void checkInternetAndShowToast(Boolean isShowInternetMessage) {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
//                isTokenExpire();

            } else {
                if (isShowInternetMessage)
                    if (isAdded() && getActivity() != null)
                        AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
                progressBar.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }

    }


    private void makeJsonObjectRequest() {
        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            AppLogs.printErrorLogs(TAG + "Feed URL", finalFeedURL);

            photoGalleryFragmentViewModel.getPhotoGallaryList(finalFeedURL);


        } else {
            if (isAdded() && getActivity() != null)
                AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            hideProgress();

        }
    }

    private void removeLoadingFromBottom() {
        if (photoGalleryListAdapter != null)
            photoGalleryListAdapter.removeItem();
    }

    private void parseFeedList(JSONObject response) throws JSONException {
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        if (photoListItemInfoArrayList == null) {
            photoListItemInfoArrayList = new ArrayList<>();
        }
        isLoading = false;
        if (resultArray.length() > 0) {
            if (LOAD_MORE_COUNT > 0) {
                visibleThreshold += resultArray.length();

                removeLoadingFromBottom();
            } else {
                photoListItemInfoArrayList.clear();
            }

            List<PhotoListItemInfo> infoList = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), PhotoListItemInfo[].class)));
            if (LOAD_MORE_COUNT == 0) {
                final PhotoGalleryListTable photoGalleryListTable = (PhotoGalleryListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(PhotoGalleryListTable.TABLE_NAME);
                photoGalleryListTable.deleteParticularPhotoGalleryListCategory(categoryInfo.id);
                photoGalleryListTable.addPhotoGalleryListItem(categoryInfo.id, photoListItemInfoArrayList);
            }
            if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.BANNER_LIST_TOGGLE, false)) {
                infoList = BannerAdPlacementController.getInstance().addPhotoListingBannerAds(infoList);
            }
            photoListItemInfoArrayList.addAll(infoList);

            /*Inserting  PhotoGallery in mdb*/
            if (LOAD_MORE_COUNT == 0) {
                if (!categoryInfo.feedUrl.endsWith("1/"))
                    LOAD_MORE_COUNT++;
                addAdsAndWidgetInCommonList();
            }
            // Tracking
            if (categoryInfo != null) {
                trackingList.add(LOAD_MORE_COUNT);
                sendTracking();
            }
        } else {
            if (LOAD_MORE_COUNT > 0) {
                LOAD_MORE_COUNT--;
                removeLoadingFromBottom();
            }
        }

        photoGalleryListAdapter.setData(photoListItemInfoArrayList, categoryInfo);
        photoGalleryListAdapter.notifyDataSetChanged();
        hideProgress();
    }

    private void addAdsAndWidgetInCommonList() {
        for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
            if (!bannerInfoList.get(bannerIndex).isAdded) {
                int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catPos);
                if (bannerPos > -1 && photoListItemInfoArrayList.size() >= bannerPos) {
                    photoListItemInfoArrayList.add(bannerPos, new PhotoListItemInfo(PhotoGalleryListAdapter.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)));
                    bannerInfoList.get(bannerIndex).isAdded = true;
                }
            }
        }
    }


    public void moveToTop() {
        if (photoGalleryRecyclerView != null) {
            photoGalleryRecyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onDbVideoClick(int position) {
        if (position >= 0 && position < photoGalleryListAdapter.getItemCount()) {
            Intent photoGalleryIntent = new Intent(getContext(), PhotoGalleryActivity.class);
            photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, photoGalleryListAdapter.getItem(position).rssId);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, photoGalleryListAdapter.getItem(position).gTrackUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_TRACK_URL, photoGalleryListAdapter.getItem(position).trackUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            startActivity(photoGalleryIntent);
        }
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Subscribe
    public void NetworkConnectionStateChange(Boolean isConnected) {
        AppLogs.printDebugLogs(TAG + "photogallary getMessage1", "" + isConnected);
        if (isConnected) {
            AppLogs.printDebugLogs(TAG + "photogallary ConnectionC", "" + isConnected);
            showProgressBar();
            swipeRefreshLayout();
        } else {
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isShowing = false;
    }

    @Override
    public void onDestroyView() {
        AdController2.getInstance(getContext()).destroy();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }

    private void hideProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }

        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {
        isShowing = true;
        sendTracking();
    }
}
