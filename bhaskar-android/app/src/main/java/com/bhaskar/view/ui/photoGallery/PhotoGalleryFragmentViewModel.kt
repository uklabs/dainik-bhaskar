package com.bhaskar.view.ui.photoGallery

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bhaskar.data.DataRepository
import com.bhaskar.data.model.PhotoResponse

class PhotoGalleryFragmentViewModel : ViewModel() {
    private var mutableLiveData: MutableLiveData<PhotoResponse>? = null
    private var dataRepository: DataRepository? = null

    fun init() {
        if (mutableLiveData != null) {
            return
        }
        dataRepository = DataRepository.getInstance()
        mutableLiveData = MutableLiveData()
    }

    fun getPhotoGallaryList(url: String?) {
        mutableLiveData = dataRepository!!.getPhotoGallaryList(url, mutableLiveData)
    }

    val repository: LiveData<PhotoResponse>?
        get() = mutableLiveData
}