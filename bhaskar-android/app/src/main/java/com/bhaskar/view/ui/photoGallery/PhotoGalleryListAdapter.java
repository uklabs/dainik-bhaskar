package com.bhaskar.view.ui.photoGallery;

import android.content.Context;
import android.graphics.PorterDuff;

import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.ProgressBar;

import com.bhaskar.R;
import com.db.ads.AdPlacementController;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.AdController2;
import com.bhaskar.appscommon.ads.BannerCardAdViewHolder;
import com.bhaskar.appscommon.ads.NativeAdViewHolder2;
import com.db.data.models.CategoryInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.data.source.server.BackgroundRequest;
import com.db.divya_new.MoreOptionInterface;
import com.db.homebanner.BannerViewHolder;
import com.db.listeners.OnDBVideoClickListener;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class PhotoGalleryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController2.onAdNotifyListener, NativeListAdController2.onAdNotifyListener {

    public static final int MAIN_LIST = 1;
    private final int VIEW_TYPE_LOADING = 2;
    public static final int LIST_HORIZONTAL = 3;
    public static final int VIEW_TYPE_BANNER = 4;

    private final int VIEW_TYPE_BANNER_ADS_LISTING = 5;
    private final int VIEW_TYPE_NATIVE_ADS_HORIZONTAL = 6;

    private boolean isFromHome = false;
    private int showCount;
    private Context mContext;
    private String color;
    private String categoryId;
    private ArrayList<PhotoListItemInfo> photoListItemInfoArrayList;
    private OnDBVideoClickListener onDBVideoClickListener;
    private int mViewType = MAIN_LIST;

    public PhotoGalleryListAdapter(Context contexts, OnDBVideoClickListener onDBVideoClickListener) {
        this.onDBVideoClickListener = onDBVideoClickListener;
        this.mContext = contexts;
        this.photoListItemInfoArrayList = new ArrayList<>();
    }

    public void setData(List<PhotoListItemInfo> newsFeed, CategoryInfo categoryInfo) {
        this.color = categoryInfo.color;
        photoListItemInfoArrayList.clear();
        photoListItemInfoArrayList.addAll(newsFeed);
    }

    public void addItem() {
        photoListItemInfoArrayList.add(null);
        notifyDataSetChanged();
    }

    public void removeItem() {
        if (photoListItemInfoArrayList.size() > 0 && photoListItemInfoArrayList.get(photoListItemInfoArrayList.size() - 1) == null)
            photoListItemInfoArrayList.remove(photoListItemInfoArrayList.size() - 1);
        notifyDataSetChanged();
    }

    public void setDataAndClear(ArrayList<PhotoListItemInfo> newsFeed, CategoryInfo categoryInfo) {
        this.categoryId = categoryInfo.id;
        this.color = categoryInfo.color;
        photoListItemInfoArrayList.clear();
        photoListItemInfoArrayList.addAll(newsFeed);

        if (isFromHome) {
            AdPlacementController.getInstance().addHomePhotoListingNativeAds(photoListItemInfoArrayList, mContext);
        } else {
            AdPlacementController.getInstance().addPhotoListingNativeAds(photoListItemInfoArrayList, mContext);
        }

        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case LIST_HORIZONTAL:
                return new PhotoGallaryViewHolder(inflater.inflate(R.layout.list_photo_gallary_horizontal, parent, false));
            case MAIN_LIST:
                return new PhotoGallaryViewHolder(inflater.inflate(R.layout.photo_gallery_recycler_list_item, parent, false));
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item, parent, false));
            case VIEW_TYPE_BANNER_ADS_LISTING:
                return new BannerCardAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_cardview, parent, false));
            case VIEW_TYPE_NATIVE_ADS_HORIZONTAL:
                return new NativeAdViewHolder2(inflater.inflate(R.layout.ad_layout_native_photo_horizontal, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            default:
                break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case MAIN_LIST:

                final PhotoGallaryViewHolder mainViewHolder = (PhotoGallaryViewHolder) holder;
                PhotoListItemInfo photoListItemInfo = photoListItemInfoArrayList.get(position);
                if(TextUtils.isEmpty(photoListItemInfo.colorCode))
                    photoListItemInfo.colorCode = color;
                if (!TextUtils.isEmpty(photoListItemInfo.rssTitle)) {
                    mainViewHolder.tvPhotoGalleryTitle.setText(AppUtils.getInstance().getHomeTitle(photoListItemInfo.iitlTitle, AppUtils.getInstance().fromHtml(photoListItemInfo.rssTitle), photoListItemInfo.colorCode));
                } else {
                    mainViewHolder.tvPhotoGalleryTitle.setVisibility(View.GONE);
                    mainViewHolder.moreIV.setVisibility(View.GONE);
                }

                float ratio1 = AppUtils.getInstance().parseImageRatio(photoListItemInfo.imageSize, Constants.ImageRatios.PHOTO_GALLARY_RATIO);
                AppUtils.getInstance().setImageViewSizeWithAspectRatio(mainViewHolder.ivPhotoGalleryImage, ratio1, 32, mContext);
                ImageUtil.setImage(mContext, photoListItemInfo.rssImage, mainViewHolder.ivPhotoGalleryImage, R.drawable.water_mark_news_detail_error);
                mainViewHolder.itemView.setOnClickListener(v -> {
                    if (onDBVideoClickListener != null) {
                        onDBVideoClickListener.onDbVideoClick(position);
                    }
                });

                mainViewHolder.moreIV.setOnClickListener(view -> {

                    PopupWindow popUpWindow = AppUtils.showMoreOptionsV2(mContext, view, new MoreOptionInterface() {
                        @Override
                        public void onBookMark() {
                            //not visible
                        }

                        @Override
                        public void onShare() {
                            PhotoListItemInfo photoListItemInfo = photoListItemInfoArrayList.get(position);

                            if (!TextUtils.isEmpty(photoListItemInfo.shareLink)) {
                                AppUtils.getInstance().shareArticle(mContext, photoListItemInfo.shareLink, photoListItemInfo.rssTitle, photoListItemInfo.gTrackUrl, false);
                            } else {
                                BackgroundRequest.getStoryShareUrl(mContext, photoListItemInfo.link, (flag, storyShareUrl) -> {
                                    photoListItemInfo.shareLink = storyShareUrl;
                                    AppUtils.getInstance().shareArticle(mContext, photoListItemInfo.shareLink, photoListItemInfo.rssTitle, photoListItemInfo.gTrackUrl, false);
                                });
                            }
                        }
                    });

                    popUpWindow.getContentView().findViewById(R.id.tvBookMark).setVisibility(View.GONE);
                    popUpWindow.getContentView().findViewById(R.id.separator_v).setVisibility(View.GONE);
                });

                break;

            case LIST_HORIZONTAL:
                final PhotoGallaryViewHolder mainHorizatalViewHolder = (PhotoGallaryViewHolder) holder;
                PhotoListItemInfo photoListItemInfo1 = photoListItemInfoArrayList.get(position);
                if(TextUtils.isEmpty(photoListItemInfo1.colorCode))
                    photoListItemInfo1.colorCode = color;
                mainHorizatalViewHolder.tvPhotoGalleryTitle.setText(AppUtils.getInstance().getHomeTitle(photoListItemInfo1.iitlTitle, AppUtils.getInstance().fromHtml(photoListItemInfo1.rssTitle), photoListItemInfo1.colorCode));
                float ratio2 = AppUtils.getInstance().parseImageRatio(photoListItemInfo1.imageSize, Constants.ImageRatios.PHOTO_GALLARY_RATIO);
                AppUtils.getInstance().setImageViewSizeWithAspectRatio(mainHorizatalViewHolder.ivPhotoGalleryImage, ratio2, 0, mContext);
                ImageUtil.setImage(mContext, photoListItemInfo1.rssImage, mainHorizatalViewHolder.ivPhotoGalleryImage, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail);
                mainHorizatalViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onDBVideoClickListener != null) {
                            onDBVideoClickListener.onDbVideoClick(position);
                        }
                    }
                });
                break;

            case VIEW_TYPE_LOADING:
                PhotoGalleryListAdapter.LoadingViewHolder loadingViewHolder = (PhotoGalleryListAdapter.LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
                break;

            case VIEW_TYPE_BANNER_ADS_LISTING:
                showBannerAds(((BannerCardAdViewHolder) holder), position);
                break;

            case VIEW_TYPE_NATIVE_ADS_HORIZONTAL:
                showHorizontalNativeAds(((NativeAdViewHolder2) holder), position);
                break;

            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData(photoListItemInfoArrayList.get(bannerViewHolder.getAdapterPosition()).mBannerInfo);
                break;

            default:
                break;
        }
    }

    @Override
    public void onNotifyAd() {
        notifyDataSetChanged();
    }

    private void showBannerAds(BannerCardAdViewHolder viewHolder, int pos) {
        PhotoListItemInfo itemInfo = photoListItemInfoArrayList.get(pos);
        viewHolder.addAds(AdController2.getInstance(mContext).getAd(itemInfo.adUnit, new WeakReference<AdController2.onAdNotifyListener>(this)));
    }

    private void showHorizontalNativeAds(NativeAdViewHolder2 holder, int pos) {
        PhotoListItemInfo adInfo = photoListItemInfoArrayList.get(pos);
        UnifiedNativeAd ad = NativeListAdController2.getInstance(mContext).getAdForHome(adInfo.adUnit, adInfo.adType, pos, categoryId, new WeakReference<NativeListAdController2.onAdNotifyListener>(this));
        holder.populateNativeAdView(ad);
    }

    @Override
    public int getItemViewType(int position) {
        if (photoListItemInfoArrayList.get(position) == null) {
            return VIEW_TYPE_LOADING;
        } else if (mViewType == LIST_HORIZONTAL) {
            if (photoListItemInfoArrayList.get(position).isAd) {
                return VIEW_TYPE_NATIVE_ADS_HORIZONTAL;
            }
            return LIST_HORIZONTAL;
        } else {
            if (photoListItemInfoArrayList.get(position).isAd) {
                return VIEW_TYPE_BANNER_ADS_LISTING;
            } else if (photoListItemInfoArrayList.get(position).type > 0) {
                return photoListItemInfoArrayList.get(position).type;
            }
            return MAIN_LIST;
        }
    }

    @Override
    public int getItemCount() {
        if (isFromHome) {
            return (showCount > 0 && photoListItemInfoArrayList != null && photoListItemInfoArrayList.size() > 0) ? showCount : photoListItemInfoArrayList == null ? 0 : (photoListItemInfoArrayList.size());
        } else {
            return photoListItemInfoArrayList == null ? 0 : (photoListItemInfoArrayList.size());
        }

    }

    public void setDataInAdapter(int viewType, int count) {
        mViewType = viewType;
        isFromHome = true;
        this.showCount = count;
        notifyDataSetChanged();
    }

    public PhotoListItemInfo getItem(int position) {
        return photoListItemInfoArrayList.get(position);
    }

    public void clear() {
        if (photoListItemInfoArrayList != null)
            photoListItemInfoArrayList.clear();
        notifyDataSetChanged();
    }

    // View Holders
    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        private LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }
}