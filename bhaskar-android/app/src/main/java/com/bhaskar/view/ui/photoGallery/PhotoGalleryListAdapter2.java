package com.bhaskar.view.ui.photoGallery;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import androidx.recyclerview.widget.RecyclerView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.ads.AdPlacementController;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.AdController2;
import com.bhaskar.appscommon.ads.BannerCardAdViewHolder;
import com.bhaskar.appscommon.ads.NativeAdViewHolder2;
import com.db.data.models.PhotoListItemInfo;
import com.db.divya_new.MoreOptionInterface;
import com.db.homebanner.BannerViewHolder;
import com.db.listeners.OnDBVideoClickListener;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class PhotoGalleryListAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController2.onAdNotifyListener, NativeListAdController2.onAdNotifyListener {

    public static final int MAIN_LIST = 1;
    public static final int LIST_HORIZONTAL = 3;
    public static final int VIEW_TYPE_BANNER = 4;

    private final int VIEW_TYPE_BANNER_ADS_LISTING = 5;
    private final int VIEW_TYPE_NATIVE_ADS_HORIZONTAL = 6;

    private Context mContext;
    private String color;
    private String categoryId;
    private ArrayList<Object> photoListItemInfoArrayList;
    private OnDBVideoClickListener onDBVideoClickListener;
    private int mViewType = MAIN_LIST;

    public PhotoGalleryListAdapter2(Context contexts, OnDBVideoClickListener onDBVideoClickListener) {
        this.onDBVideoClickListener = onDBVideoClickListener;
        this.mContext = contexts;
        this.photoListItemInfoArrayList = new ArrayList<>();
    }

    public void setData(List<Object> newsFeed, String color, String categoryId, int viewType) {
        this.categoryId = categoryId;
        this.mViewType = viewType;
        this.color = color;
        photoListItemInfoArrayList.clear();
        photoListItemInfoArrayList.addAll(newsFeed);

        AdPlacementController.getInstance().addHomePhotoListingNativeAdsWithObject(photoListItemInfoArrayList, mContext);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case LIST_HORIZONTAL:
                return new PhotoGallaryViewHolder(inflater.inflate(R.layout.list_photo_gallary_horizontal, parent, false));
            case MAIN_LIST:
                return new PhotoGallaryViewHolder(inflater.inflate(R.layout.photo_gallery_recycler_list_item, parent, false));
            case VIEW_TYPE_BANNER_ADS_LISTING:
                return new BannerCardAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_cardview, parent, false));
            case VIEW_TYPE_NATIVE_ADS_HORIZONTAL:
                return new NativeAdViewHolder2(inflater.inflate(R.layout.ad_layout_native_photo_horizontal, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            default:
                break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        PhotoListItemInfo photoListItemInfo = (PhotoListItemInfo) photoListItemInfoArrayList.get(position);

        switch (holder.getItemViewType()) {
            case MAIN_LIST:
                final PhotoGallaryViewHolder mainViewHolder = (PhotoGallaryViewHolder) holder;
                if (!TextUtils.isEmpty(photoListItemInfo.rssTitle)) {
                    mainViewHolder.tvPhotoGalleryTitle.setText(AppUtils.getInstance().getHomeTitle(photoListItemInfo.iitlTitle, AppUtils.getInstance().fromHtml(photoListItemInfo.rssTitle), color));
                } else {
                    mainViewHolder.tvPhotoGalleryTitle.setVisibility(View.GONE);
                    mainViewHolder.moreIV.setVisibility(View.GONE);
                }

                float ratio1 = AppUtils.getInstance().parseImageRatio(photoListItemInfo.imageSize, Constants.ImageRatios.PHOTO_GALLARY_RATIO);
                AppUtils.getInstance().setImageViewSizeWithAspectRatio(mainViewHolder.ivPhotoGalleryImage, ratio1, 32, mContext);
                ImageUtil.setImage(mContext, photoListItemInfo.rssImage, mainViewHolder.ivPhotoGalleryImage, R.drawable.water_mark_news_detail, R.drawable.water_mark_big_news_detail_error);
                mainViewHolder.itemView.setOnClickListener(v -> {
                    if (onDBVideoClickListener != null) {
                        onDBVideoClickListener.onDbVideoClick(position);
                    }
                });

                mainViewHolder.moreIV.setOnClickListener(view -> {
                    PopupWindow popUpWindow = AppUtils.showMoreOptionsV2(mContext, view, new MoreOptionInterface() {
                        @Override
                        public void onBookMark() {
                            //not visible
                        }

                        @Override
                        public void onShare() {
                            ImageUtil.shareImageAfterDownload(mContext, photoListItemInfo.rssImage, mContext.getResources().getString(R.string.app_name));

                            //tracking shifted
                            //send tracking
                            String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                            Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.SHARE, photoListItemInfo.link, campaign);
                        }
                    });
                    popUpWindow.getContentView().findViewById(R.id.tvBookMark).setVisibility(View.GONE);
                    popUpWindow.getContentView().findViewById(R.id.separator_v).setVisibility(View.GONE);
                });
                break;

            case LIST_HORIZONTAL:
                final PhotoGallaryViewHolder mainHorizatalViewHolder = (PhotoGallaryViewHolder) holder;
                mainHorizatalViewHolder.tvPhotoGalleryTitle.setText(AppUtils.getInstance().getHomeTitle(photoListItemInfo.iitlTitle, AppUtils.getInstance().fromHtml(photoListItemInfo.rssTitle), color));
                float ratio2 = AppUtils.getInstance().parseImageRatio(photoListItemInfo.imageSize, Constants.ImageRatios.PHOTO_GALLARY_RATIO);
                AppUtils.getInstance().setImageViewSizeWithAspectRatio(mainHorizatalViewHolder.ivPhotoGalleryImage, ratio2, 0, mContext);
                ImageUtil.setImage(mContext, photoListItemInfo.rssImage, mainHorizatalViewHolder.ivPhotoGalleryImage, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);
                mainHorizatalViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onDBVideoClickListener != null) {
                            onDBVideoClickListener.onDbVideoClick(position);
                        }
                    }
                });
                break;

            case VIEW_TYPE_BANNER_ADS_LISTING:
                showBannerAds(((BannerCardAdViewHolder) holder), position);
                break;

            case VIEW_TYPE_NATIVE_ADS_HORIZONTAL:
                showHorizontalNativeAds(((NativeAdViewHolder2) holder), position);
                break;

            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                PhotoListItemInfo photoListItemInfo1 = (PhotoListItemInfo) photoListItemInfoArrayList.get(bannerViewHolder.getAdapterPosition());

                bannerViewHolder.callwebViewOrSetWidgetData(photoListItemInfo1.mBannerInfo);
                break;

            default:
                break;
        }
    }

    @Override
    public void onNotifyAd() {
        notifyDataSetChanged();
    }

    private void showBannerAds(BannerCardAdViewHolder viewHolder, int pos) {
        PhotoListItemInfo itemInfo = (PhotoListItemInfo) photoListItemInfoArrayList.get(pos);
        viewHolder.addAds(AdController2.getInstance(mContext).getAd(itemInfo.adUnit, new WeakReference<AdController2.onAdNotifyListener>(this)));
    }

    private void showHorizontalNativeAds(NativeAdViewHolder2 holder, int pos) {
        PhotoListItemInfo adInfo = (PhotoListItemInfo) photoListItemInfoArrayList.get(pos);
        UnifiedNativeAd ad = NativeListAdController2.getInstance(mContext).getAdForHome(adInfo.adUnit, adInfo.adType, pos, categoryId, new WeakReference<NativeListAdController2.onAdNotifyListener>(this));
        holder.populateNativeAdView(ad);
    }

    @Override
    public int getItemViewType(int position) {

        PhotoListItemInfo photoListItemInfo = (PhotoListItemInfo) photoListItemInfoArrayList.get(position);
        if (mViewType == LIST_HORIZONTAL) {
            if (photoListItemInfo.isAd) {
                return VIEW_TYPE_NATIVE_ADS_HORIZONTAL;
            }
            return LIST_HORIZONTAL;
        } else {
            if (photoListItemInfo.isAd) {
                return VIEW_TYPE_BANNER_ADS_LISTING;
            } else if (photoListItemInfo.type > 0) {
                return photoListItemInfo.type;
            }
            return MAIN_LIST;
        }
    }

    @Override
    public int getItemCount() {
        return photoListItemInfoArrayList == null ? 0 : (photoListItemInfoArrayList.size());
    }

    public PhotoListItemInfo getItem(int position) {
        return (PhotoListItemInfo) photoListItemInfoArrayList.get(position);
    }
}