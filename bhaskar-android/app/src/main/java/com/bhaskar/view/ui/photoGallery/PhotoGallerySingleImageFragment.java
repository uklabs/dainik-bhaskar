package com.bhaskar.view.ui.photoGallery;


import android.content.Context;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bhaskar.R;
import com.bhaskar.util.TouchImageView;
import com.bhaskar.util.sensor.GyroscopeObserver;
import com.bhaskar.util.sensor.PanoramaImageView;
import com.db.util.ImageUtil;
import com.db.util.AppUtils;


public class PhotoGallerySingleImageFragment extends Fragment {

    private String title, imagePath1, detailText;
    private SensorManager mSensorManager;
    private GyroscopeObserver gyroscopeObserver;
    TextView txtTitle, txtSlugIntro;

    public static PhotoGallerySingleImageFragment getInstance(String title, String imagePath, String detailText) {
        PhotoGallerySingleImageFragment photoGalleryDetailFragment = new PhotoGallerySingleImageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("imagePath", imagePath);
        bundle.putString("detailText", detailText);
        photoGalleryDetailFragment.setArguments(bundle);
        return photoGalleryDetailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_gallery_detail, null);
        final ViewGroup transitionsContainer = (ViewGroup) getActivity().findViewById(R.id.transitions_container);
        txtTitle = ((TextView) rootView.findViewById(R.id.title));
        txtSlugIntro = ((TextView) rootView.findViewById(R.id.slug_intro));

        Bundle bundle = getArguments();

        if (bundle != null) {
            title = bundle.getString("title");
            imagePath1 = bundle.getString("imagePath");
            detailText = bundle.getString("detailText");
        }
        gyroscopeObserver = new GyroscopeObserver();

        txtTitle.setText(title);
        if (TextUtils.isEmpty(detailText)) {
            txtSlugIntro.setVisibility(View.GONE);
        } else {
            txtSlugIntro.setText(detailText);
        }
        AppUtils.makeTextViewResizable(((TextView) rootView.findViewById(R.id.slug_intro)), 3, "More", true);

        final String imagePath = imagePath1;
        if (mSensorManager == null) {
            mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        }
        final PanoramaImageView panoramaImageView = ((PanoramaImageView) rootView.findViewById(R.id.iv_photo_gallery_image));
        final TouchImageView touchImageView = ((TouchImageView) rootView.findViewById(R.id.iv_photo_touch_image));
        panoramaImageView.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(imagePath)) {

//            if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                panoramaImageView.setVisibility(View.VISIBLE);
//                touchImageView.setVisibility(View.GONE);
//                panoramaImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//
//                ImageUtil.setImage(getContext(), imagePath, panoramaImageView, R.drawable.water_mark_news_detail);
//            } else {
            panoramaImageView.setVisibility(View.GONE);
            touchImageView.setVisibility(View.VISIBLE);

            ImageUtil.setImage(getContext(), imagePath, touchImageView, R.drawable.water_mark_news_detail);
//            }

        } else {
            touchImageView.setVisibility(View.VISIBLE);
            touchImageView.setImageResource(R.drawable.water_mark_news_detail);
        }

        panoramaImageView.setGyroscopeObserver(gyroscopeObserver);

        panoramaImageView.setOnTouchListener(new View.OnTouchListener() {

            boolean mExpanded;
            private GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    mExpanded = !mExpanded;

                    return false;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        TransitionManager.beginDelayedTransition(transitionsContainer, new TransitionSet()
                                .addTransition(new ChangeBounds())
                                .addTransition(new ChangeImageTransform()));
                    }


                    if (!mExpanded) {
                        panoramaImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

                        txtSlugIntro.setVisibility(View.VISIBLE);
                        txtTitle.setVisibility(View.VISIBLE);
                        ViewGroup.LayoutParams params = panoramaImageView.getLayoutParams();
                        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                        panoramaImageView.setLayoutParams(params);
                        gyroscopeObserver.unregister();

                    } else {
                        txtSlugIntro.setVisibility(View.GONE);
                        txtTitle.setVisibility(View.GONE);
                        ViewGroup.LayoutParams params = panoramaImageView.getLayoutParams();
                        params.height = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.MATCH_PARENT;
                        params.width = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.MATCH_PARENT;
                        panoramaImageView.setLayoutParams(params);
                        gyroscopeObserver.register(getActivity());
                        panoramaImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                    }
                    return false;
                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

        touchImageView.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
            boolean mExpanded;

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {

                return false;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                mExpanded = !mExpanded;
                return false;
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                if (!mExpanded) {
                    txtSlugIntro.setVisibility(View.VISIBLE);
                    txtTitle.setVisibility(View.VISIBLE);
                } else {
                    txtSlugIntro.setVisibility(View.GONE);
                    txtTitle.setVisibility(View.GONE);
                }
                return false;
            }
        });


        return rootView;
    }

}
