package com.bhaskar.view.ui.splash;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.BuildConfig;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdsConstants;
import com.bhaskar.appscommon.ads.consents.Consents;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.atracker.ATracker;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.AppUrls;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.util.LoginController;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.db.appintro.activities.AppIntroDashboardActivity;
import com.db.appintro.utils.ChatIntroConst;
import com.db.data.database.BooleanExpressionTable;
import com.db.data.database.CommonListTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.NotificationStackTable;
import com.db.data.models.CategoryInfo;
import com.db.data.models.SplashBundleInfo;
import com.db.data.source.server.AppControlService;
import com.db.data.source.server.fcm.FirebaseNotificationConstants;
import com.db.dbvideo.player.BusProvider;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.VideoPlayerActivity;
import com.db.dbvideoPersonalized.detail.VideoPlayerListFragment;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.articlePage.DivyaPartraitVideoActivity;
import com.db.eventWrapper.AppControlEventWrapper;
import com.db.home.ActivityUtil;
import com.db.home.AppNotificationActivity;
import com.db.home.fantasyMenu.FantasyUtil;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.main.NotificationDialogActivity;
import com.db.main.QuickNewsNotificationService2;
import com.db.splash.SplashCallbackListener;
import com.db.splash.SplashFragmentCallbackListener;
import com.db.splash.SplashGifFragment;
import com.db.splash.SplashImageFragment;
import com.db.splash.SplashVideoFragment;
import com.db.splash.SplashWapFragment;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.JsonPreferences;
import com.db.util.NetworkStatus;
import com.db.util.Notificationutil;
import com.db.util.OpenNewsLinkInApp;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.db.views.DBSplashTimeTicker;
import com.facebook.FacebookSdk;
import com.facebook.network.connectionclass.ConnectionClassManager;
import com.facebook.network.connectionclass.ConnectionQuality;
import com.facebook.network.connectionclass.DeviceBandwidthSampler;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class SplashActivity extends AppCompatActivity implements OnFeedFetchFromServerListener {

    private static final int REQUEST_CODE_PERMISSIONS = 10;
    private static final int ACTION_LIKE = 1;
    private static final int ACTION_UNLIKE = 2;
    private static final int ACTION_SHARE = 3;
    private static final int ACTION_READ_MORE = 4;
    private final String TAG = AppConfig.BaseTag + "." + SplashActivity.class.getSimpleName();
    boolean isShowDialog = false;

    Handler finishSplashHandler = new Handler();
    Runnable finishRunnable = this::finish;
    Handler drawerCloser = new Handler();
    private ImageView openArrow, mute_volume;
    private ImageView imgSplashBg;
    private boolean isOptionDrawerOpen = false;
    private int touchType = -1;
    private long afterDurationClose = 4000L;
    private boolean isLongPress = false;
    private boolean soundOnOff = true;
    private VideoInfo videoInfo;
    private boolean haveToStopForDBQuiz, isHitCompleted, isFromPush, isFromStackNotification, isFromWidget, isFromQuickNotification,
            isStartActivityCall, clickViaNotification, firstTimeJsonLoaded, isAppControlFetchDone = false, isVideoNotification;
    private boolean isLikeProcessing = false, isShareProcessing = false, isReadMoreProcessing = false;
    private int isSplashSkipStatus = 0;
    private int splashType = -1;
    private String vidoeSectionLabel, videoFeedUrl, gaEventLabel;
    private BooleanExpressionTable booleanExpressionTable;
    /*Splash get Extra to SplashBundleInfo*/
    private SplashVideoFragment splashVideoFragment = null;
    private SplashBundleInfo splashBundleInfo;
    private LinearLayout splashActionLayout;
    private DBSplashTimeTicker dbSplashTimeTicker;
    Runnable drawerCloserRunnable = new Runnable() {
        @Override
        public void run() {
            closeOBDrawer();
            if (dbSplashTimeTicker != null)
                dbSplashTimeTicker.onResume();
        }
    };
    private ConnectionClassManager mConnectionClassManager;
    private DeviceBandwidthSampler mDeviceBandwidthSampler;
    private SplashFragmentCallbackListener splashFragmentCallbackListener;
    SplashCallbackListener splashCallbackListener = new SplashCallbackListener() {
        @Override
        public void onSplashProgress(boolean show, SplashFragmentCallbackListener splashFragmentCallbackListener) {
        }

        @Override
        public void onSplashPrepared(SplashFragmentCallbackListener splashFragmentCallbackListener) {
            try {
                String splashId = Constants.splashDataInfo.splashId;
                String countText = AppPreferences.getInstance(SplashActivity.this).getStringValue(QuickPreferences.SPLASH_COUNT, null);
                int shownCount = 0;
                if (countText != null) {
                    String[] split = countText.split(":");
                    String splashIdSaved = split[0];
                    if (splashId.equalsIgnoreCase(splashIdSaved))
                        shownCount = AdsConstants.getIntFromString(split[1]);
                }
                AppPreferences.getInstance(SplashActivity.this).setStringValue(QuickPreferences.SPLASH_COUNT, splashId + ":" + (shownCount + 1));
            } catch (Exception ignored) {
            }


            new Handler().postDelayed(() -> {
                findViewById(R.id.splash_featured_layout).setVisibility(View.VISIBLE);
                TranslateAnimation bottomOptionAnim = new TranslateAnimation(0, 0, 100, 0);
                bottomOptionAnim.setDuration(500);
                bottomOptionAnim.setFillAfter(true);
                bottomOptionAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        findViewById(R.id.bottom_parent).setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                findViewById(R.id.bottom_parent).startAnimation(bottomOptionAnim);

                TranslateAnimation timeTickerAnim = new TranslateAnimation(0, 0, -200, 0);
                timeTickerAnim.setDuration(500);
                timeTickerAnim.setFillAfter(true);
                timeTickerAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        findViewById(R.id.tick_tack_timer).setVisibility(View.VISIBLE);
                        startTickTacTimer();
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                findViewById(R.id.tick_tack_timer).startAnimation(timeTickerAnim);


                if (splashType == Constants.SplashType.SPLASH_TYPE_VIDEO) {
                    TranslateAnimation volumeAnim = new TranslateAnimation(0, 0, -200, 0);
                    volumeAnim.setDuration(500);
                    volumeAnim.setFillAfter(true);
                    volumeAnim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            findViewById(R.id.volume).setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                    findViewById(R.id.volume).startAnimation(volumeAnim);
                }

            }, 1000);

            isSplashSkipStatus = 1;
            String splashId = "";
            if (Constants.splashDataInfo != null)
                splashId = Constants.splashDataInfo.splashId;
            String campaign = AppPreferences.getInstance(SplashActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            String source = AppPreferences.getInstance(SplashActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(SplashActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String value;
            if (isFromQuickNotification || isFromStackNotification)
                value = AppFlyerConst.GAScreen.NOTIFY_SPLASH;
            else
                value = AppFlyerConst.GAScreen.SPLASH;
            Tracking.trackGAScreen(SplashActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), value, source, medium, campaign);
            Tracking.trackGAEvent(SplashActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.SPLASH, AppFlyerConst.GAAction.VIEW, splashId, campaign);
            if (splashType == Constants.SplashType.SPLASH_TYPE_VIDEO)
                initMuteIcon();
        }

        @Override
        public void onSplashComplete(SplashFragmentCallbackListener splashFragmentCallbackListener) {
            isSplashSkipStatus = 2;
            init();
            if (splashType == Constants.SplashType.SPLASH_TYPE_VIDEO && mute_volume != null)
                mute_volume.setVisibility(View.GONE);
        }

        @Override
        public void onSplashFailed(SplashFragmentCallbackListener splashFragmentCallbackListener) {
            isSplashSkipStatus = 2;
            init();
        }
    };

    @Override
    protected void
    onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setNavigationBarColor(getResources().getColor(R.color.divya_theme_color));
        }
        BusProvider.getInstance().register(this);

        if (AppUtils.isAppRunningOnEmulator()) {
            Toast.makeText(this, "Sorry! We are unable to support this device.", Toast.LENGTH_LONG).show();
            return;
        }

        AppPreferences.getInstance(this).setStringValue(QuickPreferences.FeedData.HOME_PAGE, "");
        AppPreferences.getInstance(this).setStringValue(QuickPreferences.FeedData.HOME_PAGE_PG1, "");

        mConnectionClassManager = ConnectionClassManager.getInstance();
        mDeviceBandwidthSampler = DeviceBandwidthSampler.getInstance();
        mDeviceBandwidthSampler.startSampling();

        beforeSetContent();
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_splash);

        splashBundleInfo = new SplashBundleInfo();
        imgSplashBg = findViewById(R.id.imgSplashBg);

        booleanExpressionTable = (BooleanExpressionTable) DatabaseHelper.getInstance(this).getTableObject(BooleanExpressionTable.TABLE_NAME);
        resetTheme();
        beforeOnCreate();
        setGreetings();
    }

    private void setGreetings() {
        try {
            TextView tvSalutationMsg = findViewById(R.id.tv_salutation_msg);
            FantasyUtil.SalutationType salutationType = FantasyUtil.getSalutationTypeBasedOnTime();
            String salutationMsg = salutationType.getSalutationMsg();
            ProfileInfo mProfileInfo = LoginPreferences.getInstance(this).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
            if (mProfileInfo != null && mProfileInfo.isUserLoggedIn) {
                salutationMsg = salutationMsg + "<br/> Welcome <b>" + mProfileInfo.name + "</b>";
            }
            tvSalutationMsg.setText(Html.fromHtml(salutationMsg));
        } catch (Exception ignored) {
        }
    }

    private void beforeSetContent() {
        Consents.getInstance(CommonConstants.DFP_PUBLISHER_ID_FOR_CONSENT).checkForConsentUpdate(this);
        //Store time stamp for mentain SessionId
        Long currentTimeStamp = System.currentTimeMillis();
        AppPreferences.getInstance(SplashActivity.this).setLongValue(QuickPreferences.SESSION_TIME_STAMP, currentTimeStamp);


        int totalSession = (AppPreferences.getInstance(this).getIntValue(AdsConstants.session_counted, 0) + 1);
        AppPreferences.getInstance(this).setIntValue(AdsConstants.session_counted, totalSession);
        AdsConstants.setSessionCount(totalSession);

        /*
//        SimpleDateFormat date = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
//        String logDate = date.format(new Date());
//        Debug.startMethodTracing("sample-" + logDate);
        */


        AppFlyerConst.isFirstGaScreenFired = false;// set false for new session track first screen view for new session.
    }

    // reset theme to oranage if diffrent
    private void resetTheme() {
        int selectColor = Constants.Themes.THEME_DARK_GRAY;
        if (AppPreferences.getInstance(this).getIntValue(Constants.Themes.THEME_COLOR, Constants.Themes.THEME_DEFAULT) != selectColor) {
            AppPreferences.getInstance(this).setIntValue(Constants.Themes.THEME_COLOR, selectColor);
        }
    }

    private void preInit() {
        if (!NetworkStatus.getInstance().isConnected(SplashActivity.this)) {
            retryIfFailure(false);
        } else {
            startAppControlService();
        }
    }

    private void beforeOnCreate() {
        // need execution at very first time
        checkAppUpdateAndInitializeVersion();
        firstTimeJsonLoad();
        fetchBundleExtra();
    }

    private void firstTimeJsonLoad() {
        if (!AppPreferences.getInstance(SplashActivity.this).getBooleanValue(QuickPreferences.IS_STATIC_JSON_STORED, false)) {
            new FirstTimeJsonLoadAsync().execute();
        } else {
            firstTimeJsonLoaded = true;
            preInit();
        }
    }

    public void deeplinkClickImpression(String extendedUrl) {
        try {
            String hashKey = String.valueOf(extendedUrl.hashCode());
            String jsonString = AppPreferences.getInstance(this).getStringValue(hashKey, "");
            SplashBundleInfo splashBundleInfo = new Gson().fromJson(jsonString, SplashBundleInfo.class);

            if (splashBundleInfo != null) {
                String eventType = "2";
                String aurl = "";
                //tracking
                String url = AppUrls.DOMAIN + AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN;
                Tracking.trackWisdomNotification(SplashActivity.this, TrackingHost.getInstance(this).getWisdomUrlForNotification(AppUrls.WISDOM_NOTIFICATION_URL), splashBundleInfo.catId, splashBundleInfo.pCatId, splashBundleInfo.title, splashBundleInfo.newsType, eventType, splashBundleInfo.storyId, aurl, url, CommonConstants.BHASKAR_APP_ID, splashBundleInfo.channelId, AppFlyerConst.GAExtra.NOTIFICATION_WIDGET_NOTIFY, splashBundleInfo.slId);
                Systr.println("Wisdom Notification tracking : " + splashBundleInfo.storyId);
                AppPreferences.getInstance(this).setStringValue(hashKey, "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void fetchBundleExtra() {
        Bundle extras = getIntent().getExtras();
        Uri dynamicUri = getIntent().getData();
        if (dynamicUri != null) {
            try {
                String deeplink = dynamicUri.toString();
                if (deeplink.contains(getString(R.string.dynamic_link_host1)) | deeplink.contains(getString(R.string.dynamic_link_host2))) {
                    ActivityUtil.getInstance().handleDynamicLink(this, deeplink, false, object -> {
                        try {
                            HashMap<String, String> parsedMap = (HashMap<String, String>) object;
                            String extendedUrl = parsedMap.get("extended_url");
                            deeplinkClickImpression(extendedUrl);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                } else {
                    deeplinkClickImpression(deeplink);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        /* Notification extras coming from MyFirebaseMessagingService*/
        if (extras != null) {
            AppLogs.printDebugLogs("", "" + extras.toString());
            isFromPush = extras.getBoolean(Constants.KeyPair.KEY_IS_NOTIFICATION, false);
            isFromStackNotification = extras.getBoolean(Constants.KeyPair.KEY_IS_STACK_NOTIFICATION, false);
            isFromWidget = extras.getBoolean(Constants.KeyPair.KEY_IS_FROM_APPWIDGET, false);
            if (isFromPush || isFromWidget) {

                splashBundleInfo.channelId = extras.getString(Constants.KeyPair.KEY_CHANNEL_SNO);
                splashBundleInfo.storyId = extras.getString(Constants.KeyPair.KEY_STORY_ID);
                splashBundleInfo.versionQuickNews = extras.getString(Constants.KeyPair.KEY_VERSION);
                isFromQuickNotification = extras.getBoolean(Constants.KeyPair.KEY_IS_FROM_QUICK_NOTIFICATION);

                splashBundleInfo.detailUrl = extras.getString(Constants.KeyPair.KEY_DETAIL_FEED_URL);
                splashBundleInfo.title = extras.getString(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE);
                splashBundleInfo.gaScreen = extras.getString(Constants.KeyPair.KEY_GA_SCREEN);
                splashBundleInfo.gaArticle = extras.getString(Constants.KeyPair.KEY_GA_ARTICLE);
                splashBundleInfo.displayName = extras.getString(Constants.KeyPair.KEY_GA_DISPLAY_NAME);
                clickViaNotification = extras.getBoolean(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION);

                splashBundleInfo.newsType = extras.getString(Constants.KeyPair.KEY_NEWS_TYPE);
                splashBundleInfo.catId = extras.getString(Constants.KeyPair.KEY_CATEGORY_ID);
                splashBundleInfo.pCatId = extras.getString(Constants.KeyPair.KEY_PCAT_ID);
                splashBundleInfo.menuId = extras.getString(Constants.KeyPair.KEY_MENU_ID);
                splashBundleInfo.webUrl = extras.getString(Constants.KeyPair.KEY_WEB_URL);
                splashBundleInfo.webTitle = extras.getString(Constants.KeyPair.KEY_WEB_TITLE);
                splashBundleInfo.isWapExt = extras.getString(Constants.KeyPair.KEY_IS_WAP_EXT);
                splashBundleInfo.campaignId = extras.getString(Constants.KeyPair.KEY_CAMPAIGN_ID);
                splashBundleInfo.domain = extras.getString(Constants.KeyPair.KEY_WISDOM_DOMAIN);
                splashBundleInfo.subDomain = extras.getString(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN);
                splashBundleInfo.inBackground = extras.getInt(Constants.KeyPair.IN_BACKGROUND, 0);
                splashBundleInfo.deeplink = extras.getString(Constants.KeyPair.KEY_DEEP_LINK);
                splashBundleInfo.slId = extras.getString(Constants.KeyPair.KEY_SL_ID);
                isHitCompleted = true;

                int count = AppPreferences.getInstance(SplashActivity.this).getIntValue(QuickPreferences.NOTIFICATION_COUNT, 0);
                if (count > 0) {
                    --count;
                    AppPreferences.getInstance(SplashActivity.this).setIntValue(QuickPreferences.NOTIFICATION_COUNT, count);
                }

                isVideoNotification = extras.getBoolean(Constants.KeyPair.KEY_IS_VIDEO_NOTIFICATION, false);
                if (isVideoNotification) {
                    videoInfo = (VideoInfo) extras.getSerializable(Constants.KeyPair.KEY_DBVIDEO_INFO);
                    vidoeSectionLabel = extras.getString(Constants.KeyPair.KEY_SECTION_LABEL);
                    videoFeedUrl = extras.getString(Constants.KeyPair.KEY_FEED_URL);
                    gaEventLabel = extras.getString(Constants.KeyPair.KEY_GA_EVENT_LABEL);
                }
            }
        }

        /* get Data from Deep Linking*/
        Uri data = getIntent().getData();

        if (data != null) {
            splashBundleInfo.uri = data.toString();
            String hostname = getResources().getString(R.string.host_name);
            String appScheme = getResources().getString(R.string.app_scheme);
            String appOpeningUrl = appScheme + "://" + hostname + "/";
            if (splashBundleInfo.uri.contains(appOpeningUrl) && splashBundleInfo.uri.length() > appOpeningUrl.length()) {
                splashBundleInfo.uri = splashBundleInfo.uri.replaceFirst(appOpeningUrl, "");
            } else if (splashBundleInfo.uri.equalsIgnoreCase(appOpeningUrl)) {
                splashBundleInfo.uri = "";
            }
        }
        initFirebaseInviteAndRemoteConfig();

    }

    private void quickReadWork() {
        if (NetworkStatus.getInstance().isConnected(this)) {
            if (AppPreferences.getInstance(this).getIntValue(QuickPreferences.QUICK_READ_STATUS, 0) == 1) {
                registerBroadCastReciever();
            } else {
                NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(Constants.AppWidgetIDS.STICKY_WIDGET_ID);
            }
        }
    }

    private void checkAppUpdateAndInitializeVersion() {
        int currentVersion = TrackingUtils.getAppVersionCode(SplashActivity.this);
        int oldVersion = AppPreferences.getInstance(SplashActivity.this).getIntValue(QuickPreferences.APP_VERSION, 1);
        if (oldVersion < currentVersion) {
            // set version code
            JsonPreferences.getInstance(this).setIntValue(QuickPreferences.JsonPreference.APP_HOST_VERSION, 1);
            JsonPreferences.getInstance(this).setIntValue(String.format(QuickPreferences.JsonPreference.CATEGORY_FEED_VERSION, CommonConstants.CHANNEL_ID), 1);
            JsonPreferences.getInstance(this).setIntValue(QuickPreferences.JsonPreference.BASE_APP_CONTROL_VERSION, 1);
        }
        AppPreferences.getInstance(SplashActivity.this).setIntValue(QuickPreferences.APP_VERSION, currentVersion);
    }

    private void showPrivacyPolicyAlertDialog() {
        if (isShowDialog) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final TextView message = new TextView(SplashActivity.this);
        message.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
        message.setPadding((int) AppUtils.getInstance().convertDpToPixel(8, SplashActivity.this),
                (int) AppUtils.getInstance().convertDpToPixel(16, SplashActivity.this), (int) AppUtils.getInstance().convertDpToPixel(8, SplashActivity.this), 0);
        message.setMovementMethod(AppUtils.getInstance().privacyMovementMethod(SplashActivity.this, "Privacy Policy"));
        message.setText(AppUtils.getInstance().fromHtml(getResources().getString(R.string.privacy_policy_and_term_condition_gdpr)));

        message.setTextSize(16);
        builder.setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.privacy_agree), (dialog, id) -> {
                    /*GDPR conscent */
                    AppPreferences.getInstance(SplashActivity.this).setBooleanValue(QuickPreferences.GdprConst.GA_CONSENT, false);
                    AppPreferences.getInstance(SplashActivity.this).setBooleanValue(QuickPreferences.GdprConst.ADS_CONSENT, false);
                    AppPreferences.getInstance(SplashActivity.this).setBooleanValue(QuickPreferences.GdprConst.RECOMMENDATION_CONSENT, false);
                    AppPreferences.getInstance(SplashActivity.this).setBooleanValue(QuickPreferences.GdprConst.GDPR_CONSENT_ACCEPTED, true);

                    AdsConstants.getVersionName(SplashActivity.this, BuildConfig.VERSION_NAME, AppUtils.isBlockCountry(SplashActivity.this), AppUtils.isAdEnabled(SplashActivity.this));
                    AppUtils.isAdEnabled(SplashActivity.this);

                    dialog.dismiss();
                    handleGDPRPrivacyAndAppStart();
                })
                .setView(message);


        final AlertDialog alert = builder.create();
        alert.setOnShowListener(dialog -> {
            Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
            btnPositive.setTextSize(16);
            btnPositive.setTypeface(null, Typeface.NORMAL);
        });

        isShowDialog = true;
        alert.show();
    }

    private void afterOnCreate() {

        LoginController.loginController().initAppFeedBaseURL(Urls.APP_FEED_BASE_URL);
        boolean isFacebookLoginDisabled = AppPreferences.getInstance(SplashActivity.this).getBooleanValue(QuickPreferences.IS_FACEBOOK_LOGIN_DISABLED, false);
        LoginController.loginController().setFacebookLoginCheck(isFacebookLoginDisabled);

        quickReadWork();

        try {
            String publisherId = TrackingHost.getInstance(this).getComscorePublisherId(getString(R.string.comscore_publisher_id));
            String secretId = TrackingHost.getInstance(this).getComscoreSecretId(getString(R.string.comscore_secret_id));
            boolean isBlockedCountry = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
            Tracking.initialiseComscore(this, publisherId, secretId, isBlockedCountry);
        } catch (Exception ignored) {
        }


        appUpdateTracking();

        AppPreferences.getInstance(SplashActivity.this).getStringValue(QuickPreferences.SELECTED_CHANNEL_EVENT_LABEL_TEMP, "");
        // Tracking : set GA tracker
        ((InitApplication) getApplication()).setTrackerValuesForGA();

        /*prepare citylist from json*/
        JsonParser.getInstance().prepareCityList(this, CommonConstants.CHANNEL_ID);
        // initialization for ads
        AdsConstants.getVersionName(getApplicationContext(), BuildConfig.VERSION_NAME, AppUtils.isBlockCountry(this), AppUtils.isAdEnabled(this));

        Constants.DEVICE_WIDTH = Resources.getSystem().getDisplayMetrics().widthPixels;
        Constants.DEVICE_HEIGHT = Resources.getSystem().getDisplayMetrics().heightPixels;
        // Cache Time
        Constants.FEED_CACHE_TIME = AppPreferences.getInstance(this).getIntValue(QuickPreferences.FEED_CACHE_TIME, 5) * 60 * 1000;
        checkForPermission();
        //appStart();

    }

    /************Permission***********/
    private void checkForPermission() {
        ArrayList<String> permissionArray = new ArrayList<>();
        try {
            if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionArray.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
        } catch (Exception e) {
        }

        if (!isAllPermissionProvided() && permissionArray.size() > 0) {
            ActivityCompat.requestPermissions(SplashActivity.this, permissionArray.toArray(new String[permissionArray.size()]), REQUEST_CODE_PERMISSIONS);
        } else {
            appStart();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    appStart();
                } else {
                    appStart();
                }
                break;
        }
    }

    private boolean isAllPermissionProvided() {
        try {
            if ((ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                return false;
            }
        } catch (Exception e) {
        }
        return true;
    }

    private void appUpdateTracking() {
        try {
            int curAppVersion = AppPreferences.getInstance(SplashActivity.this).getIntValue(QuickPreferences.APP_CURRENT_VERSION, 0);
            int curVersion = TrackingUtils.getAppVersionCode(SplashActivity.this);
            if (curAppVersion == 0)
                curAppVersion = curVersion;
            if (curVersion > curAppVersion) {
                Tracking.trackGAEventAlways(SplashActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.app_update, "" + curVersion, "" + curAppVersion, "");
            }
            AppPreferences.getInstance(SplashActivity.this).setIntValue(QuickPreferences.APP_CURRENT_VERSION, curVersion);
        } catch (Exception ignored) {
        }
    }

    private void checkForFcmToken() {
        // Check for FCM Token
        if (TextUtils.isEmpty(TrackingHost.getInstance(this).getFCMToken())) {
            FirebaseInstanceId instance = FirebaseInstanceId.getInstance();
            if (instance != null) {
                String refToken = instance.getToken();
                if (!TextUtils.isEmpty(refToken)) {
                    TrackingHost.getInstance(this).setFCMToken(refToken);
                    AppLogs.printDebugLogs("FcmToken", refToken);
                    String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                    String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, "");
                    String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                    String url = TrackingHost.getInstance(this).getToggleNotifyUrl(AppUrls.TOGGLE_NOTIFY_URL);

                    @ATracker.ToggleStatus int notificationValue = AppPreferences.getInstance(this).getIntValue(QuickPreferences.NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);
                    Tracking.notifyToggle(this, CommonConstants.BHASKAR_APP_ID, url, notificationValue, ATracker.ToggleType.OTHER, source, medium, campaign, null, "1", "1", "1", "1");

                }
            }
        }
    }

    @Override
    public void onPostResume() {
        super.onPostResume();
    }

    private void initFirebaseInviteAndRemoteConfig() {
        final AppPreferences prefs = AppPreferences.getInstance(SplashActivity.this);

        if (prefs.getStringValue(QuickPreferences.UTM_MEDIUM, null) == null) {
            FirebaseDynamicLinks.getInstance().getDynamicLink(getIntent())
                    .addOnSuccessListener(this, data -> {
                        if (data == null) {
                            if (prefs.getStringValue(QuickPreferences.UTM_SOURCE, "").length() == 0) {
                                prefs.setStringValue(QuickPreferences.UTM_SOURCE, QuickPreferences.UTM_DIRECT);
                                prefs.setStringValue(QuickPreferences.UTM_MEDIUM, QuickPreferences.UTM_DIRECT);
                                prefs.setStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
                            }
                            AppLogs.printErrorLogs(TAG, "getInvitation: no deep link found.");
                            return;
                        }
                        String deepLink = data.getLink().toString();
                        if (deepLink.contains("?")) {
                            try {
                                String params = deepLink.split("\\?")[1];
                                Map<String, String> paramsMap = new HashMap<>();
                                for (String s : params.split("&")) {
                                    paramsMap.put(s.split("=")[0], s.split("=")[1]);
                                    AppLogs.printErrorLogs(TAG + s.split("=")[0], s.split("=")[1]);
                                }
                                if (prefs.getStringValue(QuickPreferences.UTM_SOURCE, "").length() == 0 || prefs.getStringValue(QuickPreferences.UTM_CAMPAIGN, "").equalsIgnoreCase(QuickPreferences.UTM_DIRECT)) {
                                    prefs.setStringValue(QuickPreferences.UTM_SOURCE, paramsMap.get(QuickPreferences.UTM_SOURCE));
                                    prefs.setStringValue(QuickPreferences.UTM_MEDIUM, paramsMap.get(QuickPreferences.UTM_MEDIUM));
                                    prefs.setStringValue(QuickPreferences.UTM_CAMPAIGN, paramsMap.get(QuickPreferences.UTM_CAMPAIGN));
                                }
                                FirebaseAnalytics.getInstance(SplashActivity.this).setUserProperty(paramsMap.get(QuickPreferences.UTM_MEDIUM), paramsMap.get(QuickPreferences.UTM_CAMPAIGN));
                            } catch (Exception e) {
                            }
                        } else {
                            if (prefs.getStringValue(QuickPreferences.UTM_SOURCE, "").length() == 0) {

                                prefs.setStringValue(QuickPreferences.UTM_SOURCE, QuickPreferences.UTM_DIRECT);
                                prefs.setStringValue(QuickPreferences.UTM_MEDIUM, QuickPreferences.UTM_DIRECT);
                                prefs.setStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
                            }
                        }
                        AppLogs.printErrorLogs(TAG, deepLink);
                    })
                    .addOnFailureListener(this, e -> {

                        if (prefs.getStringValue(QuickPreferences.UTM_SOURCE, "").length() == 0) {
                            prefs.setStringValue(QuickPreferences.UTM_SOURCE, QuickPreferences.UTM_DIRECT);
                            prefs.setStringValue(QuickPreferences.UTM_MEDIUM, QuickPreferences.UTM_DIRECT);
                            prefs.setStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
                        }
                        AppLogs.printErrorLogs(TAG, "getInvitation: no deep link found.");
                    });
        }
    }

    private void startAppControlService() {
        try {
            Intent appControlServiceIntent = new Intent(SplashActivity.this, AppControlService.class);
            appControlServiceIntent.putExtra(Constants.AppHitChoice.API_HIT, Constants.AppHitChoice.APP_VERSION_API);
            startService(appControlServiceIntent);
        } catch (Exception ignored) {
        }
    }

    private void appStart() {
        if (NetworkStatus.getInstance().isConnected(SplashActivity.this)) {
            checkForFcmToken();
            try {
                if (Constants.splashDataInfo != null) {
                    String splashId = Constants.splashDataInfo.splashId;
                    String countText = AppPreferences.getInstance(this).getStringValue(QuickPreferences.SPLASH_COUNT, null);
                    int splashCount = Constants.splashDataInfo.getSplashCount();
                    if (splashCount == 0 || countText == null)
                        resetSplash();
                    else {
                        String[] split = countText.split(":");
                        String splashIdSaved = split[0];
                        int splashCountSaved = AdsConstants.getIntFromString(split[1]);
                        if (!splashId.equalsIgnoreCase(splashIdSaved) || (splashId.equalsIgnoreCase(splashIdSaved) && splashCountSaved < splashCount))
                            resetSplash();
                        else
                            isSplashSkipStatus = 2;
                    }
                } else {
                    isSplashSkipStatus = 2;
                }
            } catch (Exception e) {
                isSplashSkipStatus = 2;
            }
            init();
        } else {

            if (AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.DBQuizPrefs.TOGGLING_CONTEST_ACTIVE, false)) {
                haveToStopForDBQuiz = true;
                retryIfFailure(false);
            } else {
                /**
                 * Check Any data is available or not in Sqlite
                 */
                // Fetching Common table data  mdb
                CommonListTable commonListTable = (CommonListTable) DatabaseHelper.getInstance(SplashActivity.this).getTableObject(CommonListTable.TABLE_NAME);

                if (!isFromPush && (commonListTable.getNewsListCount() > 0)) {
                    isAppControlFetchDone = true;
                    isSplashSkipStatus = 2;
                    init();
                } else {
                    retryIfFailure(false);
                }
            }
        }
    }

    /**
     * If data is not available show retry option
     */
    public void retryIfFailure(boolean isVersionFail) {
        if (isVersionFail) {
            AppUtils.getInstance().showCustomToast(SplashActivity.this, getString(R.string.sorry_error_found_please_try_again_));
        } else {
            AppUtils.getInstance().showCustomToast(SplashActivity.this, getString(R.string.internet_connection_error_));
        }
        findViewById(R.id.retry).setVisibility(View.VISIBLE);
        findViewById(R.id.retry).setOnClickListener(view -> {
            findViewById(R.id.retry).setVisibility(View.GONE);
            preInit();
        });
    }

    private void setAppDefaults() {
        checkForCampaignValue();
        //   Tracking
        String locationUrl = TrackingHost.getInstance(this).getLocationUrl(AppUrls.LOCATION_URL);
        String notiyToggleUrl = TrackingHost.getInstance(this).getToggleNotifyUrl(AppUrls.TOGGLE_NOTIFY_URL);
        String sessionUrl = TrackingHost.getInstance(this).getSesstionUrl(AppUrls.SESSION_URL);
        String getAppsUrl = AppPreferences.getInstance(this).getStringValue(QuickPreferences.GET_APPS_URL, "");
        String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, QuickPreferences.UTM_DIRECT);
        String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, QuickPreferences.UTM_DIRECT);
        String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);

        @ATracker.ToggleStatus int rashifalStatus = AppPreferences.getInstance(this).getIntValue(QuickPreferences.RASHIFAL_STATUS, ATracker.ToggleStatus.OFF);
        @ATracker.ToggleStatus int notificationStatus = AppPreferences.getInstance(this).getIntValue(QuickPreferences.NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);

        boolean isBlockedCountry = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);

        Tracking.intializeTracking(this, CommonConstants.BHASKAR_APP_ID, locationUrl, notiyToggleUrl, sessionUrl, TrackingHost.getInstance(this).getWisdomAppSourceUrl(AppUrls.WISDOM_APP_SOURCE_URL), rashifalStatus, notificationStatus, CommonConstants.CHANNEL_ID, getAppsUrl, source, medium, campaign, isBlockedCountry);

        // app work
        isHitCompleted = true;
    }

    private void init() {

        if (!(isAppControlFetchDone) && !isStartActivityCall)
            return;

        if (isHitCompleted && !isFromPush && !isFromWidget)
            return;

        if (haveToStopForDBQuiz)
            return;

        if (!firstTimeJsonLoaded)
            return;

        if (isSplashSkipStatus == 0 || isSplashSkipStatus == 1)
            return;
        else if (isSplashSkipStatus == 2) {
            setAppDefaults();
            startNewActivity();
        } else {
            setAppDefaults();
            new Handler().postDelayed(() -> {
                if (isHitCompleted) {
                    startNewActivity();
                }
            }, 100);
        }
    }

    private void checkForCampaignValue() {
        final AppPreferences prefs = AppPreferences.getInstance(SplashActivity.this);
        if (prefs.getStringValue(QuickPreferences.UTM_SOURCE, "").length() == 0 || prefs.getStringValue(QuickPreferences.UTM_CAMPAIGN, "").equalsIgnoreCase(QuickPreferences.UTM_DIRECT)) {
            String source1 = prefs.getStringValue(QuickPreferences.UTM_SOURCE_TEMP, QuickPreferences.UTM_DIRECT);
            String medium1 = prefs.getStringValue(QuickPreferences.UTM_MEDIUM_TEMP, QuickPreferences.UTM_DIRECT);
            String campaign1 = prefs.getStringValue(QuickPreferences.UTM_CAMPAIGN_TEMP, QuickPreferences.UTM_DIRECT);

            prefs.setStringValue(QuickPreferences.UTM_SOURCE, source1);
            prefs.setStringValue(QuickPreferences.UTM_MEDIUM, medium1);
            prefs.setStringValue(QuickPreferences.UTM_CAMPAIGN, campaign1);
        }
    }

    private void startNewActivity() {
        if (!isStartActivityCall) {
            isStartActivityCall = true;
            //enable tutorial for only divya bhaskar
            if (!AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.SHOW_TUTORIAL, false) && AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.TUTORIAL_ENABLE, false)) {
                startActivity(AppIntroDashboardActivity.getIntent(this, ChatIntroConst.NavigationFlow.NAVIGATE_FROM_SPLASH, AppFlyerConst.GAScreen.TUTORIAL));
                finishSplash();
            } else if (isFromWidget) {
                int countValue = AppPreferences.getInstance(this).getIntValue(QuickPreferences.QuickNotificationPrefs.COUNT, 0);
                int paginationValue = AppPreferences.getInstance(this).getIntValue(QuickPreferences.QuickNotificationPrefs.PAGINATION_COUNT, 0);
                if (AppPreferences.getInstance(this).getIntValue(QuickPreferences.QUICK_READ_STATUS, 0) == 1) {
                    Intent intentSticky = new Intent(this, QuickNewsNotificationService2.class);
                    intentSticky.putExtra("count", countValue + 1);
                    intentSticky.putExtra("paginationCount", paginationValue);
                    intentSticky.setAction(Constants.QuickReadActions.ACTION_NEXT);
                    this.startService(intentSticky);
                }

                Intent intent = new Intent(this, AppUtils.getInstance().getArticleTypeClass());
                intent.putExtra(Constants.KeyPair.KEY_IS_FROM_APPWIDGET, isFromWidget);
                intent.putExtra(Constants.KeyPair.KEY_VERSION, splashBundleInfo.versionQuickNews);
                intent.putExtra(Constants.KeyPair.KEY_IS_FROM_QUICK_NOTIFICATION, isFromQuickNotification);
                intent.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, splashBundleInfo.channelId);
                intent.putExtra(Constants.KeyPair.KEY_STORY_ID, splashBundleInfo.storyId);
                intent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, splashBundleInfo.detailUrl);
                intent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, "");
                intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, splashBundleInfo.gaScreen);
                intent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, splashBundleInfo.displayName);
                intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, splashBundleInfo.gaArticle);
                intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
                intent.putExtra(Constants.KeyPair.IN_BACKGROUND, splashBundleInfo.inBackground);
                intent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, splashBundleInfo.domain);
                intent.putExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN, splashBundleInfo.subDomain);
                startActivity(intent);
                finishSplash();
            } else if (isFromPush) {
                boolean isRealtimeTrackingOn = false;
                boolean isGATrackingOn = false;
                String label = "";
                String action = "";
                if (!TextUtils.isEmpty(splashBundleInfo.catId)) {
                    switch (splashBundleInfo.catId) {
                        case FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_NEWS:
                        case FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_BUSINESS:
                        case FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_RELIGION:
                            isRealtimeTrackingOn = true;
                            isGATrackingOn = false;
                            setStartActivityFromPush();
                            break;
                        case FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_RASHIFAL:
                            isRealtimeTrackingOn = true;
                            isGATrackingOn = false;
                            setStartActivityFromPush();
                            break;
                        case FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_DB_VIDEOS:
                            isRealtimeTrackingOn = true;
                            isGATrackingOn = false;
                            setStartActivityFromPush();
                            break;
                        case FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_BRAND_AND_CATEGORY:
                            action = "Push_6";
                            isRealtimeTrackingOn = false;
                            isGATrackingOn = true;
                            Intent homeIntent = new Intent(this, MainActivity.class);
                            homeIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, splashBundleInfo.menuId);
                            startActivity(homeIntent);
                            finishSplash();
                            break;
                        case FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_URL:
                            label = splashBundleInfo.webUrl;
                            action = "Push_7";
                            isRealtimeTrackingOn = true;
                            isGATrackingOn = true;
                            setStartActivityFromPushForUrlType();
                            break;
                        case FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_DEEP_LINK_URL:
                            label = splashBundleInfo.deeplink;
                            action = "Push_8";
                            isRealtimeTrackingOn = true;
                            isGATrackingOn = true;
                            setStartActivityFromPushForDeepLinkType(splashBundleInfo.deeplink);

                        default:
                            setStartActivityFromPush();
                            break;
                    }
                    // Tracking notification click (Real Time)
                    if (isRealtimeTrackingOn) {

                        if (!isFromStackNotification || (isFromStackNotification && (Notificationutil.getNotificationsStackCount(SplashActivity.this) < 2))) {
                            int readStatus = 0;
                            try {
                                NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(SplashActivity.this).getTableObject(NotificationStackTable.TABLE_NAME);
                                readStatus = notificationStackTable.getNotificationReadStatus(splashBundleInfo.storyId);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (readStatus == 0) {
                                String eventType = "2";
                                String aurl = "";
                                String url = AppUrls.DOMAIN + AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN;

                                Tracking.trackWisdomNotification(SplashActivity.this, TrackingHost.getInstance(this).getWisdomUrlForNotification(AppUrls.WISDOM_NOTIFICATION_URL), splashBundleInfo.catId, splashBundleInfo.pCatId, splashBundleInfo.title, splashBundleInfo.newsType, eventType, splashBundleInfo.storyId, aurl, url, CommonConstants.BHASKAR_APP_ID, splashBundleInfo.channelId, AppFlyerConst.GAExtra.NOTIFICATION_WIDGET_NOTIFY, splashBundleInfo.slId);
                                Systr.println("Wisdom Notification tracking : " + splashBundleInfo.storyId);
                            }

                            NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(SplashActivity.this).getTableObject(NotificationStackTable.TABLE_NAME);
                            notificationStackTable.updateReadStatus(splashBundleInfo.storyId);
                        }
                    } else if (isGATrackingOn) {
                        // Tracking
                        final String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, action, label, campaign);
                        Systr.println("GA Events : " + AppFlyerConst.GACategory.NOTIFICATION + "_" + action + "_" + label + "_" + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));
                    }
                } else {
                    setStartActivityFromPush();
                }
            } else {
                if (!TextUtils.isEmpty(splashBundleInfo.uri)) {
//                    if (splashBundleInfo.uri.contains(getString(R.string.dynamic_link_host1)) | splashBundleInfo.uri.contains(getString(R.string.dynamic_link_host2))) {
//                        ActivityUtil.getInstance().handleDynamicLink(this, splashBundleInfo.uri);
//                        finishSplash();
//                    } else {
                    OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(this, splashBundleInfo.uri, Constants.KeyPair.KEY_NEWS_UPDATE, 0, "", null);
                    openNewsDetailInApp.setCallback(status -> {
                        //SUCCESS 1  FAILURE 2
                        finishSplash();
                    });
                    openNewsDetailInApp.openLink();

//                    }

                } else {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finishSplash();
                }
            }
        }
    }

    private void setStartActivityFromPushForDeepLinkType(String deepLinkUrl) {

        ActivityUtil.getInstance().handleDynamicLink(this, deepLinkUrl, false, object -> {
            try {
                if (object != null) {
                    HashMap<String, String> parsedMap = (HashMap<String, String>) object;
                    String extendedUrl = parsedMap.get("extended_url");
                    if (!TextUtils.isEmpty(extendedUrl)) {
                        assert extendedUrl != null;
                        OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(SplashActivity.this, extendedUrl, Constants.KeyPair.KEY_NEWS_UPDATE, 0, "", SplashActivity.this);
                        openNewsDetailInApp.openLink();
                        finishSplash();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent);
                        finishSplash();
                    }
                } else {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finishSplash();
                }
            } catch (Exception e) {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finishSplash();
            }

        });


    }

    private void setStartActivityFromPushForUrlType() {
        if (!TextUtils.isEmpty(splashBundleInfo.isWapExt) && splashBundleInfo.isWapExt.equalsIgnoreCase("1")) {
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, isFromPush);
            intent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, clickViaNotification);
            intent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, Urls.DEFAULT_DETAIL_URL);
            intent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, splashBundleInfo.title);
            intent.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, splashBundleInfo.channelId);
            intent.putExtra(Constants.KeyPair.KEY_STORY_ID, splashBundleInfo.storyId);
            intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, splashBundleInfo.catId);
            intent.putExtra(Constants.KeyPair.KEY_NEWS_TYPE, splashBundleInfo.newsType);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, splashBundleInfo.gaScreen);
            intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, splashBundleInfo.gaArticle);
            intent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, splashBundleInfo.domain);
            intent.putExtra(Constants.KeyPair.KEY_WEB_URL, splashBundleInfo.webUrl);
            startActivity(intent);

            //Breaking News Alert
            if (!TextUtils.isEmpty(splashBundleInfo.title) && !TextUtils.isEmpty(splashBundleInfo.title.trim())) {
                final Intent dialogIntent = new Intent(SplashActivity.this, NotificationDialogActivity.class);
                dialogIntent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, isFromPush);
                dialogIntent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, clickViaNotification);
                dialogIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, Urls.DEFAULT_DETAIL_URL);
                dialogIntent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, splashBundleInfo.title);
                dialogIntent.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, splashBundleInfo.channelId);
                dialogIntent.putExtra(Constants.KeyPair.KEY_STORY_ID, splashBundleInfo.storyId);
                dialogIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, splashBundleInfo.catId);
                dialogIntent.putExtra(Constants.KeyPair.KEY_NEWS_TYPE, splashBundleInfo.newsType);
                dialogIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, splashBundleInfo.gaScreen);
                dialogIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, splashBundleInfo.gaArticle);
                dialogIntent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, splashBundleInfo.displayName);
                dialogIntent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, splashBundleInfo.domain);
                dialogIntent.putExtra(Constants.KeyPair.KEY_IS_WAP_EXT, splashBundleInfo.isWapExt);
                dialogIntent.putExtra(Constants.KeyPair.KEY_WEB_URL, splashBundleInfo.webUrl);
                Constants.BREAKING_NEWS = Constants.KeyPair.KEY_VISIBLE;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(dialogIntent);
                    }

                }, 200);
            }
            finishSplash();
        } else if (!TextUtils.isEmpty(splashBundleInfo.isWapExt) && splashBundleInfo.isWapExt.equalsIgnoreCase("0")) {
            //implement urlv3info
            if (splashBundleInfo.webUrl.contains(getString(R.string.dynamic_link_host1)) || splashBundleInfo.webUrl.contains(getString(R.string.dynamic_link_host2))) {
                setStartActivityFromPushForDeepLinkType(splashBundleInfo.webUrl);
            } else {
                OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(SplashActivity.this, splashBundleInfo.webUrl, splashBundleInfo.webTitle, 0, "", SplashActivity.this);
                openNewsDetailInApp.openLink();
                finishSplash();
            }
        }
    }

    private void setStartActivityFromPush() {
        if (TextUtils.isEmpty(splashBundleInfo.storyId)) {
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, isFromPush);
            intent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, clickViaNotification);
            intent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, Urls.DEFAULT_DETAIL_URL);
            intent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, splashBundleInfo.title);
            intent.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, splashBundleInfo.channelId);
            intent.putExtra(Constants.KeyPair.KEY_STORY_ID, splashBundleInfo.storyId);
            intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, splashBundleInfo.catId);
            intent.putExtra(Constants.KeyPair.KEY_NEWS_TYPE, splashBundleInfo.newsType);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, splashBundleInfo.gaScreen);
            intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, splashBundleInfo.gaArticle);
            intent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, splashBundleInfo.domain);
            intent.putExtra(Constants.KeyPair.KEY_WEB_URL, splashBundleInfo.webUrl);
            startActivity(intent);

            //Breaking News Alert
            if (!TextUtils.isEmpty(splashBundleInfo.title) && !TextUtils.isEmpty(splashBundleInfo.title.trim())) {
                final Intent dialogIntent = new Intent(SplashActivity.this, NotificationDialogActivity.class);
                dialogIntent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, isFromPush);
                dialogIntent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, clickViaNotification);
                dialogIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, Urls.DEFAULT_DETAIL_URL);
                dialogIntent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, splashBundleInfo.title);
                dialogIntent.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, splashBundleInfo.channelId);
                dialogIntent.putExtra(Constants.KeyPair.KEY_STORY_ID, splashBundleInfo.storyId);
                dialogIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, splashBundleInfo.catId);
                dialogIntent.putExtra(Constants.KeyPair.KEY_NEWS_TYPE, splashBundleInfo.newsType);
                dialogIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, splashBundleInfo.gaScreen);
                dialogIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, splashBundleInfo.gaArticle);
                dialogIntent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, splashBundleInfo.displayName);
                dialogIntent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, splashBundleInfo.domain);
                dialogIntent.putExtra(Constants.KeyPair.KEY_IS_WAP_EXT, splashBundleInfo.isWapExt);
                dialogIntent.putExtra(Constants.KeyPair.KEY_WEB_URL, splashBundleInfo.webUrl);
                Constants.BREAKING_NEWS = Constants.KeyPair.KEY_VISIBLE;
                new Handler().postDelayed(() -> startActivity(dialogIntent), 200);
            }
            finishSplash();
        } else {

            if (splashBundleInfo.catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_DB_VIDEOS) && isVideoNotification) {


                if (videoInfo.portraitVideo == 1) {
                    Intent intent = DivyaPartraitVideoActivity.getIntent(this, videoInfo, vidoeSectionLabel, AppFlyerConst.DBVideosSource.VIDEO_LIST, 1, gaEventLabel, splashBundleInfo.gaArticle);
                    intent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, clickViaNotification);
                    startActivity(intent);
                } else {
                    ArrayList<VideoInfo> videoList = new ArrayList<>();
                    videoList.add(videoInfo);

                    Intent intent = VideoPlayerActivity.getIntent(this, 0, 1, videoFeedUrl, AppFlyerConst.DBVideosSource.VIDEO_LIST, gaEventLabel, splashBundleInfo.gaArticle, vidoeSectionLabel, "", VideoPlayerListFragment.TYPE_LIST, VideoPlayerListFragment.THEME_LIGHT);
                    VideoPlayerActivity.addListData(videoList);
                    intent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, clickViaNotification);
                    startActivity(intent);
                }
                finishSplash();
            } else {
                // get the notifications stack from shared preferences
                if (isFromStackNotification && (Notificationutil.getNotificationsStackCount(SplashActivity.this) > 1)) {
                    Intent intent = new Intent(SplashActivity.this, AppNotificationActivity.class);
                    intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, isFromPush);
                    intent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, clickViaNotification);
                    intent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, Urls.DEFAULT_DETAIL_URL);
                    intent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, splashBundleInfo.title);
                    intent.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, splashBundleInfo.channelId);
                    intent.putExtra(Constants.KeyPair.KEY_STORY_ID, splashBundleInfo.storyId);
                    intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, splashBundleInfo.catId);
                    intent.putExtra(Constants.KeyPair.KEY_NEWS_TYPE, splashBundleInfo.newsType);
                    intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, splashBundleInfo.gaScreen);
                    intent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, splashBundleInfo.displayName);
                    intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, splashBundleInfo.gaArticle);
                    intent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, splashBundleInfo.domain);
                    intent.putExtra(Constants.KeyPair.LAUNCH_FROM_STACK_NOTIFICATION, true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finishSplash();

                } else {
                    Intent intent = DivyaCommonArticleActivity.getIntent(SplashActivity.this, splashBundleInfo.storyId, "", splashBundleInfo.gaArticle, splashBundleInfo.gaScreen, splashBundleInfo.displayName, gaEventLabel, "");
                    intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, isFromPush);
                    intent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, clickViaNotification);
                    intent.putExtra(Constants.KeyPair.IN_BACKGROUND, splashBundleInfo.inBackground);
                    intent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, splashBundleInfo.domain);
                    intent.putExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN, splashBundleInfo.subDomain);
                    startActivity(intent);
                    finishSplash();
                }
            }
        }
    }

    private void finishSplash() {
        TrackingData.setGoogleAdvertisingId(this);
        TrackingHost.getInstance(this).setIsBlockedCountry(AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true));

        if (finishSplashHandler != null && finishRunnable != null)
            finishSplashHandler.postDelayed(finishRunnable, 500);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*
        Debug.stopMethodTracing();
        ***/
//        unRegisterReciever();
        Constants.splashDataInfo = null;
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void responseFromAppControlService(AppControlEventWrapper eventWrapper) {
        int apiHitValue = eventWrapper.getApiHitValue();
        boolean apiSuccess = eventWrapper.getFlag();
        switch (apiHitValue) {
            case 100:
                boolean isVersionFail = AppPreferences.getInstance(SplashActivity.this).getBooleanValue(QuickPreferences.GdprConst.IS_FAIL_VERSION, false);
                if (!isVersionFail) {
                    isAppControlFetchDone = true;
                    handleGDPRPrivacyAndAppStart();
                }
                break;
            case Constants.AppHitChoice.APP_VERSION_API:
                if (apiSuccess) {
                    isAppControlFetchDone = true;
                    handleGDPRPrivacyAndAppStart();
                } else {
                    retryIfFailure(true);
                }
                break;
        }
    }

    public void closeOnPauseOBDrawer() {
        final int optionsHeight = (splashActionLayout != null) ? splashActionLayout.getHeight() : 0;
        if (optionsHeight != 0) {
            isOptionDrawerOpen = false;
            openArrow.setImageResource(R.drawable.sc_arrow_up);
            TranslateAnimation anim = new TranslateAnimation(0, 0, 0, optionsHeight);
            anim.setDuration(1);
            anim.setFillAfter(true);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    splashActionLayout.setVisibility(View.GONE);
//                findViewById(R.id.splash_event_click).setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            splashActionLayout.startAnimation(anim);
        }
        if (drawerCloser != null && drawerCloserRunnable != null)
            drawerCloser.removeCallbacks(drawerCloserRunnable);
    }

    @Override
    protected void onPause() {
        if (dbSplashTimeTicker != null)
            dbSplashTimeTicker.onPause();
        closeOnPauseOBDrawer();
        super.onPause();

        if (mDeviceBandwidthSampler != null)
            mDeviceBandwidthSampler.stopSampling();

        if (AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.HOME_PAGINATION_ENABLE, false)) {
            ConnectionQuality mConnectionClass = mConnectionClassManager.getCurrentBandwidthQuality();
            switch (mConnectionClass) {
                case POOR:
                    Systr.println("HOME PAGE : set network 2g 3g");
                    AppPreferences.getInstance(this).setIntValue(QuickPreferences.FeedData.NETWORK_TYPE, NetworkStatus.NetworkType.NETWORK_2G_3G);
                    break;
                case MODERATE:
                    Systr.println("HOME PAGE : set network 4g");
                    AppPreferences.getInstance(this).setIntValue(QuickPreferences.FeedData.NETWORK_TYPE, NetworkStatus.NetworkType.NETWORK_4G);
                    break;
                case GOOD:
                case EXCELLENT:
                    Systr.println("HOME PAGE : set network wifi");
                    AppPreferences.getInstance(this).setIntValue(QuickPreferences.FeedData.NETWORK_TYPE, NetworkStatus.NetworkType.NETWORK_WIFI);
                    break;

                case UNKNOWN:
                    Systr.println("HOME PAGE : set network unknown");
                    AppPreferences.getInstance(this).setIntValue(QuickPreferences.FeedData.NETWORK_TYPE, NetworkStatus.getNetworkType(this));
                    break;
            }
        } else {
            Systr.println("HOME PAGE : set network wifi ... pagination off");
            AppPreferences.getInstance(this).setIntValue(QuickPreferences.FeedData.NETWORK_TYPE, NetworkStatus.NetworkType.NETWORK_WIFI);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dbSplashTimeTicker != null)
            dbSplashTimeTicker.onResume();
    }

    public void startTickTacTimer() {
        dbSplashTimeTicker = findViewById(R.id.tick_tack_timer);
        dbSplashTimeTicker.setVisibility(View.VISIBLE);
        dbSplashTimeTicker.setCallback(new DBSplashTimeTicker.CountDownCallback() {
            @Override
            public void onTick(long millisUntilFinished) {
                //Do not write here anything text is being set from inside the time ticker.
            }

            @Override
            public void onFinish() {
                onTimerFinished();
            }
        });
        long seconds = 5;
        if (Constants.splashDataInfo != null && !TextUtils.isEmpty(Constants.splashDataInfo.splashTime)) {
            seconds = Integer.parseInt(Constants.splashDataInfo.splashTime);
        }
        if (splashType == Constants.SplashType.SPLASH_TYPE_VIDEO)
            dbSplashTimeTicker.setTimerFinished();
        else
            dbSplashTimeTicker.setValuesAndStartTimer(seconds * 1000L, 1000L, 100L);

        setSkiTimerClickable();
    }

    public void onTimerFinished() {
        if (splashType != Constants.SplashType.SPLASH_TYPE_VIDEO) {
            if (splashFragmentCallbackListener != null)
                splashFragmentCallbackListener.stopFunctionality();
            isSplashSkipStatus = 2;
            init();
        }
    }

    public void setSkiTimerClickable() {
        dbSplashTimeTicker.setOnClickListener(view -> {
            isSplashSkipStatus = 2;
            dbSplashTimeTicker.stop();
            init();
            String splashId = "";
            if (Constants.splashDataInfo != null)
                splashId = Constants.splashDataInfo.splashId;
            String campaign = AppPreferences.getInstance(SplashActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            Tracking.trackGAEvent(SplashActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.SPLASH, AppFlyerConst.GAAction.SKIP, splashId, campaign);
        });
    }

    public void readMoreClicked() {
        isSplashSkipStatus = 2;
        setAppDefaults();
        if (Constants.splashDataInfo != null) {

            OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(SplashActivity.this, Constants.splashDataInfo.splashActionUrl, Constants.splashDataInfo.action_url_name, 0, "Splash_" + Constants.splashDataInfo.splashId + "_" + Constants.splashDataInfo.splashActionUrl, SplashActivity.this);
            openNewsDetailInApp.openLink();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initSplashActions() {
        findViewById(R.id.splash_featured_layout).setVisibility(View.GONE);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
//        width = size.x;
//        height = size.y;
        //init view
        splashActionLayout = findViewById(R.id.splash_action_layout);
        openArrow = findViewById(R.id.open_options_arrow);
        openArrow.setVisibility(View.VISIBLE);
        openArrow.setOnClickListener(v -> {
            if (isOptionDrawerOpen) {
                closeOBDrawer();
                if (dbSplashTimeTicker != null)
                    dbSplashTimeTicker.onResume();
                isOptionDrawerOpen = false;
                if (drawerCloser != null)
                    drawerCloser.removeCallbacks(drawerCloserRunnable);
            } else {
                openOBDrawer();
                isOptionDrawerOpen = true;
                if (dbSplashTimeTicker != null)
                    dbSplashTimeTicker.onPause();
                drawerCloser.removeCallbacks(drawerCloserRunnable);
                drawerCloser.postDelayed(drawerCloserRunnable, afterDurationClose);
            }
        });
        View viewById = findViewById(R.id.tap_listener);
        viewById.setOnTouchListener((v, event) -> {
            if (touchType != event.getAction()) {
                touchType = event.getAction();
                if (touchType == MotionEvent.ACTION_DOWN) {
                    if (isOptionDrawerOpen) {
                        closeOBDrawer();
                        if (dbSplashTimeTicker != null)
                            dbSplashTimeTicker.onResume();
                        isOptionDrawerOpen = false;
                    } else {
                        if (dbSplashTimeTicker != null)
                            dbSplashTimeTicker.onPause();
                        if (Constants.splashDataInfo != null && Constants.splashDataInfo.isBottomActionButtons() && !isOptionDrawerOpen) {
                            isOptionDrawerOpen = true;
                            openOBDrawer();
                        }
                    }
                    if (drawerCloser != null)
                        drawerCloser.removeCallbacks(drawerCloserRunnable);
                } else if (touchType == MotionEvent.ACTION_UP) {
                    if (Constants.splashDataInfo != null && !Constants.splashDataInfo.isBottomActionButtons()) {
                        if (dbSplashTimeTicker != null)
                            dbSplashTimeTicker.onResume();
                    } else {
                        if (isLongPress) {
                            closeOBDrawer();
                            if (dbSplashTimeTicker != null)
                                dbSplashTimeTicker.onResume();
                            if (drawerCloser != null)
                                drawerCloser.removeCallbacks(drawerCloserRunnable);
                            isLongPress = false;
                        } else if (isOptionDrawerOpen && drawerCloser != null) {
                            drawerCloser.removeCallbacks(drawerCloserRunnable);
                            drawerCloser.postDelayed(drawerCloserRunnable, afterDurationClose);
                        }
                    }
                }
            }
            return false;
        });
        viewById.setOnLongClickListener(v -> {
            isLongPress = true;
            return false;
        });

        View viewById1 = findViewById(R.id.bottom_action_parent);
        if (Constants.splashDataInfo != null && !Constants.splashDataInfo.isBottomActionButtons())
            viewById1.setVisibility(View.GONE);
    }

    public void openOBDrawer() {
        final int optionsHeight = splashActionLayout.getHeight();
        isOptionDrawerOpen = true;
        openArrow.setImageResource(R.drawable.sc_arrow_down);
        TranslateAnimation anim = new TranslateAnimation(0, 0, optionsHeight, 0);
        anim.setDuration(500);
        anim.setFillAfter(true);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                splashActionLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        splashActionLayout.startAnimation(anim);
    }

    public void closeOBDrawer() {
        final int optionsHeight = splashActionLayout.getHeight();
        isOptionDrawerOpen = false;
        openArrow.setImageResource(R.drawable.sc_arrow_up);
        TranslateAnimation anim = new TranslateAnimation(0, 0, 0, optionsHeight);
        anim.setDuration(500);
        anim.setFillAfter(true);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                splashActionLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        splashActionLayout.startAnimation(anim);
    }

    private void setSoundDrawable(boolean sound) {
        if (sound) {
            mute_volume.setImageResource(R.drawable.volume);
            AppPreferences.getInstance(this).setStringValue(QuickPreferences.SPLASH_VIDEO_SOUND, "ON");
        } else {
            mute_volume.setImageResource(R.drawable.mute_volume);
            AppPreferences.getInstance(this).setStringValue(QuickPreferences.SPLASH_VIDEO_SOUND, "OFF");
        }
        if (splashVideoFragment != null)
            splashVideoFragment.setVolume(sound);
    }

    public void muteGAEvent() {
        String splashId = "";
        if (Constants.splashDataInfo != null)
            splashId = Constants.splashDataInfo.splashId;
        String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAEvent(this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.SPLASH, AppFlyerConst.GAAction.MUTE, splashId, campaign);
    }

    public void initMuteIcon() {
        mute_volume = findViewById(R.id.volume);
        mute_volume.setVisibility(View.VISIBLE);
        String isSoundText = AppPreferences.getInstance(this).getStringValue(QuickPreferences.SPLASH_VIDEO_SOUND, null);
        soundOnOff = isSoundText == null || isSoundText.equalsIgnoreCase("ON");
        setSoundDrawable(soundOnOff);

        mute_volume.setOnClickListener(v -> {
            muteGAEvent();
            if (soundOnOff) {
                soundOnOff = false;
                setSoundDrawable(soundOnOff);
            } else {
                soundOnOff = true;
                setSoundDrawable(soundOnOff);
            }
        });
    }

    private void sendSplashActionToServer(final int actionType) {
        try {
            String url = AppPreferences.getInstance(SplashActivity.this).getStringValue(QuickPreferences.JsonPreference.ARTICLE_ACTION_URL, null);
            JSONObject jsonObject = new JSONObject();
            try {
                String mSessionId = TrackingData.getDbORDeviceId(SplashActivity.this);
                jsonObject.put("session_id", mSessionId);
                jsonObject.put("storyid", Constants.splashDataInfo != null ? Constants.splashDataInfo.splashId : "");
                jsonObject.put("host", AppPreferences.getInstance(SplashActivity.this).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN));
                jsonObject.put("channel_slno", CommonConstants.CHANNEL_ID);
                jsonObject.put("storyType", Constants.splashDataInfo != null ? Constants.splashDataInfo.splashType : "");
                jsonObject.put("application_id", CommonConstants.BHASKAR_APP_ID);
                jsonObject.put("action_for", "splash");
                switch (actionType) {
                    case ACTION_LIKE:
                        jsonObject.put("action", "like");
                        break;
                    case ACTION_UNLIKE:
                        jsonObject.put("action", "unlike");
                        break;
                    case ACTION_SHARE:
                        jsonObject.put("action", "share");
                        break;
                    case ACTION_READ_MORE:
                        jsonObject.put("action", "read_more");
                        break;
                }
            } catch (JSONException ignored) {
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    response -> parseActionData(response, actionType), error -> parseActionData(null, actionType)) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(SplashActivity.this).addToRequestQueue(request);
        } catch (Exception ignored) {
        }
    }

    private void parseActionData(JSONObject response, final int actionType) {
        if (response != null) {
            String status = response.optString("status");
            Systr.println("Action Response : " + status);

            assert status != null;
            if (!TextUtils.isEmpty(status) && status.equals("1")) {
                switch (actionType) {
                    case ACTION_LIKE:
                        if (Constants.splashDataInfo != null) {
                            int count = 0;
                            try {
                                count = Integer.parseInt(Constants.splashDataInfo.splashLikeCount);
                            } catch (Exception ignored) {
                            }
                            Constants.splashDataInfo.splashLikeCount = String.valueOf(count + 1);
                            Systr.println("inset like : " + Constants.splashDataInfo.splashId);
                            booleanExpressionTable.updateBooleanValue(Constants.splashDataInfo.splashId, Constants.BooleanConst.SPLASH_CONTENT_LIKE, true);
                        }
                        isLikeProcessing = false;
                        break;
                    case ACTION_UNLIKE:
                        if (Constants.splashDataInfo != null) {
                            int count = 0;
                            try {
                                count = Integer.parseInt(Constants.splashDataInfo.splashDislikeCount);
                            } catch (Exception ignored) {
                            }
                            Constants.splashDataInfo.splashDislikeCount = String.valueOf(count + 1);
                            Systr.println("inset unlike : " + Constants.splashDataInfo.splashId);
                            booleanExpressionTable.updateBooleanValue(Constants.splashDataInfo.splashId, Constants.BooleanConst.SPLASH_CONTENT_DISLIKE, true);
                        }
                        isLikeProcessing = false;
                        break;
                    case ACTION_SHARE:
                        isShareProcessing = false;
                        if (Constants.splashDataInfo != null)
                            booleanExpressionTable.updateBooleanValue(Constants.splashDataInfo.splashId, Constants.BooleanConst.SPLASH_CONTENT_SHARE, true);
                        break;
                }

                runOnUiThread(this::updateSplashActionView);
            } else {
                switch (actionType) {
                    case ACTION_LIKE:
                    case ACTION_UNLIKE:
                        isLikeProcessing = false;
                        runOnUiThread(() -> AppUtils.getInstance().showCustomToast(SplashActivity.this, getResources().getString(R.string.sorry_error_found_please_try_again_)));
                        break;
                    case ACTION_SHARE:
                        isShareProcessing = false;
                        break;
                }
                init();
            }
        }

    }

    private void updateSplashActionView() {
        if (Constants.splashDataInfo != null) {
            boolean aBoolean = booleanExpressionTable.isBoolean(Constants.splashDataInfo.splashId, Constants.BooleanConst.SPLASH_CONTENT_LIKE);
            boolean aBoolean1 = booleanExpressionTable.isBoolean(Constants.splashDataInfo.splashId, Constants.BooleanConst.SPLASH_CONTENT_DISLIKE);
            boolean aBoolean2 = booleanExpressionTable.isBoolean(Constants.splashDataInfo.splashId, Constants.BooleanConst.SPLASH_CONTENT_SHARE);
            boolean aBoolean3 = booleanExpressionTable.isBoolean(Constants.splashDataInfo.splashId, Constants.BooleanConst.SPLASH_CONTENT_READ_MORE);

            //Dislike
            if (aBoolean1) {
                ImageView image = findViewById(R.id.iv_1);
                TextView text = findViewById(R.id.tv_1);
                image.setColorFilter(getResources().getColor(R.color.themeBlueColor));
                text.setTextColor(getResources().getColor(R.color.themeBlueColor));
            }

            //Like
            if (aBoolean) {
                ImageView image = findViewById(R.id.iv_2);
                TextView text = findViewById(R.id.tv_2);
                image.setColorFilter(getResources().getColor(R.color.themeBlueColor));
                text.setTextColor(getResources().getColor(R.color.themeBlueColor));
            }

            ///Share
            if (aBoolean2) {
                ImageView image = findViewById(R.id.iv_3);
                TextView text = findViewById(R.id.tv_3);
                image.setColorFilter(getResources().getColor(R.color.themeBlueColor));
                text.setTextColor(getResources().getColor(R.color.themeBlueColor));
            }

            if (aBoolean3) {
                ImageView image = findViewById(R.id.iv_4);
                TextView text = findViewById(R.id.tv_4);
                image.setColorFilter(getResources().getColor(R.color.themeBlueColor));
                text.setTextColor(getResources().getColor(R.color.themeBlueColor));
            }
        }
    }

    public void initOptionViews() {
        View.OnClickListener clickListener = view -> {
            String splashId = "";
            if (Constants.splashDataInfo != null)
                splashId = Constants.splashDataInfo.splashId;
            String campaign = AppPreferences.getInstance(SplashActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            switch (view.getId()) {
                case R.id.dislike_button:
                    if (!isLikeProcessing) {
                        isLikeProcessing = true;
                        sendSplashActionToServer(ACTION_UNLIKE);
                    }
                    Tracking.trackGAEvent(SplashActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.SPLASH, AppFlyerConst.GAAction.DISLIKE, splashId, campaign);
                    break;
                case R.id.like_button:
                    if (!isLikeProcessing) {
                        isLikeProcessing = true;
                        sendSplashActionToServer(ACTION_LIKE);
                    }
                    Tracking.trackGAEvent(SplashActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.SPLASH, AppFlyerConst.GAAction.LIKE, splashId, campaign);
                    break;
                case R.id.share_button:
                    if (!isShareProcessing) {
                        isShareProcessing = true;
                        sendSplashActionToServer(ACTION_SHARE);
                        AppUtils.shareApp(SplashActivity.this);
                    }
                    Tracking.trackGAEvent(SplashActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.SPLASH, AppFlyerConst.GAAction.SHARE, splashId, campaign);
                    break;
                case R.id.read_more_button:
                    if (!isReadMoreProcessing) {
                        isReadMoreProcessing = true;
                        readMoreClicked();

                    }
                    Tracking.trackGAEvent(SplashActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.SPLASH, AppFlyerConst.GAAction.CLICK, splashId, campaign);
                    break;
            }
        };

        View dislikeButton = findViewById(R.id.dislike_button);
        dislikeButton.setOnClickListener(clickListener);

        View likeButton = findViewById(R.id.like_button);
        likeButton.setOnClickListener(clickListener);

        View shareButton = findViewById(R.id.share_button);
        shareButton.setOnClickListener(clickListener);

        View readMore = findViewById(R.id.read_more_button);
        TextView readMoreText = findViewById(R.id.tv_4);
        if (readMoreText != null && Constants.splashDataInfo != null) {
            readMoreText.setText(Constants.splashDataInfo.action_text);
        }
        if (Constants.splashDataInfo != null && (Constants.splashDataInfo.splashActionUrl == null || TextUtils.isEmpty(Constants.splashDataInfo.splashActionUrl))) {
            readMore.setVisibility(View.GONE);
        }
        readMore.setOnClickListener(clickListener);
        if (isFromPush || isFromStackNotification || isFromWidget)
            readMore.setVisibility(View.GONE);
        updateSplashActionView();
    }

    private void resetSplash() {
        if (Constants.splashDataInfo != null) {
            initSplashActions();
            try {
                splashType = Integer.parseInt(Constants.splashDataInfo.splashType);
                imgSplashBg.setVisibility(View.VISIBLE);
                Fragment fragment = null;
                switch (splashType) {
                    case Constants.SplashType.SPLASH_TYPE_IMAGE:
                        fragment = SplashImageFragment.getInstance();
                        break;
                    case Constants.SplashType.SPLASH_TYPE_HTML:
                        fragment = SplashWapFragment.getInstance();
                        break;
                    case Constants.SplashType.SPLASH_TYPE_GIF:
                        fragment = SplashGifFragment.getInstance();
                        break;
                    case Constants.SplashType.SPLASH_TYPE_VIDEO:
                        splashVideoFragment = SplashVideoFragment.getInstance(Constants.splashDataInfo.splashUrl);
                        fragment = splashVideoFragment;
                        break;
                }

                if (!isFinishing() && fragment != null) {
                    splashFragmentCallbackListener = ((SplashFragmentCallbackListener) fragment);
                    splashFragmentCallbackListener.setCallBackListener(splashCallbackListener);
                    findViewById(R.id.splash_frame_layout).setVisibility(View.VISIBLE);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.splash_fade_in, R.anim.splash_fade_out);
                    transaction.replace(R.id.splash_frame_layout, fragment);
                    transaction.commitAllowingStateLoss();
                    initOptionViews();
                }
            } catch (Exception ignored) {
            }
        } else {
            isSplashSkipStatus = 3;
            imgSplashBg.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ((InitApplication) getApplication()).setApplicationFont();
    }

    private void registerBroadCastReciever() {
    }

    @Override
    public void onDataFetch(boolean flag) {
        finishSplash();
    }

    @Override
    public void onDataFetch(boolean flag, String sectionName) {

    }

    @Override
    public void onDataFetch(boolean flag, int viewType) {

    }

    @Override
    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {

    }

    private void handleGDPRPrivacyAndAppStart() {
        boolean isGdprAccepted = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.GDPR_CONSENT_ACCEPTED, false);
        boolean isGdprBlockCountry = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);

        if (isGdprBlockCountry) {
            FacebookSdk.setAutoLogAppEventsEnabled(false);
        } else {
            FacebookSdk.setAutoLogAppEventsEnabled(true);
        }

        if (!isGdprAccepted && isGdprBlockCountry) {
            showPrivacyPolicyAlertDialog();
        } else {
            afterOnCreate();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class FirstTimeJsonLoadAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            JsonParser.getInstance().storeDefaultJson(SplashActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            firstTimeJsonLoaded = true;
            preInit();
        }
    }
}
