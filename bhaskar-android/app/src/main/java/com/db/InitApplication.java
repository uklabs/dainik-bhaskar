package com.db;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;
import androidx.room.Room;

import com.bhaskar.BuildConfig;
import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.util.FontsOverride;
import com.bhaskar.util.LoginController;
import com.crashlytics.android.Crashlytics;
import com.bhaskar.data.local.db.AppDatabase;
import com.db.home.RadioService;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.DbSharedPref;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.database.DatabaseProvider;
import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.offline.ActionFileUpgradeUtil;
import com.google.android.exoplayer2.offline.DefaultDownloadIndex;
import com.google.android.exoplayer2.offline.DefaultDownloaderFactory;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.analytics.Tracker;
import com.google.android.play.core.splitcompat.SplitCompat;
import com.google.android.play.core.splitcompat.SplitCompatApplication;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import io.fabric.sdk.android.Fabric;

import static com.db.util.Constants.KeyPair.KEY_DATA;


public class InitApplication extends SplitCompatApplication {

    private static InitApplication singleton;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public boolean isVideoPlay = false;
    //    private Tracker mTracker;
//    private GoogleAnalytics mGaInstance;
    private int isAppOpen = 0;
    private boolean wentInBackground = false;
    private String currentActivity = "";
    private String prevActivity = "";

    private AppDatabase appDatabase;


    public static InitApplication getInstance() {
        return singleton;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
        SplitCompat.install(this);
    }

    @Override
    public void onCreate() {
        CleverTapDB.getInstance(this).registerLifeCycle(this);

        userAgent = Util.getUserAgent(this, "ExoPlayerDemo");

        super.onCreate();
        singleton = InitApplication.this;

        appDatabase = Room.databaseBuilder(this, AppDatabase.class, "DivyaDatabase").fallbackToDestructiveMigration().allowMainThreadQueries().build();

        // Register activity lifecycle
        registerActivityCallback();

        // ssl certificate
//        initStetho();

        // Font initialization
        setApplicationFont();

        boolean isBlockedCountry = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);

        // Google Analytics
//        mGaInstance = GoogleAnalytics.getInstance(this);

        if (!isBlockedCountry) {
            Fabric.with(this, new Crashlytics());
        }

        // Strict policy
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                builder.detectFileUriExposure();
            }
        } catch (Exception ignored) {
        }

        //Initialize Login module
        LoginController.loginController().initializeModelManager(this, getResources().getString(R.string.default_web_client_id), InitApplication.getInstance().getDefaultTracker());
    }

    public AppDatabase getAppDatabase() {
        return appDatabase;
    }

//    private void initStetho() {
//        if (Urls.IS_TESTING) {
//            Stetho.initializeWithDefaults(this);
//        }
//    }

    synchronized public Tracker getDefaultTracker() {
//        if (mTracker == null) {
//            if (mGaInstance != null) {
//                String gaId = TrackingHost.getInstance(this).getGoogleAnalyticsId(AppPreferences.getInstance(this).getStringValue(QuickPreferences.GA_UAID, getResources().getString(R.string.ga_analytics_id)));
//                mGaInstance.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
//                mGaInstance.setLocalDispatchPeriod(30);
//
//                mTracker = mGaInstance.newTracker(gaId);
//                mTracker.enableAutoActivityTracking(false);
//                mTracker.enableAdvertisingIdCollection(true);
//            }
//        }
//
//        if (mTracker != null) {
//            if (AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true)
//                    && !AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.GA_CONSENT, false)) {
//                mTracker.setAnonymizeIp(true);
//            } else {
//                mTracker.setAnonymizeIp(false);
//            }
//        }
//
//        return mTracker;

        return null;
    }


    public void setTrackerValuesForGA() {
        Tracker defaultTracker = getDefaultTracker();
        if (defaultTracker != null) {
            defaultTracker.set("&cd1", TrackingData.getDbORDeviceId(this));
            defaultTracker.set("&cd2", TrackingData.getAppInstallDate(this));
            defaultTracker.set("&cd3", TrackingData.getFirstInstallDate(this));

            String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, "direct");
            String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, "direct");
            String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "direct");
            String value = "S:" + source + ";M:" + medium + ";C:" + campaign;
            defaultTracker.set("&cd4", value);
        }
    }

    public int isAppOpen() {
        return isAppOpen;
    }

    public boolean isWentInBackground() {
        return wentInBackground;
    }

    private int activityReferences = 0;

    private void registerActivityCallback() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

            public void onActivityCreated(Activity activity, Bundle bundle) {
                isAppOpen = 1;
            }

            public void onActivityDestroyed(Activity activity) {
                if (!prevActivity.equals(currentActivity) && currentActivity.equals(activity.getLocalClassName())) {
                    isAppOpen = 0;
                    wentInBackground = false;
                }

                if (activity.getLocalClassName().contains("MainActivity")) {
                    try {
                        Intent intent1 = new Intent(InitApplication.this, RadioService.class);
                        intent1.putExtra("stop", true);
                        stopService(intent1);
                    } catch (Exception ignored) {

                    }
                }

                if (activityReferences == 0) {
                    DbSharedPref.Companion.setPromoWidgetVisibilityStatus(InitApplication.this,true);
                }
            }

            public void onActivityPaused(Activity activity) {
                isAppOpen = 0;
            }

            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            }

            public void onActivityStarted(Activity activity) {
                ++activityReferences;
            }

            public void onActivityResumed(Activity activity) {
                isAppOpen = 1;
                prevActivity = currentActivity;
                currentActivity = activity.getLocalClassName();
                wentInBackground = false;
            }

            public void onActivityStopped(Activity activity) {
                if (!prevActivity.equals(currentActivity) && currentActivity.equals(activity.getLocalClassName())) {
                    isAppOpen = 0;
                    wentInBackground = true;
                }
                --activityReferences;
            }
        });
    }

    public String getLoginBaseUrl(String defaultUrl) {
        String value = AppPreferences.getInstance(this).getStringValue(QuickPreferences.APP_LOGIN_FEED_URL, defaultUrl);
        if (TextUtils.isEmpty(value)) {
            value = defaultUrl;
        }
        return value;
    }

    public String getBaseUrl(String defaultUrl) {
        String value = AppPreferences.getInstance(this).getStringValue(QuickPreferences.APP_FEED_BASE_URL, defaultUrl);
        if (TextUtils.isEmpty(value)) {
            value = defaultUrl;
        }
        return value;
    }

    public String getSWBaseUrl(String defaultUrl) {
        String value = AppPreferences.getInstance(this).getStringValue(QuickPreferences.EVENT_FEED_BASE_URL, defaultUrl);
        if (TextUtils.isEmpty(value)) {
            value = defaultUrl;
        }
        return value;
    }

    public long getServerTimeoutValue() {
        return Constants.FEED_CACHE_TIME;
    }

    public String getLogFeedUrl(String defaultUrl) {
        String value = AppPreferences.getInstance(this).getStringValue(QuickPreferences.APP_LOG_FEED_BASE_URL, defaultUrl);
        if (TextUtils.isEmpty(value)) {
            try {
                JSONObject hostJsonObject = JsonParser.getInstance().getJsonObjectFromAssets(this, "host.json");
                value = hostJsonObject.optJSONObject(KEY_DATA).optJSONObject("log_feed").optString("value");
                AppPreferences.getInstance(this).setStringValue(QuickPreferences.APP_LOG_FEED_BASE_URL, value);
            } catch (Exception ignored) {
            }
        }
        return value;
    }

    public void setApplicationFont() {
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/appfont.ttf");
    }

    public boolean getVideoPlay() {
        return this.isVideoPlay;
    }

    public void setVideoPlay(boolean videoPlay) {
        this.isVideoPlay = videoPlay;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    // Videos

    private static final String DOWNLOAD_ACTION_FILE = "actions";
    private static final String DOWNLOAD_TRACKER_ACTION_FILE = "tracked_actions";
    private static final String DOWNLOAD_CONTENT_DIRECTORY = "downloads";

    protected String userAgent;

    private DatabaseProvider databaseProvider;
    private File downloadDirectory;
    private Cache downloadCache;
    private DownloadManager downloadManager;

    /**
     * Returns a {@link DataSource.Factory}.
     */
    public DataSource.Factory buildDataSourceFactory() {
        DefaultDataSourceFactory upstreamFactory =
                new DefaultDataSourceFactory(this, buildHttpDataSourceFactory());
        return buildReadOnlyCacheDataSource(upstreamFactory, getDownloadCache());
    }

    /**
     * Returns a {@link HttpDataSource.Factory}.
     */
    public HttpDataSource.Factory buildHttpDataSourceFactory() {
        return new DefaultHttpDataSourceFactory(userAgent);
    }

    /**
     * Returns whether extension renderers should be used.
     */
    public boolean useExtensionRenderers() {
        return "withExtensions".equals(BuildConfig.FLAVOR);
    }

    public RenderersFactory buildRenderersFactory(boolean preferExtensionRenderer) {
        @DefaultRenderersFactory.ExtensionRendererMode
        int extensionRendererMode =
                useExtensionRenderers()
                        ? (preferExtensionRenderer
                        ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        return new DefaultRenderersFactory(/* context= */ this)
                .setExtensionRendererMode(extensionRendererMode);
    }

    public DownloadManager getDownloadManager() {
        initDownloadManager();
        return downloadManager;
    }

    protected synchronized Cache getDownloadCache() {
        if (downloadCache == null) {
            File downloadContentDirectory = new File(getDownloadDirectory(), DOWNLOAD_CONTENT_DIRECTORY);
            downloadCache =
                    new SimpleCache(downloadContentDirectory, new NoOpCacheEvictor(), getDatabaseProvider());
        }
        return downloadCache;
    }

    private synchronized void initDownloadManager() {
        if (downloadManager == null) {
            DefaultDownloadIndex downloadIndex = new DefaultDownloadIndex(getDatabaseProvider());
            upgradeActionFile(
                    DOWNLOAD_ACTION_FILE, downloadIndex, /* addNewDownloadsAsCompleted= */ false);
            upgradeActionFile(
                    DOWNLOAD_TRACKER_ACTION_FILE, downloadIndex, /* addNewDownloadsAsCompleted= */ true);
            DownloaderConstructorHelper downloaderConstructorHelper =
                    new DownloaderConstructorHelper(getDownloadCache(), buildHttpDataSourceFactory());
            downloadManager =
                    new DownloadManager(
                            this, downloadIndex, new DefaultDownloaderFactory(downloaderConstructorHelper));
        }
    }

    private void upgradeActionFile(
            String fileName, DefaultDownloadIndex downloadIndex, boolean addNewDownloadsAsCompleted) {
        try {
            ActionFileUpgradeUtil.upgradeAndDelete(
                    new File(getDownloadDirectory(), fileName),
                    /* downloadIdProvider= */ null,
                    downloadIndex,
                    /* deleteOnFailure= */ true,
                    addNewDownloadsAsCompleted);
        } catch (IOException e) {
        }
    }

    private DatabaseProvider getDatabaseProvider() {
        if (databaseProvider == null) {
            databaseProvider = new ExoDatabaseProvider(this);
        }
        return databaseProvider;
    }

    private File getDownloadDirectory() {
        if (downloadDirectory == null) {
            downloadDirectory = getExternalFilesDir(null);
            if (downloadDirectory == null) {
                downloadDirectory = getFilesDir();
            }
        }
        return downloadDirectory;
    }

    protected static CacheDataSourceFactory buildReadOnlyCacheDataSource(
            DataSource.Factory upstreamFactory, Cache cache) {
        return new CacheDataSourceFactory(
                cache,
                upstreamFactory,
                new FileDataSourceFactory(),
                /* cacheWriteDataSinkFactory= */ null,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                /* eventListener= */ null);
    }
}
