package com.db.ads;

import android.content.Context;

import com.bhaskar.appscommon.ads.AdUnitConst;
import com.bhaskar.appscommon.ads.AdsConstants;
import com.db.data.models.NewsListInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.util.AppPreferences;
import com.db.util.QuickPreferences;
import com.db.util.Systr;

import java.util.List;

public class AdPlacementController {

    private static AdPlacementController adPlacementController;

    private AdPlacementController() {
    }

    public static AdPlacementController getInstance() {
        if (adPlacementController == null) {
            adPlacementController = new AdPlacementController();
        }

        return adPlacementController;
    }

    public List<NewsListInfo> addHomeListingNativeAds(List<NewsListInfo> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 0 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                NewsListInfo adInfo3 = new NewsListInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 0 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                NewsListInfo adInfo2 = new NewsListInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 0 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                NewsListInfo adInfo1 = new NewsListInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<PhotoListItemInfo> addHomePhotoListingNativeAds(List<PhotoListItemInfo> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 0 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                PhotoListItemInfo adInfo3 = new PhotoListItemInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 0 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                PhotoListItemInfo adInfo2 = new PhotoListItemInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 0 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                PhotoListItemInfo adInfo1 = new PhotoListItemInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<Object> addHomePhotoListingNativeAdsWithObject(List<Object> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 0 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                PhotoListItemInfo adInfo3 = new PhotoListItemInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 0 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                PhotoListItemInfo adInfo2 = new PhotoListItemInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 0 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                PhotoListItemInfo adInfo1 = new PhotoListItemInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<PhotoListItemInfo> addPhotoListingNativeAds(List<PhotoListItemInfo> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 0 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                PhotoListItemInfo adInfo3 = new PhotoListItemInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 0 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                PhotoListItemInfo adInfo2 = new PhotoListItemInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 0 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                PhotoListItemInfo adInfo1 = new PhotoListItemInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<VideoInfo> addHomeVideoListingNativeAds(List<VideoInfo> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 0 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                VideoInfo adInfo3 = new VideoInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 0 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                VideoInfo adInfo2 = new VideoInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 0 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                VideoInfo adInfo1 = new VideoInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<Object> addHomeVideoListingNativeAdsObject(List<Object> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 0 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                VideoInfo adInfo3 = new VideoInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 0 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                VideoInfo adInfo2 = new VideoInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 0 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                VideoInfo adInfo1 = new VideoInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<VideoInfo> addVideoListingNativeAds(List<VideoInfo> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 0 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                VideoInfo adInfo3 = new VideoInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 0 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                VideoInfo adInfo2 = new VideoInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 0 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                VideoInfo adInfo1 = new VideoInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<Object> addHomeListingNativeAdsForRajya(List<Object> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            if (pos1 > 0) pos1 = pos1 + 1;
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            if (pos2 > 0) pos2 = pos2 + 1;
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
            if (pos3 > 0) pos3 = pos3 + 1;
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 1 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                NewsListInfo adInfo3 = new NewsListInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 1 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                NewsListInfo adInfo2 = new NewsListInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 1 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                NewsListInfo adInfo1 = new NewsListInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<Object> addHomeListingNativeAdsForRajyaWithObject(List<Object> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            if (pos1 > 0) pos1 = pos1 + 1;
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            if (pos2 > 0) pos2 = pos2 + 1;
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
            if (pos3 > 0) pos3 = pos3 + 1;
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 1 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                NewsListInfo adInfo3 = new NewsListInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 1 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                NewsListInfo adInfo2 = new NewsListInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 1 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                NewsListInfo adInfo1 = new NewsListInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_HOME_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<Object> addNewsListingNativeAdsWithObject(List<Object> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 0 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                NewsListInfo adInfo3 = new NewsListInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 0 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                NewsListInfo adInfo2 = new NewsListInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 0 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                NewsListInfo adInfo1 = new NewsListInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<NewsListInfo> addNewsListingNativeAds(List<NewsListInfo> list, Context context) {
        int pos1 = 0, pos2 = 0, pos3 = 0;
        try {
            pos1 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_1, 0);
            pos2 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_2, 0);
            pos3 = AppPreferences.getInstance(context).getIntValue(QuickPreferences.AdPref.NATIVE_POSITION_3, 0);
        } catch (Exception e) {
        }

        int added = 0;
        try {
            if (pos1 > 0 && list.size() >= pos1) {
                Systr.println("ADS : New Native Added  : AdUnit : 3");
                NewsListInfo adInfo3 = new NewsListInfo();
                adInfo3.isAd = true;
                adInfo3.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo3.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_TOP;
                list.add(pos1, adInfo3);
                added += 1;
            }

            if (pos2 > 0 && list.size() >= pos2 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 6");
                NewsListInfo adInfo2 = new NewsListInfo();
                adInfo2.isAd = true;
                adInfo2.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo2.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_MID;
                list.add(pos2 + added, adInfo2);
                added += 1;
            }

            if (pos3 > 0 && list.size() >= pos3 + added) {
                Systr.println("ADS : New Native Added  : AdUnit : 9");
                NewsListInfo adInfo1 = new NewsListInfo();
                adInfo1.isAd = true;
                adInfo1.adType = AdsConstants.AD_TYPE_NATIVE;
                adInfo1.adUnit = AdUnitConst.AD_UNIT_NATIVE_LISTING_BOTTOM;
                list.add(pos3 + added, adInfo1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }
}
