package com.db.ads;

import com.bhaskar.appscommon.ads.AdUnitConst;
import com.bhaskar.appscommon.ads.AdsConstants;
import com.db.data.models.NewsListInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.util.Systr;

import java.util.List;

public class BannerAdPlacementController {

    private static BannerAdPlacementController adPlacementController;

    private BannerAdPlacementController() {
    }

    public static BannerAdPlacementController getInstance() {
        if (adPlacementController == null) {
            adPlacementController = new BannerAdPlacementController();
        }

        return adPlacementController;
    }

    public List<PhotoListItemInfo> addPhotoListingBannerAds(List<PhotoListItemInfo> list) {
        for (int i = 2; i < list.size(); i += 3) {
            try {
                switch (i % 3) {
                    case 0:
                        Systr.println("ADS : New Banner Added  : AdUnit : " + i);
                        PhotoListItemInfo adInfo3 = new PhotoListItemInfo();
                        adInfo3.isAd = true;
                        adInfo3.adType = AdsConstants.AD_TYPE_BANNER;
                        adInfo3.adUnit = AdUnitConst.AD_UNIT_Photo_Gallery_LISTING_TOP;
                        list.add(i, adInfo3);
                        break;
                    case 1:
                        Systr.println("ADS : New Banner Added  : AdUnit : " + i);
                        PhotoListItemInfo adInfo2 = new PhotoListItemInfo();
                        adInfo2.isAd = true;
                        adInfo2.adType = AdsConstants.AD_TYPE_BANNER;
                        adInfo2.adUnit = AdUnitConst.AD_UNIT_Photo_Gallery_LISTING_MID;
                        list.add(i, adInfo2);
                        break;
                    case 2:
                        Systr.println("ADS : New Banner Added  : AdUnit : " + i);
                        PhotoListItemInfo adInfo1 = new PhotoListItemInfo();
                        adInfo1.isAd = true;
                        adInfo1.adType = AdsConstants.AD_TYPE_BANNER;
                        adInfo1.adUnit = AdUnitConst.AD_UNIT_Photo_Gallery_LISTING_BOTTOM;
                        list.add(i, adInfo1);
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return list;
    }

    public List<NewsListInfo> addNewsBreifListingBannerAds(List<NewsListInfo> list) {

        for (int i = 2; i < list.size(); i += 3) {
            try {
                switch (i % 3) {
                    case 0:
                        Systr.println("ADS : New Banner Added  : AdUnit : " + i);
                        NewsListInfo adInfo3 = new NewsListInfo();
                        adInfo3.isAd = true;
                        adInfo3.adType = AdsConstants.AD_TYPE_BANNER;
                        adInfo3.adUnit = AdUnitConst.AD_UNIT_News_Breif_LISTING_TOP;
                        list.add(i, adInfo3);
                        break;

                    case 1:
                        Systr.println("ADS : New Banner Added  : AdUnit : " + i);
                        NewsListInfo adInfo2 = new NewsListInfo();
                        adInfo2.isAd = true;
                        adInfo2.adType = AdsConstants.AD_TYPE_BANNER;
                        adInfo2.adUnit = AdUnitConst.AD_UNIT_News_Breif_LISTING_MID;
                        list.add(i, adInfo2);
                        break;

                    case 2:
                        Systr.println("ADS : New Banner Added  : AdUnit : " + i);
                        NewsListInfo adInfo1 = new NewsListInfo();
                        adInfo1.isAd = true;
                        adInfo1.adType = AdsConstants.AD_TYPE_BANNER;
                        adInfo1.adUnit = AdUnitConst.AD_UNIT_News_Breif_LISTING_BOTTOM;
                        list.add(i, adInfo1);
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return list;
    }
}
