package com.db.ads;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.appscommon.ads.AdsConstants;
import com.bhaskar.appscommon.tracking.util.Systr;
//import com.google.ads.mediation.facebook.FacebookAdapter;
//import com.google.ads.mediation.inmobi.InMobiAdapter;
import com.bhaskar.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAdView;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Vector;

import static com.google.android.gms.ads.formats.NativeAdOptions.ADCHOICES_TOP_RIGHT;

public class NativeListAdController {

    // Live
//    public static final String AD_UNIT_NATIVE_LISTING_TOP = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_ROS_Native_Top";
//    public static final String AD_UNIT_NATIVE_LISTING_MID = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_ROS_Native_Mid";
//    public static final String AD_UNIT_NATIVE_LISTING_BOTTOM = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_ROS_Native_Bottom";
//
//    public static final String AD_UNIT_NATIVE_HOME_LISTING_TOP = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_ROS_Native_Top";
//    public static final String AD_UNIT_NATIVE_HOME_LISTING_MID = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_ROS_Native_Mid";
//    public static final String AD_UNIT_NATIVE_HOME_LISTING_BOTTOM = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_ROS_Native_Bottom";

//    private static final String AD_UNIT_NATIVE_LISTING_TOP = "/6499/example/native";
//    private static final String AD_UNIT_NATIVE_LISTING_MID = "/6499/example/native";
//    private static final String AD_UNIT_NATIVE_LISTING_BOTTOM = "/6499/example/native";

    public interface onAdNotifyListener {
        void onNotifyAd();
    }

    private static NativeListAdController nativeListAdController;
    private static Context mContext;

    private NativeListAdController() {
    }

    class DataInfo {
        String unitId;
        int adType;
        int position;
        //        ViewGroup viewGroup;
        WeakReference<onAdNotifyListener> listener;

        NativeAdView adView;
        boolean hasImpression;

        @Override
        public boolean equals(Object obj) {
            return this.unitId.equals(((DataInfo) obj).unitId);
        }
    }

    private AdLoader adLoader = null;
    private Vector<DataInfo> infoList;
    private HashMap<String, DataInfo> viewHashMap = new HashMap<>();

    public static NativeListAdController getInstance(Context context) {
        mContext = context;
        if (nativeListAdController == null) {
            nativeListAdController = new NativeListAdController();
        }

        return nativeListAdController;
    }

    public void getAdRequest(String unitId, int adType, int position, WeakReference<onAdNotifyListener> listener) {
        Systr.println("ADS : Ad request : position : " + position);

        if (infoList == null) {
            infoList = new Vector<>();
        }

        if (viewHashMap.containsKey(unitId)) {
            DataInfo info = viewHashMap.get(unitId);
            if (info.adView == null) {
                viewHashMap.remove(info.unitId);
                info = null;
                adRequest(unitId, adType, position, listener);
            } else {
                if (info.hasImpression && position != info.position) {
                    if (info.adView.getParent() != null) {
                        ((ViewGroup) info.adView.getParent()).removeAllViews();
                    }
                    info.adView.destroy();
                    viewHashMap.remove(info.unitId);
                    info.adView = null;
                    info = null;

                    adRequest(unitId, adType, position, listener);
                }
            }
        } else {
            adRequest(unitId, adType, position, listener);
        }
    }


//    public void getAd(String unitId, int adType, int position, ViewGroup viewGroup, WeakReference<onAdNotifyListener> listener) {
//        if (infoList == null) {
//            infoList = new ArrayList<>();
//        }
//
////        if (viewHashMap.containsKey(unitId) && viewHashMap.get(unitId) != null) {
////            viewGroup.removeAllViews();
////            View view = viewHashMap.get(unitId);
////            if (view.getParent() != null) {
////                ((ViewGroup) view.getParent()).removeView(view);
////            }
////            viewGroup.addView(view);
////            viewGroup.setVisibility(View.VISIBLE);
////
////        } else {
////            viewGroup.setVisibility(View.GONE);
////
////            DataInfo dataInfo = new DataInfo();
////            dataInfo.unitId = unitId;
////            dataInfo.adType = adType;
////            dataInfo.position = position;
////            dataInfo.viewGroup = viewGroup;
////            dataInfo.listener = listener;
////            infoList.add(dataInfo);
////
////            checkForLoad();
////        }
//
//        if (viewHashMap.containsKey(unitId)) {
//            DataInfo info = viewHashMap.get(unitId);
//            if (info.adView == null) {
//                viewHashMap.remove(info);
//                adRequest(unitId, adType, position, viewGroup, listener);
//            } else {
//                if (info.hasImpression) {
//                    viewGroup.removeAllViews();
//                    viewGroup.setVisibility(View.GONE);
//
//                    info.adView.destroy();
//                    viewHashMap.remove(info);
//                    info = null;
//
//                    adRequest(unitId, adType, position, viewGroup, listener);
//
//                } else {
//                    viewGroup.removeAllViews();
//                    if (info.adView.getParent() != null) {
//                        ((ViewGroup) info.adView.getParent()).removeView(info.adView);
//                    }
//                    viewGroup.addView(info.adView);
//                    viewGroup.setVisibility(View.VISIBLE);
//                }
//            }
//        } else {
//            adRequest(unitId, adType, position, viewGroup, listener);
//        }
//    }

    public void getAd(String unitId, ViewGroup viewGroup, int position) {
        Systr.println("ADS : GET ad view : position : " + position);
        if (viewHashMap.containsKey(unitId)) {
            DataInfo info = viewHashMap.get(unitId);

            if (info.adView == null) {
                viewHashMap.remove(info.unitId);
            } else {
                if (info.position == position || !info.hasImpression) {
                    info.position = position;
                    viewGroup.removeAllViews();
                    if (info.adView.getParent() != null) {
                        ((ViewGroup) info.adView.getParent()).removeAllViews();
                    }
                    viewGroup.addView(info.adView);
                    viewGroup.setVisibility(View.VISIBLE);
                } else {
                    viewGroup.removeAllViews();
                    viewGroup.setVisibility(View.GONE);
                }
            }
        } else {
            viewGroup.setVisibility(View.GONE);
        }
    }

//    private void adRequest(String unitId, int adType, int position, ViewGroup viewGroup, WeakReference<onAdNotifyListener> listener) {
//        DataInfo dataInfo = new DataInfo();
//        dataInfo.unitId = unitId;
//
//        if (!infoList.contains(dataInfo)) {
//            dataInfo.adType = adType;
//            dataInfo.position = position;
////            dataInfo.viewGroup = viewGroup;
//            dataInfo.listener = listener;
//            infoList.add(dataInfo);
//
//            checkForLoad();
//        }
//    }

    private synchronized void adRequest(String unitId, int adType, int position, WeakReference<onAdNotifyListener> listener) {
        DataInfo dataInfo = new DataInfo();
        dataInfo.unitId = unitId;

        if (infoList.contains(dataInfo)) {
            int i = infoList.indexOf(dataInfo);
//            infoList.get(i).position = position;
            infoList.remove(i);
        } else {
        }
        dataInfo.adType = adType;
        dataInfo.position = position;
        dataInfo.listener = listener;
        infoList.add(dataInfo);

        checkForLoad();
    }

    boolean isLoading = false;

    private synchronized void checkForLoad() {
        if (infoList.size() > 0 && !isLoading) {
            DataInfo dataInfo = infoList.get(0);
            loadNativeAds(dataInfo);
            infoList.remove(dataInfo);
        }
    }

    public void loadNativeAds(DataInfo dataInfo) {
        Systr.println("ADS : New Native Loaded Called : AdUnit : " + dataInfo.unitId);

        isLoading = true;

        adLoader = new AdLoader.Builder(mContext.getApplicationContext(), dataInfo.unitId)
                .forAppInstallAd(appInstallAd -> {
                    Systr.println("ADS : New Native Loaded appinstall ad : AdUnit : " + dataInfo.unitId);

                    try {
                        showAdsInView(appInstallAd, null, dataInfo);

                        if (dataInfo.listener != null) {
                            dataInfo.listener.get().onNotifyAd();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .forContentAd(contentAd -> {
                    Systr.println("ADS : New Native Loaded content ad : AdUnit : " + dataInfo.unitId);

                    try {
                        showAdsInView(null, contentAd, dataInfo);

                        if (dataInfo.listener != null) {
                            dataInfo.listener.get().onNotifyAd();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int i) {
                        Systr.println("ADS : New Native Failed : AdUnit : " + dataInfo.unitId + ", Error Code : " + i);

                        isLoading = false;
                        checkForLoad();
                    }

                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();

                        isLoading = false;
                        checkForLoad();
                    }

                    @Override
                    public void onAdImpression() {
                        super.onAdImpression();
                        dataInfo.hasImpression = true;
                    }

                }).withNativeAdOptions(new NativeAdOptions.Builder()
                        .setAdChoicesPlacement(ADCHOICES_TOP_RIGHT)
                        .build()).build();

        Bundle extras = new Bundle();
        PublisherAdRequest.Builder adRequest = new PublisherAdRequest.Builder();
//        adRequest.addNetworkExtrasBundle(FacebookAdapter.class, extras);
//        adRequest.addNetworkExtrasBundle(InMobiAdapter.class, extras);

//        adRequest.addTestDevice("64EF4FFB20599B749E1E5C9DE67C079D");

        adLoader.loadAd(adRequest.build());
    }

    private void showAdsInView(NativeAppInstallAd appInstallAd, NativeContentAd contentAd, DataInfo dataInfo) {
//        dataInfo.viewGroup.removeAllViews();

        try {
            switch (dataInfo.adType) {
                case AdsConstants.AD_TYPE_NATIVE:
                    NativeAdView view = displayNewsListingNativeAd(appInstallAd, contentAd);
                    dataInfo.adView = view;
                    viewHashMap.put(dataInfo.unitId, dataInfo);

//                    dataInfo.viewGroup.addView(view);
//                    dataInfo.viewGroup.setVisibility(View.VISIBLE);
                    break;

                case AdsConstants.AD_TYPE_NATIVE_HORIZONTAL_PHOTO:
                    NativeAdView view1 = displayPhotoHorizontalListingNativeAd(appInstallAd, contentAd);
                    dataInfo.adView = view1;
                    viewHashMap.put(dataInfo.unitId, dataInfo);

//                    dataInfo.viewGroup.addView(view1);
//                    dataInfo.viewGroup.setVisibility(View.VISIBLE);
                    break;

                case AdsConstants.AD_TYPE_NATIVE_HORIZONTAL_VIDEO:
                    NativeAdView view2 = displayVideoHorizontalListingNativeAd(appInstallAd, contentAd);
                    dataInfo.adView = view2;
                    viewHashMap.put(dataInfo.unitId, dataInfo);

//                    dataInfo.viewGroup.addView(view2);
//                    dataInfo.viewGroup.setVisibility(View.VISIBLE);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private NativeAdView displayNewsListingNativeAd(NativeAppInstallAd appInstallAd, NativeContentAd contentAd) throws Exception {
//        viewGroup.removeAllViews();

        LayoutInflater inflater = (LayoutInflater) mContext.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        if (appInstallAd == null) {
            view = inflater.inflate( R.layout.ad_layout_native_ad_content_news_listing, null);

            NativeContentAdView adView = view.findViewById( R.id.unified_adview);

            TextView headlineView = adView.findViewById( R.id.ad_headline);
            headlineView.setText(contentAd.getHeadline());
            adView.setHeadlineView(headlineView);

            TextView bodyView = adView.findViewById( R.id.ad_body);
            bodyView.setText(contentAd.getBody());
            adView.setBodyView(bodyView);

            try {
                ImageView icon = adView.findViewById( R.id.ad_app_icon);
                icon.setImageDrawable(contentAd.getImages().get(0).getDrawable());
                adView.setImageView(icon);
            } catch (Exception e) {
            }

            TextView actionView = adView.findViewById( R.id.install_tv);
            actionView.setText(contentAd.getCallToAction());
            adView.setCallToActionView(actionView);

            adView.setNativeAd(contentAd);
//            viewGroup.addView(view);

            return adView;
        } else {
            view = inflater.inflate( R.layout.ad_layout_native_appinstall_news_listing, null);
            NativeAppInstallAdView adView = view.findViewById( R.id.unified_adview);

            TextView headlineView = adView.findViewById( R.id.ad_headline);
            headlineView.setText(appInstallAd.getHeadline());
            adView.setHeadlineView(headlineView);

            TextView bodyView = adView.findViewById( R.id.ad_body);
            bodyView.setText(appInstallAd.getBody());
            adView.setBodyView(bodyView);

            try {
                ImageView icon = adView.findViewById( R.id.ad_app_icon);
                icon.setImageDrawable(appInstallAd.getIcon().getDrawable());
                adView.setIconView(icon);
            } catch (Exception e) {
            }

            TextView actionView = adView.findViewById( R.id.install_tv);
            actionView.setText(appInstallAd.getCallToAction());
            adView.setCallToActionView(actionView);

            adView.setNativeAd(appInstallAd);
//            viewGroup.addView(view);

            return adView;
        }

//        viewGroup.setVisibility(View.VISIBLE);
//        return view;
    }

    private NativeAdView displayPhotoHorizontalListingNativeAd(NativeAppInstallAd appInstallAd, NativeContentAd contentAd) throws Exception {

//        viewGroup.removeAllViews();

        LayoutInflater inflater = (LayoutInflater) mContext.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        if (appInstallAd == null) {
            view = inflater.inflate( R.layout.ad_layout_native_ad_content_photo_horizontal, null);

            NativeContentAdView adView = view.findViewById( R.id.unified_adview);

            TextView headlineView = adView.findViewById( R.id.ad_headline);
            headlineView.setText(contentAd.getHeadline());
            adView.setHeadlineView(headlineView);

            TextView bodyView = adView.findViewById( R.id.ad_body);
            bodyView.setText(contentAd.getBody());
            adView.setBodyView(bodyView);

            try {
                ImageView icon = adView.findViewById( R.id.ad_app_icon);
                icon.setImageDrawable(contentAd.getImages().get(0).getDrawable());
                adView.setImageView(icon);
            } catch (Exception e) {
            }

            TextView actionView = adView.findViewById( R.id.install_tv);
            actionView.setText(contentAd.getCallToAction());
            adView.setCallToActionView(actionView);

            adView.setNativeAd(contentAd);
//            viewGroup.addView(view);
            return adView;

        } else {
            view = inflater.inflate( R.layout.ad_layout_native_appinstall_photo_horizontal, null);
            NativeAppInstallAdView adView = view.findViewById( R.id.unified_adview);

            TextView headlineView = adView.findViewById( R.id.ad_headline);
            headlineView.setText(appInstallAd.getHeadline());
            adView.setHeadlineView(headlineView);

            TextView bodyView = adView.findViewById( R.id.ad_body);
            bodyView.setText(appInstallAd.getBody());
            adView.setBodyView(bodyView);

            try {
                ImageView icon = adView.findViewById( R.id.ad_app_icon);
                icon.setImageDrawable(appInstallAd.getIcon().getDrawable());
                adView.setIconView(icon);
            } catch (Exception e) {
            }

            TextView actionView = adView.findViewById( R.id.install_tv);
            actionView.setText(appInstallAd.getCallToAction());
            adView.setCallToActionView(actionView);

            adView.setNativeAd(appInstallAd);
//            viewGroup.addView(view);
            return adView;
        }

//        viewGroup.setVisibility(View.VISIBLE);
//        return view;
    }


    private NativeAdView displayVideoHorizontalListingNativeAd(NativeAppInstallAd appInstallAd, NativeContentAd contentAd) throws Exception {

//        viewGroup.removeAllViews();

        LayoutInflater inflater = (LayoutInflater) mContext.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        if (appInstallAd == null) {
            view = inflater.inflate( R.layout.ad_layout_native_ad_content_video_horizontal, null);

            NativeContentAdView adView = view.findViewById( R.id.unified_adview);

            TextView headlineView = adView.findViewById( R.id.ad_headline);
            headlineView.setText(contentAd.getHeadline());
            adView.setHeadlineView(headlineView);

            TextView bodyView = adView.findViewById( R.id.ad_body);
            bodyView.setText(contentAd.getBody());
            adView.setBodyView(bodyView);

            try {
                ImageView icon = adView.findViewById( R.id.ad_app_icon);
                icon.setImageDrawable(contentAd.getImages().get(0).getDrawable());
                adView.setImageView(icon);
            } catch (Exception e) {
            }

            TextView actionView = adView.findViewById( R.id.install_tv);
            actionView.setText(contentAd.getCallToAction());
            adView.setCallToActionView(actionView);

            adView.setNativeAd(contentAd);
//            viewGroup.addView(view);
            return adView;

        } else {
            view = inflater.inflate( R.layout.ad_layout_native_appinstall_video_horizontal, null);
            NativeAppInstallAdView adView = view.findViewById( R.id.unified_adview);

            TextView headlineView = adView.findViewById( R.id.ad_headline);
            headlineView.setText(appInstallAd.getHeadline());
            adView.setHeadlineView(headlineView);

            TextView bodyView = adView.findViewById( R.id.ad_body);
            bodyView.setText(appInstallAd.getBody());
            adView.setBodyView(bodyView);

            try {
                ImageView icon = adView.findViewById( R.id.ad_app_icon);
                icon.setImageDrawable(appInstallAd.getIcon().getDrawable());
                adView.setIconView(icon);
            } catch (Exception e) {
            }

            TextView actionView = adView.findViewById( R.id.install_tv);
            actionView.setText(appInstallAd.getCallToAction());
            adView.setCallToActionView(actionView);

            adView.setNativeAd(appInstallAd);
//            viewGroup.addView(view);
            return adView;
        }

//        viewGroup.setVisibility(View.VISIBLE);

//        return view;
    }
}
