package com.db.appintro;

import androidx.annotation.NonNull;

public interface OnAppIntroActionCallback {
    void onVideoIconTapped(@NonNull String videoUrl);
}
