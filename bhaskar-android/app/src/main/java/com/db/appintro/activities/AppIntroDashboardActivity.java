package com.db.appintro.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.Toolbar;

import com.bhaskar.appscommon.tracking.Tracking;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.appintro.fragment.AppIntroPagerFragment;
import com.db.appintro.utils.ChatIntroConst;
import com.db.util.AppPreferences;
import com.db.util.QuickPreferences;

public class AppIntroDashboardActivity extends BaseActivity {

    private int navigateFrom;
    private String gaScreen;

    public static Intent getIntent(Context context, int navigateFrom, String gaScreen) {
        Intent intent = new Intent(context, AppIntroDashboardActivity.class);
        intent.putExtra(ChatIntroConst.NavigationFlow.KEY_NAVIGATE_FROM, navigateFrom);
        intent.putExtra("gaScreen", gaScreen);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_intro);

        Intent intent = getIntent();
        if (intent != null) {
            navigateFrom = intent.getIntExtra(ChatIntroConst.NavigationFlow.KEY_NAVIGATE_FROM, 0);
            gaScreen = intent.getStringExtra("gaScreen");
        }
        handleToolbar();
        showFragment(AppIntroPagerFragment.getInstance(null), getIntent().getExtras());

        sendTracking();
    }

    private void sendTracking() {
        String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(this, InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);
    }

    private void handleToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
//        toolbar.setBackgroundColor(color);
        if (navigateFrom == ChatIntroConst.NavigationFlow.NAVIGATE_FROM_HOME) {
            toolbar.setTitle("Tutorials");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back_black);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        } else {
            toolbar.setVisibility(View.GONE);
        }

//        headerTitle = toolbar.findViewById(R.id.toolbar_title);
//        headerTitle.setText(title);
    }


    @Override
    public void onBackPressed() {
        if (navigateFrom == ChatIntroConst.NavigationFlow.NAVIGATE_FROM_HOME) {
            super.onBackPressed();
        }
    }
}
