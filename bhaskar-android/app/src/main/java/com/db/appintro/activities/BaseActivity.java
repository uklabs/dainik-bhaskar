package com.db.appintro.activities;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import com.bhaskar.R;

public abstract class BaseActivity extends AppCompatActivity {

    /**
     * This method will be used to replace fragment
     *
     * @param fragment
     */
    protected void showFragment(Fragment fragment, Bundle extras) {
        String name = fragment.getClass().getSimpleName();
        fragment.setArguments(extras);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.main_container, fragment, name);
        fragmentTransaction.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
