package com.db.appintro.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bhaskar.R;
import com.db.dbvideo.player.PlayerControllerCallbackReference;
import com.db.dbvideo.player.VideoPlayerConstant;
import com.db.dbvideo.videoview.DeviceOrientationListener;
import com.db.dbvideo.videoview.VideoPlayerCallbackListener;
import com.db.dbvideo.videoview.VideoPlayerFragment;
import com.db.main.BaseAppCompatActivity;
import com.db.util.ImageUtil;

public class VideoPlayActivity extends BaseAppCompatActivity implements VideoPlayerCallbackListener, DeviceOrientationListener {

    private VideoPlayerFragment videoPlayerFragment;
    int userSelectedOrientation = -1;
    private Handler handler = new Handler();

    public static Intent getIntent(Context context, String videoUrl, String contentThumb) {
        Intent intent = new Intent(context, VideoPlayActivity.class);
        intent.putExtra("url", videoUrl);
        intent.putExtra("thumb", contentThumb);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);

        String url = null;
        String thumb = null;
        Intent intent = getIntent();
        if (intent != null) {
            url = intent.getStringExtra("url");
            thumb = intent.getStringExtra("thumb");
        }

        if (!TextUtils.isEmpty(url)) {
            addVideoFragment(url, thumb, "", url, "", 0, 0);
        }

        findViewById(R.id.close_iv).setOnClickListener(view -> finish());
    }

    @Override
    public void changeOrientation() {
        if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            System.out.println("Change orientation called land - port");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            userSelectedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        } else {
            System.out.println("Change orientation called port - land");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            userSelectedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (videoPlayerFragment != null) {
            videoPlayerFragment.changeVideoViewScreen();
        }

        handler.postDelayed(() -> {
            if (videoPlayerFragment != null)
                videoPlayerFragment.redrawControls();
        }, 500);

        hideUnhideDetailLayout();
    }

    @Override
    public void onBackPressed() {
        if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        } else {
            super.onBackPressed();
        }
    }

    private void hideUnhideDetailLayout() {
        int orientation = getResources().getConfiguration().orientation;
        switch (orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                showVideoDetailView();
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                hideVideoDetailView();
                break;
        }
    }

    private void showVideoDetailView() {
        showStatusBar();

        if (videoPlayerFragment != null && videoPlayerFragment.mainController != null) {
            videoPlayerFragment.mainController.changeScreenSwitcherIcon(R.drawable.ic_video_fullscreen);
        }
    }

    private void hideVideoDetailView() {
        hideStatusBar();

        if (videoPlayerFragment != null && videoPlayerFragment.mainController != null) {
            videoPlayerFragment.mainController.changeScreenSwitcherIcon(R.drawable.ic_video_fullscreen_exit);
        }
    }

    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);

        } else if (Build.VERSION.SDK_INT >= 16) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    }

    //Show Status Bar
    private void showStatusBar() {
        if (Build.VERSION.SDK_INT >= 16) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        }
    }


    private void addVideoFragment(String videoUrl, String imageUrl, String title, String shareLink, String provider_code, int providerId, int vwallbrandid) {
        videoPlayerFragment = new VideoPlayerFragment();

        Bundle bundle = new Bundle();
//        VideosAdsControl videoAdsControl = JsonParser.getVideoAdsControl(this);
//        videoAdsControl.setActive(AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.AdPref.VIDEO_TOGGLE, false));
//        videoAdsControl.setActive(false);
//        videoAdsControl.setFromArticle(false);
//        videoAdsControl.setImavasturl(String.format((videoAdsControl.getImavasturl() + Constants.VIDEO_AD_URL_EXTRA), videoUrl, title, AdsConstants.getContentUrl(shareLink), String.valueOf(new Date().getTime())));
//        videoAdsControl.setContent_url(videoUrl);
//        bundle.putParcelable(VideoPlayerConstant.AD_CONTROL, videoAdsControl);

        bundle.putString(VideoPlayerConstant.VIDEO_URL, videoUrl);
        bundle.putString(VideoPlayerConstant.PROVIDER_CODE, provider_code);
        bundle.putInt(VideoPlayerConstant.PROVIDER_ID, providerId);
        bundle.putInt(VideoPlayerConstant.VWALLBRANDID, vwallbrandid);
        bundle.putInt(VideoPlayerConstant.DEFAULT_THUMBNAIL_IMAGE_RES_ID, R.drawable.water_mark_news_detail);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NATIVE_CONTROLS, false);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NEXT_BUTTON, false);
        bundle.putBoolean(VideoPlayerConstant.HIDE_SHARE_BUTTON, true);
//        bundle.putBoolean(VideoPlayerConstant.HIDE_ZOOM_BUTTON, true);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_PLAY, true);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_NEXT_PLAY, false);
        bundle.putString(VideoPlayerConstant.SHARE_TITLE, title);
        bundle.putString(VideoPlayerConstant.SHARE_LINK, shareLink);

        videoPlayerFragment.setArguments(bundle);

        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, videoPlayerFragment).commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }

        PlayerControllerCallbackReference playerControllerCallbackReference = videoPlayerFragment.getPlayerControllerCallbackReference();
        if (playerControllerCallbackReference == null) {
            runHandler(imageUrl);
        }
    }

    private void runHandler(final String imageUrl) {
        new Handler().postDelayed(() -> setThumbnailImage(imageUrl), 500);
    }

    private void setThumbnailImage(final String imageUrl) {
        PlayerControllerCallbackReference playerControllerCallbackReference = videoPlayerFragment.getPlayerControllerCallbackReference();
        if (playerControllerCallbackReference == null) {
            runHandler(imageUrl);
            return;
        }

        final ImageView thumbnailImageView = playerControllerCallbackReference.getThumbnailImageView();
        thumbnailImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        ImageUtil.setImage(getApplicationContext(), imageUrl, thumbnailImageView, R.drawable.water_mark_news_detail);
    }

    @Override
    public void onVideoStart() {

    }

    @Override
    public void onVideoComplete() {

    }

    @Override
    public void onVideoError() {

    }

    @Override
    public void onNextButtonclick() {

    }
}
