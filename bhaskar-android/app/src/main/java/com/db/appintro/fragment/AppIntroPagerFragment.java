package com.db.appintro.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.BuildConfig;
import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.db.appintro.OnAppIntroActionCallback;
import com.db.appintro.activities.VideoPlayActivity;
import com.db.appintro.models.AppIntro;
import com.db.appintro.utils.ChatIntroConst;
import com.db.util.AppConstants;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.EasyDialogUtils;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.shuhart.bubblepagerindicator.BubblePageIndicator;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppIntroPagerFragment extends Fragment implements OnAppIntroActionCallback {

    private final String TAG = AppIntroPagerFragment.class.getSimpleName();
    private ViewPager viewPager;
    private ImageView btnSkip;
    private ProgressBar progressBar;
    private ProgressBar progressBarCenter;
    private TextView tvNoNetwork;
    private int navigateFrom;
    private Context context;
    private IntroPagerAdapter introPagerAdapter;
    private BubblePageIndicator bubblePageIndicator;
    private final int PAGER_AUTO_SWAP_INTERVAL = 3000;
    private int mViewPagerCurrentPosition;

    private Handler mAutoSwapHandler;
    private String gaValue = CTConstant.PROP_VALUE.SKIP;

    public static Fragment getInstance(Bundle bundle) {
        Fragment fragment = new AppIntroPagerFragment();
        if (bundle != null)
            fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (null != bundle) {
            navigateFrom = bundle.getInt(ChatIntroConst.NavigationFlow.KEY_NAVIGATE_FROM);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_intro_pager_dashboard, container, false);
        viewPager = view.findViewById(R.id.view_pager);
        progressBar = view.findViewById(R.id.progressBar);
        progressBarCenter = view.findViewById(R.id.progressBarCenter);
        bubblePageIndicator = view.findViewById(R.id.pager_indicator);
        initUI(view);
        return view;
    }

    private void initUI(View view) {
        btnSkip = view.findViewById(R.id.btn_skip_pager);
        tvNoNetwork = view.findViewById(R.id.tv_no_network_available);
        btnSkip.setOnClickListener(v -> {
            if (getActivity() != null) {
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    onSkipDonBtnClick();
                } else {
                    EasyDialogUtils.showInfoDialog(getActivity(), getString(R.string.alert_title_alert),
                            getString(R.string.alert_network_not_exist));
                }
            }
        });

        List<AppIntro.Datum> assets = new ArrayList<>();
        introPagerAdapter = new IntroPagerAdapter(context, assets, this);
        viewPager.setPageTransformer(false, AppUtils.getParallaxPagerTransform(R.id.iv_pager, 0, 0.5f));
        viewPager.setAdapter(introPagerAdapter);
        bubblePageIndicator.setViewPager(viewPager);

        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            getAppIntroData(assets);
        } else {
            //appIntroLogoIv.setVisibility(View.GONE);
            tvNoNetwork.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.GONE);
        }


        if (navigateFrom != AppConstants.NavigationFlow.NAVIGATE_FROM_SPLASH) {
            //show skip button
            btnSkip.setVisibility(View.GONE);
        }
    }

    private void onSkipDonBtnClick() {
        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.APPINTRO, CTConstant.PROP_NAME.CLICKON, gaValue, "");

        if (navigateFrom == AppConstants.NavigationFlow.NAVIGATE_FROM_SPLASH) {
            AppPreferences.getInstance(context).setBooleanValue(QuickPreferences.SHOW_TUTORIAL, true);
            if (context != null) {
                if (navigateFrom == ChatIntroConst.NavigationFlow.NAVIGATE_FROM_SPLASH) {
                    btnSkip.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                }
            }
        } else if (navigateFrom == AppConstants.NavigationFlow.NAVIGATE_FROM_HOME) {
            if (getActivity() != null)
                getActivity().finish();
        }
    }


    private void getAppIntroData(@NonNull List<AppIntro.Datum> assets) {
        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            progressBarCenter.setVisibility(View.VISIBLE);
            String finalFeedURL = String.format(Urls.TUTORIAL_URL, CommonConstants.CHANNEL_ID, BuildConfig.VERSION_CODE);
            Systr.println(String.format(" Feed API %s ", finalFeedURL));
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        Systr.println(String.format(" Feed API %s response %s", finalFeedURL, response.toString()));
                        progressBarCenter.setVisibility(View.GONE);
                        try {
                            AppIntro appIntro = new Gson().fromJson(response.toString(), AppIntro.class);
                            renderAppIntroResponse(assets, appIntro);
                        } catch (Exception e) {
                            e.printStackTrace();
                            AppUtils.getInstance().showCustomToast(getContext(), e.getMessage());
                        }
                    }, error -> {
                progressBarCenter.setVisibility(View.GONE);
                tvNoNetwork.setText(getString(R.string.alert_api_failed));
                tvNoNetwork.setVisibility(View.VISIBLE);
                tvNoNetwork.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.GONE);
                Systr.println(String.format(" Feed API %s error %s ", finalFeedURL, error.getMessage()));
                Activity activity = getActivity();
                if (activity != null && isAdded()) {
                    if (error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
        } else {
            if (isAdded() && getActivity() != null)
                AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
        }
    }


    private void renderAppIntroResponse(List<AppIntro.Datum> assets, AppIntro appIntroModel) {
        // Render data
        assets.clear();
        if (appIntroModel.getData() != null && !appIntroModel.getData().isEmpty()) {
            assets.addAll(appIntroModel.getData());
            introPagerAdapter.notifyDataSetChanged();
            if(assets.size()<=1){
                btnSkip.setImageResource(R.drawable.app_intro_got_it);
                btnSkip.setVisibility(View.VISIBLE);
            }
            startAutoSwapHandler();
        } else {
            if (navigateFrom == ChatIntroConst.NavigationFlow.NAVIGATE_FROM_SPLASH) {
                btnSkip.performClick();
            }
        }
        viewPager.setOffscreenPageLimit(assets.size());
        viewPager.setVisibility(View.VISIBLE);
        viewPager.setTag((appIntroModel.getSkip() - 1));
        processAdapter(appIntroModel);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void processAdapter(AppIntro appIntroModel) {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mViewPagerCurrentPosition = position;
                if (navigateFrom == AppConstants.NavigationFlow.NAVIGATE_FROM_SPLASH) {
                    if ((position >= appIntroModel.getSkip() - 1)) {
                        btnSkip.setVisibility(View.VISIBLE);
                    } else {
                        btnSkip.setVisibility(View.GONE);
                    }

                    if (position == (introPagerAdapter.getCount() - 1)) {
                        btnSkip.setImageResource(R.drawable.app_intro_got_it);
                        gaValue = CTConstant.PROP_VALUE.GOT_IT;
                    }
                }
                startAutoSwapHandler();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void startAutoSwapHandler() {
        if (mAutoSwapHandler == null) {
            mAutoSwapHandler = new Handler();
        } else {
            mAutoSwapHandler.removeCallbacks(mAutoSwapRunnable);
        }

        if (viewPager.getCurrentItem() != (introPagerAdapter.getCount() - 1)) {
            mAutoSwapHandler.postDelayed(mAutoSwapRunnable, PAGER_AUTO_SWAP_INTERVAL);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mAutoSwapHandler != null) {
            mAutoSwapHandler.removeCallbacks(mAutoSwapRunnable);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAutoSwapHandler != null) {
            mAutoSwapHandler.postDelayed(mAutoSwapRunnable, PAGER_AUTO_SWAP_INTERVAL);
        }
    }


    private final Runnable mAutoSwapRunnable = new Runnable() {
        @Override
        public void run() {
            boolean isAnimate = true;
            mViewPagerCurrentPosition++;
            if (mViewPagerCurrentPosition == introPagerAdapter.getCount()) {
                mViewPagerCurrentPosition = 0;
                isAnimate = false;
            }
            viewPager.setCurrentItem(mViewPagerCurrentPosition, isAnimate);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onVideoIconTapped(@NonNull String videoUrl) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString(ChatIntroConst.KEY_VIDEO_URL, videoUrl);
            startActivity(VideoPlayActivity.getIntent(context, videoUrl, ""));

        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }
}