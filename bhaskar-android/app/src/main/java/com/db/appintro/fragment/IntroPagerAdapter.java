package com.db.appintro.fragment;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bhaskar.R;
import com.db.appintro.OnAppIntroActionCallback;
import com.db.appintro.models.AppIntro;
import com.db.util.AppConstants;
import com.db.util.ImageUtil;

import java.util.List;

public final class IntroPagerAdapter extends PagerAdapter implements View.OnClickListener {

    private List<AppIntro.Datum> assets;
    private OnAppIntroActionCallback onAppIntroActionCallback;
    private LayoutInflater inflater;
    private final static String TAG = IntroPagerAdapter.class.getSimpleName();
    private Context context;

    /***
     *
     * @param context
     * @param assets
     * @param onAppIntroActionCallback
     */
    public IntroPagerAdapter(@NonNull Context context, @NonNull List<AppIntro.Datum> assets, @NonNull OnAppIntroActionCallback onAppIntroActionCallback) {
        super();
        this.context = context;
        this.assets = assets;
        this.onAppIntroActionCallback = onAppIntroActionCallback;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_play_video:
                onAppIntroActionCallback.onVideoIconTapped(v.getTag() + "");
                break;
        }
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_page, collection, false);
        processUI(viewGroup, position);

        collection.addView(viewGroup);
        return viewGroup;
    }

    /***
     *
     * @param viewGroup
     * @param pagerPosition
     */
    private void processUI(ViewGroup viewGroup, int pagerPosition) {
        try {
            AppIntro.Datum asset = assets.get(pagerPosition);
            ImageView videoPlayIv = viewGroup.findViewById(R.id.iv_play_video);

            switch (asset.getType()) {
                case AppConstants.CONTENT_TYPE_IMAGE:
                    videoPlayIv.setVisibility(View.GONE);
                    videoPlayIv.setOnClickListener(null);
                    videoPlayIv.setTag(null);
                    break;
                case AppConstants.CONTENT_TYPE_VIDEOS:
                    videoPlayIv.setVisibility(View.VISIBLE);
                    videoPlayIv.setOnClickListener(this);
                    videoPlayIv.setTag(asset.getVideoUrl());
                    break;
            }
            ImageView ivPager = viewGroup.findViewById(R.id.iv_pager);

            String imageUrl = asset.getImgUrl();
            if (ivPager != null && !TextUtils.isEmpty(imageUrl)) {
                ImageUtil.setImage(context, imageUrl, ivPager, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object view) {
        container.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return (null != assets) ? assets.size() : 0;
    }
}
