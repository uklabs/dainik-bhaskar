package com.db.appintro.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppIntro {

    @SerializedName("feed")
    @Expose
    private String feed;
    @SerializedName("skip")
    @Expose
    private String skip;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }

    public int getSkip() {
        try {
            return Integer.parseInt(skip);
        }catch (Exception e){
            return 0;
        }
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("img_url")
        @Expose
        private String imgUrl;
        @SerializedName("video_url")
        @Expose
        private String videoUrl;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("pos")
        @Expose
        private String pos;

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPos() {
            return pos;
        }

        public void setPos(String pos) {
            this.pos = pos;
        }

    }

}

