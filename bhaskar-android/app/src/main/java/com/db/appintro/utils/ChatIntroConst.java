package com.db.appintro.utils;

public interface ChatIntroConst {

    String KEY_VIDEO_URL = "key_video_url";
    String KEY_IMAGE_URL = "key_image_url";

    interface NavigationFlow {
        int NAVIGATE_FROM_SPLASH = 1;
        int NAVIGATE_FROM_HOME = 2;

        String KEY_NAVIGATE_FROM = "key_navigate_from";
    }
}
