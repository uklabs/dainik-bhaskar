package com.db.appintro.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;

import com.bhaskar.R;

/**
 * Class to keep static utility methods for various utility tasks Please Define
 * DIALOG_ICON before using the class
 */
public final class DialogUtils {

    public static int DIALOG_ICON = 0;

    /**
     * To show Dialog with message
     *
     * @param activity
     * @param title    can be null if title is not allowed
     * @param message
     */
    public static void showInfoDialog(final Activity activity, final String title, final String message) {

        try {
            if (activity != null && !activity.isFinishing()) {

                AlertDialog.Builder builder = null;
                if (activity.getParent() != null)
                    builder = new AlertDialog.Builder(activity.getParent());
                else
                    builder = new AlertDialog.Builder(activity);

                if (title != null) {
                    builder.setTitle(title);

                    if (title.equals(activity.getString(R.string.alert_title_exception))) {
                        builder.setIcon(R.drawable.ic_alert);
                    } else if (title.equals(activity.getString(R.string.alert_title_error))) {
                        builder.setIcon(R.drawable.ic_fail);
                    } else if (title.equals(activity.getString(R.string.alert_title_success))) {
                        builder.setIcon(R.drawable.ic_success);
                    } else {
                        builder.setIcon(DIALOG_ICON);
                    }
                } else {
                    builder.setIcon(DIALOG_ICON);
                }
                if (message != null)
                    builder.setMessage(message);

                builder.setPositiveButton("OK", (dialog, which) -> {
                    if (!activity.isFinishing()) {
                        dialog.dismiss();
                    }
                    Runtime.getRuntime().gc();
                });
                AlertDialog msg = builder.create();
                msg.setCancelable(false);
                msg.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showInfoDialog(final Context context, final String title, final String message, DialogListener dialogListener) {

        try {
            if (context != null) {

                AlertDialog.Builder builder = null;
                builder = new AlertDialog.Builder(context);


                if (title != null) {
                    builder.setTitle(title);

                    if (title.equals(context.getString(R.string.alert_title_exception))) {
                        builder.setIcon(R.drawable.ic_alert);
                    } else if (title.equals(context.getString(R.string.alert_title_error))) {
                        builder.setIcon(R.drawable.ic_fail);
                    } else if (title.equals(context.getString(R.string.alert_title_success))) {
                        builder.setIcon(R.drawable.ic_success);
                    } else {
                        builder.setIcon(DIALOG_ICON);
                    }
                } else {
                    builder.setIcon(DIALOG_ICON);
                }
                if (message != null)
                    builder.setMessage(message);

                builder.setPositiveButton("OK", (dialog, which) -> {
                    if (context != null) {
                        dialog.dismiss();
                    }
                    if (dialogListener != null)
                        dialogListener.onOk();
                    Runtime.getRuntime().gc();
                });


                AlertDialog alert = builder.create();
                alert.setOnShowListener(dialog -> {
                    Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
                    btnPositive.setTextSize(16);
                    btnPositive.setTypeface(null, Typeface.NORMAL);
                });
                alert.setCancelable(false);
                alert.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showInfoDialog(final Activity activity, final String title, final String message, DialogListener dialogListener) {

        try {
            if (activity != null && !activity.isFinishing()) {

                AlertDialog.Builder builder = null;
                if (activity.getParent() != null)
                    builder = new AlertDialog.Builder(activity.getParent());
                else
                    builder = new AlertDialog.Builder(activity);

                if (title != null) {
                    builder.setTitle(title);

                    if (title.equals(activity.getString(R.string.alert_title_exception))) {
                        builder.setIcon(R.drawable.ic_alert);
                    } else if (title.equals(activity.getString(R.string.alert_title_error))) {
                        builder.setIcon(R.drawable.ic_fail);
                    } else if (title.equals(activity.getString(R.string.alert_title_success))) {
                        builder.setIcon(R.drawable.ic_success);
                    } else {
                        builder.setIcon(DIALOG_ICON);
                    }
                } else {
                    builder.setIcon(DIALOG_ICON);
                }
                if (message != null)
                    builder.setMessage(message);

                builder.setPositiveButton("OK", (dialog, which) -> {
                    if (!activity.isFinishing()) {
                        dialog.dismiss();
                    }
                    if (dialogListener != null)
                        dialogListener.onOk();
                    Runtime.getRuntime().gc();
                });


                AlertDialog alert = builder.create();
                alert.setOnShowListener(dialog -> {
                    Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
                    btnPositive.setTextSize(16);
                    btnPositive.setTypeface(null, Typeface.NORMAL);
                });
                alert.setCancelable(false);
                alert.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * *
     * To show Dialog with message and finish an activity when tap on Ok
     *
     * @param activity
     * @param title
     * @param message
     */
    public static void showFinishDialog(final Activity activity, final String title, final String message) {
        try {
            if (activity != null && !activity.isFinishing()) {
                AlertDialog.Builder builder = null;
                if (activity.getParent() != null)
                    builder = new AlertDialog.Builder(activity.getParent());
                else
                    builder = new AlertDialog.Builder(activity);

                if (title != null)
                    builder.setTitle(title);
                builder.setCancelable(false);
                builder.setIcon(DIALOG_ICON);
                if (message != null)
                    builder.setMessage(message);

                builder.setPositiveButton("OK",
                        (dialog, which) -> {
                            if (!activity.isFinishing()) {
                                dialog.dismiss();
                            }
                            Runtime.getRuntime().gc();
                            activity.finish();
                        });
                AlertDialog msg = builder.create();
                msg.setCancelable(false);
                msg.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface DialogListener {
        void onOk();
    }
}
