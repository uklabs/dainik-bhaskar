package com.db.appwidget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.data.database.AppWidgetTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.models.NewsListInfo;
import com.bhaskar.view.ui.splash.SplashActivity;
import com.db.util.AppPreferences;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListAppWidgetProvider extends AppWidgetProvider {

    //private static String FEED_URL = MyAppWidgetProvider.FEED_URL;

    public static String FEED_URL = "https://appfeedlight.bhaskar.com/appFeedV3/WidgetNews/521/521/1/";

    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        //  Toast.makeText(context, "update called", Toast.LENGTH_SHORT).show();
        Intent intent1 = new Intent(context, ListAppWidgetProvider.class);
        intent1.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent1.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        PendingIntent pending = PendingIntent.getBroadcast(context, Constants.AppWidgetIDS.LIST_WIDGET_AUTO_REFRESH_ID, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        final AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pending);
        long interval = 1000 * 60 * 60;
        alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + interval, pending);
//        alarm.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), interval, pending);
//        ---------------------------------------------------------------
        FEED_URL = AppPreferences.getInstance(context).getStringValue(QuickPreferences.WidgetPrefs.APP_WIDGET_FEED, FEED_URL);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, FEED_URL,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response != null && response.has("feed")) {
                                JSONArray array = response.getJSONArray("feed");
                                Gson gson = new Gson();
                                List<NewsListInfo> listInfos = new ArrayList<>();
                                for (int i = 0; i < array.length(); i++) {
                                    listInfos.add(gson.fromJson(array.get(i).toString(), NewsListInfo.class));
                                }

                                ((AppWidgetTable) DatabaseHelper.getInstance(context).getTableObject(AppWidgetTable.TABLE_NAME)).addAppWidgetList(listInfos, Constants.AppWidgetType.LIST_WIDGET_TYPE);
                                ComponentName thisWidget = new ComponentName(context, ListAppWidgetProvider.class);
                                int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
                                for (int widgetId : allWidgetIds) {
                                    RemoteViews remoteViews = updateWidgetListView(context, widgetId);
                                    appWidgetManager.notifyAppWidgetViewDataChanged(widgetId, R.id.list_view);
                                    appWidgetManager.updateAppWidget(widgetId, remoteViews);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
//        ---------------------------------------------------------------

        // Get all ids
        ComponentName thisWidget = new ComponentName(context, ListAppWidgetProvider.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        for (int widgetId : allWidgetIds) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.list_appwidget_layout);

            //refresh click
            Intent intent = new Intent(context, ListAppWidgetProvider.class);
            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.AppWidgetIDS.LIST_WIDGET_REFRESH_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.refresh, pendingIntent);
//            setAppLogoAndRefreshDrawables(context, remoteViews);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

//    private void setAppLogoAndRefreshDrawables(Context context, RemoteViews remoteViews) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            remoteViews.setImageViewResource(R.id.app_logo, R.drawable.ic_header_logo);
//            remoteViews.setImageViewResource(R.id.refresh, R.drawable.ic_aw_refresh);
//        } else {
//            Drawable d = AppCompatDrawableManager.get().getDrawable(context, R.drawable.ic_header_logo);
//            Bitmap b = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//            Canvas c = new Canvas(b);
//            d.setBounds(0, 0, c.getWidth(), c.getHeight());
//            d.draw(c);
//            remoteViews.setImageViewBitmap(R.id.app_logo, b);
//            d = AppCompatDrawableManager.get().getDrawable(context, R.drawable.ic_widget_refresh);
//            b = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//            c = new Canvas(b);
//            d.setBounds(0, 0, c.getWidth(), c.getHeight());
//            d.draw(c);
//            remoteViews.setImageViewBitmap(R.id.refresh, b);
//        }
//    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }

    private RemoteViews updateWidgetListView(Context context, int appWidgetId) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.list_appwidget_layout);
        Intent svcIntent = new Intent(context, ListWidgetListViewService.class);
        svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));
        remoteViews.setRemoteAdapter(R.id.list_view, svcIntent);
        remoteViews.setEmptyView(R.id.list_view, R.id.empty_view);

        // Trigger listview item click
        Intent startActivityIntent = new Intent(context, SplashActivity.class);
//        startActivityIntent.addFlags(/*Intent.FLAG_ACTIVITY_NEW_TASK | */Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent startActivityPendingIntent = PendingIntent.getActivity(context, Constants.AppWidgetIDS.LIST_WIDGET_ID, startActivityIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        remoteViews.setPendingIntentTemplate(R.id.list_view, startActivityPendingIntent);

        //refresh click
        /*Intent intent = new Intent(context, ListAppWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.refresh, pendingIntent);
        setAppLogoAndRefreshDrawables(context, remoteViews);*/

        return remoteViews;
    }
}