package com.db.appwidget;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bhaskar.R;
import com.db.data.database.AppWidgetTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.models.NewsListInfo;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.util.AppUrls;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.Urls;

import java.util.ArrayList;
import java.util.List;

class ListViewRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private Context context;
    private List<NewsListInfo> records;

    public ListViewRemoteViewsFactory(Context context, Intent intent) {
        this.context = context;
    }

    public void onCreate() {
        records = new ArrayList<>();
        records = ((AppWidgetTable) DatabaseHelper.getInstance(context).getTableObject(AppWidgetTable.TABLE_NAME)).getAllAppWidgetList(Constants.AppWidgetType.LIST_WIDGET_TYPE);
    }

    public RemoteViews getViewAt(int position) {
        RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.list_widget_item);

        NewsListInfo info = records.get(position);
        rv.setTextViewText(R.id.item, info.title);

        Bundle extras = new Bundle();
        Intent fillInIntent = new Intent();
        String domain = AppUrls.DOMAIN;
        String storyId = info.storyId;
        if (!TextUtils.isEmpty(storyId)) {
//            storyId = AppUtils.getFilteredStoryId(storyId);
//            storyId = storyId + "@widget_list/?v=" + info.version;
            storyId = storyId + "/?v=" + info.version;
        }
//        extras.putInt(Constants.KeyPair.IN_BACKGROUND, InitApplication.getInstance().isAppOpen());
        extras.putBoolean(Constants.KeyPair.KEY_IS_FROM_APPWIDGET, true);
        extras.putString(Constants.KeyPair.KEY_DETAIL_FEED_URL, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID));
        extras.putString(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, info.title);
        extras.putString(Constants.KeyPair.KEY_CHANNEL_SNO, CommonConstants.CHANNEL_ID);
        extras.putString(Constants.KeyPair.KEY_STORY_ID, storyId);
        extras.putString(Constants.KeyPair.KEY_CATEGORY_ID, "1");
        extras.putString(Constants.KeyPair.KEY_NEWS_TYPE, "1");
        extras.putString(Constants.KeyPair.KEY_GA_DISPLAY_NAME, AppFlyerConst.GAExtra.WIDGET_LIST);
        extras.putString(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAExtra.WIDGET_LIST);
        extras.putString(Constants.KeyPair.KEY_GA_ARTICLE, AppFlyerConst.GAExtra.WIDGET_LIST_ART);
        extras.putString(Constants.KeyPair.KEY_WISDOM_DOMAIN, domain);
        extras.putString(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN, AppFlyerConst.GAExtra.WIDGET_LIST);
        fillInIntent.putExtras(extras);
//        fillInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        rv.setOnClickFillInIntent(R.id.item_layout, fillInIntent);
        return rv;
    }

    public int getCount() {
        return records.size();
    }

    public void onDataSetChanged() {
    }

    public int getViewTypeCount() {
        return 1;
    }

    public long getItemId(int position) {
        return position;
    }

    public void onDestroy() {
        records.clear();
    }

    public boolean hasStableIds() {
        return true;
    }

    public RemoteViews getLoadingView() {
        return null;
    }
}