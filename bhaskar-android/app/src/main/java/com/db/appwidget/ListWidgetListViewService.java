package com.db.appwidget;

import android.content.Intent;
import android.widget.RemoteViewsService;

public class ListWidgetListViewService extends RemoteViewsService {

    public RemoteViewsService.RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ListViewRemoteViewsFactory(this.getApplicationContext(), intent);
    }
}