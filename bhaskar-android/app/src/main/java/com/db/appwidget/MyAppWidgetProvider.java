package com.db.appwidget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RemoteViews;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.request.target.AppWidgetTarget;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.database.AppWidgetTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.models.NewsListInfo;
import com.bhaskar.view.ui.splash.SplashActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyAppWidgetProvider extends AppWidgetProvider {

    private static final String ACTION_CLICK_NEXT = "appwidget.next";
    private static final String ACTION_CLICK_PREV = "appwidget.prev";
    public static String FEED_URL = CommonConstants.APP_WIDGET_STICKY_FEED_URL;
    private AppWidgetTarget appWidgetTarget;
    private AppWidgetTarget appWidgetTarget1;

    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        Intent intent1 = new Intent(context, ListAppWidgetProvider.class);
        intent1.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent1.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        PendingIntent pending = PendingIntent.getBroadcast(context, Constants.AppWidgetIDS.APP_WIDGET_AUTO_REFRESH_ID, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        final AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pending);
        long interval = 1000 * 60 * 60;
        alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + interval, pending);
//        ---------------------------------------------------------------
        FEED_URL = AppPreferences.getInstance(context).getStringValue(QuickPreferences.WidgetPrefs.APP_WIDGET_FEED, FEED_URL);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, FEED_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response != null && response.has("feed")) {
                                JSONArray array = response.getJSONArray("feed");
                                Gson gson = new Gson();
                                List<NewsListInfo> listInfos = new ArrayList<>();
                                for (int i = 0; i < array.length(); i++) {
                                    listInfos.add(gson.fromJson(array.get(i).toString(), NewsListInfo.class));
                                }
                                ((AppWidgetTable) DatabaseHelper.getInstance(context).getTableObject(AppWidgetTable.TABLE_NAME)).addAppWidgetList(listInfos, Constants.AppWidgetType.APP_WIDGET_TYPE);
                                ComponentName thisWidget = new ComponentName(context, MyAppWidgetProvider.class);
                                int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
                                NewsListInfo info = listInfos.get(0);
                                for (int widgetId : allWidgetIds) {
                                    RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.appwidget_layout);

                                    remoteViews.setImageViewResource(R.id.feed_image, R.drawable.water_mark_news_detail);
                                    appWidgetTarget = new AppWidgetTarget(context, R.id.feed_image, remoteViews, allWidgetIds);

                                    String imagePath = "xyz";
                                    if (TextUtils.isEmpty(info.imagebigsize)) {
                                        if (!TextUtils.isEmpty(info.image)) {
                                            imagePath = info.image;
                                        }
                                    } else {
                                        imagePath = info.imagebigsize;
                                    }

                                    ImageUtil.setImageWidget(context, imagePath, appWidgetTarget, 0);

                                    remoteViews.setTextViewText(R.id.feed_title, AppUtils.getInstance().removeAndReplaceHtmlSymbols(info.title));
                                    int currentPos = 0;
                                    if (listInfos != null && currentPos == listInfos.size() - 1) {
                                        remoteViews.setViewVisibility(R.id.next, View.GONE);
                                        remoteViews.setViewVisibility(R.id.prev, View.VISIBLE);
                                    } else if (listInfos != null && currentPos == 0) {
                                        remoteViews.setViewVisibility(R.id.next, View.VISIBLE);
                                        remoteViews.setViewVisibility(R.id.prev, View.GONE);
                                    }
                                    remoteViews.setOnClickPendingIntent(R.id.feed_image, getSplashPendingIntent(context, info, 0));
                                    remoteViews.setOnClickPendingIntent(R.id.feed_title, getSplashPendingIntent(context, info, 1));
                                    AppPreferences.getInstance(context).setIntValue(QuickPreferences.APP_WIDGET_KEY_CURRENT_POS, currentPos);
                                    appWidgetManager.updateAppWidget(widgetId, remoteViews);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
//        ---------------------------------------------------------------

        // Get all ids
        ComponentName thisWidget = new ComponentName(context, MyAppWidgetProvider.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        for (int widgetId : allWidgetIds) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.appwidget_layout);
            //refresh click
            Intent intent = new Intent(context, MyAppWidgetProvider.class);
            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.AppWidgetIDS.APP_WIDGET_REFRESH_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.refresh, pendingIntent);

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                remoteViews.setImageViewResource(R.id.app_logo, R.drawable.ic_header_logo);
//                remoteViews.setImageViewResource(R.id.refresh, R.drawable.ic_aw_refresh);
//            } else {
//                Drawable d = AppCompatDrawableManager.get().getDrawable(context, R.drawable.ic_header_logo);
//                Bitmap b = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//                Canvas c = new Canvas(b);
//                d.setBounds(0, 0, c.getWidth(), c.getHeight());
//                d.draw(c);
//                remoteViews.setImageViewBitmap(R.id.app_logo, b);
//                d = AppCompatDrawableManager.get().getDrawable(context, R.drawable.ic_aw_refresh);
//                b = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//                c = new Canvas(b);
//                d.setBounds(0, 0, c.getWidth(), c.getHeight());
//                d.draw(c);
//                remoteViews.setImageViewBitmap(R.id.refresh, b);
//            }

            //next click
            remoteViews.setOnClickPendingIntent(R.id.next, getPendingSelfIntent(context, ACTION_CLICK_NEXT));
            //prev click
            remoteViews.setOnClickPendingIntent(R.id.prev, getPendingSelfIntent(context, ACTION_CLICK_PREV));

            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        try {
            NewsListInfo info = null;
            List<NewsListInfo> listInfos = ((AppWidgetTable) DatabaseHelper.getInstance(context).getTableObject(AppWidgetTable.TABLE_NAME)).getAllAppWidgetList(Constants.AppWidgetType.APP_WIDGET_TYPE);
            ComponentName watchWidget = new ComponentName(context, MyAppWidgetProvider.class);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.appwidget_layout);
            int[] allWidgetIds = appWidgetManager.getAppWidgetIds(watchWidget);
            int currentPos = AppPreferences.getInstance(context).getIntValue(QuickPreferences.APP_WIDGET_KEY_CURRENT_POS, 0);

            if (listInfos == null || listInfos.size() == 0) {
                onUpdate(context, appWidgetManager, allWidgetIds);
                return;
            }

            for (int widgetId : allWidgetIds) {
                if (ACTION_CLICK_NEXT.equals(intent.getAction())) {
                    if (currentPos == listInfos.size() - 1) {
                        this.onUpdate(context, appWidgetManager, allWidgetIds);
                    } else {
                        currentPos++;
                    }
                    info = listInfos.get(currentPos);
                } else if (ACTION_CLICK_PREV.equals(intent.getAction()) && listInfos != null && currentPos > 0) {
                    currentPos--;
                    info = listInfos.get(currentPos);
                }
                remoteViews.setImageViewResource(R.id.feed_image, R.drawable.water_mark_news_detail);
                String imagePath = "xyz";
                if (TextUtils.isEmpty(info.imagebigsize)) {
                    if (!TextUtils.isEmpty(info.image)) {
                        imagePath = info.image;
                    }
                } else {
                    imagePath = info.imagebigsize;
                }
                appWidgetTarget1 = new AppWidgetTarget(context, R.id.feed_image, remoteViews, allWidgetIds);

                ImageUtil.setImageWidget(context, imagePath, appWidgetTarget1, 0);

                remoteViews.setTextViewText(R.id.feed_title, info.title);
                remoteViews.setOnClickPendingIntent(R.id.feed_image, getSplashPendingIntent(context, info, 0));
                remoteViews.setOnClickPendingIntent(R.id.feed_title, getSplashPendingIntent(context, info, 1));
                AppPreferences.getInstance(context).setIntValue(QuickPreferences.APP_WIDGET_KEY_CURRENT_POS, currentPos);
                if (listInfos != null && currentPos == listInfos.size() - 1) {
                    remoteViews.setViewVisibility(R.id.next, View.VISIBLE);
                    remoteViews.setViewVisibility(R.id.prev, View.VISIBLE);
                } else if (listInfos != null && currentPos == 0) {
                    remoteViews.setViewVisibility(R.id.next, View.VISIBLE);
                    remoteViews.setViewVisibility(R.id.prev, View.GONE);
                } else {
                    remoteViews.setViewVisibility(R.id.next, View.VISIBLE);
                    remoteViews.setViewVisibility(R.id.prev, View.VISIBLE);
                }
                appWidgetManager.updateAppWidget(widgetId, remoteViews);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    protected PendingIntent getSplashPendingIntent(Context context, NewsListInfo info, int type) {
        String domain = AppUrls.DOMAIN;
        String storyId = info.storyId;
       /* if (!TextUtils.isEmpty(storyId)) {
//            storyId = AppUtils.getFilteredStoryId(storyId);
//            storyId = storyId + "@widget_thumb/?v=" + info.version;
            storyId = storyId + "/?v=" + info.version;
        }*/
        Intent intent = new Intent(context, SplashActivity.class);
        intent.putExtra(Constants.KeyPair.IN_BACKGROUND, InitApplication.getInstance().isAppOpen());
        intent.putExtra(Constants.KeyPair.KEY_IS_FROM_APPWIDGET, true);
        intent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID));
        intent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, info.title);
        intent.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, CommonConstants.CHANNEL_ID);
        intent.putExtra(Constants.KeyPair.KEY_STORY_ID, storyId);
        intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, "1");
        intent.putExtra(Constants.KeyPair.KEY_NEWS_TYPE, "1");
        intent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, AppFlyerConst.GAExtra.WIDGET_THUMB);
        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAExtra.WIDGET_THUMB);
        intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, AppFlyerConst.GAExtra.WIDGET_THUMB_ART);
        intent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, domain);
        intent.putExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN, AppFlyerConst.GAExtra.WIDGET_THUMB);
//        intent.addFlags(/*Intent.FLAG_ACTIVITY_NEW_TASK | */Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return PendingIntent.getActivity(context, type == 0 ? Constants.AppWidgetIDS.APP_WIDGET_ID_1 : Constants.AppWidgetIDS.APP_WIDGET_ID_2, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
    }
}