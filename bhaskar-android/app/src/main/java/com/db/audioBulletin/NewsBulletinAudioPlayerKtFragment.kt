package com.db.audioBulletin

import android.annotation.SuppressLint
import android.content.*
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import com.bhaskar.R
import com.db.audioBulletin.mediaPlayer.Audio
import com.db.audioBulletin.mediaPlayer.MediaPlayerService
import com.db.audioBulletin.mediaPlayer.StorageUtil
import com.db.audioBulletin.model.NewsBulletinItem
import com.db.core.CoreFragment
import com.db.home.RadioService
import com.db.util.AppUtils
import com.db.util.Constants
import com.db.util.ImageUtil
import kotlinx.android.synthetic.main.fragment_news_bulletin_audio_player.*
import java.util.*
import java.util.concurrent.TimeUnit

class NewsBulletinAudioPlayerKtFragment : CoreFragment() {
    private var mAudioIndex: Int = 0
    private var activity: AppCompatActivity? = null
    private var menuType = ""
    private var mNewsBulletinList: ArrayList<NewsBulletinItem>? = null
    private var mNewsBulletinItem: NewsBulletinItem? = null
    private var mAudioStartedAlready = false
    private var mFreshLaunch = false

    private var mAudioList: ArrayList<Audio>? = null
    private val TAG = NewsBulletinAudioPlayerKtFragment::class.java.simpleName
    private var mHandler: Handler? = Handler()
    private var mPlayOrPauseHandler: Handler? = Handler()
    private val ONE_HOUR = 60 * 60 * 1000

    companion object {
        var smIsPlaying: Boolean = false
        var smServiceBound = false
        var smPlayerService: MediaPlayerService? = null

        fun newInstance(items: ArrayList<NewsBulletinItem>?, index: Int?, freshLaunch: Boolean?): NewsBulletinAudioPlayerKtFragment {
            val playerFragment = NewsBulletinAudioPlayerKtFragment()

            val bundle = Bundle()
            index?.let { bundle.putInt(Constants.NewsBulletin.KEY_SELECTED_NEWS_BULLETIN_INDEX, it) }
            freshLaunch?.let { bundle.putBoolean(Constants.NewsBulletin.IS_FIRST_LAUNCH, it) }
            //bundle.putString(Constants.NewsBulletin.KEY_MENU_TYPE, menuType)
            bundle.putParcelableArrayList(Constants.NewsBulletin.KEY_NEWS_BULLETIN_LIST, items)
            playerFragment.arguments = bundle
            return playerFragment
        }

        fun stopPlayback() {
            if (smServiceBound) {
                //service is active
                if (smPlayerService != null)
                    smPlayerService?.stopThisService()
            }

            smServiceBound = false
            smPlayerService = null
            smIsPlaying = false
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.activity = context as AppCompatActivity
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news_bulletin_audio_player, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bundle = arguments
        if (bundle != null) {
            //menuType = bundle.getString(Constants.NewsBulletin.KEY_MENU_TYPE, CategoryInfo)
            mNewsBulletinList = bundle.getParcelableArrayList<NewsBulletinItem>(Constants.NewsBulletin.KEY_NEWS_BULLETIN_LIST)
            mAudioIndex = bundle.getInt(Constants.NewsBulletin.KEY_SELECTED_NEWS_BULLETIN_INDEX)
            mNewsBulletinList?.let { newsBulletinList ->
                if (newsBulletinList.size > 0) {
                    mNewsBulletinItem = newsBulletinList.get(mAudioIndex)
                    if (null != mNewsBulletinItem) {
                        // Track CleverTap page view
                        /*if (menuType.equals(AppConstants.MENU_TYPE_NEWS_BULLETIN, ignoreCase = true)) {
                            trackPageView(mNewsBulletinItem?.mAudioStreamURL, menuType)
                        } else {
                            trackPageView(CTConstant.SpecialContentValues.AUDIO_BULLETIN, CTConstant.SpecialContentValues.CONTENT_CATEGORY)
                        }*/
                    }
                }
            }
            mFreshLaunch = bundle.getBoolean(Constants.NewsBulletin.IS_FIRST_LAUNCH)
        }
        initViews();
    }

    protected fun initViews() {
        mNewsBulletinItem?.let { newsBulletinItem ->
            ImageUtil.setImage(dbApp, newsBulletinItem.mImageURL, mThumbnailImage, R.mipmap.app_icon)
        }

        mRootView.setOnTouchListener { view1, motionEvent ->
            activity?.finish()
            true
        }

        mPlayPauseButton.setOnClickListener { playPause() }

        mForwardButton.setOnClickListener { forward() }

        mBackwardButton.setOnClickListener { backward() }

        mSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (fromUser)
                    seekTo(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })
        mClose.setOnClickListener {
            activity?.finish()
            stopPlayback()
            NewsBulletinListingFragment.smCurrentNewsBulletinItem = null
        }
        loadAudio()
        if (!mFreshLaunch) {
            reload()
        } else {
            stopPlayback()
        }

        registerPlayerEventReceiver()
    }

    private fun registerPlayerEventReceiver() {
        val filer = IntentFilter(MediaPlayerService.Broadcast_PLAYER_EVENT)
        activity?.registerReceiver(playerEventReceiver, filer)
    }

    private val playerEventReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val state = intent.getIntExtra(MediaPlayerService.PLAYER_STATE, -1)
            handlePlayerState(state)
        }
    }

    private fun handlePlayerState(state: Int) {
        try {
            if (state == MediaPlayerService.PlayerState.PLAYING.ordinal) {
                smIsPlaying = true
                mPlayPauseButton.setImageResource(R.drawable.ic_news_bulletin_pause)
            } else if (state == MediaPlayerService.PlayerState.COMPLETED.ordinal) {
                smIsPlaying = false
                mPlayPauseButton.setImageResource(R.drawable.ic_news_bulletin_play)
            } else if (state == MediaPlayerService.PlayerState.PAUSED.ordinal) {
                smIsPlaying = false
                mPlayPauseButton.setImageResource(R.drawable.ic_news_bulletin_play)
            } else if (state == MediaPlayerService.PlayerState.PREPARED.ordinal) {
                updateFinalTime()
            } else if (state == MediaPlayerService.PlayerState.STOPPED.ordinal) {
                activity?.finish()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun playPause() {
        if (!smIsPlaying) {
            if (mAudioStartedAlready)
                resumeAudio()
            else
                startAudio(mAudioIndex)

            smIsPlaying = true
            mPlayPauseButton.setImageResource(R.drawable.ic_news_bulletin_pause)
        } else {
            pauseAudio()
            smIsPlaying = false
            mPlayPauseButton.setImageResource(R.drawable.ic_news_bulletin_play)
        }
    }

    private fun checkAndStopAudioMediaService() {
        if (AppUtils.isMyServiceRunning(dbApp, RadioService::class.java.name)) {
            RadioService.stopTrack(dbApp)
        }

        if (AppUtils.isMyServiceRunning(dbApp, MediaPlayerService::class.java.name)) {
            MediaPlayerService.stopTrack(dbApp)
        }
    }

    private fun startAudio(audioIndex: Int) {
        try {
            checkAndStopAudioMediaService();
            //Check is service is active
            val storage = StorageUtil(activity?.applicationContext)
            if (!smServiceBound) {
                //Store Serializable audioList to SharedPreferences

                storage.storeAudio(mAudioList)
                storage.storeAudioIndex(audioIndex)

                val playerIntent = Intent(activity, MediaPlayerService::class.java)
                playerIntent.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_LIST, mNewsBulletinList)
                playerIntent.putExtra(Constants.NewsBulletin.KEY_SELECTED_NEWS_BULLETIN_INDEX, mAudioIndex)
                playerIntent.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_FRESH_LAUNCH, mFreshLaunch)
                activity?.startService(playerIntent)

                activity?.bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE)
                Log.d(TAG, "startAudio bindService MediaPlayerService")
            } else {
                //Store the new audioIndex to SharedPreferences
                storage.storeAudioIndex(audioIndex)

                //Service is active
                //Send a broadcast to the service -> PLAY_NEW_AUDIO
                val broadcastIntent = Intent(MediaPlayerService.Broadcast_PLAY_NEW_AUDIO)
                activity?.sendBroadcast(broadcastIntent)
            }
            updateProgress()
        } catch (e: Exception) {
            Log.e(TAG, "startAudio: " + e.message)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPlayOrPauseHandler?.post(Runnable {
            if (mFreshLaunch)
                playPause()
        })
        if (smPlayerService != null)
            smPlayerService?.hideWidget()
    }

    //Binding this Client to the AudioPlayer Service
    val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as MediaPlayerService.LocalBinder
            smPlayerService = binder.service
            smPlayerService?.setServiceConnection(this)
            smServiceBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            smServiceBound = false
        }
    }

    private fun pauseAudio() {
        if (smPlayerService != null) {
            smPlayerService?.pause()
        }
    }

    private fun resumeAudio() {
        if (smPlayerService != null) {
            smPlayerService?.resume()
        }
    }

    private fun forward() {
        if (smPlayerService != null)
            smPlayerService?.fastForward()
    }

    private fun backward() {
        if (smPlayerService != null)
            smPlayerService?.rewind()
    }

    fun seekTo(position: Int) {
        if (smPlayerService != null)
            smPlayerService?.seekTo(position)
    }

    private fun loadAudio() {
        try {
            if (mNewsBulletinList != null && mNewsBulletinList!!.size > 0) {
                mAudioList = ArrayList<Audio>()
                for (item in mNewsBulletinList!!) {
                    mAudioList?.add(Audio(item.mAudioStreamURL, item.mTitle, "", "", item.mImageURL, item.mNId))
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, e.message + "")
        }
    }

    private fun reload() {
        if (smIsPlaying)
            mPlayPauseButton.setImageResource(R.drawable.ic_news_bulletin_pause)
        else
            mPlayPauseButton.setImageResource(R.drawable.ic_news_bulletin_play)
        updateProgress()
        updateFinalTime()
    }

    private fun updateProgress() {
        mHandler?.post(UpdatePlayTime)
    }

    private val UpdatePlayTime = object : Runnable {
        @SuppressLint("DefaultLocale")
        override fun run() {
            try {
                if (smPlayerService != null) {
                    val startTime = smPlayerService?.currentPosition
                    if (startTime != null) {
                        if (!mAudioStartedAlready && startTime > 0)
                            mAudioStartedAlready = true
                    }
                    if (startTime != null && startTime >= ONE_HOUR) {
                        mCurrentPlayingTime.text = String.format("%02d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(startTime.toLong()),
                                TimeUnit.MILLISECONDS.toMinutes(startTime.toLong()) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(startTime.toLong())),
                                TimeUnit.MILLISECONDS.toSeconds(startTime.toLong()) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(startTime.toLong())))
                    } else {
                        if (startTime != null) {
                            mCurrentPlayingTime.text = String.format("%02d:%02d",
                                    TimeUnit.MILLISECONDS.toMinutes(startTime.toLong()),
                                    TimeUnit.MILLISECONDS.toSeconds(startTime.toLong()) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(startTime.toLong())))
                        }
                    }
                    startTime?.let { mSeekBar.setProgress(it) }
                }
                mHandler?.postDelayed(this, 100)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
    }

    private fun updateFinalTime() {
        if (smPlayerService != null) {
            val finalTime = smPlayerService?.duration
            if (finalTime != null) {
                mSeekBar.max = finalTime
            }
            if (finalTime != null) {
                if (finalTime >= ONE_HOUR) {
                    mTotalTime.text = String.format("%02d:%02d:%02d",
                            TimeUnit.MILLISECONDS.toHours(finalTime.toLong()),
                            TimeUnit.MILLISECONDS.toMinutes(finalTime.toLong()) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(finalTime.toLong())),
                            TimeUnit.MILLISECONDS.toSeconds(finalTime.toLong()) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(finalTime.toLong())))
                } else {
                    mTotalTime.text = String.format("%02d:%02d",
                            TimeUnit.MILLISECONDS.toMinutes(finalTime.toLong()),
                            TimeUnit.MILLISECONDS.toSeconds(finalTime.toLong()) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(finalTime.toLong())))
                }
            }
        }
    }

    private fun unregisterPlayerEventReceiver() {
        try {
            activity?.unregisterReceiver(playerEventReceiver)
        } catch (e: Exception) {
            Log.e(TAG, e.message + "")
        }

    }

    override fun onDestroyView() {

        try {
            if (null != serviceConnection) {
                activity?.unbindService(serviceConnection)
            }
        } catch (e: Exception) {
        }
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (smPlayerService != null) {
            if (smIsPlaying) {
                smPlayerService?.showWidget()
            } else {
                smPlayerService?.hideWidget()
            }
        }
        unregisterPlayerEventReceiver()
        clear()
    }

    private fun clear() {
        if (mHandler != null) {
            mHandler?.removeCallbacks(UpdatePlayTime)
            mHandler = null
        }
        if (mPlayOrPauseHandler != null) {
            mPlayOrPauseHandler?.removeCallbacksAndMessages(null)
            mPlayOrPauseHandler = null
        }
    }

}