package com.db.audioBulletin

import android.app.DatePickerDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bhaskar.R
import com.bhaskar.data.api.ApiClient
import com.bhaskar.data.api.ApiHelper
import com.bhaskar.util.Action
import com.bhaskar.util.CommonConstants
import com.db.audioBulletin.mediaPlayer.MediaPlayerService
import com.db.audioBulletin.model.NewsBulletinItem
import com.db.audioBulletin.model.NewsBulletinListResponse
import com.db.audioBulletin.viewItems.NewsBulletinDateViewItem
import com.db.audioBulletin.viewItems.NewsBulletinSeparatorViewItem
import com.db.audioBulletin.viewItems.NewsBulletinSpaceViewItem
import com.db.audioBulletin.viewItems.NewsBulletinViewItem
import com.db.core.CoreFragment
import com.db.data.models.CategoryInfo
import com.db.recycler_adapter.RecyclerAdapter
import com.db.util.*
import kotlinx.android.synthetic.main.fragment_news_bulletin_listing_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class NewsBulletinListingFragment : CoreFragment(), NewsBulletinViewItem.OnItemClickListener {

    private var mNewsBulletinId: String? = null
    private var mCatId: String? = null
    private var isNewsBulletinAutoPlay: Boolean = false
    private var mAdapter: RecyclerAdapter? = null
    private var mSwipedToRefresh: Boolean = false
    private var isDatePickerOpened: Boolean = false
    private var mCurrentlySelectedCalendar: Calendar? = Calendar.getInstance()

    companion object {
        private val TAG = NewsBulletinListingFragment::class.java.simpleName

        var smCurrentNewsBulletinItem: NewsBulletinItem? = null
        fun newInstance(categoryInfo: CategoryInfo): NewsBulletinListingFragment {
            val newsBulletinFragment = NewsBulletinListingFragment()
            val bundle = Bundle()
            bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo)
            newsBulletinFragment.arguments = bundle
            return newsBulletinFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news_bulletin_listing_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        readFromBundle()
        setAdapter()
        setSwipeReffreshListener()
        calendar_layout.setOnClickListener {
            if (!isDatePickerOpened) {
                showDatePicker()
            }
        }
        clear_filter.setOnClickListener { clearFilter() }
        registerPlayerEventReceiver()
    }


    private fun readFromBundle() {
        val bundle = arguments
        if (bundle != null) {
            val catInfo: CategoryInfo = bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO) as CategoryInfo
            mCatId = catInfo.id
        }
    }

    private fun setAdapter() {
        mAdapter = RecyclerAdapter()
        mAdapter?.setItemAnimationType(RecyclerAdapter.ANIMATION_TYPE_LIST_ITEM)
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        news_bulletin_list.layoutManager = layoutManager
        news_bulletin_list.adapter = mAdapter
    }

    private fun getNewsBulletinList(minDate: String?, maxDate: String?) {
        val pDialog = ProgressbarDialog(activity)
        if (!mSwipedToRefresh) {
            pDialog.show(getString(R.string.please_wait), true)
        }
        var finalUrl: String? = null
        if (!TextUtils.isEmpty(minDate) && !TextUtils.isEmpty(maxDate)) {
            finalUrl = Urls.APP_FEED_BASE_URL + Action.CategoryAction.CAT_ACTION_AUDIO_BULLETIN_LIST + "/" + CommonConstants.CHANNEL_ID + "/" + "?created[min]=" + minDate + "&created[max]=" + maxDate
        } else {
            finalUrl = Urls.APP_FEED_BASE_URL + Action.CategoryAction.CAT_ACTION_AUDIO_BULLETIN_LIST + "/" + CommonConstants.CHANNEL_ID + "/"
        }

        Log.d(TAG, "NewsBulletinList : $finalUrl")
        ApiClient.createService(ApiHelper::class.java).getNewsBulletinList(finalUrl).enqueue(object : Callback<NewsBulletinListResponse> {
            override fun onResponse(call: Call<NewsBulletinListResponse>, response: Response<NewsBulletinListResponse>) {
                if (response.isSuccessful) {
                    val newsBulletinList = response.body()?.data?.newsBulletinList
                    if (!newsBulletinList.isNullOrEmpty()) {
                        setNewsBulletinList(newsBulletinList)
                    } else {
                        removeAllViews()
                    }
                    pDialog.dismiss()
                    stopSwipeDownRefreshProgressBar();
                }
            }

            override fun onFailure(call: Call<NewsBulletinListResponse>, t: Throwable) {
                Log.e(TAG, "getNewsBulletinList: ${t.message}")
                pDialog.dismiss()
                stopSwipeDownRefreshProgressBar()
                removeAllViews()

            }
        })
    }

    private fun setNewsBulletinList(list: List<NewsBulletinItem>?) {
        var firstNewsBulletinItem: NewsBulletinItem? = null
        if (list != null && list.isNotEmpty()) {
            val itemsMap = LinkedHashMap<String, ArrayList<NewsBulletinItem>>()
            for (item in list) {
                item.mCreatedDateTime?.let {
                    val date = it.substring(0, 15)
                    if (itemsMap.containsKey(date)) {
                        val items = itemsMap[date]
                        items?.add(item)
                        itemsMap[date] = items!!
                    } else {
                        val items = ArrayList<NewsBulletinItem>()
                        items.add(item)
                        itemsMap[date] = items
                    }
                }

            }
            var mainIndex = 0
            for ((key, items) in itemsMap) {
                val newsBulletinDateViewItem = NewsBulletinDateViewItem()
                val formattedDate = DateTimeUtil.getFormattedDateString(key, DateTimeUtil.NEWS_BULLETIN_DATE_FORMAT,
                        DateTimeUtil.NEWS_BULLETIN_DATE_DISPLAY_FORMAT)
                newsBulletinDateViewItem.data = getString(R.string.news_bulletin_date, formattedDate)
                mAdapter?.add(newsBulletinDateViewItem)
                for (index in items.indices) {
                    val item = items[index]
                    if (mainIndex == 0 && index == 0) {
                        firstNewsBulletinItem = item
                    }
                    val newsBulletinViewItem = NewsBulletinViewItem(context)
                    newsBulletinViewItem.setOnItemClickListener(this)
                    newsBulletinViewItem.data = item
                    mAdapter?.add(newsBulletinViewItem)
                    if (items.indexOf(item) < items.size - 1)
                        mAdapter?.add(NewsBulletinSeparatorViewItem())
                }
                mAdapter?.add(NewsBulletinSpaceViewItem())
                mainIndex++
            }
        }

        checkForNewsBulletinAutoPlay(firstNewsBulletinItem)
    }

    private fun checkForNewsBulletinAutoPlay(firstNewsBulletinItem: NewsBulletinItem?) {
        if (isNewsBulletinAutoPlay && !mSwipedToRefresh) {
            Handler().postDelayed({
                if (firstNewsBulletinItem != null) {
                    launchNewsBulletinAudioPlayer(firstNewsBulletinItem)
                }
            }, 250)
        }
    }

    private fun removeAllViews() {
        mAdapter?.removeAllItemsWithClass(NewsBulletinViewItem::class.java)
        mAdapter?.removeAllItemsWithClass(NewsBulletinDateViewItem::class.java)
        mAdapter?.removeAllItemsWithClass(NewsBulletinSpaceViewItem::class.java)
        mAdapter?.removeAllItemsWithClass(NewsBulletinSeparatorViewItem::class.java)
    }

    private fun showDatePicker() {
        if (NetworkStatus.getInstance().isConnected(context)) {
            isDatePickerOpened = true
            val datePickerDialog = mCurrentlySelectedCalendar?.get(Calendar.MONTH)?.let {
                DatePickerDialog(activity, { _, year, monthOfYear, dayOfMonth ->
                    mCurrentlySelectedCalendar!!.set(year, monthOfYear, dayOfMonth, 0, 0, 0)
                    showDateAndMonth()
                    fetchNewsBulletin(true)
                    isDatePickerOpened = false
                }, mCurrentlySelectedCalendar!!.get(Calendar.YEAR), it,
                        mCurrentlySelectedCalendar!!.get(Calendar.DAY_OF_MONTH))
            }
            datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
            datePickerDialog?.setOnCancelListener {
                isDatePickerOpened = false
            }
            datePickerDialog?.show()
        } else {
            DBToast.showAlertToast(context, getString(R.string.alert_title_alert), getString(R.string.alert_network_not_exist))
        }
    }

    private fun clearFilter() {
        if (NetworkStatus.getInstance().isConnected(context)) {
            mCurrentlySelectedCalendar = Calendar.getInstance()
            hideDateAndMonth()
            fetchNewsBulletin(false)
        } else {
            DBToast.showAlertToast(activity, getString(R.string.alert_title_alert), getString(R.string.alert_network_not_exist))
        }
    }

    private fun hideDateAndMonth() {
        date_month_separator.visibility = View.GONE
        date_month_layout.visibility = View.GONE
    }

    private fun showDateAndMonth() {
        try {
            if (null != mCurrentlySelectedCalendar) {
                val selectedDate = mCurrentlySelectedCalendar?.time
                if (null != selectedDate) {
                    month.text = DateTimeUtil.getStringFromDate(selectedDate, DateTimeUtil.MONTH_FORMAT)
                    date.text = DateTimeUtil.getStringFromDate(selectedDate, DateTimeUtil.DATE_FORMAT)
                    date_month_separator.visibility = View.VISIBLE
                    date_month_layout.visibility = View.VISIBLE
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, "showDateAndMonth: " + e.message)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (NetworkStatus.getInstance().isConnected(context)) {
            fetchNewsBulletin(false)
            if (!TextUtils.isEmpty(mNewsBulletinId)) {
//                fetchNewsBulletinDetails()
            }
        } else {
            DBToast.showAlertToast(activity, getString(R.string.alert_title_alert), getString(R.string.alert_network_not_exist))
        }
    }

    private fun setSwipeReffreshListener() {
        containerSwipeToRefresh.setOnRefreshListener {
            if (NetworkStatus.getInstance().isConnected(context)) {
                mSwipedToRefresh = true
                if (date_month_layout.visibility == View.VISIBLE) {
                    fetchNewsBulletin(true)
                } else {
                    fetchNewsBulletin(false)
                }
            } else {
                stopSwipeDownRefreshProgressBar()
            }
        }
    }

    private fun fetchNewsBulletin(filterApplied: Boolean) {
        var minDate = ""
        var maxDate = ""
        if (filterApplied) {
            mCurrentlySelectedCalendar?.set(Calendar.HOUR_OF_DAY, 0)
            mCurrentlySelectedCalendar?.set(Calendar.MINUTE, 0)
            mCurrentlySelectedCalendar?.set(Calendar.SECOND, 0)
            minDate = DateTimeUtil.getStringFromDate(mCurrentlySelectedCalendar?.time, DateTimeUtil.NEWS_BULLETIN_DATE_REQUEST_FORMAT)

            mCurrentlySelectedCalendar?.set(Calendar.HOUR_OF_DAY, 23)
            mCurrentlySelectedCalendar?.set(Calendar.MINUTE, 59)
            mCurrentlySelectedCalendar?.set(Calendar.SECOND, 59)
            maxDate = DateTimeUtil.getStringFromDate(mCurrentlySelectedCalendar?.time, DateTimeUtil.NEWS_BULLETIN_DATE_REQUEST_FORMAT)
            clear_filter.visibility = View.VISIBLE
        } else {
            clear_filter.visibility = View.GONE
        }
        if (filterApplied) {
            getNewsBulletinList(minDate, maxDate)
        } else {
            getNewsBulletinList(null, null)
        }
    }

    private fun stopSwipeDownRefreshProgressBar() {
        mSwipedToRefresh = false
        containerSwipeToRefresh.isRefreshing = false
    }

    override fun onDestroyView() {
        mAdapter = null
        containerSwipeToRefresh.setOnRefreshListener(null)
        unregisterPlayerEventReceiver()
        super.onDestroyView()
    }

    private fun registerPlayerEventReceiver() {
        val filter = IntentFilter(MediaPlayerService.Broadcast_PLAYER_EVENT)
        coreActivity.registerReceiver(playerEventReceiver, filter)
    }

    private fun unregisterPlayerEventReceiver() {
        coreActivity.unregisterReceiver(playerEventReceiver)
    }

    private val playerEventReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val state = intent.getIntExtra(MediaPlayerService.PLAYER_STATE, -1)
            handleVisualizerState(state)
        }
    }

    private fun handleVisualizerState(state: Int) {
        try {
            when (state) {
                MediaPlayerService.PlayerState.PLAYING.ordinal -> NewsBulletinAudioPlayerKtFragment.smIsPlaying = true
                MediaPlayerService.PlayerState.PAUSED.ordinal -> NewsBulletinAudioPlayerKtFragment.smIsPlaying = false
                MediaPlayerService.PlayerState.COMPLETED.ordinal -> {
                    NewsBulletinAudioPlayerKtFragment.smIsPlaying = false
                    smCurrentNewsBulletinItem = null
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    /**
     * @param newsBulletinItem
     */
    private fun launchNewsBulletinAudioPlayer(newsBulletinItem: NewsBulletinItem) {
        var freshLaunch = true
        if (smCurrentNewsBulletinItem != null && newsBulletinItem.mNId == smCurrentNewsBulletinItem!!.mNId) {
            freshLaunch = false
        }
        val items = ArrayList<NewsBulletinItem>()
        items.add(newsBulletinItem)

        NewsBulletinPlayerKtActivity.startNewsBulletinPlayerActivity(coreActivity,
                items, 0, freshLaunch)
        smCurrentNewsBulletinItem = newsBulletinItem
    }

    override fun onItemClick(viewPosition: Int, item: NewsBulletinItem?, view: View?) {
        if (item != null) {
            launchNewsBulletinAudioPlayer(item)
        }
    }

}
