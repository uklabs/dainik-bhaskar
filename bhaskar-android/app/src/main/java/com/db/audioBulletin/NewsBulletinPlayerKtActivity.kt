package com.db.audioBulletin

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import com.bhaskar.R
import com.bhaskar.data.api.ApiClient
import com.bhaskar.data.api.ApiHelper
import com.bhaskar.data.api.RetroCallbackProvider
import com.bhaskar.util.Action
import com.bhaskar.util.CommonConstants
import com.db.audioBulletin.mediaPlayer.MediaPlayerService
import com.db.audioBulletin.model.NewsBulletinItem
import com.db.audioBulletin.model.NewsBulletinResponse
import com.db.core.CoreActivity
import com.db.util.*
import retrofit2.Call
import java.util.*

class NewsBulletinPlayerKtActivity : CoreActivity() {

    companion object{
        private val TAG = NewsBulletinPlayerKtActivity::class.java.simpleName
        private val CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084

        fun startNewsBulletinPlayerActivity(context: Context, newsBulletinNId: String ) {
            val intent = Intent(context, NewsBulletinPlayerKtActivity::class.java)
            intent.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_ID, newsBulletinNId)
            context.startActivity(intent)
        }

        fun startNewsBulletinPlayerActivity(context: Context, items: ArrayList<NewsBulletinItem>?, index: Int,
                                                          freshLaunch: Boolean) {
            val intent = Intent(context, NewsBulletinPlayerKtActivity::class.java)
            intent.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_LIST, items)
            intent.putExtra(Constants.NewsBulletin.KEY_SELECTED_NEWS_BULLETIN_INDEX, index)
            intent.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_FRESH_LAUNCH, freshLaunch)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.news_bulletin_player_activity)

        if (NetworkStatus.getInstance().isConnected(dbApp)) {
            val bundle = intent.extras
            if (bundle != null) {
                val newsBulletinId = bundle.getString(Constants.NewsBulletin.KEY_NEWS_BULLETIN_ID)
                if (!TextUtils.isEmpty(newsBulletinId)) {
                    getNewsBulletin(newsBulletinId!!)
                } else {
                    loadFragmentToPlayNewsBulletin()
                }
            } else {
                loadFragmentToPlayNewsBulletin()
            }
        } else {
            DBToast.showAlertToast(applicationContext, getString(R.string.alert_network_not_exist))
            finish()
        }
        Log.d(TAG, "onCreate")
    }

    /**
     * @param newsBulletinId
     */
    private fun getNewsBulletin(newsBulletinId: String) {
        val pDialog = ProgressbarDialog(this)
        pDialog.show(getString(R.string.please_wait), true)

        val finalUrl = Urls.APP_FEED_BASE_URL + Action.CategoryAction.CAT_ACTION_AUDIO_BULLETIN_LIST + "/" +
                CommonConstants.CHANNEL_ID + "/" + newsBulletinId + "/"

        val responseCall = ApiClient.createService(ApiHelper::class.java).getNewsBulletinDetail(finalUrl)

        responseCall!!.enqueue(object : RetroCallbackProvider<NewsBulletinResponse>() {
            override fun onSuccess(call: Call<NewsBulletinResponse>, response: NewsBulletinResponse) {
                try {
                    if (response.data.list != null) {
                        if (NewsBulletinAudioPlayerKtFragment.smIsPlaying && MediaPlayerService.activeAudioId.toString() == newsBulletinId) {
                            loadFragment(ArrayList<NewsBulletinItem>(response.data.list), 0, false)
                        } else {
                            loadFragment(ArrayList<NewsBulletinItem>(response.data.list), 0, true)
                        }
                    }
                } catch (e: Exception) {
                    Log.e(TAG, e.message + "")
                    DBToast.showAlertToast(applicationContext, getString(R.string.alert_api_failed))
                    finish()
                } finally {
                    pDialog.dismiss()
                }
            }

            override fun onFailed(call: Call<NewsBulletinResponse>, t: Throwable) {
                pDialog.dismiss()
                DBToast.showAlertToast(applicationContext, getString(R.string.alert_api_failed))
                finish()
            }
        })
    }

    /**
     * @param items
     * @param index
     * @param isFreshLaunch
     */
    private fun loadFragment(items: ArrayList<NewsBulletinItem>?, index: Int, isFreshLaunch: Boolean?) {
        if (isFreshLaunch != null) {
            canDrawOverlay(items, index, isFreshLaunch)
        }
    }

    private fun loadPlayFragment(items: ArrayList<NewsBulletinItem>?, index: Int, isFreshLaunch: Boolean) {
        Log.d(TAG, "loadPlayFragment()")
        val fragment = NewsBulletinAudioPlayerKtFragment.newInstance(items, index,
                isFreshLaunch)

        FragmentUtil.changeFragment(supportFragmentManager, R.id.player_container, fragment, true, true)

        /* try {
             val newsBulletinItem = items?.get(index)

             val extraCTEvent1 = ExtraCTEvent(CTConstant.PROP_NAME.ACTION_TYPE, CTConstant.PROP_VALUE.AUDIO_STARTED)
             val extraCTEvent2 = ExtraCTEvent(CTConstant.PROP_NAME.AUDIO_TITLE, newsBulletinItem?.mTitle)
             val extraCTEvent3 = ExtraCTEvent(CTConstant.PROP_NAME.AUDIO_URL, newsBulletinItem?.mAudioStreamURL)

             var proValue = ""

             if (TextUtils.isEmpty(menuType) || AppConstants.MENU_TYPE_NEWS_BULLETIN.equals(menuType, ignoreCase = true)) {
                 proValue = CTConstant.PROP_VALUE.NEWS_BULLETIN
             } else {
                 proValue = CTConstant.PROP_VALUE.AUDIO_PODCAST
             }

             cleverTapTrackEvent(CTConstant.EVENT_NAME.EVENT_AUDIO,
                     CTConstant.PROP_NAME.CLICKON, proValue, extraCTEvent1, extraCTEvent2, extraCTEvent3)
         } catch (e: Exception) {
             Log.e(TAG, e.message)
         }*/

    }

    private fun canDrawOverlay(items: ArrayList<NewsBulletinItem>?, index: Int, isFreshLaunch: Boolean) {

        DbSharedPref.isDrawOverOtherAppsPermissionAsked(dbApp)

        //Check if the application has draw over other apps permission or not?
        //This permission is by default available for API<23. But for API > 23
        //you have to ask for the permission in runtime.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this) &&
                !DbSharedPref.isDrawOverOtherAppsPermissionAsked(dbApp)) {
            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            showLocationReasonsMessageToUser(items, index, isFreshLaunch)

        } else {
            loadPlayFragment(items, index, isFreshLaunch)
        }
    }

    /***
     *
     * @param items
     * @param index
     * @param isFreshLaunch
     * @param dbSharedPref
     */
    private fun showLocationReasonsMessageToUser(items: ArrayList<NewsBulletinItem>?, index: Int,
                                                 isFreshLaunch: Boolean) {

        EasyDialogUtils.showConfirmationDialog(this,
                getString(R.string.draw_over_other_app_permission_heading),
                getString(R.string.draw_over_other_app_permission), false, R.mipmap.app_icon,
                getString(R.string.allow_button),
                getString(R.string.cancel), object : DialogResponseInterface {

            override fun doOnPositiveBtnClick(activity: Activity?) {
                try {
                    @SuppressLint("InlinedApi")
                    val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:$packageName"))
                    startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION)
                } catch (e: Exception) {
                    Log.e(TAG, "onClickPositiveButton: " + e.message)
                }
            }

            override fun doOnNegativeBtnClick(activity: Activity?) {
                loadPlayFragment(items, index, isFreshLaunch)
            }
        })
        DbSharedPref.setDrawOverOtherAppsPermissionAsked(dbApp)
    }

    private fun loadFragmentToPlayNewsBulletin() {
        val bundle = intent.extras
        if (bundle != null) {
            val newsBulletinItems = bundle.getParcelableArrayList<NewsBulletinItem>(Constants.NewsBulletin.KEY_NEWS_BULLETIN_LIST)
            val index = bundle.getInt(Constants.NewsBulletin.KEY_SELECTED_NEWS_BULLETIN_INDEX)
            val isFreshLaunch = bundle.getBoolean(Constants.NewsBulletin.KEY_NEWS_BULLETIN_FRESH_LAUNCH)
            loadFragment(newsBulletinItems, index, isFreshLaunch)
        }
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        recreate()
        Log.d(TAG, "onActivityResult RequestCode : resultCode$requestCode : $resultCode")
    }
}