package com.db.audioBulletin

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.db.audioBulletin.mediaPlayer.MediaPlayerService

class NewsBulletinPushNotificationEventKtReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {

        // Below code is added on 29-10-2018 because of one issue raised on Redmine: Bug #18
        if (NewsBulletinAudioPlayerKtFragment.smPlayerService != null) {
            NewsBulletinAudioPlayerKtFragment.smPlayerService!!.hideWidget()
        }

        val broadcastIntent = Intent(MediaPlayerService.Broadcast_PLAYER_EVENT)
        broadcastIntent.putExtra(MediaPlayerService.PLAYER_STATE, MediaPlayerService.PlayerState.STOPPED.ordinal)
        context?.sendBroadcast(broadcastIntent)
    }
}