package com.db.audioBulletin.mediaPlayer;

import com.bhaskar.util.GsonProguardMarker;
import java.io.Serializable;

/**
 * Created by Saurabh Jain
 */
public class Audio implements Serializable, GsonProguardMarker {

    private String data;
    private String title;
    private String album;
    private String artist;
    private String imageURL;
    private int audioId;

    /***
     *
     * @param data
     * @param title
     * @param album
     * @param artist
     * @param imageURL
     * @param audioId
     */
    public Audio(String data, String title, String album, String artist, String imageURL, int audioId) {
        this.data = data;
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.imageURL = imageURL;
        this.audioId = audioId;
    }

    public int getAudioId() {
        return audioId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setIconId(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public String toString() {
        return "Audio{" +
                "data='" + data + '\'' +
                ", title='" + title + '\'' +
                ", album='" + album + '\'' +
                ", artist='" + artist + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", audioId=" + audioId +
                '}';
    }
}