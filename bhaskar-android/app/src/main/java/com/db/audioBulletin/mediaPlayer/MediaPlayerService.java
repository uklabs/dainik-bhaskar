package com.db.audioBulletin.mediaPlayer;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.session.MediaSessionManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bhaskar.BuildConfig;
import com.bhaskar.R;
import com.db.audioBulletin.NewsBulletinAudioPlayerKtFragment;
import com.db.audioBulletin.NewsBulletinListingFragment;
import com.db.audioBulletin.NewsBulletinPlayerKtActivity;
import com.db.audioBulletin.NewsBulletinPushNotificationEventKtReceiver;
import com.db.audioBulletin.model.NewsBulletinItem;
import com.db.util.Constants;
import com.db.util.ImageUtil;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Saurabh Jain
 */
public class MediaPlayerService extends Service implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnSeekCompleteListener,
        MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener,
        AudioManager.OnAudioFocusChangeListener {

    public static final String ACTION_PLAY = "com.bhaskar.darwin.audioplayer.ACTION_PLAY";
    public static final String ACTION_PAUSE = "com.bhaskar.darwin.audioplayer.ACTION_PAUSE";
    public static final String ACTION_PREVIOUS = "com.bhaskar.darwin.audioplayer.ACTION_PREVIOUS";
    public static final String ACTION_NEXT = "com.bhaskar.darwin.audioplayer.ACTION_NEXT";
    public static final String ACTION_STOP = "com.bhaskar.darwin.audioplayer.ACTION_STOP";
    public static final String ACTION_FORWARD = "com.bhaskar.darwin.audioplayer.ACTION_FORWARD";
    public static final String ACTION_REWIND = "com.bhaskar.darwin.audioplayer.ACTION_REWIND";
    public static final String Broadcast_PLAY_NEW_AUDIO = "com.bhaskar.darwin.audioplayer.PlayNewAudio";
    public static final String Broadcast_PLAYER_EVENT = "com.bhaskar.darwin.audioplayer.PlayerEvent";
    public static final String CHANNEL_ID = "db_channel_id";
    public static final String CHANNEL_NAME = "db_channel_name";
    public static final String CHANNEL_DESCRIPTION = "db_channel_description";
    public static final String PLAYER_STATE = "playerState";
    private static final int REQUEST_CODE = 101;
    private static final long FORWARD_OR_REWIND_TIME = 10 * 1000;
    private final String TAG = "MediaPlayerService";

    private MediaPlayer mediaPlayer;

    //MediaSession
    private MediaSessionManager mediaSessionManager;
    private MediaSessionCompat mediaSession;
    private MediaControllerCompat.TransportControls transportControls;

    //Used to pause/resume MediaPlayer
    private int resumePosition;

    //AudioFocus
    private AudioManager audioManager;

    // Binder given to clients
    private final IBinder iBinder = new LocalBinder();

    //List of available Audio files
    private ArrayList<Audio> audioList;
    private int audioIndex = -1;
    private Audio activeAudio; //an object on the currently playing audio

    public static int activeAudioId;

    //Handle incoming phone calls
    private boolean ongoingCall = false;
    private PhoneStateListener phoneStateListener;
    private TelephonyManager telephonyManager;

    private static int NOTIFICATION_ID = 101;
    private Bitmap largeIcon;

    private WindowManager mWindowManager;
    private View mWidget;
    private ImageView mVisualizer;
    private ImageView mThumbnailImage;

    private WindowManager.LayoutParams params = null;

    private ArrayList<NewsBulletinItem> mNewsBulletinList;
    private int mIndex = 0;
    private static int CLICK_ACTION_THRESHOLD = 10;
    private static final int MAX_CLICK_DURATION = 200;
    private ServiceConnection serviceConnection;


    /**
     * Service lifecycle methods
     */
    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        // Perform one-time setup procedures

        // Manage incoming phone calls during playback.
        // Pause MediaPlayer on incoming call,
        // Resume on hangup.
        try {
            callStateListener();
        } catch (Exception e) {
            Log.e(TAG, "callStateListener(): " + e.getMessage());
        }
        //ACTION_AUDIO_BECOMING_NOISY -- change in audio outputs -- BroadcastReceiver
        registerBecomingNoisyReceiver();
        //Listen for new Audio to play -- BroadcastReceiver
        register_playNewAudio();
        try {
            initWidget();
        } catch (Exception e) {
            Log.e("MediaPlayerService", e.getMessage() + "");
        }
    }


    private void initVariables(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            if (bundle.containsKey(Constants.NewsBulletin.KEY_NEWS_BULLETIN_LIST))
                mNewsBulletinList = bundle.getParcelableArrayList(Constants.NewsBulletin.KEY_NEWS_BULLETIN_LIST);
            if (bundle.containsKey(Constants.NewsBulletin.KEY_SELECTED_NEWS_BULLETIN_INDEX))
                mIndex = bundle.getInt(Constants.NewsBulletin.KEY_SELECTED_NEWS_BULLETIN_INDEX, 0);
        }
    }

    public void initWidget() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this))
            return;
        //Inflate the widget layout we created
        mWidget = LayoutInflater.from(this).inflate(R.layout.news_bulletin_player_widget, null);
        int valueInPixels = (int) getResources().getDimension(R.dimen.news_bulletin_widget_dimen);
        //Add the view to the window.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            params = new WindowManager.LayoutParams(
                    valueInPixels, valueInPixels,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                            | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    PixelFormat.TRANSLUCENT);
        } else {
            params = new WindowManager.LayoutParams(
                    valueInPixels,
                    valueInPixels,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                            | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    PixelFormat.TRANSLUCENT);
        }
        int bulletinListPadding = (int) getResources().getDimension(R.dimen.news_bulletin_list_padding);

        int[] textSizeAttr = new int[]{android.R.attr.actionBarSize};
        int indexOfAttrDimen = 0;
        TypedValue typedValue = new TypedValue();
        TypedArray a = obtainStyledAttributes(typedValue.resourceId, textSizeAttr);
        int bottomMenuHeight = a.getDimensionPixelSize(indexOfAttrDimen, -1);
        a.recycle();

        ////int bottomMenuHeight = (int) getResources().getDimension(R.dimen.actionBarSize);
        //Specify the widget position
        params.gravity = Gravity.TOP | Gravity.LEFT;        //Initially view will be added to top-left corner

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int widthPixels = displayMetrics.widthPixels;
        int heightPixels = displayMetrics.heightPixels;

        params.x = widthPixels - (valueInPixels + bulletinListPadding);
        params.y = heightPixels - (valueInPixels + bottomMenuHeight + bulletinListPadding);

        //Add the view to the window
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mWidget, params);

        mThumbnailImage = mWidget.findViewById(R.id.image_thumbnail);
        mVisualizer = mWidget.findViewById(R.id.visualizer);

        GestureDetector gestureDetector = new GestureDetector(this, new SingleTapConfirm());

        //Drag and move the widget using user's touch action.
        mWidget.setOnTouchListener(new View.OnTouchListener() {
            private int lastAction;
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;
            private float startX;
            private float startY;
            private long startClickTime;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (gestureDetector.onTouchEvent(event)) {
                    // single tap
                    Intent intent = new Intent(MediaPlayerService.this, NewsBulletinPlayerKtActivity.class);
                    intent.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_LIST, mNewsBulletinList);
                    intent.putExtra(Constants.NewsBulletin.KEY_SELECTED_NEWS_BULLETIN_INDEX, mIndex);
                    intent.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_FRESH_LAUNCH, false);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    return true;
                } else {
                    try {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                //remember the initial position.
                                initialX = params.x;
                                initialY = params.y;
                                //get the touch location
                                initialTouchX = event.getRawX();
                                initialTouchY = event.getRawY();
                                startX = event.getRawX();
                                startY = event.getRawY();
                                startClickTime = Calendar.getInstance().getTimeInMillis();
                                lastAction = event.getAction();
                                return true;
                            case MotionEvent.ACTION_UP:
                                lastAction = event.getAction();
                                return true;
                            case MotionEvent.ACTION_MOVE:
                                //Calculate the X and Y coordinates of the view.
                                params.x = initialX + (int) (event.getRawX() - initialTouchX);
                                params.y = initialY + (int) (event.getRawY() - initialTouchY);

                                //Update the layout with new X & Y coordinate
                                mWindowManager.updateViewLayout(mWidget, params);
                                lastAction = event.getAction();
                                return true;
                        }
                        return false;
                    } catch (Exception e) {
                        Log.e("MediaPlayerService", "initWidget:onTouch " + e.getMessage());
                        return false;
                    }
                }
            }
        });
        hideWidget();
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }

    public void showWidget() {
        if (mWidget != null)
            mWidget.setVisibility(View.VISIBLE);
    }

    public void hideWidget() {
        if (mWidget != null)
            mWidget.setVisibility(View.GONE);
    }

    private void removeWidget() {
        try {
            if (mWidget != null) mWindowManager.removeView(mWidget);
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage() + "");
        }
    }

    //The system calls this method when an activity, requests the service be started
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            //Load data from SharedPreferences
            StorageUtil storage = new StorageUtil(getApplicationContext());
            audioList = storage.loadAudio();
            audioIndex = storage.loadAudioIndex();

            if (audioIndex != -1 && audioIndex < audioList.size()) {
                //index is in a valid range
                activeAudio = audioList.get(audioIndex);
                activeAudioId = activeAudio.getAudioId();
            } else {
                stopThisService();
            }
            if (mThumbnailImage != null)
                ImageUtil.setImage(getApplicationContext(), activeAudio.getImageURL(), mThumbnailImage, R.mipmap.aap_icon);
            initVariables(intent);
        } catch (Exception e) {
            stopThisService();
        }

        try {
            //Request audio focus
            if (!requestAudioFocus()) {
                //Could not gain focus
                stopThisService();
            }
            if (mediaSessionManager == null) {
                try {
                    initMediaSession();
                    initMediaPlayer();
                } catch (RemoteException e) {
                    e.printStackTrace();
                    stopThisService();
                }
                buildNotification(PlaybackStatus.PLAYING, true);
            }
            //Handle Intent action from MediaSession.TransportControls
            handleIncomingActions(intent);
        } catch (Exception e) {
            stopThisService();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mediaSession.release();
        //removeNotification();
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {

        try {
            if (mediaPlayer != null) {
                stopMedia();
                mediaPlayer.release();
            }
            removeAudioFocus();
            //Disable the PhoneStateListener
            if (phoneStateListener != null && telephonyManager != null) {
                telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
            }

            removeNotification();

            //unregister BroadcastReceivers
            unregisterReceiver(becomingNoisyReceiver);
            unregisterReceiver(playNewAudio);

            //clear cached playlist
            new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();
            removeWidget();
            NewsBulletinListingFragment.Companion.setSmCurrentNewsBulletinItem(null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            super.onDestroy();
        }
    }

    /***
     *
     * @param serviceConnection
     */
    public void setServiceConnection(ServiceConnection serviceConnection) {
        this.serviceConnection = serviceConnection;
    }

    public void stopThisService() {
        try {
            if (null != serviceConnection)
                unbindService(serviceConnection);
        } catch (Exception e) {
            Log.e("MediaPlayerService", e.getMessage());
        } finally {
            stopSelf();
        }
    }

    /**
     * Service Binder
     */
    public class LocalBinder extends Binder {
        public MediaPlayerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return MediaPlayerService.this;
        }
    }


    /**
     * MediaPlayer callback methods
     */
    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        //Invoked indicating buffering status of
        //a media resource being streamed over the network.
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        removeWidget();
        //Invoked when playback of a media source has completed.
        stopMedia();

        removeNotification();
        //stop the service
        stopThisService();

        resumePosition = 0;
        sendPlayerEvent(PlayerState.COMPLETED.ordinal());
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        //Invoked when there has been an error during an asynchronous operation
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                Log.d("MediaPlayer Error", "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK " + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Log.d("MediaPlayer Error", "MEDIA ERROR SERVER DIED " + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Log.d("MediaPlayer Error", "MEDIA ERROR UNKNOWN " + extra);
                break;
        }

        return true;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        //Invoked to communicate some info
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        //Invoked when the media source is ready for playback.
        playMedia();
        sendPlayerEvent(PlayerState.PREPARED.ordinal());
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        //Invoked indicating the completion of a seek operation.
    }

    @Override
    public void onAudioFocusChange(int focusState) {

        //Invoked when the audio focus of the system is updated.
        switch (focusState) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                /*if (mediaPlayer == null) initMediaPlayer();
                else if (!mediaPlayer.isPlaying()) mediaPlayer.start();*/
                if (mediaPlayer != null) {
                    if (!mediaPlayer.isPlaying() && NewsBulletinAudioPlayerKtFragment.Companion.getSmIsPlaying()) {
                        mediaPlayer.start();
                        mediaPlayer.setVolume(1.0f, 1.0f);
                    }

                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and release media player
                /*NewsBulletinAudioPlayerFragment.stopPlayback(this);
                NewsBulletinListingFragment.smCurrentNewsBulletinItem = null;*/

                Intent broadcastIntent = new Intent(MediaPlayerService.Broadcast_PLAYER_EVENT);
                broadcastIntent.putExtra(PLAYER_STATE, PlayerState.AUDIO_FOCUS_LOSS);
                sendBroadcast(broadcastIntent);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) mediaPlayer.pause();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) mediaPlayer.setVolume(0.1f, 0.1f);
                }
                break;
        }
    }


    /**
     * AudioFocus
     */
    private boolean requestAudioFocus() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            //Focus gained
            return true;
        }
        //Could not gain focus
        return false;
    }

    private boolean removeAudioFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                audioManager.abandonAudioFocus(this);
    }


    /**
     * MediaPlayer actions
     */
    private void initMediaPlayer() {
        if (mediaPlayer == null)
            mediaPlayer = new MediaPlayer();//new MediaPlayer instance
        try {
            //Set up MediaPlayer event listeners
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnErrorListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnBufferingUpdateListener(this);
            mediaPlayer.setOnSeekCompleteListener(this);
            mediaPlayer.setOnInfoListener(this);
            //Reset so that the MediaPlayer is not pointing to another data source
            mediaPlayer.reset();
        } catch (Exception e) {
            Log.e(TAG, "initMediaPlayer: " + e.getMessage());
        }

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            if (null != activeAudio.getData()) {
                // Set the data source to the mediaFile location
                mediaPlayer.setDataSource(activeAudio.getData());
                mediaPlayer.prepareAsync();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
            stopThisService();
        }

    }

    private void playMedia() {
        if (mediaPlayer == null) return;
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
        }
    }

    private void stopMedia() {
        if (mediaPlayer == null) return;
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
    }

    private void pauseMedia() {
        if (mediaPlayer == null) return;
        try {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                resumePosition = mediaPlayer.getCurrentPosition();
            }
        } catch (Exception e) {
            Log.d(MediaPlayerService.class.getName(), e.getMessage() + "");
        }
    }

    private void resumeMedia() {
        try {
            if (mediaPlayer == null) return;
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.seekTo(resumePosition);
                mediaPlayer.start();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }

    public void fastForward() {
        try {
            if (mediaPlayer != null) {
                long totalTime = mediaPlayer.getDuration();
                long currentPosition = mediaPlayer.getCurrentPosition();
                long seekTo = currentPosition + FORWARD_OR_REWIND_TIME;
                if (seekTo < totalTime) {
                    mediaPlayer.seekTo((int) seekTo);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "fastForward: " + e.getMessage());
        }
    }

    public void rewind() {
        if (mediaPlayer != null) {
            try {
                long currentPosition = mediaPlayer.getCurrentPosition();
                long seekTo = currentPosition - FORWARD_OR_REWIND_TIME;
                if (seekTo > 0) {
                    mediaPlayer.seekTo((int) seekTo);
                }
            } catch (IllegalStateException e) {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "rewind: " + e.getMessage());
                }
            }
        }
    }

    public int getCurrentPosition() {
        if (mediaPlayer != null)
            return mediaPlayer.getCurrentPosition();
        else
            return 0;
    }

    public int getDuration() {

        try {
            if (mediaPlayer != null)
                return mediaPlayer.getDuration();
            else
                return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void seekTo(int position) {
        if (mediaPlayer != null) {
            resumePosition = position;
            try {
                mediaPlayer.seekTo(resumePosition);
            } catch (Exception e) {
                Log.e(TAG, "seekTo: " + e.getMessage());
            }
        }
    }

    private void skipToNext() {

        if (audioIndex == audioList.size() - 1) {
            //if last in playlist
            audioIndex = 0;
            activeAudio = audioList.get(audioIndex);
            activeAudioId = activeAudio.getAudioId();
        } else {
            //get next in playlist
            activeAudio = audioList.get(++audioIndex);
            activeAudioId = activeAudio.getAudioId();
        }

        //Update stored index
        new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        stopMedia();
        //reset mediaPlayer
        mediaPlayer.reset();
        initMediaPlayer();
    }

    private void skipToPrevious() {

        if (audioIndex == 0) {
            //if first in playlist
            //set index to the last of audioList
            audioIndex = audioList.size() - 1;
            activeAudio = audioList.get(audioIndex);

        } else {
            //get previous in playlist
            activeAudio = audioList.get(--audioIndex);
        }
        activeAudioId = activeAudio.getAudioId();

        //Update stored index
        new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        stopMedia();
        //reset mediaPlayer
        mediaPlayer.reset();
        initMediaPlayer();
    }

    public void pause() {
        transportControls.pause();
    }

    public void resume() {
        transportControls.play();
    }

    /**
     * ACTION_AUDIO_BECOMING_NOISY -- change in audio outputs
     */
    private BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //pause audio on ACTION_AUDIO_BECOMING_NOISY
            pauseMedia();
            buildNotification(PlaybackStatus.PAUSED, false);
        }
    };

    private void registerBecomingNoisyReceiver() {
        //register after getting audio focus
        IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(becomingNoisyReceiver, intentFilter);
    }

    /**
     * Handle PhoneState changes
     */
    private void callStateListener() {
        // Get the telephony manager
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        //Starting listening for PhoneState changes
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                try {
                    switch (state) {
                        //if at least one call exists or the phone is ringing
                        //pause the MediaPlayer
                        case TelephonyManager.CALL_STATE_OFFHOOK:
                        case TelephonyManager.CALL_STATE_RINGING:
                            if (mediaPlayer != null) {
                                pauseMedia();
                                ongoingCall = true;
                            }
                            break;
                        case TelephonyManager.CALL_STATE_IDLE:
                            // Phone idle. Start playing.
                            if (mediaPlayer != null) {
                                if (ongoingCall && NewsBulletinAudioPlayerKtFragment.Companion.getSmIsPlaying()) {
                                    ongoingCall = false;
                                    resumeMedia();
                                    // sendPlayerEvent(PlayerState.PLAYING.ordinal());
                                    //  buildNotification(PlaybackStatus.PLAYING, false);
                                }
                            }
                            break;
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onCallStateChanged() " + e.getMessage());
                }
            }
        };
        // Register the listener with the telephony manager
        // Listen for changes to the device call state.
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    /**
     * MediaSession and Notification actions
     */
    private void initMediaSession() throws RemoteException {
        if (mediaSessionManager != null) return; //mediaSessionManager exists

        mediaSessionManager = (MediaSessionManager) getSystemService(Context.MEDIA_SESSION_SERVICE);
        // Create a new MediaSession
        mediaSession = new MediaSessionCompat(getApplicationContext(), TAG);
        //Get MediaSessions transport controls
        transportControls = mediaSession.getController().getTransportControls();
        //set MediaSession -> ready to receive media commands
        mediaSession.setActive(true);
        //indicate that the MediaSession handles transport control commands
        // through its MediaSessionCompat.Callback.
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS | MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS);

        //Set mediaSession's MetaData
        updateMetaData();

        // Attach Callback to receive MediaSession updates
        mediaSession.setCallback(new MediaSessionCompat.Callback() {
            // Implement callbacks
            @Override
            public void onPlay() {
                super.onPlay();
                resumeMedia();
                buildNotification(PlaybackStatus.PLAYING, false);
                broadCastPlaybackEvent(PlaybackStateCompat.STATE_PLAYING);
            }

            @Override
            public void onPause() {
                super.onPause();
                pauseMedia();
                buildNotification(PlaybackStatus.PAUSED, false);
                broadCastPlaybackEvent(PlaybackStateCompat.STATE_PAUSED);
            }

            @Override
            public void onSkipToNext() {
                super.onSkipToNext();
                skipToNext();
                updateMetaData();
                buildNotification(PlaybackStatus.PLAYING, false);
            }

            @Override
            public void onSkipToPrevious() {
                super.onSkipToPrevious();
                skipToPrevious();
                updateMetaData();
                buildNotification(PlaybackStatus.PLAYING, true);
            }

            @Override
            public void onStop() {
                super.onStop();
                removeNotification();
                //Stop the service
                stopThisService();
                activeAudioId = -1;
            }

            @Override
            public void onSeekTo(long position) {
                super.onSeekTo(position);
            }
        });
    }

    private void updateMetaData() {
        try {
            Bitmap albumArt = BitmapFactory.decodeResource(getResources(),
                    R.drawable.placeholder); //replace with medias albumArt
            // Update the current metadata
            mediaSession.setMetadata(new MediaMetadataCompat.Builder()
                    .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, albumArt)
                    .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, activeAudio.getArtist())
                    .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, activeAudio.getAlbum())
                    .putString(MediaMetadataCompat.METADATA_KEY_TITLE, activeAudio.getTitle())
                    .build());
        } catch (Exception e) {
            Log.d(TAG, e.getMessage() + "");
        }
    }

    private void buildNotification(PlaybackStatus playbackStatus, boolean firstTime) {
        try {
            if (firstTime)
                new SendNotificationTask(getApplicationContext(), playbackStatus).execute(activeAudio.getImageURL());
            else
                sendNotification(getApplicationContext(), playbackStatus);
            if (mVisualizer != null) {
                if (playbackStatus == PlaybackStatus.PLAYING)
                    ImageUtil.setImage(getApplicationContext(), R.drawable.visualizer_gif, mVisualizer, R.mipmap.app_icon);
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        mVisualizer.setImageResource(R.drawable.visualizer_gif);
                    else
                        mVisualizer.setImageResource(0);
                }
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage() + "");
        }
    }

    private PendingIntent playbackAction(int actionNumber) {
        Intent playbackAction = new Intent(this, MediaPlayerService.class);

        playbackAction.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_LIST, mNewsBulletinList);
        playbackAction.putExtra(Constants.NewsBulletin.KEY_SELECTED_NEWS_BULLETIN_INDEX, mIndex);
        playbackAction.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_FRESH_LAUNCH, false);
        switch (actionNumber) {
            case 0:
                // Play
                playbackAction.setAction(ACTION_PLAY);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 1:
                // Pause
                playbackAction.setAction(ACTION_PAUSE);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 2:
                // Next track
                playbackAction.setAction(ACTION_NEXT);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 3:
                // Previous track
                playbackAction.setAction(ACTION_PREVIOUS);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 4:
                // Next track
                playbackAction.setAction(ACTION_FORWARD);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 5:
                // Previous track
                playbackAction.setAction(ACTION_REWIND);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            default:
                break;
        }
        return null;
    }

    private void removeNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    private void handleIncomingActions(Intent playbackAction) {
        if (playbackAction == null || playbackAction.getAction() == null) return;

        String actionString = playbackAction.getAction();
        if (actionString.equalsIgnoreCase(ACTION_PLAY)) {
            transportControls.play();
            sendPlayerEvent(PlayerState.PLAYING.ordinal());
        } else if (actionString.equalsIgnoreCase(ACTION_PAUSE)) {
            transportControls.pause();
            sendPlayerEvent(PlayerState.PAUSED.ordinal());
        } else if (actionString.equalsIgnoreCase(ACTION_NEXT)) {
            transportControls.skipToNext();
        } else if (actionString.equalsIgnoreCase(ACTION_PREVIOUS)) {
            transportControls.skipToPrevious();
        } else if (actionString.equalsIgnoreCase(ACTION_STOP)) {
            transportControls.stop();
        } else if (actionString.equalsIgnoreCase(ACTION_FORWARD)) {
            //transportControls.fastForward();
            fastForward();
        } else if (actionString.equalsIgnoreCase(ACTION_REWIND)) {
            //transportControls.rewind();
            rewind();
        } else if (Constants.NewsBulletin.ACTION_STOP_TRACKS.equalsIgnoreCase(actionString)) {
            if (transportControls != null) {
                transportControls.stop();
            }
            stopSelf();
        }
    }


    /**
     * Play new Audio
     */
    private BroadcastReceiver playNewAudio = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                //Get the new media index form SharedPreferences
                audioIndex = new StorageUtil(getApplicationContext()).loadAudioIndex();
                if (null != audioList && audioIndex != -1 && audioIndex < audioList.size()) {
                    //index is in a valid range
                    activeAudio = audioList.get(audioIndex);
                    activeAudioId = activeAudio.getAudioId();
                } else {
                    stopThisService();
                }

                //A PLAY_NEW_AUDIO action received
                //reset mediaPlayer to play the new Audio
                stopMedia();
                mediaPlayer.reset();
                initMediaPlayer();
                updateMetaData();
                buildNotification(PlaybackStatus.PLAYING, true);
            } catch (Exception e) {
                Log.e(TAG, "BroadcastReceiver playNewAudio: " + e.getMessage());
            }
        }
    };

    private void register_playNewAudio() {
        //Register playNewMedia receiver
        IntentFilter filter = new IntentFilter(Broadcast_PLAY_NEW_AUDIO);
        registerReceiver(playNewAudio, filter);
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    private PendingIntent createContentIntent() {
        Intent openUI = new Intent(this, NewsBulletinPlayerKtActivity.class);

        openUI.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_LIST, mNewsBulletinList);
        openUI.putExtra(Constants.NewsBulletin.KEY_SELECTED_NEWS_BULLETIN_INDEX, mIndex);
        openUI.putExtra(Constants.NewsBulletin.KEY_NEWS_BULLETIN_FRESH_LAUNCH, false);
        openUI.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return PendingIntent.getActivity(
                this, REQUEST_CODE, openUI, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private PendingIntent createDeleteIntent() {
        Intent intent = new Intent(getApplicationContext(), NewsBulletinPushNotificationEventKtReceiver.class);
        return PendingIntent.getBroadcast(getApplicationContext(), REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private class SendNotificationTask extends AsyncTask<String, Void, Bitmap> {
        Context context;
        PlaybackStatus playbackStatus;

        SendNotificationTask(Context context, PlaybackStatus playbackStatus) {
            super();
            this.context = context;
            this.playbackStatus = playbackStatus;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            largeIcon = null;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = ImageUtil.getBitmap(context, params[0]);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            largeIcon = result;
            sendNotification(context, playbackStatus);
        }
    }

    private void sendNotification(Context context, PlaybackStatus playbackStatus) {
        /**
         * Notification actions -> playbackAction()
         *  0 -> Play
         *  1 -> Pause
         *  2 -> Next track
         *  3 -> Previous track
         *  4 -> Fast forward
         *  5 -> Rewind
         */
        int notificationAction = R.drawable.ic_audio_pause;//needs to be initialized
        PendingIntent play_pauseAction = null;
        String playPause = "";

        // Create a new Notification
        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context, CHANNEL_ID);

        //Build a new notification according to the current state of the MediaPlayer
        if (playbackStatus == PlaybackStatus.PLAYING) {
            //create the pause action
            play_pauseAction = playbackAction(1);
            playPause = context.getString(R.string.label_pause);
            notificationBuilder.setOngoing(true);
            NewsBulletinAudioPlayerKtFragment.Companion.setSmIsPlaying(true);
        } else if (playbackStatus == PlaybackStatus.PAUSED) {
            notificationAction = R.drawable.ic_audio_play;
            //create the play action
            play_pauseAction = playbackAction(0);
            playPause = context.getString(R.string.label_play);
            notificationBuilder.setOngoing(false);
            NewsBulletinAudioPlayerKtFragment.Companion.setSmIsPlaying(false);
        }

        if (largeIcon == null) {
            largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.placeholder);
        }

        // Create a new Notification
        notificationBuilder
                // Hide the timestamp
                .setShowWhen(false)
                // Set the Notification style
                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                        // Attach our MediaSession token
                        .setMediaSession(mediaSession.getSessionToken())
                        // Show our playback controls in the compat view
                        .setShowActionsInCompactView(0, 1, 2))
                // Set the Notification color
                .setColor(getResources().getColor(R.color.colorAccent1))
                // Set the large and small icons
                .setLargeIcon(largeIcon)
                .setSmallIcon(android.R.drawable.stat_sys_headset)
                // Set Notification content information
                .setContentTitle(activeAudio.getTitle())
                .setContentIntent(createContentIntent())
                // When notification is deleted (when playback is paused and notification can be
                // deleted) fire MediaButtonPendingIntent with ACTION_STOP.
                //.setDeleteIntent(MediaButtonReceiver.buildMediaButtonPendingIntent(activityContext, PlaybackStateCompat.ACTION_STOP))
                .setDeleteIntent(createDeleteIntent())
                // Show controls on lock screen even when user hides sensitive content.
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                // Add playback actions
                .addAction(R.drawable.ic_audio_reverse, context.getString(R.string.label_rewind), playbackAction(5))
                .addAction(notificationAction, playPause, play_pauseAction)
                .addAction(R.drawable.ic_audio_forward, context.getString(R.string.label_forward), playbackAction(4));
        getNotificationsManager().notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    private NotificationManager getNotificationsManager() {
        NotificationManager notificationManager = ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE));
        //If the device is having android oreo we will create a notification channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = null;
            channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(CHANNEL_DESCRIPTION);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        return notificationManager;
    }

    private void sendPlayerEvent(int playerState) {
        Intent broadcastIntent = new Intent(MediaPlayerService.Broadcast_PLAYER_EVENT);
        broadcastIntent.putExtra(PLAYER_STATE, playerState);
        sendBroadcast(broadcastIntent);
    }

    /***
     *
     * @param playerState
     */
    private void broadCastPlaybackEvent(int playerState) {
        Intent broadcastIntent = new Intent(Constants.NewsBulletin.ACTION_BROADCAST_PLAYBACK_CONTROL);
        broadcastIntent.putExtra(Constants.NewsBulletin.KEY_AUDIO_PLAYBACK_EVENT, playerState);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        handleVisualizerState(playerState);
    }

    private void handleVisualizerState(int state) {
        try {
            if (state == MediaPlayerService.PlayerState.PLAYING.ordinal()) {
                NewsBulletinAudioPlayerKtFragment.Companion.setSmIsPlaying(true);
            } else if (state == MediaPlayerService.PlayerState.PAUSED.ordinal()) {
                NewsBulletinAudioPlayerKtFragment.Companion.setSmIsPlaying(false);
                NewsBulletinListingFragment.Companion.setSmCurrentNewsBulletinItem(null);
            } else if (state == MediaPlayerService.PlayerState.COMPLETED.ordinal()) {
                //  NewsBulletinAudioPlayerFragment.smIsPlaying = false;
                // NewsBulletinListingFragment.smCurrentNewsBulletinItem = null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public enum PlayerState {
        STARTED, PLAYING, PAUSED, COMPLETED, PREPARED, STOPPED, AUDIO_FOCUS_LOSS
    }

    /***
     *
     * @param context
     */
    public static void playTrack(@NonNull Context context) {
        Intent intent = new Intent(ACTION_PLAY, null, context, MediaPlayerService.class);
        context.startService(intent);
    }

    /***
     *
     * @param context
     */
    public static void stopTrack(@NonNull Context context) {
        Intent intent = new Intent(Constants.NewsBulletin.ACTION_STOP_TRACKS, null, context, MediaPlayerService.class);
        context.startService(intent);
    }

    /**
     * @param context
     */
    public static void pauseTrack(@NonNull Context context) {
        Intent intent = new Intent(ACTION_PAUSE, null, context, MediaPlayerService.class);
        context.startService(intent);
    }
}