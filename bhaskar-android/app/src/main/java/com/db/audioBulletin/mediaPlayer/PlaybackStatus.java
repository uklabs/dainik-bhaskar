package com.db.audioBulletin.mediaPlayer;

import com.bhaskar.util.GsonProguardMarker;

public enum PlaybackStatus implements GsonProguardMarker {
    PLAYING,
    PAUSED
}
