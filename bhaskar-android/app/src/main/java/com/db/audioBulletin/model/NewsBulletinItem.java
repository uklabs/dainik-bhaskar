package com.db.audioBulletin.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.bhaskar.util.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

public class NewsBulletinItem implements Parcelable, GsonProguardMarker {

    @SerializedName("nid")
    public int mNId;

    @SerializedName("podcast_title")
    public String mTitle = "";

    @SerializedName("podcast_body")
    public String mNewsHeadlineText = "";

    @SerializedName("podcast")
    public String mAudioStreamURL = "";

    @SerializedName("podcast_icon")
    public String mImageURL = "";

    @SerializedName("field_url_alias")
    public String mFieldUrlAlias = "";

    @SerializedName("created")
    public String mCreatedDateTime = "";

    @SerializedName("changed")
    public String mChangedDateTime = "";

    @SerializedName("gTrackUrl")
    private String gTrackUrl = "";

    public NewsBulletinItem(Parcel source) {
        readFromParcel(source);
    }

    public long duration;

    private void readFromParcel(Parcel source) {
        mNId = source.readInt();
        mTitle = source.readString();
        mNewsHeadlineText = source.readString();
        mAudioStreamURL = source.readString();
        mImageURL = source.readString();
        mFieldUrlAlias = source.readString();
        mCreatedDateTime = source.readString();
        mChangedDateTime = source.readString();
        duration = source.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mNId);
        dest.writeString(mTitle);
        dest.writeString(mNewsHeadlineText);
        dest.writeString(mAudioStreamURL);
        dest.writeString(mImageURL);
        dest.writeString(mFieldUrlAlias);
        dest.writeString(mCreatedDateTime);
        dest.writeString(mChangedDateTime);
        dest.writeLong(duration);
    }

    public static final Parcelable.Creator<NewsBulletinItem> CREATOR = new Parcelable.Creator<NewsBulletinItem>() {
        @Override
        public NewsBulletinItem createFromParcel(Parcel in) {
            return new NewsBulletinItem(in);
        }

        @Override
        public NewsBulletinItem[] newArray(int size) {
            return new NewsBulletinItem[size];
        }
    };

    @Override
    public String toString() {
        return "NewsBulletinItem{" +
                "mNId=" + mNId +
                ", mTitle='" + mTitle + '\'' +
                ", mNewsHeadlineText='" + mNewsHeadlineText + '\'' +
                ", mCreatedDateTime='" + mCreatedDateTime + '\'' +
                ", duration='" + duration + '\'' +
                '}';
    }

    public String getgTrackUrl() {
        if (TextUtils.isEmpty(gTrackUrl)) {
            return mFieldUrlAlias;
        } else {
            return gTrackUrl;
        }
    }
}

