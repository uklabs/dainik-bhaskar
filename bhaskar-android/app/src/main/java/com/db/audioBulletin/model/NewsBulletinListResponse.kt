package com.db.audioBulletin.model

import com.bhaskar.data.model.BaseResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NewsBulletinListResponse : BaseResponse() {

    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data {
        @SerializedName("podcast")
        @Expose
        var newsBulletinList: List<NewsBulletinItem>? = null

    }


}