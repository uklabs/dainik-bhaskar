package com.db.audioBulletin.model;

import com.bhaskar.data.model.BaseResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsBulletinResponse extends BaseResponse {

    @SerializedName("data")
    public NewsBulletinResponse.NewsBulletinData data;

    public static class NewsBulletinData {
        @SerializedName("podcast")
        public List<NewsBulletinItem> list;
    }
}
