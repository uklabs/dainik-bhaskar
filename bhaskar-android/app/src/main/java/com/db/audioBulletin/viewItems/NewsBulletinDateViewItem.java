package com.db.audioBulletin.viewItems;

import android.view.View;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.recycler_adapter.AdapterItem;
import com.db.recycler_adapter.RecyclerAdapterNotifier;
import com.db.recycler_adapter.RecyclerAdapterViewHolder;


public class NewsBulletinDateViewItem extends AdapterItem<NewsBulletinDateViewItem.Holder> {

    private String mDate;

    @Override
    public int getLayoutId() {
        return R.layout.item_news_bulletin_date;
    }

    @Override
    public Object getData() {
        return mDate;
    }

    @Override
    public void setData(Object obj) {
        mDate = (String) obj;
    }

    @Override
    protected void bindData(NewsBulletinDateViewItem.Holder holder, Object data, int position) {
        holder.mDate.setText(mDate);
    }

    @Override
    protected void onViewRecycled(NewsBulletinDateViewItem.Holder holder) {

    }

    public static class Holder extends RecyclerAdapterViewHolder {

        private TextView mDate;

        public Holder(View view, RecyclerAdapterNotifier adapter) {
            super(view, adapter);
            mDate = (TextView) findViewById(R.id.date);
        }
    }
}
