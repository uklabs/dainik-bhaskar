package com.db.audioBulletin.viewItems;

import android.view.View;

import com.bhaskar.R;
import com.db.recycler_adapter.AdapterItem;
import com.db.recycler_adapter.RecyclerAdapterNotifier;
import com.db.recycler_adapter.RecyclerAdapterViewHolder;

/**
 * Created by poonam on 8/15/2018.
 */

public class NewsBulletinSpaceViewItem extends AdapterItem<NewsBulletinSpaceViewItem.Holder> {

    @Override
    public int getLayoutId() {
        return R.layout.item_news_bulletin_space;
    }

    @Override
    public Object getData() {
        return null;
    }

    @Override
    public void setData(Object obj) {

    }

    @Override
    protected void bindData(NewsBulletinSpaceViewItem.Holder holder, Object data, int position) {

    }

    @Override
    protected void onViewRecycled(NewsBulletinSpaceViewItem.Holder holder) {

    }

    public static class Holder extends RecyclerAdapterViewHolder {


        public Holder(View view, RecyclerAdapterNotifier adapter) {
            super(view, adapter);
        }
    }
}
