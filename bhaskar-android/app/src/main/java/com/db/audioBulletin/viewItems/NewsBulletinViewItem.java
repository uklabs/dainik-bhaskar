package com.db.audioBulletin.viewItems;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bhaskar.R;
import com.db.audioBulletin.model.NewsBulletinItem;
import com.db.data.source.server.BackgroundRequest;
import com.db.listeners.OnStoryShareUrlListener;
import com.db.recycler_adapter.AdapterItem;
import com.db.recycler_adapter.RecyclerAdapterNotifier;
import com.db.recycler_adapter.RecyclerAdapterViewHolder;
import com.db.util.AppUtils;
import com.db.util.ImageUtil;
import com.db.util.ShareUtil;

/**
 * Created by poonam on 8/9/2018.
 */
public class NewsBulletinViewItem extends AdapterItem<NewsBulletinViewItem.Holder> {
    private Context mContext;
    private NewsBulletinItem mData;
    private OnItemClickListener mListener;

    public NewsBulletinViewItem(Context context) {
        this.mContext = context;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_news_bulletin;
    }

    @Override
    public Object getData() {
        return mData;
    }

    @Override
    public void setData(Object obj) {
        mData = (NewsBulletinItem) obj;
    }

    @Override
    protected void bindData(NewsBulletinViewItem.Holder holder, Object data, int position) {
        ImageUtil.setImage(mContext, mData.mImageURL, holder.mThumbnailImage, R.drawable.placeholder);
        holder.mNewsHeadline.setText(Html.fromHtml(mData.mNewsHeadlineText));
        holder.mShareImage.setOnClickListener(view -> {
            AppUtils.getInstance().shareClick(mContext, mData.mTitle, mData.mFieldUrlAlias, mData.getgTrackUrl(), false);

//            ShareUtil.shareNews((Activity) view.getContext(),
//                    ((NewsBulletinItem) data).mTitle, ((NewsBulletinItem) data).mFieldUrlAlias,
//                    GAConstants.Actions.NEWS,
//                    new BaseFragment.ProgressListener() {
//                        @Override
//                        public void onCompleted() {
//                        }
//
//                        @Override
//                        public void onFailed() {
//
//                        }
//                    });
        });
        holder.itemView.setOnClickListener(view -> {
            mListener.onItemClick(position, mData, view);
        });
    }

    @Override
    protected void onViewRecycled(NewsBulletinViewItem.Holder holder) {

    }

    public static class Holder extends RecyclerAdapterViewHolder {
        private ImageView mThumbnailImage, mShareImage;
        private TextView mNewsHeadline;

        public Holder(View view, RecyclerAdapterNotifier adapter) {
            super(view, adapter);
            mThumbnailImage = (ImageView) findViewById(R.id.thumbnail_image);
            mNewsHeadline = (TextView) findViewById(R.id.news_headline);
            mShareImage = (ImageView) findViewById(R.id.share_image);
        }
    }

    public void setOnItemClickListener(@NonNull OnItemClickListener onItemClickListener) {
        mListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int viewPosition, NewsBulletinItem item, View view);
    }
}
