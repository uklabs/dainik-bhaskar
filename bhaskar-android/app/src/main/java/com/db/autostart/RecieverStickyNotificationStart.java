package com.db.autostart;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.db.main.QuickNewsNotificationService2;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.QuickPreferences;

/**
 * Created by DB on 9/5/2017.
 */


public class RecieverStickyNotificationStart extends BroadcastReceiver {
//    Context ctx;

    @Override
    public void onReceive(Context context, Intent intent) {
//        ctx = context;
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            if (AppPreferences.getInstance(context).getIntValue(QuickPreferences.QUICK_READ_STATUS, 0) == 1)
                startNotification(context);
        }
    }


    private void startNotification(Context context) {
        if (context != null) {
            try {
                Intent intent = new Intent(context, QuickNewsNotificationService2.class);
                intent.putExtra("count", 0);
                intent.putExtra("paginationCount", 1);
                intent.setAction(Constants.QuickReadActions.ACTION_START);
                context.startService(intent);
            } catch (Exception e) {
            }
        }
    }
}