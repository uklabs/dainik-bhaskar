package com.db.citySelectionV2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bhaskar.R;
import com.db.data.models.CityInfo;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteCityAdapter extends ArrayAdapter<CityInfo> {
    private Context context;
    private int resource;
    private List<CityInfo> cityList;
    private List<CityInfo> tempItems;
    private List<CityInfo> suggestions;

    AutoCompleteCityAdapter(Context context, int resource, List<CityInfo> cities) {
        super(context, resource, cities);
        this.context = context;
        this.resource = resource;
        this.cityList = cities;
        this.tempItems = new ArrayList<>(cities);
        this.suggestions = new ArrayList<>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resource, parent, false);
        }
        CityInfo cityName = getItem(position);
        if (cityName != null) {
            TextView lblName = view.findViewById(R.id.textview);
            if (lblName != null)
                lblName.setText(String.format("%s (%s)", cityName.cityName, cityName.cityHindiName));
        }
        return view;
    }

    @Nullable
    @Override
    public CityInfo getItem(int position) {
        return cityList.get(position);
    }

    @Override
    public int getCount() {
        return cityList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public Filter getFilter() {
        return nameFilter;
    }


    private Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            CityInfo cityInfo = ((CityInfo) resultValue);
            return cityInfo.cityName;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (CityInfo names : tempItems) {
                    if (names.cityName.toLowerCase().contains(constraint.toString().toLowerCase()) || names.cityHindiName.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(names);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            List<CityInfo> tempValues = (List<CityInfo>) filterResults.values;
            if (filterResults.count > 0) {
                clear();
                for (CityInfo cityInfo : tempValues) {
                    add(cityInfo);
                    notifyDataSetChanged();
                }
            } else {
                clear();
                notifyDataSetChanged();
            }
        }
    };


}

