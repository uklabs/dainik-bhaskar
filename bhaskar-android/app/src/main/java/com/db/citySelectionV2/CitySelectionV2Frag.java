package com.db.citySelectionV2;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bhaskar.R;
import com.bhaskar.util.CommonConstants;
import com.db.data.models.CategoryInfo;
import com.db.data.models.CityInfo;
import com.db.news.PrefferedCityNewsListFragment;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CitySelectionV2Frag extends Fragment implements View.OnClickListener {
    private Context mContext;
    private TabLayout tabLayout;
    public SectionPagerAdapter adapter;
    public ViewPager viewPager;

    private CategoryInfo categoryInfo;
    private List<CityInfo> allCityInfoList;
    private List<CityInfo> cityTabList;
    private SectionPagerAdapter sectionPagerAdapter;
    private List<String> selectedCityIds;

    public CitySelectionV2Frag() {
        // Required empty public constructor
    }

    public static CitySelectionV2Frag newInstance(CategoryInfo categoryInfo) {
        CitySelectionV2Frag fragmentFirst = new CitySelectionV2Frag();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        fragmentFirst.setArguments(bundle);
        return fragmentFirst;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            categoryInfo = (CategoryInfo) arguments.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        cityTabList = new ArrayList<>();
        String selectedIds = AppPreferences.getInstance(mContext).getStringValue("selected_city_id", getDefaultCityId());
        selectedCityIds = Arrays.asList(selectedIds.split(","));
        allCityInfoList = JsonParser.getInstance().getCityFeedList(getContext(), CommonConstants.CHANNEL_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_selection_v2, container, false);
        tabLayout = view.findViewById(R.id.tabLayoutMyCity);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setSelectedTabIndicatorColor(AppUtils.getThemeColor(mContext, categoryInfo.color));
        tabLayout.setTabTextColors(AppUtils.getThemeColor(mContext, categoryInfo.color), AppUtils.getThemeColor(mContext, categoryInfo.color));

        viewPager = view.findViewById(R.id.viewpagermyCity);
        TextView selectTxt = view.findViewById(R.id.selectTxt);
        selectTxt.setOnClickListener(this);
        selectTxt.setBackgroundColor(AppUtils.getThemeColor(mContext, categoryInfo.color));

        cityTabList.addAll(getCityById(selectedCityIds));
        sectionPagerAdapter = new SectionPagerAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(sectionPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.selectTxt:
                showSelectedCityDialog(mContext);
                break;
        }
    }


    private class SectionPagerAdapter extends FragmentStatePagerAdapter {

        SectionPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return PrefferedCityNewsListFragment.newInstance(categoryInfo, cityTabList.get(position));
        }

        @Override
        public int getCount() {
            return cityTabList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return cityTabList.get(position).cityHindiName;
        }
    }


    private void setupAutoCompleteAdapter(AutoCompleteTextView autoCompleteTextView, final List<CityInfo> filteredList) {
        AutoCompleteCityAdapter autoCompleteCityAdapter = new AutoCompleteCityAdapter(mContext, R.layout.preferred_city_auto_complete_item, filteredList);
        autoCompleteTextView.setThreshold(2);
        autoCompleteTextView.setAdapter(autoCompleteCityAdapter);

    }

    private void showSelectedCityDialog(Context context) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        dialog.setContentView(inflater.inflate(R.layout.mera_saher_dialog, null));
        dialog.setCancelable(false);

        ImageView backIView = dialog.findViewById(R.id.backIView);
        TextView saveBtn = dialog.findViewById(R.id.saveBtn);

        List<CityInfo> filteredList = new ArrayList<>(allCityInfoList);
        List<CityInfo> finalSelectedList = new ArrayList<>();


        AutoCompleteTextView autoCompleteTextView1 = dialog.findViewById(R.id.autoCityName1);
        setupAutoCompleteAdapter(autoCompleteTextView1, filteredList);
        AutoCompleteTextView autoCompleteTextView2 = dialog.findViewById(R.id.autoCityName2);
        setupAutoCompleteAdapter(autoCompleteTextView2, filteredList);
        AutoCompleteTextView autoCompleteTextView3 = dialog.findViewById(R.id.autoCityName3);
        setupAutoCompleteAdapter(autoCompleteTextView3, filteredList);
        AutoCompleteTextView autoCompleteTextView4 = dialog.findViewById(R.id.autoCityName4);
        setupAutoCompleteAdapter(autoCompleteTextView4, filteredList);
        AutoCompleteTextView autoCompleteTextView5 = dialog.findViewById(R.id.autoCityName5);
        setupAutoCompleteAdapter(autoCompleteTextView5, filteredList);
        AutoCompleteTextView autoCompleteTextView6 = dialog.findViewById(R.id.autoCityName6);
        setupAutoCompleteAdapter(autoCompleteTextView6, filteredList);
        for (int i = 0; i < cityTabList.size(); i++) {
            switch (i) {
                case 0:
                    autoCompleteTextView1.setText(cityTabList.get(i).cityName);
                    autoCompleteTextView2.requestFocus();
                    break;

                case 1:
                    autoCompleteTextView2.setText(cityTabList.get(i).cityName);
                    autoCompleteTextView3.requestFocus();
                    break;

                case 2:
                    autoCompleteTextView3.setText(cityTabList.get(i).cityName);
                    autoCompleteTextView4.requestFocus();
                    break;

                case 3:
                    autoCompleteTextView4.setText(cityTabList.get(i).cityName);
                    autoCompleteTextView5.requestFocus();
                    break;

                case 4:
                    autoCompleteTextView5.setText(cityTabList.get(i).cityName);
                    autoCompleteTextView6.requestFocus();
                    break;

                case 5:
                    autoCompleteTextView6.setText(cityTabList.get(i).cityName);
                    autoCompleteTextView6.clearFocus();
                    break;

            }
        }

        dialog.findViewById(R.id.ivCancel1).setOnClickListener(v -> {
            autoCompleteTextView1.setText("");
        });
        dialog.findViewById(R.id.ivCancel2).setOnClickListener(v -> {
            autoCompleteTextView2.setText("");
        });
        dialog.findViewById(R.id.ivCancel3).setOnClickListener(v -> {
            autoCompleteTextView3.setText("");
        });
        dialog.findViewById(R.id.ivCancel4).setOnClickListener(v -> {
            autoCompleteTextView4.setText("");
        });
        dialog.findViewById(R.id.ivCancel5).setOnClickListener(v -> {
            autoCompleteTextView5.setText("");
        });
        dialog.findViewById(R.id.ivCancel6).setOnClickListener(v -> {
            autoCompleteTextView6.setText("");
        });

        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
////        ANIMATION
//            window.getAttributes().windowAnimations = R.style.dialogAnimation_Enter;
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.dimAmount = 0.6f;
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

        }

        backIView.setOnClickListener(view -> dialog.dismiss());

        saveBtn.setOnClickListener(view -> {
            analyzeSelectedCities(finalSelectedList, autoCompleteTextView1);
            analyzeSelectedCities(finalSelectedList, autoCompleteTextView2);
            analyzeSelectedCities(finalSelectedList, autoCompleteTextView3);
            analyzeSelectedCities(finalSelectedList, autoCompleteTextView4);
            analyzeSelectedCities(finalSelectedList, autoCompleteTextView5);
            analyzeSelectedCities(finalSelectedList, autoCompleteTextView6);
            cityTabList.clear();
            cityTabList.addAll(finalSelectedList);

            sectionPagerAdapter.notifyDataSetChanged();
            saveSelectedIds(cityTabList);
            dialog.dismiss();
        });

        dialog.show();
    }

    private void analyzeSelectedCities(List<CityInfo> finalSelectedList, AutoCompleteTextView autoCompleteTextView1) {
        String cityName = autoCompleteTextView1.getText().toString().trim();
        if (TextUtils.isEmpty(cityName))
            return;
        CityInfo cityInfo = getCityByName(cityName);
        if (cityInfo != null && !finalSelectedList.contains(cityInfo)) {
            finalSelectedList.add(cityInfo);
        }
    }

    private CityInfo getCityByName(String cityName) {
        for (CityInfo cityInfo : allCityInfoList) {
            if (cityInfo.cityName.equalsIgnoreCase(cityName))
                return cityInfo;
        }
        return null;
    }

    private void saveSelectedIds(List<CityInfo> cityTabList) {
        StringBuilder selectedIds = new StringBuilder();
        for (CityInfo city : cityTabList) {
            selectedIds.append(city.cityId).append(",");
        }
        AppPreferences.getInstance(mContext).setStringValue("selected_city_id", selectedIds.toString());

    }


    private void setAutoCompleteHint(CityInfo cityInfo, AutoCompleteTextView selectCityOne) {
        String cityName = String.format("%s (%s)", cityInfo.cityName, cityInfo.cityHindiName);//cityInfo.cityHindiName;
        selectCityOne.setHint(cityName);
    }


    private String getDefaultCityId() {
        String defaultCityId = "6729";
        CityInfo defaultCityInfo = null;
        if (CommonConstants.CHANNEL_ID.equalsIgnoreCase(Constants.AppIds.DAINIK_BHASKAR) || CommonConstants.CHANNEL_ID.equalsIgnoreCase(Constants.AppIds.MONEY_BHASKAR)) {
            //set default city bhopal
            defaultCityId = "6729";
        } else if (CommonConstants.CHANNEL_ID.equalsIgnoreCase(Constants.AppIds.DIVYA_BHASKAR)) {
            // set default city ahemdabad
            //TODO need to change
            defaultCityId = "6729";
        } else if (CommonConstants.CHANNEL_ID.equalsIgnoreCase(Constants.AppIds.DIVYA_MARATHI)) {
            //set default city maharastra
            defaultCityId = "5472";
        }
        return defaultCityId + ",";
    }

    private CityInfo getCityById(String cityId) {
        for (CityInfo cityInfo : allCityInfoList) {
            if (cityInfo.cityId.equalsIgnoreCase(cityId))
                return cityInfo;
        }
        return null;
    }

    private List<CityInfo> getCityById(List<String> cityIdList) {
        List<CityInfo> cityInfoList = new ArrayList<>();
        for (CityInfo cityInfo : allCityInfoList) {
            if (cityIdList.contains(cityInfo.cityId))
                cityInfoList.add(cityInfo);
        }
        return cityInfoList;
    }

}