package com.db.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.bhaskar.util.LoginController;
import com.db.InitApplication;
import com.db.home.ActivityUtil;
import com.db.news.WebFullScreenActivity;
import com.db.util.AppPreferences;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.OpenNewsLinkInApp;
import com.db.util.QuickPreferences;
import com.db.util.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class WebController {

    static WebController webController;

    public static WebController getWebController() {
        if (webController == null)
            webController = new WebController();
        return webController;
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    public void setWebViewSettings(Context mContext, WebView webView) {
        try {
            webView.loadUrl("about:blank");
            webView.clearCache(true);
            webView.clearHistory();

            webView.setOnKeyListener((v, keyCode, event) -> {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
                    webView.goBack();
                    return true;
                }
                return false;
            });
            webView.loadUrl("about:blank");
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setLoadWithOverviewMode(true);
            webSettings.setUseWideViewPort(true);

            File dir = InitApplication.getInstance().getCacheDir();
            if (!dir.exists()) {
                dir.mkdirs();
            }
            webSettings.setAppCachePath(dir.getPath());
            webSettings.setAppCacheEnabled(true);
            webSettings.setLoadsImagesAutomatically(true);
            webSettings.setDomStorageEnabled(true);
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            if (NetworkStatus.getInstance().isConnected(mContext)) {
                webSettings.setCacheMode(WebSettings.LOAD_DEFAULT); // load online by default
            } else {
                webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); // load online by default
            }
            webSettings.setBuiltInZoomControls(false);
            webSettings.setDisplayZoomControls(false);
            webSettings.setSupportZoom(false);
            if (Urls.IS_TESTING) {
                WebView.setWebContentsDebuggingEnabled(true);
            }

            webView.setFocusable(false);
            webView.setFocusableInTouchMode(false);
            webView.requestDisallowInterceptTouchEvent(false);

            /*Custom User Agent*/
            String userAgent = webSettings.getUserAgentString();
            userAgent = userAgent + ";CustomUserAgent:" + CommonConstants.USER_AGENT;
            webSettings.setUserAgentString(userAgent);

            webView.setHorizontalScrollBarEnabled(false);
            webView.setVerticalScrollBarEnabled(false);
            webView.setScrollContainer(true);
            webView.addJavascriptInterface(new JavaScriptInterface(mContext), "DivyaJSInterface");
            if (Urls.IS_TESTING) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        } catch (Exception e) {
            Log.e("setWebViewSettings(): ", "" + e.getMessage());
        }
    }

    public class JavaScriptInterface {

        private Context mContext;

        JavaScriptInterface(Context context) {
            mContext = context;
        }

        @JavascriptInterface
        public void openLink(String url) {
//            try {
//                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.SPECIAL_CONTENT, CTConstant.PROP_NAME.CLICKON, url, "", Utils.getUserDataMap());
//            } catch (Exception ignore) {
//            }

            OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(mContext, url, "", 1, "", null);
            openNewsDetailInApp.openLink();
        }

        @JavascriptInterface
        public void openMenu(String menuId) {
//            try {
//                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.SPECIAL_CONTENT, CTConstant.PROP_NAME.CLICKON, menuId, "", Utils.getUserDataMap());
//            } catch (Exception ignore) {
//            }

            ActivityUtil.openCategoryAccordingToAction(mContext, JsonParser.getInstance().getCategoryInfoBasedOnId(mContext, menuId), "");
        }

        @JavascriptInterface
        public void openFullPage(String pageUrl) {
//            try {
//                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.SPECIAL_CONTENT, CTConstant.PROP_NAME.CLICKON, pageUrl, "", Utils.getUserDataMap());
//            } catch (Exception ignore) {
//            }

            try {
                JSONObject jsonObject = new JSONObject(pageUrl);
                Intent webTV = new Intent(mContext, WebFullScreenActivity.class);
                webTV.putExtra(Constants.KeyPair.KEY_URL, jsonObject.optString("url"));
                webTV.putExtra(Constants.KeyPair.KEY_GA_SCREEN, pageUrl);
                webTV.putExtra(Constants.KeyPair.KEY_HAS_HEADER, jsonObject.optString("isHeader").equalsIgnoreCase("1"));
                mContext.startActivity(webTV);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void appTrackEvent(String jsonData) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                String catName = jsonObject.optString("catName");
                JSONArray eventArray = jsonObject.optJSONArray("events");

                ArrayList<ExtraCTEvent> ctEventArrayList = new ArrayList<>();
                String eventLabel = "";
                String eventValue = "";

                JSONObject jsObj;
                for (int i = 0; i < eventArray.length(); i++) {
                    jsObj = eventArray.getJSONObject(i);
                    if (i == 0) {
                        eventLabel = jsObj.optString("key");
                        eventValue = jsObj.optString("value");
                    } else {
                        if (!TextUtils.isEmpty(jsObj.optString("key"))) {
                            ctEventArrayList.add(new ExtraCTEvent(jsObj.optString("key"), jsObj.optString("value")));
                        }
                    }
                }

                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.SPECIAL_CONTENT, CTConstant.PROP_NAME.CLICKON, eventValue, "");
                CleverTapDB.getInstance(mContext).cleverTapTrackEvent(mContext, catName, eventLabel, eventValue, ctEventArrayList, LoginController.getUserDataMap());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void appTrackScreen(String screenName) {
            String source = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

            Tracking.trackGAScreen(mContext, InitApplication.getInstance().getDefaultTracker(), screenName, source, medium, campaign);
        }

        @JavascriptInterface
        public void rashiDetail(String jsonData) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                String gender = jsonObject.optString("gender");
                String rashiName = jsonObject.optString("rashiName");
                String numerono = jsonObject.optString("numerono");
                Log.e("Event Tracking ", "gender" + gender + "=> rashiName" + rashiName + "=> numerono" + numerono);

                CleverTapDB.getInstance(mContext).updateRashifalValuesOfUser(gender, rashiName, numerono);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
