package com.db.core

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.db.InitApplication

abstract class CoreActivity : AppCompatActivity() {
    lateinit var dbApp: InitApplication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    private fun init(){
        dbApp = application as InitApplication
    }

}