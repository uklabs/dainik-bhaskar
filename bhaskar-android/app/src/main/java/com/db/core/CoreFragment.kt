package com.db.core

import android.content.Context
import android.util.Log
import androidx.fragment.app.Fragment
import com.db.InitApplication

abstract class CoreFragment : Fragment() {

    lateinit var dbApp: InitApplication
    lateinit var coreActivity: CoreActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            coreActivity = context as CoreActivity
            dbApp = coreActivity.application as InitApplication
        } catch (e: Exception) {
            Log.e("CoreFragment", e.message + "")
        }
    }
}