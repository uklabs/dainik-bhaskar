package com.db.data.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.db.data.models.NewsListInfo;

import java.util.ArrayList;
import java.util.List;


/**
 * Data Storage For
 * App Widget Data
 */
public class AppWidgetTable {

    // Table Names
    public static final String TABLE_NAME = "appWidgetTable";
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    // Common column names
    private static final String KEY_CREATED_AT = "createdAt";

    // Common List Table - column names
    private static final String KEY_S_NO = "sno";
    private static final String KEY_TITLE = "title";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_IMAGE_BIG_SIZE = "imagebigsize";
    private static final String KEY_IMAGE_SIZE = "imageSize";
    private static final String KEY_STORY_ID = "storyId";
    private static final String KEY_CHANNEL_SL_NO = "channelSlNo";
    private static final String KEY_VIDEO_FLAG = "videoFlag";
    private static final String KEY_VERSION = "version";
    private static final String KEY_BIG_IMAGE = "bigImage";
    private static final String KEY_WIDGET_TYPE = "widgetType";
    Object lock = new Object();

    private DatabaseHelper appDb;

    public AppWidgetTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + KEY_S_NO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_TITLE + " TEXT," + KEY_IMAGE + " TEXT," + KEY_IMAGE_SIZE + " TEXT," + KEY_IMAGE_BIG_SIZE + " TEXT,"
                + KEY_STORY_ID + " TEXT," + KEY_CHANNEL_SL_NO + " TEXT,"
                + KEY_VIDEO_FLAG + " INTEGER," + KEY_VERSION + " TEXT,"
                + KEY_BIG_IMAGE + " INTEGER,"
                + KEY_WIDGET_TYPE + " INTEGER,"
                + KEY_CREATED_AT + " TEXT)";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            switch (oldVersion) {
                case 1:
                case 2:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                    createTable(db);
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String addAppWidgetList(List<NewsListInfo> newsListInfoList, int type) {
        deleteAllData(type);
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();

                for (int i = 0; i < newsListInfoList.size(); i++) {
                    values.clear();
                    NewsListInfo newsListInfo = newsListInfoList.get(i);
                    values.put(KEY_TITLE, newsListInfo.title);
                    values.put(KEY_IMAGE, newsListInfo.image);
                    values.put(KEY_IMAGE_BIG_SIZE, newsListInfo.imagebigsize);
                    values.put(KEY_IMAGE_SIZE, newsListInfo.imageSize);
                    values.put(KEY_STORY_ID, newsListInfo.storyId);
//                    values.put(KEY_CHANNEL_SL_NO, newsListInfo.channelSlno);
                    values.put(KEY_VIDEO_FLAG, newsListInfo.videoFlag);
                    values.put(KEY_VERSION, newsListInfo.version);
                    values.put(KEY_BIG_IMAGE, newsListInfo.bigImage);
                    values.put(KEY_WIDGET_TYPE, type);
                    values.put(KEY_CREATED_AT, getDateTime());
                    // insert row
                    returnValue = db.insert(TABLE_NAME, null, values);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }

    /**
     * //	 * getting all NewsList
     * //	 *
     */
    public List<NewsListInfo> getAllAppWidgetList(int type) {
        List<NewsListInfo> newsListInfoArrayList = new ArrayList<>();
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();
                String[] columns = new String[]{KEY_S_NO,
                        KEY_TITLE, KEY_IMAGE, KEY_IMAGE_BIG_SIZE,
                        KEY_IMAGE_SIZE, KEY_STORY_ID,
                        KEY_CHANNEL_SL_NO,
                        KEY_VIDEO_FLAG,
                        KEY_VERSION, KEY_BIG_IMAGE, KEY_CREATED_AT};
                String selection = KEY_WIDGET_TYPE + "==? ";
                String[] selectionArgs = new String[]{String.valueOf(type)};
                cursor = db.query(true, TABLE_NAME, columns, selection, selectionArgs, null, null, null, null);
                if (cursor.moveToFirst()) {
                    do {
                        NewsListInfo newsListInfo = getDataFromCursor(cursor);
                        newsListInfoArrayList.add(newsListInfo);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return newsListInfoArrayList;
    }

    private NewsListInfo getDataFromCursor(Cursor cursor) {
        NewsListInfo newsListInfo = new NewsListInfo();
        newsListInfo.title = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
        newsListInfo.image = cursor.getString((cursor.getColumnIndex(KEY_IMAGE)));
        newsListInfo.imagebigsize = cursor.getString((cursor.getColumnIndex(KEY_IMAGE_BIG_SIZE)));
        newsListInfo.imageSize = cursor.getString((cursor.getColumnIndex(KEY_IMAGE_SIZE)));
        newsListInfo.storyId = cursor.getString((cursor.getColumnIndex(KEY_STORY_ID)));
//        newsListInfo.channelSlno = cursor.getString((cursor.getColumnIndex(KEY_CHANNEL_SL_NO)));
        newsListInfo.videoFlag = cursor.getInt((cursor.getColumnIndex(KEY_VIDEO_FLAG)));
        newsListInfo.version = cursor.getString((cursor.getColumnIndex(KEY_VERSION)));
        newsListInfo.bigImage = cursor.getInt(cursor.getColumnIndex(KEY_BIG_IMAGE));
        return newsListInfo;
    }

    public int deleteAllData(int type) {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                String selection = KEY_WIDGET_TYPE + "==? ";
                String[] selectionArgs = new String[]{String.valueOf(type)};
                rowsAffected = db.delete(TABLE_NAME, selection, selectionArgs);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }

    /**
     * get datetime
     */
    private String getDateTime() {
        String currenttime = Long.toString(System.currentTimeMillis());
        return currenttime;
    }
}