package com.db.data.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.db.data.models.AuthorInfo;

import java.util.ArrayList;

public class AuthorListTable {

    // Table Names
    public static final String TABLE_NAME = "authorList";

    private static final String KEY_ID = "id";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_NAME = "name";
    private static final String KEY_TITLE = "title";

    Object lock = new Object();
    private DatabaseHelper appDb;

    public AuthorListTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + KEY_ID + " TEXT PRIMARY KEY,"
                + KEY_NAME + " TEXT," + KEY_TITLE + " TEXT," + KEY_IMAGE + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            createTable(db);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addAuthor(AuthorInfo authorInfo) {
        SQLiteDatabase db = null;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();

                values.put(KEY_ID, authorInfo.id);
                values.put(KEY_NAME, authorInfo.name);
                values.put(KEY_TITLE, authorInfo.title);
                values.put(KEY_IMAGE, authorInfo.image);

                db.insert(TABLE_NAME, null, values);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
    }

    @SuppressLint("UseSparseArrays")
    public ArrayList<AuthorInfo> getAuthorList() {
        ArrayList<AuthorInfo> authorList = new ArrayList<>();

        synchronized (lock) {
            Cursor cursor = null;
            try {
                SQLiteDatabase db = appDb.getWritableDatabase();
                cursor = db.query(TABLE_NAME, null, null, null, null, null, null);

                if (cursor.moveToFirst()) {
                    do {
                        AuthorInfo authorInfo = new AuthorInfo();
                        authorInfo.id = cursor.getString(cursor.getColumnIndex(KEY_ID));
                        authorInfo.name = cursor.getString((cursor.getColumnIndex(KEY_NAME)));
                        authorInfo.title = cursor.getString((cursor.getColumnIndex(KEY_TITLE)));
                        authorInfo.image = cursor.getString((cursor.getColumnIndex(KEY_IMAGE)));

                        authorList.add(authorInfo);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }

        return authorList;
    }

    @SuppressLint("UseSparseArrays")
    public ArrayList<String> getAuthorIdsList() {
        ArrayList<String> authorList = new ArrayList<>();

        synchronized (lock) {
            Cursor cursor = null;
            try {
                SQLiteDatabase db = appDb.getWritableDatabase();
                cursor = db.query(TABLE_NAME, null, null, null, null, null, null);

                if (cursor.moveToFirst()) {
                    do {
                        authorList.add(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }

        return authorList;
    }

    public void deleteAuthor(AuthorInfo info) {
        synchronized (lock) {
            try {
                SQLiteDatabase db = appDb.getWritableDatabase();
                db.delete(TABLE_NAME, KEY_ID + " = ?", new String[]{info.id});
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
    }
}
