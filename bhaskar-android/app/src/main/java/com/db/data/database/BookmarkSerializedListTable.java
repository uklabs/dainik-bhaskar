package com.db.data.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.db.InitApplication;
import com.db.data.models.BookmarkSerializedListInfo;
import com.db.util.AppPreferences;
import com.db.util.QuickPreferences;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DB on 8/17/2017.
 * Store BookmarkList
 */

public class BookmarkSerializedListTable {

    // Table Names
    public static final String TABLE_NAME = "bookmarkListTable";
    private final static int CREATED_VERSION = 2;
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    // BookmarkSerializedListTable column names
    private static final String KEY_CREATED_AT = "createdAt";

    // BookmarkSerializedList Table - column names
    private static final String KEY_TITLE = "title";
    private static final String KEY_COLOR = "color";
    private static final String KEY_IITL_TITLE = "iitl_title";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_STORY_ID = "story_id";
    private static final String KEY_CHANNEL_SL_NO = "channel_slno";
    private static final String KEY_SLUG_INTRO = "slug_intro";
    private static final String KEY_VIDEO_FLAG = "videoflag";
    private static final String KEY_DETAIL_URL = "detail_url";
    private static final String KEY_GA_SCREEN = "ga_screen";
    private static final String KEY_GA_ARTICLE = "ga_article";
    private static final String KEY_G_TRACK_URL = "g_track_url";
    private static final String KEY_PUB_DATE = "pubDate";
    private static final String KEY_PROVIDER_NAME = "providerName";
    private static final String KEY_PROVIDER_LOGO = "providerLogo";
    private static final String KEY_ACTION = "action";
    private static final String KEY_BRAND_ACTION = "brandAction";
    private static final String KEY_BRAND_CHANNEL = "brandChannel";
    private static final String KEY_WISDOM_DOMAIN = "wisdomDomain";
    private static final String KEY_WISDOM_SUB_DOMAIN = "wisdomSubDomain";
    private static final String KEY_CATEGORY_INFO_ID = "categoryInfoId";
    private static final String KEY_PARENT_ID = "parentId";
    private static final String KEY_CONTENT = "content";
    private static final String KEY_DISPLAY_NAME = "displayName";
    private static final String KEY_STORY_TYPE = "storyType";
    private static final String KEY_GA_EVENT_LABEL = "gaEventLabel";

    Object lock = new Object();

    private DatabaseHelper appDb;

    public BookmarkSerializedListTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
                + KEY_TITLE + " TEXT,"
                + KEY_COLOR + " TEXT,"
                + KEY_IITL_TITLE + " TEXT,"
                + KEY_IMAGE + " TEXT,"
                + KEY_STORY_ID + " TEXT,"
                + KEY_CHANNEL_SL_NO + " TEXT,"
                + KEY_SLUG_INTRO + " TEXT,"
                + KEY_VIDEO_FLAG + " INTEGER,"
                + KEY_DETAIL_URL + " TEXT,"
                + KEY_GA_SCREEN + " TEXT,"
                + KEY_GA_ARTICLE + " TEXT,"
                + KEY_G_TRACK_URL + " TEXT,"
                + KEY_PUB_DATE + " TEXT,"
                + KEY_PROVIDER_NAME + " TEXT,"
                + KEY_PROVIDER_LOGO + " TEXT,"
                + KEY_ACTION + " TEXT,"
                + KEY_BRAND_ACTION + " TEXT,"
                + KEY_BRAND_CHANNEL + " TEXT,"
                + KEY_WISDOM_DOMAIN + " TEXT,"
                + KEY_WISDOM_SUB_DOMAIN + " TEXT,"
                + KEY_CATEGORY_INFO_ID + " TEXT,"
                + KEY_DISPLAY_NAME + " TEXT,"
                + KEY_STORY_TYPE + " TEXT,"
                + KEY_GA_EVENT_LABEL + " TEXT,"
                + KEY_PARENT_ID + " TEXT,"
                + KEY_CONTENT + " TEXT,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (newVersion == CREATED_VERSION || oldVersion < CREATED_VERSION) {
                createTable(db);
                saveBookmarkoldAppData(db);

            } else {
                if (oldVersion < 9) {
                    try {
                        String cmd = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_STORY_TYPE + " TEXT";
                        String cmd1 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_GA_EVENT_LABEL + " TEXT";
                        db.execSQL(cmd);
                        db.execSQL(cmd1);
                    } catch (Exception ignored) {
                    }
                }
                if (oldVersion < 26) {
                    // need to change 15 to the db version from which new NewsV2 will get implemented.
                    try {
                        String cmd = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_PARENT_ID + " TEXT";
                        String cmd1 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_CONTENT + " TEXT";
                        String cmd2 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_IITL_TITLE + " TEXT";
                        String cmd3 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_G_TRACK_URL + " TEXT";
                        String cmd4 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_PUB_DATE + " TEXT";
                        String cmd5 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_PROVIDER_NAME + " TEXT";
                        String cmd6 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_COLOR + " TEXT";

                        try {
                            db.execSQL(cmd);
                        } catch (Exception e) {
                        }
                        try {
                            db.execSQL(cmd1);
                        } catch (Exception e) {
                        }
                        try {
                            db.execSQL(cmd2);
                        } catch (Exception e) {
                        }
                        try {
                            db.execSQL(cmd3);
                        } catch (Exception e) {
                        }
                        try {
                            db.execSQL(cmd4);
                        } catch (Exception e) {
                        }
                        try {
                            db.execSQL(cmd5);
                        } catch (Exception e) {
                        }
                        try {
                            db.execSQL(cmd6);
                        } catch (Exception e) {
                        }

                    } catch (Exception ignored) {
                    }
                }
                if (oldVersion < 27) {
                    String cmd = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_PROVIDER_LOGO + " TEXT";
                    try {
                        db.execSQL(cmd);
                    } catch (Exception e) {
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String addBookmarkSerializedItem(List<BookmarkSerializedListInfo> bookmarkSerializedListInfoList) {
//        deleteAllData();
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();

                for (int i = 0; i < bookmarkSerializedListInfoList.size(); i++) {
                    values.clear();
                    BookmarkSerializedListInfo bookmarkSerializedListInfo = bookmarkSerializedListInfoList.get(i);
                    values.put(KEY_TITLE, bookmarkSerializedListInfo.getTitle());
                    values.put(KEY_COLOR, bookmarkSerializedListInfo.getColor());
                    values.put(KEY_IITL_TITLE, bookmarkSerializedListInfo.getIitlTitle());
                    values.put(KEY_IMAGE, bookmarkSerializedListInfo.getImage());
                    values.put(KEY_STORY_ID, bookmarkSerializedListInfo.getStoryId());
//                    values.put(KEY_CHANNEL_SL_NO, bookmarkSerializedListInfo.getChannelSlno());
                    values.put(KEY_SLUG_INTRO, bookmarkSerializedListInfo.getSlugIntro());
                    values.put(KEY_VIDEO_FLAG, bookmarkSerializedListInfo.getVideoFlag());
                    values.put(KEY_DETAIL_URL, bookmarkSerializedListInfo.getDetailUrl());
                    values.put(KEY_GA_SCREEN, bookmarkSerializedListInfo.getGaScreen());
                    values.put(KEY_GA_ARTICLE, bookmarkSerializedListInfo.getGaArticle());
                    values.put(KEY_G_TRACK_URL, bookmarkSerializedListInfo.getgTrackUrl());
                    values.put(KEY_PUB_DATE, bookmarkSerializedListInfo.getPubDate());
                    values.put(KEY_PROVIDER_NAME, bookmarkSerializedListInfo.getProviderName());
                    values.put(KEY_PROVIDER_LOGO, bookmarkSerializedListInfo.getProviderLogo());
                    values.put(KEY_ACTION, bookmarkSerializedListInfo.getAction());
                    values.put(KEY_BRAND_ACTION, bookmarkSerializedListInfo.getBrandAction());
                    values.put(KEY_BRAND_CHANNEL, bookmarkSerializedListInfo.getBrandChannel());
                    values.put(KEY_WISDOM_DOMAIN, bookmarkSerializedListInfo.getWisdomDomain());
                    values.put(KEY_WISDOM_SUB_DOMAIN, bookmarkSerializedListInfo.getWisdomSubDomain());
                    values.put(KEY_CATEGORY_INFO_ID, bookmarkSerializedListInfo.getCategoryInfoId());
                    values.put(KEY_PARENT_ID, bookmarkSerializedListInfo.getParentID());
                    values.put(KEY_CONTENT, bookmarkSerializedListInfo.getContent());
                    values.put(KEY_DISPLAY_NAME, bookmarkSerializedListInfo.getDisplayName());
                    values.put(KEY_STORY_TYPE, bookmarkSerializedListInfo.getStorytype());
                    values.put(KEY_GA_EVENT_LABEL, bookmarkSerializedListInfo.getGaEventLabel());
                    values.put(KEY_CREATED_AT, getDateTime());

                    // insert row
                    returnValue = db.insert(TABLE_NAME, null, values);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }

    public String addBookmarkSerializedItem(SQLiteDatabase db, List<BookmarkSerializedListInfo> bookmarkSerializedListInfoList) {
//        deleteAllData();
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                ContentValues values = new ContentValues();

                for (int i = 0; i < bookmarkSerializedListInfoList.size(); i++) {
                    values.clear();

                    BookmarkSerializedListInfo bookmarkSerializedListInfo = bookmarkSerializedListInfoList.get(i);

                    values.put(KEY_TITLE, bookmarkSerializedListInfo.getTitle());
                    values.put(KEY_COLOR, bookmarkSerializedListInfo.getColor());
                    values.put(KEY_IITL_TITLE, bookmarkSerializedListInfo.getIitlTitle());
                    values.put(KEY_IMAGE, bookmarkSerializedListInfo.getImage());
                    values.put(KEY_STORY_ID, bookmarkSerializedListInfo.getStoryId());
//                    values.put(KEY_CHANNEL_SL_NO, bookmarkSerializedListInfo.getChannelSlno());
                    values.put(KEY_SLUG_INTRO, bookmarkSerializedListInfo.getSlugIntro());
                    values.put(KEY_VIDEO_FLAG, bookmarkSerializedListInfo.getVideoFlag());
                    values.put(KEY_DETAIL_URL, bookmarkSerializedListInfo.getDetailUrl());
                    values.put(KEY_GA_SCREEN, bookmarkSerializedListInfo.getGaScreen());
                    values.put(KEY_GA_ARTICLE, bookmarkSerializedListInfo.getGaArticle());
                    values.put(KEY_G_TRACK_URL, bookmarkSerializedListInfo.getgTrackUrl());
                    values.put(KEY_PUB_DATE, bookmarkSerializedListInfo.getPubDate());
                    values.put(KEY_PROVIDER_NAME, bookmarkSerializedListInfo.getProviderName());
                    values.put(KEY_PROVIDER_LOGO, bookmarkSerializedListInfo.getProviderLogo());
                    values.put(KEY_ACTION, bookmarkSerializedListInfo.getAction());
                    values.put(KEY_BRAND_ACTION, bookmarkSerializedListInfo.getBrandAction());
                    values.put(KEY_BRAND_CHANNEL, bookmarkSerializedListInfo.getBrandChannel());
                    values.put(KEY_WISDOM_DOMAIN, bookmarkSerializedListInfo.getWisdomDomain());
                    values.put(KEY_WISDOM_SUB_DOMAIN, bookmarkSerializedListInfo.getWisdomSubDomain());
                    values.put(KEY_CATEGORY_INFO_ID, bookmarkSerializedListInfo.getCategoryInfoId());
                    values.put(KEY_PARENT_ID, bookmarkSerializedListInfo.getParentID());
                    values.put(KEY_CONTENT, bookmarkSerializedListInfo.getContent());
                    values.put(KEY_DISPLAY_NAME, bookmarkSerializedListInfo.getDisplayName());
                    values.put(KEY_STORY_TYPE, bookmarkSerializedListInfo.getStorytype());
                    values.put(KEY_GA_EVENT_LABEL, bookmarkSerializedListInfo.getGaEventLabel());
                    values.put(KEY_CREATED_AT, getDateTime());

                    // insert row
                    returnValue = db.insert(TABLE_NAME, null, values);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
//                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }

    /**
     * //	 * getting all BookmarkSerialized List
     * //	 *
     */

    public ArrayList<BookmarkSerializedListInfo> getAllBookmarkSerializedInfoList() {
        ArrayList<BookmarkSerializedListInfo> bookmarkSerializedInfoArrayList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();
                cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        BookmarkSerializedListInfo bookmarkSerializedListInfo = new BookmarkSerializedListInfo();
                        bookmarkSerializedListInfo.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                        bookmarkSerializedListInfo.setColor(cursor.getString(cursor.getColumnIndex(KEY_COLOR)));
                        bookmarkSerializedListInfo.setIitlTitle(cursor.getString(cursor.getColumnIndex(KEY_IITL_TITLE)));
                        bookmarkSerializedListInfo.setImage(cursor.getString(cursor.getColumnIndex(KEY_IMAGE)));
                        bookmarkSerializedListInfo.setStoryId(cursor.getString(cursor.getColumnIndex(KEY_STORY_ID)));
//                        bookmarkSerializedListInfo.setChannelSlno(cursor.getString(cursor.getColumnIndex(KEY_CHANNEL_SL_NO)));
                        bookmarkSerializedListInfo.setSlugIntro(cursor.getString(cursor.getColumnIndex(KEY_SLUG_INTRO)));
                        bookmarkSerializedListInfo.setVideoFlag(cursor.getInt(cursor.getColumnIndex(KEY_VIDEO_FLAG)));
                        bookmarkSerializedListInfo.setDetailUrl(cursor.getString(cursor.getColumnIndex(KEY_DETAIL_URL)));
                        bookmarkSerializedListInfo.setGaScreen(cursor.getString(cursor.getColumnIndex(KEY_GA_SCREEN)));
                        bookmarkSerializedListInfo.setGaArticle(cursor.getString(cursor.getColumnIndex(KEY_GA_ARTICLE)));
                        bookmarkSerializedListInfo.setgTrackUrl(cursor.getString(cursor.getColumnIndex(KEY_G_TRACK_URL)));
                        bookmarkSerializedListInfo.setPubDate(cursor.getString(cursor.getColumnIndex(KEY_PUB_DATE)));
                        bookmarkSerializedListInfo.setProviderName(cursor.getString(cursor.getColumnIndex(KEY_PROVIDER_NAME)));
                        bookmarkSerializedListInfo.setProviderLogo(cursor.getString(cursor.getColumnIndex(KEY_PROVIDER_LOGO)));
                        bookmarkSerializedListInfo.setAction(cursor.getString(cursor.getColumnIndex(KEY_ACTION)));

                        bookmarkSerializedListInfo.setBrandAction(cursor.getString(cursor.getColumnIndex(KEY_BRAND_ACTION)));
                        bookmarkSerializedListInfo.setBrandChannel(cursor.getString(cursor.getColumnIndex(KEY_BRAND_CHANNEL)));
                        bookmarkSerializedListInfo.setWisdomDomain(cursor.getString(cursor.getColumnIndex(KEY_WISDOM_DOMAIN)));
                        bookmarkSerializedListInfo.setWisdomSubDomain(cursor.getString(cursor.getColumnIndex(KEY_WISDOM_SUB_DOMAIN)));
                        bookmarkSerializedListInfo.setCategoryInfoId(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY_INFO_ID)));
                        bookmarkSerializedListInfo.setParentID(cursor.getString(cursor.getColumnIndex(KEY_PARENT_ID)));
                        bookmarkSerializedListInfo.setContent(cursor.getString(cursor.getColumnIndex(KEY_CONTENT)));
                        bookmarkSerializedListInfo.setDisplayName(cursor.getString(cursor.getColumnIndex(KEY_DISPLAY_NAME)));
                        bookmarkSerializedListInfo.setStorytype(cursor.getString(cursor.getColumnIndex(KEY_STORY_TYPE)));
                        bookmarkSerializedListInfo.setGaEventLabel(cursor.getString(cursor.getColumnIndex(KEY_GA_EVENT_LABEL)));

                        // adding to bookmarkSerailzedlist
                        bookmarkSerializedInfoArrayList.add(bookmarkSerializedListInfo);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }

        return bookmarkSerializedInfoArrayList;
    }


    public void deleteParticularBookmarkSerializedListItem(String storyId) {

        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                // Delete row
                String addrWhere = KEY_STORY_ID + " = ?"; //this is where clause and below is corresponding selection args..
                String[] addrWhereParams = new String[]{String.valueOf(storyId)};
                db.delete(TABLE_NAME, addrWhere, addrWhereParams);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }

    }

    @SuppressLint("UseSparseArrays")
    public boolean isParticularBookmarkSerializedListItemAvailable(String storyId) {
//        ArrayList<BrandInfo> brandInfoArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();
                cursor = db.query(TABLE_NAME, new String[]{
                                KEY_TITLE,
                                KEY_COLOR,
                                KEY_IITL_TITLE,
                                KEY_IMAGE,
                                KEY_STORY_ID,
                                KEY_CHANNEL_SL_NO,
                                KEY_SLUG_INTRO,
                                KEY_VIDEO_FLAG,
                                KEY_DETAIL_URL,
                                KEY_GA_SCREEN,
                                KEY_GA_ARTICLE,
                                KEY_G_TRACK_URL,
                                KEY_PUB_DATE,
                                KEY_PROVIDER_NAME,
                                KEY_PROVIDER_LOGO,
                                KEY_ACTION,
                                KEY_BRAND_ACTION,
                                KEY_BRAND_CHANNEL,
                                KEY_WISDOM_DOMAIN,
                                KEY_WISDOM_SUB_DOMAIN,
                                KEY_CATEGORY_INFO_ID,
                                KEY_PARENT_ID,
                                KEY_CONTENT,
                                KEY_DISPLAY_NAME,
                                KEY_STORY_TYPE,
                                KEY_GA_EVENT_LABEL,
                                KEY_CREATED_AT}, "story_id = ?",
                        new String[]{String.valueOf(storyId)},
                        null,
                        null,
                        null);

                if (cursor.moveToFirst()) {
                    do {
                        String dbStoryId = cursor.getString((cursor.getColumnIndex(KEY_STORY_ID)));
                        if (dbStoryId.equalsIgnoreCase(storyId)) {
                            return true;
                        }
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return false;
    }

    public void saveBookmarkoldAppData(SQLiteDatabase db) {
        String jsonData = AppPreferences.getInstance(InitApplication.getInstance()).getStringValue(QuickPreferences.SerializePreference.BOOKMARKS_JSON_ARRAY, "");
        List<BookmarkSerializedListInfo> sectionInfos = new ArrayList<BookmarkSerializedListInfo>();
        if (!TextUtils.isEmpty(jsonData)) {
            try {
                JSONArray jsonArray = new JSONArray(jsonData);
                if (jsonArray != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<BookmarkSerializedListInfo>>() {
                    }.getType();
                    sectionInfos = gson.fromJson(jsonArray.toString(), type);
                    addBookmarkSerializedItem(db, sectionInfos);
                }
            } catch (Exception e) {

            }
        }

    }


    /**
     * get datetime
     */
    private String getDateTime() {
        String currenttime = Long.toString(System.currentTimeMillis());
        return currenttime;
    }
}
