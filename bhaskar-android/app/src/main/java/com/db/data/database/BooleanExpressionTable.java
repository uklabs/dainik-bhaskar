package com.db.data.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.db.util.Constants;
import com.db.util.Systr;

/**
 * Created by DB on 07-11-2017.
 * <p>Use this table, to insert boolean type expression as : isLiked, isFollowed, isReported, isAbused, etc.</p>
 */

public class BooleanExpressionTable {

    // Global Name
    public static final String TABLE_NAME = "BooleanTable";

    // In which version table created
    private final static int CREATED_VERSION = 6;

    // Table Name
    private static final String TABLE_BOOLEAN = "be_table";

    // column names
    private static final String KEY_ID = "id";
    private static final String KEY_BOOLEAN = "isBoolean";
    private static final String KEY_TYPE = "type";

    private Object lock = new Object();
    private DatabaseHelper appDb;

    public BooleanExpressionTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_BOOLEAN + "(" + KEY_ID + " TEXT," + KEY_BOOLEAN + " INTEGER," + KEY_TYPE + " INTEGER " + ")";

        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (newVersion == CREATED_VERSION || oldVersion < CREATED_VERSION) {
                createTable(db);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * <p>id : Unique id, like: video id, story id, etc.</p>
     * <p>type : It's defined in Constant.BooleanConst interface, declare your const in that interface.</p>
     * <p>value : true or false</p>
     */
    public void updateBooleanValue(String id, @Constants.BooleanConst int type, boolean value) {
        Systr.println("Update boolean value called");

        synchronized (lock) {
            try {
                SQLiteDatabase db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(KEY_BOOLEAN, value ? 1 : 0);

                int update = db.update(TABLE_BOOLEAN, values, KEY_ID + "=? AND " + KEY_TYPE + "=?", new String[]{id, String.valueOf(type)});
                if (update == 0) {
                    values.put(KEY_ID, id);
                    values.put(KEY_TYPE, type);
                    db.insert(TABLE_BOOLEAN, null, values);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
    }

    /**
     * <p>Object[] ids : It has array of ids, if has object of any other class then should override toString() method in that class and return id.</p>
     * <p>type : It's defined in Constant.BooleanConst interface, declare your const in that interface.</p>
     * <p>value : true or false</p>
     */
    public void updateBooleanValue(Object[] ids, @Constants.BooleanConst int type, boolean value) {
        Systr.println("Update boolean value called array");

        synchronized (lock) {
            try {
                SQLiteDatabase db = appDb.getWritableDatabase();

                for (int i = 0; i < ids.length; i++) {
                    ContentValues values = new ContentValues();
                    values.put(KEY_BOOLEAN, value ? 1 : 0);

                    int update = db.update(TABLE_BOOLEAN, values, KEY_ID + "=? AND " + KEY_TYPE + "=?", new String[]{ids[i].toString(), String.valueOf(type)});
                    if (update == 0) {
                        values.put(KEY_ID, ids[i].toString());
                        values.put(KEY_TYPE, type);
                        db.insert(TABLE_BOOLEAN, null, values);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
    }

    /**
     * <p>id : Unique id, like: video id, story id, etc.</p>
     * <p>type : It's defined in Constant.BooleanConst interface, declare your const in that interface.</p>
     */
    public boolean isBoolean(String id, @Constants.BooleanConst int type) {
        boolean like = false;
        synchronized (lock) {
            try {
                String countQuery = "SELECT " + KEY_BOOLEAN + " FROM " + TABLE_BOOLEAN + " where " + KEY_ID + "=? AND " + KEY_TYPE + "=?;";
                SQLiteDatabase db = appDb.getWritableDatabase();
                Cursor cursor = db.rawQuery(countQuery, new String[]{id, String.valueOf(type)});
                if (cursor.getCount() > 0 && cursor.moveToFirst()) {
                    like = (cursor.getInt(cursor.getColumnIndex(KEY_BOOLEAN)) == 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }

        return like;
    }
}
