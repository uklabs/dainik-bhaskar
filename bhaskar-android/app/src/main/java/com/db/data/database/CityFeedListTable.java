package com.db.data.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.db.data.models.CityInfo;
import com.db.data.models.RelatedArticleInfo;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by DB on 6/10/2017.
 */

/**
 * Data Storage For
 * Home StoryDetailTab :
 * 1.city Feed List
 */
public class CityFeedListTable {

    // Table Names
    public static final String TABLE_NAME = "cityFeedList";
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    // Common column names
    private static final String KEY_CREATED_AT = "createdAt";

    // dbCityFeedList Table - column names


    private static final String KEY_CITY_ID = "cityId";
    private static final String KEY_CITY_ENGLISH_NAME = "cityEnglishName";
    private static final String KEY_STATE_NAME = "stateName";

    private static final String KEY_RELATED_ARTICLE_ID = "related_article_id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_IITL = "iitl";
    private static final String KEY_STORY_ID = "storyId";
    private static final String KEY_CHANNEL_SL_NO = "channelSlNo";
    private static final String KEY_LINK = "link";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_IMAGE_SIZE = "imageSize";
    private static final String KEY_VERSION = "version";
    private static final String KEY_SLUG_INTRO = "slugIntro";
    private static final String KEY_PUB_DATE = "pubDate";
    private static final String KEY_VIDEO_FLAG = "videoFlag";
    private static final String KEY_CAT_ID = "catId";
    private static final String KEY_TEMPLATE_TYPE = "templateType";
    private static final String KEY_BIG_IMAGE = "bigImage";
    private static final String KEY_GTRACK_URL = "gTrackUrl";
    /*DB 2.1.1*/
    private static final String KEY_INFOG = "infog";
    private static final String KEY_INFOG_IMAGE1 = "infog_image1";
    private static final String KEY_INFOG_IMAGE2 = "infog_image2";
    private static final String KEY_INFOG_IMAGE3 = "infog_image3";

    /**/
    private static final String KEY_RELATED_VIDEO = "related_video";
    private static final String KEY_EXCLUSIVE = "exclusive";

    Object lock = new Object();

    private DatabaseHelper appDb;

    public CityFeedListTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + KEY_CITY_ID + " TEXT,"
                + KEY_CITY_ENGLISH_NAME + " TEXT," + KEY_STATE_NAME + " TEXT,"
                + KEY_RELATED_ARTICLE_ID + " TEXT," + KEY_TITLE + " TEXT,"+ KEY_IITL + " TEXT,"
                + KEY_STORY_ID + " TEXT," + KEY_CHANNEL_SL_NO + " TEXT,"
                + KEY_LINK + " TEXT," + KEY_IMAGE + " TEXT," + KEY_IMAGE_SIZE + " TEXT,"
                + KEY_VERSION + " TEXT," + KEY_SLUG_INTRO + " TEXT,"
                + KEY_PUB_DATE + " TEXT," + KEY_VIDEO_FLAG + " TEXT,"
                + KEY_CAT_ID + " TEXT," + KEY_TEMPLATE_TYPE + " TEXT," + KEY_BIG_IMAGE + " INTEGER,"+ KEY_GTRACK_URL + " TEXT,"
                + KEY_INFOG + " TEXT," + KEY_INFOG_IMAGE1 + " TEXT,"
                + KEY_INFOG_IMAGE2 + " TEXT," + KEY_INFOG_IMAGE3 + " TEXT,"
                + KEY_RELATED_VIDEO + " TEXT," + KEY_EXCLUSIVE + " TEXT,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }

    String ALTER_RELATED_COLUMN = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_RELATED_VIDEO + " TEXT";
    String ALTER_EXCLUSIVE_COLUMN = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_EXCLUSIVE + " TEXT";

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (oldVersion < 11) {
                db.execSQL(ALTER_RELATED_COLUMN);
                db.execSQL(ALTER_EXCLUSIVE_COLUMN);
            } else {
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                createTable(db);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        /*switch (oldVersion) {
            case 1:
            case 2:
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                createTable(db);
                break;
        }*/
    }

    public String addCityFeedListItem(CityInfo cityInfo, List<RelatedArticleInfo> relatedArticleInfoList, Context context) {
//        deleteAllData();
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();

                for (int i = 0; i < relatedArticleInfoList.size(); i++) {
                    values.clear();

                    RelatedArticleInfo relatedArticleInfo = relatedArticleInfoList.get(i);
                    values.put(KEY_CITY_ID, cityInfo.cityId);
                    values.put(KEY_CITY_ENGLISH_NAME, cityInfo.cityName);
                    values.put(KEY_STATE_NAME, cityInfo.stateName);

                    values.put(KEY_RELATED_ARTICLE_ID, relatedArticleInfo.relatedArticleId);
                    values.put(KEY_TITLE, relatedArticleInfo.title);
                    values.put(KEY_IITL, relatedArticleInfo.iitlTitle);
                    values.put(KEY_STORY_ID, relatedArticleInfo.storyId);
                    values.put(KEY_CHANNEL_SL_NO, relatedArticleInfo.channelSlno);
                    values.put(KEY_LINK, relatedArticleInfo.link);
                    values.put(KEY_IMAGE, relatedArticleInfo.image);
                    values.put(KEY_IMAGE_SIZE, relatedArticleInfo.imageSize);
                    values.put(KEY_VERSION, relatedArticleInfo.version);
                    values.put(KEY_SLUG_INTRO, relatedArticleInfo.slugIntro);
                    values.put(KEY_PUB_DATE, relatedArticleInfo.pubDate);
                    values.put(KEY_VIDEO_FLAG, relatedArticleInfo.videoFlag);
                    values.put(KEY_CAT_ID, relatedArticleInfo.catId);
                    values.put(KEY_TEMPLATE_TYPE, relatedArticleInfo.templateType);
                    values.put(KEY_BIG_IMAGE, relatedArticleInfo.bigImage);
                    values.put(KEY_GTRACK_URL, relatedArticleInfo.gTrackUrl);
                    values.put(KEY_CREATED_AT, getDateTime());
                    /*DB 2.1.1*/
                    values.put(KEY_INFOG, relatedArticleInfo.infog);
                    if (relatedArticleInfo.getImages() != null && relatedArticleInfo.getImages().size() == 3) {
                        values.put(KEY_INFOG_IMAGE1, relatedArticleInfo.getImages().get(0));
                        values.put(KEY_INFOG_IMAGE2, relatedArticleInfo.getImages().get(1));
                        values.put(KEY_INFOG_IMAGE3, relatedArticleInfo.getImages().get(2));
                    }

                    if (relatedArticleInfo.relatedVideo != null) {
                        values.put(KEY_RELATED_VIDEO, relatedArticleInfo.relatedVideo.storyId);
                        RelatedVideoTable relatedVideoTable = (RelatedVideoTable) DatabaseHelper.getInstance(context).getTableObject(RelatedVideoTable.TABLE_NAME);
                        relatedVideoTable.addRelatedVideoItem(relatedArticleInfo.relatedVideo, cityInfo.cityId, db);
                    }

                    values.put(KEY_EXCLUSIVE, relatedArticleInfo.exclusive);

                    // insert row
                    returnValue = db.insert(TABLE_NAME, null, values);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }



    /**
     * //	 * getting CityFeed List According to cityId
     * //	 *
     */

    @SuppressLint("UseSparseArrays")
    public ArrayList<RelatedArticleInfo> getAllCityFeedListInfoListAccordingToCity(String cityId, Context context) {
        ArrayList<RelatedArticleInfo> relatedArticleInfoArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();

                cursor = db.query(TABLE_NAME, new String[]{KEY_CITY_ID,
                                KEY_CITY_ENGLISH_NAME, KEY_STATE_NAME,
                                KEY_RELATED_ARTICLE_ID,
                                KEY_TITLE, KEY_IITL ,KEY_STORY_ID,
                                KEY_CHANNEL_SL_NO, KEY_LINK,
                                KEY_IMAGE, KEY_IMAGE_SIZE, KEY_VERSION,
                                KEY_SLUG_INTRO, KEY_PUB_DATE,
                                KEY_VIDEO_FLAG, KEY_CAT_ID, KEY_TEMPLATE_TYPE,
                                KEY_BIG_IMAGE, KEY_GTRACK_URL,KEY_INFOG, KEY_INFOG_IMAGE1,
                                KEY_INFOG_IMAGE2, KEY_INFOG_IMAGE3, KEY_RELATED_VIDEO,
                                KEY_CREATED_AT, KEY_EXCLUSIVE}, "cityId = ?",
                        new String[]{String.valueOf(cityId)},
                        null,
                        null,
                        null);

                if (cursor.moveToFirst()) {
                    do {
                        RelatedArticleInfo relatedArticleInfo = new RelatedArticleInfo();

                        relatedArticleInfo.relatedArticleId = cursor.getString(cursor.getColumnIndex(KEY_RELATED_ARTICLE_ID));
                        relatedArticleInfo.title = cursor.getString((cursor.getColumnIndex(KEY_TITLE)));
                        relatedArticleInfo.iitlTitle = cursor.getString((cursor.getColumnIndex(KEY_IITL)));
                        relatedArticleInfo.storyId = cursor.getString((cursor.getColumnIndex(KEY_STORY_ID)));
                        relatedArticleInfo.channelSlno = cursor.getString((cursor.getColumnIndex(KEY_CHANNEL_SL_NO)));
                        relatedArticleInfo.link = cursor.getString(cursor.getColumnIndex(KEY_LINK));
                        relatedArticleInfo.image = cursor.getString((cursor.getColumnIndex(KEY_IMAGE)));
                        relatedArticleInfo.imageSize = cursor.getString((cursor.getColumnIndex(KEY_IMAGE_SIZE)));
                        relatedArticleInfo.version = cursor.getString((cursor.getColumnIndex(KEY_VERSION)));
                        relatedArticleInfo.slugIntro = cursor.getString((cursor.getColumnIndex(KEY_SLUG_INTRO)));
                        relatedArticleInfo.pubDate = cursor.getString((cursor.getColumnIndex(KEY_PUB_DATE)));
                        relatedArticleInfo.videoFlag = cursor.getString(cursor.getColumnIndex(KEY_VIDEO_FLAG));

                        relatedArticleInfo.catId = cursor.getString((cursor.getColumnIndex(KEY_CAT_ID)));
                        relatedArticleInfo.templateType = cursor.getString((cursor.getColumnIndex(KEY_TEMPLATE_TYPE)));
                        relatedArticleInfo.bigImage = cursor.getInt((cursor.getColumnIndex(KEY_BIG_IMAGE)));
                        relatedArticleInfo.gTrackUrl = cursor.getString((cursor.getColumnIndex(KEY_GTRACK_URL)));
                        /*DB 2.1.1*/
                        relatedArticleInfo.infog = cursor.getString(cursor.getColumnIndex(KEY_INFOG));
                        if (!TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(KEY_INFOG_IMAGE1))) && !TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(KEY_INFOG_IMAGE2))) && !TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(KEY_INFOG_IMAGE3)))) {
                            ArrayList<String> infoGImages = new ArrayList<>();
                            infoGImages.add(0, cursor.getString(cursor.getColumnIndex(KEY_INFOG_IMAGE1)));
                            infoGImages.add(1, cursor.getString(cursor.getColumnIndex(KEY_INFOG_IMAGE2)));
                            infoGImages.add(2, cursor.getString(cursor.getColumnIndex(KEY_INFOG_IMAGE3)));
                            relatedArticleInfo.images = infoGImages;
                        }

                        String relatedVideoStoryId = cursor.getString(cursor.getColumnIndex(KEY_RELATED_VIDEO));
                        if (!TextUtils.isEmpty(relatedVideoStoryId)) {
                            RelatedVideoTable relatedVideoTable = (RelatedVideoTable) DatabaseHelper.getInstance(context).getTableObject(RelatedVideoTable.TABLE_NAME);
                            relatedArticleInfo.relatedVideo = relatedVideoTable.getRelatedVideo(relatedVideoStoryId, db);
                        }

                        relatedArticleInfo.exclusive = cursor.getString((cursor.getColumnIndex(KEY_EXCLUSIVE)));

                        // adding to tags list
                        relatedArticleInfoArrayList.add(relatedArticleInfo);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }

        return relatedArticleInfoArrayList;
    }


    public void deleteParticularCityFeedList(String cityId) {

        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                // Delete row
                String addrWhere = KEY_CITY_ID + " = ?"; //this is where clause and below is corresponding selection args..
                String[] addrWhereParams = new String[]{String.valueOf(cityId)};
                db.delete(TABLE_NAME, addrWhere, addrWhereParams);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }

    }

    /*
     * getting Table CityFeedList creation time
     */
    public String getCityFeedListItemCreationTime(String cityId) {
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        String createdAt = "";
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;

            try {
                db = appDb.getWritableDatabase();

                cursor = db.query(TABLE_NAME, new String[]{KEY_CITY_ID,
                                KEY_CITY_ENGLISH_NAME, KEY_STATE_NAME,
                                KEY_RELATED_ARTICLE_ID,
                                KEY_TITLE,KEY_IITL, KEY_STORY_ID,
                                KEY_CHANNEL_SL_NO, KEY_LINK,
                                KEY_IMAGE, KEY_IMAGE_SIZE, KEY_VERSION,
                                KEY_SLUG_INTRO, KEY_PUB_DATE,
                                KEY_VIDEO_FLAG, KEY_CAT_ID, KEY_TEMPLATE_TYPE,
                                KEY_BIG_IMAGE,
                                KEY_GTRACK_URL,
                                KEY_CREATED_AT}, "cityId = ?",
                        new String[]{String.valueOf(cityId)},
                        null,
                        null,
                        null);
                if (cursor.moveToFirst()) {
                    do {
                        createdAt = cursor.getString((cursor.getColumnIndex(KEY_CREATED_AT)));
                        return createdAt;
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }

        return createdAt;
    }


    /**
     * get datetime
     */
    private String getDateTime() {
        String currenttime = Long.toString(System.currentTimeMillis());
        return currenttime;
    }
}
