package com.db.data.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.db.data.models.NewsListInfo;
import com.db.data.models.NewsPhotoInfo;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Data Storage For
 * Home StoryDetailTab :
 * 1.Trending
 * 2.Khabray jara hat kay
 * 3.Rashiful
 * 4.Videsh
 * 5.Sports
 * 6.Lifestyle
 * 7.Health & diet
 * 8.DB Gadgets
 * 9.Desh
 * 10.Photograph
 * 11.Jokes
 */
public class CommonListTable {

    // Table Names
    public static final String TABLE_NAME = "commonListTable";
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    // Common column names
    private static final String KEY_CREATED_AT = "createdAt";

    // Common List Table - column names
    private static final String KEY_CATEGORY_ID = "categoryId";
    private static final String KEY_TITLE = "title";
    private static final String KEY_HEADER_NAME = "header_name";
    private static final String KEY_EDITOR_NAME = "editor_name";
    private static final String KEY_EDITOR_IMAGE = "editor_image";
    private static final String KEY_TOPIC_NAME = "topic_name";
    private static final String KEY_COLOR = "color_code";
    private static final String KEY_OPEN_GALLERY = "open_gallery";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_IMAGE_SIZE = "imageSize";
    private static final String KEY_STORY_ID = "storyId";
    private static final String KEY_CHANNEL_SL_NO = "channelSlNo";
    private static final String KEY_VIDEO_FLAG = "videoFlag";
    private static final String KEY_VERSION = "version";
    private static final String KEY_BIG_IMAGE = "bigImage";
    private static final String KEY_IITL_TITLE = "iitl_title";
    private static final String KEY_BULLETS = "bullets";

    /**
     * New changes
     **/
    private static final String KEY_PHOTO_DETAIL = "photoDetail";
    private static final String KEY_GA_G_TRACK_URL = "gTrackUrl";
    private static final String KEY_PUB_DATE = "pubDate";
    private static final String KEY_PROVIDER_NAME = "provider_name";
    private static final String KEY_PROVIDER_LOGO = "provider_logo";
    private static final String KEY_THUMBNAIL_URL = "thumbnail_url";

    /*new DB 2.1.1*/
    private static final String KEY_PAGE_COUNT_NUM = "pageCountNum";
    private static final String KEY_LIST_INDEX = "listIndex";
    private static final String KEY_SCROLL_INDEX = "scrollIndex";

    /**/
    private static final String KEY_EXCLUSIVE = "newsExclusive";
    private static final String KEY_IS_WEBVIEW = "isWebView";
    private static final String KEY_WEB_URL = "webUrl";
    Object lock = new Object();

    private DatabaseHelper appDb;

    private String stringArrayDelimeter = "ABC---123---abc";

    public CommonListTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + KEY_CATEGORY_ID + " TEXT,"
                + KEY_TITLE + " TEXT,"
                + KEY_HEADER_NAME + " TEXT,"
                + KEY_EDITOR_NAME + " TEXT,"
                + KEY_EDITOR_IMAGE + " TEXT,"
                + KEY_TOPIC_NAME + " TEXT,"
                + KEY_COLOR + " TEXT,"
                + KEY_OPEN_GALLERY + " TEXT,"
                + KEY_IMAGE + " TEXT,"
                + KEY_IMAGE_SIZE + " TEXT,"
                + KEY_STORY_ID + " TEXT,"
                + KEY_CHANNEL_SL_NO + " TEXT, "
                + KEY_VIDEO_FLAG + " INTEGER,"
                + KEY_VERSION + " TEXT, "
                + KEY_BIG_IMAGE + " INTEGER, "
                + KEY_EXCLUSIVE + " TEXT, "
                + KEY_IITL_TITLE + " TEXT, "
                + KEY_BULLETS + " TEXT, "
                + KEY_PHOTO_DETAIL + " TEXT, "
                + KEY_GA_G_TRACK_URL + " TEXT, "
                + KEY_PUB_DATE + " TEXT, "
                + KEY_PROVIDER_NAME + " TEXT, "
                + KEY_PROVIDER_LOGO + " TEXT, "
                + KEY_THUMBNAIL_URL + " TEXT, "
                + KEY_PAGE_COUNT_NUM + " INTEGER, "
                + KEY_LIST_INDEX + " INTEGER DEFAULT -1, "
                + KEY_SCROLL_INDEX + " INTEGER DEFAULT -1, "
                + KEY_IS_WEBVIEW + " INTEGER DEFAULT -1, "
                + KEY_WEB_URL + " TEXT, "
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (oldVersion < newVersion) {
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                createTable(db);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String addCommonListItem(String categoryId, List<NewsListInfo> newsListInfoList, Context context) {
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();

                for (int i = 0; i < newsListInfoList.size(); i++) {
                    values.clear();
                    NewsListInfo newsListInfo = newsListInfoList.get(i);
                    /*DB 2.1*/
                    values.put(KEY_CATEGORY_ID, categoryId);
                    values.put(KEY_TITLE, newsListInfo.title);
                    values.put(KEY_HEADER_NAME, newsListInfo.headerName);
                    values.put(KEY_EDITOR_NAME, newsListInfo.editorName);
                    values.put(KEY_EDITOR_IMAGE, newsListInfo.editorImage);
                    values.put(KEY_TOPIC_NAME, newsListInfo.exTopicName);
                    values.put(KEY_COLOR, newsListInfo.colorCode);
                    values.put(KEY_OPEN_GALLERY, (newsListInfo.isOpenGallery) ? "1" : "0");
                    values.put(KEY_IMAGE, newsListInfo.image);
                    values.put(KEY_IMAGE_SIZE, newsListInfo.imageSize);
                    values.put(KEY_STORY_ID, newsListInfo.storyId);
//                    values.put(KEY_CHANNEL_SL_NO, newsListInfo.channelSlno);
                    values.put(KEY_VIDEO_FLAG, newsListInfo.videoFlag);
                    values.put(KEY_VERSION, newsListInfo.version);
                    values.put(KEY_BIG_IMAGE, newsListInfo.bigImage);
                    values.put(KEY_EXCLUSIVE, newsListInfo.exclusive);
                    values.put(KEY_IS_WEBVIEW, (newsListInfo.isWebView) ? 1 : 0);
                    values.put(KEY_WEB_URL, newsListInfo.webUrl);
                    values.put(KEY_CREATED_AT, getDateTime());

                    values.put(KEY_IITL_TITLE, newsListInfo.iitl_title);
                    try {
                        values.put(KEY_BULLETS, TextUtils.join(stringArrayDelimeter, newsListInfo.bulletsPoint));
                    } catch (Exception e) {
                    }

                    String photodetails = new Gson().toJson(newsListInfo.photos);
                    values.put(KEY_PHOTO_DETAIL, photodetails);
                    values.put(KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
                    values.put(KEY_PUB_DATE, newsListInfo.pubdate);
                    values.put(KEY_PROVIDER_NAME, newsListInfo.provider);
                    values.put(KEY_PROVIDER_LOGO, newsListInfo.providerLogo);
                    values.put(KEY_THUMBNAIL_URL, newsListInfo.image);


                    // insert row
                    returnValue = db.insert(TABLE_NAME, null, values);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }

    }


    /**
     * getting newsList According to categoryId
     */
    @SuppressLint("UseSparseArrays")
    public ArrayList<NewsListInfo> getAllNewsListInfoListAccordingToCategory(String categoryId, Context context) {
        ArrayList<NewsListInfo> newsListInfoArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();
                cursor = db.query(TABLE_NAME, new String[]{KEY_CATEGORY_ID,
                                KEY_TITLE,
                                KEY_HEADER_NAME,
                                KEY_EDITOR_NAME,
                                KEY_EDITOR_IMAGE,
                                KEY_TOPIC_NAME,
                                KEY_COLOR,
                                KEY_OPEN_GALLERY,
                                KEY_IMAGE,
                                KEY_IITL_TITLE,
                                KEY_BULLETS,
                                KEY_PHOTO_DETAIL,
                                KEY_GA_G_TRACK_URL,
                                KEY_PUB_DATE,
                                KEY_PROVIDER_NAME,
                                KEY_PROVIDER_LOGO,
                                KEY_THUMBNAIL_URL,
                                KEY_IMAGE_SIZE,
                                KEY_STORY_ID,
                                KEY_CHANNEL_SL_NO,
                                KEY_VIDEO_FLAG,
                                KEY_VERSION,
                                KEY_BIG_IMAGE,
                                KEY_IS_WEBVIEW, KEY_WEB_URL,
                                KEY_CREATED_AT, KEY_EXCLUSIVE}, "categoryId = ?",
                        new String[]{String.valueOf(categoryId)},
                        null,
                        null,
                        null);
                if (cursor.moveToFirst()) {
                    do {
                        /*DB 2.1*/
                        NewsListInfo newsListInfo = new NewsListInfo();
                        newsListInfo.title = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        newsListInfo.headerName = cursor.getString(cursor.getColumnIndex(KEY_HEADER_NAME));
                        newsListInfo.editorName = cursor.getString(cursor.getColumnIndex(KEY_EDITOR_NAME));
                        newsListInfo.editorImage = cursor.getString(cursor.getColumnIndex(KEY_EDITOR_IMAGE));
                        newsListInfo.exTopicName = cursor.getString(cursor.getColumnIndex(KEY_TOPIC_NAME));
                        newsListInfo.colorCode = cursor.getString(cursor.getColumnIndex(KEY_COLOR));
                        newsListInfo.isOpenGallery = cursor.getString(cursor.getColumnIndex(KEY_OPEN_GALLERY)).equalsIgnoreCase("1");
                        newsListInfo.image = cursor.getString((cursor.getColumnIndex(KEY_IMAGE)));
                        newsListInfo.imageSize = cursor.getString((cursor.getColumnIndex(KEY_IMAGE_SIZE)));
                        newsListInfo.storyId = cursor.getString((cursor.getColumnIndex(KEY_STORY_ID)));
//                        newsListInfo.channelSlno = cursor.getString((cursor.getColumnIndex(KEY_CHANNEL_SL_NO)));
                        newsListInfo.videoFlag = cursor.getInt((cursor.getColumnIndex(KEY_VIDEO_FLAG)));
                        newsListInfo.version = cursor.getString((cursor.getColumnIndex(KEY_VERSION)));
                        newsListInfo.bigImage = cursor.getInt(cursor.getColumnIndex(KEY_BIG_IMAGE));
                        newsListInfo.exclusive = cursor.getString((cursor.getColumnIndex(KEY_EXCLUSIVE)));
                        newsListInfo.isWebView = (cursor.getInt((cursor.getColumnIndex(KEY_IS_WEBVIEW))) == 1);
                        newsListInfo.webUrl = cursor.getString((cursor.getColumnIndex(KEY_WEB_URL)));

                        newsListInfo.iitl_title = cursor.getString(cursor.getColumnIndex(KEY_IITL_TITLE));
                        try {
                            newsListInfo.bulletsPoint = TextUtils.split(cursor.getString(cursor.getColumnIndex(KEY_BULLETS)), stringArrayDelimeter);
                        } catch (Exception e) {
                        }

                        try {
                            String photos = cursor.getString(cursor.getColumnIndex(KEY_PHOTO_DETAIL));
                            if (!TextUtils.isEmpty(photos)) {
                                newsListInfo.photos = Arrays.asList(new Gson().fromJson(photos, NewsPhotoInfo[].class));
                            } else {
                                newsListInfo.photos = new ArrayList<>();
                            }
                        } catch (Exception e) {
                        }


                        newsListInfo.gTrackUrl = cursor.getString(cursor.getColumnIndex(KEY_GA_G_TRACK_URL));
                        newsListInfo.pubdate = cursor.getString(cursor.getColumnIndex(KEY_PUB_DATE));
                        newsListInfo.provider = cursor.getString(cursor.getColumnIndex(KEY_PROVIDER_NAME));
                        newsListInfo.providerLogo = cursor.getString(cursor.getColumnIndex(KEY_PROVIDER_LOGO));
                        newsListInfo.image = cursor.getString(cursor.getColumnIndex(KEY_THUMBNAIL_URL));

                        // adding to tags list
                        newsListInfoArrayList.add(newsListInfo);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }

        return newsListInfoArrayList;
    }


    /*
     * getting Total NewsList Count
     */
    public int getNewsListCount() {
        int count = 0;
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;

            try {
                db = appDb.getWritableDatabase();
                String countQuery = "SELECT  * FROM " + TABLE_NAME;
                cursor = db.rawQuery(countQuery, null);

                count = cursor.getCount();
                cursor.close();


            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        // return count
        return count;
    }

    public int deleteAllData(Context context, String categoryId) {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                rowsAffected = db.delete(TABLE_NAME, null, null);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
            RelatedVideoTable relatedVideoTable = (RelatedVideoTable) DatabaseHelper.getInstance(context).getTableObject(RelatedVideoTable.TABLE_NAME);
            relatedVideoTable.deleteRelatedVideo(categoryId);
        }
        return rowsAffected;
    }

    public void deleteParticularNewsListCategory(String categoryId, Context context) {

        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                // Delete row
                String addrWhere = KEY_CATEGORY_ID + " = ?"; //this is where clause and below is corresponding selection args..
                String[] addrWhereParams = new String[]{String.valueOf(categoryId)};
                db.delete(TABLE_NAME, addrWhere, addrWhereParams);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
            RelatedVideoTable relatedVideoTable = (RelatedVideoTable) DatabaseHelper.getInstance(context).getTableObject(RelatedVideoTable.TABLE_NAME);
            relatedVideoTable.deleteRelatedVideo(categoryId);
        }
    }

    /**
     * getting Common Table creation time
     */
    public String getCommonListItemCreationTime(String categoryId) {
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        String createdAt = "";
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;

            try {
                db = appDb.getWritableDatabase();
                cursor = db.query(TABLE_NAME, new String[]{KEY_CATEGORY_ID,
                                KEY_TITLE, KEY_HEADER_NAME, KEY_EDITOR_NAME, KEY_EDITOR_IMAGE, KEY_TOPIC_NAME, KEY_COLOR, KEY_OPEN_GALLERY, KEY_IMAGE,
                                KEY_IMAGE_SIZE, KEY_STORY_ID,
                                KEY_CHANNEL_SL_NO,
                                KEY_VIDEO_FLAG,
                                KEY_VERSION, KEY_BIG_IMAGE,
                                KEY_CREATED_AT}, "categoryId = ?",
                        new String[]{String.valueOf(categoryId)},
                        null,
                        null,
                        null);
                if (cursor.moveToFirst()) {
                    do {
                        createdAt = cursor.getString((cursor.getColumnIndex(KEY_CREATED_AT)));
                        return createdAt;
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return createdAt;
    }

    /**
     * get datetime
     */
    private String getDateTime() {
        String currenttime = Long.toString(System.currentTimeMillis());
        return currenttime;
    }
}
