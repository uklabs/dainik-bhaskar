package com.db.data.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.concurrent.atomic.AtomicInteger;

public class DatabaseHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 27;

    // Database Name
    private static final String DATABASE_NAME = "DainikBhaskarManager";
    private static DatabaseHelper instance = null;
    private final QuickNewsTable quicknewsTable;
    private SQLiteOpenHelper helper;
    //tables
    private CommonListTable commonListTable;
    private CityFeedListTable cityFeedListTable;
    private PhotoGalleryListTable photoGalleryListTable;

    private BookmarkSerializedListTable bookmarkSerializedListTable;
    private SelectedCityListTable selectedCityListTable;
    private AppWidgetTable appWidgetTable;
    private NotificationStackTable notificationStackTable;
    private BooleanExpressionTable booleanExpressionTable;
    private NewsSearchHistory newsSearchHistory;
    private RelatedVideoTable relatedVideoTable;
    private ReadUnreadStatusTable readUnreadStatusTable;
    private AuthorListTable authorTable;

    private AtomicInteger mOpenCounter = new AtomicInteger();
    private SQLiteDatabase mDatabase;

    private DatabaseHelper(Context context) {
        commonListTable = new CommonListTable(this);
        quicknewsTable = new QuickNewsTable(this);
        cityFeedListTable = new CityFeedListTable(this);
        photoGalleryListTable = new PhotoGalleryListTable(this);
        bookmarkSerializedListTable = new BookmarkSerializedListTable(this);
        selectedCityListTable = new SelectedCityListTable(this);
        appWidgetTable = new AppWidgetTable(this);
        notificationStackTable = new NotificationStackTable(this);
        booleanExpressionTable = new BooleanExpressionTable(this);
        newsSearchHistory = new NewsSearchHistory(this);
        relatedVideoTable = new RelatedVideoTable(this);
        readUnreadStatusTable = new ReadUnreadStatusTable(this);
        authorTable = new AuthorListTable(this);

        helper = new SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
            @Override
            public void onCreate(SQLiteDatabase db) {
                commonListTable.createTable(db);
                quicknewsTable.createTable(db);
                cityFeedListTable.createTable(db);
                photoGalleryListTable.createTable(db);
                bookmarkSerializedListTable.createTable(db);

                selectedCityListTable.createTable(db);
                appWidgetTable.createTable(db);
                notificationStackTable.createTable(db);
                booleanExpressionTable.createTable(db);
                newsSearchHistory.createTable(db);
                relatedVideoTable.createTable(db);
                readUnreadStatusTable.createTable(db);
                authorTable.createTable(db);
            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                commonListTable.onUpgrade(db, oldVersion, newVersion);
                quicknewsTable.onUpgrade(db, oldVersion, newVersion);
                cityFeedListTable.onUpgrade(db, oldVersion, newVersion);
                photoGalleryListTable.onUpgrade(db, oldVersion, newVersion);
                bookmarkSerializedListTable.onUpgrade(db, oldVersion, newVersion);

                selectedCityListTable.onUpgrade(db, oldVersion, newVersion);
                appWidgetTable.onUpgrade(db, oldVersion, newVersion);
                notificationStackTable.onUpgrade(db, oldVersion, newVersion);
                booleanExpressionTable.onUpgrade(db, oldVersion, newVersion);
                newsSearchHistory.onUpgrade(db, oldVersion, newVersion);
                relatedVideoTable.onUpgrade(db, oldVersion, newVersion);
                readUnreadStatusTable.onUpgrade(db, oldVersion, newVersion);
                authorTable.onUpgrade(db, oldVersion, newVersion);
            }
        };
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }
        return instance;
    }

    public synchronized SQLiteDatabase getWritableDatabase() {
        if (mOpenCounter.incrementAndGet() == 1 && helper != null) {
            try {
                mDatabase = helper.getWritableDatabase();
            } catch (SQLException s) {
                s.printStackTrace();
            }
        }
        return mDatabase;
    }

    public Object getTableObject(String tableName) {
        switch (tableName) {
            case CommonListTable.TABLE_NAME:
                return commonListTable;
            case QuickNewsTable.TABLE_NAME:
                return quicknewsTable;
            case CityFeedListTable.TABLE_NAME:
                return cityFeedListTable;
            case PhotoGalleryListTable.TABLE_NAME:
                return photoGalleryListTable;
            case BookmarkSerializedListTable.TABLE_NAME:
                return bookmarkSerializedListTable;
            case SelectedCityListTable.TABLE_NAME:
                return selectedCityListTable;
            case AppWidgetTable.TABLE_NAME:
                return appWidgetTable;
            case NotificationStackTable.TABLE_NAME:
                return notificationStackTable;
            case BooleanExpressionTable.TABLE_NAME:
                return booleanExpressionTable;
            case NewsSearchHistory.TABLE_NAME:
                return newsSearchHistory;
            case RelatedVideoTable.TABLE_NAME:
                return relatedVideoTable;
            case ReadUnreadStatusTable.TABLE_NAME:
                return readUnreadStatusTable;
            case AuthorListTable.TABLE_NAME:
                return authorTable;
        }
        return null;
    }

    public void closeDB() {
        if (mOpenCounter.decrementAndGet() == 0 && helper != null && mDatabase != null) {
            mDatabase.close();
        }
    }
}