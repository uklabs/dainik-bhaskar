package com.db.data.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lalgos1 on 01-02-2018.
 */

public class NewsSearchHistory {

    // Table Name
    public static final String TABLE_NAME = "NewsSearchHistory";

    // In which version table created
    private final static int CREATED_VERSION = 10;

    // column names
    private static final String KEY_ID = "id";
    private static final String KEY_VALUE = "value";
    private static final String KEY_SEARCH_AT = "searchedAt";

    private Object lock = new Object();
    private DatabaseHelper appDb;

    public NewsSearchHistory(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_VALUE + " TEXT, " + KEY_SEARCH_AT + " TEXT)";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (newVersion == CREATED_VERSION || oldVersion < CREATED_VERSION) {
                createTable(db);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String insertSearchKeyword(String searchKeyword) {
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.clear();
                values.put(KEY_VALUE, searchKeyword);
                values.put(KEY_SEARCH_AT, getDateTime());
                returnValue = db.insert(TABLE_NAME, null, values);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }

    public List<String> getAllSearchHistory() {
        List<String> searchHistoryList = new ArrayList<>();
        String selectQuery = "SELECT  " + KEY_VALUE + " FROM " + TABLE_NAME + " ORDER BY " + KEY_SEARCH_AT + " DESC";
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();
                cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        searchHistoryList.add(cursor.getString(cursor.getColumnIndex(KEY_VALUE)));
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return searchHistoryList;
    }

    private String getDateTime() {
        String currenttime = Long.toString(System.currentTimeMillis());
        return currenttime;
    }

    public String updateSearchTime(String searchKeyword) {
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.clear();
                values.put(KEY_SEARCH_AT, getDateTime());
                returnValue = db.update(TABLE_NAME, values, KEY_VALUE + "=? ", new String[]{searchKeyword});

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }

    public boolean keywordExist(String searchKeyword) {
        boolean exist = false;
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE " + KEY_VALUE + "='" + searchKeyword + "'";
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();
                cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToNext()) {
                    exist = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return exist;
    }
}
