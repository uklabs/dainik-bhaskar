package com.db.data.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.db.data.models.NotificationHubItemInfo;
import com.db.util.AppUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by DB on 8/25/2017.
 */

public class NotificationStackTable {

    // Table Names
    public static final String TABLE_NAME = "NotificationStackTable";
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    // NotificationStackTable Table column names
    private static final String KEY_CREATED_AT = "createdAt";

    // NotificationStackTable Table - column names
    private static final String KEY_TRACK_URL = "trackUrl";
    private static final String KEY_TITLE = "title";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_MENU_ID = "menuId";
    private static final String KEY_STORY_ID = "storyId";
    private static final String KEY_CHANNEL_SL_NO = "channelSlno";
    private static final String KEY_SLUG_INTRO = "slugIntro";
    private static final String KEY_PUB_DATE = "pubDate";
    private static final String KEY_VIDEO_FLAG = "videoFlag";
    private static final String KEY_G_TRACK_URL = "gTrackUrl";
    private static final String KEY_VERSION = "version";
    private static final String KEY_READ_STATUS = "readStatus";
    private static final String KEY_NOTIFICATION_ID = "notificationId";
    private static final String KEY_FILTERED_STORY_ID = "filteredStoryID";
    private static final String KEY_FETCH_TYPE = "fetchType";
    private static final String KEY_NOTIFICATION_OPEN_STATUS = "notificationOpenStatus";
    private static final String KEY_VENDOR_TYPE = "vendorType";


    Object lock = new Object();

    private DatabaseHelper appDb;

    public NotificationStackTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + KEY_TRACK_URL + " TEXT,"
                + KEY_TITLE + " TEXT,"
                + KEY_IMAGE + " TEXT," + KEY_MENU_ID + " TEXT,"
                + KEY_STORY_ID + " TEXT," + KEY_CHANNEL_SL_NO + " TEXT,"
                + KEY_SLUG_INTRO + " TEXT," + KEY_PUB_DATE + " TEXT,"
                + KEY_VIDEO_FLAG + " INTEGER," + KEY_G_TRACK_URL + " TEXT,"
                + KEY_VERSION + " TEXT," + KEY_READ_STATUS + " INTEGER," + KEY_NOTIFICATION_ID + " TEXT,"
                + KEY_FILTERED_STORY_ID + " TEXT,"
                + KEY_VENDOR_TYPE + " TEXT,"
                + KEY_FETCH_TYPE + " INTEGER,"
                + KEY_NOTIFICATION_OPEN_STATUS + " INTEGER,"
                + KEY_CREATED_AT + " INTEGER" + ")";

        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        for (int i = oldVersion; i < newVersion; i++) {
            try {
                switch (i) {
                    case 1:
                    case 2:
                        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                        createTable(db);
                        break;
                    case 3:
                    case 4:
                        db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_NOTIFICATION_OPEN_STATUS + " INTEGER");
                        break;
                    case 12:
                        db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_VENDOR_TYPE + " TEXT");
                        break;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

//        if (oldVersion < 13) {
//            db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + KEY_VENDOR_TYPE + " TEXT");
//        }
    }


    private void removeDuplicate(List<NotificationHubItemInfo> notificationHubItemInfoList) {
        for (int i = 0; i < notificationHubItemInfoList.size(); i++) {
            String storyId = notificationHubItemInfoList.get(i).storyId;
            if (ifExistStoryID(AppUtils.getFilteredStoryId(storyId))) {
                notificationHubItemInfoList.remove(notificationHubItemInfoList.get(i));
            }
        }
    }

    public String insertDataFromPush(NotificationHubItemInfo notificationHubItemInfo) {
//        deleteAllData();
        int fetchType = 0;
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();
                // insert row
                values.clear();
                returnValue = db.insert(TABLE_NAME, null, getContent(values, fetchType, notificationHubItemInfo));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }


    private ContentValues getContent(ContentValues values, int fetchType, NotificationHubItemInfo notificationHubItemInfo) {
        values.put(KEY_TRACK_URL, notificationHubItemInfo.trackUrl);
        values.put(KEY_TITLE, notificationHubItemInfo.getTitle());
        values.put(KEY_IMAGE, notificationHubItemInfo.getImage());
        values.put(KEY_MENU_ID, notificationHubItemInfo.getMenuId());
        values.put(KEY_STORY_ID, notificationHubItemInfo.getStoryId());
        values.put(KEY_CHANNEL_SL_NO, notificationHubItemInfo.getChannelSlno());
        values.put(KEY_SLUG_INTRO, notificationHubItemInfo.getSlugIntro());
        values.put(KEY_PUB_DATE, notificationHubItemInfo.getPubDate());
        values.put(KEY_VIDEO_FLAG, notificationHubItemInfo.getVideoFlag());
        values.put(KEY_G_TRACK_URL, notificationHubItemInfo.getgTrackUrl());
        values.put(KEY_VERSION, notificationHubItemInfo.getVersion());
        values.put(KEY_READ_STATUS, notificationHubItemInfo.getReadStatus());
        values.put(KEY_NOTIFICATION_ID, notificationHubItemInfo.getNotificationId());
        values.put(KEY_FILTERED_STORY_ID, AppUtils.getFilteredStoryId(notificationHubItemInfo.storyId));
        values.put(KEY_FETCH_TYPE, fetchType);
        values.put(KEY_NOTIFICATION_OPEN_STATUS, 0);
        values.put(KEY_VENDOR_TYPE, notificationHubItemInfo.vendorType);
        values.put(KEY_CREATED_AT, fetchType == 1 ? getDateTime(notificationHubItemInfo.pubDate) : getDateTime());
        return values;
    }


    /**
     * //	 * getting all NotificationStack List
     * //
     */

    public ArrayList<NotificationHubItemInfo> getAllNotificationStackTableList() {
        ArrayList<NotificationHubItemInfo> notificationHubItemInfoArrayList = new ArrayList<>();
//        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + KEY_FETCH_TYPE + " ASC, " + KEY_CREATED_AT + " DESC";
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();
//                cursor = db.rawQuery(selectQuery, null);

                String[] columns = new String[]{KEY_TRACK_URL, KEY_TITLE, KEY_IMAGE, KEY_MENU_ID,
                        KEY_STORY_ID, KEY_CHANNEL_SL_NO, KEY_SLUG_INTRO, KEY_PUB_DATE, KEY_VIDEO_FLAG,
                        KEY_G_TRACK_URL, KEY_VERSION, KEY_READ_STATUS, KEY_NOTIFICATION_ID, KEY_FILTERED_STORY_ID,
                        KEY_FETCH_TYPE, KEY_NOTIFICATION_OPEN_STATUS, KEY_CREATED_AT, KEY_VENDOR_TYPE};
                cursor = db.query(TABLE_NAME, columns, "fetchType= ?",
                        new String[]{"0"}, null, null, KEY_FETCH_TYPE + " ASC, " + KEY_CREATED_AT + " DESC", null);
                if (cursor.moveToFirst()) {
                    do {
                        NotificationHubItemInfo notificationHubItemInfo = new NotificationHubItemInfo();

                        notificationHubItemInfo.trackUrl = cursor.getString(cursor.getColumnIndex(KEY_TRACK_URL));
                        notificationHubItemInfo.title = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        notificationHubItemInfo.image = cursor.getString(cursor.getColumnIndex(KEY_IMAGE));
                        notificationHubItemInfo.menuId = cursor.getString(cursor.getColumnIndex(KEY_MENU_ID));
                        notificationHubItemInfo.storyId = cursor.getString(cursor.getColumnIndex(KEY_STORY_ID));
                        notificationHubItemInfo.channelSlno = cursor.getString(cursor.getColumnIndex(KEY_CHANNEL_SL_NO));
                        notificationHubItemInfo.slugIntro = cursor.getString(cursor.getColumnIndex(KEY_SLUG_INTRO));
                        notificationHubItemInfo.pubDate = cursor.getString(cursor.getColumnIndex(KEY_PUB_DATE));
                        notificationHubItemInfo.videoFlag = cursor.getInt(cursor.getColumnIndex(KEY_VIDEO_FLAG));
                        notificationHubItemInfo.gTrackUrl = cursor.getString(cursor.getColumnIndex(KEY_G_TRACK_URL));
                        notificationHubItemInfo.version = cursor.getString(cursor.getColumnIndex(KEY_VERSION));
                        notificationHubItemInfo.readStatus = cursor.getInt(cursor.getColumnIndex(KEY_READ_STATUS));
                        notificationHubItemInfo.notificationId = cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_ID));
                        notificationHubItemInfo.createdAt = cursor.getString(cursor.getColumnIndex(KEY_CREATED_AT));
                        notificationHubItemInfo.vendorType = cursor.getString(cursor.getColumnIndex(KEY_VENDOR_TYPE));
                        // adding to tags list
                        notificationHubItemInfoArrayList.add(notificationHubItemInfo);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return notificationHubItemInfoArrayList;
    }


    public ArrayList<NotificationHubItemInfo> getAllNotificationTrayList() {
        ArrayList<NotificationHubItemInfo> notificationHubItemInfoArrayList = new ArrayList<>();
//        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + KEY_FETCH_TYPE + " ASC, " + KEY_CREATED_AT + " DESC";
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();
//                cursor = db.rawQuery(selectQuery, null);
                String[] columns = new String[]{KEY_TRACK_URL, KEY_TITLE, KEY_IMAGE, KEY_MENU_ID,
                        KEY_STORY_ID, KEY_CHANNEL_SL_NO, KEY_SLUG_INTRO, KEY_PUB_DATE, KEY_VIDEO_FLAG,
                        KEY_G_TRACK_URL, KEY_VERSION, KEY_READ_STATUS, KEY_NOTIFICATION_ID, KEY_FILTERED_STORY_ID,
                        KEY_FETCH_TYPE, KEY_NOTIFICATION_OPEN_STATUS, KEY_CREATED_AT, KEY_VENDOR_TYPE};

                cursor = db.query(TABLE_NAME, columns, "fetchType = ? AND " + KEY_READ_STATUS + " = ? AND " + KEY_NOTIFICATION_OPEN_STATUS + " = ?",
                        new String[]{String.valueOf(0), String.valueOf(0), String.valueOf(0)},
                        null,
                        null,
                        null);
                if (cursor.moveToFirst()) {
                    do {
                        NotificationHubItemInfo notificationHubItemInfo = new NotificationHubItemInfo();

                        notificationHubItemInfo.title = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        notificationHubItemInfo.storyId = cursor.getString(cursor.getColumnIndex(KEY_STORY_ID));
                        notificationHubItemInfo.vendorType = cursor.getString(cursor.getColumnIndex(KEY_VENDOR_TYPE));

                        // adding to tags list
                        notificationHubItemInfoArrayList.add(notificationHubItemInfo);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return notificationHubItemInfoArrayList;
    }

    /**
     * //	 * getting NotificationStack List According to AppId
     * //	 *
     */



    public void updateReadStatus(String notificationId) {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                // Delete row
                String addrWhere = KEY_FILTERED_STORY_ID + " = ?"; //this is where clause and below is corresponding selection args..
                String[] addrWhereParams = new String[]{AppUtils.getFilteredStoryId(notificationId)};

                ContentValues values = new ContentValues();
                values.clear();
                values.put(KEY_READ_STATUS, 1);
                //Update Value
                db.update(TABLE_NAME, values, addrWhere, addrWhereParams);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }

    }

    public int deleteAllData() {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                rowsAffected = db.delete(TABLE_NAME, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }


    public void deleteNotificationStackBefore48hrsArrayList() {
        int rowsAffected = -1;
        synchronized (lock) {
            SQLiteDatabase db = null;

            try {
                db = appDb.getWritableDatabase();
                Long time = System.currentTimeMillis() - (48 * 60 * 60 * 1000);
                // Delete row
                String addrWhere = KEY_CREATED_AT + " <= ? AND " + KEY_FETCH_TYPE + "==?"; //this is where clause and below is corresponding selection args..
                String[] addrWhereParams = new String[]{String.valueOf(time), String.valueOf(0)};
                db.delete(TABLE_NAME, addrWhere, addrWhereParams);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }

    }

    public void updateNotificationOpenStatus(List<NotificationHubItemInfo> notificationHubItemInfoList) {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                for (int i = 0; i < notificationHubItemInfoList.size(); i++) {
                    String storyId = notificationHubItemInfoList.get(i).getStoryId();
                    String addrWhere = KEY_FILTERED_STORY_ID + " = ?"; //this is where clause and below is corresponding selection args..
                    String[] addrWhereParams = new String[]{AppUtils.getFilteredStoryId(storyId)};

                    ContentValues values = new ContentValues();
                    values.clear();
                    values.put(KEY_NOTIFICATION_OPEN_STATUS, 1);
                    //Update Value
                    db.update(TABLE_NAME, values, addrWhere, addrWhereParams);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
    }

    public boolean ifExistStoryID(String storyID) {

        SQLiteDatabase db = null;
        boolean isPresent = false;
        synchronized (lock) {
            try {
                String countQuery = "SELECT * FROM " + TABLE_NAME + " where " + KEY_FILTERED_STORY_ID + "=?;";
                db = appDb.getWritableDatabase();
                Cursor cursor = db.rawQuery(countQuery, new String[]{storyID});
                if (cursor.getCount() > 0) {
                    isPresent = true;
                } else {
                    isPresent = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return isPresent;
    }

    public void deleteParticularStoryID(String storyID) {

        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                // Delete row
                String addrWhere = KEY_FILTERED_STORY_ID + " = ? "; //this is where clause and below is corresponding selection args..
                String[] addrWhereParams = new String[]{String.valueOf(storyID)};
                db.delete(TABLE_NAME, addrWhere, addrWhereParams);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }

    }


    public int getNotificationReadStatus(String storyID) {
        int readStatus = 0;
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;

            try {
                db = appDb.getWritableDatabase();
                String countQuery = "SELECT  * FROM " + TABLE_NAME + " where " + KEY_FILTERED_STORY_ID + " = ?";
                cursor = db.rawQuery(countQuery, new String[]{AppUtils.getFilteredStoryId(storyID)});
                if (cursor.moveToFirst()) {
                    readStatus = cursor.getInt(cursor.getColumnIndex(KEY_READ_STATUS));
                }
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }

        return readStatus;
    }


    /**
     * get datetime
     */
    private Long getDateTime() {
        Long currenttime = System.currentTimeMillis();
        return currenttime;
    }

    private Long getDateTime(String date) {
        if (!TextUtils.isEmpty(date)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = new Date();
            try {
                date1 = format.parse(date);
            } catch (Exception e) {
            }
            return date1.getTime();
        } else {
            return getDateTime();
        }
    }
}

