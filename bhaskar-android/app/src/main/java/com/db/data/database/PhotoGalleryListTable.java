package com.db.data.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.db.data.models.PhotoListItemInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DB on 6/10/2017.
 */

/**
 * Data Storage For
 * Home StoryDetailTab :
 * 1.Photo gallery
 */
public class PhotoGalleryListTable {

    // Table Names
    public static final String TABLE_NAME = "photoGalleryList";
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    // Common column names
    private static final String KEY_CREATED_AT = "createdAt";

    // PhotoGallery Table - column names

    private static final String KEY_CATEGORY_ID = "categoryId";

    private static final String KEY_RSS_ID = "rssId";
    private static final String KEY_RSS_TITLE = "rssTitle";
    private static final String KEY_RSS_DESC = "rssDesc";
    private static final String KEY_RSS_IMAGE = "rssImage";
    private static final String KEY_IMAGE_SIZE = "imageSize";
    private static final String KEY_RSS_DATE = "rssDate";
    private static final String KEY_LINK = "link";
    private static final String KEY_TRACK_URL = "track_url";
    private static final String KEY_G_TRACK_URL = "g_track_url";
    private static final String KEY_PHOTO_COUNT = "photo_count";

    Object lock = new Object();

    private DatabaseHelper appDb;

    public PhotoGalleryListTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + KEY_CATEGORY_ID + " TEXT,"
                + KEY_RSS_ID + " TEXT," + KEY_RSS_TITLE + " TEXT,"
                + KEY_RSS_DESC + " TEXT," + KEY_RSS_IMAGE + " TEXT," + KEY_IMAGE_SIZE + " TEXT,"
                + KEY_RSS_DATE + " TEXT," + KEY_LINK + " TEXT,"
                + KEY_TRACK_URL + " TEXT," + KEY_G_TRACK_URL + " TEXT,"
                + KEY_PHOTO_COUNT + " TEXT,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (oldVersion < newVersion) {
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                createTable(db);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String addPhotoGalleryListItem(String categoryId, List<PhotoListItemInfo> photoListItemInfoList) {
//        deleteAllData();
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();

                for (int i = 0; i < photoListItemInfoList.size(); i++) {
                    values.clear();

                    PhotoListItemInfo photoListItemInfo = photoListItemInfoList.get(i);
                    values.put(KEY_CATEGORY_ID, categoryId);

                    values.put(KEY_RSS_ID, photoListItemInfo.rssId);
                    values.put(KEY_RSS_TITLE, photoListItemInfo.rssTitle);
                    values.put(KEY_RSS_DESC, photoListItemInfo.rssDesc);
                    values.put(KEY_RSS_IMAGE, photoListItemInfo.rssImage);
                    values.put(KEY_IMAGE_SIZE, photoListItemInfo.imageSize);
                    values.put(KEY_RSS_DATE, photoListItemInfo.rssDate);
                    values.put(KEY_LINK, photoListItemInfo.link);
                    values.put(KEY_TRACK_URL, photoListItemInfo.trackUrl);
                    values.put(KEY_G_TRACK_URL, photoListItemInfo.gTrackUrl);
                    values.put(KEY_PHOTO_COUNT, photoListItemInfo.photoCount);

                    values.put(KEY_CREATED_AT, getDateTime());
                    // insert row
                    returnValue = db.insert(TABLE_NAME, null, values);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }




    /**
     * //	 * getting PhotoGallery List According to categoryId
     * //	 *
     */

    @SuppressLint("UseSparseArrays")
    public ArrayList<PhotoListItemInfo> getAllPhotoGalleryInfoListAccordingToCategory(String categoryId) {
        ArrayList<PhotoListItemInfo> photoListItemInfoArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {

                db = appDb.getWritableDatabase();
                cursor = db.query(TABLE_NAME, new String[]{KEY_CATEGORY_ID, KEY_RSS_ID,
                                KEY_RSS_TITLE, KEY_RSS_DESC,
                                KEY_RSS_IMAGE, KEY_IMAGE_SIZE, KEY_RSS_DATE,
                                KEY_LINK, KEY_TRACK_URL,
                                KEY_G_TRACK_URL, KEY_PHOTO_COUNT,
                                KEY_CREATED_AT}, "categoryId = ?",
                        new String[]{String.valueOf(categoryId)},
                        null,
                        null,
                        null);

                if (cursor.moveToFirst()) {
                    do {
                        PhotoListItemInfo photoListItemInfo = new PhotoListItemInfo();

                        photoListItemInfo.rssId = cursor.getString(cursor.getColumnIndex(KEY_RSS_ID));
                        photoListItemInfo.rssTitle = cursor.getString((cursor.getColumnIndex(KEY_RSS_TITLE)));
                        photoListItemInfo.rssDesc = cursor.getString((cursor.getColumnIndex(KEY_RSS_DESC)));
                        photoListItemInfo.rssImage = cursor.getString((cursor.getColumnIndex(KEY_RSS_IMAGE)));
                        photoListItemInfo.imageSize = cursor.getString((cursor.getColumnIndex(KEY_IMAGE_SIZE)));
                        photoListItemInfo.rssDate = cursor.getString(cursor.getColumnIndex(KEY_RSS_DATE));
                        photoListItemInfo.link = cursor.getString((cursor.getColumnIndex(KEY_LINK)));
                        photoListItemInfo.trackUrl = cursor.getString((cursor.getColumnIndex(KEY_TRACK_URL)));
                        photoListItemInfo.gTrackUrl = cursor.getString((cursor.getColumnIndex(KEY_G_TRACK_URL)));
                        photoListItemInfo.photoCount = cursor.getString((cursor.getColumnIndex(KEY_PHOTO_COUNT)));

                        // adding to tags list
                        photoListItemInfoArrayList.add(photoListItemInfo);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }

        return photoListItemInfoArrayList;
    }



    public int deleteAllData() {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                rowsAffected = db.delete(TABLE_NAME, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }


    public void deleteParticularPhotoGalleryListCategory(String categoryId) {

        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                // Delete row
                String addrWhere = KEY_CATEGORY_ID + " = ?"; //this is where clause and below is corresponding selection args..
                String[] addrWhereParams = new String[]{String.valueOf(categoryId)};
                db.delete(TABLE_NAME, addrWhere, addrWhereParams);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }

    }

    /*
     * getting PhotoGallary Table creation time
     */
    public String getPhotoGallaryListItemCreationTime(String categoryId) {
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        String createdAt = "";
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;

            try {
                db = appDb.getWritableDatabase();
                cursor = db.query(TABLE_NAME, new String[]{KEY_CATEGORY_ID, KEY_RSS_ID,
                                KEY_RSS_TITLE, KEY_RSS_DESC,
                                KEY_RSS_IMAGE, KEY_IMAGE_SIZE, KEY_RSS_DATE,
                                KEY_LINK, KEY_TRACK_URL,
                                KEY_G_TRACK_URL, KEY_PHOTO_COUNT,
                                KEY_CREATED_AT}, "categoryId = ?",
                        new String[]{String.valueOf(categoryId)},
                        null,
                        null,
                        null);
                if (cursor.moveToFirst()) {
                    do {
                        createdAt = cursor.getString((cursor.getColumnIndex(KEY_CREATED_AT)));
                        return createdAt;
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }

        return createdAt;
    }


    /**
     * get datetime
     */
    private String getDateTime() {
        String currenttime = Long.toString(System.currentTimeMillis());
        return currenttime;
    }
}

