package com.db.data.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.db.data.models.NewsListInfo;

import java.util.ArrayList;
import java.util.List;


/**
 * Data Storage For
 * Home StoryDetailTab :
 * 1.Trending
 * 2.Khabray jara hat kay
 * 3.Rashiful
 * 4.Videsh
 * 5.Sports
 * 6.Lifestyle
 * 7.Health & diet
 * 8.DB Gadgets
 * 9.Desh
 * 10.Photograph
 * 11.Jokes
 */
public class QuickNewsTable {

    // Table Names
    public static final String TABLE_NAME = "quickNewsTable";
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    // Common column names
    private static final String KEY_CREATED_AT = "createdAt";

    // Common List Table - column names
    private static final String KEY_TITLE = "title";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_IMAGE_SIZE = "imageSize";
    private static final String KEY_STORY_ID = "storyId";
    private static final String KEY_CHANNEL_SL_NO = "channelSlNo";
    private static final String KEY_VIDEO_FLAG = "videoFlag";
    private static final String KEY_VERSION = "version";
    private static final String KEY_BIG_IMAGE = "bigImage";
    Object lock = new Object();

    private DatabaseHelper appDb;

    public QuickNewsTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
                + KEY_TITLE + " TEXT," + KEY_IMAGE + " TEXT," + KEY_IMAGE_SIZE + " TEXT,"
                + KEY_STORY_ID + " TEXT," + KEY_CHANNEL_SL_NO + " TEXT,"
                + KEY_VIDEO_FLAG + " INTEGER," + KEY_VERSION + " TEXT,"
                + KEY_BIG_IMAGE + " INTEGER,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            switch (oldVersion) {
                case 1:
                case 2:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                    createTable(db);
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String addCommonListItem(List<NewsListInfo> newsListInfoList) {
        SQLiteDatabase db = null;
        long returnValue = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();

                if (newsListInfoList != null) {
                    for (int i = 0; i < newsListInfoList.size(); i++) {
                        values.clear();
                        NewsListInfo newsListInfo = newsListInfoList.get(i);
                        values.put(KEY_TITLE, newsListInfo.title);
                        values.put(KEY_IMAGE, newsListInfo.image);
                        values.put(KEY_IMAGE_SIZE, newsListInfo.imageSize);
                        values.put(KEY_STORY_ID, newsListInfo.storyId);
//                        values.put(KEY_CHANNEL_SL_NO, newsListInfo.channelSlno);
                        values.put(KEY_VIDEO_FLAG, newsListInfo.videoFlag);
                        values.put(KEY_VERSION, newsListInfo.version);
                        values.put(KEY_BIG_IMAGE, newsListInfo.bigImage);
                        values.put(KEY_CREATED_AT, getDateTime());

                        returnValue = db.insert(TABLE_NAME, null, values);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf("");
        } else {
            return "";
        }
    }


    /**
     * //	 * getting all NewsList
     * //	 *
     */

    public ArrayList<NewsListInfo> getAllNewsListInfoList() {
        ArrayList<NewsListInfo> newsListInfoArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();
                cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        NewsListInfo newsListInfo = new NewsListInfo();
                        newsListInfo.title = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        newsListInfo.image = cursor.getString((cursor.getColumnIndex(KEY_IMAGE)));
                        newsListInfo.imageSize = cursor.getString((cursor.getColumnIndex(KEY_IMAGE_SIZE)));
                        newsListInfo.storyId = cursor.getString((cursor.getColumnIndex(KEY_STORY_ID)));
//                        newsListInfo.channelSlno = cursor.getString((cursor.getColumnIndex(KEY_CHANNEL_SL_NO)));
                        newsListInfo.videoFlag = cursor.getInt((cursor.getColumnIndex(KEY_VIDEO_FLAG)));
                        newsListInfo.version = cursor.getString((cursor.getColumnIndex(KEY_VERSION)));
                        newsListInfo.bigImage = cursor.getInt(cursor.getColumnIndex(KEY_BIG_IMAGE));
                        newsListInfoArrayList.add(newsListInfo);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }

        return newsListInfoArrayList;
    }





    public int deleteAllData() {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                rowsAffected = db.delete(TABLE_NAME, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }



  /*  public boolean isTableExists(String tableName, boolean openDb) {
        if(openDb) {
            if(appDb == null || !appDb.isOpen()) {
                appDb = getReadableDatabase();
            }

            if(!appDb.isReadOnly()) {
                appDb.close();
                appDb = getReadableDatabase();
            }
        }

        Cursor cursor = appDb.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }*/

    /**
     * get datetime
     */
    private String getDateTime() {
        String currenttime = Long.toString(System.currentTimeMillis());
        return currenttime;
    }
}