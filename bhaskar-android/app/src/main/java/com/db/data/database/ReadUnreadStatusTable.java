package com.db.data.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.db.util.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by DB on 07-11-2017.
 * <p>Use this table, to insert boolean type expression as : isLiked, isFollowed, isReported, isAbused, etc.</p>
 */

public class ReadUnreadStatusTable {

    // Global Name
    public static final String TABLE_NAME = "ReadUnreadStatusTable";

    // In which version table created
    private final static int CREATED_VERSION = 15;

    // Table Name
    private static final String TABLE_READ_UNREAD = "read_unread_table";

    // column names
    private static final String KEY_ID = "id";
    private static final String KEY_STORY_ID = "storyId";
    private static final String KEY_STORY_VERSION = "storyVersion";
    private static final String KEY_CREATED_TIME = "createdTime";

    private Object lock = new Object();
    private DatabaseHelper appDb;

    public ReadUnreadStatusTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_READ_UNREAD
                + "(" + KEY_ID + " TEXT,"
                + KEY_STORY_ID + " TEXT,"
                + KEY_STORY_VERSION + " TEXT,"
                + KEY_CREATED_TIME + " TEXT " + ")";

        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (newVersion == CREATED_VERSION || oldVersion < CREATED_VERSION) {
                createTable(db);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateStoryVersion(String storyId, String storyVersion) {
        if (TextUtils.isEmpty(storyVersion)) {
            storyVersion = "0";
        }
        synchronized (lock) {
            try {
                SQLiteDatabase db = appDb.getWritableDatabase();


                ContentValues values = new ContentValues();
                values.put(KEY_STORY_VERSION, storyVersion);

                int update = db.update(TABLE_READ_UNREAD, values, KEY_ID + "=?", new String[]{storyId});
                if (update == 0) {
                    values.put(KEY_STORY_ID, storyId);
                    values.put(KEY_STORY_VERSION, storyVersion);
                    values.put(KEY_CREATED_TIME, getDateTime());
                    db.insert(TABLE_READ_UNREAD, null, values);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
    }


    public boolean isStoryWithVersionAvailable(String storyId, String storyVersion) {
        if (TextUtils.isEmpty(storyVersion)) {
            storyVersion = "0";
        }
        boolean isAvailable = false;
        synchronized (lock) {
            try {
                String countQuery = "SELECT * FROM " + TABLE_READ_UNREAD + " where " + KEY_STORY_ID + "=? AND " + KEY_STORY_VERSION + "=?;";
                SQLiteDatabase db = appDb.getWritableDatabase();
                Cursor cursor = db.rawQuery(countQuery, new String[]{storyId, storyVersion});
                if (cursor.getCount() > 0 && cursor.moveToFirst()) {
                    isAvailable = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        /*By  Brajesh*/
        return false;
    }


    private String getDateTime() {
        String currenttime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
        try {
            Date mDate = sdf.parse(Constants.currentDate);
            currenttime = Long.toString(mDate.getTime());
        } catch (ParseException e) {
        }
        return currenttime;
    }
}
