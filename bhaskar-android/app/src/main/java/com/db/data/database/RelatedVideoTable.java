package com.db.data.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.db.dbvideoPersonalized.data.VideoInfo;


public class RelatedVideoTable {

    // Table Names
    public static final String TABLE_NAME = "relatedVideoTable";
    // Logcat tag
    private static final String LOG = "DatabaseHelper";


    private static final String KEY_TITLE = "title";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_MEDIA_TITLE = "media_title";
    private static final String KEY_STORY_ID = "story_id";
    private static final String KEY_CHANNEL_SL_NO = "channel_slno";
    private static final String KEY_PUB_DATE = "pubdate";
    private static final String KEY_VIDEO_FLAG = "videoflag";
    private static final String KEY_CAT_ID = "cat_id";
    private static final String KEY_TIME_AGO = "timeago";
    private static final String KEY_LINK = "link";
    private static final String KEY_VIDEO_URL = "video_url";
    private static final String KEY_VIEWS = "views";
    private static final String KEY_DURATION = "duration";
    private static final String KEY_INTERNAL_VIDEO = "internal_video";
    private static final String KEY_MINIFEST = "minifest";
    private static final String KEY_STORY = "story";
    private static final String KEY_VERSION = "version";
    private static final String KEY_G_TRACK_URL = "g_track_url";
    private static final String KEY_BIG_IMAGE = "bigimage";
    private static final String KEY_PROVIDER_CODE = "provider_code";
    private static final String KEY_PROVIDER_NAME = "provider_name";
    private static final String KEY_PROVIDER_LOGO = "provider_logo";
    private static final String KEY_WALL_BRAND_ID = "wall_brand_id";
    private static final String KEY_PROVIDER_ID = "provider_id";
    private static final String KEY_SHOW_PROVIDER = "show_provider";
    private static final String KEY_LIKE = "like";
    private static final String KEY_FB_SHARE = "fbshare";
    private static final String KEY_WHATS_SHARE = "whshare";
    private static final String KEY_ULTIMA_TRACK_URL = "ultima_track_url";
    private static final String KEY_CATEGORY_ID = "category_id";

    // Common column names
    private static final String KEY_CREATED_AT = "createdAt";

    Object lock = new Object();

    private DatabaseHelper appDb;

    public RelatedVideoTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + KEY_MEDIA_TITLE + " TEXT,"
                + KEY_TITLE + " TEXT," + KEY_IMAGE + " TEXT," + KEY_TIME_AGO + " TEXT,"
                + KEY_STORY_ID + " TEXT PRIMARY KEY," + KEY_CHANNEL_SL_NO + " TEXT,"
                + KEY_VIDEO_URL + " TEXT," + KEY_PUB_DATE + " TEXT,"
                + KEY_VIDEO_FLAG + " INTEGER," + KEY_VERSION + " TEXT,"
                + KEY_CAT_ID + " TEXT," + KEY_VIEWS + " TEXT,"
                + KEY_LINK + " TEXT," + KEY_BIG_IMAGE + " INTEGER,"
                + KEY_DURATION + " TEXT," + KEY_INTERNAL_VIDEO + " TEXT,"
                + KEY_MINIFEST + " TEXT," + KEY_STORY + " TEXT,"
                + KEY_G_TRACK_URL + " TEXT," + KEY_PROVIDER_CODE + " TEXT,"
                + KEY_PROVIDER_NAME + " TEXT," + KEY_PROVIDER_LOGO + " TEXT,"
                + KEY_WALL_BRAND_ID + " TEXT," + KEY_PROVIDER_ID + " TEXT,"
                + KEY_SHOW_PROVIDER + " TEXT," + KEY_LIKE + " TEXT,"
                + KEY_FB_SHARE + " TEXT," + KEY_WHATS_SHARE + " TEXT,"
                + KEY_CATEGORY_ID + " TEXT,"
                + KEY_ULTIMA_TRACK_URL + " TEXT,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }


    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (oldVersion < 11) {
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                createTable(db);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String addRelatedVideoItem(VideoInfo videoInfo, String categoryId, SQLiteDatabase db) {
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                ContentValues values = new ContentValues();
                values.clear();
                values.put(KEY_TITLE, videoInfo.title);
                values.put(KEY_IMAGE, videoInfo.image);
                values.put(KEY_MEDIA_TITLE, videoInfo.mediaTitle);
                values.put(KEY_STORY_ID, videoInfo.storyId);
//                values.put(KEY_CHANNEL_SL_NO, videoInfo.channelSlno);
//                values.put(KEY_PUB_DATE, videoInfo.pubDate);
                values.put(KEY_VIDEO_FLAG, videoInfo.videoFlag);
//                values.put(KEY_CAT_ID, videoInfo.catId);
//                values.put(KEY_TIME_AGO, videoInfo.timeAgo);
                values.put(KEY_LINK, videoInfo.link);
//                values.put(KEY_VIDEO_URL, videoInfo.videoUrl);
                values.put(KEY_VIEWS, videoInfo.views);
                values.put(KEY_DURATION, videoInfo.duration);
                values.put(KEY_INTERNAL_VIDEO, videoInfo.internalVideo);
//                values.put(KEY_MINIFEST, videoInfo.adaptiveVideo);
                values.put(KEY_STORY, videoInfo.story);
                values.put(KEY_VERSION, videoInfo.version);
                values.put(KEY_G_TRACK_URL, videoInfo.gTrackUrl);
                values.put(KEY_BIG_IMAGE, videoInfo.bigImage);
                values.put(KEY_PROVIDER_CODE, videoInfo.provider_code);
                values.put(KEY_PROVIDER_NAME, videoInfo.providerName);
//                values.put(KEY_PROVIDER_LOGO, videoInfo.providerLogo);
                values.put(KEY_WALL_BRAND_ID, videoInfo.wall_brand_id);
                values.put(KEY_PROVIDER_ID, videoInfo.providerId);
//                values.put(KEY_SHOW_PROVIDER, videoInfo.isShowProvider);
                values.put(KEY_LIKE, videoInfo.likeCount);
                values.put(KEY_FB_SHARE, videoInfo.fbShareCount);
                values.put(KEY_WHATS_SHARE, videoInfo.whatsappShareCount);
                values.put(KEY_ULTIMA_TRACK_URL, videoInfo.ultimaTrackUrl);
                values.put(KEY_CATEGORY_ID, categoryId);
                values.put(KEY_CREATED_AT, getDateTime());

                // insert row
                returnValue = db.insert(TABLE_NAME, null, values);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }

    /**
     * getting all NewsList
     */
    public VideoInfo getRelatedVideo(String storyId, SQLiteDatabase db) {
        VideoInfo videoInfo = new VideoInfo();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE " + KEY_STORY_ID + "='" + storyId + "'";
        synchronized (lock) {
            Cursor cursor = null;
            try {
                cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        videoInfo.title = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.image = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.mediaTitle = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.storyId = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
//                        videoInfo.channelSlno = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
//                        videoInfo.pubDate = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.videoFlag = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
//                        videoInfo.catId = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
//                        videoInfo.timeAgo = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.link = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
//                        videoInfo.videoUrl = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.views = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.duration = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.internalVideo = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
//                        videoInfo.adaptiveVideo = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.story = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.version = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
//                        videoInfo.gTrackUrl = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.bigImage = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.provider_code = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.providerName = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
//                        videoInfo.providerLogo = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.wall_brand_id = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.providerId = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
//                        videoInfo.isShowProvider = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.likeCount = cursor.getInt(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.fbShareCount = cursor.getInt(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.whatsappShareCount = cursor.getInt(cursor.getColumnIndex(KEY_TITLE));
                        videoInfo.ultimaTrackUrl = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        }

        return videoInfo;
    }


    public int deleteAllData() {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                rowsAffected = db.delete(TABLE_NAME, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }

    public void deleteRelatedVideo(String categoryId) {

        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                // Delete row
                String addrWhere = KEY_CATEGORY_ID + " = ?"; //this is where clause and below is corresponding selection args..
                String[] addrWhereParams = new String[]{String.valueOf(categoryId)};
                db.delete(TABLE_NAME, addrWhere, addrWhereParams);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }

    }

    /**
     * get datetime
     */
    private String getDateTime() {
        String currenttime = Long.toString(System.currentTimeMillis());
        return currenttime;
    }
}