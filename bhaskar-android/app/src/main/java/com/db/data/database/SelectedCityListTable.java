package com.db.data.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import com.db.InitApplication;
import com.db.data.models.CityInfo;
import com.db.util.AppPreferences;
import com.db.util.QuickPreferences;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * Created by DB on 8/17/2017.
 */

public class SelectedCityListTable {


    // Table Names
    public static final String TABLE_NAME = "selectedCityListTable";
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    // selectedCityList Table column names
    private static final String KEY_CREATED_AT = "createdAt";

    // selectedCityList Table - column names

    private static final String KEY_APP_ID = "appId";
    private static final String KEY_ID = "id";
    private static final String KEY_STATE_NAME = "state_name";
    private static final String KEY_CITY_NAME = "city_name";
    private static final String KEY_CITY_HINDI_NAME = "city_hindi_name";
    private static final String KEY_CITY_ID = "city_id";
    private static final String KEY_IS_PREFERRED_CITY = "isPreferredCity";

    Object lock = new Object();

    private DatabaseHelper appDb;

    public SelectedCityListTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + KEY_APP_ID + " TEXT,"
                + KEY_ID + " TEXT," + KEY_STATE_NAME + " TEXT,"
                + KEY_CITY_NAME + " TEXT," + KEY_CITY_HINDI_NAME + " TEXT,"
                + KEY_CITY_ID + " TEXT," + KEY_IS_PREFERRED_CITY + " INTEGER,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            switch (oldVersion) {
                case 1:
                case 2:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
                    createTable(db);
                    saveSelectedCityListoldAppData(db);
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String addselectedCityListItem(String appId, Vector<CityInfo> cityInfoList) {
//        deleteAllData();
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();

                for (int i = 0; i < cityInfoList.size(); i++) {
                    values.clear();

                    CityInfo cityInfo = cityInfoList.get(i);
                    values.put(KEY_APP_ID, appId);
                    values.put(KEY_ID, cityInfo.id);
                    values.put(KEY_STATE_NAME, cityInfo.stateName);
                    values.put(KEY_CITY_NAME, cityInfo.cityName);
                    values.put(KEY_CITY_HINDI_NAME, cityInfo.cityHindiName);
                    values.put(KEY_CITY_ID, cityInfo.cityId);

                    values.put(KEY_CREATED_AT, getDateTime());

                    // insert row
                    returnValue = db.insert(TABLE_NAME, null, values);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }

    public String addselectedCityListItem(SQLiteDatabase db, String appId, Vector<CityInfo> cityInfoList) {
//        deleteAllData();
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
//                db = appDb.getWritableDatabase();
                ContentValues values = new ContentValues();

                for (int i = 0; i < cityInfoList.size(); i++) {
                    values.clear();

                    CityInfo cityInfo = cityInfoList.get(i);
                    values.put(KEY_APP_ID, appId);
                    values.put(KEY_ID, cityInfo.id);
                    values.put(KEY_STATE_NAME, cityInfo.stateName);
                    values.put(KEY_CITY_NAME, cityInfo.cityName);
                    values.put(KEY_CITY_HINDI_NAME, cityInfo.cityHindiName);
                    values.put(KEY_CITY_ID, cityInfo.cityId);

                    values.put(KEY_CREATED_AT, getDateTime());

                    Log.e("DBDB_data", values.toString());
                    // insert row
                    returnValue = db.insert(TABLE_NAME, null, values);
                    Log.e("DBDB_returnValue", returnValue + "");
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
//                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }


    /**
     * //	 * getting selectedCity List According to AppId
     * //	 *
     */

    @SuppressLint("UseSparseArrays")
    public Vector<CityInfo> getAllselectedCityListAccordingToAppId(String appId) {
        Vector<CityInfo> cityInfoArrayList = new Vector<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase();
                cursor = db.query(TABLE_NAME, new String[]{KEY_APP_ID, KEY_ID,
                                KEY_STATE_NAME, KEY_CITY_NAME, KEY_CITY_HINDI_NAME, KEY_CITY_ID, KEY_IS_PREFERRED_CITY, KEY_CREATED_AT}, "appId = ?",
                        new String[]{String.valueOf(appId)},
                        null,
                        null,
                        null);

                if (cursor.moveToFirst()) {
                    do {
                        CityInfo cityInfo = new CityInfo();
                        cityInfo.id = cursor.getString(cursor.getColumnIndex(KEY_ID));
                        cityInfo.stateName = cursor.getString(cursor.getColumnIndex(KEY_STATE_NAME));
                        cityInfo.cityName = cursor.getString(cursor.getColumnIndex(KEY_CITY_NAME));
                        cityInfo.cityHindiName = cursor.getString(cursor.getColumnIndex(KEY_CITY_HINDI_NAME));
                        cityInfo.cityId = cursor.getString(cursor.getColumnIndex(KEY_CITY_ID));

                        // adding to list
                        cityInfoArrayList.add(cityInfo);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return cityInfoArrayList;
    }



    public int deleteAllData() {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                rowsAffected = db.delete(TABLE_NAME, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }


    public void deleteParticularSectionInfoArrayList(String appId) {

        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase();
                // Delete row
                String addrWhere = KEY_APP_ID + " = ?"; //this is where clause and below is corresponding selection args..
                String[] addrWhereParams = new String[]{String.valueOf(appId)};
                db.delete(TABLE_NAME, addrWhere, addrWhereParams);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }

    }


    public void saveSelectedCityListoldAppData(SQLiteDatabase db) {
        try {
            String jsonData = AppPreferences.getInstance(InitApplication.getInstance()).getStringValue(QuickPreferences.SerializePreference.PREFERED_SELECTED_CITY_JSON, "");
            SelectedCityListTable selectedCityListTable = (SelectedCityListTable) DatabaseHelper.getInstance(InitApplication.getInstance()).getTableObject(SelectedCityListTable.TABLE_NAME);
            if (!TextUtils.isEmpty(jsonData)) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonData);
                    if (jsonObject != null) {
                        Iterator<?> keys = jsonObject.keys();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            JSONArray jsonArray = jsonObject.getJSONArray(key);
                            Gson gson = new Gson();
                            Type type = new TypeToken<List<CityInfo>>() {
                            }.getType();
                            List<CityInfo> infos = gson.fromJson(jsonArray.toString(), type);
                            selectedCityListTable.addselectedCityListItem(db, key, new Vector<CityInfo>(infos));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get datetime
     */
    private String getDateTime() {
        String currenttime = Long.toString(System.currentTimeMillis());
        return currenttime;
    }
}

