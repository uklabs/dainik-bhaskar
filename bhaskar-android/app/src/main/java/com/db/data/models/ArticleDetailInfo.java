package com.db.data.models;

import java.io.Serializable;

public class ArticleDetailInfo implements Serializable {
    public String key;
    public String value;

    public ArticleDetailInfo(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public ArticleDetailInfo(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleDetailInfo info = (ArticleDetailInfo) o;

        return key != null ? key.equals(info.key) : info.key == null;

    }

    @Override
    public int hashCode() {
        return key != null ? key.hashCode() : 0;
    }
}
