package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AuthorInfo implements Serializable {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("title")
    public String title;

    @SerializedName("image")
    public String image;
}
