package com.db.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BannerInfo {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("added_on_menu")
    @Expose
    public String addedOnMenu;

    @SerializedName("enable_img_banner")
    @Expose
    public String enableImgBanner;

    @SerializedName("banner_url")
    @Expose
    public String bannerUrl;

    @SerializedName("cat_pos")
    @Expose
    public String catPos;

    @SerializedName("cat_home_pos")
    @Expose
    public String catHomePos;

    @SerializedName("action_name")
    @Expose
    public String actionName;

    @SerializedName("action_menu_id")
    @Expose
    public String actionMenuId;

    @SerializedName("action_url")
    @Expose
    public String actionUrl;

    @SerializedName("ga_event")
    @Expose
    public String gaEvent;

    @SerializedName("ga_screen")
    @Expose
    public String gaScreen;

    public boolean isAdded = false;

    public BannerInfo(String catId) {
        this.addedOnMenu = catId;
        this.isAdded = false;
    }

    public BannerInfo(BannerInfo mBannerInfo) {
        this.id = mBannerInfo.id;
        this.addedOnMenu = mBannerInfo.addedOnMenu;
        this.enableImgBanner = mBannerInfo.enableImgBanner;
        this.bannerUrl = mBannerInfo.bannerUrl;
        this.catPos = mBannerInfo.catPos;
        this.catHomePos = mBannerInfo.catHomePos;
        this.actionName = mBannerInfo.actionName;
        this.actionMenuId = mBannerInfo.actionMenuId;
        this.actionUrl = mBannerInfo.actionUrl;
        this.gaEvent = mBannerInfo.gaEvent;
        this.gaScreen = mBannerInfo.gaScreen;
        this.isAdded = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BannerInfo that = (BannerInfo) o;
        return (addedOnMenu != null ? addedOnMenu.equals(that.actionMenuId) : that.addedOnMenu == null);
    }
}
