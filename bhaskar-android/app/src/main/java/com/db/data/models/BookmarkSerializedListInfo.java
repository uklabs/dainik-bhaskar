package com.db.data.models;

import com.db.util.AppUtils;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BookmarkSerializedListInfo implements Serializable {

    public String storytype;
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("story_id")
    private String storyId;
    @SerializedName("slug_intro")
    private String slugIntro;
    @SerializedName("videoflag")
    private int videoFlag;
    @SerializedName("detail_url")
    private String detailUrl;
    @SerializedName("ga_screen")
    private String gaScreen;
    @SerializedName("ga_article")
    private String gaArticle;
    @SerializedName("ga_article")
    private String gaEventLabel;
    @SerializedName("action")
    private String action;
    private String brandAction;
    private String brandChannel;
    private String wisdomDomain;
    private String wisdomSubDomain;
    @SerializedName("categoryInfo")
    private CategoryInfo categoryInfo;
    private String categoryInfoId;
    private String parentID;
    private String content;
    private String displayName;

    @SerializedName("iitl_title")
    private String iitlTitle;
    private String gTrackUrl;
    private String pubDate;
    private String providerName;
    private String providerLogo;
    private String color;

    public BookmarkSerializedListInfo() {
    }

    public BookmarkSerializedListInfo(String storyId) {
        this.storyId = storyId;
    }

    public String getCategoryInfoId() {
        return categoryInfoId;
    }

    public void setCategoryInfoId(String categoryInfoId) {
        this.categoryInfoId = categoryInfoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDisplayName() {
        return title;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStoryId() {
        return storyId;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }

    public String getSlugIntro() {
        return slugIntro;
    }

    public void setSlugIntro(String slugIntro) {
        this.slugIntro = slugIntro;
    }

    public int getVideoFlag() {
        return videoFlag;
    }

    public void setVideoFlag(int videoFlag) {
        this.videoFlag = videoFlag;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public String getGaScreen() {
        return gaScreen;
    }

    public void setGaScreen(String gaScreen) {
        this.gaScreen = gaScreen;
    }

    public String getGaArticle() {
        return gaArticle;
    }

    public void setGaArticle(String gaArticle) {
        this.gaArticle = gaArticle;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getBrandAction() {
        return brandAction;
    }

    public void setBrandAction(String brandAction) {
        this.brandAction = brandAction;
    }

    public String getBrandChannel() {
        return brandChannel;
    }

    public void setBrandChannel(String brandChannel) {
        this.brandChannel = brandChannel;
    }

    public CategoryInfo getCategoryInfo() {
        return categoryInfo;
    }

    public void setCategoryInfo(CategoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
    }

    public String getWisdomDomain() {
        return wisdomDomain;
    }

    public void setWisdomDomain(String wisdomDomain) {
        this.wisdomDomain = wisdomDomain;
    }

    public String getWisdomSubDomain() {
        return wisdomSubDomain;
    }

    public void setWisdomSubDomain(String wisdomSubDomain) {
        this.wisdomSubDomain = wisdomSubDomain;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStorytype() {
        return storytype;
    }

    public void setStorytype(String storytype) {
        this.storytype = storytype;
    }

    public String getGaEventLabel() {
        return gaEventLabel;
    }

    public void setGaEventLabel(String gaEventLabel) {
        this.gaEventLabel = gaEventLabel;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public String getIitlTitle() {
        return iitlTitle;
    }

    public void setIitlTitle(String iitlTitle) {
        this.iitlTitle = iitlTitle;
    }

    public String getgTrackUrl() {
        return gTrackUrl;
    }

    public void setgTrackUrl(String gTrackUrl) {
        this.gTrackUrl = gTrackUrl;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getProviderLogo() {
        return providerLogo;
    }

    public void setProviderLogo(String providerLogo) {
        this.providerLogo = providerLogo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        BookmarkSerializedListInfo that = (BookmarkSerializedListInfo) o;
        String storyIdThis = AppUtils.getFilteredStoryId(storyId);
        String storyIdThat = AppUtils.getFilteredStoryId(that.storyId);

        return storyIdThis != null ? storyIdThis.equals(storyIdThat) : storyIdThat == null;
    }

    @Override
    public int hashCode() {
        return storyId != null ? storyId.hashCode() : 0;
    }

}
