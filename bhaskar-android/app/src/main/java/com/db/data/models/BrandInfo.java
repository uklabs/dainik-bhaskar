package com.db.data.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class BrandInfo implements Serializable {

    @Ignore
    public BrandInfo(String id) {
        this.id = id;
    }

    public BrandInfo() {
    }

    @Ignore
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrandInfo that = (BrandInfo) o;
        return (id != null ? id.equals(that.id) : that.id == null);
    }

    @SerializedName("id")
    @NonNull
    @PrimaryKey
    public String id;

    @SerializedName("name")
    @ColumnInfo(name = "name")
    public String name;

    @SerializedName("icon")
    @ColumnInfo(name = "icon")
    public String icon;

    @SerializedName("action")
    @ColumnInfo(name = "action")
    public String action;

    @SerializedName("new_tag")
    @ColumnInfo(name = "new_tag")
    public String isNew = "";

    @SerializedName("gdpr_block")
    @ColumnInfo(name = "gdpr_block")
    public String gdprBlock = "0";
}
