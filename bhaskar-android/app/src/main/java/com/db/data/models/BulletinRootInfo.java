package com.db.data.models;

import com.db.dbvideoPersonalized.data.VideoInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BulletinRootInfo implements Serializable {

    @SerializedName("video")
    @Expose
    public Video video;
    @SerializedName("news")
    @Expose
    public News news;

    public class News {

        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("feed")
        @Expose
        public List<NewsListInfo> feed = null;

    }

    public class Video {

        @SerializedName("is_portrait")
        @Expose
        public Integer isPortrait;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("feed")
        @Expose
        public List<VideoInfo> feed = null;

    }
}