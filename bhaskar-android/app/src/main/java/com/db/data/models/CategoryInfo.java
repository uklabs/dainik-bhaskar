package com.db.data.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class CategoryInfo implements Serializable {

    public CategoryInfo() {
    }

    @Ignore
    public CategoryInfo(String id) {
        this.id = id;
    }

    @Ignore
    public CategoryInfo(CategoryInfo another) {
        if (another != null) {
            this.id = another.id;
            this.menuName = another.menuName;
            this.displayName = another.displayName;
            this.color = another.color;
            this.parentId = another.parentId;
            this.feedUrl = another.feedUrl;
            this.detailUrl = another.detailUrl;
            this.action = another.action;
            this.gaScreen = another.gaScreen;
            this.gdprBlock = another.gdprBlock;
            this.extraVlues = another.extraVlues;
            this.gaEventLabel = another.gaEventLabel;
            this.gaArticle = another.gaArticle;
            this.subMenu = another.subMenu;
        }
    }

    @Ignore
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryInfo that = (CategoryInfo) o;
        return (id != null ? id.equals(that.id) : that.id == null);
    }

    @SerializedName("id")
    @NonNull
    @PrimaryKey
    public String id;

    @SerializedName("name")
    @ColumnInfo(name = "menu_name")
    public String menuName;

    @SerializedName("dis_name")
    @ColumnInfo(name = "display_name")
    public String displayName;

    @SerializedName("color")
    @ColumnInfo(name = "color")
    public String color;

    @SerializedName("p_id")
    @ColumnInfo(name = "parent_id")
    public String parentId = "0";

    @SerializedName("url_feed")
    @ColumnInfo(name = "feed_url")
    public String feedUrl;

    @SerializedName("url_detail")
    @ColumnInfo(name = "detail_url")
    public String detailUrl;

    @SerializedName("action")
    @ColumnInfo(name = "action")
    public String action;

    @SerializedName("ga_screen")
    @ColumnInfo(name = "ga_screen")
    public String gaScreen = "";

    @SerializedName("gdpr_block")
    @ColumnInfo(name = "gdpr_block")
    public int gdprBlock = 0;

    @SerializedName("extras")
    @ColumnInfo(name = "extra_values")
    public String extraVlues;

    @ColumnInfo(name = "ga_event_label")
    public String gaEventLabel;

    @ColumnInfo(name = "ga_article")
    public String gaArticle;

    @SerializedName("sub_menu")
    @Ignore
    public List<CategoryInfo> subMenu = new ArrayList<>();

    @SerializedName("icon_ham")
    @ColumnInfo(name = "icon")
    public String iconHamberger;

    @SerializedName("new_tag")
    @ColumnInfo(name = "cat_new")
    public String isNew = "";
}