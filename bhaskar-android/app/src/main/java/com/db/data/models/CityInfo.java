package com.db.data.models;


import com.db.util.Constants;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CityInfo implements Serializable {
    @SerializedName("link")
    public String link;
    @SerializedName("id")
    public String id;

    @SerializedName("state_name")
    public String stateName;

    @SerializedName("city_name")
    public String cityName;

    @SerializedName("city_hindi_name")
    public String cityHindiName;

    @SerializedName("city_id")
    public String cityId;

    @SerializedName("e")
    public int isDefaultCityFromServer;

    @SerializedName("f")
    public String citySelectionValue;

    public CityInfo() {
    }

    public CityInfo(String value, short stateFlag) {
        if (stateFlag == Constants.CityPreference.CITY_INFO_BASED_ON_ID) {
            cityId = value;
        } else if (stateFlag == Constants.CityPreference.CITY_INFO_BASED_ON_STATE_NAME) {
            stateName = value;
        } else if (stateFlag == Constants.CityPreference.CITY_INFO_BASED_ON_CITY_NAME) {
            cityName = value;
        }
        isDefaultCityFromServer = stateFlag;
    }

    @Override
    public boolean equals(Object obj) {
        try {
            switch (isDefaultCityFromServer) {
                case Constants.CityPreference.CITY_INFO_BASED_ON_ID:
                    if (obj instanceof String) {
                        return cityId.equalsIgnoreCase(obj.toString());
                    } else if (obj instanceof CityInfo) {
                        return cityId.equalsIgnoreCase(((CityInfo) obj).cityId);
                    }
                case Constants.CityPreference.CITY_INFO_BASED_ON_CITY_NAME:
                    if (obj instanceof String) {
                        return cityName.equalsIgnoreCase(obj.toString());
                    } else if (obj instanceof CityInfo) {
                        return cityName.equalsIgnoreCase(((CityInfo) obj).cityName);
                    }
                case Constants.CityPreference.CITY_INFO_BASED_ON_STATE_NAME:
                    if (obj instanceof String) {
                        return stateName.equalsIgnoreCase(obj.toString());
                    } else if (obj instanceof CityInfo) {
                        return stateName.equalsIgnoreCase(((CityInfo) obj).stateName);
                    }
                default:
                    return false;
            }
        } catch (Exception e) {
        }

        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
