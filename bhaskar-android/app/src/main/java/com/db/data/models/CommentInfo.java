package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommentInfo implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("story_id")
    private String stroryId;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("city")
    private String city;
    @SerializedName("comment")
    private String comment;
    @SerializedName("created")
    private String created;
    @SerializedName("image")
    private String image;

    public String getStroryId() {
        return stroryId;
    }

    public String getCity() {
        return city;
    }

    public String getComment() {
        return comment;
    }

    public String getCreated() {
        return created;
    }

    public String getEmail() {
        return email;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}