package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CouponRaniVoucherInfo implements Serializable {

    @SerializedName("Name")
    public String name;

    @SerializedName("Destination URL")
    public String destinationURL;

    @SerializedName("Code")
    public String code;

    @SerializedName("Value")
    public String value;

    @SerializedName("Description")
    public String description;

    @SerializedName("Expiry Date")
    public String expiryDate;

    @SerializedName("Store Name")
    public String storeName;

    @SerializedName("Image URL")
    public String imageURL;

    @SerializedName("Store Description")
    public String storeDescription;

    @SerializedName("Category")
    public String category;
}
