package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class DBColumnInfo implements Serializable {

    @SerializedName("header")
    public String header;

    @SerializedName("date")
    public String date;

    @SerializedName("dis_date")
    public String displayDate;

    @SerializedName("data")
    public ArrayList<StoryListInfo> feed = new ArrayList<>();
}