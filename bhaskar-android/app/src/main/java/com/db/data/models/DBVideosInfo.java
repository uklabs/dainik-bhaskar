package com.db.data.models;

import android.text.TextUtils;

import com.db.ads.AdInfo;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DBVideosInfo extends AdInfo implements Serializable {

    @SerializedName("title")
    public String title;

    @SerializedName("image")
    public String image;

    @SerializedName("duration")
    public String duration;

    @SerializedName("story_id")
    public String storyId;

    @SerializedName("videoflag")
    public String videoFlag;

    @SerializedName("link")
    public String link;

    @SerializedName("views")
    public String views;

    @SerializedName("internal_video")
    public String internalVideo;

    @SerializedName("dm_id")
    public String dailyMotionVideoId;

    @SerializedName("story")
    public String story;

    @SerializedName("version")
    public String version;

    @SerializedName("g_track_url")
    public String gTrackUrl;

    @SerializedName("ultima_track_url")
    public String ultimaTrackUrl;

    @SerializedName("media_title")
    public String mediaTitle;

    @SerializedName("big_image")
    public String bigImage;

    @SerializedName("islive")
    public int isLive = 0;

    @SerializedName("provider_code")
    public String provider_code;

    @SerializedName("provider_id")
    public String providerId;

    @SerializedName("wall_brand_id")
    public String wall_brand_id;

    public int type;

    public DBVideosInfo() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof String) {
            if (!TextUtils.isEmpty(storyId))
                return this.storyId.contains(((String) obj));
        } else if (obj instanceof DBVideosInfo) {
            if (!TextUtils.isEmpty(storyId))
                return this.storyId.equals(((DBVideosInfo) obj).storyId);
            else
                super.equals(obj);
        } else if (obj instanceof VideoInfo) {
            if (!TextUtils.isEmpty(storyId))
                return this.storyId.equals(((VideoInfo) obj).storyId);
            else
                super.equals(obj);
        }
        return super.equals(obj);
    }
}
