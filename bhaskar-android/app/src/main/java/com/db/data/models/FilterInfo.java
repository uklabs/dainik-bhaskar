package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FilterInfo implements Serializable {

    @SerializedName("id")
    public String id;

    @SerializedName("title")
    public String title;

    @Override
    public String toString() {
        return title;
    }
}

