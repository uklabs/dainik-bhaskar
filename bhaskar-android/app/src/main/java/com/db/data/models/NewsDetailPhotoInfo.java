package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class NewsDetailPhotoInfo implements Serializable {

    @SerializedName("photos")
    public ArrayList<NewsPhotoInfo> photos = new ArrayList<>();
}
