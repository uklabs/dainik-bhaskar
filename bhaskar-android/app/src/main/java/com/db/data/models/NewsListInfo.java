package com.db.data.models;

import com.db.ads.AdInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NewsListInfo extends AdInfo implements Serializable {

    @SerializedName("title")
    public String title;

    @SerializedName("title_nb")
    public String titleNB;

    @SerializedName("widget_title")
    public String widgetTitle;

    @SerializedName("iitl_title")
    public String iitl_title;

    @SerializedName("image")
    public String image;

    @SerializedName("imagebigsize")
    public String imagebigsize;

    @SerializedName("image_size")
    public String imageSize;

    @SerializedName("story_id")
    public String storyId;

    @SerializedName("videoflag")
    public int videoFlag = 0;

    @SerializedName("color_code")
    public String colorCode;

    @SerializedName("header_name")
    public String headerName;

    @SerializedName("version")
    public String version;

    @SerializedName("big_image")
    public int bigImage = -1;

    @SerializedName("g_track_url")
    public String gTrackUrl;

    @SerializedName("exclusive")
    public String exclusive;

    @SerializedName("isopenwebview")
    public boolean isWebView = false;

    @SerializedName("open_gallery")
    public boolean isOpenGallery = false;

    @SerializedName("webopenurl")
    public String webUrl = "";

    @SerializedName("bullets")
    public String[] bulletsPoint;

    @SerializedName("bullets_nb")
    public String[] bulletsPointNB;

    @SerializedName("sub_story")
    public ArrayList<NewsListInfo> subStoryList = new ArrayList<>();

    @SerializedName("editor_name")
    public String editorName;

    @SerializedName("editor_image")
    public String editorImage;

    @SerializedName("ex_topic_name")
    public String exTopicName;

    @SerializedName("photos")
    @Expose
    public List<NewsPhotoInfo> photos = null;

    @SerializedName("photos_nb")
    @Expose
    public List<NewsPhotoInfo> photosNB = null;

    @SerializedName("provider")
    @Expose
    public String provider;

    @SerializedName("provider_logo")
    @Expose
    public String providerLogo;

    @SerializedName("pubdate")
    @Expose
    public String pubdate;

    /*Local Variables*/
    public BannerInfo mBannerInfo;
    public WebBannerInfo webBannerInfo;
    public int type;
    public int viewType;
    public boolean isFlicker;
    public boolean isPhotoFlicker;
    public boolean isStockWidget;


    public NewsListInfo() {
    }

    public NewsListInfo(ArrayList<NewsListInfo> galleryData) {
        this.subStoryList = galleryData;
    }

    public NewsListInfo(int type) {
        this.type = type;
    }

    public int listIndex = -1;
    public int scrollNo = -1;

    public NewsListInfo(int viewTypeBanner, BannerInfo bannerInfo) {
        this.type = viewTypeBanner;
        mBannerInfo = bannerInfo;
    }

    public NewsListInfo(int viewTypeBanner, WebBannerInfo webBannerInfo) {
        this.type = viewTypeBanner;
        this.webBannerInfo = webBannerInfo;
    }

    @Override
    public String toString() {
        return "NewsListInfo{" +
                "title='" + title + '\'' +
                ", widgetTitle='" + widgetTitle + '\'' +
                ", iitl_title='" + iitl_title + '\'' +
                ", image='" + image + '\'' +
                ", imagebigsize='" + imagebigsize + '\'' +
                ", imageSize='" + imageSize + '\'' +
                ", storyId='" + storyId + '\'' +
//                ", channelSlno='" + channelSlno + '\'' +
                ", videoFlag=" + videoFlag +
                ", colorCode='" + colorCode + '\'' +
                ", headerName='" + headerName + '\'' +
                ", version='" + version + '\'' +
                ", bigImage=" + bigImage +
                ", gTrackUrl='" + gTrackUrl + '\'' +
                ", exclusive='" + exclusive + '\'' +
                ", isWebView=" + isWebView +
                ", isOpenGallery=" + isOpenGallery +
                ", webUrl='" + webUrl + '\'' +
                ", bulletsPoint=" + Arrays.toString(bulletsPoint) +
                ", subStoryList=" + subStoryList +
                ", editorName='" + editorName + '\'' +
                ", editorImage='" + editorImage + '\'' +
                ", exTopicName='" + exTopicName + '\'' +
                ", type=" + type +
                ", viewType=" + viewType +
                ", isFlicker=" + isFlicker +
                ", isPhotoFlicker=" + isPhotoFlicker +
                ", isStockWidget=" + isStockWidget +
                ", listIndex=" + listIndex +
                ", scrollNo=" + scrollNo +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof NewsListInfo) {
            if (((NewsListInfo) obj).type != 0) {
                return this.type == ((NewsListInfo) obj).type;
            } else {
                return super.equals(obj);
            }
        }
        return super.equals(obj);
    }
}
