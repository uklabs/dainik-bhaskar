package com.db.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NewsPhotoInfo implements Serializable {

    @SerializedName("photo_id")
    public String photoId;

    @SerializedName("story")
    public String story;

    @SerializedName("image")
    public String image;

    @SerializedName("image_size")
    public String imageSize;

    @SerializedName("title")
    public String title = "";

    @SerializedName("videoflag")
    public int videoFlag = 0;

    @SerializedName("videourl")
    public String videoUrl;

    @SerializedName("dm_id")
    public String dailyMotionVideoId;

    @SerializedName("internal_video")
    public String internalVideo;

    @SerializedName("media_title")
    public String mediaTitle;


    @SerializedName("ultima_track_url")
    public String ultimaTrackUrl;

    @SerializedName("provider_code")
    public String provider_code;

    @SerializedName("provider_id")
    public String provider_id;


    @SerializedName("is_share")
    public boolean isShare;

    @SerializedName("link")
    @Expose
    public String link;
    @SerializedName("iframe_data")
    @Expose
    public String iframeData;

    @SerializedName("thumb_img")
    @Expose
    public String thumb_img;

    public String articleTitle = "";

}
