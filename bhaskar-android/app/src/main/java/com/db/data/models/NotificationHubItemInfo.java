package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationHubItemInfo implements Serializable {

    @SerializedName("track_url")
    public String trackUrl;
    @SerializedName("title")
    public String title;
    @SerializedName("image")
    public String image;
    @SerializedName("menu_id")
    public String menuId;
    @SerializedName("story_id")
    public String storyId;
    @SerializedName("cat_id")
    public String catId;
    @SerializedName("channel_slno")
    public String channelSlno;
    @SerializedName("slug_intro")
    public String slugIntro;
    @SerializedName("link")
    public String link;
    @SerializedName("pubdate")
    public String pubDate;
    @SerializedName("videoflag")
    public int videoFlag;
    @SerializedName("g_track_url")
    public String gTrackUrl;
    @SerializedName("version")
    public String version;
    public String createdAt;
    public int readStatus = 0;

    public String vendorType;

    public NotificationHubItemInfo() {
    }

    public NotificationHubItemInfo(String storyId) {
        this.storyId = storyId;
    }

    public int getNotificationOpenStatus() {
        return notificationOpenStatus;
    }

    public void setNotificationOpenStatus(int notificationOpenStatus) {
        this.notificationOpenStatus = notificationOpenStatus;
    }

    public int notificationOpenStatus = 0;

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String notificationId;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    public String getVersion() {
        return version;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public String getMenuId() {
        return menuId;
    }

    public String getStoryId() {
        return storyId;
    }

    public String getCatId() {
        return catId;
    }

    public String getChannelSlno() {
        return channelSlno;
    }

    public String getSlugIntro() {
        return slugIntro;
    }

    public String getLink() {
        return link;
    }

    public String getPubDate() {
        return pubDate;
    }

    public int getVideoFlag() {
        return videoFlag;
    }

    public String getTrackUrl() {
        return trackUrl;
    }

    public String getgTrackUrl() {
        return gTrackUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotificationHubItemInfo that = (NotificationHubItemInfo) o;

        return storyId.equals(that.storyId);
    }

    @Override
    public int hashCode() {
        return storyId.hashCode();
    }
}
