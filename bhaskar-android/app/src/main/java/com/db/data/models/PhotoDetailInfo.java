package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PhotoDetailInfo implements Serializable {

    @SerializedName("rss_id")
    public String rssId;

    @SerializedName("rss_title")
    public String rssTitle;

    @SerializedName("rss_desc")
    public String rssDesc;

    @SerializedName("rss_image")
    public String rssImage;

    @SerializedName("track_url")
    public String trackUrl;

    @SerializedName("g_track_url")
    public String gTrackUrl;

    @SerializedName("is_share")
    public boolean isShare;

    @Override
    public String toString() {
        return "PhotoDetailInfo{" +
                "rssId='" + rssId + '\'' +
                ", rssTitle='" + rssTitle + '\'' +
                ", rssDesc='" + rssDesc + '\'' +
                ", rssImage='" + rssImage + '\'' +
                ", trackUrl='" + trackUrl + '\'' +
                ", gTrackUrl='" + gTrackUrl + '\'' +
                '}';
    }
}
