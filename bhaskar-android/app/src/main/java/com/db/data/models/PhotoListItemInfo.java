package com.db.data.models;

import com.db.ads.AdInfo;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class PhotoListItemInfo extends AdInfo implements Serializable {

    @SerializedName("rss_id")
    public String rssId;

    @SerializedName("rss_title")
    public String rssTitle;

    @SerializedName("rss_desc")
    public String rssDesc;

    @SerializedName("rss_image")
    public String rssImage;

    @SerializedName("image_size")
    public String imageSize;

    @SerializedName("rss_date")
    public String rssDate;

    @SerializedName("link")
    public String link;

    @SerializedName("track_url")
    public String trackUrl;

    @SerializedName("g_track_url")
    public String gTrackUrl;

    @SerializedName("photo_count")
    public String photoCount;

    @SerializedName("iitl_title")
    public String iitlTitle;

    @SerializedName("color_code")
    public String colorCode;

    public String shareLink;

    @SerializedName("photos")
    public ArrayList<PhotoDetailInfo> photos;

    public PhotoListItemInfo() {
    }

    public int type;
    public BannerInfo mBannerInfo;

    public PhotoListItemInfo(int type) {
        this.type = type;
    }

    public PhotoListItemInfo(int type, BannerInfo bannerInfo) {
        this.type = type;
        this.mBannerInfo = bannerInfo;
    }
}
