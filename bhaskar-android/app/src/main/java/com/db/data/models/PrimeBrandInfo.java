package com.db.data.models;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class PrimeBrandInfo implements Serializable {

    @Ignore
    public Object object;

    public PrimeBrandInfo() {
    }

    public PrimeBrandInfo(PrimeBrandInfo info, Object object) {
        this.id = info.id;
        this.action = info.action;
        this.feedUrl = info.feedUrl;
        this.detailUrl = info.detailUrl;
        this.gaEventLabel = info.gaEventLabel;
        this.gaScreen = info.gaScreen;
        this.gaArticle = info.gaArticle;

        this.object = object;
    }

    @Ignore
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrimeBrandInfo that = (PrimeBrandInfo) o;
        return (id != null ? id.equals(that.id) : that.id == null);
    }

    @SerializedName("id")
    @NonNull
    @PrimaryKey
    public String id;

    @SerializedName("action")
    @ColumnInfo(name = "action")
    public String action;

    @SerializedName("feed_url")
    @ColumnInfo(name = "feed_url")
    public String feedUrl;

    @SerializedName("detail_url")
    @ColumnInfo(name = "detail_url")
    public String detailUrl;

    @SerializedName("ga_screen")
    @ColumnInfo(name = "ga_screen")
    public String gaScreen = "";

    @ColumnInfo(name = "ga_event_label")
    public String gaEventLabel;

    @ColumnInfo(name = "ga_article")
    public String gaArticle;
}
