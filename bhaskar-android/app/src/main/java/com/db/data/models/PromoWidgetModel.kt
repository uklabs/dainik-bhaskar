package com.db.data.models

import com.google.gson.annotations.SerializedName

data class PromoWidgetModel(@SerializedName("isactive")
                            val isActive: String = "0",
                            @SerializedName("url_icon")
                            val iconUrl: String?,
                            @SerializedName("menu_id")
                            val menuId: String?,
                            @SerializedName("url_link")
                            val linkUrl: String?)