package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class QuestionInfo implements Serializable {

    @SerializedName("question_id")
    public String questionId;

    @SerializedName("question")
    public String question;

    @SerializedName("question_type")
    public String questionType;

    @SerializedName("option")
    public ArrayList<String> optionsList = new ArrayList<>();
}
