package com.db.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuizQuestion {

    @SerializedName("feed")
    @Expose
    public String feed;
    @SerializedName("quiz_data")
    @Expose
    public QuizData quizData;
    @SerializedName("que_data")
    @Expose
    public QueData queData;


    public class QueData {

        @SerializedName("q_id")
        @Expose
        public String qId;
        @SerializedName("que_id")
        @Expose
        public String queId;
        @SerializedName("que")
        @Expose
        public String que;
        @SerializedName("que_title")
        @Expose
        public String questionTitle;
        @SerializedName("options")
        @Expose
        public List<String> options = null;


    }

    public class QuizData {

        @SerializedName("q_id")
        @Expose
        public String qId;
        @SerializedName("quiz_name")
        @Expose
        public String quizName;
        @SerializedName("quiz_name_eng")
        @Expose
        public String quizNameEng;
        @SerializedName("about_url")
        @Expose
        public String aboutUrl;
        @SerializedName("correct_ans")
        @Expose
        public String correctAns;
        @SerializedName("wrong_ans")
        @Expose
        public String wrongAns;
        @SerializedName("already_ans")
        @Expose
        public String alreadyAns;
        @SerializedName("correct_ans_img")
        @Expose
        public String correctAnsImg;
        @SerializedName("wrong_ans_img")
        @Expose
        public String wrongAnsImg;
        @SerializedName("already_ans_img")
        @Expose
        public String alreadyAnsImg;

        @SerializedName("logo_img")
        @Expose
        public String logoImgUrl;

        @SerializedName("iframe_url")
        @Expose
        public String iFrameUrl;

        @SerializedName("congrats_img")
        @Expose
        public String congratsImgUrl;
        @SerializedName("empty_ques")
        @Expose
        public String emptyQuestion;


    }


}

