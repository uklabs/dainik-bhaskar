package com.db.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Radio implements Serializable {
    @SerializedName("stn_name")
    @Expose
    public String stn_name;
    @SerializedName("stn_url")
    @Expose
    public String stn_url;
    @SerializedName("img")
    @Expose
    public String img;
    @SerializedName("color")
    @Expose
    public String color;
    @SerializedName("id")
    @Expose
    public String id;
}
