package com.db.data.models;


import java.io.Serializable;

public class RajyaInfo implements Serializable {

    public String id;
    public String title;
    public String feedUrl;
    public String detailURL;
    public String action = "News";
    public String gaScreen;
    public String gaArticle;
    public String gaEventLabel;
    public String displayName;

    public RajyaInfo(String id, String title, String feedUrl,String detailURL,String gaScreen,String gaArticle,String gaEventLabel,String displayName) {
        this.id = id;
        this.title = title;
        this.feedUrl = feedUrl;
        this.detailURL = detailURL;
        this.gaArticle = gaArticle;
        this.gaScreen = gaScreen;
        this.gaEventLabel = gaEventLabel;
        this.displayName = displayName;
    }

}
