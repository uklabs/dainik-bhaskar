package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RashifalInfo implements Serializable {

    @SerializedName("rashi_name")
    public String rashiName;
    @SerializedName("rashi_title")
    public String rashiTitle;
    @SerializedName("rashi_subtitle")
    public String rashiSubtitle;
    @SerializedName("rashi_icon")
    public String rashIcon;
    @SerializedName("isSelected")
    public boolean isSelected;
    @SerializedName("content")
    public String content = "";

    public String getRashiName() {
        return rashiName;
    }

    public void setRashiName(String rashiName) {
        this.rashiName = rashiName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
