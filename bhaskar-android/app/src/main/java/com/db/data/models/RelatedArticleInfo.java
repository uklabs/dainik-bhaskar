package com.db.data.models;

import com.db.dbvideoPersonalized.data.VideoInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class RelatedArticleInfo implements Serializable {

    @SerializedName("related_article_id")
    public String relatedArticleId;

    @SerializedName("title")
    public String title;

    @SerializedName("iitl_title")
    public String iitlTitle;

    @SerializedName("story_id")
    public String storyId;

    @SerializedName("channel_slno")
    public String channelSlno;

    @SerializedName("link")
    public String link;

    @SerializedName("image")
    public String image;

    @SerializedName("image_size")
    public String imageSize;

    @SerializedName("version")
    public String version;

    @SerializedName("slug_intro")
    public String slugIntro;

    @SerializedName("pubdate")
    public String pubDate;

    @SerializedName("videoflag")
    public String videoFlag;

    @SerializedName("cat_id")
    public String catId;

    @SerializedName("template_type")
    public String templateType;

    @SerializedName("big_image")
    public int bigImage;

    @SerializedName("infog")
    public String infog;

    @SerializedName("exclusive")
    public String exclusive;

    @SerializedName("bullets")
    public String[] bulletsPoint;

    @SerializedName("color_code")
    public String colorCode;

    @SerializedName("header_name")
    public String headerName;


    @SerializedName("Tracker")
    public String Tracker;

    @SerializedName("images")
    public ArrayList<String> images = new ArrayList<String>();

    @SerializedName("related_video")
    public VideoInfo relatedVideo = null;

    @SerializedName("g_track_url")
    public String gTrackUrl;

    @SerializedName("provider")
    public String provider;

    @SerializedName("provider_logo")
    public String providerLogo;

    @SerializedName("photos")
    @Expose
    public List<NewsPhotoInfo> photos = null;

    @SerializedName("webopenurl")
    @Expose
    public String webopenurl;

    public int listIndex = -1;
    public BannerInfo bannerInfo;

    public RelatedArticleInfo(int viewTypeBanner, BannerInfo bannerInfo) {
        this.type = viewTypeBanner;
        this.bannerInfo = bannerInfo;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }


    private boolean isPoll = true;

    public int type;

    public RelatedArticleInfo(int type) {
        this.type = type;
    }

    public RelatedArticleInfo() {
    }

    public RelatedArticleInfo(String story_id) {
        this.storyId = story_id;
    }


    public boolean isPoll() {
        return isPoll;
    }

    public void setIsPoll(boolean poll) {
        isPoll = poll;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RelatedArticleInfo info = (RelatedArticleInfo) o;

        return storyId != null ? storyId.equals(info.storyId) : info.storyId == null;

    }

    @Override
    public int hashCode() {
        return storyId != null ? storyId.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "RelatedArticleInfo{" +
                "relatedArticleId='" + relatedArticleId + '\'' +
                ", title='" + title + '\'' +
                ", iitl_title='" + iitlTitle + '\'' +
                ", storyId='" + storyId + '\'' +
                ", channelSlno='" + channelSlno + '\'' +
                ", link='" + link + '\'' +
                ", image='" + image + '\'' +
                ", version='" + version + '\'' +
                ", slugIntro='" + slugIntro + '\'' +
                ", pubDate='" + pubDate + '\'' +
                ", videoFlag='" + videoFlag + '\'' +
                ", catId='" + catId + '\'' +
                ", templateType='" + templateType + '\'' +
                ", bigImage=" + bigImage +
                '}';
    }
}
