package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lalgos1 on 10-02-2018.
 */

public class SplashBundleInfo implements Serializable {

    @SerializedName("uri")
    public String uri;
    @SerializedName("channelId")
    public String channelId;
    @SerializedName("storyId")
    public String storyId;
    @SerializedName("versionQuickNews")
    public String versionQuickNews;
    @SerializedName("detailUrl")
    public String detailUrl;
    @SerializedName("title")
    public String title;
    @SerializedName("gaScreen")
    public String gaScreen;
    @SerializedName("displayName")
    public String displayName;
    @SerializedName("gaArticle")
    public String gaArticle;
    @SerializedName("catId")
    public String catId;
    @SerializedName("pCatId")
    public String pCatId;
    @SerializedName("menuId")
    public String menuId;
    @SerializedName("webUrl")
    public String webUrl = "";
    @SerializedName("webTitle")
    public String webTitle;
    @SerializedName("isWapExt")
    public String isWapExt;
    @SerializedName("newsType")
    public String newsType;
    @SerializedName("domain")
    public String domain;
    @SerializedName("subDomain")
    public String subDomain;
    @SerializedName("inBackground")
    public int inBackground;
    @SerializedName("campaignId")
    public String campaignId;
    @SerializedName("deeplink")
    public String deeplink;
    @SerializedName("slId")
    public String slId;


}
