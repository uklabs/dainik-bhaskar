package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StockListInfo implements Serializable {

    @SerializedName("id")
    public String id;

    @SerializedName("trade_name")
    public String tradeName;

    @SerializedName("last_trade_price")
    public String lastTradePrice;

    @SerializedName("net_chg")
    public String netChange;

    @SerializedName("updated_date")
    public String updateDate;

    @SerializedName("arrow")
    public String arrow;

    @SerializedName("name")
    public String name;

}
