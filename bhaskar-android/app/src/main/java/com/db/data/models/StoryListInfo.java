package com.db.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class StoryListInfo implements Serializable {

    @SerializedName("name")
    public String name;

    @SerializedName("id")
    public String catId;

    @SerializedName("title")
    public String title;

    @SerializedName("sub_title")
    public String subTitle;

    @SerializedName("image")
    public String image;

    @SerializedName("header")
    public String header;

    @SerializedName("sub_title2")
    public String subTitle2;

    /*App Speed data*/
    @SerializedName("article_title")
    public String articleTitle;

    @SerializedName("iitl_title")
    public String iitl_title;

    @SerializedName("bullets")
    public String[] bulletsPoint;

    @SerializedName("photos")
    @Expose
    public List<NewsPhotoInfo> photos = null;

    @SerializedName("provider")
    @Expose
    public String provider;

    @SerializedName("provider_logo")
    @Expose
    public String providerLogo;

    @SerializedName("pubdate")
    @Expose
    public String pubdate;

    @SerializedName("color_code")
    @Expose
    public String colorCode;

    @SerializedName("g_track_url")
    public String gTrackUrl;

    public boolean isWriterInfo = false;
}
