package com.db.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WapRedirectionUrls {

    @SerializedName("redirect_false_urls")
    public List<String> redirectFalseUrls = new ArrayList<>();

    @SerializedName("redirect_false_contains_urls")
    public List<String> redirectFalseContainsUrls = new ArrayList<>();

    @SerializedName("redirect_true_urls")
    public List<String> redirectTrueUrls = new ArrayList<>();

    @SerializedName("redirect_true_contains_urls")
    public List<String> redirectTrueContainsUrls = new ArrayList<>();
}
