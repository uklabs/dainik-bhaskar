package com.db.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WebBannerInfo {

    @SerializedName("pos")
    @Expose
    public String position;

    @SerializedName("url")
    @Expose
    public String bannerUrl;

    public boolean isAdded = false;

    public WebBannerInfo(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }
}
