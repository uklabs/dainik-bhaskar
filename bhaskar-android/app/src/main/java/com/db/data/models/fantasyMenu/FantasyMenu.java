package com.db.data.models.fantasyMenu;

import com.bhaskar.util.GsonProguardMarker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FantasyMenu implements Serializable, GsonProguardMarker {
    @SerializedName("label_view_more")
    @Expose
    public String labelViewMore;
    @SerializedName("label_view_less")
    @Expose
    public String labelViewLess;
    @SerializedName("label_menu_page_title")
    @Expose
    public String labelMenuPageTitle;
    @SerializedName("header_menu")
    @Expose
    public List<MenuItem> headerMenu = null;
    @SerializedName("label_top_section")
    @Expose
    public String labelTopSection;
    @SerializedName("top_menu")
    @Expose
    public List<MenuItem> topMenu = new ArrayList<>();
    @SerializedName("middle_menu")
    @Expose
    public List<MenuItem> middleMenu = new ArrayList<>();
    @SerializedName("bottom_menu")
    @Expose
    public List<MenuItem> bottomMenu = new ArrayList<>();


}
