package com.db.data.models.fantasyMenu;

import com.bhaskar.util.GsonProguardMarker;
import com.db.home.fantasyMenu.FantasyFragment;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;

public class MenuItem implements Serializable, GsonProguardMarker {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("icon")
    @Expose
    public String icon;
    @SerializedName("action")
    @Expose
    public String action;
    @SerializedName("gdpr_block")
    @Expose
    public String gdprBlock = "0";

    @SerializedName("color")
    @Expose
    public String color;

    @SerializedName("item")
    @Expose
    public CopyOnWriteArrayList<MenuItem> item = new CopyOnWriteArrayList<>();

    @SerializedName("sub_menu")
    @Expose
    public CopyOnWriteArrayList<MenuItem> subMenu = new CopyOnWriteArrayList<>();

    public FantasyFragment.MENU_STATUS menuStatus = FantasyFragment.MENU_STATUS.COLLAPSE;
}
