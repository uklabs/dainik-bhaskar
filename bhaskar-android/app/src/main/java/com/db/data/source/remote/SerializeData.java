package com.db.data.source.remote;

import android.content.Context;
import android.util.Log;

import com.db.data.database.BookmarkSerializedListTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.SelectedCityListTable;
import com.db.data.models.BookmarkSerializedListInfo;
import com.db.data.models.CityInfo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class SerializeData implements Serializable {

    private static final String FILE_NAME = "DainikBhaskarDataFileV2";
    private static SerializeData serializeData;
    private static Context mContext;

    private List<BookmarkSerializedListInfo> bookmarkListInfoList;
    private Vector<CityInfo> selectedCityList;

    private SerializeData() {
        bookmarkListInfoList = new ArrayList<>();
        selectedCityList = new Vector<>();
    }

    // create instance
    public static SerializeData getInstance(Context context) {
        SerializeData.mContext = context;
        if (serializeData == null) {
            synchronized (SerializeData.class) {
                SerializeData fileInstance = getFileInstance(context);
                if (fileInstance == null) {
                    if (serializeData == null) {
                        serializeData = new SerializeData();

                        save(context);
                    }
                } else {
                    serializeData = fileInstance;
                }
            }
        }
        return serializeData;
    }

    private synchronized static void save(Context context) {
        try {
            if (context != null) {
                FileOutputStream fileOut = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(serializeData);
                out.close();
                fileOut.close();
            }

        } catch (FileNotFoundException e) {
            //e.printStackTrace();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private synchronized static SerializeData getFileInstance(Context context) {
        SerializeData serializeData = null;
        try {
            if (context != null) {
                FileInputStream fileIn = context.openFileInput(FILE_NAME);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                serializeData = (SerializeData) in.readObject();
                in.close();
                fileIn.close();
            }
        } catch (IOException i) {
        } catch (Exception c) {
        }
        return serializeData;
    }

    ////////////////////////////////////BookMark Method/////////////////////////////////////////////////////
    public void saveBookmark(BookmarkSerializedListInfo bookmarkListInfo) {
        try {
            BookmarkSerializedListTable bookmarkSerializedListTable = (BookmarkSerializedListTable) DatabaseHelper.getInstance(mContext).getTableObject(BookmarkSerializedListTable.TABLE_NAME);
            bookmarkListInfoList = getBookmarListFromDataBase();
            if (!bookmarkListInfoList.contains(bookmarkListInfo)) {
                bookmarkListInfoList.clear();
                bookmarkListInfoList.add(bookmarkListInfo);
                bookmarkSerializedListTable.addBookmarkSerializedItem(bookmarkListInfoList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<BookmarkSerializedListInfo> getBookmarListFromDataBase() {
        List<BookmarkSerializedListInfo> sectionInfos = new ArrayList<BookmarkSerializedListInfo>();
        try {
            BookmarkSerializedListTable bookmarkSerializedListTable = (BookmarkSerializedListTable) DatabaseHelper.getInstance(mContext).getTableObject(BookmarkSerializedListTable.TABLE_NAME);
            sectionInfos.addAll(bookmarkSerializedListTable.getAllBookmarkSerializedInfoList());
            return sectionInfos;
        } catch (Exception e) {
            e.printStackTrace();
            return sectionInfos;
        }
    }


    public void removeBookmark(String storyId) {
        try {
            BookmarkSerializedListTable bookmarkSerializedListTable = (BookmarkSerializedListTable) DatabaseHelper.getInstance(mContext).getTableObject(BookmarkSerializedListTable.TABLE_NAME);
            bookmarkSerializedListTable.deleteParticularBookmarkSerializedListItem(storyId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public List<BookmarkSerializedListInfo> getBookmarkList() {
        try {
            BookmarkSerializedListTable bookmarkSerializedListTable = (BookmarkSerializedListTable) DatabaseHelper.getInstance(mContext).getTableObject(BookmarkSerializedListTable.TABLE_NAME);
            bookmarkListInfoList = bookmarkSerializedListTable.getAllBookmarkSerializedInfoList();
            return bookmarkListInfoList;
        } catch (Exception e) {
            e.printStackTrace();
            return bookmarkListInfoList;
        }
    }


    public boolean isBookmarkAvailable(String storyId) {
        try {
            BookmarkSerializedListTable bookmarkSerializedListTable = (BookmarkSerializedListTable) DatabaseHelper.getInstance(mContext).getTableObject(BookmarkSerializedListTable.TABLE_NAME);
            boolean isAvailable = bookmarkSerializedListTable.isParticularBookmarkSerializedListItemAvailable(storyId);
            return isAvailable;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }



    ///////////////////////////////////// Select City Method/////////////////////////////////////////////////////
    public void setSelectedCityList(String channelId, Vector<CityInfo> cityInfoVector) {
        try {
            SelectedCityListTable selectedCityListTable = (SelectedCityListTable) DatabaseHelper.getInstance(mContext).getTableObject(SelectedCityListTable.TABLE_NAME);
            selectedCityList = selectedCityListTable.getAllselectedCityListAccordingToAppId(channelId);
            if (selectedCityList.size() > 0) {
                selectedCityListTable.deleteParticularSectionInfoArrayList(channelId);
                selectedCityListTable.addselectedCityListItem(channelId, cityInfoVector);
            } else {
                selectedCityListTable.addselectedCityListItem(channelId, cityInfoVector);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Vector<CityInfo> getSelectedCityList(String channelId) {
        Log.e("DBDB_channelid get", channelId);
        try {
            SelectedCityListTable selectedCityListTable = (SelectedCityListTable) DatabaseHelper.getInstance(mContext).getTableObject(SelectedCityListTable.TABLE_NAME);
            selectedCityList = selectedCityListTable.getAllselectedCityListAccordingToAppId(channelId);
            if (selectedCityList != null && selectedCityList.size() > 0) {
                Log.e("DBDB_city list", selectedCityList.toString());
                return selectedCityList;
            } else {
                return new Vector<>(6, 6);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Vector<CityInfo>(6, 6);
    }

}
