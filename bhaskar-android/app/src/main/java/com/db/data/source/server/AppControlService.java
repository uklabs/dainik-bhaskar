package com.db.data.source.server;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.util.CommonConstants;
import com.db.dbvideo.player.BusProvider;
import com.db.eventWrapper.AppControlEventWrapper;
import com.db.eventWrapper.HomeFeedWrapper;
import com.db.splash.SplashDataInfo;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.JsonPreferences;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;

import static com.db.util.Constants.KeyPair.KEY_DATA;
import static com.db.util.Constants.KeyPair.KEY_SPLASH_DATA;
import static com.db.util.Constants.KeyPair.KEY_TODAY_DATE;
import static com.db.util.Constants.KeyPair.KEY_VERSION;

public class AppControlService extends Service {

    private Vector<JsonObjectRequest> requests = new Vector<>();
    private String finalUrl = "";
    private String todayDateOfMonth;

    @Override
    public void onCreate() {
        super.onCreate();

        VolleyNetworkSingleton.getInstance(AppControlService.this).getRequestQueue().addRequestFinishedListener((RequestQueue.RequestFinishedListener<JsonObjectRequest>) request -> {
            if (requests.contains(request)) {
                requests.remove(request);
                if (requests.size() == 0) {
                    sendFinalBroadCast();
                    AppControlService.this.stopSelf();
                }
            }
        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Bundle extras = intent.getExtras();
        int appHitValue = 0;
        if (extras != null) {
            appHitValue = extras.getInt(Constants.AppHitChoice.API_HIT);
        }

        if (appHitValue == Constants.AppHitChoice.APP_VERSION_API) {
            finalUrl = String.format(Urls.APP_VERSION_URL, CommonConstants.CHANNEL_ID);

            try {
                makeJsonObjectHit(appHitValue, finalUrl, false);
            } catch (Exception ignored) {
            }
        }

        return START_REDELIVER_INTENT;
    }

    private void preFetchHomeDataFromServer(String feedUrl) {
        Systr.println("HOME PAGE : hit on server for PG1 from splash : feed url : " + feedUrl);
        String url = Urls.APP_FEED_BASE_URL + feedUrl + "PG1/" + (Urls.IS_TESTING ? "?testing=1" : "");
        Systr.println("HOME PAGE : hit on server for PG1 from splash : url : " + url);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, response -> {
            Systr.println("HOME PAGE : GET DATA from server PG1 splash : " + response.toString());
            AppPreferences.getInstance(this).setStringValue(QuickPreferences.FeedData.HOME_PAGE_PG1, response.toString());
            sendHomeFeedBroadCast();
        }, error -> {
        });
        request.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(this).addToRequestQueue(request);
    }

    private void makeJsonObjectHit(final int appHitValue, final String finalUrl, boolean isEnableAppVersion) {
        Systr.println(String.format(" Feed API %s ", finalUrl));

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalUrl, null,
                response -> {
                    Systr.println(String.format(" Feed API %s Response %s ", finalUrl, response.toString()));

                    if (!TextUtils.isEmpty(response.toString())) {
                        try {
                            // first header should be parsing after that data json
                            if (appHitValue == Constants.AppHitChoice.APP_VERSION_API) {
                                AppPreferences.getInstance(AppControlService.this).setBooleanValue(QuickPreferences.GdprConst.IS_FAIL_VERSION, false);
                                parseHeaders(response.getJSONObject("headers"));
                            }
                            parseAndSaveJson(response.getJSONObject("data"), appHitValue);

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (appHitValue == Constants.AppHitChoice.APP_VERSION_API) {
                                AppPreferences.getInstance(AppControlService.this).setBooleanValue(QuickPreferences.GdprConst.IS_FAIL_VERSION, true);
                            }
                        }
                    }
                }, error -> {
            Systr.println(String.format(" Feed API %s Error %s ", finalUrl, error.getMessage()));
            if (appHitValue == Constants.AppHitChoice.APP_VERSION_API) {
                AppPreferences.getInstance(AppControlService.this).setBooleanValue(QuickPreferences.GdprConst.IS_FAIL_VERSION, true);
            }
            sendAppControlBroadCast(false, appHitValue);
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                if (isEnableAppVersion) {
                    headers.put("app-version", TrackingUtils.getAppVersion(AppControlService.this));
                    headers.put("version-code", "" + TrackingUtils.getAppVersionCode(AppControlService.this));
                    headers.put("app-platform", CommonConstants.USER_AGENT);
                }
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));

                    JSONObject jsonResponse = new JSONObject();
                    if (!TextUtils.isEmpty(jsonString))
                        jsonResponse.put("data", new JSONObject(jsonString));
                    Log.e("Header", "Header" + response.headers);
                    if (appHitValue == Constants.AppHitChoice.APP_VERSION_API) {
                        jsonResponse.put("headers", new JSONObject(response.headers));
                    }

                    return Response.success(jsonResponse, HttpHeaderParser.parseCacheHeaders(response));
                } catch (Exception e) {
                    return Response.error(new ParseError(e));
                }
            }
        };

        requests.add(jsonObjReq);
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(AppControlService.this).addToRequestQueue(jsonObjReq);
    }

    private void parseHeaders(JSONObject headers) throws JSONException {
        if (headers.has("GDPR") && headers.getString("GDPR").equalsIgnoreCase("Yes")) {
            AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        } else {
            AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, false);
        }
        AppUtils.isBlockCountry(getApplicationContext());

        /*Current Date*/
        if (headers.has("Date")) {
            Constants.currentDate = headers.getString("Date");
            todayDateOfMonth = getIndianDateOfMonth(headers.getString("Date"));
        } else if (headers.has("date")) {
            Constants.currentDate = headers.getString("date");
            todayDateOfMonth = getIndianDateOfMonth(headers.getString("date"));
        }
    }

    private void parseAndTriggerNextApiHit() {
        try {
            int appHitValue;
            int apiVersion;
            String apiName = "";

            JSONObject appVersionJsonObject = new JSONObject(JsonPreferences.getInstance(this).getStringValue(QuickPreferences.JsonPreference.APP_VERSION_JSON, ""));
            JSONArray jsonElements = appVersionJsonObject.getJSONArray(KEY_DATA);

            for (int i = 0; i < jsonElements.length(); i++) {
                JSONObject jsonObject = jsonElements.getJSONObject(i);
                try {
                    apiVersion = Integer.parseInt(jsonObject.optString("api_version"));
                    apiName = jsonObject.optString("api_name");
                } catch (NumberFormatException e) {
                    apiVersion = 0;
                }

                switch (apiName) {
                    case Constants.ApiName.CSS_API:
                        JsonPreferences.getInstance(this).setStringValue(QuickPreferences.CSSData.CSS_VERSION, apiVersion + "");
                        break;

                    case Constants.ApiName.HOST_API:
                        if (JsonPreferences.getInstance(this).getIntValue(QuickPreferences.JsonPreference.APP_HOST_VERSION, 0) < apiVersion || Urls.IS_TESTING) {
                            finalUrl = String.format(Urls.APP_HOST_URL, CommonConstants.CHANNEL_ID);
                            appHitValue = Constants.AppHitChoice.APP_HOST_API;
                            makeJsonObjectHit(appHitValue, finalUrl, false);
                        }
                        break;

                    case Constants.ApiName.CATEGORIES_API:
                        if (JsonPreferences.getInstance(this).getIntValue(String.format(QuickPreferences.JsonPreference.CATEGORY_FEED_VERSION, CommonConstants.CHANNEL_ID), 0) < apiVersion || Urls.IS_TESTING) {
                            finalUrl = String.format(Urls.APP_CATEGORY_URL, CommonConstants.CHANNEL_ID);
                            appHitValue = Constants.AppHitChoice.APP_CATEGORY_API;
                            makeJsonObjectHit(appHitValue, finalUrl, false);
                        }
                        break;

                    case Constants.ApiName.BASE_APP_CONTROL_API:
                        if (JsonPreferences.getInstance(this).getIntValue(QuickPreferences.JsonPreference.BASE_APP_CONTROL_VERSION, 0) < apiVersion || Urls.IS_TESTING) {
                            finalUrl = String.format(Urls.BASE_APP_CONTROL_URL, CommonConstants.CHANNEL_ID);
                            appHitValue = Constants.AppHitChoice.BASE_APP_CONTROL_API;
                            makeJsonObjectHit(appHitValue, finalUrl, true);
                        }
                        break;

                    case Constants.ApiName.CITY_LIST_API:
                        if (JsonPreferences.getInstance(this).getIntValue(String.format(QuickPreferences.JsonPreference.PREFERRED_CITY_FEED_VERSION, CommonConstants.CHANNEL_ID), 0) < apiVersion) {
                            finalUrl = String.format(Urls.APP_CITY_URL, CommonConstants.CHANNEL_ID);
                            appHitValue = Constants.AppHitChoice.APP_CITY_API;
                            makeJsonObjectHit(appHitValue, finalUrl, false);
                        }
                        break;

                    case Constants.ApiName.GA_EVENT_ENABLE:
                        parseValueData(jsonObject);
                        break;

                    case Constants.ApiName.APP_TUTORIAL:
                        AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.TUTORIAL_ENABLE, apiVersion == 1);
                        break;

                    case Constants.ApiName.GA_SCROLL_TEST:
                        AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.GA_SCROLL_TEST_ENABLE, apiVersion == 1);
                        break;

                    case Constants.ApiName.HOME_PAGINATION:
                        AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.HOME_PAGINATION_ENABLE, apiVersion == 1);
                        break;

                    case Constants.ApiName.HOME_LOGO:
                        AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.HomeLogo.IS_HOME_LOGO_ACTIVE, apiVersion == 1);
                        AppPreferences.getInstance(this).setStringValue(QuickPreferences.HomeLogo.HOME_LOGO_URL, jsonObject.optString("value"));
                        break;

                    case Constants.ApiName.ALL_CAT_HOME:
                        if (apiVersion == 1) {
                            String value = jsonObject.optString("value");
                            if (!TextUtils.isEmpty(value))
                                preFetchHomeDataFromServer(value);
                        }
                        break;
                }
            }
        } catch (Exception ignore) {
        }
    }


    private void parseAndSaveJson(JSONObject response, int appHitValue) {
        switch (appHitValue) {
            case Constants.AppHitChoice.APP_VERSION_API:
                JsonPreferences.getInstance(this).setIntValue(QuickPreferences.JsonPreference.APP_VERSION_VERSION, AppUtils.getInstance().stringToInt(response.optString(KEY_VERSION)));
                JsonPreferences.getInstance(this).setStringValue(QuickPreferences.JsonPreference.APP_VERSION_JSON, response.toString());
                JsonPreferences.getInstance(this).setStringValue(QuickPreferences.JsonPreference.APP_TODAY_DATE, response.optString(KEY_TODAY_DATE));
                parseAndTriggerNextApiHit();
                parseSplashData(response);
                parseGDPRData(response);
                break;
            default:
                JsonParser.getInstance().parseAndSaveJson(this, response, appHitValue);
        }
    }

    private void parseGDPRData(JSONObject appVersionJsonObject) {
        if (!TextUtils.isEmpty(appVersionJsonObject.toString())) {
            try {
                if (appVersionJsonObject.has("gdpr")) {
                    JSONObject mainObject = appVersionJsonObject.getJSONObject("gdpr");
                    if (mainObject != null) {
                        JSONObject fb = mainObject.optJSONObject("fb");
                        String enable = fb.optString("enable");

                        AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.VideoPref.GDRP_FB_ENABLE, enable.equalsIgnoreCase("1"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void parseSplashData(JSONObject appVersionJsonObject) {
        if (!TextUtils.isEmpty(appVersionJsonObject.toString())) {
            try {
                if (appVersionJsonObject.has(KEY_SPLASH_DATA)) {
                    JSONObject mainObject = appVersionJsonObject.getJSONObject(KEY_SPLASH_DATA);
                    if (mainObject != null) {
                        Constants.splashDataInfo = new Gson().fromJson(mainObject.toString(), SplashDataInfo.class);
                    } else {
                        Constants.splashDataInfo = null;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Constants.splashDataInfo = null;
            }
        } else {
            Constants.splashDataInfo = null;
        }
    }

    private void parseValueData(JSONObject jsonObject) {
        Systr.println("VERSION VALUE PARSING : todayDate : " + todayDateOfMonth);

        try {
            boolean value = false;
            String valueStr = jsonObject.getString("value");
            if (!TextUtils.isEmpty(valueStr)) {
                value = valueStr.contains(todayDateOfMonth);
                Systr.println("VERSION VALUE PARSING : todayDate : " + todayDateOfMonth + ", value : " + value);
            }
            TrackingHost.getInstance(AppControlService.this).setEventTrackingEnabled(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String getIndianDateOfMonth(String inputString) {
        Systr.println("VERSION VALUE date from header : " + inputString);
        DateFormat utcFormat = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
        utcFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        DateFormat indianFormat = new SimpleDateFormat("dd");
        utcFormat.setTimeZone(TimeZone.getTimeZone("IST"));

        try {
            Date timestamp = utcFormat.parse(inputString);
            String output = indianFormat.format(timestamp);
            return output;
        } catch (ParseException e) {
        }

        return null;
    }

    private void sendAppControlBroadCast(boolean serviceSuccessFlag, int appHitValue) {
        BusProvider.getInstance().post(new AppControlEventWrapper(appHitValue, serviceSuccessFlag));
    }

    private void sendFinalBroadCast() {
        BusProvider.getInstance().post(new AppControlEventWrapper(100, false));
    }

    private void sendHomeFeedBroadCast() {
        BusProvider.getInstance().post(new HomeFeedWrapper());
    }
}
