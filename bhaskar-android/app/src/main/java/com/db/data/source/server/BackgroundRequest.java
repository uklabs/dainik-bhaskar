package com.db.data.source.server;


import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.R;
import com.db.data.models.BrandInfo;
import com.db.data.models.CategoryInfo;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.listeners.OnStoryShareUrlListener;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonPreferences;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.AppUtils;
import com.db.util.VolleyNetworkSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.db.util.Constants.KeyPair.KEY_VERSION;

public class BackgroundRequest {

    public static void fetchPreferredCityFeedFromServer(final Context context, final String channelId, final OnFeedFetchFromServerListener onFeedFetchFromServerListener) {
        String Url = AppUtils.getInstance().updateApiUrl(String.format(Urls.APP_CITY_URL, channelId));
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, Url, null,
                response -> {
                    if (response != null) {
                        JsonPreferences.getInstance(context).setIntValue(String.format(QuickPreferences.JsonPreference.PREFERRED_CITY_FEED_VERSION, channelId), Integer.parseInt(response.optString(KEY_VERSION)));
                        JsonPreferences.getInstance(context).setStringValue(String.format(QuickPreferences.JsonPreference.PREFERRED_CITY_FEED_JSON, channelId), response.toString());
                        if (onFeedFetchFromServerListener != null)
                            onFeedFetchFromServerListener.onDataFetch(true);
                    }
                }, error -> {
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", context.getString(R.string.bhaskar_api_key));
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
    }

    public static void activeUser(final Context context, final String type, String gaClientID) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("db_id", TrackingData.getDBId(context));
            jsonObject.put("client_id:", gaClientID);
            jsonObject.put("app_id", CommonConstants.BHASKAR_APP_ID);
            jsonObject.put("type", type);
        } catch (Exception e) {
            AppLogs.printErrorLogs("active user jsonObject", "" + e);
        }
        AppLogs.printDebugLogs("Active user", "APP_SET_ACTIVE_USER= " + jsonObject.toString());
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, Urls.APP_SET_ACTIVE_USER, jsonObject, response -> {
            AppLogs.printDebugLogs("Active user", response.toString());
            if (type.equalsIgnoreCase(Constants.KeyPair.KEY_NOTIFY))
                AppPreferences.getInstance(context).setLongValue(QuickPreferences.ActiveUser.ACTIVE_USER_NOTIFY, System.currentTimeMillis());
            else
                AppPreferences.getInstance(context).setLongValue(QuickPreferences.ActiveUser.ACTIVE_USER_ARTICLE, System.currentTimeMillis());
        }, error -> {
            error.printStackTrace();
            AppLogs.printErrorLogs("Active user", "" + error);
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("api-key", "bh@@$k@r-D");
                params.put("User-Agent", CommonConstants.USER_AGENT);
                params.put("encrypt", "0");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(12000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public static void recommendationConsent(final Context context, String status, final boolean isWithdraw) {
        String dbId = TrackingData.getDBId(context);
        if (TextUtils.isEmpty(dbId) || dbId.equalsIgnoreCase("0")) {
            return;
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("did", dbId);
            jsonObject.put("status", status);
        } catch (Exception ignored) {
        }

        Systr.println("Recommendation Consent json : " + jsonObject.toString());

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, Urls.RECOMMENDATION_CONSENT_URL, jsonObject, response -> {
            Systr.println("Recommendation Consent res : " + response.toString());
            if (!isWithdraw)
                AppPreferences.getInstance(context).setBooleanValue(QuickPreferences.RECOMMENDATION_CONSENT_SENT, true);
            else
                AppPreferences.getInstance(context).setBooleanValue(QuickPreferences.RECOMMENDATION_CONSENT_SENT, false);
        }, error -> Systr.println("Recommendation Consent error : " + error)) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("api-key", "bh@@$k@r-D");
                params.put("User-Agent", CommonConstants.USER_AGENT);
                params.put("encrypt", "0");
                return params;
            }
        };

        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(12000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonRequest);
    }

    public static void fetchAvailableOffers(Context context, BrandInfo brandInfo, final OnFeedFetchFromServerListener feedFetchFromServerListener) {
        if (NetworkStatus.getInstance().isConnected(context)) {
            String finalFeedURL = Urls.APP_FEED_BASE_URL + String.format(Urls.APP_OFFER_URL, CommonConstants.CHANNEL_ID);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalFeedURL, null,
                    response -> {
                        if (feedFetchFromServerListener != null) {
                            if (brandInfo == null) {
                                feedFetchFromServerListener.onDataFetch(true, response.toString());
                            }
                        }
                    }
                    , error -> {
                if (feedFetchFromServerListener != null) {
                    if (brandInfo == null) {
                        feedFetchFromServerListener.onDataFetch(false, "");
                    }
                }
                if (error instanceof NoConnectionError || error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);

        } else {
            AppUtils.getInstance().showCustomToast(context, context.getString(R.string.no_network_error));
        }
    }

    public static void getLiveTvToken(final Context context, final OnFeedFetchFromServerListener onFeedFetchFromServerListener, CategoryInfo categoryInfo) {

        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject jsonDataObject = new JSONObject();
            jsonDataObject.put("unique_id", TrackingData.getDBId(context));
            jsonObject.put("data", jsonDataObject);
        } catch (JSONException ignored) {
        }
        Systr.println("getLiveTvToken send Json : " + jsonObject.toString());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Urls.LIVETV_LOGIN, jsonObject, response -> {
            try {
                int responseStatus = response.getInt("status");
                if (responseStatus == 0 || responseStatus == 1) {
                    AppPreferences.getInstance(context).setStringValue(QuickPreferences.LiveTv.TOKEN, response.optString("token"));
                    AppPreferences.getInstance(context).setStringValue(QuickPreferences.LiveTv.EXPIRY, response.optString("expiry"));
                    AppPreferences.getInstance(context).setStringValue(QuickPreferences.LiveTv.USER_ID, response.optString("userId"));
                    AppPreferences.getInstance(context).setStringValue(QuickPreferences.LiveTv.PARTNER_ID, response.optString("partnerId"));
                    if (onFeedFetchFromServerListener != null) {
                        onFeedFetchFromServerListener.onDataFetch(true, categoryInfo);
                    }
                }
            } catch (JSONException ignored) {
                if (onFeedFetchFromServerListener != null) {
                    onFeedFetchFromServerListener.onDataFetch(false, categoryInfo);
                }
            }
        }, error -> {
            error.printStackTrace();
            if (onFeedFetchFromServerListener != null) {
                onFeedFetchFromServerListener.onDataFetch(false, categoryInfo);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credentials = "Divya-Bhaskar:b0d177e0c85d0cd47d1be96f370686ca";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
                auth = auth.trim();
                headers.put("Authorization", auth);
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
    }

    public static void fetchLatestCSS(final Context context) {
        String cssVersion = JsonPreferences.getInstance(context).getStringValue(QuickPreferences.CSSData.CSS_VERSION, "1");
        if (NetworkStatus.getInstance().isConnected(context)) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET
                    , String.format(Urls.CSS_API_URL, CommonConstants.CHANNEL_ID, cssVersion)
                    , response -> JsonPreferences.getInstance(context).setStringValue(QuickPreferences.CSSData.CSS_DATA, response)
                    , error -> {
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(stringRequest);
        } else {
            AppUtils.getInstance().showCustomToast(context, context.getString(R.string.no_network_error));
        }
    }

    public static void getStoryShareUrl(final Context context, final String storyUrl, final OnStoryShareUrlListener onStoryShareUrlListener) {
        String apiUrl = String.format(Urls.ARTICLE_SHARE_URL, CommonConstants.CHANNEL_ID);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("storyurl", storyUrl);
            jsonObject.put("channel_slno", CommonConstants.CHANNEL_ID);
            jsonObject.put("app_id", CommonConstants.BHASKAR_APP_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, apiUrl, jsonObject, response -> {
            try {
                String responseData = response.getString("story_dyn_url");
                if (!TextUtils.isEmpty(responseData)) {
                    if (onStoryShareUrlListener != null) {
                        onStoryShareUrlListener.onUrlFetch(true, responseData);
                    }
                } else {
                    if (onStoryShareUrlListener != null) {
                        onStoryShareUrlListener.onUrlFetch(false, storyUrl);
                    }
                }
            } catch (JSONException ignored) {
                if (onStoryShareUrlListener != null) {
                    onStoryShareUrlListener.onUrlFetch(false, storyUrl);
                }
            }
        }, error -> {
            error.printStackTrace();
            if (onStoryShareUrlListener != null) {
                onStoryShareUrlListener.onUrlFetch(false, storyUrl);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credentials = "Divya-Bhaskar:b0d177e0c85d0cd47d1be96f370686ca";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
                auth = auth.trim();
                headers.put("Authorization", auth);
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
    }
}