package com.db.data.source.server.fcm;

public interface FirebaseNotificationConstants {

    String STORY_ID = "storyId";
    String CAT_ID = "catId";
    String PCAT_ID = "pcat_id";
    String MESSAGE = "message";
    String PUSH_TYPE = "pushtype";
    String ACTION_TXT = "action_txt";
    String TITLE = "title";
    String COLOR = "color";
    String IMAGE_URL = "img_url";
    String WEB_URL = "webUrl";
    String WEB_TITLE = "webTitle";
    String WEB_EXT = "webExt";
    String BRAND_ID = "brandId";
    String MENU_ID = "menuId";
    String DEEP_LINK_URL = "deeplink_url";
    String SL_ID = "sl_id";
    String PERSONALISED = "personalised";

    interface NotificationCatIdType {
        String CAT_ID_NEWS = "1";
        String CAT_ID_BUSINESS = "2";
        String CAT_ID_RELIGION = "3";
        String CAT_ID_RASHIFAL = "4";
        String CAT_ID_DB_VIDEOS = "5";
        String CAT_ID_BRAND_AND_CATEGORY = "6";
        String CAT_ID_URL = "7";
        String CAT_ID_DEEP_LINK_URL = "8";
        String CAT_ID_SILENT = "9";
        String CAT_ID_CRICKET = "10";
        String CAT_ID_AUDIO = "11";
    }

    interface PushType {
        int PUSH_TYPE_SMALL = 1;
        int PUSH_TYPE_BIG = 2;
        int PUSH_TYPE_MULTILINE = 3;
        int PUSH_TYPE_DEFAULT = 4;
        int PUSH_TYPE_DEFAULT_BIG = 5;
        int PUSH_TYPE_DEFAULT_MULTILINE = 6;

        int NOTIFICATION_TYPE_SYSTEM = 1;
        int NOTIFICATION_TYPE_SMALL_IMAGE = 2;
        int NOTIFICATION_TYPE_BIG_IMAGE = 3;
        int NOTIFICATION_TYPE_APPEND = 10;
    }


}
