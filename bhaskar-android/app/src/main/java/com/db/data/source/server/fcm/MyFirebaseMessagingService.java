package com.db.data.source.server.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.core.app.NotificationCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.atracker.ATracker;
import com.bhaskar.util.AppUrls;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.view.ui.splash.SplashActivity;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.NotificationStackTable;
import com.db.data.models.CategoryInfo;
import com.db.data.models.SplashBundleInfo;
import com.db.database.DatabaseClient;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.main.NotificationReceiver;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.Notificationutil;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by DB on 8/26/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private int lockVisibility = 1;
    private int notificationType = 0;
    private long notificationToken = System.currentTimeMillis();
    private boolean isRealtimeTrackingOn = true;
    private String PUSH_TAG = "CleverTapPush";

    @Override
    public void onNewToken(@NotNull String token) {
        super.onNewToken(token);
        AppLogs.printDebugLogs(TAG, "Refreshed token: " + token);

        if (!TextUtils.isEmpty(token)) {
            // set token to tracking module
            TrackingHost.getInstance(this).setFCMToken(token);
            String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            String url = TrackingHost.getInstance(this).getToggleNotifyUrl(AppUrls.TOGGLE_NOTIFY_URL);

            boolean isBlockedCountry = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
            boolean consentAccepted = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.GA_CONSENT, false);
            if (!isBlockedCountry || (isBlockedCountry && consentAccepted)) {
                @ATracker.ToggleStatus int notificationValue = AppPreferences.getInstance(this).getIntValue(QuickPreferences.NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);
                Tracking.notifyToggle(this, CommonConstants.BHASKAR_APP_ID, url, notificationValue, ATracker.ToggleType.OTHER, source, medium, campaign, null, "1", "1", "1", "1");
            }

            //Clever tap notification
            Tracking.setFCMTokenForCleverTap(getApplicationContext(), token);
            AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.IS_TOKEN_UPLOADED, false);

        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {

            AppPreferences.getInstance(this).setLongValue(QuickPreferences.NOTIFICATION_RECEIVE_TIME_MILLIS, System.currentTimeMillis());

            Systr.println("Message data payload: " + remoteMessage.getData());
            Map<String, String> map = remoteMessage.getData();
            String catId = map.get(FirebaseNotificationConstants.CAT_ID);
            String deeplink = map.get(FirebaseNotificationConstants.DEEP_LINK_URL);
            catId = (TextUtils.isEmpty(catId)) ? "" : catId;
            Bundle bundle = Tracking.isCleverTapNotification(map);
            if (bundle != null) {
                Tracking.createCleverTapNotification(getApplicationContext(), bundle);
                // Send firebase event for ClevertTap notification received
                sendFirebaseCleverTapEvent(bundle);
                return;
            }

            //Check On Setting Rashifal and notification is On Or Not
            if (catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_RASHIFAL)) {
                isRealtimeTrackingOn = true;
                // if rashifal on in settings then show rashifal notification
                if (Notificationutil.getRashifalNotificationStatus(this)) {
                    //Call Rashifal Generate method
                    fetchAllNotificationMessageData(remoteMessage);
                }
            } else if (catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_DB_VIDEOS)) {
                isRealtimeTrackingOn = true;
                // if notification on in settings then show notification
                // And Daily Frequency count not exceed max Limit
                // If DND is On Check DND time
//                if (Notificationutil.getNotificationStatus(this)) {
                //All above condition are true than generate push Notification
                fetchVideoNotificationMessageDate(remoteMessage);
//                }
            } else if (catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_BRAND_AND_CATEGORY)) {
                isRealtimeTrackingOn = true;
//                if (Notificationutil.getNotificationStatus(this)) {
                fetchAllNotificationMessageData(remoteMessage);
//                }
            } else if (catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_URL)) {
                isRealtimeTrackingOn = true;
//                if (Notificationutil.getNotificationStatus(this)) {
                fetchAllNotificationMessageData(remoteMessage);
//                }
            } else if (catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_DEEP_LINK_URL)) {
                isRealtimeTrackingOn = true;
//                if (Notificationutil.getNotificationStatus(this)) {
                Task<PendingDynamicLinkData> dynamicLink = FirebaseDynamicLinks.getInstance().getDynamicLink(Uri.parse(deeplink));
                String finalCatId = catId;
                dynamicLink.addOnSuccessListener(command -> {
                    try {
                        String extendedUrl = command.getLink().toString();
                        HashMap<String, String> parsedMap = AppUtils.parseExtendedDynamicURL(extendedUrl);
                        String menuID = parsedMap.get("menu_id");
                        if (!TextUtils.isEmpty(menuID)) {
                            CategoryInfo categoryInfo = DatabaseClient.getInstance(this).getCategoryById(menuID);
                            if (categoryInfo != null) {
                                saveExtendedUrl(deeplink, extendedUrl, map, finalCatId);
                                fetchAllNotificationMessageData(remoteMessage);
                            }
                        } else {
//                            map.put(FirebaseNotificationConstants.CAT_ID,FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_URL);
//                            map.put(FirebaseNotificationConstants.WEB_URL,extendedUrl);

                            fetchAllNotificationMessageData(remoteMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                });
//                }
            }
            else if(catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_SILENT)) {

            }else {
                isRealtimeTrackingOn = true;
                if (Notificationutil.getNotificationStackStatus(this)) {
                    // if notification on in settings then show notification
                    // If DND is On Check DND time
                    //If Stack Notification enable don't check frequency Count
//                    if (Notificationutil.getNotificationStatus(this) && ((!Notificationutil.getNotificationDNDStatus(this)) ? true : !Notificationutil.checkNotificationDNDTiming(this))) {
                    if ((!Notificationutil.getNotificationDNDStatus(this)) || !Notificationutil.checkNotificationDNDTiming(this)) {
                        //All above condition are true than generate push Notification
                        fetchAllNotificationMessageData(remoteMessage);
                    }
                } else {
                    // if notification on in settings then show notification
                    // And Daily Frequency count not exceed max Limit
                    // If DND is On Check DND time
//                    if (Notificationutil.getNotificationStatus(this) && (Notificationutil.checkFrequencyCount(this) && ((!Notificationutil.getNotificationDNDStatus(this)) ? true : !Notificationutil.checkNotificationDNDTiming(this)))) {
//                    if ((Notificationutil.checkFrequencyCount(this) &&(!Notificationutil.getNotificationDNDStatus(this)) || !Notificationutil.checkNotificationDNDTiming(this))) {
                    if (((!Notificationutil.getNotificationDNDStatus(this)) || !Notificationutil.checkNotificationDNDTiming(this))) {
                        //All above condition are true than generate push Notification
                        fetchAllNotificationMessageData(remoteMessage);
                    }
                }
            }
        }
    }

    private void saveExtendedUrl(String shortURL, String extendedUrl, Map<String, String> map, String catId) {
        SplashBundleInfo splashBundleInfo = new SplashBundleInfo();
        splashBundleInfo.deeplink = shortURL;
        String storyId = map.get(FirebaseNotificationConstants.STORY_ID);
        splashBundleInfo.catId = map.get(FirebaseNotificationConstants.CAT_ID);
        splashBundleInfo.storyId = storyId;
        splashBundleInfo.title = map.get(FirebaseNotificationConstants.TITLE);
        splashBundleInfo.slId = map.get(FirebaseNotificationConstants.SL_ID);
        splashBundleInfo.channelId = Notificationutil.getChannel(this, catId);
        splashBundleInfo.newsType = Notificationutil.getNewsType(this, storyId, catId);

        //for catId 6  Brand and Category
        splashBundleInfo.menuId = map.get(FirebaseNotificationConstants.MENU_ID);

        //for catId 7 URL
        splashBundleInfo.webTitle = map.get(FirebaseNotificationConstants.WEB_TITLE);
        splashBundleInfo.isWapExt = map.get(FirebaseNotificationConstants.WEB_EXT);
        splashBundleInfo.isWapExt = (TextUtils.isEmpty(splashBundleInfo.isWapExt) ? "0" : splashBundleInfo.isWapExt);


        Gson gson = new Gson();
        String json = gson.toJson(splashBundleInfo);
        AppPreferences.getInstance(this).setStringValue(String.valueOf(extendedUrl.hashCode()), json);
    }


    /**
     * @param bundle
     */
    private void sendFirebaseCleverTapEvent(Bundle bundle) {
        try {
            String notificationTitle = bundle.getString("nm");
            if (!TextUtils.isEmpty(notificationTitle)) {
                try {
                    //Firebase Tracking
//                    TrackerUtils.setFirebaseEvent(getApplicationContext(), notificationTitle, GAConstants.FirebaseEvent.NOTIFICATION_RECEIVED);
                } catch (Exception ignored) {

                }
            }
        } catch (Exception ignored) {
        }
    }

    private void fetchVideoNotificationMessageDate(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            Systr.println("fetchVideoNotificationMessageData Message data payload: " + remoteMessage.getData());

            Map<String, String> map = remoteMessage.getData();
            String message = map.get(FirebaseNotificationConstants.MESSAGE);
            String catId = map.get(FirebaseNotificationConstants.CAT_ID);
            String pCatId = map.get(FirebaseNotificationConstants.PCAT_ID);
            String storyId = map.get(FirebaseNotificationConstants.STORY_ID);
            String title = map.get(FirebaseNotificationConstants.TITLE);
            String color = map.get(FirebaseNotificationConstants.COLOR);
            String imageURL = map.get(FirebaseNotificationConstants.IMAGE_URL);
            String actionText = map.get(FirebaseNotificationConstants.ACTION_TXT);
            String pushType = map.get(FirebaseNotificationConstants.PUSH_TYPE);

            //for catId 6  Brand and Category
            String brandId = map.get(FirebaseNotificationConstants.BRAND_ID);
            String menuId = map.get(FirebaseNotificationConstants.MENU_ID);

            //for catId 7 URL
            String mWeburl = map.get(FirebaseNotificationConstants.WEB_URL);
            String mWebTitle = map.get(FirebaseNotificationConstants.WEB_TITLE);
            String isWapExt = map.get(FirebaseNotificationConstants.WEB_EXT);
            isWapExt = (TextUtils.isEmpty(isWapExt) ? "0" : isWapExt);

            //for catId 8 (Deeplink)
            String deeplinkURL = map.get(FirebaseNotificationConstants.DEEP_LINK_URL);
            String sl_id = map.get(FirebaseNotificationConstants.SL_ID);
            boolean isPersonlized = map.containsKey(FirebaseNotificationConstants.PERSONALISED);

            try {
                if (!TextUtils.isEmpty(storyId)) {
                    if (storyId.contains("00001")) {
                        if (AppUtils.getInstance().checkDoubleNotification(this, storyId))
                            return;
                        else {
                            String data = AppPreferences.getInstance(this).getStringValue(QuickPreferences.DOUBLE_NOTIFICATION_DATA, "");
                            data = data + storyId + ",";
                            AppPreferences.getInstance(this).setStringValue(QuickPreferences.DOUBLE_NOTIFICATION_DATA, data);
                        }
                    }
                }
            } catch (Exception ignored) {
            }


            if (map.containsKey(FirebaseNotificationConstants.PUSH_TYPE)) {
                try {
                    notificationType = Integer.parseInt(pushType != null ? pushType : "0");
                } catch (NumberFormatException ignored) {
                }
            }

            if (TextUtils.isEmpty(pCatId)) pCatId = "";
            if (TextUtils.isEmpty(catId)) catId = "";
            if (TextUtils.isEmpty(menuId)) menuId = "";
            if (TextUtils.isEmpty(brandId)) brandId = "";
            if (TextUtils.isEmpty(mWeburl)) mWeburl = "";
            if (TextUtils.isEmpty(mWebTitle)) mWebTitle = "";
            if (TextUtils.isEmpty(isWapExt)) isWapExt = "";

            String channel = Notificationutil.getChannel(this, catId);
            String newsType = Notificationutil.getNewsType(this, storyId, catId);
            String aurl = "";
            String domain = Notificationutil.getDomain(this);
            String url = domain + AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN;
            try {
                if (!TextUtils.isEmpty(message.trim())) {
                    fetchVideoData(message, catId, pCatId, storyId, imageURL, channel, newsType, aurl, url, domain, menuId, brandId, mWeburl, mWebTitle, isWapExt, title, actionText, color, sl_id, isPersonlized);
                }
            } catch (Exception ignored) {
            }
        }
    }


    private void fetchVideoData(final String message, final String catId, final String pCatId, final String storyId, final String image, final String channel, final String newsType, final String aurl, final String url, final String domain, final String menuId, final String brandId, final String mWeburl, final String mWebTitle, final String isWapExt, String title, String actionText, String color, String slid, boolean isPersonlized) {
        String fetchUrl = AppPreferences.getInstance(this).getStringValue(QuickPreferences.APP_VIDEO_NOTIFICATION_URL, AppUrls.PUSH_VIDEO_PREFIX);
        if (!TextUtils.isEmpty(fetchUrl)) {
            Systr.println("Notification video url : " + fetchUrl);
            fetchUrl = fetchUrl + CommonConstants.CHANNEL_ID + "/" + storyId + "/";
            Systr.println("Video fetch url : " + fetchUrl);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, fetchUrl, null,
                    response -> {
                        Systr.println("Notification DBVideo : " + response);
                        try {
                            if (response != null && response.length() > 0) {
                                VideoInfo videosInfo = new Gson().fromJson(response.toString(), VideoInfo.class);
                                if (videosInfo != null) {
                                    startDBVideoNotification(message, catId, pCatId, storyId, channel, newsType, aurl, url, domain, videosInfo, menuId, brandId, mWeburl, mWebTitle, isWapExt, title, actionText, color, slid, isPersonlized);
                                }
                            }
                        } catch (Exception ignored) {
                        }
                    },
                    error -> Systr.println("Notification Video Error : " + error)) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(this).addToRequestQueue(jsonObjReq);
        }
    }

    private void startDBVideoNotification(final String message, String catId, String pCatId, String storyId, String channelId, String newsType, String aurl, String url, String domain, VideoInfo videosInfo, String menuId, String brandId, String mWeburl, String mwebTitle, String isWapExt, String title, String actionText, String color, String slid, boolean isPersonlized) {
        // Tracking
        if (isRealtimeTrackingOn) {
            Tracking.trackWisdomNotification(this, TrackingHost.getInstance(this).getWisdomUrlForNotification(AppUrls.WISDOM_NOTIFICATION_URL), catId, pCatId, message, newsType, "1", storyId, aurl, url, CommonConstants.BHASKAR_APP_ID, channelId, "", slid);
            Systr.println("Wisdom Notification tracking : " + storyId);
        }

        Intent pushIntent = new Intent(this, SplashActivity.class);
        pushIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pushIntent.putExtra(Constants.KeyPair.IN_BACKGROUND, 0);
        pushIntent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, true);
        pushIntent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, true);
        pushIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID));
        pushIntent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, message);
        pushIntent.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, channelId);
        pushIntent.putExtra(Constants.KeyPair.KEY_STORY_ID, storyId);
        pushIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, catId);
        pushIntent.putExtra(Constants.KeyPair.KEY_PCAT_ID, pCatId);
        pushIntent.putExtra(Constants.KeyPair.KEY_MENU_ID, menuId);
        pushIntent.putExtra(Constants.KeyPair.KEY_WEB_URL, mWeburl);
        pushIntent.putExtra(Constants.KeyPair.KEY_WEB_TITLE, mwebTitle);
        pushIntent.putExtra(Constants.KeyPair.KEY_IS_WAP_EXT, isWapExt);
        pushIntent.putExtra(Constants.KeyPair.KEY_NEWS_TYPE, newsType);
        pushIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN);
        if (isPersonlized) {
            pushIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, AppFlyerConst.GAExtra.NOTIFICATION_GA_ARTICLE_PER);
        } else {
            pushIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, AppFlyerConst.GAExtra.NOTIFICATION_GA_ARTICLE);
        }
        pushIntent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, domain);
        pushIntent.putExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN, AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN);
        // video info
        boolean isPersonalizedFeedUrl;
        String detailUrl;
        String feedUrl;
        String gaEventLabel = "";

        detailUrl = AppPreferences.getInstance(this).getStringValue(QuickPreferences.VideoPref.SETTINGS_VIDEO_DETAIL_URL_V2, "") + CommonConstants.CHANNEL_ID + "/";
        feedUrl = AppUrls.VIDEO_RECOMMENDATION_URL + CommonConstants.CHANNEL_ID + "/" + videosInfo.storyId + "/";
        isPersonalizedFeedUrl = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.VideoPref.SETTINGS_IS_VIDEO_FEED_URL_PERSONALIZED, false);

        pushIntent.putExtra(Constants.KeyPair.KEY_IS_VIDEO_NOTIFICATION, true);
        pushIntent.putExtra(Constants.KeyPair.KEY_DBVIDEO_INFO, videosInfo);
        pushIntent.putExtra(Constants.KeyPair.KEY_SECTION_LABEL, AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN);
        pushIntent.putExtra(Constants.KeyPair.KEY_SOURCE, AppFlyerConst.DBVideosSource.VIDEO_NOTIFY);
        pushIntent.putExtra(Constants.KeyPair.KEY_POSITION, -1);
        pushIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, detailUrl);
        pushIntent.putExtra(Constants.KeyPair.KEY_FEED_URL, feedUrl);
        pushIntent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
        pushIntent.putExtra(Constants.KeyPair.KEY_FEED_URL_IS_PERSONALIZED, isPersonalizedFeedUrl);

        final PendingIntent pendingIntent;
        if (notificationType == FirebaseNotificationConstants.PushType.NOTIFICATION_TYPE_APPEND) {
            pendingIntent = PendingIntent.getActivity(this, Constants.NOTIFICATION_APPEND_ID, pushIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), pushIntent, PendingIntent.FLAG_ONE_SHOT);
        }
        //notification replace
        if (!TextUtils.isEmpty(storyId)) {
            try {
                notificationToken = Integer.parseInt(AppUtils.getFilteredStoryId(storyId));
            } catch (Exception ignored) {
            }
        } else {
            notificationToken = System.currentTimeMillis();
        }

        if (!TextUtils.isEmpty(videosInfo.image)) {
            ImageRequest imageRequest = new ImageRequest(videosInfo.image, bitmap -> {
                Systr.println("Bitmap downloaded successfully");
//                bitmap = drawTextToBitmap(MyFirebaseMessagingService.this, bitmap, "Watch Video");
                generateNotification(message, bitmap, pendingIntent, title, actionText, color, true, catId);
            }, 0, 0, ImageView.ScaleType.CENTER_CROP,
                    Bitmap.Config.RGB_565, error -> {
                Systr.println("Bitmap load failed");
                generateNotification(message, null, pendingIntent, title, actionText, color, true, catId);
            });
            imageRequest.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(this).addToRequestQueue(imageRequest);

        } else {

            generateNotification(message, null, pendingIntent, title, actionText, color, true, catId);
        }

    }


    public void fetchAllNotificationMessageData(RemoteMessage remoteMessage) {
        Notificationutil.isDynamicNotificationAvailable = true;
        if (remoteMessage.getData().size() > 0) {
            Systr.println("fetchAllNotificationMessageData Message data payload: " + remoteMessage.getData());
            Map<String, String> map = remoteMessage.getData();
            String message = map.get(FirebaseNotificationConstants.MESSAGE);
            String catId = map.get(FirebaseNotificationConstants.CAT_ID);
            String pCatId = map.get(FirebaseNotificationConstants.PCAT_ID);
            String storyId = map.get(FirebaseNotificationConstants.STORY_ID);
            String deeplink = map.get(FirebaseNotificationConstants.DEEP_LINK_URL);
            String pushType = map.get(FirebaseNotificationConstants.PUSH_TYPE);
            pushType = TextUtils.isEmpty(pushType) ? "0" : pushType;

            String title = map.get(FirebaseNotificationConstants.TITLE);
            String color = map.get(FirebaseNotificationConstants.COLOR);
            String imageURL = map.get(FirebaseNotificationConstants.IMAGE_URL);
            String actionText = map.get(FirebaseNotificationConstants.ACTION_TXT);

            //for catId 6
            String brandId = map.get(FirebaseNotificationConstants.BRAND_ID);
            String menuId = map.get(FirebaseNotificationConstants.MENU_ID);

            //for catId 7
            String mWeburl = map.get(FirebaseNotificationConstants.WEB_URL);
            String mWebTitle = map.get(FirebaseNotificationConstants.WEB_TITLE);
            String isWapExt = map.get(FirebaseNotificationConstants.WEB_EXT);
            isWapExt = (TextUtils.isEmpty(isWapExt) ? "0" : isWapExt);

            //for catId 8 (Deeplink)
            String deeplinkURL = map.get(FirebaseNotificationConstants.DEEP_LINK_URL);
            String sl_id = map.get(FirebaseNotificationConstants.SL_ID);
            boolean isPersonlized = map.containsKey(FirebaseNotificationConstants.PERSONALISED);

            try {
                if (!TextUtils.isEmpty(storyId)) {
                    if (storyId.contains("00001")) {
                        if (AppUtils.getInstance().checkDoubleNotification(this, storyId))
                            return;
                        else {
                            String data = AppPreferences.getInstance(this).getStringValue(QuickPreferences.DOUBLE_NOTIFICATION_DATA, "");
                            data = data + storyId + ",";
                            AppPreferences.getInstance(this).setStringValue(QuickPreferences.DOUBLE_NOTIFICATION_DATA, data);
                        }
                    }
                }
            } catch (Exception ignored) {
            }

            if (map.containsKey(FirebaseNotificationConstants.PUSH_TYPE)) {
                String mStoryId = AppUtils.getFilteredStoryId(storyId);
                mStoryId = mStoryId.equalsIgnoreCase("0") ? "" : mStoryId;
                if (Notificationutil.getNotificationStackStatus(this)) {
                    if (mStoryId.length() > 0 && !catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_RASHIFAL) && !(catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_BRAND_AND_CATEGORY) || catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_URL))) {
                        notificationType = FirebaseNotificationConstants.PushType.NOTIFICATION_TYPE_APPEND;
                    } else {
                        try {
                            if (pushType != null) {
                                notificationType = Integer.parseInt(pushType);
                            }
                        } catch (NumberFormatException ignored) {
                        }
                    }
                } else {
                    try {
                        if (pushType != null) {
                            notificationType = Integer.parseInt(pushType);
                        }
                    } catch (NumberFormatException ignored) {
                    }
                }
            }

            catId = (!TextUtils.isEmpty(catId)) ? catId : "";
            pCatId = (!TextUtils.isEmpty(pCatId)) ? pCatId : "";
            menuId = (!TextUtils.isEmpty(menuId)) ? menuId : "";
            brandId = (!TextUtils.isEmpty(brandId)) ? brandId : "";
            mWeburl = (!TextUtils.isEmpty(mWeburl)) ? mWeburl : "";
            mWebTitle = (!TextUtils.isEmpty(mWebTitle)) ? mWebTitle : "";
            isWapExt = (!TextUtils.isEmpty(isWapExt)) ? isWapExt : "";

            // Rashifal
            if (catId != null && catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_RASHIFAL)) {
                storyId = map.get("sign");
            }
            String channel = Notificationutil.getChannel(this, catId);
//            String image = Notificationutil.getImage(this, storyId, catId, notificationType, channel);
            String newsType = Notificationutil.getNewsType(this, storyId, catId);
            String aurl = "";
            String domain = Notificationutil.getDomain(this);
            String url = domain + AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN;
            try {
                if (!TextUtils.isEmpty(message.trim())) {
                    startDBNewsNotification(message, catId, pCatId, storyId, imageURL, channel, newsType, aurl, url, domain, menuId, brandId, mWeburl, mWebTitle, isWapExt, deeplink, title, actionText, color, sl_id, isPersonlized);
                }
            } catch (Exception ignored) {
            }
        }
    }

    private void startDBNewsNotification(final String message, String catId, String pCatId, String storyId, String imageUrl, String channelId, String newsType, String aurl, String url, String domain, String menuId, String brandId, String webUrl, String mWebTitle, String isWapExt, String deeplink, String title, String actionText, String color, String slId, boolean isPersonlized) {
        // Tracking
        if (isRealtimeTrackingOn) {
            Tracking.trackWisdomNotification(this, TrackingHost.getInstance(this).getWisdomUrlForNotification(AppUrls.WISDOM_NOTIFICATION_URL), catId, pCatId, message, newsType, "1", storyId, aurl, url, CommonConstants.BHASKAR_APP_ID, channelId, "", slId);
            Systr.println("Wisdom Notification tracking : " + storyId);
        }

        String mstoryId = AppUtils.getFilteredStoryId(storyId);
        storyId = mstoryId.equalsIgnoreCase("0") ? "" : storyId;

        Intent pushIntent = new Intent(this, SplashActivity.class);
        pushIntent.putExtra(Constants.KeyPair.IN_BACKGROUND, 0);
        pushIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (Notificationutil.getNotificationStackStatus(this)) {
            String mStoryId = AppUtils.getFilteredStoryId(storyId);
            mStoryId = mStoryId.equalsIgnoreCase("0") ? "" : mStoryId;
            if (mStoryId.length() > 0 && !catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_RASHIFAL) && !(catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_BRAND_AND_CATEGORY) || catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_URL))) {
                pushIntent.putExtra(Constants.KeyPair.KEY_IS_STACK_NOTIFICATION, true);
            } else {
                pushIntent.putExtra(Constants.KeyPair.KEY_IS_STACK_NOTIFICATION, false);
            }
        } else {
            pushIntent.putExtra(Constants.KeyPair.KEY_IS_STACK_NOTIFICATION, false);
        }
        pushIntent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, true);
        pushIntent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, true);
        pushIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID));
        pushIntent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, message);
        pushIntent.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, channelId);
        pushIntent.putExtra(Constants.KeyPair.KEY_STORY_ID, storyId);
        pushIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, catId);
        pushIntent.putExtra(Constants.KeyPair.KEY_PCAT_ID, pCatId);
        pushIntent.putExtra(Constants.KeyPair.KEY_MENU_ID, menuId);
        pushIntent.putExtra(Constants.KeyPair.KEY_WEB_URL, webUrl);
        pushIntent.putExtra(Constants.KeyPair.KEY_WEB_TITLE, mWebTitle);
        pushIntent.putExtra(Constants.KeyPair.KEY_IS_WAP_EXT, isWapExt);
        pushIntent.putExtra(Constants.KeyPair.KEY_NEWS_TYPE, newsType);
        pushIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN);
        pushIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, AppFlyerConst.GAExtra.NOTIFICATION_GA_ARTICLE);
        pushIntent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN);
        pushIntent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, domain);
        pushIntent.putExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN, AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN);

        pushIntent.putExtra(Constants.KeyPair.KEY_DEEP_LINK, deeplink);
        pushIntent.putExtra(Constants.KeyPair.KEY_SL_ID, slId);

        if (isPersonlized) {
            pushIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, AppFlyerConst.GAExtra.NOTIFICATION_GA_ARTICLE_PER);
        } else {
            pushIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, AppFlyerConst.GAExtra.NOTIFICATION_GA_ARTICLE);
        }

        pushIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        final PendingIntent pendingIntent;
        if (catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_DEEP_LINK_URL)) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(deeplink));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_ONE_SHOT);
        } else if (notificationType == FirebaseNotificationConstants.PushType.NOTIFICATION_TYPE_APPEND) {
            String mStoryId = AppUtils.getFilteredStoryId(storyId);
            mStoryId = mStoryId.equalsIgnoreCase("0") ? "" : mStoryId;
            if (mStoryId.length() > 0 && !catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_RASHIFAL) && !(catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_BRAND_AND_CATEGORY) || catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_URL))) {
                pendingIntent = PendingIntent.getActivity(this, Constants.NOTIFICATION_APPEND_ID, pushIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            } else {
                pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), pushIntent, PendingIntent.FLAG_ONE_SHOT);
            }
        } else {
            pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), pushIntent, PendingIntent.FLAG_ONE_SHOT);
        }

        String mStoryId = AppUtils.getFilteredStoryId(storyId);
        mStoryId = mStoryId.equalsIgnoreCase("0") ? "" : mStoryId;

        if (!TextUtils.isEmpty(mStoryId) &&
                !(catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_RASHIFAL)
                        || (catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_BRAND_AND_CATEGORY)
                        || catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_URL)))) {
            try {
                notificationToken = Integer.parseInt(AppUtils.getFilteredStoryId(storyId));
                NotificationStackTable notificationTable = (NotificationStackTable) DatabaseHelper.getInstance(this).getTableObject(NotificationStackTable.TABLE_NAME);
                if (notificationTable.ifExistStoryID(AppUtils.getFilteredStoryId(storyId))) {
                    notificationTable.deleteParticularStoryID(AppUtils.getFilteredStoryId(storyId));
                } else {
                    // increase alert count for app alert section
                    Notificationutil.increaseNotificationsAlertCount(this);
                }
                //Add All Notification Stack Item Into DataBase
                int videoFlag = 0;
                Notificationutil.addDainikNotificationStackItemIntoDatabase(this, message, storyId, imageUrl, channelId, videoFlag, catId);
            } catch (Exception e) {

            }
        } else if (catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_BRAND_AND_CATEGORY)) {
            notificationToken = Constants.NOTIFICATION_CAT_CATEGORY;
        } else if (catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_URL)) {
            notificationToken = Constants.NOTIFICATION_CAT_ID_URL;
        }
        Bitmap message_bitmap;
        if (!TextUtils.isEmpty(imageUrl)) {
            // For Rashifal
            if (imageUrl.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_RASHIFAL)) {
                message_bitmap = Notificationutil.getRashiBitmap(this, storyId);
                generateNotification(message, message_bitmap, pendingIntent, title, actionText, color, false, catId);

            } else {
                ImageRequest imageRequest = new ImageRequest(imageUrl, bitmap -> {
                    generateNotification(message, bitmap, pendingIntent, title, actionText, color, false, catId);
                }, 0, 0, ImageView.ScaleType.CENTER_CROP,
                        Bitmap.Config.RGB_565, error -> {
                    generateNotification(message, null, pendingIntent, title, actionText, color, false, catId);
                });
                imageRequest.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyNetworkSingleton.getInstance(this).addToRequestQueue(imageRequest);
            }

        } else {
            message_bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.app_icon);
            generateNotification(message, null, pendingIntent, title, actionText, color, false, catId);
        }
    }

    private void generateNotification(String message, Bitmap downloadedBitmap, PendingIntent pendingIntent, String title, String actionText, String color, boolean isVideo, String catId) {
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder notificationBuilder;
        String id = getResources().getString(R.string.app_name);
        title = TextUtils.isEmpty(title) ? getResources().getString(R.string.app_name) : title;
        notificationBuilder = new NotificationCompat.Builder(this, id)
                .setContentIntent(pendingIntent)
                .setWhen(when)
                .setSmallIcon(Notificationutil.getAppIcon())
                .setContentTitle(title)
                .setContentText(message)
                .setDeleteIntent(getDeleteIntent())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setVisibility(lockVisibility == 1 ? NotificationCompat.VISIBILITY_PUBLIC : NotificationCompat.VISIBILITY_SECRET);
        if (!TextUtils.isEmpty(color)) {
            notificationBuilder
                    .setColor(AppUtils.getThemeColor(this, color));
        }

        int count = AppPreferences.getInstance(this).getIntValue("notificationShowLimit", 0);
        if (count >= 48) {
            count = 0;
        }
        count += 1;

        switch (notificationType) {
            case FirebaseNotificationConstants.PushType.PUSH_TYPE_SMALL:
                Notificationutil.generateCustomNotification(this, notificationBuilder, downloadedBitmap, message, isVideo);
                break;
            case FirebaseNotificationConstants.PushType.PUSH_TYPE_BIG:
                Notificationutil.generateCustomBigSizeNotification(this, notificationBuilder, downloadedBitmap, message, isVideo);
                break;
            case FirebaseNotificationConstants.PushType.PUSH_TYPE_MULTILINE:
                Notificationutil.generateCustomMultilineNotification(this, notificationBuilder, downloadedBitmap, message, isVideo);
                break;
            case FirebaseNotificationConstants.PushType.PUSH_TYPE_DEFAULT:
                Notificationutil.generateDefaultNotification(notificationBuilder, downloadedBitmap);
                setPushAction(actionText, notificationBuilder, getActionIntent(pendingIntent, count));
                break;
            case FirebaseNotificationConstants.PushType.PUSH_TYPE_DEFAULT_BIG:
                Notificationutil.generateDefaultBigImageNotification(notificationBuilder, downloadedBitmap);
                setPushAction(actionText, notificationBuilder, getActionIntent(pendingIntent, count));
                break;
            case FirebaseNotificationConstants.PushType.PUSH_TYPE_DEFAULT_MULTILINE:
                Notificationutil.generateDefaultMultiLineNotification(notificationBuilder, downloadedBitmap, message);
                setPushAction(actionText, notificationBuilder, getActionIntent(pendingIntent, count));
                break;
            case FirebaseNotificationConstants.PushType.NOTIFICATION_TYPE_APPEND:
                Notificationutil.generateStackNotification(this, notificationBuilder, message);
                break;
            default:
                Notificationutil.generateDefaultNotification(notificationBuilder, downloadedBitmap);

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            String name = getResources().getString(R.string.app_name);
            NotificationChannel mChannel = new NotificationChannel(id, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        Notification notification = notificationBuilder.build();

        notification = Notificationutil.setNotificationProperty(this, notification);

        try {
//            if (notificationType == Constants.PushType.NOTIFICATION_TYPE_APPEND) {
//                notificationManager.notify(Constants.NOTIFICATION_APPEND_ID, notification);
//            } else {
//                notificationManager.notify((int) notificationToken, notification);


            NotificationManager notificationManager1 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager1.cancel(count);

            AppPreferences.getInstance(this).setIntValue("notificationShowLimit", count);
//            Systr.println("NOTIFICATION COUNT : " + count);
            notificationManager.notify(count, notification);
//            }
        } catch (Exception e) {
        }
    }

    private PendingIntent getDeleteIntent() {
        Intent intent = new Intent(this, NotificationReceiver.class);
        intent.setAction("com.ak.ta.divya.bhaskar.activity");
        intent.putExtra("type", "deletion");
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private PendingIntent getActionIntent(PendingIntent pendingIntent, int notificationId) {
        Intent intent = new Intent(this, NotificationReceiver.class);
        intent.setAction("com.ak.ta.divya.bhaskar.activity");
        intent.putExtra("type", "action");
        intent.putExtra(Intent.EXTRA_INTENT, pendingIntent);
        intent.putExtra("id", notificationId);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private void setPushAction(String actionText, NotificationCompat.Builder notificationBuilder, PendingIntent pendingIntent) {
        if (!TextUtils.isEmpty(actionText)) {
            NotificationCompat.Action action = new NotificationCompat.Action(0, actionText, pendingIntent);
            notificationBuilder.addAction(action);

        }
    }


//    private void sendUserActive() {
//        long lastArticleTime = AppPreferences.getInstance(this).getLongValue(QuickPreferences.ActiveUser.ACTIVE_USER_NOTIFY, (long) 0);
//        long currentTime = System.currentTimeMillis();
//        if (AppUtils.daysBetween(currentTime, lastArticleTime) != 0) {
//            String gaClientID = AppPreferences.getInstance(this).getStringValue(QuickPreferences.ActiveUser.GA_CLIENT_ID, "");
//            if (gaClientID != null && !gaClientID.equalsIgnoreCase(""))
//                BackgroundRequest.activeUser(this, Constants.KeyPair.KEY_NOTIFY, gaClientID);
//        }
//    }
}
