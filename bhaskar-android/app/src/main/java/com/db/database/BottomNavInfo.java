package com.db.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class BottomNavInfo implements Serializable {

    public BottomNavInfo() {
    }

    @Ignore
    public BottomNavInfo(String id) {
        this.id = id;
    }

    public BottomNavInfo(BottomNavInfo bottomNavInfo) {
        this.id = bottomNavInfo.id;
        this.name = bottomNavInfo.name;
        this.action = bottomNavInfo.action;
        this.icon = bottomNavInfo.icon;
        this.iconActive = bottomNavInfo.iconActive;
        this.isNew = bottomNavInfo.isNew;
        this.gdprBlock = bottomNavInfo.gdprBlock;
        this.bottomType = bottomNavInfo.bottomType;
    }

    @Ignore
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BottomNavInfo that = (BottomNavInfo) o;
        return id.equals(that.id);
    }

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public int serial;

    @SerializedName("id")
    @ColumnInfo(name = "id")
    @NonNull
    public String id;

    @SerializedName("name")
    @ColumnInfo(name = "name")
    public String name;

    @SerializedName("action")
    @ColumnInfo(name = "action")
    public String action;

    @SerializedName("icon")
    @ColumnInfo(name = "icon")
    public String icon;

    @SerializedName("icon_active")
    @ColumnInfo(name = "icon_active")
    public String iconActive;

    @SerializedName("is_new")
    @ColumnInfo(name = "bottom_new")
    public String isNew;

    @SerializedName("gdpr_block")
    @ColumnInfo(name = "gdpr_block")
    public int gdprBlock = 0;

    @ColumnInfo(name = "bottom_type")
    public int bottomType;
}
