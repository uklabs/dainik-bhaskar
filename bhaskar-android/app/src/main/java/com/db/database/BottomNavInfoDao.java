package com.db.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface BottomNavInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(BottomNavInfo task);

    @Query("SELECT * FROM bottomnavinfo where `action` IN (:catAllowAction) AND bottom_type = (:type) AND gdpr_block = '0'")
    List<BottomNavInfo> getBottomNavListGDPR(String[] catAllowAction, int type);

    @Query("SELECT * FROM bottomnavinfo where `action` IN (:catAllowAction) AND bottom_type = (:type)")
    List<BottomNavInfo> getBottomNavList(String[] catAllowAction, int type);

    @Query("DELETE FROM bottomnavinfo where bottom_type = (:type)")
    void deleteAll(int type);
}
