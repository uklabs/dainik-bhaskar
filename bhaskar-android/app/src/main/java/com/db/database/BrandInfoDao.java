package com.db.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.db.data.models.BrandInfo;

import java.util.List;

@Dao
public interface BrandInfoDao {

    @Query("SELECT * FROM brandinfo")
    List<BrandInfo> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(BrandInfo task);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<BrandInfo> value);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(BrandInfo... value);

    @Delete
    void delete(BrandInfo task);

    @Update
    void update(BrandInfo task);

//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND `action` != :actionNotAllow  AND brand_nav != '2' AND brand_header != '2' AND gdpr_block = '0'")
//    List<BrandInfo> getBrandListGDPR(String actionNotAllow, String[] allowAction);
//
//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND `action` != :actionNotAllow AND brand_nav != '2' AND brand_header != '2'")
//    List<BrandInfo> getBrandList(String actionNotAllow, String[] allowAction);

    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND gdpr_block = '0'")
    List<BrandInfo> getBrandListGDPR(String[] allowAction);

    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction)")
    List<BrandInfo> getBrandList(String[] allowAction);

//    @Query("SELECT COUNT(*) FROM brandinfo where channel_slno = (:channelSlNo) AND `action` = (:actionName)")
//    int isBrandActionAvailable(String channelSlNo, String actionName);

//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND `action` != (:action) AND brand_header != '0' AND gdpr_block = '0' order by header_pos ASC")
//    List<BrandInfo> getDynamicToolbarBrandListGDPR(String action, String[] allowAction);
//
//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND `action` != (:action) AND brand_header != '0' order by header_pos ASC")
//    List<BrandInfo> getDynamicToolbarBrandList(String action, String[] allowAction);

//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND `action` != (:action) AND brand_nav != '0' AND gdpr_block = '0' order by nav_pos ASC")
//    List<BrandInfo> getDynamicBottomBrandListGDPR(String action, String[] allowAction);
//
//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND `action` != (:action) AND brand_nav != '0' order by nav_pos ASC")
//    List<BrandInfo> getDynamicBottomBrandList(String action, String[] allowAction);

//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND `action` = (:actionName) AND gdpr_block = '0'")
//    BrandInfo getBrandBasedOnActionGDPR(String actionName, String[] allowAction);

//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND `action` = (:actionName)")
//    BrandInfo getBrandBasedOnAction(String actionName, String[] allowAction);

//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND id = (:id) AND gdpr_block = '0'")
//    BrandInfo getBrandByIdGDPR(String id, String[] allowAction);

//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND id = (:id)")
//    BrandInfo getBrandById(String id, String[] allowAction);

//    @Query("SELECT * FROM brandinfo where `action` IN (:allowAction) AND channel_slno = (:channelId)")
//    BrandInfo getBrandByChannel(String channelId, String[] allowAction);

//    @Query("SELECT COUNT(*) FROM brandinfo where id = (:brandId)")
//    int isBrandAvailable(String brandId);

    @Query("DELETE FROM brandinfo")
    void deleteAllBrand();
}
