package com.db.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.db.data.models.CategoryInfo;

import java.util.List;

@Dao
public interface CategoryInfoDao {

    @Query("SELECT * FROM categoryinfo")
    List<CategoryInfo> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CategoryInfo task);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<CategoryInfo> value);

    @Delete
    void delete(CategoryInfo task);

    @Update
    void update(CategoryInfo task);

    // All Category List
    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND gdpr_block = 0 AND parent_id = :parentId")
    List<CategoryInfo> getAllCategoryListGDPR(String parentId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND parent_id = :parentId")
    List<CategoryInfo> getAllCategoryList(String parentId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND parent_id = '0' AND gdpr_block = 0")
    List<CategoryInfo> getHambergerCategoryListGDPR(String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND parent_id = '0'")
    List<CategoryInfo> getHambergerCategoryList(String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND parent_id = (:parentId) AND gdpr_block = 0")
    List<CategoryInfo> getHambergerSubCategoryListGDPR(String parentId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND parent_id = (:parentId)")
    List<CategoryInfo> getHambergerSubCategoryList(String parentId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND parent_id = (:parentId) AND gdpr_block = 0")
    List<CategoryInfo> getTabCategoryListGDPR(String parentId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND parent_id = (:parendId)")
    List<CategoryInfo> getTabCategoryList(String parendId, String[] catAllowAction);

    // Category List according to parent id
    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND parent_id = (:parentId) AND gdpr_block = 0")
    List<CategoryInfo> getCategoryListGDPR(String parentId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND parent_id = (:parendId)")
    List<CategoryInfo> getCategoryList(String parendId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND id = (:catId) AND gdpr_block = 0")
    CategoryInfo getMoreCategoryListGDPR(String catId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND id = (:catId)")
    CategoryInfo getMoreCategoryList(String catId, String[] catAllowAction);

    // Brand Menu Category
    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction)")
    CategoryInfo getMenuFeedCategory(String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND gdpr_block = 0")
    CategoryInfo getMenuFeedCategoryGDPR(String[] catAllowAction);

    // Get category info by id and brand id
    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND id = :catId")
    CategoryInfo getCategoryById(String catId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND id = (:catId)")
    CategoryInfo getCategoryByIdAndBrand(String catId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND gdpr_block = 0 AND `action`=(:action)")
    CategoryInfo getCategoryByActionGDPR(String action, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND `action`=(:action)")
    CategoryInfo getCategoryByAction(String action, String[] catAllowAction);

    // Category Available according to id or action
    @Query("SELECT COUNT(*) FROM categoryinfo where `action` = (:actionName)")
    int isCategoryActionAvailable(String actionName);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND id = (:catId) AND gdpr_block = 0")
    int isCategoryIdAvailableGDPR(String catId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND id = (:catId)")
    int isCategoryIdAvailable(String catId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND id = (:catId) AND parent_id = '0' AND gdpr_block = 0")
    int isCategoryIdAvailableForNewGDPR(String catId, String[] catAllowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:catAllowAction) AND id = (:catId) AND parent_id = '0'")
    int isCategoryIdAvailableForNew(String catId, String[] catAllowAction);

    @Query("SELECT id FROM categoryinfo where `action` IN (:catAllowAction) AND cat_new = '1' AND parent_id = '0'")
    List<String> getNewCategoryIDList(String[] catAllowAction);

    @Query("SELECT id FROM categoryinfo where `action` IN (:catAllowAction) AND cat_new = '1' AND parent_id = '0' AND gdpr_block = 0")
    List<String> getNewCategoryIDListGDPR(String[] catAllowAction);

    @Query("DELETE FROM categoryinfo")
    void deleteAllCategory();

    @Query("SELECT * FROM categoryinfo where `action` IN (:allowAction) AND gdpr_block = '0'")
    List<CategoryInfo> getDynamicToolbarCategoryListGDPR(String[] allowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:allowAction)")
    List<CategoryInfo> getDynamicToolbarCategroyList(String[] allowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:allowAction) AND gdpr_block = '0'")
    List<CategoryInfo> getDynamicBottombarCategoryListGDPR(String[] allowAction);

    @Query("SELECT * FROM categoryinfo where `action` IN (:allowAction)")
    List<CategoryInfo> getDynamicBottombarCategoryList(String[] allowAction);

}
