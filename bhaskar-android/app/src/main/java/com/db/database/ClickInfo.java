package com.db.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class ClickInfo implements Serializable {

    @SerializedName("id")
    @NonNull
    @PrimaryKey
    public String id;

    @SerializedName("type")
    @ColumnInfo(name = "type")
    public int type;

    public ClickInfo() {
    }

    @Ignore
    public ClickInfo(String id, int type) {
        this.id = id;
        this.type = type;
    }
}
