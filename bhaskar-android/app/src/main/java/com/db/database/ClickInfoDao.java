package com.db.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ClickInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ClickInfo task);

    @Query("SELECT COUNT(*) FROM clickinfo where id = (:id) AND type = (:type)")
    int getIdExist(String id, int type);

    @Query("SELECT COUNT(*) FROM clickinfo where id IN (:catIdList) AND type = (:type)")
    int getClickedCount(List<String> catIdList, int type);
}
