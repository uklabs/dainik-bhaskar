package com.db.database;

import android.content.Context;

import androidx.annotation.IntDef;

import com.db.InitApplication;
import com.db.data.models.BrandInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.PrimeBrandInfo;
import com.bhaskar.util.Action;

import java.util.ArrayList;
import java.util.List;

public class DatabaseClient {

    private Context mCtx;
    private static DatabaseClient mInstance;

//    private AppDatabase appDatabase;


    private DatabaseClient(Context mCtx) {
        this.mCtx = mCtx;

//        appDatabase = Room.databaseBuilder(mCtx, AppDatabase.class, "DivyaDatabase").allowMainThreadQueries().build();
    }

    public static synchronized DatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(mCtx);
        }
        return mInstance;
    }

//    public AppDatabase getAppDatabase() {
//        return appDatabase;
//    }


    /**********
     * INSERT category
     ******************************/
    public void insertCategory(CategoryInfo[] word) {
//        new InsertCategoryAsyncTask(appDatabase.categoryInfoDao()).execute(word);

//        CategoryInfoDao mAsyncTaskDao = appDatabase.categoryInfoDao();
//        for (CategoryInfo info : word) {
//            mAsyncTaskDao.insert(info);
//
//            if (info.subMenu != null && info.subMenu.size() > 0) {
//                mAsyncTaskDao.insertAll(info.subMenu);
//            }
//        }

        CategoryInfoDao mAsyncTaskDao = InitApplication.getInstance().getAppDatabase().categoryInfoDao();
        for (CategoryInfo info : word) {
            info.gaArticle = info.gaScreen + "-ART";
            info.gaEventLabel = info.gaScreen.toLowerCase();
            mAsyncTaskDao.insert(info);

            if (info.subMenu != null && info.subMenu.size() > 0) {
                insertCat(mAsyncTaskDao, info.subMenu);
            }
        }
    }

    private void insertCat(CategoryInfoDao mAsyncTaskDao, List<CategoryInfo> list) {
        for (CategoryInfo info : list) {
            info.gaArticle = info.gaScreen + "-ART";
            info.gaEventLabel = info.gaScreen.toLowerCase();
            mAsyncTaskDao.insert(info);

            if (info.subMenu != null && info.subMenu.size() > 0) {
                insertCat(mAsyncTaskDao, info.subMenu);
            }
        }
    }

    public void deleteAllCategory() {
        InitApplication.getInstance().getAppDatabase().categoryInfoDao().deleteAllCategory();
    }

    /**********
     * INSERT brand
     ******************************/
    public void insertBrand(BrandInfo[] word) {
//        new InsertBrandAsyncTask(InitApplication.getInstance().getAppDatabase().brandInfoDao()).execute(word);
        for (BrandInfo info : word) {
            InitApplication.getInstance().getAppDatabase().brandInfoDao().insert(info);
        }
    }

    public void insertPrimeBrand(PrimeBrandInfo[] word) {
//        new InsertPrimeBrandAsyncTask(InitApplication.getInstance().getAppDatabase().primeBrandInfoDao()).execute(word);
        for (PrimeBrandInfo info : word) {
            info.gaArticle = info.gaScreen + "-ART";
            info.gaEventLabel = info.gaScreen.toLowerCase();
            InitApplication.getInstance().getAppDatabase().primeBrandInfoDao().insert(info);
        }
    }


    public void deleteAllBrand() {
        InitApplication.getInstance().getAppDatabase().brandInfoDao().deleteAllBrand();
    }

    public void deleteAllPrimeBrand() {
        InitApplication.getInstance().getAppDatabase().primeBrandInfoDao().deleteAllPrimeBrand();
    }

    /***********
     * Get Category
     ****************************/
    public List<CategoryInfo> getAllCategoryList(boolean isBlocked) {
        try {
            if (isBlocked) {
                List<CategoryInfo> all = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getAllCategoryListGDPR("0", Action.allCateg);
                for (CategoryInfo categoryInfo : all) {
                    categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getAllCategoryListGDPR(categoryInfo.id, Action.allCateg));
                    for (CategoryInfo catInfo : categoryInfo.subMenu) {
                        catInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getAllCategoryListGDPR(catInfo.id, Action.allCateg));
                    }
                }
                return all;

            } else {
                List<CategoryInfo> all = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getAllCategoryList("0", Action.allCateg);
                for (CategoryInfo categoryInfo : all) {
                    categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getAllCategoryList(categoryInfo.id, Action.allCateg));
                    for (CategoryInfo catInfo : categoryInfo.subMenu) {
                        catInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getAllCategoryList(catInfo.id, Action.allCateg));
                    }
                }
                return all;
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    public List<CategoryInfo> getSubCategoryList(boolean isBlocked, String parentId) {
        try {
            if (isBlocked) {
                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryListGDPR(parentId, Action.allCateg);
            } else {
                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(parentId, Action.allCateg);
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    public List<CategoryInfo> getMoreCategoryList(boolean isBlocked, String[] idsList) {
        List<CategoryInfo> moreList = new ArrayList<>();
        try {
//            if (isBlocked) {
//                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().getMoreCategoryListGDPR(idsList, Action.allCateg);
//            } else {
//                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().getMoreCategoryList(idsList, Action.allCateg);
//            }
            for (int i = 0; i < idsList.length; i++) {
                if (isBlocked) {
                    moreList.add(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getMoreCategoryListGDPR(idsList[i], Action.allCateg));
                } else {
                    moreList.add(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getMoreCategoryList(idsList[i], Action.allCateg));
                }
            }
        } catch (Exception e) {
        }
        return moreList;
    }

    public List<CategoryInfo> getHambergerCategoryList(boolean isBlocked) {
        try {
            if (isBlocked) {
                List<CategoryInfo> all = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getHambergerCategoryListGDPR(Action.allCateg);
                for (CategoryInfo categoryInfo : all) {
                    categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getHambergerSubCategoryListGDPR(categoryInfo.id, Action.allCateg));
                    for (CategoryInfo catInfo : categoryInfo.subMenu) {
                        catInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getHambergerSubCategoryListGDPR(catInfo.id, Action.allCateg));
                    }
                }
                return all;
            } else {
                List<CategoryInfo> all = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getHambergerCategoryList(Action.allCateg);
                for (CategoryInfo categoryInfo : all) {
                    categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getHambergerSubCategoryList(categoryInfo.id, Action.allCateg));
                    for (CategoryInfo catInfo : categoryInfo.subMenu) {
                        catInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getHambergerSubCategoryList(catInfo.id, Action.allCateg));
                    }
                }
                return all;
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    public List<CategoryInfo> getTabCategoryList(boolean isBlocked) {
        try {
            if (isBlocked) {
                List<CategoryInfo> all = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getTabCategoryListGDPR("0", Action.allCateg);
                for (CategoryInfo categoryInfo : all) {
                    categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getTabCategoryListGDPR(categoryInfo.id, Action.allCateg));
                    for (CategoryInfo catInfo : categoryInfo.subMenu) {
                        catInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getTabCategoryListGDPR(catInfo.id, Action.allCateg));
                    }
                }
                return all;
            } else {
                List<CategoryInfo> all = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getTabCategoryList("0", Action.allCateg);
                for (CategoryInfo categoryInfo : all) {
                    categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getTabCategoryList(categoryInfo.id, Action.allCateg));
                    for (CategoryInfo catInfo : categoryInfo.subMenu) {
                        catInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getTabCategoryList(catInfo.id, Action.allCateg));
                    }
                }
                return all;
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    public CategoryInfo getMenuFeedCategory(boolean isBlocked) {
        try {
            if (isBlocked) {
                CategoryInfo catInfo = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getMenuFeedCategoryGDPR(Action.allCateg);
                if (catInfo != null)
                    catInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryListGDPR(catInfo.id, Action.allCateg));
                return catInfo;
            } else {
                CategoryInfo catInfo = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getMenuFeedCategory(Action.allCateg);
                if (catInfo != null)
                    catInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(catInfo.id, Action.allCateg));
                return catInfo;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public CategoryInfo getCategoryById(String catId) {
        try {
            CategoryInfo categoryInfo = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryById(catId, Action.allCateg);
            if (categoryInfo != null) {
                categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(categoryInfo.id, Action.allCateg));
                for (CategoryInfo info : categoryInfo.subMenu) {
                    info.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(info.id, Action.allCateg));
                }
            }
            return categoryInfo;
        } catch (Exception e) {
        }
        return null;
    }

    public CategoryInfo getCategoryInfoByIdForBottom(String catId) {
        try {
            CategoryInfo categoryInfo = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryById(catId, Action.allBottomCateg);
            if (categoryInfo != null) {
                categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(categoryInfo.id, Action.allBottomCateg));
                for (CategoryInfo info : categoryInfo.subMenu) {
                    info.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(info.id, Action.allBottomCateg));
                }
            }
            return categoryInfo;
        } catch (Exception e) {
        }
        return null;
    }

    public CategoryInfo getCategoryByIdAndBrandId(String catId) {
        try {
            CategoryInfo categoryInfo = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryByIdAndBrand(catId, Action.allCateg);
            if (categoryInfo != null) {
                categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(categoryInfo.id, Action.allCateg));
                for (CategoryInfo info : categoryInfo.subMenu) {
                    info.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(info.id, Action.allCateg));
                }
            }
            return categoryInfo;
        } catch (Exception e) {
        }
        return null;
    }

    public CategoryInfo getSuperParentCategoryById(String catId) {
        try {
            CategoryInfo categoryById = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryById(catId, Action.allCateg);
            if (categoryById != null) {
                if (categoryById.parentId.equalsIgnoreCase("0")) {
                    categoryById.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(categoryById.id, Action.allCateg));
                    for (CategoryInfo info : categoryById.subMenu) {
                        info.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(info.id, Action.allCateg));
                    }
                    return categoryById;
                } else {
                    return getSuperParentCategoryById(categoryById.parentId);
                }
            }
        } catch (Exception e) {
        }

        return null;
    }

    public CategoryInfo getCategoryByAction(boolean isBlock, String action) {
        try {
            if (isBlock) {
                CategoryInfo categoryInfo = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryByActionGDPR(action, Action.allCateg);
                if (categoryInfo != null) {
                    categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(categoryInfo.id, Action.allCateg));
                    for (CategoryInfo info : categoryInfo.subMenu) {
                        info.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(info.id, Action.allCateg));
                    }
                }
                return categoryInfo;
            } else {
                CategoryInfo categoryInfo = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryByAction(action, Action.allCateg);
                if (categoryInfo != null) {
                    categoryInfo.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(categoryInfo.id, Action.allCateg));
                    for (CategoryInfo info : categoryInfo.subMenu) {
                        info.subMenu.addAll(InitApplication.getInstance().getAppDatabase().categoryInfoDao().getCategoryList(info.id, Action.allCateg));
                    }
                }
                return categoryInfo;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public boolean isCategoryActionAvailable(String actionName) {
        try {
            return InitApplication.getInstance().getAppDatabase().categoryInfoDao().isCategoryActionAvailable(actionName) > 0;
        } catch (Exception e) {
        }
        return false;
    }

    public boolean isCategoryIdAvailable(boolean isBlocked, String catId) {
        try {
            if (isBlocked) {
                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().isCategoryIdAvailableGDPR(catId, Action.allCateg) > 0;
            } else {
                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().isCategoryIdAvailable(catId, Action.allCateg) > 0;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean isCategoryIdAvailableForNew(boolean isBlocked, String catId) {
        try {
            if (isBlocked) {
                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().isCategoryIdAvailableForNewGDPR(catId, Action.allCateg) > 0;
            } else {
                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().isCategoryIdAvailableForNew(catId, Action.allCateg) > 0;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public List<CategoryInfo> getDynamicToolbarCategoryList(boolean isBlocked) {
        try {
            if (isBlocked) {
                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().getDynamicToolbarCategoryListGDPR(Action.allCateg);
            } else {
                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().getDynamicToolbarCategroyList(Action.allCateg);
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    public List<CategoryInfo> getDynamicBottombarCategoryList(boolean isBlocked) {
        try {
            if (isBlocked) {
                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().getDynamicBottombarCategoryListGDPR(Action.allCateg);
            } else {
                return InitApplication.getInstance().getAppDatabase().categoryInfoDao().getDynamicBottombarCategoryList(Action.allCateg);
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    /***********
     * Get Brand
     ****************************/
    public List<BrandInfo> getBrandList(boolean isBlocked) {
        try {
            if (isBlocked) {
                return InitApplication.getInstance().getAppDatabase().brandInfoDao().getBrandListGDPR(Action.allCateg);
            } else {
                return InitApplication.getInstance().getAppDatabase().brandInfoDao().getBrandList(Action.allCateg);
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

//    public List<BrandInfo> getDynamicToolbarBrandList(boolean isBlocked, String action) {
//        try {
//            if (isBlocked) {
//                return InitApplication.getInstance().getAppDatabase().brandInfoDao().getDynamicToolbarBrandListGDPR(action, Action.allBrands);
//            } else {
//                return InitApplication.getInstance().getAppDatabase().brandInfoDao().getDynamicToolbarBrandList(action, Action.allBrands);
//            }
//        } catch (Exception e) {
//        }
//        return new ArrayList<>();
//    }
//
//    public List<BrandInfo> getDynamicBottomBrandList(boolean isBlocked, String action) {
//        try {
//            if (isBlocked) {
//                return InitApplication.getInstance().getAppDatabase().brandInfoDao().getDynamicBottomBrandListGDPR(action, Action.allBrands);
//            } else {
//                return InitApplication.getInstance().getAppDatabase().brandInfoDao().getDynamicBottomBrandList(action, Action.allBrands);
//            }
//        } catch (Exception e) {
//        }
//        return new ArrayList<>();
//    }

    public List<PrimeBrandInfo> getPrimeBrandList() {
        try {
            return InitApplication.getInstance().getAppDatabase().primeBrandInfoDao().getPrimeBrandList();
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

//    public boolean isBrandActionAvailable(String channelSlNo, String actionName) {
//        try {
//            return InitApplication.getInstance().getAppDatabase().brandInfoDao().isBrandActionAvailable(channelSlNo, actionName) > 0;
//        } catch (Exception e) {
//        }
//        return false;
//    }

//    public BrandInfo getBrandBasedOnAction(boolean isBlocked, String action) {
//        try {
//            if (isBlocked) {
//                return InitApplication.getInstance().getAppDatabase().brandInfoDao().getBrandBasedOnActionGDPR(action, Action.allBrands);
//            } else {
//                return InitApplication.getInstance().getAppDatabase().brandInfoDao().getBrandBasedOnAction(action, Action.allBrands);
//            }
//        } catch (Exception e) {
//        }
//        return null;
//    }

//    public BrandInfo getBrandById(boolean isBlocked, String id) {
//        try {
//            if (isBlocked) {
//                return InitApplication.getInstance().getAppDatabase().brandInfoDao().getBrandByIdGDPR(id, Action.allBrands);
//            } else {
//                return InitApplication.getInstance().getAppDatabase().brandInfoDao().getBrandById(id, Action.allBrands);
//            }
//        } catch (Exception e) {
//        }
//        return null;
//    }

//    public BrandInfo getBrandByChannel(String channelId) {
//        try {
//            return InitApplication.getInstance().getAppDatabase().brandInfoDao().getBrandByChannel(channelId, Action.allBrands);
//        } catch (Exception e) {
//        }
//        return null;
//    }

//    public boolean isBrandAvailable(String brandId) {
//        try {
//            return InitApplication.getInstance().getAppDatabase().brandInfoDao().isBrandAvailable(brandId) > 0;
//        } catch (Exception e) {
//        }
//        return false;
//    }


    /******
     * Click information
     * *****/
    public void insertClickInfo(String id, int type) {
        try {
            InitApplication.getInstance().getAppDatabase().clickInfoDao().insert(new ClickInfo(id, type));
        } catch (Exception e) {
        }
    }

    public boolean isID_Clicked(String id, int type) {
        try {
            return InitApplication.getInstance().getAppDatabase().clickInfoDao().getIdExist(id, type) > 0;
        } catch (Exception e) {
        }
        return false;
    }

    public int newCategoryCount(boolean isBlock) {
        try {
            List<String> catIdList = null;
            if (isBlock) {
                catIdList = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getNewCategoryIDListGDPR(Action.allCateg);
            } else {
                catIdList = InitApplication.getInstance().getAppDatabase().categoryInfoDao().getNewCategoryIDList(Action.allCateg);
            }
            int clickedCount = InitApplication.getInstance().getAppDatabase().clickInfoDao().getClickedCount(catIdList, DbConstant.CATEGORY_TYPE);
            return catIdList.size() - clickedCount;
        } catch (Exception e) {
        }

        return 0;
    }

    /******
     * Tab Info
     * *************/

    public void insertTab(TabInfo[] word) {
        for (int i = 0; i < word.length; i++) {
            InitApplication.getInstance().getAppDatabase().tabInfoDao().insert(word[i]);
        }
    }

    public void deleteAllTab() {
        InitApplication.getInstance().getAppDatabase().tabInfoDao().deleteAllTab();
    }

    public List<TabInfo> getTabList(boolean isBlocked) {
        try {
            if (isBlocked) {
                return InitApplication.getInstance().getAppDatabase().tabInfoDao().getTabListGDPR(Action.allCateg,Action.outsideOpenCateg);
            } else {
                return InitApplication.getInstance().getAppDatabase().tabInfoDao().getTabList(Action.allCateg,Action.outsideOpenCateg);
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    /******
     * Header Info
     * *************/

    public void insertHeaderNav(HeaderNavInfo[] word) {
        for (int i = 0; i < word.length; i++) {
            InitApplication.getInstance().getAppDatabase().headerNavInfoDao().insert(word[i]);
        }
    }

    public void deleteAllHeaderNav() {
        InitApplication.getInstance().getAppDatabase().headerNavInfoDao().deleteAll();
    }

    public List<HeaderNavInfo> getHeaderNavList(boolean isBlocked) {
        try {
            if (isBlocked) {
                return InitApplication.getInstance().getAppDatabase().headerNavInfoDao().getHeaderNavListGDPR(Action.allCateg);
            } else {
                return InitApplication.getInstance().getAppDatabase().headerNavInfoDao().getHeaderNavList(Action.allCateg);
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }


    /******
     * Bottom Info
     * *************/

    public void insertBottomNav(BottomNavInfo[] word) {
        for (BottomNavInfo info : word) {
//            info.gaEventLabel = info.gaScreen.toLowerCase();
            InitApplication.getInstance().getAppDatabase().bottomNavInfoDao().insert(info);
        }
    }

    public void deleteAllBottomNav() {
        InitApplication.getInstance().getAppDatabase().bottomNavInfoDao().deleteAll(0);
    }

    public void insertBottomNavMore(BottomNavInfo[] word) {
        for (BottomNavInfo info : word) {
            info.bottomType = 1;
//            info.gaEventLabel = info.gaScreen.toLowerCase();
            InitApplication.getInstance().getAppDatabase().bottomNavInfoDao().insert(info);
        }
    }

    public void deleteAllBottomNavMore() {
        InitApplication.getInstance().getAppDatabase().bottomNavInfoDao().deleteAll(1);
    }

    public List<BottomNavInfo> getBottomNavList(boolean isBlocked) {
        try {
            if (isBlocked) {
                return InitApplication.getInstance().getAppDatabase().bottomNavInfoDao().getBottomNavListGDPR(Action.allBottomCateg, 0);
            } else {
                return InitApplication.getInstance().getAppDatabase().bottomNavInfoDao().getBottomNavList(Action.allBottomCateg, 0);
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    public List<BottomNavInfo> getBottomNavMoreList(boolean isBlocked) {
        try {
            if (isBlocked) {
                return InitApplication.getInstance().getAppDatabase().bottomNavInfoDao().getBottomNavListGDPR(Action.allCateg, 1);
            } else {
                return InitApplication.getInstance().getAppDatabase().bottomNavInfoDao().getBottomNavList(Action.allCateg, 1);
            }
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    /*****
     * DB constant
     * *****************/
    @IntDef({DbConstant.BRAND_TYPE, DbConstant.CATEGORY_TYPE})
    public @interface DbConstant {
        int BRAND_TYPE = 0;
        int CATEGORY_TYPE = 1;
    }
}
