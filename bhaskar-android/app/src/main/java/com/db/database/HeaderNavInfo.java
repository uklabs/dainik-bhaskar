package com.db.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class HeaderNavInfo implements Serializable {

    public HeaderNavInfo() {
    }

    @Ignore
    public HeaderNavInfo(String id) {
        this.id = id;
    }

    @Ignore
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeaderNavInfo that = (HeaderNavInfo) o;
        return id.equals(that.id);
    }

    @SerializedName("id")
    @NonNull
    @PrimaryKey
    public String id;

    @SerializedName("name")
    @ColumnInfo(name = "name")
    public String name;

    @SerializedName("action")
    @ColumnInfo(name = "action")
    public String action;

    @SerializedName("icon")
    @ColumnInfo(name = "icon")
    public String icon;

    @SerializedName("is_new")
    @ColumnInfo(name = "header_new")
    public String isNew;

    @SerializedName("gdpr_block")
    @ColumnInfo(name = "gdpr_block")
    public int gdprBlock = 0;
}
