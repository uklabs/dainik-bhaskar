package com.db.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface HeaderNavInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(HeaderNavInfo task);

    @Query("SELECT * FROM headernavinfo where `action` IN (:catAllowAction) AND gdpr_block = '0'")
    List<HeaderNavInfo> getHeaderNavListGDPR(String[] catAllowAction);

    @Query("SELECT * FROM headernavinfo where `action` IN (:catAllowAction)")
    List<HeaderNavInfo> getHeaderNavList(String[] catAllowAction);

    @Query("DELETE FROM headernavinfo")
    void deleteAll();
}
