package com.db.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.db.data.models.PrimeBrandInfo;

import java.util.List;

@Dao
public interface PrimeBrandInfoDao {

    @Query("SELECT * FROM primebrandinfo")
    List<PrimeBrandInfo> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PrimeBrandInfo task);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<PrimeBrandInfo> value);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(PrimeBrandInfo... value);

    @Delete
    void delete(PrimeBrandInfo task);

    @Update
    void update(PrimeBrandInfo task);

    @Query("SELECT * FROM primebrandinfo")
    List<PrimeBrandInfo> getPrimeBrandList();

    @Query("DELETE FROM primebrandinfo")
    void deleteAllPrimeBrand();
}
