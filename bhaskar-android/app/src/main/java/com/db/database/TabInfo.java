package com.db.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class TabInfo implements Serializable {

    public TabInfo() {
    }

    @Ignore
    public TabInfo(String id) {
        this.id = id;
    }

    @Ignore
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TabInfo that = (TabInfo) o;
        return id.equals(that.id);
    }

    @SerializedName("id")
    @NonNull
    @PrimaryKey
    public String id;

    @SerializedName("name")
    @ColumnInfo(name = "name")
    public String name;

    @SerializedName("action")
    @ColumnInfo(name = "action")
    public String action;

    @SerializedName("gdpr_block")
    @ColumnInfo(name = "gdpr_block")
    public int gdprBlock = 0;
}
