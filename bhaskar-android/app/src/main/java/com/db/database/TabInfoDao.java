package com.db.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TabInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TabInfo task);

    @Query("SELECT * FROM tabinfo where `action` IN (:catAllowAction) AND gdpr_block = '0' AND `action` NOT IN (:outsidecatAllowAction)")
    List<TabInfo> getTabListGDPR(String[] catAllowAction,String[] outsidecatAllowAction);

    @Query("SELECT * FROM tabinfo where `action` IN (:catAllowAction) AND `action` NOT IN (:outsidecatAllowAction)")
    List<TabInfo> getTabList(String[] catAllowAction,String[] outsidecatAllowAction);

    @Query("DELETE FROM tabinfo")
    void deleteAllTab();
}
