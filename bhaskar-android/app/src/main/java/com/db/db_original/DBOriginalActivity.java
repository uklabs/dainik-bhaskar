package com.db.db_original;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.bhaskar.util.LoginController;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.home.ActivityUtil;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.util.Action;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.ThemeUtil;
import com.db.views.CustomViewPager;

import java.util.ArrayList;
import java.util.List;

public class DBOriginalActivity extends BaseAppCompatActivity implements View.OnClickListener {
    private CustomViewPager viewPager;
    private TextView tvTitle;
    private CategoryInfo categoryInfo;
    private boolean allGone = false;
    List<Drawable> checkedDrawableList = new ArrayList<>();
    List<Drawable> uncheckedDrawableList = new ArrayList<>();
    TextView eventTV1;
    TextView eventTV2;
    TextView eventTV3;
    TextView eventTV4;
    int size;
    private ImageView ivShareButton;

    public static Intent getIntent(Context context, String color, String title, String gaScreen, String catId) {
        Intent intent = new Intent(context, DBOriginalActivity.class);
        intent.putExtra(Constants.KeyPair.KEY_COLOR, color);
        intent.putExtra(Constants.KeyPair.KEY_TITLE, title);
        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, catId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_dboriginal);
        setToolbar();
        getExtras();
        initViews();
        dynamicHeader();
        setPagerAdapter();
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        String catID = bundle.getString(Constants.KeyPair.KEY_CATEGORY_ID);
        categoryInfo = JsonParser.getInstance().getCategoryInfoBasedOnId(DBOriginalActivity.this, catID);
        tvTitle.setText(categoryInfo.menuName);
        checkAndRefineCategorySubMenuData();
    }

    private void checkAndRefineCategorySubMenuData() {
        ArrayList<CategoryInfo> removedValue = new ArrayList<>();
        for (int index = 0; index < categoryInfo.subMenu.size(); index++) {
            CategoryInfo catInfo = categoryInfo.subMenu.get(index);
            if (!Action.ALL_DB_ORIGINAL_ACTION.contains(catInfo.action)) {
                removedValue.add(catInfo);
            }
        }

        for (int jIndex = 0; jIndex < removedValue.size(); jIndex++) {
            categoryInfo.subMenu.remove(removedValue.get(jIndex));
        }
    }

    private void initViews() {
        viewPager = findViewById(R.id.view_pager_dborg);
        /*stop swipe*/
        viewPager.setPagingEnabled(false);
        eventTV1 = findViewById(R.id.tv_event_1);
        eventTV2 = findViewById(R.id.tv_event_2);
        eventTV3 = findViewById(R.id.tv_event_3);
        eventTV4 = findViewById(R.id.tv_event_4);
    }

    private void setToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(AppUtils.fetchAttributeColor(this, R.attr.toolbarIconColor));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back_black);
        toolbar.setNavigationOnClickListener(v -> handleBackPress());
        getSupportActionBar().setTitle(null);
        tvTitle = findViewById(R.id.toolbar_title);
        ivShareButton = findViewById(R.id.iv_share_button);
        ivShareButton.setVisibility(View.VISIBLE);
        tvTitle.setTextColor(Color.BLACK);
        ivShareButton.setOnClickListener(view -> AppUtils.shareDbOriginal(DBOriginalActivity.this));
    }

    private void setPagerAdapter() {
        viewPager.setAdapter(new DbOriginalPagerAdapter(getSupportFragmentManager()));
        viewPager.setCurrentItem(0);

        sendTracking(0);
    }

    private void dynamicHeader() {
        size = (categoryInfo.subMenu.size() > 4) ? 4 : categoryInfo.subMenu.size();
        if (size == 1) {
            findViewById(R.id.event_parent_ll).setVisibility(View.GONE);
            allGone = true;
        } else if (size == 2) {
            eventTV3.setVisibility(View.GONE);
            eventTV4.setVisibility(View.GONE);
        } else if (size == 3) {
            eventTV4.setVisibility(View.GONE);
        }

        if (!allGone) {
            for (int index = 0; index < size; index++) {
                checkedDrawableList.add(getCheckedDrawableWithColor(AppUtils.getThemeColor(this, categoryInfo.subMenu.get(index).color)));
                uncheckedDrawableList.add(getUncheckedDrawableWithColor(AppUtils.getThemeColor(this, categoryInfo.subMenu.get(index).color)));
                if (index == 0) {
                    eventTV1.setText(categoryInfo.subMenu.get(index).menuName);
                    eventTV1.setBackground(checkedDrawableList.get(index));
                    eventTV1.setTextColor(AppUtils.getThemeColor(this, categoryInfo.subMenu.get(index).color));
                } else if (index == 1) {
                    eventTV2.setText(categoryInfo.subMenu.get(index).menuName);
                    eventTV2.setBackground(uncheckedDrawableList.get(index));
                    eventTV2.setTextColor(ContextCompat.getColor(this, R.color.white));
                } else if (index == 2) {
                    eventTV3.setText(categoryInfo.subMenu.get(index).menuName);
                    eventTV3.setBackground(uncheckedDrawableList.get(index));
                    eventTV3.setTextColor(ContextCompat.getColor(this, R.color.white));
                } else {
                    eventTV4.setText(categoryInfo.subMenu.get(index).menuName);
                    eventTV4.setBackground(uncheckedDrawableList.get(index));
                    eventTV4.setTextColor(ContextCompat.getColor(this, R.color.white));
                }
            }
        }
    }

    private Drawable getCheckedDrawableWithColor(int colorCode) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{8, 8, 8, 8, 8, 8, 8, 8});
        shape.setColor(ContextCompat.getColor(DBOriginalActivity.this, R.color.white));
        shape.setStroke(3, colorCode);
        return shape;
    }

    private Drawable getUncheckedDrawableWithColor(int colorCode) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{8, 8, 8, 8, 8, 8, 8, 8});
        shape.setColor(colorCode);
        shape.setStroke(3, colorCode);
        return shape;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*dynamic View*/
            case R.id.tv_event_1:
                handleClick(0);
                break;
            case R.id.tv_event_2:
                handleClick(1);
                break;
            case R.id.tv_event_3:
                handleClick(2);
                break;
            case R.id.tv_event_4:
                handleClick(3);
                break;
        }
    }

    private void handleClick(int position) {
        if (categoryInfo.subMenu.get(position).action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_DB_ORG_VIDEO)) {
            ActivityUtil.openCategoryAccordingToAction(DBOriginalActivity.this, categoryInfo.subMenu.get(position), "");
        } else {
            viewPager.setCurrentItem(position);
            if (!allGone) {
                TextView eventTextView;
                for (int index = 0; index < size; index++) {
                    if (index == 0) {
                        eventTextView = eventTV1;
                    } else if (index == 1) {
                        eventTextView = eventTV2;
                    } else if (index == 2) {
                        eventTextView = eventTV3;
                    } else {
                        eventTextView = eventTV4;
                    }
                    if (position == index) {
                        eventTextView.setBackground(checkedDrawableList.get(index));
                        eventTextView.setTextColor(AppUtils.getThemeColor(this, categoryInfo.subMenu.get(index).color));
                    } else {
                        eventTextView.setBackground(uncheckedDrawableList.get(index));
                        eventTextView.setTextColor(ContextCompat.getColor(this, R.color.white));
                    }
                }
            }
        }

        sendTracking(position);
    }

    private void handleBackPress() {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
    }

    private void sendTracking(int position) {
        String eventValue = categoryInfo.displayName + "_" + categoryInfo.subMenu.get(position).displayName;

        Tracking.trackGAEvent(DBOriginalActivity.this, InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.SPECIAL_CONTENT, CTConstant.PROP_NAME.CLICKON, eventValue, "");
        CleverTapDB.getInstance(DBOriginalActivity.this).cleverTapTrackEvent(DBOriginalActivity.this, CTConstant.EVENT_NAME.SPECIAL_CONTENT, CTConstant.PROP_NAME.CLICKON, eventValue, LoginController.getUserDataMap());
    }

    private class DbOriginalPagerAdapter extends FragmentStatePagerAdapter {

        DbOriginalPagerAdapter(@NonNull FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            if (categoryInfo.subMenu.size() > 4) {
                return 4;
            } else {
                return categoryInfo.subMenu.size();
            }
        }

        @Override
        public Fragment getItem(int position) {
            return ActivityUtil.getFragmentByCategory(categoryInfo.subMenu.get(position), "", "");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if(requestCode==Constants.REQ_CODE_BOTTOM_NAV && resultCode == RESULT_OK){
            finish();
        }
    }
}
