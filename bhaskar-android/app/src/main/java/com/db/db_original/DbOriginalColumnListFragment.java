package com.db.db_original;


import android.app.Activity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.DBColumnInfo;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class DbOriginalColumnListFragment extends Fragment {


    public DbOriginalColumnListFragment() {
        // Required empty public constructor
    }


    private Activity mContext;
    private DBOriginalColumnAdapter dbOriginalColumnAdapter;

    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    private TextView tvErrorMsg;
    private SwipeRefreshLayout swipeRefreshLayout;

    private boolean isMarginAllowed = false;

    private CategoryInfo categoryInfo;


    public static DbOriginalColumnListFragment newInstance(CategoryInfo categoryInfo) {
        DbOriginalColumnListFragment videoBulletinFragment = new DbOriginalColumnListFragment();
        Bundle argument = new Bundle();
        argument.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        videoBulletinFragment.setArguments(argument);
        return videoBulletinFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_db_original_column_listing, container, false);

        recyclerView = rootView.findViewById(R.id.recycler_view);
        progressBar = rootView.findViewById(R.id.progress_bar);
        tvErrorMsg = rootView.findViewById(R.id.tvErrorMsg);

        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        if (isMarginAllowed) {
            TypedValue tv = new TypedValue();
            int actionBarHeight = 0;
            if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) swipeRefreshLayout.getLayoutParams();
            params.setMargins(0, 0, 0, actionBarHeight);
            swipeRefreshLayout.setLayoutParams(params);
            swipeRefreshLayout.requestLayout();
        }
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));

        dbOriginalColumnAdapter = new DBOriginalColumnAdapter(mContext, categoryInfo);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (dbOriginalColumnAdapter.isHeader(position))
                    return 3;
                return 1;
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(dbOriginalColumnAdapter);

        swipeRefreshLayout.setOnRefreshListener(() -> makeJsonObjectRequest(false));

        makeJsonObjectRequest(true);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        super.onDestroyView();
    }

    private void makeJsonObjectRequest(boolean showProgress) {
        String finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl;
        Systr.println("Feed Url : " + finalFeedURL);

        if (showProgress) {
            progressBar.setVisibility(View.VISIBLE);
        }

        if (NetworkStatus.getInstance().isConnected(mContext)) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            parseFeed(response);
                        } catch (Exception ignored) {
                        }

                        progressBar.setVisibility(View.GONE);
                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }, error -> {
                progressBar.setVisibility(View.GONE);
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                if (error instanceof NoConnectionError || error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
        }
    }

    private void parseFeed(JSONObject response) throws Exception {
        JSONObject feedData = response.getJSONObject("feed");
        JSONArray dailyColumn = feedData.optJSONArray("daily_column");
        JSONObject writerList = feedData.optJSONObject("writer_list");

        ArrayList<DBColumnInfo> dailyColumnListData = new ArrayList<>(Arrays.asList(new Gson().fromJson(dailyColumn.toString(), DBColumnInfo[].class)));
        DBColumnInfo writerListData = new Gson().fromJson(writerList.toString(), DBColumnInfo.class);
        for (int index = 0; index < writerListData.feed.size(); index++) {
            writerListData.feed.get(index).isWriterInfo = true;
        }

        ArrayList<DBColumnInfo> allData = new ArrayList<>(dailyColumnListData);
        allData.add(writerListData);

        dbOriginalColumnAdapter.setData(allData);

        if (dbOriginalColumnAdapter.getItemCount() == 0) {
            tvErrorMsg.setVisibility(View.VISIBLE);
        } else {
            tvErrorMsg.setVisibility(View.GONE);
        }
    }
}
