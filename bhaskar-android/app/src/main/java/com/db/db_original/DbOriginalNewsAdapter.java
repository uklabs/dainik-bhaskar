package com.db.db_original;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.news.WebFullScreenActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.views.viewholder.LoadingViewHolder;
import com.db.views.viewholder.NewsGalleryViewHolder;
import com.db.views.viewholder.RecListViewHolder;

import java.util.ArrayList;

import static com.db.util.Constants.REQ_CODE_BOTTOM_NAV;


public class DbOriginalNewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    /*View Types*/
    private final int VIEW_TYPE_REC_DEFAULT = 0;
    private final int VIEW_TYPE_NEWS_GALLERY = 1;
    private final int VIEW_TYPE_LOADING = 2;

    private ArrayList<Object> mNewsFeed;
    private Activity mContext;
    private FragmentManager fragmentManager;
    private CategoryInfo categoryInfo;
    private int viewPagerHeight;

    public DbOriginalNewsAdapter(Activity contexts, CategoryInfo categoryInfo, FragmentManager fragmentManager, int viewPagerHeight) {
        this.mContext = contexts;
        this.fragmentManager = fragmentManager;
        this.viewPagerHeight = viewPagerHeight;
        this.mNewsFeed = new ArrayList<>();
        this.categoryInfo = categoryInfo;
    }

    public void setData(ArrayList<NewsListInfo> newsFeed) {
        this.mNewsFeed.clear();
        this.mNewsFeed.addAll(newsFeed);
    }

    public void addItem() {
        mNewsFeed.add(null);
        notifyDataSetChanged();
    }

    public void removeItem() {
        if (mNewsFeed.size() > 0 && mNewsFeed.get(mNewsFeed.size() - 1) == null) {
            mNewsFeed.remove(mNewsFeed.size() - 1);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_REC_DEFAULT:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_rec_item, parent, false));
            case VIEW_TYPE_NEWS_GALLERY:
                NewsGalleryViewHolder newsGalleryViewHolder = new NewsGalleryViewHolder(inflater.inflate(R.layout.recyclerlist_item_news_gallery, parent, false), mContext, fragmentManager, categoryInfo, viewPagerHeight);
                newsGalleryViewHolder.setIsRecyclable(false);
                return newsGalleryViewHolder;
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_REC_DEFAULT:
                bindRecView(holder);
                break;

            case VIEW_TYPE_LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
                loadingViewHolder.progressBar.setIndeterminate(true);
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
                break;

            case VIEW_TYPE_NEWS_GALLERY:
                NewsGalleryViewHolder newsGalleryViewHolder = (NewsGalleryViewHolder) holder;
                ArrayList<NewsListInfo> newsListInfos = new ArrayList<>(((NewsListInfo) mNewsFeed.get(newsGalleryViewHolder.getAdapterPosition())).subStoryList);
                newsGalleryViewHolder.setData(newsListInfos);
                break;
            default:
                break;
        }
    }

    private void bindRecView(RecyclerView.ViewHolder holder) {
        final RecListViewHolder recListViewHolder = (RecListViewHolder) holder;
        final NewsListInfo newsListInfoRecView = (NewsListInfo) mNewsFeed.get(recListViewHolder.getAdapterPosition());
        recListViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(newsListInfoRecView.iitl_title, AppUtils.getInstance().fromHtml(newsListInfoRecView.title), newsListInfoRecView.colorCode));

        /*Exclusive*/
        if (!TextUtils.isEmpty(newsListInfoRecView.exclusive)) {
            recListViewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            recListViewHolder.exclusiveTv.setText(newsListInfoRecView.exclusive);
        } else {
            recListViewHolder.exclusiveLayout.setVisibility(View.GONE);
        }

        /*Image*/
        ImageUtil.setImage(mContext, newsListInfoRecView.image, recListViewHolder.ivThumb, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);

        /*Video Flag*/
        if (newsListInfoRecView.videoFlag == 1) {
            recListViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
            recListViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, newsListInfoRecView.colorCode));
        } else {
            recListViewHolder.imgVideoPlay.setVisibility(View.GONE);
        }

        recListViewHolder.itemView.setOnClickListener(view -> launchArticlePage(newsListInfoRecView, recListViewHolder.getAdapterPosition()));
        recListViewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
            @Override
            public void onBookMark() {
                AppUtils.getInstance().bookmarkClick(mContext, newsListInfoRecView, "", categoryInfo.color, false);
            }

            @Override
            public void onShare() {
                AppUtils.getInstance().shareClick(mContext, newsListInfoRecView.title, newsListInfoRecView.webUrl, newsListInfoRecView.gTrackUrl, false);
            }
        }));
    }

    private void launchArticlePage(NewsListInfo newsListInfo, int position) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WebFullScreenActivity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(categoryInfo.gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : categoryInfo.gaScreen) + newsListInfo.webUrl);
            mContext.startActivity(intent);
        } else {
            mContext.startActivityForResult(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo, position),REQ_CODE_BOTTOM_NAV);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mNewsFeed.get(position) == null) {
            return VIEW_TYPE_LOADING;
        } else {
            Object object = mNewsFeed.get(position);
            NewsListInfo info = (NewsListInfo) object;
            if (info.subStoryList.size() > 0) {
                return VIEW_TYPE_NEWS_GALLERY;
            } else {
                return VIEW_TYPE_REC_DEFAULT;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mNewsFeed == null ? 0 : mNewsFeed.size();
    }


    public NewsListInfo getItemAtPosition(int position) {
        if (position >= 0 && position < mNewsFeed.size() && mNewsFeed.get(position) instanceof NewsListInfo)
            return (NewsListInfo) mNewsFeed.get(position);
        return null;
    }

}