package com.db.db_original;


import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.ads.NativeListAdController2;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.dbvideo.player.BusProvider;
import com.db.divya_new.common.OnMoveTopListener;
import com.bhaskar.view.ui.news.NewsListFragment;
import com.db.listeners.FragmentLifecycle;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DbOriginalNewsListingFragment extends Fragment implements OnFeedFetchFromServerListener, OnMoveTopListener, NativeListAdController2.onAdNotifyListener, FragmentLifecycle {
    private final String TAG = AppConfig.BaseTag + "." + NewsListFragment.class.getSimpleName();

    //HpBig
    InitApplication application;
    Handler handler;
    private ProgressBar progressBar;

    //lazy loading
    public boolean isLoading;
    private int LOAD_MORE_COUNT = 0;
    private int visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
    private int lastVisibleItem, totalItemCount;

    private ArrayList<NewsListInfo> newsInfoList;
    private DbOriginalNewsAdapter dbOriginalNewsAdapter;
    private String finalFeedURL;
    private CategoryInfo categoryInfo;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Integer> trackingList = new ArrayList<>();
    private boolean isShowing = false;


    private int listIndex = 0;
    private Rect scrollBounds;
    private LinearLayoutManager linearLayoutManager;
    private int viewPagerHeight;


    public DbOriginalNewsListingFragment() {
    }

    public static DbOriginalNewsListingFragment newInstance(CategoryInfo categoryInfo) {
        DbOriginalNewsListingFragment fragmentFirst = new DbOriginalNewsListingFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        fragmentFirst.setArguments(bundle);
        return fragmentFirst;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.application = (InitApplication) getActivity().getApplication();
        BusProvider.getInstance().register(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        int deviceWidth = AppUtils.getDeviceWidthAndHeight(getActivity())[0];
        viewPagerHeight = deviceWidth + deviceWidth / 4;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        AppLogs.printErrorLogs(TAG, "" + isVisibleToUser);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_list_fragment_main, container, false);

        progressBar = rootView.findViewById(R.id.progress_bar);
        if (getActivity() != null) {
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
        recyclerView = rootView.findViewById(R.id.recycler_view);
        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        handler = new Handler();

        dbOriginalNewsAdapter = new DbOriginalNewsAdapter(getActivity(), categoryInfo, getFragmentManager(), viewPagerHeight);
        recyclerView.setAdapter(dbOriginalNewsAdapter);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout();
            }
        }, Constants.DataBaseHit.DEFAULT_TIME_IS_TOKEN);


        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                swipeRefreshLayout();
            } else {
                hideProgress();
                if (isAdded() && getActivity() != null)
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));

            }
        });
        if (MainActivity.tabReClicked)
            recyclerView.scrollToPosition(0);
        return rootView;
    }

    /**
     * Call for refreshing the content by pull to refresh and event when internet has come
     */
    public void swipeRefreshLayout() {

        if (newsInfoList == null) {
            newsInfoList = new ArrayList<>();
        } else {
            newsInfoList.clear();
        }

        listIndex = 0;
        LOAD_MORE_COUNT = 0;
        setApiUrl();
        makeJsonObjectRequest();
    }

    private void setApiUrl() {
        if (categoryInfo.feedUrl.endsWith("1/")) {
            finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG1/";
        } else {
            finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + (LOAD_MORE_COUNT + 1) + "/";
        }
    }


    public void moveToTop() {
        Systr.println("Move to top called");
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.scrollBounds = new Rect();
        this.recyclerView.getHitRect(scrollBounds);
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            LOAD_MORE_COUNT++;
                            onLoadMore();
                        }
                    }
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }
        });
    }

    @Override
    public void onNotifyAd() {
        if (dbOriginalNewsAdapter != null) {
            dbOriginalNewsAdapter.notifyDataSetChanged();
        }
    }

    public void setFragmentShow() {
        isShowing = true;
        sendTracking();
    }

    private void sendTracking() {
        // Tracking
        if (isShowing) {
            if (categoryInfo != null && trackingList != null) {
                while (trackingList.size() > 0) {
                    int value = trackingList.get(0) + (categoryInfo.feedUrl.endsWith("1/") ? 1 : 0);
                    trackingList.remove(0);

                    if (value > 1) {
                        value = value - 1;
                        String ga_action = categoryInfo.displayName;
                        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_DEPTH, ga_action, AppFlyerConst.GALabel.PG + value, campaign);
                        Systr.println("GA Events : " + AppFlyerConst.GACategory.SCROLL_DEPTH + ga_action + AppFlyerConst.GALabel.PG + value + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));
                    }
                }
            }
        }
    }

    private void onLoadMore() {
        isLoading = true;
        recyclerView.post(() -> {
            if (dbOriginalNewsAdapter != null)
                dbOriginalNewsAdapter.addItem();
        });
        new Handler().postDelayed(() -> {
            finalFeedURL = AppUtils.getInstance().makeUrlForLoadMore(Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl, LOAD_MORE_COUNT);
            makeJsonObjectRequest();
        }, 1000);
    }

    private void makeJsonObjectRequest() {
        showProgressBar();
        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            if (!isLoading && swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            AppLogs.printDebugLogs("Feed URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            if (response != null) {
                                if (!Constants.singleSessionCategoryIdList.contains(categoryInfo.id)) {
                                    Constants.singleSessionCategoryIdList.add(categoryInfo.id);
                                }
                                parseJsonObjectFeedList(response);

                                if (LOAD_MORE_COUNT > 0) {
                                    removeLoadingFromBottom();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, error -> {

                hideProgress();

                if (LOAD_MORE_COUNT > 0) {
                    removeLoadingFromBottom();
                }

                Activity activity = getActivity();
                if (activity != null && isAdded()) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
        } else {
            if (isAdded() && getActivity() != null)
                AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));

            hideProgress();
        }
    }


    private void removeLoadingFromBottom() {
        if (dbOriginalNewsAdapter != null)
            dbOriginalNewsAdapter.removeItem();
    }

    private void parseJsonObjectFeedList(JSONObject response) throws JSONException {
        if (newsInfoList == null) {
            newsInfoList = new ArrayList<>();
        }

        JSONObject jsonObject = response.optJSONObject("data");
        JSONArray newsListJSON = jsonObject.optJSONArray("news_list");


        if (newsListJSON.length() > 0) {
            List<NewsListInfo> newsListInfos = new ArrayList<>(Arrays.asList(new Gson().fromJson(newsListJSON.toString(), NewsListInfo[].class)));
            for (NewsListInfo newsInfo : newsListInfos) {
                listIndex += 1;
                newsInfo.listIndex = listIndex;
            }

            if (LOAD_MORE_COUNT == 0) {
                if (!categoryInfo.feedUrl.endsWith("1/"))
                    LOAD_MORE_COUNT++;

                newsInfoList.clear();
                JSONArray newsGalleryJSON = jsonObject.optJSONArray("news_gallery");
                ArrayList<NewsListInfo> newsGalleryList = new ArrayList<>(Arrays.asList(new Gson().fromJson(newsGalleryJSON.toString(), NewsListInfo[].class)));
                newsInfoList.add(0, new NewsListInfo(newsGalleryList));
            } else {
                removeLoadingFromBottom();
                visibleThreshold += newsInfoList.size();
            }
            newsInfoList.addAll(newsListInfos);

        } else {
            if (LOAD_MORE_COUNT > 0) {
                LOAD_MORE_COUNT--;
                removeLoadingFromBottom();
            }
        }


        dbOriginalNewsAdapter.setData(newsInfoList);
        dbOriginalNewsAdapter.notifyDataSetChanged();
        hideProgress();
        isLoading = false;
        // Tracking
        if (categoryInfo != null && newsListJSON.length() > 0) {
            trackingList.add(LOAD_MORE_COUNT);
            sendTracking();
        }
    }


    @Override
    public void onDataFetch(boolean flag) {
    }

    @Override
    public void onDataFetch(boolean flag, String sectionName) {
    }

    @Override
    public void onDataFetch(boolean flag, int viewType) {

    }

    @Override
    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {

    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Subscribe
    public void NetworkConnectionStateChange(Boolean isConnected) {
        AppLogs.printDebugLogs(TAG + "newsList getMessage1", "" + isConnected);
        if (isConnected && isVisible()) {
            AppLogs.printDebugLogs(TAG + "ConnectionStateChange", "" + "" + isConnected);
            showProgressBar();
            swipeRefreshLayout();
        } else {
            //Internet Not Available
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();
        isShowing = false;

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
        newsInfoList = null;
        dbOriginalNewsAdapter = null;
        finalFeedURL = null;
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }

        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onPauseFragment() {
       /* if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.GA_SCROLL_TEST_ENABLE, false)) {
            int j = linearLayoutManager.findLastCompletelyVisibleItemPosition();
            NewsListInfo itemAtPosition = dbOriginalNewsAdapter.getItemAtPosition(j);
            String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

            Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_TEST, AppFlyerConst.GAAction.LISTING, String.valueOf(itemAtPosition.listIndex), campaign);
        }*/
    }

    @Override
    public void onResumeFragment() {
        isShowing = true;
        sendTracking();
    }
}
