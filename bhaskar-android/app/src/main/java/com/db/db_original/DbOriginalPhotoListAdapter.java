package com.db.db_original;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.AdController2;
import com.db.data.models.CategoryInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.listeners.OnDBVideoClickListener;
import com.db.util.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class DbOriginalPhotoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController2.onAdNotifyListener, NativeListAdController2.onAdNotifyListener {

    public static final int MAIN_LIST = 1;
    private final int VIEW_TYPE_LOADING = 2;


    private boolean isFromHome = false;
    private int showCount;
    private Context mContext;
    private String color;
    private ArrayList<PhotoListItemInfo> photoListItemInfoArrayList;
    private OnDBVideoClickListener onDBVideoClickListener;
    private CategoryInfo categoryInfo;

    public DbOriginalPhotoListAdapter(Context contexts, FragmentManager fragmentManager, OnDBVideoClickListener onDBVideoClickListener) {
        this.onDBVideoClickListener = onDBVideoClickListener;
        this.mContext = contexts;
        this.photoListItemInfoArrayList = new ArrayList<>();
    }

    public void setData(List<PhotoListItemInfo> newsFeed, CategoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
        this.color = categoryInfo.color;
        photoListItemInfoArrayList.clear();
        photoListItemInfoArrayList.addAll(newsFeed);
    }

    public void addItem() {
        photoListItemInfoArrayList.add(null);
        notifyDataSetChanged();
    }

    public void removeItem() {
        if (photoListItemInfoArrayList.size() > 0 && photoListItemInfoArrayList.get(photoListItemInfoArrayList.size() - 1) == null)
            photoListItemInfoArrayList.remove(photoListItemInfoArrayList.size() - 1);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case MAIN_LIST:
                return new DbOriginalPhotoViewHolder(inflater.inflate(R.layout.dborg_photo_list_item, parent, false), categoryInfo);
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item, parent, false));
            default:
                break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case MAIN_LIST:
                final DbOriginalPhotoViewHolder mainViewHolder = (DbOriginalPhotoViewHolder) holder;
                mainViewHolder.setData(photoListItemInfoArrayList.get(mainViewHolder.getAdapterPosition()));
                if (!TextUtils.isEmpty(photoListItemInfoArrayList.get(position).rssTitle)) {
                    mainViewHolder.tvPhotoGalleryTitle.setText(AppUtils.getInstance().getHomeTitle(photoListItemInfoArrayList.get(position).iitlTitle, AppUtils.getInstance().fromHtml(photoListItemInfoArrayList.get(position).rssTitle), color));
                } else {
                    mainViewHolder.tvPhotoGalleryTitle.setVisibility(View.GONE);
                }

                mainViewHolder.itemView.setOnClickListener(v -> {
                    if (onDBVideoClickListener != null) {
                        onDBVideoClickListener.onDbVideoClick(position);
                    }
                });

                break;


            default:
                break;
        }
    }

    @Override
    public void onNotifyAd() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (photoListItemInfoArrayList.get(position) == null) {
            return VIEW_TYPE_LOADING;
        }
        return MAIN_LIST;
    }

    @Override
    public int getItemCount() {
        if (isFromHome) {
            return (showCount > 0 && photoListItemInfoArrayList != null && photoListItemInfoArrayList.size() > 0) ? showCount : photoListItemInfoArrayList == null ? 0 : (photoListItemInfoArrayList.size());
        } else {
            return photoListItemInfoArrayList == null ? 0 : (photoListItemInfoArrayList.size());
        }

    }

    public PhotoListItemInfo getItem(int position) {
        return photoListItemInfoArrayList.get(position);
    }

    public void clear() {
        if (photoListItemInfoArrayList != null)
            photoListItemInfoArrayList.clear();
        notifyDataSetChanged();
    }

    // View Holders
    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        private LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }
}