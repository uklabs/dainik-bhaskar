package com.db.db_original;


import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController2;
import com.db.data.models.CategoryInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.dbvideo.player.BusProvider;
import com.db.listeners.FragmentLifecycle;
import com.db.listeners.OnDBVideoClickListener;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryFragment;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class DbOriginalPhotoListFragment extends Fragment implements FragmentLifecycle, OnDBVideoClickListener {


    public DbOriginalPhotoListFragment() {
        // Required empty public constructor
    }

    private final String TAG = AppConfig.BaseTag + "." + PhotoGalleryFragment.class.getSimpleName();
    public boolean isLoading;
    //HpBig
    //Atf Ads
    private CategoryInfo categoryInfo;
    private DbOriginalPhotoListAdapter photoGalleryListAdapter;
    private int LOAD_MORE_COUNT = 0;
    private int visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
    private int lastVisibleItem, totalItemCount;
    private String finalFeedURL = "";
    private LinearLayoutManager linearLayoutManager;
    RecyclerView photoGalleryRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<PhotoListItemInfo> photoListItemInfoArrayList = null;
    private ProgressBar progressBar;
    private ArrayList<Integer> trackingList = new ArrayList<>();
    private boolean isShowing = false;


    public static DbOriginalPhotoListFragment newInstance(CategoryInfo categoryInfo) {
        DbOriginalPhotoListFragment photoGalleryFragment = new DbOriginalPhotoListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        photoGalleryFragment.setArguments(bundle);
        return photoGalleryFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_db_original_photo_listing, container, false);
        progressBar = rootView.findViewById(R.id.progress_bar);
        if (getActivity() != null) {
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
        photoGalleryRecyclerView = rootView.findViewById(R.id.db_videos_recycler_view);

        linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        photoGalleryRecyclerView.setLayoutManager(linearLayoutManager);

        swipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));

        photoGalleryListAdapter = new DbOriginalPhotoListAdapter(getContext(), getChildFragmentManager(), this);
        photoGalleryRecyclerView.setAdapter(photoGalleryListAdapter);
        photoGalleryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            LOAD_MORE_COUNT++;
                            onLoadMore();
                        }
                    }
                }
            }
        });
        final Handler handler = new Handler();
        handler.postDelayed(() -> swipeRefreshLayout(), Constants.DataBaseHit.DEFAULT_TIME_IS_TOKEN);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                swipeRefreshLayout();
            } else {
                hideProgress();
                if (isAdded() && getActivity() != null)
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            }
        });
        return rootView;
    }

    private Rect scrollBounds;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        scrollBounds = new Rect();
        photoGalleryRecyclerView.getHitRect(scrollBounds);
    }

    private void swipeRefreshLayout() {
        visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
        LOAD_MORE_COUNT = 0;
        setApiUrl();
        makeJsonObjectRequest();
    }

    private void setApiUrl() {
        if (categoryInfo.feedUrl.endsWith("1/")) {
            finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG1/";
        } else {
            finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + (LOAD_MORE_COUNT + 1) + "/";
        }
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }


    private void sendTracking() {
        // Tracking
        if (isShowing) {
            if (categoryInfo != null && trackingList != null) {
                while (trackingList.size() > 0) {
                    int value = trackingList.get(0);
                    trackingList.remove(0);
                    if (value > 1) {
                        value = value - 1;
                        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_DEPTH, categoryInfo.displayName, AppFlyerConst.GALabel.PG + value, campaign);
                        Systr.println("GA Events : " + AppFlyerConst.GACategory.SCROLL_DEPTH + categoryInfo.displayName + AppFlyerConst.GALabel.PG + value + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));
                    }
                }
            }
        }
    }

    private void onLoadMore() {
        isLoading = true;
        photoGalleryRecyclerView.post(() -> photoGalleryListAdapter.addItem());

        new Handler().postDelayed(() -> {
            finalFeedURL = AppUtils.getInstance().makeUrlForLoadMore(Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl, LOAD_MORE_COUNT);
            makeJsonObjectRequest();
        }, 1000);
    }

    private void makeJsonObjectRequest() {
        showProgressBar();
        if (NetworkStatus.getInstance().isConnected(getActivity())) {

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            if (response != null) {
                                parseFeedList(response);
                            }
                        } catch (Exception e) {
                            hideProgress();
                        }
                    }, error -> {
                hideProgress();
                if (LOAD_MORE_COUNT > 0) {
                    LOAD_MORE_COUNT--;
                    removeLoadingFromBottom();
                }
                Activity activity = getActivity();
                if (activity != null && isAdded()) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
            if (isAdded() && getActivity() != null)
                AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            hideProgress();

        }
    }

    private void removeLoadingFromBottom() {
        if (photoGalleryListAdapter != null)
            photoGalleryListAdapter.removeItem();
    }

    private void parseFeedList(JSONObject response) throws JSONException {
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        if (photoListItemInfoArrayList == null) {
            photoListItemInfoArrayList = new ArrayList<>();
        }
        if (resultArray.length() > 0) {
            if (LOAD_MORE_COUNT > 0) {
                visibleThreshold += resultArray.length();
                isLoading = false;
                removeLoadingFromBottom();
            } else {
                photoListItemInfoArrayList.clear();
            }

            List<PhotoListItemInfo> infoList = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), PhotoListItemInfo[].class)));
            photoListItemInfoArrayList.addAll(infoList);

            /*Inserting  PhotoGallery in mdb*/
            if (LOAD_MORE_COUNT == 0) {
                if (!categoryInfo.feedUrl.endsWith("1/"))
                    LOAD_MORE_COUNT++;
            }
            // Tracking
            if (categoryInfo != null) {
                trackingList.add(LOAD_MORE_COUNT);
                sendTracking();
            }
        } else {
            if (LOAD_MORE_COUNT > 0) {
                LOAD_MORE_COUNT--;
                removeLoadingFromBottom();
            }
        }

        photoGalleryListAdapter.setData(photoListItemInfoArrayList, categoryInfo);
        photoGalleryListAdapter.notifyDataSetChanged();
        hideProgress();
    }


    public void moveToTop() {
        if (photoGalleryRecyclerView != null) {
            photoGalleryRecyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onDbVideoClick(int position) {
        if (position >= 0 && position < photoGalleryListAdapter.getItemCount()) {
            Intent photoGalleryIntent = new Intent(getContext(), PhotoGalleryActivity.class);
            photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, photoGalleryListAdapter.getItem(position).rssId);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, photoGalleryListAdapter.getItem(position).gTrackUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_TRACK_URL, photoGalleryListAdapter.getItem(position).trackUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            startActivity(photoGalleryIntent);
        }
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Subscribe
    public void NetworkConnectionStateChange(Boolean isConnected) {
        AppLogs.printDebugLogs(TAG + "photogallary getMessage1", "" + isConnected);
        if (isConnected) {
            AppLogs.printDebugLogs(TAG + "photogallary ConnectionC", "" + isConnected);
            showProgressBar();
            swipeRefreshLayout();
        } else {
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isShowing = false;
    }

    @Override
    public void onDestroyView() {
        AdController2.getInstance(getContext()).destroy();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }

    private void hideProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }

        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {
        isShowing = true;
        sendTracking();
    }
}
