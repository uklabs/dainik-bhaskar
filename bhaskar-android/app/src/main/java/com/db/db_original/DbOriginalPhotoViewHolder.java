package com.db.db_original;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.PhotoDetailInfo;
import com.db.data.models.PhotoListItemInfo;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.shuhart.bubblepagerindicator.BubblePageIndicator;

import java.util.ArrayList;

public class DbOriginalPhotoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private Context mContext;

    TextView tvPhotoGalleryTitle;
    private TextView tvPhotoGallerySubTitle;
    private TextView tvPagerCount;
    private ImageView ivBackArrow;
    private ImageView ivForwardArrow;
    private BubblePageIndicator bubblePageIndicator;
    private ViewPager viewPagerPhoto;
    private CategoryInfo categoryInfo;
    private int selectedPosition;
    private ArrayList<PhotoDetailInfo> listData;
    private PhotoListItemInfo photoListItemInfo;
    private LinearLayout containerPhotoCounter;

    DbOriginalPhotoViewHolder(View itemView, CategoryInfo categoryInfo) {
        super(itemView);
        mContext = itemView.getContext();
        tvPhotoGalleryTitle = itemView.findViewById(R.id.tv_photo_gallery_title);
        tvPhotoGallerySubTitle = itemView.findViewById(R.id.tv_photo_gallery_subtitle);
        tvPagerCount = itemView.findViewById(R.id.tv_pager_count);
        ivBackArrow = itemView.findViewById(R.id.iv_back_arrow);
        ivForwardArrow = itemView.findViewById(R.id.iv_forward_arrow);
        ImageView ivPhotoGradient = itemView.findViewById(R.id.iv_photo_gradient);
        bubblePageIndicator = itemView.findViewById(R.id.pager_indicator);
        viewPagerPhoto = itemView.findViewById(R.id.view_pager_photo);
        containerPhotoCounter = itemView.findViewById(R.id.container_photo_counter);
        this.categoryInfo = categoryInfo;
        ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedPosition = position;
                ivBackArrow.setVisibility(View.VISIBLE);
                ivForwardArrow.setVisibility(View.VISIBLE);
                if (position == 0) {
                    ivBackArrow.setVisibility(View.GONE);
                }
                if (position == listData.size() - 1) {
                    ivForwardArrow.setVisibility(View.GONE);
                }
                tvPagerCount.setText(mContext.getString(R.string.label_pager_count, "" + (selectedPosition + 1), "" + listData.size()));
                if (TextUtils.isEmpty(listData.get(position).rssTitle)) {
                    tvPhotoGallerySubTitle.setVisibility(View.GONE);
                } else {
                    tvPhotoGallerySubTitle.setVisibility(View.VISIBLE);
                    tvPhotoGallerySubTitle.setText(listData.get(position).rssTitle);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
        viewPagerPhoto.addOnPageChangeListener(onPageChangeListener);
        ivBackArrow.setOnClickListener(this::onClick);
        ivForwardArrow.setOnClickListener(this::onClick);
        ivPhotoGradient.setBackground(AppUtils.getDbOrgGradient());

        itemView.setTag(itemView);
    }


    public void setData(PhotoListItemInfo photoListItemInfo) {
        this.photoListItemInfo = photoListItemInfo;
        listData = photoListItemInfo.photos;
        viewPagerPhoto.setAdapter(new PhotoPagerAdapter(LayoutInflater.from(mContext)));
        if (listData != null && listData.size() > 1) {
            bubblePageIndicator.setVisibility(View.VISIBLE);
            bubblePageIndicator.setViewPager(viewPagerPhoto);
            bubblePageIndicator.setFillColor(AppUtils.getThemeColor(mContext, categoryInfo.color));
            ivBackArrow.setVisibility(View.GONE);
            ivForwardArrow.setVisibility(View.VISIBLE);
            containerPhotoCounter.setVisibility(View.VISIBLE);
        } else {
            bubblePageIndicator.setVisibility(View.GONE);
            ivBackArrow.setVisibility(View.GONE);
            ivForwardArrow.setVisibility(View.GONE);
            containerPhotoCounter.setVisibility(View.GONE);

        }
        viewPagerPhoto.setPageTransformer(false, AppUtils.getParallaxPagerTransform(R.id.article_iv, 0, 0.5f));
        tvPagerCount.setText(mContext.getString(R.string.label_pager_count, "" + (selectedPosition + 1), "" + listData.size()));
        if (TextUtils.isEmpty(listData.get(0).rssTitle)) {
            tvPhotoGallerySubTitle.setVisibility(View.GONE);
        } else {
            tvPhotoGallerySubTitle.setVisibility(View.VISIBLE);
            tvPhotoGallerySubTitle.setText(listData.get(0).rssTitle);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back_arrow:
                int lpos = selectedPosition - 1;
                if (lpos >= 0)
                    viewPagerPhoto.setCurrentItem(lpos);
                break;
            case R.id.iv_forward_arrow:
                int rpos = selectedPosition + 1;
                if (rpos < listData.size())
                    viewPagerPhoto.setCurrentItem(rpos);
                break;
        }
    }

    public class PhotoPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;

        PhotoPagerAdapter(@NonNull LayoutInflater layoutInflater) {
            this.layoutInflater = layoutInflater;
        }

        @Override
        public Object instantiateItem(@NonNull ViewGroup collection, int position) {
            PhotoDetailInfo photoDetailInfo = listData.get(position);
            ImageView imageView = (ImageView) layoutInflater.inflate(R.layout.fragment_photo_pager, collection, false);
            try {
                ImageUtil.setImage(mContext, photoDetailInfo.rssImage, imageView, R.drawable.water_mark_news_detail, R.drawable.water_mark_big_news_detail_error);

                imageView.setId(position);
//                imageView.setTag(photoDetailInfo);
                imageView.setOnClickListener(v -> {
                    /*onClick work*/
                    Intent photoGalleryIntent = new Intent(mContext, PhotoGalleryActivity.class);
                    photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_ORGINIAL_PHOTO_GALLERY);
                    photoGalleryIntent.putExtra(Constants.KeyPair.KEY_LIST_INFO, photoListItemInfo);
                    photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, position);
                    photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
                    photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                    photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, photoDetailInfo.gTrackUrl);
                    mContext.startActivity(photoGalleryIntent);
                });
            } catch (Exception e) {
            }
            collection.addView(imageView);
            return imageView;
        }


        @Override
        public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }
    }
}
