package com.db.db_original;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.news.WebFullScreenActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsGalleryFragment extends Fragment {


    private NewsListInfo newsListInfo;
    private int position;
    private CategoryInfo catInfo;

    public NewsGalleryFragment() {
    }

    public static Fragment newInstance(NewsListInfo newsListInfo, int position, CategoryInfo categoryInfo) {
        NewsGalleryFragment newsGalleryFragment = new NewsGalleryFragment();
        Bundle argument = new Bundle();
        argument.putSerializable(Constants.KeyPair.KEY_LIST_INFO, newsListInfo);
        argument.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        argument.putInt(Constants.KeyPair.KEY_POSITION, position);
        newsGalleryFragment.setArguments(argument);
        return newsGalleryFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        newsListInfo = (NewsListInfo) args.getSerializable(Constants.KeyPair.KEY_LIST_INFO);
        catInfo = (CategoryInfo) args.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        position = args.getInt(Constants.KeyPair.KEY_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news_gallery, container, false);
        ImageView ivNewsImage = rootView.findViewById(R.id.iv_news_image);
        ImageView gradientIv = rootView.findViewById(R.id.iv_news_gradient);
        TextView tvSlugTitle = rootView.findViewById(R.id.tv_slug_title);

        tvSlugTitle.setText(AppUtils.getInstance().getHomeTitle(newsListInfo.iitl_title, AppUtils.getInstance().fromHtml(newsListInfo.title), newsListInfo.colorCode, "#FFFFFF"));
//        newsListInfo.image = "https://visuals.dbnewshub.com/bhaskar/cms-assets/2019-08/atm_cards_cloned_victims_lose_lakhs_1523182527.jpg";
        ImageUtil.setImage(getContext(), newsListInfo.image, ivNewsImage, R.drawable.water_mark_big_news_detail_error);
        gradientIv.setBackground(AppUtils.getDbOrgGradient());
        rootView.setOnClickListener(view -> launchArticlePage(newsListInfo));
        return rootView;
    }

    private void launchArticlePage(NewsListInfo newsListInfo) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(getContext(), WebFullScreenActivity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAScreen.WEB_VIEW + "-" + newsListInfo.webUrl);
            getContext().startActivity(intent);
        } else {
            getActivity().startActivityForResult(DivyaCommonArticleActivity.getIntent(getContext(), newsListInfo, catInfo, position),Constants.REQ_CODE_BOTTOM_NAV);
        }
    }

}
