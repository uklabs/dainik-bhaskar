package com.db.db_original;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.ads.NativeListAdController2;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.StoryListInfo;
import com.db.dbvideo.player.BusProvider;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.home.CommonListAdapter;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WriterColumnListActivity extends BaseAppCompatActivity implements OnFeedFetchFromServerListener, OnMoveTopListener, NativeListAdController2.onAdNotifyListener {

    private final String TAG = AppConfig.BaseTag + "." + WriterColumnListActivity.class.getSimpleName();

    //HpBig
    InitApplication application;
    Handler handler;
    private ProgressBar progressBar;

    //lazy loading
    public boolean isLoading;
    private int LOAD_MORE_COUNT = 0;
    private int visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
    private int lastVisibleItem, totalItemCount;

    private ArrayList<NewsListInfo> newsInfoList;
    private CommonListAdapter commonListAdapter;
    private String finalFeedURL;
    private CategoryInfo categoryInfo;
    private StoryListInfo storyListInfo;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Integer> trackingList = new ArrayList<>();
    private boolean isShowing = false;

    private int listIndex = 0;
    private Rect scrollBounds;
    private LinearLayoutManager linearLayoutManager;
    private TextView tvTitle;
    private String mainUrl;

    /*For Story*/
    public static Intent getIntent(Context context, StoryListInfo newsListInfo, CategoryInfo categoryInfo) {
        Intent detailNews = new Intent(context, WriterColumnListActivity.class);
        detailNews.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        detailNews.putExtra(Constants.KeyPair.KEY_LIST_INFO, newsListInfo);
        return detailNews;
    }

    private void setToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(AppUtils.fetchAttributeColor(this, R.attr.toolbarIconColor));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back_black);
        toolbar.setNavigationOnClickListener(v -> handleBackPress());
        getSupportActionBar().setTitle(null);
        tvTitle = findViewById(R.id.toolbar_title);
        tvTitle.setTextColor(Color.BLACK);
    }

    private void handleBackPress() {
        finish();
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            storyListInfo = (StoryListInfo) bundle.getSerializable(Constants.KeyPair.KEY_LIST_INFO);
        }
        tvTitle.setText(storyListInfo.title);
        mainUrl = categoryInfo.detailUrl + storyListInfo.catId + "/1/";
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_writer_column_list);
        setToolbar();
        getExtras();
        this.application = InitApplication.getInstance();
        BusProvider.getInstance().register(this);


        allWork();
    }

    private void allWork() {
        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(WriterColumnListActivity.this, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        recyclerView = findViewById(R.id.recycler_view);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(WriterColumnListActivity.this, R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(WriterColumnListActivity.this, R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(WriterColumnListActivity.this, R.attr.toolbarBackgroundPrimary));
        linearLayoutManager = new LinearLayoutManager(WriterColumnListActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        handler = new Handler();

        commonListAdapter = new CommonListAdapter(WriterColumnListActivity.this, categoryInfo.gaScreen, categoryInfo.color, categoryInfo);
        recyclerView.setAdapter(commonListAdapter);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout();
            }
        }, Constants.DataBaseHit.DEFAULT_TIME_IS_TOKEN);


        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (NetworkStatus.getInstance().isConnected(WriterColumnListActivity.this)) {
                swipeRefreshLayout();
            } else {
                hideProgress();

                AppUtils.getInstance().showCustomToast(WriterColumnListActivity.this, getResources().getString(R.string.no_network_error));

            }
        });
        if (MainActivity.tabReClicked)
            recyclerView.scrollToPosition(0);

        this.scrollBounds = new Rect();
        this.recyclerView.getHitRect(scrollBounds);
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(WriterColumnListActivity.this)) {
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            LOAD_MORE_COUNT++;
                            onLoadMore();
                        }
                    }
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
    }


    /**
     * Call for refreshing the content by pull to refresh and event when internet has come
     */
    public void swipeRefreshLayout() {

        if (newsInfoList == null) {
            newsInfoList = new ArrayList<>();
        } else {
            newsInfoList.clear();
        }

        listIndex = 0;
        LOAD_MORE_COUNT = 0;
        setApiUrl();
        makeJsonObjectRequest();
    }

    private void setApiUrl() {

        if (mainUrl.endsWith("1/")) {
            finalFeedURL = Urls.APP_FEED_BASE_URL + mainUrl + "PG1/";
        } else {
            finalFeedURL = Urls.APP_FEED_BASE_URL + mainUrl + "PG" + (LOAD_MORE_COUNT + 1) + "/";
        }
    }


    public void moveToTop() {
        Systr.println("Move to top called");
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }


    @Override
    public void onNotifyAd() {
        if (commonListAdapter != null) {
            commonListAdapter.notifyDataSetChanged();
        }
    }

    public void setFragmentShow() {
        isShowing = true;
        sendTracking();
    }

    private void sendTracking() {
        // Tracking
        if (isShowing) {
            if (categoryInfo != null && trackingList != null) {
                while (trackingList.size() > 0) {
                    int value = trackingList.get(0) + (categoryInfo.feedUrl.endsWith("1/") ? 1 : 0);
                    trackingList.remove(0);

                    if (value > 1) {
                        value = value - 1;
                        String ga_action = categoryInfo.displayName;
                        String campaign = AppPreferences.getInstance(WriterColumnListActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(WriterColumnListActivity.this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_DEPTH, ga_action, AppFlyerConst.GALabel.PG + value, campaign);
                        Systr.println("GA Events : " + AppFlyerConst.GACategory.SCROLL_DEPTH + ga_action + AppFlyerConst.GALabel.PG + value + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));
                    }
                }
            }
        }
    }

    private void onLoadMore() {
        isLoading = true;
        recyclerView.post(() -> {
            if (commonListAdapter != null)
                commonListAdapter.addItem();
        });
        new Handler().postDelayed(() -> {
            finalFeedURL = AppUtils.getInstance().makeUrlForLoadMore(Urls.APP_FEED_BASE_URL + mainUrl, LOAD_MORE_COUNT);
            makeJsonObjectRequest();
        }, 1000);
    }

    private void makeJsonObjectRequest() {
        if (NetworkStatus.getInstance().isConnected(this)) {
            if (!isLoading && swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            AppLogs.printDebugLogs("Feed URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            if (response != null) {
                                if (!Constants.singleSessionCategoryIdList.contains(categoryInfo.id)) {
                                    Constants.singleSessionCategoryIdList.add(categoryInfo.id);
                                }
                                parseJsonObjectFeedList(response);

                                if (LOAD_MORE_COUNT > 0) {
                                    removeLoadingFromBottom();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, error -> {

                hideProgress();

                if (LOAD_MORE_COUNT > 0) {
                    removeLoadingFromBottom();
                }

                if (error instanceof NoConnectionError || error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(WriterColumnListActivity.this, getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(WriterColumnListActivity.this, getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(WriterColumnListActivity.this).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(WriterColumnListActivity.this, getResources().getString(R.string.no_network_error));

            hideProgress();
        }
    }

    private void removeLoadingFromBottom() {
        if (commonListAdapter != null)
            commonListAdapter.removeItem();
    }


    private void parseJsonObjectFeedList(JSONObject response) throws JSONException {
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);

        if (newsInfoList == null) {
            newsInfoList = new ArrayList<>();
        }

        if (resultArray.length() > 0) {

            if (LOAD_MORE_COUNT == 0) {
                newsInfoList.clear();
            } else {
                removeLoadingFromBottom();
                visibleThreshold += resultArray.length();
            }

            List<NewsListInfo> newsListInfos = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), NewsListInfo[].class)));
            for (NewsListInfo newsInfo : newsListInfos) {
                listIndex += 1;
                newsInfo.listIndex = listIndex;
            }

            newsInfoList.addAll(newsListInfos);

        } else {
            if (LOAD_MORE_COUNT > 0) {
                LOAD_MORE_COUNT--;
                removeLoadingFromBottom();
            }
        }


        commonListAdapter.setData(newsInfoList, categoryInfo.detailUrl);
        commonListAdapter.notifyDataSetChanged();
        hideProgress();
        isLoading = false;
        // Tracking
        if (categoryInfo != null && resultArray != null && resultArray.length() > 0) {
            trackingList.add(LOAD_MORE_COUNT);
            sendTracking();
        }
    }


    @Override
    public void onDataFetch(boolean flag) {
    }

    @Override
    public void onDataFetch(boolean flag, String sectionName) {
    }

    @Override
    public void onDataFetch(boolean flag, int viewType) {

    }

    @Override
    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {

    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Subscribe
    public void NetworkConnectionStateChange(Boolean isConnected) {
        AppLogs.printDebugLogs(TAG + "newsList getMessage1", "" + isConnected);
        if (isConnected) {
            AppLogs.printDebugLogs(TAG + "ConnectionStateChange", "" + "" + isConnected);
            showProgressBar();
            swipeRefreshLayout();
        } else {
            //Internet Not Available
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();
        isShowing = false;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (commonListAdapter != null) {
            commonListAdapter.setNotify();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
        newsInfoList = null;
        commonListAdapter = null;
        finalFeedURL = null;
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }

        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == Constants.REQ_CODE_BOTTOM_NAV && resultCode == RESULT_OK) {
            try {
                setResult(Activity.RESULT_OK);
                finish();
            } catch (Exception ignored) {
            }
        }
    }
}

