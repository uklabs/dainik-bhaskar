package com.db.dbvideo.player;

public interface AdStatusListener {

    int AD_STATUS_PARTIAL_COMPLETE = 1;
    int AD_STATUS_COMPLETED = 2;
    int AD_STATUS_ERROR = 3;

    int getAdStatus();
}
