package com.db.dbvideo.player;

public interface AdsEventConstant {

    String AD_PLAY = "ad_play";
    String AD_ERROR = "ad_errors";
    String AD_SKIP = "ad_skipped";
    String AD_ENDED = "AdEnded";
    String AD_REQUEST = "ad_request";
    String AD_COMPLETED = "ad_completed";
    String AD_CLICK = "ad_click";
    String ALL_ADS_COMPLETED = "ad_completed";

    String AD_EVENT_FIRST_QUARTILE = "First Quartile";
    String AD_EVENT_SECOND_QUARTILE = "Second Quartile";
    String AD_EVENT_THIRD_QUARTILE = "Third Quartile";
}
