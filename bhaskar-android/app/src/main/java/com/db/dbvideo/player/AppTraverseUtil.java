package com.db.dbvideo.player;

import android.text.TextUtils;

import com.bhaskar.util.CommonConstants;

public class AppTraverseUtil {

//    private static final int TRAVERSE_KEY_HOME = 0;
//    private static final int TRAVERSE_KEY_SLIDE_MENU = 1;
//
//    private static final String TRAVERSE_VALUE_HOME = "Home";
//    private static final String TRAVERSE_VALUE_SLIDE_MENU = "Listing";
//    private static final String CLASS_NAME = "AppTraverseUtil";
//
//    private static int traverseKey;

    private static final String TRAVERSE_SEPARATOR = "_";

    private static String language;
    private static String videoTitle;
    private static String storyId;
    private static String sectionSubsectionValue;
    private static String videoSource;
    private static int videoIndex;

//    public static String getAppTraverseSecSubSectionValue() {
//        return sectionSubsectionValue;
//    }
//
//    public static String getNewsTraverseValue() {
//        switch (traverseKey) {
//            case TRAVERSE_KEY_SLIDE_MENU:
//                return TRAVERSE_VALUE_SLIDE_MENU;
//            default:
//            case TRAVERSE_KEY_HOME:
//                return TRAVERSE_VALUE_HOME;
//        }
//    }
//
//    public static void printValue() {
//        String value = getNewsTraverseValue() + TRAVERSE_SEPARATOR + getAppTraverseSecSubSectionValue();
//        Systr.println(CLASS_NAME + " " + value);
//    }
//
//    public static String getValue() {
//        return getNewsTraverseValue() + TRAVERSE_SEPARATOR + getAppTraverseSecSubSectionValue();
//    }

    public static String getVideoSource() {
        return videoSource;
    }

    public static String getScreenNameForVideoTracking() {
        if (TextUtils.isEmpty(language)) {
            language = CommonConstants.EVENT_LABEL;
        }
        StringBuilder stringBuilder = new StringBuilder(language);
        stringBuilder.append(TRAVERSE_SEPARATOR);
        stringBuilder.append(storyId);
        stringBuilder.append(TRAVERSE_SEPARATOR);
        stringBuilder.append(videoTitle);
        stringBuilder.append(TRAVERSE_SEPARATOR);
        stringBuilder.append(sectionSubsectionValue);
        stringBuilder.append(TRAVERSE_SEPARATOR);
        stringBuilder.append(videoSource);
        if (videoIndex > -1) {
            stringBuilder.append(TRAVERSE_SEPARATOR);
            stringBuilder.append(videoIndex);
        }

        return stringBuilder.toString();
    }

    public static void setLanguage(String language) {
        AppTraverseUtil.language = language;
    }

    public static void setVideoTitle(String videoTitle) {
        if (TextUtils.isEmpty(videoTitle)) {
            videoTitle = "V_Title";
        }
        AppTraverseUtil.videoTitle = videoTitle;
    }

    public static void setStoryId(String storyId) {
        AppTraverseUtil.storyId = storyId;
    }

    public static void setBaseSectionSubSectionValue(String nameSlug) {
        sectionSubsectionValue = nameSlug;
    }

    public static void setVideoSource(String videoSource) {
        AppTraverseUtil.videoSource = videoSource;
    }

    public static void setVideoIndex(int videoIndex) {
        AppTraverseUtil.videoIndex = videoIndex;
    }

    public static String getVideoId() {
        return storyId;
    }

    public static String getVideoTitle() {
        return videoTitle;
    }
}
