package com.db.dbvideo.player;

import com.db.util.Systr;

public class BhaskarVideoPlayerTrackerUtil {

//    static final String CATEGORY_VIDEO = "Video_Interaction";
//    private static final String CLASS_NAMES = "BhaskarVideoPlayerTrackerUtil";
//    private static final String CATEGORY_AD = "Ad Event Tracking";
//    static int VALUE = 1;
    private static String screenName = "";
//    private static String ultimaTrackerUrl;
    private static PlayerEventCallBackListener playerEventCallBackListenerReference;

    public static void sendVideoEventTracking(String action) {
        Systr.println("ACTION : " + action);

        if (playerEventCallBackListenerReference == null) {
            return;
        }

        playerEventCallBackListenerReference.sendVideoEvent(action);

        if (action.equalsIgnoreCase(VideoEventConstant.VIDEO_PLAY)) {
            playerEventCallBackListenerReference.onVideoPlay();
            return;
        }
        if (action.equalsIgnoreCase(VideoEventConstant.VIDEO_REPLAY)) {
            playerEventCallBackListenerReference.onVideoReplay();
            return;
        }
        if (action.equalsIgnoreCase(VideoEventConstant.VIDEO_PAUSE)) {
            playerEventCallBackListenerReference.onVideoPause();
            return;
        }
        if (action.equalsIgnoreCase(VideoEventConstant.VIDEO_RESUME)) {
            playerEventCallBackListenerReference.onVideoResume();
            return;
        }
        if (action.equalsIgnoreCase(VideoEventConstant.VIDEO_ERR)) {
            playerEventCallBackListenerReference.onVideoError();
            return;
        }
        if (action.equalsIgnoreCase("10%")) {
            playerEventCallBackListenerReference.onVideoPlay10();
            return;
        }
        if (action.equalsIgnoreCase("20%")) {
            playerEventCallBackListenerReference.onVideoPlay20();
            return;
        }
        if (action.equalsIgnoreCase("25%")) {
            playerEventCallBackListenerReference.onVideoPlay25();
            return;
        }
        if (action.equalsIgnoreCase("30%")) {
            playerEventCallBackListenerReference.onVideoPlay30();
            return;
        }
        if (action.equalsIgnoreCase("40%")) {
            playerEventCallBackListenerReference.onVideoPlay40();
            return;
        }
        if (action.equalsIgnoreCase("50%")) {
            playerEventCallBackListenerReference.onVideoPlay50();
            return;
        }
        if (action.equalsIgnoreCase("60%")) {
            playerEventCallBackListenerReference.onVideoPlay60();
            return;
        }
        if (action.equalsIgnoreCase("70%")) {
            playerEventCallBackListenerReference.onVideoPlay70();
            return;
        }
        if (action.equalsIgnoreCase("75%")) {
            playerEventCallBackListenerReference.onVideoPlay75();
            return;
        }
        if (action.equalsIgnoreCase("80%")) {
            playerEventCallBackListenerReference.onVideoPlay80();
            return;
        }
        if (action.equalsIgnoreCase("90%")) {
            playerEventCallBackListenerReference.onVideoPlay90();
            return;
        }
        if (action.equalsIgnoreCase("100%")) {
            playerEventCallBackListenerReference.onVideoPlay100();
            return;
        }
    }

    public static void setScreenName(String screenName) {
        BhaskarVideoPlayerTrackerUtil.screenName = screenName;
    }

    public static void sendAdEventTracking(String action) {
        if (playerEventCallBackListenerReference == null) {
            return;
        }
        playerEventCallBackListenerReference.sendAdEvent(action);
    }


    public static void setPlayerEventCallBackReference(PlayerEventCallBackListener playerEventCallBackListenerReference) {
        BhaskarVideoPlayerTrackerUtil.playerEventCallBackListenerReference = playerEventCallBackListenerReference;
    }
}
