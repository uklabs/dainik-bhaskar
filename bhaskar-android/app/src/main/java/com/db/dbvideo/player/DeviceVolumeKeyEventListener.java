package com.db.dbvideo.player;

public interface DeviceVolumeKeyEventListener {
    void onDeviceVolumeChange(int changeVolume);
}
