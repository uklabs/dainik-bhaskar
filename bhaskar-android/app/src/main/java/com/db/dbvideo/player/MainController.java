package com.db.dbvideo.player;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.appscommon.ads.AdsConstants;
import com.bhaskar.R;
import com.db.dbvideo.videoview.DeviceOrientationListener;
import com.db.dbvideo.videoview.VideoPlayerCallbackListener;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.QuickPreferences;

import java.util.HashMap;
import java.util.Map;

public class MainController implements DeviceVolumeKeyEventListener, AdStatusListener {
    private ViewGroup adUicontainer;
    private DeviceOrientationListener deviceOrientationListener;
    private SendVideoEventListener sendVideoEventListener;
    private boolean isAdCompleted;
    private int adStatus;

//    private AdDisplayContainer mAdDisplayContainer;
//
//    private AdsLoader mAdsLoader;
//    private AdsManager mAdsManager;
//    private ImaSdkFactory mSdkFactory;

    private PlayerControllerWithAdPlayback playerControllerWithAdPlayback;
    private String mCurrentIMAAdTagUrl;

    //Vmax Ad
    public Context context;
    public static final int AD_STATUS_IDLE = 1;
    public static final int AD_STATUS_READY = 2;
    public static final int AD_STATUS_STARTED = 3;
    public static final int AD_STATUS_ENDED = 4;

    private int vmaxAdStatus = AD_STATUS_IDLE;
    private String vmaxVideoAdUitid = "";

    private boolean isAdActiveLocal = false;

    private int videoAdsNetworkTypePrimary = AdsConstants.VIDEO_AD_NETWORK_IMA;
    private int videoAdsNetworkTypeSecondary = AdsConstants.VIDEO_AD_NETWORK_NONE;
    private int currentAdsNetworkType = AdsConstants.VIDEO_AD_NETWORK_NONE;
    private boolean isAutoPlay;
    private Map<String, String> customData;
    private String provider_code;
    private int provider_id;
    private int vwallbrandid;
    private String facebookAdId;
    private boolean isGDPRBlockedCountry;
    private boolean isGDPRAdFbEnabled;

    public MainController(Context context, View view, DeviceOrientationListener deviceOrientationListener, boolean showNativeControls, final VideoPlayerCallbackListener videoPlayerCallbackListener, boolean showNextButton, int thumbDefResId, boolean isAutoPlay, boolean isHideShare, String shareTitle, String shareUrl, String gTrackUrl, boolean nextAutoPlay, String provider_code, int providerId, int vwallbrandid, boolean isHideZoom) {
        this.context = context;
        this.customData = new HashMap();
        this.provider_code = provider_code;
        this.provider_id = providerId;
        this.vwallbrandid = vwallbrandid;
        this.isGDPRBlockedCountry = AppUtils.isBlockCountry(context);
        this.isGDPRAdFbEnabled = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.VideoPref.GDRP_FB_ENABLE, false);

        if (provider_code == null)
            provider_code = "";
        this.customData.put(AdsConstants.app_version, AdsConstants.getVersionNameAlreadyCalculated());
        this.customData.put(AdsConstants.auto_paly, "" + isAutoPlay);
        if (provider_code != null && !provider_code.equals(""))
            customData.put(AdsConstants.provider_code, provider_code);

//        this.mCurrentIMAAdTagUrl = videosAdsControl.getImavasturl();
//        this.facebookAdId = ((videosAdsControl.getFbvasturl() == null || videosAdsControl.getFbvasturl().equalsIgnoreCase("")) ? "972846212762696_1509773929069919" : videosAdsControl.getFbvasturl());
//        this.isAdActiveLocal = videosAdsControl.isActive();
//        this.videoAdsNetworkTypePrimary = (videosAdsControl.isFromArticle() ? videosAdsControl.getPreferredAdPartnerArticle1() : videosAdsControl.getPreferredAdPartner1());
//        this.videoAdsNetworkTypeSecondary = (videosAdsControl.isFromArticle() ? videosAdsControl.getPreferredAdPartnerArticle2() : videosAdsControl.getPreferredAdPartner2());
//        this.vmaxVideoAdUitid = AppPreferences.getInstance(context).getStringValue(QuickPreferences.VIDEO_VMAX_ADS_AD_ID, vmaxVideoAdUitid);
        this.isAutoPlay = isAutoPlay;

        if (isAdActiveLocal && isGDPRBlockedCountry) {
            this.videoAdsNetworkTypePrimary = AdsConstants.VIDEO_AD_NETWORK_IMA;
            this.videoAdsNetworkTypeSecondary = AdsConstants.VIDEO_AD_NETWORK_IMA;

            if (!isGDPRAdFbEnabled) {
                if (this.videoAdsNetworkTypePrimary == AdsConstants.VIDEO_AD_NETWORK_FACEBOOK)
                    this.videoAdsNetworkTypePrimary = AdsConstants.VIDEO_AD_NETWORK_IMA;
                if (this.videoAdsNetworkTypeSecondary == AdsConstants.VIDEO_AD_NETWORK_FACEBOOK)
                    this.videoAdsNetworkTypeSecondary = AdsConstants.VIDEO_AD_NETWORK_IMA;
            }
        }
        if (this.videoAdsNetworkTypePrimary == this.videoAdsNetworkTypeSecondary)
            this.videoAdsNetworkTypeSecondary = AdsConstants.VIDEO_AD_NETWORK_NONE;
//        if (!videosAdsControl.isRetryOtherPartner()) {
//            videoAdsNetworkTypeSecondary = AdsConstants.VIDEO_AD_NETWORK_NONE;
//        }
        this.currentAdsNetworkType = videoAdsNetworkTypePrimary;

        this.playerControllerWithAdPlayback = new PlayerControllerWithAdPlayback(context, view, deviceOrientationListener, showNativeControls, this, videoPlayerCallbackListener, showNextButton, thumbDefResId, isAutoPlay, isHideShare, shareTitle, shareUrl, gTrackUrl, nextAutoPlay, isHideZoom);
        this.playerControllerWithAdPlayback.hideController();
        this.deviceOrientationListener = deviceOrientationListener;
        this.adUicontainer = view.findViewById(R.id.adUiContainer);

        if (isAdActiveLocal) {
            switch (videoAdsNetworkTypePrimary) {
//                case AdsConstants.VIDEO_AD_NETWORK_FACEBOOK:
//                    createFacebookAds();
//                    break;
//                case AdsConstants.VIDEO_AD_NETWORK_IMA:
//                    createIMAVideoAd();
//                    break;
                case AdsConstants.VIDEO_AD_NETWORK_NONE:
                default:
                    break;
            }
            if (videoAdsNetworkTypePrimary != videoAdsNetworkTypeSecondary)
                switch (videoAdsNetworkTypeSecondary) {
//                    case AdsConstants.VIDEO_AD_NETWORK_FACEBOOK:
//                        createFacebookAds();
//                        break;
//                    case AdsConstants.VIDEO_AD_NETWORK_IMA:
//                        createIMAVideoAd();
//                        break;
                    case AdsConstants.VIDEO_AD_NETWORK_NONE:
                    default:
                        break;
                }
        }

        playerControllerWithAdPlayback.setContentCompleteListener(new PlayerControllerWithAdPlayback.OnContentCompleteListener() {
            @Override
            public void onContentComplete() {
//                if (isAdActiveLocal && videoAdsNetworkTypePrimary == AdsConstants.VIDEO_AD_NETWORK_IMA)
//                    mAdsLoader.contentComplete();
            }
        });
    }

    public void changeVideoViewScreen() {
        if (playerControllerWithAdPlayback != null) {
            playerControllerWithAdPlayback.changeVideoViewScreen();
        }
    }

    private int pxToDP(int px) {
        return (int) (px / context.getResources().getDisplayMetrics().density);
    }

//    private InstreamVideoAdView facebookVideoAdView;
//    private int facebookAdStatus = AD_STATUS_IDLE;

//    public void createFacebookAds() {
//        facebookVideoAdView = new InstreamVideoAdView(context, facebookAdId, new AdSize(pxToDP(adUicontainer.getMeasuredWidth()), pxToDP(adUicontainer.getMeasuredHeight())));
//        facebookVideoAdView.setAdListener(new FacebookAdListener());
//    }

//    private boolean isIMAalreadyCreated = false;
//
//    private IMAAdsLoadedListener adsLoadedListener;
//
//    public void createIMAVideoAd() {
//        if (!isIMAalreadyCreated) {
//            isIMAalreadyCreated = true;
//            mSdkFactory = ImaSdkFactory.getInstance();
//            ImaSdkFactory.getInstance().createImaSdkSettings();
//            ImaSdkSettings imaSdkSettings = mSdkFactory.createImaSdkSettings();
//            imaSdkSettings.setAutoPlayAdBreaks(false);
//            mAdsLoader = mSdkFactory.createAdsLoader(context);
//            adsLoadedListener = new IMAAdsLoadedListener();
//            mAdsLoader.addAdsLoadedListener(adsLoadedListener);
//            mAdsLoader.addAdErrorListener(new AdErrorEvent.AdErrorListener() {
//                @Override
//                public void onAdError(AdErrorEvent adErrorEvent) {
//                    try {
//                        if (videoAdsNetworkTypeSecondary != AdsConstants.VIDEO_AD_NETWORK_NONE && currentAdsNetworkType == videoAdsNetworkTypePrimary) {
//                            currentAdsNetworkType = videoAdsNetworkTypeSecondary;
//                            adRequested = false;
//                            requestAndPlayAds();
//                        } else {
//                            if (sendVideoEventListener != null) {
//                                AdError.AdErrorCode errorCode = adErrorEvent.getError().getErrorCode();
//                                sendVideoEventListener.sendAdEvent(AdsEventConstant.AD_ERROR + " " + errorCode.getErrorNumber());
//                            }
//
//                            adStatus = AD_STATUS_ERROR;
//                            resumeContent();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            });
//        }
//    }

//    public void playZapprWithAMAAd(String vastResponse) {
//        if (vastResponse == null || vastResponse.equalsIgnoreCase("")) {
//            adStatus = AD_STATUS_ERROR;
//            if (sendVideoEventListener != null) {
//                sendVideoEventListener.sendAdEvent(AdsEventConstant.AD_ERROR);
//            }
//            resumeContent();
//            return;
//        }
//        if (mAdsManager != null)
//            mAdsManager.destroy();
//
//        mAdsLoader.contentComplete();
//        mAdDisplayContainer = mSdkFactory.createAdDisplayContainer();
//        mAdDisplayContainer.setPlayer(playerControllerWithAdPlayback.getVideoAdPlayer());
//        mAdDisplayContainer.setAdContainer(adUicontainer);
//        AdsRequest request = mSdkFactory.createAdsRequest();
//        request.setAdsResponse(vastResponse);
//        request.setAdDisplayContainer(mAdDisplayContainer);
//        request.setContentProgressProvider(playerControllerWithAdPlayback.getContentProgressProvider());//need to be updated
//        if (adsLoadedListener != null)
//            adsLoadedListener.setRequestingAd(IMAAdsLoadedListener.ZAPR_AD);
//        mAdsLoader.requestAds(request);
//        BhaskarVideoPlayerTrackerUtil.sendAdEventTracking(AdsEventConstant.AD_REQUEST);
//    }

    public void setShowNativeControls(boolean showNativeControls) {
        playerControllerWithAdPlayback.setShowNativeControls(showNativeControls);
    }

    public SampleVideoPlayer getVideoPlayer() {
        return playerControllerWithAdPlayback.getVideoPlayer();
    }

    public boolean isAddCompleted() {
        return isAdCompleted;
    }

    public void setTrackerEventListener(RestartTrakerListener restartTrakerListener) {
        playerControllerWithAdPlayback.setTrackerEventListener(restartTrakerListener);
    }

    public boolean isAutoPause() {
        return playerControllerWithAdPlayback.isAutoPause();
    }

    public void setAutoPause(boolean autoPause) {
        playerControllerWithAdPlayback.setAutoPause(autoPause);
    }

    boolean isAdPauseCalled;
    boolean isZaperLoadedForIMA;

    public void pauseVideo(boolean isAutoPause) {
//        if (isAdActiveLocal) {
//            isAdPauseCalled = true;
//            if (currentAdsNetworkType == AdsConstants.VIDEO_AD_NETWORK_IMA && mAdsManager != null && playerControllerWithAdPlayback.getIsAdDisplayed()) {
//                mAdsManager.pause();
//            }
//            else if (currentAdsNetworkType == AdsConstants.VIDEO_AD_NETWORK_FACEBOOK && facebookVideoAdView != null && (facebookAdStatus == AD_STATUS_STARTED || facebookAdStatus == AD_STATUS_READY)) {
//                if (adUicontainer != null)
//                    adUicontainer.removeView(facebookVideoAdView);
//            }
//            else {
//                setAutoPause(isAutoPause);
//                playerControllerWithAdPlayback.savePosition();
//                playerControllerWithAdPlayback.pause();
//            }
//        } else {
        setAutoPause(isAutoPause);
        playerControllerWithAdPlayback.savePosition();
        playerControllerWithAdPlayback.pause();
//        }
    }

    public void resumeVideo() {
//        if (isAdActiveLocal) {
//            isAdPauseCalled = false;
//            if (currentAdsNetworkType == AdsConstants.VIDEO_AD_NETWORK_IMA && mAdsManager != null && playerControllerWithAdPlayback.getIsAdDisplayed()) {
//                mAdsManager.resume();
//            }
//            else if (currentAdsNetworkType == AdsConstants.VIDEO_AD_NETWORK_FACEBOOK && facebookVideoAdView != null && (facebookAdStatus == AD_STATUS_STARTED || facebookAdStatus == AD_STATUS_READY)) {
//                if (adUicontainer != null) {
//                    adUicontainer.removeAllViews();
//                    adUicontainer.addView(facebookVideoAdView);
//                }
//            }
//            else {
//                playerControllerWithAdPlayback.restorePosition();
//                if (isAutoPause())
//                    playerControllerWithAdPlayback.play();
//            }
//        } else {
        playerControllerWithAdPlayback.restorePosition();
        if (isAutoPause())
            playerControllerWithAdPlayback.play();
//        }
    }

    private void pauseContent() {
        playerControllerWithAdPlayback.pauseContentForAdPlayback();
        isAdCompleted = false;
        setPlayPauseOnAdTouch();
    }

    private void setPlayPauseOnAdTouch() {
    }

    public void resumeContent() {
        if (!isAdPauseCalled) {
            playerControllerWithAdPlayback.setAdStatus(adStatus);
            playerControllerWithAdPlayback.resumeContentAfterAdPlayback();
            isAdCompleted = true;
        }
    }


    boolean adRequested = false;
    Handler handler = new Handler();
    Runnable cancleAdRequestPlay = new Runnable() {
        @Override
        public void run() {

        }
    };

    int cancelPlayDefaultTimer = 8;

    public void clearTimer() {
        if (handler != null && cancleAdRequestPlay != null)
            handler.removeCallbacks(cancleAdRequestPlay);
    }

    public void startTimer() {
        if (handler != null && cancleAdRequestPlay != null) {
            long timerValue = 1000 /** AppPreferences.getInstance(context).getIntValue(QuickPreferences.VIDEO_ADS_TIMEOUT, cancelPlayDefaultTimer)*/;
            handler.postDelayed(cancleAdRequestPlay, timerValue);
        }
    }

    public void requestAndPlayAds() {
        if (!adRequested) {
            adRequested = true;
            if (isAdActiveLocal) {
                playerControllerWithAdPlayback.showProgressBar();
                switch (currentAdsNetworkType) {
//                    case AdsConstants.VIDEO_AD_NETWORK_FACEBOOK:
//                        if (facebookVideoAdView != null && facebookAdStatus == AD_STATUS_IDLE || vmaxAdStatus == AD_STATUS_ENDED) {
//                            facebookVideoAdView.loadAd();
//                            BhaskarVideoPlayerTrackerUtil.sendAdEventTracking(AdsEventConstant.AD_REQUEST);
//                        }
//                        break;
//                    case AdsConstants.VIDEO_AD_NETWORK_IMA:
//                        if (mAdsManager != null)
//                            mAdsManager.destroy();
//                        mAdsLoader.contentComplete();
//                        mAdDisplayContainer = mSdkFactory.createAdDisplayContainer();
//                        mAdDisplayContainer.setPlayer(playerControllerWithAdPlayback.getVideoAdPlayer());
//                        mAdDisplayContainer.setAdContainer(adUicontainer);
//                        AdsRequest request = mSdkFactory.createAdsRequest();
//                        mCurrentIMAAdTagUrl = mCurrentIMAAdTagUrl + "&cust_params=" + AdsConstants.app_version + "%3D" + customData.get(AdsConstants.app_version);
//                        if (!isGDPRBlockedCountry)
//                            mCurrentIMAAdTagUrl = mCurrentIMAAdTagUrl + "%26" + AdsConstants.auto_paly + "%3D" + customData.get(AdsConstants.auto_paly);
//                        if (!isGDPRBlockedCountry && provider_code != null && !provider_code.equals(""))
//                            mCurrentIMAAdTagUrl = mCurrentIMAAdTagUrl + "%26" + AdsConstants.provider_code + "%3D" + customData.get(AdsConstants.provider_code);
//                        if (!isGDPRBlockedCountry) {
//                            String vtpc = "";
//                            if (vwallbrandid != 0)
//                                mCurrentIMAAdTagUrl = mCurrentIMAAdTagUrl + "%26" + AdsConstants.vwallbrandid + "%3D" + vwallbrandid;
//                            if (provider_id > 9 && provider_id != 17)
//                                vtpc = "yes";
//                            else
//                                vtpc = "no";
//                            mCurrentIMAAdTagUrl = mCurrentIMAAdTagUrl + "%26" + AdsConstants.vtpc + "%3D" + vtpc;
//                        }
//                        mCurrentIMAAdTagUrl = mCurrentIMAAdTagUrl + "%26" + AdsConstants.gdpr_blocked + "%3D" + isGDPRBlockedCountry;
//
//                        request.setAdTagUrl(mCurrentIMAAdTagUrl);
//                        request.setAdDisplayContainer(mAdDisplayContainer);
//                        request.setContentProgressProvider(playerControllerWithAdPlayback.getContentProgressProvider());//need to be updated
//                        if (!AppUtils.isAdEnabled(context) && AppUtils.isBlockCountry(context))
//                            request.setExtraParameter("npa", "1");
//                        if (adsLoadedListener != null)
//                            adsLoadedListener.setRequestingAd(IMAAdsLoadedListener.IMA_AD);
//                        mAdsLoader.requestAds(request);
//                        BhaskarVideoPlayerTrackerUtil.sendAdEventTracking(AdsEventConstant.AD_REQUEST);
//                        break;
                    default:
                        resumeContent();
                        break;
                }
            } else
                resumeContent();
        }
    }

    public void setContentVideo(String videoPath) {
        playerControllerWithAdPlayback.setContentVideoPath(videoPath);//need to be updated
    }

    public void reDrawControls() {
        playerControllerWithAdPlayback.redrawControls();//need to be updated

    }

    public void changeScreenSwitcherIcon(int id) {
        if (playerControllerWithAdPlayback == null) return;
        playerControllerWithAdPlayback.changeScreenSwitcherIcon(id);//need to be updated
    }

    @Override
    public void onDeviceVolumeChange(int changeVolume) {
        playerControllerWithAdPlayback.onDeviceVolumeChange(changeVolume);//need to be updated
    }

    public void savePosition() {
        playerControllerWithAdPlayback.pauseContent();//need to be updated
    }

    public void restorePosition() {
        if (isAutoPause()) {
            return;
        }
        playerControllerWithAdPlayback.resumeContent();//need to be updated
    }

    public void resetVolume() {
        playerControllerWithAdPlayback.resetVolume();
    }

    public void setVideoEventListener(SendVideoEventListener sendVideoEventListener) {
        this.sendVideoEventListener = sendVideoEventListener;
        this.playerControllerWithAdPlayback.setVideoEventListener(sendVideoEventListener);
    }

    public int getAdStatus() {
        return adStatus;
    }

    public PlayerControllerCallbackReference getPlayerControllerCallbackReference() {
        return playerControllerWithAdPlayback;
    }

//    private class FacebookAdListener implements InstreamVideoAdListener {
//        @Override
//        public void onAdVideoComplete(Ad ad) {
//            facebookAdStatus = AD_STATUS_ENDED;
//            adStatus = AdStatusListener.AD_STATUS_PARTIAL_COMPLETE;
//            isAdCompleted = true;
//            adUicontainer.setBackgroundColor(Color.TRANSPARENT);
//            adUicontainer.setVisibility(View.GONE);
//            if (sendVideoEventListener != null)
//                sendVideoEventListener.sendAdEvent(AdsEventConstant.AD_COMPLETED);
//            resumeContent();
//        }
//
//        @Override
//        public void onError(Ad ad, com.facebook.ads.AdError adError) {
//            sendVideoEventListener.sendAdEvent(AdsEventConstant.AD_ERROR + " " + adError.getErrorCode());
//            adStatus = AD_STATUS_ERROR;
//            facebookAdStatus = AD_STATUS_IDLE;
//            adUicontainer.setBackgroundColor(Color.TRANSPARENT);
//            adUicontainer.setVisibility(View.GONE);
//            if (videoAdsNetworkTypeSecondary != AdsConstants.VIDEO_AD_NETWORK_NONE && currentAdsNetworkType == videoAdsNetworkTypePrimary) {
//                currentAdsNetworkType = videoAdsNetworkTypeSecondary;
//                adRequested = false;
//                requestAndPlayAds();
//            } else
//                resumeContent();
//        }
//
//        @Override
//        public void onAdLoaded(Ad ad) {
//            try {
//                playerControllerWithAdPlayback.hideController();
//                if (facebookVideoAdView != null) {
//                    facebookAdStatus = AD_STATUS_READY;
//                    adUicontainer.setBackgroundColor(Color.BLACK);
//                    adUicontainer.setVisibility(View.VISIBLE);
//                    adUicontainer.removeAllViews();
//                    adUicontainer.addView(facebookVideoAdView);
//                    facebookVideoAdView.show();
//                }
//            } catch (Exception e) {
//            }
//        }
//
//        @Override
//        public void onAdClicked(Ad ad) {
//        }
//
//        @Override
//        public void onLoggingImpression(Ad ad) {
//        }
//    }

    boolean isLoaded = false;

//    private class IMAAdsLoadedListener implements AdsLoader.AdsLoadedListener {
//        IMAAdEventListener imaAdEventListener;
//        private int requestType = 0;
//        static final int IMA_AD = 1;
//        static final int ZAPR_AD = 2;
//
//        public void setRequestingAd(int type) {
//            requestType = type;
//        }
//
//        @Override
//        public void onAdsManagerLoaded(AdsManagerLoadedEvent adsManagerLoadedEvent) {
//            mAdsManager = adsManagerLoadedEvent.getAdsManager();
//            imaAdEventListener = new IMAAdEventListener();
//            imaAdEventListener.setRequestingAd(requestType);
//            mAdsManager.addAdEventListener(imaAdEventListener);
//            mAdsManager.init();
//        }
//    }


//    private class IMAAdEventListener implements AdEvent.AdEventListener {
//        private int requestType = 0;
//
//        public void setRequestingAd(int type) {
//            requestType = type;
//        }
//
//        @Override
//        public void onAdEvent(AdEvent adEvent) {
//            if (sendVideoEventListener != null) {
//                sendVideoEventListener.logEvent("" + adEvent.getType());
//            }
//            switch (adEvent.getType()) {
//                case AD_BREAK_READY:
//                    mAdsManager.start();
//                    playerControllerWithAdPlayback.hideController();
//                    break;
//                case LOADED:
//                    if (!isLoaded) {
//                        isLoaded = true;
//                        mAdsManager.start();
//                        playerControllerWithAdPlayback.hideController();
//                    }
//                    break;
//                case CONTENT_PAUSE_REQUESTED:
//                    pauseContent();
//                    break;
//                case CONTENT_RESUME_REQUESTED:
//                    resumeContent();
//                    break;
//                case PAUSED:
//                    break;
//                case RESUMED:
//                    break;
//                case STARTED:
//                    isZaperLoadedForIMA = true;
//                    if (requestType == IMAAdsLoadedListener.ZAPR_AD && mAdsManager != null && isAdPauseCalled) {
//                        mAdsManager.pause();
//                    } else {
//                        playerControllerWithAdPlayback.hideController();
//                        if (sendVideoEventListener != null) {
//                            sendVideoEventListener.sendAdEvent(AdsEventConstant.AD_PLAY);
//                        }
//                    }
//                    break;
//                case CLICKED:
//                    if (sendVideoEventListener != null) {
//                        sendVideoEventListener.sendAdEvent(AdsEventConstant.AD_CLICK);
//                    }
//                    break;
//                case SKIPPED:
//                    playerControllerWithAdPlayback.hideController();
//                    if (sendVideoEventListener != null) {
//                        sendVideoEventListener.sendAdEvent(AdsEventConstant.AD_SKIP);
//                    }
//                    break;
//                case COMPLETED:
//                    adStatus = AdStatusListener.AD_STATUS_PARTIAL_COMPLETE;
//                    if (sendVideoEventListener != null) {
//                        sendVideoEventListener.sendAdEvent(AdsEventConstant.AD_COMPLETED);
//                    }
//                    isAdCompleted = true;
//                    break;
//                default:
//                    break;
//            }
//        }
//    }


    public void cancelAutoPlayTimer() {
        if (isAdActiveLocal)
            switch (currentAdsNetworkType) {
                case AdsConstants.VIDEO_AD_NETWORK_IMA:
                    break;
//                case AdsConstants.VIDEO_AD_NETWORK_FACEBOOK:
//                    if (facebookVideoAdView != null)
//                        facebookVideoAdView.destroy();
//                    break;
            }
    }
}
