package com.db.dbvideo.player;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.util.ImageUtil;
import com.db.util.Systr;

public class PlayerController implements View.OnTouchListener, GestureDetector.OnGestureListener {

    private static final long DELAYED_TIME = 2000;
    private static float MAX_Y;
    private Context context;
    private View controllerLayout;
    private ImageView imageViewPlayButton;
    private ImageView imageViewOrientationButton;
    private boolean visible = false;
    private View videoControlsLayout;
    private Animation inTranslateAnimation;
    private Animation outTranslateAnimation;
    private Handler handler;
    private HideControllerTask hideControllerTask;
    private PlayerControllerWithAdPlayback playerControllerWithAdPlayback;
    private boolean showNextButton;
    private int thumbDefResId;
    private boolean isAutoPlay;
    private GestureDetector gestureDetector;
    private ImageView imageViewThumbnail;
    private SeekBar seekBar;
    private boolean seekbarDragging;
    private View replayNextButtonLayout;
    private ImageView imageViewReplayButton;
    private ImageView imageViewNextButton;
    private TextView txtTotal, txtCurrent;
    private ImageView imgShare;
    private boolean isHideShare = true;
    private boolean isAutoNextPlay = false;
    private boolean isHideZoom = false;

    private View autoNextPlayButtonLayout;
    private TextView cancelButtonNextAutoPlay;
    private ImageView autoNextPlayView;
    private ImageView imageViewReplayButtonNextAutoPlay;
    private ProgressBar circularProgress;

    public PlayerController(Context context, View controllerLayout, PlayerControllerWithAdPlayback playerControllerWithAdPlayback, boolean showNextButton, int thumbDefResId, boolean isAutoPlay, boolean isHideShare, boolean isAutoNextPlay, boolean isHideZoom) {
        this.context = context;
        this.controllerLayout = controllerLayout;
        this.playerControllerWithAdPlayback = playerControllerWithAdPlayback;
        this.showNextButton = showNextButton;
        this.thumbDefResId = thumbDefResId;
        this.isAutoPlay = isAutoPlay;
        this.isHideShare = isHideShare;
        this.isAutoNextPlay = isAutoNextPlay;
        this.isHideZoom = isHideZoom;

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        MAX_Y = displayMetrics.heightPixels / 10;

        init();
    }

    private void init() {

        handler = new Handler();

        imageViewThumbnail =  controllerLayout.findViewById(R.id.image_view_thumbnail);

        ImageUtil.setImage(context, thumbDefResId, imageViewThumbnail, 0);

        videoControlsLayout = controllerLayout.findViewById(R.id.layout_video_control);
        imageViewPlayButton = (ImageView) controllerLayout.findViewById(R.id.imageView_play_button);
        imageViewOrientationButton = (ImageView) controllerLayout.findViewById(R.id.imageView_change_orientation_button);
        imgShare = (ImageView) controllerLayout.findViewById(R.id.vcv_img_share);

        replayNextButtonLayout = controllerLayout.findViewById(R.id.layout_replay_next_control);
        imageViewNextButton = (ImageView) controllerLayout.findViewById(R.id.imageView_next_button);
        imageViewReplayButton = (ImageView) controllerLayout.findViewById(R.id.imageView_replay_button);


        txtTotal = (TextView) controllerLayout.findViewById(R.id.txt_total);
        txtCurrent = (TextView) controllerLayout.findViewById(R.id.txt_current);

        autoNextPlayButtonLayout = controllerLayout.findViewById(R.id.layout_auto_next_play_control);
        cancelButtonNextAutoPlay = (TextView) controllerLayout.findViewById(R.id.cancel_button_next_auto_play);
        autoNextPlayView = (ImageView) controllerLayout.findViewById(R.id.auto_next_play_view);
        imageViewReplayButtonNextAutoPlay = (ImageView) controllerLayout.findViewById(R.id.imageView_replay_button_next_auto_play);
        circularProgress = (ProgressBar) controllerLayout.findViewById(R.id.circularProgressbar);

//        if (!showNextButton) {
//            imageViewNextButton.setVisibility(View.GONE);
//        }
        if (isHideShare) {
            imgShare.setVisibility(View.GONE);
        } else {
            imgShare.setVisibility(View.VISIBLE);
            imgShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playerControllerWithAdPlayback.shareButtonCLick();
                }
            });
        }

        if(isHideZoom){
            imageViewOrientationButton.setVisibility(View.INVISIBLE);
        }else{
            imageViewOrientationButton.setVisibility(View.VISIBLE);
        }

        inTranslateAnimation = new AlphaAnimation(0, 1);
        inTranslateAnimation.setInterpolator(new DecelerateInterpolator()); //setData this
        inTranslateAnimation.setDuration(300);

        outTranslateAnimation = new AlphaAnimation(1, 0);
        outTranslateAnimation.setInterpolator(new AccelerateInterpolator()); //and this
        outTranslateAnimation.setDuration(300);

//        inTranslateAnimation.setDuration(ANIMATION_DURATION);
//        outTranslateAnimation.setDuration(ANIMATION_DURATION);
//        inTranslateAnimation.setFillAfter(true);
//        outTranslateAnimation.setFillAfter(true);

        hideControllerTask = new HideControllerTask();
        gestureDetector = new GestureDetector(context, this);
        /*controllerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visible=!visible;
                handler.removeCallbacks(hideControllerTask);
                if (visible) {
                    showController();
                    return;
                }
                hideController();
            }
        });*/

//        controllerLayout.setOnTouchListener(this);
        controllerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visible = !visible;
                handler.removeCallbacks(hideControllerTask);
                if (visible) {
                    showController();
                    return;
                }
                hideController();
            }
        });

        if (!isAutoPlay) {
            imageViewReplayButton.setImageResource(R.drawable.ic_video_play_48);
            imageViewNextButton.setVisibility(View.GONE);
            replayNextButtonLayout.setVisibility(View.VISIBLE);
            visible = true;
        }

        imageViewPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(hideControllerTask);
                playerControllerWithAdPlayback.onPlayPauseButtonClick();
                startTimerForHideController();
            }
        });
        imageViewOrientationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(hideControllerTask);
                playerControllerWithAdPlayback.changeOrientation();
                startTimerForHideController();
            }
        });
        imageViewReplayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAutoPlay) {
                    BusProvider.getInstance().post(new PlayerEventOttoWrapper());
                    isAutoPlay = true;
                    imageViewReplayButton.setImageResource(R.drawable.ic_video_replay);
                    visible = false;
                } else {
                    playerControllerWithAdPlayback.onPlayPauseButtonClick();
                }
                replayNextButtonLayout.setVisibility(View.GONE);
            }
        });
        imageViewNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerControllerWithAdPlayback.onNextButtonClick();
                replayNextButtonLayout.setVisibility(View.GONE);
            }
        });
        seekBar = (SeekBar) controllerLayout.findViewById(R.id.seek_bar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
               /* if (fromUser) {
                }*/
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                handler.removeCallbacks(hideControllerTask);
                seekbarDragging = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                startTimerForHideController();
                seekbarDragging = false;
                seekBar.getProgress();
                playerControllerWithAdPlayback.seekPlayerTo(seekBar.getProgress());
            }
        });

        /*******/
        imageViewReplayButtonNextAutoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelTimer();

                if (!isAutoPlay) {
                    BusProvider.getInstance().post(new PlayerEventOttoWrapper());
                    isAutoPlay = true;
                    imageViewReplayButton.setImageResource(R.drawable.ic_video_replay);
                    visible = false;
                } else {
                    playerControllerWithAdPlayback.onPlayPauseButtonClick();
                }
                autoNextPlayButtonLayout.setVisibility(View.GONE);
                ImageUtil.setImage(context, thumbDefResId, imageViewThumbnail, 0);
            }
        });

        autoNextPlayView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelTimer();

                playerControllerWithAdPlayback.onNextButtonClick();
                autoNextPlayButtonLayout.setVisibility(View.GONE);
            }
        });

        cancelButtonNextAutoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoNextPlayButtonLayout.setVisibility(View.GONE);
                replayNextButtonLayout.setVisibility(View.VISIBLE);
                if (showNextButton) {
                    imageViewNextButton.setVisibility(View.VISIBLE);
                } else {
                    imageViewNextButton.setVisibility(View.GONE);
                }

                cancelTimer();
            }
        });

//        imageViewNextButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (countDownTimer != null) {
//                    countDownTimer.cancel();
//                    countDownTimer = null;
//                }
//
//                playerControllerWithAdPlayback.onNextButtonClick();
//                autoNextPlayButtonLayout.setVisibility(View.GONE);
//            }
//        });
    }

    void changeProgress(int videoProgress, long totalDuration, long currentDuration) {
//        progressBar.setProgress(videoProgress);
        if (seekbarDragging) {
            return;
        }
        seekBar.setProgress(videoProgress);
        if (txtTotal != null && txtCurrent != null) {
            totalDuration = totalDuration / 1000;
            currentDuration = currentDuration / 1000;
            txtTotal.setText((totalDuration / 60) + ":" + (String.valueOf(totalDuration % 60).length() > 1 ? String.valueOf(totalDuration % 60) : "0" + String.valueOf(totalDuration % 60)));
            txtCurrent.setText((currentDuration / 60) + ":" + (String.valueOf(currentDuration % 60).length() > 1 ? String.valueOf(currentDuration % 60) : "0" + String.valueOf(currentDuration % 60)));
        }
    }

    private void hideController() {
        videoControlsLayout.startAnimation(outTranslateAnimation);
        videoControlsLayout.setVisibility(View.GONE);
    }

    private void showController() {
        handler.removeCallbacks(hideControllerTask);
        videoControlsLayout.startAnimation(inTranslateAnimation);
        videoControlsLayout.setVisibility(View.VISIBLE);

        startTimerForHideController();
    }

    private void startTimerForHideController() {
        handler.postDelayed(hideControllerTask, DELAYED_TIME);
    }

    public void changeImageSwitcherIcon(int resId) {
        if (imageViewOrientationButton == null) return;
        imageViewOrientationButton.setImageResource(resId);
    }

    public void changePlayPauseImage(int imageResId) {
//        imageViewPlayButton.setBackgroundResource(imageResId);
        imageViewPlayButton.setImageResource(imageResId);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        visible = !visible;
        handler.removeCallbacks(hideControllerTask);
        if (visible) {
            showController();
            return true;
        }
        hideController();
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return true;
    }

    public void onVideoComplete() {
        /*imageViewPlayButton.setImageResource(R.drawable.ic_action_play_over_video);
        controllerLayout.setVisibility(View.VISIBLE);
        startTimerForHideUnhideController();*/
        showThumbnail();
        if (isAutoNextPlay) {
            autoNextPlayButtonLayout.setVisibility(View.VISIBLE);
            startTimer();
        } else {
            Systr.println("SHOW NEXT BUTTON : " + showNextButton);
            if (showNextButton) {
                imageViewNextButton.setVisibility(View.VISIBLE);
            } else {
                imageViewNextButton.setVisibility(View.GONE);
            }
            replayNextButtonLayout.setVisibility(View.VISIBLE);
        }
        handler.removeCallbacks(hideControllerTask);
//        imageViewPlayButton.setBackgroundResource(R.drawable.ic_action_play_over_video);
//        startTimerForHideController();
        hideController();

//        playerControllerWithAdPlayback.onPlayPauseButtonClick();
    }

    private CountDownTimer countDownTimer;

    private void startTimer() {
        countDownTimer = new CountDownTimer(5000, 250) {
            @Override
            public void onTick(long millisUntilFinished) {
                int per = (int) (((5000 - millisUntilFinished) / 5000.0f) * 100);
                circularProgress.setProgress(per);
                Systr.println("Circle Progress : " + per);
            }

            @Override
            public void onFinish() {
                circularProgress.setProgress(100);
                playerControllerWithAdPlayback.onNextButtonClick();
                autoNextPlayButtonLayout.setVisibility(View.GONE);
            }
        };

        countDownTimer.start();
    }

    public void cancelTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    public void showControlls() {
    }

    public void showControll() {
        visible = true;
        showController();
    }

    public void hideControll() {
        visible = false;
        hideController();
    }

    public ImageView getImageViewThumbnail() {
        return imageViewThumbnail;
    }

    public void hideThumbnail() {
        imageViewThumbnail.setVisibility(View.GONE);
        replayNextButtonLayout.setVisibility(View.GONE);
    }

    public void showThumbnail() {
        imageViewThumbnail.setVisibility(View.VISIBLE);

    }

    public void setThumbnailImageView(ImageView thumbnailImageView) {
//        this.imageViewThumbnail = thumbnailImageView;
    }

    void onOrientaitionChangeToLandscpe() {
        handler.removeCallbacks(hideControllerTask);
        startTimerForHideController();
    }

    void onOrientaitionChangeToPortrait() {
        handler.removeCallbacks(hideControllerTask);
        startTimerForHideController();
    }

    private class HideControllerTask implements Runnable {
        @Override
        public void run() {
            if (playerControllerWithAdPlayback.isVideoPlaying()) {
                visible = false;
                hideController();
            } else {
                if (!visible) {
                    visible = true;
                    showController();
                }
            }
        }
    }
}
