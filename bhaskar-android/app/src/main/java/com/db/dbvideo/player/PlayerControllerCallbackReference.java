package com.db.dbvideo.player;

import android.widget.ImageView;


public interface PlayerControllerCallbackReference {
    boolean isAutoPause();

    void setAutoPause(boolean autoPause);

    boolean isVideoPlaying();

    ImageView getThumbnailImageView();

    void setThumbnailImageView(ImageView imageView);

    void onOrientationChangeToLandScape();

    void onOrientationChangeToPortrait();
}
