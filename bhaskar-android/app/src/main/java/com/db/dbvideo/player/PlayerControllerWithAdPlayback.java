package com.db.dbvideo.player;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.source.server.BackgroundRequest;
import com.db.dbvideo.videoview.DeviceOrientationListener;
import com.db.dbvideo.videoview.VideoPlayerCallbackListener;
import com.db.util.AppUtils;
import com.db.util.ShareUtil;
import com.db.util.Systr;

public class PlayerControllerWithAdPlayback implements VideoPlayer.PlayerCallback, MediaPlayer.OnPreparedListener, DeviceVolumeKeyEventListener, PlayerControllerCallbackReference {
    //    private final List<VideoAdPlayer.VideoAdPlayerCallback> mAdCallbacks = new ArrayList<>(1);
    private DeviceOrientationListener deviceOrientationListener;
    private VideoPlayerCallbackListener videoPlayerCallbackListener;
    private AdStatusListener adStatusListener;
    private SampleVideoPlayer videoPlayer;
    private Context context;
    private String videoLink;
    private Handler mHandler = new Handler();
    private boolean isVideoPlaying;
    private boolean isVideoCompleted;
    private int progress;
    private int currentVideoPosition;
    private int pauseVideoPosition;
    private boolean mIsAdDisplayed;
    private String videoPath;
    //    private ContentProgressProvider mContentProgressProvider;
//    private VideoAdPlayer videoAdPlayer;
    private boolean showNativeControls;
    private RestartTrakerListener restartTrakerListener;
    private OnContentCompleteListener contentCompleteListener;
    private SendVideoEventListener videoEventListener;
    private PlayerController playerController;
    private View circularProgressView;
    private int adStatus;
    private boolean autoPause;
    private int videoState;
    private boolean manualPause;
    private boolean isPlayReplaySend;
    private String shareTitle = "", shareUrl = "";
    private String gTrackUrl;

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = videoPlayer.getDuration();
            long currentDuration = videoPlayer.getCurrentPosition();
            progress = AppUtils.getProgressPercentage(currentDuration, totalDuration);
            playerController.changeProgress(progress, totalDuration, currentDuration);
            currentVideoPosition = (int) currentDuration;
            mHandler.postDelayed(this, 100);
        }
    };
    private int mSavedAdPosition;
    private int mSavedContentPosition;
    private float videoProportion;

    public PlayerControllerWithAdPlayback(Context context, View view, DeviceOrientationListener deviceOrientationListener, boolean showNativeControls, AdStatusListener adStatusListener, VideoPlayerCallbackListener videoPlayerCallbackListener, boolean showNextButton, int thumbDefResId, boolean isAutoPlay, boolean isHideShare, String shareTitle, String shareUrl, String gTrackUrl, boolean nextAutoPlay, boolean isHideZoom) {
        this.context = context;
        this.deviceOrientationListener = deviceOrientationListener;
        this.videoPlayerCallbackListener = videoPlayerCallbackListener;
        this.showNativeControls = false;//showNativeControls;
        this.adStatusListener = adStatusListener;
        this.shareTitle = shareTitle;
        this.shareUrl = shareUrl;
        this.adStatusListener = adStatusListener;
        this.gTrackUrl = gTrackUrl;

        videoState = 0;
        isPlayReplaySend = false;
        findViews(view, showNextButton, thumbDefResId, isAutoPlay, isHideShare, nextAutoPlay, isHideZoom);
        if (!isAutoPlay) {
            circularProgressView.setVisibility(View.GONE);
        }
    }

    private void findViews(View view, boolean showNextButton, int thumbDefResId, boolean isAutoPlay, boolean isHideShare, boolean nextAutoPlay, boolean isHideZoom) {
        playerController = new PlayerController(context, view.findViewById(R.id.layout_touch_video_control), this, showNextButton, thumbDefResId, isAutoPlay, isHideShare, nextAutoPlay, isHideZoom);
        videoPlayer = (SampleVideoPlayer) view.findViewById(R.id.sampleVideoPlayer);
        videoPlayer.addPlayerCallback(this);
        circularProgressView = view.findViewById(R.id.progressbar);
        videoPlayer.setOnPrepareListener(this);
        if (showNativeControls) {
        }
//        initializeVideoAdPlayer();
//        initializeContentProvider();
    }

    public void onPlayPauseButtonClick() {
        if (isVideoCompleted) {
            Systr.println("video completed");
//            isVideoCompleted=false;
            videoState = 0;//VideoPlayerConstant.VIDEO_STATE_COMPLETED;
            resumeContentAfterAdPlayback();
            isPlayReplaySend = false;
            if (restartTrakerListener != null) {
                restartTrakerListener.startTrackerAgain();
            }
            return;
        }
        isVideoPlaying = videoPlayer.isPlaying();
//        int imageResId;
        if (isVideoPlaying) {
//            imageResId = R.drawable.fvl_play_reader_white;
//            imageViewPlayPause.setImageResource(imageResId);//setPauseImage
//            videoPlayer.pause();
            setAutoPause(false);
            manualPause = true;

        } else {
//            videoPlayer.play();
//            imageResId = R.drawable.fvl_pause_reader_white;
            manualPause = false;

//            imageViewPlayPause.setImageResource(imageResId);//setPlayImage
        }
//        playerController.changePlayPauseImage(imageResId);
        enableDisablePlayPauseButton();
    }

    public void onResumeClick() {
        enableResumeButton();
    }

    private void enableResumeButton() {
//        if (mIsAdDisplayed) {
//            videoAdPlayer.playAd();
//        }
//        else {
//            isVideoPlaying = videoPlayer.isPlaying();
//            if (!isVideoPlaying) {
//                setAutoPause(false);
//                Systr.println("Resume called");
//                videoPlayer.play();
//            }
//        }
//        int imageResId = R.drawable.ic_video_pause;
//        playerController.changePlayPauseImage(imageResId);
    }

    public void onPauseClick() {
        enablePauseButton();
    }

    private void enablePauseButton() {
//        if (mIsAdDisplayed) {
//            videoAdPlayer.pauseAd();
//        } else {
        isVideoPlaying = videoPlayer.isPlaying();
        if (isVideoPlaying) {
            setAutoPause(true);
            Systr.println("Pause called");
            videoPlayer.pause();
        }
//        }
        int imageResId = R.drawable.ic_video_play_48;
        playerController.changePlayPauseImage(imageResId);
    }

    public void changeOrientation() {
        deviceOrientationListener.changeOrientation();
    }

    public void redrawControls() {

    }

    public void shareButtonCLick() {
        //tracking shifted
        // Tracking
//        String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
//        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.SHARE, shareUrl, campaign);
//        Systr.println("GA Events : " + AppFlyerConst.GACategory.ARTICLE_EVENT + AppFlyerConst.GAAction.SHARE + gTrackUrl + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));

//        AppUtils.getInstance().shareArticle(context, shareUrl, shareTitle);
        BackgroundRequest.getStoryShareUrl(context, shareUrl, (flag, url) -> ShareUtil.shareDefault(context, shareTitle, context.getResources().getString(R.string.checkout_this_video), url, gTrackUrl, true));
    }

    public void setVideoLink(String videoLink, boolean isRtsp, Uri videoUri) {
        this.videoLink = videoLink;
        if (!TextUtils.isEmpty(videoLink))
            videoPlayer.setVideoPath(videoLink);
    }

    public void updateProgressBar() {
        if (showNativeControls) {
            return;
        }
        circularProgressView.setVisibility(View.GONE);
        mHandler.postDelayed(mUpdateTimeTask, 100);

    }

    public void resumeContent() {
//       videoPlayer.resume();
        if (autoPause) {
            return;
        }
        /*if (videoState==VideoPlayerConstant.VIDEO_STATE_RESUME  || videoState==VideoPlayerConstant.VIDEO_STATE_PLAY) {
            return;
        }*/
        if (videoState == VideoPlayerConstant.VIDEO_STATE_COMPLETED || videoState == VideoPlayerConstant.VIDEO_STATE_RESUME || videoState == VideoPlayerConstant.VIDEO_STATE_PLAY) {
            return;
        }
        if (manualPause) {
            return;
        }
        videoPlayer.seekTo(pauseVideoPosition);
        restartTrakerListener.startTrackerAgain();
        videoPlayer.play();
    }

    public void pauseContent() {
        if (videoState == VideoPlayerConstant.VIDEO_STATE_COMPLETED || videoState == VideoPlayerConstant.VIDEO_STATE_PAUSE) {
            return;
        }
        pauseVideoPosition = videoPlayer.getCurrentPosition();
        videoPlayer.pause();
        restartTrakerListener.stopTracker();
    }

    public void resumeContentAfterAdPlayback() {
        pauseVideoPosition = currentVideoPosition = 0;
        mIsAdDisplayed = false;
        manualPause = false;
//        videoState=0;
//        isVideoCompleted=false;onPl
        playerController.showThumbnail();
        circularProgressView.setVisibility(View.VISIBLE);
        showController();
        if (showNativeControls) {
            videoPlayer.enablePlaybackControls();
        } else {
            videoPlayer.disablePlaybackControls();
        }
        setVideoLink(videoPath, false, null);
        enableControls();
        resumeContent();
       /* int adStatus = adStatusListener.getAdStatus();
        if (adStatus==AdStatusListener.AD_STATUS_COMPLETED) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isVideoPlaying = videoPlayer.isPlaying();
                    if (videoPlayer.isPlaying())
                    {
                        enableDisablePlayPauseButton();
                    }
                }
            },2000);
        }*/
    }

    public void setContentVideoPath(String videoPath) {
        this.videoPath = videoPath;
        isVideoCompleted = false;
    }

    public void pauseContentForAdPlayback() {

        disableControls();
        pauseContent();
        videoPlayer.stopPlayback();
    }

    private void enableControls() {
        if (showNativeControls) {
            return;
        }

    }

    private void disableControls() {
        if (showNativeControls) {
            return;
        }
    }

    @Override
    public void onPlay() {
        if (mIsAdDisplayed) {
            circularProgressView.setVisibility(View.GONE);
            hideController();
//            for (VideoAdPlayer.VideoAdPlayerCallback callback : mAdCallbacks) {
//                callback.onPlay();
//
//            }
            return;
        }
        if (isVideoCompleted) {
            videoPlayer.pause();
            return;
        }

        if (videoPlayerCallbackListener != null)
            videoPlayerCallbackListener.onVideoStart();
    }

    @Override
    public void onPause() {
        if (mIsAdDisplayed) {

//            for (VideoAdPlayer.VideoAdPlayerCallback callback : mAdCallbacks) {
//                videoState = VideoPlayerConstant.VIDEO_STATE_PAUSE;
//                callback.onPause();
//            }
            return;
        }
        if (videoState == VideoPlayerConstant.VIDEO_STATE_PAUSE || videoState == VideoPlayerConstant.VIDEO_STATE_COMPLETED || videoState == 0) {
            return;
        }
        if (!isPlayReplaySend) {
            return;
        }
        videoState = VideoPlayerConstant.VIDEO_STATE_PAUSE;
        videoEventListener.sendVideoEvent(VideoEventConstant.VIDEO_PAUSE);
//        videoEventListener.sendVideoEvent(VideoEventConstant.VIDEO_PAUSE);
    }

    @Override
    public void onResume() {

        if (mIsAdDisplayed) {

//            for (VideoAdPlayer.VideoAdPlayerCallback callback : mAdCallbacks) {
//                videoState = VideoPlayerConstant.VIDEO_STATE_RESUME;
//                callback.onResume();
//
//            }
            return;
        }
        if (videoState == VideoPlayerConstant.VIDEO_STATE_RESUME || videoState == VideoPlayerConstant.VIDEO_STATE_PLAY || videoState == 0) {
            return;
        }
        if (!isPlayReplaySend) {
            return;
        }
        videoState = VideoPlayerConstant.VIDEO_STATE_RESUME;
        videoEventListener.sendVideoEvent(VideoEventConstant.VIDEO_RESUME);

        playerController.hideThumbnail();
        circularProgressView.setVisibility(View.GONE);
    }

    @Override
    public void onCompleted() {
        if ((adStatus != AdStatusListener.AD_STATUS_ERROR || adStatus != AdStatusListener.AD_STATUS_PARTIAL_COMPLETE)) {
            if (mIsAdDisplayed) {
//                for (VideoAdPlayer.VideoAdPlayerCallback callback : mAdCallbacks) {
//                    callback.onEnded();
//                    resumeContentAfterAdPlayback();
//                }
                return;
            }
        }
        manualPause = false;
        videoState = VideoPlayerConstant.VIDEO_STATE_COMPLETED;
        mHandler.removeCallbacks(mUpdateTimeTask);
        isVideoCompleted = true;
        videoEventListener.sendVideoCompleteEvent();
        contentCompleteListener.onContentComplete();
        videoPlayerCallbackListener.onVideoComplete();
        if (restartTrakerListener != null) {
            restartTrakerListener.stopTracker();
        }

        /*if (showNativeControls) {
            return;
        }*/
        playerController.onVideoComplete();
    }

    @Override
    public void onError() {
        if ((adStatus != AdStatusListener.AD_STATUS_ERROR || adStatus != AdStatusListener.AD_STATUS_PARTIAL_COMPLETE)) {

            if (mIsAdDisplayed) {
//                for (VideoAdPlayer.VideoAdPlayerCallback callback : mAdCallbacks) {
//                    callback.onError();
//
//                }
                return;
            }
        }
        if (circularProgressView != null)
            circularProgressView.setVisibility(View.GONE);
//        playerController.imageViewThumbnail.setImageResource(R.drawable.error_video_player);
//        playerController.setThumbnailImageView(getThumbnailImageView());
//        Toast.makeText(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_), Toast.LENGTH_SHORT).show();
        videoEventListener.sendVideoEvent(VideoEventConstant.VIDEO_ERR);
        videoPlayerCallbackListener.onVideoError();
    }

    @Override
    public void onPlayStart() {
        restartTrakerListener.startTrackerAgain();
//        AppLogs.printErrorLogs("TAG_VIDEO","onPlayStart");

        videoState = VideoPlayerConstant.VIDEO_STATE_PLAY;
    }

    public void changeVideoViewScreen() {
        try {
            int screenWidth = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
            int screenHeight = ((Activity) context).getWindowManager().getDefaultDisplay().getHeight();
            float screenProportion = (float) screenWidth / (float) screenHeight;
            android.view.ViewGroup.LayoutParams lp = videoPlayer.getLayoutParams();
            if (videoProportion > screenProportion) {
                lp.width = screenWidth;
                lp.height = (int) ((float) screenWidth / videoProportion);
            } else {
                lp.width = (int) (videoProportion * (float) screenHeight);
                lp.height = screenHeight;
            }
            videoPlayer.setLayoutParams(lp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        int videoWidth = mp.getVideoWidth();
        int videoHeight = mp.getVideoHeight();
        videoProportion = (float) videoWidth / (float) videoHeight;
        changeVideoViewScreen();

        if (videoState == VideoPlayerConstant.VIDEO_STATE_PAUSE) {
            return;
        }
        if ((adStatus != AdStatusListener.AD_STATUS_ERROR || adStatus != AdStatusListener.AD_STATUS_PARTIAL_COMPLETE)) {

            if (mIsAdDisplayed) {
                playerController.hideThumbnail();
                circularProgressView.setVisibility(View.GONE);
                return;
            }

            if (videoState == VideoPlayerConstant.VIDEO_STATE_COMPLETED) {
                return;
            }
            if (isVideoCompleted) {
                if (!isPlayReplaySend) {
                    videoEventListener.sendVideoEvent(VideoEventConstant.VIDEO_REPLAY);
                    isPlayReplaySend = true;
                }
                isVideoCompleted = false;

            } else {
                if (!isPlayReplaySend) {
                    videoEventListener.sendVideoEvent(VideoEventConstant.VIDEO_PLAY);
                    isPlayReplaySend = true;
                }
            }
        }
        /*if (videoState==VideoPlayerConstant.VIDEO_STATE_PAUSE) {
            return;
        }*/
        {
            playerController.hideThumbnail();
            circularProgressView.setVisibility(View.GONE);
            mp.start();
            updateProgressBar();
            enableDisablePlayPauseButton();
        }
    }

    public void savePosition() {
        if (mIsAdDisplayed) {
            mSavedAdPosition = videoPlayer.getCurrentPosition();
        } else {
            mSavedContentPosition = videoPlayer.getCurrentPosition();
        }
    }

    /**
     * Restore the currently loaded video to its previously saved playback progress state. This is
     * called when content is resumed after ad playback or when focus has returned to the app.
     */
    public void restorePosition() {
        if (mIsAdDisplayed) {
            videoPlayer.seekTo(mSavedAdPosition);
        } else {
            videoPlayer.seekTo(mSavedContentPosition);
        }
    }


    private void enableDisablePlayPauseButton() {
        if (showNativeControls) {
            Systr.println("Show native controls");
            return;
        }
        int imageResId;
        if (isVideoPlaying) {
            Systr.println("Pause called");
//            imageResId = R.drawable.fvl_play_reader_white;
            imageResId = R.drawable.ic_video_play_48;
//            imageViewPlayPause.setImageResource(imageResId);//setPauseImage
            videoPlayer.pause();
//            videoEventListener.sendVideoEvent(VideoEventConstant.VIDEO_PAUSE);
        } else {
            Systr.println("Play called");
            videoPlayer.play();
//            imageResId = R.drawable.fvl_pause_reader_white;
            imageResId = R.drawable.ic_video_pause;
//            videoEventListener.sendVideoEvent(VideoEventConstant.VIDEO_RESUME);
//            imageViewPlayPause.setImageResource(imageResId);//setPlayImage
        }
        playerController.changePlayPauseImage(imageResId);
    }


    public void showController() {
        if (showNativeControls) {
            return;
        }
        playerController.showControlls();
    }

    public void hideController() {
        if (showNativeControls) {
            return;
        }

        playerController.showControlls();
        videoPlayer.disablePlaybackControls();
    }

    @Override
    public void onDeviceVolumeChange(int changeVolume) {
//        volumeControllerLayout.onVolumeChangeByDevice(changeVolume);
    }

    public void changeScreenSwitcherIcon(int resId) {
//        imageViewOrientation.setImageResource(resId);
        if (playerController == null) return;
        playerController.changeImageSwitcherIcon(resId);
    }

//    public VideoAdPlayer getVideoAdPlayer() {
//        if (videoAdPlayer == null) {
//            initializeVideoAdPlayer();
//            mIsAdDisplayed = true;
//        }
//        return videoAdPlayer;
//    }

//    private void initializeVideoAdPlayer() {
//        videoAdPlayer = new VideoAdPlayer() {
//            @Override
//            public int getVolume() {
//                return 100;
//            }
//
//            @Override
//            public VideoProgressUpdate getAdProgress() {
//                if (!mIsAdDisplayed || videoPlayer.getDuration() <= 0) {
//                    return VideoProgressUpdate.VIDEO_TIME_NOT_READY;
//                }
//                return new VideoProgressUpdate(videoPlayer.getCurrentPosition(), videoPlayer.getDuration());
//            }
//
//            @Override
//            public void playAd() {
//                mIsAdDisplayed = true;
//                videoPlayer.play();
////                videoState=VideoPlayerConstant.VIDEO_STATE_PLAY;
////                videoAdPlayer.playAd();
//                hideController();
//            }
//
//            @Override
//            public void loadAd(String url) {
//                try {
//                    mIsAdDisplayed = true;
//                    videoPlayer.setVideoPath(url);
//                } catch (Exception e) {
//                }
//            }
//
//            @Override
//            public void stopAd() {
//                videoPlayer.stopPlayback();
//            }
//
//            @Override
//            public void pauseAd() {
//                videoPlayer.pause();
////                videoState=VideoPlayerConstant.VIDEO_STATE_PAUSE;
//
//            }
//
//            @Override
//            public void resumeAd() {
//                playAd();
////                videoState=VideoPlayerConstant.VIDEO_STATE_RESUME;
//
//            }
//
//            @Override
//            public void addCallback(VideoAdPlayerCallback videoAdPlayerCallback) {
//                mAdCallbacks.add(videoAdPlayerCallback);
//            }
//
//            @Override
//            public void removeCallback(VideoAdPlayerCallback videoAdPlayerCallback) {
//                mAdCallbacks.remove(videoAdPlayerCallback);
//            }
//
//        };
//    }
//
//    public ContentProgressProvider getContentProgressProvider() {
//        if (mContentProgressProvider == null) {
//            initializeContentProvider();
//        }
//        return mContentProgressProvider;
//    }
//
//    private void initializeContentProvider() {
//        mContentProgressProvider = new ContentProgressProvider() {
//            @Override
//            public VideoProgressUpdate getContentProgress() {
//                if (mIsAdDisplayed || videoPlayer.getDuration() <= 0) {
//                    return VideoProgressUpdate.VIDEO_TIME_NOT_READY;
//                }
//                return new VideoProgressUpdate(videoPlayer.getCurrentPosition(),
//                        videoPlayer.getDuration());
//            }
//        };
//
//    }


    public void resetVolume() {
//        volumeControllerLayout.resetVolume();
    }

    public void setShowNativeControls(boolean showNativeControls) {
        this.showNativeControls = showNativeControls;
    }

    public SampleVideoPlayer getVideoPlayer() {
        return videoPlayer;
    }

    public void setTrackerEventListener(RestartTrakerListener restartTrakerListener) {

        this.restartTrakerListener = restartTrakerListener;
    }

    public void setVideoEventListener(SendVideoEventListener videoEventListener) {
        this.videoEventListener = videoEventListener;
    }

    public boolean isVideoPlaying() {
        /*if (!isAutoPause()) {
            return true;
        }*/
        return videoPlayer.isPlaying();
    }

    @Override
    public ImageView getThumbnailImageView() {
        return playerController.getImageViewThumbnail();
    }

    @Override
    public void setThumbnailImageView(ImageView imageView) {
        playerController.setThumbnailImageView(imageView);
    }

    @Override
    public void onOrientationChangeToLandScape() {
        playerController.onOrientaitionChangeToLandscpe();
    }

    @Override
    public void onOrientationChangeToPortrait() {
        playerController.onOrientaitionChangeToPortrait();
    }

    public void setAdStatus(int adStatus) {
        this.adStatus = adStatus;
    }

    public boolean isAutoPause() {
        return autoPause;
    }

    public void setAutoPause(boolean autoPause) {
        this.autoPause = autoPause;
    }

    public void seekPlayerTo(int progress) {
        currentVideoPosition = AppUtils.progressToTimer(progress, videoPlayer.getDuration());
        videoPlayer.seekTo(currentVideoPosition);
    }

    public void onNextButtonClick() {
        videoPlayerCallbackListener.onNextButtonclick();
    }

    public void cancelAutoPlayTimer() {
        if (playerController != null)
            playerController.cancelTimer();
    }

    public void showProgressBar() {
        if (circularProgressView != null) {
            circularProgressView.setVisibility(View.VISIBLE);
        }
    }

    public void setContentCompleteListener(OnContentCompleteListener contentCompleteListener) {

        this.contentCompleteListener = contentCompleteListener;
    }

    public PlayerControllerCallbackReference getPlayerControllerCallbackReference() {
        return this;
    }

    public boolean getIsAdDisplayed() {
        return mIsAdDisplayed;
    }

    public void play() {
        videoPlayer.play();

        int imageResId = R.drawable.ic_video_pause;
        playerController.changePlayPauseImage(imageResId);
//        playerController.hideControll();
    }

    public void pause() {
        videoPlayer.pause();

        int imageResId = R.drawable.ic_video_play_48;
        playerController.changePlayPauseImage(imageResId);
//        playerController.showControll();
    }

    /**
     * Interface for alerting caller of video completion.
     */
    public interface OnContentCompleteListener {
        void onContentComplete();
    }
}
