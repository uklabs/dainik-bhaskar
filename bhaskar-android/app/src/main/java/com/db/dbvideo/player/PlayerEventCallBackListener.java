package com.db.dbvideo.player;

public interface PlayerEventCallBackListener {

    void sendVideoEvent(String action);

    void sendAdEvent(String action);

    void onVideoPlay();

    void onVideoPlay10();

    void onVideoPlay20();

    void onVideoPlay25();

    void onVideoPlay30();

    void onVideoPlay40();

    void onVideoPlay50();

    void onVideoPlay60();

    void onVideoPlay70();

    void onVideoPlay75();

    void onVideoPlay80();

    void onVideoPlay90();

    void onVideoPlay100();

    void onVideoPause();

    void onVideoResume();

    void onVideoError();

    void onVideoReplay();
}
