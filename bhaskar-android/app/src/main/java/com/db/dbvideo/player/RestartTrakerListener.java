package com.db.dbvideo.player;

public interface RestartTrakerListener {
    void startTrackerAgain();

    void stopTracker();
}
