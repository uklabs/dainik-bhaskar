package com.db.dbvideo.player;

import android.os.Handler;

import com.db.InitApplication;

public class SendPlayerEventsTask implements Runnable, RestartTrakerListener, SendVideoEventListener {
    private static final String CLASS_NAME = "SendPlayerEventsTask";
    //    boolean play, play20, play40, play60, play80, play100;
//    private String events[]={AppUtil.encode("Play"),AppUtil.encode("Play-20%"),AppUtil.encode("Play-40%"),AppUtil.encode("Play-60%"),AppUtil.encode("Play-80%"),AppUtil.encode("Play-100%")};
//    private String videoEvents[]={"25%","50%","75%","100%"};
    final int[] percentageLevelArr = {10, 20, 25, 30, 40, 50, 60, 70, 75, 80, 90, 100};
    boolean[] percStatusTrackingArr = new boolean[12];
    private MainController mainController;
    private SampleVideoPlayer mVideoPlayer;
    private SendVideoEventListener sendVideoEventListener;
    private String fileName = "";
    private Handler mHandler;
    private int intervalDelayedTime;
    private int maxDuration;
    private int currentPercentageLevelIndex;
    private int currentPercentageMark;


    public SendPlayerEventsTask(MainController mainController, SampleVideoPlayer mVideoPlayer) {
        this.mainController = mainController;
        this.mVideoPlayer = mVideoPlayer;
        mHandler = new Handler();
    }

    @Override
    public void run() {
        sendVideoEvent();

//        mHandler.postDelayed(this, intervalDelayedTime);
        mHandler.postDelayed(this, 500);
//        mHandler.post(this);
    }

    private void sendVideoEvent() {
        try {
            if (isAdCompleted() && mVideoPlayer.isPlaying() && maxDuration > 0) {
                int currentPosition = mVideoPlayer.getCurrentPosition();
                float percentage = (currentPosition * 100) / maxDuration;
                calculatePercentageLevel((int) percentage);
            }
        } catch (Exception e) {
        }
    }

    private void calculatePercentageLevel(int percentage) {
//        AppLogs.printErrorLogs("PER", "per=" + percentage);
        if (percentage == 100) {
            currentPercentageLevelIndex = percentageLevelArr.length;
            currentPercentageMark = percentage;
            sendVideoEvent(currentPercentageMark + "%");
//          AppUtil.notifyUser(this.getClass().getSimpleName(),"percentageMark "+currentPercentageMark);
            return;
        }
        int calculatedPercentageLevelArrIndex = getCalculatedPercentageLevelArrIndex(percentage);

        for (int i = calculatedPercentageLevelArrIndex; i >= 0; i--) {
            if (i < percStatusTrackingArr.length && !percStatusTrackingArr[i]) {
                sendVideoEvent(percentageLevelArr[i] + "%");
                percStatusTrackingArr[i] = true;
            }
        }
    }

    private int getCalculatedPercentageLevelArrIndex(int percentage) {
        for (int i = 0; i < percentageLevelArr.length - 1; i++) {
            int l1 = percentageLevelArr[i];
            int l2 = percentageLevelArr[i + 1];
            if (percentage >= l1 && percentage < l2) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void startTrackerAgain() {
//        play=play20=play40=play60=play80=play100=false;

        maxDuration = mVideoPlayer.getDuration();
        int currentPosition = mVideoPlayer.getCurrentPosition();
        intervalDelayedTime = maxDuration / percentageLevelArr.length;
        int currentDelayedTime = intervalDelayedTime - (maxDuration - currentPosition) / percentageLevelArr.length;
        mHandler.post(this);
//        mHandler.postDelayed(this,currentDelayedTime);
    }

    @Override
    public void stopTracker() {
        mHandler.removeCallbacks(this);
//        play=play20=play40=play60=play80=play100=false;

    }

    @Override
    public void sendVideoEvent(String message) {
//        Log.e("myevent : ", message);
        if (message.equalsIgnoreCase(VideoEventConstant.VIDEO_PLAY) || message.equalsIgnoreCase(VideoEventConstant.VIDEO_RESUME)) {
            if (InitApplication.getInstance() != null)
                InitApplication.getInstance().setVideoPlay(true);
        }
        if (message.equalsIgnoreCase(VideoEventConstant.VIDEO_PAUSE) || message.equalsIgnoreCase("100%")) {
//            Log.e("myevent : ", "pause manual");
            if (InitApplication.getInstance() != null)
                InitApplication.getInstance().setVideoPlay(false);
        }
        if (message.equalsIgnoreCase(VideoEventConstant.VIDEO_REPLAY)) {
            if (InitApplication.getInstance()!= null)
                InitApplication.getInstance().setVideoPlay(true);
            percStatusTrackingArr = new boolean[12];
        }
        BhaskarVideoPlayerTrackerUtil.sendVideoEventTracking(message);
//        AppUtil.notifyUser(VideoEventConstant.TAG_VIDEO,message);
    }

    @Override
    public void sendAdEvent(String message) {
//            Log.v("ADS", message);
        BhaskarVideoPlayerTrackerUtil.sendAdEventTracking(message);
//        AppUtil.notifyUser(VideoEventConstant.TAG_VIDEO,message);
    }

    @Override
    public void sendAdErrorEvent(String message) {
        sendAdEvent(AdsEventConstant.AD_ERROR);
//            Log.v("ADS", message);
//        AppUtil.notifyUser(CLASS_NAME,message);
    }

    @Override
    public void logEvent(String message) {
        //Log.v("ADS",message);
        System.out.println(message);
//        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sendVideoCompleteEvent() {
//        currentPercentageMark=100;
//        sendVideoEvent(currentPercentageMark+"%");
//        currentPercentageMark=currentPercentageLevelIndex=0;
        int calculatedPercentageLevelArrIndex = percentageLevelArr.length - 1;
        for (int i = calculatedPercentageLevelArrIndex; i >= 0; i--) {
            if (i < percStatusTrackingArr.length && !percStatusTrackingArr[i]) {
                sendVideoEvent(percentageLevelArr[i] + "%");
                percStatusTrackingArr[i] = true;

            }
        }
        percStatusTrackingArr = new boolean[4];
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private boolean isAdCompleted() {
        /*if (mainController!=null) {
            return mainController.isAddCompleted();
        }
        return true;*/
        return mainController == null || mainController.isAddCompleted();
    }
}
