package com.db.dbvideo.player;

public interface SendVideoEventListener {
    void sendVideoEvent(String message);

    void sendAdEvent(String message);

    void logEvent(String message);

    void sendVideoCompleteEvent();

    void sendAdErrorEvent(String message);
}
