package com.db.dbvideo.player;

public interface VideoEventConstant {
    String VIDEO_PLAY = "Play";
    String VIDEO_PLAY_20 = "Video_Play_20";
    String VIDEO_PLAY_40 = "Video_Play_40";
    String VIDEO_PLAY_60 = "Video_Play_60";
    String VIDEO_PLAY_80 = "Video_Play_80";
    String VIDEO_PLAY_100 = "Video_Play_100";
    String VIDEO_RESUME = "Resume";
    String VIDEO_REPLAY = "Replay";

    String VIDEO_ERR = "Error";

    String VIDEO_PAUSE = "Pause";
    String VIDEO_DOWNLOAD = "Download";

    String PLAY_25 = "25%";
    String PLAY_50 = "50%";
    String PLAY_75 = "75%";
    String PLAY_100 = "100%";

    String PLAY_DM = "Play_DM";
    String COMPLETED_DM = "Completed_DM";
    String AD_PALY_DM = "Ad_Play_DM";
    String AD_COMPLETED_DM = "Ad_Completed_DM";

    String TAG_VIDEO = "VIDEO_TAG";
    String Ad_Pre_ROLL_SKIPPABLE = "http://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/"
            + "single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast"
            + "&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct"
            + "%3Dskippablelinear&correlator=";

    // String PRE_ROLL_NO_SKIP="https://pubads.g.doubleclick.net/gampad/live/ads?sz=400x300&iu=/28928716/db_video_app_android&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator= ";
    String PRE_ROLL_NO_SKIP = "http://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=";
//    String DB_AD="https://pubads.g.doubleclick.net/gampad/live/ads?sz=400x300&iu=/28928716/db_video_app_android&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1";
}
