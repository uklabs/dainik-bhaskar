package com.db.dbvideo.player;


public interface VideoPlayerConstant {

    String AD_CONTROL = "adControl";
    String VIDEO_URL = "videoUrl";
    String PROVIDER_CODE = "providerCode";
    String PROVIDER_ID = "providerId";
    String VWALLBRANDID = "vwallbrandid";
    String VIDEO_FILE_NAME = "fileName";
    String DEFAULT_THUMBNAIL_IMAGE_RES_ID = "thumbDefaultResId";
    String SHOW_NEXT_BUTTON = "showNextButton";
    String HIDE_SHARE_BUTTON = "hideShareButton";
    String HIDE_ZOOM_BUTTON = "hideZommButton";
    String SHARE_TITLE = "shareTitle";
    String SHARE_LINK = "shareLink";
    String SHOW_NATIVE_CONTROLS = "showNativeControls";
    String IS_AD_ACTIVE = "isAdActive";

    int VIDEO_STATE_PLAY = 3;
    int VIDEO_STATE_PAUSE = 1;
    int VIDEO_STATE_RESUME = 2;
    int VIDEO_STATE_COMPLETED = 4;

    String IS_AUTO_PLAY = "isAutoPlay";
    String DEFAULT_NEXT_AUTO_THUMBNAIL_IMAGE_RES_ID = "nextAutoThumbDefaultResId";
    String IS_AUTO_NEXT_PLAY = "isAutoNextPlay";
    String VIDEO_IS_LIVE = "videoIsLive";
    String ULTIMA_TRACK_URL = "ultimaTrackUrl";
    String IMAGE_PATH = "imagePath";
    String MEDIA_TITLE = "mediaTitle";

    String SECTION_LABEL = "sectionLabel";
    String GA_EVENT_LABEL = "gaEventLabel";
    String VIDEO_SOURCE = "videoSource";
    String VIDEO_INDEX = "videoIndex";
    String VIDEO_ID = "videoId";
    String VIDEO_DAILY_MOTION_ID = "videoDailyMotionId";
}
