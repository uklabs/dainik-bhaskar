package com.db.dbvideo.player;

import android.content.Context;
import android.text.TextUtils;

import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.bhaskar.appscommon.tracking.ultima.UltimaTracking;
import com.bhaskar.util.LoginController;
import com.comscore.Analytics;
import com.db.InitApplication;
import com.db.util.AppPreferences;
import com.bhaskar.util.CommonConstants;
import com.db.util.QuickPreferences;
import com.db.util.Systr;

public class VideoPlayerEventTracker implements PlayerEventCallBackListener {

    public static final String CATEGORY_VIDEO = "Video_Interaction";
    public static final String CATEGORY_AD = "Ad Event Tracking";

    private static final String VIDEO_PREFIX = "-video";
    public static boolean isCrossButtonClick;
    static VideoPlayerEventTracker videoPlayerEventTracker;
    private String videoUrl;
    private Context context;
    private String ultimaTrackerUrl;
    private String storyId;

    private String ultimaAdUrl = "https://ultima.bhaskar.com/tracking/ads_tracking.php";


    private VideoPlayerEventTracker(Context context, String videoUrl, String ultimaTrackerUrl, String storyId) {
        this.context = context;
        this.videoUrl = videoUrl;
        this.ultimaTrackerUrl = ultimaTrackerUrl;
        this.storyId = storyId;
    }

    public static VideoPlayerEventTracker getInstance(Context context, String videoUrl, String ultimaTrackUrl, String storyId) {
//        if (videoPlayerEventTracker == null)
        videoPlayerEventTracker = new VideoPlayerEventTracker(context, videoUrl, ultimaTrackUrl, storyId);
        return videoPlayerEventTracker;
    }

    @Override
    public void sendVideoEvent(String action) {
        if (isCrossButtonClick) {
            isCrossButtonClick = false;
            return;
        }

//        if (!TextUtils.isEmpty(action)) {
//            String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
//            Tracking.trackGAEvent(InitApplication.getInstance().getDefaultTracker(), CATEGORY_VIDEO, action, AppTraverseUtil.getScreenNameForVideoTracking(), campaign);
//            Systr.println("GA_EVENT VIDEO : " + CATEGORY_VIDEO + action + AppTraverseUtil.getScreenNameForVideoTracking() + campaign);
//        }
    }

    @Override
    public void sendAdEvent(String action) {
        if (!TextUtils.isEmpty(action)) {
            String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), CATEGORY_AD, action, AppTraverseUtil.getScreenNameForVideoTracking(), campaign);
            Systr.println("GA_EVENT AD_VIDEO : " + CATEGORY_AD + action + AppTraverseUtil.getScreenNameForVideoTracking() + ((campaign.equalsIgnoreCase("direct")) ? "" : campaign));
        }

        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        if (action.equalsIgnoreCase(AdsEventConstant.AD_REQUEST)) {
            Tracking.trackUltimaVideoAdRequest(context, ultimaAdUrl, AppTraverseUtil.getVideoSource(), CommonConstants.CHANNEL_ID, AppTraverseUtil.getVideoId(), isBlockedCountry);
        } else {
            if (action.equalsIgnoreCase(AdsEventConstant.AD_COMPLETED)) {
                Tracking.trackUltimaVideoAd(context, ultimaAdUrl, AppTraverseUtil.getVideoSource(), CommonConstants.CHANNEL_ID, AppTraverseUtil.getVideoId(), 3, "", isBlockedCountry);
            } else if (action.equalsIgnoreCase(AdsEventConstant.AD_SKIP)) {
                Tracking.trackUltimaVideoAd(context, ultimaAdUrl, AppTraverseUtil.getVideoSource(), CommonConstants.CHANNEL_ID, AppTraverseUtil.getVideoId(), 2, "", isBlockedCountry);
//            } else if (action.equalsIgnoreCase(AdsEventConstant.AD_PLAY)) {
//                Tracking.trackUltimaVideoAd(context, ultimaAdUrl, AppTraverseUtil.getVideoSource(), CommonConstants.CHANNEL_ID, AppTraverseUtil.getVideoId(), 1, isBlockedCountry);
            } else if (action.contains(AdsEventConstant.AD_ERROR)) {
                Tracking.trackUltimaVideoAd(context, ultimaAdUrl, AppTraverseUtil.getVideoSource(), CommonConstants.CHANNEL_ID, AppTraverseUtil.getVideoId(), 0, action, isBlockedCountry);
            }
        }
    }

    @Override
    public void onVideoPlay() {
        Systr.println("VIDEO : onVideoPlay");

        Analytics.notifyUxActive();
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideo(context, ultimaTrackerUrl, AppTraverseUtil.getVideoSource(), isBlockedCountry);

        String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), CATEGORY_VIDEO, VideoEventConstant.VIDEO_PLAY, AppTraverseUtil.getScreenNameForVideoTracking(), campaign);

        CleverTapDB.getInstance(context).cleverTapTrackEvent(context, CTConstant.EVENT_NAME.VIDEO_WATCHED, CTConstant.PROP_NAME.VIDEO_TITLE, AppTraverseUtil.getVideoTitle(), LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.ACTION_TYPE, CTConstant.PROP_VALUE.VIDEO_STARTED));
    }

    @Override
    public void onVideoPlay10() {
        Systr.println("VIDEO : onVideoPlay10");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideoQuartile(context, ultimaTrackerUrl, UltimaTracking.VIDEO_QUARTILE.PLAY_10, isBlockedCountry);
    }

    @Override
    public void onVideoPlay20() {
        Systr.println("VIDEO : onVideoPlay20");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideoQuartile(context, ultimaTrackerUrl, UltimaTracking.VIDEO_QUARTILE.PLAY_20, isBlockedCountry);
    }

    @Override
    public void onVideoPlay25() {
        String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), CATEGORY_VIDEO, VideoEventConstant.PLAY_25, AppTraverseUtil.getScreenNameForVideoTracking(), campaign);
        Systr.println("GA_EVENT VIDEO : " + CATEGORY_VIDEO + VideoEventConstant.PLAY_25 + AppTraverseUtil.getScreenNameForVideoTracking() + ((campaign.equalsIgnoreCase("direct")) ? "" : campaign));
    }

    @Override
    public void onVideoPlay30() {
        Systr.println("VIDEO : onVideoPlay30");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideoQuartile(context, ultimaTrackerUrl, UltimaTracking.VIDEO_QUARTILE.PLAY_30, isBlockedCountry);
    }

    @Override
    public void onVideoPlay40() {
        Systr.println("VIDEO : onVideoPlay40");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideoQuartile(context, ultimaTrackerUrl, UltimaTracking.VIDEO_QUARTILE.PLAY_40, isBlockedCountry);
    }

    @Override
    public void onVideoPlay50() {
        Systr.println("VIDEO : onVideoPlay50");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideoQuartile(context, ultimaTrackerUrl, UltimaTracking.VIDEO_QUARTILE.PLAY_50, isBlockedCountry);

        String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), CATEGORY_VIDEO, VideoEventConstant.PLAY_50, AppTraverseUtil.getScreenNameForVideoTracking(), campaign);
        Systr.println("GA_EVENT VIDEO : " + CATEGORY_VIDEO + VideoEventConstant.PLAY_50 + AppTraverseUtil.getScreenNameForVideoTracking() + ((campaign.equalsIgnoreCase("direct")) ? "" : campaign));
    }

    @Override
    public void onVideoPlay60() {
        Systr.println("VIDEO : onVideoPlay60");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideoQuartile(context, ultimaTrackerUrl, UltimaTracking.VIDEO_QUARTILE.PLAY_60, isBlockedCountry);
    }

    @Override
    public void onVideoPlay70() {
        Systr.println("VIDEO : onVideoPlay70");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideoQuartile(context, ultimaTrackerUrl, UltimaTracking.VIDEO_QUARTILE.PLAY_70, isBlockedCountry);
    }

    @Override
    public void onVideoPlay75() {
        String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), CATEGORY_VIDEO, VideoEventConstant.PLAY_75, AppTraverseUtil.getScreenNameForVideoTracking(), campaign);
        Systr.println("GA_EVENT VIDEO : " + CATEGORY_VIDEO + VideoEventConstant.PLAY_75 + AppTraverseUtil.getScreenNameForVideoTracking() + ((campaign.equalsIgnoreCase("direct")) ? "" : campaign));
    }

    @Override
    public void onVideoPlay80() {
        Systr.println("VIDEO : onVideoPlay80");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideoQuartile(context, ultimaTrackerUrl, UltimaTracking.VIDEO_QUARTILE.PLAY_80, isBlockedCountry);
    }

    @Override
    public void onVideoPlay90() {
        Systr.println("VIDEO : onVideoPlay90");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideoQuartile(context, ultimaTrackerUrl, UltimaTracking.VIDEO_QUARTILE.PLAY_90, isBlockedCountry);
    }


    @Override
    public void onVideoPlay100() {
        Systr.println("VIDEO : onVideoPlay100");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideoQuartile(context, ultimaTrackerUrl, UltimaTracking.VIDEO_QUARTILE.PLAY_100, isBlockedCountry);

        String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), CATEGORY_VIDEO, VideoEventConstant.PLAY_100, AppTraverseUtil.getScreenNameForVideoTracking(), campaign);

        CleverTapDB.getInstance(context).cleverTapTrackEvent(context, CTConstant.EVENT_NAME.VIDEO_WATCHED, CTConstant.PROP_NAME.VIDEO_TITLE, AppTraverseUtil.getVideoTitle(), LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.ACTION_TYPE, CTConstant.PROP_VALUE.VIDEO_END));
    }

    @Override
    public void onVideoPause() {
        Analytics.notifyUxInactive();
        Systr.println("VIDEO : onVideoPause");
    }

    @Override
    public void onVideoResume() {
        Analytics.notifyUxActive();
        Systr.println("VIDEO : onVideoResume");
    }

    @Override
    public void onVideoError() {
        Systr.println("VIDEO : onVideoError");
    }

    @Override
    public void onVideoReplay() {
        Systr.println("VIDEO : onVideoReplay");
        boolean isBlockedCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.trackUltimaVideo(context, ultimaTrackerUrl, AppTraverseUtil.getVideoSource(), isBlockedCountry);

        String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), CATEGORY_VIDEO, VideoEventConstant.VIDEO_REPLAY, AppTraverseUtil.getScreenNameForVideoTracking(), campaign);
        Systr.println("GA_EVENT VIDEO : " + CATEGORY_VIDEO + VideoEventConstant.VIDEO_REPLAY + AppTraverseUtil.getScreenNameForVideoTracking() + ((campaign.equalsIgnoreCase("direct")) ? "" : campaign));
    }
}
