package com.db.dbvideo.videoview;

public interface DeviceOrientationListener {
    void changeOrientation();
}
