package com.db.dbvideo.videoview;

public interface VideoPlayerCallbackListener {
    void onVideoStart();

    void onVideoComplete();

    void onVideoError();

    void onNextButtonclick();
}
