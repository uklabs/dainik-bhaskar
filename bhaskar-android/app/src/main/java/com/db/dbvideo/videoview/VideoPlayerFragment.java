package com.db.dbvideo.videoview;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bhaskar.R;
import com.db.dbvideo.player.BusProvider;
import com.db.dbvideo.player.MainController;
import com.db.dbvideo.player.PlayerControllerCallbackReference;
import com.db.dbvideo.player.PlayerEventOttoWrapper;
import com.db.dbvideo.player.SampleVideoPlayer;
import com.db.dbvideo.player.SendPlayerEventsTask;
import com.db.dbvideo.player.SettingsContentObserver;
import com.db.dbvideo.player.VideoPlayerConstant;
import com.db.util.Constants;
import com.squareup.otto.Subscribe;

public class VideoPlayerFragment extends Fragment {
    public MainController mainController;
    private String videoUrl;
    private String adUrl;
    private Handler handler = new Handler();
    private boolean showNativeControls;
    private String fileName;
    private Context context;
    private SampleVideoPlayer mVideoPlayer;
    private SettingsContentObserver mSettingsContentObserver;
    private SendPlayerEventsTask sendPlayerEventsTask;
    private int defaultThumbResId;
    private boolean showNextButton;
    private boolean isAutoPlay;
    private boolean isHideShare;
    private String shareTitle = "";
    private String shareLink = "";
    private String gTrackUrl;
    private String providerCode;
    private int providerId;
    private int vwallbrandid;
    private boolean isAutoNextPlay;
    //    private VideosAdsControl videosAdsControl;
    private int isLive;
    private boolean isHideZoom;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Subscribe
    public void initPlayer(PlayerEventOttoWrapper playerEventOttoWrapper) {
        if (mainController != null)
            mainController.requestAndPlayAds();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();
        getData();
        View view = inflater.inflate(R.layout.z_fragment_video_custom_controls, container, false);
        try {
            setAndFindViews(view);
            setDeviceVolumeReceiver();
            hideShareButton();
        } catch (Exception e) {
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isLive == 1) {
            view.findViewById(R.id.live_button_view).setVisibility(View.VISIBLE);
        }
    }

    private void setDeviceVolumeReceiver() {
        try {
            mSettingsContentObserver = new SettingsContentObserver(context, new Handler(), mainController);
            context.getApplicationContext().getContentResolver().registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, mSettingsContentObserver);
        } catch (Exception e) {
        }
    }

    private void setAndFindViews(View view) {
//        mainController = new MainController(context, view.findViewById(R.id.rootLayout), (DeviceOrientationListener) getActivity(), showNativeControls, (VideoPlayerCallbackListener) getActivity(), showNextButton, defaultThumbResId, isAutoPlay, isHideShare, shareTitle, shareLink, gTrackUrl);
        mainController = new MainController(context, view.findViewById(R.id.rootLayout), (DeviceOrientationListener) getActivity(), showNativeControls, (VideoPlayerCallbackListener) getActivity(), showNextButton, defaultThumbResId, isAutoPlay, isHideShare, shareTitle, shareLink, gTrackUrl, isAutoNextPlay, providerCode, providerId, vwallbrandid, isHideZoom);
        mainController.setContentVideo(videoUrl);

        mVideoPlayer = mainController.getVideoPlayer();
//      sendPlayerEventsTask = new SendPlayerEventsTask(mainController, mVideoPlayer);
        sendPlayerEventsTask = new SendPlayerEventsTask(mainController, mVideoPlayer);

        mainController.setVideoEventListener(sendPlayerEventsTask);
        mainController.setTrackerEventListener(sendPlayerEventsTask);

        if (!isAutoPlay) {
            return;
        }

        mainController.requestAndPlayAds();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mainController.reDrawControls();
            }
        }, 500);
    }

    public void changeVideoViewScreen() {
        if (mainController != null) {
            mainController.changeVideoViewScreen();
        }
    }

    private void getData() {
        Bundle bundle = getArguments();
//        videosAdsControl = bundle.getParcelable(VideoPlayerConstant.AD_CONTROL);
        videoUrl = bundle.getString(VideoPlayerConstant.VIDEO_URL);
        providerCode = bundle.getString(VideoPlayerConstant.PROVIDER_CODE);
        providerId = bundle.getInt(VideoPlayerConstant.PROVIDER_ID);
        vwallbrandid = bundle.getInt(VideoPlayerConstant.VWALLBRANDID);
        fileName = bundle.getString(VideoPlayerConstant.VIDEO_FILE_NAME);
        providerCode = bundle.getString(VideoPlayerConstant.PROVIDER_CODE);
        showNativeControls = bundle.getBoolean(VideoPlayerConstant.SHOW_NATIVE_CONTROLS, false);
        showNextButton = bundle.getBoolean(VideoPlayerConstant.SHOW_NEXT_BUTTON, false);
        isAutoPlay = bundle.getBoolean(VideoPlayerConstant.IS_AUTO_PLAY, false);
        defaultThumbResId = bundle.getInt(VideoPlayerConstant.DEFAULT_THUMBNAIL_IMAGE_RES_ID);
        isHideShare = bundle.getBoolean(VideoPlayerConstant.HIDE_SHARE_BUTTON);
        isAutoNextPlay = bundle.getBoolean(VideoPlayerConstant.IS_AUTO_NEXT_PLAY, false);
        isHideZoom = bundle.getBoolean(VideoPlayerConstant.HIDE_ZOOM_BUTTON, false);
        if (!isHideShare) {
            shareTitle = bundle.getString(VideoPlayerConstant.SHARE_TITLE);
            shareLink = bundle.getString(VideoPlayerConstant.SHARE_LINK);
        }
        if (TextUtils.isEmpty(videoUrl) || videoUrl.contains("manifest.f4m")) {
            getActivity().finish();
            return;
        }
        isLive = bundle.getInt(VideoPlayerConstant.VIDEO_IS_LIVE, 0);
        gTrackUrl = bundle.getString(Constants.KeyPair.KEY_GA_G_TRACK_URL);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
        if (null == mSettingsContentObserver) {
            return;
        }
        context.getApplicationContext().getContentResolver().unregisterContentObserver(mSettingsContentObserver);
        if (mainController != null) {
            mainController.cancelAutoPlayTimer();
        }
    }

    private void startRepeatingTask() {
        handler.post(sendPlayerEventsTask);
    }

    private void stopRepeatingTask() {
        handler.removeCallbacks(sendPlayerEventsTask);
    }


    public void changeVideo(String videoUrl) {

        /*mainController.setContentVideo(videoUrl);
//        mainController.requestAndPlayAds();
        if (!mainController.isAddCompleted()) {
            return;
        }
        mainController.startContent();*/
    }

//    @Override
//    public void onPause() {
//        super.onPause();
//        try {
//            if (mainController != null) {
//                mainController.savePosition();
//                stopRepeatingTask();
//            }
//        } catch (Exception e) {
//        }
//    }
//
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        try {
//            if (mainController != null) {
//                mainController.restorePosition();
//                startRepeatingTask();
//            }
//        } catch (Exception e) {
//        }
//    }


    @Override
    public void onPause() {
        super.onPause();
        pauseVideo(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeVideo();
    }


    public void pauseVideo(boolean isAutoPause) {
        try {
            if (mainController != null) {
                mainController.pauseVideo(isAutoPause);
            }
        } catch (Exception e) {
        }
    }

    public void resumeVideo() {
        try {
            if (mainController != null) {
                mainController.resumeVideo();
            }
        } catch (Exception e) {
        }
    }

//    public void pauseVideo() {
//        try {
//            if (mainController != null) {
//                mainController.setAutoPause(true);
//                mainController.savePosition();
//                stopRepeatingTask();
//            }
//        } catch (Exception e) {
//        }
//    }

//    public void resumeVideo(boolean isAutoPlay) {
//        if (mainController.isAutoPause()) {
//            try {
//                if (mainController != null) {
//                    mainController.setAutoPause(false);
//                    mainController.restorePosition();
//                    startRepeatingTask();
//                }
//            } catch (Exception e) {
//            }
//        } else {
//            if (isAutoPlay) {
//                mainController.requestAndPlayAds();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mainController.reDrawControls();
//                    }
//                }, 500);
//            }
//        }
//    }

    public void hideShareButton() {
        try {
            if (mainController != null) {
                mainController.setAutoPause(false);
                mainController.restorePosition();
                startRepeatingTask();
            }
        } catch (Exception e) {
        }
    }

    public void redrawControls() {
        if (mainController != null) {
            mainController.reDrawControls();
        }
    }

    public PlayerControllerCallbackReference getPlayerControllerCallbackReference() {
        if (mainController == null) {
            return null;
        }
        return mainController.getPlayerControllerCallbackReference();
    }

    private void replayVideo() {
        if (mainController == null) {
            return;
        }
        mainController.resumeContent();
    }
}
