package com.db.dbvideoPersonalized;

/**
 * Created by DB on 26-10-2017.
 */

public interface OnAddPlaylistListener {
    boolean onAddToPlaylist();
}
