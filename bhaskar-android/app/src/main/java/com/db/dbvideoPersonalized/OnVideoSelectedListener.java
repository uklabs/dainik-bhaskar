package com.db.dbvideoPersonalized;

import com.db.dbvideoPersonalized.data.VideoInfo;

public interface OnVideoSelectedListener {
    void onVideoSelected(VideoInfo videosInfo, String source, int position);
}
