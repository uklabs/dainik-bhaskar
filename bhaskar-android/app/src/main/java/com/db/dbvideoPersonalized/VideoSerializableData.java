package com.db.dbvideoPersonalized;

import android.content.Context;

import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.data.VideoPlaylistInfo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Vector;

/**
 * Created by DB on 25-10-2017.
 */

public class VideoSerializableData implements Serializable {

    private static final String FILE_NAME = "VideoData";

    private static VideoSerializableData serializeData;
    private static Context mContext;

    // add to playlist videos
    private Vector<VideoPlaylistInfo> videoPlayList;

    // fetch videos info
    private Vector<VideoInfo> videoList;
    private int videoPageCount;
    private long videoUpdateTime;

    private VideoSerializableData() {
        videoPlayList = new Vector<>(2, 1);
        videoList = new Vector<>(20, 10);
    }

    // create instance
    public static VideoSerializableData getInstance(Context context) {
        VideoSerializableData.mContext = context;
        if (serializeData == null) {
            synchronized (VideoSerializableData.class) {
                VideoSerializableData fileInstance = getFileInstance(context);
                if (fileInstance == null) {
                    if (serializeData == null) {
                        serializeData = new VideoSerializableData();

                        save(context);
                    }
                } else {
                    serializeData = fileInstance;
                }
            }
        }
        return serializeData;
    }

    private synchronized static void save(Context context) {
        try {
            if (context != null) {
                FileOutputStream fileOut = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(serializeData);
                out.close();
                fileOut.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private synchronized static VideoSerializableData getFileInstance(Context context) {
        VideoSerializableData serializeData = null;
        try {
            if (context != null) {
                FileInputStream fileIn = context.openFileInput(FILE_NAME);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                serializeData = (VideoSerializableData) in.readObject();
                in.close();
                fileIn.close();
            }
        } catch (Exception i) {
        }

        return serializeData;
    }

    public boolean addVideoIntoPlaylist(VideoInfo videoInfo, String detailUrl, String feedUrl, String gaArticle, String gaEventLabel, boolean isPersonalizedFeedUrl) {
        boolean added = false;
        VideoPlaylistInfo videoPlaylistInfo = new VideoPlaylistInfo();
        videoPlaylistInfo.videoInfo = videoInfo;
        videoPlaylistInfo.detailUrl = detailUrl;
        videoPlaylistInfo.feedUrl = feedUrl;
        videoPlaylistInfo.gaArticle = gaArticle;
        videoPlaylistInfo.gaEventLabel = gaEventLabel;
        videoPlaylistInfo.isPersonalizedFeedUrl = isPersonalizedFeedUrl;

        if (!videoPlayList.contains(videoPlaylistInfo)) {
            videoPlayList.add(videoPlaylistInfo);
            added = true;
        }

        save(mContext);

        return added;
    }

    public Vector<VideoPlaylistInfo> getVideoPlaylist() {
        return videoPlayList;
    }

    public boolean isVideoAddedIntoPlaylist(String videoId) {
        return videoPlayList.contains(videoId);
    }

    public void removeVideoFromPlaylist(VideoPlaylistInfo videoId) {
        videoPlayList.remove(videoId);
        save(mContext);
    }

    public void addVideoList(Vector<VideoInfo> list, int pageCount) {
        videoList = list;
        videoPageCount = pageCount;
        videoUpdateTime = System.currentTimeMillis();

        save(mContext);
    }

    public Vector<VideoInfo> getVideoList() {
        return videoList;
    }

    public int getVideoPageCount() {
        return videoPageCount;
    }

    public long getVideoUpdatedTime() {
        return videoUpdateTime;
    }
}
