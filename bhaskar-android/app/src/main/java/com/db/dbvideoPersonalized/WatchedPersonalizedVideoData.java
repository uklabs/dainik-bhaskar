package com.db.dbvideoPersonalized;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class WatchedPersonalizedVideoData implements Serializable {

    private static final long TWO_DAYS_IN_MILLIS = 172800000L;

    private static final String FILE_NAME = "WatchedPersonalizedVideo";
    private static WatchedPersonalizedVideoData serializeData;
    private static Context mContext;

    private HashMap<String, Long> videoList;

    private WatchedPersonalizedVideoData() {
        videoList = new HashMap<>();
    }

    // create instance
    public static WatchedPersonalizedVideoData getInstance(Context context) {
        WatchedPersonalizedVideoData.mContext = context;

        if (serializeData == null) {
            synchronized (WatchedPersonalizedVideoData.class) {
                WatchedPersonalizedVideoData fileInstance = getFileInstance(context);
                if (fileInstance == null) {
                    if (serializeData == null) {
                        serializeData = new WatchedPersonalizedVideoData();

                        save(context);
                    }
                } else {
                    serializeData = fileInstance;
                }
            }
        }
        return serializeData;
    }

    private synchronized static void save(Context context) {
        try {
            if (context != null) {
                FileOutputStream fileOut = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(serializeData);
                out.close();
                fileOut.close();
            }
        } catch (Exception e) {
        }
    }

    private synchronized static WatchedPersonalizedVideoData getFileInstance(Context context) {
        WatchedPersonalizedVideoData serializeData = null;
        try {
            if (context != null) {
                FileInputStream fileIn = context.openFileInput(FILE_NAME);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                serializeData = (WatchedPersonalizedVideoData) in.readObject();
                in.close();
                fileIn.close();
            }
        } catch (Exception i) {
        }

        return serializeData;
    }

    public void saveVideo(String videoId) {
        videoList.put(videoId, System.currentTimeMillis());
        save(mContext);
    }

    public ArrayList<String> getVideoList() {
        return new ArrayList<>(videoList.keySet());
    }

    public boolean hasVideo(String videoId) {
        if (videoList.containsKey(videoId)) {
            if (System.currentTimeMillis() > (videoList.get(videoId) + TWO_DAYS_IN_MILLIS)) {
                videoList.remove(videoId);
                save(mContext);

            } else {
                return true;
            }
        }

        return false;
    }
}
