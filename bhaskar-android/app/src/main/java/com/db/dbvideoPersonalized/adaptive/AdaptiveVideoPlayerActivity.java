package com.db.dbvideoPersonalized.adaptive;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdsConstants;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.db.dbvideo.player.AppTraverseUtil;
import com.db.dbvideo.player.BhaskarVideoPlayerTrackerUtil;
import com.db.dbvideo.player.PlayerControllerCallbackReference;
import com.db.dbvideo.player.VideoPlayerConstant;
import com.db.dbvideo.player.VideoPlayerEventTracker;
import com.db.dbvideo.videoview.VideoPlayerCallbackListener;
import com.db.dbvideo.videoview.VideoPlayerFragment;
import com.db.dbvideoPersonalized.OnVideoSelectedListener;
import com.db.dbvideoPersonalized.WatchedPersonalizedVideoData;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.DailyMotionPlayerFragment;
import com.db.dbvideoPersonalized.detail.VideoPlayerActivity;
import com.db.dbvideoPersonalized.detail.VideoPlayerListFragment;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;

/**
 * Created by USNews on 8/11/2019.
 */
public class AdaptiveVideoPlayerActivity extends AppCompatActivity implements OnVideoCommandsListener, OnVideoSelectedListener, VideoPlayerCallbackListener {

    private static final CookieManager DEFAULT_COOKIE_MANAGER;

    static {
        DEFAULT_COOKIE_MANAGER = new CookieManager();
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    public VideoInfo selectedVideoInfo;
    private Handler handler = new Handler();

    private AdaptiveVideoPlayerFragment videoPlayerFragment;
    private VideoPlayerListFragment dbPlayerListFragment;

    private RelativeLayout bottomLayout;

    private int videoIndex;
    private int nextPageCount;
    private String feedUrl;
    private String videoSource;
    private String gaEventLabel;
    private String gaArticle;
    private String sectionLabel;
    private String mColor;
    private boolean clickViaNotification = false;

    private boolean isAutoPlay = true;
    private boolean isFullView;
    private int userSelectedOrientation = -1;

    private OrientationEventListener orientation;
    private int layoutType;
    private int themeType;
    public static ArrayList<VideoInfo> list = new ArrayList<>();

    public static Intent getIntent(Context context, int position, int nextPageCount, String feedUrl, String source, String gaEventLabel, String gaArticle, String sectionLabel, String color, int layoutType, int themeType) {
        Intent intent = new Intent(context, AdaptiveVideoPlayerActivity.class);
        try {
            intent.putExtra(Constants.KeyPair.KEY_POSITION, position);
            intent.putExtra(Constants.KeyPair.KEY_NEXT_PAGE_COUNT, nextPageCount);
            intent.putExtra(Constants.KeyPair.KEY_FEED_URL, feedUrl);
            intent.putExtra(Constants.KeyPair.KEY_SOURCE, source);
            intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
            intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
            intent.putExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL, sectionLabel);
            intent.putExtra(Constants.KeyPair.KEY_COLOR, color);
            intent.putExtra("layer", layoutType);
            intent.putExtra("theme", themeType);
        } catch (Exception ignored) {
        }

        return intent;
    }

    public static void addListData(ArrayList<VideoInfo> filteredList) {
        list.clear();
        list.addAll(filteredList);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }
        setContentView(R.layout.activity_video_player);

        bottomLayout = findViewById(R.id.bottom_layout);

        initViews();
        AppUtils.getInstance().setStatusBarColor(this, mColor);
    }

    private void initViews() {
        Intent intent = getIntent();
        if (intent != null) {
            Bundle extraBundle = intent.getExtras();
            if (extraBundle != null) {
                videoIndex = extraBundle.getInt(Constants.KeyPair.KEY_POSITION);
                nextPageCount = extraBundle.getInt(Constants.KeyPair.KEY_NEXT_PAGE_COUNT, 1);
                feedUrl = extraBundle.getString(Constants.KeyPair.KEY_FEED_URL);
                videoSource = extraBundle.getString(Constants.KeyPair.KEY_SOURCE);
                gaEventLabel = extraBundle.getString(Constants.KeyPair.KEY_GA_EVENT_LABEL);
                gaArticle = extraBundle.getString(Constants.KeyPair.KEY_GA_ARTICLE);
                sectionLabel = intent.getStringExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL);
                mColor = extraBundle.getString(Constants.KeyPair.KEY_COLOR, "");
                layoutType = extraBundle.getInt("layer", VideoPlayerListFragment.TYPE_GRID);
                themeType = extraBundle.getInt("theme", VideoPlayerListFragment.THEME_DARK);

                clickViaNotification = extraBundle.getBoolean(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION);
            }
        }

        if (list != null) {
            if (list.size() > videoIndex) {
                selectedVideoInfo = list.get(videoIndex);
                list.remove(videoIndex);
            } else if (list.size() > 0) {
                selectedVideoInfo = list.get(0);
                list.remove(0);
                videoIndex = 0;
            }
        }

        if (selectedVideoInfo != null) {
            // add video fragment
            addVideoPlayerFragment(selectedVideoInfo.internalVideo, selectedVideoInfo.image, selectedVideoInfo.title, selectedVideoInfo.link);

            // add recommendation list fragment
            dbPlayerListFragment = (VideoPlayerListFragment) getSupportFragmentManager().findFragmentById(R.id.player_list_fragment);
            dbPlayerListFragment.setData(list, layoutType, themeType, nextPageCount, feedUrl, mColor);
            dbPlayerListFragment.setVideo(selectedVideoInfo, null);
        }

        orientation = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int i) {

                if (Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1) {
                    if ((i > 85 && i < 95) || (i > 265 && i < 275)) {
                        if (userSelectedOrientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                            if (getResources().getConfiguration().orientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                            }
                            userSelectedOrientation = -1;
                        }

                    } else if ((i > 350 && i <= 360) || (i > 170 && i < 190)) {
                        if (userSelectedOrientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                            if (getResources().getConfiguration().orientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                            }
                            userSelectedOrientation = -1;
                        }
                    }
                }
            }
        };
    }

    private void addVideoPlayerFragment(String videoUrl, String imageUrl, String title, String shareLink) {
        // video fragment
        videoPlayerFragment = AdaptiveVideoPlayerFragment.newInstance();

        Bundle bundle = new Bundle();
        bundle.putSerializable("url", videoUrl);
        videoPlayerFragment.setArguments(bundle);

        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.videoContainer, videoPlayerFragment).commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }
    }

    @Override
    public void onVideoSelected(VideoInfo dbVideosInfo, String source, int position) {
        if (dbVideosInfo != null) {
            VideoInfo preVideoInfo = null;
            try {
                preVideoInfo = (VideoInfo) selectedVideoInfo.clone();
            } catch (CloneNotSupportedException e) {
            }
            selectedVideoInfo = dbVideosInfo;
            videoSource = source;
            videoIndex = position;

            dbPlayerListFragment.setVideo(selectedVideoInfo, preVideoInfo);

            addVideoPlayerFragment(dbVideosInfo.internalVideo, dbVideosInfo.image, dbVideosInfo.title, dbVideosInfo.link);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Systr.println("On Configuration changed called");

        hideUnhideDetailLayout();
    }

    @Override
    public void onOrientationChange() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            hideStatusBar();
        }
    }

    @Override
    public void onRequestToChangeOrientation() {
        Systr.println("change orientation called");

        if (selectedVideoInfo != null && selectedVideoInfo.portraitVideo == 1) {
            fullPortraitLayout();

        } else {
            if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
                System.out.println("Change orientation called land - port");
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                userSelectedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            } else {
                System.out.println("Change orientation called port - land");
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                userSelectedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
            }
        }
    }

    private void hideUnhideDetailLayout() {
        int orientation = getResources().getConfiguration().orientation;
        switch (orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                showVideoDetailView();
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                hideVideoDetailView();
                break;
        }
    }

    // Hide Status Bar
    private void hideStatusBar() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    //Show Status Bar
    private void showStatusBar() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
    }

    private void hideVideoDetailView() {
        hideStatusBar();

        bottomLayout.setVisibility(View.GONE);
        if (videoPlayerFragment != null) {
            videoPlayerFragment.setFullScreen(true);
        }
    }

    private void showVideoDetailView() {
        showStatusBar();

        bottomLayout.setVisibility(View.VISIBLE);
        if (videoPlayerFragment != null) {
            videoPlayerFragment.setFullScreen(false);
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        } else if (isFullView) {
            fullPortraitLayout();

        } else {
            if (clickViaNotification) {
                Intent intent = new Intent(AdaptiveVideoPlayerActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent();
                intent.putExtra("video_update", selectedVideoInfo);
                setResult(100, intent);
                finish();
            }
        }
    }

    private void fullPortraitLayout() {
        if (isFullView) {
            isFullView = false;
            showVideoDetailView();
        } else {
            isFullView = true;
            hideVideoDetailView();
        }

//        if (videoPlayerFragment != null) {
//            videoPlayerFragment.changeVideoViewScreen();
//        }
    }

    @Override
    public void onVideoStart() {
        if (selectedVideoInfo != null) {
            WatchedPersonalizedVideoData.getInstance(AdaptiveVideoPlayerActivity.this).saveVideo(AppUtils.getFilteredStoryId(selectedVideoInfo.storyId));
        }
    }

    @Override
    public void onVideoComplete() {
        return;
    }

    @Override
    public void onVideoError() {
    }

    @Override
    public void onNextButtonclick() {
        Systr.println("Next Button Called");
        if (dbPlayerListFragment != null) {
            VideoInfo firstItem = dbPlayerListFragment.getFirstItem();
            if (firstItem != null) {
                videoSource = AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION;
                onVideoSelected(firstItem, videoSource, 0);

                Tracking.trackVideoImpression(AdaptiveVideoPlayerActivity.this, selectedVideoInfo.impressionTrackUrl, 0, 1, "auto_click", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);
            }
        }
    }

    private void setTrackingData(VideoInfo dbVideosInfo, String videoUrl) {
        if (TextUtils.isEmpty(gaEventLabel)) {
            gaEventLabel = CommonConstants.EVENT_LABEL;
        }
        AppTraverseUtil.setLanguage(gaEventLabel);
        AppTraverseUtil.setVideoTitle(dbVideosInfo.mediaTitle);
        AppTraverseUtil.setStoryId(dbVideosInfo.storyId);
        AppTraverseUtil.setBaseSectionSubSectionValue(sectionLabel);
        AppTraverseUtil.setVideoSource(videoSource);
        AppTraverseUtil.setVideoIndex(videoIndex + 1);

        BhaskarVideoPlayerTrackerUtil.setScreenName(AppTraverseUtil.getScreenNameForVideoTracking());
        String ultimaTrackingUrlWithParameter = dbVideosInfo.ultimaTrackUrl + "&http_referer=" + dbVideosInfo.link;
        VideoPlayerEventTracker videoPlayerEventTracker = VideoPlayerEventTracker.getInstance(this, videoUrl, ultimaTrackingUrlWithParameter, dbVideosInfo.storyId);
        BhaskarVideoPlayerTrackerUtil.setPlayerEventCallBackReference(videoPlayerEventTracker);

        // Tracking
        String source = AppPreferences.getInstance(AdaptiveVideoPlayerActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(AdaptiveVideoPlayerActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(AdaptiveVideoPlayerActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(AdaptiveVideoPlayerActivity.this, InitApplication.getInstance().getDefaultTracker(), gaArticle + (TextUtils.isEmpty(dbVideosInfo.mediaTitle) ? "" : ("-" + dbVideosInfo.mediaTitle)), source, medium, campaign);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (orientation != null) {
            orientation.enable();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (orientation != null) {
            orientation.disable();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        list.clear();
    }
}
