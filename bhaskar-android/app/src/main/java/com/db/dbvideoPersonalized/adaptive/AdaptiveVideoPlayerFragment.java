package com.db.dbvideoPersonalized.adaptive;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.bhaskar.R;
import com.db.InitApplication;
import com.db.dbvideo.player.VideoPlayerConstant;
import com.db.util.Constants;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.util.ErrorMessageProvider;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;

public class AdaptiveVideoPlayerFragment extends Fragment implements PlayerControlView.VisibilityListener, PlaybackPreparer, View.OnClickListener {

    public static AdaptiveVideoPlayerFragment newInstance() {
        AdaptiveVideoPlayerFragment fragment = new AdaptiveVideoPlayerFragment();
        return fragment;
    }

    private View replayLayout;
    private View nextPlayLayout;
    private View errorLayout;
    private ImageView nextButton;

    private ProgressBar progressBar;

    private PlayerView playerView;

    private DataSource.Factory dataSourceFactory;
    private SimpleExoPlayer player;
    private MediaSource mediaSource;
    private DefaultTrackSelector trackSelector;
    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private TrackGroupArray lastSeenTrackGroupArray;

    private boolean startAutoPlay = true;
    private int startWindow;
    private long startPosition;


    private boolean showNextButton;
    private boolean isHideShare;
    private boolean isAutoPlay;
    private boolean isAutoNextPlay;

    private String mVideoUrl;
    private String title;
    private String mediaTitle;
    private String storyId;
    private String gTrackUrl;
    private String ultimaTrackUrl;
    private String link;
    private String imagePath;
    private int isLive;

    private String mVideoSource;
    private String gaEventLabel;
    private int videoIndex;
    private String sectionLabel;

    private OnVideoCommandsListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (OnVideoCommandsListener) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataSourceFactory = buildDataSourceFactory();

        getData();
    }

    private void getData() {
        Bundle bundle = getArguments();

        showNextButton = bundle.getBoolean(VideoPlayerConstant.SHOW_NEXT_BUTTON, false);
        isHideShare = bundle.getBoolean(VideoPlayerConstant.HIDE_SHARE_BUTTON);
        isAutoPlay = bundle.getBoolean(VideoPlayerConstant.IS_AUTO_PLAY, false);
        isAutoNextPlay = bundle.getBoolean(VideoPlayerConstant.IS_AUTO_NEXT_PLAY, false);

        mVideoUrl = bundle.getString(VideoPlayerConstant.VIDEO_URL);
        title = bundle.getString(VideoPlayerConstant.SHARE_TITLE);
        mediaTitle = bundle.getString(VideoPlayerConstant.MEDIA_TITLE);
        link = bundle.getString(VideoPlayerConstant.SHARE_LINK);
        gTrackUrl = bundle.getString(Constants.KeyPair.KEY_GA_G_TRACK_URL);
        isLive = bundle.getInt(VideoPlayerConstant.VIDEO_IS_LIVE, 0);
        ultimaTrackUrl = bundle.getString(VideoPlayerConstant.ULTIMA_TRACK_URL);
        imagePath = bundle.getString(VideoPlayerConstant.IMAGE_PATH);
        storyId = bundle.getString(VideoPlayerConstant.VIDEO_ID);

        sectionLabel = bundle.getString(VideoPlayerConstant.SECTION_LABEL);
        gaEventLabel = bundle.getString(VideoPlayerConstant.GA_EVENT_LABEL);
        mVideoSource = bundle.getString(VideoPlayerConstant.VIDEO_SOURCE);
        videoIndex = bundle.getInt(VideoPlayerConstant.VIDEO_INDEX, -1);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_detail_view, container, false);

        progressBar = view.findViewById(R.id.progressBar);

        replayLayout = view.findViewById(R.id.replay_layout);
        nextPlayLayout = view.findViewById(R.id.next_play_control_layout);
        errorLayout = view.findViewById(R.id.error_layout);
        nextButton = view.findViewById(R.id.next);

        playerView = view.findViewById(R.id.player_view);
        playerView.setControllerVisibilityListener(this);
        playerView.setErrorMessageProvider(new PlayerErrorMessageProvider());
        playerView.requestFocus();

        ((ImageView) playerView.findViewById(R.id.full_screen)).setImageResource(R.drawable.dbv_ic_fullscreen);
        playerView.findViewById(R.id.full_screen).setOnClickListener(this);
        playerView.setUseController(false);
        trackSelectorParameters = new DefaultTrackSelector.ParametersBuilder().build();
        clearStartPosition();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.replay).setOnClickListener(this);
        view.findViewById(R.id.next).setOnClickListener(this);
        view.findViewById(R.id.btn_retry).setOnClickListener(this);
        view.findViewById(R.id.iv_video_replay).setOnClickListener(this);
        view.findViewById(R.id.iv_auto_next_play_view).setOnClickListener(this);
        view.findViewById(R.id.cancel_button_next_auto_play).setOnClickListener(this);
        view.findViewById(R.id.iv_play_icon_upon_thumb).setOnClickListener(this);
    }

    private void resetLayout() {
        replayLayout.setVisibility(View.GONE);
        nextPlayLayout.setVisibility(View.GONE);
        errorLayout.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.full_screen:
                if (listener != null) {
                    listener.onRequestToChangeOrientation();
                }
                break;

            case R.id.replay:
//                replayVideo();
                break;

            case R.id.cancel_button_next_auto_play:
                nextPlayLayout.setVisibility(View.GONE);
                replayLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
            if (playerView != null) {
                playerView.onResume();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Util.SDK_INT <= 23 || player == null) {
            initializePlayer();
            if (playerView != null) {
                playerView.onResume();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            if (playerView != null) {
                playerView.onPause();
            }
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            if (playerView != null) {
                playerView.onPause();
            }
            releasePlayer();
        }
    }

    @Override
    public void onVisibilityChange(int visibility) {

    }

    @Override
    public void preparePlayback() {
        player.retry();
    }

    private void initializePlayer() {
        if (player == null) {
            Uri[] uris = new Uri[]{Uri.parse(mVideoUrl)};
            String[] extensions = new String[]{""};

            if (!Util.checkCleartextTrafficPermitted(uris)) {
//                showToast(R.string.error_cleartext_not_permitted);
                return;
            }

            TrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory();
            RenderersFactory renderersFactory = ((InitApplication) getActivity().getApplication()).buildRenderersFactory(false);

            trackSelector = new DefaultTrackSelector(trackSelectionFactory);
            trackSelector.setParameters(trackSelectorParameters);
            lastSeenTrackGroupArray = null;

//            DefaultAllocator defaultAllocator = new DefaultAllocator(true, DEFAULT_BUFFER_SEGMENT_SIZE);
//            DefaultLoadControl loadControl = new DefaultLoadControl(defaultAllocator, 1500, 2000, 1000, 1500, -1, true);
            DefaultLoadControl.Builder loadControl = new DefaultLoadControl.Builder();
            loadControl.setBufferDurationsMs(1500, 2000, 1000, 1500);

//            player = ExoPlayerFactory.newSimpleInstance(getContext(), new DefaultRenderersFactory(getContext()), new DefaultTrackSelector(), new DefaultLoadControl());
            player = ExoPlayerFactory.newSimpleInstance(getContext(), renderersFactory, trackSelector, loadControl.createDefaultLoadControl());
            player.addListener(new PlayerEventListener());
            player.setPlayWhenReady(startAutoPlay);
            player.addAnalyticsListener(new EventLogger(trackSelector));
            playerView.setPlayer(player);
            playerView.setPlaybackPreparer(this);

            MediaSource[] mediaSources = new MediaSource[uris.length];
            for (int i = 0; i < uris.length; i++) {
                mediaSources[i] = buildMediaSource(uris[i], extensions[i]);
            }
            mediaSource = mediaSources.length == 1 ? mediaSources[0] : new ConcatenatingMediaSource(mediaSources);
        }

        boolean haveStartPosition = startWindow != C.INDEX_UNSET;
        if (haveStartPosition) {
//            player.seekTo(startWindow, startPosition);
        }
//        player.prepare(mediaSource, !haveStartPosition, false);
        player.prepare(mediaSource, true, false);
    }

    public void setFullScreen(boolean isShowing) {
        if (isShowing) {
            ((ImageView) playerView.findViewById(R.id.full_screen)).setImageResource(R.drawable.ic_video_fullscreen_exit);
        } else {
            ((ImageView) playerView.findViewById(R.id.full_screen)).setImageResource(R.drawable.ic_video_fullscreen);
        }
    }

    private void replayVideo() {
        resetLayout();
        clearStartPosition();
        if (player != null) {
            player.seekTo(0, 0);
            player.setPlayWhenReady(true);
        }
    }

    private void releasePlayer() {
        if (player != null) {
            updateTrackSelectorParameters();
            updateStartPosition();
            player.release();
            player = null;
            mediaSource = null;
            trackSelector = null;
        }
    }

    private void updateTrackSelectorParameters() {
        if (trackSelector != null) {
            trackSelectorParameters = trackSelector.getParameters();
        }
    }

    private void updateStartPosition() {
        if (player != null) {
            startAutoPlay = player.getPlayWhenReady();
            startWindow = player.getCurrentWindowIndex();
            startPosition = Math.max(0, player.getContentPosition());
        }
    }

    private void clearStartPosition() {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }

    private DataSource.Factory buildDataSourceFactory() {
        return ((InitApplication) getActivity().getApplication()).buildDataSourceFactory();
    }

    private MediaSource buildMediaSource(Uri uri, @Nullable String overrideExtension) {
        @C.ContentType int type = Util.inferContentType(uri, overrideExtension);
        switch (type) {
//            case C.TYPE_HLS:
//                return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
//            case C.TYPE_SS:
//                return new SsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }

    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    private class PlayerEventListener implements Player.EventListener {

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (playWhenReady) {
                switch (playbackState) {
                    case ExoPlayer.STATE_READY:
                        keepScreenOnOff(true);

                        playerView.setUseController(true);
                        progressBar.setVisibility(View.GONE);
                        break;

                    case ExoPlayer.STATE_BUFFERING:
                        keepScreenOnOff(true);
                        progressBar.setVisibility(View.VISIBLE);
                        break;

                    case ExoPlayer.STATE_ENDED:
                        endVideo();
                        break;

                    case ExoPlayer.STATE_IDLE:
                        keepScreenOnOff(false);
                        progressBar.setVisibility(View.GONE);
                        break;
                }
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onPlayerError(ExoPlaybackException e) {
            if (isBehindLiveWindow(e)) {
                clearStartPosition();
                initializePlayer();
            } else {
//                showControls();
            }
        }

        @Override
        @SuppressWarnings("ReferenceEquality")
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            if (trackGroups != lastSeenTrackGroupArray) {
                MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
                if (mappedTrackInfo != null) {
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO) == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
//                        showToast(R.string.error_unsupported_video);
                    }
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_AUDIO) == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
//                        showToast(R.string.error_unsupported_audio);
                    }
                }
                lastSeenTrackGroupArray = trackGroups;
            }
        }
    }

    private void keepScreenOnOff(boolean isOn) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            if (isOn) {
                activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            } else {
                activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        }
    }

    private void endVideo() {
        keepScreenOnOff(false);

        playerView.setUseController(false);

        progressBar.setVisibility(View.GONE);

        replayLayout.setVisibility(View.VISIBLE);
        if (showNextButton) {
            nextButton.setVisibility(View.VISIBLE);
        } else {
            nextButton.setVisibility(View.GONE);
        }
    }

    private class PlayerErrorMessageProvider implements ErrorMessageProvider<ExoPlaybackException> {

        @Override
        public Pair<Integer, String> getErrorMessage(ExoPlaybackException e) {
            String errorString = "";
//            String errorString = getString(R.string.error_generic);

            if (e.type == ExoPlaybackException.TYPE_RENDERER) {
                Exception cause = e.getRendererException();
                if (cause instanceof MediaCodecRenderer.DecoderInitializationException) {
                    MediaCodecRenderer.DecoderInitializationException decoderInitializationException = (MediaCodecRenderer.DecoderInitializationException) cause;
                    if (decoderInitializationException.decoderName == null) {
                        if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
//                            errorString = getString(R.string.error_querying_decoders);
                        } else if (decoderInitializationException.secureDecoderRequired) {
//                            errorString = getString(R.string.error_no_secure_decoder, decoderInitializationException.mimeType);
                        } else {
//                            errorString = getString(R.string.error_no_decoder, decoderInitializationException.mimeType);
                        }
                    } else {
//                        errorString = getString(R.string.error_instantiating_decoder, decoderInitializationException.decoderName);
                    }
                }
            }

            return Pair.create(0, errorString);
        }
    }
}
