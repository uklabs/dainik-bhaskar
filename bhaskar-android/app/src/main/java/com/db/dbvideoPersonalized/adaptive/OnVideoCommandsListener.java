package com.db.dbvideoPersonalized.adaptive;

/**
 * Created by DB on 13-10-2017.
 */

public interface OnVideoCommandsListener {
    void onOrientationChange();

    void onRequestToChangeOrientation();
}
