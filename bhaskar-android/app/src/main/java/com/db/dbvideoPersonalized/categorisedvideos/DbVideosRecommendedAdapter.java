package com.db.dbvideoPersonalized.categorisedvideos;

import android.content.Context;
import android.graphics.PorterDuff;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.video.VideoAdapter;
import com.db.homebanner.BannerViewHolder;
import com.db.util.AppUtils;
import com.db.util.ImageUtil;

import java.util.List;
import java.util.Vector;

public class DbVideosRecommendedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_LOADING = 2;
    public static final int VIEW_TYPE_BANNER = 3;

    public static boolean isHomeBannerLoad;

    private Vector<VideoInfo> videoList;
    private Context mContext;
    VideoAdapter.OnVideoActionListener mListener;

    DbVideosRecommendedAdapter(Context context, VideoAdapter.OnVideoActionListener mListener) {
        mContext = context;
        videoList = new Vector<>();
        this.mListener = mListener;
    }

    void addAndClear(List<VideoInfo> list) {
        videoList.clear();
        videoList.addAll(list);
        notifyDataSetChanged();
    }

    public void add(Vector<VideoInfo> list) {
        for (int i = 0; i < list.size(); i++) {
            if (!videoList.contains(list.get(i))) {
                videoList.add(list.get(i));
            }
        }
        notifyDataSetChanged();
    }

    void addInFirst(Vector<VideoInfo> list) {
        for (int i = list.size() - 1; i >= 0; i--) {
            if (!videoList.contains(list.get(i))) {
                videoList.add(0, list.get(i));
            }
        }
        notifyDataSetChanged();
    }


    void updateVideoInfo(VideoInfo videoInfo) {
        int i = videoList.indexOf(videoInfo);
        if (i >= 0 && i < videoList.size()) {
            videoList.set(i, videoInfo);
        }
        notifyDataSetChanged();
    }

    void deleteVideo(int index) {
        videoList.remove(index);
        notifyDataSetChanged();
    }


    void addNullItem() {
        videoList.add(null);
        notifyDataSetChanged();
    }

    void removeNullItem() {
        if (videoList != null && !videoList.isEmpty() && videoList.get(videoList.size() - 1) == null) {
            videoList.remove(videoList.size() - 1);
            notifyDataSetChanged();
        }
    }

    VideoInfo getItemAtPosition(int position) {
        if (position >= 0 && position < videoList.size())
            return videoList.get(position);
        return null;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dbvideos_videos_grid_list_item, parent, false);
                return new CategorySectionItemViewHolder(itemView);
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.in_app_home_banner, parent, false));
        }
        return null;
    }

    String displayName = "personalised_video";

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
                return;
            case VIEW_TYPE_ITEM:
                if (holder instanceof CategorySectionItemViewHolder) {
                    CategorySectionItemViewHolder viewHolder = (CategorySectionItemViewHolder) holder;
                    VideoInfo info = videoList.get(position);
                    if (info == null) {
                        return;
                    }

                    ImageUtil.setImage(mContext, info.image, viewHolder.videoIV, R.drawable.water_mark_news_list);

                    viewHolder.videoTitleTV.setText(info.title);
                    if (!TextUtils.isEmpty(info.views)) {
                        viewHolder.videoViewLL.setVisibility(View.VISIBLE);
                        viewHolder.videoViewsTV.setText(AppUtils.getInstance().getSIPrefixForCount(info.views));
                    } else {
                        viewHolder.videoViewLL.setVisibility(View.GONE);
                    }
                }
                break;
//            case VIEW_TYPE_HOME_BANNER:
//                if (!isHomeBannerLoad) {
//                    isHomeBannerLoad = true;
//                    ((HomeBannerViewHolder) holder).progressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
//                    HomeBannerUtils.getHomeBannerUtils(mContext).callwebViewOrSetWidgetData((HomeBannerViewHolder) holder, this);
//                }
        }
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (videoList.get(position) == null)
            return VIEW_TYPE_LOADING;
        else if (videoList.get(position) != null && videoList.get(position).type == VIEW_TYPE_BANNER)
            return VIEW_TYPE_BANNER;
        else
            return VIEW_TYPE_ITEM;
    }

    Vector<VideoInfo> getVideoList() {
        return videoList;
    }

    //View Holders
    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        private LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }

    class CategorySectionItemViewHolder extends RecyclerView.ViewHolder {

        ImageView videoIV;
        TextView videoViewsTV;
        TextView videoTitleTV;
        LinearLayout videoViewLL;

        CategorySectionItemViewHolder(View view) {
            super(view);
            videoIV = view.findViewById(R.id.video_item_image);
            videoViewLL = view.findViewById(R.id.ll_video_view_count);
            videoViewsTV = view.findViewById(R.id.dbvideos_video_view_count_tv);
            videoTitleTV = view.findViewById(R.id.dbvideos_video_title_name_tv);

            view.setOnClickListener(v -> {
                int adapterPosition = getAdapterPosition();

                if (adapterPosition >= 0 && adapterPosition < videoList.size() && mListener != null) {
                    mListener.onVideoSelect(adapterPosition);
                }
            });

        }
    }

}
