package com.db.dbvideoPersonalized.categorisedvideos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;
import com.bhaskar.util.CommonConstants;
import com.db.InitApplication;
import com.db.data.database.BooleanExpressionTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.models.CategoryInfo;
import com.db.dbvideo.player.BusProvider;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.data.VideoProviderInfo;
import com.db.dbvideoPersonalized.detail.DBVideoPlayerActivity2;
import com.db.dbvideoPersonalized.video.VideoAdapter;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class DbVideosSectionVideosFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, VideoAdapter.OnVideoActionListener {

    private View rootView;
    private DbVideosRecommendedAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isHomeBannerAdded = false;
    private boolean isLoading;
    private int visibleThreshold = 5;

    private int pageCount;
    private boolean isLoadMoreEnable = true;
    private CategoryInfo categoryInfo;
    private String sectionName;
    private InitApplication application;
    private boolean isShowing = true;
    private ArrayList<Integer> trackingList = new ArrayList<>();

    public DbVideosSectionVideosFragment() {
    }

    public static DbVideosSectionVideosFragment newInstance(String sectionName, CategoryInfo categoryInfo) {
        DbVideosSectionVideosFragment videoFragment = new DbVideosSectionVideosFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString("SectionName", sectionName);
        videoFragment.setArguments(bundle);
        return videoFragment;
    }

    private Rect scrollBounds;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            sectionName = bundle.getString("SectionName");
        }

        application = (InitApplication) getActivity().getApplication();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_recommended_video, container, false);

        isLoadMoreEnable = true;
        init();

        sendTrackingForGaScreen();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.scrollBounds = new Rect();
        this.recyclerView.getHitRect(scrollBounds);

        fetchVideoFromFirstPage();
        if (!AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.FOLLOW_LIST_UPDATE_FIRST_TIME, false)) {
//            String videoProvider = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.VideoPref.VALUE_VIDEO_PROVIDER, Urls.DEFAULT_VIDEO_PROVIDER);
            String videoProvider = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.VideoPref.SETTINGS_VIDEO_PROVIDER_URL, Urls.DEFAULT_VIDEO_PROVIDER);
            String mSessionId = TrackingData.getDbORDeviceId(getContext());
            String url = Urls.APP_FEED_BASE_URL + videoProvider + (mSessionId + "/");

            fetchFollowListFromServer(url);
        }
    }

    private void init() {
        mSwipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));

        // Recycler View
        recyclerView = rootView.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        // adapter create and set in recycler view
        adapter = new DbVideosRecommendedAdapter(getContext(), this);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {

                        if (isLoadMoreEnable) {
                            onLoadMore();
                            isLoading = true;
                        }
                    }
                }

                checkForImpressionTracking();
            }
        });
    }

    private ArrayList<String> impressionList = new ArrayList<>();

    private void checkForImpressionTracking() {
        if (recyclerView != null) {
            View childAt;
            int height;

            VideoInfo itemAtPosition;
            int childAdapterPosition;
            String trackStr;

            for (int i = 0; i < recyclerView.getChildCount(); i++) {
                try {
                    childAt = recyclerView.getChildAt(i);
                    height = childAt.getHeight();

                    if (childAt.getLocalVisibleRect(scrollBounds)) {
                        if (scrollBounds.bottom == height) {
                            childAdapterPosition = recyclerView.getChildAdapterPosition(childAt);
                            itemAtPosition = adapter.getItemAtPosition(childAdapterPosition);
                            trackStr = itemAtPosition.pageNumber + "-" + itemAtPosition.pageVideoIndex;
                            if (!impressionList.contains(trackStr)) {
                                Systr.println("Impression Tracking : " + childAdapterPosition + ", PageNo : " + itemAtPosition.pageNumber + ", Index : " + itemAtPosition.pageVideoIndex);
                                Tracking.trackVideoImpression(getContext(), itemAtPosition.impressionTrackUrl, itemAtPosition.pageNumber, itemAtPosition.pageVideoIndex, "impression", categoryInfo.displayName, categoryInfo.id, AppFlyerConst.DBVideosSource.VIDEO_LIST);
                                impressionList.add(trackStr);
                            }
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    private void sendTrackingForGaScreen() {
        // Tracking
        if (categoryInfo != null) {
            String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

            Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), categoryInfo.gaScreen, source, medium, campaign);
        }
    }

    public void moveToTop() {
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    public void onLoadMore() {
        Systr.println("on load more called");
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                adapter.addNullItem();
            }
        });

        //Load more data for recycler view
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                fetchVideosFromServer(false, false, false);
            }
        }, 500);
    }

    @Override
    public void onRefresh() {
        isLoadMoreEnable = true;
        DbVideosRecommendedAdapter.isHomeBannerLoad = false;
        pageCount = 0;
        fetchVideosFromServer(true, false, true);
    }

    private void onItemsLoadComplete() {
        Context context = getContext();
        if (context != null) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Stop refresh animation
                    if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
            });
        }
    }

    private void fetchVideoFromFirstPage() {
        pageCount = 0;

        fetchVideosFromServer(false, true, true);
    }

    private void fetchVideosFromServer(boolean isRefresh, boolean showProgress, boolean mClearOldList) {

        if (pageCount == 0)
            pageCount++;
        String url = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + ("PG" + pageCount + "/");
        AppLogs.printDebugLogs("DBVideosSectionVideosFragment", "URL : " + url);

        makeJsonObjectRequest(url, isRefresh, showProgress, mClearOldList);
    }

    private void makeJsonObjectRequest(String url, final boolean isRefresh, final boolean showProgress, final boolean clearOldList) {

        if (showProgress)
            rootView.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Systr.println("hVideo list fetc response Json : " + response);

                parseLiveVideoServerJson(response, isRefresh, clearOldList);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Video list fetch Error : " + error);

                rootView.findViewById(R.id.progress_bar).setVisibility(View.GONE);

                adapter.removeNullItem();

                onItemsLoadComplete();
                isLoading = false;
                Context context = getContext();
                if (error instanceof NetworkError && context != null) {
                    AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.internet_connection_error_));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void parseLiveVideoServerJson(JSONObject jsonObject, boolean isRefresh, boolean clearOldList) {
        Vector<VideoInfo> list = new Vector<>();

        int arrayLength = 0;
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("feed");
            arrayLength = jsonArray.length();

            list = new Vector<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), VideoInfo[].class)));

        } catch (Exception e) {
            e.printStackTrace();
        }

        VideoInfo vInfo;
        for (int i = 0; i < list.size(); i++) {
            vInfo = list.get(i);
            vInfo.pageNumber = pageCount;
            vInfo.pageVideoIndex = i + 1;
        }

        rootView.findViewById(R.id.progress_bar).setVisibility(View.GONE);

        adapter.removeNullItem();

        Systr.println("Clear old list : " + clearOldList + ", isRefresh : " + isRefresh + ", pageCount : " + pageCount);

        if (clearOldList) {
            adapter.addAndClear(list);

        } else {
            if (isRefresh) {
                adapter.addInFirst(list);
            } else {
                adapter.add(list);
            }
        }

        if (isRefresh) {
            onItemsLoadComplete();
        }
        Context context = getContext();
        if (context != null) {
            // Tracking
            if (categoryInfo != null && arrayLength > 0) {
                trackingList.add(pageCount);
                sendTracking(context);
            }

            isLoading = false;

            // page increment
            if (arrayLength == 0) {
                isLoadMoreEnable = false;

            } else {
                isLoadMoreEnable = true;
                pageCount++;
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkForImpressionTracking();
            }
        }, 200);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Systr.println("ON ACTIVITY RESULT called : result : " + resultCode + " request : " + requestCode);

        if (requestCode == 100 && resultCode == 100) {
            VideoInfo videoInfo = (VideoInfo) data.getSerializableExtra("video_update");
            adapter.updateVideoInfo(videoInfo);
            Context context = getContext();
            if (context != null) {
//                VideoSerializableData.getInstance(context).addVideoList(adapter.getVideoList(), pageCount);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onVideoSelect(int position) {

        Context context = getContext();
        if (context != null) {
            String sectionLabel = AppPreferences.getInstance(context).getStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME) + (TextUtils.isEmpty(sectionName) ? "_" : ("_" + sectionName + "_")) + categoryInfo.displayName;
            if (NetworkStatus.getInstance().isConnected(context)) {

                VideoInfo videoInfo = adapter.getItemAtPosition(position);

                if (videoInfo != null) {

                    Intent intent;
//                    if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.VideoPref.SETTINGS_IS_ADAPTIVE_PLAYER_ON, false)) {
//                        intent = VideoDetailActivity.getIntent(getContext(), videoInfo, sectionLabel, AppFlyerConst.DBVideosSource.VIDEO_LIST, position + 1, categoryInfo.detailUrl, categoryInfo.feedUrl, categoryInfo.gaEventLabel, categoryInfo.gaArticle, false, "", categoryInfo.color);
//                    } else {
//                    }
                    intent = DBVideoPlayerActivity2.getIntent(getContext(), videoInfo, sectionLabel, AppFlyerConst.DBVideosSource.VIDEO_LIST, position + 1, categoryInfo.detailUrl, categoryInfo.feedUrl, categoryInfo.gaEventLabel, categoryInfo.gaArticle, false, "", categoryInfo.color);
                    startActivityForResult(intent, 100);

                    Tracking.trackVideoImpression(getContext(), videoInfo.impressionTrackUrl, videoInfo.pageNumber, videoInfo.pageVideoIndex, "click", categoryInfo.displayName, categoryInfo.id, AppFlyerConst.DBVideosSource.VIDEO_LIST);

                } else {
                    AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_video_not_found));
                }

            } else {
                AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
            }
        }
    }

    @Override
    public void onVideoDelete(View view, int position, String videoId) {

        initiatePopupWindow(view, position, videoId);
    }

    private void initiatePopupWindow(final View v, final int index, final String videoId) {
        try {
            int[] a = new int[2];
            v.getLocationInWindow(a);

            final View layout = LayoutInflater.from(getContext()).inflate(R.layout.popup_view, null);
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            layout.measure(display.getWidth(), display.getHeight());
            int viewWidth = layout.getMeasuredWidth();

            final PopupWindow pw = new PopupWindow(layout, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
            pw.setBackgroundDrawable(new ColorDrawable());
            pw.setOutsideTouchable(true);
            pw.showAtLocation(v, 0, (a[0] - viewWidth), a[1]);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendDeleteVideoToServer(index, videoId);
                    pw.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDeleteVideoToServer(final int index, String videoId) {
//        String url = Urls.APP_FEED_BASE_URL + AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.VideoPref.VALUE_VIDEO_ACTION, Urls.DEFAULT_VIDEO_ACTION)
        String url = Urls.APP_FEED_BASE_URL + AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.VideoPref.SETTINGS_VIDEO_ACTION, Urls.DEFAULT_VIDEO_ACTION);

        JSONObject jsonObject = new JSONObject();
        try {
            String mSessionId = TrackingData.getDbORDeviceId(getContext());
            jsonObject.put("session_id", mSessionId);
            jsonObject.put("vid", videoId);
            jsonObject.put("action", "delete");
            jsonObject.put("channel_slno", CommonConstants.CHANNEL_ID);

        } catch (JSONException e) {
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Systr.println("Action Json : " + response);
                Context context = getContext();
                if (context != null) {
                    adapter.deleteVideo(index);
//                    VideoSerializableData.getInstance(context).addVideoList(adapter.getVideoList(), pageCount);
                    AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.video_delete_note));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Action Error : " + error);
                Context context = getContext();
                if (context != null) {
                    AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void sendTracking(Context context) {
        // Tracking
        if (isShowing) {
            if (categoryInfo != null && trackingList != null) {
                while (trackingList.size() > 0) {
                    int value = trackingList.get(0);
                    trackingList.remove(0);
                    if (value > 1) {
                        String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_DEPTH, categoryInfo.displayName, AppFlyerConst.GALabel.PG + value, campaign);
                    }
                }
            }
        }
    }

    private void fetchFollowListFromServer(String url) {
        Systr.println("Provider url : " + url);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Systr.println("Provider list fetch response Json : " + response);

                try {
                    Context context = getContext();
                    if (context != null) {
                        Vector<VideoProviderInfo> list = new Vector<>(Arrays.asList(new Gson().fromJson(response.toString(), VideoProviderInfo[].class)));
                        updateFollow(context, list);
                    }
                } catch (Exception e) {
                    // display logs
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Provider list fetch Error : " + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void updateFollow(Context context, Vector<VideoProviderInfo> list) {
        BooleanExpressionTable booleanExpressionTable = (BooleanExpressionTable) DatabaseHelper.getInstance(context).getTableObject(BooleanExpressionTable.TABLE_NAME);
        booleanExpressionTable.updateBooleanValue(list.toArray(), Constants.BooleanConst.VIDEO_FOLLOW, true);

        AppPreferences.getInstance(context).setBooleanValue(QuickPreferences.FOLLOW_LIST_UPDATE_FIRST_TIME, true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }
}
