package com.db.dbvideoPersonalized.data;

import android.text.TextUtils;

import com.db.data.models.DBVideosInfo;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DB on 11-10-2017.
 */

public class VideoInfo extends DBVideosInfo implements Serializable, Cloneable {

    @SerializedName("like")
    public int likeCount;

    @SerializedName("whshare")
    public int whatsappShareCount;

    @SerializedName("fbshare")
    public int fbShareCount;

    @SerializedName("provider_name")
    public String providerName;

    @SerializedName("imp_click_track")
    public String impressionTrackUrl;

    @SerializedName("iitl_title")
    public String titleHead;

    @SerializedName("is_portrait")
    public int portraitVideo = 0;

    @SerializedName("square_size")
    public String squareImageUrl;

    public boolean isLiked;

    public int pageNumber;
    public int pageVideoIndex;

    @Override
    public boolean equals(Object obj) {
        if (type == 0) {
            if (obj instanceof VideoInfo) {
                if (!TextUtils.isEmpty(storyId))
                    return this.storyId.equals(((VideoInfo) obj).storyId);
                else
                    super.equals(obj);
            }
        } else {
            if (obj != null && ((VideoInfo) obj).type != 0) {
                return this.type == ((VideoInfo) obj).type;
            } else {
                return super.equals(obj);
            }
        }
        return super.equals(obj);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
