package com.db.dbvideoPersonalized.data;

import android.text.TextUtils;

import com.db.data.models.DBVideosInfo;

import java.io.Serializable;

/**
 * Created by DB on 31-10-2017.
 */

public class VideoPlaylistInfo implements Serializable{

    public VideoInfo videoInfo;
    public String detailUrl;
    public String feedUrl;
    public String gaEventLabel;
    public String gaArticle;
    public boolean isPersonalizedFeedUrl;

    @Override
    public boolean equals(Object obj) {
        if (!TextUtils.isEmpty(videoInfo.storyId)) {
            if (obj instanceof VideoPlaylistInfo) {
                return this.videoInfo.storyId.equals(((VideoPlaylistInfo) obj).videoInfo.storyId);
            } else if (obj instanceof String) {
                return this.videoInfo.storyId.equals(obj);
            } else if (obj instanceof VideoInfo) {
                return this.videoInfo.storyId.equals(((VideoInfo) obj).storyId);
            } else if (obj instanceof DBVideosInfo) {
                return this.videoInfo.storyId.equals(((DBVideosInfo) obj).storyId);
            }
        }

        return false;
    }
}
