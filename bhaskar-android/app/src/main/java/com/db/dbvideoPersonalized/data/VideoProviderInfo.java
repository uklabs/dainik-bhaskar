package com.db.dbvideoPersonalized.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DB on 04-11-2017.
 */

public class VideoProviderInfo {

    @SerializedName("provider_id")
    public String providerId;

    @SerializedName("provider_name")
    public String providerName;

    @SerializedName("provider_logo")
    public String providerLogo;

    @Override
    public String toString() {
        return providerId;
    }
}
