package com.db.dbvideoPersonalized.detail;

import android.content.Context;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.bhaskar.util.LoginController;
import com.dailymotion.android.player.sdk.PlayerWebView;
import com.dailymotion.android.player.sdk.events.PlayerEvent;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.dbvideo.player.AppTraverseUtil;
import com.db.dbvideo.player.VideoEventConstant;
import com.db.dbvideo.player.VideoPlayerConstant;
import com.db.dbvideo.player.VideoPlayerEventTracker;
import com.db.dbvideo.videoview.DeviceOrientationListener;
import com.db.dbvideo.videoview.VideoPlayerCallbackListener;
import com.db.util.AppPreferences;
import com.db.util.QuickPreferences;

import java.util.HashMap;

public class DailyMotionPlayerFragment extends Fragment implements PlayerWebView.PlayerEventListener, View.OnClickListener {

    private boolean isAutoPlay;
    private boolean isHideShare;
    private String shareTitle = "";
    private String shareLink = "";
    private boolean isAutoNextPlay;
    private boolean showNextButton;

    private String videoId;
    private boolean isFullScreen;

    private PlayerWebView playerWebView;
    private DeviceOrientationListener deviceOrientationListener;
    private VideoPlayerCallbackListener videoPlayerCallbackListener;

    private View replayNextButtonLayout;
    private ImageView imageViewReplayButton;
    private ImageView imageViewNextButton;

    private View autoNextPlayButtonLayout;
    private TextView cancelButtonNextAutoPlay;
    private ImageView autoNextPlayView;
    private ImageView imageViewReplayButtonNextAutoPlay;
    private ProgressBar circularProgress;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            deviceOrientationListener = (DeviceOrientationListener) context;
        } catch (Exception e) {
        }
        try {
            videoPlayerCallbackListener = (VideoPlayerCallbackListener) context;
        } catch (Exception e) {
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getData();
        View view = inflater.inflate(R.layout.fragment_daily_motion_player, container, false);

        replayNextButtonLayout = view.findViewById(R.id.layout_replay_next_control);
        imageViewNextButton = view.findViewById(R.id.imageView_next_button);
        imageViewReplayButton = view.findViewById(R.id.imageView_replay_button);

        autoNextPlayButtonLayout = view.findViewById(R.id.layout_auto_next_play_control);
        cancelButtonNextAutoPlay = view.findViewById(R.id.cancel_button_next_auto_play);
        autoNextPlayView = view.findViewById(R.id.auto_next_play_view);
        imageViewReplayButtonNextAutoPlay = view.findViewById(R.id.imageView_replay_button_next_auto_play);
        circularProgress = view.findViewById(R.id.circularProgressbar);

        imageViewReplayButton.setOnClickListener(this);
        imageViewNextButton.setOnClickListener(this);

        imageViewReplayButtonNextAutoPlay.setOnClickListener(this);
        autoNextPlayView.setOnClickListener(this);
        cancelButtonNextAutoPlay.setOnClickListener(this);

        playerWebView = view.findViewById(R.id.player_webview);

        HashMap<String, Object> playerParams = new HashMap<>();
        playerWebView.load(videoId, playerParams);

        playerWebView.setPlayerEventListener(this);
        playerWebView.setWebViewErrorListener(new PlayerWebView.WebViewErrorListener() {
            @Override
            public void onErrorReceived(WebView webView, int errorCode, String description, String failingUrl) {

            }

            @Override
            public void onErrorReceived(WebView view, WebResourceRequest request, WebResourceError error) {

            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {

            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {

            }
        });

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView_next_button:
                videoPlayerCallbackListener.onNextButtonclick();
                replayNextButtonLayout.setVisibility(View.GONE);
                break;

            case R.id.imageView_replay_button:
//                if (!isAutoPlay) {
//                    BusProvider.getInstance().post(new PlayerEventOttoWrapper());
//                    isAutoPlay = true;
//                    imageViewReplayButton.setImageResource(R.drawable.ic_video_replay);
//                    visible = false;
//                } else {
//                    playerControllerWithAdPlayback.onPlayPauseButtonClick();
//                }
                playerWebView.showControls(true);
                playerWebView.play();
                replayNextButtonLayout.setVisibility(View.GONE);
                break;

            case R.id.imageView_replay_button_next_auto_play:
                cancelTimer();

//                if (!isAutoPlay) {
//                    BusProvider.getInstance().post(new PlayerEventOttoWrapper());
//                    isAutoPlay = true;
//                    imageViewReplayButton.setImageResource(R.drawable.ic_video_replay);
//                    visible = false;
//                } else {
//                    playerControllerWithAdPlayback.onPlayPauseButtonClick();
//                }
                playerWebView.showControls(true);
                playerWebView.play();
                autoNextPlayButtonLayout.setVisibility(View.GONE);
//                ImageUtil.setImage(context, thumbDefResId, imageViewThumbnail, 0);
                break;

            case R.id.auto_next_play_view:
                cancelTimer();

                videoPlayerCallbackListener.onNextButtonclick();
                autoNextPlayButtonLayout.setVisibility(View.GONE);
                break;

            case R.id.cancel_button_next_auto_play:
                autoNextPlayButtonLayout.setVisibility(View.GONE);
                replayNextButtonLayout.setVisibility(View.VISIBLE);
                if (showNextButton) {
                    imageViewNextButton.setVisibility(View.VISIBLE);
                } else {
                    imageViewNextButton.setVisibility(View.GONE);
                }

                cancelTimer();
                break;
        }
    }

    private void getData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            isAutoPlay = bundle.getBoolean(VideoPlayerConstant.IS_AUTO_PLAY, false);
            isHideShare = bundle.getBoolean(VideoPlayerConstant.HIDE_SHARE_BUTTON);
            isAutoNextPlay = bundle.getBoolean(VideoPlayerConstant.IS_AUTO_NEXT_PLAY, false);
            showNextButton = bundle.getBoolean(VideoPlayerConstant.SHOW_NEXT_BUTTON, false);
            videoId = bundle.getString(VideoPlayerConstant.VIDEO_DAILY_MOTION_ID);

            if (!isHideShare) {
                shareTitle = bundle.getString(VideoPlayerConstant.SHARE_TITLE);
                shareLink = bundle.getString(VideoPlayerConstant.SHARE_LINK);
            }
        }
    }

    private void changeVideoViewScreen() {
        if (deviceOrientationListener != null) {
            deviceOrientationListener.changeOrientation();
        }
    }

    public void changeVideoOrientation() {
        isFullScreen = !isFullScreen;

        if (playerWebView != null)
            playerWebView.setFullscreenButton(isFullScreen);
    }

    public void stopPlayer() {
        if (playerWebView != null) {
            playerWebView.pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        playerWebView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        playerWebView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        playerWebView.destroy();
    }

    @Override
    public void onStop() {
        super.onStop();
        playerWebView.pause();
    }

    @Override
    public void onEvent(@NonNull PlayerEvent event) {
        switch (event.getName()) {
            case PlayerWebView.EVENT_VIDEO_START:
                if (getContext() != null) {
                    String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                    Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), VideoPlayerEventTracker.CATEGORY_VIDEO, VideoEventConstant.PLAY_DM, AppTraverseUtil.getScreenNameForVideoTracking(), campaign);
                    CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.VIDEO_WATCHED, CTConstant.PROP_NAME.VIDEO_TITLE, AppTraverseUtil.getVideoTitle(), LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.ACTION_TYPE, CTConstant.PROP_VALUE.VIDEO_STARTED));
                }
                break;
            case PlayerWebView.EVENT_VIDEO_END:
                if (getContext() != null) {
                    String campaign1 = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                    Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), VideoPlayerEventTracker.CATEGORY_VIDEO, VideoEventConstant.COMPLETED_DM, AppTraverseUtil.getScreenNameForVideoTracking(), campaign1);
                    CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.VIDEO_WATCHED, CTConstant.PROP_NAME.VIDEO_TITLE, AppTraverseUtil.getVideoTitle(), LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.ACTION_TYPE, CTConstant.PROP_VALUE.VIDEO_END));
                }
                break;
            case PlayerWebView.EVENT_END:
                playerWebView.showControls(false);
                onVideoComplete();
                break;
            case PlayerWebView.EVENT_FULLSCREEN_TOGGLE_REQUESTED:
                changeVideoViewScreen();
                break;
            case PlayerWebView.EVENT_AD_START:
                if (getContext() != null) {
                    String campaign2 = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                    Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), VideoPlayerEventTracker.CATEGORY_AD, VideoEventConstant.AD_PALY_DM, AppTraverseUtil.getScreenNameForVideoTracking(), campaign2);
                }
                break;
            case PlayerWebView.EVENT_AD_END:
                if (getContext() != null) {
                    String campaign3 = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                    Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), VideoPlayerEventTracker.CATEGORY_AD, VideoEventConstant.AD_COMPLETED_DM, AppTraverseUtil.getScreenNameForVideoTracking(), campaign3);
                }
                break;
        }
    }

    private void onVideoComplete() {
        if (isAutoNextPlay) {
            autoNextPlayButtonLayout.setVisibility(View.VISIBLE);
            startTimer();
        } else {
            if (showNextButton) {
                imageViewNextButton.setVisibility(View.VISIBLE);
            } else {
                imageViewNextButton.setVisibility(View.GONE);
            }
            replayNextButtonLayout.setVisibility(View.VISIBLE);
        }
    }

    private CountDownTimer countDownTimer;

    private void startTimer() {
        countDownTimer = new CountDownTimer(5000, 250) {
            @Override
            public void onTick(long millisUntilFinished) {
                int per = (int) (((5000 - millisUntilFinished) / 5000.0f) * 100);
                circularProgress.setProgress(per);
            }

            @Override
            public void onFinish() {
                circularProgress.setProgress(100);
                if (videoPlayerCallbackListener != null)
                    videoPlayerCallbackListener.onNextButtonclick();
                autoNextPlayButtonLayout.setVisibility(View.GONE);
            }
        };

        countDownTimer.start();
    }

    public void cancelTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }
}
