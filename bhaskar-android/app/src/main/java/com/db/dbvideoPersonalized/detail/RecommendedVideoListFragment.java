package com.db.dbvideoPersonalized.detail;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.view.ui.MainActivity;
import com.bhaskar.R;
import com.db.data.database.BooleanExpressionTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.ReadUnreadStatusTable;
import com.db.data.source.server.BackgroundRequest;
import com.db.dbvideoPersonalized.OnAddPlaylistListener;
import com.db.dbvideoPersonalized.OnVideoSelectedListener;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.dbvideoPersonalized.WatchedPersonalizedVideoData;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.playlist.ProviderVideoListActivity;
import com.db.listeners.OnStoryShareUrlListener;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.ShareUtil;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.ViewAnimationUtils;
import com.db.views.viewholder.LoadingViewHolder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RecommendedVideoListFragment extends Fragment {
    private static final int ACTION_LIKE = 1;
    private static final int ACTION_UNLIKE = 2;
    private static final int ACTION_FOLLOW = 3;
    private static final int ACTION_UNFOLLOW = 4;
    private static final int ACTION_FB_SHARE = 5;
    private static final int ACTION_WHATSAPP_SHARE = 6;
    private static final int ACTION_VIEW = 7;
    private LinearLayout feedBackLayout;

    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;

    private ProgressBar progressBar;
    private View retryLayout;

    private OnVideoSelectedListener videoSelectListener;
    private OnAddPlaylistListener addPlaylistListener;
    private ArrayList<VideoInfo> dbVideosInfoArrayList;
    private PlayListAdapter adapter;

    private String mSectionLabel = "";
    private String mDetailUrl;
    private String mFeedUrl;
    private String mGaEventLabel;

    private VideoInfo mVideoInfo;
    private boolean isSubscribed;

    private WatchedPersonalizedVideoData watchedVideoData;

    private boolean isLoading;
    private int visibleThreshold = 2;
    private int pageCount = 0;
    private boolean isLoadMoreEnable = true;
    private BooleanExpressionTable booleanExpressionTable;
    private String mGaArticle;
    private ReadUnreadStatusTable readUnreadStatusTable;
    private String mColor;

    public RecommendedVideoListFragment() {
    }

    private boolean mIsPersonalizedFeedUrl;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            videoSelectListener = (OnVideoSelectedListener) context;
        } catch (ClassCastException e) {
        }

        try {
            addPlaylistListener = (OnAddPlaylistListener) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbVideosInfoArrayList = new ArrayList<>();

        readUnreadStatusTable = (ReadUnreadStatusTable) DatabaseHelper.getInstance(getContext()).getTableObject(ReadUnreadStatusTable.TABLE_NAME);
        booleanExpressionTable = (BooleanExpressionTable) DatabaseHelper.getInstance(getContext()).getTableObject(BooleanExpressionTable.TABLE_NAME);
    }

    Rect scrollBounds;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recommendation_list, container, false);

        retryLayout = view.findViewById(R.id.retry_layout);
        progressBar = view.findViewById(R.id.progressBar);
        recyclerView = view.findViewById(R.id.recycler_view);

        watchedVideoData = WatchedPersonalizedVideoData.getInstance(getContext());

        pageCount = 0;
        isLoadMoreEnable = true;

        view.findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryLayout.setVisibility(View.GONE);
                fetchDataFromServer();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.scrollBounds = new Rect();
        this.recyclerView.getHitRect(scrollBounds);
        adapter = new PlayListAdapter();

        mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        SpacesDecoration spacesItemDecoration = new SpacesDecoration(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.list_spacing_video));
        recyclerView.addItemDecoration(spacesItemDecoration);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {

                        if (isLoadMoreEnable) {
                            onLoadMore();
                            isLoading = true;
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                Systr.println("Tracking : Recommendation state change : " + newState);
                checkForImpressionTracking();
            }
        });
    }

    private ArrayList<Integer> impressionList = new ArrayList<>();

    private void checkForImpressionTracking() {
//        Systr.println("Tracking : Impression called Recommendation");
        if (recyclerView != null) {
            View childAt;
            int height;

            VideoInfo itemAtPosition;
            int childAdapterPosition;
            for (int i = 0; i < recyclerView.getChildCount(); i++) {
//                Systr.println("Tracking : Impression called in loop Recommendation");
                try {
                    childAt = recyclerView.getChildAt(i);
                    height = childAt.getHeight();
                    if (childAt.getLocalVisibleRect(scrollBounds)) {
                        if (scrollBounds.bottom == height) {
//                            Systr.println("Tracking : Impression called height found Recommendation");
                            childAdapterPosition = recyclerView.getChildAdapterPosition(childAt);
                            if (childAdapterPosition > 0) {
                                itemAtPosition = adapter.getItemAtPosition(childAdapterPosition);
                                if (!impressionList.contains(childAdapterPosition)) {
                                    Systr.println("Impression Tracking : Recommendation : " + childAdapterPosition + ", PageNo : 0, Index : " + childAdapterPosition);
                                    Tracking.trackVideoImpression(getContext(), itemAtPosition.impressionTrackUrl, 0, childAdapterPosition, "impression", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);
                                    impressionList.add(childAdapterPosition);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void checkForImpressionLoadTime() {
        if (recyclerView != null) {
            int firstPosition = mLayoutManager.findFirstVisibleItemPosition();
            int lastPosition = mLayoutManager.findLastCompletelyVisibleItemPosition();

            VideoInfo itemAtPosition;
            for (int i = firstPosition; i <= lastPosition; i++) {
                try {
                    if (i > 0) {
                        itemAtPosition = adapter.getItemAtPosition(i);
                        if (!impressionList.contains(i)) {
                            Systr.println("Impression Tracking : Recommendation : " + i + ", PageNo : 0, Index : " + i);
                            Tracking.trackVideoImpression(getContext(), itemAtPosition.impressionTrackUrl, 0, i, "impression", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);
                            impressionList.add(i);
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    public void onLoadMore() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (dbVideosInfoArrayList != null)
                    dbVideosInfoArrayList.add(null);

                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });

        //Load more data for reyclerview
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetchDataFromServer();
            }
        }, 500);
    }

    private void fetchDataFromServer() {
        String recommendedUrl;

        if (pageCount == 0) {
            if (mVideoInfo != null) {
//                recommendedUrl = AppUtils.updateApiUrl(String.format(Urls.DBVIDEOS_RECOMMENDATION_URL, mDetailUrl, mVideoInfo.storyId));
                recommendedUrl = AppUtils.getInstance().updateApiUrl(Urls.APP_FEED_BASE_URL + mDetailUrl + mVideoInfo.storyId + "/");
                makeJsonArrayRequest(recommendedUrl);
            }

        } else {

            if (mIsPersonalizedFeedUrl) {
                String mSessionId = TrackingData.getDbORDeviceId(getContext());
                recommendedUrl = Urls.APP_FEED_BASE_URL + mFeedUrl + (mSessionId + "/PG" + pageCount + "/");
            } else {
                recommendedUrl = AppUtils.getInstance().makeUrlForLoadMore(Urls.APP_FEED_BASE_URL + mFeedUrl, pageCount);
            }

            makeJsonObjectRequest(recommendedUrl);
        }
    }

    public void setVideo(VideoInfo info, String sectionLabel, String detailUrl, String feedUrl, String gaArticle, String gaEventLabel, boolean isPersonalizedFeedUrl, String color) {
        this.mSectionLabel = sectionLabel;
        this.mDetailUrl = detailUrl;
        this.mFeedUrl = feedUrl;
        this.mVideoInfo = info;
        this.mGaEventLabel = gaEventLabel;
        this.mGaArticle = gaArticle;
        this.mIsPersonalizedFeedUrl = isPersonalizedFeedUrl;
        this.mColor = color;

        pageCount = 0;
        isLoadMoreEnable = true;
        isSubscribed = false;

        if (mVideoInfo != null) {
            isSubscribed = booleanExpressionTable.isBoolean(mVideoInfo.providerId, Constants.BooleanConst.VIDEO_FOLLOW);
            mVideoInfo.isLiked = booleanExpressionTable.isBoolean(mVideoInfo.storyId, Constants.BooleanConst.VIDEO_LIKE);
        }

        showDescriptionLayout = false;
        impressionList.clear();

        dbVideosInfoArrayList.clear();
        adapter.notifyDataSetChanged();

        retryLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        if (mLayoutManager != null)
            mLayoutManager.scrollToPosition(0);

        Systr.println("VIDEO Detail url : " + mDetailUrl);
        fetchDataFromServer();
    }

    private void makeJsonArrayRequest(String recommendedUrl) {
        Systr.println("Recommended Url for video : " + recommendedUrl);

        progressBar.setVisibility(View.VISIBLE);

        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET, recommendedUrl, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Systr.println("Recommended Video array response : " + response);
                        try {
                            if (response != null && response.length() > 0) {
                                recyclerView.setVisibility(View.VISIBLE);
                                parseFeedList(response, true);
                            } else if (dbVideosInfoArrayList.size() == 0) {
                                retryLayout.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                        }

                        progressBar.setVisibility(View.GONE);
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Systr.println("Recommended Video Error : " + error);
                        Activity activity = getActivity();
                        if (activity != null && isAdded()) {

                            progressBar.setVisibility(View.GONE);

                            if (dbVideosInfoArrayList != null) {
                                dbVideosInfoArrayList.remove(null);
                            }
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }

                            if (dbVideosInfoArrayList.size() == 0) {
                                retryLayout.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            }

                            if (error instanceof NoConnectionError || error instanceof NetworkError) {
                                showToast(activity, activity.getResources().getString(R.string.internet_connection_error_));
                            } else {
                                showToast(activity, activity.getResources().getString(R.string.sorry_error_found_please_try_again_));
                            }
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
    }

    private void makeJsonObjectRequest(String recommendedUrl) {
        Systr.println("Recommended Url for video : " + recommendedUrl);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, recommendedUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Systr.println("Recommended Video response : " + response);
                        try {
                            if (response != null && response.length() > 0) {
                                parseFeedList(response.getJSONArray("feed"), false);
                            }
                        } catch (Exception e) {
                        }

                        progressBar.setVisibility(View.GONE);
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Systr.println("Recommended Video Error : " + error);

                        progressBar.setVisibility(View.GONE);

                        Activity activity = getActivity();
                        if (activity != null && isAdded()) {

                            if (dbVideosInfoArrayList != null) {
                                dbVideosInfoArrayList.remove(null);
                            }
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }

                            if (error instanceof NoConnectionError || error instanceof NetworkError) {
                                showToast(activity, activity.getResources().getString(R.string.internet_connection_error_));
                            } else {
                                showToast(activity, activity.getResources().getString(R.string.sorry_error_found_please_try_again_));
                            }
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
    }

    private void parseFeedList(JSONArray resultArray, boolean clearList) throws JSONException {

        if (clearList) {
            dbVideosInfoArrayList.clear();
            adapter.notifyDataSetChanged();
        }

        String mStoryId = (mVideoInfo != null ? mVideoInfo.storyId : "");

        Gson gson = new Gson();
        JSONObject feedJsonObject;
        for (int i = 0; i < resultArray.length(); i++) {
            feedJsonObject = resultArray.getJSONObject(i);
            VideoInfo newsFeeds = gson.fromJson(feedJsonObject.toString(), VideoInfo.class);
            if (TextUtils.isEmpty(mStoryId) && !TextUtils.isEmpty(newsFeeds.storyId)) {
                if (!watchedVideoData.hasVideo(newsFeeds.storyId) && !dbVideosInfoArrayList.contains(newsFeeds)) {
                    dbVideosInfoArrayList.add(newsFeeds);
                }
            } else {
                if (!TextUtils.isEmpty(newsFeeds.storyId) && !mStoryId.equalsIgnoreCase(newsFeeds.storyId)) {
                    if (!watchedVideoData.hasVideo(newsFeeds.storyId) && !dbVideosInfoArrayList.contains(newsFeeds)) {
                        dbVideosInfoArrayList.add(newsFeeds);
                    }
                }
            }
        }

        // remove null item and page count for load more
        dbVideosInfoArrayList.remove(null);
        adapter.notifyDataSetChanged();

        if (dbVideosInfoArrayList.size() >= 50 || resultArray.length() == 0) {
            isLoadMoreEnable = false;
            isLoading = false;

        } else {
            isLoading = false;
            pageCount++;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkForImpressionLoadTime();
            }
        }, 200);

        try {
            if (dbVideosInfoArrayList.size() < 4 && NetworkStatus.getInstance().isConnected(getContext())) {
                onLoadMore();
            }
        } catch (Exception e) {
        }
    }

    public VideoInfo getFirstItem() {
        if (dbVideosInfoArrayList != null && dbVideosInfoArrayList.size() > 0)
            return dbVideosInfoArrayList.get(0);
        return null;
    }

    private class PlayListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int VIEW_TYPE_HEADING = 1;
        private static final int VIEW_TYPE_ITEM = 2;
        private static final int VIEW_TYPE_LOADING = 3;
        private Context context;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_HEADING:
                    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommended_list_heading, parent, false);
                    return new FirstViewHolder(itemView);
                case VIEW_TYPE_LOADING:
                    View loadingView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false);
                    return new LoadingViewHolder(loadingView);
                default:
                    View listView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommended_video_list, parent, false);
                    return new VideoViewHolder(listView);
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof VideoViewHolder) {
                final VideoViewHolder viewHolder = (VideoViewHolder) holder;

                final int pos = position - 1;
                final VideoInfo info = dbVideosInfoArrayList.get(pos);

                ImageUtil.setImage(getContext(), info.image, viewHolder.iconImageView, R.drawable.water_mark_news_list);
                viewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(info.titleHead, AppUtils.getInstance().fromHtml(info.title), mColor));

                if (readUnreadStatusTable.isStoryWithVersionAvailable(AppUtils.getFilteredStoryId(info.storyId), info.version)) {
                    viewHolder.titleTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.article_read_color));
                } else {
                    viewHolder.titleTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.list_title_color));
                }
                viewHolder.durationTextView.setVisibility(View.GONE);
//                if (TextUtils.isEmpty(info.duration)) {
//                } else {
//                    viewHolder.durationTextView.setVisibility(View.VISIBLE);
//                    viewHolder.durationTextView.setText(info.duration.trim());
//                }

                int viewsCount = 0;
                try {
                    viewsCount = Integer.valueOf(info.views);
                } catch (Exception e) {
                }

                context = getContext();
                if (viewsCount < 1000) {
                    viewHolder.viewsTextView.setVisibility(View.GONE);
                } else {
                    if (context != null) {
                        viewHolder.viewsTextView.setVisibility(View.VISIBLE);
                        viewHolder.viewsTextView.setText(context.getResources().getString(R.string.views_count, AppUtils.getInstance().getSIPrefixForCount(viewsCount)));
                    }
                }

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        viewHolder.titleTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.article_read_color));
                        String filteredStoryId = AppUtils.getFilteredStoryId(info.storyId);
                        if (!readUnreadStatusTable.isStoryWithVersionAvailable(filteredStoryId, info.version)) {
                            readUnreadStatusTable.updateStoryVersion(filteredStoryId, info.version);
                        }
                        Context context = getContext();
                        if (context != null) {
                            if (NetworkStatus.getInstance().isConnected(context)) {
                                if (videoSelectListener != null) {
                                    videoSelectListener.onVideoSelected(info, AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION, (pos + 1));
                                }

//                                if (!AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.TOGGLING_SCREEN_DEPTH_VIDEO, true)) {
//
//                                } else {
//
//                                    Intent intent;
////                                    if (AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.VideoPref.SETTINGS_IS_ADAPTIVE_PLAYER_ON, false)) {
////                                        intent = VideoDetailActivity.getIntent(context, info, mSectionLabel, AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION, (pos + 1), mDetailUrl, mFeedUrl, mGaEventLabel, mGaArticle, mIsPersonalizedFeedUrl, "", mColor);
////                                    } else {
////                                    }
//                                    intent = DBVideoPlayerActivity2.getIntent(context, info, mSectionLabel, AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION, (pos + 1), mDetailUrl, mFeedUrl, mGaEventLabel, mGaArticle, mIsPersonalizedFeedUrl, "", mColor);
//                                    startActivity(intent);
//                                }

                                Tracking.trackVideoImpression(getContext(), info.impressionTrackUrl, 0, pos, "click", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);

                            } else {
                                showToast(context, context.getResources().getString(R.string.internet_connection_error_));
                            }
                        }
                    }
                });

            } else if (holder instanceof FirstViewHolder) {

                FirstViewHolder firstViewHolder = (FirstViewHolder) holder;
                if (mVideoInfo != null) {

                    Context context = getContext();
                    if (context != null) {
                        firstViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(mVideoInfo.titleHead, AppUtils.getInstance().fromHtml(mVideoInfo.title), mColor));

                        int viewCount = 0;
                        try {
                            viewCount = Integer.parseInt(mVideoInfo.views);
                        } catch (Exception e) {
                        }
                        if (viewCount < 1000) {
                            firstViewHolder.viewsTextView.setVisibility(View.GONE);
                        } else {
                            firstViewHolder.viewsTextView.setVisibility(View.VISIBLE);
                            firstViewHolder.viewsTextView.setText(context.getResources().getString(R.string.views_count, AppUtils.getInstance().getSIPrefixForCount(mVideoInfo.views)));
                        }
                        if (mVideoInfo.whatsappShareCount == 0) {
                            firstViewHolder.whatsappTextView.setVisibility(View.INVISIBLE);
                        } else {
                            firstViewHolder.whatsappTextView.setVisibility(View.VISIBLE);
                            firstViewHolder.whatsappTextView.setText(AppUtils.getInstance().getSIPrefixForCount(mVideoInfo.whatsappShareCount));
                        }
                        if (mVideoInfo.fbShareCount == 0) {
                            firstViewHolder.fbTextView.setVisibility(View.INVISIBLE);
                        } else {
                            firstViewHolder.fbTextView.setVisibility(View.VISIBLE);
                            firstViewHolder.fbTextView.setText(AppUtils.getInstance().getSIPrefixForCount(mVideoInfo.fbShareCount));
                        }
                        firstViewHolder.descTextView.setText(mVideoInfo.story);

                        firstViewHolder.providerLayout.setVisibility(View.GONE);

                        if (mVideoInfo.isLiked && mVideoInfo.likeCount == 0) {
                            firstViewHolder.likeTextView.setText(AppUtils.getInstance().getSIPrefixForCount(1));
                        } else {
                            if (mVideoInfo.likeCount == 0) {
                                firstViewHolder.likeTextView.setVisibility(View.INVISIBLE);
                            } else {
                                firstViewHolder.likeTextView.setVisibility(View.VISIBLE);
                                firstViewHolder.likeTextView.setText(AppUtils.getInstance().getSIPrefixForCount(mVideoInfo.likeCount));
                            }
                        }

                        if (mVideoInfo.isLiked) {
                            firstViewHolder.likeImageView.setColorFilter(ContextCompat.getColor(context, R.color.blue));
                        } else {
                            firstViewHolder.likeImageView.setColorFilter(ContextCompat.getColor(context, R.color.video_player_title_color));
                        }
                    }
                }
                if (AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.TOGGLING_ARTICLE_RATING_TOP, false)) {
                    feedBackLayout.setVisibility(View.VISIBLE);
                } else {
                    feedBackLayout.setVisibility(View.GONE);
                }
                if (!showDescriptionLayout) {
                    firstViewHolder.descLayout.setVisibility(View.GONE);
                    firstViewHolder.dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_down);
                }

            } else if (holder instanceof LoadingViewHolder) {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return dbVideosInfoArrayList == null ? 0 : (dbVideosInfoArrayList.size() + 1);
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0)
                return VIEW_TYPE_HEADING;
            else if (dbVideosInfoArrayList.get(position - 1) == null)
                return VIEW_TYPE_LOADING;
            else
                return VIEW_TYPE_ITEM;
        }

        public VideoInfo getItemAtPosition(int position) {
            if (position > 0 && position < dbVideosInfoArrayList.size())
                return dbVideosInfoArrayList.get(position - 1);
            return null;
        }
    }

    private void showDialogToUnfollow(String providerName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        if (TextUtils.isEmpty(providerName))
            builder.setMessage("Do you want to unfollow from DB Videos.");
        else
            builder.setMessage("Do you want to unfollow from " + providerName);

        builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendActionToServer(ACTION_UNFOLLOW);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.no), null);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private class VideoViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView;
        TextView viewsTextView;
        TextView durationTextView;
        ImageView iconImageView;

        VideoViewHolder(View view) {
            super(view);

            iconImageView = view.findViewById(R.id.iv_video_icon);
            titleTextView = view.findViewById(R.id.tv_title);
            durationTextView = view.findViewById(R.id.tv_duration);
            viewsTextView = view.findViewById(R.id.tv_view);
        }
    }

    private boolean isLikeProcessing;
    private boolean showDescriptionLayout = false;

    private class FirstViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView viewsTextView;
        TextView titleTextView;
        ImageView dropDownImageView;
        TextView descTextView;
        LinearLayout descLayout;

        LinearLayout likeLayout;
        LinearLayout addToPlaylistLayout;
        LinearLayout whatsappLayout;
        LinearLayout fbLayout;

        ImageView likeImageView;
        TextView likeTextView;
        TextView whatsappTextView;
        TextView fbTextView;

        LinearLayout brandLayout;
        ImageView brandImageView;
        TextView brandTextView;

        LinearLayout followLayout;
        TextView followTextView;
        LinearLayout providerLayout;

        FirstViewHolder(View view) {
            super(view);

            titleTextView = view.findViewById(R.id.tv_title);
            viewsTextView = view.findViewById(R.id.tv_views);
            descTextView = view.findViewById(R.id.tv_desc);
            descLayout = view.findViewById(R.id.desc_layout);

            dropDownImageView = view.findViewById(R.id.iv_dropdown);

            likeLayout = view.findViewById(R.id.ll_like);
            addToPlaylistLayout = view.findViewById(R.id.ll_add_to_playlist);
            addToPlaylistLayout.setVisibility(View.GONE);
            whatsappLayout = view.findViewById(R.id.ll_whatsapp_share);
            fbLayout = view.findViewById(R.id.ll_fb_share);
            feedBackLayout = view.findViewById(R.id.ll_feedback);

            likeImageView = view.findViewById(R.id.iv_like);
            likeTextView = view.findViewById(R.id.tv_like);
            whatsappTextView = view.findViewById(R.id.tv_whatsapp_share);
            fbTextView = view.findViewById(R.id.tv_fb_share);

            brandLayout = view.findViewById(R.id.ll_brand);
            brandImageView = view.findViewById(R.id.iv_brand_logo);
            brandTextView = view.findViewById(R.id.tv_brand);

            followLayout = view.findViewById(R.id.ll_follow);
            followTextView = view.findViewById(R.id.tv_follow);
            providerLayout = view.findViewById(R.id.ll_provider);

            dropDownImageView.setOnClickListener(this);
            likeLayout.setOnClickListener(this);
            addToPlaylistLayout.setOnClickListener(this);
            whatsappLayout.setOnClickListener(this);
            fbLayout.setOnClickListener(this);
            followLayout.setOnClickListener(this);
            brandLayout.setOnClickListener(this);
            feedBackLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_dropdown:
                    if (descLayout.getVisibility() == View.VISIBLE) {
                        ViewAnimationUtils.collapse(descLayout, new ViewAnimationUtils.OnCompleteListener() {
                            @Override
                            public void onComplete() {
                                dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_down);
                                showDescriptionLayout = false;
                            }
                        });
                    } else {
                        ViewAnimationUtils.expand(descLayout, new ViewAnimationUtils.OnCompleteListener() {
                            @Override
                            public void onComplete() {
                                dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_up);
                                showDescriptionLayout = true;
                            }
                        });
                    }
                    break;

                case R.id.ll_like:
                    if (!isLikeProcessing) {
                        isLikeProcessing = true;
                        if (mVideoInfo.isLiked) {
                            sendActionToServer(ACTION_UNLIKE);
                        } else {
                            sendActionToServer(ACTION_LIKE);
                        }
                    }
                    break;

                case R.id.ll_add_to_playlist:
                    if (addPlaylistListener != null) {
                        boolean added = addPlaylistListener.onAddToPlaylist();
                        Context context = getContext();
                        if (context != null) {
                            if (added) {
                                showToast(context, "Added successfully into playlist.");
                            } else {
                                showToast(context, "Already added into playlist.");
                            }
                        }
                    }
                    break;

                case R.id.ll_whatsapp_share:
                    if (mVideoInfo != null) {
                        Activity activity = getActivity();
                        if (activity != null) {
                            BackgroundRequest.getStoryShareUrl(activity, mVideoInfo.link, new OnStoryShareUrlListener() {
                                @Override
                                public void onUrlFetch(boolean flag, String shareUrl) {
                                    if (ShareUtil.whatsappShare(activity, mVideoInfo.title, shareUrl))
                                        sendActionToServer(ACTION_WHATSAPP_SHARE);
                                }
                            });
                        }
                    }
                    break;

                case R.id.ll_fb_share:
                    if (mVideoInfo != null) {
                        Activity activity = getActivity();
                        if (activity != null) {
                            BackgroundRequest.getStoryShareUrl(activity, mVideoInfo.link, new OnStoryShareUrlListener() {
                                @Override
                                public void onUrlFetch(boolean flag, String shareUrl) {
                                    if (ShareUtil.facebookShare(activity, mVideoInfo.title, shareUrl)) {
                                        sendActionToServer(ACTION_FB_SHARE);
                                    } else {
                                        AppUtils.getInstance().showCustomToast(activity, "Please install Facebook first.");
                                    }
                                }
                            });
                        }
                    }
                    break;

                case R.id.ll_follow:
                    if (isSubscribed) {
                        showDialogToUnfollow(mVideoInfo.providerName);
                    } else {
                        sendActionToServer(ACTION_FOLLOW);
                    }
                    break;

                case R.id.ll_brand:
                    Context context = getContext();
                    if (context != null && mVideoInfo != null) {
//                        String url = Urls.APP_FEED_BASE_URL + AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.VideoPref.VALUE_VIDEO_PROVIDER, Urls.DEFAULT_VIDEO_PROVIDER);
                        String url = Urls.APP_FEED_BASE_URL + AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.VideoPref.SETTINGS_VIDEO_PROVIDER_URL, Urls.DEFAULT_VIDEO_PROVIDER);

                        Intent intent = new Intent(context, ProviderVideoListActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(ProviderVideoListActivity.EXTRA_PROVIDER_ID, mVideoInfo.providerId);
                        intent.putExtra(ProviderVideoListActivity.EXTRA_PROVIDER_NAME, mVideoInfo.providerName);
                        intent.putExtra(ProviderVideoListActivity.EXTRA_URL, url);
                        intent.putExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL, mSectionLabel);
                        intent.putExtra(Constants.KeyPair.KEY_FEED_URL, mFeedUrl);
                        intent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, mDetailUrl);
                        intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, mGaEventLabel);
                        intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, mGaArticle);
                        intent.putExtra(Constants.KeyPair.KEY_FEED_URL_IS_PERSONALIZED, mIsPersonalizedFeedUrl);

                        startActivity(intent);
                    }
                    break;

//                case R.id.ll_feedback:
//                    // display the Feedback popup.
////                    showFeedbackDialog();
//                    RateUtil.getInstance().showRateArticleVideoDialog(getActivity(), mVideoInfo.storyId, getString(R.string.rate_video));
//                    break;
            }
        }
    }

    private void sendActionToServer(final int actionType) {
//        String url = Urls.APP_FEED_BASE_URL + AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.VideoPref.VALUE_VIDEO_ACTION, Urls.DEFAULT_VIDEO_ACTION);
        String url = Urls.APP_FEED_BASE_URL + AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.VideoPref.SETTINGS_VIDEO_ACTION, Urls.DEFAULT_VIDEO_ACTION);

        JSONObject jsonObject = new JSONObject();
        try {
            String mSessionId = TrackingData.getDbORDeviceId(getContext());
            jsonObject.put("session_id", mSessionId);
            jsonObject.put("channel_slno", CommonConstants.CHANNEL_ID);

            if (mVideoInfo != null) {
                switch (actionType) {
                    case ACTION_LIKE:
                        jsonObject.put("action", "like");
                        jsonObject.put("vid", mVideoInfo.storyId);
                        break;

                    case ACTION_UNLIKE:
                        jsonObject.put("action", "dislike");
                        jsonObject.put("vid", mVideoInfo.storyId);
                        break;

                    case ACTION_FOLLOW:
                        jsonObject.put("action", "follow");
                        jsonObject.put("vid", mVideoInfo.providerId);
                        break;

                    case ACTION_UNFOLLOW:
                        jsonObject.put("action", "unfollow");
                        jsonObject.put("vid", mVideoInfo.providerId);
                        break;

                    case ACTION_FB_SHARE:
                        jsonObject.put("action", "fbshare");
                        jsonObject.put("vid", mVideoInfo.storyId);
                        break;

                    case ACTION_WHATSAPP_SHARE:
                        jsonObject.put("action", "washare");
                        jsonObject.put("vid", mVideoInfo.storyId);
                        break;

                    case ACTION_VIEW:
                        jsonObject.put("action", "view");
                        jsonObject.put("vid", mVideoInfo.storyId);
                        break;
                }
            }
        } catch (JSONException e) {
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Systr.println("Action Json : " + response);
                parseActionData(response, actionType);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Action Error : " + error);
                parseActionData(null, actionType);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Context context = getContext();
        if (context != null) {
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(request);
        }
    }

    private void parseActionData(JSONObject response, int actionType) {
        String status = null;
        if (response != null)
            status = response.optString("status");

        Systr.println("Action Response : " + status);

        if (!TextUtils.isEmpty(status) && status.equals("1")) {
            switch (actionType) {
                case ACTION_LIKE:
                    if (mVideoInfo != null) {
                        mVideoInfo.isLiked = true;
                        mVideoInfo.likeCount = mVideoInfo.likeCount + 1;
                        booleanExpressionTable.updateBooleanValue(mVideoInfo.storyId, Constants.BooleanConst.VIDEO_LIKE, true);
                    }
                    isLikeProcessing = false;
                    break;

                case ACTION_UNLIKE:
                    if (mVideoInfo != null) {
                        mVideoInfo.isLiked = false;
                        int count;
                        mVideoInfo.likeCount = ((count = mVideoInfo.likeCount - 1) < 0 ? 0 : count);
                        booleanExpressionTable.updateBooleanValue(mVideoInfo.storyId, Constants.BooleanConst.VIDEO_LIKE, false);
                    }
                    isLikeProcessing = false;
                    break;

                case ACTION_FOLLOW:
                    isSubscribed = true;
                    if (mVideoInfo != null) {
                        booleanExpressionTable.updateBooleanValue(mVideoInfo.providerId, Constants.BooleanConst.VIDEO_FOLLOW, true);
//                        updateFollow(mVideoInfo.providerId, isSubscribed);
                    }
                    break;

                case ACTION_UNFOLLOW:
                    isSubscribed = false;
                    if (mVideoInfo != null) {
                        booleanExpressionTable.updateBooleanValue(mVideoInfo.providerId, Constants.BooleanConst.VIDEO_FOLLOW, false);
//                        updateFollow(mVideoInfo.providerId, isSubscribed);
                    }
                    break;

                case ACTION_FB_SHARE:
                    if (mVideoInfo != null)
                        mVideoInfo.fbShareCount = mVideoInfo.fbShareCount + 1;
                    break;

                case ACTION_WHATSAPP_SHARE:
                    if (mVideoInfo != null)
                        mVideoInfo.whatsappShareCount = mVideoInfo.whatsappShareCount + 1;
                    break;
            }

            if (actionType != ACTION_VIEW) {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }

        } else {

            Context context = getContext();
            if (context != null) {
                switch (actionType) {
                    case ACTION_LIKE:
                        isLikeProcessing = false;
                        showToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                        break;

                    case ACTION_UNLIKE:
                        isLikeProcessing = false;
                        showToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                        break;

                    case ACTION_FOLLOW:
                        showToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                        break;

                    case ACTION_UNFOLLOW:
                        showToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                        break;

                    case ACTION_FB_SHARE:
//                    showToast("FB fail");
                        break;

                    case ACTION_WHATSAPP_SHARE:
//                    showToast("Whatsapp fail");
                        break;
                }
            }
        }
    }

    private void showToast(Context context, String msg) {
        AppUtils.getInstance().showCustomToast(context, msg);
    }
}
