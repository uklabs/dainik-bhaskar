package com.db.dbvideoPersonalized.detail;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdsConstants;
import com.db.data.models.BulletinRootInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.source.server.BackgroundRequest;
import com.db.dbvideo.player.AppTraverseUtil;
import com.db.dbvideo.player.BhaskarVideoPlayerTrackerUtil;
import com.db.dbvideo.player.PlayerControllerCallbackReference;
import com.db.dbvideo.player.VideoPlayerConstant;
import com.db.dbvideo.player.VideoPlayerEventTracker;
import com.db.dbvideo.videoview.DeviceOrientationListener;
import com.db.dbvideo.videoview.VideoPlayerCallbackListener;
import com.db.dbvideo.videoview.VideoPlayerFragment;
import com.db.dbvideoPersonalized.OnVideoSelectedListener;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.listeners.OnStoryShareUrlListener;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.ShareUtil;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.db.views.TopCropImageView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.view.View.GONE;

public class VideoBulletinActivity extends BaseAppCompatActivity implements OnVideoSelectedListener, DeviceOrientationListener, VideoPlayerCallbackListener {

    public static final String EXTRA_VIDEO_INFO_LIST = "db_video_info_list";

    private VideoPlayerFragment videoPlayerFragment;
    private DailyMotionPlayerFragment dailyMotionPlayerFragment;
    private Handler handler = new Handler();

    private VideoInfo selectedVideoInfo;

    private ArrayList<VideoInfo> mList = new ArrayList<>();
    private int videoIndex;
    private String videoSource;
    private String sectionLabel;
    private String gaEventLabel;
    private String gaArticle;
    private String mColor;
    private CategoryInfo categoryInfo;

    private OrientationEventListener orientation;
    private int userSelectedOrientation = -1;
    private RelativeLayout headLayout;
    private BottomSheetBehavior bottomSheetBehavior;
    private PlayListAdapter playListAdapter;
    private ProgressBar progressBar;

    public static Intent getIntent(Context context, ArrayList<VideoInfo> infoList, int videoIndex, String videoSource, String sectionLabel, String gaEventLabel, String gaArticle, String color, CategoryInfo categoryInfo) {
        Intent intent = new Intent(context, VideoBulletinActivity.class);
        intent.putExtra(EXTRA_VIDEO_INFO_LIST, infoList);
        intent.putExtra(Constants.KeyPair.KEY_POSITION, videoIndex);
        intent.putExtra(Constants.KeyPair.KEY_SOURCE, videoSource);
        intent.putExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL, sectionLabel);
        intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
        intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        intent.putExtra(Constants.KeyPair.KEY_COLOR, color);
        intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_video_bulletin);
        loadBundleData();
        initViews();

        if (mList == null || mList.isEmpty()) {
            makeJsonObjectRequest();
        } else {
            handleUI();
        }
    }

    private void makeJsonObjectRequest() {
        String finalFeedURL = String.format("%s%s", Urls.APP_FEED_BASE_URL, categoryInfo.feedUrl);
        Systr.println("Feed Url : " + finalFeedURL);
        progressBar.setVisibility(View.VISIBLE);
        if (NetworkStatus.getInstance().isConnected(this)) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            parseFeed(response);
                        } catch (Exception ignored) {
                        }

                        progressBar.setVisibility(View.GONE);
                    }, error -> {
                progressBar.setVisibility(View.GONE);
                if (error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(VideoBulletinActivity.this, getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(VideoBulletinActivity.this, getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(VideoBulletinActivity.this).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(VideoBulletinActivity.this, getResources().getString(R.string.no_network_error));
        }
    }

    private void parseFeed(JSONObject response) {
        BulletinRootInfo bulletinRootInfo = new Gson().fromJson(response.toString(), BulletinRootInfo.class);
        mList.clear();
        mList.addAll(bulletinRootInfo.video.feed);
        //notify adapter
        videoIndex = 0;
        handleUI();
    }


    private void loadBundleData() {
        // fetch data from intent
        Intent intent = getIntent();
        if (intent != null) {
            Bundle extraBundle = intent.getExtras();
            if (extraBundle != null) {
                try {
                    mList = (ArrayList<VideoInfo>) extraBundle.getSerializable(EXTRA_VIDEO_INFO_LIST);
                    videoIndex = extraBundle.getInt(Constants.KeyPair.KEY_POSITION);
                    videoSource = extraBundle.getString(Constants.KeyPair.KEY_SOURCE);
                    sectionLabel = intent.getStringExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL);
                    gaEventLabel = extraBundle.getString(Constants.KeyPair.KEY_GA_EVENT_LABEL);
                    gaArticle = extraBundle.getString(Constants.KeyPair.KEY_GA_ARTICLE);
                    mColor = extraBundle.getString(Constants.KeyPair.KEY_COLOR, "");
                    categoryInfo = (CategoryInfo) extraBundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
                } catch (Exception e) {
                }
            }
        }
    }

    private void handleUI() {
        if (mList != null) {
            if (mList.size() > videoIndex) {
                selectedVideoInfo = mList.get(videoIndex);
            } else if (mList.size() > 0) {
                selectedVideoInfo = mList.get(0);
                videoIndex = 0;
            }
            mList.remove(videoIndex);
        }

        if (selectedVideoInfo != null) {
            // add video fragment
            setTitleAndShare(selectedVideoInfo.titleHead, selectedVideoInfo.title, mColor);
            addVideoFragment(selectedVideoInfo.internalVideo, selectedVideoInfo.dailyMotionVideoId, selectedVideoInfo.image, selectedVideoInfo.title, selectedVideoInfo.link, AdsConstants.getIntFromString(""), true, selectedVideoInfo.portraitVideo == 1);
        }

        orientation = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int i) {

                if (Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1) {
                    if ((i > 85 && i < 95) || (i > 265 && i < 275)) {
                        if (userSelectedOrientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                            if (getResources().getConfiguration().orientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                            }
                            userSelectedOrientation = -1;
                        }
                    } else if ((i > 350 && i <= 360) || (i > 170 && i < 190)) {
                        if (userSelectedOrientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                            if (getResources().getConfiguration().orientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                            }
                            userSelectedOrientation = -1;
                        }
                    }
                }
            }
        };

        if (playListAdapter != null) {
            playListAdapter.notifyDataSetChanged();
        }

        // Hide header & bottom after 3sec
        new Handler().postDelayed(() -> {
            hideHeader();
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }, 3000);
    }

    private void initViews() {
        RelativeLayout llBottomSheet = findViewById(R.id.bottom_layout);
        progressBar = findViewById(R.id.progress_bar);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        headLayout = findViewById(R.id.head_rl);
        ((ImageView) findViewById(R.id.dropdown_iv)).setImageResource(R.drawable.sc_arrow_down);

        findViewById(R.id.dropdown_iv).setOnClickListener(view -> {
            if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                showHeader();
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                hideHeader();
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (i == BottomSheetBehavior.STATE_EXPANDED) {
                    ((ImageView) findViewById(R.id.dropdown_iv)).setImageResource(R.drawable.sc_arrow_down);
                    showHeader();
                } else {
                    ((ImageView) findViewById(R.id.dropdown_iv)).setImageResource(R.drawable.sc_arrow_up);
                    hideHeader();
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {
            }
        });

        findViewById(R.id.back_iv).setOnClickListener(view -> VideoBulletinActivity.this.finish());
        findViewById(R.id.share_iv).setOnClickListener(view -> {
            if (selectedVideoInfo != null)
                BackgroundRequest.getStoryShareUrl(VideoBulletinActivity.this, selectedVideoInfo.link, new OnStoryShareUrlListener() {
                    @Override
                    public void onUrlFetch(boolean flag, String shareUrl) {
                        ShareUtil.shareDefault(VideoBulletinActivity.this, selectedVideoInfo.title, getResources().getString(R.string.checkout_this_video), shareUrl, selectedVideoInfo.gTrackUrl, true);
                    }
                });
        });


        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerView.addItemDecoration(new SpacesDecoration(getResources().getDimensionPixelSize(R.dimen.list_spacing_min), getResources().getDimensionPixelSize(R.dimen.list_spacing_min), 0, 0));

        playListAdapter = new PlayListAdapter();
        recyclerView.setAdapter(playListAdapter);
    }


    private void addVideoFragment(String videoUrl, String dailyMotionVideoId, String imageUrl, String title, String shareLink, int vwallbrandid, boolean autoPlay, boolean hideZoom) {
        if (!TextUtils.isEmpty(dailyMotionVideoId)) {
            addDailyMotionVideoFragment(dailyMotionVideoId, title, shareLink, autoPlay);
        } else {
            addVideoPlayerFragment(videoUrl, imageUrl, title, shareLink, vwallbrandid, autoPlay, hideZoom);
        }
    }

    private void addVideoPlayerFragment(String videoUrl, String imageUrl, String title, String shareLink, int vwallbrandid, boolean autoPlay, boolean hideZoom) {
        videoPlayerFragment = new VideoPlayerFragment();

        Bundle bundle = new Bundle();
//        VideosAdsControl videoAdsControl = JsonParser.getVideoAdsControl(this);
//        videoAdsControl.setActive(AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.AdPref.VIDEO_TOGGLE, false));
//        videoAdsControl.setFromArticle(false);
//        videoAdsControl.setImavasturl(String.format((videoAdsControl.getImavasturl() + Constants.VIDEO_AD_URL_EXTRA), videoUrl, title, AdsConstants.getContentUrl(shareLink), String.valueOf(new Date().getTime())));
//        videoAdsControl.setContent_url(videoUrl);
//        bundle.putParcelable(VideoPlayerConstant.AD_CONTROL, videoAdsControl);

        bundle.putString(VideoPlayerConstant.VIDEO_URL, videoUrl);
        bundle.putInt(VideoPlayerConstant.VWALLBRANDID, vwallbrandid);
        bundle.putInt(VideoPlayerConstant.DEFAULT_THUMBNAIL_IMAGE_RES_ID, R.drawable.water_mark_news_detail);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NATIVE_CONTROLS, false);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NEXT_BUTTON, true);
        bundle.putBoolean(VideoPlayerConstant.HIDE_ZOOM_BUTTON, hideZoom);
        bundle.putBoolean(VideoPlayerConstant.HIDE_SHARE_BUTTON, true);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_PLAY, autoPlay);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_NEXT_PLAY, true);
        bundle.putString(VideoPlayerConstant.SHARE_TITLE, title);
        bundle.putString(VideoPlayerConstant.SHARE_LINK, shareLink);
        bundle.putString(Constants.KeyPair.KEY_GA_G_TRACK_URL, selectedVideoInfo.gTrackUrl);

        videoPlayerFragment.setArguments(bundle);

        setTrackingData(selectedVideoInfo, videoUrl);

        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.videoContainer, videoPlayerFragment).commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }

        PlayerControllerCallbackReference playerControllerCallbackReference = videoPlayerFragment.getPlayerControllerCallbackReference();
        if (playerControllerCallbackReference == null) {
            runHandler(imageUrl);
        }

    }

    private void addDailyMotionVideoFragment(String dailyMotionVideoId, String title, String shareLink, boolean isAutoPlay) {
        dailyMotionPlayerFragment = new DailyMotionPlayerFragment();

        Bundle bundle = new Bundle();
        bundle.putString(VideoPlayerConstant.VIDEO_DAILY_MOTION_ID, dailyMotionVideoId);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NEXT_BUTTON, true);
        bundle.putBoolean(VideoPlayerConstant.HIDE_SHARE_BUTTON, true);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_PLAY, isAutoPlay);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_NEXT_PLAY, true);
        bundle.putString(VideoPlayerConstant.SHARE_TITLE, title);
        bundle.putString(VideoPlayerConstant.SHARE_LINK, shareLink);

        dailyMotionPlayerFragment.setArguments(bundle);

        setTrackingDataForDailyMotion(selectedVideoInfo);

        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.videoContainer, dailyMotionPlayerFragment).commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }

    }

    private void setTitleAndShare(String iitl, String text, String iitlColor) {
        ((TextView) findViewById(R.id.title_tv)).setText(AppUtils.getInstance().getHomeTitleNew(iitl, AppUtils.getInstance().fromHtml(text), iitlColor));
    }

    private void showHeader() {
        headLayout.animate()
                .translationY(0).alpha(1.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        headLayout.setVisibility(View.VISIBLE);
                        headLayout.setAlpha(0.0f);
                    }
                });
    }

    private void hideHeader() {
        headLayout.animate()
                .translationY(-headLayout.getHeight()).alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        headLayout.setVisibility(GONE);
                    }
                });
    }

    @Override
    public void onVideoSelected(VideoInfo dbVideosInfo, String source, int position) {
        if (dbVideosInfo != null) {
            try {
                VideoInfo preVideoInfo = (VideoInfo) selectedVideoInfo.clone();
                if (preVideoInfo != null) {
                    mList.add(preVideoInfo);
                    mList.remove(position);
                    if (playListAdapter != null) {
                        playListAdapter.notifyDataSetChanged();
                    }
                }
            } catch (CloneNotSupportedException e) {
            }
            selectedVideoInfo = dbVideosInfo;
            videoSource = source;
            videoIndex = position;

            setTitleAndShare(selectedVideoInfo.titleHead, selectedVideoInfo.title, mColor);
            addVideoFragment(dbVideosInfo.internalVideo, dbVideosInfo.dailyMotionVideoId, dbVideosInfo.image, dbVideosInfo.title, dbVideosInfo.link, AdsConstants.getIntFromString(dbVideosInfo.wall_brand_id), true, selectedVideoInfo.portraitVideo == 1);
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        } else {
            super.onBackPressed();
        }
    }

    private void setThumbnailImage(final String imageUrl) {
        if (videoPlayerFragment != null) {
            PlayerControllerCallbackReference playerControllerCallbackReference = videoPlayerFragment.getPlayerControllerCallbackReference();
            if (playerControllerCallbackReference == null) {
                runHandler(imageUrl);
                return;
            }

            final ImageView thumbnailImageView = playerControllerCallbackReference.getThumbnailImageView();
            ImageUtil.setImage(getApplicationContext(), imageUrl, thumbnailImageView, R.drawable.water_mark_news_detail);
        }
    }

    private void runHandler(final String imageUrl) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setThumbnailImage(imageUrl);
            }
        }, 500);
    }

    @Override
    public void changeOrientation() {
        if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            userSelectedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            userSelectedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (videoPlayerFragment != null) {
            videoPlayerFragment.changeVideoViewScreen();
        }

        if (dailyMotionPlayerFragment != null) {
            dailyMotionPlayerFragment.changeVideoOrientation();
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (videoPlayerFragment != null) {
                    videoPlayerFragment.redrawControls();
                }
            }
        }, 500);

        hideUnhideDetailLayout();
    }

    private void hideUnhideDetailLayout() {
        int orientation = getResources().getConfiguration().orientation;
        switch (orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                showVideoDetailView();
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                hideVideoDetailView();
                break;
        }
    }

    private void hideVideoDetailView() {
        hideStatusBar();

        findViewById(R.id.bottom_space_view).setVisibility(View.GONE);

        if (videoPlayerFragment != null && videoPlayerFragment.mainController != null) {
            videoPlayerFragment.mainController.changeScreenSwitcherIcon(R.drawable.ic_video_fullscreen_exit);
        }
    }

    private void showVideoDetailView() {
        showStatusBar();

        findViewById(R.id.bottom_space_view).setVisibility(View.VISIBLE);

        if (videoPlayerFragment != null && videoPlayerFragment.mainController != null) {
            videoPlayerFragment.mainController.changeScreenSwitcherIcon(R.drawable.ic_video_fullscreen);
        }
    }

    @Override
    public void onVideoStart() {
    }

    @Override
    public void onVideoComplete() {
        return;
    }

    @Override
    public void onVideoError() {
    }

    @Override
    public void onNextButtonclick() {
        if (mList.size() > 0) {
            VideoInfo videoInfo = mList.get(0);
            onVideoSelected(videoInfo, videoSource, 0);
        }
    }

    private void setTrackingData(VideoInfo dbVideosInfo, String videoUrl) {
        if (TextUtils.isEmpty(gaEventLabel)) {
            gaEventLabel = CommonConstants.EVENT_LABEL;
        }
        AppTraverseUtil.setLanguage(gaEventLabel);
        AppTraverseUtil.setVideoTitle(dbVideosInfo.mediaTitle);
        AppTraverseUtil.setStoryId(dbVideosInfo.storyId);
        AppTraverseUtil.setBaseSectionSubSectionValue(sectionLabel);
        AppTraverseUtil.setVideoSource(videoSource);
        AppTraverseUtil.setVideoIndex(videoIndex + 1);

        BhaskarVideoPlayerTrackerUtil.setScreenName(AppTraverseUtil.getScreenNameForVideoTracking());
        String ultimaTrackingUrlWithParameter = dbVideosInfo.ultimaTrackUrl + "&http_referer=" + dbVideosInfo.link;
        VideoPlayerEventTracker videoPlayerEventTracker = VideoPlayerEventTracker.getInstance(this, videoUrl, ultimaTrackingUrlWithParameter, dbVideosInfo.storyId);
        BhaskarVideoPlayerTrackerUtil.setPlayerEventCallBackReference(videoPlayerEventTracker);

        // Tracking
        String source = AppPreferences.getInstance(VideoBulletinActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(VideoBulletinActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(VideoBulletinActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(VideoBulletinActivity.this, InitApplication.getInstance().getDefaultTracker(), gaArticle + (TextUtils.isEmpty(dbVideosInfo.mediaTitle) ? "" : ("-" + dbVideosInfo.mediaTitle)), source, medium, campaign);
    }

    private void setTrackingDataForDailyMotion(VideoInfo dbVideosInfo) {
        if (TextUtils.isEmpty(gaEventLabel)) {
            gaEventLabel = CommonConstants.EVENT_LABEL;
        }
        AppTraverseUtil.setLanguage(gaEventLabel);
        AppTraverseUtil.setVideoTitle(dbVideosInfo.mediaTitle);
        AppTraverseUtil.setStoryId(dbVideosInfo.storyId);
        AppTraverseUtil.setBaseSectionSubSectionValue(sectionLabel);
        AppTraverseUtil.setVideoSource(videoSource);
        AppTraverseUtil.setVideoIndex(videoIndex + 1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // Hide Status Bar
    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);

        } else if (Build.VERSION.SDK_INT >= 16) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    }

    //Show Status Bar
    private void showStatusBar() {
        if (Build.VERSION.SDK_INT >= 16) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (orientation != null) {
            orientation.enable();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (orientation != null) {
            orientation.disable();
        }
    }

    class PlayListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_video_bulletin_grid, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            VideoInfo videoInfo = mList.get(position);

            if (videoInfo != null) {
                if (videoInfo.portraitVideo == 1) {
                    ((ViewHolder) holder).imageviewTop.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).imageview.setVisibility(GONE);
                    ImageUtil.setImage(VideoBulletinActivity.this, videoInfo.image, ((ViewHolder) holder).imageviewTop, 0);
                }
                else {
                    ((ViewHolder) holder).imageview.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).imageviewTop.setVisibility(GONE);
                    ImageUtil.setImage(VideoBulletinActivity.this, videoInfo.image, ((ViewHolder) holder).imageview, 0);
                }
                }

            ((ViewHolder) holder).textview.setText(AppUtils.getInstance().getHomeTitle(videoInfo.titleHead, AppUtils.getInstance().fromHtml(videoInfo.title), mColor));

            holder.itemView.setOnClickListener(view -> {
                final VideoInfo info = mList.get(position);
                if (NetworkStatus.getInstance().isConnected(VideoBulletinActivity.this)) {
                    onVideoSelected(info, AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION, position);
                } else {
                    Toast.makeText(VideoBulletinActivity.this, getResources().getString(R.string.internet_connection_error_), Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return mList != null ? mList.size() : 0;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TopCropImageView imageviewTop;
        ImageView imageview;
        TextView textview;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageview = itemView.findViewById(R.id.iv_video_thum);
            imageviewTop = itemView.findViewById(R.id.iv_video_thumb_top_crop);
            textview = itemView.findViewById(R.id.tv_text);
        }
    }
}
