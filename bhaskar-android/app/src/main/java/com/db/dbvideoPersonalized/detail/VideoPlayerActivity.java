package com.db.dbvideoPersonalized.detail;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdsConstants;
import com.db.dbvideo.player.AppTraverseUtil;
import com.db.dbvideo.player.BhaskarVideoPlayerTrackerUtil;
import com.db.dbvideo.player.PlayerControllerCallbackReference;
import com.db.dbvideo.player.VideoPlayerConstant;
import com.db.dbvideo.player.VideoPlayerEventTracker;
import com.db.dbvideo.videoview.DeviceOrientationListener;
import com.db.dbvideo.videoview.VideoPlayerCallbackListener;
import com.db.dbvideo.videoview.VideoPlayerFragment;
import com.db.dbvideoPersonalized.OnVideoSelectedListener;
import com.db.dbvideoPersonalized.WatchedPersonalizedVideoData;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;

import java.util.ArrayList;

public class VideoPlayerActivity extends BaseAppCompatActivity implements OnVideoSelectedListener, DeviceOrientationListener, VideoPlayerCallbackListener {

    public static final String EXTRA_VIDEO_INFO_LIST = "db_video_info_list";

    public VideoInfo selectedVideoInfo;
    private Handler handler = new Handler();

    private VideoPlayerFragment videoPlayerFragment;
    private DailyMotionPlayerFragment dailyMotionPlayerFragment;

    private VideoPlayerListFragment dbPlayerListFragment;

    private RelativeLayout bottomLayout;

    private int videoIndex;
    private int nextPageCount;
    private String feedUrl;
    private String videoSource;
    private String gaEventLabel;
    private String gaArticle;
    private String sectionLabel;
    private String mColor;
    private boolean clickViaNotification = false;

    private boolean isAutoPlay = true;
    private boolean isFullView;
    private int userSelectedOrientation = -1;

    private OrientationEventListener orientation;
    private int layoutType;
    private int themeType;
    public static ArrayList<VideoInfo> list = new ArrayList<>();

    public static Intent getIntent(Context context, int position, int nextPageCount, String feedUrl, String source, String gaEventLabel, String gaArticle, String sectionLabel, String color, int layoutType, int themeType) {
        Intent intent = new Intent(context, VideoPlayerActivity.class);
        try {
//            intent.putExtra("dataInJson", new Gson().toJson(infoList));
            intent.putExtra(Constants.KeyPair.KEY_POSITION, position);
            intent.putExtra(Constants.KeyPair.KEY_NEXT_PAGE_COUNT, nextPageCount);
            intent.putExtra(Constants.KeyPair.KEY_FEED_URL, feedUrl);
            intent.putExtra(Constants.KeyPair.KEY_SOURCE, source);
            intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
            intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
            intent.putExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL, sectionLabel);
            intent.putExtra(Constants.KeyPair.KEY_COLOR, color);
            intent.putExtra("layer", layoutType);
            intent.putExtra("theme", themeType);
        } catch (Exception ignored) {

        }

        return intent;
    }

    public static void addListData(ArrayList<VideoInfo> filteredList) {
        list.clear();
        list.addAll(filteredList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_video_player);

        bottomLayout = findViewById(R.id.bottom_layout);

        initViews();
        AppUtils.getInstance().setStatusBarColor(this, mColor);
    }

    private void initViews() {


        Intent intent = getIntent();
        if (intent != null) {
            Bundle extraBundle = intent.getExtras();
            if (extraBundle != null) {
//                String dataFromJson = extraBundle.getString("dataInJson");
//                Type listType = new TypeToken<ArrayList<VideoInfo>>() {
//                }.getType();
//                list = new Gson().fromJson(dataFromJson, listType);
                videoIndex = extraBundle.getInt(Constants.KeyPair.KEY_POSITION);
                nextPageCount = extraBundle.getInt(Constants.KeyPair.KEY_NEXT_PAGE_COUNT, 1);
                feedUrl = extraBundle.getString(Constants.KeyPair.KEY_FEED_URL);
                videoSource = extraBundle.getString(Constants.KeyPair.KEY_SOURCE);
                gaEventLabel = extraBundle.getString(Constants.KeyPair.KEY_GA_EVENT_LABEL);
                gaArticle = extraBundle.getString(Constants.KeyPair.KEY_GA_ARTICLE);
                sectionLabel = intent.getStringExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL);
                mColor = extraBundle.getString(Constants.KeyPair.KEY_COLOR, "");
                layoutType = extraBundle.getInt("layer", VideoPlayerListFragment.TYPE_GRID);
                themeType = extraBundle.getInt("theme", VideoPlayerListFragment.THEME_DARK);

                clickViaNotification = extraBundle.getBoolean(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION);
            }
        }

        if (list != null) {
            if (list.size() > videoIndex) {
                selectedVideoInfo = list.get(videoIndex);
                list.remove(videoIndex);
            } else if (list.size() > 0) {
                selectedVideoInfo = list.get(0);
                list.remove(0);
                videoIndex = 0;
            }
        }

        if (selectedVideoInfo != null) {
            // add video fragment
            addVideoFragment(selectedVideoInfo.internalVideo, selectedVideoInfo.dailyMotionVideoId, selectedVideoInfo.image, selectedVideoInfo.title, selectedVideoInfo.link, selectedVideoInfo.provider_code, AdsConstants.getIntFromString(selectedVideoInfo.providerId), AdsConstants.getIntFromString(selectedVideoInfo.wall_brand_id));

            // add recommendation list fragment
            dbPlayerListFragment = (VideoPlayerListFragment) getSupportFragmentManager().findFragmentById(R.id.player_list_fragment);
            dbPlayerListFragment.setData(list, layoutType, themeType, nextPageCount, feedUrl, mColor);
            dbPlayerListFragment.setVideo(selectedVideoInfo, null);
        }

        orientation = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int i) {

                if (Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1) {
                    if ((i > 85 && i < 95) || (i > 265 && i < 275)) {
                        if (userSelectedOrientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                            if (getResources().getConfiguration().orientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                            }
                            userSelectedOrientation = -1;
                        }

                    } else if ((i > 350 && i <= 360) || (i > 170 && i < 190)) {
                        if (userSelectedOrientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                            if (getResources().getConfiguration().orientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                            }
                            userSelectedOrientation = -1;
                        }
                    }
                }
            }
        };
    }

    private void addVideoFragment(String videoUrl, String dailyMotionVideoId, String imageUrl, String title, String shareLink, String provider_code, int providerId, int vwallbrandid) {
        if (!TextUtils.isEmpty(dailyMotionVideoId)) {
            addDailyMotionVideoFragment(dailyMotionVideoId, title, shareLink);
        } else {
            addVideoPlayerFragment(videoUrl, imageUrl, title, shareLink, provider_code, providerId, vwallbrandid);
        }
    }

    private void addVideoPlayerFragment(String videoUrl, String imageUrl, String title, String shareLink, String provider_code, int providerId, int vwallbrandid) {
        videoPlayerFragment = new VideoPlayerFragment();

        Bundle bundle = new Bundle();
//        VideosAdsControl videoAdsControl = JsonParser.getVideoAdsControl(this);
//        videoAdsControl.setActive(AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.AdPref.VIDEO_TOGGLE, false));
//        videoAdsControl.setFromArticle(false);
//        videoAdsControl.setImavasturl(String.format((videoAdsControl.getImavasturl() + Constants.VIDEO_AD_URL_EXTRA), videoUrl, title, AdsConstants.getContentUrl(shareLink), String.valueOf(new Date().getTime())));
//        videoAdsControl.setContent_url(videoUrl);
//        bundle.putParcelable(VideoPlayerConstant.AD_CONTROL, videoAdsControl);

        bundle.putString(VideoPlayerConstant.VIDEO_URL, videoUrl);
        bundle.putString(VideoPlayerConstant.PROVIDER_CODE, provider_code);
        bundle.putInt(VideoPlayerConstant.PROVIDER_ID, providerId);
        bundle.putInt(VideoPlayerConstant.VWALLBRANDID, vwallbrandid);
        bundle.putInt(VideoPlayerConstant.DEFAULT_THUMBNAIL_IMAGE_RES_ID, R.drawable.water_mark_news_detail);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NATIVE_CONTROLS, false);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NEXT_BUTTON, true);
        bundle.putBoolean(VideoPlayerConstant.HIDE_SHARE_BUTTON, false);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_PLAY, isAutoPlay);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_NEXT_PLAY, AppPreferences.getInstance(VideoPlayerActivity.this).getBooleanValue(QuickPreferences.TOGGLING_VIDEO_AUTO_NEXT_PLAY, false));
        bundle.putString(VideoPlayerConstant.SHARE_TITLE, title);
        bundle.putString(VideoPlayerConstant.SHARE_LINK, shareLink);
        bundle.putString(Constants.KeyPair.KEY_GA_G_TRACK_URL, selectedVideoInfo.gTrackUrl);
        bundle.putInt(VideoPlayerConstant.VIDEO_IS_LIVE, selectedVideoInfo.isLive);

        videoPlayerFragment.setArguments(bundle);

        setTrackingData(selectedVideoInfo, videoUrl);

        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.videoContainer, videoPlayerFragment).commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }

        PlayerControllerCallbackReference playerControllerCallbackReference = videoPlayerFragment.getPlayerControllerCallbackReference();
        if (playerControllerCallbackReference == null) {
            runHandler(imageUrl);
        }

        if (AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.AdPref.VIDEO_BANNER_TOGGLE, false)) {
            AdController.showVideoGalleryBanner(this, findViewById(R.id.ads_view));
        }

    }

    private void addDailyMotionVideoFragment(String dailyMotionVideoId, String title, String shareLink) {
        dailyMotionPlayerFragment = new DailyMotionPlayerFragment();

        Bundle bundle = new Bundle();
        bundle.putString(VideoPlayerConstant.VIDEO_DAILY_MOTION_ID, dailyMotionVideoId);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NEXT_BUTTON, true);
        bundle.putBoolean(VideoPlayerConstant.HIDE_SHARE_BUTTON, false);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_PLAY, isAutoPlay);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_NEXT_PLAY, AppPreferences.getInstance(VideoPlayerActivity.this).getBooleanValue(QuickPreferences.TOGGLING_VIDEO_AUTO_NEXT_PLAY, false));
        bundle.putString(VideoPlayerConstant.SHARE_TITLE, title);
        bundle.putString(VideoPlayerConstant.SHARE_LINK, shareLink);

        dailyMotionPlayerFragment.setArguments(bundle);

        setTrackingDataForDailyMotion(selectedVideoInfo);

        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.videoContainer, dailyMotionPlayerFragment).commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }

        if (AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.AdPref.VIDEO_BANNER_TOGGLE, false)) {
            AdController.showVideoGalleryBanner(this, findViewById(R.id.ads_view));
        }

    }

    @Override
    public void onVideoSelected(VideoInfo dbVideosInfo, String source, int position) {
        if (dbVideosInfo != null) {
            VideoInfo preVideoInfo = null;
            try {
                preVideoInfo = (VideoInfo) selectedVideoInfo.clone();
            } catch (CloneNotSupportedException e) {
            }
            selectedVideoInfo = dbVideosInfo;
            videoSource = source;
            videoIndex = position;

            dbPlayerListFragment.setVideo(selectedVideoInfo, preVideoInfo);

            addVideoFragment(dbVideosInfo.internalVideo, dbVideosInfo.dailyMotionVideoId, dbVideosInfo.image, dbVideosInfo.title, dbVideosInfo.link, dbVideosInfo.provider_code, AdsConstants.getIntFromString(dbVideosInfo.providerId), AdsConstants.getIntFromString(dbVideosInfo.wall_brand_id));
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        } else if (isFullView) {
            fullPortraitLayout();

        } else {
            if (dailyMotionPlayerFragment != null) {
                dailyMotionPlayerFragment.stopPlayer();
            }
            if (clickViaNotification) {
                Intent intent = new Intent(VideoPlayerActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent();
                intent.putExtra("video_update", selectedVideoInfo);
                setResult(100, intent);
                finish();
            }
        }
    }

    private void setThumbnailImage(final String imageUrl) {
        if (videoPlayerFragment != null) {
            PlayerControllerCallbackReference playerControllerCallbackReference = videoPlayerFragment.getPlayerControllerCallbackReference();
            if (playerControllerCallbackReference == null) {
                runHandler(imageUrl);
                return;
            }

            final ImageView thumbnailImageView = playerControllerCallbackReference.getThumbnailImageView();
            ImageUtil.setImage(getApplicationContext(), imageUrl, thumbnailImageView, R.drawable.water_mark_news_detail);
        }
    }

    private void runHandler(final String imageUrl) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setThumbnailImage(imageUrl);
            }
        }, 500);
    }

    @Override
    public void changeOrientation() {
        if (selectedVideoInfo != null && selectedVideoInfo.portraitVideo == 1) {
            fullPortraitLayout();
        } else {
            if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
                System.out.println("Change orientation called land - port");
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                userSelectedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            } else {
                System.out.println("Change orientation called port - land");
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                userSelectedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (videoPlayerFragment != null) {
            videoPlayerFragment.changeVideoViewScreen();
        }

        if (dailyMotionPlayerFragment != null) {
            dailyMotionPlayerFragment.changeVideoOrientation();
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (videoPlayerFragment != null)
                    videoPlayerFragment.redrawControls();
            }
        }, 500);

        hideUnhideDetailLayout();
    }

    private void fullPortraitLayout() {
        if (isFullView) {
            isFullView = false;
            showVideoDetailView();
        } else {
            isFullView = true;
            hideVideoDetailView();
        }

        if (videoPlayerFragment != null) {
            videoPlayerFragment.changeVideoViewScreen();
        }
    }

    private void hideUnhideDetailLayout() {
        int orientation = getResources().getConfiguration().orientation;
        switch (orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                showVideoDetailView();
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                hideVideoDetailView();
                break;
        }
    }

    private void hideVideoDetailView() {
        hideStatusBar();

        bottomLayout.setVisibility(View.GONE);

        if (videoPlayerFragment != null && videoPlayerFragment.mainController != null) {
            videoPlayerFragment.mainController.changeScreenSwitcherIcon(R.drawable.ic_video_fullscreen_exit);
        }
    }

    private void showVideoDetailView() {
        showStatusBar();

        bottomLayout.setVisibility(View.VISIBLE);

        if (videoPlayerFragment != null && videoPlayerFragment.mainController != null) {
            videoPlayerFragment.mainController.changeScreenSwitcherIcon(R.drawable.ic_video_fullscreen);
        }
    }

    @Override
    public void onVideoStart() {
        if (selectedVideoInfo != null) {
            WatchedPersonalizedVideoData.getInstance(VideoPlayerActivity.this).saveVideo(AppUtils.getFilteredStoryId(selectedVideoInfo.storyId));
        }
    }

    @Override
    public void onVideoComplete() {
        return;
    }

    @Override
    public void onVideoError() {
    }

    @Override
    public void onNextButtonclick() {
        Systr.println("Next Button Called");
        if (dbPlayerListFragment != null) {

            VideoInfo firstItem = dbPlayerListFragment.getFirstItem();
            if (firstItem != null) {
                setThumbnailImage(firstItem.image);
                videoSource = AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION;

                onVideoSelected(firstItem, videoSource, 0);
//                dbPlayerListFragment.setVideo(firstItem, sectionLabel, detailUrl, feedUrl, gaArticle, gaEventLabel, mColor);

                Tracking.trackVideoImpression(VideoPlayerActivity.this, selectedVideoInfo.impressionTrackUrl, 0, 1, "auto_click", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);
            }
        }
    }

    private void setTrackingData(VideoInfo dbVideosInfo, String videoUrl) {
        if (TextUtils.isEmpty(gaEventLabel)) {
            gaEventLabel = CommonConstants.EVENT_LABEL;
        }
        AppTraverseUtil.setLanguage(gaEventLabel);
        AppTraverseUtil.setVideoTitle(dbVideosInfo.mediaTitle);
        AppTraverseUtil.setStoryId(dbVideosInfo.storyId);
        AppTraverseUtil.setBaseSectionSubSectionValue(sectionLabel);
        AppTraverseUtil.setVideoSource(videoSource);
        AppTraverseUtil.setVideoIndex(videoIndex + 1);

        BhaskarVideoPlayerTrackerUtil.setScreenName(AppTraverseUtil.getScreenNameForVideoTracking());
        String ultimaTrackingUrlWithParameter = dbVideosInfo.ultimaTrackUrl + "&http_referer=" + dbVideosInfo.link;
        VideoPlayerEventTracker videoPlayerEventTracker = VideoPlayerEventTracker.getInstance(this, videoUrl, ultimaTrackingUrlWithParameter, dbVideosInfo.storyId);
        BhaskarVideoPlayerTrackerUtil.setPlayerEventCallBackReference(videoPlayerEventTracker);

        // Tracking
        String source = AppPreferences.getInstance(VideoPlayerActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(VideoPlayerActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(VideoPlayerActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(VideoPlayerActivity.this, InitApplication.getInstance().getDefaultTracker(), gaArticle + (TextUtils.isEmpty(dbVideosInfo.mediaTitle) ? "" : ("-" + dbVideosInfo.mediaTitle)), source, medium, campaign);
    }

    private void setTrackingDataForDailyMotion(VideoInfo dbVideosInfo) {
        if (TextUtils.isEmpty(gaEventLabel)) {
            gaEventLabel = CommonConstants.EVENT_LABEL;
        }
        AppTraverseUtil.setLanguage(gaEventLabel);
        AppTraverseUtil.setVideoTitle(dbVideosInfo.mediaTitle);
        AppTraverseUtil.setStoryId(dbVideosInfo.storyId);
        AppTraverseUtil.setBaseSectionSubSectionValue(sectionLabel);
        AppTraverseUtil.setVideoSource(videoSource);
        AppTraverseUtil.setVideoIndex(videoIndex + 1);

        // Tracking
        String source = AppPreferences.getInstance(VideoPlayerActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(VideoPlayerActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(VideoPlayerActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(VideoPlayerActivity.this, InitApplication.getInstance().getDefaultTracker(), gaArticle + (TextUtils.isEmpty(dbVideosInfo.mediaTitle) ? "" : ("-" + dbVideosInfo.mediaTitle)), source, medium, campaign);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        list.clear();
    }

    // Hide Status Bar
    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);

        } else if (Build.VERSION.SDK_INT >= 16) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    }

    //Show Status Bar
    private void showStatusBar() {
        if (Build.VERSION.SDK_INT >= 16) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (orientation != null) {
            orientation.enable();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (orientation != null) {
            orientation.disable();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

}
