package com.db.dbvideoPersonalized.detail;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.R;
import com.db.data.source.server.BackgroundRequest;
import com.db.dbvideoPersonalized.OnVideoSelectedListener;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.listeners.OnStoryShareUrlListener;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.ShareUtil;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.ViewAnimationUtils;
import com.db.views.viewholder.LoadingViewHolder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VideoPlayerListFragment extends Fragment {

    public static final int TYPE_LIST = 1001;
    public static final int TYPE_GRID = 1002;

    public static final int THEME_DARK = 1003;
    public static final int THEME_LIGHT = 1004;

    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;

    private ProgressBar progressBar;
    private View retryLayout;

    private OnVideoSelectedListener videoSelectListener;
    private ArrayList<VideoInfo> dbVideosInfoArrayList;
    private PlayListAdapter adapter;

    private String mColor;
    //    private String mDetailUrl;
    private String mFeedUrl;

//    private String mSectionLabel = "";
//    private String mGaEventLabel;
//    private String mGaArticle;

    private VideoInfo mVideoInfo;

    private boolean isLoading;
    private int visibleThreshold = 2;
    private int pageCount = 1;
    private boolean isLoadMoreEnable = true;

    private int layoutType;
    private int themeType;
    private SpacesDecoration spacesItemDecoration;
    private RelativeLayout rootLayout;

    public VideoPlayerListFragment() {
    }

    private Rect scrollBounds;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            videoSelectListener = (OnVideoSelectedListener) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbVideosInfoArrayList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_player_list, container, false);

        spacesItemDecoration = new SpacesDecoration(getResources().getDimensionPixelSize(R.dimen.list_spacing_max), getResources().getDimensionPixelSize(R.dimen.list_spacing_max), 0, getResources().getDimensionPixelSize(R.dimen.list_spacing_max));

        rootLayout = view.findViewById(R.id.root_rl);
        retryLayout = view.findViewById(R.id.retry_layout);
        progressBar = view.findViewById(R.id.progressBar);
        recyclerView = view.findViewById(R.id.recycler_view);

        pageCount = 1;
        isLoadMoreEnable = true;

        view.findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryLayout.setVisibility(View.GONE);
                fetchDataFromServer();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.scrollBounds = new Rect();
        this.recyclerView.getHitRect(scrollBounds);
        adapter = new PlayListAdapter();

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (isLoadMoreEnable) {
                            onLoadMore();
                            isLoading = true;
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                checkForImpressionTracking();
            }
        });
    }

    private void setLayoutTypeRecyclerView() {
        if (layoutType == TYPE_GRID) {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.removeItemDecoration(spacesItemDecoration);
            recyclerView.addItemDecoration(spacesItemDecoration);
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    int itemViewType = adapter.getItemViewType(position);
                    if (itemViewType == PlayListAdapter.VIEW_TYPE_HEADING || itemViewType == PlayListAdapter.VIEW_TYPE_LOADING)
                        return 2;
                    else
                        return 1;
                }
            });
            mLayoutManager = gridLayoutManager;
        } else {
            mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.removeItemDecoration(spacesItemDecoration);
            recyclerView.addItemDecoration(spacesItemDecoration);
        }
    }

    private ArrayList<Integer> impressionList = new ArrayList<>();

    private void checkForImpressionTracking() {
        if (recyclerView != null) {
            View childAt;
            int height;

            VideoInfo itemAtPosition;
            int childAdapterPosition;
            for (int i = 0; i < recyclerView.getChildCount(); i++) {
                try {
                    childAt = recyclerView.getChildAt(i);
                    height = childAt.getHeight();
                    if (childAt.getLocalVisibleRect(scrollBounds)) {
                        if (scrollBounds.bottom == height) {
                            childAdapterPosition = recyclerView.getChildAdapterPosition(childAt);
                            if (childAdapterPosition > 0) {
                                itemAtPosition = adapter.getItemAtPosition(childAdapterPosition);
                                if (!impressionList.contains(childAdapterPosition)) {
                                    Tracking.trackVideoImpression(getContext(), itemAtPosition.impressionTrackUrl, 0, childAdapterPosition, "impression", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);
                                    impressionList.add(childAdapterPosition);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void checkForImpressionLoadTime() {
        if (recyclerView != null) {
            int firstPosition = mLayoutManager.findFirstVisibleItemPosition();
            int lastPosition = mLayoutManager.findLastCompletelyVisibleItemPosition();

            VideoInfo itemAtPosition;
            for (int i = firstPosition; i <= lastPosition; i++) {
                try {
                    if (i > 0) {
                        itemAtPosition = adapter.getItemAtPosition(i);
                        if (!impressionList.contains(i)) {
                            Tracking.trackVideoImpression(getContext(), itemAtPosition.impressionTrackUrl, 0, i, "impression", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);
                            impressionList.add(i);
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    public void onLoadMore() {
        if (TextUtils.isEmpty(mFeedUrl)) {
            return;
        }

        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (dbVideosInfoArrayList != null)
                    dbVideosInfoArrayList.add(null);

                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });

        //Load more data for reyclerview
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetchDataFromServer();
            }
        }, 500);
    }

    private void fetchDataFromServer() {
        String recommendedUrl = Urls.APP_FEED_BASE_URL + mFeedUrl + "PG" + pageCount + "/";
        makeJsonObjectRequest(recommendedUrl);
    }

    public void setData(ArrayList<VideoInfo> infoList, int layoutType, int themeType, int nextPageCount, String feedUrl, String color) {
        this.layoutType = layoutType;
        this.themeType = themeType;
        this.pageCount = nextPageCount;
        this.mFeedUrl = feedUrl;
        this.mColor = color;

        if (themeType == THEME_DARK) {
            rootLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.input_text_title_color));
        } else {
            rootLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        }

        setLayoutTypeRecyclerView();

        dbVideosInfoArrayList.clear();
        this.dbVideosInfoArrayList.addAll(infoList);
        adapter.notifyDataSetChanged();

        progressBar.setVisibility(View.GONE);
    }

    public void setVideo(VideoInfo info, VideoInfo prevVideoInfo) {
        this.mVideoInfo = info;

        if (dbVideosInfoArrayList.size() > 0) {
            dbVideosInfoArrayList.remove(info);
            if (prevVideoInfo != null) {
                dbVideosInfoArrayList.remove(prevVideoInfo);
                dbVideosInfoArrayList.add(prevVideoInfo);
            }
            adapter.notifyDataSetChanged();

            if (dbVideosInfoArrayList.size() < 1) {
                fetchDataFromServer();
            }
        } else {
            pageCount = 1;
            isLoadMoreEnable = true;
            impressionList.clear();
            fetchDataFromServer();
        }

        if (mLayoutManager != null)
            mLayoutManager.scrollToPosition(0);
    }

    private void makeJsonObjectRequest(String recommendedUrl) {
        Systr.println("Recommended Url for video : " + recommendedUrl);

        StringRequest jsonObjReq = new StringRequest(Request.Method.GET, recommendedUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Systr.println("Recommended Video response : " + response);
                        try {
                            try {
                                JSONObject object = new JSONObject(response);
                                parseFeedList(object.getJSONArray("feed"), false);
                            } catch (Exception e) {
                                try {
                                    JSONArray array = new JSONArray(response);
                                    parseFeedList(array, false);
                                } catch (Exception ignore) {
                                }
                            }
                        } catch (Exception ignore) {
                        }

//                        try {
//                            if (response != null && response.length() > 0) {
//                                parseFeedList(response.getJSONArray("feed"), false);
//                            }
//                        } catch (Exception e) {
//                        }

                        progressBar.setVisibility(View.GONE);
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Systr.println("Recommended Video Error : " + error);

                        progressBar.setVisibility(View.GONE);

                        Activity activity = getActivity();
                        if (activity != null && isAdded()) {

                            if (dbVideosInfoArrayList != null) {
                                dbVideosInfoArrayList.remove(null);
                            }
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }

                            if (error instanceof NoConnectionError || error instanceof NetworkError) {
                                showToast(activity, activity.getResources().getString(R.string.internet_connection_error_));
                            } else {
                                showToast(activity, activity.getResources().getString(R.string.sorry_error_found_please_try_again_));
                            }
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
    }

    private void parseFeedList(JSONArray resultArray, boolean clearList) throws JSONException {
        if (clearList) {
            dbVideosInfoArrayList.clear();
            adapter.notifyDataSetChanged();
        }

        String mStoryId = (mVideoInfo != null ? mVideoInfo.storyId : "");

        Gson gson = new Gson();
        JSONObject feedJsonObject;
        for (int i = 0; i < resultArray.length(); i++) {
            feedJsonObject = resultArray.getJSONObject(i);
            VideoInfo newsFeeds = gson.fromJson(feedJsonObject.toString(), VideoInfo.class);

            if (!TextUtils.isEmpty(newsFeeds.storyId) && !newsFeeds.storyId.equalsIgnoreCase(mStoryId) && !dbVideosInfoArrayList.contains(newsFeeds)) {
                dbVideosInfoArrayList.add(newsFeeds);
            }
        }

        // remove null item and page count for load more
        dbVideosInfoArrayList.remove(null);
        adapter.notifyDataSetChanged();

        if (resultArray.length() == 0) {
            isLoadMoreEnable = false;
            isLoading = false;

        } else {
            isLoading = false;
            pageCount++;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkForImpressionLoadTime();
            }
        }, 200);

        //TODO WHY Fraink sir Why
      /*  try {
            if (dbVideosInfoArrayList.size() < 5 && NetworkStatus.getInstance().isConnected(getContext())) {
                onLoadMore();
            }
        } catch (Exception e) {
        }*/
    }

    public VideoInfo getFirstItem() {
        if (dbVideosInfoArrayList != null && dbVideosInfoArrayList.size() > 0)
            return dbVideosInfoArrayList.get(0);
        return null;
    }

    private class PlayListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        public static final int VIEW_TYPE_HEADING = 1;
        public static final int VIEW_TYPE_LOADING = 2;
        public static final int VIEW_TYPE_ITEM_LIST = 3;
        public static final int VIEW_TYPE_ITEM_GRID = 4;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_HEADING:
                    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_video_player_list_heading, parent, false);
                    return new FirstViewHolder(itemView);
                case VIEW_TYPE_LOADING:
                    View loadingView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false);
                    return new LoadingViewHolder(loadingView);
                case VIEW_TYPE_ITEM_GRID:
                    View gridView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_video_player_list_video_grid, parent, false);
                    return new VideoViewHolder(gridView);
                default:
                    View listView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_video_player_list_video, parent, false);
                    return new VideoViewHolder(listView);
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof VideoViewHolder) {
                final VideoViewHolder viewHolder = (VideoViewHolder) holder;

                final int pos = position - 1;
                final VideoInfo info = dbVideosInfoArrayList.get(pos);

                ImageUtil.setImage(getContext(), info.image, viewHolder.iconImageView, R.drawable.water_mark_news_list);
                if (layoutType == TYPE_GRID) {
                    viewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitleNew(info.titleHead, AppUtils.getInstance().fromHtml(info.title), mColor));
                } else {
                    viewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(info.titleHead, AppUtils.getInstance().fromHtml(info.title), mColor));
                }

                viewHolder.playImageView.setColorFilter(AppUtils.getThemeColor(getContext(), mColor));

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Context context = getContext();
                        if (context != null) {
                            if (NetworkStatus.getInstance().isConnected(context)) {
                                if (videoSelectListener != null) {
                                    videoSelectListener.onVideoSelected(info, AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION, pos);
                                }

                                Tracking.trackVideoImpression(getContext(), info.impressionTrackUrl, 0, pos, "click", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);

                            } else {
                                showToast(context, context.getResources().getString(R.string.internet_connection_error_));
                            }
                        }
                    }
                });

            } else if (holder instanceof FirstViewHolder) {

                FirstViewHolder firstViewHolder = (FirstViewHolder) holder;
                if (mVideoInfo != null) {

                    Context context = getContext();
                    if (context != null) {
                        if (themeType == THEME_DARK) {
                            firstViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitleNew(mVideoInfo.titleHead, AppUtils.getInstance().fromHtml(mVideoInfo.title), mColor));
                            firstViewHolder.descTextView.setTextColor(Color.LTGRAY);
                        } else {
                            firstViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(mVideoInfo.titleHead, AppUtils.getInstance().fromHtml(mVideoInfo.title), mColor));
                            firstViewHolder.descTextView.setTextColor(Color.DKGRAY);
                        }
                        firstViewHolder.descTextView.setText(mVideoInfo.story);

                        firstViewHolder.shareImageView.setColorFilter(AppUtils.getThemeColor(context, mColor));
                        firstViewHolder.dropDownImageView.setColorFilter(AppUtils.getThemeColor(context, mColor));
                        firstViewHolder.dropDownTextView.setTextColor(AppUtils.getThemeColor(context, mColor));
                    }
                }

                if (!showDescriptionLayout) {
                    firstViewHolder.descLayout.setVisibility(View.GONE);
                    firstViewHolder.dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_down);
                }

            } else if (holder instanceof LoadingViewHolder) {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return dbVideosInfoArrayList == null ? 0 : (dbVideosInfoArrayList.size() + 1);
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0)
                return VIEW_TYPE_HEADING;
            else if (dbVideosInfoArrayList.get(position - 1) == null)
                return VIEW_TYPE_LOADING;
            else if (layoutType == TYPE_GRID) {
                return VIEW_TYPE_ITEM_GRID;
            } else {
                return VIEW_TYPE_ITEM_LIST;
            }
        }

        public VideoInfo getItemAtPosition(int position) {
            if (position > 0 && position < dbVideosInfoArrayList.size())
                return dbVideosInfoArrayList.get(position - 1);
            return null;
        }
    }

    private class VideoViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView;
        ImageView iconImageView;
        ImageView playImageView;

        VideoViewHolder(View view) {
            super(view);

            iconImageView = view.findViewById(R.id.iv_video_icon);
            playImageView = view.findViewById(R.id.video_play_icon);
            titleTextView = view.findViewById(R.id.tv_title);
        }
    }

    private boolean showDescriptionLayout = false;

    private class FirstViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView titleTextView;
        LinearLayout dropDownLayout;
        TextView dropDownTextView;
        ImageView dropDownImageView;
        ImageView shareImageView;
        TextView descTextView;
        LinearLayout descLayout;

        FirstViewHolder(View view) {
            super(view);

            titleTextView = view.findViewById(R.id.tv_title);
            descTextView = view.findViewById(R.id.tv_desc);
            descLayout = view.findViewById(R.id.desc_layout);

            dropDownLayout = view.findViewById(R.id.dropdown_layout);
            dropDownImageView = view.findViewById(R.id.iv_dropdown);
            dropDownTextView = view.findViewById(R.id.tv_dropdown);
            dropDownLayout.setOnClickListener(this);

            shareImageView = view.findViewById(R.id.iv_share);
            shareImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.dropdown_layout:
                    if (descLayout.getVisibility() == View.VISIBLE) {
                        ViewAnimationUtils.collapse(descLayout, () -> {
                            dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_down);
                            dropDownTextView.setText(getContext().getString(R.string.read_more_));
                            showDescriptionLayout = false;
                        });
                    } else {
                        ViewAnimationUtils.expand(descLayout, () -> {
                            dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_up);
                            dropDownTextView.setText(getContext().getString(R.string.close));
                            showDescriptionLayout = true;
                        });
                    }
                    break;

                case R.id.iv_share:
                    Context context = getContext();
                    if (context != null) {
                        BackgroundRequest.getStoryShareUrl(context, mVideoInfo.link, new OnStoryShareUrlListener() {
                            @Override
                            public void onUrlFetch(boolean flag, String shareUrl) {
                                ShareUtil.shareDefault(context, mVideoInfo.title, context.getResources().getString(R.string.checkout_this_video), shareUrl, mVideoInfo.gTrackUrl, true);
                            }
                        });
                    }
                    break;
            }
        }
    }

    private void showToast(Context context, String msg) {
        AppUtils.getInstance().showCustomToast(context, msg);
    }
}
