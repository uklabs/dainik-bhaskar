package com.db.dbvideoPersonalized.playlist;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.ReadUnreadStatusTable;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.DBVideoPlayerActivity2;
import com.db.dbvideoPersonalized.video.VideoAdapter;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.db.util.AppUtils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by DB on 03-11-2017.
 */

public class ProviderVideoListActivity extends BaseAppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, VideoAdapter.OnVideoActionListener {

    public static final String EXTRA_PROVIDER_ID = "provider_id";
    public static final String EXTRA_PROVIDER_NAME = "provider_name";
    public static final String EXTRA_URL = "url";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private VideoAdapter adapter;

    private String providerId;
    private String providerName;
    private String mUrl;

    private int pageCount = 0;
    private boolean isLoading = false;
    private boolean isLoadMoreEnable = true;
    private int visibleThreshold = 5;

    private String videoSource;
    private int videoIndex;
    private String detailUrl;
    private String feedUrl;
    private String sectionLabel;
    private String gaEventLabel;
    private boolean isPersonalizedFeedUrl;
    private String gaArticle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_video_playlist);

        Intent intent = getIntent();
        if (intent != null) {
            providerName = intent.getStringExtra(EXTRA_PROVIDER_NAME);
            providerId = intent.getStringExtra(EXTRA_PROVIDER_ID);
            mUrl = intent.getStringExtra(EXTRA_URL);

            sectionLabel = intent.getStringExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL);
            videoSource = intent.getStringExtra(Constants.KeyPair.KEY_SOURCE);
            videoIndex = intent.getIntExtra(Constants.KeyPair.KEY_POSITION, -1);
            detailUrl = intent.getStringExtra(Constants.KeyPair.KEY_DETAIL_URL);
            feedUrl = intent.getStringExtra(Constants.KeyPair.KEY_FEED_URL);
            gaEventLabel = intent.getStringExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL);
            gaArticle = intent.getStringExtra(Constants.KeyPair.KEY_GA_ARTICLE);
            isPersonalizedFeedUrl = intent.getBooleanExtra(Constants.KeyPair.KEY_FEED_URL_IS_PERSONALIZED, false);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(AppUtils.fetchAttributeColor(this, R.attr.toolbarIconColor));
        toolbar.setTitle(providerName);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_appbar_left_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProviderVideoListActivity.this.finish();
            }
        });

        init();
    }

    private void init() {

        // refresh layout on swipe
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(this, R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(this, R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(this, R.attr.toolbarBackgroundPrimary));

        // Recycler View
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        SpacesDecoration dividerItemDecoration = new SpacesDecoration(0, 0, getResources().getDimensionPixelSize(R.dimen.list_spacing), getResources().getDimensionPixelSize(R.dimen.list_spacing));
        recyclerView.addItemDecoration(dividerItemDecoration);

        // adapter create and set in recycler view
        ReadUnreadStatusTable readUnreadStatusTable = (ReadUnreadStatusTable) DatabaseHelper.getInstance(ProviderVideoListActivity.this).getTableObject(ReadUnreadStatusTable.TABLE_NAME);
        adapter = new VideoAdapter(this, false, this, readUnreadStatusTable);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {

                        if (isLoadMoreEnable) {
                            onLoadMore();
                            isLoading = true;
                        }
                    }
                }
            }
        });

        fetchVideosFromServer(false, true);
    }

    public void onLoadMore() {
        Systr.println("on load more called");
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                adapter.addNullItem();
            }
        });

        //Load more data for recycler view
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetchVideosFromServer(false, false);
            }
        }, 500);
    }

    @Override
    public void onRefresh() {
        pageCount = 0;
        fetchVideosFromServer(true, false);
    }

    private void onItemsLoadComplete() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Stop refresh animation
                if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    @Override
    public void onVideoSelect(int position) {
        Intent intent;
//        if (AppPreferences.getInstance(ProviderVideoListActivity.this).getBooleanValue(QuickPreferences.VideoPref.SETTINGS_IS_ADAPTIVE_PLAYER_ON, false)) {
//            intent = VideoDetailActivity.getIntent(ProviderVideoListActivity.this, adapter.getItemAtPosition(position), sectionLabel, AppFlyerConst.DBVideosSource.VIDEO_PROVIDER, position + 1, detailUrl, feedUrl, gaEventLabel, gaArticle, isPersonalizedFeedUrl, AppFlyerConst.DBVideosSource.VIDEO_PROVIDER + "_" + providerName, "");
//        } else {
//        }
        intent = DBVideoPlayerActivity2.getIntent(ProviderVideoListActivity.this, adapter.getItemAtPosition(position), sectionLabel, AppFlyerConst.DBVideosSource.VIDEO_PROVIDER, position + 1, detailUrl, feedUrl, gaEventLabel, gaArticle, isPersonalizedFeedUrl, AppFlyerConst.DBVideosSource.VIDEO_PROVIDER + "_" + providerName, "");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, 100);
    }

    @Override
    public void onVideoDelete(View view, int position, String videoId) {
    }

    private void fetchVideosFromServer(boolean isRefresh, boolean showProgress) {

        String url = mUrl + (providerId + "/PG" + pageCount + "/");
        Systr.println("URL : " + url);

        makeJsonObjectRequest(url, isRefresh, showProgress);
    }

    private void makeJsonObjectRequest(String url, final boolean isRefresh, final boolean showProgress) {

        if (showProgress)
            findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Systr.println("Video list fetch response Json : " + response);

                parseLiveVideoServerJson(response, isRefresh);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Video list fetch Error : " + error);

                findViewById(R.id.progress_bar).setVisibility(View.GONE);

                adapter.removeNullItem();
                onItemsLoadComplete();
                isLoading = false;

                if (error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(ProviderVideoListActivity.this, getResources().getString(R.string.internet_connection_error_));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(ProviderVideoListActivity.this).addToRequestQueue(request);
    }

    private void parseLiveVideoServerJson(JSONObject jsonObject, boolean isRefresh) {
        Vector<VideoInfo> list = new Vector<>();

        int arrayLength = -1;

        try {
            JSONArray jsonArray = jsonObject.getJSONArray("feed");
            arrayLength = jsonArray.length();

            list = new Vector<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), VideoInfo[].class)));

        } catch (Exception e) {
            e.printStackTrace();
        }

        findViewById(R.id.progress_bar).setVisibility(View.GONE);
        adapter.removeNullItem();

        if (isRefresh) {
            adapter.addAndClear(list);
            onItemsLoadComplete();
        } else {
            adapter.add(list);
        }

        if (arrayLength > 0) {
            isLoading = false;
            pageCount++;

        } else {
            isLoadMoreEnable = false;
            isLoading = false;
        }
    }
}