package com.db.dbvideoPersonalized.video;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.database.ReadUnreadStatusTable;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;

import java.util.List;
import java.util.Locale;
import java.util.Vector;

/**
 * Created by DB on 28-12-2016.
 */

public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ReadUnreadStatusTable readUnreadStatusTable;

    public interface OnVideoActionListener {
        void onVideoSelect(int position);

        void onVideoDelete(View view, int position, String videoId);
    }

    private final int VIEW_TYPE_ITEM = 1;
    private final int VIEW_TYPE_LOADING = 2;
    private Vector<VideoInfo> videoList;
    private Context mContext;
    private OnVideoActionListener mListener;
    private boolean mShowDeleteOption;

    public VideoAdapter(Context context, boolean showDeleteOption, OnVideoActionListener listener, ReadUnreadStatusTable readUnreadStatusTable) {
        mContext = context;
        mListener = listener;
        mShowDeleteOption = showDeleteOption;
        videoList = new Vector<>();
        this.readUnreadStatusTable = readUnreadStatusTable;
    }

    public void addAndClear(List<VideoInfo> list) {
        videoList.clear();
        videoList.addAll(list);
        notifyDataSetChanged();
    }

    public void add(Vector<VideoInfo> list) {
        for (int i = 0; i < list.size(); i++) {
            if (!videoList.contains(list.get(i))) {
                videoList.add(list.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addInFirst(Vector<VideoInfo> list) {
        for (int i = list.size() - 1; i >= 0; i--) {
            if (!videoList.contains(list.get(i))) {
                videoList.add(0, list.get(i));
            }
        }
        notifyDataSetChanged();
    }


    public void updateVideoInfo(VideoInfo videoInfo) {
        int i = videoList.indexOf(videoInfo);
        if (i >= 0 && i < videoList.size()) {
            videoList.set(i, videoInfo);
        }
        notifyDataSetChanged();
    }

    public void deleteVideo(int index) {
        videoList.remove(index);
        notifyDataSetChanged();
    }


    public void addNullItem() {
        videoList.add(null);
        notifyDataSetChanged();
    }

    public void removeNullItem() {
        if (videoList.size() > 0 && videoList.get(videoList.size() - 1) == null) {
            videoList.remove(videoList.size() - 1);
            notifyDataSetChanged();
        }
    }

    public VideoInfo getItemAtPosition(int position) {
        if (position >= 0 && position < videoList.size())
            return videoList.get(position);
        return null;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_video, parent, false);
                return new ViewHolder(itemView);
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false));
        }
        return null;
    }

    String displayName = "personalised_video";

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
                return;
            case VIEW_TYPE_ITEM:
                final ViewHolder viewHolder = (ViewHolder) holder;
                final VideoInfo info = videoList.get(position);
                if (info == null) {
                    return;
                }

                if (TextUtils.isEmpty(info.image)) info.image = null;
                AppUtils.getInstance().setImageViewSizeWithAspectRatio(viewHolder.thumbnailImageView, Constants.ImageRatios.DBVIDEOS_RATIO, 0, mContext);
                ImageUtil.setImage(mContext, info.image, viewHolder.thumbnailImageView, R.drawable.water_mark_news_list);
                viewHolder.titleTextView.setText(info.title);
                if (readUnreadStatusTable.isStoryWithVersionAvailable(AppUtils.getFilteredStoryId(info.storyId), info.version)) {
                    viewHolder.titleTextView.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));
                } else {
                    viewHolder.titleTextView.setTextColor(ContextCompat.getColor(mContext, R.color.list_title_color));
                }

                if (TextUtils.isEmpty(info.duration)) {
                    viewHolder.durationTextView.setVisibility(View.GONE);
                } else {
                    viewHolder.durationTextView.setVisibility(View.VISIBLE);
                    viewHolder.durationTextView.setText(info.duration);
                }

//            viewHolder.viewsTextView.setText(String.format(Locale.getDefault(), mContext.getResources().getString(R.string.views_count), AppUtils.getInstance().getSIPrefixForCount(info.views)));
                viewHolder.whatsappTextView.setText(String.format(Locale.getDefault(), mContext.getResources().getString(R.string.views_count), AppUtils.getInstance().getSIPrefixForCount(info.views)));

                int shareCount = info.whatsappShareCount + info.fbShareCount;
                if (shareCount > 0) {
                    if (shareCount > 1) {
                        viewHolder.facebookTextView.setText(AppUtils.getInstance().getSIPrefixForCount(shareCount) + " Shares");
                    } else {
                        viewHolder.facebookTextView.setText(AppUtils.getInstance().getSIPrefixForCount(shareCount) + " Share");
                    }
                    viewHolder.facebookTextView.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.facebookTextView.setVisibility(View.GONE);
                }

                if (info.likeCount > 0) {
                    if (info.likeCount > 1) {
                        viewHolder.likeTextView.setText(AppUtils.getInstance().getSIPrefixForCount(info.likeCount) + " Likes");
                    } else {
                        viewHolder.likeTextView.setText(AppUtils.getInstance().getSIPrefixForCount(info.likeCount) + " Like");
                    }
                    viewHolder.likeTextView.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.likeTextView.setVisibility(View.GONE);
                }

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int adapterPosition = position;
                        String filteredStoryId = AppUtils.getFilteredStoryId(info.storyId);
                        if (!readUnreadStatusTable.isStoryWithVersionAvailable(filteredStoryId, info.version)) {
                            readUnreadStatusTable.updateStoryVersion(filteredStoryId, info.version);
                        }
//                        viewHolder.titleTextView.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));
                        if (adapterPosition >= 0 && adapterPosition < videoList.size() && mListener != null) {
                            mListener.onVideoSelect(adapterPosition);
                        }
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (videoList.get(position) == null)
            return VIEW_TYPE_LOADING;
        else
            return VIEW_TYPE_ITEM;
    }

    public Vector<VideoInfo> getVideoList() {
        return videoList;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView thumbnailImageView;
        ImageView playIconImageView;

        TextView titleTextView;
        //        TextView viewsTextView;
        TextView durationTextView;

        TextView whatsappTextView;
        TextView facebookTextView;
        TextView likeTextView;

        ImageView deleteImageView;

        ViewHolder(View view) {
            super(view);

            thumbnailImageView = view.findViewById(R.id.iv_thumbnail);
            playIconImageView = view.findViewById(R.id.iv_play_icon);

            durationTextView = view.findViewById(R.id.tv_duration);
//            viewsTextView = (TextView) view.findViewById(R.id.tv_views);

            titleTextView = view.findViewById(R.id.tv_title);

            whatsappTextView = view.findViewById(R.id.tv_whatsapp);
            facebookTextView = view.findViewById(R.id.tv_facebook);
            likeTextView = view.findViewById(R.id.tv_like);

            deleteImageView = view.findViewById(R.id.iv_delete);
            deleteImageView.setOnClickListener(this);


            if (mShowDeleteOption) {
                deleteImageView.setVisibility(View.VISIBLE);
            } else {
                deleteImageView.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_delete:
                    int adapterPosition = getAdapterPosition();

                    if (adapterPosition >= 0 && adapterPosition < videoList.size() && mListener != null) {
                        VideoInfo videoInfo = videoList.get(adapterPosition);
                        mListener.onVideoDelete(v, adapterPosition, videoInfo.storyId);
                    }

                    break;
            }
        }
    }

    //View Holders
    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        private LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }

}
