package com.db.divya_new;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.CatMenuTabListAdapter;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHome;
import com.db.divya_new.data.CatHomeData;
import com.db.divya_new.divyashree.CatHomeAdapter;
import com.bhaskar.util.Action;
import com.db.util.JsonParser;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CatHomeParentViewHolderNew extends RecyclerView.ViewHolder {

    public RecyclerView recyclerViewOne;
    public RecyclerView recyclerView;
    public RecyclerView recycler_view_tab;
    private View tabDividerView;
    public CatHomeAdapter mAdapter;
    public CatMenuTabListAdapter tabListAdapter;
    ArrayList<CatHome> catHomeArrayList = new ArrayList<>();
    ArrayList<CategoryInfo> categoryTitleArrayList = new ArrayList<>();
    private Activity mContext;
    private ProgressBar progressBar;
    private TextView moreDropdownTv;
    ImageView ivDropdown;
    final View vDropDown;
    Button btnHeaderLoadMore;
    private String mSuperParentId;
    private CategoryInfo mCategoryInfo;

    public CatHomeParentViewHolderNew(Activity mContext, View itemView, String mSuperParentId, final CategoryInfo categoryInfo) {
        super(itemView);
        this.mContext = mContext;
        this.mSuperParentId = mSuperParentId;
        this.mCategoryInfo = categoryInfo;

        ivDropdown = itemView.findViewById(R.id.ivDropdown);
        vDropDown = itemView.findViewById(R.id.vDropdown);
        btnHeaderLoadMore = itemView.findViewById(R.id.btnHeaderLoadMore);
        recyclerViewOne = itemView.findViewById(R.id.recycler_view_one);
        recycler_view_tab = itemView.findViewById(R.id.recycler_view_tab);
        recyclerView = itemView.findViewById(R.id.recycler_view);
        tabDividerView = itemView.findViewById(R.id.tab_divider_view);

        progressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        moreDropdownTv = itemView.findViewById(R.id.more_dropdown_tv);

        mAdapter = new CatHomeAdapter(this.mContext, mSuperParentId, categoryInfo);

        tabListAdapter = new CatMenuTabListAdapter(new CatMenuTabListAdapter.OnTabClickLitener() {
            @Override
            public void onTabClick(CategoryInfo info) {
                for (int i = 0; i < catHomeArrayList.size(); i++) {
                    CatHome cat = catHomeArrayList.get(i);
                    if (cat.id.equalsIgnoreCase(info.id)) {
                        mAdapter.clear();
                        mAdapter.add(cat);
                    }
                }
            }
        });

        recyclerViewOne.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));

        recycler_view_tab.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        recycler_view_tab.setAdapter(tabListAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
//        recyclerView.addItemDecoration(new android.support.v7.widget.DividerItemDecoration(mContext, android.support.v7.widget.DividerItemDecoration.VERTICAL));
        // recyclerView.addItemDecoration(spacesDecoration);

        recyclerView.setAdapter(mAdapter);
    }

    public void onTabClick(CategoryInfo info) {

    }


    private void setTitle(CatHome catHome, int position) {
        String title = catHome.name, color;
        if (TextUtils.isEmpty(catHome.color))
            color = "";
        else
            color = catHome.color;

        if (position == 0) {
            color = "";
            title = "";
        }
        final String mSuperParentId = catHome.superParentId;

        LinearLayout parentHeaderLL = itemView.findViewById(R.id.header_title_LL);
        TextView headerTitleTV = itemView.findViewById(R.id.headerTitleTV);
        TextView headerBoxTV = itemView.findViewById(R.id.headerBoxTV);
        TextView headerLineTV = itemView.findViewById(R.id.headerLineTV);
        View tabDividerView = itemView.findViewById(R.id.tab_divider_view);
        ImageView diagonalImageView = itemView.findViewById(R.id.diagonal_iv);

        if (!TextUtils.isEmpty(color)) {
            int intColor = Color.parseColor(color);
            headerBoxTV.setBackgroundColor(intColor);
            headerLineTV.setBackgroundColor(intColor);


            Drawable drawable = mContext.getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_24dp);
            drawable.clearColorFilter();
            drawable.setColorFilter(new PorterDuffColorFilter(intColor, PorterDuff.Mode.SRC_IN));
            ivDropdown.setImageDrawable(drawable);
//            headerTitleTV.setTextColor(intColor);
            parentHeaderLL.setBackgroundColor(intColor);
            tabDividerView.setBackgroundColor(intColor);
            diagonalImageView.setColorFilter(intColor);
            moreDropdownTv.setTextColor(intColor);
        }
        if (!TextUtils.isEmpty(title)) {
            parentHeaderLL.setVisibility(View.VISIBLE);
            headerTitleTV.setText(title);
        } else {
            parentHeaderLL.setVisibility(View.GONE);
        }

        parentHeaderLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                    TabController.getInstance().setTabClicked(true);
                    TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mSuperParentId);
                }
            }
        });

        if (!TextUtils.isEmpty(color))
            tabListAdapter.setColor(Color.parseColor(color));
    }

    public void setDataForCatHome(final CatHome catHome, final int position) {
        mAdapter.setFromParentHome(true);
        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(catHome.id);
        if (catHomeData != null && catHomeData.dataArrayList != null && catHomeData.dataArrayList.size() > 0) {
            catHomeArrayList.clear();
            catHomeArrayList.addAll(catHomeData.dataArrayList);
            categoryTitleArrayList.clear();
            categoryTitleArrayList.addAll(catHomeData.titleArrayList);
            if (setDataToUI(catHome)) {
                setTitle(catHome, position);
            }
        } else {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    fetchDataFromCategory(catHome);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (setDataToUI(catHome)) {
                        setTitle(catHome, position);
                    }
                }
            }.execute();
        }
    }

    private void fetchDataFromCategory(CatHome catHome) {
        if (catHome.categoryInfo != null && !TextUtils.isEmpty(catHome.categoryInfo.extraVlues)) {
            try {
                JSONObject jsonObject = new JSONObject(catHome.categoryInfo.extraVlues);
                JSONArray jsonArray = jsonObject.optJSONArray("cat_home_ids");
                ArrayList<CatHome> localCatHomeArrayList = new ArrayList<>();
                ArrayList<CategoryInfo> localCategoryTitleArrayList = new ArrayList<>();
                if (jsonArray.length() > 0) {

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            CatHome cat = new Gson().fromJson(jsonArray.get(i).toString(), CatHome.class);
                            CategoryInfo info = JsonParser.getInstance().getCategoryInfoById(mContext, cat.id);
//                            if (!catHome.categoryInfo.parentId.equalsIgnoreCase(info.parentId))
//                                break;

                            if (info != null && catHome.categoryInfo.parentId.equalsIgnoreCase(info.parentId)) {
                                // getting the action of the category
                                if (info.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_CAT_MENU)) {
                                    cat.action = info.subMenu.get(0).action;
                                } else {
                                    cat.action = info.action;
                                }

                                // if the action is not handled then the data will not be parsed for that category
                                if (checkActionAvailability(cat.action)) {
//

                                    if (info.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_CAT_MENU)) {

                                        cat.categoryInfo = new CategoryInfo(info.subMenu.get(0));


                                    } else {
                                        cat.categoryInfo = new CategoryInfo(info);
                                    }
                                    cat.parentId = info.id;
                                    cat.superParentId = info.parentId;

                                    // this is checking if the parent catHome has type ="1" then the child will have their own heading
                                    if (!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))
                                        cat.type = "1";

                                    if (!cat.isGAValueAdded) {
                                        if (!TextUtils.isEmpty(catHome.categoryInfo.gaScreen))
                                            cat.categoryInfo.gaScreen = (catHome.categoryInfo.gaScreen + "-" + cat.categoryInfo.gaScreen);
                                        if (!TextUtils.isEmpty(catHome.categoryInfo.gaArticle))
                                            cat.categoryInfo.gaArticle = (catHome.categoryInfo.gaScreen + "-" + cat.categoryInfo.gaArticle);
                                        if (!TextUtils.isEmpty(catHome.categoryInfo.gaEventLabel))
                                            cat.categoryInfo.gaEventLabel = (catHome.categoryInfo.gaEventLabel + "-" + cat.categoryInfo.gaEventLabel);

                                        cat.isGAValueAdded = true;
                                    }
                                    localCategoryTitleArrayList.add(info);

                                    cat.headerTitle = info.menuName;
                                    localCatHomeArrayList.add(cat);
                                }
                            }
                        } catch (Exception e) {
                        }
                    }

                    catHomeArrayList.clear();
                    catHomeArrayList.addAll(localCatHomeArrayList);
                    categoryTitleArrayList.clear();
                    categoryTitleArrayList.addAll(localCategoryTitleArrayList);

                    if (!TextUtils.isEmpty(catHome.id)) {
                        if (localCatHomeArrayList.size() > 0) {

                            CatHomeData catHomeData = new CatHomeData(localCatHomeArrayList, localCategoryTitleArrayList);
                            DataParser.getInstance().addCatHomeDataHashMap(catHome.id, catHomeData);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean setDataToUI(CatHome catHome) {
        mAdapter.clear();
        tabListAdapter.clear();
        // this condition will set data to adapter only if the UI has to be ALL_CAT_HOME UI
        if (catHomeArrayList.size() > 0) {

            if (categoryTitleArrayList.size() > 1) {
                vDropDown.setVisibility(View.VISIBLE);
                vDropDown.setOnClickListener(view -> {
                    Context wrapper = new ContextThemeWrapper(mContext, R.style.popupMenuStyle);
                    PopupMenu popup = new PopupMenu(wrapper, vDropDown);
                    CategoryInfo cInfo;
                    for (int i = 0; i < categoryTitleArrayList.size(); i++) {
                        cInfo = categoryTitleArrayList.get(i);
                        popup.getMenu().add(Integer.parseInt(cInfo.parentId), Integer.parseInt(cInfo.id), i, cInfo.menuName);
                    }

                    popup.setOnMenuItemClickListener(item -> {
                        TabController.getInstance().setTabClicked(true);
                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(String.valueOf(item.getGroupId()), String.valueOf(item.getItemId()), null);

                        return true;
                    });
                    popup.show();
                });
            } else {
                vDropDown.setVisibility(View.GONE);
            }

            // this condition will set data to adapter only if the UI has to same as CAT_HOME UI
            if ((!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))) {
                recyclerViewOne.setVisibility(View.GONE);
                recycler_view_tab.setVisibility(View.GONE);
                tabDividerView.setVisibility(View.GONE);

                if (catHome.displayCount > 0) {
                    mAdapter.addAll(catHomeArrayList.subList(0, catHome.displayCount > catHomeArrayList.size() ? catHomeArrayList.size() : catHome.displayCount));
                } else {
                    mAdapter.addAll(catHomeArrayList);
                }

            } else if ((!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("2"))) {
                recycler_view_tab.setVisibility(View.VISIBLE);
                tabDividerView.setVisibility(View.VISIBLE);
                recyclerViewOne.setVisibility(View.VISIBLE);

                CatHomeAdapter adapter = new CatHomeAdapter(this.mContext, mSuperParentId, mCategoryInfo);
                adapter.setFromParentHome(true);
                recyclerViewOne.setAdapter(adapter);
                CatHome catHome1 = catHomeArrayList.get(0);
                adapter.add(catHome1);
                tabListAdapter.add(categoryTitleArrayList.subList(1, categoryTitleArrayList.size()));
                tabListAdapter.setSelectedPosition(0);
                mAdapter.add(catHomeArrayList.get(1));
            } else if ((!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("3"))) {
                recyclerViewOne.setVisibility(View.GONE);
                recycler_view_tab.setVisibility(View.GONE);
                tabDividerView.setVisibility(View.GONE);
                mAdapter.add(catHomeArrayList.get(0));
            } else {
                if (categoryTitleArrayList.size() > 1) {
                    recyclerViewOne.setVisibility(View.GONE);
                    recycler_view_tab.setVisibility(View.VISIBLE);
                    tabDividerView.setVisibility(View.VISIBLE);

                    tabListAdapter.add(categoryTitleArrayList);
                    mAdapter.add(catHomeArrayList.get(0));
                    tabListAdapter.setSelectedPosition(0);
                } else {
                    recycler_view_tab.setVisibility(View.GONE);
                    tabDividerView.setVisibility(View.GONE);
                    mAdapter.add(catHomeArrayList.get(0));
                    tabListAdapter.setSelectedPosition(0);
                }
            }
            return true;
        } else
            return false;
    }

    private boolean checkActionAvailability(String action) {
        switch (action) {
            case Action.CategoryAction.CAT_ACTION_STORY_LIST:
            case Action.CategoryAction.CAT_ACTION_STORY_LIST_V2:
            case Action.CategoryAction.CAT_ACTION_NEWS:
            case Action.CategoryAction.CAT_ACTION_RASHIFAL_V2:
            case Action.CategoryAction.CAT_ACTION_RATNA:
            case Action.CategoryAction.CAT_ACTION_GURUVANI_GURU:
            case Action.CategoryAction.CAT_ACTION_GURUVANI_TOPIC:
            case Action.CategoryAction.CAT_ACTION_SONGS:
            case Action.CategoryAction.CAT_ACTION_VIDEO_GALLERY_V3:
            case Action.CategoryAction.CAT_ACTION_PHOTO_GALLERY:
                return Action.ALL_HOME_CATEGORY_ACTIONS.contains(action);
            default:
                return false;
        }
    }


}
