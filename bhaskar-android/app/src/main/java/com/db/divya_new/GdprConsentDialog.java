package com.db.divya_new;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.bhaskar.R;

public class GdprConsentDialog extends AlertDialog {

    public interface onDialogListener {
        void onUserGivenConsent(boolean enabled);
    }

    public static final int ASK_QUESTION = 1;
    public static final int ASK_QUESTION_JYOTISH = 2;
    public static final int JOBS_APPLY = 3;
    public static final int ARTICLE_COMMENT = 4;

    private Activity context;
    private int type;
    private final onDialogListener listener;

    private boolean isEnabled;

    private TextView titleTextView;
    private TextView msgTextView;

    public GdprConsentDialog(@NonNull Activity context, int type, onDialogListener listener) {
        super(context);

        this.context = context;
        this.type = type;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        setContentView(R.layout.layout_gdpr_consent_dialog);

        titleTextView = findViewById(R.id.tv_heading);
        msgTextView = findViewById(R.id.tv_message);
//        (findViewById(R.id.submit_textview)).setBackground(ContextCompat.getDrawable(getContext(), R.drawable.comments_comment_btn_grey_background));
//        ((TextView) findViewById(R.id.submit_textview)).setTextColor(Color.BLACK);

        ((CheckBox) findViewById(R.id.agree_checkbox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isEnabled = b;
//                if (b) {
//                    (findViewById(R.id.submit_textview)).setBackground(ContextCompat.getDrawable(getContext(), R.drawable.comments_comment_btn_blue_background));
//                    ((TextView) findViewById(R.id.submit_textview)).setTextColor(Color.WHITE);
//                } else {
//                    (findViewById(R.id.submit_textview)).setBackground(ContextCompat.getDrawable(getContext(), R.drawable.comments_comment_btn_grey_background));
//                    ((TextView) findViewById(R.id.submit_textview)).setTextColor(Color.BLACK);
//                }
            }
        });

        setContent(type);

        // Click listener
        findViewById(R.id.accept_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEnabled) {
                    if (listener != null) {
                        listener.onUserGivenConsent(isEnabled);
                    }
                    dismiss();
                }
            }
        });

//        findViewById(R.id.cancel_textview).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (listener != null) {
//                    listener.onUserGivenConsent(false);
//                }
//                dismiss();
//            }
//        });
    }

    private void setContent(int type) {
        switch (type) {
            case ASK_QUESTION:
                titleTextView.setText("Ask Question Consent");
                msgTextView.setText("Please provide information");
                break;

            case ASK_QUESTION_JYOTISH:
                titleTextView.setText("Ask Question Jyotish Consent");
                msgTextView.setText("Please provide information");
                break;

            case JOBS_APPLY:
                titleTextView.setText("Jobs Apply Consent");
                msgTextView.setText("Please provide information");
                break;

            case ARTICLE_COMMENT:
                titleTextView.setText(context.getResources().getString(R.string.withdraw_consent_login_chb_text));
                msgTextView.setText(context.getResources().getString(R.string.i_allow_db_corp_ltd));
                break;
        }
    }
}
