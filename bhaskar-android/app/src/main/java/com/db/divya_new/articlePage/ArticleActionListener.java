package com.db.divya_new.articlePage;

public interface ArticleActionListener {
    void onArticleSuccess(ArticleCommonInfo commonInfo);
    void onShowingNextStory(boolean isAutoSwiped);
    void onShowingPreviousStory();
    boolean isViewPagerAnimationRunning();
}
