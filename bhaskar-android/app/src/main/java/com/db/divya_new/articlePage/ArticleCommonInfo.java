package com.db.divya_new.articlePage;

import com.db.data.models.NewsPhotoInfo;
import com.db.data.models.RelatedArticleInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ArticleCommonInfo implements Serializable {

    private final static long serialVersionUID = -5921630101659701329L;

    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("image_size")
    @Expose
    public String imageSize;
    @SerializedName("story")
    @Expose
    public String story;
    @SerializedName("story_id")
    @Expose
    public String storyId;
    @SerializedName("header_name")
    @Expose
    public String headerName;
    @SerializedName("slug_intro")
    @Expose
    public String slugIntro;
    @SerializedName("mobile_description")
    @Expose
    public String[] mobileDescription;
    @SerializedName("link")
    @Expose
    public String link;
    @SerializedName("share_link")
    @Expose
    public String shareLink;
    @SerializedName("pubdate")
    @Expose
    public String pubdate;
    @SerializedName("timeago")
    @Expose
    public String timeago;
    @SerializedName("provider")
    @Expose
    public String provider;
    @SerializedName("post_id_url")
    @Expose
    public String postIdUrl;
    @SerializedName("photo_gallery")
    @Expose
    public ArrayList<NewsPhotoInfo> photosGallery = null;
    @SerializedName("photos")
    @Expose
    public ArrayList<NewsPhotoInfo> photos = null;

    @SerializedName("related_article")
    @Expose
    public List<RelatedArticleInfo> relatedArticle = null;

    @SerializedName("next_article")
    @Expose
    public NextArticle nextArticle = null;

    @SerializedName("version")
    @Expose
    public String version;
    @SerializedName("cat_id")
    @Expose
    public String catId;
    @SerializedName("iitl_title")
    @Expose
    public String iitlTitle;
    @SerializedName("g_track_url")
    @Expose
    public String gTrackUrl;
    @SerializedName("track_url")
    @Expose
    public String trackUrl;
    @SerializedName("template_type")
    @Expose
    public String templateType;
    @SerializedName("keywords")
    @Expose
    public String keywords;

    @SerializedName("provider_logo")
    @Expose
    public String providerImage = "";
    @SerializedName("color_code")
    @Expose
    public String colorCode = "";


/*
    public ArticleCommonInfo(String title, String image, String imageSize, String story, String storyId, String slugIntro, String[] mobileDescription, String link, String pubdate, String timeago, String provider, ArrayList<NewsPhotoInfo> photos, List<RelatedArticleInfo> relatedArticle, String version, String catId, String iitlTitle, String gTrackUrl, String trackUrl, String templateType) {
        super();
        this.title = title;
        this.image = image;
        this.imageSize = imageSize;
        this.story = story;
        this.storyId = storyId;
        this.slugIntro = slugIntro;
        this.mobileDescription = mobileDescription;
        this.link = link;
        this.pubdate = pubdate;
        this.timeago = timeago;
        this.provider = provider;
        this.photos = photos;
        this.relatedArticle = relatedArticle;
        this.version = version;
        this.catId = catId;
        this.iitlTitle = iitlTitle;
        this.gTrackUrl = gTrackUrl;
        this.trackUrl = trackUrl;
        this.templateType = templateType;
    }
*/

    public class NextArticle {
        @SerializedName("storyid")
        @Expose
        public String storyId;

        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("videoflag")
        @Expose
        public String videoflag;

        @SerializedName("iitl_title")
        @Expose
        public String iitlTitle;

        @SerializedName("header_name")
        @Expose
        public String headerName;

        @SerializedName("cat_color")
        @Expose
        public String catColor;

    }
}