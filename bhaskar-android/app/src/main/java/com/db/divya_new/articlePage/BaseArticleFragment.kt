package com.db.divya_new.articlePage

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.db.core.CoreFragment
import com.db.data.models.PromoWidgetModel
import com.db.providers.NewsDetailDragUiProvider
import com.db.util.Constants
import com.db.util.DbSharedPref

abstract class BaseArticleFragment : CoreFragment() {

    abstract fun isLayoutTypeVertical(): Boolean

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let {
            val promoWidgetModel = checkAndGetPromoWidget()
            promoWidgetModel?.let {
                showPromoWidgetModel(view, it)
            }
        }
    }

    private fun checkAndGetPromoWidget(): PromoWidgetModel? {

        return if (DbSharedPref.isPromoWidgetModelVisible(dbApp)) {
            val promoWidgetModel = DbSharedPref.getPromoWidgetModel(dbApp)

            /*val iconUrl = "https://i10.dainikbhaskar.com/images/appfeed/nav_icon/521_sudoku.png"
            val linkUrl = "https://bhaskar.page.link/CBNZ"
            val promoWidgetModel = PromoWidgetModel(true, iconUrl, "1040", linkUrl)*/

            promoWidgetModel?.let {
                if (Constants.ACTIVE_TRUE.equals(promoWidgetModel.isActive,true) && ((!TextUtils.isEmpty(it.menuId) &&
                                !it.menuId.equals("0", true))) ||
                        !TextUtils.isEmpty(it.linkUrl)) {
                    it
                } else {
                    null
                }
            } ?: run {
                return null
            }
        } else {
            return null
        }
    }

    /**
     * @param view
     */
    private fun showPromoWidgetModel(view: View, promoWidgetModel: PromoWidgetModel) {
        NewsDetailDragUiProvider(coreActivity).init(view, promoWidgetModel,
                isLayoutTypeVertical())
    }
}