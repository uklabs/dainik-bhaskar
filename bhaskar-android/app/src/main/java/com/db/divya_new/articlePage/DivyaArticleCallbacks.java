package com.db.divya_new.articlePage;


public interface DivyaArticleCallbacks {

    void openRashifalArticle(String detailUrl, int indexNumber, String rashiTitle, String rashiSubTitle, String rashiIcon, String mGaArticle);
}
