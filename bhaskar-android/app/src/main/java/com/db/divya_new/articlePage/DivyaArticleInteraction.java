package com.db.divya_new.articlePage;

import com.db.data.models.RelatedArticleInfo;

public interface DivyaArticleInteraction {
    void onTagItemClicked();
    void OnRelatedArticleClick(String storyId, String channel_Slno, int position, String color, String headerName);
    void OnRelatedArticleClick(RelatedArticleInfo relatedArticleInfo);
    void onCommentClick(ArticleCommonInfo articleCommonInfo);
    void onBookmarkClick(ArticleCommonInfo articleCommonInfo);
    void onArticleSuccess(ArticleCommonInfo commonInfo);
    void onShowingNextStory(boolean isAutoSwiped);
    void onShowingPreviousStory();
    boolean isViewPagerAnimationRunning();
    void addNextArticle(ArticleCommonInfo articleCommonInfo);
    void changeStatusBarcolor(String color);

    void showNextStory();
}
