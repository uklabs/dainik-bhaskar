package com.db.divya_new.articlePage;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.RelatedArticleInfo;
import com.db.data.models.StoryListInfo;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.google.gson.Gson;


public class DivyaCommonArticleActivity {

    /*For News*/
    public static Intent getIntent(Context context, NewsListInfo newsListInfo, CategoryInfo categoryInfo, int position) {
        Intent detailNews = new Intent(context, AppUtils.getInstance().getArticleTypeClass());
        detailNews.putExtra(Constants.KeyPair.KEY_STORY_ID, newsListInfo.storyId);
        detailNews.putExtra(Constants.KeyPair.KEY_POSITION, position);
        if (categoryInfo != null) {
            String headerName = TextUtils.isEmpty(newsListInfo.headerName)?categoryInfo.menuName:newsListInfo.headerName;
            detailNews.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, categoryInfo.menuName);
            String color = TextUtils.isEmpty(newsListInfo.colorCode)?categoryInfo.color:newsListInfo.colorCode;
            detailNews.putExtra(Constants.KeyPair.KEY_COLOR, color);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, categoryInfo.displayName);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, categoryInfo.gaEventLabel);
        }

        /*New Changes article speed*/
        detailNews.putExtra(Constants.KeyPair.KEY_PHOTO_DETAIL, new Gson().toJson(newsListInfo.photos));
        detailNews.putExtra(Constants.KeyPair.KEY_BULLETS, newsListInfo.bulletsPoint);
        detailNews.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.title);
        detailNews.putExtra(Constants.KeyPair.KEY_IITL_TITLE, newsListInfo.iitl_title);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
        detailNews.putExtra(Constants.KeyPair.KEY_PUB_DATE, newsListInfo.pubdate);
        detailNews.putExtra(Constants.KeyPair.KEY_PROVIDER_NAME, newsListInfo.provider);
        detailNews.putExtra(Constants.KeyPair.KEY_PROVIDER_LOGO, newsListInfo.providerLogo);

        return detailNews;
    }

    /*For Story*/
    public static Intent getIntent(Context context, StoryListInfo newsListInfo, CategoryInfo categoryInfo) {
        Intent detailNews = new Intent(context, AppUtils.getInstance().getArticleTypeClass());
        detailNews.putExtra(Constants.KeyPair.KEY_STORY_ID, newsListInfo.catId);

        if (categoryInfo != null) {
            String headerName = TextUtils.isEmpty(newsListInfo.header)?categoryInfo.menuName:newsListInfo.header;
            detailNews.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, categoryInfo.menuName);
            String color = TextUtils.isEmpty(newsListInfo.colorCode)?categoryInfo.color:newsListInfo.colorCode;
            detailNews.putExtra(Constants.KeyPair.KEY_COLOR,color);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, categoryInfo.displayName);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, categoryInfo.gaEventLabel);
        }

        /*New Changes article speed*/
        detailNews.putExtra(Constants.KeyPair.KEY_PHOTO_DETAIL, new Gson().toJson(newsListInfo.photos));
        detailNews.putExtra(Constants.KeyPair.KEY_BULLETS, newsListInfo.bulletsPoint);
        detailNews.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.articleTitle);
        detailNews.putExtra(Constants.KeyPair.KEY_IITL_TITLE, newsListInfo.iitl_title);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
        detailNews.putExtra(Constants.KeyPair.KEY_PUB_DATE, newsListInfo.pubdate);
        detailNews.putExtra(Constants.KeyPair.KEY_PROVIDER_NAME, newsListInfo.provider);
        detailNews.putExtra(Constants.KeyPair.KEY_PROVIDER_LOGO, newsListInfo.providerLogo);
        return detailNews;
    }

    /*For News*/
    public static Intent getIntent(Context context, NewsListInfo newsListInfo, String gaArticle, String gaScreen, String displayName, String gaEventLabel,String categoryTitle) {
        Intent detailNews = new Intent(context, AppUtils.getInstance().getArticleTypeClass());
        detailNews.putExtra(Constants.KeyPair.KEY_STORY_ID, newsListInfo.storyId);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, displayName);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
        detailNews.putExtra(Constants.KeyPair.KEY_COLOR, newsListInfo.colorCode);

        String headerName = TextUtils.isEmpty(newsListInfo.headerName)?categoryTitle:newsListInfo.headerName;
        detailNews.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, categoryTitle);
//        String color = TextUtils.isEmpty(newsListInfo.colorCode)?categoryInfo.color:newsListInfo.colorCode;
        detailNews.putExtra(Constants.KeyPair.KEY_COLOR,newsListInfo.colorCode);


        /*New Changes article speed*/
        detailNews.putExtra(Constants.KeyPair.KEY_PHOTO_DETAIL, new Gson().toJson(newsListInfo.photos));
        detailNews.putExtra(Constants.KeyPair.KEY_BULLETS, newsListInfo.bulletsPoint);
        detailNews.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.title);
        detailNews.putExtra(Constants.KeyPair.KEY_IITL_TITLE, newsListInfo.iitl_title);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
        detailNews.putExtra(Constants.KeyPair.KEY_PUB_DATE, newsListInfo.pubdate);
        detailNews.putExtra(Constants.KeyPair.KEY_PROVIDER_NAME, newsListInfo.provider);
        detailNews.putExtra(Constants.KeyPair.KEY_PROVIDER_LOGO, newsListInfo.providerLogo);
        return detailNews;
    }

    /*For News*/
    public static Intent getIntent(Context context, NewsListInfo newsListInfo, CategoryInfo categoryInfo) {
        Intent detailNews = new Intent(context, AppUtils.getInstance().getArticleTypeClass());
        detailNews.putExtra(Constants.KeyPair.KEY_STORY_ID, newsListInfo.storyId);

        if (categoryInfo != null) {
            String headerName = TextUtils.isEmpty(newsListInfo.headerName)?categoryInfo.menuName:newsListInfo.headerName;
            detailNews.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, categoryInfo.menuName);
            String color = TextUtils.isEmpty(newsListInfo.colorCode)?categoryInfo.color:newsListInfo.colorCode;
            detailNews.putExtra(Constants.KeyPair.KEY_COLOR,color);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, categoryInfo.displayName);
            detailNews.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, categoryInfo.gaEventLabel);
        }

        /*New Changes article speed*/
        detailNews.putExtra(Constants.KeyPair.KEY_PHOTO_DETAIL, new Gson().toJson(newsListInfo.photos));
        detailNews.putExtra(Constants.KeyPair.KEY_BULLETS, newsListInfo.bulletsPoint);
        detailNews.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.title);
        detailNews.putExtra(Constants.KeyPair.KEY_IITL_TITLE, newsListInfo.iitl_title);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
        detailNews.putExtra(Constants.KeyPair.KEY_PUB_DATE, newsListInfo.pubdate);
        detailNews.putExtra(Constants.KeyPair.KEY_PROVIDER_NAME, newsListInfo.provider);
        detailNews.putExtra(Constants.KeyPair.KEY_PROVIDER_LOGO, newsListInfo.providerLogo);
        return detailNews;
    }


    /*RelatedArticle*/
    public static Intent getIntent(Context context, RelatedArticleInfo relatedArticleInfo, String gaScreen, String displayName, String gaEventLabel) {
        Intent detailNews = new Intent(context, AppUtils.getInstance().getArticleTypeClass());
        detailNews.putExtra(Constants.KeyPair.KEY_STORY_ID, relatedArticleInfo.storyId);
        detailNews.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, relatedArticleInfo.headerName);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, AppFlyerConst.GAScreen.RECOMMENDATION_GA_ARTICLE);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, displayName);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
        detailNews.putExtra(Constants.KeyPair.KEY_COLOR, relatedArticleInfo.colorCode);

        /*New Changes article speed*/
        detailNews.putExtra(Constants.KeyPair.KEY_PHOTO_DETAIL, new Gson().toJson(relatedArticleInfo.photos));
        detailNews.putExtra(Constants.KeyPair.KEY_BULLETS, relatedArticleInfo.bulletsPoint);
        detailNews.putExtra(Constants.KeyPair.KEY_TITLE, relatedArticleInfo.title);
        detailNews.putExtra(Constants.KeyPair.KEY_IITL_TITLE, relatedArticleInfo.iitlTitle);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, relatedArticleInfo.gTrackUrl);
        detailNews.putExtra(Constants.KeyPair.KEY_PUB_DATE, relatedArticleInfo.pubDate);
        detailNews.putExtra(Constants.KeyPair.KEY_PROVIDER_NAME, relatedArticleInfo.provider);
        detailNews.putExtra(Constants.KeyPair.KEY_PROVIDER_LOGO, relatedArticleInfo.providerLogo);

        detailNews.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return detailNews;
    }

    /*For Story*/
    public static Intent getIntent(Context context, String storyID, String headerName, String gaArticle, String gaScreen, String displayName, String gaEventLabel, String colorCode) {
        Intent detailNews = new Intent(context, AppUtils.getInstance().getArticleTypeClass());
        detailNews.putExtra(Constants.KeyPair.KEY_STORY_ID, storyID);
        detailNews.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, headerName);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, displayName);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);

        detailNews.putExtra(Constants.KeyPair.KEY_COLOR, colorCode);
        return detailNews;
    }


}
