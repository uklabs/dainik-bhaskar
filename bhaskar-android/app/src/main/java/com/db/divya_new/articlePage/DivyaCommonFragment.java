package com.db.divya_new.articlePage;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.LoginController;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.BookmarkSerializedListInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsDetailPhotoInfo;
import com.db.data.models.NewsPhotoInfo;
import com.db.data.source.remote.SerializeData;
import com.db.data.source.server.BackgroundRequest;
import com.db.home.ActivityUtil;
import com.db.listeners.FragmentLifecycleArticle;
import com.db.listeners.OnItemClickListener;
import com.db.news.RecommendationAdapterNew;
import com.db.news.TagsLabelAdapterNew;
import com.db.news.WebViewActivity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.db.shimmer.ShimmerFrameLayout;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.bhaskar.util.CssConstants;
import com.db.util.ImageUtil;
import com.db.util.JsonParser;
import com.db.util.JsonPreferences;
import com.db.util.OpenNewsLinkInApp;
import com.db.util.PhotoViewPager;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.db.views.JustifiedTextView;
import com.db.views.WrappingFragmentStatePagerAdapter;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.gson.Gson;
import com.shuhart.bubblepagerindicator.BubblePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DivyaCommonFragment extends Fragment implements OnItemClickListener, FragmentLifecycleArticle {

    /*Views*/
    private CardView nextArticleCard;
    private TextView articleTitleTv, nextArticleTitle;
    private ImageView nextArticleImage, nextArticleVideoFlag;
    private TextView articlePubDateTv, articleRecommendationTv, articleProviderNameTv;
    private WebView newsDetailWebView;
    private PhotoViewPager photoViewPager;
    private RelativeLayout photoLayout;
    private RecyclerView tagsRV, recommendationRv;
    private View rootView;
    private ProgressBar progressBar;
    private LinearLayout bulletLayout;
    private ImageView iv_save_offline;
    private NestedScrollView nestedScrollView;

    /*adapter*/
    private RecommendationAdapterNew recommendationAdapter;

    /*Model*/
    private ArticleCommonInfo articleCommonInfo;

    /*Variables*/
//    private String cssData;
    private int color;
    private String gaScreen, gaArticle;
    private String displayName;
    private String colorString;
    private String storyId;
    private boolean isFirstTime;
    private long delayTime = 40;

    /*Callback Listeners*/
    private String iitlTitle;
    private String newsTitle;
    private String[] bullets;
    private ArrayList<NewsPhotoInfo> photoInfosList;

    private String pubDate;
    private String providerName;
    private String providerLogo;
    private boolean prefetchDataExist = false;
    private boolean isPhotoFailed = false;
    private int listPosition;
    private String mPrevStoryId;
    private String cssData;
    private String gaEventLabel;
    private String gaCampaign;
    private boolean isNextStoryTagShow;
    private CircleImageView articleProviderImage;
    private ArrayList<Integer> percentageList = new ArrayList<>();
    private long startTime;
    //    private boolean isSentGATracking = false;
    private boolean isResumed = false;
    private boolean isNextStoryTrackingSend = false;
    private boolean isRecommendationTrackingSend = false;
    private boolean contentCompleteTrackingSend = false;
    private boolean isPhotoTrackingSend = false;
    private Rect visibleRect = new Rect();
    private TextView tvcomment;
    private DivyaArticleInteraction divyaArticleInteraction;

    /*Constructor*/
    public DivyaCommonFragment() {
        // Required empty public constructor
    }

    public static DivyaCommonFragment getInstance(String storyID, String color, String gaScreen, String gaArticle, String gaEventLabel, String displayName, boolean isFirstTime, int position) {
        DivyaCommonFragment divyaCommonFragment = new DivyaCommonFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KeyPair.KEY_STORY_ID, storyID);
        bundle.putString(Constants.KeyPair.KEY_COLOR, color);
        bundle.putBoolean(Constants.KeyPair.IS_FIRST_TIME, isFirstTime);
        bundle.putString(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        bundle.putString(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        bundle.putString(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
        bundle.putString(Constants.KeyPair.KEY_DISPLAY_NAME, displayName);
        bundle.putInt(Constants.KeyPair.KEY_POSITION, position);
        divyaCommonFragment.setArguments(bundle);
        return divyaCommonFragment;
    }

    public void setFirstTimeData(String iitlTitle, String newsTitle, String[] bullets, ArrayList<NewsPhotoInfo> photoInfosList, String gTrackUrl, String pubDate, String providerName, String providerLogo) {
        this.iitlTitle = iitlTitle;
        this.newsTitle = newsTitle;
        this.bullets = bullets;
        this.photoInfosList = photoInfosList;
        this.pubDate = pubDate;
        this.providerName = providerName;
        this.providerLogo = providerLogo;
        if (!TextUtils.isEmpty(newsTitle)) {
            prefetchDataExist = true;
        }
        if (photoInfosList == null || photoInfosList.isEmpty()) {
            isPhotoFailed = true;
            this.photoInfosList = new ArrayList<>();
            this.photoInfosList.add(getDefaultEmptyPhotoPojo());
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            colorString = getArguments().getString(Constants.KeyPair.KEY_COLOR);
            storyId = getArguments().getString(Constants.KeyPair.KEY_STORY_ID);
            isFirstTime = getArguments().getBoolean(Constants.KeyPair.IS_FIRST_TIME);
            colorString = AppUtils.getThemeColor(colorString);
            color = AppUtils.getThemeColor(getActivity(), colorString);
            gaScreen = getArguments().getString(Constants.KeyPair.KEY_GA_SCREEN);
            gaArticle = getArguments().getString(Constants.KeyPair.KEY_GA_ARTICLE);
            displayName = getArguments().getString(Constants.KeyPair.KEY_DISPLAY_NAME);
            listPosition = getArguments().getInt(Constants.KeyPair.KEY_POSITION, -1);
            gaEventLabel = getArguments().getString(Constants.KeyPair.KEY_GA_EVENT_LABEL);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_divya_common, container, false);
        progressBar = rootView.findViewById(R.id.progressBar);
        iv_save_offline = rootView.findViewById(R.id.iv_save_offline);
        tvcomment = rootView.findViewById(R.id.tv_comment);
        if (articleCommonInfo == null) {
            makeArticleDetailRequest();
        }

        initView();
        if (prefetchDataExist) {
            setPrefetchData();
        }
        setupWebView();
        handleArticleOperationLayout();
        handleArticleIframe();

        if (articleCommonInfo != null)
            initiateView(false);
        gaCampaign = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        return rootView;
    }


    private void setPrefetchData() {

        rootView.findViewById(R.id.page_shimmer).setVisibility(View.GONE);
        rootView.findViewById(R.id.article_layout).setVisibility(View.VISIBLE);

        /*Title*/
        articleTitleTv.setText(AppUtils.getInstance().getHomeTitle(iitlTitle, AppUtils.getInstance().fromHtml(newsTitle), colorString));

        /*mobile description*/
        prefetchBullets();

        /*photo Pager*/
        prefetchPhotoPager();

        /*providerName*/
        if (!TextUtils.isEmpty(providerName)) {
            articleProviderNameTv.setVisibility(View.VISIBLE);
            articleProviderNameTv.setText(providerName);
        } else {
            articleProviderNameTv.setVisibility(View.GONE);
        }

        /*Provider Image*/
        if (TextUtils.isEmpty(providerLogo)) {
            articleProviderImage.setVisibility(View.GONE);
        } else {
            articleProviderImage.setVisibility(View.VISIBLE);
            ImageUtil.setImage(getContext(), providerLogo, articleProviderImage, R.drawable.default_pic, R.drawable.default_pic);
        }

        /*Pub date*/
        if (!TextUtils.isEmpty(pubDate)) {
            articlePubDateTv.setVisibility(View.VISIBLE);
            articlePubDateTv.setText(pubDate);
        } else {
            articlePubDateTv.setVisibility(View.GONE);
        }
        changeFontSize(false);
    }


    private void initiateView(boolean isFragmentAdded) {
        handleUI(isFragmentAdded);
        if (isResumed) {
            if (divyaArticleInteraction != null)
                divyaArticleInteraction.onArticleSuccess(articleCommonInfo);
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResumeFragment(String prevStoryId, int position) {
        showAds();

        isResumed = true;

        mPrevStoryId = prevStoryId;
        startTime = System.currentTimeMillis();

        sendScreenTracking(articleCommonInfo, true, true);


       /* if (!isPhotoTrackingSend) {
            isPhotoTrackingSend = true;
            if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.GA_SCROLL_TEST_ENABLE, false))
                Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_TEST, AppFlyerConst.GAAction.ARTICLE, AppFlyerConst.GALabel.PHOTO, gaCampaign);
        }
*/
        if (divyaArticleInteraction != null) {
            divyaArticleInteraction.onArticleSuccess(articleCommonInfo);
            divyaArticleInteraction.changeStatusBarcolor(colorString);
        }

    }

    @Override
    public void onPauseFragment() {
        isResumed = false;
        int time = (int) (System.currentTimeMillis() - startTime);

        if (articleCommonInfo != null)
            initializeTracking(articleCommonInfo, time, mPrevStoryId);
    }

    public void sendScreenTracking(ArticleCommonInfo articleCommonInfo, boolean isNext, boolean isClicked) {
//        if (articleCommonInfo == null || isSentGATracking || !isResumed) {
        if (articleCommonInfo == null || !isResumed) {
            return;
        }
//        isSentGATracking = true;

//        String value = (isNext ? AppFlyerConst.GAScreen.NEXT_ARTICLE_GA_ARTICLE : gaArticle) + "-" + articleCommonInfo.gTrackUrl;
        if (isNext)
            gaArticle = AppFlyerConst.GAScreen.SWIPE_ARTICLE_GA_ARTICLE;
        String value = (isClicked ? AppFlyerConst.GAScreen.NEXT_ARTICLE_GA_ARTICLE : gaArticle) + "-" + articleCommonInfo.gTrackUrl;
        String publisher = articleCommonInfo.provider;

        String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

        Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), value, source, medium, campaign, publisher);
    }

    private void initializeTracking(ArticleCommonInfo articleCommonInfo, int time, String prevStoryId) {
        // Tracking
        if (!TextUtils.isEmpty(articleCommonInfo.trackUrl)) {
            String domain = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
            String url = domain + displayName;
            ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
            String email = "";
            String mobile = "";
            if (profileInfo != null) {
                email = profileInfo.email;
                mobile = profileInfo.mobile;
            }

            Systr.println("Tracking : time : " + time + ", PrevStoryId : " + prevStoryId + ", CurrentStoryId : " + articleCommonInfo.storyId);
            Tracking.trackWisdomArticlePage(getActivity(), articleCommonInfo.trackUrl, url, 1, email, mobile, CommonConstants.BHASKAR_APP_ID, listPosition, -1, time, prevStoryId);
        }
    }

    private void showAds() {
        if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
            ImageView imageView = new ImageView(getContext());
            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, getContext())));
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setAdjustViewBounds(true);
            imageView.setClickable(true);
            String homeBannerUrl = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
            ((ViewGroup) rootView.findViewById(R.id.adlayoutTop)).addView(imageView);
            ImageUtil.setImage(getContext(), homeBannerUrl, imageView, 0);
            imageView.setOnClickListener(view -> AppUtils.getInstance().clickOnAdBanner(getContext()));
        }

        if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.ATF_ARTICLE_TOGGLE, false)) {
            AdController.showArticleATF(getContext(), rootView.findViewById(R.id.adlayoutTop));
        } else {
            rootView.findViewById(R.id.adlayoutTop).setVisibility(View.GONE);
        }

        if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.BTF_ROS_ARTICLE_TOGGLE, false)) {
            AdController.showArticleBTF(getContext(), rootView.findViewById(R.id.adlayoutBottom));
        } else {
            rootView.findViewById(R.id.adlayoutBottom).setVisibility(View.GONE);
        }
    }

    private void makeArticleDetailRequest() {
        String finalDetailUrl = Urls.APP_FEED_BASE_URL + String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID) + storyId + "/";
        Systr.println("ARTICLE RESPONSE Url : " + finalDetailUrl);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, finalDetailUrl, null, response -> {
            progressBar.setVisibility(View.GONE);
            articleCommonInfo = new Gson().fromJson(response.toString(), ArticleCommonInfo.class);
            try {
                Systr.println("ARTICLE RESPONSE : " + response.toString(4));
                resetBookmark(articleCommonInfo);
            } catch (JSONException e) {
            }
            if (isAdded()) {
                initiateView(true);
            }

            sendScreenTracking(articleCommonInfo, false, false);

        }, error -> {
            error.printStackTrace();
            progressBar.setVisibility(View.GONE);

            if (isAdded()) {
                removeFragmentFromAdapter(storyId);

                if (error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.no_network_error));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void addFragmentToAdapter() {
        try {
            divyaArticleInteraction.addNextArticle(articleCommonInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void removeFragmentFromAdapter(String storyId) {
        /*try {
            ((DivyaCommonArticleActivity) Objects.requireNonNull(getActivity())).removeFragmentFromAdapter(storyId);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    @SuppressLint({"SetJavaScriptEnabled", "JavaScriptInterface"})
    private void setupWebView() {

        newsDetailWebView = rootView.findViewById(R.id.content_web_view);
        newsDetailWebView.setOnLongClickListener(v -> true);
        newsDetailWebView.setLongClickable(false);
        newsDetailWebView.setHorizontalScrollBarEnabled(false);
        newsDetailWebView.setVerticalScrollBarEnabled(false);
//        if (Urls.IS_TESTING) {
            WebView.setWebContentsDebuggingEnabled(true);
//        }
        // Enable Javascript
        WebSettings webSettings = newsDetailWebView.getSettings();
        // Add a WebViewClient
//        progressBar.setVisibility(View.VISIBLE);

        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);

        JavaScriptInterface javaScriptInterface = new JavaScriptInterface(getActivity());
        newsDetailWebView.addJavascriptInterface(javaScriptInterface, "JavaScriptInterface");


        webSettings.setEnableSmoothTransition(true);
        webSettings.setSaveFormData(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);

        newsDetailWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                // Inject CSS when page is done loading
                injectArticleCSS();
                progressBar.setVisibility(View.GONE);
                super.onPageFinished(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                OpenNewsLinkInApp openNewsLinkInApp = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    openNewsLinkInApp = new OpenNewsLinkInApp(getActivity(), url, Constants.KeyPair.KEY_NEWS_UPDATE, 1, AppFlyerConst.GAScreen.OPEN_LINK_GA_SCREEN, null);
                }
                openNewsLinkInApp.openLink();
                return true;

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                OpenNewsLinkInApp openNewsLinkInApp = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    openNewsLinkInApp = new OpenNewsLinkInApp(getActivity(), request.getUrl().toString(), Constants.KeyPair.KEY_NEWS_UPDATE, 1, AppFlyerConst.GAScreen.OPEN_LINK_GA_SCREEN, null);
                }
                openNewsLinkInApp.openLink();
                return true;
            }

        });
    }

    private void initView() {
        bulletLayout = rootView.findViewById(R.id.bullet_layout);
        photoLayout = rootView.findViewById(R.id.photo_layout);
        photoViewPager = rootView.findViewById(R.id.photo_viewpager);
        articleTitleTv = rootView.findViewById(R.id.article_title_tv);
        nextArticleTitle = rootView.findViewById(R.id.next_article_title);
        nextArticleImage = rootView.findViewById(R.id.next_article_image);
        nextArticleVideoFlag = rootView.findViewById(R.id.next_article_video_flag);
        nextArticleCard = rootView.findViewById(R.id.next_article_card);
        nestedScrollView = rootView.findViewById(R.id.nestedScrollView);

        if (nestedScrollView != null) {
            nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                if (!isNextStoryTrackingSend && nextArticleCard != null && isNextStoryTagShow) {
                    int height = nextArticleCard.getHeight();
                    nextArticleCard.getLocalVisibleRect(visibleRect);
                    if (visibleRect.bottom == height) {
                        isNextStoryTrackingSend = true;
                        if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.GA_SCROLL_TEST_ENABLE, false))
                            Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_TEST, AppFlyerConst.GAAction.ARTICLE, AppFlyerConst.GALabel.NEXT_STORY, gaCampaign);
                    }
                } /*else if (!isRecommendationTrackingSend) {
                    LinearLayout linearLayout = rootView.findViewById(R.id.recommendation_ll);
                    int height = linearLayout.getHeight();
                    linearLayout.getLocalVisibleRect(visibleRect);
                    if (visibleRect.bottom == height) {
                        isRecommendationTrackingSend = true;
                        if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.GA_SCROLL_TEST_ENABLE, false))
                            Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_TEST, AppFlyerConst.GAAction.ARTICLE, AppFlyerConst.GALabel.RECOMMENDATION, gaCampaign);
                    }
                }*/

                if (!contentCompleteTrackingSend && AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.GA_SCROLL_TEST_ENABLE, false)) {
                    View view = rootView.findViewById(R.id.content_complete_view);
                    int height = view.getHeight();
                    view.getLocalVisibleRect(visibleRect);
                    if (visibleRect.bottom == height) {
                        contentCompleteTrackingSend = true;
                        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_TEST, AppFlyerConst.GAAction.ARTICLE, AppFlyerConst.GALabel.COMPLETE, gaCampaign);
                    }
                }
            });
        }
        /*shimmer */
        ShimmerFrameLayout container = rootView.findViewById(R.id.page_shimmer);
        container.startShimmer();

        nextArticleCard.setOnClickListener(v -> {
            divyaArticleInteraction.showNextStory();
            //onRelatedArticleClickListener.onNextArticleClick(articleCommonInfo.nextArticle.storyId, articleCommonInfo.nextArticle.catColor, articleCommonInfo.nextArticle.headerName)
        });
        articleRecommendationTv = rootView.findViewById(R.id.article_recommendation_tv);
        tagsRV = rootView.findViewById(R.id.tags_recycler_view);
        recommendationRv = rootView.findViewById(R.id.article_recommendation_rv);
        articlePubDateTv = rootView.findViewById(R.id.article_pub_date_tv);
        articleProviderNameTv = rootView.findViewById(R.id.article_provider_name);
        articleProviderImage = rootView.findViewById(R.id.provider_iv);
    }

    private void resetBookmark(ArticleCommonInfo articleCommonInfo) {
        boolean isStoryBookmarked = false;
        if (articleCommonInfo != null)
            isStoryBookmarked = SerializeData.getInstance(getActivity()).isBookmarkAvailable(AppUtils.getFilteredStoryId(articleCommonInfo.storyId));

        if (iv_save_offline != null) {
            if (isStoryBookmarked) {
                iv_save_offline.setImageResource(R.drawable.ic_appbar_bookmark_filled);
            } else {
                iv_save_offline.setImageResource(R.drawable.ic_appbar_bookmark);
            }
        }
    }

    public void handleArticleOperationLayout() {
        ImageView iv_comment = rootView.findViewById(R.id.iv_comment);
        ImageView iv_share = rootView.findViewById(R.id.iv_share);
        TextView tv_comment = rootView.findViewById(R.id.tv_comment);
        TextView tv_save_offline = rootView.findViewById(R.id.tv_save_offline);
        TextView tv_share = rootView.findViewById(R.id.tv_share);

        iv_comment.setColorFilter(color);
        iv_save_offline.setColorFilter(color);
        //resetBookmark();
        iv_share.setColorFilter(color);
        tv_comment.setTextColor(color);
        tv_save_offline.setTextColor(color);
        tv_share.setTextColor(color);
        LinearLayout ll_comment = rootView.findViewById(R.id.ll_comment);
        boolean isGdprBlockCountry = AppPreferences.getInstance(getActivity()).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        if (isGdprBlockCountry) {
            ll_comment.setVisibility(View.GONE);
        }
//
//        AppUtils.getInstance().setArticleFontSize(getActivity(), tv_comment, Constants.Font.CAT_SMALL_LARGE);
//        AppUtils.getInstance().setArticleFontSize(getActivity(), tv_save_offline, Constants.Font.CAT_SMALL_LARGE);
//        AppUtils.getInstance().setArticleFontSize(getActivity(), tv_share, Constants.Font.CAT_SMALL_LARGE);

        ll_comment.setOnClickListener(v -> {
            if (divyaArticleInteraction != null) {
                divyaArticleInteraction.onCommentClick(articleCommonInfo);
            }
        });
        LinearLayout llSaveOffline = rootView.findViewById(R.id.ll_save_offline);
        llSaveOffline.setOnClickListener(v -> {
            if (divyaArticleInteraction != null) {
                divyaArticleInteraction.onBookmarkClick(articleCommonInfo);
            }
            onBookmarkClicked(articleCommonInfo, true);
        });

        LinearLayout llShare = rootView.findViewById(R.id.ll_share);
        llShare.setOnClickListener(v -> {
            if (articleCommonInfo != null) {
                if (!TextUtils.isEmpty(articleCommonInfo.shareLink)) {
                    AppUtils.getInstance().shareArticle(getActivity(), articleCommonInfo.shareLink, articleCommonInfo.title, articleCommonInfo.gTrackUrl, true);
                } else {
                    BackgroundRequest.getStoryShareUrl(getContext(), articleCommonInfo.link, (flag, storyShareUrl) -> {
                        if (flag) {
//                            articleCommonInfo.shareLink = storyShareUrl;
                        }
                        AppUtils.getInstance().shareArticle(getContext(), storyShareUrl, articleCommonInfo.title, articleCommonInfo.gTrackUrl, true);
                    });
                }
            }
        });

    }

    private void onBookmarkClicked(ArticleCommonInfo articleCommonInfo, boolean fromDetail) {
        if (articleCommonInfo != null) {
            boolean isBookMarked = SerializeData.getInstance(getContext()).isBookmarkAvailable(AppUtils.getFilteredStoryId(articleCommonInfo.storyId));
            if (!isBookMarked) {

                // Tracking
                String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.BOOKMARK, articleCommonInfo.gTrackUrl, campaign);
                CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.BOOKMARKED, CTConstant.PROP_NAME.CONTENT_URL, articleCommonInfo.gTrackUrl, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.BOOKMARKED_THROUGH, fromDetail ? CTConstant.PROP_VALUE.STORY_DETAIL : CTConstant.PROP_VALUE.THREE_DOTS));

                BookmarkSerializedListInfo bookmarkSerializedListInfo = new BookmarkSerializedListInfo();

                bookmarkSerializedListInfo.setIitlTitle(articleCommonInfo.iitlTitle);
                bookmarkSerializedListInfo.setColor(colorString);
                bookmarkSerializedListInfo.setTitle(articleCommonInfo.title);
                bookmarkSerializedListInfo.setImage(articleCommonInfo.image);
                bookmarkSerializedListInfo.setStoryId(AppUtils.getFilteredStoryId(articleCommonInfo.storyId));
//                bookmarkSerializedListInfo.setChannelSlno(CommonConstants.CHANNEL_ID);
                bookmarkSerializedListInfo.setSlugIntro(articleCommonInfo.slugIntro);
                bookmarkSerializedListInfo.setVideoFlag(2);
                bookmarkSerializedListInfo.setContent(new Gson().toJson(articleCommonInfo));
                bookmarkSerializedListInfo.setDetailUrl(Urls.DEFAULT_DETAIL_URL);
                bookmarkSerializedListInfo.setGaScreen(gaScreen);
                bookmarkSerializedListInfo.setGaArticle(gaArticle);
                bookmarkSerializedListInfo.setGaEventLabel(gaEventLabel);
                SerializeData.getInstance(getContext()).saveBookmark(bookmarkSerializedListInfo);
                AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.bookmark_added));

            }

        }
        resetBookmark(articleCommonInfo);

    }


    private void handleUI(boolean isFragmentAdded) {
        readyCssData(false);
        /*Story*/
        if (!TextUtils.isEmpty(articleCommonInfo.story)) {
            if (articleCommonInfo.story.contains("twitter") || articleCommonInfo.story.contains("insta") || articleCommonInfo.story.contains("facebook"))
                delayTime = 100;
            newsDetailWebView.loadDataWithBaseURL("file:///android_asset/", CssConstants.getContentWithFontFamily(getContext(), articleCommonInfo.story, cssData), "text/html; charset=utf-8", null, "");
        } else {
            newsDetailWebView.setVisibility(View.GONE);
        }

        /*Title*/
        if (TextUtils.isEmpty(newsTitle)) {
            articleTitleTv.setText(AppUtils.getInstance().getHomeTitle(articleCommonInfo.iitlTitle, AppUtils.getInstance().fromHtml(articleCommonInfo.title), colorString));
        }

        /*Image*/
        if (photoInfosList == null || isPhotoFailed) {
            handlePhotoPager();
        }

        /*mobile description*/
        if (bullets == null || bullets.length == 0) {
            handleMobileDescription();
        }

        /*Provider*/
        if (TextUtils.isEmpty(providerName)) {
            if (!TextUtils.isEmpty(articleCommonInfo.provider)) {
                articleProviderNameTv.setVisibility(View.VISIBLE);
                articleProviderNameTv.setText(articleCommonInfo.provider);
            } else {
                articleProviderNameTv.setVisibility(View.GONE);
            }
        }

        /*Provider Image*/
        if (TextUtils.isEmpty(articleCommonInfo.providerImage)) {
            articleProviderImage.setVisibility(View.GONE);
        } else {
            articleProviderImage.setVisibility(View.VISIBLE);
            ImageUtil.setImage(getContext(), articleCommonInfo.providerImage, articleProviderImage, R.drawable.default_pic, R.drawable.default_pic);
        }

        /*Pub date*/
        if (TextUtils.isEmpty(pubDate)) {
            if (!TextUtils.isEmpty(articleCommonInfo.pubdate)) {
                articlePubDateTv.setVisibility(View.VISIBLE);
                articlePubDateTv.setText(articleCommonInfo.pubdate);
            } else {
                articlePubDateTv.setVisibility(View.GONE);
            }
        }

        /*For News Tags */
        if (!TextUtils.isEmpty(articleCommonInfo.keywords)) {
            FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
            layoutManager.setFlexDirection(FlexDirection.ROW);
            layoutManager.setJustifyContent(JustifyContent.FLEX_START);
            tagsRV.setLayoutManager(layoutManager);
            tagsRV.setHasFixedSize(true);
            tagsRV.setVisibility(View.VISIBLE);
            TagsLabelAdapterNew tagsLabelAdapter = new TagsLabelAdapterNew(getActivity(), divyaArticleInteraction, articleCommonInfo.keywords);
            tagsRV.setAdapter(tagsLabelAdapter);
        } else {
            tagsRV.setVisibility(View.GONE);
        }

        rootView.findViewById(R.id.page_shimmer).setVisibility(View.GONE);
        rootView.findViewById(R.id.article_layout).setVisibility(View.VISIBLE);

        /*Next Article */
        if (articleCommonInfo.nextArticle != null && !TextUtils.isEmpty(articleCommonInfo.nextArticle.storyId)) {
            nextArticleCard.setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.next_article_layout).setVisibility(View.VISIBLE);
            isNextStoryTagShow = true;
            if (isFragmentAdded)
                addFragmentToAdapter();
            if (!TextUtils.isEmpty(articleCommonInfo.nextArticle.title)) {
                if (!TextUtils.isEmpty(articleCommonInfo.nextArticle.iitlTitle)) {
                    nextArticleTitle.setText(AppUtils.getInstance().getHomeTitle(articleCommonInfo.nextArticle.iitlTitle, AppUtils.getInstance().fromHtml(articleCommonInfo.nextArticle.title), articleCommonInfo.nextArticle.catColor));
                } else {
                    nextArticleTitle.setText(articleCommonInfo.nextArticle.title);
                }
            }
            if (!TextUtils.isEmpty(articleCommonInfo.nextArticle.image)) {
                ImageUtil.setImage(getContext(), articleCommonInfo.nextArticle.image, nextArticleImage, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);
                if (articleCommonInfo.nextArticle.videoflag.equalsIgnoreCase("1")) {
                    nextArticleVideoFlag.setVisibility(View.VISIBLE);
                    nextArticleVideoFlag.setColorFilter(AppUtils.getThemeColor(getContext(), articleCommonInfo.nextArticle.catColor));
                } else {
                    nextArticleVideoFlag.setVisibility(View.GONE);
                }
            } else {
                rootView.findViewById(R.id.thumb_image_layout).setVisibility(View.GONE);
            }
        } else {
            nextArticleCard.setVisibility(View.GONE);
            rootView.findViewById(R.id.next_article_layout).setVisibility(View.GONE);
            isNextStoryTagShow = false;
        }

        /*Recommendation*/
        if (articleCommonInfo.relatedArticle != null && articleCommonInfo.relatedArticle.size() > 0) {
            rootView.findViewById(R.id.recommendation_ll).setVisibility(View.VISIBLE);
            recommendationRv.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
            recommendationAdapter = new RecommendationAdapterNew(getActivity(), divyaArticleInteraction, articleCommonInfo.relatedArticle, false, false);
            recommendationAdapter.useInNewArticle(true, colorString);
            recommendationRv.setAdapter(recommendationAdapter);

        } else {
            rootView.findViewById(R.id.recommendation_ll).setVisibility(View.GONE);
        }

        changeFontSize(false);
        getAllArticleComments(AppUtils.getInstance().postHashCode(articleCommonInfo.postIdUrl));
    }


    private void getAllArticleComments(int postId) {
        String finalUrl = String.format(Urls.RETRIEVE_ALL_COMMENTS, CommonConstants.CHANNEL_ID, postId);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl, response -> {
            try {
                if (!TextUtils.isEmpty(TrackingUtils.decryptString(response))) {
                    parseArticleComments(new JSONObject(TrackingUtils.decryptString(response)));
                }
            } catch (Exception ignored) {
            }
        }, error -> {
            Activity activity = getActivity();
            if (activity != null && isAdded()) {
                if (error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "1");
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    private void parseArticleComments(JSONObject response) {
        int totalParentComments = response.optInt("totalParentComments");
        String commentText;
        if (totalParentComments > 1) {
            commentText = " Comments";
        } else {
            commentText = " Comment";
        }

        if (totalParentComments > 0) {
            commentText = ((totalParentComments >= 100) ? "99k" : totalParentComments) + commentText;
        } else {
            commentText = "Comments";
        }
        if (tvcomment != null)
            tvcomment.setText(commentText);
    }

    private void handleMobileDescription() {
        if (articleCommonInfo != null && articleCommonInfo.mobileDescription != null && articleCommonInfo.mobileDescription.length > 0) {
            bulletLayout.removeAllViews();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            for (int i = 0; i < articleCommonInfo.mobileDescription.length; i++) {
                View item = inflater.inflate(R.layout.layout_article_bullet_type, null);
                String desc = articleCommonInfo.mobileDescription[i];
                JustifiedTextView bullectTv = item.findViewById(R.id.bullet_tv);
                bullectTv.setText(AppUtils.getInstance().fromHtml(desc));
                AppUtils.getInstance().setArticleFontSizeJustify(getContext(), bullectTv, Constants.Font.CAT_SMALL_LARGE);
                ((JustifiedTextView) item.findViewById(R.id.bullet_tv)).setText(AppUtils.getInstance().fromHtml(desc));
                ((ImageView) item.findViewById(R.id.iv_bullet_circle)).setColorFilter(AppUtils.getThemeColor(getContext(), colorString));
                bulletLayout.addView(item);
            }
            bulletLayout.setVisibility(View.VISIBLE);
        } else {
            bulletLayout.setVisibility(View.GONE);
        }
    }

    private void prefetchBullets() {
        if (bullets != null && bullets.length > 0) {
            bulletLayout.removeAllViews();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            for (String bullet : bullets) {
                View item = inflater.inflate(R.layout.layout_article_bullet_type, null);
                JustifiedTextView bullectTv = item.findViewById(R.id.bullet_tv);
                bullectTv.setText(AppUtils.getInstance().fromHtml(bullet));
                AppUtils.getInstance().setArticleFontSizeJustify(getContext(), bullectTv, Constants.Font.CAT_SMALL_LARGE);
                ((ImageView) item.findViewById(R.id.iv_bullet_circle)).setColorFilter(AppUtils.getThemeColor(getContext(), colorString));
                bulletLayout.addView(item);
            }
            bulletLayout.setVisibility(View.VISIBLE);
        } else {
            bulletLayout.setVisibility(View.GONE);
        }
    }

    private void prefetchPhotoPager() {
        if ((photoInfosList != null && photoInfosList.size() > 0)) {
            photoLayout.setVisibility(View.VISIBLE);
            PhotoPagerAdapter photoPagerAdapter = new PhotoPagerAdapter(getChildFragmentManager(), photoInfosList);
            photoViewPager.setAdapter(photoPagerAdapter);
            photoViewPager.setPageTransformer(false, AppUtils.getParallaxPagerTransform(R.id.article_iv, 0, 0.5f));


        } else {
            photoViewPager.setVisibility(View.GONE);
            photoLayout.setVisibility(View.GONE);
        }
        BubblePageIndicator pager_indicator = rootView.findViewById(R.id.pager_indicator);
        if (photoInfosList != null && photoInfosList.size() > 1) {
            pager_indicator.setViewPager(photoViewPager);
            pager_indicator.setFillColor(color);
        } else {
            pager_indicator.setVisibility(View.GONE);
        }

    }

    private void handlePhotoPager() {
        if (articleCommonInfo.photos.size() == 0) {
            if (!TextUtils.isEmpty(articleCommonInfo.image)) {
                NewsPhotoInfo newsPhotoInfo = new NewsPhotoInfo();
                newsPhotoInfo.image = articleCommonInfo.image;
                newsPhotoInfo.imageSize = "730x548";
                articleCommonInfo.photos.add(newsPhotoInfo);
            } else {
                articleCommonInfo.photos.add(getDefaultEmptyPhotoPojo());
            }
        }
        if ((articleCommonInfo.photos != null && articleCommonInfo.photos.size() > 0) || !TextUtils.isEmpty(articleCommonInfo.image)) {
            photoLayout.setVisibility(View.VISIBLE);
            PhotoPagerAdapter photoPagerAdapter = new PhotoPagerAdapter(getChildFragmentManager(), articleCommonInfo.photos);
            photoViewPager.setAdapter(photoPagerAdapter);
            photoViewPager.setPageTransformer(false, AppUtils.getParallaxPagerTransform(R.id.article_iv, 0, 0.5f));

        }

        BubblePageIndicator pager_indicator = rootView.findViewById(R.id.pager_indicator);
        if (articleCommonInfo.photos != null && articleCommonInfo.photos.size() > 1) {
            pager_indicator.setViewPager(photoViewPager);
            pager_indicator.setFillColor(color);
        } else {
            pager_indicator.setVisibility(View.GONE);
        }


    }


    private void handleArticleIframe() {
        AppPreferences appPreferences = AppPreferences.getInstance(getActivity());
        int articleIframePosition = appPreferences.getIntValue(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_POSITION, 0);
        if (appPreferences.getBooleanValue(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_IS_ACTIVE, false)) {
            LinearLayout iFrameLayout;

            if (articleIframePosition == 1) {
                iFrameLayout = rootView.findViewById(R.id.iframe_layout1);
            } else if (articleIframePosition == 2) {
                iFrameLayout = rootView.findViewById(R.id.iframe_layout2);
            } else if (articleIframePosition == 3) {
                iFrameLayout = rootView.findViewById(R.id.iframe_layout3);
            } else {
                iFrameLayout = rootView.findViewById(R.id.iframe_layout0);
            }
            iFrameLayout.removeAllViews();
            String iframeUrl = appPreferences.getStringValue(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_URL, "");
            final String actionUrl = appPreferences.getStringValue(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_ACTION_URL, "");
            final String actionName = appPreferences.getStringValue(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_ACTION_NAME, "");
            final String gaScreen = appPreferences.getStringValue(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_GA_SCREEN, "");
            final boolean isIframeLoginRequired = appPreferences.getBooleanValue(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_LOGIN_REQUIRED, false);

            final LayoutInflater inflater = LayoutInflater.from(getContext());
            View web = inflater.inflate(R.layout.layout_article_detail_table_view, null);

            WebView mWebView = web.findViewById(R.id.article_table_view);
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setLoadWithOverviewMode(true);
            webSettings.setUseWideViewPort(true);
            webSettings.setDomStorageEnabled(true);
            mWebView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return true;
                }
            });
            mWebView.setScrollContainer(true);
            mWebView.setWebViewClient(new MyWebViewClient());

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_TOP);
            mWebView.setLayoutParams(params);

            if (!TextUtils.isEmpty(iframeUrl))
                mWebView.loadUrl(iframeUrl);

            mWebView.setOnTouchListener(new View.OnTouchListener() {
                private float startX;
                private float startY;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            startX = event.getX();
                            startY = event.getY();
                            break;
                        case MotionEvent.ACTION_UP:
                            float endX = event.getX();
                            float endY = event.getY();
                            if (AppUtils.getInstance().isAClick(startX, endX, startY, endY)) {
                                try {
                                    onIFrameClick(isIframeLoginRequired, actionUrl, actionName);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            break;
                    }
                    return true;
                }
            });

            iFrameLayout.addView(mWebView);
            iFrameLayout.setVisibility(View.VISIBLE);
        }
    }

    private void onIFrameClick(boolean isIframeLoginRequired, String actionUrl, String actionName) throws Exception {
        if (!TextUtils.isEmpty(actionUrl)) {
            if (actionUrl.contains(getString(R.string.dynamic_link_host1)) || actionUrl.contains(getString(R.string.dynamic_link_host2))) {
                ActivityUtil.getInstance().handleDynamicLink(getContext(), actionUrl, true, object -> {
                    HashMap<String, String> parsedMap = (HashMap<String, String>) object;
                    String menuID = parsedMap.get("menu_id");
                    CategoryInfo categoryInfo = JsonParser.getInstance().getCategoryInfoById(getContext(), menuID);
                    ActivityUtil.openCategoryAccordingToAction(getContext(), categoryInfo, "");
                });
            } else {
                if (isIframeLoginRequired) {
                    if (LoginController.loginController().isUserLoggedIn()) {
                        if (!TextUtils.isEmpty(actionUrl)) {
                            String newActionUrl = actionUrl;
                            if (actionUrl.contains("?")) {
                                newActionUrl = newActionUrl + "&db_id=" + TrackingData.getDBId(getActivity());
                            } else {
                                newActionUrl = newActionUrl + "?db_id=" + TrackingData.getDBId(getActivity());
                            }
                            Intent webIntent = new Intent(getActivity(), WebViewActivity.class);
                            webIntent.putExtra(Constants.KeyPair.KEY_URL, newActionUrl);
                            webIntent.putExtra(Constants.KeyPair.KEY_TITLE, actionName);
                            webIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
                            startActivity(webIntent);
                        }
                    } else {
                        LoginController.loginController().getLoginIntent(getActivity(), TrackingData.getDBId(getContext()), TrackingData.getDeviceId(getContext()), 0);
                    }
                }
            }
        }
    }

    private void injectArticleCSS() {
        new Handler().postDelayed(() -> {
            rootView.findViewById(R.id.webview_shimmer).setVisibility(View.GONE);
            newsDetailWebView.setVisibility(View.VISIBLE);
            /*article operation layout*/
            rootView.findViewById(R.id.article_operation_layout).setVisibility(View.VISIBLE);
        }, delayTime);
    }

    public void readyCssData(boolean reload) {
        String fontSize;
        String lineHeight;
        int lastFontSize = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
        if (lastFontSize == Constants.Font.MEDIUM) {
            fontSize = CssConstants.fontSizeRegular;
            lineHeight = CssConstants.lineHeightRegular;
        } else {
            fontSize = CssConstants.fontSizeLarge;
            lineHeight = CssConstants.lineHeightLarge;
        }

        cssData = JsonPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.CSSData.CSS_DATA, CssConstants.defaultCssData);

        cssData = cssData.replaceAll("app_color", colorString);
        cssData = cssData.replaceAll("font_size", fontSize);
        cssData = cssData.replaceAll("line_height", lineHeight);
        cssData = cssData.replaceAll("anchor_color", CssConstants.anchorColor);

        // load WebViewData
        if (reload && articleCommonInfo != null) {
            newsDetailWebView.loadDataWithBaseURL("file:///android_asset/", CssConstants.getContentWithFontFamily(getContext(), articleCommonInfo.story, cssData), "text/html; charset=utf-8", null, "");
        }
    }

    public void changeFontSize(boolean reload) {
        AppUtils.getInstance().setArticleFontSize(getActivity(), articleTitleTv, Constants.Font.CAT_LARGE);
        AppUtils.getInstance().setArticleFontSize(getActivity(), nextArticleTitle, Constants.Font.CAT_SMALL_LARGE);
        AppUtils.getInstance().setArticleFontSize(getActivity(), articleProviderNameTv, Constants.Font.CAT_SMALL);
        AppUtils.getInstance().setArticleFontSize(getActivity(), articlePubDateTv, Constants.Font.CAT_PUB_DATE);
//        AppUtils.getInstance().setArticleFontSize(getActivity(), articleRecommendationTv, Constants.Font.CAT_MEDIUM);

        if (recommendationAdapter != null) {
            recommendationAdapter.notifyDataSetChanged();
        }

        if (tagsRV != null && tagsRV.getAdapter() != null) {
            tagsRV.getAdapter().notifyDataSetChanged();
        }


        if (photoViewPager != null && photoViewPager.getAdapter() != null) {
            photoViewPager.getAdapter().notifyDataSetChanged();
        }

        if (reload) {
            readyCssData(reload);
            handleMobileDescription();
        }
    }

    public void moveToTop() {
        nestedScrollView.scrollTo(0, 0);
    }

    @Override
    public void onClick(View view, int position) {
        if (articleCommonInfo == null || articleCommonInfo.photosGallery == null || articleCommonInfo.photos == null)
            return;

        Intent photoGalleryIntent = new Intent(getContext(), PhotoGalleryActivity.class);
        photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_DETAIL);
        photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, position);
        NewsDetailPhotoInfo newsDetailPhotoInfo = new NewsDetailPhotoInfo();
        newsDetailPhotoInfo.photos = (articleCommonInfo.photosGallery == null || articleCommonInfo.photosGallery.size() == 0) ? articleCommonInfo.photos : articleCommonInfo.photosGallery;
        photoGalleryIntent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL, newsDetailPhotoInfo);
        photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, articleCommonInfo.gTrackUrl);
        photoGalleryIntent.putExtra(Constants.KeyPair.KEY_LINK, articleCommonInfo.link);
        photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_TRACK_URL, articleCommonInfo.trackUrl);
        startActivity(photoGalleryIntent);
    }

    public ArticleCommonInfo getCurrentArticleInfo() {
        return articleCommonInfo;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void refreshBookMark(ArticleCommonInfo articleCommonInfo) {
        resetBookmark(articleCommonInfo);
    }

    private NewsPhotoInfo getDefaultEmptyPhotoPojo() {
        NewsPhotoInfo newsPhotoInfo = new NewsPhotoInfo();
        newsPhotoInfo.image = "";
        newsPhotoInfo.imageSize = "730x548";
        newsPhotoInfo.videoFlag = 0;
        return newsPhotoInfo;
    }

    public void hideNextStory() {
        nextArticleCard.setVisibility(View.GONE);
        rootView.findViewById(R.id.next_article_layout).setVisibility(View.GONE);
        isNextStoryTagShow = false;
    }

    public void setArticleListener(DivyaArticleInteraction divyaArticleInteraction) {
        this.divyaArticleInteraction = divyaArticleInteraction;
    }

    class PhotoPagerAdapter extends WrappingFragmentStatePagerAdapter {
        ArrayList<NewsPhotoInfo> mList;

        PhotoPagerAdapter(FragmentManager fm, ArrayList<NewsPhotoInfo> list) {
            super(fm);
            mList = list;
        }

        @Override
        public Fragment getItem(int position) {
            NewsPhotoInfo newsPhotoInfo = mList.get(position);
            if (TextUtils.isEmpty(newsPhotoInfo.title))
                newsPhotoInfo.articleTitle = newsTitle;
            DivyaCommonPhotoFragment divyaCommonPhotoFragment = DivyaCommonPhotoFragment.newInstance(newsPhotoInfo, position, newsPhotoInfo.imageSize, gaArticle, gaEventLabel, displayName, true, colorString);
            divyaCommonPhotoFragment.setListener(DivyaCommonFragment.this);
            return divyaCommonPhotoFragment;
        }

        @Override
        public int getCount() {
            return (mList != null) ? mList.size() : 0;
        }
    }

    class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Context context = getContext();
            if (context != null) {
                if (!TextUtils.isEmpty(url)) {
                    if (url.contains(context.getString(R.string.dynamic_link_host1)) | url.contains(context.getString(R.string.dynamic_link_host2))) {
                        ActivityUtil.getInstance().handleDynamicLink(getContext(), url);
                    } else {
                        OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(context, url, Constants.KeyPair.KEY_NEWS_UPDATE, 0, "", null);
                        openNewsDetailInApp.openLink();
                    }
                }
            }
            return true;
        }
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void alertAndroid() {
            Systr.println("JavaScriptInterface->alertAndroid");
        }
    }
}
