package com.db.divya_new.articlePage;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bhaskar.R;
import com.db.data.models.NewsPhotoInfo;
import com.db.dbvideo.player.BusProvider;
import com.db.listeners.OnItemClickListener;
import com.db.eventWrapper.FragmentItemClickEventWrapper;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.bhaskar.util.CssConstants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.Urls;

/**
 * A simple {@link Fragment} subclass.
 */
public class DivyaCommonPhotoFragment extends Fragment {
    private NewsPhotoInfo newsPhotoInfo;
    private String imageSize;
    private String gaArticle, gaEventLabel, displayName;
    private OnItemClickListener listener;
    private int index;
    private WebView videoWebView;
    private boolean showZoomIcon;
    private String color;

    public DivyaCommonPhotoFragment() {
        // Required empty public constructor
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public static DivyaCommonPhotoFragment newInstance(NewsPhotoInfo newsPhotoInfo, int index, String imageSize, String gaArticle, String gaEventLabel, String displayName, boolean showZoomIcon,String color) {
        DivyaCommonPhotoFragment fragment = new DivyaCommonPhotoFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("data", newsPhotoInfo);
        bundle.putInt("index", index);
        bundle.putString("imageSize", imageSize);
        bundle.putString("gaArticle", gaArticle);
        bundle.putString("gaEventLabel", gaEventLabel);
        bundle.putString("displayName", displayName);
        bundle.putBoolean("zoomIcon", showZoomIcon);
        bundle.putString("color", color);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            newsPhotoInfo = (NewsPhotoInfo) getArguments().getSerializable("data");
            index = getArguments().getInt("index");
            imageSize = getArguments().getString("imageSize");
            gaArticle = getArguments().getString("gaArticle");
            gaEventLabel = getArguments().getString("gaEventLabel");
            displayName = getArguments().getString("displayName");
            showZoomIcon = getArguments().getBoolean("zoomIcon");
            color = getArguments().getString("color");
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        videoWebView.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        videoWebView.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_divya_common_photo, container, false);

        RelativeLayout imageLayout = rootView.findViewById(R.id.image_layout);
        videoWebView = rootView.findViewById(R.id.video_web_view);
        setUpVideoWebView(videoWebView);
        ImageView articleIV = rootView.findViewById(R.id.article_iv);
        ImageView videoIcon = rootView.findViewById(R.id.img_video_play);
        ImageView zoomIcon = rootView.findViewById(R.id.zoom_icon);
        TextView photoCaption = rootView.findViewById(R.id.photo_caption);

        if (!TextUtils.isEmpty(imageSize)) {
            float ratio = AppUtils.getInstance().parseImageRatio(imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
            AppUtils.getInstance().setImageViewSizeWithAspectRatio(articleIV, ratio, 0, getActivity());
        }

        if (!showZoomIcon) {
            zoomIcon.setVisibility(View.GONE);
        }

        String imagePath = null;
        String imageThumb = null;
        if (newsPhotoInfo != null) {
            imagePath = newsPhotoInfo.image;
            imageThumb = newsPhotoInfo.thumb_img;
            if (newsPhotoInfo.videoFlag == 1) {
                videoIcon.setVisibility(View.VISIBLE);
                videoIcon.setColorFilter(AppUtils.getThemeColor(getContext(),color));
                zoomIcon.setVisibility(View.GONE);
                imageLayout.setVisibility(View.VISIBLE);
                videoWebView.setVisibility(View.GONE);
            } else if (!TextUtils.isEmpty(newsPhotoInfo.iframeData)) {
                imageLayout.setVisibility(View.GONE);
                videoWebView.setVisibility(View.VISIBLE);
                videoWebView.loadDataWithBaseURL("file:///android_asset/", CssConstants.getVideoContent(newsPhotoInfo.iframeData), "text/html; charset=utf-8", null, "");
            } else {
                imageLayout.setVisibility(View.VISIBLE);
                videoWebView.setVisibility(View.GONE);
            }
            rootView.setOnClickListener(v -> {
                if (newsPhotoInfo.videoFlag == 1) {
                    //TODO Need to set title
                    String sectionLabel = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME) + "_" + displayName;
                    Intent videoIntent = DivyaCommonVideoActivity.getIntent(getActivity(), newsPhotoInfo, sectionLabel, AppFlyerConst.DBVideosSource.ARTICLE, -1, gaEventLabel, gaArticle);
                    getActivity().startActivity(videoIntent);
                } else {
                    if (!TextUtils.isEmpty(newsPhotoInfo.image) && TextUtils.isEmpty(newsPhotoInfo.iframeData)) {
                        if (listener != null) {
                            listener.onClick(v, index);
                        } else {
                            BusProvider.getInstance().post(new FragmentItemClickEventWrapper(v, index));
                        }
                    }
                }
            });
            /*Slug Intro*/
            if (!TextUtils.isEmpty(newsPhotoInfo.title)) {
                AppUtils.getInstance().setArticleFontSize(getActivity(), photoCaption, Constants.Font.CAT_SMALL_LARGE);
                photoCaption.setText(newsPhotoInfo.title);
                photoCaption.setVisibility(View.VISIBLE);
            } else {
                photoCaption.setVisibility(View.GONE);
            }
        }

        if (TextUtils.isEmpty(imageThumb)) {
            ImageUtil.setImage(getActivity(), imagePath, articleIV, R.drawable.water_mark_news_detail, R.drawable.water_mark_big_news_detail_error);
        } else {
            ImageUtil.setImageWithThumbnail(getActivity(), imagePath, imageThumb, articleIV, R.drawable.water_mark_news_detail, R.drawable.water_mark_big_news_detail_error);
        }
        zoomIcon.setOnClickListener(v -> {
            if (listener != null && !TextUtils.isEmpty(newsPhotoInfo.image) && TextUtils.isEmpty(newsPhotoInfo.iframeData)) {
                listener.onClick(v, index);
            }

        });

        return rootView;
    }

    private void setUpVideoWebView(WebView webView) {
        if (Urls.IS_TESTING) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        // Enable Javascript
        WebSettings webSettings = webView.getSettings();
        // Add a WebViewClient
//        progressBar.setVisibility(View.VISIBLE);

        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);
    }
}
