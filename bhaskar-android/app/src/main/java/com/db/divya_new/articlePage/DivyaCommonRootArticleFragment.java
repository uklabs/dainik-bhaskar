package com.db.divya_new.articlePage;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.bhaskar.util.AppUrls;
import com.bhaskar.util.LoginController;
import com.bhaskar.util.ModuleUtilAll;
import com.db.InitApplication;
import com.db.data.models.BookmarkSerializedListInfo;
import com.db.data.models.NewsPhotoInfo;
import com.db.data.models.RelatedArticleInfo;
import com.db.data.source.remote.SerializeData;
import com.db.data.source.server.BackgroundRequest;
import com.db.listeners.FragmentLifecycleArticle;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Vector;


public class DivyaCommonRootArticleFragment extends BaseArticleFragment {

    public boolean isClickedNextStoryButton;
    /*Views*/
    private ViewPager viewPager;
    private TextView headerTitle;
    private Menu rootMenu;
    /*Adapter*/
    private SectionPagerAdapter sectionPagerAdapter;
    /*Variables*/
    private String colorString;
    private int color;
    private String gaScreen;
    private String displayName;
    private String gaArticle;
    private String gaEventLabel;
    private String storyId;
    private String title;
    private boolean bundleIsFromWidget;
    private boolean bundleIsFromPush;
    private boolean bundleIsDeeplink;
    private String bundleFrom;
    private boolean isFirstTime = true;
    /*New changes*/
    private String newsTitle;
    private ArrayList<NewsPhotoInfo> photoList;
    private String[] bullets;
    private String iitlTitle;
    private String gTrackUrl;
    private String providerName;
    private String pubDate;
    private int position;
    private int currentPos = -1;
    private String preStoryId;
    private String providerLogo;
    private RelativeLayout dataLayout;
    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            checkForPauseResumeFragment(position, preStoryId);
            resetBookmark(preStoryId);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };
    private boolean isMarginAllowed;
    private Toolbar toolbar;

    public static Fragment getInstance(Bundle bundle) {
        Fragment fragment = new DivyaCommonRootArticleFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getBundleData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_divya_common_root_article, container, false);
        viewPager = view.findViewById(R.id.viewPager);
        dataLayout = view.findViewById(R.id.data_layout);
        handleToolbar(view);
        sectionPagerAdapter = new SectionPagerAdapter(getChildFragmentManager());
        DivyaCommonFragment divyaCommonFragment = DivyaCommonFragment.getInstance(storyId, colorString, gaScreen, gaArticle, gaEventLabel, displayName, true, position);
        divyaCommonFragment.setFirstTimeData(iitlTitle, newsTitle, bullets, photoList, gTrackUrl, pubDate, providerName, providerLogo);
        divyaCommonFragment.setArticleListener(divyaArticleInteraction);
        sectionPagerAdapter.addFragment(divyaCommonFragment);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(sectionPagerAdapter);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        viewPager.post(() -> onPageChangeListener.onPageSelected(viewPager.getCurrentItem()));
        handleIsHideable();
        return view;
    }

    /**
     * By Saurabh Jain
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private DivyaArticleInteraction divyaArticleInteraction = new DivyaArticleInteraction() {
        @Override
        public void onTagItemClicked() {

        }

        @Override
        public void OnRelatedArticleClick(String storyId, String channel_Slno, int position, String color, String headerName) {
            Intent intent = DivyaCommonArticleActivity.getIntent(getContext(), storyId, headerName, AppFlyerConst.GAScreen.RECOMMENDATION_GA_ARTICLE, gaScreen, displayName, gaEventLabel, color);
            intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, bundleIsFromPush);
            intent.putExtra(Constants.KeyPair.KEY_FROM, bundleFrom);
            intent.putExtra(Constants.KeyPair.KEY_IS_FROM_APPWIDGET, bundleIsFromWidget);
            startActivityForResult(intent, Constants.REQ_CODE_BOTTOM_NAV);
            getActivity().finish();
        }

        @Override
        public void OnRelatedArticleClick(RelatedArticleInfo relatedArticleInfo) {
            Intent intent = DivyaCommonArticleActivity.getIntent(getContext(), relatedArticleInfo, gaScreen, displayName, gaEventLabel);
            intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, bundleIsFromPush);
            intent.putExtra(Constants.KeyPair.KEY_FROM, bundleFrom);
            intent.putExtra(Constants.KeyPair.KEY_IS_FROM_APPWIDGET, bundleIsFromWidget);
            startActivityForResult(intent, Constants.REQ_CODE_BOTTOM_NAV);
            getActivity().finish();
        }

        @Override
        public void onCommentClick(ArticleCommonInfo articleCommonInfo) {
            String host = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
            boolean isLogin = LoginController.loginController().isUserLoggedIn();
//            ModuleUtil.getInstance().launchComment(getContext());
            ModuleUtilAll.Companion.getInstance(getContext()).launchCommentModule(ModuleUtilAll.COMMENT_PAGE, getActivity(), articleCommonInfo.postIdUrl, articleCommonInfo.storyId, title, colorString);
//            startActivity(DivyaCommentActivity.getIntent(getContext(), isLogin, articleCommonInfo.postIdUrl, CommonConstants.CHANNEL_ID, 10, host, articleCommonInfo.storyId, title, colorString));

        }

        @Override
        public void onArticleSuccess(ArticleCommonInfo commonInfo) {
            setHeaderTitle(commonInfo);
        }

        @Override
        public void onShowingNextStory(boolean isAutoSwiped) {
            try {
                if (viewPager != null) {
                    if (!isViewPagerAnimationRunning()) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                    }
                }
            } catch (Exception ignored) {
            }
        }

        @Override
        public void onShowingPreviousStory() {
            try {
                if (viewPager != null && viewPager.getCurrentItem() > 0) {
                    if (!isViewPagerAnimationRunning()) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
                    }
                }
            } catch (Exception ignored) {
            }
        }

        @Override
        public boolean isViewPagerAnimationRunning() {
            return false;
        }

        @Override
        public void addNextArticle(ArticleCommonInfo articleCommonInfo) {
            addFragmentToAdapter(articleCommonInfo);
        }

        @Override
        public void changeStatusBarcolor(String color) {
            if (TextUtils.isEmpty(color)) {
                color = colorString;
            }
            setStatusBarColor(color);
        }

        @Override
        public void showNextStory() {
            int position = viewPager.getCurrentItem();
            if (position < sectionPagerAdapter.getCount() - 1) {
                isClickedNextStoryButton = true;
                viewPager.setCurrentItem(position + 1, true);
            } else {
                AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.sorry_error_found_please_try_again_));
            }
        }


        @Override
        public void onBookmarkClick(ArticleCommonInfo articleCommonInfo) {
            onBookmarkClicked(articleCommonInfo, true);
        }
    };

    private void checkForPauseResumeFragment(int position, String preStoryId) {
        if (viewPager != null) {
            PagerAdapter newsPagerAdapter = viewPager.getAdapter();
            if (newsPagerAdapter != null) {
                try {
                    FragmentLifecycleArticle fragmentToShow = (FragmentLifecycleArticle) newsPagerAdapter.instantiateItem(viewPager, currentPos);
                    fragmentToShow.onPauseFragment();
                } catch (Exception ignored) {
                }
                try {
                    FragmentLifecycleArticle lifecycle = (FragmentLifecycleArticle) newsPagerAdapter.instantiateItem(viewPager, position);
                    lifecycle.onResumeFragment(preStoryId, position);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        currentPos = position;
    }


    private ArticleCommonInfo getCurrentArticleInfoFromViewPager(ViewPager viewPager) {
        ArticleCommonInfo articleCommonInfo = null;
        try {
            Object o = sectionPagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem());
            if (o instanceof DivyaCommonFragment) {
                articleCommonInfo = (((DivyaCommonFragment) o).getCurrentArticleInfo());
            }
        } catch (Exception ignored) {
        }

        return articleCommonInfo;
    }

    public void setHeaderTitle(ArticleCommonInfo articleCommonInfo) {
        if (articleCommonInfo != null) {
            if (!TextUtils.isEmpty(articleCommonInfo.headerName)) {
                preStoryId = articleCommonInfo.storyId;
                headerTitle.setText(articleCommonInfo.headerName);
            } else {
                headerTitle.setText(title + "");
            }
        }
    }

    private void setStatusBarColor(String colorString) {
        if (toolbar != null) {
            int color = AppUtils.getThemeColor(getContext(), colorString);
            toolbar.setBackgroundColor(color);
            AppUtils.getInstance().setStatusBarColor(getActivity(), color);
        }
    }


    public boolean getIsFirstTime() {
        return isFirstTime;
    }

    public void setIsFirstTime(boolean firstTime) {
        isFirstTime = firstTime;
    }


    public void showPreviousStory() {
        int position = viewPager.getCurrentItem();
        if (position >= 1) {
            viewPager.setCurrentItem(position - 1, true);
        }
    }

    public synchronized void addFragmentToAdapter(ArticleCommonInfo articleCommonInfo) {
        if (articleCommonInfo != null) {
            DivyaCommonFragment divyaCommonFragment = DivyaCommonFragment.getInstance(articleCommonInfo.nextArticle.storyId, articleCommonInfo.nextArticle.catColor, gaScreen, gaArticle, gaEventLabel, displayName, false, position);
            divyaCommonFragment.setArticleListener(divyaArticleInteraction);
            sectionPagerAdapter.addFragment(divyaCommonFragment);
        }
    }

    public synchronized void removeFragmentFromAdapter(String storyId) {
        if (storyId != null) {
            int totalCount = sectionPagerAdapter.getCount();
            if (totalCount >= 1) {
                sectionPagerAdapter.removeLastFragment(sectionPagerAdapter.getCount() - 1);
            }
        }

        DivyaCommonFragment divyaCommonFragment = (DivyaCommonFragment) sectionPagerAdapter.getItem(sectionPagerAdapter.getCount() - 1);
        divyaCommonFragment.hideNextStory();
    }

    private void getBundleData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            bundleFrom = bundle.getString(Constants.KeyPair.KEY_FROM);
            if (!TextUtils.isEmpty(bundleFrom)) {
                bundleIsDeeplink = bundleFrom.equalsIgnoreCase(Constants.DeepLinking.DEEPLINK);
            }
            bundleIsFromWidget = bundle.getBoolean(Constants.KeyPair.KEY_IS_FROM_APPWIDGET);
            bundleIsFromPush = bundle.getBoolean(Constants.KeyPair.KEY_IS_NOTIFICATION);
            storyId = bundle.getString(Constants.KeyPair.KEY_STORY_ID);
            title = bundle.getString(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE);
            colorString = bundle.getString(Constants.KeyPair.KEY_COLOR);
            color = AppUtils.getThemeColor(getContext(), colorString);
            gaArticle = bundle.getString(Constants.KeyPair.KEY_GA_ARTICLE);
            gaScreen = bundle.getString(Constants.KeyPair.KEY_GA_SCREEN);
            gaEventLabel = bundle.getString(Constants.KeyPair.KEY_GA_EVENT_LABEL);
            displayName = bundle.getString(Constants.KeyPair.KEY_GA_DISPLAY_NAME);
            position = bundle.getInt(Constants.KeyPair.KEY_POSITION, -1);
            bullets = bundle.getStringArray(Constants.KeyPair.KEY_BULLETS);
            newsTitle = bundle.getString(Constants.KeyPair.KEY_TITLE);
            iitlTitle = bundle.getString(Constants.KeyPair.KEY_IITL_TITLE);
            gTrackUrl = bundle.getString(Constants.KeyPair.KEY_GA_G_TRACK_URL);
            pubDate = bundle.getString(Constants.KeyPair.KEY_PUB_DATE);
            providerName = bundle.getString(Constants.KeyPair.KEY_PROVIDER_NAME);
            providerLogo = bundle.getString(Constants.KeyPair.KEY_PROVIDER_LOGO);

            isMarginAllowed = bundle.getBoolean("isMarginAllowed");

            /*photo array, bullets, newsTitle*/
            String photos = bundle.getString(Constants.KeyPair.KEY_PHOTO_DETAIL);
            try {
                photoList = new ArrayList<>(Arrays.asList(new Gson().fromJson(photos, NewsPhotoInfo[].class)));
            } catch (Exception ignored) {
            }
        }
    }

    private void handleIsHideable() {
        if (isMarginAllowed) {
            TypedValue tv = new TypedValue();
            int actionBarHeight = 0;
            if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) dataLayout.getLayoutParams();
            params.setMargins(0, 0, 0, actionBarHeight);
            dataLayout.setLayoutParams(params);
            dataLayout.requestLayout();
        }
    }

    private void handleToolbar(View view) {
        /*Tool Bar*/
        toolbar = view.findViewById(R.id.toolbar);

        toolbar.setBackgroundColor(color);
        toolbar.setTitle("");
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        ImageView ivBack = view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(v -> getActivity().onBackPressed());
        headerTitle = toolbar.findViewById(R.id.toolbar_title);
//        headerTitle.setText(title);
        AppUtils.getInstance().setStatusBarColor(getActivity(), color);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.article_menu, menu);
        rootMenu = menu;
        resetBookmark(storyId);
        resetFont();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    private synchronized void resetBookmark(String storyID) {
        if (!TextUtils.isEmpty(storyID) && rootMenu != null) {
            boolean isStoryBookmarked = SerializeData.getInstance(getContext()).isBookmarkAvailable(AppUtils.getFilteredStoryId(storyID));
            if (isStoryBookmarked) {
                rootMenu.findItem(R.id.action_bookmark).setChecked(true);
                rootMenu.findItem(R.id.action_bookmark).setIcon(R.drawable.ic_appbar_bookmark_filled);
            } else {
                rootMenu.findItem(R.id.action_bookmark).setChecked(false);
                rootMenu.findItem(R.id.action_bookmark).setIcon(R.drawable.ic_appbar_bookmark);
            }
        }
    }


    private void resetFont() {
        if (rootMenu != null) {
            int lastFontSize = AppPreferences.getInstance(getContext()).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
            if (lastFontSize == Constants.Font.MEDIUM) {
                rootMenu.findItem(R.id.action_font).setIcon(R.drawable.arc_small_font);
            } else {
                rootMenu.findItem(R.id.action_font).setIcon(R.drawable.arc_large_font);
            }
        }
    }

    private void onBookmarkClicked(ArticleCommonInfo articleCommonInfo, boolean fromDetail) {
        if (articleCommonInfo != null) {
            boolean isBookMarked = SerializeData.getInstance(getContext()).isBookmarkAvailable(AppUtils.getFilteredStoryId(articleCommonInfo.storyId));
            if (!isBookMarked) {

                // Tracking
                String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.BOOKMARK, articleCommonInfo.gTrackUrl, campaign);
                CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.BOOKMARKED, CTConstant.PROP_NAME.CONTENT_URL, articleCommonInfo.gTrackUrl, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.BOOKMARKED_THROUGH, fromDetail ? CTConstant.PROP_VALUE.STORY_DETAIL : CTConstant.PROP_VALUE.THREE_DOTS));

                BookmarkSerializedListInfo bookmarkSerializedListInfo = new BookmarkSerializedListInfo();
                bookmarkSerializedListInfo.setTitle(articleCommonInfo.title);
                bookmarkSerializedListInfo.setImage(articleCommonInfo.image);
                bookmarkSerializedListInfo.setIitlTitle(articleCommonInfo.iitlTitle);
                bookmarkSerializedListInfo.setColor(colorString);
                bookmarkSerializedListInfo.setStoryId(AppUtils.getFilteredStoryId(articleCommonInfo.storyId));
                bookmarkSerializedListInfo.setSlugIntro(articleCommonInfo.slugIntro);
                bookmarkSerializedListInfo.setVideoFlag(2);
                bookmarkSerializedListInfo.setContent(new Gson().toJson(articleCommonInfo));
                bookmarkSerializedListInfo.setDetailUrl(Urls.DEFAULT_DETAIL_URL);
                bookmarkSerializedListInfo.setGaScreen(gaScreen);
                bookmarkSerializedListInfo.setGaArticle(gaArticle);
                bookmarkSerializedListInfo.setGaEventLabel(gaEventLabel);
                SerializeData.getInstance(getContext()).saveBookmark(bookmarkSerializedListInfo);
                AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.bookmark_added));

            } else {
                AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.bookmark_already_added));
            }
            refreshFragment(articleCommonInfo);
            resetBookmark(articleCommonInfo.storyId);
        }


    }

    private void refreshFragment(ArticleCommonInfo articleCommonInfo) {
        DivyaCommonFragment divyaCommonFragment = (DivyaCommonFragment) sectionPagerAdapter.getItem(viewPager.getCurrentItem());
        if (divyaCommonFragment != null && articleCommonInfo != null) {
            divyaCommonFragment.refreshBookMark(articleCommonInfo);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ArticleCommonInfo articleCommonInfo = getCurrentArticleInfoFromViewPager(viewPager);
        if (articleCommonInfo == null)
            return true;
        switch (item.getItemId()) {
            case R.id.action_bookmark:
                onBookmarkClicked(articleCommonInfo, true);
                break;
            case R.id.action_share:
                onShare(articleCommonInfo);
                break;
            case R.id.action_font:
                onFontSizeChange();
                break;
        }
        return true;
    }

    private void onFontSizeChange() {
        int lastFontSize = AppPreferences.getInstance(getContext()).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
        if (lastFontSize == Constants.Font.MEDIUM) {
            AppPreferences.getInstance(getContext()).setIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.LARGE);
        } else {
            AppPreferences.getInstance(getContext()).setIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
        }

        resetFont();
        List<Fragment> fragments = sectionPagerAdapter.getVisibleFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof DivyaCommonFragment) {
                    ((DivyaCommonFragment) fragment).changeFontSize(true);
                }
            }
        }
        // Tracking
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        String value = (lastFontSize == Constants.Font.MEDIUM ? AppFlyerConst.GALabel.LARGE : AppFlyerConst.GALabel.MEDIUM);
        Tracking.trackGAEvent(getContext(), ((InitApplication) getContext().getApplicationContext()).getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.FONT, value, campaign);
    }

    private void onShare(ArticleCommonInfo articleCommonInfo) {
        if (!TextUtils.isEmpty(articleCommonInfo.shareLink)) {
            AppUtils.getInstance().shareArticle(getContext(), articleCommonInfo.shareLink, articleCommonInfo.title, articleCommonInfo.gTrackUrl, true);
        } else {
            BackgroundRequest.getStoryShareUrl(getContext(), articleCommonInfo.link, (flag, storyShareUrl) -> {
                if (flag) {
//                    articleCommonInfo.shareLink = storyShareUrl;
                }
                AppUtils.getInstance().shareArticle(getContext(), storyShareUrl, articleCommonInfo.title, articleCommonInfo.gTrackUrl, true);
            });

        }

        //tracking shifted
//        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
//        Tracking.trackGAEvent(InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.SHARE, articleCommonInfo.gTrackUrl, campaign);
    }


    @Override
    public void onDestroyView() {
        try {
            PagerAdapter newsPagerAdapter = viewPager.getAdapter();
            FragmentLifecycleArticle fragmentToShow = (FragmentLifecycleArticle) newsPagerAdapter.instantiateItem(viewPager, currentPos);
            fragmentToShow.onPauseFragment();
        } catch (Exception ignored) {
        }

        super.onDestroyView();
    }


    class SectionPagerAdapter extends FragmentPagerAdapter {
        Vector<DivyaCommonFragment> fragmentList;

        SectionPagerAdapter(FragmentManager fm) {
            super(fm);
            fragmentList = new Vector<>();
        }

        void addFragment(DivyaCommonFragment fragment) {
            fragmentList.add(fragment);
            notifyDataSetChanged();
        }

        void removeLastFragment(int position) {
            fragmentList.remove(position);
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return super.getPageTitle(position);
        }

        List<Fragment> getVisibleFragments() {
            return getChildFragmentManager().getFragments();
        }
    }

    @Override
    public boolean isLayoutTypeVertical() {
        return false;
    }
}
