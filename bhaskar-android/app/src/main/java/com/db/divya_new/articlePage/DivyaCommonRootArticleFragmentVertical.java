package com.db.divya_new.articlePage;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.bhaskar.util.LoginController;
import com.bhaskar.util.ModuleUtilAll;
import com.db.InitApplication;
import com.db.data.models.BookmarkSerializedListInfo;
import com.db.data.models.NewsPhotoInfo;
import com.db.data.models.RelatedArticleInfo;
import com.db.data.source.remote.SerializeData;
import com.db.data.source.server.BackgroundRequest;
import com.db.listeners.FragmentLifecycleArticle;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import static com.db.divya_new.articlePage.DivyaCommonFragmentVertical.TUTORIAL_DIALOG;


public class DivyaCommonRootArticleFragmentVertical extends BaseArticleFragment {

    private final String TAG = DivyaCommonRootArticleFragmentVertical.class.getSimpleName();
    private int currentPos = -1;

    /*Views*/
    private ViewPager2 viewPager;
    private Menu rootMenu;
    /*Adapter*/
    private SectionPagerAdapter sectionPagerAdapter;
    /*Variables*/

    private TextView headerTitle;
    private String colorString;
    private String gaScreen;
    private String displayName;
    private String gaArticle;
    private String gaEventLabel;
    private String storyId;
    private String title;
    private boolean bundleIsFromWidget;
    private boolean bundleIsFromPush;
    private boolean bundleIsDeeplink;
    private String bundleFrom;
    private boolean isFirstTime = true;
    /*New changes*/
    private String newsTitle;
    private ArrayList<NewsPhotoInfo> photoList;
    private String[] bullets;
    private String iitlTitle;
    private String gTrackUrl;
    private String providerName;
    private String pubDate;
    private int position;
    private String preStoryId;
    private String providerLogo;
    private ArticleCommonInfo articleCommonInfo;
    private Toolbar toolbar;

    private RelativeLayout dataLayout;

    public static Fragment getInstance(Bundle bundle) {
        Fragment fragment = new DivyaCommonRootArticleFragmentVertical();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getBundleData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_divya_common_article_vertical_new, container, false);
        handleToolbar(view);

        viewPager = view.findViewById(R.id.viewPager);

        dataLayout = view.findViewById(R.id.container_news_detail_pager);
        sectionPagerAdapter = new SectionPagerAdapter(getChildFragmentManager(), lifecycle);
        DivyaCommonFragmentVertical divyaCommonFragment = DivyaCommonFragmentVertical.getInstance(storyId, colorString, gaScreen, gaArticle, gaEventLabel, displayName, true, position);
        divyaCommonFragment.setFirstTimeData(iitlTitle, newsTitle, bullets, photoList, gTrackUrl, pubDate, providerName, providerLogo);
        divyaCommonFragment.setArticleListener(divyaArticleInteraction);

        viewPager.setOffscreenPageLimit(1);

        viewPager.registerOnPageChangeCallback(onPageChangeCallback);
        viewPager.setUserInputEnabled(false);
        viewPager.setOrientation(ViewPager2.ORIENTATION_VERTICAL);

        sectionPagerAdapter.addFragment(divyaCommonFragment);
        viewPager.setAdapter(sectionPagerAdapter);
        viewPager.post(() -> onPageChangeCallback.onPageSelected(viewPager.getCurrentItem()));

        return view;
    }


    private void handleToolbar(View view) {
        /*Tool Bar*/
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(AppUtils.getThemeColor(getContext(), colorString));
        toolbar.setTitle("");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ImageView ivBack = view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(v -> getActivity().onBackPressed());
        setMenuVisibility(true);
        headerTitle = toolbar.findViewById(R.id.toolbar_title);
//        headerTitle.setText(title);
        AppUtils.getInstance().setStatusBarColor(getActivity(), AppUtils.getThemeColor(getContext(), colorString));
    }


    private DivyaArticleInteraction divyaArticleInteraction = new DivyaArticleInteraction() {


        @Override
        public void onTagItemClicked() {

        }

        @Override
        public void OnRelatedArticleClick(String storyId, String channel_Slno, int position, String color, String headerName) {
            Intent intent = DivyaCommonArticleActivity.getIntent(getContext(), storyId, headerName, AppFlyerConst.GAScreen.RECOMMENDATION_GA_ARTICLE, gaScreen, displayName, gaEventLabel, color);
            intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, bundleIsFromPush);
            intent.putExtra(Constants.KeyPair.KEY_FROM, bundleFrom);
            intent.putExtra(Constants.KeyPair.KEY_IS_FROM_APPWIDGET, bundleIsFromWidget);
            getActivity().startActivityForResult(intent, Constants.REQ_CODE_BOTTOM_NAV);
            getActivity().finish();

        }

        @Override
        public void OnRelatedArticleClick(RelatedArticleInfo relatedArticleInfo) {
            Intent intent = DivyaCommonArticleActivity.getIntent(getContext(), relatedArticleInfo, gaScreen, displayName, gaEventLabel);
            intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, bundleIsFromPush);
            intent.putExtra(Constants.KeyPair.KEY_FROM, bundleFrom);
            intent.putExtra(Constants.KeyPair.KEY_IS_FROM_APPWIDGET, bundleIsFromWidget);
            getActivity().startActivityForResult(intent, Constants.REQ_CODE_BOTTOM_NAV);
            getActivity().finish();
        }

        @Override
        public void onCommentClick(ArticleCommonInfo articleCommonInfo) {
            String host = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
            boolean isLogin = LoginController.loginController().isUserLoggedIn();
//            startActivity(DivyaCommentActivity.getIntent(getContext(), isLogin, articleCommonInfo.postIdUrl, CommonConstants.CHANNEL_ID, 10, host, articleCommonInfo.storyId, title, colorString));
//            ModuleUtil.getInstance().launchComment(getContext());
            ModuleUtilAll.Companion.getInstance(getContext()).launchCommentModule(ModuleUtilAll.COMMENT_PAGE, getActivity(), articleCommonInfo.postIdUrl, articleCommonInfo.storyId, title, colorString);
        }

        @Override
        public void onArticleSuccess(ArticleCommonInfo commonInfo) {
            if (commonInfo != null) {
                articleCommonInfo = commonInfo;
                setHeaderTitle(articleCommonInfo);
            }

        }

        @Override
        public void onShowingNextStory(boolean isAutoSwiped) {
            try {
                if (viewPager != null) {
                    if (!isViewPagerAnimationRunning()) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                    }
                }
            } catch (Exception ignored) {
            }
        }

        @Override
        public void onShowingPreviousStory() {
            try {
                if (viewPager != null && viewPager.getCurrentItem() > 0) {
                    if (!isViewPagerAnimationRunning()) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
                    }
                }
            } catch (Exception ignored) {
            }
        }

        @Override
        public boolean isViewPagerAnimationRunning() {
            return false;
        }

        @Override
        public void addNextArticle(ArticleCommonInfo articleCommonInfo) {
            addFragmentToAdapter(articleCommonInfo);
        }

        @Override
        public void changeStatusBarcolor(String color) {
            if (TextUtils.isEmpty(color)) {
                color = colorString;
            }
            setToolbarStatusBarColor(color);
        }

        @Override
        public void showNextStory() {

        }


        @Override
        public void onBookmarkClick(ArticleCommonInfo articleCommonInfo) {
            onBookmarkClicked(articleCommonInfo, true);
        }
    };

    private void setToolbarStatusBarColor(String colorString) {
        if (toolbar != null) {
            int color = AppUtils.getThemeColor(getContext(), colorString);
            toolbar.setBackgroundColor(color);
            AppUtils.getInstance().setStatusBarColor(getActivity(), color);
        }
    }

    private void getBundleData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            bundleFrom = bundle.getString(Constants.KeyPair.KEY_FROM);
            if (!TextUtils.isEmpty(bundleFrom)) {
                bundleIsDeeplink = bundleFrom.equalsIgnoreCase(Constants.DeepLinking.DEEPLINK);
            }
            bundleIsFromWidget = bundle.getBoolean(Constants.KeyPair.KEY_IS_FROM_APPWIDGET);
            bundleIsFromPush = bundle.getBoolean(Constants.KeyPair.KEY_IS_NOTIFICATION);
            storyId = bundle.getString(Constants.KeyPair.KEY_STORY_ID);
            title = bundle.getString(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE);
            colorString = bundle.getString(Constants.KeyPair.KEY_COLOR);
            gaArticle = bundle.getString(Constants.KeyPair.KEY_GA_ARTICLE);
            gaScreen = bundle.getString(Constants.KeyPair.KEY_GA_SCREEN);
            gaEventLabel = bundle.getString(Constants.KeyPair.KEY_GA_EVENT_LABEL);
            displayName = bundle.getString(Constants.KeyPair.KEY_GA_DISPLAY_NAME);
            position = bundle.getInt(Constants.KeyPair.KEY_POSITION, -1);
            bullets = bundle.getStringArray(Constants.KeyPair.KEY_BULLETS);
            newsTitle = bundle.getString(Constants.KeyPair.KEY_TITLE);
            iitlTitle = bundle.getString(Constants.KeyPair.KEY_IITL_TITLE);
            gTrackUrl = bundle.getString(Constants.KeyPair.KEY_GA_G_TRACK_URL);
            pubDate = bundle.getString(Constants.KeyPair.KEY_PUB_DATE);
            providerName = bundle.getString(Constants.KeyPair.KEY_PROVIDER_NAME);
            providerLogo = bundle.getString(Constants.KeyPair.KEY_PROVIDER_LOGO);

            /*photo array, bullets, newsTitle*/
            String photos = bundle.getString(Constants.KeyPair.KEY_PHOTO_DETAIL);
            try {
                photoList = new ArrayList<>(Arrays.asList(new Gson().fromJson(photos, NewsPhotoInfo[].class)));
            } catch (Exception ignored) {
            }
        }
    }


    private int previousPosition = -1;
    private ViewPager2.OnPageChangeCallback onPageChangeCallback = new ViewPager2.OnPageChangeCallback() {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            if (previousPosition != position) {
                checkForPauseResumeFragment(position, preStoryId);
                if (position >= 2 && !isNewsDetailTutorial_V_Shown()) {
                    new Handler().postDelayed(() -> {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            showSettingTutorialDialog();
                        }
                    }, 500);
                }
            }
            previousPosition = position;
        }


    };

    private void checkForPauseResumeFragment(int position, String preStoryId) {
        if (viewPager != null) {
            try {
                SectionPagerAdapter newsPagerAdapter = (SectionPagerAdapter) viewPager.getAdapter();
                if (newsPagerAdapter != null) {
                    try {
                        FragmentLifecycleArticle fragmentToShow = (FragmentLifecycleArticle) newsPagerAdapter.getCurrentFragment(currentPos);
                        fragmentToShow.onPauseFragment();
                    } catch (Exception ignored) {
                    }
                    try {
                        FragmentLifecycleArticle lifecycle = (FragmentLifecycleArticle) newsPagerAdapter.getCurrentFragment(position);
                        lifecycle.onResumeFragment(preStoryId, position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception ignore) {
            }
        }
        currentPos = position;
    }


    public synchronized void removeFragmentFromAdapter(String storyId) {
        if (storyId != null) {
            int totalCount = sectionPagerAdapter.getItemCount();
            if (totalCount >= 1) {
                sectionPagerAdapter.removeLastFragment(sectionPagerAdapter.getItemCount() - 1);
            }
        }
    }

    private Lifecycle lifecycle = new Lifecycle() {
        @Override
        public void addObserver(@NonNull LifecycleObserver observer) {
        }

        @Override
        public void removeObserver(@NonNull LifecycleObserver observer) {
        }

        @NonNull
        @Override
        public State getCurrentState() {
            return null;
        }
    };


    public void setHeaderTitle(ArticleCommonInfo articleCommonInfo) {
        if (articleCommonInfo != null) {
            if (!TextUtils.isEmpty(articleCommonInfo.headerName)) {
                preStoryId = articleCommonInfo.storyId;
                headerTitle.setText(articleCommonInfo.headerName);
            } else {
                headerTitle.setText(title + "");
            }
        }
    }

    public boolean getIsFirstTime() {
        return isFirstTime;
    }

    public void setIsFirstTime(boolean firstTime) {
        isFirstTime = firstTime;
    }

    public void showNextStory() {
        int position = viewPager.getCurrentItem();
        if (position < sectionPagerAdapter.getItemCount() - 1) {
            boolean isClickedNextStoryButton = true;
            viewPager.setCurrentItem(position + 1, true);
        } else {
            AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.sorry_error_found_please_try_again_));
        }
    }

    public void showPreviousStory() {
        int position = viewPager.getCurrentItem();
        if (position >= 1) {
            viewPager.setCurrentItem(position - 1, true);
        }
    }

    public synchronized void addFragmentToAdapter(ArticleCommonInfo articleCommonInfo) {
        if (articleCommonInfo != null) {
            DivyaCommonFragmentVertical divyaCommonFragmentVertical = DivyaCommonFragmentVertical.getInstance((articleCommonInfo.nextArticle.storyId), articleCommonInfo.nextArticle.catColor, gaScreen, gaArticle, gaEventLabel, displayName, false, position);
            divyaCommonFragmentVertical.setArticleListener(divyaArticleInteraction);
            sectionPagerAdapter.addFragment(divyaCommonFragmentVertical);
        }
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.article_menu_verticle, menu);
        rootMenu = menu;
        resetFont();
        super.onCreateOptionsMenu(menu, inflater);
    }


    private void resetFont() {
        int lastFontSize = AppPreferences.getInstance(getContext()).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
        if (lastFontSize == Constants.Font.MEDIUM) {
            rootMenu.findItem(R.id.action_font).setIcon(R.drawable.arc_small_font);
        } else {
            rootMenu.findItem(R.id.action_font).setIcon(R.drawable.arc_large_font);
        }
    }

    private void onBookmarkClicked(ArticleCommonInfo articleCommonInfo, boolean fromDetail) {
        if (articleCommonInfo != null) {
            boolean isBookMarked = SerializeData.getInstance(getContext()).isBookmarkAvailable(AppUtils.getFilteredStoryId(articleCommonInfo.storyId));
            if (!isBookMarked) {

                // Tracking
                String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.BOOKMARK, articleCommonInfo.gTrackUrl, campaign);

                CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.BOOKMARKED, CTConstant.PROP_NAME.CONTENT_URL, articleCommonInfo.gTrackUrl, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.BOOKMARKED_THROUGH, fromDetail ? CTConstant.PROP_VALUE.STORY_DETAIL : CTConstant.PROP_VALUE.THREE_DOTS));

                BookmarkSerializedListInfo bookmarkSerializedListInfo = new BookmarkSerializedListInfo();
                bookmarkSerializedListInfo.setTitle(articleCommonInfo.title);
                bookmarkSerializedListInfo.setImage(articleCommonInfo.image);
                bookmarkSerializedListInfo.setIitlTitle(articleCommonInfo.iitlTitle);
                bookmarkSerializedListInfo.setColor(colorString);
                bookmarkSerializedListInfo.setStoryId(AppUtils.getFilteredStoryId(articleCommonInfo.storyId));
//                bookmarkSerializedListInfo.setChannelSlno(CommonConstants.CHANNEL_ID);
                bookmarkSerializedListInfo.setSlugIntro(articleCommonInfo.slugIntro);
                bookmarkSerializedListInfo.setVideoFlag(2);
                bookmarkSerializedListInfo.setContent(new Gson().toJson(articleCommonInfo));
                bookmarkSerializedListInfo.setDetailUrl(Urls.DEFAULT_DETAIL_URL);
                bookmarkSerializedListInfo.setGaScreen(gaScreen);
                bookmarkSerializedListInfo.setGaArticle(gaArticle);
                bookmarkSerializedListInfo.setGaEventLabel(gaEventLabel);
                SerializeData.getInstance(getContext()).saveBookmark(bookmarkSerializedListInfo);
                AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.bookmark_added));
            } else {
                AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.bookmark_already_added));
            }
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (articleCommonInfo == null)
            return true;
        switch (item.getItemId()) {
            case R.id.action_share:
                if (articleCommonInfo != null) {
                    onShare(articleCommonInfo);
                }
                break;
            case R.id.action_font:
                onFontSizeChange();
                break;
        }
        return true;
    }

    private void onFontSizeChange() {
        int lastFontSize = AppPreferences.getInstance(getContext()).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
        if (lastFontSize == Constants.Font.MEDIUM) {
            AppPreferences.getInstance(getContext()).setIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.LARGE);
        } else {
            AppPreferences.getInstance(getContext()).setIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
        }

        resetFont();
        List<Fragment> fragments = sectionPagerAdapter.getVisibleFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof DivyaCommonFragmentVertical) {
                    ((DivyaCommonFragmentVertical) fragment).changeFontSize(true);
                }
            }
        }
        // Tracking
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        String value = (lastFontSize == Constants.Font.MEDIUM ? AppFlyerConst.GALabel.LARGE : AppFlyerConst.GALabel.MEDIUM);
        Tracking.trackGAEvent(getContext(), ((InitApplication) getContext().getApplicationContext()).getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.FONT, value, campaign);
    }

    private void onShare(ArticleCommonInfo articleCommonInfo) {
        if (!TextUtils.isEmpty(articleCommonInfo.shareLink)) {
            AppUtils.getInstance().shareArticle(getContext(), articleCommonInfo.shareLink, articleCommonInfo.title, articleCommonInfo.gTrackUrl, true);
        } else {
            BackgroundRequest.getStoryShareUrl(getContext(), articleCommonInfo.link, (flag, storyShareUrl) -> {
                if (flag) {
//                    articleCommonInfo.shareLink = storyShareUrl;
                }
                AppUtils.getInstance().shareArticle(getContext(), storyShareUrl, articleCommonInfo.title, articleCommonInfo.gTrackUrl, true);
            });

        }

    }


//    @Override
//    public void onBackPressed() {
//        int fSize = getSupportFragmentManager().getBackStackEntryCount();
//        if (fSize >= 1) {
//            getSupportFragmentManager().popBackStack();
//            if (rootMenu != null) {
//                rootMenu.setGroupVisible(R.id.article_option_menu, true);
//            }
//        } else {
//            if (bundleIsFromWidget) {
//                Intent intent = new Intent(getContext(), MainActivity.class);
////                intent.putExtra(Constants.KeyPair.KEY_FRAGMENT_VALUE, MainActivity.NEWS_FRAGMENT);
//                startActivity(intent);
//            } else if ((bundleFrom != null && bundleIsDeeplink) || bundleIsFromPush) {
//                Intent intent = new Intent(getContext(), MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//                finish();
//            }
//            finish();
//        }
//    }


    @Override
    public void onDestroyView() {
        try {
            viewPager.unregisterOnPageChangeCallback(onPageChangeCallback);
        } catch (Exception ignored) {
        }
        super.onDestroyView();

    }

    private void showSettingTutorialDialog() {
        NewsDetailTutorialDialog newsDetailTutorialDialog = new NewsDetailTutorialDialog(getContext(),
                NewsDetailTutorialDialog.TutorialType.SETTINGS);

        newsDetailTutorialDialog.show();
        newsDetailTutorialDialog.setOnDismissListener(dialog -> saveNewsDetailTutorial_V_Shown());
    }

    private void saveNewsDetailTutorial_V_Shown() {
        AppPreferences.getInstance(getContext()).setBooleanValue(TUTORIAL_DIALOG, true);
    }

    private boolean isNewsDetailTutorial_V_Shown() {
        return AppPreferences.getInstance(getContext()).getBooleanValue(TUTORIAL_DIALOG, false);
    }

    class SectionPagerAdapter extends FragmentStateAdapter {
        Vector<DivyaCommonFragmentVertical> fragmentList;

        SectionPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
            super(fragmentManager, lifecycle);
            fragmentList = new Vector<>();
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            return fragmentList.get(position);
        }

        void addFragment(DivyaCommonFragmentVertical fragment) {
            fragmentList.add(fragment);
            notifyDataSetChanged();
        }

        void removeLastFragment(int position) {
            fragmentList.remove(position);
            notifyDataSetChanged();
        }


        List<Fragment> getVisibleFragments() {
            return getChildFragmentManager().getFragments();
        }

        @Override
        public int getItemCount() {
            return fragmentList.size();
        }

        Fragment getCurrentFragment(int position) {
            if (position >= 0 && position < fragmentList.size())
                return fragmentList.get(position);

            return null;
        }
    }

    @Override
    public boolean isLayoutTypeVertical() {
        return true;
    }
}
