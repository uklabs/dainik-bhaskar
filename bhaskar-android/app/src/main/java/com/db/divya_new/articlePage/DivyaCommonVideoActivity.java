package com.db.divya_new.articlePage;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.ImageView;

import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdsConstants;
import com.db.data.models.NewsPhotoInfo;
import com.db.dbvideo.player.AppTraverseUtil;
import com.db.dbvideo.player.BhaskarVideoPlayerTrackerUtil;
import com.db.dbvideo.player.PlayerControllerCallbackReference;
import com.db.dbvideo.player.VideoPlayerConstant;
import com.db.dbvideo.player.VideoPlayerEventTracker;
import com.db.dbvideo.videoview.DeviceOrientationListener;
import com.db.dbvideo.videoview.VideoPlayerCallbackListener;
import com.db.dbvideo.videoview.VideoPlayerFragment;
import com.db.dbvideoPersonalized.OnVideoSelectedListener;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.DailyMotionPlayerFragment;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.db.util.AppPreferences;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.ThemeUtil;

public class DivyaCommonVideoActivity extends BaseAppCompatActivity implements OnVideoSelectedListener, DeviceOrientationListener, VideoPlayerCallbackListener {

    public static final String EXTRA_VIDEO_INFO = "db_video_info";

    private VideoPlayerFragment videoPlayerFragment;
    private DailyMotionPlayerFragment dailyMotionPlayerFragment;

    private Handler handler = new Handler();

    public NewsPhotoInfo selectedVideoInfo;
    private String sectionLabel;
    private String videoSource;
    private int videoIndex;
    private String gaEventLabel;
    private String gaArticle;

    private OrientationEventListener orientation;
    private int userSelectedOrientation = -1;
    private boolean clickViaNotification = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_article_video);

        initViews();
    }


    public static Intent getIntent(Context context, NewsPhotoInfo photo, String sectionLabel, String videoSource, int videoIndex, String gaEventLabel, String gaArticle) {
        Intent intent = new Intent(context, DivyaCommonVideoActivity.class);
        intent.putExtra(EXTRA_VIDEO_INFO, photo);
        intent.putExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL, sectionLabel);
        intent.putExtra(Constants.KeyPair.KEY_SOURCE, videoSource);
        intent.putExtra(Constants.KeyPair.KEY_POSITION, videoIndex);
        intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
        intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        return intent;
    }

    private void initViews() {
        // fetch data from intent
        Intent intent = getIntent();
        if (intent != null) {
            Bundle extraBundle = intent.getExtras();
            if (extraBundle != null) {
                try {
                    if (extraBundle.containsKey(EXTRA_VIDEO_INFO)) {
                        selectedVideoInfo = (NewsPhotoInfo) intent.getExtras().getSerializable(EXTRA_VIDEO_INFO);
                        sectionLabel = intent.getStringExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL);
                    }
                    videoSource = extraBundle.getString(Constants.KeyPair.KEY_SOURCE);
                    videoIndex = extraBundle.getInt(Constants.KeyPair.KEY_POSITION);
                    gaEventLabel = extraBundle.getString(Constants.KeyPair.KEY_GA_EVENT_LABEL);
                    gaArticle = extraBundle.getString(Constants.KeyPair.KEY_GA_ARTICLE);
                    clickViaNotification = extraBundle.getBoolean(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION);
                } catch (Exception e) {
                }
            }
        }

        if (selectedVideoInfo != null) {
            // add video fragment
            String title = (TextUtils.isEmpty(selectedVideoInfo.title) ? selectedVideoInfo.articleTitle : selectedVideoInfo.title);
            addVideoFragment(selectedVideoInfo.internalVideo, selectedVideoInfo.dailyMotionVideoId, selectedVideoInfo.image, title, selectedVideoInfo.link, AdsConstants.getIntFromString(""));
        }

        orientation = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int i) {

                if (Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1) {

                    if ((i > 85 && i < 95) || (i > 265 && i < 275)) {
                        if (userSelectedOrientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                            if (getResources().getConfiguration().orientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                            }
                            userSelectedOrientation = -1;
                        }

                    } else if ((i > 350 && i <= 360) || (i > 170 && i < 190)) {
                        if (userSelectedOrientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                            if (getResources().getConfiguration().orientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                            }
                            userSelectedOrientation = -1;
                        }
                    }
                }
            }
        };
    }

    private void addVideoFragment(String videoUrl, String dailyMotionVideoId, String imageUrl, String title, String shareLink, int vwallbrandid) {
        if (!TextUtils.isEmpty(dailyMotionVideoId)) {
            addDailyMotionVideoFragment(dailyMotionVideoId, title, shareLink);
        } else {
            addVideoPlayerFragment(videoUrl, imageUrl, title, shareLink, vwallbrandid);
        }
    }

    private void addVideoPlayerFragment(String videoUrl, String imageUrl, String title, String shareLink, int vwallbrandid) {
        videoPlayerFragment = new VideoPlayerFragment();

        Bundle bundle = new Bundle();
//        VideosAdsControl videoAdsControl = JsonParser.getVideoAdsControl(this);
//        videoAdsControl.setActive(AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.AdPref.VIDEO_TOGGLE, false));
//        videoAdsControl.setFromArticle(false);
//        videoAdsControl.setImavasturl(String.format((videoAdsControl.getImavasturl() + Constants.VIDEO_AD_URL_EXTRA), videoUrl, title, AdsConstants.getContentUrl(shareLink), String.valueOf(new Date().getTime())));
//        videoAdsControl.setContent_url(videoUrl);
//        bundle.putParcelable(VideoPlayerConstant.AD_CONTROL, videoAdsControl);

        bundle.putString(VideoPlayerConstant.VIDEO_URL, videoUrl);
        bundle.putInt(VideoPlayerConstant.VWALLBRANDID, vwallbrandid);
        bundle.putInt(VideoPlayerConstant.DEFAULT_THUMBNAIL_IMAGE_RES_ID, R.drawable.water_mark_news_detail);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NATIVE_CONTROLS, false);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NEXT_BUTTON, false);
        bundle.putBoolean(VideoPlayerConstant.HIDE_SHARE_BUTTON, false);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_PLAY, true);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_NEXT_PLAY, false);
        bundle.putString(VideoPlayerConstant.SHARE_TITLE, title);
        bundle.putString(VideoPlayerConstant.SHARE_LINK, shareLink);
        bundle.putString(Constants.KeyPair.KEY_GA_G_TRACK_URL, "");
//        bundle.putString(Constants.KeyPair.KEY_GA_G_TRACK_URL, selectedVideoInfo.gTrackUrl);

        videoPlayerFragment.setArguments(bundle);

        setTrackingData(selectedVideoInfo, videoUrl);

        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.videoContainer, videoPlayerFragment).commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }

        PlayerControllerCallbackReference playerControllerCallbackReference = videoPlayerFragment.getPlayerControllerCallbackReference();
        if (playerControllerCallbackReference == null) {
            runHandler(imageUrl);
        }

    }

    private void addDailyMotionVideoFragment(String dailyMotionVideoId, String title, String shareLink) {
        dailyMotionPlayerFragment = new DailyMotionPlayerFragment();

        Bundle bundle = new Bundle();
        bundle.putString(VideoPlayerConstant.VIDEO_DAILY_MOTION_ID, dailyMotionVideoId);
        bundle.putBoolean(VideoPlayerConstant.SHOW_NEXT_BUTTON, false);
        bundle.putBoolean(VideoPlayerConstant.HIDE_SHARE_BUTTON, false);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_PLAY, true);
        bundle.putBoolean(VideoPlayerConstant.IS_AUTO_NEXT_PLAY, false);
        bundle.putString(VideoPlayerConstant.SHARE_TITLE, title);
        bundle.putString(VideoPlayerConstant.SHARE_LINK, shareLink);

        dailyMotionPlayerFragment.setArguments(bundle);

        setTrackingDataForDailyMotion(selectedVideoInfo);

        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.videoContainer, dailyMotionPlayerFragment).commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }

    }

    @Override
    public void onVideoSelected(VideoInfo dbVideosInfo, String source, int position) {
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        } else {
            if (clickViaNotification) {
                Intent intent = new Intent(DivyaCommonVideoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    private void setThumbnailImage(final String imageUrl) {
        if (videoPlayerFragment != null) {
            PlayerControllerCallbackReference playerControllerCallbackReference = videoPlayerFragment.getPlayerControllerCallbackReference();
            if (playerControllerCallbackReference == null) {
                runHandler(imageUrl);
                return;
            }

            final ImageView thumbnailImageView = playerControllerCallbackReference.getThumbnailImageView();
            ImageUtil.setImage(getApplicationContext(), imageUrl, thumbnailImageView, R.drawable.water_mark_news_detail);
        }
    }

    private void runHandler(final String imageUrl) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setThumbnailImage(imageUrl);
            }
        }, 500);
    }

    @Override
    public void changeOrientation() {
        if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            userSelectedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            userSelectedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (videoPlayerFragment != null) {
            videoPlayerFragment.changeVideoViewScreen();
        }

        if (dailyMotionPlayerFragment != null) {
            dailyMotionPlayerFragment.changeVideoOrientation();
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (videoPlayerFragment != null) {
                    videoPlayerFragment.redrawControls();
                }
            }
        }, 500);

        hideUnhideDetailLayout();
    }

    private void hideUnhideDetailLayout() {
        int orientation = getResources().getConfiguration().orientation;
        switch (orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                showVideoDetailView();
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                hideVideoDetailView();
                break;
        }
    }

    private void hideVideoDetailView() {
        hideStatusBar();

        if (videoPlayerFragment != null && videoPlayerFragment.mainController != null) {
            videoPlayerFragment.mainController.changeScreenSwitcherIcon(R.drawable.ic_video_fullscreen_exit);
        }
    }

    private void showVideoDetailView() {
        showStatusBar();

        if (videoPlayerFragment != null && videoPlayerFragment.mainController != null) {
            videoPlayerFragment.mainController.changeScreenSwitcherIcon(R.drawable.ic_video_fullscreen);
        }
    }

    @Override
    public void onVideoStart() {
    }

    @Override
    public void onVideoComplete() {
        return;
    }

    @Override
    public void onVideoError() {
    }

    @Override
    public void onNextButtonclick() {
    }

    private void setTrackingData(NewsPhotoInfo dbVideosInfo, String videoUrl) {
        if (TextUtils.isEmpty(gaEventLabel)) {
            gaEventLabel = CommonConstants.EVENT_LABEL;
        }
        AppTraverseUtil.setLanguage(gaEventLabel);
        AppTraverseUtil.setVideoTitle(dbVideosInfo.mediaTitle);
        AppTraverseUtil.setStoryId(dbVideosInfo.photoId);
        AppTraverseUtil.setBaseSectionSubSectionValue(sectionLabel);
        AppTraverseUtil.setVideoSource(videoSource);
        AppTraverseUtil.setVideoIndex(videoIndex);

        BhaskarVideoPlayerTrackerUtil.setScreenName(AppTraverseUtil.getScreenNameForVideoTracking());
        String ultimaTrackingUrlWithParameter = dbVideosInfo.ultimaTrackUrl + "&http_referer=" + selectedVideoInfo.link;
        VideoPlayerEventTracker videoPlayerEventTracker = VideoPlayerEventTracker.getInstance(this, videoUrl, ultimaTrackingUrlWithParameter, dbVideosInfo.photoId);
        BhaskarVideoPlayerTrackerUtil.setPlayerEventCallBackReference(videoPlayerEventTracker);

        // Tracking
        String source = AppPreferences.getInstance(DivyaCommonVideoActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(DivyaCommonVideoActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(DivyaCommonVideoActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(DivyaCommonVideoActivity.this, InitApplication.getInstance().getDefaultTracker(), gaArticle + (TextUtils.isEmpty(dbVideosInfo.mediaTitle) ? "" : ("-" + dbVideosInfo.mediaTitle)), source, medium, campaign);
    }

    private void setTrackingDataForDailyMotion(NewsPhotoInfo dbVideosInfo) {
        if (TextUtils.isEmpty(gaEventLabel)) {
            gaEventLabel = CommonConstants.EVENT_LABEL;
        }
        AppTraverseUtil.setLanguage(gaEventLabel);
        AppTraverseUtil.setVideoTitle(dbVideosInfo.mediaTitle);
        AppTraverseUtil.setStoryId(dbVideosInfo.photoId);
        AppTraverseUtil.setBaseSectionSubSectionValue(sectionLabel);
        AppTraverseUtil.setVideoSource(videoSource);
        AppTraverseUtil.setVideoIndex(videoIndex);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // Hide Status Bar
    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);

        } else if (Build.VERSION.SDK_INT >= 16) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    }

    //Show Status Bar
    private void showStatusBar() {
        if (Build.VERSION.SDK_INT >= 16) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (orientation != null) {
            orientation.enable();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (orientation != null) {
            orientation.disable();
        }
    }
}
