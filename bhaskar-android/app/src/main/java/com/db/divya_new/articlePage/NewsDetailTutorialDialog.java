package com.db.divya_new.articlePage;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bhaskar.R;

public final class NewsDetailTutorialDialog extends Dialog {

    private static final String TAG = NewsDetailTutorialDialog.class.getSimpleName();
    private TutorialType tutorialType;

    public NewsDetailTutorialDialog(@NonNull Context context, @NonNull TutorialType tutorialType) {
        super(context, R.style.TransLucentLoader);
        this.tutorialType = tutorialType;
        setCancelable(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (tutorialType == TutorialType.NEXT) {
            setContentView(R.layout.dialog_news_detail_tutorial_1);
            ImageView mIvTutorial = findViewById(R.id.iv_tutorial);
            Glide.with(getContext()).asGif().load(R.drawable.news_brief_tutorial_large).into(mIvTutorial);
            findViewById(R.id.ll_tutorial_parent).setOnClickListener(v -> dismiss());
        } else if (tutorialType == TutorialType.BRIEF) {
            setContentView(R.layout.dialog_news_brief_tutorial);
            ImageView mIvTutorial = findViewById(R.id.iv_tutorial);
            Glide.with(getContext()).asGif().load(R.drawable.news_brief_tutorial_large).into(mIvTutorial);
            findViewById(R.id.ll_tutorial_parent).setOnClickListener(v -> dismiss());
        } else if(tutorialType == TutorialType.SETTINGS){
            setContentView(R.layout.dialog_news_detail_tutorial_settings);
            findViewById(R.id.btnOk).setOnClickListener(v->dismiss());
        }else {
            setContentView(R.layout.dialog_news_detail_tutorial_2);
            ImageView mIvTutorial = findViewById(R.id.iv_tutorial);
            if(mIvTutorial!=null)
                Glide.with(getContext()).asGif().load(R.drawable.news_brief_tutorial_large).into(mIvTutorial);
            findViewById(R.id.ll_tutorial_parent).setOnClickListener(v -> dismiss());
        }
    }



    public enum TutorialType {
        NEXT, PREVIOUS, BRIEF, SETTINGS
    }


}
