package com.db.divya_new.articlePage;

public interface OnResponseCallBack {
    void setResponse(int position, ArticleCommonInfo articleCommonInfo);

    void nextArticleSwipe(int position);

    void setNextArticle(int position, String storyId);
}
