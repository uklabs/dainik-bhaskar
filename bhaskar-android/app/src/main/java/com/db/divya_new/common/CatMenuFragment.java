package com.db.divya_new.common;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.util.Action;
import com.db.InitApplication;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.divyashree.CatHomeFragment;
import com.db.home.ActivityUtil;
import com.db.listeners.FragmentLifecycle;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.Systr;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/***
 * Params : CategoryInfo (for action Cat_Menu)
 **/
public class CatMenuFragment extends Fragment implements CatMenuTabListAdapter.OnTabClickLitener, FragmentLifecycle, OnMoveTopListener {

    private CategoryInfo mCategoryInfo;
    private CatMenuTabListAdapter tabListAdapter;
    private String mParentId;

    private boolean isFragmentShowing;
    private String prefixSectionName;
    private ArrayList<String> previousTrackList = new ArrayList<>();

    public static CatMenuFragment newInstance(String parentId, CategoryInfo categoryInfo, String prefixSectionName) {
        CatMenuFragment fragment = new CatMenuFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString(Constants.KeyPair.KEY_ID, parentId);
        bundle.putString(Constants.KeyPair.KEY_PREFIX_SECTION_NAME, prefixSectionName);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mCategoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            mParentId = bundle.getString(Constants.KeyPair.KEY_ID);
            prefixSectionName = bundle.getString(Constants.KeyPair.KEY_PREFIX_SECTION_NAME);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cat_menu, container, false);

        // Add tab menu list in horizontal recycler view
        tabListAdapter = new CatMenuTabListAdapter(this);

        RecyclerView horizontalRecyclerView = view.findViewById(R.id.recycler_view);
        horizontalRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        horizontalRecyclerView.setAdapter(tabListAdapter);

        ArrayList<CategoryInfo> tabMenuList = new ArrayList<>();

        // add tab menu list of category
        if (mCategoryInfo.subMenu != null && mCategoryInfo.subMenu.size() > 0)
            tabMenuList.addAll(mCategoryInfo.subMenu);

        if (tabMenuList.size() > 0) {
            if (tabMenuList.size() == 1) {
                horizontalRecyclerView.setVisibility(View.GONE);
            } else {
                horizontalRecyclerView.setVisibility(View.VISIBLE);
            }
            tabListAdapter.add(tabMenuList);
            addFragment(tabMenuList.get(0));
        }

        return view;
    }

    private void addFragment(CategoryInfo info) {

        tabListAdapter.setColor(AppUtils.getThemeColor(getContext(), info.color));

        Systr.println("ADD Fragment : " + info.action);
        Fragment fragment;
        boolean isTrackingOn = true;
        switch (info.action) {
            case Action.CategoryAction.CAT_ACTION_CAT_MENU:
                fragment = CatMenuFragment.newInstance(mParentId, info, prefixSectionName);
                isTrackingOn = false;
                break;

            case Action.CategoryAction.CAT_ACTION_ALL_CAT_HOME:
                fragment = CatHomeFragment.newInstance(info, mParentId);
                break;

            default:
                fragment = ActivityUtil.getFragmentByCategory(info, "", prefixSectionName);
                break;

        }


        try {
            FragmentActivity activity = getActivity();
            assert activity != null;
            if (!activity.isFinishing() && fragment != null) {
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.inner_frame_layout, fragment);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        } catch (Exception ignored) {
        }

        if (isTrackingOn) {
            sendTracking(info.gaScreen);
        }
    }

    private void sendTracking(String gaScreen) {
        if (isFragmentShowing) {
            String souce = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

            String gaScreenN = TextUtils.isEmpty(prefixSectionName) ? gaScreen : prefixSectionName + "_" + gaScreen;
            Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), gaScreenN, souce, medium, campaign);
        } else {
            previousTrackList.add(gaScreen);
        }
    }

    @Override
    public void onPauseFragment() {
        isFragmentShowing = false;
    }

    @Override
    public void onResumeFragment() {
        isFragmentShowing = true;
        while (previousTrackList.size() > 0) {
            sendTracking(previousTrackList.get(0));
            previousTrackList.remove(previousTrackList.get(0));
        }
    }

    @Override
    public void onTabClick(CategoryInfo info) {
        addFragment(info);
    }


    @Override
    public void moveToTop() {
        try {
            Object fragmentInstance = getChildFragmentManager().findFragmentById(R.id.inner_frame_layout);
            if (fragmentInstance instanceof OnMoveTopListener) {
                ((OnMoveTopListener) fragmentInstance).moveToTop();
            }
        } catch (Exception ignored) {
        }
    }
}
