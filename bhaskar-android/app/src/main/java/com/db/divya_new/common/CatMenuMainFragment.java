package com.db.divya_new.common;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.divyashree.CatHomeFragment;
import com.db.home.ActivityUtil;
import com.db.home.NewsFragment;
import com.db.listeners.FragmentLifecycle;
import com.bhaskar.util.Action;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static android.app.Activity.RESULT_OK;

/***
 * Params : CategoryInfo (for action Cat_Menu)
 **/
public class CatMenuMainFragment extends Fragment implements FragmentLifecycle, TabController.SubMenuTabController, OnMoveTopListener {

    private CopyOnWriteArrayList<CategoryInfo> tabMenuList;
    private CategoryInfo mCategoryInfo;
    private TabLayout tabLayout;
    public ViewPager pager;
    private ViewPagerAdapter viewPagerAdapter;

    private ViewPager.OnPageChangeListener onPageChangeListener;
    private int currentPos;
    private boolean isFragmentShowing;
    private String prefixSectionName;

    private ArrayList<String> previousTrackList = new ArrayList<>();

    public static CatMenuMainFragment newInstance(CategoryInfo categoryInfo, String prefixName) {
        CatMenuMainFragment fragment = new CatMenuMainFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString(Constants.KeyPair.KEY_PREFIX_SECTION_NAME, prefixName);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mCategoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            prefixSectionName = bundle.getString(Constants.KeyPair.KEY_PREFIX_SECTION_NAME);
        }
        tabMenuList = new CopyOnWriteArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cat_menu_tab, container, false);

        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setVisibility(View.VISIBLE);

        tabLayout.setBackgroundColor(AppUtils.getThemeColor(getContext(), mCategoryInfo.color));

        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        pager = view.findViewById(R.id.viewpager);

        pager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(pager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                try {
                    if (pager != null && pager.getAdapter() != null) {

                        Object fragmentInstance = pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                        if (fragmentInstance instanceof OnMoveTopListener) {
                            ((OnMoveTopListener) fragmentInstance).moveToTop();
                        }
                    }
                } catch (Exception ignored) {

                }


            }
        });
        onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Systr.println("TabClick: CatMenuMainFragment onPageSelected: " + position);
                checkForPauseResumeFragment(position);
                boolean isTrackingOn = true;
                try {
                    Object fragmentInstance = viewPagerAdapter.instantiateItem(pager, position);
                    if (fragmentInstance instanceof CatMenuFragment) {
                        isTrackingOn = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (isTrackingOn) {
                    if (tabMenuList != null && tabMenuList.size() > position) {
                        previousTrackList.add(tabMenuList.get(position).gaScreen);
                        sendTracking();
                    }
                }

                if (tabMenuList.size() > position)
                    tabLayout.setBackgroundColor(AppUtils.getThemeColor(getContext(), tabMenuList.get(position).color));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
        pager.addOnPageChangeListener(onPageChangeListener);

        // add tab menu list of category
        if (mCategoryInfo.subMenu != null && mCategoryInfo.subMenu.size() > 0) {

            tabMenuList.addAll(mCategoryInfo.subMenu);
            filterOutsideCategory(tabMenuList);
            if (tabMenuList.size() > 1) {
                tabLayout.setVisibility(View.VISIBLE);
            } else {
                tabLayout.setVisibility(View.GONE);
            }
            viewPagerAdapter.notifyDataSetChanged();
        }

        return view;
    }

    private void filterOutsideCategory(List<CategoryInfo> subMenuList) {
        List<String> outsideActionList = Arrays.asList(Action.outsideOpenCateg);
        for (CategoryInfo categoryInfo:subMenuList) {
            if(outsideActionList.contains(categoryInfo.action)){
                subMenuList.remove(categoryInfo);
            }
        }
    }

    private void sendTracking() {
        if (isFragmentShowing) {
            while (previousTrackList.size() > 0) {
                String gaScreen = TextUtils.isEmpty(prefixSectionName) ? previousTrackList.get(0) : prefixSectionName + "_" + previousTrackList.get(0);
                String souce = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
                String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

                Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), gaScreen, souce, medium, campaign);

                previousTrackList.remove(previousTrackList.get(0));
            }
        }
    }

    private void checkForPauseResumeFragment(int position) {
        if (pager != null) {
            PagerAdapter newsPagerAdapter = pager.getAdapter();
            if (newsPagerAdapter != null) {
                try {
                    FragmentLifecycle fragmentToShow = (FragmentLifecycle) newsPagerAdapter.instantiateItem(pager, currentPos);
                    fragmentToShow.onPauseFragment();
                } catch (Exception e) {
                }
                try {
                    FragmentLifecycle lifecycle = (FragmentLifecycle) newsPagerAdapter.instantiateItem(pager, position);
                    lifecycle.onResumeFragment();
                } catch (Exception e) {
                }
            }
        }
        currentPos = position;
    }

    private Fragment getFragment(CategoryInfo info) {
        Fragment fragment = null;
        switch (info.action) {
            case Action.CategoryAction.CAT_ACTION_CAT_MENU:
                fragment = CatMenuFragment.newInstance(mCategoryInfo.id, info, prefixSectionName);
                break;
            case Action.CategoryAction.CAT_ACTION_ALL_CAT_HOME:
                fragment = CatHomeFragment.newInstance(info, mCategoryInfo.id);
                break;
            default:
                fragment = ActivityUtil.getFragmentByCategory(info, "", prefixSectionName);
                break;
        }

        return fragment;
    }


    @Override
    public void onPauseFragment() {
        isFragmentShowing = false;
        if (currentPos != -1) {
            try {
                FragmentLifecycle fragment = (FragmentLifecycle) pager.getAdapter().instantiateItem(pager, currentPos);
                fragment.onPauseFragment();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (TabController.getInstance().getSubMenuTabControllerInstance() == null && isFragmentShowing) {
            TabController.getInstance().setSubMenuTabController(this);

        }
    }

    @Override
    public void onResumeFragment() {
        TabController.getInstance().setSubMenuTabController(this);

        isFragmentShowing = true;
        if (!TextUtils.isEmpty(NewsFragment.child_id)) {
            TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(NewsFragment.child_id);
            NewsFragment.child_id = "";
        } else {
            pager.post(new Runnable() {
                @Override
                public void run() {
                    onPageChangeListener.onPageSelected(pager.getCurrentItem());
                }
            });
        }
        if (currentPos != -1) {
            try {
                FragmentLifecycle fragment = (FragmentLifecycle) pager.getAdapter().instantiateItem(pager, currentPos);
                fragment.onResumeFragment();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == MainActivity.REQUEST_CODE_CALL_FOR_FOOTER_TAB) {
            String id = data.getStringExtra(Constants.IntentExtra.CHILD_TAB_ID);
            if (pager != null && tabMenuList != null) {
                TabController.getInstance().setTabClicked(false);
                pager.setCurrentItem(tabMenuList.indexOf(new CategoryInfo(id)));
            }
        }
    }

    @Override
    public void onSubMenuTabClick(String childId) {
        if (pager != null && tabMenuList != null) {
            int index = 0;
            index = tabMenuList.indexOf(new CategoryInfo(childId));
            if (index != -1) {
                pager.setCurrentItem(index);
            } else {
                if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                    TabController.getInstance().setTabClicked(true);
                    TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(childId);
                }
            }
        }
    }

    @Override
    public void onSubMenuTabClick(String childId, LoadMoreController controller) {
        if (pager != null && tabMenuList != null) {
            int index = 0;
            index = tabMenuList.indexOf(new CategoryInfo(childId));
            if (index != -1) {
                pager.setCurrentItem(index);
                if (controller != null) controller.onLoadFinish();
            } else {
                if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {

                    if (controller != null) controller.onLoadFinish();
                    TabController.getInstance().setTabClicked(true);
                    TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(childId, controller);
                }
            }
        }
    }

    @Override
    public void moveToTop() {
        try {
            if (pager != null && pager.getAdapter() != null) {
                Object fragmentInstance = pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                if (fragmentInstance instanceof OnMoveTopListener) {
                    ((OnMoveTopListener) fragmentInstance).moveToTop();
                }
            }
        } catch (Exception ignored) {
        }
    }


    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            if (tabMenuList.size() > position)
                return getFragment(tabMenuList.get(position));

            return null;
        }

        @Override
        public int getCount() {
            return tabMenuList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabMenuList.get(position).menuName;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }
    }
}
