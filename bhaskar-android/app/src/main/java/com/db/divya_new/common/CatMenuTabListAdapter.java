package com.db.divya_new.common;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;

import java.util.ArrayList;
import java.util.List;

public class CatMenuTabListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private OnTabClickLitener mListener;
    private ArrayList<CategoryInfo> mList;
    private int selectedPosition;
    private int color;

    public CatMenuTabListAdapter(OnTabClickLitener listener) {
        mList = new ArrayList<>();
        mListener = listener;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    public void setColor(int color) {
        this.color = color;
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public void add(List<CategoryInfo> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_tab_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            itemBindHolder((ItemViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    /******** Bind Holders Methods ********/

    private void itemBindHolder(ItemViewHolder viewHolder, final int pos) {

        final CategoryInfo info = mList.get(pos);
//        viewHolder.rootRelativeTab.setBackgroundColor(Color.parseColor());
        viewHolder.textView.setText(info.menuName);
        if (color != 0)
            viewHolder.selectionView.setBackgroundColor(color);

        if (selectedPosition == pos) {
            viewHolder.textView.setBackgroundResource(R.drawable.text_bckgrnd_round_newsaction);
            viewHolder.textView.getBackground().setTint(Color.parseColor(info.color));
            viewHolder.textView.setTextColor(Color.WHITE);
            viewHolder.selectionView.setVisibility(View.GONE);
        } else {
            viewHolder.textView.setBackgroundResource(R.drawable.txt_bckgrnd_unselect_newsaction);
            viewHolder.textView.setTextColor(Color.BLACK);
            viewHolder.selectionView.setVisibility(View.GONE);
        }

        viewHolder.rootRelativeTab.getBackground().setTint(Color.parseColor(info.color));
        viewHolder.textView.setOnClickListener(view -> {
            selectedPosition = pos;
            if (mListener != null) {
                mListener.onTabClick(info);
            }
            notifyDataSetChanged();
        });
    }


    public interface OnTabClickLitener {
        void onTabClick(CategoryInfo info);
    }

    /******** View Holders Classes ********/

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;
        private View selectionView;
        private RelativeLayout rootRelativeTab;

        public ItemViewHolder(View itemView) {
            super(itemView);
            rootRelativeTab = itemView.findViewById(R.id.root_view_tab);
            textView = itemView.findViewById(R.id.tv_title);
            selectionView = itemView.findViewById(R.id.selection_view);
        }
    }
}
