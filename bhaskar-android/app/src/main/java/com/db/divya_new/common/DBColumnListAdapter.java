package com.db.divya_new.common;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.StoryListInfo;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.common.viewholder.LoadMoreViewHolder;
import com.db.util.Constants;
import com.db.util.ImageUtil;

import java.util.ArrayList;
import java.util.List;

public class DBColumnListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Object> mList;
    private Activity mContext;
    private boolean isFromHome;
    private CategoryInfo categoryInfo;

    public DBColumnListAdapter(Activity context) {
        mList = new ArrayList<>();
        this.mContext = context;
    }

    public void setData(CategoryInfo categoryInfo, boolean isFromHome) {
        this.categoryInfo = categoryInfo;
        this.isFromHome = isFromHome;

    }

    public void add(Object object) {
        mList.add(object);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ArticleViewHolder(inflater.inflate(R.layout.listitem_circular_image_title_subtitle, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleViewHolder) {
            articleBindHolder((ArticleViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    /******** Bind Holders Methods ********/

    private void articleBindHolder(ArticleViewHolder viewHolder, int position) {
        final StoryListInfo storyListInfo = (StoryListInfo) mList.get(position);
        viewHolder.tvTitle.setText(storyListInfo.title);
        if (!TextUtils.isEmpty(storyListInfo.subTitle)) {
            viewHolder.tvSubTitle.setVisibility(View.VISIBLE);
            viewHolder.tvSubTitle.setText(storyListInfo.subTitle);

        } else {
            viewHolder.tvSubTitle.setVisibility(View.GONE);
        }

        if (isFromHome) {
            viewHolder.itemView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            viewHolder.itemView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        }
        ImageUtil.setImage(viewHolder.itemView.getContext(), storyListInfo.image, viewHolder.ivPic, R.drawable.water_mark_brand_category);

        viewHolder.itemView.setOnClickListener(view -> {
            if(TextUtils.isEmpty(storyListInfo.colorCode)){
               storyListInfo.colorCode =  categoryInfo.color;
            }
            mContext.startActivityForResult(DivyaCommonArticleActivity.getIntent(mContext, storyListInfo, categoryInfo), Constants.REQ_CODE_BOTTOM_NAV);
        });

    }

    private void loadmoreBindHolder(LoadMoreViewHolder viewHolder, int position) {
    }

    public void addAll(List<StoryListInfo> storyInfoList) {
        mList.addAll(storyInfoList);
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public void addAndClearAll(List<StoryListInfo> storyInfoList) {
        mList.clear();
        mList.addAll(storyInfoList);
        notifyDataSetChanged();
    }

    /******** View Holders Classes ********/
    class ArticleViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivPic;
        private TextView tvTitle, tvSubTitle;

        public ArticleViewHolder(View itemView) {
            super(itemView);
            ivPic = itemView.findViewById(R.id.iv_pic);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvSubTitle = itemView.findViewById(R.id.tv_sub_title);
        }

    }
}
