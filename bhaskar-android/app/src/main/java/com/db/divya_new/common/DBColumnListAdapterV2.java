package com.db.divya_new.common;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.StoryListInfo;
import com.db.database.DatabaseClient;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.data.CatHome2;
import com.db.util.ImageUtil;

import java.util.ArrayList;
import java.util.List;

public class DBColumnListAdapterV2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> mList;
    private Context mContext;
    private String gaDisplayName = "";
    private String categoryID;

    public DBColumnListAdapterV2(Context context) {
        mList = new ArrayList<>();
        this.mContext = context;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ArticleViewHolder(inflater.inflate(R.layout.listitem_circular_image_title_subtitle, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleViewHolder) {
            articleBindHolder((ArticleViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    /******** Bind Holders Methods ********/
    private void articleBindHolder(ArticleViewHolder viewHolder, int position) {
        final StoryListInfo storyListInfo = (StoryListInfo) mList.get(position);
        viewHolder.tvTitle.setText(storyListInfo.title);
        if (!TextUtils.isEmpty(storyListInfo.subTitle)) {
            viewHolder.tvSubTitle.setVisibility(View.VISIBLE);
            viewHolder.tvSubTitle.setText(storyListInfo.subTitle);

        } else {
            viewHolder.tvSubTitle.setVisibility(View.GONE);
        }

        viewHolder.itemView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
        ImageUtil.setImage(viewHolder.itemView.getContext(), storyListInfo.image, viewHolder.ivPic, R.drawable.water_mark_brand_category);

        viewHolder.itemView.setOnClickListener(view -> {
            CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(categoryID);
            if(TextUtils.isEmpty(storyListInfo.colorCode)){
                storyListInfo.colorCode =  categoryInfo.color;
            }
            mContext.startActivity(DivyaCommonArticleActivity.getIntent(mContext, storyListInfo, categoryInfo));
        });

    }

    public void addAll(List<StoryListInfo> storyInfoList) {
        mList.addAll(storyInfoList);
        notifyDataSetChanged();
    }

    public void setData(CatHome2 catHome) {
        this.categoryID = catHome.id;
        mList.clear();
        mList.addAll(catHome.list);
        notifyDataSetChanged();
    }

    /******** View Holders Classes ********/
    class ArticleViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivPic;
        private TextView tvTitle, tvSubTitle;

        ArticleViewHolder(View itemView) {
            super(itemView);
            ivPic = itemView.findViewById(R.id.iv_pic);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvSubTitle = itemView.findViewById(R.id.tv_sub_title);
        }

    }
}
