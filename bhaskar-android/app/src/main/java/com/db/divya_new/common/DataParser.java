package com.db.divya_new.common;

import com.db.divya_new.data.CatHomeData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DataParser {

    private static DataParser dataParser = null;

    private Map<String, CatHomeData> catHomeDataHashMap = new HashMap<>();

    private DataParser() {
    }

    public static DataParser getInstance() {
        if (dataParser == null) {
            synchronized (DataParser.class) {
                if (dataParser == null)
                    dataParser = new DataParser();
            }
        }
        return dataParser;
    }

    public CatHomeData getCatHomeDataHashMapById(String id) {
        if (catHomeDataHashMap.containsKey(id)) {
            return catHomeDataHashMap.get(id);
        } else
            return null;
    }

    public void removeCatHomeDataHashMapById(String id) {
        catHomeDataHashMap.remove(id);
    }

    public void removeAllDataExceptCatHome() {
        Set<String> arr = catHomeDataHashMap.keySet();
        List<String> removingList = new ArrayList<>();
        for (String str : arr) {
            if (catHomeDataHashMap.get(str).type != 0) {
                removingList.add(str);
            }
        }
        if (removingList.size() > 0) {
            for (String str : removingList) {
                catHomeDataHashMap.remove(str);
            }
        }
    }

    public void addCatHomeDataHashMap(String id, CatHomeData catHomeData) {
        catHomeDataHashMap.put(id, catHomeData);
    }
}
