package com.db.divya_new.common;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bhaskar.R;
import com.db.ads.AdPlacementController;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.NativeAdViewHolder2;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.home.FlickerViewHolder;
import com.db.divya_new.home.StockMarketViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.news.WapV2Activity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppLogs;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.views.JustifiedTextView;
import com.db.views.viewholder.BigImageListViewHolder;
import com.db.views.viewholder.GridFourViewHolderNew;
import com.db.views.viewholder.GuardianListViewHolder;
import com.db.views.viewholder.RecListViewHolder;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements NativeListAdController2.onAdNotifyListener {

    private final int VIEW_TYPE_BIG_IMAGE = 0;
    private final int VIEW_TYPE_NORMAL_LIST = 1;
    private final int VIEW_TYPE_FLICKER = 2;
    private final int VIEW_TYPE_STOCK = 3;
    private final int VIEW_TYPE_BIG_IMAGE_3 = 6;
    private final int VIEW_TYPE_BIG_IMAGE_4 = 7;
    private final int VIEW_TYPE_GUARDIAN = 8;
    private final int VIEW_TYPE_GRID_4 = 9;
    private final int VIEW_TYPE_PHOTO_FLICKER = 10;
    public static final int VIEW_TYPE_BANNER = 11;
    private final int VIEW_TYPE_NATIVE_ADS = 26;

    private CategoryInfo categoryInfo;
    private ArrayList<NewsListInfo> mList;
    private Activity mContext;
    private String color;

    public NewsAdapter(Activity context) {
        mList = new ArrayList<>();
        this.mContext = context;
    }

    public void setCategoryInfo(CategoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
        this.color = categoryInfo.color;
        notifyDataSetChanged();
    }

    public void add(NewsListInfo object) {
        mList.add(object);
    }

    public void add(NewsListInfo object, int position) {
        try {
            mList.add(position, object);
        } catch (Exception ignore) {
        }
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public void addAndClearAll(List<NewsListInfo> storyInfoList) {
        mList.clear();
        mList.addAll(storyInfoList);
        AdPlacementController.getInstance().addHomeListingNativeAds(mList, mContext);
    }

    public void setData(List<NewsListInfo> objList) {
        if (mList == null) {
            mList = new ArrayList<>();
        }
        mList.addAll(objList);
        AdPlacementController.getInstance().addHomeListingNativeAds(mList, mContext);
    }

    public NewsListInfo getItemAtPosition(int position) {
        if (position >= 0 && position < mList.size())
            return mList.get(position);
        return null;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_NORMAL_LIST:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_rec_item, parent, false));
            case VIEW_TYPE_BIG_IMAGE:
                return new BigImageListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image, parent, false));
            case VIEW_TYPE_BIG_IMAGE_3:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image_three, parent, false));
            case VIEW_TYPE_BIG_IMAGE_4:
                return new BigImageListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image_four, parent, false));
            case VIEW_TYPE_GUARDIAN:
                return new GuardianListViewHolder(inflater.inflate(R.layout.recyclerlist_item_guardian_view, parent, false));
            case VIEW_TYPE_GRID_4:
                return new GridFourViewHolderNew(inflater.inflate(R.layout.recyclerlist_item_grid_four_view_new, parent, false));

            case VIEW_TYPE_FLICKER:
                return new FlickerViewHolder(inflater.inflate(R.layout.listitem_flicker, parent, false), mContext, categoryInfo.id, false);
            case VIEW_TYPE_PHOTO_FLICKER:
                return new FlickerViewHolder(inflater.inflate(R.layout.listitem_flicker, parent, false), mContext, categoryInfo.id, true);
            case VIEW_TYPE_STOCK:
                return new StockMarketViewHolder(inflater.inflate(R.layout.listitem_stock_market, parent, false), mContext);
            case VIEW_TYPE_NATIVE_ADS:
                return new NativeAdViewHolder2(inflater.inflate(R.layout.ad_layout_native_listing_view, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_BIG_IMAGE_3:
            case VIEW_TYPE_NORMAL_LIST:
                recListBindHolder((RecListViewHolder) holder, position);
                break;
            case VIEW_TYPE_BIG_IMAGE:
                bigImageListBindHolder((BigImageListViewHolder) holder, false, position);
                break;
            case VIEW_TYPE_BIG_IMAGE_4:
                bigImageListBindHolder((BigImageListViewHolder) holder, true, position);
                break;
            case VIEW_TYPE_GUARDIAN:
                guardianViewBindHolder((GuardianListViewHolder) holder, position);
                break;
            case VIEW_TYPE_GRID_4:
                gridFourViewBindHolderNew((GridFourViewHolderNew) holder, position);
                break;
            case VIEW_TYPE_NATIVE_ADS:
                showNativeAds((NativeAdViewHolder2) holder, position);
                break;
            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData(mList.get(holder.getAdapterPosition()).mBannerInfo);
                break;
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if(holder instanceof FlickerViewHolder){
            ((FlickerViewHolder) holder).resumeTimer();
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if(holder instanceof FlickerViewHolder){
            ((FlickerViewHolder) holder).pauseTimer();
        }
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        NewsListInfo listInfo = mList.get(position);
        if (listInfo.isAd) {
            return VIEW_TYPE_NATIVE_ADS;
        } else if (listInfo.isFlicker) {
            return VIEW_TYPE_FLICKER;
        } else if (listInfo.isPhotoFlicker) {
            return VIEW_TYPE_PHOTO_FLICKER;
        } else if (listInfo.isStockWidget) {
            return VIEW_TYPE_STOCK;
        } else if (listInfo.subStoryList.size() == 2) {
            return VIEW_TYPE_GUARDIAN;
        } else if (listInfo.subStoryList.size() == 4) {
            return VIEW_TYPE_GRID_4;
        } else if (listInfo.type > 1) {
            return listInfo.type;
        } else {
            switch (listInfo.bigImage) {
                case 1:
                    return VIEW_TYPE_BIG_IMAGE;
                case 3:
                    return VIEW_TYPE_BIG_IMAGE_3;
                case 4:
                    return VIEW_TYPE_BIG_IMAGE_4;
                default:
                    return VIEW_TYPE_NORMAL_LIST;
            }
        }
    }

    @Override
    public void onNotifyAd() {
        notifyDataSetChanged();
    }

    private void showNativeAds(NativeAdViewHolder2 holder, int pos) {
        NewsListInfo adInfo = mList.get(pos);
//        NativeListAdController.getInstance().getAd(adInfo.adUnit, adInfo.adType, holder.getAdapterPosition(), holder.adCardLayout, new WeakReference<>(new NativeListAdController.onAdNotifyListener() {
//            @Override
//            public void onNotifyAd() {
//
//            }
//        }));

        UnifiedNativeAd ad = NativeListAdController2.getInstance(mContext).getAdForHome(adInfo.adUnit, adInfo.adType, pos, categoryInfo.id, new WeakReference<NativeListAdController2.onAdNotifyListener>(this));
        holder.populateNativeAdView(ad);
    }

    /******** Bind Holders Methods ********/

    private void bigImageListBindHolder(BigImageListViewHolder viewHolder, boolean isTitleOverlay, int position) {
        final BigImageListViewHolder bigImageListViewHolder = viewHolder;
        final NewsListInfo bigNewsInfo = mList.get(bigImageListViewHolder.getAdapterPosition());

        /*bullet point*/
        if (bigNewsInfo.bulletsPoint != null && bigNewsInfo.bulletsPoint.length > 0) {
            bigImageListViewHolder.bulletLayout.removeAllViews();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            for (int i = 0; i < bigNewsInfo.bulletsPoint.length; i++) {
                View item = inflater.inflate(R.layout.layout_bullet_type, null);
                String desc = bigNewsInfo.bulletsPoint[i];
                ((JustifiedTextView) item.findViewById(R.id.bullet_tv)).setText(AppUtils.getInstance().fromHtml(desc));
                ((ImageView) item.findViewById(R.id.iv_bullet_circle)).setColorFilter(AppUtils.getThemeColor(mContext, bigNewsInfo.colorCode));
                bigImageListViewHolder.bulletLayout.addView(item);
            }
            bigImageListViewHolder.bulletLayout.setVisibility(View.VISIBLE);
        } else {
            bigImageListViewHolder.bulletLayout.setVisibility(View.GONE);
        }
        /*Image*/
        float ratio = AppUtils.getInstance().parseImageRatio(bigNewsInfo.imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
        AppUtils.getInstance().setImageViewSizeWithAspectRatio(bigImageListViewHolder.ivThumb, ratio, 0, mContext);
        ImageUtil.setImage(mContext, bigNewsInfo.image, bigImageListViewHolder.ivThumb, R.drawable.water_mark_news_detail, R.drawable.water_mark_big_news_detail_error);
        /*Video Flag*/
        if (bigNewsInfo.videoFlag == 1) {
            bigImageListViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
            bigImageListViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, bigNewsInfo.colorCode));
        } else {
            bigImageListViewHolder.imgVideoPlay.setVisibility(View.GONE);
        }
        /*Exclusive*/
        if (!TextUtils.isEmpty(bigNewsInfo.exclusive)) {
            bigImageListViewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            bigImageListViewHolder.exclusiveTv.setText(bigNewsInfo.exclusive);
        } else {
            bigImageListViewHolder.exclusiveLayout.setVisibility(View.GONE);
        }
        bigImageListViewHolder.itemView.setOnClickListener(v -> {
            launchArticlePage(bigNewsInfo, position);
            bigImageListViewHolder.titleTextView.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));

        });

        try {
            bigImageListViewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    AppUtils.getInstance().bookmarkClick(mContext, bigNewsInfo, categoryInfo.detailUrl, color, false);
                }

                @Override
                public void onShare() {
                    AppUtils.getInstance().shareClick(mContext, bigNewsInfo.title, bigNewsInfo.webUrl, bigNewsInfo.gTrackUrl, false);
                }
            }));

            if (isTitleOverlay) {
                bigImageListViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitleNew(bigNewsInfo.iitl_title, AppUtils.getInstance().fromHtml(bigNewsInfo.title), bigNewsInfo.colorCode));
            } else {
                bigImageListViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(bigNewsInfo.iitl_title, AppUtils.getInstance().fromHtml(bigNewsInfo.title), bigNewsInfo.colorCode));
            }
        } catch (Exception ex) {
            AppLogs.printDebugLogs("VIEW_TYPE_BIG_IMAGE exception", ex.toString());
        }
    }

    private void recListBindHolder(RecListViewHolder viewHolder, int position) {

        final RecListViewHolder recListViewHolder = viewHolder;
        final NewsListInfo normalNewsInfo = mList.get(recListViewHolder.getAdapterPosition());
        /*Exclusive*/
        if (!TextUtils.isEmpty(normalNewsInfo.exclusive)) {
            recListViewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            recListViewHolder.exclusiveTv.setText(normalNewsInfo.exclusive);
        } else {
            recListViewHolder.exclusiveLayout.setVisibility(View.GONE);
        }
        /*Image*/
        String imageUrl = normalNewsInfo.image.replace("116x87", "156x117");
        ImageUtil.setImage(mContext, imageUrl, recListViewHolder.ivThumb, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);

        /*video flag*/
        if (normalNewsInfo.videoFlag == 1) {
            recListViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
            recListViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, normalNewsInfo.colorCode));
        } else {
            recListViewHolder.imgVideoPlay.setVisibility(View.GONE);
        }

        recListViewHolder.itemView.setOnClickListener(view -> launchArticlePage(normalNewsInfo, position));

        try {
            recListViewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    AppUtils.getInstance().bookmarkClick(mContext, normalNewsInfo, categoryInfo.detailUrl, color, false);
                }

                @Override
                public void onShare() {
                    AppUtils.getInstance().shareClick(mContext, normalNewsInfo.title, normalNewsInfo.webUrl, normalNewsInfo.gTrackUrl, false);
                }
            }));
            recListViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(normalNewsInfo.iitl_title, AppUtils.getInstance().fromHtml(normalNewsInfo.title), normalNewsInfo.colorCode));
        } catch (Exception ex) {
            AppLogs.printDebugLogs("MAIN LIST Exception", ex.toString());
        }
    }

    private void guardianViewBindHolder(GuardianListViewHolder viewHolder, int position) {
        final GuardianListViewHolder guardianListViewHolder = viewHolder;
        NewsListInfo newsListInfo = mList.get(guardianListViewHolder.getAdapterPosition());
        if (newsListInfo.subStoryList.size() < 2) {
            guardianListViewHolder.itemView.setVisibility(View.GONE);
            return;
        } else {
            guardianListViewHolder.itemView.setVisibility(View.VISIBLE);
        }

        final NewsListInfo info1 = newsListInfo.subStoryList.get(0);
        final NewsListInfo info2 = newsListInfo.subStoryList.get(1);
        //float ratio = AppUtils.getInstance().parseImageRatio(info1.imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
        //aka @akamahesh or lalit for more
        float ratio = AppUtils.getInstance().parseImageRatio("16x13", Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
        // first card
        try {
            guardianListViewHolder.ivMore_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                        @Override
                        public void onBookMark() {
                            AppUtils.getInstance().bookmarkClick(mContext, info1, categoryInfo.detailUrl, color, false);
                        }

                        @Override
                        public void onShare() {
                            AppUtils.getInstance().shareClick(mContext, info1.title, info1.webUrl, info1.gTrackUrl, false);
                        }
                    });
                }
            });
            guardianListViewHolder.titleTextView_1.setText(AppUtils.getInstance().getHomeTitle(info1.iitl_title, AppUtils.getInstance().fromHtml(info1.title), info1.colorCode));
        } catch (Exception ex) {
        }
        AppUtils.getInstance().setImageViewHalfSizeWithAspectRatio(guardianListViewHolder.ivThumb_1, ratio, 20, mContext);
        ImageUtil.setImage(mContext, info1.image, guardianListViewHolder.ivThumb_1, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);

        if (info1.videoFlag == 1) {
            guardianListViewHolder.ivVideoPlay_1.setVisibility(View.VISIBLE);
            guardianListViewHolder.ivVideoPlay_1.setColorFilter(AppUtils.getThemeColor(mContext, info1.colorCode));
        } else {
            guardianListViewHolder.ivVideoPlay_1.setVisibility(View.GONE);
        }
        /*Exclusive*/
        if (!TextUtils.isEmpty(info1.exclusive)) {
            guardianListViewHolder.exclusiveLayout1.setVisibility(View.VISIBLE);
            guardianListViewHolder.exclusiveTv1.setText(info1.exclusive);
        } else {
            guardianListViewHolder.exclusiveLayout1.setVisibility(View.GONE);
        }
        guardianListViewHolder.carLayout_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchArticlePage(info1, position);
                guardianListViewHolder.titleTextView_1.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));
            }
        });

        // second card
        try {
            guardianListViewHolder.ivMore_2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                        @Override
                        public void onBookMark() {
                            AppUtils.getInstance().bookmarkClick(mContext, info2, categoryInfo.detailUrl, color, false);
                        }

                        @Override
                        public void onShare() {
                            AppUtils.getInstance().shareClick(mContext, info2.title, info2.webUrl, info2.gTrackUrl, false);
                        }
                    });
                }
            });
            guardianListViewHolder.titleTextView_2.setText(AppUtils.getInstance().getHomeTitle(info2.iitl_title, AppUtils.getInstance().fromHtml(info2.title), info2.colorCode));

        } catch (Exception ex) {
        }

        AppUtils.getInstance().setImageViewHalfSizeWithAspectRatio(guardianListViewHolder.ivThumb_2, ratio, 20, mContext);
        ImageUtil.setImage(mContext, info2.image, guardianListViewHolder.ivThumb_2, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);


        if (info2.videoFlag == 1) {
            guardianListViewHolder.ivVideoPlay_2.setVisibility(View.VISIBLE);
            guardianListViewHolder.ivVideoPlay_2.setColorFilter(AppUtils.getThemeColor(mContext, info2.colorCode));
        } else {
            guardianListViewHolder.ivVideoPlay_2.setVisibility(View.GONE);
        }
        /*Exclusive*/
        if (!TextUtils.isEmpty(info2.exclusive)) {
            guardianListViewHolder.exclusiveLayout2.setVisibility(View.VISIBLE);
            guardianListViewHolder.exclusiveTv2.setText(info2.exclusive);
        } else {
            guardianListViewHolder.exclusiveLayout2.setVisibility(View.GONE);
        }
        guardianListViewHolder.carLayout_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchArticlePage(info2, position);
                guardianListViewHolder.titleTextView_2.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));
            }
        });
    }

    private void gridFourViewBindHolderNew(GridFourViewHolderNew viewHolder, int position) {
        NewsListInfo newsListInfo = mList.get(position);
        List<NewsListInfo> subStoryList = newsListInfo.subStoryList;
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, RecyclerView.VERTICAL);
        viewHolder.recyclerView.setLayoutManager(layoutManager);
        GridFourViewHolderNew.Adapter adapter = new GridFourViewHolderNew.Adapter(mContext, subStoryList, categoryInfo);
        viewHolder.recyclerView.setAdapter(adapter);
    }

    private void launchArticlePage(NewsListInfo newsListInfo, int position) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(categoryInfo.gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : categoryInfo.gaScreen) + newsListInfo.webUrl);
            mContext.startActivity(intent);

        } else if (newsListInfo.isOpenGallery) {
            Intent photoGalleryIntent = new Intent(mContext, PhotoGalleryActivity.class);
            photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, newsListInfo.storyId);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
            mContext.startActivity(photoGalleryIntent);

        } else {
            mContext.startActivity(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo, position));
        }
    }

    private String getSubTitle(String newsSubTitle) {
        String subTitle = "";
        try {
            subTitle = AppUtils.getInstance().removeAndReplaceHtmlSymbols(newsSubTitle);
        } catch (Exception ex) {
            AppLogs.printDebugLogs("", ex.toString());
        }

        if (subTitle != null && subTitle.length() > 120) {
            subTitle = subTitle.substring(0, 120);
        }
        AppLogs.printErrorLogs("", "subTitle = " + subTitle);
        return subTitle;
    }
}
