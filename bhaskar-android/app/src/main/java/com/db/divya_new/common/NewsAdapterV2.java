package com.db.divya_new.common;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bhaskar.R;
import com.db.ads.AdPlacementController;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.NativeAdViewHolder2;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.database.DatabaseClient;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.home.FlickerViewHolder;
import com.db.divya_new.home.StockMarketViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.homebanner.WebBannerViewHolder;
import com.db.news.WapV2Activity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.views.JustifiedTextView;
import com.db.views.viewholder.BigImageListViewHolder;
import com.db.views.viewholder.GridFourViewHolderNewV2;
import com.db.views.viewholder.GuardianListViewHolder;
import com.db.views.viewholder.RecListViewHolder;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NewsAdapterV2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements NativeListAdController2.onAdNotifyListener {

    private final int VIEW_TYPE_BIG_IMAGE = 0;
    private final int VIEW_TYPE_NORMAL_LIST = 1;
    private final int VIEW_TYPE_FLICKER = 2;
    private final int VIEW_TYPE_STOCK = 3;
    private final int VIEW_TYPE_BIG_IMAGE_3 = 6;
    private final int VIEW_TYPE_BIG_IMAGE_4 = 7;
    private final int VIEW_TYPE_GUARDIAN = 8;
    private final int VIEW_TYPE_GRID_4 = 9;
    private final int VIEW_TYPE_PHOTO_FLICKER = 10;
    public static final int VIEW_TYPE_BANNER = 11;
    public static final int VIEW_TYPE_WEB_BANNER = 12;
    private final int VIEW_TYPE_NATIVE_ADS = 26;

    private ArrayList<Object> mList;
    private Activity mContext;
    private String color;
    private String categoryID;

    public NewsAdapterV2(Activity context) {
        mList = new ArrayList<>();
        this.mContext = context;
    }

    public void setCategoryInfo(String categoryID, String color) {
        this.categoryID = categoryID;
        this.color = color;
        notifyDataSetChanged();
    }

    public void add(NewsListInfo object, int position) {
        try {
            mList.add(position, object);
        } catch (Exception ignore) {
        }
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public void setData(List<Object> objList) {
        if (mList == null) {
            mList = new ArrayList<>();
        }
        mList.addAll(objList);
        AdPlacementController.getInstance().addNewsListingNativeAdsWithObject(mList, mContext);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_NORMAL_LIST:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_rec_item, parent, false));

            case VIEW_TYPE_BIG_IMAGE:
                return new BigImageListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image, parent, false));

            case VIEW_TYPE_BIG_IMAGE_3:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image_three, parent, false));

            case VIEW_TYPE_BIG_IMAGE_4:
                return new BigImageListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image_four, parent, false));

            case VIEW_TYPE_GUARDIAN:
                return new GuardianListViewHolder(inflater.inflate(R.layout.recyclerlist_item_guardian_view, parent, false));

            case VIEW_TYPE_GRID_4:
                return new GridFourViewHolderNewV2(inflater.inflate(R.layout.recyclerlist_item_grid_four_view_new, parent, false));

            case VIEW_TYPE_FLICKER:
                return new FlickerViewHolder(inflater.inflate(R.layout.listitem_flicker, parent, false), mContext, categoryID, false);

            case VIEW_TYPE_PHOTO_FLICKER:
                return new FlickerViewHolder(inflater.inflate(R.layout.listitem_flicker, parent, false), mContext, categoryID, true);

            case VIEW_TYPE_STOCK:
                return new StockMarketViewHolder(inflater.inflate(R.layout.listitem_stock_market, parent, false), mContext);

            case VIEW_TYPE_NATIVE_ADS:
                return new NativeAdViewHolder2(inflater.inflate(R.layout.ad_layout_native_listing_view, parent, false));

            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
                bannerViewHolder.setIsRecyclable(false);
                return bannerViewHolder;

            case VIEW_TYPE_WEB_BANNER:
                WebBannerViewHolder webBannerViewHolder = new WebBannerViewHolder(inflater.inflate(R.layout.layout_web_banner, parent, false));
                webBannerViewHolder.setIsRecyclable(false);
                return webBannerViewHolder;
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if(holder instanceof FlickerViewHolder){
            ((FlickerViewHolder) holder).resumeTimer();
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if(holder instanceof FlickerViewHolder){
            ((FlickerViewHolder) holder).pauseTimer();
        }
    }




    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_BIG_IMAGE_3:
            case VIEW_TYPE_NORMAL_LIST:
                recListBindHolder((RecListViewHolder) holder, position);
                break;

            case VIEW_TYPE_BIG_IMAGE:
                bigImageListBindHolder((BigImageListViewHolder) holder, false, position);
                break;

            case VIEW_TYPE_BIG_IMAGE_4:
                bigImageListBindHolder((BigImageListViewHolder) holder, true, position);
                break;

            case VIEW_TYPE_GUARDIAN:
                guardianViewBindHolder((GuardianListViewHolder) holder, position);
                break;

            case VIEW_TYPE_GRID_4:
                gridFourViewBindHolderNew((GridFourViewHolderNewV2) holder, position);
                break;

            case VIEW_TYPE_NATIVE_ADS:
                showNativeAds((NativeAdViewHolder2) holder, position);
                break;

            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                NewsListInfo newsListInfo = (NewsListInfo) mList.get(holder.getAdapterPosition());
                bannerViewHolder.callwebViewOrSetWidgetData(newsListInfo.mBannerInfo);
                break;

            case VIEW_TYPE_WEB_BANNER:
                WebBannerViewHolder webBannerViewHolder = (WebBannerViewHolder) holder;
                webBannerViewHolder.callwebViewOrSetWidgetData(((NewsListInfo) mList.get(holder.getAdapterPosition())).webBannerInfo);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        NewsListInfo listInfo = (NewsListInfo) mList.get(position);
        if (listInfo.isAd) {
            return VIEW_TYPE_NATIVE_ADS;
        } else if (listInfo.isFlicker) {
            return VIEW_TYPE_FLICKER;
        } else if (listInfo.isPhotoFlicker) {
            return VIEW_TYPE_PHOTO_FLICKER;
        } else if (listInfo.isStockWidget) {
            return VIEW_TYPE_STOCK;
        } else if (listInfo.subStoryList.size() == 2) {
            return VIEW_TYPE_GUARDIAN;
        } else if (listInfo.subStoryList.size() == 4) {
            return VIEW_TYPE_GRID_4;
        } else if (listInfo.type > 1) {
            return listInfo.type;
        } else {
            switch (listInfo.bigImage) {
                case 1:
                    return VIEW_TYPE_BIG_IMAGE;
                case 3:
                    return VIEW_TYPE_BIG_IMAGE_3;
                case 4:
                    return VIEW_TYPE_BIG_IMAGE_4;
                default:
                    return VIEW_TYPE_NORMAL_LIST;
            }
        }
    }

    @Override
    public void onNotifyAd() {
        notifyDataSetChanged();
    }

    /******** Bind Holders Methods ********/

    private void showNativeAds(NativeAdViewHolder2 holder, int pos) {
        NewsListInfo adInfo = (NewsListInfo) mList.get(pos);

        UnifiedNativeAd ad = NativeListAdController2.getInstance(mContext).getAdForHome(adInfo.adUnit, adInfo.adType, pos, categoryID, new WeakReference<NativeListAdController2.onAdNotifyListener>(this));
        holder.populateNativeAdView(ad);
    }

    private void bigImageListBindHolder(BigImageListViewHolder bigImageListViewHolder, boolean isTitleOverlay, int position) {
        final NewsListInfo bigNewsInfo = (NewsListInfo) mList.get(bigImageListViewHolder.getAdapterPosition());
        if (TextUtils.isEmpty(bigNewsInfo.colorCode))
            bigNewsInfo.colorCode = color;
        /*bullet point*/
        if (bigNewsInfo.bulletsPoint != null && bigNewsInfo.bulletsPoint.length > 0) {
            bigImageListViewHolder.bulletLayout.removeAllViews();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            for (int i = 0; i < bigNewsInfo.bulletsPoint.length; i++) {
                View item = inflater.inflate(R.layout.layout_bullet_type, null);
                String desc = bigNewsInfo.bulletsPoint[i];
                ((JustifiedTextView) item.findViewById(R.id.bullet_tv)).setText(AppUtils.getInstance().fromHtml(desc));
                ((ImageView) item.findViewById(R.id.iv_bullet_circle)).setColorFilter(AppUtils.getThemeColor(mContext, bigNewsInfo.colorCode));
                bigImageListViewHolder.bulletLayout.addView(item);
            }
            bigImageListViewHolder.bulletLayout.setVisibility(View.VISIBLE);
        } else {
            bigImageListViewHolder.bulletLayout.setVisibility(View.GONE);
        }

        /*Image*/
        float ratio = AppUtils.getInstance().parseImageRatio(bigNewsInfo.imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
        AppUtils.getInstance().setImageViewSizeWithAspectRatio(bigImageListViewHolder.ivThumb, ratio, 0, mContext);
        ImageUtil.setImage(mContext, bigNewsInfo.image, bigImageListViewHolder.ivThumb, R.drawable.water_mark_news_detail, R.drawable.water_mark_big_news_detail_error);

        /*Video Flag*/
        if (bigNewsInfo.videoFlag == 1) {
            bigImageListViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
            bigImageListViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, bigNewsInfo.colorCode));
        } else {
            bigImageListViewHolder.imgVideoPlay.setVisibility(View.GONE);
        }

        /*Exclusive*/
        if (!TextUtils.isEmpty(bigNewsInfo.exclusive)) {
            bigImageListViewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            bigImageListViewHolder.exclusiveTv.setText(bigNewsInfo.exclusive);
        } else {
            bigImageListViewHolder.exclusiveLayout.setVisibility(View.GONE);
        }

        bigImageListViewHolder.itemView.setOnClickListener(v -> {
            launchArticlePage(bigNewsInfo, position);
            bigImageListViewHolder.titleTextView.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));
        });

        try {
            bigImageListViewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(categoryID);
                    if (categoryInfo != null) {
                        AppUtils.getInstance().bookmarkClick(mContext, bigNewsInfo, categoryInfo.detailUrl, categoryInfo.color, false);
                    }
                }

                @Override
                public void onShare() {
                    AppUtils.getInstance().shareClick(mContext, bigNewsInfo.title, bigNewsInfo.webUrl, bigNewsInfo.gTrackUrl, false);
                }
            }));

            if (isTitleOverlay) {
                bigImageListViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitleNew(bigNewsInfo.iitl_title, AppUtils.getInstance().fromHtml(bigNewsInfo.title), bigNewsInfo.colorCode));
            } else {
                bigImageListViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(bigNewsInfo.iitl_title, AppUtils.getInstance().fromHtml(bigNewsInfo.title), bigNewsInfo.colorCode));
            }
        } catch (Exception ignore) {
        }
    }

    private void recListBindHolder(RecListViewHolder recListViewHolder, int position) {
        final NewsListInfo normalNewsInfo = (NewsListInfo) mList.get(recListViewHolder.getAdapterPosition());
        if (TextUtils.isEmpty(normalNewsInfo.colorCode))
            normalNewsInfo.colorCode = color;
        /*Exclusive*/
        if (!TextUtils.isEmpty(normalNewsInfo.exclusive)) {
            recListViewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            recListViewHolder.exclusiveTv.setText(normalNewsInfo.exclusive);
        } else {
            recListViewHolder.exclusiveLayout.setVisibility(View.GONE);
        }

        /*Image*/
        String imageUrl = normalNewsInfo.image.replace("116x87", "156x117");
        ImageUtil.setImage(mContext, imageUrl, recListViewHolder.ivThumb, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);

        /*video flag*/
        if (normalNewsInfo.videoFlag == 1) {
            recListViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
            recListViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, normalNewsInfo.colorCode));
        } else {
            recListViewHolder.imgVideoPlay.setVisibility(View.GONE);
        }

        recListViewHolder.itemView.setOnClickListener(view -> launchArticlePage(normalNewsInfo, position));

        try {
            recListViewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(categoryID);
                    if (categoryInfo != null) {
                        AppUtils.getInstance().bookmarkClick(mContext, normalNewsInfo, categoryInfo.detailUrl, categoryInfo.color, false);
                    }
                }

                @Override
                public void onShare() {
                    AppUtils.getInstance().shareClick(mContext, normalNewsInfo.title, normalNewsInfo.webUrl, normalNewsInfo.gTrackUrl, false);
                }
            }));

            recListViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(normalNewsInfo.iitl_title, AppUtils.getInstance().fromHtml(normalNewsInfo.title), normalNewsInfo.colorCode));
        } catch (Exception ignore) {
        }
    }

    private void guardianViewBindHolder(GuardianListViewHolder guardianListViewHolder, int position) {
        NewsListInfo newsListInfo = (NewsListInfo) mList.get(guardianListViewHolder.getAdapterPosition());
        if (TextUtils.isEmpty(newsListInfo.colorCode))
            newsListInfo.colorCode = color;
        if (newsListInfo.subStoryList.size() < 2) {
            guardianListViewHolder.itemView.setVisibility(View.GONE);
            return;
        } else {
            guardianListViewHolder.itemView.setVisibility(View.VISIBLE);
        }

        final NewsListInfo info1 = newsListInfo.subStoryList.get(0);
        final NewsListInfo info2 = newsListInfo.subStoryList.get(1);

        float ratio = AppUtils.getInstance().parseImageRatio("16x13", Constants.ImageRatios.ARTICLE_DETAIL_RATIO);

        // first card
        try {
            guardianListViewHolder.ivMore_1.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(categoryID);
                    if (categoryInfo != null) {
                        AppUtils.getInstance().bookmarkClick(mContext, info1, categoryInfo.detailUrl, categoryInfo.color, false);
                    }
                }

                @Override
                public void onShare() {
                    AppUtils.getInstance().shareClick(mContext, info1.title, info1.webUrl, info1.gTrackUrl, false);
                }
            }));

            guardianListViewHolder.titleTextView_1.setText(AppUtils.getInstance().getHomeTitle(info1.iitl_title, AppUtils.getInstance().fromHtml(info1.title), info1.colorCode));
        } catch (Exception ignore) {
        }

        AppUtils.getInstance().setImageViewHalfSizeWithAspectRatio(guardianListViewHolder.ivThumb_1, ratio, 20, mContext);
        ImageUtil.setImage(mContext, info1.image, guardianListViewHolder.ivThumb_1, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);

        if (info1.videoFlag == 1) {
            guardianListViewHolder.ivVideoPlay_1.setVisibility(View.VISIBLE);
            guardianListViewHolder.ivVideoPlay_1.setColorFilter(AppUtils.getThemeColor(mContext, info1.colorCode));
        } else {
            guardianListViewHolder.ivVideoPlay_1.setVisibility(View.GONE);
        }

        /*Exclusive*/
        if (!TextUtils.isEmpty(info1.exclusive)) {
            guardianListViewHolder.exclusiveLayout1.setVisibility(View.VISIBLE);
            guardianListViewHolder.exclusiveTv1.setText(info1.exclusive);
        } else {
            guardianListViewHolder.exclusiveLayout1.setVisibility(View.GONE);
        }

        guardianListViewHolder.carLayout_1.setOnClickListener(v -> {
            launchArticlePage(info1, position);
            guardianListViewHolder.titleTextView_1.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));
        });

        // second card
        try {
            guardianListViewHolder.ivMore_2.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(categoryID);
                    if (categoryInfo != null) {
                        AppUtils.getInstance().bookmarkClick(mContext, info2, categoryInfo.detailUrl, categoryInfo.color, false);
                    }
                }

                @Override
                public void onShare() {
                    AppUtils.getInstance().shareClick(mContext, info2.title, info2.webUrl, info2.gTrackUrl, false);
                }
            }));

            guardianListViewHolder.titleTextView_2.setText(AppUtils.getInstance().getHomeTitle(info2.iitl_title, AppUtils.getInstance().fromHtml(info2.title), info2.colorCode));
        } catch (Exception ignore) {
        }

        AppUtils.getInstance().setImageViewHalfSizeWithAspectRatio(guardianListViewHolder.ivThumb_2, ratio, 20, mContext);
        ImageUtil.setImage(mContext, info2.image, guardianListViewHolder.ivThumb_2, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);

        if (info2.videoFlag == 1) {
            guardianListViewHolder.ivVideoPlay_2.setVisibility(View.VISIBLE);
            guardianListViewHolder.ivVideoPlay_2.setColorFilter(AppUtils.getThemeColor(mContext, info2.colorCode));
        } else {
            guardianListViewHolder.ivVideoPlay_2.setVisibility(View.GONE);
        }

        /*Exclusive*/
        if (!TextUtils.isEmpty(info2.exclusive)) {
            guardianListViewHolder.exclusiveLayout2.setVisibility(View.VISIBLE);
            guardianListViewHolder.exclusiveTv2.setText(info2.exclusive);
        } else {
            guardianListViewHolder.exclusiveLayout2.setVisibility(View.GONE);
        }

        guardianListViewHolder.carLayout_2.setOnClickListener(v -> {
            launchArticlePage(info2, position);
            guardianListViewHolder.titleTextView_2.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));
        });
    }

    private void gridFourViewBindHolderNew(GridFourViewHolderNewV2 viewHolder, int position) {
        NewsListInfo newsListInfo = (NewsListInfo) mList.get(position);
        if (TextUtils.isEmpty(newsListInfo.colorCode))
            newsListInfo.colorCode = color;
        List<NewsListInfo> subStoryList = newsListInfo.subStoryList;

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, RecyclerView.VERTICAL);
        viewHolder.recyclerView.setLayoutManager(layoutManager);

        GridFourViewHolderNewV2.Adapter adapter = new GridFourViewHolderNewV2.Adapter(mContext, subStoryList, categoryID);
        viewHolder.recyclerView.setAdapter(adapter);
    }

    private void launchArticlePage(NewsListInfo newsListInfo, int position) {
        CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(categoryID);
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            if (categoryInfo != null) {
                intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(categoryInfo.gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : categoryInfo.gaScreen) + newsListInfo.webUrl);
            } else {
                intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAScreen.WEB_VIEW + "-" + newsListInfo.webUrl);
            }
            mContext.startActivity(intent);

        } else if (newsListInfo.isOpenGallery) {
            if (categoryInfo != null) {
                Intent photoGalleryIntent = new Intent(mContext, PhotoGalleryActivity.class);
                photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, newsListInfo.storyId);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, categoryInfo.detailUrl);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, categoryInfo.detailUrl);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                mContext.startActivity(photoGalleryIntent);
            }
        } else {
            mContext.startActivity(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo, position));
        }
    }
}
