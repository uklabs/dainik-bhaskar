package com.db.divya_new.common;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.ads.NativeListAdController2;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.data.CatHome;
import com.db.divya_new.data.CatHomeData;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class NewsParentViewHolder extends RecyclerView.ViewHolder implements LoadMoreController, NativeListAdController2.onAdNotifyListener {

    public RecyclerView recyclerView;
    public TextView tvTitle;
    public Button btnHeaderLoadMore;
    public Button btnSubHeaderLoadMore;
    public Button btn_retry;
    CategoryInfo categoryInfo;
    private NewsAdapter mAdapter;
    private String mParentId;
    private boolean isFromParentHome;
    private Activity mContext;
    private String feedUrl;
    private String finalFeedUrl;
    private String mSuperParentId;
    private int showCount = 0;
    private ProgressBar loadMoreProgressBar;
    private String id;
    private String pageTitle;
    private boolean isLoadMoreEnabled;
    private boolean isFromHome;
    private boolean isStockActive;
    private boolean isFlickerActive;
    private LinearLayoutManager linearLayoutManager;

    public NewsParentViewHolder(Activity mContext, View itemView) {
        super(itemView);
        this.mContext = mContext;
        isStockActive = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.StockPrefs.STOCK_IS_ACTIVE, false);
        isFlickerActive = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.FlickerPrefs.FLICKER_IS_ACTIVE, false);
        tvTitle = itemView.findViewById(R.id.tv_title);
        tvTitle.setVisibility(View.GONE);
        btn_retry = itemView.findViewById(R.id.btn_retry);
        btnHeaderLoadMore = itemView.findViewById(R.id.btnHeaderLoadMore);
        btnSubHeaderLoadMore = itemView.findViewById(R.id.btnSubHeaderLoadMore);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        recyclerView = itemView.findViewById(R.id.recycler_view);
        mAdapter = new NewsAdapter(this.mContext);
        btn_retry.setOnClickListener(v -> fetchData(true));
        linearLayoutManager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);


    }

    @Override
    public void onNotifyAd() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void loadMoreLogic() {
        if (isFromParentHome) {
            btnHeaderLoadMore.setVisibility(View.VISIBLE);
            btnSubHeaderLoadMore.setVisibility(View.GONE);
        } else if (isFromHome) {
            btnSubHeaderLoadMore.setVisibility(View.VISIBLE);
            btnHeaderLoadMore.setVisibility(View.GONE);
        } else {
            //infinite loading true
            btnHeaderLoadMore.setVisibility(View.GONE);
            btnSubHeaderLoadMore.setVisibility(View.GONE);
        }


        btnHeaderLoadMore.setOnClickListener(v -> onLoadMore());
        btnSubHeaderLoadMore.setOnClickListener(v -> onLoadMore());
    }


    private void onLoadMore() {
        if (isFromHome && isFromParentHome) {
            if (!TabController.getInstance().isTabClicked() && TabController.getInstance().getMenuTabControllerInstance() != null) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                new Handler().postDelayed(() -> {
                    try {
                        TabController.getInstance().setTabClicked(true);
                        if (TextUtils.isEmpty(mParentId) || mParentId.equalsIgnoreCase("0")) {
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(id, NewsParentViewHolder.this);
                        } else {
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mParentId, id, NewsParentViewHolder.this);
                        }
                    } catch (Exception e) {
                    }
                }, 500);
            }
        } else {
            if (!TabController.getInstance().isTabClicked() && TabController.getInstance().getMenuTabControllerInstance() != null) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            TabController.getInstance().setTabClicked(true);
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mSuperParentId, mParentId, NewsParentViewHolder.this);
                        } catch (Exception e) {
                        }
                    }
                }, 500);
            }
        }
    }

    public void setCategoryInfo(CategoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
        mAdapter.setCategoryInfo(categoryInfo);
        feedUrl = categoryInfo.feedUrl;
        id = categoryInfo.id;
    }

    public void clearDataAndLoadNew() {
        mAdapter.clear();
    }

    // Adding the title is available in the categoryInfo else it will be gone
    private void setUpPageTitle(String extraVlues) {
        String pageTitle = null;
        if (!TextUtils.isEmpty(extraVlues)) {
            try {
                if (new JSONObject(extraVlues).has("category_intro")) {
                    pageTitle = new JSONObject(extraVlues).optString("category_intro");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.pageTitle = pageTitle;

            setPageTitle(pageTitle);
        }
    }

    public void setPageTitle(String pageTitle) {
        TextView tvTitle = itemView.findViewById(R.id.tv_title);

        if (!TextUtils.isEmpty(pageTitle)) {
            this.pageTitle = pageTitle;
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(pageTitle);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                tvTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        } else {
            tvTitle.setVisibility(View.GONE);
        }
    }

    public void setData(CatHome catHome, boolean isFromHome, boolean isFromParentHome) {
        this.isFromHome = isFromHome;
        mSuperParentId = catHome.superParentId;
        if (isFromParentHome)
            mParentId = catHome.superParentId;
        else mParentId = catHome.parentId;

        this.isFromParentHome = isFromParentHome;
        if (TextUtils.isEmpty(catHome.count))
            this.showCount = 0;
        else
            this.showCount = Integer.parseInt(catHome.count);

        recyclerView.setAdapter(mAdapter);

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue())) {
            if (!TextUtils.isEmpty(catHomeData.pageTitle)) {
                setPageTitle(catHomeData.pageTitle);
            }
            parseJsonObjectFeedList(catHomeData, true);
        } else {
            setUpPageTitle(catHome.categoryInfo.extraVlues);
            fetchData(true);
        }
    }

    private void fetchData(boolean isClear) {
        loadMoreProgressBar.setVisibility(View.VISIBLE);
        btn_retry.setVisibility(View.GONE);

        finalFeedUrl = Urls.APP_FEED_BASE_URL + feedUrl + "PG1/";

        makeJsonObjectRequest(isClear);
    }

    private void makeJsonObjectRequest(final boolean isClear) {
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            AppLogs.printDebugLogs("Feed URL", "== " + finalFeedUrl);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedUrl), null,
                    response -> {
                        loadMoreProgressBar.setVisibility(View.GONE);
                        try {
                            if (!TextUtils.isEmpty(response.toString())) {
                                parseJsonObjectFeedList(response, isClear);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                VolleyLog.d("NewsListFragment", "Error: " + error.getMessage());
                Activity activity = mContext;
                if (activity != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(activity, mContext.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(activity, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
                loadMoreProgressBar.setVisibility(View.GONE);
                btn_retry.setVisibility(View.VISIBLE);
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext, 2).addToRequestQueue(jsonObjReq);
        } else {
            if (mContext != null) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
            }
            loadMoreProgressBar.setVisibility(View.GONE);
            btn_retry.setVisibility(View.VISIBLE);
        }
    }

    private void parseJsonObjectFeedList(JSONObject response, boolean isClear) throws
            JSONException {
        int count = response.optInt(Constants.KeyPair.KEY_COUNT);
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        ArrayList<NewsListInfo> itemList = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), NewsListInfo[].class)));
        if (count < 1) count = 11;

        if (!TextUtils.isEmpty(id)) {
            CatHomeData catHomeData = new CatHomeData(itemList, count, CatHomeData.TYPE_NEWS, System.currentTimeMillis());
            if (!TextUtils.isEmpty(pageTitle))
                catHomeData.pageTitle = pageTitle;
            DataParser.getInstance().addCatHomeDataHashMap(id, catHomeData);
        }

        handleUI(isClear, itemList);
    }

    private void parseJsonObjectFeedList(CatHomeData catHomeData, boolean isClear) {
        int count = catHomeData.count;
        ArrayList<NewsListInfo> itemList = (ArrayList<NewsListInfo>) catHomeData.getArrayList(CatHomeData.TYPE_NEWS);
        if (count < 1) count = 11;
        handleUI(isClear, itemList);
    }

    private void handleUI(boolean isClear, List<NewsListInfo> list) {
        //passing the data to the adapter to show.
        if (showCount > 0 && showCount < list.size()) {
            if (isClear) {
                mAdapter.clear();
            }
            mAdapter.setData(list.subList(0, showCount));
            isLoadMoreEnabled = true;

        } else {
            if (isClear) {
                mAdapter.addAndClearAll(list);
            } else
                mAdapter.setData(list);
            if (isFromHome || isFromParentHome) isLoadMoreEnabled = false;
        }


        if (isFlickerActive) {
            checkFlickerWidget(isClear);
        }

        if (isStockActive) {
            checkStockWidget(isClear);
        }

        addBanner();

        mAdapter.notifyDataSetChanged();
        // handling the load more button based on the data we got

        btn_retry.setVisibility(View.GONE);
        loadMoreLogic();
    }

    private void addBanner() {
        /*Banner*/
        List<BannerInfo> bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
        for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
            int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catHomePos);
            if (bannerPos > -1 && mAdapter.getItemCount() >= bannerPos) {
                mAdapter.add(new NewsListInfo(NewsAdapter.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)), bannerPos);
            }
        }
    }

    private void checkFlickerWidget(boolean isClear) {
        int posHome;
        String addedOn;
        if (isFromParentHome || isFromHome) {
            posHome = AppPreferences.getInstance(mContext).getIntValue(QuickPreferences.FlickerPrefs.FLICKER_HOME_POS, showCount);
            addedOn = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_ADDED_ON_HOME, "");
            if (posHome > showCount || posHome > mAdapter.getItemCount())
                return;
        } else {
            addedOn = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_ADDED_ON_MENU, "");
            posHome = AppPreferences.getInstance(mContext).getIntValue(QuickPreferences.FlickerPrefs.FLICKER_MENU_POS, 0);
            if (posHome > mAdapter.getItemCount())
                return;
        }

        JSONArray jsonElements;
        try {
            jsonElements = new JSONArray(addedOn);
            List<String> stringList = new ArrayList<>();
            for (int i = 0; i < jsonElements.length(); i++) {
                stringList.add(jsonElements.getString(i));
            }
            if (isFromParentHome && isClear && stringList.contains(categoryInfo.id)) {
                NewsListInfo listInfo = new NewsListInfo();
                listInfo.isFlicker = true;
                mAdapter.add(listInfo, posHome);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void checkStockWidget(boolean isClear) {
        int posHome;
        String addedOn;
        if (isFromParentHome || isFromHome) {
            posHome = AppPreferences.getInstance(mContext).getIntValue(QuickPreferences.StockPrefs.STOCK_HOME_POS, showCount);
            addedOn = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.StockPrefs.STOCK_ADDED_ON_HOME, "");
            if (posHome > showCount || posHome > mAdapter.getItemCount())
                return;
        } else {
            addedOn = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.StockPrefs.STOCK_ADDED_ON_MENU, "");
            posHome = AppPreferences.getInstance(mContext).getIntValue(QuickPreferences.StockPrefs.STOCK_MENU_POS, 0);
            if (posHome > mAdapter.getItemCount())
                return;
        }
        JSONArray jsonElements;
        try {
            jsonElements = new JSONArray(addedOn);
            List<String> stringList = new ArrayList<>();
            for (int i = 0; i < jsonElements.length(); i++) {
                stringList.add(jsonElements.getString(i));
            }
            if (isFromParentHome && isClear && stringList.contains(categoryInfo.id)) {
                NewsListInfo listInfo = new NewsListInfo();
                listInfo.isStockWidget = true;
                mAdapter.add(listInfo, posHome);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLoadFinish() {
        btn_retry.setVisibility(View.GONE);
        loadMoreProgressBar.setVisibility(View.GONE);
    }

    public void rewindMinHeight(String count, CategoryInfo categoryInfo) {
        Systr.println(" " + count);
        if (!TextUtils.isEmpty(count)) {
            int pixels = Integer.parseInt(count) * Constants.MIN_HEIGHT_UNIT;
            itemView.setMinimumHeight((int) AppUtils.getInstance().convertDpToPixel(pixels, mContext));
        }
    }
}
