package com.db.divya_new.common;

import android.app.Activity;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.ads.NativeListAdController2;
import com.db.data.models.BannerInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.WebBannerInfo;
import com.db.divya_new.data.CatHome2;
import com.db.util.AppPreferences;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class NewsParentViewHolderV2 extends RecyclerView.ViewHolder implements NativeListAdController2.onAdNotifyListener {

    private Activity mContext;
    private RecyclerView recyclerView;
    private NewsAdapterV2 mAdapter;

    private String categoryID;

    private boolean isStockActive;
//    private boolean isFlickerActive;

    public NewsParentViewHolderV2(Activity mContext, View itemView) {
        super(itemView);
        this.mContext = mContext;

        isStockActive = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.StockPrefs.STOCK_IS_ACTIVE, false);
//        isFlickerActive = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.FlickerPrefs.FLICKER_IS_ACTIVE, false);

        mAdapter = new NewsAdapterV2(this.mContext);

        recyclerView = itemView.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onNotifyAd() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void setData(CatHome2 catHome) {
        categoryID = catHome.id;

        mAdapter.setCategoryInfo(catHome.id, catHome.color);
        recyclerView.setAdapter(mAdapter);

        handleUI(catHome);
    }

    private void handleUI(CatHome2 catHome2) {
        mAdapter.clear();
        mAdapter.setData(catHome2.list);
//        mAdapter.notifyDataSetChanged();

        if (catHome2.flickerActive && AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.FlickerPrefs.FLICKER_IS_ACTIVE, false)) {
            checkFlickerWidget();
        }

        if (isStockActive) {
            checkStockWidget();
        }


        addBanner();

        addWebBanner(catHome2.webBannerInfos);

        //addButtonTopWidget();

        mAdapter.notifyDataSetChanged();
    }

    private void addButtonTopWidget() {
        String addedMenuId = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ButtonTopWGTPref.ADDED_MENU, "");
        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ButtonTopWGTPref.IS_ACTIVE, false) && addedMenuId.equalsIgnoreCase(categoryID)) {
            String bannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ButtonTopWGTPref.BANNER_URL, "");
            mAdapter.add(new NewsListInfo(NewsAdapterV2.VIEW_TYPE_WEB_BANNER, new WebBannerInfo(bannerUrl)), 0);
        }
    }

    private void addBanner() {
        /*Banner*/
        try {
            List<BannerInfo> bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryID);
//            try {
//                Collections.sort(bannerInfoList, (o1, o2) -> Integer.compare(Integer.parseInt(o1.catHomePos), Integer.parseInt(o2.catHomePos)));
//            } catch (Exception ignored) {
//
//            }
            for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
                int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catHomePos);
                if (bannerPos > -1 && mAdapter.getItemCount() >= bannerPos) {
                    mAdapter.add(new NewsListInfo(NewsAdapterV2.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)), bannerPos);
                }
            }
        } catch (Exception e) {
        }
    }

    public void addWebBanner(ArrayList<WebBannerInfo> webBannerInfoList) {
        for (int bannerIndex = 0; bannerIndex < webBannerInfoList.size(); bannerIndex++) {
            int bannerPos = Integer.parseInt(webBannerInfoList.get(bannerIndex).position);
            if (bannerPos > -1 && mAdapter.getItemCount() >= bannerPos) {
                mAdapter.add(new NewsListInfo(NewsAdapterV2.VIEW_TYPE_WEB_BANNER, webBannerInfoList.get(bannerIndex)), bannerPos);
            }
        }
    }


    private void checkFlickerWidget() {
        int posHome;
        String addedOn;
        posHome = AppPreferences.getInstance(mContext).getIntValue(QuickPreferences.FlickerPrefs.FLICKER_HOME_POS, 0);
        addedOn = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_ADDED_ON_HOME, "");
        if (posHome > mAdapter.getItemCount())
            return;

        JSONArray jsonElements;
        try {
            jsonElements = new JSONArray(addedOn);
            List<String> stringList = new ArrayList<>();
            for (int i = 0; i < jsonElements.length(); i++) {
                stringList.add(jsonElements.getString(i));
            }
            if (stringList.contains(categoryID)) {
                NewsListInfo listInfo = new NewsListInfo();
                listInfo.isFlicker = true;
                mAdapter.add(listInfo, posHome);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkStockWidget() {
        int posHome;
        String addedOn;
        posHome = AppPreferences.getInstance(mContext).getIntValue(QuickPreferences.StockPrefs.STOCK_HOME_POS, 0);
        addedOn = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.StockPrefs.STOCK_ADDED_ON_HOME, "");
        if (posHome > mAdapter.getItemCount())
            return;
        JSONArray jsonElements;
        try {
            jsonElements = new JSONArray(addedOn);
            List<String> stringList = new ArrayList<>();
            for (int i = 0; i < jsonElements.length(); i++) {
                stringList.add(jsonElements.getString(i));
            }
            if (stringList.contains(categoryID)) {
                NewsListInfo listInfo = new NewsListInfo();
                listInfo.isStockWidget = true;
                mAdapter.add(listInfo, posHome);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
