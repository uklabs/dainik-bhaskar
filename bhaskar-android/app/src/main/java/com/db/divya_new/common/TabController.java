package com.db.divya_new.common;

import com.db.home.NewsFragment;
import com.db.util.Systr;

public class TabController {
    private static TabController tabController;
    private MenuTabController menuTabController;
    private SubMenuTabController subMenuTabController;
    private PreferredCitySearchRefresh preferredCitySearchRefresh;

    public static TabController getInstance() {
        if (tabController == null) {
            synchronized (TabController.class) {
                if (tabController == null) {
                    tabController = new TabController();
                }
            }
        }
        return tabController;
    }

    public boolean isTabClicked() {
        return false;
    }

    public void setTabClicked(boolean tabClicked) {
        if (!tabClicked) {
            NewsFragment.child_id = "";
        }
        Systr.println("TabClick: setTabClicked: " + tabClicked);
    }

    public TabController.MenuTabController getMenuTabControllerInstance() {
        return menuTabController;
    }

    public TabController.PreferredCitySearchRefresh getPreferredCitySearchRefresh() {
        return preferredCitySearchRefresh;
    }

    public void setPreferredCitySearchRefresh(TabController.PreferredCitySearchRefresh ppreferredCitySearchRefresh) {
        preferredCitySearchRefresh = ppreferredCitySearchRefresh;
    }

    public TabController.SubMenuTabController getSubMenuTabControllerInstance() {
        return subMenuTabController;
    }

    public void setMenuTabController(TabController.MenuTabController pMenuTabController) {
        menuTabController = pMenuTabController;
    }

    public void setSubMenuTabController(TabController.SubMenuTabController pSubMenuTabController) {
        subMenuTabController = pSubMenuTabController;
    }

    public interface MenuTabController {
        void onMenuTabClick(String id, String childId, LoadMoreController controller);

        void onMenuTabClick(String id, LoadMoreController controller);

        void onMenuTabClick(String id);
    }

    public interface PreferredCitySearchRefresh {
        void RefreshList();
    }

    public interface SubMenuTabController {
        void onSubMenuTabClick(String parentId);

        void onSubMenuTabClick(String parentId, LoadMoreController controller);
    }
}
