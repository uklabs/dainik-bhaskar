package com.db.divya_new.common.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

public class BlankViewHolder extends RecyclerView.ViewHolder {

    public BlankViewHolder(View itemView) {
        super(itemView);
    }
}
