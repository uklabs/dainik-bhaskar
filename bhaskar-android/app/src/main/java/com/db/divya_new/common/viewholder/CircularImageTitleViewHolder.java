package com.db.divya_new.common.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.R;

public class CircularImageTitleViewHolder extends RecyclerView.ViewHolder {

    public RelativeLayout rootview;
    public TextView titleTextView;
    public ImageView iconImageView;

    public CircularImageTitleViewHolder(View itemView) {
        super(itemView);

        rootview = itemView.findViewById(R.id.rootview);
        titleTextView = itemView.findViewById(R.id.tv_title);
        iconImageView = itemView.findViewById(R.id.iv_pic);
    }
}
