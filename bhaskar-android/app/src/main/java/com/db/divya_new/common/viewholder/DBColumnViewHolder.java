package com.db.divya_new.common.viewholder;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Handler;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.DBColumnInfo;
import com.db.data.models.StoryListInfo;
import com.db.divya_new.common.DBColumnListAdapter;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.LoadMoreController;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHomeData;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.NetworkStatus;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DBColumnViewHolder extends RecyclerView.ViewHolder implements LoadMoreController {

    public RecyclerView recyclerView;
    public DBColumnListAdapter mAdapter;
    public Button btn_retry;
    private String mSuperParentId;
    private Activity mContext;
    private String finalFeedURL;
    private int showCount = 0;
    private boolean mIsFromHome;
    private boolean isLoading;
    private boolean isFromParentHome;
    private String parentId;
    private ProgressBar loadMoreProgressBar;
    private String pageTitle = null;
    private String id;
    private TextView tvTitle;
    private Button btnHeaderLoadMore;
    private Button btnSubHeaderLoadMore;
    private boolean isRefreshing;

    public DBColumnViewHolder(View itemView, Activity activity) {
        super(itemView);
        this.mContext = activity;
        tvTitle = itemView.findViewById(R.id.tv_title);
        btnHeaderLoadMore = itemView.findViewById(R.id.btnHeaderLoadMore);
        btnSubHeaderLoadMore = itemView.findViewById(R.id.btnSubHeaderLoadMore);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        btn_retry = itemView.findViewById(R.id.btn_retry);
        loadMoreProgressBar.setVisibility(View.GONE);
        recyclerView = itemView.findViewById(R.id.recycler_view);

        mAdapter = new DBColumnListAdapter(mContext);

        btn_retry.setOnClickListener(view -> {
            if (!isLoading) {
                btn_retry.setVisibility(View.INVISIBLE);
                fetchData();
            }
        });

    }

    private void onLoadMore() {
        if (mIsFromHome && isFromParentHome) {
            if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                new Handler().postDelayed(() -> {
                    if (TabController.getInstance().getMenuTabControllerInstance() != null) {
                        if (mSuperParentId.equalsIgnoreCase("0")) {
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(parentId);
                        } else
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mSuperParentId);
                    }
                    loadMoreProgressBar.setVisibility(View.GONE);
                }, 500);
            }
        } else if (mIsFromHome) {
            if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(parentId);
            }
        }
    }

    private void loadMoreLogic() {
        if (isFromParentHome) {
            btnHeaderLoadMore.setVisibility(View.VISIBLE);
            btnSubHeaderLoadMore.setVisibility(View.GONE);
        } else if (mIsFromHome) {
            btnSubHeaderLoadMore.setVisibility(View.VISIBLE);
            btnHeaderLoadMore.setVisibility(View.GONE);
        } else {
            //infinite loading true
            btnHeaderLoadMore.setVisibility(View.GONE);
            btnSubHeaderLoadMore.setVisibility(View.GONE);
        }

        btnHeaderLoadMore.setOnClickListener(v -> onLoadMore());
        btnSubHeaderLoadMore.setOnClickListener(v -> onLoadMore());
    }

    public void clearDataAndLoadNew() {
        mAdapter.clear();
        mAdapter.notifyDataSetChanged();
    }

    public void setPageTitle(String pageTitle) {
        if (!TextUtils.isEmpty(pageTitle)) {
            this.pageTitle = pageTitle;
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(pageTitle);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setGravity(Gravity.CENTER);
            tvTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            tvTitle.setVisibility(View.GONE);
        }
    }

    // Adding the title is available in the categoryInfo else it will be gone
    private void setUpPageTitle(String extraVlues) {
        String pageTitle = null;
        if (!TextUtils.isEmpty(extraVlues)) {
            try {
                if (new JSONObject(extraVlues).has("category_intro")) {
                    pageTitle = new JSONObject(extraVlues).optString("category_intro");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.pageTitle = pageTitle;

            setPageTitle(pageTitle);
        }
    }

    public void setData(CategoryInfo categoryInfo, boolean mIsFromHome, boolean isFromParentHome) {
        this.mIsFromHome = mIsFromHome;
        this.isFromParentHome = isFromParentHome;
        this.parentId = categoryInfo.id;
        this.id = categoryInfo.id;
        this.mSuperParentId = categoryInfo.parentId;
        this.showCount = 0;

        recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false)/*new GridLayoutManager(itemView.getContext(), AppUtils.getSpan())*/);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setData(categoryInfo, mIsFromHome);

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue())) {
            if (!TextUtils.isEmpty(catHomeData.pageTitle)) {
                setPageTitle(catHomeData.pageTitle);
            }
            parseOfflineData(catHomeData, true);
        } else {
            setUpPageTitle(categoryInfo.extraVlues);
            fetchData();
        }
    }


    private void fetchData() {
        finalFeedURL = String.format(Urls.APP_DB_COLUMN_URL, CommonConstants.CHANNEL_ID);
        btn_retry.setVisibility(View.GONE);
        makeJsonObjectRequest();
    }

    private void makeJsonObjectRequest() {
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            if (!isRefreshing)
                loadMoreProgressBar.setVisibility(View.VISIBLE);
            isLoading = true;
            Systr.println(String.format("Feed API  %s", finalFeedURL));
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        Systr.println(String.format("Feed API %s Response params %s", finalFeedURL, response.toString()));
                        loadMoreProgressBar.setVisibility(View.GONE);
                        try {
                            if (!TextUtils.isEmpty(response.toString())) {
                                parseOnlineData(response);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        isLoading = false;
                    }, error -> {
                Systr.println(String.format("Feed API %s Error params %s", finalFeedURL, error.getMessage()));
                Activity activity = (Activity) mContext;
                if (activity != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(activity, mContext.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(activity, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }

                isLoading = false;
                loadMoreProgressBar.setVisibility(View.GONE);

                if (isFromParentHome || mIsFromHome)
                    btn_retry.setVisibility(View.VISIBLE);
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            if (!mIsFromHome)
                VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
            else
                VolleyNetworkSingleton.getInstance(mContext, 2).addToRequestQueue(jsonObjReq);
        } else {
            if (mContext != null) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
            }
            isLoading = false;
            loadMoreProgressBar.setVisibility(View.GONE);
            if (isFromParentHome || mIsFromHome)
                btn_retry.setVisibility(View.VISIBLE);
        }
    }

    private void parseOnlineData(JSONObject jsonObject) {
        JSONArray jsonArray = jsonObject.optJSONArray("feed");
        if (jsonArray != null) {
            ArrayList<DBColumnInfo> mList = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), DBColumnInfo[].class)));
            ArrayList<StoryListInfo> list = mList.get(0).feed;

            if (!TextUtils.isEmpty(id)) {
                CatHomeData catHomeData = new CatHomeData(list, 0, CatHomeData.TYPE_DB_COLUMN, System.currentTimeMillis());
                if (!TextUtils.isEmpty(pageTitle)) {
                    catHomeData.pageTitle = pageTitle;
                }
                DataParser.getInstance().addCatHomeDataHashMap(id, catHomeData);
            }

            handleUI(list);
        }
    }

    private void parseOfflineData(CatHomeData catHomeData, boolean isClear) {
        ArrayList<StoryListInfo> list = (ArrayList<StoryListInfo>) catHomeData.getArrayList(CatHomeData.TYPE_DB_COLUMN);
        if (list != null) {

            handleUI(list);
        }
    }

    private void handleUI(ArrayList<StoryListInfo> list) {

        //passing the data to the adapter to show.
        if (showCount > 0 && showCount < list.size()) {
            mAdapter.clear();
            for (int i = 0; i < showCount; i++) {
                mAdapter.add(list.get(i));
            }
        } else {
            mAdapter.addAndClearAll(list);
        }

        btn_retry.setVisibility(View.GONE);
        loadMoreLogic();
    }

    @Override
    public void onLoadFinish() {
        btn_retry.setVisibility(View.GONE);
        loadMoreProgressBar.setVisibility(View.GONE);
    }

    public void onRefresh() {
        if (!isLoading)
            fetchData();
    }


    public void setIsRefreshing(boolean isRefreshing) {
        this.isRefreshing = isRefreshing;
    }
}
