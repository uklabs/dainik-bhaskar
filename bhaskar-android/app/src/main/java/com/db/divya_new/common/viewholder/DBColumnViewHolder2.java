package com.db.divya_new.common.viewholder;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.bhaskar.R;
import com.db.divya_new.common.DBColumnListAdapterV2;
import com.db.divya_new.data.CatHome2;

public class DBColumnViewHolder2 extends RecyclerView.ViewHolder {

    private DBColumnListAdapterV2 mAdapter;

    public DBColumnViewHolder2(View itemView, Context context) {
        super(itemView);
        RecyclerView recyclerView = itemView.findViewById(R.id.recycler_view);

        mAdapter = new DBColumnListAdapterV2(context);
        recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false));
        recyclerView.setAdapter(mAdapter);
    }

    public void setData(CatHome2 catHome) {
        mAdapter.setData(catHome);
    }
}
