package com.db.divya_new.common.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.bhaskar.R;

public class LoadMoreViewHolder extends RecyclerView.ViewHolder {
    public Button btnLoadMore;
    public LoadMoreViewHolder(View itemView) {
        super(itemView);
        btnLoadMore = itemView.findViewById(R.id.btn_load_more);
    }
}
