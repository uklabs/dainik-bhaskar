package com.db.divya_new.common.viewholder;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.RajyaInfo;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHome;
import com.db.divya_new.data.CatHomeData;
import com.db.news.rajya.RajyaNewsAdapter;
import com.db.util.AppLogs;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RajyaCommonCatHomeViewHolder extends RecyclerView.ViewHolder {
    public static final String TAG = RajyaCommonCatHomeViewHolder.class.getSimpleName();
    public TextView tvHeading;
    public Context context;
    public Button btnHeaderLoadMore;
    private Button btnRetry;
    private RajyaNewsAdapter newsListAdapter;
    private RajyaInfo rajyaInfo;
    private ArrayList<Object> rajyaNewsInfoList = new ArrayList<>();
    private ProgressBar progressBar;
    private int LOAD_MORE_COUNT = 1;

    private boolean isLoading;
    private String mParentId;
    private String mID;
    private String mSuperParentId;

    public RajyaCommonCatHomeViewHolder(View itemView, CategoryInfo categoryInfo) {
        super(itemView);
        context = itemView.getContext();
        btnRetry = itemView.findViewById(R.id.btn_retry);
        progressBar = itemView.findViewById(R.id.progressBar);
        btnHeaderLoadMore = itemView.findViewById(R.id.btnHeaderLoadMore);
        btnHeaderLoadMore.setVisibility(View.VISIBLE);
        View vDivider = itemView.findViewById(R.id.v_divider);
        String themeColor = categoryInfo.color;
        vDivider.setBackgroundColor(AppUtils.getThemeColor(context, themeColor));
        btnHeaderLoadMore.setTextColor(AppUtils.getThemeColor(context, themeColor));
        tvHeading = itemView.findViewById(R.id.tv_heading);
        RecyclerView newsRecyclerView = itemView.findViewById(R.id.recycler_view_news);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        newsRecyclerView.setLayoutManager(linearLayoutManager);
        SpacesDecoration dividerItemDecoration = new SpacesDecoration(0, 0, 2, 0);
        newsRecyclerView.addItemDecoration(dividerItemDecoration);
        newsRecyclerView.setNestedScrollingEnabled(true);
        newsListAdapter = new RajyaNewsAdapter(context, rajyaNewsInfoList, false);

        newsRecyclerView.setAdapter(newsListAdapter);
        btnHeaderLoadMore.setOnClickListener(v -> {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    try {
//                        TabController.getInstance().setTabClicked(true);
//                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mSuperParentId, mParentId, null);
//                    } catch (Exception e) {
//                    }
                    if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                        TabController.getInstance().setTabClicked(true);
                        if (TextUtils.isEmpty(mSuperParentId) || mSuperParentId.equalsIgnoreCase("0")) {
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mID);
                        } else {
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mSuperParentId, mID, null);
                        }
                    }
                }
            }, 500);
        });

        btnRetry.setOnClickListener(v -> {
            btnRetry.setVisibility(View.GONE);
            makeJsonObjectRequest(rajyaInfo);
        });
    }

    private void clearContent() {
        this.rajyaNewsInfoList.clear();
        newsListAdapter.notifyDataSetChanged();
    }


    public void setData(CatHome catHome) {
        CategoryInfo categoryInfo = catHome.categoryInfo;
        mID = catHome.id;
        mParentId = catHome.parentId;
        mSuperParentId = catHome.superParentId;
        this.rajyaInfo = new RajyaInfo(categoryInfo.id, categoryInfo.menuName, categoryInfo.feedUrl, categoryInfo.detailUrl, categoryInfo.gaScreen, categoryInfo.gaArticle, categoryInfo.gaEventLabel, categoryInfo.displayName);
        newsListAdapter.setData(rajyaInfo, categoryInfo);
        this.LOAD_MORE_COUNT = 1;
        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(rajyaInfo.id + rajyaInfo.displayName);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && LOAD_MORE_COUNT <= 2) {
            parseOfflineData(catHomeData, rajyaInfo);
        } else if (!isLoading) {
            makeJsonObjectRequest(rajyaInfo);
        }
    }

    private void parseOfflineData(CatHomeData catHomeData, RajyaInfo rajyaInfo) {
        List<Object> newsListInfos = new ArrayList<>(catHomeData.getArrayList(CatHomeData.TYPE_RAJYA_NEWS));
        if (!newsListInfos.isEmpty()) {
            //passing the data to the adapter to show.
            newsListAdapter.addItems(newsListInfos, true);
        } else {
            makeJsonObjectRequest(rajyaInfo);
        }
    }


    private void makeJsonObjectRequest(RajyaInfo rajyaInfo) {
        if (NetworkStatus.getInstance().isConnected(context)) {
            isLoading = true;
            String finalFeedURL = createFeedUrl(rajyaInfo);
            progressBar.setVisibility(View.VISIBLE);
            AppLogs.printDebugLogs(TAG + "Rajya Feed URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalFeedURL, null,
                    response -> {
                        progressBar.setVisibility(View.GONE);
                        btnRetry.setVisibility(View.GONE);
                        isLoading = false;
                        onResponseFromServer(response, rajyaInfo);
                    }, error -> {
                isLoading = false;
                progressBar.setVisibility(View.GONE);
                if (newsListAdapter.getItemCount() == 0) {
                    btnRetry.setVisibility(View.VISIBLE);
                }
                LOAD_MORE_COUNT--;
                if (context != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);

        } else {
            if (context != null) {
                AppUtils.getInstance().showCustomToast(context, context.getString(R.string.no_network_error));
                if (newsListAdapter.getItemCount() == 0) {
                    btnRetry.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private String createFeedUrl(RajyaInfo rajyaInfo) {
        //rajya
        return Urls.APP_FEED_BASE_URL + rajyaInfo.feedUrl + "PG" + LOAD_MORE_COUNT + "/";
    }


    private void onResponseFromServer(JSONObject response, RajyaInfo rajyaInfo) {
        Log.v(TAG, response + "");
        try {
            if (response != null) {
                JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
                int length = resultArray.length();
                int count = response.getInt("count");

                if (length > 0) {
                    Gson gson = new Gson();
                    Type founderListType = new TypeToken<ArrayList<NewsListInfo>>() {
                    }.getType();
                    ArrayList<Object> founderList = gson.fromJson(String.valueOf(resultArray), founderListType);
                    ArrayList<Object> newsListInfos;
                    if (founderList.size() > 5) {
                        newsListInfos = new ArrayList<>(founderList.subList(0, 5));
                    } else {
                        newsListInfos = founderList;
                    }
                    //rajyaNewsInfoList.addAll(founderList);
                    // newsListAdapter.addItems(founderList,true);
                    newsListAdapter.addItems(newsListInfos, true);
                    if (LOAD_MORE_COUNT <= 1 && !TextUtils.isEmpty(rajyaInfo.id)) {
                        CatHomeData catHomeData = new CatHomeData(newsListInfos, count, CatHomeData.TYPE_RAJYA_NEWS, System.currentTimeMillis());
                        DataParser.getInstance().addCatHomeDataHashMap(rajyaInfo.id + rajyaInfo.displayName, catHomeData);
                    }

                    if (length >= count) {
                        LOAD_MORE_COUNT++;
                    }

                } else {
                    if (LOAD_MORE_COUNT > 1) {
                        LOAD_MORE_COUNT--;
                    }
                }
            }
        } catch (Exception e) {
            AppLogs.printErrorLogs(TAG + "RajyaNewsList", "Error: " + e);
            if (LOAD_MORE_COUNT > 1) {
                LOAD_MORE_COUNT--;
            }
        }
    }
}
