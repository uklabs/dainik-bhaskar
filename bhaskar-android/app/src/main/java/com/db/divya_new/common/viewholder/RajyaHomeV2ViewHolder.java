package com.db.divya_new.common.viewholder;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.RajyaInfo;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.LoadMoreController;
import com.db.divya_new.data.CatHomeData;
import com.db.news.rajya.RajyaHomeNewsAdapter;
import com.db.util.AppLogs;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RajyaHomeV2ViewHolder extends RecyclerView.ViewHolder implements LoadMoreController {
    public static final String TAG = RajyaHomeV2ViewHolder.class.getSimpleName();
    public Context context;
    private boolean isLoading;
    private RajyaHomeNewsAdapter newsListAdapter;
    private RajyaInfo rajyaInfo;
    private ArrayList<Object> rajyaNewsInfoList = new ArrayList<>();
    private int LOAD_MORE_COUNT = 1;
    private ProgressBar progressBar;
    private CategoryInfo categoryInfo;
    private Button btnRetry;
    private int showCount;

    public RajyaHomeV2ViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        RecyclerView newsRecyclerView = itemView.findViewById(R.id.recycler_view);
        btnRetry = itemView.findViewById(R.id.btn_retry);

        progressBar = itemView.findViewById(R.id.progress_bar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        newsRecyclerView.setLayoutManager(linearLayoutManager);
        SpacesDecoration dividerItemDecoration = new SpacesDecoration(0, 0, 2, 0);
        newsRecyclerView.addItemDecoration(dividerItemDecoration);
        newsListAdapter = new RajyaHomeNewsAdapter(context, rajyaNewsInfoList, true);
        newsRecyclerView.setAdapter(newsListAdapter);

        btnRetry.setOnClickListener(view -> {
            if (!isLoading) {
                btnRetry.setVisibility(View.GONE);
                hitapi();
            }
        });
    }


    private void makeJsonObjectRequest(RajyaInfo rajyaInfo) {
        if (NetworkStatus.getInstance().isConnected(context)) {
            isLoading = true;
            String finalFeedURL = createFeedUrl(rajyaInfo);
            progressBar.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.GONE);

            AppLogs.printDebugLogs(TAG + "Rajya Feed URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalFeedURL, null,
                    response -> {
                        progressBar.setVisibility(View.GONE);
                        isLoading = false;
                        onResponseFromServer(response);
                    }, error -> {
                AppLogs.printVolleyLogs(TAG + "Rajya newsList", "Error: " + error.getMessage());
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                if (context != null) {
                    if (error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
                if (newsListAdapter.getItemCount() == 0)
                    btnRetry.setVisibility(View.VISIBLE);
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);

        } else {
            if (context != null) {
                AppUtils.getInstance().showCustomToast(context, context.getString(R.string.no_network_error));
            }
            if (newsListAdapter.getItemCount() == 0) {
                btnRetry.setVisibility(View.VISIBLE);
            }
            isLoading = false;
            progressBar.setVisibility(View.GONE);
        }
    }

    private String createFeedUrl(RajyaInfo rajyaInfo) {
        return Urls.APP_FEED_BASE_URL + rajyaInfo.feedUrl + "PG" + LOAD_MORE_COUNT + "/";
    }


    private void onResponseFromServer(JSONObject response) {
        Log.v(TAG, response + "");
        try {
            if (response != null) {
                JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
                int length = resultArray.length();
                int count = response.getInt("count");
                if (length > 0) {
                    rajyaNewsInfoList.clear();
                    List<NewsListInfo> founderList = Arrays.asList(new Gson().fromJson(String.valueOf(resultArray), NewsListInfo[].class));//new ArrayList<>(gson.fromJson(String.valueOf(resultArray), founderListType));
                    newsListAdapter.setData(rajyaInfo, categoryInfo);
                    //for search view holder
                    if (founderList.size() > showCount) {
                        rajyaNewsInfoList.addAll(founderList.subList(0, showCount));
                    } else {
                        rajyaNewsInfoList.addAll(founderList);
                    }
                    addBanner();
                    rajyaNewsInfoList.add(0, new NewsListInfo(RajyaHomeNewsAdapter.VIEW_TYPE_SEARCH));

                    newsListAdapter.addItems(rajyaNewsInfoList, true);
                    //save for offline
                    if (LOAD_MORE_COUNT <= 1 && !TextUtils.isEmpty(categoryInfo.id)) {
                        CatHomeData catHomeData = new CatHomeData(rajyaNewsInfoList, count, CatHomeData.TYPE_RAJYA_NEWS, System.currentTimeMillis());
                        DataParser.getInstance().addCatHomeDataHashMap(categoryInfo.id, catHomeData);
                    }

                    LOAD_MORE_COUNT++;
                } else {
                    if (LOAD_MORE_COUNT > 1) {
                        LOAD_MORE_COUNT--;
                    }
                }

                // handling the load more button based on the data we got
                //loadMoreButton.setVisibility(View.VISIBLE);

            } else {
                if (newsListAdapter.getItemCount() == 0)
                    btnRetry.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            AppLogs.printErrorLogs(TAG + "RajyaNewsList", "Error: " + e);
            if (LOAD_MORE_COUNT > 1) {
                LOAD_MORE_COUNT--;
            }
            if (newsListAdapter.getItemCount() == 0)
                btnRetry.setVisibility(View.VISIBLE);
        }
    }

    private void addBanner() {
//        if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
            /*Banner*/
            List<BannerInfo> bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
            for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
                int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catHomePos);
                if (bannerPos > -1 && rajyaNewsInfoList.size() >= bannerPos) {
                    rajyaNewsInfoList.add(bannerPos, new NewsListInfo(RajyaHomeNewsAdapter.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)));
                }
            }
//        }
    }

    public void setData(CategoryInfo categoryInfo, int count) {
        this.categoryInfo = categoryInfo;
        this.showCount = count;
        this.rajyaInfo = new RajyaInfo(categoryInfo.id, categoryInfo.menuName, categoryInfo.feedUrl, categoryInfo.detailUrl, categoryInfo.gaScreen, categoryInfo.gaArticle, categoryInfo.gaEventLabel, categoryInfo.displayName);
        LOAD_MORE_COUNT = 1;

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(categoryInfo.id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && LOAD_MORE_COUNT <= 2) {
            parseOfflineData(catHomeData);
            //LOAD_MORE_COUNT = 2;
        } else if (!isLoading) {
            hitapi();
        }
    }


    private void hitapi() {
        LOAD_MORE_COUNT = 1;
        makeJsonObjectRequest(this.rajyaInfo);
    }


    private void parseOfflineData(CatHomeData catHomeData) {
        List<Object> newsListInfos = new ArrayList<>(catHomeData.getArrayList(CatHomeData.TYPE_RAJYA_NEWS));
        if (!newsListInfos.isEmpty()) {
            //passing the data to the adapter to show.
            newsListAdapter.setData(rajyaInfo, categoryInfo);
            newsListAdapter.addItems(newsListInfos, true);
        } else {
            hitapi();
        }
    }


    @Override
    public void onLoadFinish() {

    }

    public void rewindMinHeight(String count) {
        Systr.println(" " + count);
        if (!TextUtils.isEmpty(count)) {
            int pixels = Integer.parseInt(count) * Constants.MIN_HEIGHT_UNIT;
            itemView.setMinimumHeight((int) AppUtils.getInstance().convertDpToPixel(pixels, context));
        }
    }
}
