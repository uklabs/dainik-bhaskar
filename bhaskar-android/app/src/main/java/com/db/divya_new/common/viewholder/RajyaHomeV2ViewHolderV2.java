package com.db.divya_new.common.viewholder;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.BannerInfo;
import com.db.data.models.NewsListInfo;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.divya_new.data.CatHome2;
import com.db.news.rajya.RajyaHomeNewsAdapterV2;
import com.db.util.JsonParser;

import java.util.ArrayList;
import java.util.List;

public class RajyaHomeV2ViewHolderV2 extends RecyclerView.ViewHolder {
    private Context context;
    private RajyaHomeNewsAdapterV2 newsListAdapter;
    private ArrayList<Object> rajyaNewsInfoList;

    private String categoryID;
    private String color;

    public RajyaHomeV2ViewHolderV2(View itemView) {
        super(itemView);
        context = itemView.getContext();
        RecyclerView newsRecyclerView = itemView.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        newsRecyclerView.setLayoutManager(linearLayoutManager);
        SpacesDecoration dividerItemDecoration = new SpacesDecoration(0, 0, 2, 0);
        newsRecyclerView.addItemDecoration(dividerItemDecoration);
        rajyaNewsInfoList = new ArrayList<>();
        newsListAdapter = new RajyaHomeNewsAdapterV2(context, rajyaNewsInfoList );
        newsRecyclerView.setAdapter(newsListAdapter);
    }

    private void handleUI(List<Object> mList) {
        rajyaNewsInfoList.clear();
        newsListAdapter.setData(categoryID, color);
        //for search view holder
        rajyaNewsInfoList.addAll(mList);
        addBanner();
        rajyaNewsInfoList.add(0, new NewsListInfo(RajyaHomeNewsAdapterV2.VIEW_TYPE_SEARCH));

        newsListAdapter.addItems(rajyaNewsInfoList, true);
    }

    private void addBanner() {
        /*Banner*/
        List<BannerInfo> bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryID);
        for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
            int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catHomePos);
            if (bannerPos > -1 && rajyaNewsInfoList.size() >= bannerPos) {
                rajyaNewsInfoList.add(bannerPos, new NewsListInfo(RajyaHomeNewsAdapterV2.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)));
            }
        }
    }

    public void setData(CatHome2 catHome) {
        this.categoryID = catHome.id;
        this.color = catHome.color;
        handleUI(catHome.list);
    }
}
