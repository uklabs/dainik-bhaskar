package com.db.divya_new.common.viewholder;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.database.CityFeedListTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.models.CategoryInfo;
import com.db.data.models.CityInfo;
import com.db.data.models.RelatedArticleInfo;
import com.db.data.source.remote.SerializeData;
import com.db.divya_new.common.TabController;
import com.bhaskar.util.LoginController;
import com.bhaskar.data.model.ProfileInfo;
import com.db.news.PreferredCityAdapter;
import com.db.news.PrefferedCityNewsListFragment;
import com.db.preferredcity.CityFeedListAdapter;
import com.db.preferredcity.NamesAdapter;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class SearchPreferredCityViewHolder extends RecyclerView.ViewHolder implements PreferredCityAdapter.CityListItemClickListener {

    private final String TAG = AppConfig.BaseTag + "." + PrefferedCityNewsListFragment.class.getSimpleName();
    public AutoCompleteTextView selectCityAutocomplete;
    InitApplication application;
    private CityFeedListAdapter cityFeedListAdapter;
    private RecyclerView cityFeedListRecyclerView;
    private LinearLayoutManager linearLayoutManager1;
    private PreferredCityAdapter preferredCityAdapter;
    private RecyclerView preferredCityRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private RelativeLayout layoutLoadingItem;
    private List<CityInfo> cityInfoArrayList;
    private Activity mContext;
    private String mParentId;
    private CategoryInfo categoryInfo;
    private int showCount;
    private Vector<CityInfo> cityInfoVector;
    private NamesAdapter cityAutoCompleteListAdapter;

    private int selectedPosition;
    private CityInfo cityInfo;
    private String gaArticle;
    private String gaScreen;
    private String displayName;
    private String gaEventLabel;
    private int LOAD_MORE_COUNT = 0;
    private String finalFeedURL;
    private TextView noNewsAvailable;
    private int listIndex = 0;

    private List<RelatedArticleInfo> cityFeedList = new ArrayList<>();
    private boolean isLoading;
    private String pageTitle;
    private RelativeLayout searchRelativeLayout;
    private View vSearchSubtitle;
    private View vEditTextSearch;
    private View vEditSearch;
    private View vSearch;

    public Button btnHeaderLoadMore;

    public SearchPreferredCityViewHolder(View itemView, Activity activity) {
        super(itemView);

        mContext = activity;
        itemView.findViewById(R.id.add_city_iv).setVisibility(View.GONE);
        selectCityAutocomplete = itemView.findViewById(R.id.select_city_autocomplete);
        vSearchSubtitle = itemView.findViewById(R.id.vSubTitle);
        vSearch = itemView.findViewById(R.id.vSearch);
        vEditSearch = itemView.findViewById(R.id.vEditSearch);
        vEditTextSearch = itemView.findViewById(R.id.vEditTextSearch);
        vSearchSubtitle = itemView.findViewById(R.id.vSubTitle);
        btnHeaderLoadMore = itemView.findViewById(R.id.btnHeaderLoadMore);

        //search_view icon color
        searchRelativeLayout = itemView.findViewById(R.id.v_search);
        searchRelativeLayout.setVisibility(View.GONE);
        //  searchRelativeLayout.setBackgroundColor(Color.parseColor(themeColor));
        layoutLoadingItem = itemView.findViewById(R.id.layout_loading_item);
        noNewsAvailable = itemView.findViewById(R.id.error_message_no_data);


        vSearch.setOnClickListener(v -> {
            vSearchSubtitle.setVisibility(View.GONE);
            vEditSearch.setVisibility(View.VISIBLE);
        });

        application = (InitApplication) mContext.getApplication();


        cityInfoVector = JsonParser.getInstance().analyzePreferredCity(mContext, CommonConstants.CHANNEL_ID);
        cityInfoArrayList = JsonParser.getInstance().getCityFeedList(mContext, CommonConstants.CHANNEL_ID);

        cityAutoCompleteListAdapter = new NamesAdapter(mContext, R.layout.preferred_city_auto_complete_item, new ArrayList<>(cityInfoArrayList), cityInfoVector);
        selectCityAutocomplete.setThreshold(2);
        selectCityAutocomplete.setAdapter(cityAutoCompleteListAdapter);

        selectCityAutocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long l) {
                CityInfo cityInfo = cityAutoCompleteListAdapter.getItem(pos);
                cityInfo.citySelectionValue = Constants.CityRajyaSelection.SELECTED;
                AppUtils.hideKeyboard(mContext);
                selectCityAutocomplete.setText("");
                selectCityAutocomplete.clearFocus();

                if (TabController.getInstance().getMenuTabControllerInstance() != null) {
                    TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mParentId);
                }
            }
        });

        preferredCityRecyclerView = itemView.findViewById(R.id.city_recycler_view);
        linearLayoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        preferredCityRecyclerView.setLayoutManager(linearLayoutManager);
        preferredCityRecyclerView.scrollToPosition(0);
        preferredCityRecyclerView.setNestedScrollingEnabled(true);
        preferredCityAdapter = new PreferredCityAdapter(this);
        preferredCityRecyclerView.setAdapter(preferredCityAdapter);

        cityFeedListRecyclerView = itemView.findViewById(R.id.city_feed_list_recycler_view);

        linearLayoutManager1 = new LinearLayoutManager(mContext);
        cityFeedListRecyclerView.setLayoutManager(linearLayoutManager1);

        cityFeedListRecyclerView.setHasFixedSize(true);
        cityFeedListAdapter = new CityFeedListAdapter(mContext);
        cityFeedListRecyclerView.setAdapter(cityFeedListAdapter);
        setDataToSelectedCities();
        btnHeaderLoadMore.setOnClickListener(v -> onLoadMore());

    }


    private void onLoadMore(){
        if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
            TabController.getInstance().setTabClicked(true);
            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mParentId);
        }
    }

    void overrideTheme() {
        try {
            vSearch.getBackground().clearColorFilter();
            vSearch.getBackground().setColorFilter(AppUtils.getThemeColor(mContext, categoryInfo.color), PorterDuff.Mode.MULTIPLY);
            vEditTextSearch.getBackground().clearColorFilter();
            vEditTextSearch.getBackground().setColorFilter(AppUtils.getThemeColor(mContext, categoryInfo.color), PorterDuff.Mode.MULTIPLY);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        TextView tvTitle = itemView.findViewById(R.id.tv_title);

        if (!TextUtils.isEmpty(pageTitle)) {
            this.pageTitle = pageTitle;
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(pageTitle);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                tvTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        } else {
            this.pageTitle = "";
            tvTitle.setVisibility(View.GONE);
        }
    }


    // Adding the title is available in the categoryInfo else it will be gone
    private void setUpPageTitle(String extraVlues) {
        if (TextUtils.isEmpty(pageTitle) && !TextUtils.isEmpty(extraVlues)) {
            try {
                if (new JSONObject(extraVlues).has("category_intro")) {
                    pageTitle = new JSONObject(extraVlues).optString("category_intro");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            setPageTitle(pageTitle);
        } else if (!TextUtils.isEmpty(pageTitle)) {
            setPageTitle(pageTitle);
        } else {
            setPageTitle("");
        }
    }

    private void getCityInfoVector() {

        String currentCity = TrackingData.getCity(mContext);
        String currentState = TrackingData.getState(mContext);

        if (AppUtils.getInstance().isValidString(currentCity)) {

            CityInfo cityInfoBasedOnCityName = new CityInfo(currentCity, Constants.CityPreference.CITY_INFO_BASED_ON_CITY_NAME);
            CityInfo cityInfoBasedOnStateName = new CityInfo(currentState, Constants.CityPreference.CITY_INFO_BASED_ON_STATE_NAME);

            if (cityInfoArrayList.contains(cityInfoBasedOnCityName)) {
                CityInfo cityInfo = cityInfoArrayList.get(cityInfoArrayList.indexOf(cityInfoBasedOnCityName));
                cityInfo.citySelectionValue = Constants.CityRajyaSelection.FETCHED;
                cityInfoVector.add(cityInfo);
            } else if (cityInfoArrayList.contains(cityInfoBasedOnStateName)) {
                CityInfo cityInfo = cityInfoArrayList.get(cityInfoArrayList.indexOf(cityInfoBasedOnStateName));
                cityInfo.citySelectionValue = Constants.CityRajyaSelection.FETCHED;
                cityInfoVector.add(cityInfo);
            } else {
                // Add a default option in to the City list in case of no preferred city exists.
                String cityId = AppPreferences.getInstance(mContext).getStringValue(String.format(QuickPreferences.DEFAULT_PREFERRED_CITY, CommonConstants.CHANNEL_ID), Constants.Utils.DEFAULT);
                CityInfo cityInfoBasedOnCityId = new CityInfo(cityId, Constants.CityPreference.CITY_INFO_BASED_ON_ID);

                if (cityInfoBasedOnCityId != null && cityInfoArrayList.contains(cityInfoBasedOnCityId)) {
                    CityInfo cityInfo = cityInfoArrayList.get(cityInfoArrayList.indexOf(cityInfoBasedOnCityId));
                    cityInfo.citySelectionValue = Constants.CityRajyaSelection.DEFAULT;
                    cityInfoVector.add(cityInfo);
                }
            }
        } else {
            // Add a default option in to the City list in case of no preferred city exists.
            String cityId = AppPreferences.getInstance(mContext).getStringValue(String.format(QuickPreferences.DEFAULT_PREFERRED_CITY, CommonConstants.CHANNEL_ID), Constants.Utils.DEFAULT);
            CityInfo cityInfoBasedOnCityId = new CityInfo(cityId, Constants.CityPreference.CITY_INFO_BASED_ON_ID);

            if (cityInfoBasedOnCityId != null && cityInfoArrayList.contains(cityInfoBasedOnCityId)) {
                CityInfo cityInfo = cityInfoArrayList.get(cityInfoArrayList.indexOf(cityInfoBasedOnCityId));
                cityInfo.citySelectionValue = Constants.CityRajyaSelection.DEFAULT;
                cityInfoVector.add(cityInfo);
            }
        }

        // save the default value in the Vector
        SerializeData.getInstance(mContext).setSelectedCityList(CommonConstants.CHANNEL_ID, cityInfoVector);

        cityAutoCompleteListAdapter = new NamesAdapter(mContext, R.layout.preferred_city_auto_complete_item, new ArrayList<>(cityInfoArrayList), cityInfoVector);
        selectCityAutocomplete.setThreshold(2);
        selectCityAutocomplete.setAdapter(cityAutoCompleteListAdapter);

        preferredCityAdapter.setCityInfoVector(cityInfoVector);

        // Save the selected City to the Serialized Data.
        if (cityInfoVector.size() > 0) {
            AppPreferences.getInstance(mContext).setStringValue(String.format(QuickPreferences.SELECTED_PREF_CITY, CommonConstants.CHANNEL_ID), cityInfoVector.get(0).cityId);
        }
        int lastSelectionPosition = getLastSavedCityIndex();
        updateGADetails(lastSelectionPosition);
        preferredCityAdapter.itemSelection(lastSelectionPosition);
    }

    public void setData(final String catId, String title, String color, CategoryInfo categoryInfo, int showCount) {
        this.mParentId = catId;
        this.categoryInfo = categoryInfo;

        searchRelativeLayout.setVisibility(View.VISIBLE);
        this.showCount = showCount;
        setDataToSelectedCities();
        try {
            setDataToCityNewsList(cityInfo.cityHindiName);
        } catch (Exception ignored) {

        }

        setUpPageTitle(categoryInfo.extraVlues);

        overrideTheme();

    }


    private void updateGADetails(int position) {
        showProgress();
        selectedPosition = position;
        if (cityInfoVector.size() > 0) {
            cityInfo = cityInfoVector.get(selectedPosition);
            if (categoryInfo != null) {
                gaArticle = categoryInfo.gaArticle;
                gaScreen = categoryInfo.gaScreen;
                displayName = categoryInfo.displayName;
                gaEventLabel = categoryInfo.gaEventLabel;

                gaScreen += "-" + cityInfo.cityName;
                gaArticle += "-" + cityInfo.cityName;
                displayName += "_" + cityInfo.cityName;
            }
            if (cityFeedListAdapter != null) {
                cityFeedListAdapter.updateValues(gaScreen, gaArticle, displayName, gaEventLabel);
            }

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!NetworkStatus.getInstance().isConnected(mContext) || Constants.singleSessionCategoryIdList.contains(cityInfo.cityId)) {
                        isTokenExpire();
                    } else {
                        swipeRefreshLayout();
                    }
                }
            }, Constants.DataBaseHit.DEFAULT_TIME_IS_TOKEN);

        }
    }

    private int getLastSavedCityIndex() {
        String id = AppPreferences.getInstance(mContext).getStringValue(String.format(QuickPreferences.SELECTED_PREF_CITY, CommonConstants.CHANNEL_ID), Constants.Utils.DEFAULT);
        CityInfo cityInfoBasedOnCityId = new CityInfo(id, Constants.CityPreference.CITY_INFO_BASED_ON_ID);
        if (cityInfoVector != null && cityInfoVector.size() > 0 && cityInfoVector.contains(cityInfoBasedOnCityId)) {
            return cityInfoVector.indexOf(cityInfoBasedOnCityId);
        }
        return 0;
    }

    public void setDataToSelectedCities() {
        if (cityInfoVector != null && cityInfoVector.size() > 0) {
            //update cityInfoVector
            cityInfoVector = JsonParser.getInstance().analyzePreferredCity(mContext, CommonConstants.CHANNEL_ID);
            preferredCityAdapter.setCityInfoVector(cityInfoVector);
            int lastSelectionPosition = getLastSavedCityIndex();
            updateGADetails(lastSelectionPosition);
            preferredCityAdapter.itemSelection(lastSelectionPosition);
        } else {
            getCityInfoVector();
        }
    }


    private String changeFinalUrlForLazyLoading() {
        StringBuilder baseUrl = new StringBuilder(1);
        if (cityInfo != null) {
            baseUrl.append(Urls.APP_FEED_BASE_URL)
                    .append(String.format(Urls.DEFAULT_LIST_URL, CommonConstants.CHANNEL_ID))
                    .append(cityInfo.cityId)
                    .append("/");

            if (LOAD_MORE_COUNT > 0) {
                baseUrl.append("3/PG").append(LOAD_MORE_COUNT);
            } else if (LOAD_MORE_COUNT == 0) {
                baseUrl.append("1/PG1");
            }

            baseUrl.append("/");
            return baseUrl.toString();
        } else {
            return null;
        }
    }

    private void isTokenExpire() {
        try {
            if (NetworkStatus.getInstance().isConnected(mContext)) {
                CityFeedListTable cityFeedListTable = (CityFeedListTable) DatabaseHelper.getInstance(mContext).getTableObject(CityFeedListTable.TABLE_NAME);

                String dbEntryCreationTime = cityFeedListTable.getCityFeedListItemCreationTime(cityInfo.cityId);

                if (dbEntryCreationTime != null && !dbEntryCreationTime.equalsIgnoreCase("")) {
                    long dbEntryCreationTimemilliSeconds = Long.parseLong(dbEntryCreationTime);
                    long currentTime = System.currentTimeMillis();

                    if ((currentTime - dbEntryCreationTimemilliSeconds) <= application.getServerTimeoutValue()) {
                        //token not Expire
                        removeLoader();
                        setOfflineCityFeedData();
                    } else {
                        //token Expire
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                removeLoader();
                                firstApiHit();
                            }
                        }, 500L);
                    }

                } else {
                    removeLoader();
                    firstApiHit();
                }
            } else {
//                removeLoader();
                setOfflineCityFeedData();
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs("", ex.toString());
        }
    }

    private void setOfflineCityFeedData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    showProgress();
                    boolean isShowInternetMessage = true;
                    if (cityFeedList == null) {
                        cityFeedList = new ArrayList<>();
                    } else {
                        cityFeedList.clear();
                    }
                    CityFeedListTable cityFeedListTable = (CityFeedListTable) DatabaseHelper.getInstance(mContext).getTableObject(CityFeedListTable.TABLE_NAME);
                    ArrayList<RelatedArticleInfo> infoList = cityFeedListTable.getAllCityFeedListInfoListAccordingToCity(cityInfo.cityId, mContext);
                    for (RelatedArticleInfo info : infoList) {
                        listIndex += 1;
                        info.listIndex = listIndex;
                    }
                    cityFeedList.addAll(infoList);

                    if (cityFeedList != null && cityFeedList.size() > 0) {
                        isShowInternetMessage = false;
                    }

                    noNewsAvailable.setVisibility(View.GONE);
                    cityFeedListRecyclerView.setVisibility(View.VISIBLE);
                    cityFeedListAdapter = new CityFeedListAdapter(mContext, gaScreen, gaArticle, displayName, gaEventLabel);
                    cityFeedListAdapter.setThemeColor(categoryInfo.color);
                    cityFeedListRecyclerView.setAdapter(cityFeedListAdapter);

                    setDataToCityNewsList(cityInfo.cityHindiName);


                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            removeLoader();
                        }
                    }, 500);
                    swipeRefreshLayoutWithOutApiHit();
                    checkInternetAndShowToast(isShowInternetMessage);
                } catch (Exception ex) {
                    AppLogs.printErrorLogs(TAG, ex.toString());
                }

            }
        }, Constants.DataBaseHit.DEFAULT_TIME);

    }

    public void setDataToCityNewsList(String hindiTitle) {
        ArrayList<RelatedArticleInfo> arrayList = new ArrayList<>();
        if (showCount > 0 && cityFeedList != null && cityFeedList.size() > 0 && showCount < cityFeedList.size()) {
            for (int i = 0; i < showCount; i++) {
                arrayList.add(cityFeedList.get(i));
            }
        } else {
            arrayList = (ArrayList<RelatedArticleInfo>) cityFeedList;
        }
        cityFeedListAdapter.setData(arrayList, hindiTitle);
        cityFeedListAdapter.notifyDataSetChanged();
    }

    private void checkInternetAndShowToast(Boolean isShowInternetMessage) {
        try {
            if (NetworkStatus.getInstance().isConnected(mContext)) {
//                isTokenExpire();
            } else {
                if (isShowInternetMessage)
                    if (mContext != null)
                        AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));

            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }

    }

    private void swipeRefreshLayoutWithOutApiHit() {
        LOAD_MORE_COUNT = 0;
        if (!finalFeedURL.contains("/1/"))
            LOAD_MORE_COUNT++;
    }

    private void firstApiHit() {
        try {
            if (NetworkStatus.getInstance().isConnected(mContext)) {
                AppLogs.printDebugLogs("" + "firstApiHit :", "firstApiHit");
                showProgress();
                swipeRefreshLayout();
            } else {
                removeLoader();
                if (mContext != null)
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs("", ex.toString());
        }
    }

    private void swipeRefreshLayout() {
        listIndex = 0;
        LOAD_MORE_COUNT = 0;
        makeJsonObjectRequest();
    }

    private void makeJsonObjectRequest() {
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            finalFeedURL = changeFinalUrlForLazyLoading();
            AppLogs.printErrorLogs("Search Preferred City VH", "Final Feed Url= " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL),null,
                    response -> {
                        try {
                            if (response != null) {
                                if (!Constants.singleSessionCategoryIdList.contains(cityInfo.cityId)) {
                                    Constants.singleSessionCategoryIdList.add(cityInfo.cityId);
                                }
                                parseFeedList(response);
                            }
                        } catch (Exception e) {
                        }

                        removeLoader();
                    }, error -> {
                        if (LOAD_MORE_COUNT >= 0) {
                            LOAD_MORE_COUNT--;
                        }
                        removeLoader();
                        if (mContext != null) {
                            if (error instanceof NoConnectionError || error instanceof NetworkError) {
                                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                            } else {

                                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);

        } else {
            if (mContext != null) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                removeLoader();
            }
        }
    }

    private void parseFeedList(JSONObject response) throws JSONException {

        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        int responseSize = resultArray.length();

        if (responseSize > 0) {

            if (LOAD_MORE_COUNT == 0) {
                cityFeedList.clear();
            }

            noNewsAvailable.setVisibility(View.GONE);
            cityFeedListRecyclerView.setVisibility(View.VISIBLE);

            for (int i = 0; i < responseSize; i++) {
                JSONObject feedJsonObject = resultArray.getJSONObject(i);
                Gson gson = new Gson();
                RelatedArticleInfo newsFeeds = gson.fromJson(feedJsonObject.toString(), RelatedArticleInfo.class);
                listIndex += 1;
                newsFeeds.listIndex = listIndex;
                cityFeedList.add(newsFeeds);
            }

            // Inserting DB CityFeedList in mdb
            final CityFeedListTable cityFeedListTable = (CityFeedListTable) DatabaseHelper.getInstance(mContext).getTableObject(CityFeedListTable.TABLE_NAME);
            if (LOAD_MORE_COUNT == 0) {
                cityFeedListTable.deleteParticularCityFeedList(cityInfo.cityId);
                cityFeedListTable.addCityFeedListItem(cityInfo, cityFeedList, mContext);
                if (!finalFeedURL.contains("/1/"))
                    LOAD_MORE_COUNT++;

                cityFeedListAdapter = new CityFeedListAdapter(mContext, gaScreen, gaArticle, displayName, gaEventLabel);
                cityFeedListAdapter.setThemeColor(categoryInfo.color);
                cityFeedListRecyclerView.setAdapter(cityFeedListAdapter);
            }

            setDataToCityNewsList(cityInfo.cityHindiName);

        } else {
            if (LOAD_MORE_COUNT == 0) {
                noNewsAvailable.setVisibility(View.VISIBLE);
                cityFeedListRecyclerView.setVisibility(View.GONE);
            } else {
                LOAD_MORE_COUNT--;
            }
        }
    }


    private void removeLoader() {
        if (LOAD_MORE_COUNT > 0) {
            if (cityFeedListAdapter != null) {
                cityFeedListAdapter.removeItem();
            }
            if (isLoading) {
                isLoading = false;
            }
        }
        if (layoutLoadingItem != null)
            layoutLoadingItem.setVisibility(View.GONE);

    }

    private void showProgress() {
        if (layoutLoadingItem != null)
            layoutLoadingItem.setVisibility(View.VISIBLE);
        if (cityFeedListRecyclerView != null) {
            cityFeedListRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCityItemClick(int cityPosition) {

        // Save the selected City to the Serialized Data.
        AppPreferences.getInstance(mContext).setStringValue(String.format(QuickPreferences.SELECTED_PREF_CITY, CommonConstants.CHANNEL_ID), cityInfoVector.get(cityPosition).cityId);

        int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
        int lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
        int centerPosition = (firstVisibleItemPosition + lastVisibleItemPosition) / 2;

        if (cityPosition > centerPosition) {
            preferredCityRecyclerView.smoothScrollToPosition(cityPosition + 1);
        } else if (cityPosition < centerPosition) {
            preferredCityRecyclerView.smoothScrollToPosition(cityPosition);
        }
        if (cityPosition == selectedPosition) {
            // set the position to top of the news List.
        } else {
            listIndex = 0;
            LOAD_MORE_COUNT = 0;
            updateGADetails(cityPosition);


            // Tracking
            if (categoryInfo != null && cityInfoVector != null && cityInfoVector.size() > 0) {
                CityInfo selectedCityInfo = cityInfoVector.get(cityPosition);
                String gaScreen = categoryInfo.gaScreen + "-" + selectedCityInfo.cityName + ((TextUtils.isEmpty(selectedCityInfo.citySelectionValue) ? "" : "_" + selectedCityInfo.citySelectionValue));

                String source = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_SOURCE, "");
                String medium = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAScreen(mContext, InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);

                String wisdomUrl = TrackingHost.getInstance(mContext).getWisdomUrl(AppUrls.WISDOM_URL);
                String domain = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
                String subDomain = categoryInfo.displayName + "/" + selectedCityInfo.cityName;
                AppUtils.GLOBAL_DISPLAY_NAME = subDomain;
                String url = domain + subDomain;
                ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
                String email = "";
                String mobile = "";
                if(profileInfo!=null){
                    email = profileInfo.email;
                    mobile = profileInfo.mobile;
                }
                Tracking.trackWisdomListingPage(mContext, wisdomUrl, url, email, mobile, CommonConstants.BHASKAR_APP_ID);
                Systr.println("Wisdom : " + url);
            }
        }
    }

}