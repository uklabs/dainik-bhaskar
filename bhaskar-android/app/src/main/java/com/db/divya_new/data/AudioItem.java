package com.db.divya_new.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

// POJO to hold data about audio items
public class AudioItem implements Serializable {

    // raw resource id of audio cell
//    private int audioResId;

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("title")
    public String title;

    @SerializedName("lyrics_title")
    public String lyricsTitle;

    @SerializedName("image")
    public String image;

    @SerializedName("audio_url")
    public String audioUrl;

    @SerializedName("lyrics_description")
    public String lyricsDescription;

    private boolean isVolumeMuted;

    public boolean isVolumeMuted() {
        return isVolumeMuted;
    }

    public void setVolumeMuted(boolean volumeMuted) {
        isVolumeMuted = volumeMuted;
    }

    public int getResumePosition() {
        return resumePosition;
    }

    public void setResumePosition(int resumePosition) {
        this.resumePosition = resumePosition;
    }

    private int resumePosition;
}