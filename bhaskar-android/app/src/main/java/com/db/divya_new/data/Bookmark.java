package com.db.divya_new.data;

import java.io.Serializable;

public class Bookmark implements Serializable {
    public String id;
    public String title;
    public String imageURL;
    public String detailURL;
    public String content;
    public String videoFlag;
    public String action; // NV2
    public String pTitle;
    public String pParentId;
    public String pCategoryId;
    public String gaScreen;
    public String gaArticle;
    public String displayName;
    public String gaEventLabel;
    //String pStoryId, String pTitle, String pParentId, String pCategoryId, String gaScreen, String gaArticle, String displayName, String gaEventLabel

    public Bookmark(){

    }

    public Bookmark(String id, String title, String imageURL, String feedURL,String content, String videoFlag) {
        this.id = id;
        this.title = title;
        this.imageURL = imageURL;
        this.detailURL = feedURL;
        this.content = content;
        this.videoFlag = videoFlag;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
