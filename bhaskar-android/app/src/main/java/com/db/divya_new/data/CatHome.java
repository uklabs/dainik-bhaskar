package com.db.divya_new.data;

import com.db.data.models.CategoryInfo;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CatHome implements Serializable {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("count")
    public String count;

    @SerializedName("type")
    public String type;

    @SerializedName("horizontal_list")
    public String horizontalList;

//    @SerializedName("more")
//    public String more;

    @SerializedName("type1_display_count")
    public int displayCount;

    @SerializedName("superParentId")
    public String superParentId;

    @SerializedName("recommendationUrl")
    public String recommendationUrl;

    @SerializedName("parentId")
    public String parentId;

    @SerializedName("headerTitle")
    public String headerTitle;

    @SerializedName("action")
    public String action;

    @SerializedName("ga_event_label")
    public String gaEventLabel;

    @SerializedName("ga_screen")
    public String gaScreen;

    @SerializedName("ga_article")
    public String gaArticle;

    @SerializedName("color")
    public String color;

    @SerializedName("categoryInfo")
    public CategoryInfo categoryInfo;

    @SerializedName("isGAValueAdded")
    public boolean isGAValueAdded;

    public CatHome(String action) {
        this.action = action;
    }

    public CatHome() {
    }

    @Override
    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        CategoryInfo that = (CategoryInfo) o;
//        return (id != null ? id.equals(that.id) : that.id == null);
        if (o instanceof CategoryInfo) {
            return this.id.equalsIgnoreCase(((CategoryInfo) o).id);
        } else if (o instanceof CatHome) {
            return this.id.equalsIgnoreCase(((CatHome) o).id);
        }

        return super.equals(o);
    }
}