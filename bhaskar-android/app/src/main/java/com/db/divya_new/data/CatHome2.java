package com.db.divya_new.data;

import com.db.data.models.WebBannerInfo;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class CatHome2 implements Serializable {

    @SerializedName("id")
    public String id;

    @SerializedName("color")
    public String color;

    @SerializedName("name")
    public String name;

    @SerializedName("horizontal_list")
    public String horizontalList;

    @SerializedName("action")
    public String action;

    @SerializedName("ga_screen")
    public String ga_screen;

    @SerializedName("more")
    public ArrayList<String> moreLists = new ArrayList<>();

    public ArrayList<CatHome2> subDatalist = new ArrayList<>();

    public ArrayList<Object> list = new ArrayList<>();

    public boolean flickerActive = true;

    @SerializedName("web_banner")
    public ArrayList<WebBannerInfo> webBannerInfos = new ArrayList<>();


}