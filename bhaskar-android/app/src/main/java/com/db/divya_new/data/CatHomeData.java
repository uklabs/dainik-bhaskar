package com.db.divya_new.data;

import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.data.models.StoryListInfo;
import com.db.dbvideoPersonalized.data.VideoInfo;

import java.io.Serializable;
import java.util.ArrayList;

public class CatHomeData implements Serializable {
    public final static int TYPE_STORY = 1;
    public final static int TYPE_NEWS = 2;
    public final static int TYPE_VIDEOS = 3;
    public final static int TYPE_SONGS = 4;
    public final static int TYPE_GURU = 5;
    public final static int TYPE_RITUALS = 7;
    public final static int TYPE_PHOTOS = 8;
    public final static int TYPE_RAJYA_NEWS = 9;
    public final static int TYPE_DB_COLUMN = 10;
    public final static int TYPE_MERA_PAGE = 11;


    //for catHomeParentViewHolder
    public ArrayList<CatHome> dataArrayList;
    public ArrayList<CategoryInfo> titleArrayList;
    public int count;
    public String pageTitle;
    public String currentMonth;
    //for preferred city list
    private long feedEntryTime;
    private FilterData filterData;
    public int type;
    // for ritual data
    private ArrayList<RitualData> ritualDataArrayList;
    //for NewsV2,News Data
    private ArrayList<NewsListInfo> newsListInfoArrayList;
    //for Story,Ratna Data
    private ArrayList<StoryListInfo> storyListInfoArrayList;
    //for Gujarati Data
    private ArrayList<VideoInfo> videoInfoArrayList;
    //for Songs Data
    private ArrayList<AudioItem> audioItemArrayList;
    //for Guru Data
    private ArrayList<GuruInfo> guruInfoArrayList;
    //for Photo Gallary
    private ArrayList<PhotoListItemInfo> photoListItemInfoArrayList;
    //for rajya news feed
    private ArrayList<NewsListInfo> rajyaInfoList;

    //for Story,Ratna Data
    private ArrayList<StoryListInfo> storyDbColumnList;
    //for mera page news
    private ArrayList<NewsListInfo> myPageNewsList;

    public CatHomeData(ArrayList<?> arrayList, int count, int type, long feedEntryTime) {
        switch (type) {
            case TYPE_STORY:
                storyListInfoArrayList = (ArrayList<StoryListInfo>) arrayList;
                break;
            case TYPE_NEWS:
                newsListInfoArrayList = (ArrayList<NewsListInfo>) arrayList;
                break;
            case TYPE_VIDEOS:
                videoInfoArrayList = (ArrayList<VideoInfo>) arrayList;
                break;
            case TYPE_SONGS:
                audioItemArrayList = (ArrayList<AudioItem>) arrayList;
                break;
            case TYPE_GURU:
                guruInfoArrayList = (ArrayList<GuruInfo>) arrayList;
                break;
            case TYPE_PHOTOS:
                photoListItemInfoArrayList = (ArrayList<PhotoListItemInfo>) arrayList;
                break;
            case TYPE_RAJYA_NEWS:
                rajyaInfoList = (ArrayList<NewsListInfo>) arrayList;
                break;
            case TYPE_DB_COLUMN:
                storyDbColumnList = (ArrayList<StoryListInfo>) arrayList;
                break;
            case TYPE_MERA_PAGE:
                myPageNewsList = (ArrayList<NewsListInfo>) arrayList;
                break;
        }
        this.type = type;
        this.count = count;
        this.feedEntryTime = feedEntryTime;
    }


    public CatHomeData(ArrayList<RitualData> ritualData, String currentMonth, long feedEntryTime) {
        this.ritualDataArrayList = ritualData;
        this.currentMonth = currentMonth;
        this.feedEntryTime = feedEntryTime;
        this.type = TYPE_RITUALS;
    }

    public CatHomeData() {
    }

    public CatHomeData(ArrayList<CatHome> dataArrayList, ArrayList<CategoryInfo> titleArrayList) {
        this.dataArrayList = dataArrayList;
        this.titleArrayList = titleArrayList;
        this.type = 0;
    }

    public long getFeedEntryTime() {
        return feedEntryTime;
    }

    public void setFeedEntryTime(long feedEntryTime) {
        this.feedEntryTime = feedEntryTime;
    }

    public FilterData getFilterData() {
        return filterData;
    }

    public void setFilterData(FilterData filterData) {
        this.filterData = filterData;
    }

    public ArrayList<?> getArrayList(int type) {
        switch (type) {
            case TYPE_STORY:
                return storyListInfoArrayList;
            case TYPE_DB_COLUMN:
                return storyDbColumnList;
            case TYPE_NEWS:
                return newsListInfoArrayList;
            case TYPE_VIDEOS:
                return videoInfoArrayList;
            case TYPE_SONGS:
                return audioItemArrayList;
            case TYPE_GURU:
                return guruInfoArrayList;
            case TYPE_RITUALS:
                return ritualDataArrayList;
            case TYPE_PHOTOS:
                return photoListItemInfoArrayList;
            case TYPE_RAJYA_NEWS:
                return rajyaInfoList;
            case TYPE_MERA_PAGE:
                return myPageNewsList;
        }
        return new ArrayList<>();
    }


}
