package com.db.divya_new.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CommentFeed implements Serializable {
    @SerializedName("count")
    @Expose
    public Integer count;
    @SerializedName("total")
    @Expose
    public Integer total;
    @SerializedName("feed")
    @Expose
    public List<Feed> feed = null;

    public static class Feed {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("user_id")
        @Expose
        public String userID;
        @SerializedName("likes")
        @Expose
        public String likes;
        @SerializedName("dislikes")
        @Expose
        public String dislikes;
        @SerializedName("post_text")
        @Expose
        public String postText;
        @SerializedName("timeago")
        @Expose
        public String timeago;
        @SerializedName("post_postcount")
        @Expose
        public String postPostcount;
        @SerializedName("username")
        @Expose
        public String username;
        @SerializedName("user_pic")
        @Expose
        public String userPic;
        @SerializedName("reply")
        @Expose
        public Reply reply;

        public static class Reply {

            @SerializedName("total")
            @Expose
            public Integer total;
            @SerializedName("feed")
            @Expose
            public List<Feed_> feed = null;

            public static class Feed_ {

                @SerializedName("id")
                @Expose
                public String id;
                @SerializedName("user_id")
                @Expose
                public String userId;
                @SerializedName("likes")
                @Expose
                public String likes;
                @SerializedName("dislikes")
                @Expose
                public String dislikes;
                @SerializedName("post_text")
                @Expose
                public String postText;
                @SerializedName("timeago")
                @Expose
                public String timeago;
                @SerializedName("post_postcount")
                @Expose
                public String postPostcount;
                @SerializedName("username")
                @Expose
                public String username;
                @SerializedName("user_pic")
                @Expose
                public String userPic;

                public Feed_(String username,String userPic, String postText) {
                    this.username = username;
                    this.userPic = userPic;
                    this.postText = postText;
                }
            }

        }

    }
}
