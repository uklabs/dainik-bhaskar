package com.db.divya_new.data;

import java.io.Serializable;

public class CommentsData implements Serializable{
    private String id;

    private String username;

    private String timeago;

    private String post_text;

    private String likes;

    private CommentsData reply;

    private String post_postcount;

    private String dislikes;

    private String user_pic;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTimeago() {
        return timeago;
    }

    public void setTimeago(String timeago) {
        this.timeago = timeago;
    }

    public String getPost_text() {
        return post_text;
    }

    public void setPost_text(String post_text) {
        this.post_text = post_text;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public CommentsData getReply() {
        return reply;
    }

    public void setReply(CommentsData reply) {
        this.reply = reply;
    }

    public String getPost_postcount() {
        return post_postcount;
    }

    public void setPost_postcount(String post_postcount) {
        this.post_postcount = post_postcount;
    }

    public String getDislikes() {
        return dislikes;
    }

    public void setDislikes(String dislikes) {
        this.dislikes = dislikes;
    }

    public String getUser_pic() {
        return user_pic;
    }

    public void setUser_pic(String user_pic) {
        this.user_pic = user_pic;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", username = " + username + ", timeago = " + timeago + ", post_text = " + post_text + ", likes = " + likes + ", reply = " + reply + ", post_postcount = " + post_postcount + ", dislikes = " + dislikes + ", user_pic = " + user_pic + "]";
    }
}
		