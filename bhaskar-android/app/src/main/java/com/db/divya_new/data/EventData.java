package com.db.divya_new.data;

import com.google.gson.annotations.SerializedName;

public class EventData {

    @SerializedName("id")
    public String id;

    @SerializedName("title")
    public String title;

    @SerializedName("day")
    public String day;
}