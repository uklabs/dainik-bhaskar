package com.db.divya_new.data;

import com.db.data.models.FilterInfo;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FilterData {

    @SerializedName("search_hint")
    private String search_hint;

    @SerializedName("title")
    private String title;

    @SerializedName("data")
    private ArrayList<FilterInfo> data;

    public String getSearch_hint() {
        return search_hint;
    }

    public void setSearch_hint(String search_hint) {
        this.search_hint = search_hint;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<FilterInfo> getData() {
        return data;
    }

    public void setData(ArrayList<FilterInfo> data) {
        this.data = data;
    }

}