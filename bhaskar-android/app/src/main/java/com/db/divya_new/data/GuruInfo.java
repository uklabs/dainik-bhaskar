package com.db.divya_new.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GuruInfo implements Serializable{

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("title")
    public String title;

    @SerializedName("sub_title")
    public String subTitle;

    @SerializedName("image")
    public String image;

    @SerializedName("coming_soon")
    public String comingSoon;
}
