package com.db.divya_new.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GuruvaniGuruTabInfo implements Serializable{

    @SerializedName("videos_list")
    public GuruvaniGuruTabValueInfo videoInfo = new GuruvaniGuruTabValueInfo();

    @SerializedName("quotes_list")
    public GuruvaniGuruTabValueInfo qutoesInfo = new GuruvaniGuruTabValueInfo();

    @SerializedName("schedule_list")
    public GuruvaniGuruTabValueInfo scheduleInfo = new GuruvaniGuruTabValueInfo();

    @SerializedName("video_rec_url")
    public String videoRecUrl;

    public class GuruvaniGuruTabValueInfo {
        @SerializedName("menu_name")
        public String menuName;

        @SerializedName("display_name")
        public String displayName;

        @SerializedName("feed_url")
        public String feedUrl;

        @SerializedName("ga_screen")
        public String gaScreen;

        @SerializedName("ga_article")
        public String gaArticle;
    }
}
