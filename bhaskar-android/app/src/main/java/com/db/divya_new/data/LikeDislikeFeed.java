package com.db.divya_new.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LikeDislikeFeed implements Serializable {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("code")
    @Expose
    public Integer code;
    @SerializedName("action")
    @Expose
    public String action;

    public class Data {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("post_id")
        @Expose
        public String postId;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("post_like_status")
        @Expose
        public String postLikeStatus;

    }
}
