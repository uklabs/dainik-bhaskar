package com.db.divya_new.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PostFeed implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("likes")
    @Expose
    public String likes;
    @SerializedName("dislikes")
    @Expose
    public String dislikes;
    @SerializedName("post_text")
    @Expose
    public String postText;
    @SerializedName("timeago")
    @Expose
    public String timeago;
    @SerializedName("post_postcount")
    @Expose
    public String postPostcount;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("user_pic")
    @Expose
    public String userPic;
    @SerializedName("reply")
    @Expose
    public Reply reply;

    public class Reply {

        @SerializedName("total")
        @Expose
        public Integer total;
        @SerializedName("feed")
        @Expose
        public List<Feed_> feed = null;

        public class Feed_ {

            @SerializedName("id")
            @Expose
            public String id;
            @SerializedName("likes")
            @Expose
            public String likes;
            @SerializedName("dislikes")
            @Expose
            public String dislikes;
            @SerializedName("post_text")
            @Expose
            public String postText;
            @SerializedName("timeago")
            @Expose
            public String timeago;
            @SerializedName("post_postcount")
            @Expose
            public String postPostcount;
            @SerializedName("username")
            @Expose
            public String username;
            @SerializedName("user_pic")
            @Expose
            public String userPic;

        }

    }
}
