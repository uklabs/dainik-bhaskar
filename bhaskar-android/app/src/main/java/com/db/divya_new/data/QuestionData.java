package com.db.divya_new.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class QuestionData implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("updated")
    private String updated;
    @SerializedName("created")
    private String created;
    @SerializedName("email")
    private String email;
    @SerializedName("pandit_name")
    private String pandit_name;
    @SerializedName("name")
    private String name;
    @SerializedName("answer")
    private String answer;
    @SerializedName("question")
    private String question;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPandit_name() {
        return pandit_name;
    }

    public void setPandit_name(String pandit_name) {
        this.pandit_name = pandit_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", updated = " + updated + ", created = " + created + ", email = " + email + ", pandit_name = " + pandit_name + ", name = " + name + ", answer = " + answer + ", question = " + question + "]";
    }
}
