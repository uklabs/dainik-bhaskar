package com.db.divya_new.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class QuotesInfo implements Serializable{

    @SerializedName("id")
    public String id;

    @SerializedName("guru_id")
    public String guruId;

    @SerializedName("image_size")
    public String imageSize;

    @SerializedName("quotes_image")
    public String image;

    @SerializedName("cat_id")
    public String catId;
}
