package com.db.divya_new.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RitualData {

    @SerializedName("name")
    public String name;

    @SerializedName("title")
    public String title;

    @SerializedName("list")
    public ArrayList<EventData> eventList = new ArrayList<>();

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RitualData) {
            return ((RitualData) obj).name.equalsIgnoreCase(this.name);
        }
        return super.equals(obj);
    }
}