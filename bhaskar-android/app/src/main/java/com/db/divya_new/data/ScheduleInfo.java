package com.db.divya_new.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ScheduleInfo implements Serializable{

    @SerializedName("date")
    public String date;

    @SerializedName("description")
    public String desc;
}
