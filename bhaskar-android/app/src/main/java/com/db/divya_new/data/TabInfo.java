package com.db.divya_new.data;

import com.google.gson.annotations.SerializedName;

public class TabInfo {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("title")
    public String title;

    @SerializedName("sub_title")
    public String subTitle;

    @SerializedName("image")
    public String image;

    @Override
    public String toString() {
        return title;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TabInfo) {
            return ((TabInfo) obj).id.equalsIgnoreCase(this.id);
        }
//        else if (obj instanceof String) {
//            return ((String) obj).equalsIgnoreCase(this.id);
//        }
        return super.equals(obj);
    }
}
