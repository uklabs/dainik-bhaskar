package com.db.divya_new.data;

import com.db.dbvideoPersonalized.data.VideoInfo;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VideoSectionInfo implements Serializable {

    @SerializedName("feed_url")
    public String feedUrl;

    @SerializedName("category_name")
    public String categoryName;

    @SerializedName("video")
    public VideoInfo videoInfo;
}
