package com.db.divya_new.dharamDarshan;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bhaskar.view.ui.MainActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.bhaskar.R;
import com.db.divya_new.data.AudioItem;
import com.db.util.ImageUtil;
import com.db.util.Notificationutil;

import static com.facebook.FacebookSdk.getApplicationContext;

public class NotificationMusic {

    Context context = null;
    Service service = null;
    RemoteViews notificationView = null;
    Notification.Builder notificationBuilder = null;
    NotificationManager notificationManager = null;
    private int NOTIFICATION_ID = 6780;
    /**
     * Updates the Notification icon if the music is paused.
     */
    private boolean mIsPaused;

    /**
     * Cancels all sent notifications.
     */
    public static void cancelAll(Context c) {
        NotificationManager manager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancelAll();
    }

    public void notifySong(Context context, final Service service, AudioItem song) {

        // NOTIFICATION_ID++;

        if (this.context == null)
            this.context = context;
        if (this.service == null)
            this.service = service;

        // Intent that launches the "Now Playing" Activity
        Intent notifyIntent = new Intent(context, MainActivity.class);
        notifyIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Letting the Intent be executed later by other application.
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Setting our custom appearance for the notification
        notificationView = new RemoteViews(context.getPackageName(), R.layout.notification_music);

        // Manually settings the buttons and text
        // (ignoring the defaults on the XML)

        notificationView.setImageViewResource(R.id.iv_icon, R.drawable.water_mark_news_detail);
        try {
            Glide.with(getApplicationContext())
                    .asBitmap()
                    .load(ImageUtil.HttpHeader.getUrl(song.image))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            notificationView.setImageViewBitmap(R.id.iv_icon, resource);
                            notifyView();
                        }
                    });
        } catch (Exception e) {
        }

        notificationView.setImageViewResource(R.id.notification_button_play, R.drawable.ic_pause_circle_filled_red_96dp);
//        notificationView.setImageViewResource(R.id.notific\ation_button_skip, R.drawable.ic_video_skip_next);
        notificationView.setTextViewText(R.id.notification_text_title, song.title);
//        notificationView.setTextViewText(R.id.notification_text_artist, song.name);

        // On the notification we have two buttons - Play and Skip
        // Here we make sure the class `NotificationButtonHandler`
        // gets called when user selects one of those.
        //
        // First, building the play button and attaching it.
        Intent buttonPlayIntent = new Intent(context, NotificationPlayButtonHandler.class);
        buttonPlayIntent.putExtra("action", "togglePause");
        PendingIntent buttonPlayPendingIntent = PendingIntent.getBroadcast(context, 0, buttonPlayIntent, 0);
        notificationView.setOnClickPendingIntent(R.id.notification_button_play, buttonPlayPendingIntent);

        // And now, building and attaching the Skip button.
        Intent buttonStopIntent = new Intent(context, NotificationStopButtonHandler.class);
        buttonStopIntent.putExtra("action", "stop");
        PendingIntent buttonStopPendingIntent = PendingIntent.getBroadcast(context, 0, buttonStopIntent, 0);
        notificationView.setOnClickPendingIntent(R.id.notification_cross_button, buttonStopPendingIntent);

        // Finally... Actually creating the Notification
        notificationBuilder = new Notification.Builder(context);

        notificationBuilder.setContentIntent(pendingIntent)
                .setSmallIcon(Notificationutil.getAppIcon())
                .setTicker("Playing '" + song.title)
                .setOngoing(true)
                .setContentTitle(song.title)
                .setContent(notificationView);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(context.getString(R.string.app_name));
        }

        Notification notification = notificationBuilder.build();

        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            String name = context.getString(R.string.app_name);
            String id = context.getString(R.string.app_name);
            NotificationChannel mChannel = new NotificationChannel(id, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        // Sets the notification to run on the foreground.
        // (why not the former commented line?)
        service.startForeground(NOTIFICATION_ID, notification);
    }

    public void notifyPaused(boolean isPaused) {
        mIsPaused = isPaused;
        if ((notificationView == null) || (notificationBuilder == null))
            return;

        int iconID = ((isPaused) ?
                R.drawable.ic_play_circle_filled_red_96dp :
                R.drawable.ic_pause_circle_filled_red_96dp);

        notificationView.setImageViewResource(R.id.notification_button_play, iconID);

        notificationBuilder.setContent(notificationView);

        // Sets the notification to run on the foreground.
        // (why not the former commented line?)
        notifyView();
    }

    private void notifyView() {
        if (mIsPaused) {
            service.stopForeground(false);
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        } else {
            service.startForeground(NOTIFICATION_ID, notificationBuilder.build());
        }
    }

    /**
     * Cancels this notification.
     */
    public void cancel() {
        service.stopForeground(true);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    /**
     * Called when user clicks the "play/pause" button on the on-going system Notification.
     */
    public static class NotificationPlayButtonHandler extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (SongBinder.musicService != null)
                SongBinder.musicService.togglePlayback();
        }
    }

    /**
     * Called when user clicks the "skip" button on the on-going system Notification.
     */
    public static class NotificationStopButtonHandler extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (SongBinder.musicService != null)
                SongBinder.musicService.destroySelf(true);
        }
    }
}
