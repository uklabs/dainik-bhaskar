package com.db.divya_new.dharamDarshan;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.db.divya_new.data.AudioItem;
import com.db.divya_new.dharamDarshan.Songs.SongService;

public class SongBinder {
    /**
     * All the songs on the device.
     */
//    public static SongList songs = new SongList();

    /**
     * All the app's configurations/preferences/settings.
     */
//    public static Settings settings = new Settings();

    /**
     * Our custom service that allows the music to play
     * even when the app is not on focus.
     */
    public static SongService musicService = null;

//    public static ArrayList<Song> musicList = null;

//    public static ArrayList<Song> nowPlayingList = null;

    public static boolean mainMenuHasNowPlayingItem = false;

    // GENERAL PROGRAM INFO
    public static String applicationName = "kure Music Player";
    public static String packageName = "<unknown>";
    public static String versionName = "<unknown>";
    public static int versionCode = -1;
    public static long firstInstalledTime = -1;
    public static long lastUpdatedTime = -1;
    /**
     * The actual connection to the MusicService.
     * We start it with an Intent.
     * <p>
     * These callbacks will bind the MusicService to our internal
     * variables.
     * We can only know it happened through our flag, `musicBound`.
     */
    public static ServiceConnection musicConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            SongService.MusicBinder binder = (SongService.MusicBinder) service;

            // Here's where we finally create the MusicService
            musicService = binder.getService();
//            musicService.setList(SongBinder.songs.songs);
            musicService.musicBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicService.musicBound = false;
        }
    };
    private static boolean isBind;
    /**
     * Our will to start a new music Service.
     * Android requires that we start a service through an Intent.
     */
    private static Intent musicServiceIntent = null;

    /**
     * Creates everything.
     * <p>
     * Must be called only once at the beginning
     * of the program.
     */
    public static void initialize(Context c) {

    }

    /**
     * Destroys everything.
     * <p>
     * Must be called only once when the program
     * being destroyed.
     */
    public static void destroy() {
//        songs.destroy();
    }

    /**
     * Initializes the Music Service at Activity/Context c.
     *
     * @note Only starts the service once - does nothing when
     * called multiple times.
     */
    public static void startMusicService(Context c, AudioItem item) {

//        if (musicServiceIntent != null)
//            return;
//
//        if (SongBinder.musicService != null)
//            return;

        // Create an intent to bind our Music Connection to
        // the MusicService.
        musicServiceIntent = new Intent(c, SongService.class);
        musicServiceIntent.putExtra("item", item);
        isBind = c.bindService(musicServiceIntent, musicConnection, Context.BIND_AUTO_CREATE);
        c.startService(musicServiceIntent);
    }

    /**
     * Makes the music Service stop and clean itself at
     * Activity/Context c.
     */
    public static void stopMusicService(Context c) {

        if (musicServiceIntent == null)
            return;

        try {
            if (musicConnection != null && isBind) {
                c.unbindService(musicConnection);
                isBind = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            c.stopService(musicServiceIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

        musicServiceIntent = null;
        SongBinder.musicService = null;
    }

    /**
     * Forces the whole application to quit.
     *
     * Please read more info on this StackOverflow answer:
     * http://stackoverflow.com/a/4737595
     *
     * @note This is dangerous, make sure to cleanup
     *       everything before calling this.
     */
//    public static void forceExit(Activity c) {
//
//        // Instead of just calling `System.exit(0)` we use
//        // a temporary Activity do to the dirty job for us
//        // (clearing all other Activities and finishing() itself).
//        Intent intent = new Intent(c, ActivityQuit.class);
//
//        // Clear all other Activities
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        c.startActivity(intent);
//
//        // Clear the Activity calling this function
//        c.finish();
//    }
}
