package com.db.divya_new.dharamDarshan.Songs;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.provider.Settings;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.divya_new.data.AudioItem;
import com.db.divya_new.dharamDarshan.NotificationMusic;
import com.db.divya_new.dharamDarshan.SongBinder;
import com.db.util.Constants;
import com.db.util.Systr;

import java.io.IOException;

public class SongService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener, AudioManager.OnAudioFocusChangeListener {

    public static final String BROADCAST_ACTION = "com.db.musicplayer.MUSIC_SERVICE";
    public static final String BROADCAST_EXTRA_STATE = "x_japan";
    public static final String BROADCAST_EXTRA_SONG_ID = "tenacious_d";

    public static final String BROADCAST_ORDER = "com.db.musicplayer.MUSIC_SERVICE";
    public static final String BROADCAST_EXTRA_GET_ORDER = "com.db.musicplayer.get.MUSIC_SERVICE";
    public static final String BROADCAST_ORDER_PLAY = "com.db.musicplayer.action.PLAY";
    public static final String BROADCAST_ORDER_PAUSE = "com.db.musicplayer.action.PAUSE";

    public static final String BROADCAST_EXTRA_PLAYING = "beatles";
    public static final String BROADCAST_EXTRA_PAUSED = "santana";
    public static final String BROADCAST_EXTRA_UNPAUSED = "iron_maiden";
    public static final String BROADCAST_EXTRA_COMPLETED = "los_hermanos";

    private static final int MSG_UPDATE_SEEK_BAR = 2324;

    private Context mService;
    private MediaPlayer player;
    private AudioItem audioItem;

    private boolean pausedTemporarilyDueToAudioFocus = false;
    private boolean loweredVolumeDueToAudioFocus = false;

    private ServiceState serviceState = ServiceState.Preparing;
    private NotificationMusic notification;

    // Floating widget
    private View mOverlayView;
    private WindowManager mWindowManager;
    private RelativeLayout floatingLayout;
    private WindowManager.LayoutParams params;

    private Handler uiUpdateHandler;

    enum ServiceState {
        // MediaPlayer is stopped and not prepared to play
        Stopped,
        // MediaPlayer is preparing...
        Preparing,
        // Playback active - media player ready!
        // (but the media player may actually be paused in
        // this state if we don't have audio focus).
        Playing,
        // So that we know we have to resume playback once we get focus back)
        // playback paused (media player ready!)
        Paused
    }

    public boolean musicBound = false;
    private final IBinder musicBind = new MusicBinder();

    public class MusicBinder extends Binder {
        public SongService getService() {
            return SongService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mService = this;

        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(localBroadcastReceiver, new IntentFilter(SongService.BROADCAST_ORDER));

        initMusicPlayer();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.canDrawOverlays(this))
                createWidget();
        } else {
            createWidget();
        }

        uiUpdateHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case MSG_UPDATE_SEEK_BAR: {
                        updateSeekBar();
                        uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
                        return true;
                    }
                }
                return false;
            }
        });
    }


    BroadcastReceiver localBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Getting the information sent by the MusicService
            // (and ignoring it if invalid)
            String order = intent.getStringExtra(SongService.BROADCAST_EXTRA_GET_ORDER);
            if (order == null)
                return;

            if (order.equals(SongService.BROADCAST_ORDER_PAUSE)) {
                pausePlayer();
            } else if (order.equals(SongService.BROADCAST_ORDER_PLAY)) {
                unpausePlayer();
            }
        }
    };

    @Override
    public void onAudioFocusChange(int focusChange) {
        Systr.println("SONG : onAudioFocusChange : " + focusChange);
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                Systr.println("SONG : onAudioFocusChange : gain");
                if (player == null)
                    initMusicPlayer();

                if (pausedTemporarilyDueToAudioFocus) {
                    pausedTemporarilyDueToAudioFocus = false;
                    unpausePlayer();
                }

                if (loweredVolumeDueToAudioFocus) {
                    loweredVolumeDueToAudioFocus = false;
                    player.setVolume(1.0f, 1.0f);
                }
                break;

            // Damn, lost the audio focus for a (presumable) long time
            case AudioManager.AUDIOFOCUS_LOSS:
                Systr.println("SONG : onAudioFocusChange : loss");
                stopMusicPlayer();
                break;

            // Just lost audio focus but will get it back shortly
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                Systr.println("SONG : onAudioFocusChange : transient");
                if (!isPaused()) {
                    pausePlayer();
                    pausedTemporarilyDueToAudioFocus = true;
                }
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                Systr.println("SONG : onAudioFocusChange : can duck");
                player.setVolume(0.1f, 0.1f);
                loweredVolumeDueToAudioFocus = true;
                break;
        }
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        serviceState = ServiceState.Playing;

        if (uiUpdateHandler != null)
            uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);

        // Start playback
        player.start();

        // If the user clicks on the notification, let's spawn the
        // Now Playing screen.
        notifyCurrentSong();
        setSeekBar();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        serviceState = ServiceState.Stopped;

        if (uiUpdateHandler != null)
            uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);

        destroy();

        broadcastState(SongService.BROADCAST_EXTRA_COMPLETED);
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        mediaPlayer.reset();
        return false;
    }

    public void notifyCurrentSong() {
        if (audioItem == null)
            return;

        if (notification == null)
            notification = new NotificationMusic();

        notification.notifySong(this, this, audioItem);
    }

    public boolean isPaused() {
        return serviceState == ServiceState.Paused;
    }

    public void initMusicPlayer() {
        if (player == null)
            player = new MediaPlayer();

        player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        player.setOnPreparedListener(this); // player initialized
        player.setOnCompletionListener(this); // song completed
        player.setOnErrorListener(this);
    }

    public void stopMusicPlayer() {
        if (player == null)
            return;

        player.stop();
        player.release();
        player = null;
    }

    public void playSong() {
        if (player == null) {
            initMusicPlayer();
        }
        player.reset();

        try {
            player.setDataSource(getApplicationContext(), Uri.parse(audioItem.audioUrl));
        } catch (IOException io) {
            destroySelf(false);
        } catch (Exception e) {
            destroySelf(false);
        }

        player.prepareAsync();
        serviceState = ServiceState.Preparing;

        broadcastState(SongService.BROADCAST_EXTRA_PLAYING);
    }

    public void pausePlayer() {
        if (serviceState != ServiceState.Paused && serviceState != ServiceState.Playing)
            return;

        player.pause();
        serviceState = ServiceState.Paused;
        if (notification != null)
            notification.notifyPaused(true);

        updateFloatingWidget();

        broadcastState(SongService.BROADCAST_EXTRA_PAUSED);
    }

    public void unpausePlayer() {
        if (serviceState != ServiceState.Paused && serviceState != ServiceState.Playing)
            return;

        player.start();
        serviceState = ServiceState.Playing;

        if (notification != null)
            notification.notifyPaused(false);

        updateFloatingWidget();

        broadcastState(SongService.BROADCAST_EXTRA_UNPAUSED);
    }

    public void togglePlayback() {
        if (serviceState == ServiceState.Paused)
            unpausePlayer();
        else
            pausePlayer();
    }

    private void broadcastState(String state) {
        if (audioItem == null)
            return;

        Intent broadcastIntent = new Intent(SongService.BROADCAST_ACTION);
        broadcastIntent.putExtra(SongService.BROADCAST_EXTRA_STATE, state);
        broadcastIntent.putExtra(SongService.BROADCAST_EXTRA_SONG_ID, audioItem);

        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(broadcastIntent);
    }

    public void destroySelf(boolean removeNotification) {
//        destroy();
        try {
            pausePlayer();
        } catch (Exception e) {
        }
        try {
            if (mWindowManager != null) {
                mWindowManager.removeView(mOverlayView);
            }
        } catch (Exception e) {
        }
//        SongBinder.stopMusicService(this);
        try {
            stopForeground(removeNotification);
            stopSelf();
        } catch (Exception e) {
        }
//        SongService.this.stopSelf();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (intent.getBooleanExtra("action", false)) {
                destroySelf(true);
            } else {

                audioItem = (AudioItem) intent.getSerializableExtra("item");

                if (mOverlayView != null) {
                    try {
                        if (mOverlayView.getParent() != null) {
                            mWindowManager.removeView(mOverlayView);
                        }
                    } catch (Exception e) {
                    }

                    try {
                        if (mWindowManager != null)
                            mWindowManager.addView(mOverlayView, params);
                    } catch (Exception e) {
                    }
                }

                playSong();
                updateFloatingWidget();
                if (mWindowManager == null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (Settings.canDrawOverlays(this))
                            createWidget();
                    } else {
                        createWidget();
                    }
                }
            }
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroy();
    }

    private void destroy() {
        if (uiUpdateHandler != null)
            uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);

        if (notification != null)
            notification.cancel();

        try {
            if (mOverlayView != null && mWindowManager != null)
                mWindowManager.removeView(mOverlayView);
        } catch (Exception e) {
        }

        SongBinder.stopMusicService(getApplicationContext());

        try {
            if (localBroadcastReceiver != null)
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(localBroadcastReceiver);
        } catch (Exception e) {
        }

        audioItem = null;
    }

    private void createWidget() {
        mOverlayView = LayoutInflater.from(this).inflate(R.layout.music_overlay_layout, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        }

        //Specify the view position
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = (Constants.DEVICE_HEIGHT > 200) ? Constants.DEVICE_HEIGHT - 350 : 100;

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        floatingLayout = mOverlayView.findViewById(R.id.layout);
        floatingLayout.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;

                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;

                    case MotionEvent.ACTION_UP:
                        //Add code for launching application and positioning the widget to nearest edge.
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        float Xdiff = Math.round(event.getRawX() - initialTouchX);
                        float Ydiff = Math.round(event.getRawY() - initialTouchY);

                        //Calculate the X and Y coordinates of the view.
                        params.x = initialX + (int) Xdiff;
                        params.y = initialY + (int) Ydiff;

                        //Update the layout with new X & Y coordinates
                        mWindowManager.updateViewLayout(mOverlayView, params);
                        return true;
                }

                return false;
            }
        });

        ImageView playPauseButton = mOverlayView.findViewById(R.id.notification_button_play);
        playPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                togglePlayback();
                updateFloatingWidget();
            }
        });

        ((SeekBar) mOverlayView.findViewById(R.id.sbProgress)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser && player != null) {
                    ((TextView) mOverlayView.findViewById(R.id.tv_startTime)).setText(getDurationFromMilliSeconds(progress));
                    player.seekTo(progress);
                    setSeekBar();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        mOverlayView.findViewById(R.id.iv_cross).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                destroySelf(true);
//                Intent intent = new Intent(SongService.this, SongService.class);
//                intent.putExtra("action", true);
//                stopService(intent);
            }
        });
    }

    public void updateSeekBar() {
        if (mOverlayView != null && player != null) {
            ((SeekBar) mOverlayView.findViewById(R.id.sbProgress)).setProgress(player.getCurrentPosition());
            ((TextView) mOverlayView.findViewById(R.id.tv_startTime)).setText(getDurationFromMilliSeconds(player.getCurrentPosition()));
        }
    }

    public void updateFloatingWidget() {
        try {
            if (mOverlayView != null) {
                if (audioItem != null) {
                    ((TextView) mOverlayView.findViewById(R.id.notification_text_title)).setText(audioItem.title);
                }

                int iconID = ((isPaused()) ? R.drawable.ic_play_circle_filled_red_96dp : R.drawable.ic_pause_circle_filled_red_96dp);
                ((ImageView) mOverlayView.findViewById(R.id.notification_button_play)).setImageResource(iconID);

                if (mWindowManager != null)
                    mWindowManager.updateViewLayout(mOverlayView, params);
            }
        } catch (Exception e) {
        }
    }

    private void setSeekBar() {
        if (mOverlayView != null) {
            ((TextView) mOverlayView.findViewById(R.id.tv_endTime)).setText(getDurationFromMilliSeconds(player.getDuration()));
            ((TextView) mOverlayView.findViewById(R.id.tv_startTime)).setText(getDurationFromMilliSeconds(player.getCurrentPosition()));

            ((SeekBar) mOverlayView.findViewById(R.id.sbProgress)).setMax(player.getDuration());
            ((SeekBar) mOverlayView.findViewById(R.id.sbProgress)).setProgress(player.getCurrentPosition());
            ((SeekBar) mOverlayView.findViewById(R.id.sbProgress)).setEnabled(true);
        }
    }

    public String getDurationFromMilliSeconds(int args) {
        long minutes = 00;
        if (args == 0) {
            return "00:00";
        } else {
            minutes = (args / 1000) / 60;
            long seconds = 00;
            seconds = (args / 1000) % 60;

            String min = String.valueOf((minutes == 0 ? "00" : minutes));
            String sec = String.valueOf((seconds == 0 ? "00" : seconds));
            return (min.length() == 1 ? "0" + min : min) + ":" + (sec.length() == 1 ? "0" + sec : sec);
        }
    }
}
