package com.db.divya_new.dharamDarshan.Songs;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.divya_new.dharamDarshan.Songs.adapter.SongsAdapter;
import com.db.util.Constants;
import com.db.util.DividerItemDecoration;

import java.util.ArrayList;

public class SongsFragment extends Fragment implements OnMoveTopListener {

    public String mParentId;
    private CategoryInfo mCategoryInfo;
    private SongsAdapter adapter;

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;

    public static SongsFragment newInstance(CategoryInfo categoryInfo, String id) {
        SongsFragment fragment = new SongsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString(Constants.KeyPair.KEY_ID, id);
        fragment.setArguments(bundle);

        return fragment;
    }

    public SongsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mCategoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            mParentId = bundle.getString(Constants.KeyPair.KEY_ID);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null) {
            adapter.destroyMediaPlayer();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_common, container, false);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        adapter = new SongsAdapter(getActivity(), mParentId, mCategoryInfo);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        recyclerView.setAdapter(adapter);

        ArrayList<Object> list = new ArrayList<>();
        list.add(Constants.ViewHolderType.VIEW_HOLDER_SONGS);
        adapter.addAll(list);

        return view;
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            adapter.onSwipeRefresh(swipeRefreshLayout);
        }
    };

    @Override
    public void moveToTop() {
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }
}
