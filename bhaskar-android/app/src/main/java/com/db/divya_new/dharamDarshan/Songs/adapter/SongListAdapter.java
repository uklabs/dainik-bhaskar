package com.db.divya_new.dharamDarshan.Songs.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.divya_new.data.AudioItem;
import com.db.divya_new.dharamDarshan.SongBinder;
import com.db.divya_new.dharamDarshan.Songs.viewHolder.SongsListViewHolder;
import com.db.listeners.OnDownloadListener;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.AppUtils;

import java.io.IOException;
import java.util.ArrayList;

import static com.db.util.Constants.IS_OVERLAY_PERMISSION_ASKED;

public class SongListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Handler.Callback {

    //    private static final int REQUEST_CODE_DOWNLOAD_PERMISSIONS = 11;
    private static final int MSG_UPDATE_SEEK_BAR = 1845;
    private ArrayList<AudioItem> mList;
    private Activity mContext;
    private MediaPlayer mediaPlayer;
    private Handler uiUpdateHandler;
    private int playingPosition;
    private SongsListViewHolder playingHolder;
    private String mDisplayName;
    private String parentId;
    private boolean isFromHome;
    private boolean isFromParentHome;

    public SongListAdapter(Activity context) {
        mList = new ArrayList<>();
        mContext = context;
        this.playingPosition = -1;
        uiUpdateHandler = new Handler(this);
    }

    public void setData(String displayName, String parentId, boolean isFromHome, boolean isFromParentHome) {
        mDisplayName = displayName;
        this.parentId = parentId;
        this.isFromHome = isFromHome;
        this.isFromParentHome = isFromParentHome;
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public void addAndClearList(ArrayList<AudioItem> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void addList(ArrayList<AudioItem> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void add(AudioItem list) {
        mList.add(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SongsListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_song, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SongsListViewHolder) {
            itemBindHolder((SongsListViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        if (holder instanceof SongsListViewHolder) {
            ((SongsListViewHolder) holder).registerReceiver();
        }
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        if (holder instanceof SongsListViewHolder) {
            ((SongsListViewHolder) holder).unRegisterReceiver();
        }
        super.onViewDetachedFromWindow(holder);
    }

    /******** Bind Holders Methods ********/

    private void itemBindHolder(final SongsListViewHolder viewHolder, final int position) {
        final AudioItem item = mList.get(position);
        viewHolder.bindContent(item);
        if (position == playingPosition) {
            playingHolder = viewHolder;
            // this view holder corresponds to the currently playing audio cell
            // update its view to show playing progress
            updatePlayingView();
        } else {
            // and this one corresponds to non playing
            updateNonPlayingView(item, viewHolder);
        }

        viewHolder.tv_title.setText(item.title);

        if (item.isVolumeMuted()) {
            viewHolder.ivVolume.setImageDrawable(mContext.getResources().getDrawable(R.drawable.mute_volume));
        } else
            viewHolder.ivVolume.setImageDrawable(mContext.getResources().getDrawable(R.drawable.volume));

        ImageUtil.setImage(mContext, item.image, viewHolder.iv_pic, R.drawable.water_mark_news_detail);

//        MediaPlayer m = MediaPlayer.create(getApplicationContext(), item.getAudioResId());
        /*MediaPlayer mp = new MediaPlayer();
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mp.setDataSource(mContext, Uri.parse("https://i10.dainikbhaskar.com/web2images/web2audio/audio-20180723190235.mp3"));
            mp.prepare();
        } catch (IOException e) {
            Toast.makeText(mContext, "Media not ready try again", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }*/
        viewHolder.tv_endTime.setText(getDurationFromMilliSeconds(00));
        viewHolder.tv_startTime.setText(getDurationFromMilliSeconds(00));

        viewHolder.tv_read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(mContext, item.lyricsTitle, item.lyricsDescription);

                String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), mDisplayName, AppFlyerConst.GAAction.READ_AARTI, item.title + "-" + item.audioUrl, campaign);
            }
        });

        viewHolder.ivVolume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.isVolumeMuted()) {
                    if (position == playingPosition && mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.setVolume(1, 1);
                    }
                    item.setVolumeMuted(false);
                    viewHolder.ivVolume.setImageDrawable(mContext.getResources().getDrawable(R.drawable.volume));
                } else {
                    if (position == playingPosition && mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.setVolume(0, 0);
                    }
                    item.setVolumeMuted(true);
                    viewHolder.ivVolume.setImageDrawable(mContext.getResources().getDrawable(R.drawable.mute_volume));
                }
            }
        });

        viewHolder.tv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
//                Toast.makeText(mContext, viewHolder.tv_title.getText() + "Download Clicked", Toast.LENGTH_SHORT).show();
                AppUtils.downloadFile(mContext, item.audioUrl, new OnDownloadListener() {
                    @Override
                    public void onStart() {
                        String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), mDisplayName, AppFlyerConst.GAAction.DOWNLOAD_AARTI, item.title + "-" + item.audioUrl, campaign);
                    }
                });
            }
        });

        viewHolder.ivPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (isFromParentHome && isFromHome) {
//                    if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
//                        TabController.getInstance().setTabClicked(true);
//                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(parentId);
//                    }
//                } else if (isFromHome) {
//                    /*if (TabController.getInstance().getSubMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
//                        TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(parentId);
//                        TabController.getInstance().setTabClicked(true);
//                    }*/
//                    if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
//                        TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(parentId);
//                    }
//                } else {
//                String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

//                    if (position == playingPosition) {
//                        // toggle between play/pause of audio
//                        if (mediaPlayer.isPlaying()) {
//                            item.setResumePosition(mediaPlayer.getCurrentPosition());
//                            mediaPlayer.pause();
//                        } else {
//                            if (item.isVolumeMuted()) {
//                                mediaPlayer.setVolume(0, 0);
//                            }
//                            mediaPlayer.start();
//                        }
//
//                        Tracking.trackGAEvent(InitApplication.getInstance().getDefaultTracker(), mDisplayName, AppFlyerConst.GAAction.PAUSE_AARTI, item.title + "-" + item.audioUrl, campaign);
//
//                    } else {
//                        // start another audio playback
//                        if (mediaPlayer != null) {
//                            if (null != playingHolder) {
//                                mList.get(playingPosition).setResumePosition(mediaPlayer.getCurrentPosition());
//                                mediaPlayer.pause();
//                                updateNonPlayingView(mList.get(playingPosition), playingHolder);
//                            }
//                            mediaPlayer.release();
//                        }
//                        playingPosition = position;
//                        playingHolder = viewHolder;
//                        startMediaPlayer(mList.get(playingPosition), item.audioUrl);
//
//                        Tracking.trackGAEvent(InitApplication.getInstance().getDefaultTracker(), mDisplayName, AppFlyerConst.GAAction.PLAY_AARTI, item.title + "-" + item.audioUrl, campaign);
//                    }
//                    updatePlayingView();

//                    Intent s = new Intent(mContext, SongService.class);
//                    s.putExtra("item", item);
//                    mContext.startService(s);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.canDrawOverlays(mContext)) {
                        SongBinder.startMusicService(mContext.getApplicationContext(), item);
                    } else {
                        if (IS_OVERLAY_PERMISSION_ASKED) {
                            SongBinder.startMusicService(mContext.getApplicationContext(), item);
                        } else {
                            if (mContext instanceof BaseAppCompatActivity)
                                ((BaseAppCompatActivity) mContext).checkDrawOverlayPermission();
                            IS_OVERLAY_PERMISSION_ASKED = true;
                        }

                    }
                } else {
                    SongBinder.startMusicService(mContext.getApplicationContext(), item);
                }

                String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), mDisplayName, AppFlyerConst.GAAction.PLAY_AARTI, item.title + "-" + item.audioUrl, campaign);
//                }
            }
        });

        viewHolder.sbProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser && mediaPlayer != null) {
                    viewHolder.tv_startTime.setText(getDurationFromMilliSeconds(progress));
                    if (position == playingPosition)
                        mediaPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }


    public String getDurationFromMilliSeconds(int args) {

        long minutes = 00;
        if (args == 0) {
            return "00:00";
        } else {
            minutes = (args / 1000) / 60;
            long seconds = 00;
            seconds = (args / 1000) % 60;

            String min = String.valueOf((minutes == 0 ? "00" : minutes));
            String sec = String.valueOf((seconds == 0 ? "00" : seconds));
//        System.out.format("%d Milliseconds = %d minutes and %d seconds.", milliseconds, minutes, seconds);
            return (min.length() == 1 ? "0" + min : min) + ":" + (sec.length() == 1 ? "0" + sec : sec);
        }
    }

    /**
     * Changes the view to non playing state
     * - icon is changed to play arrow
     * - seek bar disabled
     * - remove seek bar updater, if needed
     *
     * @param holder ViewHolder whose state is to be chagned to non playing
     */
    private void updateNonPlayingView(AudioItem item, SongsListViewHolder holder) {
        if (holder == playingHolder) {
            uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);
        }

        if (item.getResumePosition() != 0) {
            holder.sbProgress.setProgress(item.getResumePosition());
            holder.tv_startTime.setText(getDurationFromMilliSeconds(item.getResumePosition()));
        } else {
            holder.sbProgress.setProgress(0);
            holder.tv_startTime.setText(getDurationFromMilliSeconds(00));
        }

        holder.ivPlayPause.setImageResource(R.drawable.ic_play_circle_filled_red_24dp);
    }

    /**
     * Changes the view to playing state
     * - icon is changed to pause
     * - seek bar enabled
     * - start seek bar updater, if needed
     */
    private void updatePlayingView() {
        playingHolder.tv_endTime.setText(getDurationFromMilliSeconds(mediaPlayer.getDuration()));
        playingHolder.tv_startTime.setText(getDurationFromMilliSeconds(mediaPlayer.getCurrentPosition()));

        playingHolder.sbProgress.setMax(mediaPlayer.getDuration());
        playingHolder.sbProgress.setProgress(mediaPlayer.getCurrentPosition());
        playingHolder.sbProgress.setEnabled(true);

        if (mediaPlayer.isPlaying()) {
            uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
            playingHolder.ivPlayPause.setImageResource(R.drawable.ic_pause_circle_filled_red_24dp);
        } else {
            uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);
            playingHolder.ivPlayPause.setImageResource(R.drawable.ic_play_circle_filled_red_24dp);
        }
    }


    public void stopPlayer() {
        if (mediaPlayer != null) {
            releaseMediaPlayer();
        }
    }

    private void startMediaPlayer(final AudioItem item, String fileUrl) {
//        mediaPlayer = MediaPlayer.create(getApplicationContext(), item.getAudioResId());
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(mContext, Uri.parse(fileUrl));
            mediaPlayer.prepare();
        } catch (IOException e) {
            AppUtils.getInstance().showCustomToast(mContext, "Media not ready try again");
            e.printStackTrace();
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                releaseMediaPlayer();
            }
        });
        if (item.getResumePosition() != 0) {
            mediaPlayer.seekTo(item.getResumePosition());
        }
        if (item.isVolumeMuted()) {
            mediaPlayer.setVolume(0, 0);
        }
        mediaPlayer.start();
    }

    private void releaseMediaPlayer() {
        if (mediaPlayer.getDuration() == mediaPlayer.getCurrentPosition()) {
            mList.get(playingPosition).setResumePosition(00);
        } else
            mList.get(playingPosition).setResumePosition(mediaPlayer.getCurrentPosition());

        if (null != playingHolder) {
            updateNonPlayingView(mList.get(playingPosition), playingHolder);
        }
        mediaPlayer.release();
        mediaPlayer = null;
        playingPosition = -1;
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MSG_UPDATE_SEEK_BAR: {
                playingHolder.sbProgress.setProgress(mediaPlayer.getCurrentPosition());
                playingHolder.tv_startTime.setText(getDurationFromMilliSeconds(mediaPlayer.getCurrentPosition()));
                uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
                return true;
            }
        }
        return false;
    }

    public void showDialog(Activity activity, String title, String desc) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_for_lyrics_song);

        (dialog.findViewById(R.id.crossIV)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((TextView) dialog.findViewById(R.id.titleTV)).setText(title);
        ((TextView) dialog.findViewById(R.id.lyricsTV)).setText(AppUtils.getInstance().fromHtml(desc));

        dialog.show();

        Window window = dialog.getWindow();
        if (window != null)
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
}
