package com.db.divya_new.dharamDarshan.Songs.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.dharamDarshan.Songs.viewHolder.SongsViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.util.AppPreferences;
import com.db.util.ImageUtil;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;
import com.db.util.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class SongsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {
    public static final int VIEW_TYPE_BANNER = 4;
    private final static int VIEW_TYPE_AD = 25;
    private static final int VIEW_TYPE_SONGS = 2;

    private SongsViewHolder songsViewHolder;
    private String mSuperParentId;
    private Activity mContext;
    private ArrayList<Object> mList;
    private CategoryInfo categoryInfo;
    private List<BannerInfo> bannerInfoList;

    private boolean isRefreshing = false;

    public SongsAdapter(Activity context, String id, CategoryInfo categoryInfo) {
        mList = new ArrayList<>();
        mContext = context;
        mSuperParentId = id;
        this.categoryInfo = categoryInfo;
        /*Banner*/
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    public void addAll(ArrayList<Object> objectList) {
        mList.addAll(objectList);
        addBanner();
        addAds();
        notifyDataSetChanged();
    }

    private void addBanner() {
//        if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
            for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
                if (!bannerInfoList.get(bannerIndex).isAdded)
                    if (Integer.parseInt(bannerInfoList.get(bannerIndex).catPos) > -1) {
                        mList.add(0, bannerInfoList.get(bannerIndex));
                        bannerInfoList.get(bannerIndex).isAdded = true;
                    }
            }
//        }
    }

    private void addAds() {
        isBannerLoaded = false;
        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
            mList.add(0, AdController.showATFForAllListing(mContext, this));
        }
    }

    public void destroyMediaPlayer() {
        if (songsViewHolder != null) {
            if (songsViewHolder.songsListAdapter != null) {
                songsViewHolder.songsListAdapter.stopPlayer();
            }
        }
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;
        notifyItemChanged(0);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));
            case VIEW_TYPE_SONGS:
                songsViewHolder = new SongsViewHolder(inflater.inflate(R.layout.listitem_recylerview_title, parent, false), mContext);
                return songsViewHolder;
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData((BannerInfo) mList.get(holder.getAdapterPosition()));
                break;
            case VIEW_TYPE_AD:
//                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
//                adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
                if (((AdInfo) mList.get(position)).catType == 1) {
                    if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                        adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);

                    } else {
                        ImageView imageView = new ImageView(mContext);
                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setAdjustViewBounds(true);
                        imageView.setClickable(true);
                        String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                        ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                        adViewHolder.addAdsUrl(imageView);

                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AppUtils.getInstance().clickOnAdBanner(mContext);
                            }
                        });
                    }
                } else {
                    adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
                }
                break;
            case VIEW_TYPE_SONGS:
                songsBindHolder((SongsViewHolder) holder, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mList.get(position);
        if (object instanceof AdInfo) {
            return VIEW_TYPE_AD;
        } else if (object instanceof BannerInfo) {
            return VIEW_TYPE_BANNER;
        }
        return VIEW_TYPE_SONGS;
    }


    /******** Bind Holders Methods ********/

    private void songsBindHolder(SongsViewHolder viewHolder, int position) {
        viewHolder.setData(categoryInfo, isRefreshing);
    }

    public void onSwipeRefresh(SwipeRefreshLayout swipeRefreshLayout) {
        isRefreshing = true;
        notifyDataSetChanged();
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
