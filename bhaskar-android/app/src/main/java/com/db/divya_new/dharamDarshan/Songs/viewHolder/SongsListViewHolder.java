package com.db.divya_new.dharamDarshan.Songs.viewHolder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.divya_new.data.AudioItem;
import com.db.divya_new.dharamDarshan.SongBinder;

import static com.db.main.BaseAppCompatActivity.INTENT_FILTER_OVERLAY_RESULT;

public class SongsListViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivVolume, iv_pic, ivPlayPause;
    public TextView tv_title;
    public TextView tv_endTime;
    public TextView tv_startTime;
    public ImageView tv_download;
    public ImageView tv_read;
    public SeekBar sbProgress;
    public Context context;
    private BroadcastReceiver broadcastReceiver;
    private AudioItem audioItem;

    public SongsListViewHolder(View itemView) {
        super(itemView);
        this.context = itemView.getContext();
        sbProgress = itemView.findViewById(R.id.sbProgress);
        tv_read = itemView.findViewById(R.id.tv_read);
        tv_download = itemView.findViewById(R.id.tv_download);
        tv_startTime = itemView.findViewById(R.id.tv_startTime);
        tv_endTime = itemView.findViewById(R.id.tv_endTime);
        tv_title = itemView.findViewById(R.id.tv_title);
        ivPlayPause = itemView.findViewById(R.id.ivPlayPause);
        ivVolume = itemView.findViewById(R.id.ivVolume);
        iv_pic = itemView.findViewById(R.id.iv_pic);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SongBinder.startMusicService(context.getApplicationContext(), audioItem);
            }
        };
    }

    public void registerReceiver() {
        LocalBroadcastManager.getInstance(context).registerReceiver(broadcastReceiver, new IntentFilter(INTENT_FILTER_OVERLAY_RESULT));
    }

    public void unRegisterReceiver() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiver);
    }

    public void bindContent(AudioItem item) {
        this.audioItem = item;
    }
}
