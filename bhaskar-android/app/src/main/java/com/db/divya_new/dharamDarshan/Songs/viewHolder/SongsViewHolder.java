package com.db.divya_new.dharamDarshan.Songs.viewHolder;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.LoadMoreController;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.AudioItem;
import com.db.divya_new.data.CatHome;
import com.db.divya_new.data.CatHomeData;
import com.db.divya_new.dharamDarshan.Songs.adapter.SongListAdapter;
import com.db.util.AppUtils;
import com.db.util.DividerItemDecoration;
import com.db.util.NetworkStatus;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class SongsViewHolder extends RecyclerView.ViewHolder implements LoadMoreController {

    public RecyclerView recyclerView;
    public SongListAdapter songsListAdapter;
    public Activity mContext;
    private TextView titleTextView;
    private String mSuperParentId;
    private Button loadMoreButton;
    private Button btn_retry;
    private ProgressBar loadMoreProgressBar;
    private int pageCount = 1;
    private boolean isLoading;
    private boolean isLoadMoreEnabled = true;
    private boolean isLoadMoreClicked = false;
    private String mFeedUrl;
    private String mParentId;
    private boolean isFromHome;
    private boolean isFromParentHome;
    private int showCount;
    private String id;
    private String pageTitle;
    private Button btnSubHeaderLoadMore;

    public SongsViewHolder(View itemView, Activity activity) {
        super(itemView);
        mContext = activity;

        titleTextView = itemView.findViewById(R.id.tv_title);
        titleTextView.setVisibility(View.GONE);

        loadMoreButton = itemView.findViewById(R.id.btn_load_more);
        btn_retry = itemView.findViewById(R.id.btn_retry);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        btnSubHeaderLoadMore = itemView.findViewById(R.id.btnSubHeaderLoadMore);
        songsListAdapter = new SongListAdapter(mContext);

        recyclerView = itemView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(mContext));
        recyclerView.setAdapter(songsListAdapter);

        loadMoreButton.setVisibility(View.GONE);
        loadMoreProgressBar.setVisibility(View.GONE);

        // load more button clicked, to add more data in list
        loadMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLoadMore();
            }
        });
        btn_retry.setOnClickListener(view -> {
            if (!isLoading) {
                fetchData(false);
            }
        });

    }

    private void onLoadMore() {
        if (isFromParentHome && isFromHome) {
            if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {

                loadMoreButton.setVisibility(View.GONE);
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TabController.getInstance().setTabClicked(true);
                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mSuperParentId, mParentId, SongsViewHolder.this);

                    }
                }, 200);
            }
        } else if (isFromHome) {
            if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {

                loadMoreButton.setVisibility(View.GONE);
                loadMoreProgressBar.setVisibility(View.VISIBLE);

                TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(mParentId);
            }
        } else if (!isFromHome && !isFromParentHome) {
            if (!isLoading) {
                fetchData(false);
            }
        }
    }

    private void loadmoreLogic() {
        if (isFromHome) {
            btnSubHeaderLoadMore.setVisibility(View.VISIBLE);
        }

        btnSubHeaderLoadMore.setOnClickListener(v -> onLoadMore());

    }

    public void clearDataAndLoadNew() {
        songsListAdapter.clear();
    }

    public void setData(CategoryInfo categoryInfo, boolean isRefreshing) {
        this.mFeedUrl = categoryInfo.feedUrl;
        this.mParentId = categoryInfo.id;
        this.mSuperParentId = categoryInfo.parentId;
        this.isFromHome = false;
        this.isFromParentHome = false;
        this.showCount = 0;
        this.id = categoryInfo.id;

        loadMoreButton.setText(R.string.load_more);

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && pageCount <= 2 && !isRefreshing) {
            parseOfflineData(catHomeData, true);
            pageCount = 2;
            if (!TextUtils.isEmpty(catHomeData.pageTitle)) {
                setPageTitle(catHomeData.pageTitle);
            }
        } else {
            pageCount = 1;
            if (!isLoading)
                fetchData(true);
            setUpPageTitle(categoryInfo.extraVlues);
        }
        songsListAdapter.setData(categoryInfo.displayName, mParentId, false, false);
    }

    public void setData(CatHome catHome, boolean isFromHome, boolean isFromParentHome) {
        this.mFeedUrl = catHome.categoryInfo.feedUrl;
        this.mParentId = catHome.parentId;
        this.mSuperParentId = catHome.superParentId;
        this.isFromHome = isFromHome;
        this.isFromParentHome = isFromParentHome;
        if (TextUtils.isEmpty(catHome.count))
            this.showCount = 0;
        else
            this.showCount = Integer.parseInt(catHome.count);
        this.id = catHome.categoryInfo.id;

        loadMoreButton.setText(R.string.see_more);


        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && pageCount <= 2) {
            parseOfflineData(catHomeData, true);
            pageCount = 2;
            if (!TextUtils.isEmpty(catHomeData.pageTitle)) {
                setPageTitle(catHomeData.pageTitle);
            }
        } else {
            pageCount = 1;
            fetchData(true);
            setUpPageTitle(catHome.categoryInfo.extraVlues);
        }

        songsListAdapter.setData(catHome.categoryInfo.displayName, catHome.parentId, isFromHome, isFromParentHome);


    }

    // Adding the title is available in the categoryInfo else it will be gone
    private void setUpPageTitle(String extraVlues) {
        String pageTitle = null;
        if (!TextUtils.isEmpty(extraVlues)) {
            try {
                if (new JSONObject(extraVlues).has("category_intro")) {
                    pageTitle = new JSONObject(extraVlues).optString("category_intro");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.pageTitle = pageTitle;

            setPageTitle(pageTitle);
        }
    }

    public void setPageTitle(String pageTitle) {
        TextView tvTitle = itemView.findViewById(R.id.tv_title);

        if (!TextUtils.isEmpty(pageTitle)) {
            this.pageTitle = pageTitle;
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(pageTitle);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                tvTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        } else {
            tvTitle.setVisibility(View.GONE);
        }
    }

    private void fetchData(boolean isClear) {
        loadMoreButton.setVisibility(View.GONE);
        loadMoreProgressBar.setVisibility(View.VISIBLE);

        try {
            fetchGuruDataFromServer(Urls.APP_FEED_BASE_URL + mFeedUrl + "PG" + pageCount + "/", isClear);
        } catch (Exception e) {
        }
    }

    private void fetchGuruDataFromServer(String url, final boolean isClear) {
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            isLoading = true;

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loadMoreProgressBar.setVisibility(View.GONE);
                    parseOnlineData(response, isClear);
                    isLoading = false;
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Activity activity = mContext;
                    if (activity != null) {
                        if (error instanceof NoConnectionError || error instanceof NetworkError) {
                            AppUtils.getInstance().showCustomToast(activity, mContext.getResources().getString(R.string.no_network_error));
                        } else {
                            AppUtils.getInstance().showCustomToast(activity, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                        }
                    }
                    isLoading = false;
                    loadMoreProgressBar.setVisibility(View.GONE);

                    btn_retry.setVisibility(View.VISIBLE);
                }
            });

            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            if (!isFromHome && !isFromParentHome)
                VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(request);
            else
                VolleyNetworkSingleton.getInstance(mContext, 2).addToRequestQueue(request);
        } else {
            if (mContext != null) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
            }
            isLoading = false;
            loadMoreProgressBar.setVisibility(View.GONE);

            btn_retry.setVisibility(View.VISIBLE);
        }
    }

    private void parseOnlineData(JSONObject jsonObject, boolean isClear) {
        int count = jsonObject.optInt("count");
        JSONArray jsonArray = jsonObject.optJSONArray("feed");
        if (jsonArray != null) {
            ArrayList<AudioItem> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), AudioItem[].class)));
            if (pageCount <= 1 && !TextUtils.isEmpty(id)) {
                CatHomeData catHomeData = new CatHomeData(list, count, CatHomeData.TYPE_SONGS, System.currentTimeMillis());
                if (!TextUtils.isEmpty(pageTitle)) {
                    catHomeData.pageTitle = pageTitle;
                }
                DataParser.getInstance().addCatHomeDataHashMap(id, catHomeData);
            }
            if (jsonArray.length() < count) {
                isLoadMoreEnabled = false;
            } else {
                isLoadMoreEnabled = true;
                pageCount++;
            }
            handleUI(isClear, list);
        }
    }

    private void parseOfflineData(CatHomeData catHomeData, boolean isClear) {
        int count = catHomeData.count;
        ArrayList<AudioItem> list = (ArrayList<AudioItem>) catHomeData.getArrayList(CatHomeData.TYPE_SONGS);
        if (list != null) {
            if (list.size() < count) {
                isLoadMoreEnabled = false;
            } else {
                isLoadMoreEnabled = true;
            }
            handleUI(isClear, list);
        }
    }

    public void handleUI(boolean isClear, ArrayList<AudioItem> list) {

        //passing the data to the adapter to show.
        if (showCount > 0 && showCount < list.size()) {
            if (isClear) {
                songsListAdapter.clear();
            }
            for (int i = 0; i < showCount; i++) {
                songsListAdapter.add(list.get(i));
            }
            isLoadMoreEnabled = true;
        } else {
            if (isClear) {
                songsListAdapter.addAndClearList(list);
            } else
                songsListAdapter.addList(list);
            if (isFromHome) isLoadMoreEnabled = false;
        }

        // handling the load more button based on the data we got
        if (isLoadMoreEnabled) {
//            loadMoreButton.setVisibility(View.VISIBLE);
            loadMoreButton.setVisibility(View.GONE);
        } else {
            loadMoreButton.setVisibility(View.GONE);
        }

        btn_retry.setVisibility(View.GONE);
        loadmoreLogic();
    }

    @Override
    public void onLoadFinish() {
//        loadMoreButton.setVisibility(View.VISIBLE);
        loadMoreButton.setVisibility(View.GONE);
        btn_retry.setVisibility(View.GONE);
        loadMoreProgressBar.setVisibility(View.GONE);
    }
}