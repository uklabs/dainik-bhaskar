package com.db.divya_new.divyashree;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.bhaskar.util.Action;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.NewsParentViewHolder;
import com.db.divya_new.common.TabController;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.common.viewholder.RajyaCommonCatHomeViewHolder;
import com.db.divya_new.data.CatHome;
import com.db.divya_new.data.GuruvaniGuruTabInfo;
import com.db.divya_new.dharamDarshan.Songs.viewHolder.SongsViewHolder;
import com.db.divya_new.divyashree.viewholder.StoryViewHolder;
import com.db.divya_new.gujarati.GujaratiVideoViewHolder;
import com.db.divya_new.guruvani.viewholder.GuruvaniGuruViewHolder;
import com.db.divya_new.guruvani.viewholder.GuruvaniTopicViewHolder;
import com.db.divya_new.jyotishVastu.DivyaRashifalViewHolder;
import com.db.divya_new.jyotishVastu.RatnaViewHolder;
import com.db.divya_new.photoGallary.PhotoGallaryParentViewHolder;
import com.db.news.rajya.SearchViewHolder;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CatHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {

    public final static int ACTION_STORY_LIST = 2;
    public final static int ACTION_NEWS = 3;
    public final static int ACTION_RASHIFAL_V2 = 5;
    public final static int ACTION_RATNA = 6;
    public final static int ACTION_GURUVANI_GURU = 7;
    public final static int ACTION_GURUVANI_TOPIC = 8;
    public final static int ACTION_SONGS = 10;
    private final static int ACTION_VIDEO = 17;
    private final static int ACTION_BLANK = 19;
    private final static int ACTION_STORY_LIST_V2 = 20;
    private final static int VIEW_TYPE_PHOTO_GALLARY = 21;
    private final static int ACTION_RAJYA_LIST = 22;
    private final static int ACTION_SEARCH_WIDGET = 23;

    private final static int VIEW_TYPE_AD = 25;

    private ArrayList<Object> mList;
    private CategoryInfo categoryInfo;
    private String mSuperParentId;
    private Activity mContext;
    private boolean isFromParentHome = false;

    private boolean isRefreshing = false;

    public CatHomeAdapter(Activity context, String id, CategoryInfo categoryInfo) {
        mList = new ArrayList<>();
        this.mContext = context;
        this.categoryInfo = categoryInfo;
        mSuperParentId = id;
    }

    public void setFromParentHome(boolean fromParentHome) {
        isFromParentHome = fromParentHome;
    }

    public void add(CatHome object) {
        mList.add(object);
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<CatHome> object) {
        mList.addAll(object);

        if (!isFromParentHome) {
            addAds();
        }
        notifyDataSetChanged();
    }


    private void addAds() {
        isBannerLoaded = false;
        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
            mList.add(0, AdController.showATFForAllListing(mContext, this));
        }
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;
        notifyItemChanged(0);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ACTION_STORY_LIST:
                return new StoryViewHolder(inflater.inflate(R.layout.listitem_recylerview_title_story, parent, false), mContext,  false, false);
            case ACTION_STORY_LIST_V2:
                return new StoryViewHolder(inflater.inflate(R.layout.listitem_recylerview_title_story, parent, false), mContext,  false, true);
            case ACTION_NEWS:
                return new NewsParentViewHolder(mContext, inflater.inflate(R.layout.listitem_recylerview_title_news, parent, false));
            case ACTION_RASHIFAL_V2:
                return new DivyaRashifalViewHolder(mContext, inflater.inflate(R.layout.listitem_recylerview_title_rashifal, parent, false), true, null);
            case ACTION_RATNA:
                return new RatnaViewHolder(inflater.inflate(R.layout.listitem_recylerview_title, parent, false), mContext, true,  false);
            case ACTION_GURUVANI_GURU:
                return new GuruvaniGuruViewHolder(mContext, inflater.inflate(R.layout.listitem_recylerview_title_tab, parent, false), mSuperParentId);
            case ACTION_GURUVANI_TOPIC:
                return new GuruvaniTopicViewHolder(inflater.inflate(R.layout.listitem_recylerview_title_tab, parent, false), mSuperParentId, mContext);
            case ACTION_SONGS:
                return new SongsViewHolder(inflater.inflate(R.layout.listitem_recylerview_title, parent, false), mContext);
            case ACTION_VIDEO:
                return new GujaratiVideoViewHolder(inflater.inflate(R.layout.listitem_recylerview_title, parent, false), mContext);
            case VIEW_TYPE_PHOTO_GALLARY:
                return new PhotoGallaryParentViewHolder(inflater.inflate(R.layout.listitem_recylerview_title, parent, false), categoryInfo.parentId);
            case ACTION_RAJYA_LIST:
                return new RajyaCommonCatHomeViewHolder(inflater.inflate(R.layout.listitem_rajya_common, parent, false), categoryInfo);
            case ACTION_SEARCH_WIDGET:
                return new SearchViewHolder(inflater.inflate(R.layout.layout_search_city, parent, false));
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));

            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ACTION_GURUVANI_GURU:
                guruVaniBindHolder((GuruvaniGuruViewHolder) holder, position);
                break;
            case ACTION_GURUVANI_TOPIC:
                guruVaniTopicBindHolder((GuruvaniTopicViewHolder) holder, position);
                break;
            case ACTION_STORY_LIST:
                StoryBindHolder((StoryViewHolder) holder, position);
                break;
            case ACTION_STORY_LIST_V2:
                StoryBindHolder((StoryViewHolder) holder, position);
                break;
            case ACTION_SONGS:
                songsBindHolder((SongsViewHolder) holder, position);
                break;
            case ACTION_RATNA:
                RatnaBindHolder((RatnaViewHolder) holder, position);
                break;
            case ACTION_NEWS:
                NewsBindHolder((NewsParentViewHolder) holder, position);
                break;
            case ACTION_RASHIFAL_V2:
                RashifalBindHolder((DivyaRashifalViewHolder) holder, position);
                break;
            case ACTION_VIDEO:
                videoBindHolder((GujaratiVideoViewHolder) holder, position);
                break;
            case VIEW_TYPE_PHOTO_GALLARY:
                photoGallaryBindHolder((PhotoGallaryParentViewHolder) holder, position);
                break;
            case ACTION_RAJYA_LIST:
                rajyaListBindHolder((RajyaCommonCatHomeViewHolder) holder, position);
                break;
            case ACTION_SEARCH_WIDGET:
                bindSearchViewHolder((SearchViewHolder) holder, position);
            case ACTION_BLANK:
                break;
            case VIEW_TYPE_AD:
                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
                if (((AdInfo) mList.get(position)).catType == 1) {
                    if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                        adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);

                    } else {
                        ImageView imageView = new ImageView(mContext);
                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setAdjustViewBounds(true);
                        imageView.setClickable(true);
                        String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                        ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                        adViewHolder.addAdsUrl(imageView);

                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AppUtils.getInstance().clickOnAdBanner(mContext);
                            }
                        });
                    }
                } else {
                    adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
                }
                break;
        }

    }

    private void rajyaListBindHolder(RajyaCommonCatHomeViewHolder holder, int position) {
        CatHome catHome = (CatHome) mList.get(position);
        String menuName = catHome.categoryInfo.menuName;
        holder.tvHeading.setText(menuName);
        holder.setData(catHome);
    }

    private void bindSearchViewHolder(SearchViewHolder holder, int position) {
        holder.setData(categoryInfo);
    }

    private void photoGallaryBindHolder(PhotoGallaryParentViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);

        if (!isFromParentHome) {
            setTitle(viewHolder, catHome);
        } else if (position != 0 && (!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))) {
            setSubTitle(viewHolder, catHome);
        } else {
            viewHolder.clearDataAndLoadNew();
            setVisibilityGoneView(viewHolder);
        }

        if (TextUtils.isEmpty(catHome.count)) catHome.count = "0";
        viewHolder.setIsRefreshing(isRefreshing);
        viewHolder.setData(catHome.categoryInfo, Integer.parseInt(catHome.count), true, isFromParentHome, catHome.parentId, catHome.superParentId, catHome.horizontalList);
    }

    private void setVisibilityGoneView(final RecyclerView.ViewHolder viewHolder) {
        View view = null;
        switch (viewHolder.getItemViewType()) {
            case ACTION_VIDEO:
                view = ((GujaratiVideoViewHolder) viewHolder).itemView;
                break;
            case VIEW_TYPE_PHOTO_GALLARY:
                view = ((PhotoGallaryParentViewHolder) viewHolder).itemView;
                break;
            case ACTION_STORY_LIST:
                view = ((StoryViewHolder) viewHolder).itemView;
                break;
            case ACTION_STORY_LIST_V2:
                view = ((StoryViewHolder) viewHolder).itemView;
                break;
            case ACTION_NEWS:
                view = ((NewsParentViewHolder) viewHolder).itemView;
                break;
            case ACTION_GURUVANI_GURU:
                view = ((GuruvaniGuruViewHolder) viewHolder).itemView;
                break;
            case ACTION_GURUVANI_TOPIC:
                view = ((GuruvaniTopicViewHolder) viewHolder).itemView;
                break;
            case ACTION_RASHIFAL_V2:
                view = ((DivyaRashifalViewHolder) viewHolder).itemView;
                break;
            case ACTION_RATNA:
                view = ((RatnaViewHolder) viewHolder).itemView;
                break;
            case ACTION_SONGS:
                view = ((SongsViewHolder) viewHolder).itemView;
                break;
        }

        LinearLayout subParentHeaderLL = view.findViewById(R.id.sub_header_title_LL);
        LinearLayout parentHeaderLL = view.findViewById(R.id.header_cat_title_LL);

        subParentHeaderLL.setVisibility(View.GONE);
        parentHeaderLL.setVisibility(View.GONE);
    }


    private void StoryBindHolder(final StoryViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);

        if (!isFromParentHome) {
            setTitle(viewHolder, catHome);

        } else if (position != 0 && (!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))) {
            setSubTitle(viewHolder, catHome);

        } else {
            viewHolder.clearDataAndLoadNew();
            setVisibilityGoneView(viewHolder);
        }
        viewHolder.setIsRefreshing(isRefreshing);
        viewHolder.setData(catHome.categoryInfo, catHome.parentId, catHome.superParentId, true, isFromParentHome, Integer.parseInt(catHome.count), catHome.horizontalList);

    }

    private void guruVaniBindHolder(final GuruvaniGuruViewHolder viewHolder, int listPosition) {
        CatHome catHome = (CatHome) mList.get(listPosition);
        if (!isFromParentHome) {
            setTitle(viewHolder, catHome);
        } else if (listPosition != 0 && (!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))) {
            setSubTitle(viewHolder, catHome);
        } else {
            viewHolder.clearDataAndLoadNew();
            setVisibilityGoneView(viewHolder);
        }


        if (catHome.categoryInfo != null && Constants.guruTabInfo == null) {
            Constants.guruTabInfo = new Gson().fromJson(catHome.categoryInfo.extraVlues, GuruvaniGuruTabInfo.class);
            if (Constants.guruTabInfo != null)
                AppPreferences.getInstance(mContext).setStringValue(QuickPreferences.DivyaNew.VIDEO_REC_URL, Constants.guruTabInfo.videoRecUrl);
        }

        viewHolder.setData(catHome.categoryInfo, true, isFromParentHome, catHome.parentId, catHome.superParentId, Integer.parseInt(catHome.count));

    }

    private void guruVaniTopicBindHolder(final GuruvaniTopicViewHolder viewHolder,
                                         int listPosition) {
        CatHome catHome = (CatHome) mList.get(listPosition);
        if (!isFromParentHome) {
            setTitle(viewHolder, catHome);
        } else if (listPosition != 0 && (!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))) {
            setSubTitle(viewHolder, catHome);
        } else {
            viewHolder.clearDataAndLoadNew();
            setVisibilityGoneView(viewHolder);
        }

        if (catHome.categoryInfo != null && !TextUtils.isEmpty(catHome.categoryInfo.extraVlues) && TextUtils.isEmpty(AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.DivyaNew.VIDEO_REC_URL, ""))) {
            try {
                JSONObject jsonObject = new JSONObject(catHome.categoryInfo.extraVlues);
                String videoRecUrl = jsonObject.getString("video_rec_url");

                AppPreferences.getInstance(mContext).setStringValue(QuickPreferences.DivyaNew.VIDEO_REC_URL, videoRecUrl);
            } catch (Exception ignored) {
            }
        }
        viewHolder.setData(catHome.categoryInfo, Integer.parseInt(catHome.count), true, isFromParentHome, catHome.parentId, catHome.superParentId);
    }

    private void songsBindHolder(SongsViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);

        if (!isFromParentHome) {
            setTitle(viewHolder, catHome);
        } else if (position != 0 && (!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))) {
            setSubTitle(viewHolder, catHome);
        } else {
            viewHolder.clearDataAndLoadNew();
            setVisibilityGoneView(viewHolder);
        }

        viewHolder.setData(catHome, true, isFromParentHome);
    }

    private void RatnaBindHolder(RatnaViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);

        if (!isFromParentHome) {
            setTitle(viewHolder, catHome);
        } else if (position != 0 && (!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))) {
            setSubTitle(viewHolder, catHome);
        } else {
            viewHolder.clearDataAndLoadNew();
            setVisibilityGoneView(viewHolder);
        }

        viewHolder.setData(catHome, isFromParentHome);
    }

    private void RashifalBindHolder(DivyaRashifalViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);

        if (!isFromParentHome) {
            setTitle(viewHolder, catHome);
        } else if (position != 0 && (!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))) {
            setSubTitle(viewHolder, catHome);
        } else {
            viewHolder.clearDataAndLoadNew();
            setVisibilityGoneView(viewHolder);
        }

        viewHolder.setData(catHome);
    }

    private void NewsBindHolder(NewsParentViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);

        if (!isFromParentHome) {
            setTitle(viewHolder, catHome);
        } else if (position != 0 && (!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))) {
            setSubTitle(viewHolder, catHome);
        } else {
            setVisibilityGoneView(viewHolder);
            viewHolder.clearDataAndLoadNew();
        }

        viewHolder.setCategoryInfo(catHome.categoryInfo);
        viewHolder.setData(catHome, true, isFromParentHome);
        viewHolder.rewindMinHeight(catHome.count, catHome.categoryInfo);
    }

    private void videoBindHolder(GujaratiVideoViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);

        if (!isFromParentHome) {
            setTitle(viewHolder, catHome);
        } else if (position != 0 && (!TextUtils.isEmpty(catHome.type) && catHome.type.equalsIgnoreCase("1"))) {
            setSubTitle(viewHolder, catHome);
        } else {
            viewHolder.clearDataAndLoadNew();
            setVisibilityGoneView(viewHolder);
        }
        viewHolder.setIsRefreshing(isRefreshing);
        viewHolder.setData(catHome.categoryInfo, Integer.parseInt(catHome.count), true, isFromParentHome, catHome.parentId, catHome.superParentId, categoryInfo.displayName);
    }

    private void setTitle(RecyclerView.ViewHolder viewHolder, CatHome catHome) {
        String title = catHome.headerTitle;
        int color = AppUtils.getThemeColor(mContext, catHome.categoryInfo.color);

        final String mParentId = catHome.parentId;
//        final String mSuperParentId = catHome.superParentId;
        View view = null;
        switch (viewHolder.getItemViewType()) {
            case ACTION_VIDEO:
                view = ((GujaratiVideoViewHolder) viewHolder).itemView;
                break;
            case VIEW_TYPE_PHOTO_GALLARY:
                view = ((PhotoGallaryParentViewHolder) viewHolder).itemView;
                break;
            case ACTION_STORY_LIST:
                view = ((StoryViewHolder) viewHolder).itemView;
                break;
            case ACTION_STORY_LIST_V2:
                view = ((StoryViewHolder) viewHolder).itemView;
                break;
            case ACTION_NEWS:
                view = ((NewsParentViewHolder) viewHolder).itemView;
                break;
            case ACTION_GURUVANI_GURU:
                view = ((GuruvaniGuruViewHolder) viewHolder).itemView;
                break;
            case ACTION_GURUVANI_TOPIC:
                view = ((GuruvaniTopicViewHolder) viewHolder).itemView;
                break;
            case ACTION_RASHIFAL_V2:
                view = ((DivyaRashifalViewHolder) viewHolder).itemView;
                break;
            case ACTION_RATNA:
                view = ((RatnaViewHolder) viewHolder).itemView;
                break;
            case ACTION_SONGS:
                view = ((SongsViewHolder) viewHolder).itemView;
                break;
        }

        LinearLayout parentHeaderLL = view.findViewById(R.id.header_cat_title_LL);
        TextView headerTitleTV = view.findViewById(R.id.cat_headerTitleTV);
        Button btnSubHeaderLoadMore = view.findViewById(R.id.btnSubHeaderLoadMore);
        View headerLineTV = view.findViewById(R.id.divider_view);

        headerLineTV.setBackgroundColor(color);
        btnSubHeaderLoadMore.setTextColor(color);
//        headerTitleTV.setTextColor(color);

        if (!TextUtils.isEmpty(title)) {
            parentHeaderLL.setVisibility(View.VISIBLE);
            headerTitleTV.setText(title);
        } else {
            parentHeaderLL.setVisibility(View.GONE);
        }

        parentHeaderLL.setOnClickListener(v -> {
            if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(mParentId);
            }
        });
    }

    private void setSubTitle(final RecyclerView.ViewHolder viewHolder, CatHome catHome) {
        String title = catHome.headerTitle, color;
        color = catHome.categoryInfo.color;
        final String mParentId = catHome.parentId;
        final String mSuperParentId = catHome.superParentId;
        View view = null;
        switch (viewHolder.getItemViewType()) {
            case ACTION_VIDEO:
                view = ((GujaratiVideoViewHolder) viewHolder).itemView;
                break;
            case VIEW_TYPE_PHOTO_GALLARY:
                view = ((PhotoGallaryParentViewHolder) viewHolder).itemView;
                break;
            case ACTION_STORY_LIST:
                view = ((StoryViewHolder) viewHolder).itemView;
                break;
            case ACTION_STORY_LIST_V2:
                view = ((StoryViewHolder) viewHolder).itemView;
                break;
            case ACTION_NEWS:
                view = ((NewsParentViewHolder) viewHolder).itemView;
                break;
            case ACTION_GURUVANI_GURU:
                view = ((GuruvaniGuruViewHolder) viewHolder).itemView;
                break;
            case ACTION_GURUVANI_TOPIC:
                view = ((GuruvaniTopicViewHolder) viewHolder).itemView;
                break;
            case ACTION_RASHIFAL_V2:
                view = ((DivyaRashifalViewHolder) viewHolder).itemView;
                break;
            case ACTION_RATNA:
                view = ((RatnaViewHolder) viewHolder).itemView;
                break;
            case ACTION_SONGS:
                view = ((SongsViewHolder) viewHolder).itemView;
                break;
        }

        LinearLayout subParentHeaderLL = view.findViewById(R.id.sub_header_title_LL);
        TextView subHeaderTitleTV = view.findViewById(R.id.subHeaderTitleTV);

        TextView subHeaderLineTV1 = view.findViewById(R.id.subHeaderLineTV1);
        TextView subHeaderLineTV2 = view.findViewById(R.id.subHeaderLineTV2);

        int colortheme = AppUtils.getThemeColor(mContext, color);
        subHeaderLineTV1.setBackgroundColor(colortheme);
        subHeaderLineTV2.setBackgroundColor(colortheme);
        subHeaderTitleTV.setTextColor(colortheme);
        if (!TextUtils.isEmpty(title)) {
            subParentHeaderLL.setVisibility(View.VISIBLE);
            subHeaderTitleTV.setText(title);
        } else {
            subParentHeaderLL.setVisibility(View.GONE);
        }
        subParentHeaderLL.setOnClickListener(v -> {
            if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                TabController.getInstance().setTabClicked(true);
                TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mSuperParentId, mParentId, null);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mList.get(position);
        if (object instanceof AdInfo) {
            return VIEW_TYPE_AD;
        } else if (object instanceof CatHome) {
            CatHome catHome = (CatHome) mList.get(position);
            return getViewHolderType(catHome.action);
        }
        return 0;
    }

    private int getViewHolderType(String action) {

        switch (action) {
            case Action.CategoryAction.CAT_ACTION_STORY_LIST:
                return ACTION_STORY_LIST;
            case Action.CategoryAction.CAT_ACTION_STORY_LIST_V2:
                return ACTION_STORY_LIST_V2;
            case Action.CategoryAction.CAT_ACTION_NEWS:
                return ACTION_NEWS;
            case Action.CategoryAction.CAT_ACTION_RASHIFAL_V2:
                return ACTION_RASHIFAL_V2;
            case Action.CategoryAction.CAT_ACTION_RATNA:
                return ACTION_RATNA;
            case Action.CategoryAction.CAT_ACTION_GURUVANI_GURU:
                return ACTION_GURUVANI_GURU;
            case Action.CategoryAction.CAT_ACTION_GURUVANI_TOPIC:
                return ACTION_GURUVANI_TOPIC;
            case Action.CategoryAction.CAT_ACTION_SONGS:
                return ACTION_SONGS;
            case Action.CategoryAction.CAT_ACTION_VIDEO_GALLERY_V3:
                return ACTION_VIDEO;
            case Action.CategoryAction.CAT_ACTION_PHOTO_GALLERY:
                return VIEW_TYPE_PHOTO_GALLARY;
            case Action.CategoryAction.CAT_ACTION_RAJYA_LIST:
                return ACTION_RAJYA_LIST;
            case Action.CategoryAction.CAT_ACTION_SEARCH_WIDGET:
                return ACTION_SEARCH_WIDGET;
        }
        return ACTION_BLANK;
    }

    /******** Bind Holders Methods ********/

    public void onSwipeRefresh(SwipeRefreshLayout swipeRefreshLayout) {
        // clearing the offline data from device for the selected cat home.
        for (Object object : mList) {
            if (object instanceof CatHome) {
                CatHome catHome = (CatHome) object;
                if (catHome.categoryInfo != null)
                    DataParser.getInstance().removeCatHomeDataHashMapById(catHome.categoryInfo.id);
            }
        }
        isRefreshing = true;

        notifyDataSetChanged();
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }
}
