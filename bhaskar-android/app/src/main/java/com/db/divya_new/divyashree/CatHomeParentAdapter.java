package com.db.divya_new.divyashree;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.db.data.models.CategoryInfo;
import com.db.data.models.CityInfo;
import com.db.divya_new.CatHomeParentViewHolderNew;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.NewsParentViewHolder;
import com.db.divya_new.common.TabController;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.common.viewholder.DBColumnViewHolder;
import com.db.divya_new.common.viewholder.RajyaHomeV2ViewHolder;
import com.db.divya_new.common.viewholder.SearchPreferredCityViewHolder;
import com.db.divya_new.data.CatHome;
import com.db.divya_new.divyashree.viewholder.StoryViewHolder;
import com.db.divya_new.gujarati.GujaratiVideoViewHolder;
import com.db.divya_new.jyotishVastu.DivyaRashifalViewHolder;
import com.db.divya_new.photoGallary.PhotoGallaryParentViewHolder;
import com.db.preferredcity.PreferredCityActivity;
import com.bhaskar.util.Action;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;

import java.util.ArrayList;
import java.util.Vector;

public class CatHomeParentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {

    private final int VIEW_TYPE_CAT_HOME = 1;
    private final int VIEW_TYPE_NEWS = 2;
    private final int VIEW_TYPE_CITY = 4;
    private final int VIEW_TYPE_VIDEO = 9;
    private final static int VIEW_TYPE_PHOTO_GALLARY = 10;
    private final static int VIEW_TYPE_STORY_V2 = 11;
    private final static int VIEW_TYPE_RAJYA_V2 = 12;
    private final static int VIEW_TYPE_RASHIFAL_V2 = 13;
    private final static int VIEW_TYPE_DB_COLOUMN = 14;
    private final static int VIEW_TYPE_AD = 25;

    private ArrayList<Object> mList;
    private CategoryInfo categoryInfo;
    private String mSuperParentId;
    private Activity mContext;

    private Vector<CityInfo> cityInfoVector;

    public CatHomeParentAdapter(Activity context, String id, CategoryInfo categoryInfo) {
        mList = new ArrayList<>();
        this.mContext = context;
        this.categoryInfo = categoryInfo;
        mSuperParentId = id;
    }

    public void add(CatHome object) {
        mList.add(object);
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<CatHome> object) {
        mList.clear();
        mList.addAll(object);
        addAds();
        notifyDataSetChanged();
    }

    private void addAds() {
        isBannerLoaded = false;
        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
            mList.add(0, AdController.showATFForHomeListing(mContext, this));
        }
        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.BTF_ROS_LIST_TOGGLE, false)) {
            mList.add(mList.size(), AdController.showBTFForHomeListing(mContext));
        }
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;
        notifyItemChanged(0);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_CAT_HOME:
                return new CatHomeParentViewHolderNew(mContext, inflater.inflate(R.layout.listitem_cat_home_parent_recyclerview, parent, false), mSuperParentId, categoryInfo);
            case VIEW_TYPE_NEWS:
                return new NewsParentViewHolder(mContext, inflater.inflate(R.layout.listitem_recylerview_title_news, parent, false));
            case VIEW_TYPE_CITY:
                return new SearchPreferredCityViewHolder(inflater.inflate(R.layout.layout_preferred_city, parent, false), mContext);
            case VIEW_TYPE_VIDEO:
                return new GujaratiVideoViewHolder(inflater.inflate(R.layout.listitem_recylerview_title, parent, false), mContext);
            case VIEW_TYPE_PHOTO_GALLARY:
                return new PhotoGallaryParentViewHolder(inflater.inflate(R.layout.listitem_recylerview_title, parent, false), categoryInfo.parentId);
            case VIEW_TYPE_STORY_V2:
                return new StoryViewHolder(inflater.inflate(R.layout.listitem_recylerview_title_story, parent, false), mContext,  false, true);
            case VIEW_TYPE_RAJYA_V2:
                return new RajyaHomeV2ViewHolder(inflater.inflate(R.layout.layout_rajya_v2, parent, false));
            case VIEW_TYPE_RASHIFAL_V2:
                return new DivyaRashifalViewHolder(mContext, inflater.inflate(R.layout.listitem_recylerview_title_rashifal, parent, false), true, null);
            case VIEW_TYPE_DB_COLOUMN:
                return new DBColumnViewHolder(inflater.inflate(R.layout.listitem_recylerview_title_story, parent, false), mContext);
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_CAT_HOME:
                CatHomeParentBindHolder((CatHomeParentViewHolderNew) holder, position);
                break;
            case VIEW_TYPE_NEWS:
                NewsBindHolder((NewsParentViewHolder) holder, position);
                break;
            case VIEW_TYPE_CITY:
                SearchPreferredCity((SearchPreferredCityViewHolder) holder, position, VIEW_TYPE_CITY);
                break;
            case VIEW_TYPE_RAJYA_V2:
                RajyaV2((RajyaHomeV2ViewHolder) holder, position);
                break;
            case VIEW_TYPE_VIDEO:
                videoBindHolder((GujaratiVideoViewHolder) holder, position);
                break;
            case VIEW_TYPE_PHOTO_GALLARY:
                photoGallaryBindHolder((PhotoGallaryParentViewHolder) holder, position);
                break;
            case VIEW_TYPE_STORY_V2:
                storyV2BindHolder((StoryViewHolder) holder, position);
                break;
            case VIEW_TYPE_RASHIFAL_V2:
                RashifalBindHolder((DivyaRashifalViewHolder) holder, position);
                break;
            case VIEW_TYPE_DB_COLOUMN:
                DBColumnBindHolder((DBColumnViewHolder) holder, position);
                break;
            case VIEW_TYPE_AD:
                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
                if (((AdInfo) mList.get(position)).catType == 1) {
                    if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                        adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);

                    } else {
                        ImageView imageView = new ImageView(mContext);
                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setAdjustViewBounds(true);
                        imageView.setClickable(true);
                        String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                        ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                        adViewHolder.addAdsUrl(imageView);

                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AppUtils.getInstance().clickOnAdBanner(mContext);
                            }
                        });
                    }
                } else {
                    adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
                }
                break;
        }
    }

    private void DBColumnBindHolder(DBColumnViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);
        if (TextUtils.isEmpty(catHome.count)) catHome.count = "0";
        setTitle(viewHolder, catHome, position);
        viewHolder.setData(catHome.categoryInfo, true, true);
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        try {
            Object object = mList.get(position);
            if (object instanceof AdInfo) {
                return VIEW_TYPE_AD;
            } else if (object instanceof CatHome) {
                CatHome catHome = (CatHome) object;
                if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_CAT_HOME))
                    return VIEW_TYPE_CAT_HOME;
                else if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_NEWS))
                    return VIEW_TYPE_NEWS;
                else if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_CITY_SELECTION))
                    return VIEW_TYPE_CITY;
                else if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_VIDEO_GALLERY_V3))
                    return VIEW_TYPE_VIDEO;
                else if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_PHOTO_GALLERY))
                    return VIEW_TYPE_PHOTO_GALLARY;
                else if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_STORY_LIST_V2))
                    return VIEW_TYPE_STORY_V2;
                else if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_RAJYA))
                    return VIEW_TYPE_RAJYA_V2;
                else if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_RASHIFAL_V2))
                    return VIEW_TYPE_RASHIFAL_V2;
                else if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_DB_COLUMN))
                    return VIEW_TYPE_DB_COLOUMN;
            }
        } catch (Exception ignored) {
        }

        return 0;
    }


    /******** Bind Holders Methods ********/

    private void RashifalBindHolder(DivyaRashifalViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);
        setTitle(viewHolder, catHome, position);
        viewHolder.setData(catHome);
    }

    private void videoBindHolder(GujaratiVideoViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);
        if (TextUtils.isEmpty(catHome.count)) catHome.count = "0";
        setTitle(viewHolder, catHome, position);
        viewHolder.setData(catHome.categoryInfo, Integer.parseInt(catHome.count), true, true, catHome.parentId, catHome.superParentId, categoryInfo.displayName);
    }

    private void photoGallaryBindHolder(PhotoGallaryParentViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);
        if (TextUtils.isEmpty(catHome.count)) catHome.count = "0";
        setTitle(viewHolder, catHome, position);
        viewHolder.setData(catHome.categoryInfo, Integer.parseInt(catHome.count), true, true, catHome.parentId, catHome.superParentId, catHome.horizontalList);
    }

    private void storyV2BindHolder(StoryViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);
        if (TextUtils.isEmpty(catHome.count)) catHome.count = "0";
        setTitle(viewHolder, catHome, position);
        viewHolder.setData(catHome.categoryInfo, catHome.parentId, catHome.parentId, true, true, Integer.parseInt(catHome.count), catHome.horizontalList);
    }

    private void NewsBindHolder(NewsParentViewHolder viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);
        viewHolder.setCategoryInfo(catHome.categoryInfo);
        setTitle(viewHolder, catHome, position);
        viewHolder.setData(catHome, true, true);
        viewHolder.rewindMinHeight(catHome.count, catHome.categoryInfo);
    }

    private void SearchPreferredCity(SearchPreferredCityViewHolder holder, int position, int action) {
        CatHome catHome = (CatHome) mList.get(position);
        holder.setData(catHome.superParentId, "", "", catHome.categoryInfo, (TextUtils.isEmpty(catHome.count) ? 0 : Integer.parseInt(catHome.count)));
        setTitle(holder, catHome, position);
    }

    private void RajyaV2(RajyaHomeV2ViewHolder holder, int position) {
        CatHome catHome = (CatHome) mList.get(position);
        holder.setData(catHome.categoryInfo, (TextUtils.isEmpty(catHome.count) ? 0 : Integer.parseInt(catHome.count)));
        setTitle(holder, catHome, position);
        holder.rewindMinHeight(catHome.count);
    }

    private void CatHomeParentBindHolder(CatHomeParentViewHolderNew viewHolder, int position) {
        CatHome catHome = (CatHome) mList.get(position);
        viewHolder.setDataForCatHome(catHome, position);
    }

    private void setTitle(RecyclerView.ViewHolder viewHolder, final CatHome catHome, int position) {
        try {
            String title = catHome.name, color;
            if (TextUtils.isEmpty(catHome.color))
                color = "";
            else
                color = catHome.color;

            if (position == 0) {
                color = "";
                title = "";
            }
            final String mSuperParentId = catHome.superParentId;
            View view = null;
            switch (viewHolder.getItemViewType()) {
                case VIEW_TYPE_CAT_HOME:
                    view = ((CatHomeParentViewHolderNew) viewHolder).itemView;
                    break;
                case VIEW_TYPE_NEWS:
                    view = ((NewsParentViewHolder) viewHolder).itemView;
                    break;
                case VIEW_TYPE_CITY:
                    view = ((SearchPreferredCityViewHolder) viewHolder).itemView;
                    break;
                case VIEW_TYPE_PHOTO_GALLARY:
                    view = ((PhotoGallaryParentViewHolder) viewHolder).itemView;
                    break;
                case VIEW_TYPE_STORY_V2:
                    view = ((StoryViewHolder) viewHolder).itemView;
                    break;
                case VIEW_TYPE_VIDEO:
                    view = ((GujaratiVideoViewHolder) viewHolder).itemView;
                    break;
                case VIEW_TYPE_RAJYA_V2:
                    view = ((RajyaHomeV2ViewHolder) viewHolder).itemView;
                    break;
                case VIEW_TYPE_RASHIFAL_V2:
                    view = ((DivyaRashifalViewHolder) viewHolder).itemView;
                    break;
                case VIEW_TYPE_DB_COLOUMN:
                    view = ((DBColumnViewHolder) viewHolder).itemView;
                    break;
            }

            LinearLayout parentHeaderLL = view.findViewById(R.id.header_title_LL);
            TextView headerTitleTV = view.findViewById(R.id.headerTitleTV);
            TextView headerBoxTV = view.findViewById(R.id.headerBoxTV);
            TextView headerLineTV = view.findViewById(R.id.headerLineTV);
            Button btnHeaderLoadMore = view.findViewById(R.id.btnHeaderLoadMore);
            ImageView diagonalImageView = view.findViewById(R.id.diagonal_iv);
            final TextView moreDropdownTv = view.findViewById(R.id.more_dropdown_tv);
            ImageView ivDropdown = view.findViewById(R.id.ivDropdown);
            final View vDropDown = view.findViewById(R.id.vDropdown);

            if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_RAJYA)) {
                vDropDown.setVisibility(View.VISIBLE);
                vDropDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Context wrapper = new ContextThemeWrapper(mContext, R.style.popupMenuStyle);
                        PopupMenu popup = new PopupMenu(wrapper, vDropDown);
                        CityInfo cInfo;
                        cityInfoVector = JsonParser.getInstance().analyzePreferredCity(mContext, CommonConstants.CHANNEL_ID);

                        popup.getMenu().add(-1, -1, 0, catHome.categoryInfo.menuName);
                        for (int i = 1; i <= cityInfoVector.size(); i++) {
                            cInfo = cityInfoVector.get(i - 1);
                            popup.getMenu().add(Integer.parseInt(cInfo.cityId), Integer.parseInt(cInfo.id), i - 1, cInfo.cityHindiName);
                        }

                        popup.setOnMenuItemClickListener(item -> {
                            String id = String.valueOf(item.getGroupId());
                            if (item.getGroupId() == -1) {
                                if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                                    TabController.getInstance().setTabClicked(true);
                                    if (mSuperParentId.equalsIgnoreCase("0")) {
                                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(catHome.parentId);
                                    } else
                                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mSuperParentId);
                                }
                            } else {
                                Intent intent = new Intent(mContext, PreferredCityActivity.class);
                                intent.putExtra(PreferredCityActivity.CALL_PREFERRED_CITY_HOME, false);
                                intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, catHome.gaArticle);
                                intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, catHome.gaScreen);
                                intent.putExtra(Constants.KeyPair.KEY_COLOR, catHome.color);
                                intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, catHome.gaEventLabel);
                                intent.putExtra(Constants.KeyPair.KEY_CITY_INFO, getCityInfo(id));
                                intent.putExtra(Constants.KeyPair.KEY_TITLE, catHome.name);
                                intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, catHome.categoryInfo);
                                mContext.startActivity(intent);
                            }

                            return true;
                        });
                        popup.show();
                    }
                });
            } else {
                vDropDown.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(color)) {
                int intColor = Color.parseColor(color);

                headerBoxTV.setBackgroundColor(intColor);
                headerLineTV.setBackgroundColor(intColor);
                btnHeaderLoadMore.setTextColor(intColor);
                Drawable drawable = mContext.getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_24dp);
                drawable.clearColorFilter();
                drawable.setColorFilter(new PorterDuffColorFilter(intColor, PorterDuff.Mode.SRC_IN));
                ivDropdown.setImageDrawable(drawable);

                parentHeaderLL.setBackgroundColor(intColor);
                diagonalImageView.setColorFilter(intColor);
                moreDropdownTv.setTextColor(intColor);
            }

            if (!TextUtils.isEmpty(title)) {
                parentHeaderLL.setVisibility(View.VISIBLE);
                headerTitleTV.setText(title);
            } else {
                parentHeaderLL.setVisibility(View.GONE);
            }

            parentHeaderLL.setOnClickListener(v -> {
                if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                    TabController.getInstance().setTabClicked(true);
                    if (mSuperParentId.equalsIgnoreCase("0")) {
                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(catHome.parentId);
                    } else
                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mSuperParentId);
                }
            });
        } catch (Exception e) {

        }

    }

    public CityInfo getCityInfo(String cityID) {
        for (CityInfo cityinfo : cityInfoVector) {
            if (cityinfo.cityId.equalsIgnoreCase(cityID)) {
                return cityinfo;
            }
        }
        return null;
    }


    public void onSwipeRefresh(SwipeRefreshLayout swipeRefreshLayout) {
// clearing the offline data from device for the selected cat home.

        DataParser.getInstance().removeAllDataExceptCatHome();
        notifyDataSetChanged();
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);

    }
}
