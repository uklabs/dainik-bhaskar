package com.db.divya_new.divyashree;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.data.CatHome2;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.views.viewholder.LoadingViewHolder;

import java.util.ArrayList;

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {

    private static final int VIEW_TYPE_CAT_HOME = 1;
    private static final int VIEW_TYPE_AD = 2;
    private static final int VIEW_TYPE_BLANK = 3;
    private static final int VIEW_TYPE_LOAD_MORE = 4;

    private ArrayList<Object> mList;
    private Activity mContext;
    private boolean isTopAdAdded;
    private boolean isBottomAdAdded;

    public HomeAdapter(Activity context) {
        mList = new ArrayList<>();
        this.mContext = context;
    }

    public void clearAll() {
        mList.clear();
        isTopAdAdded = false;
        isBottomAdAdded = false;
    }

    public void addAll(ArrayList<CatHome2> object) {
        int preIndex = mList.size();
        if (preIndex == 0) {
            addTopAd();
        }
        mList.addAll(object);

        mContext.runOnUiThread(() -> {
            if (preIndex == 0) {
                notifyDataSetChanged();
            } else {
                notifyItemRangeInserted(preIndex, object.size());
            }
        });
    }

    public void addAllAndClear(ArrayList<CatHome2> object) {
        clearAll();
        mList.addAll(object);
        addAds();

        mContext.runOnUiThread(() -> notifyDataSetChanged());
    }

    public void addNullItem() {
        mList.add(null);

        mContext.runOnUiThread(() -> notifyItemInserted(mList.size() - 1));
    }

    public void addBlankItem() {
        CatHome2 ch = new CatHome2();
        ch.id = "BLANK";
        mList.add(ch);

        mContext.runOnUiThread(() -> notifyItemInserted(mList.size() - 1));
    }

    public void removeNullItem() {
        if (mList.size() > 0 && mList.get(mList.size() - 1) == null) {
            int pos = mList.size() - 1;
            mList.remove(pos);

            mContext.runOnUiThread(() -> notifyItemRemoved(pos));
        }
    }

    private void addAds() {
        addTopAd();
        addBottomAd();
    }

    private boolean addTopAd() {
        isBannerLoaded = false;

        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false) && !isTopAdAdded) {
            mList.add(0, AdController.showATFForHomeListing(mContext, this));
            return true;
        }
        return false;
    }

    private void addBottomAd() {
        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.BTF_ROS_LIST_TOGGLE, false) && !isBottomAdAdded) {
            mList.add(mList.size(), AdController.showBTFForHomeListing(mContext));
        }
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;

        mContext.runOnUiThread(() -> notifyItemChanged(0));
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_CAT_HOME:
                return new HomeCategoryViewHolder(mContext, inflater.inflate(R.layout.listitem_home_viewholder, parent, false));
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));
            case VIEW_TYPE_BLANK:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view_10, parent, false));
            case VIEW_TYPE_LOAD_MORE:
                return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_CAT_HOME:
                homeCategoryViewHolder((HomeCategoryViewHolder) holder, position);
                break;

            case VIEW_TYPE_AD:
                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
                if (((AdInfo) mList.get(position)).catType == 1) {
                    if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                        adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);

                    } else {
                        ImageView imageView = new ImageView(mContext);
                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setAdjustViewBounds(true);
                        imageView.setClickable(true);
                        String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                        ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                        adViewHolder.addAdsUrl(imageView);

                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AppUtils.getInstance().clickOnAdBanner(mContext);
                            }
                        });
                    }
                } else {
                    adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
                }
                break;

            case VIEW_TYPE_LOAD_MORE:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
                loadingViewHolder.progressBar.setIndeterminate(true);
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        try {
            Object object = mList.get(position);
            if (object == null) {
                return VIEW_TYPE_LOAD_MORE;
            } else if (object instanceof AdInfo) {
                return VIEW_TYPE_AD;
            } else if (object instanceof CatHome2) {
                if (((CatHome2) object).id.equalsIgnoreCase("BLANK"))
                    return VIEW_TYPE_BLANK;
                return VIEW_TYPE_CAT_HOME;
            }
        } catch (Exception ignored) {
        }

        return 0;
    }

    private void homeCategoryViewHolder(HomeCategoryViewHolder viewHolder, int position) {
        CatHome2 catHome = (CatHome2) mList.get(position);
        viewHolder.setDataForCatHome(catHome);
    }

    public CatHome2 getItemAtPosition(int position) {
        if (position >= 0 && position < mList.size()) {
            Object o = mList.get(position);
            if (o instanceof CatHome2)
                return ((CatHome2) o);
        }
        return null;
    }
}
