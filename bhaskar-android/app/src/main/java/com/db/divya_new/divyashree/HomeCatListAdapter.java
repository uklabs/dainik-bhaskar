package com.db.divya_new.divyashree;

import android.app.Activity;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.database.DatabaseClient;
import com.db.divya_new.common.NewsParentViewHolderV2;
import com.db.divya_new.common.TabController;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.common.viewholder.DBColumnViewHolder2;
import com.db.divya_new.common.viewholder.RajyaHomeV2ViewHolderV2;
import com.db.divya_new.data.CatHome2;
import com.db.divya_new.divyashree.viewholder.StoryViewHolder2;
import com.db.divya_new.gujarati.GujaratiVideoViewHolder2;
import com.db.divya_new.jyotishVastu.DivyaRashifalViewHolderV2;
import com.db.divya_new.photoGallary.PhotoGallaryParentViewHolder2;
import com.bhaskar.util.Action;
import com.bhaskar.util.CssConstants;

import java.util.ArrayList;
import java.util.List;

public class HomeCatListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final static int VIEW_TYPE_NEWS = 1;
    private final static int VIEW_TYPE_STORY_LIST = 2;
    private final static int VIEW_TYPE_STORY_LIST_V2 = 3;
    private final static int VIEW_TYPE_VIDEO = 4;
    private final static int VIEW_TYPE_PHOTO_GALLARY = 5;
    private final static int VIEW_TYPE_RASHIFAL_V2 = 6;
    private final static int VIEW_TYPE_DB_COLOUMN = 7;
    private final static int VIEW_TYPE_RAJYA_V2 = 8;

    private ArrayList<CatHome2> mList;
    private Activity mContext;

    public HomeCatListAdapter(Activity context) {
        mList = new ArrayList<>();
        this.mContext = context;
    }

    public void add(CatHome2 object) {
        mList.add(object);
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<CatHome2> object) {
        mList.addAll(object);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_STORY_LIST:
                return new StoryViewHolder2(inflater.inflate(R.layout.listitem_recylerview_title_story_v2, parent, false), mContext, false, false);
            case VIEW_TYPE_STORY_LIST_V2:
                return new StoryViewHolder2(inflater.inflate(R.layout.listitem_recylerview_title_story_v2, parent, false), mContext, false, true);
            case VIEW_TYPE_NEWS:
                return new NewsParentViewHolderV2(mContext, inflater.inflate(R.layout.listitem_recylerview_title_news_v2, parent, false));
            case VIEW_TYPE_RASHIFAL_V2:
                return new DivyaRashifalViewHolderV2(mContext, inflater.inflate(R.layout.listitem_recylerview_title_rashifal_v2, parent, false));
            case VIEW_TYPE_VIDEO:
                return new GujaratiVideoViewHolder2(inflater.inflate(R.layout.listitem_recylerview_title_v2, parent, false), mContext);
            case VIEW_TYPE_PHOTO_GALLARY:
                return new PhotoGallaryParentViewHolder2(inflater.inflate(R.layout.listitem_recylerview_title_v2, parent, false));
            case VIEW_TYPE_DB_COLOUMN:
                return new DBColumnViewHolder2(inflater.inflate(R.layout.listitem_recylerview_title_story_v2, parent, false), mContext);
            case VIEW_TYPE_RAJYA_V2:
                return new RajyaHomeV2ViewHolderV2(inflater.inflate(R.layout.layout_rajya_v2_2, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_STORY_LIST:
                StoryBindHolder((StoryViewHolder2) holder, position);
                break;
            case VIEW_TYPE_STORY_LIST_V2:
                StoryBindHolder((StoryViewHolder2) holder, position);
                break;
            case VIEW_TYPE_NEWS:
                NewsBindHolder((NewsParentViewHolderV2) holder, position);
                break;
            case VIEW_TYPE_RASHIFAL_V2:
                RashifalBindHolder((DivyaRashifalViewHolderV2) holder, position);
                break;
            case VIEW_TYPE_VIDEO:
                videoBindHolder((GujaratiVideoViewHolder2) holder, position);
                break;
            case VIEW_TYPE_PHOTO_GALLARY:
                photoGallaryBindHolder((PhotoGallaryParentViewHolder2) holder, position);
                break;
            case VIEW_TYPE_RAJYA_V2:
                rajyaBindHolder((RajyaHomeV2ViewHolderV2) holder, position);
                break;
            case VIEW_TYPE_DB_COLOUMN:
                dbColumnBindHolder((DBColumnViewHolder2) holder, position);
                break;
        }
    }

    private void photoGallaryBindHolder(PhotoGallaryParentViewHolder2 viewHolder, int position) {
        CatHome2 catHome = mList.get(position);
        viewHolder.setData(catHome);
        setSubTitle(viewHolder, catHome, position > 0);
    }

    private void StoryBindHolder(final StoryViewHolder2 viewHolder, int position) {
        CatHome2 catHome = mList.get(position);
        viewHolder.setData(catHome);
        setSubTitle(viewHolder, catHome, position > 0);
    }

    private void RashifalBindHolder(DivyaRashifalViewHolderV2 viewHolder, int position) {
        CatHome2 catHome = mList.get(position);
        viewHolder.setData(catHome);
        setSubTitle(viewHolder, catHome, position > 0);
    }

    private void NewsBindHolder(NewsParentViewHolderV2 viewHolder, int position) {
        CatHome2 catHome = mList.get(position);
        viewHolder.setData(catHome);
        setSubTitle(viewHolder, catHome, position > 0);
    }

    private void videoBindHolder(GujaratiVideoViewHolder2 viewHolder, int position) {
        CatHome2 catHome = mList.get(position);
        viewHolder.setData(catHome);
        setSubTitle(viewHolder, catHome, position > 0);
    }

    private void dbColumnBindHolder(DBColumnViewHolder2 viewHolder, int position) {
        CatHome2 catHome = mList.get(position);
        viewHolder.setData(catHome);
        setSubTitle(viewHolder, catHome, position > 0);
    }

    private void rajyaBindHolder(RajyaHomeV2ViewHolderV2 viewHolder, int position) {
        CatHome2 catHome = mList.get(position);
        viewHolder.setData(catHome);
        setSubTitle(viewHolder, catHome, position > 0);
    }

    private void setSubTitle(final RecyclerView.ViewHolder viewHolder, CatHome2 catHome, boolean showTitle) {
        String title = catHome.name;
        String color = catHome.color;
        if(TextUtils.isEmpty(color))
            color = CssConstants.themeColor;

        View view = null;
        switch (viewHolder.getItemViewType()) {
            case VIEW_TYPE_VIDEO:
                view = ((GujaratiVideoViewHolder2) viewHolder).itemView;
                break;
            case VIEW_TYPE_PHOTO_GALLARY:
                view = ((PhotoGallaryParentViewHolder2) viewHolder).itemView;
                break;
            case VIEW_TYPE_STORY_LIST:
                view = ((StoryViewHolder2) viewHolder).itemView;
                break;
            case VIEW_TYPE_STORY_LIST_V2:
                view = ((StoryViewHolder2) viewHolder).itemView;
                break;
            case VIEW_TYPE_NEWS:
                view = ((NewsParentViewHolderV2) viewHolder).itemView;
                break;
            case VIEW_TYPE_RASHIFAL_V2:
                view = ((DivyaRashifalViewHolderV2) viewHolder).itemView;
                break;
            case VIEW_TYPE_RAJYA_V2:
                view = ((RajyaHomeV2ViewHolderV2) viewHolder).itemView;
                break;
            case VIEW_TYPE_DB_COLOUMN:
                view = ((DBColumnViewHolder2) viewHolder).itemView;
                break;
        }
        if (view == null)
            return;

        LinearLayout subParentHeaderLL = view.findViewById(R.id.sub_header_title_LL);
        TextView subHeaderTitleTV = view.findViewById(R.id.subHeaderTitleTV);

        if (showTitle) {
            subParentHeaderLL.setVisibility(View.VISIBLE);
            subHeaderTitleTV.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(color)) {
                int intColor = Color.parseColor(color);
                subHeaderTitleTV.setTextColor(intColor);
            }
            if (!TextUtils.isEmpty(title)) {
                subParentHeaderLL.setVisibility(View.VISIBLE);
                subHeaderTitleTV.setText(title);
            } else {
                subParentHeaderLL.setVisibility(View.GONE);
            }
            subParentHeaderLL.setOnClickListener(v -> {
                if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
//                    TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(catHome.id);
                    CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(catHome.id);
                    if(categoryInfo != null) {
                        TabController.getInstance().setTabClicked(true);
                        if (categoryInfo.parentId.equalsIgnoreCase("0")) {
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(categoryInfo.id);
                        } else {
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(categoryInfo.parentId, categoryInfo.id, null);
                        }
                    }
                }
            });
        } else {
            subParentHeaderLL.setVisibility(View.GONE);
            subHeaderTitleTV.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        CatHome2 catHome = mList.get(position);
        return getViewHolderType(catHome.action);
    }

    private int getViewHolderType(String action) {
        switch (action) {
            case Action.CategoryAction.CAT_ACTION_STORY_LIST:
                return VIEW_TYPE_STORY_LIST;
            case Action.CategoryAction.CAT_ACTION_STORY_LIST_V2:
                return VIEW_TYPE_STORY_LIST_V2;
            case Action.CategoryAction.CAT_ACTION_NEWS:
                return VIEW_TYPE_NEWS;
            case Action.CategoryAction.CAT_ACTION_RASHIFAL_V2:
                return VIEW_TYPE_RASHIFAL_V2;
            case Action.CategoryAction.CAT_ACTION_VIDEO_GALLERY_V3:
                return VIEW_TYPE_VIDEO;
            case Action.CategoryAction.CAT_ACTION_PHOTO_GALLERY:
                return VIEW_TYPE_PHOTO_GALLARY;
            case Action.CategoryAction.CAT_ACTION_RAJYA:
                return VIEW_TYPE_RAJYA_V2;
            case Action.CategoryAction.CAT_ACTION_DB_COLUMN:
                return VIEW_TYPE_DB_COLOUMN;
        }
        return 0;
    }
}
