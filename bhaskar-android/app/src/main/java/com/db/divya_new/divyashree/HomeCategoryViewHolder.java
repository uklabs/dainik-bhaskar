package com.db.divya_new.divyashree;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.bhaskar.util.CommonConstants;
import com.db.data.models.CategoryInfo;
import com.db.data.models.CityInfo;
import com.db.database.DatabaseClient;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHome2;
import com.db.preferredcity.PreferredCityActivity;
import com.bhaskar.util.Action;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.bhaskar.util.CssConstants;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;

import java.util.List;

public class HomeCategoryViewHolder extends RecyclerView.ViewHolder {

    public RecyclerView recyclerView;
    public HomeCatListAdapter mAdapter;
    private Activity mContext;

    private TextView moreDropdownTv;
    private ImageView ivDropdown;
    private View vDropDown;
    private TextView headerLoadMoreTv;
    private List<CityInfo> cityInfoList;

    HomeCategoryViewHolder(Activity mContext, View itemView) {
        super(itemView);
        this.mContext = mContext;
        cityInfoList = JsonParser.getInstance().analyzePreferredCity(mContext, CommonConstants.CHANNEL_ID);

        recyclerView = itemView.findViewById(R.id.recycler_view);
        mAdapter = new HomeCatListAdapter(mContext);

        headerLoadMoreTv = itemView.findViewById(R.id.btnHeaderLoadMore);

        moreDropdownTv = itemView.findViewById(R.id.more_dropdown_tv);
        ivDropdown = itemView.findViewById(R.id.ivDropdown);
        vDropDown = itemView.findViewById(R.id.vDropdown);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);
    }

    public void setDataForCatHome(final CatHome2 catHome) {
        if (catHome != null) {
            setTitle(catHome);

            boolean isBlockCountry = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);

            List<CategoryInfo> subCategoryList = DatabaseClient.getInstance(mContext).getMoreCategoryList(isBlockCountry, catHome.moreLists.toArray(new String[]{}));
            if (subCategoryList != null && subCategoryList.size() > 0) {
                showMore(subCategoryList);
                headerLoadMoreTv.setVisibility(View.GONE);
            } else if (catHome.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_RAJYA)) {
                showMoreRajya(catHome, cityInfoList);
                headerLoadMoreTv.setVisibility(View.GONE);
            } else {
                vDropDown.setVisibility(View.GONE);
                headerLoadMoreTv.setVisibility(View.VISIBLE);
            }
        }

        mAdapter.clear();
        mAdapter.add(catHome);
        if (catHome.subDatalist.size() > 0)
            mAdapter.addAll(catHome.subDatalist);
    }

    private void showMore(List<CategoryInfo> cityInfoVector) {
        vDropDown.setVisibility(View.VISIBLE);
        vDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context wrapper = new ContextThemeWrapper(mContext, R.style.popupMenuStyle);
                PopupMenu popup = new PopupMenu(wrapper, vDropDown);

//                popup.getMenu().add(-1, -1, 0, catHome.categoryInfo.menuName);
                for (int i = 1; i <= cityInfoVector.size(); i++) {
                    CategoryInfo cInfo = cityInfoVector.get(i - 1);
                    if (cInfo != null)
                        popup.getMenu().add(Integer.parseInt(cInfo.parentId), Integer.parseInt(cInfo.id), i - 1, cInfo.menuName);
                }

                popup.setOnMenuItemClickListener(item -> {
                    String parentId = String.valueOf(item.getGroupId());
                    String id = String.valueOf(item.getItemId());
                    TabController.getInstance().setTabClicked(true);
                    TabController.MenuTabController menuTabControllerInstance = TabController.getInstance().getMenuTabControllerInstance();
                    if (menuTabControllerInstance != null) {
                        if (parentId.equalsIgnoreCase("0")) {
                            menuTabControllerInstance.onMenuTabClick(id);
                        } else {
                            menuTabControllerInstance.onMenuTabClick(parentId, id, null);
                        }
                    }
                    return true;
                });
                popup.show();
            }
        });
    }

    private void showMoreRajya(CatHome2 catHome, List<CityInfo> cityInfoVector) {
        vDropDown.setVisibility(View.VISIBLE);
        vDropDown.setOnClickListener(view -> {
            Context wrapper = new ContextThemeWrapper(mContext, R.style.popupMenuStyle);
            PopupMenu popup = new PopupMenu(wrapper, vDropDown);

            popup.getMenu().add(-1, -1, 0, catHome.name);
            for (int i = 1; i <= cityInfoVector.size(); i++) {
                CityInfo cInfo = cityInfoVector.get(i - 1);
                if (cInfo != null)
                    popup.getMenu().add(Integer.parseInt(cInfo.cityId), Integer.parseInt(cInfo.id), i - 1, cInfo.cityHindiName);
            }

            popup.setOnMenuItemClickListener(item -> {
                if (item.getGroupId() == -1) {
                    TabController.getInstance().setTabClicked(true);
                    TabController.MenuTabController menuTabControllerInstance = TabController.getInstance().getMenuTabControllerInstance();
                    if (menuTabControllerInstance != null) {
                        menuTabControllerInstance.onMenuTabClick(catHome.id);
                    }
                } else {
                    String id = String.valueOf(item.getGroupId());
                    CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(catHome.id);
                    Intent intent = new Intent(mContext, PreferredCityActivity.class);
                    intent.putExtra(PreferredCityActivity.CALL_PREFERRED_CITY_HOME, false);
                    intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
                    intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                    intent.putExtra(Constants.KeyPair.KEY_COLOR, catHome.color);
                    intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, categoryInfo.gaEventLabel);
                    intent.putExtra(Constants.KeyPair.KEY_CITY_INFO, getCityInfo(id));
                    intent.putExtra(Constants.KeyPair.KEY_TITLE, catHome.name);
                    intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
                    mContext.startActivity(intent);
                }
                return true;
            });
            popup.show();
        });
    }

    public CityInfo getCityInfo(String cityID) {
        for (CityInfo cityinfo : cityInfoList) {
            if (cityinfo.cityId.equalsIgnoreCase(cityID)) {
                return cityinfo;
            }
        }
        return null;
    }


    private void setTitle(CatHome2 catHome) {
        String title = catHome.name, color = catHome.color;
        if(TextUtils.isEmpty(color))
            color = CssConstants.themeColor;


        LinearLayout parentHeaderLL = itemView.findViewById(R.id.header_title_LL);

        if (!TextUtils.isEmpty(title)) {
            parentHeaderLL.setVisibility(View.VISIBLE);

            TextView headerTitleTV = itemView.findViewById(R.id.headerTitleTV);
            headerTitleTV.setText(title);

            TextView headerBoxTV = itemView.findViewById(R.id.headerBoxTV);
            ImageView diagonalImageView = itemView.findViewById(R.id.diagonal_iv);

            if (!TextUtils.isEmpty(color)) {
                int intColor = Color.parseColor(color);

                parentHeaderLL.setBackgroundColor(intColor);
                headerBoxTV.setBackgroundColor(intColor);
                diagonalImageView.setColorFilter(intColor);
                moreDropdownTv.setTextColor(intColor);
                headerLoadMoreTv.setTextColor(intColor);

                Drawable drawable = mContext.getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_24dp);
                drawable.clearColorFilter();
                drawable.setColorFilter(new PorterDuffColorFilter(intColor, PorterDuff.Mode.SRC_IN));
                ivDropdown.setImageDrawable(drawable);
            }
        } else {
            parentHeaderLL.setVisibility(View.GONE);
        }

        parentHeaderLL.setOnClickListener(v -> {
            if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
//                TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(catHome.id);
                CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(catHome.id);
                if (categoryInfo != null) {
                    TabController.getInstance().setTabClicked(true);
                    TabController.MenuTabController menuTabControllerInstance = TabController.getInstance().getMenuTabControllerInstance();
                    if (menuTabControllerInstance != null) {
                        if (categoryInfo.parentId.equalsIgnoreCase("0")) {
                            menuTabControllerInstance.onMenuTabClick(categoryInfo.id);
                        } else {
                            menuTabControllerInstance.onMenuTabClick(categoryInfo.parentId, categoryInfo.id, null);
                        }
                    }
                }
            }
        });
    }
}
