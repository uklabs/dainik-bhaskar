package com.db.divya_new.divyashree;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.db.InitApplication;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.source.server.BackgroundRequest;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.main.BaseAppCompatActivity;
import com.db.news.WapV2Activity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.bhaskar.util.CssConstants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.ShareUtil;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.views.AutoScrollViewPager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NewsFlickerActivity extends BaseAppCompatActivity implements View.OnClickListener {
    private static final String TAG = AppCompatActivity.class.getSimpleName();
    private CategoryInfo categoryInfo;
    private TextView tvTitle;
    private AutoScrollViewPager viewPager;
    private View vHeaderView;
    private List<NewsListInfo> mList;
    private ImageView ivPrev;
    private ImageView ivNext;
    private SectionPagerAdapter sectionPagerAdapter;
    private final int PERIOD_MS = 5000; // time in milliseconds between successive task executions.
    private ImageView ivBack;
    private static String title;
    private String subjectColor;
    private String themeColor;
    private int selectedPosition;

    public static Intent getIntent(Context context, ArrayList<NewsListInfo> mList, CategoryInfo categoryInfo, String title, boolean isPhotoFlicker, int selectedPosition) {
        Intent intent = new Intent(context, NewsFlickerActivity.class);
        intent.putExtra("data", mList);
        intent.putExtra("categoryinfo", categoryInfo);
        intent.putExtra("title", title);
        intent.putExtra("selectedPosition", selectedPosition);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_flicker);
        handleExtras(getIntent());

        AppUtils.getInstance().setStatusBarColor(this, themeColor);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvTitle = findViewById(R.id.tv_title);
        ivBack = findViewById(R.id.iv_back);
        Drawable backDrawable = getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp);
//        backDrawable.setColorFilter(AppUtils.getThemeColor(this, "#ffffff"), PorterDuff.Mode.MULTIPLY);
        ivBack.setImageDrawable(backDrawable);

        tvTitle.setTextColor(AppUtils.getThemeColor(this, "#ffffff"));
        ivPrev = findViewById(R.id.ivPrev);
        ivNext = findViewById(R.id.ivNext);
        viewPager = findViewById(R.id.viewPager);
        viewPager.startAutoScroll(PERIOD_MS);
        viewPager.setInterval(PERIOD_MS);
        viewPager.setCycle(true);
        viewPager.setStopScrollWhenTouch(true);
        ivPrev.setVisibility(View.GONE);
        vHeaderView = findViewById(R.id.v_header_view);
        setToolbarTitle(title);
        vHeaderView.setBackgroundColor(AppUtils.getThemeColor(this, themeColor));
        ivBack.setOnClickListener(this);
        sectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager(), mList);
        viewPager.setAdapter(sectionPagerAdapter);
        ivNext.setOnClickListener(v -> {
            int currentIndex = viewPager.getCurrentItem();
            if (currentIndex <= sectionPagerAdapter.getCount()) {
                viewPager.setCurrentItem(currentIndex + 1, true);
            }
        });

        ivPrev.setOnClickListener(v -> {
            int currentIndex = viewPager.getCurrentItem();
            if (currentIndex > 0) {
                viewPager.setCurrentItem(currentIndex - 1, true);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    ivPrev.setVisibility(View.GONE);
                    ivNext.setVisibility(View.VISIBLE);
                } else if (position == sectionPagerAdapter.getCount() - 1) {
                    ivNext.setVisibility(View.GONE);
                } else {
                    ivNext.setVisibility(View.VISIBLE);
                    ivPrev.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Systr.println("");
            }
        });

        viewPager.setCurrentItem(selectedPosition);

        if (AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
            AdController.showArticleATF(this, findViewById(R.id.ads_view_top));
        } else {
            findViewById(R.id.ads_view_top).setVisibility(View.GONE);
        }
        if (AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.AdPref.BTF_ROS_LIST_TOGGLE, false)) {
            AdController.showArticleBTF_50(this, findViewById(R.id.ads_view_bottom));
        } else {
            findViewById(R.id.ads_view_bottom).setVisibility(View.GONE);
        }

        String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GAScreen.FLICKER_SCREEN, source, medium, campaign);
    }

    private void handleExtras(Intent intent) {
        if (intent != null) {
            mList = (List<NewsListInfo>) intent.getSerializableExtra("data");
            categoryInfo = (CategoryInfo) intent.getSerializableExtra("categoryinfo");
            title = intent.getStringExtra("title");
            selectedPosition = intent.getIntExtra("selectedPosition", 0);
        }

        subjectColor = AppPreferences.getInstance(this).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_SUBJECT_COLOR, CssConstants.themeColor);
        themeColor = AppPreferences.getInstance(this).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_COLOR, CssConstants.themeColor);
    }

    public void setToolbarTitle(String title) {
        tvTitle.setText(title);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public static class PlaceholderFragment extends Fragment {
        private NewsListInfo newsListInfo;
        private View topHeaderView;
        private TextView tvTitle;
        private ImageView ivMore;
        private ImageView ivStory;
        private TextView tvCounter;
        private int position;
        private String themeColor;
        private View vHolder;
        private FrameLayout shareFrameLayout;
        private String subjectColor;

        public static Fragment getFragment(NewsListInfo newsListInfo, int position, String themeColor, String subjectColor) {
            Fragment fragment = new PlaceholderFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("data", newsListInfo);
            bundle.putInt("position", position);
            bundle.putString("themeColor", themeColor);
            bundle.putString("subjectColor", subjectColor);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                newsListInfo = (NewsListInfo) getArguments().getSerializable("data");
                position = getArguments().getInt("position");
                themeColor = getArguments().getString("themeColor");
                subjectColor = getArguments().getString("subjectColor");
            }
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_news_flicker_item, container, false);
            topHeaderView = view.findViewById(R.id.vTopView);
            shareFrameLayout = view.findViewById(R.id.share_frame_layout);
            tvTitle = view.findViewById(R.id.tvTitle);
            ivMore = view.findViewById(R.id.ivMore);
            ivStory = view.findViewById(R.id.ivStory);
            tvCounter = view.findViewById(R.id.tvCounter);
            vHolder = view.findViewById(R.id.vHolder);
            tvCounter.setText(String.valueOf(position + 1));
            ivMore.setOnClickListener(v -> {
                //showMoreOptions(v);
                AppUtils.showMoreOptionsV2(getContext(), v, new MoreOptionInterface() {
                    @Override
                    public void onBookMark() {
                        AppUtils.getInstance().bookmarkClick(getContext(), newsListInfo, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID), themeColor, false);
                    }

                    @Override
                    public void onShare() {
                        AppUtils.getInstance().shareClick(getContext(), newsListInfo.title, newsListInfo.webUrl, newsListInfo.gTrackUrl, false);
                    }
                });
            });
            vHolder.setOnClickListener(v -> {
                ((NewsFlickerActivity) Objects.requireNonNull(getActivity())).launchArticlePage(newsListInfo);
            });

            shareFrameLayout.setOnClickListener(view1 -> {
                BackgroundRequest.getStoryShareUrl(getContext(), newsListInfo.image, (flag, url) -> ShareUtil.shareDefault(getContext(), title, "", url, newsListInfo.gTrackUrl, true));
//                    ShareUtil.shareDefault(getContext(), title, "", newsListInfo.image));
            });

            return view;
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            String title = newsListInfo.title;
            String iitl = newsListInfo.iitl_title;
            if (TextUtils.isEmpty(newsListInfo.image)) {
                newsListInfo.image = null;
            }

            float ratio = AppUtils.getInstance().parseImageRatio("500x500"/*newsListInfo.imageSize*/, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
            AppUtils.getInstance().setImageViewSizeWithAspectRatio(ivStory, ratio, 16, getContext());
            ImageUtil.setImage(getContext(), newsListInfo.image, ivStory, R.drawable.water_mark_news_detail);

            tvTitle.setText(AppUtils.getInstance().getHomeTitle(iitl, AppUtils.getInstance().fromHtml(title), subjectColor, "#FFFFFF"));

            topHeaderView.setVisibility(View.VISIBLE);
            shareFrameLayout.setVisibility(View.GONE);
        }
    }

    private void launchArticlePage(NewsListInfo newsListInfo) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(this, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(categoryInfo.gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : categoryInfo.gaScreen) + newsListInfo.webUrl);
            this.startActivity(intent);
        } else {
            categoryInfo.gaArticle = AppFlyerConst.GAScreen.FLICKER_WIDGET_GA_ARTICLE;
            startActivityForResult(DivyaCommonArticleActivity.getIntent(NewsFlickerActivity.this, newsListInfo, categoryInfo),Constants.REQ_CODE_BOTTOM_NAV);
        }
    }


    class SectionPagerAdapter extends FragmentPagerAdapter {

        private List<NewsListInfo> mList;

        SectionPagerAdapter(FragmentManager fm, List<NewsListInfo> mList) {
            super(fm);
            this.mList = mList;
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.getFragment(mList.get(position), position, subjectColor, subjectColor);
        }

        @Override
        public int getCount() {
            return mList == null ? 0 : mList.size();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if(requestCode == Constants.REQ_CODE_BOTTOM_NAV){
            finish();
        }
    }
}
