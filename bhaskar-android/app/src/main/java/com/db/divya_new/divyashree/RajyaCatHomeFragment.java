package com.db.divya_new.divyashree;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.divya_new.data.CatHome;
import com.bhaskar.util.Action;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RajyaCatHomeFragment extends Fragment implements OnMoveTopListener {

    ArrayList<CatHome> catHomeArrayList = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    private CatHomeAdapter adapter;
    private CategoryInfo mCategoryInfo;
    private String mSuperParentId;

    public RajyaCatHomeFragment() {
    }

    public static RajyaCatHomeFragment newInstance(CategoryInfo info, String mParentId) {
        RajyaCatHomeFragment fragment = new RajyaCatHomeFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, info);
        bundle.putString(Constants.KeyPair.KEY_ID, mParentId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mCategoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            mSuperParentId = bundle.getString(Constants.KeyPair.KEY_ID);
        }
    }

    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_common, container, false);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        adapter = new CatHomeAdapter(getActivity(), mSuperParentId, mCategoryInfo);
        catHomeArrayList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        setDataForCatHome();

        //Adding Search widget
//        CatHome catHome = new CatHome();
//        catHome.action = Action.CategoryAction.CAT_ACTION_SEARCH_WIDGET;
//        catHomeArrayList.add(0, catHome); // for search

        adapter.addAll(catHomeArrayList);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            adapter.onSwipeRefresh(swipeRefreshLayout);
        });

        return view;
    }

    public void setDataForCatHome() {
        adapter.setFromParentHome(false);
        if (mCategoryInfo != null && !TextUtils.isEmpty(mCategoryInfo.extraVlues)) {
            try {
                JSONObject jsonObject = new JSONObject(mCategoryInfo.extraVlues);
                JSONArray jsonArray = jsonObject.optJSONArray("cat_home_ids");
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {

                        CatHome cat = new Gson().fromJson(jsonArray.get(i).toString(), CatHome.class);
                        CategoryInfo info = JsonParser.getInstance().getCategoryInfoById(getActivity(), cat.id);
                        if (info != null) {
                            if (info.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_CAT_MENU)) {
                                cat.action = info.subMenu.get(0).action;
                                cat.parentId = info.id;
                                cat.superParentId = info.parentId;
                                cat.categoryInfo = new CategoryInfo(info.subMenu.get(0));
                            } else {
                                cat.action = info.action;
                                cat.parentId = info.id;
                                cat.superParentId = info.parentId;
                                cat.categoryInfo = new CategoryInfo(info);
                            }

                            if (checkActionAvailability(cat.action)) {
                                if (!cat.isGAValueAdded) {
                                    if (!TextUtils.isEmpty(mCategoryInfo.gaScreen))
                                        cat.categoryInfo.gaScreen = (mCategoryInfo.gaScreen + "-" + cat.categoryInfo.gaScreen);
                                    if (!TextUtils.isEmpty(mCategoryInfo.gaArticle))
                                        cat.categoryInfo.gaArticle = (mCategoryInfo.gaScreen + "-" + cat.categoryInfo.gaArticle);
                                    if (!TextUtils.isEmpty(mCategoryInfo.gaEventLabel))
                                        cat.categoryInfo.gaEventLabel = (mCategoryInfo.gaEventLabel + "-" + cat.categoryInfo.gaEventLabel);

                                    cat.isGAValueAdded = true;
                                }

                                cat.headerTitle = info.menuName;
                                catHomeArrayList.add(cat);
                            }
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private boolean checkActionAvailability(String action) {
        switch (action) {
            case Action.CategoryAction.CAT_ACTION_STORY_LIST:
            case Action.CategoryAction.CAT_ACTION_STORY_LIST_V2:
            case Action.CategoryAction.CAT_ACTION_NEWS:
            case Action.CategoryAction.CAT_ACTION_RASHIFAL_V2:
            case Action.CategoryAction.CAT_ACTION_RATNA:
            case Action.CategoryAction.CAT_ACTION_GURUVANI_GURU:
            case Action.CategoryAction.CAT_ACTION_GURUVANI_TOPIC:
            case Action.CategoryAction.CAT_ACTION_SONGS:
            case Action.CategoryAction.CAT_ACTION_PHOTO_GALLERY:
            case Action.CategoryAction.CAT_ACTION_VIDEO_GALLERY_V3:
            case Action.CategoryAction.CAT_ACTION_RAJYA_LIST:
                return Action.ALL_HOME_CATEGORY_ACTIONS.contains(action);
            default:
                return false;
        }
    }

    public void moveToTop() {
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }
}
