package com.db.divya_new.divyashree;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.StoryListInfo;
import com.db.database.DatabaseClient;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class StoryListAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {

    private static final int TYPE_LIST = 2;
    private final static int VIEW_TYPE_AD = 25;
    private static final int VIEW_TYPE_BANNER = 10;

    private ArrayList<Object> mList;
    private Activity mContext;
    private String catId;
    private boolean isHorizontal;
    private boolean isNightModeApplicable;
    private boolean isStoryV2;

    public StoryListAdapter2(Activity context, boolean nightModeApplicable, boolean isStoryV2) {
        this.isNightModeApplicable = nightModeApplicable;
        this.isStoryV2 = isStoryV2;
        mList = new ArrayList<>();
        this.mContext = context;
    }

    public void setData(String catId, boolean isHorizontal) {
        this.catId = catId;
        this.isHorizontal = isHorizontal;
    }

    public void addAndClearAll(List<Object> storyInfoList) {
        isBannerLoaded = false;
        mList.clear();
        mList.addAll(storyInfoList);
        notifyDataSetChanged();
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;
        notifyItemChanged(0);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner, parent, false));

            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));

            case TYPE_LIST:
                if (isStoryV2) {
                    return new ArticleViewHolderV2(inflater.inflate(R.layout.listitem_rect_image_title_subtitle, parent, false));
                } else {
                    return new ArticleViewHolder(inflater.inflate(R.layout.listitem_circular_image_title_subtitle, parent, false));
                }

            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleViewHolder) {
            articleBindHolder((ArticleViewHolder) holder, position);

        } else if (holder instanceof ArticleViewHolderV2) {
            articleBindHolderV2((ArticleViewHolderV2) holder, position);

        } else if (holder instanceof BannerAdViewHolder) {
            BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
            if (((AdInfo) mList.get(position)).catType == 1) {
                if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                    adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);

                } else {
                    ImageView imageView = new ImageView(mContext);
                    imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    imageView.setAdjustViewBounds(true);
                    imageView.setClickable(true);
                    String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                    ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                    adViewHolder.addAdsUrl(imageView);

                    imageView.setOnClickListener(view -> AppUtils.getInstance().clickOnAdBanner(mContext));
                }
            } else {
                adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
            }

        } else if (holder instanceof BannerViewHolder) {
            BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
            bannerViewHolder.callwebViewOrSetWidgetData((BannerInfo) mList.get(holder.getAdapterPosition()));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mList.get(position);
        if (object instanceof BannerInfo) {
            return VIEW_TYPE_BANNER;
        } else if (object instanceof AdInfo) {
            return VIEW_TYPE_AD;
        } else if (object instanceof StoryListInfo) {
            return TYPE_LIST;
        }

        return 0;
    }

    /******** Bind Holders Methods ********/

    private void articleBindHolder(ArticleViewHolder viewHolder, int position) {
        final StoryListInfo storyListInfo = (StoryListInfo) mList.get(position);
        viewHolder.tvTitle.setText(storyListInfo.title);
        if (!TextUtils.isEmpty(storyListInfo.subTitle)) {
            viewHolder.tvSubTitle.setVisibility(View.VISIBLE);
            viewHolder.tvSubTitle.setText(storyListInfo.subTitle);

        } else {
            viewHolder.tvSubTitle.setVisibility(View.GONE);
        }

        if (isHorizontal) {
            viewHolder.itemView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            viewHolder.itemView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        }
        ImageUtil.setImage(viewHolder.itemView.getContext(), storyListInfo.image, viewHolder.ivPic, R.drawable.water_mark_brand_category);

        viewHolder.itemView.setOnClickListener(view -> {
            CategoryInfo catInfo = DatabaseClient.getInstance(mContext).getCategoryById(catId);
            if (catInfo != null){
                if(TextUtils.isEmpty(storyListInfo.colorCode)){
                    storyListInfo.colorCode =  catInfo.color;
                }
                mContext.startActivityForResult(DivyaCommonArticleActivity.getIntent(mContext, storyListInfo, catInfo), Constants.REQ_CODE_BOTTOM_NAV);

            }
        });

        viewHolder.notifyDisplayPrefs();
    }

    private void articleBindHolderV2(ArticleViewHolderV2 viewHolder, int position) {
        final StoryListInfo storyListInfo = (StoryListInfo) mList.get(position);
        viewHolder.tvTitle.setText(storyListInfo.title);
        if (!TextUtils.isEmpty(storyListInfo.subTitle)) {
            viewHolder.tvSubTitle.setVisibility(View.VISIBLE);
            viewHolder.tvSubTitle.setText(storyListInfo.subTitle);

        } else {
            viewHolder.tvSubTitle.setVisibility(View.GONE);
        }
        if (isHorizontal) {
            viewHolder.itemView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            viewHolder.itemView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        }
        ImageUtil.setImage(viewHolder.itemView.getContext(), storyListInfo.image, viewHolder.ivPic, R.drawable.water_mark_news_list);

        viewHolder.itemView.setOnClickListener(view -> {
            CategoryInfo catInfo = DatabaseClient.getInstance(mContext).getCategoryById(catId);
            if (catInfo != null){
                if(TextUtils.isEmpty(storyListInfo.colorCode)){
                    storyListInfo.colorCode =  catInfo.color;
                }
                mContext.startActivityForResult(DivyaCommonArticleActivity.getIntent(mContext, storyListInfo, catInfo),Constants.REQ_CODE_BOTTOM_NAV);
            }
        });

        viewHolder.notifyDisplayPrefs();
    }

    /******** View Holders Classes ********/

    class ArticleViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivPic;
        private TextView tvTitle, tvSubTitle;
        private View rootView;

        ArticleViewHolder(View itemView) {
            super(itemView);
            ivPic = itemView.findViewById(R.id.iv_pic);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvSubTitle = itemView.findViewById(R.id.tv_sub_title);
            rootView = itemView.findViewById(R.id.root_view);
        }

        public void notifyDisplayPrefs() {
            if (!isNightModeApplicable)
                return;
            boolean isNightMode = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.IS_NIGHT_MODE, false);
            tvTitle.setTextColor((isNightMode) ? Color.WHITE : Color.BLACK);
            tvSubTitle.setTextColor((isNightMode) ? Color.WHITE : Color.BLACK);
            rootView.setBackgroundColor((isNightMode) ? Color.BLACK : Color.WHITE);
        }
    }

    class ArticleViewHolderV2 extends RecyclerView.ViewHolder {
        private ImageView ivPic;
        private TextView tvTitle, tvSubTitle;
        private View rootView;

        ArticleViewHolderV2(View itemView) {
            super(itemView);
            ivPic = itemView.findViewById(R.id.iv_pic);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvSubTitle = itemView.findViewById(R.id.tv_sub_title);
            rootView = itemView.findViewById(R.id.root_view);
        }

        public void notifyDisplayPrefs() {
            if (!isNightModeApplicable)
                return;
            boolean isNightMode = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.IS_NIGHT_MODE, false);
            tvTitle.setTextColor((isNightMode) ? Color.WHITE : Color.BLACK);
            tvSubTitle.setTextColor((isNightMode) ? Color.WHITE : Color.BLACK);
            rootView.setBackgroundColor((isNightMode) ? Color.BLACK : Color.WHITE);
        }
    }
}
