package com.db.divya_new.divyashree;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.FilterInfo;
import com.db.data.models.StoryListInfo;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.divya_new.data.FilterData;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.db.util.Constants.KeyPair.KEY_IS_STORY_V2;

public class StoryListFragment extends Fragment implements OnMoveTopListener {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    private TextView tvTitle;
    private TextView tvPopup;
    private TextView selectedItemPopup;
    private RelativeLayout popupLayout;

    private StoryListAdapter adapter;

    private CategoryInfo categoryInfo;
    private boolean isStoryV2;

    private String finalFeedURL;
    private int LOAD_MORE_COUNT = 1;
    private ArrayList<FilterInfo> filterList;
    private String popupTitle;
    private int selectedPopIdPos = -1;
    private boolean isLoading;
    private boolean isLoadMoreEnabled = true;
    private String popupSearchHint;
    private List<BannerInfo> bannerInfoList;
    private int totalItemCount, lastVisibleItem;
    private static final int visibleThreshold = 5;

    public static StoryListFragment newInstance(CategoryInfo categoryInfo, String id, boolean isStoryV2) {
        StoryListFragment fragment = new StoryListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putBoolean(KEY_IS_STORY_V2, isStoryV2);
        bundle.putString(Constants.KeyPair.KEY_ID, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    public StoryListFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            isStoryV2 = bundle.getBoolean(KEY_IS_STORY_V2);
        }
        /*Banner*/
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_story, container, false);

        ProgressBar progressBar = view.findViewById(R.id.progressBar);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setNestedScrollingEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        if (getActivity() != null) {
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }

        tvTitle = view.findViewById(R.id.tv_title);
        tvPopup = view.findViewById(R.id.tv_popup);
        selectedItemPopup = view.findViewById(R.id.selected_item_tv);
        popupLayout = view.findViewById(R.id.popup_layout);

        adapter = new StoryListAdapter(getActivity(),  false, isStoryV2);
        adapter.setData(categoryInfo,false);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), AppUtils.getSpan());
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    if (dy > 0) {
                        totalItemCount = layoutManager.getItemCount();
                        lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                        if (!isLoading && isLoadMoreEnabled && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            onLoadMore();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        tvPopup.setOnClickListener(v -> {
            if (filterList != null && filterList.size() > 0) {
                showListingDialog(popupSearchHint);
            }
        });

        selectedItemPopup.setOnClickListener(v -> {
            selectedPopIdPos = -1;
            LOAD_MORE_COUNT = 1;
            selectedItemPopup.setVisibility(View.GONE);
            fetchData(true);
        });

        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case StoryListAdapter.TYPE_LOADING:
                    case StoryListAdapter.VIEW_TYPE_AD:
                    case StoryListAdapter.VIEW_TYPE_BANNER:
                        return AppUtils.getSpan();
                    case StoryListAdapter.TYPE_LIST:
                        return 1;
                    default:
                        return 1;
                }
            }
        });

        setUpPageTitle(categoryInfo.extraVlues);
        fetchData(true);

        return view;
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = () -> {
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
        LOAD_MORE_COUNT = 1;
        fetchData(true);
    };

    private void onLoadMore() {
        recyclerView.post(() -> {
            adapter.addItem();
        });

        fetchData(false);
    }

    public void moveToTop() {
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    private void setUpPageTitle(String extraVlues) {
        String pageTitle = null;
        if (!TextUtils.isEmpty(extraVlues)) {
            try {
                if (new JSONObject(extraVlues).has("category_intro")) {
                    pageTitle = new JSONObject(extraVlues).optString("category_intro");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setPageTitle(pageTitle);
        }
    }

    public void setPageTitle(String pageTitle) {
        if (!TextUtils.isEmpty(pageTitle)) {
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(pageTitle);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                tvTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        } else {
            tvTitle.setVisibility(View.GONE);
        }
    }

    private void showListingDialog(String header) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(header);

        String[] arr = new String[filterList.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = filterList.get(i).title;
        }

        builder.setSingleChoiceItems(arr, selectedPopIdPos, (dialogInterface, i) -> {
            selectedPopIdPos = i;
            LOAD_MORE_COUNT = 1;

            selectedItemPopup.setText(filterList.get(selectedPopIdPos).title);
            selectedItemPopup.setVisibility(View.VISIBLE);

            fetchData(true);
            dialogInterface.dismiss();
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void fetchData(boolean isClear) {
        String id = "";
        if (filterList != null && selectedPopIdPos >= 0 && selectedPopIdPos < filterList.size()) {
            id = filterList.get(selectedPopIdPos).id + "/";
        }
        finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + id + "PG" + LOAD_MORE_COUNT + "/";
        makeJsonObjectRequest(getContext(), isClear);
    }

    private void makeJsonObjectRequest(Context mContext, final boolean isClear) {
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            isLoading = true;
            Systr.println("Feed URL == " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL),null,
                    response -> {
                        if (LOAD_MORE_COUNT > 1)
                            adapter.removeItem();

                        try {
                            if (!TextUtils.isEmpty(response.toString())) {
                                parseOnlineData(response, isClear);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }

                        isLoading = false;

                    }, error -> {
                VolleyLog.d("NewsListFragment", "Error: " + error.getMessage());

                if (LOAD_MORE_COUNT > 1)
                    adapter.removeItem();

                Activity activity = (Activity) mContext;
                if (activity != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(activity, mContext.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(activity, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                isLoading = false;
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        } else {
            if (mContext != null) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
            }

            isLoading = false;
        }
    }

    private void parseOnlineData(JSONObject jsonObject, boolean isClear) {
        int count = jsonObject.optInt("count");
        JSONArray jsonArray = jsonObject.optJSONArray("feed");
        FilterData filterData = null;
        if (jsonArray != null) {
            ArrayList<Object> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), StoryListInfo[].class)));

//            if (LOAD_MORE_COUNT <= 1 && !TextUtils.isEmpty(id)) {
//                CatHomeData catHomeData = new CatHomeData(list, count, CatHomeData.TYPE_STORY, System.currentTimeMillis());
//                if (jsonObject.has("filter")) {
//                    catHomeData.setFilterData(new Gson().fromJson(jsonObject.optJSONObject("filter").toString(), FilterData.class));
//                    filterData = catHomeData.getFilterData();
//                }
//                if (!TextUtils.isEmpty(pageTitle)) {
//                    catHomeData.pageTitle = pageTitle;
//                }
//                DataParser.getInstance().addCatHomeDataHashMap(id, catHomeData);
//            }

            if (jsonObject.has("filter")) {
                filterData = new Gson().fromJson(jsonObject.optJSONObject("filter").toString(), FilterData.class);
            }

            if (jsonArray.length() < count) {
                isLoadMoreEnabled = false;
            } else {
                isLoadMoreEnabled = true;
                LOAD_MORE_COUNT++;
            }
            handleUI(isClear, list, filterData);
        }
    }

    private void handleUI(boolean isClear, ArrayList<Object> list, FilterData filterData) {

        if (isClear) {
            // add ads in listing
            addBanner(list);
            if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
                list.add(0, AdController.showATFForAllListing(getActivity(), adapter));
            }
            adapter.addAndClearAll(list);
        } else
            adapter.addAll(list);

        if (filterData != null) {
            try {
                popupTitle = filterData.getTitle();
                popupSearchHint = filterData.getSearch_hint();
                filterList = filterData.getData();
            } catch (Exception e) {
            }
        }

        if (filterList == null || filterList.size() <= 0) {
            popupLayout.setVisibility(View.GONE);
        } else {
            tvPopup.setText(popupTitle);
            popupLayout.setVisibility(View.VISIBLE);
        }
    }

    private void addBanner(ArrayList<Object> list) {
//        if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
            for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
                if (!bannerInfoList.get(bannerIndex).isAdded)
                    if (Integer.parseInt(bannerInfoList.get(bannerIndex).catPos) != -1) {
                        list.add(0, bannerInfoList.get(bannerIndex));
                        bannerInfoList.get(bannerIndex).isAdded = true;
                    }
            }
        }
//    }
}
