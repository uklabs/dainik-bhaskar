package com.db.divya_new.divyashree;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.divyashree.viewholder.StoryViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.util.AppPreferences;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;

import java.util.ArrayList;
import java.util.List;

public class StoryParentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int VIEW_TYPE_BANNER = 4;
    private final static int VIEW_TYPE_AD = 25;
    private static final int VIEW_TYPE_STORY = 1;

    private List<Object> mList;
    private Activity mContext;
    private CategoryInfo categoryInfo;
    private String mSuperParentId;


    private boolean isRefreshing = false;
    private boolean isStoryV2;
    private List<BannerInfo> bannerInfoList;

    public StoryParentAdapter(Activity context, CategoryInfo categoryInfo, String id, boolean isStoryV2) {
        this.isStoryV2 = isStoryV2;
        this.mList = new ArrayList<>();
        this.mContext = context;
        this.mSuperParentId = id;
        this.categoryInfo = categoryInfo;
        /*Banner*/
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    public void clear() {
        mList.clear();
    }

    public void addAll(ArrayList<Object> objectList) {
        mList.addAll(objectList);
        addBanner();
        addAds();
        notifyDataSetChanged();
    }

    private void addBanner() {
//        if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
            for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
                if (!bannerInfoList.get(bannerIndex).isAdded)
                    if (Integer.parseInt(bannerInfoList.get(bannerIndex).catPos) > -1) {
                        mList.add(0, bannerInfoList.get(bannerIndex));
                        bannerInfoList.get(bannerIndex).isAdded = true;
                    }
            }
//        }
    }


    private void addAds() {
        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
            mList.add(0, AdController.showATFForAllListing(mContext, null));
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_STORY:
                StoryViewHolder storyViewHolder = new StoryViewHolder(inflater.inflate(R.layout.listitem_recylerview_title_story, parent, false), mContext,  false, isStoryV2);
                storyViewHolder.setData(categoryInfo);
                return storyViewHolder;

            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof StoryViewHolder) {
            storyBindHolder((StoryViewHolder) holder);
        } else if (holder instanceof BannerAdViewHolder) {
            BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
            adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
        } else if (holder instanceof BannerViewHolder) {
            BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
            bannerViewHolder.callwebViewOrSetWidgetData((BannerInfo) mList.get(holder.getAdapterPosition()));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mList.get(position);
        if (object instanceof AdInfo) {
            return VIEW_TYPE_AD;
        } else if (object instanceof BannerInfo) {
            return VIEW_TYPE_BANNER;
        }
        return VIEW_TYPE_STORY;
    }

    /******** Bind Holders Methods ********/


    private void storyBindHolder(final StoryViewHolder viewHolder) {
        if (isRefreshing)
            viewHolder.onRefresh();
    }

    public void onSwipeRefresh(SwipeRefreshLayout swipeRefreshLayout) {
        isRefreshing = true;
        notifyDataSetChanged();
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }
}
