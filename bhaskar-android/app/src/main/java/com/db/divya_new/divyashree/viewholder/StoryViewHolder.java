package com.db.divya_new.divyashree.viewholder;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.FilterInfo;
import com.db.data.models.StoryListInfo;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.LoadMoreController;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHomeData;
import com.db.divya_new.data.FilterData;
import com.db.divya_new.divyashree.StoryListAdapter;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class StoryViewHolder extends RecyclerView.ViewHolder implements LoadMoreController {

    public RecyclerView recyclerView;
    public StoryListAdapter mAdapter;
    public Button btn_retry;
    private TextView tvPopup;
    private TextView selectedItemPopup;
    private RelativeLayout popupLayout;
    private String mSuperParentId;
    private Activity mContext;
    private String feedUrl;
    private String finalFeedURL;
    private int LOAD_MORE_COUNT = 1;
    private int showCount = 0;
    private boolean mIsFromHome;
    private boolean isLoading;
    private boolean isFromParentHome;
    private String parentId;
    private ProgressBar loadMoreProgressBar;
    private boolean isLoadMoreEnabled = true;
    private String pageTitle = null;
    private String popupSearchHint;
    private ArrayList<FilterInfo> filterList;
    private String popupTitle;
    private int selectedPopIdPos = -1;
    private String id;
    private boolean isNightModeApplicable;
    private TextView tvTitle;
    private TextView tvHeaderTitle;
    private Button btnHeaderLoadMore;
    private Button btnSubHeaderLoadMore;
    private Button btnLoadMore;
    private boolean isRefreshing;

    public StoryViewHolder(View itemView, Activity activity, boolean isNightModeApplicable, boolean isStoryV2) {
        super(itemView);
        this.mContext = activity;
        this.isNightModeApplicable = isNightModeApplicable;
        tvTitle = itemView.findViewById(R.id.tv_title);
        tvHeaderTitle = itemView.findViewById(R.id.subHeaderTitleTV);
        btnHeaderLoadMore = itemView.findViewById(R.id.btnHeaderLoadMore);
        btnSubHeaderLoadMore = itemView.findViewById(R.id.btnSubHeaderLoadMore);
        btnLoadMore = itemView.findViewById(R.id.btn_load_more);
        tvPopup = itemView.findViewById(R.id.tv_popup);
        selectedItemPopup = itemView.findViewById(R.id.selected_item_tv);
        popupLayout = itemView.findViewById(R.id.popup_layout);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        btn_retry = itemView.findViewById(R.id.btn_retry);
        loadMoreProgressBar.setVisibility(View.GONE);
        recyclerView = itemView.findViewById(R.id.recycler_view);
        mAdapter = new StoryListAdapter(mContext, isNightModeApplicable, isStoryV2);
        recyclerView.setNestedScrollingEnabled(true);
        btn_retry.setOnClickListener(view -> {
            if (!isLoading) {
                btn_retry.setVisibility(View.INVISIBLE);
                fetchData(false, true);
            }
        });

        tvPopup.setOnClickListener(view -> {
            if (filterList != null && filterList.size() > 0) {
                showListingDialog(popupSearchHint);
            }
        });

        selectedItemPopup.setOnClickListener(view -> {
            selectedPopIdPos = -1;
            LOAD_MORE_COUNT = 1;
            selectedItemPopup.setVisibility(View.GONE);
            fetchData(true, false);
        });


    }

    private void onloadmore() {
        if (mIsFromHome && isFromParentHome) {
            if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                new Handler().postDelayed(() -> {
                    TabController.getInstance().setTabClicked(true);
                    TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mSuperParentId, parentId, StoryViewHolder.this);
                }, 500);
            }
        } else if (mIsFromHome) {
            if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(parentId);
            }
        } else if (!isFromParentHome && !mIsFromHome) {
            if (!isLoading) {
                fetchData(false, true);
            }
        }
    }

    private void loadMoreLogic() {
        if (isFromParentHome) {
            btnHeaderLoadMore.setVisibility(View.VISIBLE);
            btnSubHeaderLoadMore.setVisibility(View.GONE);
        } else if (mIsFromHome) {
            btnSubHeaderLoadMore.setVisibility(View.VISIBLE);
            btnHeaderLoadMore.setVisibility(View.GONE);
        } else {
            //infinite loading true
            btnHeaderLoadMore.setVisibility(View.GONE);
            btnSubHeaderLoadMore.setVisibility(View.GONE);
            btnLoadMore.setVisibility(View.VISIBLE);
        }

        btnHeaderLoadMore.setOnClickListener(v -> onloadmore());
        btnSubHeaderLoadMore.setOnClickListener(v -> onloadmore());
        btnLoadMore.setOnClickListener(v -> {
            btnLoadMore.setVisibility(View.GONE);
            onloadmore();
        });
    }


    public void clearDataAndLoadNew() {
        mAdapter.clear();
        mAdapter.notifyDataSetChanged();
    }

    public void setPageTitle(String pageTitle) {

        if (!TextUtils.isEmpty(pageTitle)) {
            this.pageTitle = pageTitle;
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(pageTitle);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                tvTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        } else {
            tvTitle.setVisibility(View.GONE);
        }
    }

    // Adding the title is available in the categoryInfo else it will be gone
    private void setUpPageTitle(String extraVlues) {
        String pageTitle = null;
        if (!TextUtils.isEmpty(extraVlues)) {
            try {
                if (new JSONObject(extraVlues).has("category_intro")) {
                    pageTitle = new JSONObject(extraVlues).optString("category_intro");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.pageTitle = pageTitle;
            setPageTitle(pageTitle);
        }
    }

    public void setData(CategoryInfo categoryInfo) {
        this.mIsFromHome = false;
        this.isFromParentHome = false;
        this.parentId = categoryInfo.id;
        this.id = categoryInfo.id;
        this.mSuperParentId = categoryInfo.parentId;
        this.showCount = 0;

        this.feedUrl = categoryInfo.feedUrl;
        GridLayoutManager layoutManager = new GridLayoutManager(itemView.getContext(), AppUtils.getSpan());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setData(categoryInfo, mIsFromHome);
        // Adds the scroll listener to RecyclerView
        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && LOAD_MORE_COUNT <= 2) {
            if (!TextUtils.isEmpty(catHomeData.pageTitle)) {
                setPageTitle(catHomeData.pageTitle);
            }
            parseOfflineData(catHomeData, true);
            LOAD_MORE_COUNT = 2;
        } else {
            setUpPageTitle(categoryInfo.extraVlues);
            LOAD_MORE_COUNT = 1;
            fetchData(true, false);
        }

        loadMoreLogic();
    }

    public void setDataa(CategoryInfo categoryInfo, String parentId, String superParentId, boolean isFromHome, boolean isFromParentHome, int showCount, String horizontalList) {
        this.id = categoryInfo.id;
        this.mIsFromHome = isFromHome;
        this.isFromParentHome = isFromParentHome;
        this.mSuperParentId = superParentId;
        this.parentId = parentId;
        this.showCount = showCount;
        this.feedUrl = categoryInfo.feedUrl;

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && LOAD_MORE_COUNT <= 2) {
            parseOfflineData(catHomeData, true);
            LOAD_MORE_COUNT = 2;
        } else {
            LOAD_MORE_COUNT = 1;
            fetchData(true, false);
        }


        if (!TextUtils.isEmpty(horizontalList) && horizontalList.equalsIgnoreCase("1")) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
        } else {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(itemView.getContext(), AppUtils.getSpan());

            recyclerView.setLayoutManager(gridLayoutManager);
        }


        recyclerView.setAdapter(mAdapter);
        mAdapter.setData(categoryInfo, mIsFromHome);

        // Adds the scroll listener to RecyclerView
        setUpPageTitle(categoryInfo.extraVlues);
        loadMoreLogic();
    }

    public void setData(CategoryInfo categoryInfo, String parentId, String superParentId, boolean isFromHome, boolean isFromParentHome, int showCount, String horizontalList) {
        this.id = categoryInfo.id;
        this.mIsFromHome = isFromHome;
        this.isFromParentHome = isFromParentHome;
        this.mSuperParentId = superParentId;
        this.parentId = parentId;
        this.showCount = showCount;
        this.feedUrl = categoryInfo.feedUrl;

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && LOAD_MORE_COUNT <= 2) {
            parseOfflineData(catHomeData, true);
            LOAD_MORE_COUNT = 2;
        } else {
            LOAD_MORE_COUNT = 1;
            fetchData(true, false);
        }


        if (!TextUtils.isEmpty(horizontalList) && horizontalList.equalsIgnoreCase("1")) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
        } else {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(itemView.getContext(), AppUtils.getSpan());

            recyclerView.setLayoutManager(gridLayoutManager);
        }


        recyclerView.setAdapter(mAdapter);
        mAdapter.setData(categoryInfo, mIsFromHome);

        // Adds the scroll listener to RecyclerView
        setUpPageTitle(categoryInfo.extraVlues);
        loadMoreLogic();
    }

    private void fetchData(boolean isClear, boolean isLoadMore) {
        String id = "";
        if (filterList != null && selectedPopIdPos >= 0 && selectedPopIdPos < filterList.size()) {
            id = filterList.get(selectedPopIdPos).id + "/";
        }
        finalFeedURL = Urls.APP_FEED_BASE_URL + feedUrl + id + "PG" + LOAD_MORE_COUNT + "/";
        btn_retry.setVisibility(View.GONE);
        makeJsonObjectRequest(isClear);
    }

    private void makeJsonObjectRequest(final boolean isClear) {
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            if (!isRefreshing)
                loadMoreProgressBar.setVisibility(View.VISIBLE);

            isLoading = true;
            AppLogs.printDebugLogs("Feed URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        loadMoreProgressBar.setVisibility(View.GONE);
                        try {
                            if (!TextUtils.isEmpty(response.toString())) {
                                parseOnlineData(response, isClear);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        isLoading = false;
                    }, error -> {
                VolleyLog.d("NewsListFragment", "Error: " + error.getMessage());
                Activity activity = (Activity) mContext;
                if (activity != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(activity, mContext.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(activity, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }

                isLoading = false;
                loadMoreProgressBar.setVisibility(View.GONE);

                if (isFromParentHome || mIsFromHome)
                    btn_retry.setVisibility(View.VISIBLE);
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            if (!mIsFromHome)
                VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
            else
                VolleyNetworkSingleton.getInstance(mContext, 2).addToRequestQueue(jsonObjReq);
        } else {
            if (mContext != null) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
            }

            isLoading = false;
            loadMoreProgressBar.setVisibility(View.GONE);

            if (isFromParentHome || mIsFromHome)
                btn_retry.setVisibility(View.VISIBLE);
        }
    }


    private void parseOnlineData(JSONObject jsonObject, boolean isClear) {
        int count = jsonObject.optInt("count");
        JSONArray jsonArray = jsonObject.optJSONArray("feed");
        FilterData filterData = null;
        if (jsonArray != null) {
            ArrayList<Object> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), StoryListInfo[].class)));

            if (LOAD_MORE_COUNT <= 1 && !TextUtils.isEmpty(id)) {
                CatHomeData catHomeData = new CatHomeData(list, count, CatHomeData.TYPE_STORY, System.currentTimeMillis());
                if (jsonObject.has("filter")) {
                    catHomeData.setFilterData(new Gson().fromJson(jsonObject.optJSONObject("filter").toString(), FilterData.class));
                    filterData = catHomeData.getFilterData();
                }
                if (!TextUtils.isEmpty(pageTitle)) {
                    catHomeData.pageTitle = pageTitle;
                }
                DataParser.getInstance().addCatHomeDataHashMap(id, catHomeData);
            }

            if (jsonArray.length() < count) {
                isLoadMoreEnabled = false;
                btnLoadMore.setVisibility(View.GONE);
            } else {
                isLoadMoreEnabled = true;
                if (!mIsFromHome && !isFromParentHome)
                    btnLoadMore.setVisibility(View.VISIBLE);
                LOAD_MORE_COUNT++;
            }
            handleUI(isClear, list, filterData);

        }
    }

    private void parseOfflineData(CatHomeData catHomeData, boolean isClear) {
        int count = catHomeData.count;
        FilterData filterData = catHomeData.getFilterData();
        ArrayList<Object> list = (ArrayList<Object>) catHomeData.getArrayList(CatHomeData.TYPE_STORY);
        if (list != null) {

            isLoadMoreEnabled = list.size() >= count;

            handleUI(isClear, list, filterData);
        }
    }

    private void handleUI(boolean isClear, ArrayList<Object> list, FilterData filterData) {

        //passing the data to the adapter to show.
        if (showCount > 0 && showCount < list.size()) {
            if (isClear) {
                mAdapter.clear();
            }
            for (int i = 0; i < showCount; i++) {
                mAdapter.add(list.get(i));
            }
            isLoadMoreEnabled = true;
        } else {
            if (isClear) {
                mAdapter.addAndClearAll(list);
            } else
                mAdapter.addAll(list);

            if (mIsFromHome) isLoadMoreEnabled = false;
        }

        if (filterData != null) {
            try {
                popupTitle = filterData.getTitle();
                popupSearchHint = filterData.getSearch_hint();
                filterList = filterData.getData();
            } catch (Exception e) {
            }
        }

        if (filterList == null || filterList.size() <= 0) {
            popupLayout.setVisibility(View.GONE);
        } else {
            tvPopup.setText(popupTitle);
            popupLayout.setVisibility(View.VISIBLE);
        }

        btn_retry.setVisibility(View.GONE);
    }

    private void showListingDialog(String header) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(header);

        String[] arr = new String[filterList.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = filterList.get(i).title;
        }

        builder.setSingleChoiceItems(arr, selectedPopIdPos, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectedPopIdPos = i;
                LOAD_MORE_COUNT = 1;

                selectedItemPopup.setText(filterList.get(selectedPopIdPos).title);
                selectedItemPopup.setVisibility(View.VISIBLE);

                fetchData(true, false);
                dialogInterface.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onLoadFinish() {
        btn_retry.setVisibility(View.GONE);
        loadMoreProgressBar.setVisibility(View.GONE);
    }

    public void onRefresh() {
        LOAD_MORE_COUNT = 1;
        if (!isLoading)
            fetchData(true, false);
    }

    public void notifyDisplayPrefs() {
        if (!isNightModeApplicable)
            return;
        //AppUtils.getInstance().setFontSize(mContext, tvArticleName, Constants.Font.CAT_SMALL_LARGE);
        boolean isNightMode = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.IS_NIGHT_MODE, false);
        tvTitle.setTextColor((isNightMode) ? Color.WHITE : Color.BLACK);
        tvHeaderTitle.setTextColor((isNightMode) ? Color.WHITE : Color.BLACK);
        mAdapter.notifyDataSetChanged();
    }

    public void setIsRefreshing(boolean isRefreshing) {
        this.isRefreshing = isRefreshing;
    }
}
