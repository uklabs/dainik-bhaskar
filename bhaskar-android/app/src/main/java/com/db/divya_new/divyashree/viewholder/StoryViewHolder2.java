package com.db.divya_new.divyashree.viewholder;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.divya_new.data.CatHome2;
import com.db.divya_new.divyashree.StoryListAdapter2;
import com.db.util.AppUtils;

public class StoryViewHolder2 extends RecyclerView.ViewHolder {

    public RecyclerView recyclerView;
    public StoryListAdapter2 mAdapter;
    private Activity mContext;

    public StoryViewHolder2(View itemView, Activity activity, boolean isNightModeApplicable, boolean isStoryV2) {
        super(itemView);
        this.mContext = activity;

        mAdapter = new StoryListAdapter2(mContext, isNightModeApplicable, isStoryV2);

        recyclerView = itemView.findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(itemView.getContext(), AppUtils.getSpan());
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    public void setData(CatHome2 catHome) {
        boolean isHorizontal;
        if (!TextUtils.isEmpty(catHome.horizontalList) && catHome.horizontalList.equalsIgnoreCase("1")) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            isHorizontal = true;
        } else {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(itemView.getContext(), AppUtils.getSpan());
            recyclerView.setLayoutManager(gridLayoutManager);
            isHorizontal = false;
        }

        mAdapter.setData(catHome.id, isHorizontal);
        mAdapter.addAndClearAll(catHome.list);
    }
}
