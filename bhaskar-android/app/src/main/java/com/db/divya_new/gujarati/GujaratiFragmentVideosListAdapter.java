package com.db.divya_new.gujarati;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.bhaskar.appscommon.ads.NativeAdViewHolder2;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.source.server.BackgroundRequest;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.listeners.OnDBVideoClickListener;
import com.db.listeners.OnItemClickListener;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.ShareUtil;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.ArrayList;
import java.util.List;

public class GujaratiFragmentVideosListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {

    public static final int LIST_VERTICAL = 1;
    public static final int LIST_VERTICAL_BIG = 2;
    public static final int VIEW_TYPE_LOADING = 3;

    public static final int VIEW_TYPE_BANNER = 4;

    private final int VIEW_TYPE_NATIVE_ADS = 5;
    private final static int VIEW_TYPE_AD = 6;

    private Context mContext;
    private ArrayList<Object> dbVideosInfoArrayList;
    private OnDBVideoClickListener onDBVideoClickListener;
    private String mColorCode;
    private CategoryInfo categoryInfo;
    private List<BannerInfo> bannerInfoList;

    public GujaratiFragmentVideosListAdapter(Context contexts, OnDBVideoClickListener onDBVideoClickListener, CategoryInfo categoryInfo) {
        this.onDBVideoClickListener = onDBVideoClickListener;
        this.mContext = contexts;
        mColorCode = categoryInfo.color;
        this.categoryInfo = categoryInfo;
        dbVideosInfoArrayList = new ArrayList<>();
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    public void setData(List<VideoInfo> newsFeed) {
        dbVideosInfoArrayList.addAll(newsFeed);
        notifyDataSetChanged();
    }

    public void setDataAndClear(List<VideoInfo> newsFeed) {
        dbVideosInfoArrayList.clear();
        dbVideosInfoArrayList.addAll(newsFeed);
        addBanner();
        addAds();
        notifyDataSetChanged();
    }

    public void addItem() {
        dbVideosInfoArrayList.add(null);
    }

    private void addAds() {
        isBannerLoaded = false;
        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
            dbVideosInfoArrayList.add(0, AdController.showATFForAllListing(mContext, this));
        }
    }

    private void addBanner() {
//        if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
        for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
            if (!bannerInfoList.get(bannerIndex).isAdded) {
                int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catPos);
                if (bannerPos > -1 && dbVideosInfoArrayList.size() >= bannerPos) {
                    dbVideosInfoArrayList.add(bannerPos, bannerInfoList.get(bannerIndex));
                    bannerInfoList.get(bannerIndex).isAdded = true;
                }
            }
        }
//        }
    }

    public void removeItem() {
        if (dbVideosInfoArrayList.size() > 0 && dbVideosInfoArrayList.get(dbVideosInfoArrayList.size() - 1) == null) {
            dbVideosInfoArrayList.remove(dbVideosInfoArrayList.size() - 1);
            notifyDataSetChanged();
        }
    }

    public VideoInfo getItemAtPosition(int position) {
        if (position >= 0 && position < dbVideosInfoArrayList.size() && dbVideosInfoArrayList.get(position) instanceof VideoInfo)
            return (VideoInfo) dbVideosInfoArrayList.get(position);
        return null;
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;
        notifyItemChanged(0);
    }

    public ArrayList<Object> getItemsList(){
        return dbVideosInfoArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case LIST_VERTICAL_BIG:
                return new ViewHolder(inflater.inflate(R.layout.listitem_guruvani_video_item, parent, false));
            case LIST_VERTICAL:
                return new ViewHolder(inflater.inflate(R.layout.listitem_guruvani_video_item_small, parent, false));
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false));
            case VIEW_TYPE_NATIVE_ADS:
                return new NativeAdViewHolder2(inflater.inflate(R.layout.ad_layout_native_listing_view, parent, false));
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            default:
                return new BlankViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LIST_VERTICAL_BIG:
            case LIST_VERTICAL:
                final ViewHolder mainViewHolder = (ViewHolder) holder;
                final VideoInfo dbVideosInfo = (VideoInfo) dbVideosInfoArrayList.get(mainViewHolder.getAdapterPosition());

                mainViewHolder.tvDBVideosTitle.setText(AppUtils.getInstance().getHomeTitle(dbVideosInfo.titleHead, AppUtils.getInstance().fromHtml(dbVideosInfo.title), mColorCode));
                mainViewHolder.playImageView.setColorFilter(AppUtils.getThemeColor(mContext, mColorCode));

                if (TextUtils.isEmpty(dbVideosInfo.duration)) {
                    mainViewHolder.tvDbVideosDuration.setVisibility(View.GONE);
                } else {
                    mainViewHolder.tvDbVideosDuration.setVisibility(View.GONE);
                    mainViewHolder.tvDbVideosDuration.setText(dbVideosInfo.duration);
                }

                AppUtils.getInstance().setImageViewSizeWithAspectRatio(mainViewHolder.ivDBVideosThumbnailImage, Constants.ImageRatios.DBVIDEOS_RATIO, 16, mContext);
                ImageUtil.setImage(mContext, dbVideosInfo.image, mainViewHolder.ivDBVideosThumbnailImage, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);
                mainViewHolder.itemView.setOnClickListener(view -> {
                    if (NetworkStatus.getInstance().isConnected(mContext)) {
                        if (onDBVideoClickListener != null)
                            onDBVideoClickListener.onDbVideoClick(mainViewHolder.getAdapterPosition());
                    } else {
                        AppUtils.getInstance().showCustomToast(mContext, mContext.getString(R.string.no_network_error));
                    }
                });

                mainViewHolder.moreImg.setOnClickListener(view -> {
                    PopupWindow popUpWindow = AppUtils.showMoreOptionsV2(mContext, view, new MoreOptionInterface() {
                        @Override
                        public void onBookMark() {
                            //not visible
                        }

                        @Override
                        public void onShare() {
                            BackgroundRequest.getStoryShareUrl(mContext, dbVideosInfo.link, (flag, shareUrl) -> ShareUtil.shareDefault(mContext, dbVideosInfo.title, mContext.getResources().getString(R.string.checkout_this_video), shareUrl, dbVideosInfo.gTrackUrl, false));
                        }
                    });
                    popUpWindow.getContentView().findViewById(R.id.tvBookMark).setVisibility(View.GONE);
                    popUpWindow.getContentView().findViewById(R.id.separator_v).setVisibility(View.GONE);
                });
                break;
            case VIEW_TYPE_NATIVE_ADS:
                showNativeAds((NativeAdViewHolder2) holder, position);
                break;
            case VIEW_TYPE_AD:
//                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
//                adViewHolder.addAds(((AdInfo) dbVideosInfoArrayList.get(position)).bannerAdView);
                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
                if (((AdInfo) dbVideosInfoArrayList.get(position)).catType == 1) {
                    if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                        adViewHolder.addAds(((AdInfo) dbVideosInfoArrayList.get(position)).bannerAdView);

                    } else {
                        ImageView imageView = new ImageView(mContext);
                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setAdjustViewBounds(true);
                        imageView.setClickable(true);
                        String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                        ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                        adViewHolder.addAdsUrl(imageView);

                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AppUtils.getInstance().clickOnAdBanner(mContext);
                            }
                        });
                    }
                } else {
                    adViewHolder.addAds(((AdInfo) dbVideosInfoArrayList.get(position)).bannerAdView);
                }
                break;
            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData((BannerInfo) dbVideosInfoArrayList.get(position));
                break;
            case VIEW_TYPE_LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void showNativeAds(NativeAdViewHolder2 holder, int pos) {
        VideoInfo adInfo = (VideoInfo) dbVideosInfoArrayList.get(pos);

        UnifiedNativeAd ad = NativeListAdController2.getInstance(mContext).getAd(adInfo.adUnit, pos);
        holder.populateNativeAdView(ad);
    }

    @Override
    public int getItemViewType(int position) {
        if (dbVideosInfoArrayList.get(position) == null)
            return VIEW_TYPE_LOADING;
        else {
            try {
                Object object = dbVideosInfoArrayList.get(position);
                if (object instanceof AdInfo) {
                    return VIEW_TYPE_AD;
                } else if (object instanceof BannerInfo) {
                    return VIEW_TYPE_BANNER;
                } else {
                    VideoInfo info = (VideoInfo) object;
                    if (info.isAd) {
                        return VIEW_TYPE_NATIVE_ADS;
                    } else if (info.bigImage.equalsIgnoreCase("1"))
                        return LIST_VERTICAL_BIG;
                    else
                        return LIST_VERTICAL;
                }
            } catch (Exception e) {
            }
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return dbVideosInfoArrayList == null ? 0 : dbVideosInfoArrayList.size();
    }

    public void clear() {
        dbVideosInfoArrayList.clear();
        notifyDataSetChanged();
    }

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private ImageView ivDBVideosThumbnailImage, moreImg, playImageView;
        private TextView tvDBVideosTitle, tvViews, tvDbVideosDuration;

        private OnItemClickListener clickListener;

        private ViewHolder(View itemView) {
            super(itemView);
            ivDBVideosThumbnailImage = itemView.findViewById(R.id.iv_pic);
            tvDBVideosTitle = itemView.findViewById(R.id.tv_title);
            tvViews = itemView.findViewById(R.id.tv_views);
            tvDbVideosDuration = itemView.findViewById(R.id.tv_duration);
            moreImg = itemView.findViewById(R.id.more_img);
            playImageView = itemView.findViewById(R.id.video_play_icon);
            itemView.setTag(itemView);
        }

        private void setClickListener(OnItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
            return true;
        }
    }

    //View Holders
    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        private LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }
}