package com.db.divya_new.gujarati;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.ads.AdPlacementController;
import com.db.ads.NativeListAdController2;
import com.db.data.models.CategoryInfo;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.VideoPlayerActivity;
import com.db.dbvideoPersonalized.detail.VideoPlayerListFragment;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.listeners.OnDBVideoClickListener;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GujaratiVideoFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnMoveTopListener, OnDBVideoClickListener, NativeListAdController2.onAdNotifyListener {

    private CategoryInfo categoryInfo;
    private String sectionName;

    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private GujaratiFragmentVideosListAdapter dbVideosListAdapter;

    private int pageCount = 1;
    private boolean isLoading;
    private int visibleThreshold = 5;
    private boolean isLoadMoreEnable = true;

    public static GujaratiVideoFragment newInstance(String sectionName, CategoryInfo categoryInfo, String id) {
        GujaratiVideoFragment fragment = new GujaratiVideoFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString(Constants.KeyPair.KEY_ID, id);
        bundle.putString(Constants.KeyPair.KEY_SECTION_LABEL, sectionName);
        fragment.setArguments(bundle);

        return fragment;
    }

    public GujaratiVideoFragment() {
    }

    private Rect scrollBounds;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            sectionName = bundle.getString(Constants.KeyPair.KEY_SECTION_LABEL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_common, container, false);

        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));

        // Recycler View
        recyclerView = view.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        // adapter create and set in recycler view
        dbVideosListAdapter = new GujaratiFragmentVideosListAdapter(getContext(), this, categoryInfo);
        recyclerView.setAdapter(dbVideosListAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (isLoadMoreEnable) {
                            onLoadMore();
                            isLoading = true;
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int i = mLayoutManager.findFirstVisibleItemPosition();
                int j = mLayoutManager.findLastVisibleItemPosition();
                for (int k = i; k <= j; k++) {
                    VideoInfo itemAtPosition = dbVideosListAdapter.getItemAtPosition(k);
                    if (itemAtPosition != null && itemAtPosition.isAd) {
//                            NativeListAdController.getInstance(getContext()).getAdRequest(itemAtPosition.adUnit, itemAtPosition.adType, k, new WeakReference<NativeListAdController.onAdNotifyListener>(NewsListFragment.this));
                        NativeListAdController2.getInstance(getContext()).getAdRequest(itemAtPosition.adUnit, itemAtPosition.adType, k, new WeakReference<NativeListAdController2.onAdNotifyListener>(GujaratiVideoFragment.this));
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onNotifyAd() {
        if (dbVideosListAdapter != null) {
            dbVideosListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.scrollBounds = new Rect();
        this.recyclerView.getHitRect(scrollBounds);

        pageCount = 1;
        fetchData(true);
    }

    private void onItemsLoadComplete() {
        Context context = getContext();
        if (context != null) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Stop refresh animation
                    if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
            });
        }
    }

    public void onLoadMore() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                dbVideosListAdapter.addItem();
            }
        });

        //Load more data for recycler view
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetchData(false);
            }
        }, 500);
    }

    @Override
    public void onRefresh() {
        pageCount = 1;
        isLoadMoreEnable = true;
        fetchData(true);
    }

    public void moveToTop() {
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    private void fetchData(boolean isClear) {

        makeJsonObjectRequest(Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + pageCount + "/", isClear);
    }

    private void makeJsonObjectRequest(String finalFeedURL, final boolean isClear) {
        Systr.println("Feed Url : " + finalFeedURL);

        if (NetworkStatus.getInstance().isConnected(getContext())) {
            isLoading = true;

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (pageCount > 1)
                                dbVideosListAdapter.removeItem();
                            try {
                                if (response != null) {
                                    parseFeedList(response, isClear);
                                }
                            } catch (Exception e) {
                            }

                            isLoading = false;
                            onItemsLoadComplete();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    isLoading = false;
                    if (pageCount > 1)
                        dbVideosListAdapter.removeItem();

                    onItemsLoadComplete();

                    Context context = getContext();
                    if (error instanceof NetworkError && context != null) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.internet_connection_error_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
            if (getContext() != null)
                AppUtils.getInstance().showCustomToast(getContext(), getContext().getResources().getString(R.string.no_network_error));
        }
    }

    private void parseFeedList(JSONObject response, boolean isClear) throws JSONException {
        int count = response.optInt("count");
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        if (resultArray != null) {
            List<VideoInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), VideoInfo[].class)));

            if (pageCount < 5) {
                list = AdPlacementController.getInstance().addVideoListingNativeAds(list, getContext());
            }

            if (resultArray.length() < count) {
                isLoadMoreEnable = false;
            } else {
                isLoadMoreEnable = true;
                pageCount++;
            }

            if (isClear) {
                dbVideosListAdapter.setDataAndClear(list);
            } else {
                dbVideosListAdapter.setData(list);
            }
        }
    }

    @Override
    public void onDbVideoClick(int position) {
        String sectionLabel = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME) + (TextUtils.isEmpty(sectionName) ? "_" : ("_" + sectionName + "_")) + categoryInfo.displayName;

        Intent intent;
//        if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.VideoPref.SETTINGS_IS_ADAPTIVE_PLAYER_ON, false)) {
//            intent = VideoDetailActivity.getIntent(getContext(), dbVideosListAdapter.getItem(position), sectionLabel, AppFlyerConst.DBVideosSource.VIDEO_LIST, position + 1, categoryInfo.detailUrl, categoryInfo.feedUrl, categoryInfo.gaEventLabel, categoryInfo.gaArticle, false, "", categoryInfo.color);
//        } else {
//        }

//        intent = DBVideoPlayerActivity2.getIntent(getContext(), dbVideosListAdapter.getItemAtPosition(position), sectionLabel, AppFlyerConst.DBVideosSource.VIDEO_LIST, position + 1, categoryInfo.detailUrl, categoryInfo.feedUrl, categoryInfo.gaEventLabel, categoryInfo.gaArticle, false, "", categoryInfo.color);
//        getContext().startActivity(intent);
        VideoInfo itemAtPosition = dbVideosListAdapter.getItemAtPosition(position);
        ArrayList<VideoInfo> filteredList = new ArrayList<>();
        ArrayList<Object> itemsList = dbVideosListAdapter.getItemsList();
        for (int i = 0; i < itemsList.size(); i++) {
            if (itemsList.get(i) instanceof VideoInfo && position != i) {
                filteredList.add((VideoInfo) itemsList.get(i));
            }
        }
        filteredList.add(0, itemAtPosition);
        intent = VideoPlayerActivity.getIntent(getContext(), 0, pageCount, categoryInfo.feedUrl, AppFlyerConst.DBVideosSource.VIDEO_LIST, categoryInfo.gaEventLabel, categoryInfo.gaArticle, sectionLabel, categoryInfo.color, VideoPlayerListFragment.TYPE_LIST, VideoPlayerListFragment.THEME_LIGHT);
        VideoPlayerActivity.addListData(filteredList);
        Objects.requireNonNull(getContext()).startActivity(intent);
    }
}
