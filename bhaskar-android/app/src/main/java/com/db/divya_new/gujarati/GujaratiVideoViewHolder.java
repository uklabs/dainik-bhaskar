package com.db.divya_new.gujarati;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.DBVideoPlayerActivity2;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.LoadMoreController;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHomeData;
import com.db.listeners.OnDBVideoClickListener;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class GujaratiVideoViewHolder extends RecyclerView.ViewHolder implements OnDBVideoClickListener, LoadMoreController {

    private GujaratiVideosListAdapter dbVideosListAdapter;
    private RecyclerView dbVideosRecyclerView;
    private Activity mContext;

    private Button btnRetry;
    public Button btnHeaderLoadMore;
    public Button btnSubHeaderLoadMore;
    private ProgressBar loadMoreProgressBar;

    private String sectionName;
    private CategoryInfo categoryInfo;

    private boolean isFromHome;
    private boolean isFromParentHome;
    private String mParentId;
    private String id;
    private int showCount;

    private int pageCount = 1;
    private boolean isLoading;
    private boolean isLoadMoreEnabled = true;

    public GujaratiVideoViewHolder(View itemView, Activity context) {
        super(itemView);
        mContext = context;


        dbVideosListAdapter = new GujaratiVideosListAdapter(mContext, this);

        dbVideosRecyclerView = itemView.findViewById(R.id.recycler_view);
        dbVideosRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));

        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        btnRetry = itemView.findViewById(R.id.btn_retry);
        btnHeaderLoadMore = itemView.findViewById(R.id.btnHeaderLoadMore);
        btnSubHeaderLoadMore = itemView.findViewById(R.id.btnSubHeaderLoadMore);
        btnRetry.setOnClickListener(view -> onRetry());

    }

    private void onRetry() {
        if (!isLoading) {
            loadMoreProgressBar.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.GONE);
            fetchData(true);
        }
    }

    private void onLoadMore() {
        if (isFromHome && isFromParentHome) {
            if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);

                new Handler().postDelayed(() -> {
                    try {
                        TabController.getInstance().setTabClicked(true);
                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mParentId, GujaratiVideoViewHolder.this);
                    } catch (Exception e) {
                    }
                }, 200);
            }
        } else if (isFromHome) {
            if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(mParentId);
            }
        } else if (!isFromHome && !isFromParentHome) {
            if (!isLoading) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                btnRetry.setVisibility(View.GONE);
                fetchData(false);
            }
        }
    }

    public void setData(CategoryInfo categoryInfo, String sectionName) {
        this.showCount = 0;
        this.categoryInfo = categoryInfo;
        this.sectionName = sectionName;
        this.isFromHome = false;
        this.id = this.mParentId = categoryInfo.id;

        dbVideosListAdapter.setDataInAdapter(GujaratiVideosListAdapter.LIST_VERTICAL, AppUtils.getThemeColor(mContext, categoryInfo.color), categoryInfo.id);
        dbVideosRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
        dbVideosRecyclerView.setAdapter(dbVideosListAdapter);

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && pageCount <= 2) {
            parseOfflineData(catHomeData, true);
            pageCount = 2;

        } else {
            pageCount = 1;
            loadMoreProgressBar.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.GONE);
            fetchData(true);
        }

    }

    public void setData(CategoryInfo categoryInfo, int showCount, boolean isFromHome, boolean isFromParentHome, String pParentId, String mSuperParentId, String sectionName) {
        this.categoryInfo = categoryInfo;
        this.showCount = showCount;
        this.sectionName = sectionName;

        this.isFromHome = isFromHome;
        this.isFromParentHome = isFromParentHome;
        this.mParentId = pParentId;
        this.id = categoryInfo.id;

        dbVideosListAdapter.setDataInAdapter(GujaratiVideosListAdapter.LIST_HORIZONTAL, AppUtils.getThemeColor(mContext, categoryInfo.color), categoryInfo.id);
        dbVideosRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        dbVideosRecyclerView.setAdapter(dbVideosListAdapter);

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && pageCount <= 2) {
            parseOfflineData(catHomeData, true);
            pageCount = 2;
        } else {
            pageCount = 1;
            loadMoreProgressBar.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.GONE);
            fetchData(true);
        }
    }

    public void clearDataAndLoadNew() {
        dbVideosListAdapter.clear();
    }

    private void fetchData(boolean isClear) {
        makeJsonObjectRequest(Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + pageCount + "/", isClear);
    }

    private void makeJsonObjectRequest(String finalFeedURL, final boolean isClear) {
        Systr.println("Feed Url : " + finalFeedURL);
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            isLoading = true;
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            if (response != null) {
                                parseFeedList(response, isClear);
                            }
                        } catch (Exception e) {
                        }

                        isLoading = false;
                        loadMoreProgressBar.setVisibility(View.GONE);
                    }, error -> {
                if (error instanceof NoConnectionError || error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }

                loadMoreProgressBar.setVisibility(View.GONE);
                isLoading = false;

                if (dbVideosListAdapter.getItemCount() == 0) {
                    btnRetry.setVisibility(View.VISIBLE);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            if (!isFromHome && !isFromParentHome)
                VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
            else
                VolleyNetworkSingleton.getInstance(mContext, 2).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
            loadMoreProgressBar.setVisibility(View.GONE);
            if (dbVideosListAdapter.getItemCount() == 0) {
                btnRetry.setVisibility(View.VISIBLE);
            }
        }
    }

    private void parseFeedList(JSONObject response, boolean isClear) throws JSONException {
        int count = response.optInt("count");
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        if (resultArray != null) {
            ArrayList<VideoInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), VideoInfo[].class)));

            if (pageCount <= 1 && !TextUtils.isEmpty(id)) {
                CatHomeData catHomeData = new CatHomeData(list, count, CatHomeData.TYPE_VIDEOS, System.currentTimeMillis());
                DataParser.getInstance().addCatHomeDataHashMap(id, catHomeData);
            }

            if (resultArray.length() < count) {
                isLoadMoreEnabled = false;
            } else {
                isLoadMoreEnabled = true;
                pageCount++;
            }

            handleUI(list, isClear);
        }
    }

    private void parseOfflineData(CatHomeData catHomeData, boolean isClear) {
        int count = catHomeData.count;
        ArrayList<VideoInfo> list = (ArrayList<VideoInfo>) catHomeData.getArrayList(CatHomeData.TYPE_VIDEOS);
        if (list != null) {
            isLoadMoreEnabled = list.size() >= count;
            handleUI(list, isClear);
        }
    }

    private void handleUI(ArrayList<VideoInfo> list, boolean isClear) {

        //passing the data to the adapter to show.
        if (showCount > 0 && showCount < list.size()) {
            ArrayList<VideoInfo> localList = new ArrayList<>();
            for (int i = 0; i < showCount; i++) {
                localList.add(list.get(i));
            }
            if (isClear) {
                dbVideosListAdapter.setDataAndClear(localList);
            } else {
                dbVideosListAdapter.setData(localList);
            }

        } else {
            if (isClear) {
                dbVideosListAdapter.setDataAndClear(list);
            } else {
                dbVideosListAdapter.setData(list);
            }
        }

        loadMoreLogic();
    }

    private void loadMoreLogic() {
        if (isFromParentHome) {
            btnHeaderLoadMore.setVisibility(View.VISIBLE);
            btnSubHeaderLoadMore.setVisibility(View.GONE);
        } else if (isFromHome) {
            btnSubHeaderLoadMore.setVisibility(View.VISIBLE);
            btnHeaderLoadMore.setVisibility(View.GONE);

        } else {
            //infinite loading true
            btnSubHeaderLoadMore.setVisibility(View.GONE);
            btnHeaderLoadMore.setVisibility(View.GONE);
        }

        btnHeaderLoadMore.setOnClickListener(v -> onLoadMore());
        btnSubHeaderLoadMore.setOnClickListener(v -> onLoadMore());
    }


    @Override
    public void onDbVideoClick(int position) {
        String sectionLabel = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME) + (TextUtils.isEmpty(sectionName) ? "_" : ("_" + sectionName + "_")) + categoryInfo.displayName;

        Intent intent = DBVideoPlayerActivity2.getIntent(mContext, dbVideosListAdapter.getItem(position), sectionLabel, AppFlyerConst.DBVideosSource.VIDEO_LIST, position + 1, categoryInfo.detailUrl, categoryInfo.feedUrl, categoryInfo.gaEventLabel, categoryInfo.gaArticle, false, "", categoryInfo.color);
        mContext.startActivity(intent);
    }

    @Override
    public void onLoadFinish() {
        loadMoreProgressBar.setVisibility(View.GONE);
    }

    public void setIsRefreshing(boolean isRefreshing) {
    }
}
