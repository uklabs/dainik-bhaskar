package com.db.divya_new.gujarati;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.bhaskar.util.AppUrls;
import com.db.data.models.CategoryInfo;
import com.db.database.DatabaseClient;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.VideoPlayerActivity;
import com.db.dbvideoPersonalized.detail.VideoPlayerListFragment;
import com.db.divya_new.data.CatHome2;
import com.db.listeners.OnDBVideoClickListener;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.QuickPreferences;

import java.util.ArrayList;

public class GujaratiVideoViewHolder2 extends RecyclerView.ViewHolder implements OnDBVideoClickListener {

    private GujaratiVideosListAdapter2 dbVideosListAdapter;
    private RecyclerView dbVideosRecyclerView;
    private Activity mContext;

    private String sectionName;
    private String catID;

    public GujaratiVideoViewHolder2(View itemView, Activity context) {
        super(itemView);
        mContext = context;

        dbVideosListAdapter = new GujaratiVideosListAdapter2(mContext, this);

        dbVideosRecyclerView = itemView.findViewById(R.id.recycler_view);
        dbVideosRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        dbVideosRecyclerView.setAdapter(dbVideosListAdapter);
    }

    public void setData(CatHome2 catHome) {
        this.catID = catHome.id;
        this.sectionName = catHome.name;

        if (TextUtils.isEmpty(catHome.horizontalList) || catHome.horizontalList.equalsIgnoreCase("0")) {
            dbVideosListAdapter.setDataInAdapter(GujaratiVideosListAdapter.LIST_VERTICAL, catHome.color, catHome.id);
            dbVideosRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
        } else {
            dbVideosListAdapter.setDataInAdapter(GujaratiVideosListAdapter.LIST_HORIZONTAL, catHome.color, catHome.id);
            dbVideosRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        }

        dbVideosListAdapter.setDataAndClear(catHome.list);
    }

    @Override
    public void onDbVideoClick(int position) {
        CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(catID);
        String sectionLabel = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME) + (TextUtils.isEmpty(sectionName) ? "_" : ("_" + sectionName + "_")) + categoryInfo.displayName;

//        Intent intent = DBVideoPlayerActivity2.getIntent(mContext, dbVideosListAdapter.getItem(position), sectionLabel, AppFlyerConst.DBVideosSource.VIDEO_LIST, position, categoryInfo.detailUrl, categoryInfo.feedUrl, categoryInfo.gaEventLabel, categoryInfo.gaArticle, false, "", categoryInfo.color);
//        mContext.startActivity(intent);

        VideoInfo videoInfo = dbVideosListAdapter.getItem(position);
        ArrayList<VideoInfo> videoList = new ArrayList<>();
        videoList.add(videoInfo);

//        String feedUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.VideoPref.SETTINGS_VIDEO_FEED_URL_V2, "") + CommonConstants.CHANNEL_ID + "/" + videoInfo.storyId + "/";
        String feedUrl = AppUrls.VIDEO_RECOMMENDATION_URL + CommonConstants.CHANNEL_ID + "/" + videoInfo.storyId + "/";
        Intent intent = VideoPlayerActivity.getIntent(mContext, 0, 1, feedUrl, AppFlyerConst.DBVideosSource.VIDEO_LIST, categoryInfo.gaEventLabel, categoryInfo.gaArticle, sectionLabel, categoryInfo.color, VideoPlayerListFragment.TYPE_LIST, VideoPlayerListFragment.THEME_LIGHT);
        VideoPlayerActivity.addListData(videoList);
        mContext.startActivity(intent);
    }
}
