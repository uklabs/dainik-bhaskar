package com.db.divya_new.gujarati;

import android.content.Context;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.ads.AdPlacementController;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.NativeAdViewHolder2;
import com.db.data.source.server.BackgroundRequest;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.divya_new.MoreOptionInterface;
import com.db.listeners.OnDBVideoClickListener;
import com.db.listeners.OnItemClickListener;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.ShareUtil;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class GujaratiVideosListAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements NativeListAdController2.onAdNotifyListener {

    private static final int LIST_HORIZONTAL = 1;
    private static final int LIST_VERTICAL = 2;
    private static final int LIST_VERTICAL_BIG = 3;
    private final int VIEW_TYPE_NATIVE_ADS_HORIZONTAL = 26;

    private Context mContext;
    private ArrayList<Object> dbVideosInfoArrayList;
    private OnDBVideoClickListener onDBVideoClickListener;
    private int mViewType;
    private String mColorCode;
    private String categoryId;

    GujaratiVideosListAdapter2(Context contexts, OnDBVideoClickListener onDBVideoClickListener) {
        this.onDBVideoClickListener = onDBVideoClickListener;
        this.mContext = contexts;
        dbVideosInfoArrayList = new ArrayList<>();
    }

    void setDataInAdapter(int viewType, String colorCode, String catId) {
        mViewType = viewType;
        mColorCode = colorCode;
        categoryId = catId;
        notifyDataSetChanged();
    }

    void setDataAndClear(ArrayList<Object> newsFeed) {
        dbVideosInfoArrayList.clear();
        dbVideosInfoArrayList.addAll(newsFeed);
        AdPlacementController.getInstance().addHomeVideoListingNativeAdsObject(dbVideosInfoArrayList, mContext);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case LIST_HORIZONTAL:
                return new ViewHolder(inflater.inflate(R.layout.list_gujarati_video_horizontal, parent, false));
            case LIST_VERTICAL_BIG:
                return new ViewHolder(inflater.inflate(R.layout.listitem_guruvani_video_item, parent, false));
            case VIEW_TYPE_NATIVE_ADS_HORIZONTAL:
                return new NativeAdViewHolder2(inflater.inflate(R.layout.ad_layout_native_video_horizontal, parent, false));
            default:
                return new ViewHolder(inflater.inflate(R.layout.listitem_guruvani_video_item_small, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof NativeAdViewHolder2) {
            showHorizontalNativeAds(((NativeAdViewHolder2) holder), position);

        } else {
            final ViewHolder mainViewHolder = (ViewHolder) holder;
            final VideoInfo dbVideosInfo = (VideoInfo) dbVideosInfoArrayList.get(mainViewHolder.getAdapterPosition());

//            SpannableStringBuilder builder = new SpannableStringBuilder();
//            if (!TextUtils.isEmpty(dbVideosInfo.titleHead)) {
//                String title = dbVideosInfo.titleHead;
//                SpannableString str1 = new SpannableString(title);
//                str1.setSpan(new ForegroundColorSpan(mColorCode), 0, title.length(), 0);
//                builder.append(str1);
//            }
//            builder.append(dbVideosInfo.title);
//            mainViewHolder.tvDBVideosTitle.setText(builder, TextView.BufferType.SPANNABLE);
            mainViewHolder.tvDBVideosTitle.setText(AppUtils.getInstance().getHomeTitle(dbVideosInfo.titleHead, AppUtils.getInstance().fromHtml(dbVideosInfo.title), AppUtils.getThemeColor(mColorCode)));

            mainViewHolder.playImageView.setColorFilter(AppUtils.getThemeColor(mContext,mColorCode));

            if (TextUtils.isEmpty(dbVideosInfo.duration)) {
                mainViewHolder.tvDbVideosDuration.setVisibility(View.GONE);
            } else {
                mainViewHolder.tvDbVideosDuration.setVisibility(View.VISIBLE);
                mainViewHolder.tvDbVideosDuration.setText(dbVideosInfo.duration);
            }

            AppUtils.getInstance().setImageViewSizeWithAspectRatio(mainViewHolder.ivDBVideosThumbnailImage, Constants.ImageRatios.DBVIDEOS_RATIO, 0, mContext);
            ImageUtil.setImage(mContext, dbVideosInfo.image, mainViewHolder.ivDBVideosThumbnailImage, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);

            mainViewHolder.itemView.setOnClickListener(view -> {
                if (NetworkStatus.getInstance().isConnected(mContext)) {
                    if (onDBVideoClickListener != null)
                        onDBVideoClickListener.onDbVideoClick(mainViewHolder.getAdapterPosition());
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getString(R.string.no_network_error));
                }
            });

            mainViewHolder.moreImg.setOnClickListener(view -> {
                PopupWindow popUpWindow = AppUtils.showMoreOptionsV2(mContext, view, new MoreOptionInterface() {
                    @Override
                    public void onBookMark() {
                    }

                    @Override
                    public void onShare() {
                        BackgroundRequest.getStoryShareUrl(mContext, dbVideosInfo.link, (flag, url) -> ShareUtil.shareDefault(mContext, dbVideosInfo.title, mContext.getResources().getString(R.string.checkout_this_video), url, dbVideosInfo.gTrackUrl, false));
//                    ShareUtil.shareDefault(mContext, dbVideosInfo.title, mContext.getResources().getString(R.string.checkout_this_video), dbVideosInfo.link);
                    }
                });
                popUpWindow.getContentView().findViewById(R.id.tvBookMark).setVisibility(View.GONE);
                popUpWindow.getContentView().findViewById(R.id.separator_v).setVisibility(View.GONE);
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        Object obj = dbVideosInfoArrayList.get(position);
        if (obj instanceof VideoInfo) {
            VideoInfo videoInfo = (VideoInfo) obj;
            if (mViewType == LIST_HORIZONTAL) {
                if (videoInfo.isAd)
                    return VIEW_TYPE_NATIVE_ADS_HORIZONTAL;
                else
                    return LIST_HORIZONTAL;
            } else if (mViewType == LIST_VERTICAL) {
                try {
                    if (videoInfo.bigImage.equalsIgnoreCase("1"))
                        return LIST_VERTICAL_BIG;
                    else
                        return LIST_VERTICAL;
                } catch (Exception e) {
                }
            }
        }
        return LIST_VERTICAL;
    }

    @Override
    public int getItemCount() {
        return dbVideosInfoArrayList == null ? 0 : dbVideosInfoArrayList.size();
    }

    @Override
    public void onNotifyAd() {
        notifyDataSetChanged();
    }

    public VideoInfo getItem(int position) {
        if (position >= 0 && position < dbVideosInfoArrayList.size())
            return (VideoInfo) dbVideosInfoArrayList.get(position);
        else
            return (VideoInfo) dbVideosInfoArrayList.get(0);
    }

    public void clear() {
        dbVideosInfoArrayList.clear();
        notifyDataSetChanged();
    }

    private void showHorizontalNativeAds(NativeAdViewHolder2 holder, int pos) {
        VideoInfo adInfo = (VideoInfo) dbVideosInfoArrayList.get(pos);
        UnifiedNativeAd ad = NativeListAdController2.getInstance(mContext).getAdForHome(adInfo.adUnit, adInfo.adType, pos, categoryId, new WeakReference<NativeListAdController2.onAdNotifyListener>(this));
        holder.populateNativeAdView(ad);
    }

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private ImageView ivDBVideosThumbnailImage, moreImg, playImageView;
        private TextView tvDBVideosTitle, tvDbVideosDuration;

        private OnItemClickListener clickListener;

        private ViewHolder(View itemView) {
            super(itemView);
            ivDBVideosThumbnailImage = itemView.findViewById(R.id.iv_pic);
            tvDBVideosTitle = itemView.findViewById(R.id.tv_title);
            tvDbVideosDuration = itemView.findViewById(R.id.tv_duration);
            moreImg = itemView.findViewById(R.id.more_img);
            playImageView = itemView.findViewById(R.id.video_play_icon);
            itemView.setTag(itemView);
        }

        private void setClickListener(OnItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
            return true;
        }
    }
}