package com.db.divya_new.gujarati;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.util.LoginController;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.VideoPlayerActivity;
import com.db.dbvideoPersonalized.detail.VideoPlayerListFragment;
import com.db.divya_new.data.VideoSectionInfo;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.db.views.viewholder.LoadingViewHolder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class VideoSectionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView recyclerView;
    private ViewPager mViewPager;
    private ProgressBar progressBar;
    private RelativeLayout flipperLayout;

    private GridLayoutManager mLayoutManager;
    private SpacesDecoration spacesItemDecoration;

    private TopPagerAdapter topAdapter;
    private PlayListAdapter adapter;

    private ArrayList<VideoInfo> topInfoArrayList;
    private ArrayList<VideoSectionInfo> sectionInfoArrayList;

    private CategoryInfo categoryInfo;
    private String sectionName;

    private String topCategoryName;

    private final int INTERVAL = 3500;
    private Handler myHandler = new Handler();
    private Runnable flipController = this::startTimer;
    private Timer timer;
    private ImageView ivPrev;
    private ImageView ivNext;

    private void startTimer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (isAdded() && getActivity() != null)
                    getActivity().runOnUiThread(() -> {
                        if (mViewPager.getCurrentItem() < topInfoArrayList.size() - 1)
                            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
                        else
                            mViewPager.setCurrentItem(0);
                    });
            }
        }, INTERVAL, INTERVAL);
    }

    public static VideoSectionFragment newInstance(String sectionName, CategoryInfo categoryInfo) {
        VideoSectionFragment fragment = new VideoSectionFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString(Constants.KeyPair.KEY_SECTION_LABEL, sectionName);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            sectionName = bundle.getString(Constants.KeyPair.KEY_SECTION_LABEL);
        }

        topInfoArrayList = new ArrayList<>();
        sectionInfoArrayList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_section, container, false);

        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_spacing);
        spacesItemDecoration = new SpacesDecoration(spacingInPixels, spacingInPixels, spacingInPixels, spacingInPixels);
        progressBar = view.findViewById(R.id.progressBar);
        flipperLayout = view.findViewById(R.id.flipper_layout);
        recyclerView = view.findViewById(R.id.recycler_view);
        mViewPager = view.findViewById(R.id.view_pager);

        ivPrev = view.findViewById(R.id.ivPrev);
        ivNext = view.findViewById(R.id.ivNext);
        ivNext.setOnClickListener(v -> next());

        ivPrev.setOnClickListener(v -> prev());

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                ivPrev.setVisibility(View.VISIBLE);
                ivNext.setVisibility(View.VISIBLE);
                if (position == 0) {
                    ivPrev.setVisibility(View.INVISIBLE);
                }
                if (position == topInfoArrayList.size() - 1) {
                    ivNext.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        fetchDataFromServer();

        return view;
    }

    private synchronized void next() {
        myHandler.removeCallbacks(flipController);
        cancelTimer();

        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
        myHandler.postDelayed(flipController, INTERVAL);
    }

    private synchronized void prev() {
        myHandler.removeCallbacks(flipController);
        cancelTimer();

        mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
        myHandler.postDelayed(flipController, INTERVAL);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        topAdapter = new TopPagerAdapter();
        adapter = new PlayListAdapter();

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        mLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.removeItemDecoration(spacesItemDecoration);
        recyclerView.addItemDecoration(spacesItemDecoration);

        mViewPager.setAdapter(topAdapter);
    }

    private synchronized void cancelTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (topInfoArrayList.size() > 0)
            startTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        cancelTimer();
    }

    @Override
    public void onRefresh() {
        fetchDataFromServer();
    }

    private void fetchDataFromServer() {
        String recommendedUrl = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl;
//        String recommendedUrl = "https://appfeedlight.bhaskar.com/appFeedV3/MoreVideoGallery/960/";
        makeJsonObjectRequest(recommendedUrl);
    }

    private void makeJsonObjectRequest(String recommendedUrl) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, recommendedUrl, null,
                response -> {
                    try {
                        if (response != null && response.length() > 0) {
                            parseFeedList(response.getJSONObject("feed"));
                        }
                    } catch (Exception ignore) {
                    }

                    if (mSwipeRefreshLayout.isRefreshing()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    progressBar.setVisibility(View.GONE);
                },

                error -> {
                    Activity activity = getActivity();
                    if (activity != null && isAdded()) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        progressBar.setVisibility(View.GONE);

                        if (error instanceof NetworkError) {
                            showToast(activity, activity.getResources().getString(R.string.internet_connection_error_));
                        } else {
                            showToast(activity, activity.getResources().getString(R.string.sorry_error_found_please_try_again_));
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
    }

    private void parseFeedList(JSONObject jsonObject) throws JSONException {
        Gson gson = new Gson();

        JSONObject topJsonObject = jsonObject.getJSONObject("top_list");
        topCategoryName = topJsonObject.optString("category_name");
        topInfoArrayList = new ArrayList<>(Arrays.asList(gson.fromJson(topJsonObject.getJSONArray("data").toString(), VideoInfo[].class)));
        topAdapter.notifyDataSetChanged();
        if (topInfoArrayList.isEmpty()) {
            ivPrev.setVisibility(View.GONE);
            ivNext.setVisibility(View.GONE);
        } else {
            startTimer();
        }

        flipperLayout.setVisibility(View.VISIBLE);
        JSONArray sectionJsonOArray = jsonObject.getJSONArray("section_list");
        sectionInfoArrayList = new ArrayList<>(Arrays.asList(gson.fromJson(sectionJsonOArray.toString(), VideoSectionInfo[].class)));
        adapter.notifyDataSetChanged();
    }

    private class PlayListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View gridView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_gujarati_video_list_grid, parent, false);
            return new VideoViewHolder(gridView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof VideoViewHolder) {
                final VideoViewHolder viewHolder = (VideoViewHolder) holder;

                final VideoSectionInfo info = sectionInfoArrayList.get(position);
                viewHolder.titleTextView.setText(info.categoryName);

                viewHolder.playImageView.setColorFilter(AppUtils.getThemeColor(getContext(), categoryInfo.color));

                if (info.videoInfo != null) {
                    ImageUtil.setImage(getContext(), info.videoInfo.squareImageUrl, viewHolder.iconImageView, R.drawable.water_mark_news_list);

                    viewHolder.itemView.setOnClickListener(view -> {
                        Context context = getContext();
                        if (context != null) {
                            String sectionLabel = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME) + (TextUtils.isEmpty(sectionName) ? "_" : ("_" + sectionName + "_")) + categoryInfo.displayName;

                            if (info.videoInfo != null) {
                                ArrayList<VideoInfo> list = new ArrayList<>();
                                list.add(info.videoInfo);
                                Intent intent = VideoPlayerActivity.getIntent(getContext(), 1, 1, info.feedUrl, AppFlyerConst.DBVideosSource.VIDEO_LIST, categoryInfo.gaEventLabel, categoryInfo.gaArticle, sectionLabel, categoryInfo.color, VideoPlayerListFragment.TYPE_LIST, VideoPlayerListFragment.THEME_LIGHT);
                                VideoPlayerActivity.addListData(list);
                                getContext().startActivity(intent);

                                sendTrackingForClick(info.categoryName);
                            }
                        }
                    });
                }

            } else if (holder instanceof LoadingViewHolder) {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return sectionInfoArrayList == null ? 0 : sectionInfoArrayList.size();
        }
    }

    private class VideoViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        ImageView iconImageView;
        ImageView playImageView;

        VideoViewHolder(View view) {
            super(view);

            iconImageView = view.findViewById(R.id.iv_video_icon);
            playImageView = view.findViewById(R.id.video_play_icon);
            titleTextView = view.findViewById(R.id.tv_title);
        }
    }

    private void showToast(Context context, String msg) {
        AppUtils.getInstance().showCustomToast(context, msg);
    }

    class TopPagerAdapter extends PagerAdapter {

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View itemView = LayoutInflater.from(getContext()).inflate(R.layout.layout_gujarati_video_section_list, container, false);

            VideoInfo info = topInfoArrayList.get(position);

            ImageView imageView = itemView.findViewById(R.id.iv_video_icon);
            ImageUtil.setImage(getContext(), info.squareImageUrl, imageView, R.drawable.water_mark_news_list);

            ImageView playIconImageView = itemView.findViewById(R.id.video_play_icon);
            playIconImageView.setColorFilter(AppUtils.getThemeColor(getContext(), categoryInfo.color));

            TextView titleTextView = itemView.findViewById(R.id.tv_title);
            titleTextView.setText(topCategoryName);

            TextView descTextView = itemView.findViewById(R.id.tv_desc);
            descTextView.setText(AppUtils.getInstance().getHomeTitle(info.titleHead, AppUtils.getInstance().fromHtml(info.title), categoryInfo.color));

            itemView.setOnClickListener(view -> {
                Context context = getContext();
                if (context != null) {
                    String sectionLabel = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME) + (TextUtils.isEmpty(sectionName) ? "_" : ("_" + sectionName + "_")) + categoryInfo.displayName;

                    Intent intent = VideoPlayerActivity.getIntent(getContext(), position, 2, "", AppFlyerConst.DBVideosSource.VIDEO_LIST, categoryInfo.gaEventLabel, categoryInfo.gaArticle, sectionLabel, categoryInfo.color, VideoPlayerListFragment.TYPE_LIST, VideoPlayerListFragment.THEME_LIGHT);
                    VideoPlayerActivity.addListData(topInfoArrayList);
                    getContext().startActivity(intent);

                    sendTrackingForClick(topCategoryName);
                }
            });

            container.addView(itemView);

            return itemView;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return topInfoArrayList == null ? 0 : topInfoArrayList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            Animation animZoomIn = AnimationUtils.loadAnimation(getContext(), R.anim.zoom_in_current);
            view.findViewById(R.id.iv_video_icon).startAnimation(animZoomIn);

            return view == object;
        }
    }

    private void sendTrackingForClick(String categoryName) {
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.VIDEO_GALLERY, AppFlyerConst.GAAction.CLICK, categoryName, campaign);

        CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.VIDEO_GALLERY_CLICKS, CTConstant.PROP_NAME.CLICKON, categoryName, LoginController.getUserDataMap());
    }
}
