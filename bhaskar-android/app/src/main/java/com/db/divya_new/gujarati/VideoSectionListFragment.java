package com.db.divya_new.gujarati;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.util.LoginController;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.VideoPlayerActivity;
import com.db.dbvideoPersonalized.detail.VideoPlayerListFragment;
import com.db.divya_new.data.VideoSectionInfo;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.views.viewholder.LoadingViewHolder;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class VideoSectionListFragment extends Fragment {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    private GridLayoutManager mLayoutManager;
    private SpacesDecoration spacesItemDecoration;

    private PlayListAdapter adapter;

    private ArrayList<VideoSectionInfo> mList;

    private CategoryInfo categoryInfo;
    private String sectionName;


    private final int INTERVAL = 3500;
    private int LOAD_MORE = 1;

    private int totalItemCount;
    private int lastVisibleItem;
    private int visibleItemCount;
    private int count;
    private boolean isLoading;
    private int deviceWidth;


    public static VideoSectionListFragment newInstance(String sectionName, CategoryInfo categoryInfo) {
        VideoSectionListFragment fragment = new VideoSectionListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString(Constants.KeyPair.KEY_SECTION_LABEL, sectionName);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            sectionName = bundle.getString(Constants.KeyPair.KEY_SECTION_LABEL);
        }
        mList = new ArrayList<>();

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay()
                .getMetrics(displaymetrics);
        deviceWidth = AppUtils.getDeviceWidthAndHeight(getActivity())[0];
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_section_list, container, false);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            LOAD_MORE = 1;
            mList.clear();
            makeJsonObjectRequest();
        });
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_spacing);
        spacesItemDecoration = new SpacesDecoration(spacingInPixels, spacingInPixels, spacingInPixels, spacingInPixels);
        progressBar = view.findViewById(R.id.progressBar);
        recyclerView = view.findViewById(R.id.recycler_view);
        adapter = new PlayListAdapter();
        mLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(spacesItemDecoration);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        setInfiniteScroll();

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        makeJsonObjectRequest();
    }

    private void setInfiniteScroll() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + lastVisibleItem) >= totalItemCount && !isLoading) {
                    //bottom of recyclerview
                    LOAD_MORE++;
                    makeJsonObjectRequest();
//                    Toast.makeText(getContext(),"Loadmore "+LOAD_MORE,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void makeJsonObjectRequest() {
        isLoading = true;
        String finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + LOAD_MORE + "/";
        Systr.println(String.format(" Feed API %s ", finalFeedURL));
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalFeedURL, null,
                response -> {
                    mSwipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);
                    parseFeedList(response);
                },
                error -> {
                    isLoading = false;
                    Activity activity = getActivity();
                    if (activity != null && isAdded()) {
                        if (mSwipeRefreshLayout.isRefreshing()) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        progressBar.setVisibility(View.GONE);

                        if (error instanceof NetworkError) {
                            showToast(activity, activity.getResources().getString(R.string.internet_connection_error_));
                        } else {
                            showToast(activity, activity.getResources().getString(R.string.sorry_error_found_please_try_again_));
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
    }

    private void parseFeedList(JSONObject jsonObject) {
        JSONObject feedJSONObj = jsonObject.optJSONObject("feed");
        JSONArray sectionJsonOArray = feedJSONObj.optJSONArray("section_list");
        if (sectionJsonOArray.length() > 0) {
            mList.addAll(Arrays.asList(new Gson().fromJson(sectionJsonOArray.toString(), VideoSectionInfo[].class)));
            adapter.notifyDataSetChanged();
            isLoading = false;
        }
    }

    private class PlayListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View gridView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_gujarati_video_list_grid, parent, false);
            return new VideoViewHolder(gridView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof VideoViewHolder) {
                final VideoViewHolder viewHolder = (VideoViewHolder) holder;
                final VideoSectionInfo info = mList.get(position);
                ImageUtil.setImage(getContext(), info.videoInfo.squareImageUrl, viewHolder.iconImageView, R.drawable.water_mark_news_list);
                viewHolder.titleTextView.setText(info.categoryName);
                viewHolder.playImageView.setColorFilter(AppUtils.getThemeColor(getContext(), categoryInfo.color));
                viewHolder.itemView.setOnClickListener(view -> {
                    Context context = getContext();
                    if (context != null) {
                        String sectionLabel = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME) + (TextUtils.isEmpty(sectionName) ? "_" : ("_" + sectionName + "_")) + categoryInfo.displayName;
                        ArrayList<VideoInfo> list = new ArrayList<>();
                        list.add(info.videoInfo);
                        Intent intent = VideoPlayerActivity.getIntent(getContext(), 1, 1, info.feedUrl, AppFlyerConst.DBVideosSource.VIDEO_LIST, categoryInfo.gaEventLabel, categoryInfo.gaArticle, sectionLabel, categoryInfo.color, VideoPlayerListFragment.TYPE_LIST, VideoPlayerListFragment.THEME_LIGHT);
                        VideoPlayerActivity.addListData(list);
                        getContext().startActivity(intent);
                        sendTrackingForClick(info.categoryName);
                    }
                });

            } else if (holder instanceof LoadingViewHolder) {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return mList == null ? 0 : mList.size();
        }
    }

    private class VideoViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        ImageView iconImageView;
        ImageView playImageView;
        CardView cardView;

        VideoViewHolder(View view) {
            super(view);
            iconImageView = view.findViewById(R.id.iv_video_icon);
            playImageView = view.findViewById(R.id.video_play_icon);
            titleTextView = view.findViewById(R.id.tv_title);
            cardView = view.findViewById(R.id.rootView);
            makeItSquare(cardView);
        }
    }

    private void makeItSquare(CardView cardView) {
       cardView.getLayoutParams().height = (int) ((deviceWidth/2) - AppUtils.convertDpToPixel(getResources().getDimension(R.dimen.grid_spacing),getContext())*2);
    }

    private void showToast(Context context, String msg) {
        AppUtils.getInstance().showCustomToast(context, msg);
    }

    private void sendTrackingForClick(String categoryName) {
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.VIDEO_GALLERY, AppFlyerConst.GAAction.CLICK, categoryName, campaign);
        CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.VIDEO_GALLERY_CLICKS, CTConstant.PROP_NAME.CLICKON, categoryName, LoginController.getUserDataMap());
    }
}
