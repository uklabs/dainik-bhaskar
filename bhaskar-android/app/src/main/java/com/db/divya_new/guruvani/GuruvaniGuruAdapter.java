package com.db.divya_new.guruvani;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.guruvani.viewholder.GuruvaniGuruViewHolder;
import com.db.homebanner.BannerViewHolder;

import java.util.ArrayList;
import java.util.List;

public class GuruvaniGuruAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_GURU = 1;
    public static final int VIEW_TYPE_BANNER = 3;
    private String mParentId;
    private List<Object> mList;
    private Activity mContext;
    private CategoryInfo categoryInfo;


    private boolean isRefreshing = false;

    public GuruvaniGuruAdapter(Activity context, String id, CategoryInfo categoryInfo) {
        mContext = context;
        this.categoryInfo = categoryInfo;
        mParentId = id;
        this.mList = new ArrayList<>();
    }

    public void addAll(ArrayList<Object> objectList) {
        mList.addAll(objectList);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case VIEW_TYPE_GURU:
                GuruvaniGuruViewHolder guruvaniGuruViewHolder = new GuruvaniGuruViewHolder(mContext, inflater.inflate(R.layout.listitem_recylerview_title_tab, parent, false), mParentId);
                guruvaniGuruViewHolder.setData(categoryInfo);
                return guruvaniGuruViewHolder;
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_blank_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_GURU:
                guruvaniBindHolder((GuruvaniGuruViewHolder) holder);
                break;
            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData((BannerInfo) mList.get(holder.getAdapterPosition()));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mList.get(position);
        if (object instanceof BannerInfo) {
            return VIEW_TYPE_BANNER;
        } else {
            return VIEW_TYPE_GURU;
        }
    }


    /******** Bind Holders Methods ********/

    private void guruvaniBindHolder(final GuruvaniGuruViewHolder viewHolder) {
        if (isRefreshing)
            viewHolder.onSwipeRefresh();
    }


    public void onSwipeRefresh(SwipeRefreshLayout swipeRefreshLayout) {
        isRefreshing = true;
        notifyDataSetChanged();
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
