package com.db.divya_new.guruvani;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.divya_new.data.GuruInfo;
import com.db.divya_new.data.GuruvaniGuruTabInfo;
import com.db.divya_new.guruvani.list.GuruvaniGuruListAdapter;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GuruvaniGuruFragment extends Fragment implements OnMoveTopListener, View.OnClickListener {

    String mId;
    private CategoryInfo categoryInfo;
    private GuruvaniGuruListAdapter adapter;

    private SwipeRefreshLayout swipeRefreshLayout;
    public TextView videoTabView;
    public TextView subakyaTabView;
    public TextView programTabView;
    private View dividerView;

    private RecyclerView recyclerView;

    private int tabSelection = 1;
    private int pageCount = 1;
    private boolean isLoading;
    private boolean isLoadMoreEnabled;

    private int totalItemCount, lastVisibleItem;
    private final static int visibleThreshold = 5;

    private List<BannerInfo> bannerInfoList;

    public static GuruvaniGuruFragment newInstance(CategoryInfo categoryInfo, String id) {
        GuruvaniGuruFragment fragment = new GuruvaniGuruFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString(Constants.KeyPair.KEY_ID, id);
        fragment.setArguments(bundle);

        return fragment;
    }

    public GuruvaniGuruFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            mId = bundle.getString(Constants.KeyPair.KEY_ID);
        }

        if (categoryInfo != null) {
            Constants.guruTabInfo = new Gson().fromJson(categoryInfo.extraVlues, GuruvaniGuruTabInfo.class);
            if (Constants.guruTabInfo != null)
                AppPreferences.getInstance(getContext()).setStringValue(QuickPreferences.DivyaNew.VIDEO_REC_URL, Constants.guruTabInfo.videoRecUrl);
        }
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guruvani_guru, container, false);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        videoTabView = view.findViewById(R.id.tv_tab_1);
        subakyaTabView = view.findViewById(R.id.tv_tab_2);
        programTabView = view.findViewById(R.id.tv_tab_3);

        dividerView = view.findViewById(R.id.tab_divider_view);
        dividerView.setVisibility(View.VISIBLE);
        dividerView.setBackgroundColor(AppUtils.getThemeColor(getContext(), categoryInfo.color));

        if (Constants.guruTabInfo != null) {
            videoTabView.setText(Constants.guruTabInfo.videoInfo.menuName);
            subakyaTabView.setText(Constants.guruTabInfo.qutoesInfo.menuName);
            programTabView.setText(Constants.guruTabInfo.scheduleInfo.menuName);
        }

        videoTabView.setOnClickListener(this);
        subakyaTabView.setOnClickListener(this);
        programTabView.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    if (dy > 0) {
                        totalItemCount = gridLayoutManager.getItemCount();
                        lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && isLoadMoreEnabled && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            onLoadMore();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        adapter = new GuruvaniGuruListAdapter(getActivity(), mId);
        adapter.setData(categoryInfo);
        recyclerView.setAdapter(adapter);

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case GuruvaniGuruListAdapter.TYPE_LOADING:
                    case GuruvaniGuruListAdapter.VIEW_TYPE_AD:
                    case GuruvaniGuruListAdapter.VIEW_TYPE_BANNER:
                        return 2;
                    case GuruvaniGuruListAdapter.TYPE_LIST:
                        return 1;
                    default:
                        return 1;
                }
            }
        });

        setTabView(tabSelection);

        fetchData(true);

        return view;
    }

    private void onLoadMore() {
        recyclerView.post(() -> {
            adapter.addItem();
        });

        fetchData(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_tab_1:
                tabSelection = 1;
                break;

            case R.id.tv_tab_2:
                tabSelection = 2;
                break;

            case R.id.tv_tab_3:
                tabSelection = 3;
                break;
        }
        setTabView(tabSelection);
    }

    private void setTabView(int tabSelection) {
        adapter.setTabSelection(tabSelection);

        videoTabView.setBackgroundResource(R.drawable.inner_tab_default_bg_2);
        subakyaTabView.setBackgroundResource(R.drawable.inner_tab_default_bg_2);
        programTabView.setBackgroundResource(R.drawable.inner_tab_default_bg_2);

        switch (tabSelection) {
            case 1:
                videoTabView.setBackgroundResource(R.drawable.inner_tab_selected_bg_2);
                break;

            case 2:
                subakyaTabView.setBackgroundResource(R.drawable.inner_tab_selected_bg_2);
                break;

            case 3:
                programTabView.setBackgroundResource(R.drawable.inner_tab_selected_bg_2);
                break;
        }
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = () -> {
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
        pageCount = 1;
        fetchData(true);
    };

    public void moveToTop() {
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    private void fetchData(boolean isClear) {
        fetchGuruDataFromServer(getActivity(), Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + pageCount + "/", isClear);
    }

    private void fetchGuruDataFromServer(Context mContext, String url, boolean isClear) {
        Systr.println("Guruvani Fetch Url : " + url);

        if (NetworkStatus.getInstance().isConnected(mContext)) {
            isLoading = true;
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,null, response -> {
                isLoading = false;

                if (pageCount > 1)
                    adapter.removeItem();

                parseOnlineData(response, isClear);

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }, error -> {
                isLoading = false;
                if (pageCount > 1)
                    adapter.removeItem();

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            });

            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(request);
        } else {
            isLoading = false;
        }
    }

    private void parseOnlineData(JSONObject jsonObject, boolean isClear) {
        int count = jsonObject.optInt("count");
        JSONArray jsonArray = jsonObject.optJSONArray("Items");
        if (jsonArray != null) {
            ArrayList<Object> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), GuruInfo[].class)));

            if (jsonArray.length() < count) {
                isLoadMoreEnabled = false;
            } else {
                isLoadMoreEnabled = true;
                pageCount++;
            }

            if (isClear) {
                adapter.clear();

                // add banner & ads
                addBanner(list);
                if (AppPreferences.getInstance(getActivity()).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
                    list.add(0, AdController.showATFForAllListing(getActivity(), adapter));
                }
            }

            adapter.add(list);
        }
    }

    private void addBanner(ArrayList<Object> list) {
//        if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
            for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
                if (!bannerInfoList.get(bannerIndex).isAdded)
                    if (Integer.parseInt(bannerInfoList.get(bannerIndex).catPos) > -1) {
                        list.add(0, bannerInfoList.get(bannerIndex));
                        bannerInfoList.get(bannerIndex).isAdded = true;
                    }
            }
//        }
    }
}
