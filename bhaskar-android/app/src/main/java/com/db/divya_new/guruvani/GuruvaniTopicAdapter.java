package com.db.divya_new.guruvani;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.guruvani.viewholder.GuruvaniTopicViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;

import java.util.ArrayList;
import java.util.List;

public class GuruvaniTopicAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {
    private static final int VIEW_TYPE_GURU = 1;
    public static final int VIEW_TYPE_BANNER = 3;
    private final static int VIEW_TYPE_AD = 25;

    private List<Object> mList;
    private Activity mContext;
    private String mParentId;
    private CategoryInfo categoryInfo;

    private boolean isRefreshing = false;

    public GuruvaniTopicAdapter(Activity context, String id, CategoryInfo categoryInfo) {
        mContext = context;
        mParentId = id;
        this.categoryInfo = categoryInfo;
        this.mList = new ArrayList<>();
    }

    public void addAll(ArrayList<Object> objectList) {
        mList.addAll(objectList);
        notifyDataSetChanged();
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;
        notifyItemChanged(0);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case VIEW_TYPE_GURU:
                return new GuruvaniTopicViewHolder(inflater.inflate(R.layout.listitem_recylerview_title_tab, parent, false), mParentId, mContext);
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_blank_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_GURU:
                topicBindHolder((GuruvaniTopicViewHolder) holder);
                break;
            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData(((NewsListInfo) mList.get(holder.getAdapterPosition())).mBannerInfo);
                break;
            case VIEW_TYPE_AD:
//                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
//                adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
                if (((AdInfo) mList.get(position)).catType == 1) {
                    if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                        adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);

                    } else {
                        ImageView imageView = new ImageView(mContext);
                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setAdjustViewBounds(true);
                        imageView.setClickable(true);
                        String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                        ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                        adViewHolder.addAdsUrl(imageView);

                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AppUtils.getInstance().clickOnAdBanner(mContext);
                            }
                        });
                    }
                } else {
                    adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mList.get(position);
        if (object instanceof AdInfo) {
            return VIEW_TYPE_AD;
        } else if (object instanceof NewsListInfo) {
            NewsListInfo newsListInfo = (NewsListInfo) object;
            if (newsListInfo.type == GuruvaniGuruAdapter.VIEW_TYPE_BANNER) {
                return VIEW_TYPE_BANNER;
            }
        }

        return VIEW_TYPE_GURU;
    }


    private void topicBindHolder(GuruvaniTopicViewHolder viewHolder) {
        viewHolder.setData(categoryInfo, isRefreshing);
    }


    public void onSwipeRefresh(SwipeRefreshLayout swipeRefreshLayout) {
        isRefreshing = true;
        notifyDataSetChanged();
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
