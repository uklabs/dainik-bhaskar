package com.db.divya_new.guruvani;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.DividerItemDecoration;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GuruvaniTopicFragment extends Fragment implements OnMoveTopListener {

    private String mId;
    private CategoryInfo categoryInfo;
    private GuruvaniTopicAdapter adapter;

    private SwipeRefreshLayout swipeRefreshLayout;
    private List<BannerInfo> bannerInfoList;

    public static GuruvaniTopicFragment newInstance(CategoryInfo categoryInfo, String id) {
        GuruvaniTopicFragment fragment = new GuruvaniTopicFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString(Constants.KeyPair.KEY_ID, id);
        fragment.setArguments(bundle);

        return fragment;
    }

    public GuruvaniTopicFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            mId = bundle.getString(Constants.KeyPair.KEY_ID);
        }

        if (categoryInfo != null) {
            try {
                JSONObject jsonObject = new JSONObject(categoryInfo.extraVlues);
                String videoRecUrl = jsonObject.getString("video_rec_url");

                AppPreferences.getInstance(getContext()).setStringValue(QuickPreferences.DivyaNew.VIDEO_REC_URL, videoRecUrl);
            } catch (Exception e) {
            }
        }
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_common, container, false);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        adapter = new GuruvaniTopicAdapter(getActivity(), mId, categoryInfo);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        recyclerView.setAdapter(adapter);

        ArrayList<Object> list = new ArrayList<>();
        list.add(Constants.ViewHolderType.VIEW_HOLDER_STORY);

        // add banner & ads
        addBanner(list);
        if (AppPreferences.getInstance(getActivity()).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
            list.add(0, AdController.showATFForAllListing(getActivity(), adapter));
        }

        adapter.addAll(list);

        return view;
    }

    private void addBanner(ArrayList<Object> list) {
//        if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
            for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
                if (!bannerInfoList.get(bannerIndex).isAdded)
                    if (Integer.parseInt(bannerInfoList.get(bannerIndex).catPos) > -1) {
                        list.add(0, new NewsListInfo(GuruvaniGuruAdapter.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)));
                        bannerInfoList.get(bannerIndex).isAdded = true;
                    }
            }
//        }
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
            adapter.onSwipeRefresh(swipeRefreshLayout);
        }
    };

    public void moveToTop() {
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }
}
