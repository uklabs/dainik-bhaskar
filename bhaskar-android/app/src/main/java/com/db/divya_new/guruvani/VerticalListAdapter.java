package com.db.divya_new.guruvani;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.R;

import java.util.ArrayList;

public class VerticalListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> mList;

    public VerticalListAdapter() {
        mList = new ArrayList<>();
    }

    public void add(ArrayList<String> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_video_list_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            itemBindHolder((ItemViewHolder) holder);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    /******** Bind Holders Methods ********/

    private void itemBindHolder(ItemViewHolder viewHolder) {

    }

    /******** View Holders Classes ********/

    class ItemViewHolder extends RecyclerView.ViewHolder {

        public ItemViewHolder(View itemView) {
            super(itemView);
        }
    }
}
