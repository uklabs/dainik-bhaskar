package com.db.divya_new.guruvani.article;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.data.GuruInfo;
import com.db.main.BaseAppCompatActivity;
import com.db.util.Constants;
import com.db.util.DividerItemDecoration;
import com.db.util.ThemeUtil;

/**
 * intent params:
 * EXTRA_GURU_INFO - guru info object
 * EXTRA_GURU_TAB_SELECTION - tab selection
 */
public class GuruvaniArticleActivity extends BaseAppCompatActivity {

    public static final String EXTRA_GURU_INFO = "guruInfo";
    public static final String EXTRA_GURU_TAB_SELECTION = "tabSelection";
    public static final String EXTRA_PARENT_ID = "ParentId";
    public static final String EXTRA_CATEGORY_INFO = "categoryInfo";

    private GuruvaniArticleAdapter adapter;
    private GuruInfo guruInfo;
    private int tabSelection;
    private String mParentId;
    private CategoryInfo categoryInfo;

    private BroadcastReceiver receiverToCloseActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equalsIgnoreCase(Constants.IntentFilterType.FILTER_FOR_ACTIVITY)) {
                String id = intent.getStringExtra(Constants.IntentExtra.ID);
                if (!TextUtils.isEmpty(id) && this != null) {
                    GuruvaniArticleActivity.this.finish();
                }
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_guruvani_article);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> GuruvaniArticleActivity.this.finish());

        Intent intent = getIntent();
        if (intent != null) {
            categoryInfo = (CategoryInfo) intent.getSerializableExtra(EXTRA_CATEGORY_INFO);
            guruInfo = (GuruInfo) intent.getSerializableExtra(EXTRA_GURU_INFO);
            tabSelection = intent.getIntExtra(EXTRA_GURU_TAB_SELECTION, 1);
            mParentId = intent.getStringExtra(EXTRA_PARENT_ID);
        }
        TextView tvTitle = findViewById(R.id.toolbar_title);
        tvTitle.setTextColor(Color.BLACK);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ica_back_black);
            actionBar.setTitle(null);

            if (guruInfo != null) {
                tvTitle.setText(guruInfo.title);
            } else {
                tvTitle.setText(getString(R.string.guruvani_txt));
            }
        }

        adapter = new GuruvaniArticleAdapter(this, 1, tabSelection, guruInfo, mParentId, categoryInfo);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiverToCloseActivity);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        LocalBroadcastManager.getInstance(this).registerReceiver(receiverToCloseActivity, new IntentFilter(Constants.IntentFilterType.FILTER_FOR_ACTIVITY));

    }
}
