package com.db.divya_new.guruvani.article;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.data.GuruInfo;
import com.db.divya_new.guruvani.viewholder.GuruvaniArticleViewHolder;

public class GuruvaniArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mContext;
    private int mTabSelection = 1;
    private GuruInfo mGuruInfo;
    private CategoryInfo categoryInfo;
    private int mListCount;


    public GuruvaniArticleAdapter(Activity context, int listCount, int tabSelection, GuruInfo guruInfo, String id, CategoryInfo categoryInfo) {
        mContext = context;
        mTabSelection = tabSelection;
        mGuruInfo = guruInfo;
        mListCount = listCount;
        this.categoryInfo = categoryInfo;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new GuruvaniArticleViewHolder(inflater.inflate(R.layout.listitem_guruvani_article_header, parent, false), mTabSelection, mGuruInfo, mContext, categoryInfo);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof GuruvaniArticleViewHolder) {
            headerBindHolder((GuruvaniArticleViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mListCount;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /******** Bind Holders Methods ********/

    private void headerBindHolder(GuruvaniArticleViewHolder viewHolder, int position) {

    }

    /******** View Holders Classes ********/
}
