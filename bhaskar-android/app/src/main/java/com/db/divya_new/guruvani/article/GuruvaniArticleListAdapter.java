package com.db.divya_new.guruvani.article;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.guruvani.viewholder.sub.ProgramViewHolder;
import com.db.divya_new.guruvani.viewholder.sub.QuotesViewHolder;
import com.db.divya_new.guruvani.viewholder.sub.VideoViewHolder;
import com.db.util.Constants;

public class GuruvaniArticleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mContext;
    //    private ArrayList<Object> mList;
    private int mType = 1;
    private String mGuruId;
    private String mGuruName;
    private String mGaScreen;
    private String mGaArticle;
    private String mGaEventLabel;
    private String mSectionLabel;
    private String mColorCode;

    public GuruvaniArticleListAdapter(Activity context, String guruId, String guruName, String gaScreen, String gaArticle, String gaEventLabel, String sectionLabel, String colorCode) {
//        mList = new ArrayList<>();
        mContext = context;
        mGuruId = guruId;
        mGuruName = guruName;
        mGaScreen = gaScreen;
        mGaArticle = gaArticle;
        mGaEventLabel = gaEventLabel;
        mSectionLabel = sectionLabel;
        mColorCode = colorCode;
    }

//    public void add(Object object, int action) {
//        mList.add(object);
//        mType = action;
//        notifyDataSetChanged();
//    }

    public void add(int type) {
        mType = type;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 1:
                String videoFeedUrl = Constants.guruTabInfo != null ? Constants.guruTabInfo.videoInfo.feedUrl : "";
                String videoDisplayName = Constants.guruTabInfo != null ? Constants.guruTabInfo.videoInfo.displayName : "";
                return new VideoViewHolder(inflater.inflate(R.layout.listitem_guruvani_article_tab_video, parent, false), videoFeedUrl, mGuruId, mGuruName, VideoViewHolder.TYPE_GURU, mGaScreen, mGaArticle, mGaEventLabel, mSectionLabel, mColorCode);

            case 2:
                String quoteFeedUrl = Constants.guruTabInfo != null ? Constants.guruTabInfo.qutoesInfo.feedUrl : "";
                String quoteDisplayName = Constants.guruTabInfo != null ? Constants.guruTabInfo.qutoesInfo.displayName : "";
                return new QuotesViewHolder(inflater.inflate(R.layout.listitem_guruvani_article_tab_article, parent, false), quoteFeedUrl, quoteDisplayName,mGuruId, mGuruName, mGaScreen, mContext);

            case 3:
                String programFeedUrl = Constants.guruTabInfo != null ? Constants.guruTabInfo.scheduleInfo.feedUrl : "";
                return new ProgramViewHolder(inflater.inflate(R.layout.listitem_guruvani_article_tab_program, parent, false), programFeedUrl, mGuruId);

            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VideoViewHolder) {
            videoBindHolder((VideoViewHolder) holder, position);
        } else if (holder instanceof QuotesViewHolder) {
            quotesBindHolder((QuotesViewHolder) holder, position);
        } else if (holder instanceof ProgramViewHolder) {
            programBindHolder((ProgramViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return mType;
    }

    /******** Bind Holders Methods ********/

    private void videoBindHolder(VideoViewHolder viewHolder, int position) {

    }

    private void quotesBindHolder(QuotesViewHolder viewHolder, int position) {

    }

    private void programBindHolder(ProgramViewHolder viewHolder, int position) {

    }
}