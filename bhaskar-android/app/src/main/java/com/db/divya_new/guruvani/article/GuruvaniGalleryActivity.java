package com.db.divya_new.guruvani.article;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.divya_new.data.QuotesInfo;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.util.sensor.CustomViewPager;
import com.bhaskar.appscommon.tracking.Tracking;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.QuickPreferences;
//import com.db.util.animation.TabletTransformer;

import java.util.ArrayList;

public class GuruvaniGalleryActivity extends BaseAppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_LIST_INFO = "list_info";
    public static final String EXTRA_POSITION = "position";
    public static final String EXTRA_TRACK_VALUE = "track_value";

    private ArrayList<QuotesInfo> galleryList;
    private CustomViewPager galleryVierPager;
    private PhotoPagerAdapter photoPagerAdapter;

    private int selectedPosition;
    private String trackValue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_guruvani_gallery);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        Intent intent = getIntent();
        if (intent != null) {
            selectedPosition = intent.getIntExtra(EXTRA_POSITION, 0);
            galleryList = (ArrayList<QuotesInfo>) intent.getSerializableExtra(EXTRA_LIST_INFO);
            trackValue = intent.getStringExtra(EXTRA_TRACK_VALUE);
        }

        galleryVierPager = findViewById(R.id.viewpager);
        galleryVierPager.setPageTransformer(false, AppUtils.getParallaxPagerTransform(R.id.iv_photo_touch_image, 0, 0.5f));
//        galleryVierPager.setPageTransformer(false, new TabletTransformer());
        galleryVierPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedPosition = position;

                findViewById(R.id.left_swipe_arrow).setVisibility(View.VISIBLE);
                findViewById(R.id.right_swipe_arrow).setVisibility(View.VISIBLE);
                if (position == 0) {
                    findViewById(R.id.left_swipe_arrow).setVisibility(View.GONE);
                }
                if (position == galleryList.size() - 1) {
                    findViewById(R.id.right_swipe_arrow).setVisibility(View.GONE);
                }

                sendTracking();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        //left swipe gone at first
        findViewById(R.id.left_swipe_arrow).setVisibility(View.GONE);

        photoPagerAdapter = new PhotoPagerAdapter(getSupportFragmentManager());
        galleryVierPager.setAdapter(photoPagerAdapter);
        photoPagerAdapter.notifyDataSetChanged();
        galleryVierPager.setCurrentItem(selectedPosition);

        if (selectedPosition == 0) {
            sendTracking();
        }

        findViewById(R.id.cross_iv).setOnClickListener(this);
        findViewById(R.id.left_swipe_arrow).setOnClickListener(this);
        findViewById(R.id.right_swipe_arrow).setOnClickListener(this);
    }

    private void sendTracking() {
        String value = trackValue + "-IMAGE_ID_" + galleryList.get(selectedPosition).id;
        String source = AppPreferences.getInstance(GuruvaniGalleryActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(GuruvaniGalleryActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(GuruvaniGalleryActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(GuruvaniGalleryActivity.this, InitApplication.getInstance().getDefaultTracker(), value, source, medium, campaign);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cross_iv:
                finish();
                break;

            case R.id.left_swipe_arrow:
                int lpos = selectedPosition - 1;
                if (lpos >= 0)
                    galleryVierPager.setCurrentItem(lpos);
                break;

            case R.id.right_swipe_arrow:
                int rpos = selectedPosition + 1;
                if (rpos < galleryList.size())
                    galleryVierPager.setCurrentItem(rpos);
                break;
        }
    }

    private class PhotoPagerAdapter extends FragmentStatePagerAdapter {

        private PhotoPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return GuruvaniImageFragment.getInstance(galleryList.get(position).image);
        }

        @Override
        public int getCount() {
            return galleryList.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
}
