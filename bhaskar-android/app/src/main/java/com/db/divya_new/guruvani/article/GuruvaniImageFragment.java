package com.db.divya_new.guruvani.article;


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bhaskar.R;
import com.bhaskar.util.TouchImageView;
import com.bhaskar.util.sensor.GyroscopeObserver;
import com.bhaskar.util.sensor.PanoramaImageView;
import com.db.util.ImageUtil;

public class GuruvaniImageFragment extends Fragment {

    private String imagePath;
    private SensorManager mSensorManager;
    private GyroscopeObserver gyroscopeObserver;

    public static GuruvaniImageFragment getInstance(String imagePath) {
        GuruvaniImageFragment photoGalleryDetailFragment = new GuruvaniImageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("imagePath", imagePath);
        photoGalleryDetailFragment.setArguments(bundle);
        return photoGalleryDetailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            imagePath = bundle.getString("imagePath");
        }

        gyroscopeObserver = new GyroscopeObserver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_guruvani_gallery, null);

        final ViewGroup transitionsContainer = getActivity().findViewById(R.id.transitions_container);

        if (mSensorManager == null) {
            mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        }
        final PanoramaImageView panoramaImageView = (rootView.findViewById(R.id.iv_photo_gallery_image));
        final TouchImageView touchImageView = (rootView.findViewById(R.id.iv_photo_touch_image));

        if (!TextUtils.isEmpty(imagePath)) {
            if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                panoramaImageView.setVisibility(View.VISIBLE);
                touchImageView.setVisibility(View.GONE);
                panoramaImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

                ImageUtil.setImage(getContext(), imagePath, panoramaImageView, R.drawable.water_mark_news_detail);
            } else {
                panoramaImageView.setVisibility(View.GONE);
                touchImageView.setVisibility(View.VISIBLE);

                ImageUtil.setImage(getContext(), imagePath, touchImageView, R.drawable.water_mark_news_detail);
            }
        } else {
            touchImageView.setImageResource(R.drawable.water_mark_news_detail);
        }

        panoramaImageView.setGyroscopeObserver(gyroscopeObserver);

        panoramaImageView.setOnTouchListener(new View.OnTouchListener() {

            boolean mExpanded;
            private GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    mExpanded = !mExpanded;
                    return false;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        TransitionManager.beginDelayedTransition(transitionsContainer, new TransitionSet()
                                .addTransition(new ChangeBounds())
                                .addTransition(new ChangeImageTransform()));
                    }

                    if (!mExpanded) {
                        panoramaImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        ViewGroup.LayoutParams params = panoramaImageView.getLayoutParams();
                        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                        panoramaImageView.setLayoutParams(params);
                        gyroscopeObserver.unregister();

                    } else {
                        ViewGroup.LayoutParams params = panoramaImageView.getLayoutParams();
                        params.height = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.MATCH_PARENT;
                        params.width = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.MATCH_PARENT;
                        panoramaImageView.setLayoutParams(params);
                        gyroscopeObserver.register(getActivity());
                        panoramaImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }
                    return false;
                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

        return rootView;
    }
}
