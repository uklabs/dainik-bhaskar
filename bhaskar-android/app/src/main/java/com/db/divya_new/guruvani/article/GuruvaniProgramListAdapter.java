package com.db.divya_new.guruvani.article;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.divya_new.data.ScheduleInfo;

import java.util.ArrayList;

public class GuruvaniProgramListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ScheduleInfo> mList;
    private Context mContext;

    public GuruvaniProgramListAdapter(Context context) {
        mList = new ArrayList<>();
        mContext = context;
    }

    public void add(ScheduleInfo object) {
        mList.add(object);
        notifyDataSetChanged();
    }

    public void addAndClearList(ArrayList<ScheduleInfo> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void addList(ArrayList<ScheduleInfo> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 1:
                return new ProgramHolder(inflater.inflate(R.layout.listitem_guruvani_program_item, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ProgramHolder) {
            programBindHolder((ProgramHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    /******** Bind Holders Methods ********/

    private void programBindHolder(ProgramHolder viewHolder, final int position) {
        ScheduleInfo info = mList.get(position);

        viewHolder.dateTextView.setText(info.date);
        viewHolder.addressTextView.setText(info.desc);
    }

    /******** View Holders Classes ********/

    class ProgramHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView dateTextView;
        TextView addressTextView;

        public ProgramHolder(View itemView) {
            super(itemView);

            dateTextView = itemView.findViewById(R.id.tv_date);
            addressTextView = itemView.findViewById(R.id.tv_address);
        }

        @Override
        public void onClick(View view) {
        }
    }
}
