package com.db.divya_new.guruvani.article;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.InitApplication;
import com.db.divya_new.data.QuotesInfo;
import com.db.listeners.OnDownloadListener;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.util.ShareUtil;
import com.db.util.Systr;

import java.util.ArrayList;

public class GuruvaniQuotesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<QuotesInfo> mList;
    private Activity mContext;
    private String mGuruName;
    private String mDisplayName;
    private String mGaScreen;
    private String tackValue;

    public GuruvaniQuotesListAdapter(Activity context, String guruName, String displayName, String gaScreen) {
        mList = new ArrayList<>();
        mContext = context;
        mGuruName = guruName;
        mDisplayName = displayName;
        mGaScreen = gaScreen;

        tackValue = mGaScreen + "-" + mGuruName + "-" + mDisplayName;
    }

    public void add(QuotesInfo object) {
        mList.add(object);
        notifyDataSetChanged();
    }

    public void addAndClearList(ArrayList<QuotesInfo> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void addList(ArrayList<QuotesInfo> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 1:
                return new ImageViewHolder(inflater.inflate(R.layout.listitem_guruvani_image_item, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ImageViewHolder) {
            headerBindHolder((ImageViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    /******** Bind Holders Methods ********/

    private void headerBindHolder(ImageViewHolder viewHolder, final int position) {

        final QuotesInfo quotesInfo = mList.get(position);

        float ratio = AppUtils.getInstance().parseImageRatio(quotesInfo.imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
        Systr.println("Image Ratio : " + ratio);
        if (ratio > 0) {
            AppUtils.getInstance().setImageViewSizeWithAspectRatio(viewHolder.picImageView, ratio, 16, mContext);
        }

        ImageUtil.setImage(mContext, quotesInfo.image, viewHolder.picImageView, R.drawable.water_mark_news_detail);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, GuruvaniGalleryActivity.class);
                intent.putExtra(GuruvaniGalleryActivity.EXTRA_LIST_INFO, mList);
                intent.putExtra(GuruvaniGalleryActivity.EXTRA_POSITION, position);
                intent.putExtra(GuruvaniGalleryActivity.EXTRA_TRACK_VALUE, tackValue);
                mContext.startActivity(intent);
            }
        });

        viewHolder.facebookImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ShareUtil.facebookShare(mContext, mGuruName, quotesInfo.image)) {
                    String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                    Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), mDisplayName, AppFlyerConst.GAAction.SHARE, AppFlyerConst.GALabel.FB + "_" + mGuruName, campaign);
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, "Please install Facebook first.");
                }
            }
        });

        viewHolder.whatsappImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareUtil.whatsappShare(mContext, mGuruName, quotesInfo.image);

                String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), mDisplayName, AppFlyerConst.GAAction.SHARE, AppFlyerConst.GALabel.WHATSAPP + "_" + mGuruName, campaign);
            }
        });
        viewHolder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ShareUtil.whatsappShare(mContext, mGuruName, quotesInfo.image);
                ImageUtil.shareImageAfterDownload(mContext, quotesInfo.image, mContext.getString(R.string.app_name));
                String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), mDisplayName, AppFlyerConst.GAAction.SHARE, AppFlyerConst.GALabel.WHATSAPP + "_" + mGuruName, campaign);
            }
        });

        viewHolder.downloadImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.downloadFile(mContext, quotesInfo.image, new OnDownloadListener() {
                    @Override
                    public void onStart() {
                        String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), mDisplayName, AppFlyerConst.GAAction.DOWNLOAD, mGuruName, campaign);
                    }
                });
            }
        });
    }

    /******** View Holders Classes ********/

    class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView picImageView;

        ImageView facebookImageView;
        ImageView whatsappImageView;
        ImageView ivShare;
        ImageView downloadImageView;

        public ImageViewHolder(View itemView) {
            super(itemView);

            picImageView = itemView.findViewById(R.id.iv_pic);

            facebookImageView = itemView.findViewById(R.id.iv_facebook);
            whatsappImageView = itemView.findViewById(R.id.iv_whatsapp);
            ivShare = itemView.findViewById(R.id.iv_share);
            downloadImageView = itemView.findViewById(R.id.iv_download);
        }

        @Override
        public void onClick(View view) {
        }
    }
}
