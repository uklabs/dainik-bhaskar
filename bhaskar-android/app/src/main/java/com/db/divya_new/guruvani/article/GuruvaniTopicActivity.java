package com.db.divya_new.guruvani.article;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.data.GuruInfo;
import com.db.divya_new.data.HeaderInfo;
import com.db.main.BaseAppCompatActivity;
import com.db.util.Constants;
import com.db.util.DividerItemDecoration;
import com.db.util.ThemeUtil;

/**
 * intent params:
 * EXTRA_GURU_INFO - guru info object
 */
public class GuruvaniTopicActivity extends BaseAppCompatActivity {

    public static final String EXTRA_DETAIL_URL = "detail_url";
    public static final String EXTRA_TOPIC_INFO = "topic_info";
    public static final String EXTRA_PARENT_ID = "parent_id";
    public static final String EXTRA_CATEGORY_INFO = "categoryInfo";

    private GuruvaniTopicAdapter adapter;
    private String mDetailUrl;
    private GuruInfo mTopicInfo;
    private String mId;
    TextView tvTitle;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equalsIgnoreCase(Constants.IntentFilterType.FILTER_TITLE)) {
                String title = intent.getStringExtra(Constants.IntentExtra.TITLE);
                if (!TextUtils.isEmpty(title)) {
                    tvTitle.setText(title);
                }
            }
        }
    };

    private CategoryInfo categoryInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_guruvani_article);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuruvaniTopicActivity.this.finish();
            }
        });

        tvTitle = findViewById(R.id.toolbar_title);
        tvTitle.setTextColor(Color.BLACK);

        Intent intent = getIntent();
        if (intent != null) {
            categoryInfo = (CategoryInfo) intent.getSerializableExtra(EXTRA_CATEGORY_INFO);
            mDetailUrl = intent.getStringExtra(EXTRA_DETAIL_URL);
            mTopicInfo = (GuruInfo) intent.getSerializableExtra(EXTRA_TOPIC_INFO);
            mId = intent.getStringExtra(EXTRA_PARENT_ID);
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ica_back_black);
            getSupportActionBar().setTitle(null);
            if (mTopicInfo != null) {
                tvTitle.setText(mTopicInfo.title);
            } else {
                tvTitle.setText(getString(R.string.guruvani_txt));
            }
        }

        adapter = new GuruvaniTopicAdapter(this, mDetailUrl, mTopicInfo.id, mTopicInfo.name, mId, categoryInfo);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        recyclerView.setAdapter(adapter);

        adapter.add(new HeaderInfo());
        adapter.add("Footer");
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(Constants.IntentFilterType.FILTER_TITLE));
//        LocalBroadcastManager.getInstance(this).registerReceiver(receiverToCloseActivity, new IntentFilter(Constants.IntentFilterType.FILTER_FOR_ACTIVITY));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiverToCloseActivity);
    }
}
