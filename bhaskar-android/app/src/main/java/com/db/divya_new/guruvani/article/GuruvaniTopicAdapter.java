package com.db.divya_new.guruvani.article;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.data.HeaderInfo;
import com.db.divya_new.guruvani.viewholder.sub.VideoViewHolder;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.QuickPreferences;

import java.util.ArrayList;

public class GuruvaniTopicAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mContext;
    private ArrayList<Object> mList;
    private String mDetailUrl;
    private String mTopicId;
    private String mTopicName;
    private String mId;
    private CategoryInfo categoryInfo;

    public GuruvaniTopicAdapter(Activity context, String detailUrl, String topicId, String topicName, String id, CategoryInfo categoryInfo) {
        mList = new ArrayList<>();
        mContext = context;
        mDetailUrl = detailUrl;
        mTopicId = topicId;
        mTopicName = topicName;
        mId = id;
        this.categoryInfo = categoryInfo;
    }

    public void add(Object object) {
        mList.add(object);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 1:
                String sectionLabel = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME) + "_" + categoryInfo.displayName;
                return new VideoViewHolder(inflater.inflate(R.layout.listitem_guruvani_article_tab_video, parent, false), mDetailUrl, mTopicId, mTopicName, VideoViewHolder.TYPE_TOPIC, categoryInfo.gaScreen, categoryInfo.gaArticle, categoryInfo.gaEventLabel, sectionLabel, categoryInfo.color);
        }
        return new BlankViewHolder(inflater.inflate(R.layout.listitem_blank_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof VideoViewHolder) {
            videoBindHolder((VideoViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mList.get(position);
        if (object instanceof HeaderInfo) {
            return 1;
        }
        return 0;
    }

    /******** Bind Holders Methods ********/

    private void videoBindHolder(VideoViewHolder viewHolder, int position) {

    }

    /******** View Holders Classes ********/
}
