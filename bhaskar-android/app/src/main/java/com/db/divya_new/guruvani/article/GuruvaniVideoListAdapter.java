package com.db.divya_new.guruvani.article;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.source.server.BackgroundRequest;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.guruvani.video.GuruvaniVideoActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.ShareUtil;

import java.util.ArrayList;

public class GuruvaniVideoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<VideoInfo> mList;
    private Context mContext;
    private String mGuruId;
    private String mTopicId;
    private String mGaArticle;
    private String mGaEventLabel;
    private String mSectionLabel;
    private String mGuruNTopicName;
    private String mColorCode;

    public GuruvaniVideoListAdapter(Context context, String gaArticle, String gaEventLabel, String sectionLabel, String colorCode) {
        mList = new ArrayList<>();
        mContext = context;
        mGaArticle = gaArticle;
        mGaEventLabel = gaEventLabel;
        mSectionLabel = sectionLabel;
        mColorCode = colorCode;
    }

    public void setIds(String guruId, String topicId, String guruNTopicName) {
        mTopicId = topicId;
        mGuruId = guruId;
        mGuruNTopicName = guruNTopicName;
    }

    public void add(VideoInfo object) {
        mList.add(object);
        notifyDataSetChanged();
    }

    public void addAndClearList(ArrayList<VideoInfo> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void addList(ArrayList<VideoInfo> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 1:
                return new VideoHolder(inflater.inflate(R.layout.listitem_guruvani_video_item, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof VideoHolder) {
            videoBindHolder((VideoHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    /******** Bind Holders Methods ********/

    private void videoBindHolder(VideoHolder viewHolder, final int position) {
        VideoInfo info = mList.get(position);

        viewHolder.titleTextView.setText(info.title);

        viewHolder.durationTextView.setVisibility(View.GONE);
        viewHolder.playImageView.setColorFilter(AppUtils.getThemeColor(mContext, mColorCode));

        ImageUtil.setImage(mContext, info.image, viewHolder.picImageView, R.drawable.water_mark_news_detail);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSectionLabel = mSectionLabel + "_" + mGuruNTopicName;

                Intent intent = new Intent(mContext, GuruvaniVideoActivity.class);
                intent.putExtra(GuruvaniVideoActivity.EXTRA_VIDEO_INFO, info);
                intent.putExtra(GuruvaniVideoActivity.EXTRA_GURU_ID, mGuruId);
                intent.putExtra(GuruvaniVideoActivity.EXTRA_TOPIC_ID, mTopicId);
                intent.putExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL, mSectionLabel);
                intent.putExtra(Constants.KeyPair.KEY_SOURCE, AppFlyerConst.DBVideosSource.VIDEO_LIST);
                intent.putExtra(Constants.KeyPair.KEY_POSITION, position + 1);
                intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, mGaArticle);
                intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, mGaEventLabel);
                intent.putExtra(Constants.KeyPair.KEY_COLOR, mColorCode);
                mContext.startActivity(intent);
            }
        });

        viewHolder.moreImageView.setOnClickListener(view -> {
            PopupWindow popUpWindow = AppUtils.showMoreOptionsV2(mContext, view, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    //not visible
                }

                @Override
                public void onShare() {
                    BackgroundRequest.getStoryShareUrl(mContext, info.link, (flag, url) -> ShareUtil.shareDefault(mContext, info.title, mContext.getResources().getString(R.string.checkout_this_video), url, info.gTrackUrl, false));
                }
            });

            popUpWindow.getContentView().findViewById(R.id.tvBookMark).setVisibility(View.GONE);
            popUpWindow.getContentView().findViewById(R.id.separator_v).setVisibility(View.GONE);
        });
    }

    /******** View Holders Classes ********/

    class VideoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView picImageView;
        ImageView moreImageView;
        ImageView playImageView;
        TextView titleTextView;
        TextView durationTextView;
        TextView viewsTextView;

        public VideoHolder(View itemView) {
            super(itemView);

            picImageView = itemView.findViewById(R.id.iv_pic);
            titleTextView = itemView.findViewById(R.id.tv_title);
            durationTextView = itemView.findViewById(R.id.tv_duration);
            viewsTextView = itemView.findViewById(R.id.tv_views);
            playImageView = itemView.findViewById(R.id.video_play_icon);
            moreImageView = itemView.findViewById(R.id.more_img);
        }

        @Override
        public void onClick(View view) {
        }
    }
}
