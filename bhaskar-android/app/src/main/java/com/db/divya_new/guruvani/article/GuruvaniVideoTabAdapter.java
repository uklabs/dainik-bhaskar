package com.db.divya_new.guruvani.article;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.divya_new.data.TabInfo;
import com.db.util.ImageUtil;

import java.util.ArrayList;

public class GuruvaniVideoTabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface OnTabClickLitener {
        void onTabClick(TabInfo info);
    }

    private OnTabClickLitener mListener;
    private ArrayList<TabInfo> mList;
    private Context mContext;
    private int selectedPos = -1;

    public GuruvaniVideoTabAdapter(Context context, OnTabClickLitener listener) {
        mContext = context;
        mList = new ArrayList<>();
        mListener = listener;
    }

    public void setSelectedPos(int pos) {
        selectedPos = pos;
    }

    public void add(ArrayList<TabInfo> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_tab_image_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            itemBindHolder((ItemViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    /******** Bind Holders Methods ********/

    private void itemBindHolder(ItemViewHolder viewHolder, final int pos) {

        final TabInfo info = mList.get(pos);

        viewHolder.textView.setText(info.title);
        ImageUtil.setImage(mContext, info.image, viewHolder.iconView, R.drawable.water_mark_brand_category);

        if (selectedPos == pos) {
            viewHolder.mainLayout.setBackgroundResource(R.drawable.round_border_selected);
        } else {
            viewHolder.mainLayout.setBackgroundResource(R.drawable.round_border);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = pos;
                if (mListener != null) {
                    mListener.onTabClick(info);
                }

                notifyDataSetChanged();
            }
        });
    }

    /******** View Holders Classes ********/

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout mainLayout;
        private TextView textView;
        private ImageView iconView;

        public ItemViewHolder(View itemView) {
            super(itemView);

            mainLayout = itemView.findViewById(R.id.tab_layout);
            textView = itemView.findViewById(R.id.tv_title);
            iconView = itemView.findViewById(R.id.iv_icon);
        }
    }
}
