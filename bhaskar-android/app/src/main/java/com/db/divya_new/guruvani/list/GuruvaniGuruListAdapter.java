package com.db.divya_new.guruvani.list;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bhaskar.view.ui.MainActivity;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.viewholder.CircularImageTitleViewHolder;
import com.db.divya_new.data.GuruInfo;
import com.db.divya_new.guruvani.article.GuruvaniArticleActivity;
import com.db.homebanner.BannerViewHolder;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.views.viewholder.LoadingViewHolder;

import java.util.ArrayList;
import java.util.List;

public class GuruvaniGuruListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {

    public static final int TYPE_LOADING = 1;
    public static final int TYPE_LIST = 2;
    public final static int VIEW_TYPE_AD = 25;
    public static final int VIEW_TYPE_BANNER = 10;

    private ArrayList<Object> mList;
    private Activity mContext;
    private String mId;
    private CategoryInfo categoryInfo;
    private int tabSelection;

    public GuruvaniGuruListAdapter(Activity context, String id) {
        mList = new ArrayList<>();
        mContext = context;
        mId = id;
    }

    public void clear() {
        isBannerLoaded = false;
        mList.clear();
        notifyDataSetChanged();
    }

    public void setData(CategoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
    }

    public void setTabSelection(int tabSelect) {
        tabSelection = tabSelect;
    }

    public void add(List<Object> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void addAndClear(List<Object> list) {
        isBannerLoaded = false;
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem() {
        mList.add(null);
        notifyDataSetChanged();
    }

    public void removeItem() {
        if (mList.size() > 0 && mList.get(mList.size() - 1) == null) {
            mList.remove(mList.size() - 1);
        }
        notifyDataSetChanged();
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;
        notifyItemChanged(0);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_LOADING) {
            return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item, parent, false));
        } else if (viewType == VIEW_TYPE_AD) {
            return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));
        } else if (viewType == VIEW_TYPE_BANNER) {
            return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
        } else {
            return new CircularImageTitleViewHolder(inflater.inflate(R.layout.listitem_circular_image_title, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CircularImageTitleViewHolder) {
            itemBindHolder((CircularImageTitleViewHolder) holder, position);
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
            loadingViewHolder.progressBar.setIndeterminate(true);
            loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
        } else if (holder instanceof BannerAdViewHolder) {
//            BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
//            adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
            BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
            if (((AdInfo) mList.get(position)).catType == 1) {
                if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                    adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);

                } else {
                    ImageView imageView = new ImageView(mContext);
                    imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    imageView.setAdjustViewBounds(true);
                    imageView.setClickable(true);
                    String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                    ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                    adViewHolder.addAdsUrl(imageView);

                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AppUtils.getInstance().clickOnAdBanner(mContext);
                        }
                    });
                }
            } else {
                adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
            }
        } else if (holder instanceof BannerViewHolder) {
            BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
            bannerViewHolder.callwebViewOrSetWidgetData((BannerInfo) mList.get(holder.getAdapterPosition()));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mList.get(position);
        if (object == null) {
            return TYPE_LOADING;
        } else if (object instanceof BannerInfo) {
            return VIEW_TYPE_BANNER;
        } else if (object instanceof AdInfo) {
            return VIEW_TYPE_AD;
        }
        return TYPE_LIST;
    }

    /******** Bind Holders Methods ********/

    private void itemBindHolder(CircularImageTitleViewHolder viewHolder, final int position) {
        GuruInfo guruInfo = (GuruInfo) mList.get(position);
        viewHolder.titleTextView.setText(guruInfo.title);

        ImageUtil.setImage(mContext, guruInfo.image, viewHolder.iconImageView, R.drawable.water_mark_brand_category);

        viewHolder.rootview.setOnClickListener(view -> {
            if (guruInfo.comingSoon.equalsIgnoreCase("0")) {
                Intent intent = new Intent(mContext, GuruvaniArticleActivity.class);
                intent.putExtra(GuruvaniArticleActivity.EXTRA_GURU_TAB_SELECTION, tabSelection);
                intent.putExtra(GuruvaniArticleActivity.EXTRA_GURU_INFO, guruInfo);
                intent.putExtra(GuruvaniArticleActivity.EXTRA_PARENT_ID, mId);
                intent.putExtra(GuruvaniArticleActivity.EXTRA_CATEGORY_INFO, categoryInfo);
                mContext.startActivityForResult(intent, MainActivity.REQUEST_CODE_CALL_FOR_FOOTER_TAB);
            }
        });
    }
}
