package com.db.divya_new.guruvani.list;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.view.ui.MainActivity;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.viewholder.ImageTitleViewHolder;
import com.db.divya_new.data.GuruInfo;
import com.db.divya_new.guruvani.article.GuruvaniTopicActivity;
import com.db.util.ImageUtil;

import java.util.ArrayList;

public class GuruvaniTopicListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<GuruInfo> mList;
    private Activity mContext;
    private String mDetailUrl;
    private String mId;
    private CategoryInfo categoryInfo;

    public GuruvaniTopicListAdapter(Activity context, String id) {
        mList = new ArrayList<>();
        mContext = context;
        mId = id;
    }

    public void setData(CategoryInfo categoryInfo) {
        mDetailUrl = categoryInfo.detailUrl;
        this.categoryInfo = categoryInfo;

    }

    public void add(ArrayList<GuruInfo> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageTitleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_image_title, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ImageTitleViewHolder) {
            itemBindHolder((ImageTitleViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    /******** Bind Holders Methods ********/

    private void itemBindHolder(ImageTitleViewHolder viewHolder, final int position) {
        viewHolder.titleTextView.setText(mList.get(position).title);

        ImageUtil.setImage(mContext, mList.get(position).image, viewHolder.iconImageView, R.drawable.water_mark_brand_category);

        viewHolder.rootview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, GuruvaniTopicActivity.class);
                intent.putExtra(GuruvaniTopicActivity.EXTRA_DETAIL_URL, mDetailUrl);
                intent.putExtra(GuruvaniTopicActivity.EXTRA_TOPIC_INFO, mList.get(position));
                intent.putExtra(GuruvaniTopicActivity.EXTRA_PARENT_ID, mId);
                intent.putExtra(GuruvaniTopicActivity.EXTRA_CATEGORY_INFO, categoryInfo);
                mContext.startActivityForResult(intent, MainActivity.REQUEST_CODE_CALL_FOR_FOOTER_TAB);
            }
        });
    }
}
