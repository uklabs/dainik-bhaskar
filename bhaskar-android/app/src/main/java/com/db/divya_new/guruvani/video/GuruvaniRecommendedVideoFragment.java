package com.db.divya_new.guruvani.video;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;
import com.bhaskar.util.CommonConstants;
import com.db.data.database.BooleanExpressionTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.ReadUnreadStatusTable;
import com.db.data.source.server.BackgroundRequest;
import com.db.dbvideoPersonalized.OnVideoSelectedListener;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.dbvideoPersonalized.WatchedPersonalizedVideoData;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.listeners.OnStoryShareUrlListener;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.ShareUtil;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.ViewAnimationUtils;
import com.db.views.viewholder.LoadingViewHolder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GuruvaniRecommendedVideoFragment extends Fragment {
    private Rect scrollBounds;
    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private ProgressBar progressBar;
    private View retryLayout;
    private OnVideoSelectedListener videoSelectListener;
    private ArrayList<VideoInfo> dbVideosInfoArrayList;
    private PlayListAdapter adapter;
    private String guruId;
    private String topicId;
    private String mColorCode;
    private String mRecommendationUrl;
    private VideoInfo mVideoInfo;
    private WatchedPersonalizedVideoData watchedVideoData;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int pageCount = 1;
    private boolean isLoadMoreEnable = true;
    private BooleanExpressionTable booleanExpressionTable;
    private ReadUnreadStatusTable readUnreadStatusTable;
    private ArrayList<Integer> impressionList = new ArrayList<>();
    private boolean showDescriptionLayout = false;

    public GuruvaniRecommendedVideoFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            videoSelectListener = (OnVideoSelectedListener) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbVideosInfoArrayList = new ArrayList<>();

        readUnreadStatusTable = (ReadUnreadStatusTable) DatabaseHelper.getInstance(getContext()).getTableObject(ReadUnreadStatusTable.TABLE_NAME);
        booleanExpressionTable = (BooleanExpressionTable) DatabaseHelper.getInstance(getContext()).getTableObject(BooleanExpressionTable.TABLE_NAME);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recommendation_list, container, false);

        retryLayout = view.findViewById(R.id.retry_layout);
        progressBar = view.findViewById(R.id.progressBar);
        recyclerView = view.findViewById(R.id.recycler_view);

        watchedVideoData = WatchedPersonalizedVideoData.getInstance(getContext());

        pageCount = 0;
        isLoadMoreEnable = true;

        view.findViewById(R.id.retry_button).setOnClickListener(view1 -> {
            retryLayout.setVisibility(View.GONE);
            fetchDataFromServer();
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.scrollBounds = new Rect();
        this.recyclerView.getHitRect(scrollBounds);
        adapter = new PlayListAdapter();

        mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        SpacesDecoration spacesItemDecoration = new SpacesDecoration(getResources().getDimensionPixelSize(R.dimen.list_spacing_max), getResources().getDimensionPixelSize(R.dimen.list_spacing_max), 0, getResources().getDimensionPixelSize(R.dimen.list_spacing_max));
//        SpacesDecoration spacesItemDecoration = new SpacesDecoration(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.list_spacing_video));
        recyclerView.addItemDecoration(spacesItemDecoration);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (isLoadMoreEnable) {
                            onLoadMore();
                            isLoading = true;
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                Systr.println("Tracking : Recommendation state change : " + newState);
                checkForImpressionTracking();
            }
        });
    }

    private void checkForImpressionTracking() {
        if (recyclerView != null) {
            View childAt;
            int height;

            VideoInfo itemAtPosition;
            int childAdapterPosition;
            for (int i = 0; i < recyclerView.getChildCount(); i++) {
                try {
                    childAt = recyclerView.getChildAt(i);
                    height = childAt.getHeight();
                    if (childAt.getLocalVisibleRect(scrollBounds)) {
                        if (scrollBounds.bottom == height) {
                            childAdapterPosition = recyclerView.getChildAdapterPosition(childAt);
                            if (childAdapterPosition > 0) {
                                itemAtPosition = adapter.getItemAtPosition(childAdapterPosition);
                                if (!impressionList.contains(childAdapterPosition)) {
                                    Systr.println("Impression Tracking : Recommendation : " + childAdapterPosition + ", PageNo : 0, Index : " + childAdapterPosition);
                                    Tracking.trackVideoImpression(getContext(), itemAtPosition.impressionTrackUrl, 0, childAdapterPosition, "impression", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);
                                    impressionList.add(childAdapterPosition);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void checkForImpressionLoadTime() {
        if (recyclerView != null) {
            int firstPosition = mLayoutManager.findFirstVisibleItemPosition();
            int lastPosition = mLayoutManager.findLastCompletelyVisibleItemPosition();

            VideoInfo itemAtPosition;
            for (int i = firstPosition; i <= lastPosition; i++) {
                try {
                    if (i > 0) {
                        itemAtPosition = adapter.getItemAtPosition(i);
                        if (!impressionList.contains(i)) {
                            Systr.println("Impression Tracking : Recommendation : " + i + ", PageNo : 0, Index : " + i);
                            Tracking.trackVideoImpression(getContext(), itemAtPosition.impressionTrackUrl, 0, i, "impression", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);
                            impressionList.add(i);
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    public void onLoadMore() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (dbVideosInfoArrayList != null)
                    dbVideosInfoArrayList.add(null);

                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });

        //Load more data for reyclerview
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetchDataFromServer();
            }
        }, 500);
    }

    private void fetchDataFromServer() {
        if (mVideoInfo != null) {
            String recommendedUrl = AppUtils.getInstance().updateApiUrl(Urls.APP_FEED_BASE_URL + mRecommendationUrl + guruId + "-" + topicId + "-" + mVideoInfo.storyId + "/PG" + pageCount + "/");

            makeJsonObjectRequest(recommendedUrl);
        }
    }

    public void setVideo(VideoInfo info, String recommondationUrl, String guruId, String topicId, String color) {
        this.mVideoInfo = info;
        this.mRecommendationUrl = recommondationUrl;
        this.guruId = guruId;
        this.topicId = topicId;
        this.mColorCode = color;

        pageCount = 1;
        isLoadMoreEnable = true;

        if (mVideoInfo != null) {
            mVideoInfo.isLiked = booleanExpressionTable.isBoolean(mVideoInfo.storyId, Constants.BooleanConst.VIDEO_LIKE);
        }

        showDescriptionLayout = false;
        impressionList.clear();

        dbVideosInfoArrayList.clear();
        adapter.notifyDataSetChanged();

        retryLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        if (mLayoutManager != null)
            mLayoutManager.scrollToPosition(0);

        fetchDataFromServer();
    }

    private void makeJsonObjectRequest(String recommendedUrl) {
        Systr.println("Recommended Url for video : " + recommendedUrl);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, recommendedUrl, null,
                response -> {
                    Systr.println("Recommended Video response : " + response);
                    try {
                        if (response != null && response.length() > 0) {
                            parseFeedList(response.getJSONArray("feed"), false);
                        }
                    } catch (Exception e) {
                    }

                    progressBar.setVisibility(View.GONE);
                },

                error -> {
                    Systr.println("Recommended Video Error : " + error);

                    progressBar.setVisibility(View.GONE);

                    Activity activity = getActivity();
                    if (activity != null && isAdded()) {

                        if (dbVideosInfoArrayList != null) {
                            dbVideosInfoArrayList.remove(null);
                        }
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }

                        if (error instanceof NoConnectionError || error instanceof NetworkError) {
                            showToast(activity, activity.getResources().getString(R.string.internet_connection_error_));
                        } else {
                            showToast(activity, activity.getResources().getString(R.string.sorry_error_found_please_try_again_));
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
    }

    private void parseFeedList(JSONArray resultArray, boolean clearList) throws JSONException {

        if (clearList) {
            dbVideosInfoArrayList.clear();
            adapter.notifyDataSetChanged();
        }

        String mStoryId = (mVideoInfo != null ? mVideoInfo.storyId : "");

        Gson gson = new Gson();
        JSONObject feedJsonObject;
        for (int i = 0; i < resultArray.length(); i++) {
            feedJsonObject = resultArray.getJSONObject(i);
            VideoInfo newsFeeds = gson.fromJson(feedJsonObject.toString(), VideoInfo.class);
            if (TextUtils.isEmpty(mStoryId) && !TextUtils.isEmpty(newsFeeds.storyId)) {
                if (!watchedVideoData.hasVideo(newsFeeds.storyId) && !dbVideosInfoArrayList.contains(newsFeeds)) {
                    dbVideosInfoArrayList.add(newsFeeds);
                }
            } else {
                if (!TextUtils.isEmpty(newsFeeds.storyId) && !mStoryId.equalsIgnoreCase(newsFeeds.storyId)) {
                    if (!watchedVideoData.hasVideo(newsFeeds.storyId) && !dbVideosInfoArrayList.contains(newsFeeds)) {
                        dbVideosInfoArrayList.add(newsFeeds);
                    }
                }
            }
        }

        // remove null item and page count for load more
        dbVideosInfoArrayList.remove(null);
        adapter.notifyDataSetChanged();

        if (dbVideosInfoArrayList.size() >= 50 || resultArray.length() == 0) {
            isLoadMoreEnable = false;
            isLoading = false;

        } else {
            isLoading = false;
            pageCount++;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkForImpressionLoadTime();
            }
        }, 200);

        try {
            if (dbVideosInfoArrayList.size() < 4 && NetworkStatus.getInstance().isConnected(getContext())) {
                onLoadMore();
            }
        } catch (Exception e) {
        }
    }

    public VideoInfo getFirstItem() {
        if (dbVideosInfoArrayList != null && dbVideosInfoArrayList.size() > 0)
            return dbVideosInfoArrayList.get(0);
        return null;
    }

//    private void sendActionToServer(final int actionType) {
//        String url = Urls.APP_FEED_BASE_URL + AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.VideoPref.SETTINGS_VIDEO_ACTION, Urls.DEFAULT_VIDEO_ACTION);
//
//        JSONObject jsonObject = new JSONObject();
//        try {
//            String mSessionId = TrackingData.getDbORDeviceId(getContext());
//            jsonObject.put("session_id", mSessionId);
//            jsonObject.put("channel_slno", (MainActivity.isBrandMenu) ? CommonConstants.CHANNEL_ID : Constants.GLOBAL_CHANNEL_ID);
//
//            if (mVideoInfo != null) {
//                switch (actionType) {
//                    case ACTION_LIKE:
//                        jsonObject.put("action", "like");
//                        jsonObject.put("vid", mVideoInfo.storyId);
//                        break;
//
//                    case ACTION_UNLIKE:
//                        jsonObject.put("action", "dislike");
//                        jsonObject.put("vid", mVideoInfo.storyId);
//                        break;
//
//                    case ACTION_FOLLOW:
//                        jsonObject.put("action", "follow");
//                        jsonObject.put("vid", mVideoInfo.providerId);
//                        break;
//
//                    case ACTION_UNFOLLOW:
//                        jsonObject.put("action", "unfollow");
//                        jsonObject.put("vid", mVideoInfo.providerId);
//                        break;
//
//                    case ACTION_FB_SHARE:
//                        jsonObject.put("action", "fbshare");
//                        jsonObject.put("vid", mVideoInfo.storyId);
//                        break;
//
//                    case ACTION_WHATSAPP_SHARE:
//                        jsonObject.put("action", "washare");
//                        jsonObject.put("vid", mVideoInfo.storyId);
//                        break;
//
//                    case ACTION_VIEW:
//                        jsonObject.put("action", "view");
//                        jsonObject.put("vid", mVideoInfo.storyId);
//                        break;
//                }
//            }
//        } catch (JSONException e) {
//        }
//
//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                Systr.println("Action Json : " + response);
//                parseActionData(response, actionType);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Systr.println("Action Error : " + error);
//                parseActionData(null, actionType);
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("api-key", "bh@@$k@r-D");
//                headers.put("User-Agent", CommonConstants.USER_AGENT);
//                headers.put("encrypt", "0");
//                headers.put("Content-Type", "application/json charset=utf-8");
//                headers.put("Accept", "application/json");
//                return headers;
//            }
//        };
//
//        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        Context context = getContext();
//        if (context != null) {
//            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(request);
//        }
//    }

//    private void parseActionData(JSONObject response, int actionType) {
//        String status = null;
//        if (response != null)
//            status = response.optString("status");
//
//        Systr.println("Action Response : " + status);
//
//        if (!TextUtils.isEmpty(status) && status.equals("1")) {
//            switch (actionType) {
//                case ACTION_LIKE:
//                    if (mVideoInfo != null) {
//                        mVideoInfo.isLiked = true;
//                        mVideoInfo.likeCount = mVideoInfo.likeCount + 1;
//                        booleanExpressionTable.updateBooleanValue(mVideoInfo.storyId, Constants.BooleanConst.VIDEO_LIKE, true);
//                    }
//                    isLikeProcessing = false;
//                    break;
//
//                case ACTION_UNLIKE:
//                    if (mVideoInfo != null) {
//                        mVideoInfo.isLiked = false;
//                        int count;
//                        mVideoInfo.likeCount = ((count = mVideoInfo.likeCount - 1) < 0 ? 0 : count);
//                        booleanExpressionTable.updateBooleanValue(mVideoInfo.storyId, Constants.BooleanConst.VIDEO_LIKE, false);
//                    }
//                    isLikeProcessing = false;
//                    break;
//
//                case ACTION_FOLLOW:
////                    isSubscribed = true;
//                    if (mVideoInfo != null) {
//                        booleanExpressionTable.updateBooleanValue(mVideoInfo.providerId, Constants.BooleanConst.VIDEO_FOLLOW, true);
//                    }
//                    break;
//
//                case ACTION_UNFOLLOW:
////                    isSubscribed = false;
//                    if (mVideoInfo != null) {
//                        booleanExpressionTable.updateBooleanValue(mVideoInfo.providerId, Constants.BooleanConst.VIDEO_FOLLOW, false);
//                    }
//                    break;
//
//                case ACTION_FB_SHARE:
//                    if (mVideoInfo != null)
//                        mVideoInfo.fbShareCount = mVideoInfo.fbShareCount + 1;
//                    break;
//
//                case ACTION_WHATSAPP_SHARE:
//                    if (mVideoInfo != null)
//                        mVideoInfo.whatsappShareCount = mVideoInfo.whatsappShareCount + 1;
//                    break;
//            }
//
//            if (actionType != ACTION_VIEW) {
//                if (getActivity() != null) {
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            adapter.notifyDataSetChanged();
//                        }
//                    });
//                }
//            }
//
//        } else {
//
//            Context context = getContext();
//            if (context != null) {
//                switch (actionType) {
//                    case ACTION_LIKE:
//                        isLikeProcessing = false;
//                        showToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
//                        break;
//
//                    case ACTION_UNLIKE:
//                        isLikeProcessing = false;
//                        showToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
//                        break;
//
//                    case ACTION_FOLLOW:
//                        showToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
//                        break;
//
//                    case ACTION_UNFOLLOW:
//                        showToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
//                        break;
//
//                    case ACTION_FB_SHARE:
//                        break;
//
//                    case ACTION_WHATSAPP_SHARE:
//                        break;
//                }
//            }
//        }
//    }

    private void showToast(Context context, String msg) {
        AppUtils.getInstance().showCustomToast(context, msg);
    }

    private class PlayListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int VIEW_TYPE_HEADING = 1;
        private static final int VIEW_TYPE_ITEM = 2;
        private static final int VIEW_TYPE_LOADING = 3;
        private Context context;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_HEADING:
//                    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_recommended_heading, parent, false);
                    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_video_player_list_heading, parent, false);
                    return new FirstViewHolder(itemView);
                case VIEW_TYPE_LOADING:
                    View loadingView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false);
                    return new LoadingViewHolder(loadingView);
                default:
//                    View listView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommended_video_list, parent, false);
                    View listView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_video_player_list_video, parent, false);
                    return new VideoViewHolder(listView);
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof VideoViewHolder) {
                final VideoViewHolder viewHolder = (VideoViewHolder) holder;

                final int pos = position - 1;
                final VideoInfo info = dbVideosInfoArrayList.get(pos);

                ImageUtil.setImage(getContext(), info.image, viewHolder.iconImageView, R.drawable.water_mark_news_list);
                viewHolder.playImageView.setColorFilter(AppUtils.getThemeColor(getContext(), mColorCode));
                viewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(info.titleHead, AppUtils.getInstance().fromHtml(info.title), mColorCode));

//                viewHolder.titleTextView.setText(info.title);
//                if (readUnreadStatusTable.isStoryWithVersionAvailable(AppUtils.getFilteredStoryId(info.storyId), info.version)) {
//                    viewHolder.titleTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.article_read_color));
//                } else {
//                    viewHolder.titleTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.list_title_color));
//                }
//                viewHolder.durationTextView.setVisibility(View.GONE);
//                if (TextUtils.isEmpty(info.duration)) {
//                } else {
//                    viewHolder.durationTextView.setVisibility(View.VISIBLE);
//                    viewHolder.durationTextView.setText(info.duration.trim());
//                }

                context = getContext();

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        viewHolder.titleTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.article_read_color));
                        String filteredStoryId = AppUtils.getFilteredStoryId(info.storyId);
                        if (!readUnreadStatusTable.isStoryWithVersionAvailable(filteredStoryId, info.version)) {
                            readUnreadStatusTable.updateStoryVersion(filteredStoryId, info.version);
                        }
                        Context context = getContext();
                        if (context != null) {
                            if (NetworkStatus.getInstance().isConnected(context)) {
                                if (videoSelectListener != null) {
                                    videoSelectListener.onVideoSelected(info, AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION, (pos + 1));
                                }

//                                if (!AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.TOGGLING_SCREEN_DEPTH_VIDEO, true)) {
//                                } else {
//
//                                    Intent intent = new Intent(context, GuruvaniVideoActivity.class);
//                                    intent.putExtra(GuruvaniVideoActivity.EXTRA_VIDEO_INFO, info);
//                                    intent.putExtra(Constants.KeyPair.KEY_VIDEO_SECTION_LABEL, mSectionLabel);
//                                    intent.putExtra(Constants.KeyPair.KEY_SOURCE, AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);
//                                    intent.putExtra(Constants.KeyPair.KEY_POSITION, (pos + 1));
//                                    intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, mGaEventLabel);
//                                    intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, mGaArticle);
//                                    intent.putExtra(GuruvaniVideoActivity.EXTRA_GURU_ID, guruId);
//                                    intent.putExtra(GuruvaniVideoActivity.EXTRA_TOPIC_ID, topicId);
//                                    startActivity(intent);
//                                }

                                Tracking.trackVideoImpression(getContext(), info.impressionTrackUrl, 0, pos, "click", "video_recommendation", "", AppFlyerConst.DBVideosSource.VIDEO_RECOMMENDATION);

                            } else {
                                showToast(context, context.getResources().getString(R.string.internet_connection_error_));
                            }
                        }
                    }
                });

            } else if (holder instanceof FirstViewHolder) {

                FirstViewHolder firstViewHolder = (FirstViewHolder) holder;
                if (mVideoInfo != null) {

                    Context context = getContext();
                    if (context != null) {
//                        firstViewHolder.titleTextView.setText(mVideoInfo.title);
                        firstViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(mVideoInfo.titleHead, AppUtils.getInstance().fromHtml(mVideoInfo.title), mColorCode));
                        firstViewHolder.descTextView.setText(mVideoInfo.story);

                        firstViewHolder.shareImageView.setColorFilter(AppUtils.getThemeColor(context, mColorCode));
                        firstViewHolder.dropDownImageView.setColorFilter(AppUtils.getThemeColor(context, mColorCode));
                        firstViewHolder.dropDownTextView.setTextColor(AppUtils.getThemeColor(context, mColorCode));
                    }
                }

                if (!showDescriptionLayout) {
                    firstViewHolder.descLayout.setVisibility(View.GONE);
                    firstViewHolder.dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_down);
                }

            } else if (holder instanceof LoadingViewHolder) {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return dbVideosInfoArrayList == null ? 0 : (dbVideosInfoArrayList.size() + 1);
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0)
                return VIEW_TYPE_HEADING;
            else if (dbVideosInfoArrayList.get(position - 1) == null)
                return VIEW_TYPE_LOADING;
            else
                return VIEW_TYPE_ITEM;

        }

        public VideoInfo getItemAtPosition(int position) {
            if (position > 0 && position < dbVideosInfoArrayList.size())
                return dbVideosInfoArrayList.get(position - 1);
            return null;
        }
    }

//    private class VideoViewHolder extends RecyclerView.ViewHolder {
//
//        TextView titleTextView;
//        TextView viewsTextView;
//        TextView durationTextView;
//        ImageView iconImageView;
//
//        VideoViewHolder(View view) {
//            super(view);
//
//            iconImageView = view.findViewById(R.id.iv_video_icon);
//            titleTextView = view.findViewById(R.id.tv_title);
//            durationTextView = view.findViewById(R.id.tv_duration);
//            viewsTextView = view.findViewById(R.id.tv_view);
//        }
//    }

    private class VideoViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView;
        ImageView iconImageView;
        ImageView playImageView;

        VideoViewHolder(View view) {
            super(view);

            iconImageView = view.findViewById(R.id.iv_video_icon);
            playImageView = view.findViewById(R.id.video_play_icon);
            titleTextView = view.findViewById(R.id.tv_title);
        }
    }

//    private class FirstViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//
//        TextView titleTextView;
//        TextView viewsTextView;
//        TextView descTextView;
//        LinearLayout descLayout;
//        ImageView dropDownImageView;
//
//        LinearLayout whatsappLayout;
//        LinearLayout fbLayout;
//        LinearLayout downloadLayout;
//
//        FirstViewHolder(View view) {
//            super(view);
//
//            titleTextView = view.findViewById(R.id.tv_title);
//            viewsTextView = view.findViewById(R.id.tv_views);
//            descTextView = view.findViewById(R.id.tv_desc);
//            descLayout = view.findViewById(R.id.desc_layout);
//
//            dropDownImageView = view.findViewById(R.id.iv_dropdown);
//
//            whatsappLayout = view.findViewById(R.id.ll_whatsapp_share);
//            fbLayout = view.findViewById(R.id.ll_fb_share);
//            downloadLayout = view.findViewById(R.id.ll_download);
//
//            dropDownImageView.setOnClickListener(this);
//            whatsappLayout.setOnClickListener(this);
//            fbLayout.setOnClickListener(this);
//            downloadLayout.setOnClickListener(this);
//        }
//
//        @Override
//        public void onClick(View view) {
//            switch (view.getId()) {
//                case R.id.iv_dropdown:
//                    if (descLayout.getVisibility() == View.VISIBLE) {
//                        ViewAnimationUtils.collapse(descLayout, new ViewAnimationUtils.OnCompleteListener() {
//                            @Override
//                            public void onComplete() {
//                                dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_down);
//                                showDescriptionLayout = false;
//                            }
//                        });
//                    } else {
//                        ViewAnimationUtils.expand(descLayout, new ViewAnimationUtils.OnCompleteListener() {
//                            @Override
//                            public void onComplete() {
//                                dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_up);
//                                showDescriptionLayout = true;
//                            }
//                        });
//                    }
//                    break;
//
//                case R.id.ll_like:
//                    if (!isLikeProcessing) {
//                        isLikeProcessing = true;
//                        if (mVideoInfo.isLiked) {
//                            sendActionToServer(ACTION_UNLIKE);
//                        } else {
//                            sendActionToServer(ACTION_LIKE);
//                        }
//                    }
//                    break;
//
//                case R.id.ll_whatsapp_share:
//                    if (mVideoInfo != null) {
//                        Activity activity = getActivity();
//                        if (activity != null) {
//                            if (ShareUtil.whatsappShare(activity, mVideoInfo.title, mVideoInfo.link))
//                                sendActionToServer(ACTION_WHATSAPP_SHARE);
//                        }
//                    }
//                    break;
//
//                case R.id.ll_fb_share:
//                    if (mVideoInfo != null) {
//                        Activity activity = getActivity();
//                        if (activity != null) {
//                            if (ShareUtil.facebookShare(activity, mVideoInfo.title, mVideoInfo.link)) {
//                                sendActionToServer(ACTION_FB_SHARE);
//                            } else {
//                                AppUtils.getInstance().showCustomToast(activity, "Please install Facebook first.");
//                            }
//                        }
//                    }
//                    break;
//
//                case R.id.ll_download:
//                    if (mVideoInfo != null) {
//                        final Activity activity = getActivity();
//                        if (activity != null) {
//                            AppUtils.downloadFile(activity, mVideoInfo.internalVideo, new OnDownloadListener() {
//                                @Override
//                                public void onStart() {
//                                    String campaign = AppPreferences.getInstance(activity).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
//                                    Tracking.trackGAEvent(activity, InitApplication.getInstance().getDefaultTracker(), VideoPlayerEventTracker.CATEGORY_VIDEO, VideoEventConstant.VIDEO_DOWNLOAD, mVideoInfo.mediaTitle + "_" + mVideoInfo.link, campaign);
//                                }
//                            });
//                        }
//                    }
//                    break;
//            }
//        }
//    }

    private class FirstViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView titleTextView;
        LinearLayout dropDownLayout;
        TextView dropDownTextView;
        ImageView dropDownImageView;
        ImageView shareImageView;
        TextView descTextView;
        LinearLayout descLayout;

        FirstViewHolder(View view) {
            super(view);

            titleTextView = view.findViewById(R.id.tv_title);
            descTextView = view.findViewById(R.id.tv_desc);
            descLayout = view.findViewById(R.id.desc_layout);

            dropDownLayout = view.findViewById(R.id.dropdown_layout);
            dropDownImageView = view.findViewById(R.id.iv_dropdown);
            dropDownTextView = view.findViewById(R.id.tv_dropdown);
            dropDownLayout.setOnClickListener(this);

            shareImageView = view.findViewById(R.id.iv_share);
            shareImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.dropdown_layout:
                    if (descLayout.getVisibility() == View.VISIBLE) {
                        ViewAnimationUtils.collapse(descLayout, () -> {
                            dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_down);
                            dropDownTextView.setText(getContext().getString(R.string.read_more_));
                            showDescriptionLayout = false;
                        });
                    } else {
                        ViewAnimationUtils.expand(descLayout, () -> {
                            dropDownImageView.setImageResource(R.drawable.dbv_ic_drop_up);
                            dropDownTextView.setText(getContext().getString(R.string.close));
                            showDescriptionLayout = true;
                        });
                    }
                    break;

                case R.id.iv_share:
                    Context context = getContext();
                    if (context != null) {
                        BackgroundRequest.getStoryShareUrl(context, mVideoInfo.link, new OnStoryShareUrlListener() {
                            @Override
                            public void onUrlFetch(boolean flag, String shareUrl) {
                                ShareUtil.shareDefault(context, mVideoInfo.title, context.getResources().getString(R.string.checkout_this_video), shareUrl, mVideoInfo.gTrackUrl, true);
                            }
                        });
                    }
                    break;
            }
        }
    }
}
