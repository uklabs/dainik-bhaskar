package com.db.divya_new.guruvani.viewholder;

import android.app.Activity;
import android.graphics.Color;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.data.GuruInfo;
import com.db.divya_new.guruvani.article.GuruvaniArticleListAdapter;
import com.bhaskar.appscommon.tracking.Tracking;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;

public class GuruvaniArticleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private Activity mContext;
    private TextView videoTabView;
    private TextView subakyaTabView;
    private TextView programTabView;

    private TextView headerTextView;
    private ImageView picImageView;
    private RecyclerView recyclerView;
    private int mTabSelection;
    private GuruvaniArticleListAdapter articleListAdapter;
    private GuruInfo mGuruInfo;
    private CategoryInfo mCategoryInfo;

    public GuruvaniArticleViewHolder(View itemView, int tabSelection, GuruInfo guruInfo, Activity activity, CategoryInfo categoryInfo) {
        super(itemView);
        mContext = activity;
        mTabSelection = tabSelection;
        mGuruInfo = guruInfo;
        mCategoryInfo = categoryInfo;

        headerTextView = itemView.findViewById(R.id.tv_header);
        picImageView = itemView.findViewById(R.id.iv_pic);

        videoTabView = itemView.findViewById(R.id.tv_tab_1);
        subakyaTabView = itemView.findViewById(R.id.tv_tab_2);
        programTabView = itemView.findViewById(R.id.tv_tab_3);

        headerTextView.setText(mGuruInfo.title);
        ImageUtil.setImage(mContext, mGuruInfo.image, picImageView, R.drawable.water_mark_brand_category);

        if (Constants.guruTabInfo != null) {
            videoTabView.setText(Constants.guruTabInfo.videoInfo.menuName);
            subakyaTabView.setText(Constants.guruTabInfo.qutoesInfo.menuName);
            programTabView.setText(Constants.guruTabInfo.scheduleInfo.menuName);
        }

        videoTabView.setOnClickListener(this);
        subakyaTabView.setOnClickListener(this);
        programTabView.setOnClickListener(this);

        String sectionLabel = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.BASE_NAME, Constants.BaseName.HOME) + "_" + categoryInfo.displayName;
        articleListAdapter = new GuruvaniArticleListAdapter(mContext, mGuruInfo.id, mGuruInfo.name, categoryInfo.gaScreen, categoryInfo.gaArticle, categoryInfo.gaEventLabel, sectionLabel, categoryInfo.color);

        recyclerView = itemView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        recyclerView.setAdapter(articleListAdapter);

        setTabView(tabSelection);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_tab_1:
                mTabSelection = 1;
                break;

            case R.id.tv_tab_2:
                mTabSelection = 2;
                break;

            case R.id.tv_tab_3:
                mTabSelection = 3;
                break;
        }
        setTabView(mTabSelection);
    }

    private void setTabView(int tabSelection) {
//        articleListAdapter.add("", tabSelection);
        articleListAdapter.add(tabSelection);

        videoTabView.setBackgroundResource(R.drawable.inner_tab_default_bg);
        subakyaTabView.setBackgroundResource(R.drawable.inner_tab_default_bg);
        programTabView.setBackgroundResource(R.drawable.inner_tab_default_bg);

        videoTabView.setTextColor(Color.BLACK);
        subakyaTabView.setTextColor(Color.BLACK);
        programTabView.setTextColor(Color.BLACK);

        String value = "";
        switch (tabSelection) {
            case 1:
                videoTabView.setBackgroundResource(R.drawable.inner_tab_selected_bg);
                videoTabView.setTextColor(Color.WHITE);
                value = mCategoryInfo.gaScreen + "-" + mGuruInfo.name + "-" + (Constants.guruTabInfo != null ? Constants.guruTabInfo.videoInfo.displayName : "");
                break;

            case 2:
                subakyaTabView.setBackgroundResource(R.drawable.inner_tab_selected_bg);
                subakyaTabView.setTextColor(Color.WHITE);
                value = mCategoryInfo.gaScreen + "-" + mGuruInfo.name + "-" + (Constants.guruTabInfo != null ? Constants.guruTabInfo.qutoesInfo.displayName : "");
                break;

            case 3:
                programTabView.setBackgroundResource(R.drawable.inner_tab_selected_bg);
                programTabView.setTextColor(Color.WHITE);
                value = mCategoryInfo.gaScreen + "-" + mGuruInfo.name + "-" + (Constants.guruTabInfo != null ? Constants.guruTabInfo.scheduleInfo.displayName : "");
                break;
        }

        if (!TextUtils.isEmpty(value)) {
            String source = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            Tracking.trackGAScreen(mContext, InitApplication.getInstance().getDefaultTracker(), value, source, medium, campaign);
        }
    }
}