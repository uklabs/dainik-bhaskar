package com.db.divya_new.guruvani.viewholder;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.LoadMoreController;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHomeData;
import com.db.divya_new.data.GuruInfo;
import com.db.divya_new.guruvani.list.GuruvaniGuruListAdapter;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GuruvaniGuruViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, LoadMoreController {

    public Button btn_retry;
    public RecyclerView gridRecyclerView;
    public GuruvaniGuruListAdapter guruvaniListAdapter;
    public TextView videoTabView;
    public TextView subakyaTabView;
    public TextView programTabView;
    public boolean isFromParentHome;
    boolean isLoadMoreClicked = false;
    private ProgressBar loadMoreProgressBar;
    private Activity mContext;
    private int tabSelection = 1;
    private int pageCount = 1;
    private boolean isLoading;
    private String mFeedUrl;
    private boolean isFromHome;
    private String mParentId;
    private String mSuperParentId;
    private boolean isLoadMoreEnabled = true;
    private int showCount;
    private String id;
    private String pageTitle;
    private View dividerView;
    private Button btnSubHeaderLoadMore;

    public GuruvaniGuruViewHolder(Activity activity, View itemView, String id) {
        super(itemView);
        mContext = activity;
        btnSubHeaderLoadMore = itemView.findViewById(R.id.btnSubHeaderLoadMore);

        guruvaniListAdapter = new GuruvaniGuruListAdapter(mContext, id);
        videoTabView = itemView.findViewById(R.id.tv_tab_1);
        subakyaTabView = itemView.findViewById(R.id.tv_tab_2);
        programTabView = itemView.findViewById(R.id.tv_tab_3);

        dividerView = itemView.findViewById(R.id.tab_divider_view);
        dividerView.setVisibility(View.VISIBLE);

        if (Constants.guruTabInfo != null) {
            videoTabView.setText(Constants.guruTabInfo.videoInfo.menuName);
            subakyaTabView.setText(Constants.guruTabInfo.qutoesInfo.menuName);
            programTabView.setText(Constants.guruTabInfo.scheduleInfo.menuName);
        }

        videoTabView.setOnClickListener(this);
        subakyaTabView.setOnClickListener(this);
        programTabView.setOnClickListener(this);

        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        btn_retry = itemView.findViewById(R.id.btn_retry);


        gridRecyclerView = itemView.findViewById(R.id.recycler_view);
        gridRecyclerView.setLayoutManager(new GridLayoutManager(itemView.getContext(), 2));
        gridRecyclerView.setAdapter(guruvaniListAdapter);

        // load more button clicked, to add more data in list
        btn_retry.setOnClickListener(view -> onRetry());
        setTabView(tabSelection);


    }

    private void onRetry() {
        if (!isLoading) {
            loadMoreProgressBar.setVisibility(View.VISIBLE);
            btn_retry.setVisibility(View.INVISIBLE);
            fetchData(false);
        }
    }

    private void onLoadMore() {
        if (isFromHome && isFromParentHome) {
            if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TabController.getInstance().setTabClicked(true);
                        TabController.getInstance().setTabClicked(true);
                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mParentId, GuruvaniGuruViewHolder.this);
                    }
                }, 200);
            }
        } else if (isFromHome) {
            if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(mParentId, GuruvaniGuruViewHolder.this);
            }
        } else if (!isFromParentHome && !isFromHome) {
            if (!isLoading) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                fetchData(false);
            }
        }
    }

    public void clearDataAndLoadNew() {
        guruvaniListAdapter.clear();
    }

    public void setData(CategoryInfo categoryInfo) {
        this.mFeedUrl = categoryInfo.feedUrl;
        this.isFromHome = false;
        this.isFromParentHome = false;
        this.mParentId = categoryInfo.id;
        this.mSuperParentId = categoryInfo.parentId;
        this.showCount = 0;
        this.id = categoryInfo.id;

        dividerView.setBackgroundColor(AppUtils.getThemeColor(mContext, categoryInfo.color));

        guruvaniListAdapter.setData(categoryInfo);

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && pageCount <= 2) {
            parseOfflineData(catHomeData);
            pageCount = 2;
            if (!TextUtils.isEmpty(catHomeData.pageTitle)) {
                setPageTitle(catHomeData.pageTitle);
            }
        } else {
            setUpPageTitle(categoryInfo.extraVlues);
            pageCount = 1;
            fetchData(true);
        }

    }

    public void setData(CategoryInfo categoryInfo, boolean isFromHome, boolean isFromParentHome, String mpParentId, String mSuperParentId, int showCount) {
        this.mFeedUrl = categoryInfo.feedUrl;
        this.isFromHome = isFromHome;
        this.isFromParentHome = isFromParentHome;
        this.mParentId = mpParentId;
        this.mSuperParentId = mSuperParentId;
        this.showCount = showCount;
        this.id = categoryInfo.id;

        dividerView.setBackgroundColor(AppUtils.getThemeColor(mContext, categoryInfo.color));


        guruvaniListAdapter.setData(categoryInfo);

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && pageCount <= 2) {
            parseOfflineData(catHomeData);
            pageCount = 2;
            if (!TextUtils.isEmpty(catHomeData.pageTitle)) {
                setPageTitle(catHomeData.pageTitle);
            }
        } else {
            setUpPageTitle(categoryInfo.extraVlues);
            pageCount = 1;
            fetchData(true);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_tab_1:
                tabSelection = 1;
                break;

            case R.id.tv_tab_2:
                tabSelection = 2;
                break;

            case R.id.tv_tab_3:
                tabSelection = 3;
                break;
        }
        setTabView(tabSelection);
    }

    // Adding the title is available in the categoryInfo else it will be gone
    private void setUpPageTitle(String extraVlues) {
        String pageTitle = null;
        if (!TextUtils.isEmpty(extraVlues)) {
            try {
                if (new JSONObject(extraVlues).has("category_intro")) {
                    pageTitle = new JSONObject(extraVlues).optString("category_intro");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.pageTitle = pageTitle;

            setPageTitle(pageTitle);
        }
    }

    public void setPageTitle(String pageTitle) {
        TextView tvTitle = itemView.findViewById(R.id.tv_title);

        if (!TextUtils.isEmpty(pageTitle)) {
            this.pageTitle = pageTitle;
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(pageTitle);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                tvTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        } else {
            tvTitle.setVisibility(View.GONE);
        }
    }

    private void setTabView(int tabSelection) {
        guruvaniListAdapter.setTabSelection(tabSelection);

        videoTabView.setBackgroundResource(R.drawable.inner_tab_default_bg_2);
        subakyaTabView.setBackgroundResource(R.drawable.inner_tab_default_bg_2);
        programTabView.setBackgroundResource(R.drawable.inner_tab_default_bg_2);

//        videoTabView.setTextColor(Color.BLACK);
//        subakyaTabView.setTextColor(Color.BLACK);
//        programTabView.setTextColor(Color.BLACK);

        switch (tabSelection) {
            case 1:
                videoTabView.setBackgroundResource(R.drawable.inner_tab_selected_bg_2);
//                videoTabView.setTextColor(Color.WHITE);
                break;

            case 2:
                subakyaTabView.setBackgroundResource(R.drawable.inner_tab_selected_bg_2);
//                subakyaTabView.setTextColor(Color.WHITE);
                break;

            case 3:
                programTabView.setBackgroundResource(R.drawable.inner_tab_selected_bg_2);
//                programTabView.setTextColor(Color.WHITE);
                break;
        }
    }

    private void fetchData(boolean isClear) {
        fetchGuruDataFromServer(Urls.APP_FEED_BASE_URL + mFeedUrl + "PG" + pageCount + "/", isClear);
    }

    private void fetchGuruDataFromServer(String url, boolean isClear) {
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            isLoading = true;
            loadMoreProgressBar.setVisibility(View.VISIBLE);
            btn_retry.setVisibility(View.GONE);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loadMoreProgressBar.setVisibility(View.GONE);
                    isLoading = false;
                    parseOnlineData(response, isClear);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadMoreProgressBar.setVisibility(View.GONE);
                    isLoading = false;
                    if (isFromHome || isFromParentHome)
                        btn_retry.setVisibility(View.VISIBLE);
                }
            });

            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            if (!isFromHome && !isFromParentHome)
                VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(request);
            else
                VolleyNetworkSingleton.getInstance(mContext, 2).addToRequestQueue(request);
        } else {
            loadMoreProgressBar.setVisibility(View.GONE);
            isLoading = false;
            if (isFromHome || isFromParentHome)
                btn_retry.setVisibility(View.VISIBLE);
        }
    }


    private void parseOnlineData(JSONObject jsonObject, boolean isClear) {
        int count = jsonObject.optInt("count");
        JSONArray jsonArray = jsonObject.optJSONArray("Items");
        if (jsonArray != null) {
            ArrayList<Object> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), GuruInfo[].class)));

            if (pageCount <= 1 && !TextUtils.isEmpty(id)) {
                CatHomeData catHomeData = new CatHomeData(list, count, CatHomeData.TYPE_GURU, System.currentTimeMillis());
                if (!TextUtils.isEmpty(pageTitle)) {
                    catHomeData.pageTitle = pageTitle;
                }
                DataParser.getInstance().addCatHomeDataHashMap(id, catHomeData);
            }

            if (jsonArray.length() < count) {
                isLoadMoreEnabled = false;
            } else {
                isLoadMoreEnabled = true;
                pageCount++;
            }
            handleUI(list, isClear);
        }
    }

    private void parseOfflineData(CatHomeData catHomeData) {
        int count = catHomeData.count;
        ArrayList<Object> list = (ArrayList<Object>) catHomeData.getArrayList(catHomeData.TYPE_GURU);
        if (list != null) {
            if (list.size() < count) {
                isLoadMoreEnabled = false;
            } else {
                isLoadMoreEnabled = true;
            }
            handleUI(list, false);
        }
    }

    public void handleUI(ArrayList<Object> list, boolean isClear) {

        //passing the data to the adapter to show.
        List<Object> localList;
        if (showCount > 0 && showCount < list.size()) {
            guruvaniListAdapter.clear();
            localList = list.subList(0, showCount > list.size() ? list.size() : showCount);
            guruvaniListAdapter.add(localList);
            isLoadMoreEnabled = true;
        } else {
            if (isClear) {
                guruvaniListAdapter.clear();
            }
            guruvaniListAdapter.add(list);
            if (isFromHome) isLoadMoreEnabled = false;
        }

        // handling the load more button based on the data we got
        btn_retry.setVisibility(View.GONE);
        loadmoreLogic();
    }

    private void loadmoreLogic() {
        if (isFromHome) {
            btnSubHeaderLoadMore.setVisibility(View.VISIBLE);
        }

        btnSubHeaderLoadMore.setOnClickListener(v -> onLoadMore());

    }

    @Override
    public void onLoadFinish() {
        loadMoreProgressBar.setVisibility(View.GONE);
        //loadMoreTextView.setVisibility(View.VISIBLE);

    }

    public void onSwipeRefresh() {
        pageCount = 1;
        if (!isLoading)
            fetchData(true);
    }
}