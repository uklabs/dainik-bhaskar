package com.db.divya_new.guruvani.viewholder;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.LoadMoreController;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHomeData;
import com.db.divya_new.data.GuruInfo;
import com.db.divya_new.guruvani.list.GuruvaniTopicListAdapter;
import com.db.util.NetworkStatus;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class GuruvaniTopicViewHolder extends RecyclerView.ViewHolder implements LoadMoreController {

    public TextView titleTextView;
    public RecyclerView gridRecyclerView;
    public GuruvaniTopicListAdapter subjectListAdapter;
    public ProgressBar loadmoreprogressBar;
    public Button btn_retry;
    private int showCount;
    private boolean isFromHome;
    private String mParentCatId;
    private CategoryInfo categoryInfo;
    private Activity mContext;
    private String mFeedUrl;
    private String mSuperParentId;
    private String mParentId;
    private String id;
    private boolean isFromParentHome;
    private String pageTitle;
    private boolean isLoadMoreEnabled;
    private boolean isLoadMoreClicked = false;
    private boolean isLoading;
    private Button btnSubHeaderLoadMore;

    public GuruvaniTopicViewHolder(View itemView, final String mpSuperParentId, Activity activity) {
        super(itemView);
        mContext = activity;

        subjectListAdapter = new GuruvaniTopicListAdapter(mContext, mpSuperParentId);

        itemView.findViewById(R.id.tab_layout).setVisibility(View.GONE);
        itemView.findViewById(R.id.tv_title).setVisibility(View.GONE);

        btn_retry = itemView.findViewById(R.id.btn_retry);
        loadmoreprogressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        btnSubHeaderLoadMore = itemView.findViewById(R.id.btnSubHeaderLoadMore);
        gridRecyclerView = itemView.findViewById(R.id.recycler_view);
        gridRecyclerView.setLayoutManager(new GridLayoutManager(itemView.getContext(), 3));
        btn_retry.setOnClickListener(v -> {
            btn_retry.setVisibility(View.INVISIBLE);
            fetchData();
        });
    }

    private void onLoadMore() {
        if (isFromHome && isFromParentHome) {
            if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {

                loadmoreprogressBar.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TabController.getInstance().setTabClicked(true);
                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mParentId, GuruvaniTopicViewHolder.this);
                    }
                }, 200);
            }
        } else if (isFromHome) {
                        /*if (TabController.getInstance().getSubMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                            TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(mParentId);
                            TabController.getInstance().setTabClicked(true);
                        }*/
            if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                loadmoreprogressBar.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TabController.getInstance().setTabClicked(true);
                        TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(mParentId, GuruvaniTopicViewHolder.this);
                    }
                }, 200);
            }
        }
    }

    public void clearDataAndLoadNew() {
        subjectListAdapter.clear();
    }

    // Adding the title is available in the categoryInfo else it will be gone
    private void setUpPageTitle(String extraVlues) {
        String pageTitle = null;
        if (!TextUtils.isEmpty(extraVlues)) {
            try {
                if (new JSONObject(extraVlues).has("category_intro")) {
                    pageTitle = new JSONObject(extraVlues).optString("category_intro");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.pageTitle = pageTitle;

            setPageTitle(pageTitle);
        }
    }

    public void setPageTitle(String pageTitle) {
        TextView tvTitle = itemView.findViewById(R.id.tv_title);

        if (!TextUtils.isEmpty(pageTitle)) {
            this.pageTitle = pageTitle;
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(pageTitle);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                tvTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        } else {
            tvTitle.setVisibility(View.GONE);
        }
    }

    public void setData(CategoryInfo categoryInfo, boolean isRefreshing) {
        this.mFeedUrl = categoryInfo.feedUrl;
        this.showCount = 0;
        this.categoryInfo = categoryInfo;
        this.mSuperParentId = categoryInfo.parentId;
        this.isFromHome = false;
        this.id = this.mParentCatId = categoryInfo.id;

        subjectListAdapter.setData(categoryInfo);
        gridRecyclerView.setAdapter(subjectListAdapter);

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && !isRefreshing) {
            parseData(catHomeData);
        } else {
            if (!isLoading)
                fetchData();
        }



        setUpPageTitle(categoryInfo.extraVlues);
    }

    public void setData(CategoryInfo categoryInfo, int showCount, boolean isFromHome, boolean isFromParentHome, String pParentId, String mSuperParentId) {
        this.mFeedUrl = categoryInfo.feedUrl;
        this.categoryInfo = categoryInfo;
        this.showCount = showCount;
        this.isFromHome = isFromHome;
        this.isFromParentHome = isFromParentHome;
        this.mParentId = pParentId;
        this.id = categoryInfo.id;
        this.mSuperParentId = mSuperParentId;
        subjectListAdapter.setData(categoryInfo);

        if (isFromParentHome) {
            gridRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        } else {
            gridRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        }

        gridRecyclerView.setAdapter(subjectListAdapter);
        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue())) {
            parseData(catHomeData);
            if (!TextUtils.isEmpty(catHomeData.pageTitle)) {
                setPageTitle(catHomeData.pageTitle);
            }
        } else {
            setUpPageTitle(categoryInfo.extraVlues);
            fetchData();
        }

    }

    private void fetchData() {
        fetchTopicDataFromServer(Urls.APP_FEED_BASE_URL + mFeedUrl);
    }

    private void fetchTopicDataFromServer(String url) {
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            isLoading = true;
            btn_retry.setVisibility(View.GONE);
            loadmoreprogressBar.setVisibility(View.VISIBLE);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    parseData(response);
                    isLoading = false;
                    loadmoreprogressBar.setVisibility(View.GONE);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    btn_retry.setVisibility(View.VISIBLE);
                    isLoading = false;
                    loadmoreprogressBar.setVisibility(View.GONE);
                }
            });

            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            if (!isFromHome && !isFromParentHome)
                VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(request);
            else
                VolleyNetworkSingleton.getInstance(mContext, 2).addToRequestQueue(request);
        } else {
            btn_retry.setVisibility(View.VISIBLE);
            loadmoreprogressBar.setVisibility(View.GONE);
            isLoading = false;
        }
    }

    private void parseData(JSONObject jsonObject) {
        JSONArray jsonArray = jsonObject.optJSONArray("feed");
        if (jsonArray != null) {
            ArrayList<GuruInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), GuruInfo[].class)));

            if (!TextUtils.isEmpty(id)) {
                CatHomeData catHomeData = new CatHomeData(list, 0, CatHomeData.TYPE_GURU, System.currentTimeMillis());
                if (!TextUtils.isEmpty(pageTitle)) {
                    catHomeData.pageTitle = pageTitle;
                }
                DataParser.getInstance().addCatHomeDataHashMap(id, catHomeData);
            }
            handleUI(list);
        }
    }


    private void parseData(CatHomeData catHomeData) {
        ArrayList<GuruInfo> list = (ArrayList<GuruInfo>) catHomeData.getArrayList(CatHomeData.TYPE_GURU);
        if (list != null) {
            handleUI(list);
        }
    }

    private void handleUI(ArrayList<GuruInfo> list) {
        ArrayList<GuruInfo> localList = new ArrayList<>();

        btn_retry.setVisibility(View.GONE);

        if (showCount > 0 && showCount < list.size()) {
            for (int i = 0; i < showCount; i++) {
                localList.add(list.get(i));
            }
            subjectListAdapter.add(localList);
            isLoadMoreEnabled = true;
        } else {
            subjectListAdapter.add(list);
            if (isFromHome) isLoadMoreEnabled = false;
        }

        // handling the load more button based on the data we got

        btn_retry.setVisibility(View.GONE);
        loadmoreLogic();
    }


    private void loadmoreLogic() {
        if(isFromHome){
            btnSubHeaderLoadMore.setVisibility(View.VISIBLE);
        }

        btnSubHeaderLoadMore.setOnClickListener(v-> onLoadMore());

    }

    @Override
    public void onLoadFinish() {
        loadmoreprogressBar.setVisibility(View.GONE);
        //loadMoreTextView.setVisibility(View.VISIBLE);
    }
}
