package com.db.divya_new.guruvani.viewholder.sub;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.divya_new.data.TabInfo;
import com.db.util.Systr;

import java.util.ArrayList;

public class DropDownListAdapter extends ArrayAdapter<TabInfo> {

    public interface OnItemSelectListener {
        void onSelect();
    }

    private ArrayList<TabInfo> mListItems;
    private ArrayList<TabInfo> mSelectedItems;
    private String mHeaderText;

    private LayoutInflater mInflater;
    private OnItemSelectListener mListener;

    public DropDownListAdapter(Context context, ArrayList<TabInfo> items, String headerText, OnItemSelectListener listener) {
        super(context, 0, items);

        mListItems = new ArrayList<>();
        mSelectedItems = new ArrayList<>();
        mListener = listener;

        mHeaderText = headerText;
        mListItems.addAll(items);
        mInflater = LayoutInflater.from(context);
    }

    public ArrayList<TabInfo> getSelectedItems() {
        return mSelectedItems;
    }

//    public void removeSelectedItem(int pos) {
//        mSelectedItems.remove(pos);
//        notifyDataSetChanged();
//
//        if (mListener != null) {
//            mListener.onSelect();
//        }
//    }

    public void removeSelectedItem(TabInfo pos) {
        mSelectedItems.remove(pos);
        notifyDataSetChanged();

        if (mListener != null) {
            mListener.onSelect();
        }
    }

//    @Override
//    public int getCount() {
////        return mListItems.size();
//        return mListItems.size() + 1;
//    }
//
//    @Override
//    public Object getItem(int arg0) {
//        return mListItems.get(arg0);
//    }
//
//    @Override
//    public long getItemId(int arg0) {
//        return 0;
//    }

    private boolean isFromView;

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_multiple_check, null);
            holder = new ViewHolder();
            holder.tv = convertView.findViewById(R.id.textview);
            holder.chkbox = convertView.findViewById(R.id.checkbox);
            holder.checkLayout = convertView.findViewById(R.id.check_layout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Systr.println("DropDown : pos : " + position);
        if (position < 1) {
            holder.chkbox.setVisibility(View.GONE);
            holder.tv.setText(mHeaderText);
        } else {
            final int listPos = position - 1;
//        final int listPos = position;
            holder.chkbox.setVisibility(View.VISIBLE);
            holder.tv.setText(mListItems.get(listPos).title);

            final TabInfo item = mListItems.get(listPos);
//            isFromView = true;
//            boolean isSel = mSelectedItems.contains(item);
//            isFromView = false;

//            holder.chkbox.setOnCheckedChangeListener(null);
//            holder.chkbox.setChecked(isSel);
//
//            holder.chkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (!isFromView) {
//                        if (isChecked) {
//                            mSelectedItems.add(item);
//                        } else {
//                            mSelectedItems.remove(item);
//                        }
//
//                        if (mListener != null) {
//                            mListener.onSelect();
//                        }
//                    }
//                }
//            });

//            holder.tv.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    holder.chkbox.toggle();
//                }
//            });

            holder.checkLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mSelectedItems.contains(item)) {
                        Systr.println("DropDown : remove item : " + item.toString());
                        mSelectedItems.remove(item);
                        holder.chkbox.setChecked(false);
                    } else {
                        Systr.println("DropDown : add item : " + item.toString());
                        mSelectedItems.add(item);
                        holder.chkbox.setChecked(true);
                    }

                    if (mListener != null) {
                        mListener.onSelect();
                    }
                }
            });
        }

        return convertView;
    }

    private class ViewHolder {
        TextView tv;
        CheckBox chkbox;
        RelativeLayout checkLayout;
    }
}