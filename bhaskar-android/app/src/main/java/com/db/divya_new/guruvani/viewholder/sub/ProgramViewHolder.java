package com.db.divya_new.guruvani.viewholder.sub;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.divya_new.data.ScheduleInfo;
import com.db.divya_new.guruvani.article.GuruvaniProgramListAdapter;
import com.db.util.DividerItemDecoration;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class ProgramViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private GuruvaniProgramListAdapter guruvaniProgramListAdapter;

    private TextView loadMoreTextView;
    private TextView titleTextView;
    private RecyclerView recyclerView;
    private ProgressBar loadMoreProgressBar;

    private String mGuruId;
    private String mFeedUrl;

    private Context mContext;

    private int pageCount = 1;
    private boolean isLoading;
    private boolean isLoadMoreEnabled = true;

    public ProgramViewHolder(View itemView, String feedUrl, String guruId) {
        super(itemView);
        mGuruId = guruId;
        mFeedUrl = feedUrl;
        mContext = itemView.getContext();

        titleTextView = itemView.findViewById(R.id.tv_title);
        loadMoreTextView = itemView.findViewById(R.id.tv_load_more);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);

        guruvaniProgramListAdapter = new GuruvaniProgramListAdapter(itemView.getContext());
        recyclerView = itemView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(itemView.getContext()));
        recyclerView.setAdapter(guruvaniProgramListAdapter);

        // load more button clicked, to add more data in list
        loadMoreTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLoading) {
                    loadMoreTextView.setVisibility(View.GONE);
                    loadMoreProgressBar.setVisibility(View.VISIBLE);
                    fetchData(false);
                }
            }
        });

        loadMoreProgressBar.setVisibility(View.VISIBLE);
        fetchData(true);
    }

    @Override
    public void onClick(View view) {
    }

    private void fetchData(boolean isClear) {
        try {
            fetchGuruDataFromServer(Urls.APP_FEED_BASE_URL + mFeedUrl + mGuruId + "/PG" + pageCount + "/", isClear);
        } catch (Exception e) {
        }
    }

    private void fetchGuruDataFromServer(String url, final boolean isClear) {
        isLoading = true;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,null, response -> {
            loadMoreProgressBar.setVisibility(View.GONE);
            parseData(response, isClear);
            isLoading = false;
        }, error -> {
            isLoading = false;
            loadMoreProgressBar.setVisibility(View.GONE);

            if (isLoadMoreEnabled)
                loadMoreTextView.setVisibility(View.VISIBLE);
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void parseData(JSONObject jsonObject, boolean isClear) {
        String title = jsonObject.optString("title");
        if (!TextUtils.isEmpty(title)) {
            titleTextView.setText(title);
        }

        int count = jsonObject.optInt("count");

        JSONArray jsonArray = jsonObject.optJSONArray("feed");
        if (jsonArray != null) {
            ArrayList<ScheduleInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), ScheduleInfo[].class)));

            if (isClear) {
                guruvaniProgramListAdapter.addAndClearList(list);
            } else {
                guruvaniProgramListAdapter.addList(list);
            }

            if (jsonArray.length() < count) {
                isLoading = false;
                isLoadMoreEnabled = false;
                loadMoreTextView.setVisibility(View.GONE);
            } else {
                pageCount += 1;
                loadMoreTextView.setVisibility(View.VISIBLE);
            }
        }
    }
}