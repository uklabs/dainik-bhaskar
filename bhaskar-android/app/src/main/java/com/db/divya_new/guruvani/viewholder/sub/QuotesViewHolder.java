package com.db.divya_new.guruvani.viewholder.sub;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.divya_new.data.QuotesInfo;
import com.db.divya_new.data.TabInfo;
import com.db.divya_new.guruvani.article.GuruvaniQuotesListAdapter;
import com.db.util.DividerItemDecoration;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.nex3z.flowlayout.FlowLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class QuotesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private GuruvaniQuotesListAdapter quotesListAdapter;

    private RecyclerView recyclerView;
    private TextView dataNotExistTextView;
    private TextView loadMoreTextView;
    private ProgressBar loadMoreProgressBar;
    private TextView spinnerTextView;
    //    private Spinner spinner;
//    private FlexboxLayout flexbox;
    private FlowLayout flowLayout;

    ArrayList<TabInfo> mDropDownList;
    //    private PopupWindow popupQuotes;
    private Activity mContext;

    private String mGuruId;
    private String mGuruName;
    private String mFeedUrl;
    private String mDisplayName;
    private String mGaScreen;

    private boolean isLoading;
    private int pageCount = 1;
    private boolean isLoadMoreEnabled = true;
//    private DropDownListAdapter adapter;
//    private ChipCloud chipCloud;

    private String selectedIds;

    public QuotesViewHolder(View itemView, String feedUrl, String displayName, String guruId, String guruName, String gaScreen, Activity activity) {
        super(itemView);
        mContext = activity;
        mGuruId = guruId;
        mGuruName = guruName;
        mFeedUrl = feedUrl;
        mDisplayName = displayName;
        mGaScreen = gaScreen;

        quotesListAdapter = new GuruvaniQuotesListAdapter(mContext, mGuruName, mDisplayName, mGaScreen);

        spinnerTextView = itemView.findViewById(R.id.tv_spinner_textview);
        spinnerTextView.setText(mContext.getString(R.string.all_topics_txt));
//        spinner = itemView.findViewById(R.id.tv_spinner);
        flowLayout = itemView.findViewById(R.id.section_label_layout);

        dataNotExistTextView = itemView.findViewById(R.id.tv_data_not_exist);
        loadMoreTextView = itemView.findViewById(R.id.tv_load_more);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);

        recyclerView = itemView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(itemView.getContext()));
        recyclerView.setAdapter(quotesListAdapter);

        // load more button clicked, to add more data in list
        loadMoreTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLoading) {
                    loadMoreTextView.setVisibility(View.GONE);
                    loadMoreProgressBar.setVisibility(View.VISIBLE);
                    fetchData(false);
                }
            }
        });

//        initializeChipsLayout();

        spinnerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListingDialog();
            }
        });

//        spinnerTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//            }
//        });

        loadMoreProgressBar.setVisibility(View.VISIBLE);
        fetchData(true);
    }

    private void checkForData() {
        if (quotesListAdapter.getItemCount() <= 0) {
            dataNotExistTextView.setVisibility(View.VISIBLE);
            dataNotExistTextView.setText(mContext.getString(R.string.no_record_found_txt));
        } else {
            dataNotExistTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
    }

    private void fetchData(boolean isClear) {

        try {
            String url;
            if (TextUtils.isEmpty(selectedIds)) {
                url = Urls.APP_FEED_BASE_URL + mFeedUrl + mGuruId + "/PG" + pageCount + "/";
            } else {
                url = Urls.APP_FEED_BASE_URL + mFeedUrl + mGuruId + "/" + selectedIds + "/PG" + pageCount + "/";
            }
            fetchGuruDataFromServer(url, isClear);
        } catch (Exception e) {
        }
    }

    private void fetchGuruDataFromServer(String url, final boolean isClear) {
        Systr.println("URL : " + url);
        isLoading = true;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,null, response -> {
            loadMoreProgressBar.setVisibility(View.GONE);
            parseData(response, isClear);
            isLoading = false;

            checkForData();
        }, error -> {
            isLoading = false;
            loadMoreProgressBar.setVisibility(View.GONE);

            if (isLoadMoreEnabled)
                loadMoreTextView.setVisibility(View.VISIBLE);

            checkForData();
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void parseData(JSONObject jsonObject, boolean isClear) {
        int count = jsonObject.optInt("count");
        JSONArray jsonArray = jsonObject.optJSONArray("feed");
        if (jsonArray != null) {
            ArrayList<QuotesInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), QuotesInfo[].class)));

            if (isClear) {
                quotesListAdapter.addAndClearList(list);
            } else {
                quotesListAdapter.addList(list);
            }

            if (jsonArray.length() < count) {
                isLoading = false;
                isLoadMoreEnabled = false;
                loadMoreTextView.setVisibility(View.GONE);
            } else {
                pageCount += 1;
                loadMoreTextView.setVisibility(View.VISIBLE);
            }
        }

        if (mDropDownList == null || mDropDownList.size() <= 0) {
            JSONArray topicJsonArray = jsonObject.optJSONArray("topics");
            if (topicJsonArray != null) {
                ArrayList<TabInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(topicJsonArray.toString(), TabInfo[].class)));
                if (list.size() > 0) {
                    mDropDownList = list;

                    setSpinner();
                }
            }
        }
    }

    String[] animals = null;
    boolean[] checkedItems = null;

    private void setSpinner() {
        Systr.println("DropDown : set spinner");

        animals = new String[mDropDownList.size()];
        checkedItems = new boolean[mDropDownList.size()];

        for (int i = 0; i < mDropDownList.size(); i++) {
            animals[i] = mDropDownList.get(i).title;
            checkedItems[i] = false;
        }
    }

    private void addMenu(final TabInfo str, final int pos) {
        final View v = View.inflate(mContext, R.layout.listitem_chip_layout, null);
        TextView title = v.findViewById(R.id.title_tv);
        LinearLayout footer_category_RL = v.findViewById(R.id.chip_layout);
        title.setText(str.toString());

        footer_category_RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                adapter.removeSelectedItem(str);
                checkedItems[pos] = false;
                flowLayout.removeView(v);
                addLayoutItems();
            }
        });

        flowLayout.addView(v);
    }

    private void showListingDialog() {
        String header = "બધા વિષય";
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//        builder.setTitle(header);

        View titleView = LayoutInflater.from(mContext).inflate(R.layout.listitem_title, null);
        ((TextView) titleView.findViewById(R.id.tv_title)).setText(header);
        builder.setCustomTitle(titleView);

        builder.setMultiChoiceItems(animals, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                // user checked or unchecked a box
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // user clicked OK
                addLayoutItems();
            }
        });
        builder.setNegativeButton("Cancel", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void addLayoutItems() {
        flowLayout.removeAllViews();
        selectedIds = "";
        if(mDropDownList != null) {
            for (int i = 0; i < mDropDownList.size(); i++) {
                if (checkedItems[i]) {
                    addMenu(mDropDownList.get(i), i);
                    selectedIds += mDropDownList.get(i).id + "-";
                }
            }
        }
        if (selectedIds.length() > 2)
            selectedIds = selectedIds.substring(0, selectedIds.length() - 1);

        pageCount = 1;
        fetchData(true);
    }
}