package com.db.divya_new.guruvani.viewholder.sub;

import android.content.Context;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.divya_new.data.TabInfo;
import com.db.divya_new.guruvani.article.GuruvaniVideoListAdapter;
import com.db.divya_new.guruvani.article.GuruvaniVideoTabAdapter;
import com.bhaskar.appscommon.tracking.Tracking;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.DividerItemDecoration;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.AppUtils;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class VideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public static final int TYPE_TOPIC = 1;
    public static final int TYPE_GURU = 2;

    private GuruvaniVideoTabAdapter guruvaniVideoMenuAdapter;
    private GuruvaniVideoListAdapter guruvaniVideoListAdapter;

    private ProgressBar tabProgressBar;
    private ProgressBar loadMoreProgressBar;
    private TextView loadMoreTextView;
    private TextView retryTextView;
    private RecyclerView recyclerView;
    private RecyclerView horizontalRecyclerView;

    private Context mContext;
    private TabInfo selectedTabInfo;

    private String mFeedUrl;
    private String mGuruNTopicId;
    private String mGuruNTopicName;
    private int mType;

    private String mGaScreen;
    private String mGaArticle;

    private boolean isLoading;
    private int pageCount = 1;
    private boolean isLoadMoreEnabled = true;

    public VideoViewHolder(View itemView, String feedUrl, String guruNTopicId, String guruNTopicName, int type, String gaScreen, String gaArticle, String gaEventLabel, String sectionLabel, String colorCode) {
        super(itemView);
        mContext = itemView.getContext();
        mFeedUrl = feedUrl;
        mGuruNTopicId = guruNTopicId;
        mGuruNTopicName = guruNTopicName;
        mType = type;
        mGaScreen = gaScreen;

        guruvaniVideoMenuAdapter = new GuruvaniVideoTabAdapter(mContext, new GuruvaniVideoTabAdapter.OnTabClickLitener() {
            @Override
            public void onTabClick(TabInfo info) {
                pageCount = 1;
                selectedTabInfo = info;

                tabProgressBar.setVisibility(View.VISIBLE);

                setIds();
                fetchData(true);

                // send broadcast to update title on header
                Intent intent = new Intent(Constants.IntentFilterType.FILTER_TITLE);
                intent.putExtra(Constants.IntentExtra.TITLE, selectedTabInfo.title);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                sendTracking();
            }
        });

        tabProgressBar = itemView.findViewById(R.id.tabprogressBar);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        loadMoreTextView = itemView.findViewById(R.id.tv_load_more);
        retryTextView = itemView.findViewById(R.id.tv_retry);

        horizontalRecyclerView = itemView.findViewById(R.id.horizontal_recycler_view);
        horizontalRecyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false));
        horizontalRecyclerView.setAdapter(guruvaniVideoMenuAdapter);

        guruvaniVideoListAdapter = new GuruvaniVideoListAdapter(itemView.getContext(), gaArticle, gaEventLabel, sectionLabel, colorCode);
        setIds();

        recyclerView = itemView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(itemView.getContext()));
        recyclerView.setAdapter(guruvaniVideoListAdapter);

        // load more button clicked, to add more data in list
        loadMoreTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLoading) {
                    loadMoreTextView.setVisibility(View.GONE);
                    loadMoreProgressBar.setVisibility(View.VISIBLE);
                    fetchData(false);
                }
            }
        });

        retryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLoading) {
                    retryTextView.setVisibility(View.GONE);
                    loadMoreTextView.setVisibility(View.GONE);
                    loadMoreProgressBar.setVisibility(View.VISIBLE);
                    fetchData(true);
                }
            }
        });

        loadMoreProgressBar.setVisibility(View.VISIBLE);
        fetchData(true);

        sendTracking();
    }

    private void sendTracking() {
        if (mType == TYPE_TOPIC) {
            String source = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            String value = mGaScreen + "-" + (selectedTabInfo == null ? mGuruNTopicName : selectedTabInfo.name);
            Tracking.trackGAScreen(mContext, InitApplication.getInstance().getDefaultTracker(), value, source, medium, campaign);
        }
    }

    private void setIds() {
        String guruIdRec;
        String topicIdRec;

        if (mType == TYPE_GURU) {
            guruIdRec = mGuruNTopicId;
            topicIdRec = selectedTabInfo == null ? "0" : selectedTabInfo.id;
        } else if (mType == TYPE_TOPIC) {
            guruIdRec = "0";
            topicIdRec = selectedTabInfo == null ? mGuruNTopicId : selectedTabInfo.id;
        } else {
            guruIdRec = "0";
            topicIdRec = "0";
        }

        if (guruvaniVideoListAdapter != null) {
            String name = mGuruNTopicName;
            if (mType == TYPE_TOPIC) {
                name = (selectedTabInfo == null ? mGuruNTopicName : selectedTabInfo.name);
            }
            guruvaniVideoListAdapter.setIds(guruIdRec, topicIdRec, name);
        }
    }

    @Override
    public void onClick(View view) {
    }

    private void fetchData(boolean isClear) {
        try {
            String url = Urls.APP_FEED_BASE_URL + mFeedUrl;

            if (mType == TYPE_GURU) {
                url += mGuruNTopicId + "/";
                if (selectedTabInfo != null) {
                    url += selectedTabInfo.id + "/PG" + pageCount + "/";
                } else {
                    url += "PG" + pageCount + "/";
                }
            } else {
                if (selectedTabInfo != null) {
                    url += selectedTabInfo.id + "/PG" + pageCount + "/";
                } else {
                    url += mGuruNTopicId + "/PG" + pageCount + "/";
                }
            }

            fetchGuruDataFromServer(url, isClear);

        } catch (Exception e) {
        }
    }

    private void fetchGuruDataFromServer(String url, final boolean isClear) {
        isLoading = true;

        Systr.println("VIDEO URL : " + url);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,null, response -> {
            tabProgressBar.setVisibility(View.GONE);
            loadMoreProgressBar.setVisibility(View.GONE);
            parseData(response, isClear);
            isLoading = false;
        }, error -> {
            isLoading = false;
            tabProgressBar.setVisibility(View.GONE);
            loadMoreProgressBar.setVisibility(View.GONE);

            if (isLoadMoreEnabled)
                loadMoreTextView.setVisibility(View.VISIBLE);

            if (isClear) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                guruvaniVideoListAdapter.addAndClearList(new ArrayList<VideoInfo>());

                loadMoreTextView.setVisibility(View.GONE);
                retryTextView.setVisibility(View.VISIBLE);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(request);
    }

    private void parseData(JSONObject jsonObject, boolean isClear) {
        int count = jsonObject.optInt("count");
        JSONArray jsonArray = jsonObject.optJSONArray("feed");
        if (jsonArray != null) {
            ArrayList<VideoInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), VideoInfo[].class)));

            if (isClear) {
                guruvaniVideoListAdapter.addAndClearList(list);
            } else {
                guruvaniVideoListAdapter.addList(list);
            }

            if (jsonArray.length() < count) {
                isLoading = false;
                isLoadMoreEnabled = false;
                loadMoreTextView.setVisibility(View.GONE);
            } else {
                pageCount += 1;
                loadMoreTextView.setVisibility(View.VISIBLE);
            }
        }

        JSONArray topicJsonArray = jsonObject.optJSONArray("topics");
        if (topicJsonArray != null) {
            ArrayList<TabInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(topicJsonArray.toString(), TabInfo[].class)));

            int selectedPos = -1;
            if (mType == TYPE_TOPIC && selectedTabInfo == null) {
                TabInfo tabInfo = new TabInfo();
                tabInfo.id = mGuruNTopicId;
                selectedPos = list.indexOf(tabInfo);
            }
            if (list.size() > 0) {
                if (selectedPos != -1)
                    guruvaniVideoMenuAdapter.setSelectedPos(selectedPos);
                guruvaniVideoMenuAdapter.add(list);
                if (selectedPos > 0)
                    horizontalRecyclerView.scrollToPosition(selectedPos);
            }
        }
    }
}