package com.db.divya_new.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.util.CommonConstants;
import com.db.InitApplication;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.database.DatabaseClient;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.divyashree.NewsFlickerActivity;
import com.db.news.WapV2Activity;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class FlickerViewHolder extends RecyclerView.ViewHolder {
    private Activity mContext;
    private ArrayList<NewsListInfo> mList;
    private ViewFlipper mViewFlipper;

    private String themeColor, subjectColor;
    private CardView cardView;
    private TextView tvHeaderText;
    private TextView tvHeaderSubText;
    private String textColor;
    private CategoryInfo categoryInfo;
    private String titleText;
    private View itemView;
    private View seperatorView;
    private String feedUrl;
    private Handler myHandler = new Handler();
    private final int INTERVAL = 3500;
    private Runnable flipController = this::startTimer;
    private Timer timer;

    public FlickerViewHolder(final View itemView, Activity context, String catId, final boolean isPhotoFlicker) {
        super(itemView);
        mContext = context;
        this.itemView = itemView;
        this.categoryInfo = DatabaseClient.getInstance(context).getCategoryById(catId);

        subjectColor = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_SUBJECT_COLOR, "#d7e1e4");
        themeColor = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_COLOR, "#d7e1e4");
        textColor = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_TEXT_COLOR, "#000000");
        feedUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_API, Urls.APP_FEED_BASE_URL + String.format(Urls.FLICKER_WIDGET_URL, CommonConstants.CHANNEL_ID));
        tvHeaderText = itemView.findViewById(R.id.title_textview);
        tvHeaderSubText = itemView.findViewById(R.id.tvHeaderSubText);
        seperatorView = itemView.findViewById(R.id.separator_title);

        ImageView ivPrev = itemView.findViewById(R.id.ivPrev);
        ImageView ivNext = itemView.findViewById(R.id.ivNext);
        itemView.findViewById(R.id.right_arrow_iv).setOnClickListener(v -> {
            if (mList != null)
                mContext.startActivity(NewsFlickerActivity.getIntent(mContext, mList, categoryInfo, titleText, isPhotoFlicker, mViewFlipper.getDisplayedChild()));
        });
        ivNext.setOnClickListener(v -> {
            mViewFlipper.stopFlipping();
            myHandler.removeCallbacks(flipController);
            if (timer != null) {
                timer.cancel();
                timer.purge();
            }
            mViewFlipper.setInAnimation(itemView.getContext(), R.anim.slide_from_right);
            mViewFlipper.setOutAnimation(itemView.getContext(), R.anim.slide_to_left);
            mViewFlipper.showNext();

            myHandler.postDelayed(flipController, INTERVAL);
        });

        ivPrev.setOnClickListener(v -> {
            mViewFlipper.stopFlipping();
            myHandler.removeCallbacks(flipController);
            if (timer != null) {
                timer.cancel();
                timer.purge();
            }

            mViewFlipper.setInAnimation(itemView.getContext(), R.anim.slide_from_left);
            mViewFlipper.setOutAnimation(itemView.getContext(), R.anim.slide_to_right);
            mViewFlipper.showPrevious();
            myHandler.postDelayed(flipController, INTERVAL);
        });

        seperatorView.setBackgroundColor(AppUtils.getThemeColor(mContext, textColor));
        tvHeaderText.setTextColor(AppUtils.getThemeColor(mContext, textColor));
        tvHeaderSubText.setTextColor(AppUtils.getThemeColor(mContext, textColor));
        mViewFlipper = itemView.findViewById(R.id.view_flipper);
        cardView = itemView.findViewById(R.id.related_article_recycler_view_item_rl);
        defaultAnimation();

        itemView.setOnClickListener(v -> {
            if (mList != null && mList.size() > 0) {
                NewsListInfo newsListInfo = mList.get(mViewFlipper.getDisplayedChild());
                launchArticlePage(newsListInfo);
            }
        });
        itemView.findViewById(R.id.loadmoreprogressBar).setVisibility(View.VISIBLE);
        cardView.setCardBackgroundColor(AppUtils.getThemeColor(context, themeColor));
        makeJsonObjectRequest();
    }

    public void pauseTimer(){
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }
    public void resumeTimer(){
        Log.v("TAG","resume timer");
    }



    private void startTimer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mContext.runOnUiThread(() -> {
                    defaultAnimation();
                    mViewFlipper.showNext();
                });
            }
        }, INTERVAL / 2, INTERVAL);
    }


    private void defaultAnimation() {
        mViewFlipper.setInAnimation(itemView.getContext(), R.anim.slide_from_right);
        mViewFlipper.setOutAnimation(itemView.getContext(), R.anim.slide_to_left);
    }

    private void launchArticlePage(NewsListInfo newsListInfo) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(categoryInfo.gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : categoryInfo.gaScreen) + newsListInfo.webUrl);
            mContext.startActivity(intent);
        } else {
            categoryInfo.gaArticle = AppFlyerConst.GAScreen.FLICKER_WIDGET_HOME_GA_ARTICLE;
            mContext.startActivityForResult(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo), Constants.REQ_CODE_BOTTOM_NAV);
        }
    }


    private void sendTracking(String gaScreen) {
        String souce = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(mContext, InitApplication.getInstance().getDefaultTracker(), gaScreen, souce, medium, campaign);
    }

    private void makeJsonObjectRequest() {
        Systr.println("Feed Url : " + feedUrl);

        if (NetworkStatus.getInstance().isConnected(mContext)) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(feedUrl), null,
                    response -> {
                        try {
                            if (response != null) {
                                parseFeedList(response);
                            } else {
                                itemView.findViewById(R.id.loadmoreprogressBar).setVisibility(View.GONE);
                                cardView.setVisibility(View.GONE);
                            }
                        } catch (Exception ignored) {
                            itemView.findViewById(R.id.loadmoreprogressBar).setVisibility(View.GONE);
                            cardView.setVisibility(View.GONE);
                        }
                    }, error -> {
                itemView.findViewById(R.id.loadmoreprogressBar).setVisibility(View.GONE);
                cardView.setVisibility(View.GONE);
                if (error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
        }
    }

    private void parseFeedList(JSONObject response) throws JSONException {
//        int count = response.optInt("count");
        cardView.setVisibility(View.VISIBLE);
        String subTitleText = response.optString("sub_title");
        if (!TextUtils.isEmpty(subTitleText)) {
            seperatorView.setVisibility(View.GONE);
        }
        tvHeaderSubText.setText(subTitleText);

        titleText = response.optString("title");
        tvHeaderText.setText(titleText);

        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        if (resultArray != null) {
            mList = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), NewsListInfo[].class)));
            itemView.findViewById(R.id.header_layout).setVisibility(View.VISIBLE);
            itemView.findViewById(R.id.loadmoreprogressBar).setVisibility(View.GONE);
            handleUI();
        }
    }

    private void handleUI() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        for (int mPosition = 0; mPosition < mList.size(); mPosition++) {
            View view = inflater.inflate(R.layout.flipper_item, null);
            ImageView storyIv = view.findViewById(R.id.story_iv);
//            float ratio = AppUtils.getInstance().parseImageRatio(mList.get(mPosition).imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
            AppUtils.getInstance().setImageViewSizeWithAspectRatio(storyIv, Constants.ImageRatios.FLICKER_RATIO, 28, mContext);

            TextView titleTv = view.findViewById(R.id.title_tv);
            if (TextUtils.isEmpty(mList.get(mPosition).widgetTitle)) {
                titleTv.setVisibility(View.GONE);
            } else {
                titleTv.setVisibility(View.VISIBLE);
                titleTv.setText(AppUtils.getInstance().getHomeTitleNew(mList.get(mPosition).iitl_title, AppUtils.getInstance().fromHtml(mList.get(mPosition).widgetTitle), subjectColor));
                titleTv.setTextColor(AppUtils.getThemeColor(mContext, textColor));
            }

            ImageUtil.setImage(mContext, mList.get(mPosition).image, storyIv, R.drawable.water_mark_news_detail);
            mViewFlipper.addView(view);
        }

//        mViewFlipper.startFlipping();
        startTimer();
    }
}
