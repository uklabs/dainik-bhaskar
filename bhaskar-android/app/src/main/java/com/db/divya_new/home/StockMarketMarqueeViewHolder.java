package com.db.divya_new.home;

import android.content.Context;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.StockListInfo;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.util.CssConstants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class StockMarketMarqueeViewHolder extends RecyclerView.ViewHolder {
    private Context mContext;
    private CategoryInfo categoryInfo;
    private ArrayList<StockListInfo> mList;
    private ProgressBar progressBar;
    private WebView webViewTicker;

    private String feedUrl;
    //private boolean loading = true;

    public StockMarketMarqueeViewHolder(final View itemView, final Context context, CategoryInfo categoryInfo) {
        super(itemView);
        mContext = context;
        this.categoryInfo = categoryInfo;
        mList = new ArrayList<>();
        progressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        webViewTicker = itemView.findViewById(R.id.webViewTicker);
        webViewTicker.setOnLongClickListener(v -> true);
        webViewTicker.setLongClickable(false);
        webViewTicker.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webViewTicker.getSettings().setJavaScriptEnabled(true);
        webViewTicker.setVerticalScrollBarEnabled(false);
        webViewTicker.setHorizontalScrollBarEnabled(false);
        // Below line prevent vibration on Long click
        webViewTicker.setHapticFeedbackEnabled(false);
        // webView set cachMode When Network available or Not
        webViewTicker.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webViewTicker.getSettings().setUserAgentString(CommonConstants.USER_AGENT);
        // webView method call with java script
        // app setting icon
        // Manage retry option when network not available  or page not load
        webViewTicker.setFocusable(false);
        webViewTicker.setFocusableInTouchMode(false);
        feedUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.StockPrefs.STOCK_API, "");
        fetchData();
    }

    public void setData() {
        fetchData();
    }

    private void fetchData() {
        makeJsonObjectRequest(feedUrl);
    }

    private void makeJsonObjectRequest(String finalFeedURL) {
        Systr.println("Feed Url : " + finalFeedURL);
        progressBar.setVisibility(View.VISIBLE);
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            if (response != null) {
                                progressBar.setVisibility(View.GONE);
                                parseFeedList(response);
                            }
                        } catch (Exception ignored) {
                        }
                    }, error -> {
                progressBar.setVisibility(View.GONE);

                if (error instanceof NoConnectionError || error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
        }
    }

    private void parseFeedList(JSONArray resultArray) throws Exception {
        if (resultArray != null) {
            mList.clear();
            mList.addAll(Arrays.asList(new Gson().fromJson(resultArray.toString(), StockListInfo[].class)));
            // marqueeAdapter.notifyDataSetChanged();
            //autoScroll(recyclerView);
            String downArrow = "<font color='#d50000' >&#9660</font>";
            String upArrow = "<font color='#2e7d32' >&#9650</font>";
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < mList.size(); i++) {

                StockListInfo stockListInfo = mList.get(i);
                boolean isMarketUp1 = stockListInfo.arrow.equalsIgnoreCase("uparo");
                String color = (isMarketUp1 ? "#2e7d32" : "#d50000");
                String arrow = (isMarketUp1 ? upArrow : downArrow);
                if (i > 0) {
                    stringBuilder.append(" | ");
                }
                stringBuilder.append(String.format("%s %s %s %s ", arrow, stockListInfo.name, stockListInfo.lastTradePrice, "<font color='" + color + "'>" + stockListInfo.netChange + "</font>"));

            }
            String finalString = stringBuilder.toString();
            webViewTicker.loadDataWithBaseURL("", CssConstants.getStockTickerContent(finalString), "text/html; charset=utf-8", null, "");

        }
    }


}
