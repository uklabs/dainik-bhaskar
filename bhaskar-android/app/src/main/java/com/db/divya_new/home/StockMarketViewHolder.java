package com.db.divya_new.home;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bhaskar.R;
import com.db.data.models.StockListInfo;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class StockMarketViewHolder extends RecyclerView.ViewHolder {
    private Context mContext;
    private ArrayList<StockListInfo> mList;
    private TextView tvStock1;
    private TextView tvStock2;
    private TextView tvStockName1;
    private TextView tvStockName2;
    private TextView tvStockAmount1;
    private TextView tvStockAmount2;
    private LinearLayout stockLayout;
    //    private ImageView ivArrow1;
//    private ImageView ivArrow2;
    private ProgressBar progressBar;


    public StockMarketViewHolder(final View itemView, Context context) {
        super(itemView);
        mContext = context;
        tvStock1 = itemView.findViewById(R.id.tvStock1);
        tvStock2 = itemView.findViewById(R.id.tvStock2);
        tvStockAmount1 = itemView.findViewById(R.id.tvStockAmount1);
        tvStockAmount2 = itemView.findViewById(R.id.tvStockAmount2);
        tvStockName1 = itemView.findViewById(R.id.tvStockName1);
        tvStockName2 = itemView.findViewById(R.id.tvStockName2);
        stockLayout = itemView.findViewById(R.id.stock_layout);
        progressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        fetchData();
    }

    public void setData() {
        fetchData();
    }

    private void fetchData() {
        String feedUrl;
        feedUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.StockPrefs.STOCK_API, "");
        makeJsonObjectRequest(feedUrl);
    }

    private void makeJsonObjectRequest(String finalFeedURL) {
        Systr.println("Feed Url : " + finalFeedURL);
        progressBar.setVisibility(View.VISIBLE);
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL),null,
                    response -> {
                        try {
                            if (response != null) {

                                progressBar.setVisibility(View.GONE);
                                stockLayout.setVisibility(View.VISIBLE);
                                parseFeedList(response);
                            }
                        } catch (Exception ignored) {
                        }
                    }, error -> {
                        progressBar.setVisibility(View.GONE);

                        if (error instanceof NoConnectionError || error instanceof NetworkError) {
                            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                        } else {
                            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
        }
    }

    private void parseFeedList(JSONArray resultArray) throws Exception {
        if (resultArray != null) {
            mList = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), StockListInfo[].class)));
            handleUI();
        }
    }

    private void handleUI() throws Exception {
        StockListInfo stockListInfo1;
        StockListInfo stockListInfo2;

        if (mList.size() >= 2) {
            stockListInfo1 = mList.get(0);
            stockListInfo2 = mList.get(1);
            tvStockName1.setText(stockListInfo1.tradeName);
            tvStockName2.setText(stockListInfo2.tradeName);
            tvStockAmount1.setText(stockListInfo1.lastTradePrice);
            tvStockAmount2.setText(stockListInfo2.lastTradePrice);

            boolean isMarketUp1 = stockListInfo1.arrow.equalsIgnoreCase("uparo");
            boolean isMarketUp2 = stockListInfo2.arrow.equalsIgnoreCase("uparo");
            Drawable drawableUp = ContextCompat.getDrawable(mContext, R.drawable.ic_arrow_up_stock);
            Drawable drawableDown = ContextCompat.getDrawable(mContext, R.drawable.ic_arrow_down_stock);
            tvStock1.setCompoundDrawablesWithIntrinsicBounds((isMarketUp1) ? drawableUp : drawableDown, null, null, null);
            tvStock2.setCompoundDrawablesWithIntrinsicBounds((isMarketUp2) ? drawableUp : drawableDown, null, null, null);
            int stockUpColor = ContextCompat.getColor(mContext, R.color.stock_up);
            int stockDownColor = ContextCompat.getColor(mContext, R.color.stock_down);
            tvStock1.setTextColor((isMarketUp1) ? stockUpColor : stockDownColor);
            tvStock2.setTextColor((isMarketUp2) ? stockUpColor : stockDownColor);
            tvStock1.setText(String.format(stockListInfo1.netChange));
            tvStock2.setText(String.format(stockListInfo2.netChange));
        }


    }
}
