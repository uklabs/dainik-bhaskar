package com.db.divya_new.jyotishVastu;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.Action;
import com.bhaskar.util.AppUrls;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.util.LoginController;
import com.db.InitApplication;
import com.db.listeners.OnDBVideoClickListener;
import com.db.main.BaseAppCompatActivity;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.google.android.gms.analytics.Tracker;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class DivyaRashifalDetailActivity extends BaseAppCompatActivity implements OnDBVideoClickListener {

    public static final int PAGE_COUNT = 4;



    private Menu rootMenu;
    private ViewPager viewPager;
    private Tracker tracker;
    private SectionPagerAdapter sectionPagerAdapter;

    private String detailUrl;
    private String headerTitle;
    private String headerSubTitle;
    private String rashIconUrl;
    private String gaArticle;
    private String wisdomDomain;
    private String mParentId, mSuperParentId;
    private String mDisplayName;

    private int index;
    private String rashifalType;
    private String categoryId;
    private int colorInt;
    private String colorString;
    private String rashiName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_details_rashifal);

        Intent intent = getIntent();
        if (intent != null) {
            index = intent.getIntExtra("indexnumber", 0);
            rashifalType = intent.getStringExtra("rashi.type");
            detailUrl = intent.getStringExtra(Constants.KeyPair.KEY_DETAIL_URL);
            headerTitle = intent.getStringExtra(Constants.KeyPair.KEY_TITLE);
            headerSubTitle = intent.getStringExtra(Constants.KeyPair.KEY_SUB_TITLE);
            rashIconUrl = intent.getStringExtra(Constants.KeyPair.KEY_ICON);
            wisdomDomain = intent.getStringExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN);
            gaArticle = intent.getStringExtra(Constants.KeyPair.KEY_GA_ARTICLE);
            mParentId = intent.getStringExtra(Constants.KeyPair.KEY_ID);
            mSuperParentId = intent.getStringExtra(Constants.KeyPair.KEY_PARENT_ID);
            categoryId = intent.getStringExtra(Constants.KeyPair.KEY_CATEGORY_ID);
            mDisplayName = intent.getStringExtra(Constants.KeyPair.KEY_DISPLAY_NAME);
            colorString = intent.getStringExtra(Constants.KeyPair.KEY_COLOR);
            rashiName = intent.getStringExtra(Constants.KeyPair.KEY_NAME);
            colorInt = AppUtils.getThemeColor(DivyaRashifalDetailActivity.this, colorString);

        }
        tracker = ((InitApplication) getApplication()).getDefaultTracker();

        handleToolbar();
        viewPager = findViewById(R.id.viewPagerBigDetail);
        sectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(sectionPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//        tabLayout.setTabGravity(TabLayout.MODE_SCROLLABLE);
        tabLayout.setVisibility(View.VISIBLE);
        tabLayout.setupWithViewPager(viewPager);
        /*change color*/
        tabLayout.setBackgroundColor(colorInt);
        tabLayout.setTabTextColors(getResources().getColor(R.color.white), getResources().getColor(R.color.white));
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.white));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Systr.println("Unselected Clicked");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                setToTop();
            }
        });
        viewPager.addOnPageChangeListener(onPageChangeListener);
        viewPager.post(() -> onPageChangeListener.onPageSelected(viewPager.getCurrentItem()));

    }

    private void handleToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(colorInt);
        toolbar.setTitle("");


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbar.setNavigationOnClickListener(v -> finish());
        if (headerSubTitle != null)
            mTitle.setText(String.format("%s %s", headerTitle, headerSubTitle));
        else
            mTitle.setText(String.format("%s %s", headerTitle, ""));
    }

    final ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { // Empty Method
        }

        @Override
        public void onPageSelected(int position) {
            String rashiName = String.valueOf(index);
            try {
                if (rashifalType.equalsIgnoreCase("rashi")) {
                    String[] rashiArray = getResources().getStringArray(R.array.sunsign_array);
                    rashiName = rashiArray[index - 1];
                }
            } catch (Exception e) {
                AppLogs.printErrorLogs("RashifalDetails", e.toString());
            }

            String value = gaArticle + "-" + rashiName + "-" + CommonConstants.rashifalTabTitlesEnglish[position];

            // Tracking
            String source = AppPreferences.getInstance(DivyaRashifalDetailActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(DivyaRashifalDetailActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(DivyaRashifalDetailActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            Tracking.trackGAScreen(DivyaRashifalDetailActivity.this, tracker, value, source, medium, campaign);

            String wisdomUrl = TrackingHost.getInstance(DivyaRashifalDetailActivity.this).getWisdomUrl(AppUrls.WISDOM_URL);
            String domain = AppPreferences.getInstance(DivyaRashifalDetailActivity.this).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
            String subDomain = Action.CategoryAction.CAT_ACTION_RASHIFAL_V2 + "/" + CommonConstants.rashifalTabTitlesEnglish[position] + "/" + rashiName;
            AppUtils.GLOBAL_DISPLAY_NAME = subDomain;
            String url = (TextUtils.isEmpty(wisdomDomain) ? domain : wisdomDomain) + subDomain;
            ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
            String email = "";
            String mobile = "";
            if (profileInfo != null) {
                email = profileInfo.email;
                mobile = profileInfo.mobile;
            }
            Tracking.trackWisdomListingPage(DivyaRashifalDetailActivity.this, wisdomUrl, url, email, mobile, CommonConstants.BHASKAR_APP_ID);
        }

        @Override
        public void onPageScrollStateChanged(int state) { // Empty Method
        }
    };

    private void setToTop() {
        Object fragmentInstance = Objects.requireNonNull(viewPager.getAdapter()).instantiateItem(viewPager, viewPager.getCurrentItem());

        Systr.println("Reselected Clicked instance not null");

        if (fragmentInstance instanceof DivyaRashifalDetailFragment) {
            ((DivyaRashifalDetailFragment) fragmentInstance).moveToTop();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rashifal_menu, menu);
        rootMenu = menu;
        resetFont();
        return true;
    }

    private void resetFont() {
        int lastFontSize = AppPreferences.getInstance(DivyaRashifalDetailActivity.this).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
        if (lastFontSize == Constants.Font.MEDIUM) {
            rootMenu.findItem(R.id.action_font).setIcon(R.drawable.arc_small_font);
        } else {
            rootMenu.findItem(R.id.action_font).setIcon(R.drawable.arc_large_font);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_MENU)) {
            return true;
        }
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            super.onKeyDown(keyCode, event);
            return true;
        }
        if ((keyCode == KeyEvent.KEYCODE_HOME)) {
            super.onKeyDown(keyCode, event);
            return true;
        }
        return false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_font) {
            int lastFontSize = AppPreferences.getInstance(DivyaRashifalDetailActivity.this).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
            if (lastFontSize == Constants.Font.MEDIUM) {
                AppPreferences.getInstance(DivyaRashifalDetailActivity.this).setIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.LARGE);
            } else {
                AppPreferences.getInstance(DivyaRashifalDetailActivity.this).setIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
            }
            resetFont();
            sectionPagerAdapter.notifyDataSetChanged();
            // Tracking
            String campaign = AppPreferences.getInstance(DivyaRashifalDetailActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            String value = (lastFontSize == Constants.Font.LARGE ? AppFlyerConst.GALabel.LARGE : AppFlyerConst.GALabel.MEDIUM);
            Tracking.trackGAEvent(DivyaRashifalDetailActivity.this, tracker, AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.FONT, value, campaign);
            Systr.println("GA Events : " + AppFlyerConst.GACategory.ARTICLE_EVENT + AppFlyerConst.GAAction.FONT + value + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDbVideoClick(int position) {
        if (position < viewPager.getAdapter().getCount() - 1) {
            viewPager.setCurrentItem(position + 1);
        }
    }

    private class SectionPagerAdapter extends FragmentStatePagerAdapter {

        SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @NotNull
        @Override
        public Fragment getItem(int position) {
            return DivyaRashifalDetailFragment.getInstance(categoryId, detailUrl, index, position, rashifalType, headerTitle, headerSubTitle,rashIconUrl, mParentId, mSuperParentId, mDisplayName, rashiName);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CommonConstants.rashifalTabTitles[position];
        }

        @Override
        public int getItemPosition(Object object) {
            if (object instanceof DivyaRashifalDetailFragment) {
                ((DivyaRashifalDetailFragment) object).changeFontSize();
            }
            return super.getItemPosition(object);
        }
    }

}
