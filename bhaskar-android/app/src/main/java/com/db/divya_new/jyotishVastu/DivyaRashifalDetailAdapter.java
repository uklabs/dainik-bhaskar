package com.db.divya_new.jyotishVastu;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;

import java.util.List;


public class DivyaRashifalDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static final int VIEW_TYPE_RASHIFAL = 101;
    private List<DivyaRashifalDetailInfo.RashifalDetailInfo> lisData;
    private Activity mContext;

    DivyaRashifalDetailAdapter(Activity context, List<DivyaRashifalDetailInfo.RashifalDetailInfo> list) {
        mContext = context;
        this.lisData = list;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == VIEW_TYPE_RASHIFAL) {
            return new RashifalDetailListViewHolder(inflater.inflate(R.layout.rashifal_detail_v2_list_item, parent, false));
        }
        return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
    }

    void changeFontSize() {
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder.getItemViewType() == VIEW_TYPE_RASHIFAL) {
            final DivyaRashifalDetailInfo.RashifalDetailInfo info = lisData.get(holder.getAdapterPosition());
            RashifalDetailListViewHolder viewHolder = (RashifalDetailListViewHolder) holder;

            viewHolder.rashifalTitle.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            viewHolder.rashifalComment.setTextColor(ContextCompat.getColor(mContext, R.color.list_slug_color));
            viewHolder.rlMainlayout.setBackgroundColor(Color.WHITE);

            AppUtils.getInstance().setArticleFontSize(mContext, viewHolder.rashifalTitle, Constants.Font.CAT_MEDIUM);
            AppUtils.getInstance().setArticleFontSize(mContext, viewHolder.rashifalComment, Constants.Font.CAT_SMALL_LARGE);

            /*Title*/
            if (!TextUtils.isEmpty(info.title)) {
                viewHolder.rashifalTitle.setText(info.title);
            } else {
                viewHolder.rashifalTitle.setVisibility(View.GONE);
            }
            /*Comment*/
            if (!TextUtils.isEmpty(info.comment)) {
                viewHolder.rashifalComment.setText(info.comment);
            } else {
                viewHolder.rashifalComment.setVisibility(View.GONE);
            }
            /**/
            if (!TextUtils.isEmpty(info.image)) {
                ImageUtil.setImage(mContext, info.image, viewHolder.rashifalIV, 0);
            } else {
                viewHolder.rashifalIV.setVisibility(View.GONE);
            }

            viewHolder.notifyDisplayPrefs();
        }
    }

    @Override
    public int getItemCount() {
        return lisData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return lisData.get(position).viewType;
    }

    private class RashifalDetailListViewHolder extends RecyclerView.ViewHolder {
        private ImageView rashifalIV;
        private TextView rashifalTitle;
        private TextView rashifalComment;
        private RelativeLayout rlMainlayout;

        private RashifalDetailListViewHolder(View view) {
            super(view);
            rashifalIV = view.findViewById(R.id.iv_thumb_image);
            rashifalTitle = view.findViewById(R.id.tv_title);
            rashifalComment = view.findViewById(R.id.tv_comment);
            rlMainlayout = view.findViewById(R.id.rl_MainRlayout);
        }

        public void notifyDisplayPrefs() {
            boolean isNightMode = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.IS_NIGHT_MODE, false);
            if (isNightMode) {
                rashifalTitle.setTextColor(Color.WHITE);
                rashifalComment.setTextColor(Color.WHITE);
                itemView.setBackgroundColor(Color.BLACK);
            } else {
                rashifalTitle.setTextColor(Color.BLACK);
                rashifalComment.setTextColor(mContext.getResources().getColor(R.color.list_slug_color));
                itemView.setBackgroundColor(Color.WHITE);
            }
        }
    }
}