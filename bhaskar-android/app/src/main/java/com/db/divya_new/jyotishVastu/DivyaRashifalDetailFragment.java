package com.db.divya_new.jyotishVastu;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.util.CommonConstants;
import com.db.divya_new.articlePage.DivyaArticleCallbacks;
import com.db.listeners.OnDBVideoClickListener;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DivyaRashifalDetailFragment extends Fragment implements DivyaArticleCallbacks {

    OnDBVideoClickListener onDBVideoClickListener;
    private String categoryId;
    private String rashiDuration;
    private DivyaRashifalDetailAdapter rashifalDetailV2Adapter;
    private List<DivyaRashifalDetailInfo.RashifalDetailInfo> rashifalDetailInfoArrayList;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private View view;
    private String detailUrl;
    private NestedScrollView rashifalDetailScrollview;
    private String mParentId;
    private String mSuperParentId;
    private String mRashi;
    private String mDisplayName;
    private LinearLayoutManager mLayoutManager;
    private TextView tvRashifal, tvRashifalSubTitle;
    private String mRashiIconUrl;

    public static DivyaRashifalDetailFragment getInstance(String categoryId, String detailUrl, int index, int position, String rashifalType, String title, String subTitle, String rashiIconUrl, String pParentId, String pSuperParentId, String displayName, String rashi) {
        DivyaRashifalDetailFragment rashifalFragmentDetails = new DivyaRashifalDetailFragment();
        Bundle args = new Bundle();
        args.putString(Constants.KeyPair.KEY_DETAIL_URL, detailUrl);
        args.putInt("index", index);
        args.putString("rashifalType", rashifalType);
        args.putString(Constants.KeyPair.KEY_TITLE, title);
        args.putString(Constants.KeyPair.KEY_SUB_TITLE, subTitle);
        args.putInt(Constants.KeyPair.KEY_POSITION, position);
        args.putString(Constants.KeyPair.KEY_ID, pParentId);
        args.putString(Constants.KeyPair.KEY_PARENT_ID, pSuperParentId);
        args.putString(Constants.KeyPair.KEY_CATEGORY_ID, categoryId);
        args.putString(Constants.KeyPair.KEY_DISPLAY_NAME, displayName);
        args.putString(Constants.KeyPair.KEY_NAME, rashi);
        args.putString(Constants.KeyPair.KEY_ICON, rashiIconUrl);
        switch (position) {
            case 0:
                args.putString("rashiDuration", "daily");
                break;
            case 1:
                args.putString("rashiDuration", "weekly");
                break;
            case 2:
                args.putString("rashiDuration", "monthly");
                break;
            case 3:
                args.putString("rashiDuration", "yearly");
                break;
        }
        rashifalFragmentDetails.setArguments(args);
        return rashifalFragmentDetails;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onDBVideoClickListener = (OnDBVideoClickListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mParentId = getArguments().getString(Constants.KeyPair.KEY_ID);
        mSuperParentId = getArguments().getString(Constants.KeyPair.KEY_PARENT_ID);
        mDisplayName = getArguments().getString(Constants.KeyPair.KEY_DISPLAY_NAME);
        categoryId = getArguments().getString(Constants.KeyPair.KEY_CATEGORY_ID);
        rashiDuration = getArguments().getString("rashiDuration");
        detailUrl = getArguments().getString(Constants.KeyPair.KEY_DETAIL_URL);
        mRashi = getArguments().getString(Constants.KeyPair.KEY_NAME);
        mRashiIconUrl = getArguments().getString(Constants.KeyPair.KEY_ICON);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.rashifal_detail_v2_layout, container, false);

        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        tvRashifal = view.findViewById(R.id.tv_rashifal);
        tvRashifalSubTitle = view.findViewById(R.id.tv_rashifal_sub_title);
        recyclerView = view.findViewById(R.id.recycler_view);
        rashifalDetailScrollview = view.findViewById(R.id.rashifal_detail_scrollview);


        setUpRecyclerView();
        changeFontSize();

        ImageView rashiIV = view.findViewById(R.id.iv_rashifal);
        ImageUtil.setImage(getContext(), mRashiIconUrl, rashiIV, 0);

        if (NetworkStatus.getInstance().isConnected(getActivity()))
            makeJsonObjectRequest(mRashi);
        else
            AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));

        if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.ATF_ARTICLE_TOGGLE, false)) {
            AdController.showArticleATF(getContext(), view.findViewById(R.id.ads_view_top));
        } else {
            view.findViewById(R.id.ads_view_top).setVisibility(View.GONE);
        }
        if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.BTF_ROS_ARTICLE_TOGGLE, false)) {
            AdController.showArticleBTF(getContext(), view.findViewById(R.id.ads_view_bottom));
        } else {
            view.findViewById(R.id.ads_view_bottom).setVisibility(View.GONE);
        }

        return view;
    }

    private void setUpRecyclerView() {
        try {
            if (rashifalDetailInfoArrayList == null)
                rashifalDetailInfoArrayList = new ArrayList<>();
            mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            rashifalDetailV2Adapter = new DivyaRashifalDetailAdapter(getActivity(), rashifalDetailInfoArrayList);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter((rashifalDetailV2Adapter));
            recyclerView.setNestedScrollingEnabled(false);
        } catch (Exception ignored) {
        }
    }


    private void makeJsonObjectRequest(String rashiType) {
        String Url = AppUtils.getInstance().updateApiUrl(Urls.APP_FEED_BASE_URL + detailUrl + rashiType + "/" + rashiDuration + "/");
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET, Url, null,
                response -> {
                    if (response != null) {
                        DivyaRashifalDetailInfo rashifalDetailResponse = DivyaRashifalDetailParser.parseJson(response);
                        if (rashifalDetailResponse != null && rashifalDetailResponse.data != null) {
                            if (rashifalDetailInfoArrayList == null)
                                rashifalDetailInfoArrayList = new ArrayList<>();
                            else {
                                rashifalDetailInfoArrayList.clear();
                            }
                            rashifalDetailInfoArrayList.addAll(rashifalDetailResponse.data);
                            tvRashifal.setVisibility(View.GONE);
                            tvRashifalSubTitle.setText(rashifalDetailResponse.rashifalDate);
                            for (int index = 0; index < rashifalDetailInfoArrayList.size(); index++) {
                                rashifalDetailInfoArrayList.get(index).viewType = DivyaRashifalDetailAdapter.VIEW_TYPE_RASHIFAL;
                            }
                            rashifalDetailScrollview.setVisibility(View.VISIBLE);
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                }, error -> {
            AppLogs.printErrorLogs("Rashifal Detail V2 error = ", "= " + error.toString());

            progressBar.setVisibility(View.GONE);

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getActivity()).addToRequestQueue(jsonObjReq);
    }

    public void moveToTop() {
        view.findViewById(R.id.rashifal_detail_scrollview).scrollTo(0, 0);
    }

    public void changeFontSize() {
        if (view != null && getContext() != null) {
            AppUtils.getInstance().setArticleFontSize(getContext(), tvRashifal, Constants.Font.CAT_SMALL);
            AppUtils.getInstance().setArticleFontSize(getContext(), tvRashifalSubTitle, Constants.Font.CAT_SMALL);
            if (rashifalDetailV2Adapter != null) {
                rashifalDetailV2Adapter.changeFontSize();
            }
        }
    }

    @Override
    public void openRashifalArticle(String detailUrl, int indexNumber, String rashiTitle, String rashiSubTitle, String rashiIcon, String mGaArticle) {

        Intent intent = new Intent(getContext(), DivyaRashifalDetailActivity.class);
        intent.putExtra("indexnumber", indexNumber);
        intent.putExtra("rashi.type", "rashi");
        intent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, detailUrl);
        intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, mGaArticle);
        intent.putExtra(Constants.KeyPair.KEY_DISPLAY_NAME, mDisplayName);
        intent.putExtra(Constants.KeyPair.KEY_TITLE, rashiTitle);
        if (!TextUtils.isEmpty(rashiSubTitle)) {
            intent.putExtra(Constants.KeyPair.KEY_SUB_TITLE, rashiSubTitle);
        }
        intent.putExtra(Constants.KeyPair.KEY_ICON, rashiIcon);
        intent.putExtra(Constants.KeyPair.KEY_ID, mParentId);
        intent.putExtra(Constants.KeyPair.KEY_PARENT_ID, mSuperParentId);
        intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, categoryId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        startActivity(intent);
        getActivity().finish();
    }

}
