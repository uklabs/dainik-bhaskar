package com.db.divya_new.jyotishVastu;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DivyaRashifalDetailInfo {

    @SerializedName("feed")
    public List<RashifalDetailInfo> data = null;

    @SerializedName("date")
    public String rashifalDate;

    public static class RashifalDetailInfo {
        RashifalDetailInfo(int viewType) {
            this.viewType = viewType;
        }

        @SerializedName("title")
        public String title;
        @SerializedName("text")
        public String comment;
        @SerializedName("image")
        public String image;
        @SerializedName("name")
        public String name;

        public int viewType;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof RashifalDetailInfo)) return false;
            RashifalDetailInfo that = (RashifalDetailInfo) o;
            return viewType == that.viewType;
        }

    }
}
