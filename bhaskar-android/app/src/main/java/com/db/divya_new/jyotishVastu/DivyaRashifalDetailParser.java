package com.db.divya_new.jyotishVastu;

import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.google.gson.Gson;

import org.json.JSONObject;


public class DivyaRashifalDetailParser {
    private static final String TAG = AppConfig.BaseTag + "." + DivyaRashifalDetailParser.class.getSimpleName();

    /**
     * Method to parseExtraData the JSON string received at the time of get MLA Page list
     *
     * @param json JSON in the form of String
     * @return
     */
    public static DivyaRashifalDetailInfo parseJson(JSONObject json) {
        try {
            return new Gson().fromJson(json.toString(), DivyaRashifalDetailInfo.class);
        } catch (Exception e) {
            AppLogs.printErrorLogs(TAG, e.getMessage());
            return null;
        }
    }
}


