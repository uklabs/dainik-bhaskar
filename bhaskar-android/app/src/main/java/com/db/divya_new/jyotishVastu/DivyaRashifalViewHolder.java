package com.db.divya_new.jyotishVastu;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.util.CommonConstants;
import com.db.data.models.CategoryInfo;
import com.db.data.models.RashifalInfo;
import com.db.divya_new.articlePage.DivyaArticleCallbacks;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHome;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.NetworkStatus;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.db.views.WrappingFragmentStatePagerAdapter;
import com.db.views.WrappingViewPager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DivyaRashifalViewHolder extends RecyclerView.ViewHolder {
    private final String TAG = AppConfig.BaseTag + "." + DivyaRashifalViewHolder.class.getSimpleName();
    public RashifalAdapter adapter;
    private SectionPagerAdapter sectionPagerAdapter;
    private String mParentId;
    private String mSuperParentId;
    private String pageTitle;
    private Activity mContext;
    private RecyclerView rvRashis;
    private boolean isLoading;
    private ProgressBar loadMoreProgressBar;
    private List<RashifalInfo> rashiData = new ArrayList<>();
    private Button btnTryAgain;
    private WrappingViewPager viewPager;
    private String RASHI_FAL_LIST = "rashifalList";


    public DivyaRashifalViewHolder(Activity context, View itemView, boolean isFromHome, DivyaArticleCallbacks divyaArticleCallbacks) {
        super(itemView);
        this.mContext = context;
        rvRashis = itemView.findViewById(R.id.recycler_view);
        viewPager = itemView.findViewById(R.id.viewPager);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        btnTryAgain = itemView.findViewById(R.id.btnTryAgain);
        sectionPagerAdapter = new SectionPagerAdapter(((AppCompatActivity) mContext).getSupportFragmentManager());
        viewPager.setAdapter(sectionPagerAdapter);

        Button btnLoadMore = itemView.findViewById(R.id.btn_load_more);
        Button btnSubHeaderLoadMore = itemView.findViewById(R.id.btnSubHeaderLoadMore);

        adapter = new RashifalAdapter(mContext, isFromHome, divyaArticleCallbacks);

        RashifalAdapter.ItemListener itemListener = (rashifalInfo, position) -> viewPager.setCurrentItem(position);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                rvRashis.smoothScrollToPosition(position);
                adapter.setSelectedItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        adapter.setItemListener(itemListener);

        if (isFromHome) {
            rvRashis.setLayoutManager(new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false));
            btnSubHeaderLoadMore.setVisibility(View.VISIBLE);
        } else {
            int gridCount = 3;
            rvRashis.setLayoutManager(new GridLayoutManager(itemView.getContext(), gridCount));
            viewPager.setVisibility(View.GONE);
            btnSubHeaderLoadMore.setVisibility(View.GONE);
        }

        btnLoadMore.setVisibility(View.GONE);
        rvRashis.setAdapter(adapter);
        btnSubHeaderLoadMore.setOnClickListener(v -> {
            if (isFromHome && TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(mParentId);
            }
        });

    }

    private void fetchData() {
        String finalFeedURL = Urls.APP_FEED_BASE_URL + String.format(Urls.RASHIFAL_WIDGET_URL, CommonConstants.CHANNEL_ID);

        if (rashiData == null && !isLoading)
            makeJsonObjectRequest(finalFeedURL);
        else {
            RashifalInfo rashifalInfo = AppPreferences.getInstance(mContext).getDataFromPreferences(RASHI_FAL_LIST, RashifalInfo.class);

            if (rashifalInfo == null) {
                adapter.add(getAllItemLists());
                sectionPagerAdapter.addList(getAllItemLists());
            }
        }

    }

    private void makeJsonObjectRequest(String finalFeedURL) {
        isLoading = true;
        btnTryAgain.setVisibility(View.GONE);
        loadMoreProgressBar.setVisibility(View.VISIBLE);
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            AppLogs.printErrorLogs(TAG + "Feed URL", finalFeedURL);

            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            if (response != null) {
                                loadMoreProgressBar.setVisibility(View.GONE);
                                parseFeedList(response);
                            }
                        } catch (Exception e) {
                            btnTryAgain.setVisibility(View.VISIBLE);
                        }
                        isLoading = false;
                        loadMoreProgressBar.setVisibility(View.GONE);
                    }, error -> {
                if (error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
                btnTryAgain.setVisibility(View.VISIBLE);

                loadMoreProgressBar.setVisibility(View.GONE);
                isLoading = false;
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);

        } else {
            if (mContext != null)
                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
            loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void parseFeedList(JSONArray response) {
        rashiData = Arrays.asList(new Gson().fromJson(String.valueOf(response), RashifalInfo[].class));
        adapter.add(getAllItemLists());
        sectionPagerAdapter.addList(getAllItemLists());
    }


    public void clearDataAndLoadNew() {
        adapter.clear();
    }

    // Adding the title is available in the categoryInfo else it will be gone
    private void setUpPageTitle(String extraVlues) {
        if (TextUtils.isEmpty(pageTitle) && !TextUtils.isEmpty(extraVlues)) {
            try {
                if (new JSONObject(extraVlues).has("category_intro")) {
                    pageTitle = new JSONObject(extraVlues).optString("category_intro");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            setPageTitle(pageTitle);
        } else if (!TextUtils.isEmpty(pageTitle)) {
            setPageTitle(pageTitle);
        } else {
            setPageTitle("");
        }
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        TextView tvTitle = itemView.findViewById(R.id.tv_title);

        if (!TextUtils.isEmpty(pageTitle)) {
            this.pageTitle = pageTitle;
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(pageTitle);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setGravity(Gravity.CENTER);
            tvTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            int paddingVerticle = (int) AppUtils.convertDpToPixel(12, mContext);
            tvTitle.setPadding(0, paddingVerticle, 0, paddingVerticle);

        } else {
            tvTitle.setVisibility(View.GONE);
        }
    }

    public void setData(CategoryInfo categoryInfo) {
        mSuperParentId = categoryInfo.parentId;
        mParentId = categoryInfo.id;
        adapter.setData(categoryInfo.id, categoryInfo.detailUrl, categoryInfo.gaArticle, mParentId, mSuperParentId, categoryInfo.displayName);
        sectionPagerAdapter.setData(categoryInfo.id, categoryInfo.detailUrl, categoryInfo.gaArticle, mParentId, mSuperParentId, categoryInfo.displayName, categoryInfo.color);
        setUpPageTitle(categoryInfo.extraVlues);
        RashifalInfo rashifalInfo = AppPreferences.getInstance(mContext).getDataFromPreferences(RASHI_FAL_LIST, RashifalInfo.class);
        if (rashifalInfo == null) {
            adapter.add(getAllItemLists());
        }
    }

    public void setData(CatHome catHome) {
        mSuperParentId = catHome.superParentId;
        mParentId = catHome.parentId;
        CategoryInfo categoryInfo = catHome.categoryInfo;
        adapter.setData(catHome.categoryInfo.id, catHome.categoryInfo.detailUrl, catHome.categoryInfo.gaArticle, mParentId, mSuperParentId, catHome.categoryInfo.displayName);
        adapter.setThemeColor(categoryInfo.color);
        sectionPagerAdapter.setData(catHome.categoryInfo.id, catHome.categoryInfo.detailUrl, catHome.categoryInfo.gaArticle, mParentId, mSuperParentId, catHome.categoryInfo.displayName, catHome.categoryInfo.color);
        setUpPageTitle(catHome.categoryInfo.extraVlues);
        fetchData();
    }

    private List<RashifalInfo> getAllItemLists() {

        if (NetworkStatus.getInstance().isConnected(mContext)) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, String.format(Urls.RASHIFAL_ITEMS_URL, CommonConstants.BHASKAR_APP_ID), null,
                    response -> {
                        JSONArray fdArray = response.optJSONArray("feed");
                        rashiData = Arrays.asList(new Gson().fromJson(String.valueOf(fdArray), RashifalInfo[].class));
                        adapter.add(rashiData);
                        adapter.getDataRashifal(rashiData);
                        adapter.notifyDataSetChanged();
                        saveArrayList(rashiData);
                    }, error -> {

            }) {
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        }
        return rashiData;
    }

    class SectionPagerAdapter extends WrappingFragmentStatePagerAdapter {
        List<RashifalInfo> mList;
        private String id;
        private String detailUrl;
        private String gaArticle;
        private String mParentId;
        private String mSuperParentId;
        private String displayName;
        private String color;

        SectionPagerAdapter(FragmentManager fm) {
            super(fm);
            this.mList = new ArrayList<>();
        }

        void setData(String id, String detailUrl, String gaArticle, String mParentId, String mSuperParentId, String displayName, String color) {
            this.id = id;
            this.detailUrl = detailUrl;
            this.gaArticle = gaArticle;
            this.mParentId = mParentId;
            this.mSuperParentId = mSuperParentId;
            this.displayName = displayName;
            this.color = color;
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            return RashifalPerviewFragment.newInstance(mList.get(position), id, detailUrl, gaArticle, mParentId, mSuperParentId, displayName, position, color);
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        public void addList(List<RashifalInfo> allItemList) {
            mList.clear();
            mList.addAll(allItemList);
            notifyDataSetChanged();
        }
    }

    public void saveArrayList(List<RashifalInfo> rashiData) {
        AppPreferences.getInstance(mContext).saveDataIntoPreferences(rashiData, RASHI_FAL_LIST);
    }

}