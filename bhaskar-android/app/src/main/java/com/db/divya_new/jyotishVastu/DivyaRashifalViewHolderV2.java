package com.db.divya_new.jyotishVastu;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bhaskar.R;
import com.db.InitApplication;
import com.db.data.models.CategoryInfo;
import com.db.data.models.RashifalInfo;
import com.db.database.DatabaseClient;
import com.db.divya_new.data.CatHome2;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.NetworkStatus;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.db.views.WrappingFragmentStatePagerAdapter;
import com.db.views.WrappingViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DivyaRashifalViewHolderV2 extends RecyclerView.ViewHolder {

    private Activity mContext;
    private RecyclerView rvRashis;
    private ProgressBar loadMoreProgressBar;
    private Button btnTryAgain;
    private WrappingViewPager viewPager;

    private RashifalAdapter adapter;
    private SectionPagerAdapter sectionPagerAdapter;

    private boolean isLoading;
    private String mChannelId;
    private CategoryInfo categoryInfo;
    private JSONArray rashiData;
    private String RASHI_FAL_INDEX = "rashifalIndex";


    public DivyaRashifalViewHolderV2(Activity context, View itemView) {
        super(itemView);
        this.mContext = context;

//        this.mChannelId = InitApplication.getInstance().getSelectedChannelId();
        this.mChannelId = CommonConstants.CHANNEL_ID;

        rvRashis = itemView.findViewById(R.id.recycler_view);
        viewPager = itemView.findViewById(R.id.viewPager);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);
        btnTryAgain = itemView.findViewById(R.id.btnTryAgain);

        sectionPagerAdapter = new SectionPagerAdapter(((AppCompatActivity) mContext).getSupportFragmentManager());
        adapter = new RashifalAdapter(mContext, true, null);

        viewPager.setAdapter(sectionPagerAdapter);
        RashifalAdapter.ItemListener itemListener = (rashifalInfo, position) -> viewPager.setCurrentItem(position);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                rvRashis.smoothScrollToPosition(position);
                adapter.setSelectedItem(position);
                AppPreferences.getInstance(context).saveDataIntoPreferences(adapter.getSelectedItem(position),RASHI_FAL_INDEX);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        adapter.setItemListener(itemListener);
        rvRashis.setLayoutManager(new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false));
        rvRashis.setAdapter(adapter);
    }

    private void fetchData() {
        String finalFeedURL = Urls.APP_FEED_BASE_URL + String.format(Urls.RASHIFAL_WIDGET_URL, CommonConstants.CHANNEL_ID);
        if (rashiData == null && !isLoading)
            makeJsonObjectRequest(finalFeedURL);
        else {
            List<RashifalInfo> rashifalInfos = getAllItemLists(rashiData);
            adapter.add(rashifalInfos);
            sectionPagerAdapter.addList(rashifalInfos);
        }
    }

    private void makeJsonObjectRequest(String finalFeedURL) {
        isLoading = true;
        btnTryAgain.setVisibility(View.GONE);
        loadMoreProgressBar.setVisibility(View.VISIBLE);

        if (NetworkStatus.getInstance().isConnected(mContext)) {
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            if (response != null) {
                                loadMoreProgressBar.setVisibility(View.GONE);
                                parseFeedList(response);
                            }
                        } catch (Exception e) {
                            btnTryAgain.setVisibility(View.VISIBLE);
                        }
                        isLoading = false;
                        loadMoreProgressBar.setVisibility(View.GONE);
                    }, error -> {
                if (error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
                btnTryAgain.setVisibility(View.VISIBLE);

                loadMoreProgressBar.setVisibility(View.GONE);
                isLoading = false;
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);

        } else {
            if (mContext != null)
                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
            loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void parseFeedList(JSONArray response) {
        rashiData = response;
        List<RashifalInfo> rashifalInfos = getAllItemLists(rashiData);
        adapter.add(rashifalInfos);
        sectionPagerAdapter.addList(rashifalInfos);
    }

    private List<RashifalInfo> getAllItemLists(JSONArray response) {
//        RashifalInfo rashifalInfo = AppPreferences.getInstance(mContext).getDataFromPreferences(RASHI_FAL_INDEX,List.class);
        List<RashifalInfo> rashifalInfo = AppPreferences.getInstance(mContext).getDataFromPreferences(RASHI_FAL_INDEX,RashifalInfo.class);
        List<RashifalInfo> allItems = new ArrayList<>();
        /*try {
            allItems.add(new RashifalInfo("aries", getRashiContent("aries", response), R.drawable.mesh_v2));
            allItems.add(new RashifalInfo("taurus", getRashiContent("taurus", response), R.drawable.vrishk_v2));
            allItems.add(new RashifalInfo("gemini", getRashiContent("gemini", response), R.drawable.mithun_v2));
            allItems.add(new RashifalInfo("cancer", getRashiContent("cancer", response), R.drawable.karka_v2));
            allItems.add(new RashifalInfo("leo", getRashiContent("leo", response), R.drawable.singh_v2));
            allItems.add(new RashifalInfo("virgo", getRashiContent("virgo", response), R.drawable.kanya_v2));
            allItems.add(new RashifalInfo("libra", getRashiContent("libra", response), R.drawable.tula_v2));
            allItems.add(new RashifalInfo("scorpio", getRashiContent("scorpio", response), R.drawable.vrushchik_v2));
            allItems.add(new RashifalInfo("sagittarius", getRashiContent("sagittarius", response), R.drawable.dhanu_v2));
            allItems.add(new RashifalInfo("capricorn", getRashiContent("capricorn", response), R.drawable.makar_v2));
            allItems.add(new RashifalInfo("aquarius", getRashiContent("aquarius", response), R.drawable.kumbha_v2));
            allItems.add(new RashifalInfo("pisces", getRashiContent("pisces", response), R.drawable.meen_v2));
            if(rashifalInfo!=null){
                allItems.remove(rashifalInfo);
                allItems.add(0, rashifalInfo);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        return allItems;
    }

    private String getRashiContent(String rashiName, JSONArray response) throws JSONException {
        if (response == null)
            return "";
        for (int i = 0; i < response.length(); i++) {
            JSONObject jsonObject = response.getJSONObject(i);
            if (jsonObject.getString("rashiName").equalsIgnoreCase(rashiName))
                return jsonObject.getString("content");
        }
        return "";
    }

    public void setData(CatHome2 catHome) {
        if (categoryInfo == null)
            categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(catHome.id);
        if (categoryInfo != null) {
            adapter.setData(categoryInfo.id, categoryInfo.detailUrl, categoryInfo.gaArticle, "", "", categoryInfo.displayName);
            adapter.setThemeColor(catHome.color);
            sectionPagerAdapter.setData(categoryInfo.id, categoryInfo.detailUrl, categoryInfo.gaArticle, "", "", categoryInfo.displayName, catHome.color);
        }
        // fetch rashifal icons and title and sub title and name


        fetchData();
    }

    class SectionPagerAdapter extends WrappingFragmentStatePagerAdapter {
        List<RashifalInfo> mList;
        private String id;
        private String detailUrl;
        private String gaArticle;
        private String mParentId;
        private String mSuperParentId;
        private String displayName;
        private String color;

        SectionPagerAdapter(FragmentManager fm) {
            super(fm);
            this.mList = new ArrayList<>();
        }

        void setData(String id, String detailUrl, String gaArticle, String mParentId, String mSuperParentId, String displayName, String color) {
            this.id = id;
            this.detailUrl = detailUrl;
            this.gaArticle = gaArticle;
            this.mParentId = mParentId;
            this.mSuperParentId = mSuperParentId;
            this.displayName = displayName;
            this.color = color;
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            return RashifalPerviewFragment.newInstance(mList.get(position), id, detailUrl, gaArticle, mParentId, mSuperParentId, displayName, position, color);
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        public void addList(List<RashifalInfo> allItemList) {
            mList.clear();
            mList.addAll(allItemList);
            notifyDataSetChanged();
        }
    }
}