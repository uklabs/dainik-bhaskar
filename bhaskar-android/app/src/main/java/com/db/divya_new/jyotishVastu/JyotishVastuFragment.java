package com.db.divya_new.jyotishVastu;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.util.Constants;
import com.db.util.DividerItemDecoration;

import java.util.ArrayList;
import java.util.Objects;

public class JyotishVastuFragment extends Fragment implements OnMoveTopListener {
    public String id;
    private CategoryInfo mCategoryInfo;
    private RecyclerView recyclerView;

    public JyotishVastuFragment() {
    }

    public static JyotishVastuFragment newInstance(CategoryInfo info, String id) {
        JyotishVastuFragment fragment = new JyotishVastuFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, info);
        bundle.putString(Constants.KeyPair.KEY_ID, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategoryInfo = (CategoryInfo) getArguments().getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            id = getArguments().getString(Constants.KeyPair.KEY_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jyoti_vastu, container, false);

        JyotiVastuAdapter mAdapter = new JyotiVastuAdapter(getActivity(), id, mCategoryInfo);

        recyclerView = view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getContext())));
        recyclerView.setAdapter(mAdapter);

        ArrayList<Object> list = new ArrayList<>();
        list.add(Constants.ViewHolderType.VIEW_HOLDER_STORY);
        mAdapter.addAll(list);

        return view;
    }

    public void moveToTop() {
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }
}
