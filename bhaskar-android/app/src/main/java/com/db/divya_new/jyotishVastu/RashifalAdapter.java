package com.db.divya_new.jyotishVastu;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.RashifalInfo;
import com.db.divya_new.articlePage.DivyaArticleCallbacks;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;

import java.util.ArrayList;
import java.util.List;


public class RashifalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String mParentId, mSuperParentId;
    private ArrayList<RashifalInfo> rashifalInfoArrayList;
    private List<RashifalInfo> rashifalData;
    private Activity mContext;
    private String categoryId;
    private String mDetailUrl;
    private String mGaArticle;
    private boolean isFromHome;
    private DivyaArticleCallbacks divyaArticleCallbacks;
    private String mDisplayName;
    private boolean isNightModeApplicable;
    private ItemListener mListener;
    private String themeColor;


    RashifalAdapter(Activity context, boolean isFromHome, DivyaArticleCallbacks divyaArticleCallbacks) {
        mContext = context;
        this.isFromHome = isFromHome;
        this.divyaArticleCallbacks = divyaArticleCallbacks;
        rashifalInfoArrayList = new ArrayList<>();
    }

    public void setData(String categoryId, String pDetailUrl, String gaArticle, String pParentId, String pSuperParentId, String displayName) {
        this.categoryId = categoryId;
        mDetailUrl = pDetailUrl;
        mGaArticle = gaArticle;
        this.mParentId = pParentId;
        this.mSuperParentId = pSuperParentId;
        mDisplayName = displayName;
    }

    public void getDataRashifal(List<RashifalInfo> rashifalInfoArrayList) {
        this.rashifalData = rashifalInfoArrayList;
    }

    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
        notifyDataSetChanged();
    }

    public void add(List<RashifalInfo> categoryInfoArrayList) {
        this.rashifalInfoArrayList.clear();
        this.rashifalInfoArrayList.addAll(categoryInfoArrayList);
        resetSelection();
        if (!rashifalInfoArrayList.isEmpty())
            rashifalInfoArrayList.get(0).isSelected = true;
        notifyDataSetChanged();
    }

    void setSelectedItem(int position) {
        resetSelection();
        rashifalInfoArrayList.get(position).isSelected = true;
        notifyDataSetChanged();
    }

    RashifalInfo getSelectedItem(int position) {
        return rashifalInfoArrayList.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_rashifal_grid_item, parent, false);
        return new ViewHolder(itemView, isFromHome);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof RashifalAdapter.ViewHolder) {
            final RashifalInfo info = rashifalData.get(holder.getAdapterPosition());

            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.tvRashi.setText(info.rashiTitle);
            viewHolder.tvRashiSubtitle.setText(info.rashiSubtitle);
            ImageUtil.setImage(mContext, info.rashIcon, viewHolder.ivRashifal, R.drawable.profilepic);
            viewHolder.vIndicator.setBackgroundColor(AppUtils.getThemeColor(mContext, themeColor));
            if (info.isSelected && isFromHome) {
                viewHolder.vIndicator.setVisibility(View.VISIBLE);
                viewHolder.ivRashifal.setSelected(true);
                viewHolder.tvRashi.setTextColor(AppUtils.getThemeColor(mContext, themeColor));
            } else {
                viewHolder.vIndicator.setVisibility(View.INVISIBLE);
                viewHolder.ivRashifal.setSelected(false);
                viewHolder.tvRashi.setTextColor(mContext.getResources().getColor(R.color.black));
            }

            if (isFromHome) {
                holder.itemView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
            } else {
                holder.itemView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            }

            viewHolder.itemView.setOnClickListener(view -> {
                if (isFromHome) {
                    resetSelection();
                    info.isSelected = true;
                    notifyDataSetChanged();
                    if (mListener != null)
                        mListener.onItemSelect(info, position);
                } else {
                    openDetailPage(info, holder);
                }
            });
            if (isNightModeApplicable) {
                viewHolder.notifyDisplayPrefs();
            }
        }
    }

    private void openDetailPage(RashifalInfo info, RecyclerView.ViewHolder holder) {
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            String rashi = "";
            if (info != null) {
                rashi = info.rashiName;
            }
            if (divyaArticleCallbacks != null) {
                divyaArticleCallbacks.openRashifalArticle(mDetailUrl, holder.getAdapterPosition() + 1, info.rashiTitle, info.rashiSubtitle, info.rashIcon, mGaArticle);
            } else {
                Intent intent = new Intent(mContext, DivyaRashifalDetailActivity.class);
                intent.putExtra("indexnumber", holder.getAdapterPosition() + 1);
                intent.putExtra("rashi.type", "rashi");
                intent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, mDetailUrl);
                intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, mGaArticle);
                intent.putExtra(Constants.KeyPair.KEY_DISPLAY_NAME, mDisplayName);
                intent.putExtra(Constants.KeyPair.KEY_TITLE, info.rashiTitle);
                if (!TextUtils.isEmpty(info.rashiSubtitle)) {
                    intent.putExtra(Constants.KeyPair.KEY_SUB_TITLE, info.rashiSubtitle);
                }
                intent.putExtra(Constants.KeyPair.KEY_ICON, info.rashIcon);
                intent.putExtra(Constants.KeyPair.KEY_NAME, rashi);
                intent.putExtra(Constants.KeyPair.KEY_ID, mParentId);
                intent.putExtra(Constants.KeyPair.KEY_PARENT_ID, mSuperParentId);
                intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, categoryId);

                mContext.startActivityForResult(intent, MainActivity.REQUEST_CODE_CALL_FOR_FOOTER_TAB);
            }

        } else {
            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
        }
    }

    private void resetSelection() {
        for (RashifalInfo info : rashifalInfoArrayList) {
            info.isSelected = false;
        }
    }

    @Override
    public int getItemCount() {
        return rashifalInfoArrayList.size();
    }

    public void clear() {
        rashifalInfoArrayList.clear();
        notifyDataSetChanged();
    }

    public void notifyDisplayPrefs(boolean isNightModeApplicable) {
        this.isNightModeApplicable = isNightModeApplicable;
    }

    public void setItemListener(ItemListener itemListener) {
        this.mListener = itemListener;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivRashifal;
        private TextView tvRashi;
        private TextView tvRashiSubtitle;
        private LinearLayout linearLayoutRootView;
        private View vIndicator;

        private ViewHolder(View view, boolean isFromHome) {
            super(view);
            ivRashifal = view.findViewById(R.id.iv_rashifal);
            tvRashi = view.findViewById(R.id.tv_rashi);
            tvRashiSubtitle = view.findViewById(R.id.tv_rashi_subtitle);
            vIndicator = view.findViewById(R.id.v_indicator);
            linearLayoutRootView = view.findViewById(R.id.root_view);
            if (!isFromHome) {
                linearLayoutRootView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            } else {
                linearLayoutRootView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                tvRashiSubtitle.setVisibility(View.GONE);
            }
        }

        public void notifyDisplayPrefs() {
            boolean isNightMode = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.IS_NIGHT_MODE, false);
            if (isNightMode) {
                tvRashi.setTextColor(Color.WHITE);
                tvRashiSubtitle.setTextColor(Color.WHITE);
            } else {
                tvRashi.setTextColor(mContext.getResources().getColor(R.color.input_text_title_color));
                tvRashiSubtitle.setTextColor(mContext.getResources().getColor(R.color.input_text_color));
            }
        }
    }

    public interface ItemListener {
        void onItemSelect(RashifalInfo rashifalInfo, int position);
    }
}