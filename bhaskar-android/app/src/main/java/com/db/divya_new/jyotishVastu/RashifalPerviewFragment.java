package com.db.divya_new.jyotishVastu;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bhaskar.R;
import com.db.data.models.RashifalInfo;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.NetworkStatus;

/**
 * A simple {@link Fragment} subclass.
 */
public class RashifalPerviewFragment extends Fragment {

    private RashifalInfo rashifalInfo;
    private int index;
    private String id;
    private String detailUrl;
    private String gaArticle;
    private String mParentId;
    private String mSuperParentId;
    private String displayName;
    private String color;

    public RashifalPerviewFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance(RashifalInfo rashifalInfo, String id, String detailUrl, String gaArticle, String mParentId, String mSuperParentId, String displayName, int index, String color) {
        Fragment fragment = new RashifalPerviewFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", rashifalInfo);
        bundle.putInt("index", index);
        bundle.putString("id", id);
        bundle.putString("detailUrl", detailUrl);
        bundle.putString("gaArticle", gaArticle);
        bundle.putString("mParentId", mParentId);
        bundle.putString("mSuperParentId", mSuperParentId);
        bundle.putString("displayName", displayName);
        bundle.putString("color", color);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            rashifalInfo = (RashifalInfo) getArguments().getSerializable("data");
            index = getArguments().getInt("index");
            id = getArguments().getString("id");
            detailUrl = getArguments().getString("detailUrl");
            gaArticle = getArguments().getString("gaArticle");
            mParentId = getArguments().getString("mParentId");
            mSuperParentId = getArguments().getString("mSuperParentId");
            displayName = getArguments().getString("displayName");
            color = getArguments().getString("color");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rashifal_perview, container, false);
        TextView tvLoadMore = view.findViewById(R.id.tv_load_more);
        tvLoadMore.setTextColor(AppUtils.getThemeColor(getContext(), color));
        tvLoadMore.setOnClickListener(v -> openDetailPage(rashifalInfo));
        return view;
    }

    private void openDetailPage(RashifalInfo info) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            String rashi = "";
            Intent intent = new Intent(getContext(), DivyaRashifalDetailActivity.class);
            intent.putExtra("indexnumber", index + 1);
            intent.putExtra("rashi.type", "rashi");
            intent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, detailUrl);
            intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
            intent.putExtra(Constants.KeyPair.KEY_DISPLAY_NAME, displayName);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, info.rashiTitle);
            if (!TextUtils.isEmpty(info.rashiSubtitle)) {
                intent.putExtra(Constants.KeyPair.KEY_SUB_TITLE, info.rashiSubtitle);
            }
            intent.putExtra(Constants.KeyPair.KEY_ICON, info.rashIcon);
            intent.putExtra(Constants.KeyPair.KEY_NAME, rashi);
            intent.putExtra(Constants.KeyPair.KEY_ID, mParentId);
            intent.putExtra(Constants.KeyPair.KEY_PARENT_ID, mSuperParentId);
            intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, id);
            getContext().startActivity(intent);
        } else {
            AppUtils.getInstance().showCustomToast(getContext(), getContext().getResources().getString(R.string.no_network_error));
        }
    }
}
