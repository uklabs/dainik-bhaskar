package com.db.divya_new.jyotishVastu;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.bhaskar.R;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.ImageUtil;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;

import java.util.ArrayList;
import java.util.List;

public class RatnaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {
    private static final int VIEW_TYPE_RATNA = 1;
    private static final int VIEW_TYPE_BANNER = 3;
    private static final int VIEW_TYPE_AD = 4;

    private ArrayList<Object> mList;
    private CategoryInfo categoryInfo;
    private String mParentId;
    private Activity mContext;

    private boolean isRefreshing = false;
    private List<BannerInfo> bannerInfoList;

    public RatnaAdapter(Activity context, CategoryInfo categoryInfo, String id) {
        mList = new ArrayList<>();
        this.mContext = context;
        this.categoryInfo = categoryInfo;
        this.mParentId = id;
        /*Banner*/
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    public void addAll(ArrayList<Object> objectList) {
        mList.addAll(objectList);
        addBanner();
        addAds();
        notifyDataSetChanged();
    }

    private void addBanner() {
//        if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
        for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
            if (!bannerInfoList.get(bannerIndex).isAdded)
                if (Integer.parseInt(bannerInfoList.get(bannerIndex).catPos) > -1) {
                    mList.add(0, bannerInfoList.get(bannerIndex));
                    bannerInfoList.get(bannerIndex).isAdded = true;
                }
        }
//        }
    }

    private void addAds() {
        isBannerLoaded = false;
        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
            mList.add(0, AdController.showATFForAllListing(mContext, this));
        }
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;
        notifyItemChanged(0);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));
            case VIEW_TYPE_RATNA:
                RatnaViewHolder ratnaViewHolder = new RatnaViewHolder(inflater.inflate(R.layout.listitem_recylerview_title, parent, false), mContext, false,  false);
                ratnaViewHolder.setData(categoryInfo, mParentId);
                return ratnaViewHolder;
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData((BannerInfo) mList.get(holder.getAdapterPosition()));
                break;
            case VIEW_TYPE_AD:
//                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
//                adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
                if (((AdInfo) mList.get(position)).catType == 1) {
                    if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                        adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);

                    } else {
                        ImageView imageView = new ImageView(mContext);
                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setAdjustViewBounds(true);
                        imageView.setClickable(true);
                        String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                        ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                        adViewHolder.addAdsUrl(imageView);

                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AppUtils.getInstance().clickOnAdBanner(mContext);
                            }
                        });
                    }
                } else {
                    adViewHolder.addAds(((AdInfo) mList.get(position)).bannerAdView);
                }
                break;
            case VIEW_TYPE_RATNA:
                bindRatnaViewHolder((RatnaViewHolder) holder);
                break;
        }
    }

    private void bindRatnaViewHolder(RatnaViewHolder holder) {
        if (isRefreshing) {
            holder.onSwipeRefresh();
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mList.get(position);
        if (object instanceof AdInfo) {
            return VIEW_TYPE_AD;
        } else if (object instanceof BannerInfo) {
            return VIEW_TYPE_BANNER;
        }

        return 1;
    }

    public void onSwipeRefresh(SwipeRefreshLayout swipeRefreshLayout) {
        isRefreshing = true;
        notifyDataSetChanged();
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }
}
