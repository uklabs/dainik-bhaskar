package com.db.divya_new.jyotishVastu;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.R;

public class RatnaJyotishFragment extends Fragment {

    public static RatnaJyotishFragment newInstance() {
        RatnaJyotishFragment fragment = new RatnaJyotishFragment();

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jyoti_vastu, container, false);
        return view;
    }
}
