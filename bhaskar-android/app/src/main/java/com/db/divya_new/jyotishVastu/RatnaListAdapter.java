package com.db.divya_new.jyotishVastu;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.StoryListInfo;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;

import java.util.ArrayList;
import java.util.List;

public class RatnaListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<StoryListInfo> mList;
    private String  title;
    private String gaScreen;
    private String gaArticle;
    private String gaDisplayName;
    private String gaEventLabel;
    private String color;
    private boolean isFromHome;
    private Activity mContext;
    private boolean isNightModeApplicable;

    RatnaListAdapter(Activity mContext, boolean isFromHome, boolean isNightModeApplicable) {
        this.mContext = mContext;
        this.isNightModeApplicable = isNightModeApplicable;
        mList = new ArrayList<>();
        this.isFromHome = isFromHome;
    }

    public void setData(String pTitle, String gaScreen, String gaArticle, String gaDisplayName, String gaEventLabel, String color) {
        this.title = pTitle;
        this.gaScreen = gaScreen;
        this.gaArticle = gaArticle;
        this.gaDisplayName = gaDisplayName;
        this.gaEventLabel = gaEventLabel;
        this.color = color;
    }

    public void addList(List<StoryListInfo> object) {
        mList.addAll(object);
        notifyDataSetChanged();
    }

    public void addAndClearList(List<StoryListInfo> ratnaNameList) {
        mList.clear();
        mList.addAll(ratnaNameList);
        notifyDataSetChanged();
    }

    public void add(StoryListInfo item) {
        mList.add(item);
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        return new ArticleViewHolder(inflater.inflate(R.layout.listitem_round_rec_image_title, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleViewHolder) {
            articleBindHolder((ArticleViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    /******** Bind Holders Methods ********/

    private void articleBindHolder(final ArticleViewHolder viewHolder, int position) {
        final StoryListInfo ratnaListInfo = mList.get(position);
        viewHolder.tvTitle.setText(ratnaListInfo.title);

        ImageUtil.setImage(viewHolder.itemView.getContext(), ratnaListInfo.image, viewHolder.ivPic, R.drawable.water_mark_news_list);
        viewHolder.itemView.setOnClickListener(view -> mContext.startActivityForResult(DivyaCommonArticleActivity.getIntent(mContext, ratnaListInfo.catId, title, gaArticle, gaScreen, gaDisplayName, gaEventLabel, color), Constants.REQ_CODE_BOTTOM_NAV));
        viewHolder.notifyDisplayPrefs();
    }

    /******** View Holders Classes ********/

    class ArticleViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivPic;
        private TextView tvTitle;
        private LinearLayout linearLayoutRootView;

        ArticleViewHolder(View itemView) {
            super(itemView);
            ivPic = itemView.findViewById(R.id.iv_pic);
            tvTitle = itemView.findViewById(R.id.tv_title);
            linearLayoutRootView = itemView.findViewById(R.id.root_view);
            if (!isFromHome) {
                linearLayoutRootView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            } else {
                linearLayoutRootView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
        }

        public void notifyDisplayPrefs() {
            if (!isNightModeApplicable)
                return;
            boolean isNightMode = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.IS_NIGHT_MODE, false);
            tvTitle.setTextColor((isNightMode) ? Color.WHITE : Color.BLACK);
            linearLayoutRootView.setBackgroundColor((isNightMode) ? Color.BLACK : Color.WHITE);
        }

    }
}
