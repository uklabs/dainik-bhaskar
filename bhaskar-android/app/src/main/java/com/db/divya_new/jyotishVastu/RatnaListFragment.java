package com.db.divya_new.jyotishVastu;


import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.util.Constants;
import com.db.util.AppUtils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class RatnaListFragment extends Fragment implements OnMoveTopListener {

    private RatnaAdapter adapter;
    private CategoryInfo categoryInfo;
    private String mParentId;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            adapter.onSwipeRefresh(swipeRefreshLayout);
        }
    };
    private RecyclerView recyclerView;

    public RatnaListFragment() {
    }

    public static RatnaListFragment newInstance(CategoryInfo categoryInfo, String id) {
        RatnaListFragment fragment = new RatnaListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString(Constants.KeyPair.KEY_ID, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            mParentId = bundle.getString(Constants.KeyPair.KEY_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_common, container, false);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        progressBar = view.findViewById(R.id.progressBar);
        if (getActivity() != null) {
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }

        adapter = new RatnaAdapter(getActivity(), categoryInfo, mParentId);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        ArrayList<Object> list = new ArrayList<>();
        list.add(Constants.ViewHolderType.VIEW_HOLDER_STORY);
        adapter.addAll(list);

        return view;
    }

    public void moveToTop() {
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }
}
