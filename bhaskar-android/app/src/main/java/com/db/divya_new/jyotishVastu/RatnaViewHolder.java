package com.db.divya_new.jyotishVastu;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.StoryListInfo;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHome;
import com.db.divya_new.data.CatHomeData;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class RatnaViewHolder extends RecyclerView.ViewHolder {

    public RecyclerView recyclerView;
    public Activity mContext;
    private TextView titleTextView;
    private boolean isNightModeApplicable;
    private Button btnHeaderLoadMore;
    private Button btnSubHeaderLoadMore;
    private Button btn_retry;
    private ProgressBar loadMoreProgressBar;
    private RatnaListAdapter listAdapter;
    private String mFeedUrl;
    private boolean isFromHome;
    private String mParentId;
    private String mSuperParentId;
    private String id;
    private String pageTitle;
    private boolean isLoading;
    private boolean isFromParentHome;
    private View rootView;

    public RatnaViewHolder(View itemView, Activity activity, boolean isFromHome, boolean isNightModeApplicable) {
        super(itemView);
        this.rootView = itemView.findViewById(R.id.rootview);
        mContext = activity;
        this.isFromHome = isFromHome;
        titleTextView = itemView.findViewById(R.id.tv_title);
        this.isNightModeApplicable = isNightModeApplicable;
        titleTextView.setVisibility(View.GONE);

        btn_retry = itemView.findViewById(R.id.btn_retry);
        btnHeaderLoadMore = itemView.findViewById(R.id.btnHeaderLoadMore);
        btnSubHeaderLoadMore = itemView.findViewById(R.id.btnSubHeaderLoadMore);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);

        loadMoreProgressBar.setVisibility(View.GONE);
        recyclerView = itemView.findViewById(R.id.recycler_view);

        if (isFromHome) {
            btnSubHeaderLoadMore.setVisibility(View.VISIBLE);
            btnHeaderLoadMore.setVisibility(View.GONE);
            btnSubHeaderLoadMore.setOnClickListener(v -> {
                onLoadMore();
            });
        }

        btn_retry.setOnClickListener(v -> fetchData());
        listAdapter = new RatnaListAdapter(mContext, this.isFromHome,  isNightModeApplicable);

    }

    private void onLoadMore() {
        if (isFromHome && TabController.getInstance().getSubMenuTabControllerInstance() != null) {
            TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(mParentId);
        }
    }


    public void clearDataAndLoadNew() {
        listAdapter.clear();
    }

    // Adding the title is available in the categoryInfo else it will be gone
    private void setUpPageTitle(String extraVlues) {
        String pageTitle = null;
        if (!TextUtils.isEmpty(extraVlues)) {
            try {
                if (new JSONObject(extraVlues).has("category_intro")) {
                    pageTitle = new JSONObject(extraVlues).optString("category_intro");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.pageTitle = pageTitle;

            setPageTitle(pageTitle);
        }
    }

    public void setPageTitle(String pageTitle) {
        TextView tvTitle = itemView.findViewById(R.id.tv_title);

        if (!TextUtils.isEmpty(pageTitle)) {
            this.pageTitle = pageTitle;
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(pageTitle);
            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
            tvTitle.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                tvTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        } else {
            tvTitle.setVisibility(View.GONE);
        }
    }

    public void setData(CategoryInfo categoryInfo, String pSuperParentId) {
        mFeedUrl = categoryInfo.feedUrl;
        isFromHome = false;
        isFromParentHome = false;
        this.mParentId = categoryInfo.id;
        mSuperParentId = pSuperParentId;
        this.id = categoryInfo.id;
        if (isFromHome) {
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        }
        listAdapter.setData(categoryInfo.menuName, categoryInfo.gaScreen, categoryInfo.gaArticle, categoryInfo.displayName, categoryInfo.gaEventLabel, categoryInfo.color);
        recyclerView.setAdapter(listAdapter);

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue())) {
            if (!TextUtils.isEmpty(catHomeData.pageTitle)) {
                setPageTitle(catHomeData.pageTitle);
            }
            parseData(catHomeData);
        } else {
            fetchData();
            setUpPageTitle(categoryInfo.extraVlues);
        }
    }

    public void setData(CatHome catHome, boolean isFromParentHome) {
        this.isFromParentHome = isFromParentHome;
        CategoryInfo categoryInfo = catHome.categoryInfo;
        mFeedUrl = categoryInfo.feedUrl;
        isFromHome = true;
        this.mParentId = catHome.parentId;
        mSuperParentId = catHome.superParentId;
        this.id = categoryInfo.id;
        if (isFromHome) {
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        }
        listAdapter.setData(categoryInfo.menuName, categoryInfo.gaScreen, categoryInfo.gaArticle, categoryInfo.displayName, categoryInfo.gaEventLabel, categoryInfo.color);
        recyclerView.setAdapter(listAdapter);
        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue())) {
            if (!TextUtils.isEmpty(catHomeData.pageTitle)) {
                setPageTitle(catHomeData.pageTitle);
            }
            parseData(catHomeData);
        } else {
            fetchData();
            setUpPageTitle(categoryInfo.extraVlues);
        }
    }

    private void fetchData() {
        try {
            fetchGuruDataFromServer(Urls.APP_FEED_BASE_URL + mFeedUrl);
        } catch (Exception e) {
        }
    }

    private void fetchGuruDataFromServer(String url) {
        isLoading = true;
        loadMoreProgressBar.setVisibility(View.VISIBLE);

        if (NetworkStatus.getInstance().isConnected(mContext)) {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    parseData(response);
                    isLoading = false;
                    loadMoreProgressBar.setVisibility(View.GONE);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));

                    loadMoreProgressBar.setVisibility(View.GONE);
                    if (isFromHome)
                        btn_retry.setVisibility(View.VISIBLE);
                    isLoading = false;
                }
            });

            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            if (!isFromHome && !isFromParentHome)
                VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(request);
            else
                VolleyNetworkSingleton.getInstance(mContext, 2).addToRequestQueue(request);
        } else {
            if (mContext != null) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
            }
            if (isFromHome)
                btn_retry.setVisibility(View.VISIBLE);
            isLoading = false;
            loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void parseData(JSONObject jsonObject) {

        JSONArray jsonArray = jsonObject.optJSONArray("feed");
        if (jsonArray != null) {
            ArrayList<StoryListInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(jsonArray.toString(), StoryListInfo[].class)));

            if (!TextUtils.isEmpty(id)) {
                CatHomeData catHomeData = new CatHomeData(list, 0, CatHomeData.TYPE_STORY, System.currentTimeMillis());
                if (!TextUtils.isEmpty(pageTitle))
                    catHomeData.pageTitle = pageTitle;
                DataParser.getInstance().addCatHomeDataHashMap(id, catHomeData);
            }
            listAdapter.addAndClearList(list);
        }

        btn_retry.setVisibility(View.GONE);

    }

    private void parseData(CatHomeData catHomeData) {
        ArrayList<StoryListInfo> list = (ArrayList<StoryListInfo>) catHomeData.getArrayList(CatHomeData.TYPE_STORY);

        if (list != null) {

            listAdapter.addAndClearList(list);
//            if (isFromHome)
//                loadMoreButton.setVisibility(View.VISIBLE);
//            else loadMoreButton.setVisibility(View.GONE);
        }

        btn_retry.setVisibility(View.GONE);

    }

    public void onSwipeRefresh() {
        if (!isLoading)
            fetchData();
    }

    public void notifyDisplayPrefs() {
        if (!isNightModeApplicable)
            return;
        //AppUtils.getInstance().setFontSize(mContext, tvArticleName, Constants.Font.CAT_SMALL_LARGE);
        boolean isNightMode = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.IS_NIGHT_MODE, false);
        titleTextView.setTextColor((isNightMode) ? Color.WHITE : Color.BLACK);
        rootView.setBackgroundColor((isNightMode) ? Color.BLACK : Color.WHITE);

        listAdapter.notifyDataSetChanged();
    }
}
