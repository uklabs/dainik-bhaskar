package com.db.divya_new.offers;

import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Scroller;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bhaskar.data.local.prefs.SWAppPreferences;
import com.bhaskar.data.model.Offer;
import com.bhaskar.R;
import com.db.util.ImageUtil;

public class CouponOfferFragment extends Fragment {

    private static OffersDialogFragment.OfferDialogFragmentInterface mOfferDialogListener;
    private Offer mOffers;

    public static CouponOfferFragment getInstance(Offer offers, OffersDialogFragment.OfferDialogFragmentInterface offerDialogInterface) {
        mOfferDialogListener = offerDialogInterface;
        CouponOfferFragment fragment = new CouponOfferFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(SWAppPreferences.Keys.OFFER, offers);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mOffers = (Offer) getArguments().getSerializable(SWAppPreferences.Keys.OFFER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_coupon_offer, container, false);
        TextView tvHeader = view.findViewById(R.id.tvHeader);
        EditText edtMessage = view.findViewById(R.id.tvMessage);
        edtMessage.setKeyListener(null);
        edtMessage.setScroller(new Scroller(getContext()));
        edtMessage.setVerticalScrollBarEnabled(true);
        edtMessage.setMovementMethod(new ScrollingMovementMethod());
        edtMessage.setText(Html.fromHtml(mOffers.description));
        tvHeader.setText(mOffers.name);
        Button btnClaim = view.findViewById(R.id.btnClaim);
        btnClaim.setText("Continue");
        TextView tvRemindLater = view.findViewById(R.id.tvRemindLater);
        tvRemindLater.setOnClickListener(v -> {
            if (mOfferDialogListener != null)
                mOfferDialogListener.onClose();
        });
        ImageView ivBanner = view.findViewById(R.id.ivBanner);
        String imageURL = mOffers.dailog_banner_img;
        if (imageURL.contains(".gif")) {
            ImageUtil.setImageGif(getContext(), imageURL, ivBanner, R.drawable.placeholder);
        } else {
            ImageUtil.setImage(getContext(), imageURL, ivBanner, R.drawable.placeholder);
        }

        btnClaim.setOnClickListener(v -> {
                if (mOfferDialogListener != null)
                    mOfferDialogListener.onContinue(mOffers);
        });

        return view;
    }


}
