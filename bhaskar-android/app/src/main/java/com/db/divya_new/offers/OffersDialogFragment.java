package com.db.divya_new.offers;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bhaskar.data.local.prefs.SWAppPreferences;
import com.bhaskar.data.model.Offer;
import com.bhaskar.R;
import com.db.util.AppUtils;
import com.db.util.NetworkStatus;
import com.db.views.WrappingFragmentStatePagerAdapter;
import com.db.views.WrappingViewPager;
import com.shuhart.bubblepagerindicator.BubblePageIndicator;

import java.io.Serializable;
import java.util.List;


public class OffersDialogFragment extends DialogFragment {

    private static OffersDialogFragment.OfferDialogInterface mListener;

    OfferDialogFragmentInterface offerDialogFragmentInterface = new OfferDialogFragmentInterface() {
        @Override
        public void onContinue(Offer offer) {
            String dialogAction = offer.dailog_action;
            if (dialogAction.equalsIgnoreCase("1")) {
                //open particular offer
                if (NetworkStatus.getInstance().isConnected(getContext())) {
                    mListener.onContinue(offer);
                    if (isAdded())
                        dismiss();
                } else {
                    if (isAdded())
                        AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.no_network_error));
                }
            } else if (dialogAction.equalsIgnoreCase("2")) {
                //open listing
                if (NetworkStatus.getInstance().isConnected(getContext())) {
                    mListener.onContinue(offer);
                    if (isAdded())
                        dismiss();
                } else {
                    AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.no_network_error));
                }
            } else {
                //just dismiss
                if (isAdded()) {
                    SWAppPreferences.getInstance(getContext()).setBooleanValue(SWAppPreferences.Keys.OFFER_DISABLED, true);
                    dismiss();
                }
            }
        }

        @Override
        public void onClose() {
            dismiss();
        }
    };
    private List<Offer> offerList;

    public static OffersDialogFragment getInstance(List<Offer> offers, OfferDialogInterface offerDialogInterface) {
        mListener = offerDialogInterface;
        OffersDialogFragment offersDialogFragment = new OffersDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(SWAppPreferences.Keys.OFFER, (Serializable) offers);
        offersDialogFragment.setArguments(bundle);
        return offersDialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_offer, container);
        handleExtras(getArguments());
        WrappingViewPager viewPager = view.findViewById(R.id.viewPager);
        view.findViewById(R.id.ivCross).setOnClickListener(v -> {
            SWAppPreferences.getInstance(getContext()).setBooleanValue(SWAppPreferences.Keys.OFFER_DISABLED, true);
            if (isAdded())
                dismiss();
        });

        BubblePageIndicator pager_indicator = view.findViewById(R.id.pager_indicator);

        SectionPagerAdapter sectionPagerAdapter = new SectionPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(sectionPagerAdapter);
        pager_indicator.setViewPager(viewPager);
        pager_indicator.setVisibility(View.INVISIBLE);
        if (!offerList.isEmpty() && offerList.size() > 1) {
            pager_indicator.setVisibility(View.VISIBLE);
        }
        pager_indicator.setFillColor(getResources().getColor(R.color.colorPrimary1));

        return view;
    }

    private void handleExtras(Bundle bundle) {
        if (bundle != null) {
            offerList = (List<Offer>) bundle.getSerializable(SWAppPreferences.Keys.OFFER);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public interface OfferDialogFragmentInterface {
        void onContinue(Offer offer);

        void onClose();
    }

    public interface OfferDialogInterface {
        void onContinue(Offer offer);
    }

    class SectionPagerAdapter extends WrappingFragmentStatePagerAdapter {

        SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return CouponOfferFragment.getInstance(offerList.get(position), offerDialogFragmentInterface);
        }

        @Override
        public int getCount() {
            return offerList.size();
        }
    }


}
