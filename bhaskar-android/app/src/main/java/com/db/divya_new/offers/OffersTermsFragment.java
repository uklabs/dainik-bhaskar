package com.db.divya_new.offers;

import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Scroller;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.bhaskar.data.model.Offer;
import com.bhaskar.R;


public class OffersTermsFragment extends DialogFragment {
    public static OfferDialogInterface mListener;
    private Offer offer;

    public static OffersTermsFragment getInstance(Offer offer, OfferDialogInterface offerDialogInterface) {
        mListener = offerDialogInterface;
        OffersTermsFragment offersDialogFragment = new OffersTermsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("offer", offer);
        offersDialogFragment.setArguments(bundle);
        return offersDialogFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_offer_terms, container);
        handleExtras(getArguments());
        TextView tvNotNow = view.findViewById(R.id.tvNotNow);
        TextView tvHeader = view.findViewById(R.id.tvHeader);
        tvNotNow.setOnClickListener(v -> dismiss());
        tvHeader.setText(offer.name);
        view.findViewById(R.id.btnContinue).setOnClickListener(v -> {
            mListener.onContinue();
            dismiss();
        });
        EditText edtMessage = view.findViewById(R.id.tvMessage);
        edtMessage.setText(offer.dialogTnc);
        edtMessage.setKeyListener(null);
        edtMessage.setScroller(new Scroller(getContext()));
        edtMessage.setVerticalScrollBarEnabled(true);
        edtMessage.setMovementMethod(new ScrollingMovementMethod());
        edtMessage.setText(Html.fromHtml(offer.dialogTnc));
        return view;
    }

    private void handleExtras(Bundle bundle) {
        if (bundle != null) {
            offer = (Offer) bundle.getSerializable("offer");
        }
    }

    public interface OfferDialogInterface {
        void onContinue();
    }


}
