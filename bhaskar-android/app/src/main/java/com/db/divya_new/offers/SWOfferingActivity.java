package com.db.divya_new.offers;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bhaskar.data.model.Offer;
import com.bhaskar.R;
import com.db.main.BaseAppCompatActivity;
import com.db.util.AppUtils;
import com.db.util.Constants;

import java.util.ArrayList;
import java.util.List;


public class SWOfferingActivity extends BaseAppCompatActivity implements Switcher {

    private List<Offer> offersList;
    private String themeColor;
    private String mTitle, gaScreen;
    private boolean isComingFromHamburger = false;


    public static Intent getIntent(Context context, String color, String title, String gaScreen, boolean isComingFromHam) {
        Intent intent = new Intent(context, SWOfferingActivity.class);
        intent.putExtra(Constants.KeyPair.KEY_COLOR, color);
        intent.putExtra(Constants.KeyPair.KEY_TITLE, title);
        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        intent.putExtra("isComingFromHa,", isComingFromHam);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sw_offering);
        offersList = new ArrayList<>();
        handleExtras(getIntent());
        handleToolbar();
        changeFragment(getSupportFragmentManager(), R.id.rootContainer, SWOfferingFragment.getInstance(gaScreen, isComingFromHamburger), false, false);
    }

    private void handleExtras(Intent intent) {
        if (intent == null)
            return;
        themeColor = intent.getStringExtra(Constants.KeyPair.KEY_COLOR);
        mTitle = intent.getStringExtra(Constants.KeyPair.KEY_TITLE);
        gaScreen = intent.getStringExtra(Constants.KeyPair.KEY_GA_SCREEN);
        isComingFromHamburger = intent.getBooleanExtra("isComingFromHa", false);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 999) {
        }
    }

    private void handleToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(AppUtils.getThemeColor(SWOfferingActivity.this, themeColor));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back);
        }

        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        AppUtils.getInstance().setStatusBarColor(SWOfferingActivity.this, themeColor);
        TextView tvToolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        tvToolbarTitle.setText(mTitle);


    }

    @Override
    public void switcher(Fragment fragment, boolean saveInBackstack, boolean animate) {

    }

    public void changeFragment(FragmentManager fragmentManager, int id, Fragment fragment, boolean saveInBackstack, boolean animate) {
        String backStateName = fragment.getClass().getName();
        try {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (animate) {
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            }
            transaction.replace(id, fragment, backStateName);
            if (saveInBackstack) {
                transaction.addToBackStack(backStateName);
            }

            transaction.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }
}
