package com.db.divya_new.offers;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.local.prefs.SWAppPreferences;
import com.bhaskar.data.model.Offer;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.source.server.BackgroundRequest;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.bhaskar.util.Action;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SWOfferingFragment extends Fragment {
    private static final String TAG = SWOfferingFragment.class.getName();
    private List<Offer> offersList;
    private ProgressBar progressBar;
    private OfferingAdapter offeringAdapter;
    private String gaScreen;
    private TextView tvErrorMsg;

    public static SWOfferingFragment getInstance(String gaScreen, boolean isComingFromHamburger) {
        SWOfferingFragment fragment = new SWOfferingFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        bundle.putBoolean("isComingFromHa", isComingFromHamburger);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            gaScreen = getArguments().getString(Constants.KeyPair.KEY_GA_SCREEN);
        }

        offersList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sw_offering, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewOffering);
        progressBar = view.findViewById(R.id.progressBar);
        tvErrorMsg = view.findViewById(R.id.tvErrorMsg);
        offeringAdapter = new OfferingAdapter(getContext(), offersList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(offeringAdapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getLatestOffers();

        String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);
    }

    private void getLatestOffers() {
        if (JsonParser.getInstance().isOfferActionAvailable(getContext())) {
            showProgress(true);
            BackgroundRequest.fetchAvailableOffers(getContext(), null, new OnFeedFetchFromServerListener() {

                @Override
                public void onDataFetch(boolean flag) {
                }

                @Override
                public void onDataFetch(boolean flag, String response) {
                    showProgress(false);
                    if (flag) {
                        onResponseFromServer(response);
                    } else {
                        emptyCheck();
                    }
                }

                @Override
                public void onDataFetch(boolean flag, int viewType) {
                }

                @Override
                public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {
                }
            });
        }
    }

    private void onResponseFromServer(String response) {
        offersList.clear();
        List<Offer> offers = JsonParser.getInstance().getAvailableOffers(response);
        if (offers.size() == 1) {
            launchDashBoard(offers.get(0));
        } else {
            offersList.addAll(JsonParser.getInstance().getAvailableOffers(response));
            offeringAdapter.notifyDataSetChanged();
            emptyCheck();
        }

    }

    private void emptyCheck() {
        if (offeringAdapter.getItemCount() > 0) {
            tvErrorMsg.setVisibility(View.GONE);
        } else {
            tvErrorMsg.setVisibility(View.VISIBLE);
        }
    }

    private void showProgress(boolean show) {
        if (show) {
            if (progressBar != null)
                progressBar.setVisibility(View.VISIBLE);
        } else {
            if (progressBar != null)
                progressBar.setVisibility(View.GONE);
        }
    }

    private void launchDashBoard(Offer offer) {
        boolean isTermsDisabled = SWAppPreferences.getInstance(getContext()).getBooleanValue(String.format(SWAppPreferences.Keys.OFFER_TERMS_DISABLED, offer.id), false);
        boolean isDialogTncEmpty = TextUtils.isEmpty(offer.dialogTnc);
        switch (offer.action) {
            case Action.CategoryAction.CAT_ACTION_OFFER_SHARE_WIN:
                if (!isTermsDisabled && !isDialogTncEmpty) {
                    showTermsnCondition(offer);
                } else {
                    if (NetworkStatus.getInstance().isConnected(getContext())) {
                        AppUtils.getInstance().startOffer(getActivity(), offer, false);
                    } else {
                        AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.no_network_error));
                    }
                }
                break;
            case Action.CategoryAction.CAT_ACTION_WAP:
                if (!isTermsDisabled && !isDialogTncEmpty) {
                    showTermsnCondition(offer);
                } else {
                    AppUtils.getInstance().startWapOffer(getActivity(), offer);
                }
                break;
            case Action.CategoryAction.CAT_ACTION_OFFER_EMPTY:
                if (!isTermsDisabled && !isDialogTncEmpty) {
                    showTermsnCondition(offer);
                } else {

                }
        }
        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.OFFER, AppFlyerConst.Key.CLICK, offer.gaEventLabel, AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_CAMPAIGN, ""));
    }

    private void showTermsnCondition(Offer offer) {
        OffersTermsFragment offersTermsFragment = OffersTermsFragment.getInstance(offer, () -> {
            SWAppPreferences.getInstance(getContext()).setBooleanValue(String.format(SWAppPreferences.Keys.OFFER_TERMS_DISABLED, offer.id), true);
            switch (offer.action) {
                case Action.CategoryAction.CAT_ACTION_OFFER_SHARE_WIN:
                    if (NetworkStatus.getInstance().isConnected(getContext())) {
                        AppUtils.getInstance().startOffer(getActivity(), offer, false);
                    } else {
                        AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.no_network_error));
                    }
                    break;
                case Action.CategoryAction.CAT_ACTION_WAP:
                    AppUtils.getInstance().startWapOffer(getActivity(), offer);
                    break;
                case Action.CategoryAction.CAT_ACTION_OFFER_EMPTY:
                    break;
            }
        });
        offersTermsFragment.show(getChildFragmentManager(), OffersTermsFragment.class.getName());
    }

    class OfferingAdapter extends RecyclerView.Adapter<OfferingAdapter.ViewHolder> {

        private Context context;
        private List<Offer> mList;

        OfferingAdapter(Context context, List<Offer> offers) {
            this.context = context;
            this.mList = offers;

        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new ViewHolder(inflater.inflate(R.layout.row_offer_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Offer offers = mList.get(position);
            holder.tvName.setText(offers.name);
            String imageURL = offers.bannerImage;
            if (!TextUtils.isEmpty(imageURL)) {
                if (imageURL.contains(".gif")) {
                    ImageUtil.setImageGif(context, imageURL, holder.ivOffer, R.drawable.placeholder);

                } else {
                    ImageUtil.setImage(context, imageURL, holder.ivOffer, R.drawable.placeholder);
                }
            }


            holder.itemView.setOnClickListener(v -> launchDashBoard(offers));
        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView ivOffer;
            private TextView tvName;

            public ViewHolder(View itemView) {
                super(itemView);
                ivOffer = itemView.findViewById(R.id.ivOffer);
                tvName = itemView.findViewById(R.id.tvName);
            }

        }
    }
}
