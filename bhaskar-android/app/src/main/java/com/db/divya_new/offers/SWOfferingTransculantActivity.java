package com.db.divya_new.offers;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.local.prefs.SWAppPreferences;
import com.bhaskar.data.model.Offer;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.source.server.BackgroundRequest;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.util.Action;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;

import java.util.ArrayList;
import java.util.List;


public class SWOfferingTransculantActivity extends BaseAppCompatActivity implements Switcher {

    private String themeColor;
    private String mTitle, gaScreen;
    private boolean isComingFromHamburger = false;
    private ProgressBar progressBar;
    private List<Offer> offersList;

    public static Intent getIntent(Context context, String color, String title, String gaScreen, boolean isComingFromHam) {
        Intent intent = new Intent(context, SWOfferingTransculantActivity.class);
        intent.putExtra(Constants.KeyPair.KEY_COLOR, color);
        intent.putExtra(Constants.KeyPair.KEY_TITLE, title);
        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        intent.putExtra("isComingFromHa,", isComingFromHam);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // overridePendingTransition(R.anim.splash_fade_in, R.anim.splash_fade_out);
        //android O fix bug orientation
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_sw_trans_offering);
        progressBar = findViewById(R.id.progressBar);
        offersList = new ArrayList<>();
        handleExtras(getIntent());
        getLatestOffers();
//        handleToolbar();
    }

    private void handleExtras(Intent intent) {
        if (intent == null)
            return;
        themeColor = intent.getStringExtra(Constants.KeyPair.KEY_COLOR);
        mTitle = intent.getStringExtra(Constants.KeyPair.KEY_TITLE);
        gaScreen = intent.getStringExtra(Constants.KeyPair.KEY_GA_SCREEN);
        isComingFromHamburger = intent.getBooleanExtra("isComingFromHa", false);
    }


    private void getLatestOffers() {
        if (JsonParser.getInstance().isOfferActionAvailable(SWOfferingTransculantActivity.this)) {
            showProgress(true);
            BackgroundRequest.fetchAvailableOffers(SWOfferingTransculantActivity.this, null, new OnFeedFetchFromServerListener() {
//                @Override
//                public void onDataFetch(boolean flag, BrandInfo brandInfo) {
//                }

                @Override
                public void onDataFetch(boolean flag) {
                }

                @Override
                public void onDataFetch(boolean flag, String response) {
                    showProgress(false);
                    if (flag) {
                        onResponseFromServer(response);
                    } else {
                        finish();
                    }
                }

                @Override
                public void onDataFetch(boolean flag, int viewType) {
                }

                @Override
                public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {
                }
            });
        }
    }

    private void showProgress(boolean show) {
        if (show) {
            if (progressBar != null)
                progressBar.setVisibility(View.VISIBLE);
        } else {
            if (progressBar != null)
                progressBar.setVisibility(View.GONE);
        }
    }

    private void onResponseFromServer(String response) {
        offersList = JsonParser.getInstance().getAvailableOffers(response);
        List<Offer> offers = JsonParser.getInstance().getAvailableOffers(response);
        if (offers.size() == 1) {
            launchDashBoard(offers.get(0));
        } else {
            startActivity(SWOfferingActivity.getIntent(SWOfferingTransculantActivity.this, themeColor, mTitle, gaScreen, isComingFromHamburger));
        }
        finish();
    }


    private void launchDashBoard(Offer offer) {
        boolean isTermsDisabled = SWAppPreferences.getInstance(SWOfferingTransculantActivity.this).getBooleanValue(String.format(SWAppPreferences.Keys.OFFER_TERMS_DISABLED, offer.id), false);
        boolean isDialogTncEmpty = TextUtils.isEmpty(offer.dialogTnc);
        switch (offer.action) {
            case Action.CategoryAction.CAT_ACTION_OFFER_SHARE_WIN:
                if (!isTermsDisabled && !isDialogTncEmpty) {
                    showTermsnCondition(offer);
                } else {
                    if (NetworkStatus.getInstance().isConnected(SWOfferingTransculantActivity.this)) {
                        AppUtils.getInstance().startOffer(SWOfferingTransculantActivity.this, offer, false);
                    } else {
                        AppUtils.getInstance().showCustomToast(SWOfferingTransculantActivity.this, getString(R.string.no_network_error));
                    }
                }
                break;
            case Action.CategoryAction.CAT_ACTION_WAP:
                if (!isTermsDisabled && !isDialogTncEmpty) {
                    showTermsnCondition(offer);
                } else {
                    AppUtils.getInstance().startWapOffer(SWOfferingTransculantActivity.this, offer);
                }
                break;
            case Action.CategoryAction.CAT_ACTION_OFFER_EMPTY:
                if (!isTermsDisabled && !isDialogTncEmpty) {
                    showTermsnCondition(offer);
                }
        }
        Tracking.trackGAEvent(SWOfferingTransculantActivity.this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.OFFER, AppFlyerConst.Key.CLICK, offer.gaEventLabel, AppPreferences.getInstance(SWOfferingTransculantActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, ""));
    }

    private void showTermsnCondition(Offer offer) {
        OffersTermsFragment offersTermsFragment = OffersTermsFragment.getInstance(offer, () -> {
            SWAppPreferences.getInstance(SWOfferingTransculantActivity.this).setBooleanValue(String.format(SWAppPreferences.Keys.OFFER_TERMS_DISABLED, offer.id), true);
            switch (offer.action) {
                case Action.CategoryAction.CAT_ACTION_OFFER_SHARE_WIN:
                    if (NetworkStatus.getInstance().isConnected(SWOfferingTransculantActivity.this)) {
                        AppUtils.getInstance().startOffer(SWOfferingTransculantActivity.this, offer, false);
                    } else {
                        AppUtils.getInstance().showCustomToast(SWOfferingTransculantActivity.this, getString(R.string.no_network_error));
                    }
                    break;
                case Action.CategoryAction.CAT_ACTION_WAP:
                    AppUtils.getInstance().startWapOffer(SWOfferingTransculantActivity.this, offer);
                    break;
                case Action.CategoryAction.CAT_ACTION_OFFER_EMPTY:
                    break;
            }
        });
        offersTermsFragment.show(getSupportFragmentManager(), OffersTermsFragment.class.getName());
    }

    private void handleToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(AppUtils.getThemeColor(SWOfferingTransculantActivity.this, themeColor));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back);
        }

        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        AppUtils.getInstance().setStatusBarColor(SWOfferingTransculantActivity.this, themeColor);
        TextView tvToolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        tvToolbarTitle.setText(mTitle);
    }

    @Override
    public void switcher(Fragment fragment, boolean saveInBackstack, boolean animate) {

    }
}
