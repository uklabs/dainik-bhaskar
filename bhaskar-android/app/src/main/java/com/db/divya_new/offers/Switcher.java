package com.db.divya_new.offers;

import androidx.fragment.app.Fragment;

public interface Switcher {

    void switcher(Fragment fragment, boolean saveInBackstack, boolean animate);

}
