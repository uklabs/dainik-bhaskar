package com.db.divya_new.photoGallary;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.divya_new.common.DataParser;
import com.db.divya_new.common.LoadMoreController;
import com.db.divya_new.common.TabController;
import com.db.divya_new.data.CatHomeData;
import com.db.listeners.OnDBVideoClickListener;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryListAdapter;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class PhotoGallaryParentViewHolder extends RecyclerView.ViewHolder implements OnDBVideoClickListener, LoadMoreController {
    private final String TAG = AppConfig.BaseTag + "." + PhotoGallaryParentViewHolder.class.getSimpleName();
    private Context context;
    private Button btnHeaderLoadMore;
    private Button btnSubHeaderLoadMore;
    private ProgressBar loadMoreProgressBar;
    private RecyclerView photoGallaryRecyclerView;
    private PhotoGalleryListAdapter photoGalleryListAdapter;
    private CategoryInfo categoryInfo;
    private int showCount;
    private String id;
    private Button btnRetry;
    private boolean isLoading;
    private boolean isLoadMoreEnabled = true;
    private String mParentId;
    private boolean isFromHome;
    private boolean isFromParentHome;

    private int LOAD_MORE_COUNT = 0;
    private boolean isRefreshing;

    public PhotoGallaryParentViewHolder(View itemView, String superParentId) {
        super(itemView);
        this.context = itemView.getContext();
        photoGalleryListAdapter = new PhotoGalleryListAdapter(context, this);
        photoGallaryRecyclerView = itemView.findViewById(R.id.recycler_view);
        btnHeaderLoadMore = itemView.findViewById(R.id.btnHeaderLoadMore);
        btnSubHeaderLoadMore = itemView.findViewById(R.id.btnSubHeaderLoadMore);
        btnRetry = itemView.findViewById(R.id.btn_retry);
        loadMoreProgressBar = itemView.findViewById(R.id.loadmoreprogressBar);

        btnRetry.setOnClickListener(view -> onRetry());

    }

    private void onRetry() {
        if (!isLoading) {
            if (!isRefreshing)
                loadMoreProgressBar.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.GONE);
            LOAD_MORE_COUNT = 1;
            makeJsonObjectRequest(Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + LOAD_MORE_COUNT + "/", true);
        }
    }

    private void loadMoreLogic() {
        if (isFromParentHome) {
            btnHeaderLoadMore.setVisibility(View.VISIBLE);
            btnSubHeaderLoadMore.setVisibility(View.GONE);
        } else if (isFromHome) {
            btnHeaderLoadMore.setVisibility(View.GONE);
            btnSubHeaderLoadMore.setVisibility(View.VISIBLE);
        } else {
            //infinite loading true
            btnHeaderLoadMore.setVisibility(View.GONE);
            btnSubHeaderLoadMore.setVisibility(View.GONE);
        }

        btnHeaderLoadMore.setOnClickListener(v -> onLoadMore());
        btnSubHeaderLoadMore.setOnClickListener(v -> onLoadMore());
    }

    private void onLoadMore() {
        if (isFromHome && isFromParentHome) {
            if (TabController.getInstance().getMenuTabControllerInstance() != null && !TabController.getInstance().isTabClicked()) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            TabController.getInstance().setTabClicked(true);
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(id, PhotoGallaryParentViewHolder.this);
                            loadMoreProgressBar.setVisibility(View.GONE);
                            //loadMoreTextView.setVisibility(View.VISIBLE);
                        } catch (Exception ignored) {
                        }
                    }
                }, 200);
            }
        } else if (isFromHome) {
            if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(mParentId);
            }
        } else if (!isFromHome && !isFromParentHome) {
            if (!isLoading) {
                loadMoreProgressBar.setVisibility(View.VISIBLE);
                btnRetry.setVisibility(View.GONE);
                makeJsonObjectRequest(Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + LOAD_MORE_COUNT + "/", false);
            }
        }
    }

    @Override
    public void onDbVideoClick(int position) {
        if (position >= 0 && position < photoGalleryListAdapter.getItemCount()) {
            Intent photoGalleryIntent = new Intent(context, PhotoGalleryActivity.class);
            photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, photoGalleryListAdapter.getItem(position).rssId);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, photoGalleryListAdapter.getItem(position).gTrackUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_TRACK_URL, photoGalleryListAdapter.getItem(position).trackUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            context.startActivity(photoGalleryIntent);
        }
    }


    public void setData(CategoryInfo categoryInfo, int count, boolean isFromHome, boolean isFromParentHome, String pParentId, String pSuperParentId, String horizontalList) {
        this.categoryInfo = categoryInfo;
        showCount = count;

        this.isFromHome = isFromHome;
        this.isFromParentHome = isFromParentHome;
        if (isFromParentHome) {
            this.mParentId = pSuperParentId;
        } else {
            this.mParentId = pParentId;
        }
        this.id = categoryInfo.id;
        LOAD_MORE_COUNT = 0;
        if (TextUtils.isEmpty(horizontalList)) {
            horizontalList = "0";
        }
//        if (showCount == 0 && horizontalList.equalsIgnoreCase("0")) {
//            showCount = 5;
//        }

        if (horizontalList.equalsIgnoreCase("0")) {
            photoGallaryRecyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        } else {
            photoGallaryRecyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        }
        photoGallaryRecyclerView.setAdapter(photoGalleryListAdapter);

        photoGalleryListAdapter.setDataInAdapter((horizontalList.equalsIgnoreCase("0")) ? PhotoGalleryListAdapter.MAIN_LIST : PhotoGalleryListAdapter.LIST_HORIZONTAL, this.showCount);

        CatHomeData catHomeData = DataParser.getInstance().getCatHomeDataHashMapById(id);
        long currentTime = System.currentTimeMillis();
        if (catHomeData != null && ((currentTime - catHomeData.getFeedEntryTime()) <= InitApplication.getInstance().getServerTimeoutValue()) && LOAD_MORE_COUNT <= 2) {
            parseOfflineData(catHomeData);
            LOAD_MORE_COUNT = 2;
        } else {
            LOAD_MORE_COUNT = 1;
            if (!isRefreshing)
                loadMoreProgressBar.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.GONE);
            makeJsonObjectRequest(Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + LOAD_MORE_COUNT + "/", true);
        }
    }

    private void parseOfflineData(CatHomeData catHomeData) {
        int count = catHomeData.count;

        ArrayList<PhotoListItemInfo> list = null;
        try {
            list = (ArrayList<PhotoListItemInfo>) catHomeData.getArrayList(CatHomeData.TYPE_PHOTOS);
        } catch (Exception ignored) {
        }
        if (list != null) {
            isLoadMoreEnabled = list.size() >= count;
            handleUI(list, true);
        }
    }


    private void handleUI(ArrayList<PhotoListItemInfo> list, boolean isClear) {
        //passing the data to the adapter to show.
        if (showCount > 0 && showCount < list.size()) {
            ArrayList<PhotoListItemInfo> localList = new ArrayList<>();
            for (int i = 0; i < showCount; i++) {
                localList.add(list.get(i));
            }
            if (isClear) {
                photoGalleryListAdapter.setDataAndClear(localList, categoryInfo);
            } else {
                photoGalleryListAdapter.setData(localList, categoryInfo);
            }

        } else {
            if (isClear) {
                photoGalleryListAdapter.setDataAndClear(list, categoryInfo);
            } else {
                photoGalleryListAdapter.setData(list, categoryInfo);
            }
        }

        // handling the load more button based on the data we got
//        if (isFromParentHome || (!isFromHome && isLoadMoreEnabled)) {
//            loadMoreTextView.setVisibility(View.VISIBLE);
//        } else {
//            loadMoreTextView.setVisibility(View.GONE);
//        }
        loadMoreLogic();
    }


    private void makeJsonObjectRequest(String finalFeedURL, final boolean isClear) {
        if (NetworkStatus.getInstance().isConnected(context)) {
            isLoading = true;
            AppLogs.printErrorLogs(TAG + "Feed URL", finalFeedURL);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response != null) {
                                    parseFeedList(response, isClear);
                                }
                            } catch (Exception ignored) {
                            }
                            isLoading = false;
                            loadMoreProgressBar.setVisibility(View.GONE);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }

                    loadMoreProgressBar.setVisibility(View.GONE);
                    isLoading = false;

                    if (photoGalleryListAdapter.getItemCount() == 0) {
                        btnRetry.setVisibility(View.VISIBLE);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);

        } else {
            if (context != null)
                AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
            //hideProgress();
            loadMoreProgressBar.setVisibility(View.GONE);
            if (photoGalleryListAdapter.getItemCount() == 0) {
                btnRetry.setVisibility(View.VISIBLE);
            }
        }
    }

    private void parseFeedList(JSONObject response, boolean isClear) throws JSONException {
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        int count = response.optInt("count");
        if (resultArray != null) {
            ArrayList<PhotoListItemInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), PhotoListItemInfo[].class)));

            if (LOAD_MORE_COUNT <= 1 && !TextUtils.isEmpty(id)) {
                CatHomeData catHomeData = new CatHomeData(list, count, CatHomeData.TYPE_PHOTOS, System.currentTimeMillis());
                DataParser.getInstance().addCatHomeDataHashMap(id, catHomeData);
            }

            if (resultArray.length() < count) {
                isLoadMoreEnabled = false;
            } else {
                isLoadMoreEnabled = true;
                LOAD_MORE_COUNT++;
            }

            handleUI(list, isClear);
        }
    }


    @Override
    public void onLoadFinish() {

    }

    public void clearDataAndLoadNew() {
        photoGalleryListAdapter.clear();
    }

    public void setIsRefreshing(boolean isRefreshing) {

        this.isRefreshing = isRefreshing;
    }
}
