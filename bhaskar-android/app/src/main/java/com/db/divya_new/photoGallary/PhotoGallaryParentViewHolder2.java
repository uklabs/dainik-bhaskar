package com.db.divya_new.photoGallary;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.database.DatabaseClient;
import com.db.divya_new.data.CatHome2;
import com.db.listeners.OnDBVideoClickListener;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryListAdapter2;
import com.db.util.Constants;

public class PhotoGallaryParentViewHolder2 extends RecyclerView.ViewHolder implements OnDBVideoClickListener {

    private Context context;
    private RecyclerView photoGallaryRecyclerView;
    private PhotoGalleryListAdapter2 photoGalleryListAdapter;
    private String catID;

    public PhotoGallaryParentViewHolder2(View itemView) {
        super(itemView);
        this.context = itemView.getContext();

        photoGalleryListAdapter = new PhotoGalleryListAdapter2(context, this);
        photoGallaryRecyclerView = itemView.findViewById(R.id.recycler_view);
        photoGallaryRecyclerView.setAdapter(photoGalleryListAdapter);
    }

    @Override
    public void onDbVideoClick(int position) {
        if (position >= 0 && position < photoGalleryListAdapter.getItemCount()) {
            CategoryInfo categoryInfo = DatabaseClient.getInstance(context).getCategoryById(catID);
            if (categoryInfo != null) {
                Intent photoGalleryIntent = new Intent(context, PhotoGalleryActivity.class);
                photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, categoryInfo.detailUrl);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, photoGalleryListAdapter.getItem(position).rssId);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, photoGalleryListAdapter.getItem(position).gTrackUrl);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_TRACK_URL, photoGalleryListAdapter.getItem(position).trackUrl);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
                context.startActivity(photoGalleryIntent);
            }
        }
    }

    public void setData(CatHome2 catHome) {
        catID = catHome.id;

        if (TextUtils.isEmpty(catHome.horizontalList) || catHome.horizontalList.equalsIgnoreCase("0")) {
            photoGallaryRecyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
            photoGalleryListAdapter.setData(catHome.list, catHome.color, catHome.id, PhotoGalleryListAdapter2.MAIN_LIST);
        } else {
            photoGallaryRecyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
            photoGalleryListAdapter.setData(catHome.list, catHome.color, catHome.id, PhotoGalleryListAdapter2.LIST_HORIZONTAL);
        }
    }
}
