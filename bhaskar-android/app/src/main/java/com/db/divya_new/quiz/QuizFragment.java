package com.db.divya_new.quiz;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.QuizOption;
import com.db.data.models.QuizQuestion;
import com.db.homebanner.BannerViewHolder;
import com.bhaskar.util.LoginController;
import com.bhaskar.data.model.ProfileInfo;
import com.db.news.WapV2Activity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuizFragment extends Fragment {
    private static final String TAG = QuizFragment.class.getName();
    public static String QUIZ_STATUS = "quiz_%s_%s";//quiz id and user id
    public final int LOGIN_REQUEST_CODE = 446;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    private boolean isLoading;
    private TextView tvQuestion;
    private RecyclerView recyclerViewBanner;
    private TextView tvQuestionTitle;
    private TextView tvOption;
    private QuizQuestion quizQuestion;
    private View vQuiz;
    private CategoryInfo categoryInfo;
    private OptionAdapter optionAdapter;
    private List<QuizOption> quizOptionList;
    private RecyclerView recyclerViewOption;
    private TextView tvAnsSubtitle;
    private ImageView ivCongrats;
    private ImageView ivIcon;
    private Button btnSubmit;
    private View vAbout;
    private View vQuizQuestion;
    private ImageView ivInfo;
    private TextView tvAbout;
    private String themeColor;
    private WebView webViewPromotion;
    private ImageView ivLogo;
    private ProfileInfo profileInfo;
    private List<BannerInfo> bannerInfoList;
    private String correctAns;


    public static QuizFragment getInstance(CategoryInfo categoryInfo) {
        QuizFragment fragment = new QuizFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryInfo = (CategoryInfo) getArguments().getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            themeColor = categoryInfo != null ? categoryInfo.color : null;
        }
        quizOptionList = new ArrayList<>();
        profileInfo = LoginController.loginController().getCurrentUser();

        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quiz, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        tvQuestion = view.findViewById(R.id.tvQuestion);
        tvQuestionTitle = view.findViewById(R.id.tvQuestionTitle);
        recyclerViewBanner = view.findViewById(R.id.recyclerViewBanner);
        tvOption = view.findViewById(R.id.tvOption);
        tvAnsSubtitle = view.findViewById(R.id.tvAnsSubtitle);
        ivCongrats = view.findViewById(R.id.ivCongrats);
        tvAbout = view.findViewById(R.id.tvAbout);
        vQuizQuestion = view.findViewById(R.id.vQuizQuestion);
        ivInfo = view.findViewById(R.id.ivInfo);
        vAbout = view.findViewById(R.id.vAbout);
        ivIcon = view.findViewById(R.id.ivIcon);
        vQuiz = view.findViewById(R.id.vQuiz);
        btnSubmit = view.findViewById(R.id.btnSubmit);
        recyclerViewOption = view.findViewById(R.id.recycler_view_options);
        webViewPromotion = view.findViewById(R.id.webViewPromotion);
        ivLogo = view.findViewById(R.id.ivLogo);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);


        swipeRefreshLayout.setEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(() -> getQuizDetails(false));
        vAbout.setOnClickListener(v -> {
            if (quizQuestion != null) {
                Intent intent = new Intent(getContext(), WapV2Activity.class);
                intent.putExtra(Constants.KeyPair.KEY_URL, quizQuestion.quizData.aboutUrl);
                intent.putExtra(Constants.KeyPair.KEY_TITLE, quizQuestion.quizData.quizName);
                intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                startActivity(intent);
            }
        });
        btnSubmit.setOnClickListener(v -> {
            if (optionAdapter != null) {
                int ansOption = optionAdapter.getSelectedOption() + 1;
                if (ansOption > 0) {
                    if (LoginController.loginController().isUserLoggedIn()) {
                        profileInfo = LoginController.loginController().getCurrentUser();
                        submitQuizAnswer(String.valueOf(ansOption));
                    } else {
                        showQuizLoginDialog();
                    }
                } else {
                    AppUtils.getInstance().showCustomToast(getContext(), "Please select an answer first!");
                }

            }
        });
        overrideTheme();

        //setupbanner
        BannerAdapter bannerAdapter = new BannerAdapter(getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerViewBanner.setLayoutManager(layoutManager);
        recyclerViewBanner.setAdapter(bannerAdapter);
        return view;
    }

    private void showQuizLoginDialog() {
        Context context = getContext();
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_login_quiz, null);
            builder.setView(view);
            builder.setCancelable(false);

            final AlertDialog alert = builder.create();
            TextView tvBodyMsg = view.findViewById(R.id.tvBodyMsg);
            tvBodyMsg.setText(getString(R.string.quiz_dialog_msg));

            view.findViewById(R.id.btnNegative).setOnClickListener(view12 -> {
                alert.dismiss();
            });

            view.findViewById(R.id.btnPositive).setOnClickListener(view1 -> {
                alert.dismiss();
                LoginController.loginController().getLoginIntent(getActivity(), TrackingData.getDBId(getContext()), TrackingData.getDeviceId(getContext()), LOGIN_REQUEST_CODE);
            });

            alert.show();
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), categoryInfo.gaScreen, source, medium, campaign);
        getQuizDetails(true);
    }


    public void getQuizDetails(boolean showProgress) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
            isLoading = true;
            profileInfo = LoginController.loginController().getCurrentUser();
            if (showProgress)
                progressBar.setVisibility(View.VISIBLE);
            String finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl;
            AppLogs.printDebugLogs(TAG + "Rajya Feed URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalFeedURL, null,
                    response -> {
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        try {
                            if (response != null) {
                                Gson gson = new Gson();
                                quizQuestion = gson.fromJson(response.toString(), QuizQuestion.class);
                                ImageUtil.setImage(getContext(), quizQuestion.quizData.logoImgUrl, ivLogo, 0);

                                String userId = (profileInfo == null) ? "" : profileInfo.uID;
                                String quizState = AppPreferences.getInstance(getContext()).getStringValue(String.format(QUIZ_STATUS, userId, quizQuestion.quizData.qId), "");
                                if (!TextUtils.isEmpty(quizState) && profileInfo.isUserLoggedIn) {
                                    JSONObject jsonObject = new JSONObject(quizState);
                                    //questionId ansStatus"
                                    String questionID = jsonObject.optString("questionId");
                                    String ansStatus = jsonObject.optString("ansStatus");
                                    if (!TextUtils.isEmpty(quizQuestion.queData.queId) && quizQuestion.queData.queId.equalsIgnoreCase(questionID)) {
                                        showQuizState("2");
                                    } else {
                                        updateQuizUI(quizQuestion);
                                    }

                                } else {
                                    updateQuizUI(quizQuestion);
                                }
                            }
                        } catch (Exception e) {
                            AppLogs.printErrorLogs(TAG + "RajyaNewsList", "Error: " + e);
                        }
                    }, error -> {
                isLoading = false;
                progressBar.setVisibility(View.GONE);
                Context context = getContext();
                swipeRefreshLayout.setRefreshing(false);
                if (context != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
        } else {
            if (getContext() != null) {
                AppUtils.getInstance().showCustomToast(getContext(), getContext().getString(R.string.no_network_error));
            }
        }
    }

    void updateQuizUI(QuizQuestion quizQuestion) {
        try {
            setPromotionalWebView(quizQuestion.quizData.iFrameUrl);
            quizOptionList.clear();
            List<String> optionList = quizQuestion.queData.options;
            if (optionList == null)
                optionList = new ArrayList<>();
            for (String option : optionList) {
                quizOptionList.add(new QuizOption(option));
            }
            showQuizState("-1");
            updateUI(quizQuestion);
        } catch (Exception ignored) {
        }

    }

    private void setPromotionalWebView(String iFrame) {
        if (TextUtils.isEmpty(iFrame)) {
            webViewPromotion.setVisibility(View.GONE);
        } else {
            webViewPromotion.setVisibility(View.VISIBLE);
            webViewPromotion.loadUrl(iFrame);
        }
    }

    public void submitQuizAnswer(String quizOption) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            isLoading = true;
            progressBar.setVisibility(View.VISIBLE);
            JSONObject requestObject = getRequestObject(quizOption);
            AppLogs.printDebugLogs(TAG + "submitQuizAnswer", "== " + Urls.APP_QUIZ_URL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Urls.APP_QUIZ_URL, requestObject,
                    response -> {
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                        try {
                            if (response != null) {
                                String status = response.optString("status");
                                if (status.equalsIgnoreCase("Success")) {
                                    String ansStatus = response.optString("ans_status");
                                    correctAns = response.optString("correct_ans");
                                    saveQuizState(ansStatus, quizQuestion);
                                    showQuizState(ansStatus);
                                } else {
                                    String msg = response.optString("msg");
                                    AppUtils.getInstance().showCustomToast(getContext(), msg);
                                }

                            }
                        } catch (Exception e) {
                            AppLogs.printErrorLogs(TAG + "RajyaNewsList", "Error: " + e);
                        }
                    }, error -> {
                isLoading = false;
                progressBar.setVisibility(View.GONE);
                Context context = getContext();
                if (context != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
            if (getContext() != null) {
                AppUtils.getInstance().showCustomToast(getContext(), getContext().getString(R.string.no_network_error));
            }
        }
    }

    private void saveQuizState(String ansStatus, QuizQuestion quizQuestion) {
        AppPreferences.getInstance(getContext()).setStringValue(String.format(QUIZ_STATUS, profileInfo.uID, quizQuestion.quizData.qId), getQuizState(ansStatus, quizQuestion));
    }

    private String getQuizState(String ansStatus, QuizQuestion quizQuestion) {
        String quizState = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("questionId", quizQuestion.queData.queId);
            jsonObject.put("ansStatus", ansStatus);
            quizState = jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return quizState;
    }


    /**
     * -1 = normal
     * 0 = wrong
     * 1 = right
     * 2 = already ans
     *
     * @param state
     */
    private void showQuizState(String state) {
        switch (state) {
            case "0":
                recyclerViewOption.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.GONE);
                tvQuestion.setVisibility(View.GONE);
                tvQuestionTitle.setVisibility(View.GONE);
                tvAnsSubtitle.setVisibility(View.VISIBLE);

                tvAnsSubtitle.setText(Html.fromHtml(String.format(quizQuestion.quizData.wrongAns, getCorrectAns())));
                ivIcon.setVisibility(View.VISIBLE);
                ivCongrats.setVisibility(View.GONE);
                ImageUtil.setImage(getContext(), quizQuestion.quizData.wrongAnsImg, ivIcon, R.drawable.water_mark_news_list);
                break;
            case "1":
                recyclerViewOption.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.GONE);
                tvQuestion.setVisibility(View.GONE);
                tvQuestionTitle.setVisibility(View.GONE);
                tvAnsSubtitle.setVisibility(View.VISIBLE);
                tvAnsSubtitle.setText(Html.fromHtml(quizQuestion.quizData.correctAns));
                ivIcon.setVisibility(View.VISIBLE);
                ivCongrats.setVisibility(View.VISIBLE);
                ImageUtil.setImage(getContext(), quizQuestion.quizData.congratsImgUrl, ivCongrats, 0);
                ImageUtil.setImage(getContext(), quizQuestion.quizData.correctAnsImg, ivIcon, R.drawable.water_mark_news_list);
                break;
            case "2":
                recyclerViewOption.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.GONE);
                tvQuestion.setVisibility(View.GONE);
                tvQuestionTitle.setVisibility(View.GONE);
                tvAnsSubtitle.setVisibility(View.VISIBLE);
                tvAnsSubtitle.setText(Html.fromHtml(quizQuestion.quizData.alreadyAns));
                ivIcon.setVisibility(View.VISIBLE);
                ivCongrats.setVisibility(View.GONE);
                ImageUtil.setImage(getContext(), quizQuestion.quizData.alreadyAnsImg, ivIcon, R.drawable.water_mark_news_list);
                break;
            default:
                recyclerViewOption.setVisibility(View.VISIBLE);
                btnSubmit.setVisibility(View.VISIBLE);
                tvQuestion.setVisibility(View.VISIBLE);
                tvQuestionTitle.setVisibility(View.VISIBLE);
                tvAnsSubtitle.setVisibility(View.GONE);
                ivCongrats.setVisibility(View.GONE);
                ivIcon.setVisibility(View.GONE);

        }
    }

    private String getCorrectAns() {
        try {
            int option = Integer.parseInt(correctAns);
            return quizOptionList.get(option - 1).option;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    private JSONObject getRequestObject(String option) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("channel_slno", CommonConstants.CHANNEL_ID);
            jsonObject.put("u_id", (profileInfo == null) ? "" : profileInfo.uID);
            jsonObject.put("que_id", quizQuestion.queData.queId);
            jsonObject.put("ans", option);
            jsonObject.put("app_id", CommonConstants.BHASKAR_APP_ID);
            jsonObject.put("db_id", TrackingData.getDBId(getContext()));
            jsonObject.put("device_id", TrackingData.getDeviceId(getContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void updateUI(QuizQuestion quizQuestion) {
        vQuiz.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.VISIBLE);
        String question;
        if (!TextUtils.isEmpty(quizQuestion.queData.que)) {
            question = quizQuestion.queData.que;
        } else if (!TextUtils.isEmpty(quizQuestion.quizData.emptyQuestion)) {
            question = quizQuestion.quizData.emptyQuestion;
            btnSubmit.setVisibility(View.GONE);
        } else {
            question = "";
            btnSubmit.setVisibility(View.GONE);
        }
        String questionTitle = quizQuestion.queData.questionTitle;
        tvQuestion.setText(Html.fromHtml(question));
        if (!TextUtils.isEmpty(questionTitle)) {
            tvQuestionTitle.setVisibility(View.VISIBLE);
            tvQuestionTitle.setText(Html.fromHtml(questionTitle));
        } else {
            tvQuestionTitle.setVisibility(View.GONE);
        }
        setUpOptions();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE) {
            if (LoginController.loginController().isUserLoggedIn()) {
                profileInfo = LoginController.loginController().getCurrentUser();
                if(optionAdapter!=null){
                    submitQuizAnswer(String.valueOf(optionAdapter.getSelectedOption() + 1));
                }

            }
        }
    }

    public boolean doWeNeedSingleSpan(String word) {
        TextPaint paint = tvOption.getPaint();
        int wordwidth = (int) paint.measureText(word, 0, word.length() - 1);
        int screenwidth = getResources().getDisplayMetrics().widthPixels / 2;
        return wordwidth + 72 >= screenwidth;
    }

    private void overrideTheme() {
        vQuizQuestion.getBackground().clearColorFilter();
        vQuizQuestion.getBackground().setColorFilter(AppUtils.getThemeColor(getContext(), themeColor), PorterDuff.Mode.SRC_IN);
        tvAbout.setTextColor(AppUtils.getThemeColor(getContext(), themeColor));
        btnSubmit.getBackground().setColorFilter(AppUtils.getThemeColor(getContext(), themeColor), PorterDuff.Mode.SRC);
        ivInfo.getDrawable().clearColorFilter();
        ivInfo.getDrawable().setColorFilter(AppUtils.getThemeColor(getContext(), themeColor), PorterDuff.Mode.SRC_IN);
    }

    private void setUpOptions() {
        optionAdapter = new OptionAdapter(getContext(), quizOptionList);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), getSpanCount());
        recyclerViewOption.setLayoutManager(layoutManager);
        recyclerViewOption.setAdapter(optionAdapter);
    }

    private int getSpanCount() {
        for (QuizOption option : quizOptionList) {
            if (doWeNeedSingleSpan(option.option)) {
                return 1;
            }
        }
        return 2;
    }

    private void showProgress(boolean show) {
        if (show) {
            if (progressBar != null)
                progressBar.setVisibility(View.VISIBLE);
        } else {
            if (progressBar != null)
                progressBar.setVisibility(View.GONE);
        }
    }


    class OptionAdapter extends RecyclerView.Adapter<OptionAdapter.ViewHolder> {

        private List<QuizOption> optionList;
        private Drawable emptyDrawable;
        private Drawable filledDrawable;

        OptionAdapter(Context context, List<QuizOption> options) {
            this.optionList = options;
            emptyDrawable = context.getDrawable(R.drawable.ic_radio_button_unchecked_quiz_24dp);
            emptyDrawable.setColorFilter(AppUtils.getThemeColor(getContext(), themeColor), PorterDuff.Mode.SRC_IN);
            filledDrawable = context.getDrawable(R.drawable.ic_radio_button_checked_quiz_24dp);
            filledDrawable.setColorFilter(AppUtils.getThemeColor(getContext(), themeColor), PorterDuff.Mode.SRC_IN);
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new ViewHolder(inflater.inflate(R.layout.row_option_layout, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            QuizOption optionItem = optionList.get(position);
            holder.bindContent(optionItem);
            holder.tvOption.setText(Html.fromHtml(optionItem.option));
            if (optionItem.isSeleted) {
                holder.ivCheckBox.setImageDrawable(filledDrawable);
            } else {
                holder.ivCheckBox.setImageDrawable(emptyDrawable);
            }

        }

        @Override
        public int getItemCount() {
            return optionList.size();
        }

        int getSelectedOption() {
            for (int i = 0; i < optionList.size(); i++) {
                if (optionList.get(i).isSeleted)
                    return i;
            }
            return -1;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            private QuizOption answer;
            private ImageView ivCheckBox;
            private TextView tvOption;

            public ViewHolder(View itemView) {
                super(itemView);
                tvOption = itemView.findViewById(R.id.tvOption);
                ivCheckBox = itemView.findViewById(R.id.ivCheck);
                itemView.setOnClickListener(v -> {
                    clearAll();
                    answer.isSeleted = true;
                    notifyDataSetChanged();
                });
            }

            private void clearAll() {
                for (QuizOption quizOption : optionList) {
                    quizOption.isSeleted = false;
                }
            }

            private void bindContent(QuizOption ans) {
                this.answer = ans;
            }

        }
    }


    class BannerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {

        private final static int VIEW_TYPE_AD = 25;
        private List<Object> list = new ArrayList<>();
        private Context mContext;
        private boolean isBannerLoaded;

        private BannerAdapter(Context context) {
            mContext = context;
            addBanner();
            addAds();
        }

        private void addAds() {
            isBannerLoaded = false;
            if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
                list.add(0, AdController.showATFForAllListing(mContext, this));
            }
        }

        public void addBanner() {
//            if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
            for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
                int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catPos);
                if (!bannerInfoList.get(bannerIndex).isAdded && bannerPos > -1) {
                    list.add(0, bannerInfoList.get(bannerIndex));
                    bannerInfoList.get(bannerIndex).isAdded = true;
                }
            }
//            }
        }

        @Override
        public void onBannerLoaded() {
            isBannerLoaded = true;
            notifyItemChanged(0);
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            switch (viewType) {
                case VIEW_TYPE_AD:
                    return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));
                default:
                    return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof BannerViewHolder) {
                ((BannerViewHolder) holder).callwebViewOrSetWidgetData((BannerInfo) list.get(position));

            } else if (holder instanceof BannerAdViewHolder) {
                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
                if (((AdInfo) list.get(position)).catType == 1) {
                    if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                        adViewHolder.addAds(((AdInfo) list.get(position)).bannerAdView);

                    } else {
                        ImageView imageView = new ImageView(mContext);
                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setAdjustViewBounds(true);
                        imageView.setClickable(true);
                        String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                        ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                        adViewHolder.addAdsUrl(imageView);

                        imageView.setOnClickListener(view -> AppUtils.getInstance().clickOnAdBanner(mContext));
                    }
                } else {
                    adViewHolder.addAds(((AdInfo) list.get(position)).bannerAdView);
                }
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public int getItemViewType(int position) {
            Object object = list.get(position);
            if (object instanceof AdInfo) {
                return VIEW_TYPE_AD;
            }
            return 0;
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            public ViewHolder(View itemView) {
                super(itemView);

                itemView.setOnClickListener(v -> {
                });
            }
        }
    }
}
