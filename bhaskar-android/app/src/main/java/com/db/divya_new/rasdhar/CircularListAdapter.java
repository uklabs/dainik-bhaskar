package com.db.divya_new.rasdhar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.db.divya_new.common.viewholder.CircularImageTitleViewHolder;

import java.util.ArrayList;

public class CircularListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int mType;
    private ArrayList<String> mList;

    public CircularListAdapter(int type) {
        mList = new ArrayList<>();
        mType = type;
    }

    public void add(ArrayList<String> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mType == 0) {
            return new CircularImageTitleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_circular_image_title, parent, false));
        } else {
            return new CircularImageTitleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_circular_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CircularImageTitleViewHolder) {
            itemBindHolder((CircularImageTitleViewHolder) holder);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    /******** Bind Holders Methods ********/

    private void itemBindHolder(CircularImageTitleViewHolder viewHolder) {

    }
}
