package com.db.eventWrapper;

public class AppControlEventWrapper {

    private int apiHitValue;
    private boolean flag;

    public AppControlEventWrapper(int apiHitValue, boolean flag) {
        this.apiHitValue = apiHitValue;
        this.flag = flag;
    }

    public int getApiHitValue() {
        return apiHitValue;
    }

    public void setApiHitValue(int apiHitValue) {
        this.apiHitValue = apiHitValue;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
