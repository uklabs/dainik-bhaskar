package com.db.eventWrapper;

import android.view.View;

public class FragmentItemClickEventWrapper {

    private View view;
    private int position;

    public FragmentItemClickEventWrapper(View view, int position) {
        this.view = view;
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
