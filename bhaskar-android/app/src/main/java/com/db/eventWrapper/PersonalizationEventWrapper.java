package com.db.eventWrapper;

public class PersonalizationEventWrapper {

    private int refreshScreen;

    public PersonalizationEventWrapper(int refreshScreen) {
        this.refreshScreen = refreshScreen;
    }

    public int getRefreshScreen() {
        return refreshScreen;
    }

    public void setRefreshScreen(int refreshScreen) {
        this.refreshScreen = refreshScreen;
    }
}
