package com.db.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.bhaskar.BuildConfig;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.util.Action;
import com.bhaskar.util.ModuleUtilAll;
import com.bhaskar.view.ui.MainActivity;
import com.bhaskar.view.ui.home.HomeFragment;
import com.bhaskar.view.ui.myPage.MeraPageNewsFragment;
import com.bhaskar.view.ui.news.NewsListFragment;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryFragment;
import com.db.appintro.utils.DialogUtils;
import com.db.audioBulletin.NewsBulletinListingFragment;
import com.db.audioBulletin.NewsBulletinPlayerKtActivity;
import com.db.citySelectionV2.CitySelectionV2Frag;
import com.db.data.models.CategoryInfo;
import com.db.db_original.DBOriginalActivity;
import com.db.db_original.DbOriginalColumnListFragment;
import com.db.db_original.DbOriginalNewsListingFragment;
import com.db.db_original.DbOriginalPhotoListFragment;
import com.db.dbvideoPersonalized.detail.VideoBulletinActivity;
import com.db.divya_new.common.CatMenuFragment;
import com.db.divya_new.common.CatMenuMainFragment;
import com.db.divya_new.dharamDarshan.Songs.SongsFragment;
import com.db.divya_new.divyashree.CatHomeFragment;
import com.db.divya_new.divyashree.RajyaCatHomeFragment;
import com.db.divya_new.divyashree.StoryListFragment;
import com.db.divya_new.gujarati.GujaratiVideoFragment;
import com.db.divya_new.gujarati.VideoSectionFragment;
import com.db.divya_new.gujarati.VideoSectionListFragment;
import com.db.divya_new.guruvani.GuruvaniGuruFragment;
import com.db.divya_new.guruvani.GuruvaniTopicFragment;
import com.db.divya_new.jyotishVastu.JyotishVastuFragment;
import com.db.divya_new.jyotishVastu.RatnaListFragment;
import com.db.divya_new.offers.SWOfferingFragment;
import com.db.divya_new.offers.SWOfferingTransculantActivity;
import com.db.divya_new.quiz.QuizFragment;
import com.db.home.bookmark.BookmarkListActivity;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.news.PrefferedCityNewsListFragment;
import com.db.news.WapV2Activity;
import com.db.news.WebFullScreenActivity;
import com.db.news.WebViewGameActivity;
import com.db.news.rajya.RajyaNewsListFragment;
import com.db.news.rajya.RajyaV3Fragment;
import com.db.preferredcity.PreferredCityActivity;
import com.db.settings.SettingActivity;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.OpenNewsLinkInApp;
import com.db.util.QuickPreferences;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import java.util.ArrayList;
import java.util.HashMap;

public class
ActivityUtil {

    private static ActivityUtil activityUtil = null;

    public static ActivityUtil getInstance() {
        if (activityUtil == null)
            activityUtil = new ActivityUtil();
        return activityUtil;
    }

    public static Fragment getFragmentByCategory(CategoryInfo tabCategoryInfo, String sectionName, String prefixSectionName) {

        Fragment pagerFragment;

        switch (tabCategoryInfo.action) {
            case Action.CategoryAction.CAT_ACTION_CITY_SELECTION_V2:
                pagerFragment = CitySelectionV2Frag.newInstance(tabCategoryInfo);
//                pagerFragment = PrefferedCityNewsListFragment.newInstance(tabCategoryInfo, null);
                break;
            case Action.CategoryAction.CAT_ACTION_NEWS:
                pagerFragment = NewsListFragment.newInstance(tabCategoryInfo, false, "");
                break;

            case Action.CategoryAction.CAT_ACTION_PHOTO_GALLERY:
                pagerFragment = PhotoGalleryFragment.getInstance(tabCategoryInfo);
                break;

            case Action.CategoryAction.CAT_ACTION_VIDEO_GALLERY_V3:
                pagerFragment = GujaratiVideoFragment.newInstance(sectionName, tabCategoryInfo, tabCategoryInfo.id);
                break;

            case Action.CategoryAction.CAT_ACTION_VIDEO_SECTION:
                pagerFragment = VideoSectionFragment.newInstance(sectionName, tabCategoryInfo);
                break;
            case Action.CategoryAction.CAT_ACTION_VIDEO_SECTION_LIST:
                pagerFragment = VideoSectionListFragment.newInstance(sectionName, tabCategoryInfo);
                break;

            case Action.CategoryAction.CAT_ACTION_CITY_SELECTION:
                pagerFragment = PrefferedCityNewsListFragment.newInstance(tabCategoryInfo, null);
                break;

            case Action.CategoryAction.CAT_ACTION_RAJYA:
                pagerFragment = RajyaV3Fragment.newInstance(tabCategoryInfo, true);
                break;

            case Action.CategoryAction.CAT_ACTION_RAJYA_HOME:
                pagerFragment = RajyaCatHomeFragment.newInstance(tabCategoryInfo, tabCategoryInfo.parentId);
                break;

            case Action.CategoryAction.CAT_ACTION_RAJYA_LIST:
                pagerFragment = RajyaNewsListFragment.newInstance(tabCategoryInfo);
                break;

            case Action.CategoryAction.CAT_ACTION_WAP:
            case Action.CategoryAction.CAT_ACTION_WAP_V3:
                pagerFragment = WapV2Fragment.getInstance(tabCategoryInfo, true, false);
                break;

            case Action.CategoryAction.CAT_ACTION_WAP_USER:
                pagerFragment = WapV2Fragment.getInstance(tabCategoryInfo, true, true);
                break;

            case Action.CategoryAction.CAT_ACTION_CAT_MENU:
                pagerFragment = CatMenuMainFragment.newInstance(tabCategoryInfo, prefixSectionName);
                break;

            case Action.CategoryAction.CAT_ACTION_ALL_CAT_HOME:
                pagerFragment = HomeFragment.newInstance(tabCategoryInfo.feedUrl);
                break;

            case Action.CategoryAction.CAT_ACTION_STORY_LIST:
                pagerFragment = StoryListFragment.newInstance(tabCategoryInfo, tabCategoryInfo.parentId, false);
                break;
            case Action.CategoryAction.CAT_ACTION_STORY_LIST_V2:
                pagerFragment = StoryListFragment.newInstance(tabCategoryInfo, tabCategoryInfo.parentId, true);
                break;

            case Action.CategoryAction.CAT_ACTION_CAT_HOME:
                pagerFragment = CatHomeFragment.newInstance(tabCategoryInfo, tabCategoryInfo.parentId);
                break;

            case Action.CategoryAction.CAT_ACTION_RASHIFAL_V2:
                pagerFragment = JyotishVastuFragment.newInstance(tabCategoryInfo, tabCategoryInfo.parentId);
                break;

            case Action.CategoryAction.CAT_ACTION_RATNA:
                pagerFragment = RatnaListFragment.newInstance(tabCategoryInfo, tabCategoryInfo.parentId);
                break;

            case Action.CategoryAction.CAT_ACTION_GURUVANI_GURU:
                pagerFragment = GuruvaniGuruFragment.newInstance(tabCategoryInfo, tabCategoryInfo.parentId);
                break;

            case Action.CategoryAction.CAT_ACTION_GURUVANI_TOPIC:
                pagerFragment = GuruvaniTopicFragment.newInstance(tabCategoryInfo, tabCategoryInfo.parentId);
                break;

            case Action.CategoryAction.CAT_ACTION_SONGS:
                pagerFragment = SongsFragment.newInstance(tabCategoryInfo, tabCategoryInfo.parentId);
                break;

            case Action.CategoryAction.CAT_ACTION_OFFER:
                pagerFragment = SWOfferingFragment.getInstance(tabCategoryInfo.gaScreen, false);
                break;

            case Action.CategoryAction.CAT_ACTION_BULLETIN:
                pagerFragment = BulletinFragment.getInstance(tabCategoryInfo);
                break;

            case Action.CategoryAction.CAT_ACTION_DB_COLUMN:
                pagerFragment = DBColumnFragment.getInstance(false, tabCategoryInfo);
                break;

            case Action.CategoryAction.CAT_ACTION_NEWS_BRIEF:
                pagerFragment = NewsBriefListFragment2.getInstance(tabCategoryInfo);
                break;

            case Action.CategoryAction.CAT_ACTION_KAHANIYA:
                pagerFragment = com.db.novel.NovelFragment.newInstance(tabCategoryInfo);
                break;

            case Action.CategoryAction.CAT_ACTION_EXCLUSIVE:
                pagerFragment = ExclusiveFragment.newInstance(tabCategoryInfo, true, sectionName);
                break;

            case Action.CategoryAction.CAT_ACTION_MERA_PAGE:
                pagerFragment = MeraPageNewsFragment.newInstance(tabCategoryInfo);
                break;

            case Action.CategoryAction.CAT_ACTION_DAILY_QUIZ:
                pagerFragment = QuizFragment.getInstance(tabCategoryInfo);
                break;
            case Action.CategoryAction.CAT_ACTION_DB_ORG_NEWS:
                pagerFragment = DbOriginalNewsListingFragment.newInstance(tabCategoryInfo);
                break;
            case Action.CategoryAction.CAT_ACTION_DB_ORG_COLUMN:
                pagerFragment = DbOriginalColumnListFragment.newInstance(tabCategoryInfo);
                break;
            case Action.CategoryAction.CAT_ACTION_DB_ORG_PHOTO:
                pagerFragment = DbOriginalPhotoListFragment.newInstance(tabCategoryInfo);
                break;
            case Action.CategoryAction.CAT_ACTION_AUDIO_BULLETIN_LIST:
                pagerFragment = NewsBulletinListingFragment.Companion.newInstance(tabCategoryInfo);
                break;
            default:
                pagerFragment = NewsListFragment.newInstance(tabCategoryInfo, false, sectionName);
                break;
        }

        return pagerFragment;
    }

    public static Fragment getFragmentByCategoryForMenu(CategoryInfo tabCategoryInfo, String sectionName, String prefixSectionName) {

        Fragment pagerFragment;

        switch (tabCategoryInfo.action) {
            case Action.CategoryAction.CAT_ACTION_CAT_MENU:
                pagerFragment = CatMenuFragment.newInstance(tabCategoryInfo.parentId, tabCategoryInfo, prefixSectionName);
                break;

            default:
                pagerFragment = getFragmentByCategory(tabCategoryInfo, sectionName, prefixSectionName);
                break;
        }

        return pagerFragment;
    }

    public static boolean openCategoryActionOutside(Context activity, CategoryInfo categoryInfo) {
        if (categoryInfo != null) {
            switch (categoryInfo.action) {
                case Action.CategoryAction.CAT_ACTION_RADIO:
                    Intent intent = new Intent(activity, RadioActivity.class);
                    intent.putExtra(Constants.KeyPair.KEY_FEED_URL, categoryInfo.feedUrl);
                    intent.putExtra(Constants.KeyPair.KEY_TITLE, categoryInfo.menuName);
                    intent.putExtra(Constants.KeyPair.KEY_COLOR, categoryInfo.color);
                    intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                    activity.startActivity(intent);
                    return true;

                case Action.CategoryAction.CAT_ACTION_EPAPER:
                    String source = AppPreferences.getInstance(activity).getStringValue(QuickPreferences.UTM_SOURCE, "");
                    String medium = AppPreferences.getInstance(activity).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                    String campaign = AppPreferences.getInstance(activity).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
//                    EpaperSplashActivity.getInstance(activity).startEpaperInitialization(InitApplication.getInstance().getDefaultTracker(), categoryInfo.gaScreen, source, medium, campaign);
//                    ModuleUtil.getInstance().launchEpaper(activity);
                    ModuleUtilAll.Companion.getInstance(activity).launchModule(ModuleUtilAll.EPAPER_SPLASH, activity);
                    return true;

                case Action.CategoryAction.CAT_ACTION_SETTINGS:
                    Intent settingIntent = new Intent(activity, SettingActivity.class);
                    settingIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
                    activity.startActivity(settingIntent);
                    return true;

                case Action.CategoryAction.CAT_ACTION_BOOKMARK:
                    Intent bookmarkIntent = new Intent(activity, BookmarkListActivity.class);
                    bookmarkIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
                    activity.startActivity(bookmarkIntent);
                    return true;

                case Action.CategoryAction.CAT_ACTION_NOTIFICATION_CENTER:
                    Intent notiIntent = new Intent(activity, AppNotificationActivity.class);
                    notiIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
                    notiIntent.putExtra(Constants.KeyPair.LAUNCH_FROM_STACK_NOTIFICATION, false);
                    activity.startActivity(notiIntent);
                    return true;

                case Action.CategoryAction.CAT_ACTION_TV:
                    AppUtils.getInstance().callLiveTV(activity, categoryInfo, new OnFeedFetchFromServerListener() {
                        @Override
                        public void onDataFetch(boolean flag) {
                        }

                        @Override
                        public void onDataFetch(boolean flag, String sectionName) {
                        }

                        @Override
                        public void onDataFetch(boolean flag, int viewType) {
                        }

                        @Override
                        public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {
                            AppUtils.getInstance().callLiveTV(activity, categoryInfo, null);
                        }
                    });
                    return true;

                case Action.CategoryAction.CAT_ACTION_CHROME:
                    Intent webIntent2 = new Intent(activity, WebViewGameActivity.class);
                    webIntent2.putExtra(Constants.KeyPair.KEY_URL, categoryInfo.feedUrl);
                    webIntent2.putExtra(Constants.KeyPair.KEY_TITLE, categoryInfo.menuName);
                    webIntent2.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                    activity.startActivity(webIntent2);
                    return true;

                case Action.CategoryAction.CAT_ACTION_WAP:
                    Intent webIntent = new Intent(activity, WapV2Activity.class);
                    webIntent.putExtra(Constants.KeyPair.KEY_URL, categoryInfo.feedUrl);
                    webIntent.putExtra(Constants.KeyPair.KEY_TITLE, categoryInfo.menuName);
                    webIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                    activity.startActivity(webIntent);
                    return true;

                case Action.CategoryAction.CAT_ACTION_CHROME_TAB:
                    AppUtils.getInstance().openChromeTab(activity, categoryInfo.feedUrl, categoryInfo.color);
                    return true;

                case Action.CategoryAction.CAT_ACTION_FULL_WAP:
                    Intent webTV = new Intent(activity, WebFullScreenActivity.class);
                    webTV.putExtra(Constants.KeyPair.KEY_URL, categoryInfo.feedUrl);
                    webTV.putExtra(Constants.KeyPair.KEY_TITLE, categoryInfo.menuName);
                    webTV.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                    webTV.putExtra(Constants.KeyPair.KEY_EXTRA_VALUES, categoryInfo.extraVlues);
                    activity.startActivity(webTV);
                    return true;
                case Action.CategoryAction.CAT_ACTION_DB_ORG_VIDEO:
                    AppUtils.getInstance().openDBOriginalVideo(activity, categoryInfo.feedUrl, categoryInfo.gaScreen, categoryInfo.gaArticle);
                    return true;
                /*Offer Actions*/
                case Action.CategoryAction.CAT_ACTION_OFFER:
                    activity.startActivity(SWOfferingTransculantActivity.getIntent(activity, categoryInfo.color, categoryInfo.menuName, categoryInfo.gaScreen, true));
                    return true;

                case Action.CategoryAction.CAT_ACTION_SHARE_APP:
                    AppUtils.shareApp(activity);
                    return true;

                case Action.CategoryAction.CAT_ACTION_DB_ORG:
                    activity.startActivity(new Intent(DBOriginalActivity.getIntent(activity, categoryInfo.color, categoryInfo.menuName, categoryInfo.gaScreen, categoryInfo.id)));
                    return true;

                case Action.CategoryAction.CAT_ACTION_NEWS_BRIEF:
                    Intent intent1 = new Intent(activity, NewsBriefActivity.class);
                    intent1.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
                    activity.startActivity(intent1);
                    return true;
                case Action.CategoryAction.CAT_ACTION_BULLETIN:
                    Intent intentBulletin = VideoBulletinActivity.getIntent(activity, new ArrayList<>(), 0, AppFlyerConst.DBVideosSource.VIDEO_LIST, categoryInfo.displayName, categoryInfo.gaEventLabel, categoryInfo.gaArticle, categoryInfo.color, categoryInfo);
                    activity.startActivity(intentBulletin);
                    return true;
                case Action.CategoryAction.CAT_ACTION_AUDIO_BULLETIN_PLAYER:
                    NewsBulletinPlayerKtActivity.Companion.startNewsBulletinPlayerActivity(activity, categoryInfo.id);
                    return true;
            }
        }
        return false;
    }


    /**
     * This context must be an Activity Context
     *
     * @param activityContext
     * @param categoryInfo
     * @param prefixSectionName
     */
    public static void openCategoryAccordingToAction(@NonNull Context activityContext, CategoryInfo categoryInfo,
                                                     String prefixSectionName) {

        if (categoryInfo != null) {
            // open rest of categories
            boolean isOpened = openCategoryActionOutside(activityContext, categoryInfo);

            // if not exist in above list then check below action
            if (!isOpened) {
                switch (categoryInfo.action) {
                    case Action.CategoryAction.CAT_ACTION_CITY_SELECTION_V2:
                    case Action.CategoryAction.CAT_ACTION_CITY_SELECTION:
                        Intent intent = new Intent(activityContext, PreferredCityActivity.class);
                        intent.putExtra(PreferredCityActivity.CALL_PREFERRED_CITY_HOME, false);
                        intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
                        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                        intent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, categoryInfo.displayName);
                        intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, categoryInfo.gaEventLabel);
                        intent.putExtra(Constants.KeyPair.KEY_COLOR, categoryInfo.color);
                        intent.putExtra(Constants.KeyPair.KEY_TITLE, categoryInfo.menuName);
                        intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
                        activityContext.startActivity(intent);
                        break;

                    case Action.CategoryAction.CAT_ACTION_RAJYA:
                    default:
                        Intent newsTopIntent = new Intent(activityContext, MainActivity.class);
                        CategoryInfo categoryInfo1 = new CategoryInfo(categoryInfo);
                        newsTopIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo1);
                        newsTopIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, categoryInfo1.id);
                        newsTopIntent.putExtra(Constants.KeyPair.KEY_PREFIX_SECTION_NAME, prefixSectionName);
                        activityContext.startActivity(newsTopIntent);
                        break;
                }
            }
        }
    }

    /**
     * This context must be an activityContext
     *
     * @param activityContext
     * @param dynamicURL
     */
    public void handleDynamicLink(Context activityContext, String dynamicURL) {
        try {
            Task<PendingDynamicLinkData> dynamicLink = FirebaseDynamicLinks.getInstance().getDynamicLink(Uri.parse(dynamicURL));
            ProgressDialog progressDialog = AppUtils.generateProgressDialog(activityContext, true);
            if (progressDialog != null)
                progressDialog.show();

            dynamicLink.addOnSuccessListener(command -> {
                try {
                    String extendedURL = command.getLink().toString();
                    int minVersion = command.getMinimumAppVersion();
                    if (progressDialog != null)
                        progressDialog.cancel();
                    if (minVersion > BuildConfig.VERSION_CODE) {
                        //open playstore
                        activityContext.startActivity(command.getUpdateAppIntent(activityContext));
                        return;
                    }
                    if (!TextUtils.isEmpty(extendedURL)) {
                        OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(activityContext, extendedURL, "", 0, "", null);
                        openNewsDetailInApp.openLink();
                    } else {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(dynamicURL));
                        activityContext.startActivity(intent);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void handleDynamicLink(Context mContext, String dynamicURL, boolean showProgress, Callback callback) {
        try {
            Task<PendingDynamicLinkData> dynamicLink = FirebaseDynamicLinks.getInstance().getDynamicLink(Uri.parse(dynamicURL));
            ProgressDialog progressDialog = AppUtils.generateProgressDialog(mContext, true);
            if (showProgress) {
                if (progressDialog != null)
                    progressDialog.show();
            }

            dynamicLink.addOnSuccessListener(command -> {
                if (command != null) {
                    try {
                        String extendedURL = command.getLink().toString();
                        //check min version
                        int minVersion = command.getMinimumAppVersion();
                        if (showProgress) {
                            if (progressDialog != null)
                                progressDialog.cancel();
                        }
                        if (minVersion > BuildConfig.VERSION_CODE) {
                            showUpdateDialog(mContext);
                            return;
                        }

                        if (!TextUtils.isEmpty(extendedURL)) {
                            HashMap<String, String> parsedMap = AppUtils.parseExtendedDynamicURL(extendedURL);
                            if (callback != null) {
                                callback.onCallback(parsedMap);
                            }

                        } else {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(dynamicURL));
                            mContext.startActivity(intent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showUpdateDialog(Context context) {
        DialogUtils.showInfoDialog(context, "New Update is available!", "Please update app to use this feature!", () -> {
            try {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + context.getPackageName())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    public interface Callback {
        void onCallback(Object object);
    }
}
