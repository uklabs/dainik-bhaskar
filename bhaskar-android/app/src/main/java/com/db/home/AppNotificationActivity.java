package com.db.home;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.widget.Toolbar;

import android.widget.TextView;

import com.bhaskar.view.ui.MainActivity;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.home.notification.NotificationFragment;
import com.db.main.BaseAppCompatActivity;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ThemeUtil;

public class AppNotificationActivity extends BaseAppCompatActivity {
    boolean launchFromStack, launchFromNotification;
    private TextView tvTitle;
    private CategoryInfo mCategoryInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleExtras(getIntent());
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_app_notification);
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(AppUtils.fetchAttributeColor(this, R.attr.toolbarIconColor));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back_black);
        toolbar.setNavigationOnClickListener(v -> handleBackPress());
        getSupportActionBar().setTitle(null);
        tvTitle = findViewById(R.id.toolbar_title);
        tvTitle.setTextColor(Color.BLACK);
        String title = getString(R.string.notification_center_txt);
        if(mCategoryInfo!=null){
            title = mCategoryInfo.menuName;
        }
        setHeaderTitle(title);
        launchFromStack = getIntent().getExtras().getBoolean(Constants.KeyPair.LAUNCH_FROM_STACK_NOTIFICATION);
        launchFromNotification = getIntent().getExtras().getBoolean(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION);

        /*Set Fragment*/
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, NotificationFragment.getInstance(launchFromStack));
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    private void handleExtras(Intent intent) {
        if(intent!=null){
            mCategoryInfo = (CategoryInfo) intent.getSerializableExtra(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
    }

    public void setHeaderTitle(String headerTitle) {
        tvTitle.setText(headerTitle);
    }

    private void handleBackPress() {
        if (launchFromStack || launchFromNotification) {
            Intent intent = new Intent(AppNotificationActivity.this, MainActivity.class);
            if (getIntent().getExtras() != null)
                intent.putExtras(getIntent().getExtras());
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        handleBackPress();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if( requestCode == Constants.REQ_CODE_BOTTOM_NAV){
            finish();
        }

    }
}
