package com.db.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bhaskar.R;
import com.bhaskar.util.AppUrls;
import com.db.util.ImageUtil;

public class AutoStartDialogActivity extends AppCompatActivity {

    private boolean launchSettings = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_start_dialog);

        boolean scrollImage = true;
        Intent intent = getIntent();
        if (intent != null) {
            scrollImage = intent.getBooleanExtra("img_scroll", true);
            launchSettings = intent.getBooleanExtra("launch_settings", true);
        }

        if (scrollImage) {
            ImageUtil.setImage(this, AppUrls.AUTO_START_URL, findViewById(R.id.imageview), 0);
        } else {
            ImageUtil.setImage(this, AppUrls.AUTO_START_2_URL, findViewById(R.id.imageview), 0);
        }

        findViewById(R.id.lets_go_button).setOnClickListener(view -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        if (launchSettings) {
            launchSettings();
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private void launchSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myAppSettings);
    }
}
