package com.db.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.database.BottomNavInfo;
import com.db.listeners.OnDBVideoClickListener;
import com.db.util.ImageUtil;

import java.util.List;

public class BottomNavGridAdapter extends RecyclerView.Adapter {


    public static final int HORIZONTAL_TYPE = 1;
    public static final int GRID_TYPE = 2;
    private List<BottomNavInfo> bottomNavMoreList;
    private OnDBVideoClickListener onDBVideoClickListener;
    private int viewType;

    public BottomNavGridAdapter(List<BottomNavInfo> bottomNavMoreList, OnDBVideoClickListener onDBVideoClickListener, int viewType) {

        this.bottomNavMoreList = bottomNavMoreList;
        this.onDBVideoClickListener = onDBVideoClickListener;
        this.viewType = viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case GRID_TYPE:
                return new BottomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_bottom_bar, parent, false));
            case HORIZONTAL_TYPE:
                return new BottomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_bottom_horiz_bar, parent, false));
            default:
                return new BottomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_bottom_bar, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (viewType) {
            case HORIZONTAL_TYPE:
            case GRID_TYPE:
                BottomViewHolder bottomViewHolder = (BottomViewHolder) holder;
                ImageUtil.setImage(holder.itemView.getContext(), bottomNavMoreList.get(holder.getAdapterPosition()).icon, bottomViewHolder.icon, R.drawable.ic_category);
                bottomViewHolder.name.setText(bottomNavMoreList.get(holder.getAdapterPosition()).name);
                bottomViewHolder.itemView.setOnClickListener(v -> onDBVideoClickListener.onDbVideoClick(holder.getAdapterPosition()));
                break;
        }


    }

    @Override
    public int getItemCount() {
        return bottomNavMoreList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return viewType;
    }

    private class BottomViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView name;

        public BottomViewHolder(View inflate) {
            super(inflate);
            icon = inflate.findViewById(R.id.tab_img);
            name = inflate.findViewById(R.id.tab);
        }
    }


}
