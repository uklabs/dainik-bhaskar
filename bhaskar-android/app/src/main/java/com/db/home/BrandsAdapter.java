package com.db.home;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bhaskar.R;
import com.db.data.models.BrandInfo;
import com.db.database.DatabaseClient;
import com.db.util.AppPreferences;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;

import java.util.List;

public class BrandsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<BrandInfo> homeInfoArrayList;
    private Context mContext;
    private final int VIEW_TYPE_OTHER_BRANDS = 2;

    public BrandsAdapter(Context context, List<BrandInfo> homeInfoArrayList) {
        mContext = context;
        this.homeInfoArrayList = homeInfoArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_OTHER_BRANDS) {
            return new BrandViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.other_brand_item_view, parent, false));
        } else {
            return new BrandViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.brand_item_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof BrandViewHolder) {

            final BrandViewHolder viewHolder = (BrandViewHolder) holder;
            final BrandInfo info = homeInfoArrayList.get(position);

            ImageUtil.setImage(mContext, info.icon, viewHolder.iconImageView, R.drawable.water_mark_prime, R.drawable.water_mark_prime);

            final String newIconUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.JsonPreference.PRIME_NEW_ICON_URL, "");
            ImageUtil.setImage(mContext, newIconUrl, viewHolder.iconNewTagImageView, R.drawable.ic_category_new_tag);

            // show new tag
            if (!DatabaseClient.getInstance(mContext).isID_Clicked(info.id, DatabaseClient.DbConstant.BRAND_TYPE) && info.isNew.equalsIgnoreCase("1")) {
                viewHolder.iconNewTagImageView.setVisibility(View.VISIBLE);
            } else {
                viewHolder.iconNewTagImageView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return homeInfoArrayList.size();
    }

    class BrandViewHolder extends RecyclerView.ViewHolder {
        private ImageView iconImageView;
        private ImageView iconNewTagImageView;

        private BrandViewHolder(View view) {
            super(view);
            iconImageView = view.findViewById(R.id.icon_image_view);
            iconNewTagImageView = view.findViewById(R.id.icon_new_tag_image_view);
        }
    }
}