package com.db.home;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.VideoBulletinActivity;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.news.WapV2Activity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.Urls;

import java.util.ArrayList;
import java.util.List;

public class BulletinAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int VIDEO_VIEW_TYPE = 0;
    public static final int NEWS_VIEW_TYPE = 1;

    private ArrayList<VideoInfo> videoInfoList;
    private List<NewsListInfo> newsListInfoList;
    private Activity mContext;
    private String themeColor;
    private CategoryInfo categoryInfo;
    private int type;

    public BulletinAdapter(Activity mContext, CategoryInfo categoryInfo) {
        this.mContext = mContext;
        this.videoInfoList = new ArrayList<>();
        this.newsListInfoList = new ArrayList<>();
        this.themeColor = categoryInfo.color;
        this.categoryInfo = categoryInfo;
    }

    public void setNewsData(List<NewsListInfo> list, int type) {
        this.type = type;
        this.newsListInfoList.clear();
        this.newsListInfoList.addAll(list);
        notifyDataSetChanged();
    }

    public void setVideoData(List<VideoInfo> list, int type) {
        this.type = type;
        this.videoInfoList.clear();
        this.videoInfoList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIDEO_VIEW_TYPE:
                return new VideoBulletinViewHolder(inflater.inflate(R.layout.listitem_news_bulletin_item, parent, false));

            case NEWS_VIEW_TYPE:
                return new NewsBulletinViewHolder(inflater.inflate(R.layout.listitem_news_bulletin_item, parent, false));

        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIDEO_VIEW_TYPE:
                final VideoBulletinViewHolder videoBulletinViewHolder = (VideoBulletinViewHolder) holder;
                VideoInfo videoInfo = videoInfoList.get(position);
                ImageUtil.setImage(mContext, videoInfo.image, videoBulletinViewHolder.imageView, R.drawable.water_mark_vertical);
                videoBulletinViewHolder.itemView.setOnClickListener(v -> {
                    Intent intent = VideoBulletinActivity.getIntent(mContext, videoInfoList, position, AppFlyerConst.DBVideosSource.VIDEO_LIST, categoryInfo.displayName, categoryInfo.gaEventLabel, categoryInfo.gaArticle, categoryInfo.color,categoryInfo);
//                    Intent intent = DivyaPartraitVideoActivity.getIntent(mContext, videoInfo, categoryInfo.displayName, AppFlyerConst.DBVideosSource.VIDEO_LIST, -1, categoryInfo.gaEventLabel, categoryInfo.gaArticle);
                    mContext.startActivity(intent);
                });

                break;
            case NEWS_VIEW_TYPE:
                final NewsBulletinViewHolder newsBulletinViewHolder = (NewsBulletinViewHolder) holder;
                ImageUtil.setImage(mContext, newsListInfoList.get(position).image, newsBulletinViewHolder.imageView, R.drawable.water_mark_vertical);
                newsBulletinViewHolder.itemView.setOnClickListener(v -> launchArticlePage(newsListInfoList.get(position), position));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return (type == NEWS_VIEW_TYPE) ? (newsListInfoList == null ? 0 : newsListInfoList.size()) : (videoInfoList == null ? 0 : videoInfoList.size());
    }

    public class VideoBulletinViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        VideoBulletinViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.card_view_image);
        }
    }

    public class NewsBulletinViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        NewsBulletinViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.card_view_image);
        }
    }

    private void launchArticlePage(NewsListInfo newsListInfo, int position) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(categoryInfo.gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : categoryInfo.gaScreen) + newsListInfo.webUrl);
            mContext.startActivity(intent);

        } else if (newsListInfo.isOpenGallery) {
            Intent photoGalleryIntent = new Intent(mContext, PhotoGalleryActivity.class);
            photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, Urls.DEFAULT_DETAIL_URL);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, Urls.DEFAULT_DETAIL_URL);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, newsListInfo.storyId);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
            mContext.startActivity(photoGalleryIntent);

        } else {
            mContext.startActivity(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo, position));
        }
    }
}
