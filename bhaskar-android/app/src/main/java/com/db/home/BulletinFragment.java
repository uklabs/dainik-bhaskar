package com.db.home;


import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.data.models.BulletinRootInfo;
import com.db.data.models.CategoryInfo;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class BulletinFragment extends Fragment {

    private Activity mContext;
    private TextView videoBltnTv, newsBltnTv;
    private ProgressBar progressBar;
    private CategoryInfo categoryInfo;

    private BulletinAdapter videoBulletinAdapter;
    private BulletinAdapter newsBulletinAdapter;

    private NestedScrollView nestedScrollView;


    public BulletinFragment() {
    }

    public static BulletinFragment getInstance(CategoryInfo categoryInfo) {
        BulletinFragment videoBulletinFragment = new BulletinFragment();
        Bundle argument = new Bundle();
        argument.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        videoBulletinFragment.setArguments(argument);
        return videoBulletinFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bulletin, container, false);
        nestedScrollView = rootView.findViewById(R.id.nested_scrollview);
        videoBltnTv = rootView.findViewById(R.id.video_bltn_tv);
        newsBltnTv = rootView.findViewById(R.id.news_bltn_tv);
        RecyclerView videoBltnRv = rootView.findViewById(R.id.video_bltn_rv);
        RecyclerView newsBltnRv = rootView.findViewById(R.id.news_bltn_rv);
        progressBar = rootView.findViewById(R.id.progress_bar);


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        videoBulletinAdapter = new BulletinAdapter(mContext, categoryInfo);
        videoBltnRv.setLayoutManager(layoutManager);
        videoBltnRv.setAdapter(videoBulletinAdapter);

        LinearLayoutManager newLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);

        newsBulletinAdapter = new BulletinAdapter(mContext, categoryInfo);
        newsBltnRv.setLayoutManager(newLayoutManager);
        newsBltnRv.setAdapter(newsBulletinAdapter);

        makeJsonObjectRequest();
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void makeJsonObjectRequest() {
        String finalFeedURL = String.format("%s%s", Urls.APP_FEED_BASE_URL, categoryInfo.feedUrl);
        Systr.println("Feed Url : " + finalFeedURL);

        progressBar.setVisibility(View.VISIBLE);

        if (NetworkStatus.getInstance().isConnected(mContext)) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL),null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                parseFeed(response);
                            } catch (Exception ignored) {
                            }

                            progressBar.setVisibility(View.GONE);
                            nestedScrollView.setVisibility(View.VISIBLE);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    nestedScrollView.setVisibility(View.GONE);
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
        }
    }

    private BulletinRootInfo bulletinRootInfo;

    private void parseFeed(JSONObject response) {
        bulletinRootInfo = new Gson().fromJson(response.toString(), BulletinRootInfo.class);
        if (bulletinRootInfo.video != null) {
            videoBltnTv.setText(bulletinRootInfo.video.title);
            videoBulletinAdapter.setVideoData(bulletinRootInfo.video.feed, BulletinAdapter.VIDEO_VIEW_TYPE);
        }

        if (bulletinRootInfo.news != null) {
            newsBltnTv.setText(bulletinRootInfo.news.title);
            newsBulletinAdapter.setNewsData(bulletinRootInfo.news.feed, BulletinAdapter.NEWS_VIEW_TYPE);
        }

    }


}
