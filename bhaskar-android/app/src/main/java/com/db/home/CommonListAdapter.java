package com.db.home;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bhaskar.R;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.bhaskar.appscommon.ads.NativeAdViewHolder2;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.divya_new.home.FlickerViewHolder;
import com.db.divya_new.home.StockMarketMarqueeViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.homebanner.WebBannerViewHolder;
import com.db.news.WapV2Activity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.views.JustifiedTextView;
import com.db.views.viewholder.BigImageListViewHolder;
import com.db.views.viewholder.GridFourViewHolderNew;
import com.db.views.viewholder.GuardianListViewHolder;
import com.db.views.viewholder.LoadingViewHolder;
import com.db.views.viewholder.RecListViewHolder;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.ArrayList;
import java.util.List;


public class CommonListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {
    /*View Types*/
    private final int VIEW_TYPE_REC_DEFAULT = 0;
    private final int VIEW_TYPE_BIG_IMAGE = 1;
    private final int VIEW_TYPE_REC = 2;
    private final int VIEW_TYPE_REC_RIGHT = 3;
    private final int VIEW_TYPE_BIG_IMAGE_HOVER = 4;
    private final int VIEW_TYPE_GUARDIAN = 5;
    private final int VIEW_TYPE_GRID_4 = 6;
    private final int VIEW_TYPE_LOADING = 7;

    /*Banners*/
    public static final int VIEW_TYPE_BANNER = 8;
    public static final int VIEW_TYPE_WEB_BANNER = 9;

    /*Widget*/
    private static final int VIEW_TYPE_STOCK = 10;
    private static final int VIEW_TYPE_FLICKER = 11;
    private static final int VIEW_TYPE_PHOTO_FLICKER = 12;

    /*Ads*/
    private final int VIEW_TYPE_NATIVE_ADS = 13;
    private final static int VIEW_TYPE_AD = 14;

    private ArrayList<Object> mNewsFeed;
    private Activity mContext;
    private CategoryInfo categoryInfo;
    private String detailFeedUrl, gaScreen;
    private String color;

    public CommonListAdapter(Activity contexts, String gaScreen, String color, CategoryInfo categoryInfo) {
        setHasStableIds(true);

        this.mContext = contexts;
        this.categoryInfo = categoryInfo;
        this.mNewsFeed = new ArrayList<>();
        this.gaScreen = gaScreen;
        this.color = color;
    }

    private void addAds() {
        isBannerLoaded = false;
        if (AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
            mNewsFeed.add(0, AdController.showATFForAllListing(mContext, this));
        }
    }

    public void setData(ArrayList<NewsListInfo> newsFeed) {
        if (newsFeed == null) {
            mNewsFeed.clear();
            notifyDataSetChanged();
        }
    }

    public void setData(List<NewsListInfo> newsFeed, String detailFeedUrl) {
        this.mNewsFeed.clear();
        this.mNewsFeed.addAll(newsFeed);
        this.detailFeedUrl = detailFeedUrl;

        addAds();
    }

    public void setLoadMoreData(List<NewsListInfo> newsFeed) {
        this.mNewsFeed.addAll(newsFeed);
    }

    public void setNotify() {
        notifyDataSetChanged();
    }

    public void updateGA(String gaScreen) {
        this.gaScreen = gaScreen;
    }

    public void addItem() {
        mNewsFeed.add(null);
        notifyDataSetChanged();
    }

    public void removeItem() {
        if (mNewsFeed.size() > 0 && mNewsFeed.get(mNewsFeed.size() - 1) == null) {
            mNewsFeed.remove(mNewsFeed.size() - 1);
        }
        notifyDataSetChanged();
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;
        notifyItemChanged(0);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_BIG_IMAGE:
                return new BigImageListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image, parent, false));
            case VIEW_TYPE_REC:
            case VIEW_TYPE_REC_DEFAULT:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_rec_item, parent, false));
            case VIEW_TYPE_REC_RIGHT:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image_three, parent, false));
            case VIEW_TYPE_BIG_IMAGE_HOVER:
                return new BigImageListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image_four, parent, false));

            case VIEW_TYPE_GUARDIAN:
                return new GuardianListViewHolder(inflater.inflate(R.layout.recyclerlist_item_guardian_view, parent, false));
            case VIEW_TYPE_GRID_4:
                return new GridFourViewHolderNew(inflater.inflate(R.layout.recyclerlist_item_grid_four_view_new, parent, false));
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item, parent, false));
            case VIEW_TYPE_STOCK:
                return new StockMarketMarqueeViewHolder(inflater.inflate(R.layout.layout_stock_market_marquee, parent, false), mContext, categoryInfo);
            case VIEW_TYPE_FLICKER:
                return new FlickerViewHolder(inflater.inflate(R.layout.listitem_flicker, parent, false), mContext, categoryInfo.id, false);
            case VIEW_TYPE_PHOTO_FLICKER:
                return new FlickerViewHolder(inflater.inflate(R.layout.listitem_flicker, parent, false), mContext, categoryInfo.id, true);
            case VIEW_TYPE_NATIVE_ADS:
                return new NativeAdViewHolder2(inflater.inflate(R.layout.ad_layout_native_listing_view, parent, false));
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            case VIEW_TYPE_WEB_BANNER:
                return new WebBannerViewHolder(inflater.inflate(R.layout.layout_web_banner, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_BIG_IMAGE:
            case VIEW_TYPE_BIG_IMAGE_HOVER:
                bindBigImage(holder);
                return;

            case VIEW_TYPE_REC:
            case VIEW_TYPE_REC_RIGHT:
            case VIEW_TYPE_REC_DEFAULT:
                bindRecViewHolder(holder);
                break;

            case VIEW_TYPE_LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
                loadingViewHolder.progressBar.setIndeterminate(true);
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
                break;

            case VIEW_TYPE_GUARDIAN:
                guardianViewBindHolder(holder);
                break;
            case VIEW_TYPE_GRID_4:
                gridFourViewBindHolderNew((GridFourViewHolderNew) holder);
                break;

            case VIEW_TYPE_NATIVE_ADS:
                showNativeAds((NativeAdViewHolder2) holder, position);
                break;

            case VIEW_TYPE_AD:
                BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
                if (((AdInfo) mNewsFeed.get(position)).catType == 1) {
                    if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                        adViewHolder.addAds(((AdInfo) mNewsFeed.get(position)).bannerAdView);

                    } else {
                        ImageView imageView = new ImageView(mContext);
                        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        imageView.setAdjustViewBounds(true);
                        imageView.setClickable(true);
                        String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                        ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                        adViewHolder.addAdsUrl(imageView);

                        imageView.setOnClickListener(view -> AppUtils.getInstance().clickOnAdBanner(mContext));
                    }
                } else {
                    adViewHolder.addAds(((AdInfo) mNewsFeed.get(position)).bannerAdView);
                }
                break;

            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData(((NewsListInfo) mNewsFeed.get(bannerViewHolder.getAdapterPosition())).mBannerInfo);
                break;
            case VIEW_TYPE_WEB_BANNER:
                WebBannerViewHolder webBannerViewHolder = (WebBannerViewHolder) holder;
                webBannerViewHolder.callwebViewOrSetWidgetData(((NewsListInfo) mNewsFeed.get(webBannerViewHolder.getAdapterPosition())).webBannerInfo);
                break;
            default:
                break;
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if(holder instanceof FlickerViewHolder){
            ((FlickerViewHolder) holder).resumeTimer();
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if(holder instanceof FlickerViewHolder){
            ((FlickerViewHolder) holder).pauseTimer();
        }
    }

    private void showNativeAds(NativeAdViewHolder2 holder, int pos) {
        NewsListInfo adInfo = (NewsListInfo) mNewsFeed.get(pos);
        UnifiedNativeAd ad = NativeListAdController2.getInstance(mContext).getAd(adInfo.adUnit, pos);
        holder.populateNativeAdView(ad);
    }

    private void launchArticlePage(NewsListInfo newsListInfo, int position) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : gaScreen) + newsListInfo.webUrl);
            mContext.startActivity(intent);

        } else if (newsListInfo.isOpenGallery) {
            Intent photoGalleryIntent = new Intent(mContext, PhotoGalleryActivity.class);
            photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, newsListInfo.storyId);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
            mContext.startActivity(photoGalleryIntent);

        } else {
            mContext.startActivityForResult(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo, position), Constants.REQ_CODE_BOTTOM_NAV);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (mNewsFeed.get(position) == null) {
            return VIEW_TYPE_LOADING;
        } else {
            Object object = mNewsFeed.get(position);
            if (object instanceof AdInfo) {
                return VIEW_TYPE_AD;
            } else {
                NewsListInfo info = (NewsListInfo) object;
                if (info.isAd) {
                    return VIEW_TYPE_NATIVE_ADS;
                } else if (info.isFlicker) {
                    return VIEW_TYPE_FLICKER;
                } else if (info.isPhotoFlicker) {
                    return VIEW_TYPE_PHOTO_FLICKER;
                } else if (info.isStockWidget) {
                    return VIEW_TYPE_STOCK;
                } else if (info.type > 0) {
                    return info.type;
                } else if (info.subStoryList.size() == 2) {
                    return VIEW_TYPE_GUARDIAN;
                } else if (info.subStoryList.size() == 4) {
                    return VIEW_TYPE_GRID_4;
                } else {
                    switch (info.bigImage) {
                        case 1:
                            return VIEW_TYPE_BIG_IMAGE;
                        case 2:
                            return VIEW_TYPE_REC;
                        case 3:
                            return VIEW_TYPE_REC_RIGHT;
                        case 4:
                            return VIEW_TYPE_BIG_IMAGE_HOVER;
                        default:
                            return VIEW_TYPE_REC_DEFAULT;
                    }
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mNewsFeed == null ? 0 : mNewsFeed.size();
    }

    public NewsListInfo getItemAtPosition(int position) {
        if (position >= 0 && position < mNewsFeed.size() && mNewsFeed.get(position) instanceof NewsListInfo)
            return (NewsListInfo) mNewsFeed.get(position);
        return null;
    }


    private void bindRecViewHolder(RecyclerView.ViewHolder holder) {
        final RecListViewHolder recListViewHolder = (RecListViewHolder) holder;
        final NewsListInfo newsListInfoRecView = (NewsListInfo) mNewsFeed.get(recListViewHolder.getAdapterPosition());
        if (TextUtils.isEmpty(newsListInfoRecView.colorCode))
            newsListInfoRecView.colorCode = color;
        recListViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(newsListInfoRecView.iitl_title, AppUtils.getInstance().fromHtml(newsListInfoRecView.title), newsListInfoRecView.colorCode));

        /*Exclusive*/
        if (!TextUtils.isEmpty(newsListInfoRecView.exclusive)) {
            recListViewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            recListViewHolder.exclusiveTv.setText(newsListInfoRecView.exclusive);
        } else {
            recListViewHolder.exclusiveLayout.setVisibility(View.GONE);
        }

        /*Image*/
        ImageUtil.setImage(mContext, newsListInfoRecView.image, recListViewHolder.ivThumb, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);

        /*Video Flag*/
        if (newsListInfoRecView.videoFlag == 1) {
            recListViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
            recListViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, newsListInfoRecView.colorCode));
        } else {
            recListViewHolder.imgVideoPlay.setVisibility(View.GONE);
        }

        recListViewHolder.itemView.setOnClickListener(view -> launchArticlePage(newsListInfoRecView, holder.getAdapterPosition()));
        recListViewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
            @Override
            public void onBookMark() {
                AppUtils.getInstance().bookmarkClick(mContext, newsListInfoRecView, detailFeedUrl, color, false);
            }

            @Override
            public void onShare() {
                AppUtils.getInstance().shareClick(mContext, newsListInfoRecView.title, newsListInfoRecView.webUrl, newsListInfoRecView.gTrackUrl, false);
            }
        }));
    }

    private void bindBigImage(RecyclerView.ViewHolder holder) {
        final BigImageListViewHolder bigImageListViewHolder = (BigImageListViewHolder) holder;
        final NewsListInfo newsListInfoBigView = (NewsListInfo) mNewsFeed.get(bigImageListViewHolder.getAdapterPosition());
        if (TextUtils.isEmpty(newsListInfoBigView.colorCode))
            newsListInfoBigView.colorCode = color;
        /*Exclusive*/
        if (!TextUtils.isEmpty(newsListInfoBigView.exclusive)) {
            bigImageListViewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            bigImageListViewHolder.exclusiveTv.setText(newsListInfoBigView.exclusive);
        } else {
            bigImageListViewHolder.exclusiveLayout.setVisibility(View.GONE);
        }

        /*bullet point*/
        if (newsListInfoBigView.bulletsPoint != null && newsListInfoBigView.bulletsPoint.length > 0) {
            bigImageListViewHolder.bulletLayout.removeAllViews();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            for (int i = 0; i < newsListInfoBigView.bulletsPoint.length; i++) {
                View item = inflater.inflate(R.layout.layout_bullet_type, null);
                String desc = newsListInfoBigView.bulletsPoint[i];
                ((JustifiedTextView) item.findViewById(R.id.bullet_tv)).setText(AppUtils.getInstance().fromHtml(desc));
                ((ImageView) item.findViewById(R.id.iv_bullet_circle)).setColorFilter(AppUtils.getThemeColor(mContext, newsListInfoBigView.colorCode));
                bigImageListViewHolder.bulletLayout.addView(item);
            }
            bigImageListViewHolder.bulletLayout.setVisibility(View.VISIBLE);
        } else {
            bigImageListViewHolder.bulletLayout.setVisibility(View.GONE);
        }


        float ratio = AppUtils.getInstance().parseImageRatio(newsListInfoBigView.imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
        AppUtils.getInstance().setImageViewSizeWithAspectRatio(bigImageListViewHolder.ivThumb, ratio, 0, mContext);
        ImageUtil.setImage(mContext, newsListInfoBigView.image, bigImageListViewHolder.ivThumb, R.drawable.water_mark_news_detail, R.drawable.water_mark_big_news_detail_error);
        if (newsListInfoBigView.videoFlag == 1) {
            bigImageListViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
            bigImageListViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, newsListInfoBigView.colorCode));
        } else {
            bigImageListViewHolder.imgVideoPlay.setVisibility(View.GONE);
        }

        bigImageListViewHolder.itemView.setOnClickListener(v -> launchArticlePage(newsListInfoBigView, holder.getAdapterPosition()));

        bigImageListViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(newsListInfoBigView.iitl_title, AppUtils.getInstance().fromHtml(newsListInfoBigView.title), newsListInfoBigView.colorCode));


        bigImageListViewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
            @Override
            public void onBookMark() {
                AppUtils.getInstance().bookmarkClick(mContext, newsListInfoBigView, detailFeedUrl, color, false);
            }

            @Override
            public void onShare() {
                AppUtils.getInstance().shareClick(mContext, newsListInfoBigView.title, newsListInfoBigView.webUrl, newsListInfoBigView.gTrackUrl, false);
            }
        }));
    }

    private void guardianViewBindHolder(RecyclerView.ViewHolder holder) {
        final GuardianListViewHolder guardianListViewHolder = (GuardianListViewHolder) holder;
        NewsListInfo newsListInfo = (NewsListInfo) mNewsFeed.get(guardianListViewHolder.getAdapterPosition());
        if (TextUtils.isEmpty(newsListInfo.colorCode))
            newsListInfo.colorCode = color;
        if (newsListInfo.subStoryList.size() < 2) {
            guardianListViewHolder.itemView.setVisibility(View.GONE);
            return;
        } else {
            guardianListViewHolder.itemView.setVisibility(View.VISIBLE);
        }

        final NewsListInfo info1 = newsListInfo.subStoryList.get(0);
        final NewsListInfo info2 = newsListInfo.subStoryList.get(1);

        //ask @akamahesh or lalit for more
        float ratio = AppUtils.getInstance().parseImageRatio("16x13", Constants.ImageRatios.ARTICLE_DETAIL_RATIO);


        AppUtils.getInstance().setImageViewHalfSizeWithAspectRatio(guardianListViewHolder.ivThumb_1, ratio, 20, mContext);
        ImageUtil.setImage(mContext, info1.image, guardianListViewHolder.ivThumb_1, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);


        if (info1.videoFlag == 1) {
            guardianListViewHolder.ivVideoPlay_1.setVisibility(View.VISIBLE);
            guardianListViewHolder.ivVideoPlay_1.setColorFilter(AppUtils.getThemeColor(mContext, info1.colorCode));
        } else {
            guardianListViewHolder.ivVideoPlay_1.setVisibility(View.GONE);
        }
        /*Exclusive*/
        if (!TextUtils.isEmpty(info1.exclusive)) {
            guardianListViewHolder.exclusiveLayout1.setVisibility(View.VISIBLE);
            guardianListViewHolder.exclusiveTv1.setText(info1.exclusive);
        } else {
            guardianListViewHolder.exclusiveLayout1.setVisibility(View.GONE);
        }
        guardianListViewHolder.carLayout_1.setOnClickListener(v -> {
            launchArticlePage(info1, holder.getAdapterPosition());
            guardianListViewHolder.titleTextView_1.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));
        });

        // second card
        try {
            guardianListViewHolder.ivMore_2.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    AppUtils.getInstance().bookmarkClick(mContext, info2, categoryInfo.detailUrl, categoryInfo.color, false);
                }

                @Override
                public void onShare() {
                    AppUtils.getInstance().shareClick(mContext, info2.title, info2.webUrl, info2.gTrackUrl, false);
                }
            }));

            guardianListViewHolder.titleTextView_2.setText(AppUtils.getInstance().getHomeTitle(info2.iitl_title, AppUtils.getInstance().fromHtml(info2.title), info2.colorCode));

        } catch (Exception ex) {
        }

        AppUtils.getInstance().setImageViewHalfSizeWithAspectRatio(guardianListViewHolder.ivThumb_2, ratio, 20, mContext);
        ImageUtil.setImage(mContext, info2.image, guardianListViewHolder.ivThumb_2, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);


        if (info2.videoFlag == 1) {
            guardianListViewHolder.ivVideoPlay_2.setVisibility(View.VISIBLE);
            guardianListViewHolder.ivVideoPlay_2.setColorFilter(AppUtils.getThemeColor(mContext, info2.colorCode));
        } else {
            guardianListViewHolder.ivVideoPlay_2.setVisibility(View.GONE);
        }
        /*Exclusive*/
        if (!TextUtils.isEmpty(info2.exclusive)) {
            guardianListViewHolder.exclusiveLayout2.setVisibility(View.VISIBLE);
            guardianListViewHolder.exclusiveTv2.setText(info2.exclusive);
        } else {
            guardianListViewHolder.exclusiveLayout2.setVisibility(View.GONE);
        }

        guardianListViewHolder.carLayout_2.setOnClickListener(v -> {
            launchArticlePage(info2, holder.getAdapterPosition());
            guardianListViewHolder.titleTextView_2.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));
        });

        // first card
        try {
            guardianListViewHolder.ivMore_1.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    AppUtils.getInstance().bookmarkClick(mContext, info1, categoryInfo.detailUrl, categoryInfo.color, false);
                }

                @Override
                public void onShare() {
                    AppUtils.getInstance().shareClick(mContext, info1.title, info1.webUrl, info1.gTrackUrl, false);
                }
            }));

            guardianListViewHolder.titleTextView_1.setText(AppUtils.getInstance().getHomeTitle(info1.iitl_title, AppUtils.getInstance().fromHtml(info1.title), info1.colorCode));

        } catch (Exception ex) {
        }
    }

    private void gridFourViewBindHolderNew(GridFourViewHolderNew viewHolder) {
        NewsListInfo newsListInfo = (NewsListInfo) mNewsFeed.get(viewHolder.getAdapterPosition());
        List<NewsListInfo> subStoryList = newsListInfo.subStoryList;
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, RecyclerView.VERTICAL);
        viewHolder.recyclerView.setLayoutManager(layoutManager);
        GridFourViewHolderNew.Adapter adapter = new GridFourViewHolderNew.Adapter(mContext, subStoryList, categoryInfo);
        viewHolder.recyclerView.setAdapter(adapter);
    }
}