package com.db.home;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.DBColumnInfo;
import com.db.data.models.StoryListInfo;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.util.Constants;
import com.db.util.ImageUtil;

import java.util.ArrayList;
import java.util.List;

public class DBColumnAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    public static final int HEADER_VIEW_TYPE = 0;
    private static final int ITEM_VIEW_TYPE = 1;

    private List<DBColumnInfo> mNewsFeed;
    private Activity mContext;
    private CategoryInfo categoryInfo;

    public DBColumnAdapter(Activity mContext, CategoryInfo categoryInfo) {
        this.mContext = mContext;
        this.mNewsFeed = new ArrayList<>();
        this.categoryInfo = categoryInfo;
    }

    public void setData(ArrayList<DBColumnInfo> list) {
        this.mNewsFeed.clear();
        this.mNewsFeed.addAll(list);

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == HEADER_VIEW_TYPE) {
            return new VideoBulletinSectionViewHolder(inflater.inflate(R.layout.listitem_video_bulletin_header_item, parent, false));
        } else if (viewType == ITEM_VIEW_TYPE) {
            return new DBColumnViewHolder(inflater.inflate(R.layout.listitem_circular_image_title_subtitle, parent, false));
        }
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {
        VideoBulletinSectionViewHolder viewHolder = (VideoBulletinSectionViewHolder) holder;

        viewHolder.headerTextView.setText("" + mNewsFeed.get(section).displayDate);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int relativePosition, int absolutePosition) {
        final DBColumnViewHolder viewHolder = (DBColumnViewHolder) holder;
        final StoryListInfo storyListInfo = mNewsFeed.get(section).feed.get(relativePosition);

        viewHolder.tvTitle.setText(storyListInfo.title);
        if (!TextUtils.isEmpty(storyListInfo.subTitle)) {
            viewHolder.tvSubTitle.setVisibility(View.VISIBLE);
            viewHolder.tvSubTitle.setText(storyListInfo.subTitle);

        } else {
            viewHolder.tvSubTitle.setVisibility(View.GONE);
        }

        ImageUtil.setImage(viewHolder.itemView.getContext(), storyListInfo.image, viewHolder.ivPic, R.drawable.water_mark_brand_category);

        viewHolder.itemView.setOnClickListener(view -> mContext.startActivityForResult(DivyaCommonArticleActivity.getIntent(mContext, storyListInfo, categoryInfo), Constants.REQ_CODE_BOTTOM_NAV));

    }

    @Override
    public int getSectionCount() {
        return mNewsFeed.size();
    }

    @Override
    public int getItemCount(int section) {
        if (mNewsFeed.get(section) != null)
            return mNewsFeed.get(section).feed.size();

        return 0;
    }

    @Override
    public int getHeaderViewType(int section) {
        return HEADER_VIEW_TYPE;
    }

    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
        return ITEM_VIEW_TYPE;
    }

    public void clear() {
        mNewsFeed.clear();
        notifyDataSetChanged();
    }

    public class VideoBulletinSectionViewHolder extends RecyclerView.ViewHolder {
        TextView headerTextView;

        VideoBulletinSectionViewHolder(View itemView) {
            super(itemView);
            headerTextView = itemView.findViewById(R.id.header_tv);
        }
    }

    public class DBColumnViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivPic;
        private TextView tvTitle, tvSubTitle;

        public DBColumnViewHolder(View itemView) {
            super(itemView);
            ivPic = itemView.findViewById(R.id.iv_pic);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvSubTitle = itemView.findViewById(R.id.tv_sub_title);
        }

    }
}
