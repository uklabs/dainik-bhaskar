package com.db.home;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.DBColumnInfo;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class DBColumnFragment extends Fragment {

    private Activity mContext;
    private DBColumnAdapter bulletinAdapter;

    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    private TextView clearFilterTv;
    private View dateDivider;
    private LinearLayout dateLayout;
    private TextView selectedMonthTv;
    private TextView tvErrorMsg;
    private TextView selectedDateTv;
    private SwipeRefreshLayout swipeRefreshLayout;

    private int selectedDate;
    private String selectedMonth;
    private long currentDateMillis;
    private String currentSelectedDate;
    private String selectedDateFormat;
    private boolean isMarginAllowed;

    private CategoryInfo categoryInfo;

    public DBColumnFragment() {
    }

    public static DBColumnFragment getInstance(boolean isMarginAllowed, CategoryInfo categoryInfo) {
        DBColumnFragment videoBulletinFragment = new DBColumnFragment();
        Bundle argument = new Bundle();
        argument.putBoolean("isMarginAllowed", isMarginAllowed);
        argument.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        videoBulletinFragment.setArguments(argument);
        return videoBulletinFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            isMarginAllowed = bundle.getBoolean("isMarginAllowed");
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video_bulletin, container, false);

        recyclerView = rootView.findViewById(R.id.recycler_view);
        progressBar = rootView.findViewById(R.id.progress_bar);
        clearFilterTv = rootView.findViewById(R.id.clear_filter_tv);
        dateDivider = rootView.findViewById(R.id.date_divider);
        dateLayout = rootView.findViewById(R.id.date_layout);
        tvErrorMsg = rootView.findViewById(R.id.tvErrorMsg);

        selectedMonthTv = rootView.findViewById(R.id.selected_month_tv);
        selectedDateTv = rootView.findViewById(R.id.selected_date_tv);

        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        if (isMarginAllowed) {
            TypedValue tv = new TypedValue();
            int actionBarHeight = 0;
            if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) swipeRefreshLayout.getLayoutParams();
            params.setMargins(0, 0, 0, actionBarHeight);
            swipeRefreshLayout.setLayoutParams(params);
            swipeRefreshLayout.requestLayout();
        }
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));

        bulletinAdapter = new DBColumnAdapter(mContext, categoryInfo);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (bulletinAdapter.isHeader(position))
                    return 3;
                return 1;
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(bulletinAdapter);

        rootView.findViewById(R.id.calendar_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(currentSelectedDate)) {
                    AppUtils.getInstance().showCustomToast(getContext(), getResources().getString(R.string.sorry_error_found_please_try_again));
                } else {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    try {
                        Date parse = format.parse(currentSelectedDate);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(parse);
                        int year = calendar.get(Calendar.YEAR);
                        int month = calendar.get(Calendar.MONTH);
                        int date = calendar.get(Calendar.DAY_OF_MONTH);

                        dateDialog(year, month, date);
                    } catch (ParseException e) {
                    }
                }
            }
        });

        clearFilterTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeJsonObjectRequest(true, "");
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                makeJsonObjectRequest(false, selectedDateFormat);
            }
        });

        makeJsonObjectRequest(true, "");

        return rootView;
    }

    private void dateDialog(int mYear, int month, int dayOfMonth) {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                bulletinAdapter.clear();
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);

                selectedDate = dayOfMonth;
                selectedMonth = new SimpleDateFormat("MMM", Locale.getDefault()).format(calendar.getTime());

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                selectedDateFormat = format.format(calendar.getTime());
                makeJsonObjectRequest(true, selectedDateFormat);
            }
        };
        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), listener, mYear, month, dayOfMonth);
        dpDialog.getDatePicker().setMaxDate(currentDateMillis);
        dpDialog.show();
    }

    @Override
    public void onDestroyView() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        super.onDestroyView();
    }

    private void makeJsonObjectRequest(boolean showProgress, final String selectedDate) {
        String finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + (TextUtils.isEmpty(selectedDate) ? "" : selectedDate + "/");
        Systr.println("Feed Url : " + finalFeedURL);

        if (showProgress) {
            progressBar.setVisibility(View.VISIBLE);
        }

        if (NetworkStatus.getInstance().isConnected(mContext)) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL),null,
                    response -> {
                        try {
                            parseFeed(response, selectedDate);
                        } catch (Exception ignored) {
                        }

                        progressBar.setVisibility(View.GONE);
                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }, error -> {
                progressBar.setVisibility(View.GONE);
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                if (error instanceof NoConnectionError || error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
        }
    }

    private void parseFeed(JSONObject response, String selDate) throws Exception {
        JSONArray data = response.getJSONArray("feed");
        ArrayList<DBColumnInfo> mList = new ArrayList<>(Arrays.asList(new Gson().fromJson(data.toString(), DBColumnInfo[].class)));

        bulletinAdapter.setData(mList);

        if (TextUtils.isEmpty(selDate)) {
            String currentDate = mList.get(0).date;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date parse = format.parse(currentDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(parse);
            currentDateMillis = calendar.getTimeInMillis();

            clearFilterTv.setVisibility(View.GONE);
            dateLayout.setVisibility(View.GONE);
            dateDivider.setVisibility(View.GONE);

            selectedDate = 0;
            selectedMonth = "";
            selectedDateFormat = "";

        } else {
            clearFilterTv.setVisibility(View.VISIBLE);
            dateLayout.setVisibility(View.VISIBLE);
            dateDivider.setVisibility(View.VISIBLE);
            selectedDateTv.setText("" + selectedDate);
            selectedMonthTv.setText(selectedMonth);
        }
        if(!mList.isEmpty())
            currentSelectedDate = mList.get(0).date;

        if(bulletinAdapter.getItemCount() == 0){
            tvErrorMsg.setVisibility(View.VISIBLE);
        }else{
            tvErrorMsg.setVisibility(View.GONE);
        }
    }
}
