package com.db.home;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.database.CommonListTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.dbvideo.player.BusProvider;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.listeners.FragmentLifecycle;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.util.Action;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExclusiveFragment extends Fragment implements OnFeedFetchFromServerListener, FragmentLifecycle, OnMoveTopListener {

    private final String TAG = AppConfig.BaseTag + "." + ExclusiveFragment.class.getSimpleName();

    //lazy loading
    public boolean isLoading;
    private int LOAD_MORE_COUNT = 0;
    private int visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
    private int lastVisibleItem, totalItemCount;

    //HpBig
    private InitApplication application;
    Handler handler;
    private ProgressBar progressBar;
    private ArrayList<NewsListInfo> newsInfoList;
    private ExclusiveListAdapter exclusiveListAdapter;
    private String finalFeedURL;
    private CategoryInfo categoryInfo;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Integer> trackingList = new ArrayList<>();
    private boolean isShowing = false;
    //widgets
    private boolean isStockActive;
    private boolean isStockAdded;
    private boolean isFlickerActive;
    private boolean isFlickerAdded;
    //    private String parentDisplayName;
    private int listIndex = 0;
    private Rect scrollBounds;
    private List<BannerInfo> bannerInfoList;

    public ExclusiveFragment() {
    }

    public static ExclusiveFragment newInstance(CategoryInfo categoryInfo, boolean flag, String parentDisplayName) {
        ExclusiveFragment fragmentFirst = new ExclusiveFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putBoolean("flag", flag);
        bundle.putString("displayName", parentDisplayName);
        fragmentFirst.setArguments(bundle);
        return fragmentFirst;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.application = (InitApplication) getActivity().getApplication();
        isStockActive = AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.StockPrefs.STOCK_IS_ACTIVE, false);
//        isFlickerActive = AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.FlickerPrefs.FLICKER_IS_ACTIVE, false);
        BusProvider.getInstance().register(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
//            parentDisplayName = bundle.getString("displayName");
        }
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        AppLogs.printErrorLogs(TAG, "" + isVisibleToUser);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_list_fragment_main, container, false);

        progressBar = rootView.findViewById(R.id.progress_bar);
        if (getActivity() != null) {
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
        recyclerView = rootView.findViewById(R.id.recycler_view);
        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        handler = new Handler();
        exclusiveListAdapter = new ExclusiveListAdapter(getActivity(), categoryInfo.gaScreen, categoryInfo);
        recyclerView.setAdapter(exclusiveListAdapter);
        handler.postDelayed(() -> {
            if (!NetworkStatus.getInstance().isConnected(getActivity()) || Constants.singleSessionCategoryIdList.contains(categoryInfo.id)) {
                isTokenExpire();
            } else {
                showProgressBar();
                swipeRefreshLayout();
            }
        }, Constants.DataBaseHit.DEFAULT_TIME_IS_TOKEN);


        swipeRefreshLayout.setOnRefreshListener(() ->
        {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                swipeRefreshLayout();
            } else {
                hideProgress();
                if (isAdded() && getActivity() != null)
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));

            }
        });
        if (MainActivity.tabReClicked)
            recyclerView.scrollToPosition(0);
        return rootView;
    }


    /**
     * Call for refreshing the content by pull to refresh and event when internet has come
     */
    public void swipeRefreshLayout() {
        AppLogs.printDebugLogs("call swipeRefreshLayout", "swipeRefreshLayout");
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
        if (categoryInfo.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_NEWS)) {
            if (newsInfoList == null) {
                newsInfoList = new ArrayList<>();
            } else {
                newsInfoList.clear();
            }
        }

        listIndex = 0;
        LOAD_MORE_COUNT = 0;
        isFlickerAdded = false;
        isStockAdded = false;
        setApiUrl();

        makeJsonObjectRequest();
    }

    private void swipeRefreshLayoutWithOutApi() {
        LOAD_MORE_COUNT = 0;
        setApiUrl();
        if (!categoryInfo.feedUrl.endsWith("1/"))
            LOAD_MORE_COUNT++;
    }

    private void setApiUrl() {
        if (categoryInfo.feedUrl.endsWith("1/")) {
            finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG1/";
        } else {
            finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + (LOAD_MORE_COUNT + 1) + "/";
        }
    }


    public void moveToTop() {
        Systr.println("Move to top called");
        if (recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.scrollBounds = new Rect();
        this.recyclerView.getHitRect(scrollBounds);
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            LOAD_MORE_COUNT++;
                            onLoadMore();
                        }
                    }
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }
        });
    }

    private void onLoadMore() {
        isLoading = true;
        recyclerView.post(() -> {
            if (exclusiveListAdapter != null)
                exclusiveListAdapter.addItem();
        });
        new Handler().postDelayed(() -> {
            finalFeedURL = AppUtils.getInstance().makeUrlForLoadMore(Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl, LOAD_MORE_COUNT);
            makeJsonObjectRequest();
        }, 1000);
    }

    public void setFragmentShow() {
        isShowing = true;
        sendTracking();
    }

    private void sendTracking() {
        // Tracking
        if (isShowing) {
            if (categoryInfo != null && trackingList != null) {
                while (trackingList.size() > 0) {
                    int value = trackingList.get(0) + (categoryInfo.feedUrl.endsWith("1/") ? 1 : 0);
                    trackingList.remove(0);

                    if (value > 1) {
                        value = value - 1;
                        String ga_action = categoryInfo.displayName;
                        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_DEPTH, ga_action, AppFlyerConst.GALabel.PG + value, campaign);
                        Systr.println("GA Events : " + AppFlyerConst.GACategory.SCROLL_DEPTH + ga_action + AppFlyerConst.GALabel.PG + value + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));
                    }
                }
            }
        }
    }

    private void makeJsonObjectRequest() {
        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            if (!isLoading && swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            AppLogs.printDebugLogs("Feed URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            if (response != null) {
                                if (!Constants.singleSessionCategoryIdList.contains(categoryInfo.id)) {
                                    Constants.singleSessionCategoryIdList.add(categoryInfo.id);
                                }
                                parseJsonObjectFeedList(response);
                                if (LOAD_MORE_COUNT > 0) {
                                    removeLoadingFromBottom();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                hideProgress();
                if (LOAD_MORE_COUNT > 0) {
                    removeLoadingFromBottom();
                }

                Activity activity = getActivity();
                if (activity != null && isAdded()) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
        } else {
            if (isAdded() && getActivity() != null)
                AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));

            hideProgress();
        }
    }

    private void setOfflineCommonData() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            try {
                showProgressBar();
                boolean isShowInternetMessage = true;
                if (newsInfoList == null) {
                    newsInfoList = new ArrayList<>();
                } else {
                    newsInfoList.clear();
                }
                CommonListTable commonListTable = (CommonListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CommonListTable.TABLE_NAME);

                List<NewsListInfo> newsListInfos = commonListTable.getAllNewsListInfoListAccordingToCategory(categoryInfo.id, getContext());
                for (NewsListInfo newsInfo : newsListInfos) {
                    ++listIndex;
                    newsInfo.listIndex = listIndex;
                }
                newsInfoList.addAll(newsListInfos);

                addStockPhotoFlickerWidget();
                addAdsAndWidgetInCommonList();
                exclusiveListAdapter.setData(newsInfoList);
//                exclusiveListAdapter.notifyDataSetChanged();
                hideProgress();
                swipeRefreshLayoutWithOutApi();
//                checkInternetAndShowToast(isShowInternetMessage);
            } catch (Exception ex) {
                hideProgress();
                AppLogs.printErrorLogs(TAG, ex.toString());
            }

        }, Constants.DataBaseHit.DEFAULT_TIME);

    }


    private void checkInternetAndShowToast(Boolean isShowInternetMessage) {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                isTokenExpire();
            } else {
                if (isShowInternetMessage)
                    if (isAdded() && getActivity() != null)
                        AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }
    }

    private void isTokenExpire() {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                CommonListTable commonListTable = (CommonListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CommonListTable.TABLE_NAME);
                String dbEntryCreationTime = "";
                dbEntryCreationTime = commonListTable.getCommonListItemCreationTime(categoryInfo.id);
                if (dbEntryCreationTime != null && !dbEntryCreationTime.equalsIgnoreCase("")) {
                    long dbEntryCreationTimemilliSeconds = Long.parseLong(dbEntryCreationTime);
                    long currentTime = System.currentTimeMillis();
                    if ((currentTime - dbEntryCreationTimemilliSeconds) <= application.getServerTimeoutValue()) {
                        //token not Expire
                        hideProgress();
                        setOfflineCommonData();
                    } else {
                        //token Expire
                        new Handler().postDelayed(() -> {
                            hideProgress();
                            firstApiHit();
                        }, 500L);
                    }
                } else {
                    hideProgress();
                    firstApiHit();
                }
                AppLogs.printDebugLogs(TAG + "valueFromServer :", "" + application.getServerTimeoutValue());
                AppLogs.printDebugLogs(TAG + "dbEntryCreationTime :", dbEntryCreationTime);
            } else {
                hideProgress();
                setOfflineCommonData();
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }
    }

    private void firstApiHit() {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                AppLogs.printDebugLogs(TAG + "firstApiHit :", "firstApiHit");
                showProgressBar();
                swipeRefreshLayout();
            } else {
                hideProgress();
                if (isAdded() && getActivity() != null)
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            }

        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }

    }


    private void removeLoadingFromBottom() {
        if (exclusiveListAdapter != null)
            exclusiveListAdapter.removeItem();
    }

    private void parseJsonObjectFeedList(JSONObject response) throws JSONException {
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);

        if (newsInfoList == null) {
            newsInfoList = new ArrayList<>();
        }
        if (resultArray.length() > 0) {

            if (LOAD_MORE_COUNT == 0) {
                isFlickerActive = response.optString("flicker_active").equalsIgnoreCase("1");
                newsInfoList.clear();
            } else {
                removeLoadingFromBottom();
                visibleThreshold += resultArray.length();
            }

            List<NewsListInfo> newsListInfos = Arrays.asList(new Gson().fromJson(resultArray.toString(), NewsListInfo[].class));
            for (NewsListInfo newsInfo : newsListInfos) {
                listIndex += 1;
                newsInfo.listIndex = listIndex;
            }

            newsInfoList.addAll(newsListInfos);

            // Inserting NewsList in mdb
            if (LOAD_MORE_COUNT == 0) {
                final CommonListTable commonListTable = (CommonListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CommonListTable.TABLE_NAME);
                commonListTable.deleteParticularNewsListCategory(categoryInfo.id, getContext());
                commonListTable.addCommonListItem(categoryInfo.id, newsInfoList, getContext());
                if (!categoryInfo.feedUrl.endsWith("1/"))
                    LOAD_MORE_COUNT++;
            }

            addStockPhotoFlickerWidget();
            addAdsAndWidgetInCommonList();

        } else {
            if (LOAD_MORE_COUNT > 0) {
                LOAD_MORE_COUNT--;
                removeLoadingFromBottom();
            }
        }

        exclusiveListAdapter.setData(newsInfoList);
        hideProgress();
        isLoading = false;
        // Tracking
        if (categoryInfo != null && resultArray != null && resultArray.length() > 0) {
            trackingList.add(LOAD_MORE_COUNT);
            sendTracking();
        }
    }

    private void addStockPhotoFlickerWidget() {
        if (isStockActive) {
            int posHome;
            String addedOn;
            addedOn = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.StockPrefs.STOCK_ADDED_ON_MENU, "");
            posHome = AppPreferences.getInstance(getContext()).getIntValue(QuickPreferences.StockPrefs.STOCK_MENU_POS, 0);
            JSONArray jsonElements;
            try {
                jsonElements = new JSONArray(addedOn);
                List<String> stringList = new ArrayList<>();
                for (int i = 0; i < jsonElements.length(); i++) {
                    stringList.add(jsonElements.getString(i));
                }
                if (stringList.contains(categoryInfo.id) && !isStockAdded) {
                    NewsListInfo listInfo = new NewsListInfo();
                    listInfo.isStockWidget = true;
                    isStockAdded = true;
                    newsInfoList.add(posHome, listInfo);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (isFlickerActive) {
            int posHome;
            String addedOn;
            posHome = AppPreferences.getInstance(getContext()).getIntValue(QuickPreferences.FlickerPrefs.FLICKER_MENU_POS, 0);
            addedOn = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.FlickerPrefs.FLICKER_ADDED_ON_MENU, "");

            JSONArray jsonElements;
            try {
                jsonElements = new JSONArray(addedOn);
                List<String> stringList = new ArrayList<>();
                for (int i = 0; i < jsonElements.length(); i++) {
                    stringList.add(jsonElements.getString(i));
                }
                if (stringList.contains(categoryInfo.id) && !isFlickerAdded) {
                    NewsListInfo listInfo = new NewsListInfo();
                    listInfo.isFlicker = true;
                    isFlickerAdded = true;
                    newsInfoList.add(posHome, listInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void addAdsAndWidgetInCommonList() {
//        if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
        for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
            if (!bannerInfoList.get(bannerIndex).isAdded) {
                int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catPos);
                if (bannerPos > -1 && newsInfoList.size() >= bannerPos) {
                    newsInfoList.add(bannerPos, new NewsListInfo(ExclusiveListAdapter.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)));
                    bannerInfoList.get(bannerIndex).isAdded = true;
                }
            }
        }
//        }
    }

//    @Override
//    public void onDataFetch(boolean flag, BrandInfo brandInfo) {
//
//    }

    @Override
    public void onDataFetch(boolean flag) {
    }

    @Override
    public void onDataFetch(boolean flag, String sectionName) {
    }

    @Override
    public void onDataFetch(boolean flag, int viewType) {

    }

    @Override
    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {

    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Subscribe
    public void NetworkConnectionStateChange(Boolean isConnected) {
        AppLogs.printDebugLogs(TAG + "newsList getMessage1", "" + isConnected);
        if (isConnected && isVisible()) {
            AppLogs.printDebugLogs(TAG + "ConnectionStateChange", "" + "" + isConnected);
            showProgressBar();
            swipeRefreshLayout();
        } else {
            //Internet Not Available
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();
        isShowing = false;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (exclusiveListAdapter != null) {
//            exclusiveListAdapter.setNotify();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
        newsInfoList = null;
        exclusiveListAdapter = null;
        finalFeedURL = null;
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }

        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {

    }
}
