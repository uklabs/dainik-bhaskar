package com.db.home;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;

import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.news.WapV2Activity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.views.viewholder.ExclusiveListViewHolder;
import com.db.views.viewholder.LoadingViewHolder;

import java.util.ArrayList;


public class ExclusiveListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_EXCLUSIVE = 1;
    public static final int VIEW_TYPE_BANNER = 2;
    private final int VIEW_TYPE_LOADING = 3;


    private ArrayList<NewsListInfo> mNewsFeed;

    private String gaScreen;
    private Context mContext;
    private CategoryInfo categoryInfo;

    public ExclusiveListAdapter(Context contexts, String gaScreen, CategoryInfo categoryInfo) {
        this.mContext = contexts;
        this.categoryInfo = categoryInfo;
        this.mNewsFeed = new ArrayList<>();
        this.gaScreen = gaScreen;
    }

    public void setData(ArrayList<NewsListInfo> newsFeed) {
        this.mNewsFeed.clear();
        this.mNewsFeed.addAll(newsFeed);
        notifyDataSetChanged();
    }

    public void addItem() {
        mNewsFeed.add(null);
//        notifyDataSetChanged();
    }

    public void removeItem() {
        if (mNewsFeed.size() > 0 && mNewsFeed.get(mNewsFeed.size() - 1) == null) {
            mNewsFeed.remove(mNewsFeed.size() - 1);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_EXCLUSIVE:
                return new ExclusiveListViewHolder(inflater.inflate(R.layout.recyclerlist_exclusive_item, parent, false));
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        switch (holder.getItemViewType()) {

            case VIEW_TYPE_EXCLUSIVE:
                onBindViewHolderExclusive((ExclusiveListViewHolder) holder, position);
                break;

            case VIEW_TYPE_LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
                loadingViewHolder.progressBar.setIndeterminate(true);
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
                break;
            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData(mNewsFeed.get(holder.getAdapterPosition()).mBannerInfo);
                break;
            default:
                break;
        }
    }

    private void onBindViewHolderExclusive(ExclusiveListViewHolder holder, int position) {
        final NewsListInfo newsListInfo = mNewsFeed.get(position);
        holder.tvTitle.setText(AppUtils.getInstance().getHomeTitle(newsListInfo.iitl_title, AppUtils.getInstance().fromHtml(newsListInfo.title), newsListInfo.colorCode));
        holder.tvAuthor.setText(newsListInfo.editorName);
        int themeColor = AppUtils.getThemeColor(mContext, newsListInfo.colorCode);

        if (TextUtils.isEmpty(newsListInfo.headerName)) {
            holder.vHeader.setVisibility(View.GONE);
        } else {
            holder.vHeader.setVisibility(View.VISIBLE);
            holder.tvHeader.setBackgroundColor(themeColor);
            holder.vHeaderDivider.setBackgroundColor(themeColor);
            holder.ivDiagonal.setColorFilter(themeColor);
            holder.tvHeader.setText(newsListInfo.exTopicName);
            holder.tvHeader.setTextColor(Color.WHITE);
        }
        ImageUtil.setImage(mContext, newsListInfo.editorImage, holder.ivAuthor, R.drawable.water_mark_news_list);
        holder.vContainer.setOnClickListener(view -> launchArticlePage(newsListInfo, position));
    }

    private void launchArticlePage(NewsListInfo newsListInfo, int position) {

        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : gaScreen) + newsListInfo.webUrl);
            mContext.startActivity(intent);

        } else if (newsListInfo.isOpenGallery) {
            Intent photoGalleryIntent = new Intent(mContext, PhotoGalleryActivity.class);
            photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, newsListInfo.storyId);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
            mContext.startActivity(photoGalleryIntent);

        } else {
            mContext.startActivity(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo, position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (this.mNewsFeed.get(position) == null)
            return VIEW_TYPE_LOADING;
        else if (this.mNewsFeed.get(position).type > 1) {
            return this.mNewsFeed.get(position).type;
        } else {
            return VIEW_TYPE_EXCLUSIVE;
        }
    }

    @Override
    public int getItemCount() {
        return mNewsFeed == null ? 0 : mNewsFeed.size();
    }

    public NewsListInfo getItemAtPosition(int position) {
        if (position >= 0 && position < mNewsFeed.size() && mNewsFeed.get(position) instanceof NewsListInfo)
            return (NewsListInfo) mNewsFeed.get(position);
        return null;
    }

}