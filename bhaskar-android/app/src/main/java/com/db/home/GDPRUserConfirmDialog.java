package com.db.home;

import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bhaskar.R;

public class GDPRUserConfirmDialog extends DialogFragment {

    View rootView;
    String messageHeading;
    String message;
    int dialogType;
    String yestText;
    String noText;
    GDPRUserConfirmation gdprUserConfirmation;
    private static final String MESSAGE_HEADING = "message_heading";
    private static final String MESSAGE = "message";
    private static final String DIALOG_TYPE = "dialog_type";
    private static final String YES_TEXT = "yes_text";
    private static final String NO_TEXT = "no_text";


    public static GDPRUserConfirmDialog newInstance(String messageHeading, String message, int dialogType, String yesText, String noText) {
        GDPRUserConfirmDialog f = new GDPRUserConfirmDialog();
        Bundle b = new Bundle();
        b.putString(MESSAGE_HEADING, messageHeading);
        b.putString(MESSAGE, message);
        b.putInt(DIALOG_TYPE, dialogType);
        b.putString(YES_TEXT, yesText);
        b.putString(NO_TEXT, noText);
        f.setArguments(b);
        return f;
    }

    public void setConfirmListener(GDPRUserConfirmation gdprUserConfirmation) {
        this.gdprUserConfirmation = gdprUserConfirmation;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.messageHeading = arguments.getString(MESSAGE_HEADING);
            this.message = arguments.getString(MESSAGE);
            this.dialogType = arguments.getInt(DIALOG_TYPE);
            this.yestText = arguments.getString(YES_TEXT);
            this.noText = arguments.getString(NO_TEXT);
        }
    }

    TextView tv_heading;
    TextView tv_message;
    TextView tv_yes;
    TextView tv_no;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getDialog() != null && getDialog().getWindow() != null)
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.transparent_desc_bg_color)));
        rootView = inflater.inflate(R.layout.gdpr_confirm_dialog, container, false);
        tv_heading = rootView.findViewById(R.id.tv_heading);
        tv_message = rootView.findViewById(R.id.tv_message);
        tv_no = rootView.findViewById(R.id.tv_no);
        tv_yes = rootView.findViewById(R.id.tv_yes);

        tv_heading.setText((messageHeading != null) ? messageHeading.toUpperCase() : "CONFIRM DIALOG");
        tv_message.setText(((message != null) ? Html.fromHtml(message) : "Please choose your option")/*, TextView.BufferType.SPANNABLE*/);
        tv_no.setText((noText != null ? noText : "CANCEL"));

        switch (dialogType) {
            case GDPRUserConfirmation.DIALOG_WITHDRAW_CONSENT:
                tv_yes.setText((yestText != null ? yestText : "WITHDRAW"));
//                tv_yes.setText("WITHDRAW");
                break;
            case GDPRUserConfirmation.DIALOG_DELETE_PROFILE:
                tv_yes.setText((yestText != null ? yestText : "DELETE"));
//                tv_yes.setText("DELETE");
                break;
            case GDPRUserConfirmation.DIALOG_DOWNLOAD_PROFILE:
                tv_yes.setText((yestText != null ? yestText : "DOWNLOAD"));
//                tv_yes.setText("DOWNLOAD");
                break;
        }
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.tv_no:
                        dismiss();
                        break;
                    case R.id.tv_yes:
                        if (gdprUserConfirmation != null)
                            gdprUserConfirmation.selectedOption(true, dialogType);
                        else
                            dismiss();
                        break;
                }
            }
        };
        tv_no.setOnClickListener(clickListener);
        tv_yes.setOnClickListener(clickListener);
        return rootView;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (gdprUserConfirmation != null)
            gdprUserConfirmation.selectedOption(false, dialogType);
    }
}
