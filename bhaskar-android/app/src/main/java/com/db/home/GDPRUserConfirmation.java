package com.db.home;

public interface GDPRUserConfirmation {
    int DIALOG_WITHDRAW_CONSENT=1;
    int DIALOG_DELETE_PROFILE=2;
    int DIALOG_DOWNLOAD_PROFILE=3;
    void selectedOption(boolean option, int type);
}
