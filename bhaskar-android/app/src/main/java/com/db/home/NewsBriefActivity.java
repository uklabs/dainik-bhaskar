package com.db.home;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.main.BaseAppCompatActivity;
import com.db.util.AppUtils;
import com.db.util.Constants;

public class NewsBriefActivity extends BaseAppCompatActivity {

    private CategoryInfo categoryInfo;
    private CardView appbarLayout;
    private NewsBriefListFragment2 fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.getInstance().setStatusBarColor(this, getResources().getColor(R.color.black));
        setContentView(R.layout.activity_news_brief);

        TextView tvTitle = findViewById(R.id.toolbar_title);
        tvTitle.setTextColor(Color.BLACK);
        findViewById(R.id.back_icon).setOnClickListener(view -> NewsBriefActivity.this.finish());
        findViewById(R.id.refresh_icon).setOnClickListener(view -> fragment.refreshItems());

        Intent intent = getIntent();
        if (intent != null) {
            categoryInfo = (CategoryInfo) intent.getSerializableExtra(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        tvTitle.setText(categoryInfo.menuName);

        appbarLayout = findViewById(R.id.app_bar_layout);

        fragment = NewsBriefListFragment2.getInstance(categoryInfo);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.root_container, fragment);
        transaction.commitAllowingStateLoss();

        findViewById(R.id.rlayout).setOnClickListener(view -> {
            onClickCalled();
        });
    }

    public void onClickCalled() {
//        if (appbarLayout.getVisibility() == View.VISIBLE) {
//            appbarLayout.setVisibility(View.GONE);
//        } else {
//            appbarLayout.setVisibility(View.VISIBLE);
//        }

        if (appbarLayout.getVisibility() == View.GONE) {
            appbarLayout.animate()
                    .translationY(0).alpha(1.0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            super.onAnimationStart(animation);
                            appbarLayout.setVisibility(View.VISIBLE);
                            appbarLayout.setAlpha(0.0f);
                        }
                    });
        } else {
            appbarLayout.animate()
                    .translationY(-appbarLayout.getHeight()).alpha(0.0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            appbarLayout.setVisibility(View.GONE);
                        }
                    });
        }
    }

    public void hideTop() {
//        appbarLayout.setVisibility(View.GONE);

        appbarLayout.animate()
                .translationY(-appbarLayout.getHeight()).alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        appbarLayout.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Constants.REQ_CODE_BOTTOM_NAV){
            setResult(RESULT_OK);
            finish();
        }
    }
}
