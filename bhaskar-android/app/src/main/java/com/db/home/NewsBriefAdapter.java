package com.db.home;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController2;
import com.bhaskar.appscommon.ads.BannerCardAdViewHolder;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.news.WapV2Activity;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.views.JustifiedTextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NewsBriefAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController2.onAdNotifyListener {

    private static final int VIEW_TYPE_LIST = 1;
    public static final int VIEW_TYPE_BANNER = 2;
    private final int VIEW_TYPE_BANNER_ADS_LISTING = 25;

    private final CategoryInfo categoryInfo;
    private List<NewsListInfo> mNewsFeed;
    private Context mContext;
    private int count;

    private int lastPosition = -1;
    private String color;
    private int listSize;

    public NewsBriefAdapter(Context mContext, CategoryInfo categoryInfo) {
        this.mContext = mContext;
        this.categoryInfo = categoryInfo;
        this.color = categoryInfo.color;
        this.mNewsFeed = new ArrayList<>();
    }

    public void setData(List<NewsListInfo> mNewsFeed, int listSize) {
        this.listSize = listSize;
        this.mNewsFeed.clear();
        this.mNewsFeed.addAll(mNewsFeed);
        count = mNewsFeed.size();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_BANNER_ADS_LISTING:
                return new BannerCardAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_cardview, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            case VIEW_TYPE_LIST:
                return new NewsBriefViewHolder(inflater.inflate(R.layout.recyclerlist_item_news_brief, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_LIST:
                final NewsBriefViewHolder newsBriefViewHolder = (NewsBriefViewHolder) holder;
                final NewsListInfo newsListItem = mNewsFeed.get(newsBriefViewHolder.getAdapterPosition());
                newsBriefViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(newsListItem.iitl_title, AppUtils.getInstance().fromHtml(newsListItem.title), newsListItem.colorCode));

                float ratio = AppUtils.getInstance().parseImageRatio(newsListItem.imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
                AppUtils.getInstance().setImageViewSizeWithAspectRatio(newsBriefViewHolder.storyIv, Constants.DEVICE_WIDTH - ((int) AppUtils.getInstance().convertDpToPixel(8, mContext)), ratio);
                if (!TextUtils.isEmpty(newsListItem.image)) {
                    if (newsListItem.image.contains(".gif")) {
                        ImageUtil.setImageGif(mContext, newsListItem.image, newsBriefViewHolder.storyIv, R.drawable.water_mark_news_detail);
                    } else {
                        ImageUtil.setImage(mContext, newsListItem.image, newsBriefViewHolder.storyIv, R.drawable.water_mark_news_detail);
                    }
                } else {
                    newsBriefViewHolder.storyIv.setImageResource(R.drawable.water_mark_news_detail);
                }

                if (newsListItem.videoFlag == 1) {
                    newsBriefViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
                    newsBriefViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, color));
                } else {
                    newsBriefViewHolder.imgVideoPlay.setVisibility(View.GONE);
                }
                if (newsListItem.bulletsPoint != null && newsListItem.bulletsPoint.length > 0) {
                    LayoutInflater inflater = LayoutInflater.from(mContext);
                    newsBriefViewHolder.bulletLayout.removeAllViews();
                    for (int i = 0; i < newsListItem.bulletsPoint.length; i++) {
                        View item = inflater.inflate(R.layout.layout_bullet_type, null);
                        String desc = newsListItem.bulletsPoint[i];
                        ((JustifiedTextView) item.findViewById(R.id.bullet_tv)).setText(AppUtils.getInstance().fromHtml(desc));
                        ((JustifiedTextView) item.findViewById(R.id.bullet_tv)).setTextColor(Color.parseColor("#333333"));
                        ((ImageView) item.findViewById(R.id.iv_bullet_circle)).setColorFilter(AppUtils.getThemeColor(mContext, newsListItem.colorCode));
                        newsBriefViewHolder.bulletLayout.addView(item);
                    }
                    newsBriefViewHolder.bulletLayout.setVisibility(View.VISIBLE);
                } else {
                    newsBriefViewHolder.bulletLayout.setVisibility(View.GONE);
                }
                newsBriefViewHolder.storyCountTv.setText(mContext.getResources().getString(R.string.news_brief_story_count, newsListItem.listIndex, listSize));
//                newsBriefViewHolder.storyShareIv.setOnClickListener(v -> BackgroundRequest.getStoryShareUrl(mContext, newsListItem.webUrl, (flag, shareUrl) -> {
//                    if (flag) {
//                        AppUtils.getInstance().shareClick(mContext, newsListItem.title, shareUrl, newsListItem.gTrackUrl, true);
//                    } else {
//                        AppUtils.getInstance().shareClick(mContext, newsListItem.title, newsListItem.webUrl, newsListItem.gTrackUrl, true);
//                    }
//                })
//                );
                newsBriefViewHolder.storyShareIv.setOnClickListener(view -> AppUtils.getInstance().shareClick(mContext, newsListItem.title, newsListItem.webUrl, newsListItem.gTrackUrl, true));

                newsBriefViewHolder.itemView.setOnClickListener(v -> launchArticlePage(newsListItem, position));

                // Here you apply the animation when the view is bound
                setAnimation(holder.itemView, position);
                break;
            case VIEW_TYPE_BANNER_ADS_LISTING:
                showBannerAds(((BannerCardAdViewHolder) holder), position);
                break;
            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData(mNewsFeed.get(holder.getAdapterPosition()).mBannerInfo);
                break;
        }
    }

    @Override
    public void onNotifyAd() {
        notifyDataSetChanged();
    }

    private void showBannerAds(BannerCardAdViewHolder viewHolder, int pos) {
        NewsListInfo itemInfo = mNewsFeed.get(pos);
        viewHolder.addAds(AdController2.getInstance(mContext).getAd(itemInfo.adUnit, new WeakReference<>(this)));
    }

    private void launchArticlePage(NewsListInfo newsListInfo, int position) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            mContext.startActivity(intent);
        } else {
//            newsListInfo.bulletsPoint = null;
            mContext.startActivity(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo, position));
        }
    }

    @Override
    public int getItemCount() {
        return mNewsFeed.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mNewsFeed.get(position).isAd) {
            return VIEW_TYPE_BANNER_ADS_LISTING;
        } else if (mNewsFeed.get(position).type > 1) {
            return mNewsFeed.get(position).type;
        }

        return VIEW_TYPE_LIST;
    }

    public class NewsBriefViewHolder extends RecyclerView.ViewHolder {

        ImageView storyIv;
        TextView titleTextView, storyCountTv;
        ImageView imgVideoPlay, storyShareIv;
        LinearLayout bulletLayout;

        NewsBriefViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.title_tv);
            storyIv = itemView.findViewById(R.id.story_iv);
            imgVideoPlay = itemView.findViewById(R.id.video_play_iv);
            bulletLayout = itemView.findViewById(R.id.bullet_layout);
            storyCountTv = itemView.findViewById(R.id.story_count_tv);
            storyShareIv = itemView.findViewById(R.id.story_share_iv);
            itemView.setTag(itemView);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.zoom_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
