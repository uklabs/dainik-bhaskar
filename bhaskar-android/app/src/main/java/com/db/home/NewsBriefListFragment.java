package com.db.home;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.ads.BannerAdPlacementController;
import com.bhaskar.appscommon.ads.AdController2;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsBriefListFragment extends Fragment {

    private String feedURL;


    private boolean isMarginAllowed;
    private Context mContext;
    private List<NewsListInfo> mList;
    private RecyclerView newsBriefRv;
    private NewsBriefAdapter newsBriefAdapter;
    private ProgressBar progressBar;
    private LinearLayout dataLayout;
    private TextView dateTv;
    private CategoryInfo categoryInfo;
    private List<BannerInfo> bannerInfoList;

    public NewsBriefListFragment() {
        // Required empty public constructor
    }

    public static NewsBriefListFragment getInstance(boolean isMarginAllowed, CategoryInfo categoryInfo) {
        NewsBriefListFragment newsBriefListFragment = new NewsBriefListFragment();
        Bundle argument = new Bundle();
        argument.putBoolean("isMarginAllowed", isMarginAllowed);
        argument.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        newsBriefListFragment.setArguments(argument);
        return newsBriefListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            isMarginAllowed = bundle.getBoolean("isMarginAllowed");
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        if (getActivity() != null) {
            mContext = getActivity();
        }
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_news_breif_list, container, false);
        newsBriefRv = rootView.findViewById(R.id.news_brief_rv);
        dateTv = rootView.findViewById(R.id.date_tv);
        progressBar = rootView.findViewById(R.id.progress_bar);
        dataLayout = rootView.findViewById(R.id.data_layout);
        if (isMarginAllowed) {
            TypedValue tv = new TypedValue();
            int actionBarHeight = 0;
            if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) dataLayout.getLayoutParams();
            params.setMargins(0, 0, 0, actionBarHeight);
            dataLayout.setLayoutParams(params);
            dataLayout.requestLayout();
        }

        newsBriefAdapter = new NewsBriefAdapter(mContext, categoryInfo);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
        newsBriefRv.setLayoutManager(linearLayoutManager);
        newsBriefRv.setAdapter(newsBriefAdapter);

        feedURL = String.format("%s%s", Urls.APP_FEED_BASE_URL, categoryInfo.feedUrl);
        makeJsonObjectRequest(feedURL);
        return rootView;
    }

    public void refreshItems() {
        bannerInfoList = JsonParser.getInstance().getBannersToAdd( categoryInfo.id);
        AppUtils.getInstance().showCustomToast(getContext(), "Refreshing..");
        makeJsonObjectRequest(feedURL);
    }

    private void makeJsonObjectRequest(String finalFeedURL) {
        Systr.println("Feed Url : " + finalFeedURL);

        if (NetworkStatus.getInstance().isConnected(mContext)) {
            progressBar.setVisibility(View.VISIBLE);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            if (response != null) {
                                parseFeedList(response);
                            }
                        } catch (Exception ignored) {
                        }
                    }, error -> {
                if (error instanceof NoConnectionError || error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
        }
    }

    private void parseFeedList(JSONObject response) throws JSONException {
        String date = response.optString("date");
        if (!TextUtils.isEmpty(date)) {
            dateTv.setText(date);
            dateTv.setVisibility(View.VISIBLE);
        } else {
            dateTv.setVisibility(View.GONE);
        }

        int listIndex = 0;
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        int listSize = resultArray.length();
        if (resultArray != null) {
            mList = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), NewsListInfo[].class)));
            for (NewsListInfo newsInfo : mList) {
                ++listIndex;
                newsInfo.listIndex = listIndex;
            }
            if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.BANNER_LIST_TOGGLE, false)) {
                mList = BannerAdPlacementController.getInstance().addNewsBreifListingBannerAds(mList);
            }
            addAdsAndWidgetInCommonList();
            handleUI(listSize);
        }
    }

    public void addAdsAndWidgetInCommonList() {
//        if (categoryInfo.brandId.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
            for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
                if (!bannerInfoList.get(bannerIndex).isAdded) {
                    int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catPos);
                    if (bannerPos > -1 && mList.size() >= bannerPos) {
                        mList.add(bannerPos, new NewsListInfo(NewsBriefAdapter.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)));
                        bannerInfoList.get(bannerIndex).isAdded = true;
                    }
                }
            }
//        }
    }

    private void handleUI(int listSize) {
        dataLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        newsBriefAdapter.setData(mList, listSize);
        newsBriefRv.scrollToPosition(0);
    }


    @Override
    public void onDestroyView() {
        AdController2.getInstance(getContext()).destroy();
        super.onDestroyView();
    }
}
