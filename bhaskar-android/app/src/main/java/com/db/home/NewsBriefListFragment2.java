package com.db.home;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.divya_new.articlePage.NewsDetailTutorialDialog;
import com.db.listeners.FragmentLifecycle;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.db.views.vp.VerticalPageTransformer;
import com.db.views.vp.VerticalViewPager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.os.Looper.getMainLooper;

public class NewsBriefListFragment2 extends Fragment {

    private Context mContext;
    private CategoryInfo categoryInfo;
    private String feedURL;
    private String date;

    private ProgressBar progressBar;
    private VerticalViewPager pager;

    private ViewPagerAdapter viewPagerAdapter;
    private int currentPos;
    public static final int ANIMATE_SCROLL_DURATION = 500;
    private ViewPager.OnPageChangeListener onPageChangeListener;

    private Handler scrollHandler;
    private String BRIEF_TUTORIAL = "BriefTutorial";
    private final Runnable newsBriefCallback = () -> {
        if (getContext() != null && isAdded()) {
            NewsDetailTutorialDialog newsDetailTutorialDialog = new NewsDetailTutorialDialog(getContext(),
                    NewsDetailTutorialDialog.TutorialType.BRIEF);
            newsDetailTutorialDialog.show();
            newsDetailTutorialDialog.setOnDismissListener(dialog -> saveNewsBriefShown());
        }
    };

    private void saveNewsBriefShown() {
        AppPreferences.getInstance(getContext()).setBooleanValue(BRIEF_TUTORIAL, true);
    }

    private boolean isNewsBriefTutorial_Shown() {
        return AppPreferences.getInstance(getContext()).getBooleanValue(BRIEF_TUTORIAL, false);
    }

    public void handlerNestedRefreshViewScroll() {
        if (isNewsBriefTutorial_Shown())
            return;

        if (scrollHandler != null) {
            scrollHandler.postDelayed(newsBriefCallback, ANIMATE_SCROLL_DURATION);
        }
    }

    public NewsBriefListFragment2() {
    }

    public static NewsBriefListFragment2 getInstance(CategoryInfo categoryInfo) {
        NewsBriefListFragment2 newsBriefListFragment = new NewsBriefListFragment2();
        Bundle argument = new Bundle();
        argument.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        newsBriefListFragment.setArguments(argument);
        return newsBriefListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }

        if (getActivity() != null) {
            mContext = getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news_breif_list2, container, false);
        progressBar = rootView.findViewById(R.id.progress_bar);

        pager = rootView.findViewById(R.id.viewpager2);
        pager.setPageTransformer(true, new VerticalPageTransformer());

        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());

        feedURL = String.format("%s%s", Urls.APP_FEED_BASE_URL, categoryInfo.feedUrl);
        makeJsonObjectRequest(feedURL);

        onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                checkForPauseResumeFragment(position);

                try {
                    ((NewsBriefActivity) getActivity()).hideTop();
                } catch (Exception e) {
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        };
        pager.setOnPageChangeListener(onPageChangeListener);
        scrollHandler = new Handler(getMainLooper());
        return rootView;
    }

    private void checkForPauseResumeFragment(int position) {
        if (pager != null) {
            PagerAdapter newsPagerAdapter = pager.getAdapter();
            if (newsPagerAdapter != null) {
                try {
                    FragmentLifecycle fragmentToShow = (FragmentLifecycle) newsPagerAdapter.instantiateItem(pager, currentPos);
                    fragmentToShow.onPauseFragment();
                } catch (Exception ignored) {
                }
                try {
                    FragmentLifecycle lifecycle = (FragmentLifecycle) newsPagerAdapter.instantiateItem(pager, position);
                    lifecycle.onResumeFragment();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        currentPos = position;
    }

    public void refreshItems() {
        makeJsonObjectRequest(feedURL);
    }

    private void makeJsonObjectRequest(String finalFeedURL) {
        Systr.println("Feed Url : " + finalFeedURL);
        if (NetworkStatus.getInstance().isConnected(mContext)) {
            progressBar.setVisibility(View.VISIBLE);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    response -> {
                        try {
                            if (response != null) {
                                parseFeedList(response);
                            }
                        } catch (Exception ignored) {
                        }
                        progressBar.setVisibility(View.GONE);
                    }, error -> {
                if (error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        } else {
            AppUtils.getInstance().showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
        }
    }

    private void parseFeedList(JSONObject response) throws JSONException {
        date = response.optString("date");

        int listIndex = 0;
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        if (resultArray != null) {
            List<NewsListInfo> mList = new ArrayList<>(Arrays.asList(new Gson().fromJson(resultArray.toString(), NewsListInfo[].class)));
            for (NewsListInfo newsInfo : mList) {
                ++listIndex;
                newsInfo.listIndex = listIndex;
            }

            if (getActivity() != null) {
                getActivity().runOnUiThread(() -> {
                    viewPagerAdapter.addData(mList);
                    pager.setAdapter(viewPagerAdapter);
                    pager.setCurrentItem(0);
                    onPageChangeListener.onPageSelected(0);
                });
            }

            try {
                ((NewsBriefActivity) getActivity()).hideTop();
            } catch (Exception e) {
            }

            handlerNestedRefreshViewScroll();
        }
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private List<NewsListInfo> mnList;

        private ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            mnList = new ArrayList<>();
        }

        void addData(List<NewsListInfo> list) {
            mnList.clear();
            mnList.addAll(list);
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            return NewsBriefListSubFragment2.getInstance(categoryInfo, mnList.get(position), position + 1, getCount(), date);
        }

        @Override
        public int getCount() {
            return mnList.size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }
    }
}
