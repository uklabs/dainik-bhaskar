package com.db.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.NewsPhotoInfo;
import com.db.dbvideo.player.BusProvider;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.articlePage.DivyaCommonPhotoFragment;
import com.db.listeners.FragmentLifecycle;
import com.db.listeners.OnItemClickListener;
import com.db.eventWrapper.FragmentItemClickEventWrapper;
import com.db.news.WapV2Activity;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.views.vp.EllipsizingTextView;
import com.shuhart.bubblepagerindicator.BubblePageIndicator;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class NewsBriefListSubFragment extends Fragment implements OnItemClickListener, FragmentLifecycle {

    private Context mContext;
    private CategoryInfo categoryInfo;
    private NewsListInfo newsListItem;
    private int position;
    private int listSize;
    private String color;
    private String date;

    private ViewPager viewpager;
    private TextView titleTextView, storyCountTv;
    private EllipsizingTextView bulletTextView;
    private ImageView storyShareIv;
    private TextView dateTv;

    private ImageView leftArrowImageView;
    private ImageView rightArrowImageView;
    private ViewPagerAdapter adapter;

    private int selectedPosition;

    public NewsBriefListSubFragment() {
    }

    public static NewsBriefListSubFragment getInstance(CategoryInfo categoryInfo, NewsListInfo newsListInfo, int position, int totalSize, String date) {
        NewsBriefListSubFragment newsBriefListFragment = new NewsBriefListSubFragment();

        Bundle argument = new Bundle();
        argument.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        argument.putSerializable("newsinfo", newsListInfo);
        argument.putInt("pos", position);
        argument.putInt("total", totalSize);
        argument.putString("date", date);
        newsBriefListFragment.setArguments(argument);

        return newsBriefListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            newsListItem = (NewsListInfo) bundle.getSerializable("newsinfo");
            position = bundle.getInt("pos");
            listSize = bundle.getInt("total");
            date = bundle.getString("date");

            color = categoryInfo.color;
        }

        if (getActivity() != null) {
            mContext = getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub_newsbrief, container, false);

        viewpager = rootView.findViewById(R.id.nb_viewpager);
        storyCountTv = rootView.findViewById(R.id.nb_story_count_tv);
        storyShareIv = rootView.findViewById(R.id.nb_story_share_iv);
        dateTv = rootView.findViewById(R.id.nb_date_tv);
        titleTextView = rootView.findViewById(R.id.nb_title_tv);
        bulletTextView = rootView.findViewById(R.id.nb_bullet_tv);

        leftArrowImageView = rootView.findViewById(R.id.left_swipe_arrow);
        rightArrowImageView = rootView.findViewById(R.id.right_swipe_arrow);

        setData();

        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addData(newsListItem.photosNB);
        viewpager.setAdapter(adapter);

        BubblePageIndicator pager_indicator = rootView.findViewById(R.id.nb_pager_indicator);
        if (newsListItem.photosNB != null && newsListItem.photosNB.size() > 1) {
            pager_indicator.setVisibility(View.VISIBLE);
            pager_indicator.setViewPager(viewpager);
            pager_indicator.setFillColor(AppUtils.getThemeColor(getActivity(), categoryInfo.color));

            leftArrowImageView.setVisibility(View.GONE);
            rightArrowImageView.setVisibility(View.VISIBLE);

        } else {
            pager_indicator.setVisibility(View.GONE);

            leftArrowImageView.setVisibility(View.GONE);
            rightArrowImageView.setVisibility(View.GONE);
        }

        rootView.setOnClickListener(v -> launchArticlePage(newsListItem, position));

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedPosition = position;

                leftArrowImageView.setVisibility(View.VISIBLE);
                rightArrowImageView.setVisibility(View.VISIBLE);
                if (position == 0) {
                    leftArrowImageView.setVisibility(View.GONE);
                }
                if (position == adapter.getCount() - 1) {
                    rightArrowImageView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        return rootView;
    }

    @Override
    public void onResumeFragment() {
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPauseFragment() {
        BusProvider.getInstance().unregister(this);
    }

    private void setData() {
        if (TextUtils.isEmpty(date)) {
            dateTv.setVisibility(View.GONE);
        } else {
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM");
//            date = simpleDateFormat.format(new Date());
            dateTv.setText(date);
        }

        titleTextView.setText(AppUtils.getInstance().getHomeTitle(newsListItem.iitl_title, AppUtils.getInstance().fromHtml(newsListItem.titleNB), newsListItem.colorCode));
        if (newsListItem.bulletsPointNB != null && newsListItem.bulletsPointNB.length > 0) {
            String desc = "";
            for (int i = 0; i < newsListItem.bulletsPointNB.length; i++) {
                desc = desc + newsListItem.bulletsPointNB[i];
            }
            if (desc.length() > 300) {
                desc = desc.substring(0, desc.lastIndexOf(" ", 300)) + "...";
            }
            bulletTextView.setText(AppUtils.getInstance().fromHtml(desc));
        }

        storyCountTv.setText(mContext.getResources().getString(R.string.news_brief_story_count, newsListItem.listIndex, listSize));

//        storyShareIv.setOnClickListener(v -> BackgroundRequest.getStoryShareUrl(mContext, newsListItem.webUrl, new OnStoryShareUrlListener() {
//                    @Override
//                    public void onUrlFetch(boolean flag, String shareUrl) {
//                        if (flag) {
//                            AppUtils.getInstance().shareClick(mContext, newsListItem.title, shareUrl, newsListItem.gTrackUrl, true);
//                        } else {
//                            AppUtils.getInstance().shareClick(mContext, newsListItem.title, newsListItem.webUrl, newsListItem.gTrackUrl, true);
//                        }
//                    }
//                })
//        );
        storyShareIv.setOnClickListener(v -> AppUtils.getInstance().shareClick(mContext, newsListItem.title, newsListItem.webUrl, newsListItem.gTrackUrl, true));

        rightArrowImageView.setOnClickListener(view -> {
            int rpos = selectedPosition + 1;
            if (rpos < adapter.getCount())
                viewpager.setCurrentItem(rpos);
        });

        leftArrowImageView.setOnClickListener(view -> {
            int lpos = selectedPosition - 1;
            if (lpos >= 0)
                viewpager.setCurrentItem(lpos);
        });
    }

    private void launchArticlePage(NewsListInfo newsListInfo, int position) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            mContext.startActivity(intent);
        } else {
//            newsListInfo.bulletsPoint = null;
            mContext.startActivity(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo, position));
        }
    }

    @Subscribe
    public void responseFromFragmentClick(FragmentItemClickEventWrapper eventWrapper) {
        launchArticlePage(newsListItem, eventWrapper.getPosition());
    }

    @Override
    public void onClick(View view, int position) {
        launchArticlePage(newsListItem, position);
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<NewsPhotoInfo> mnList;

        private ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            mnList = new ArrayList<>();
        }

        void addData(List<NewsPhotoInfo> list) {
            mnList.clear();
            mnList.addAll(list);
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            NewsPhotoInfo newsPhotoInfo = mnList.get(position);
            if(TextUtils.isEmpty(newsPhotoInfo.title)&&newsListItem!=null){
                newsPhotoInfo.title = newsListItem.title;
            }
            DivyaCommonPhotoFragment divyaCommonPhotoFragment = DivyaCommonPhotoFragment.newInstance(newsPhotoInfo, position, newsPhotoInfo.imageSize, categoryInfo.gaArticle, categoryInfo.gaEventLabel, categoryInfo.displayName, false,color);
            divyaCommonPhotoFragment.setListener(NewsBriefListSubFragment.this);
            return divyaCommonPhotoFragment;
        }

        @Override
        public int getCount() {
            return mnList.size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }
    }
}
