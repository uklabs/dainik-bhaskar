package com.db.home;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.NewsPhotoInfo;
import com.db.data.source.remote.SerializeData;
import com.db.dbvideo.player.BusProvider;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.articlePage.DivyaCommonPhotoFragment;
import com.db.listeners.FragmentLifecycle;
import com.db.listeners.OnItemClickListener;
import com.db.eventWrapper.FragmentItemClickEventWrapper;
import com.db.news.WapV2Activity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.shuhart.bubblepagerindicator.BubblePageIndicator;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class NewsBriefListSubFragment2 extends Fragment implements OnItemClickListener, FragmentLifecycle {

    private Context mContext;
    private CategoryInfo categoryInfo;
    private NewsListInfo newsListItem;
    private int position;
    private int listSize;
    private String color;
    private String date;

    private ViewPager viewpager;
    private TextView titleTextView, storyCountTv;
    private EditText bulletTextView;

    private TextView dateTv;
    private TextView readMoreTextView;
    private ImageView bottomReadMoreImageView;

    private ImageView ivBookmark;

    private ImageView leftArrowImageView;
    private ImageView rightArrowImageView;
    private ViewPagerAdapter adapter;

    private int selectedPosition;
    private RelativeLayout bottomLayout2;
    private RelativeLayout imageLayout;

    public NewsBriefListSubFragment2() {
    }

    public static NewsBriefListSubFragment2 getInstance(CategoryInfo categoryInfo, NewsListInfo newsListInfo, int position, int totalSize, String date) {
        NewsBriefListSubFragment2 newsBriefListFragment = new NewsBriefListSubFragment2();

        Bundle argument = new Bundle();
        argument.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        argument.putSerializable("newsinfo", newsListInfo);
        argument.putInt("pos", position);
        argument.putInt("total", totalSize);
        argument.putString("date", date);
        newsBriefListFragment.setArguments(argument);

        return newsBriefListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            newsListItem = (NewsListInfo) bundle.getSerializable("newsinfo");
            position = bundle.getInt("pos");
            listSize = bundle.getInt("total");
            date = bundle.getString("date");

            color = categoryInfo.color;
        }

        if (getActivity() != null) {
            mContext = getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub_newsbrief2, container, false);

        viewpager = rootView.findViewById(R.id.nb_viewpager);
        storyCountTv = rootView.findViewById(R.id.nb_story_count_tv);
//        storyShareIv = rootView.findViewById(R.id.nb_story_share_iv);
        dateTv = rootView.findViewById(R.id.nb_date_tv);
        titleTextView = rootView.findViewById(R.id.nb_title_tv);
        readMoreTextView = rootView.findViewById(R.id.read_more_tv);
        bottomLayout2 = rootView.findViewById(R.id.layout_bottom2);
        bottomReadMoreImageView = rootView.findViewById(R.id.bottom_readmore_image);

        leftArrowImageView = rootView.findViewById(R.id.left_swipe_arrow);

        rightArrowImageView = rootView.findViewById(R.id.right_swipe_arrow);
        bulletTextView = rootView.findViewById(R.id.nb_bullet_tv);

        imageLayout = rootView.findViewById(R.id.nb_img_layout);
        ivBookmark = rootView.findViewById(R.id.iv_bookmark);
        resetBookmark();
        AppUtils.getInstance().setViewSizeWithAspectRatioOrMax43(imageLayout, AppUtils.getInstance().parseImageRatio(newsListItem.imageSize, Constants.ImageRatios.NEWS_BRIEF_RATIO));


        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addData(newsListItem.photosNB);
        viewpager.setAdapter(adapter);

        BubblePageIndicator pager_indicator = rootView.findViewById(R.id.nb_pager_indicator);
        if (newsListItem.photosNB != null && newsListItem.photosNB.size() > 1) {
            pager_indicator.setVisibility(View.VISIBLE);
            pager_indicator.setViewPager(viewpager);
            pager_indicator.setFillColor(AppUtils.getThemeColor(getActivity(), categoryInfo.color));

            leftArrowImageView.setVisibility(View.GONE);
            rightArrowImageView.setVisibility(View.VISIBLE);

        } else {
            pager_indicator.setVisibility(View.GONE);

            leftArrowImageView.setVisibility(View.GONE);
            rightArrowImageView.setVisibility(View.GONE);
        }

        readMoreTextView.setOnClickListener(v -> launchArticlePage(newsListItem, position));

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectedPosition = position;

                leftArrowImageView.setVisibility(View.VISIBLE);
                rightArrowImageView.setVisibility(View.VISIBLE);
                if (position == 0) {
                    leftArrowImageView.setVisibility(View.GONE);
                }
                if (position == adapter.getCount() - 1) {
                    rightArrowImageView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        rootView.findViewById(R.id.nb_root_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                headerCheck();
            }
        });

        bulletTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                headerCheck();
            }
        });

//        rootView.findViewById(R.id.share_layout).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                BackgroundRequest.getStoryShareUrl(mContext, newsListItem.webUrl, new OnStoryShareUrlListener() {
//                    @Override
//                    public void onUrlFetch(boolean flag, String shareUrl) {
//                        if (flag) {
//                            AppUtils.getInstance().shareClick(mContext, newsListItem.title, shareUrl, newsListItem.gTrackUrl, true);
//                        } else {
//                            AppUtils.getInstance().shareClick(mContext, newsListItem.title, newsListItem.webUrl, newsListItem.gTrackUrl, true);
//                        }
//                    }
//                });
//            }
//        });

        rootView.findViewById(R.id.share_layout).setOnClickListener(view -> AppUtils.getInstance().shareClick(mContext, newsListItem.title, newsListItem.webUrl, newsListItem.gTrackUrl, true));

        rootView.findViewById(R.id.bookmark_layout).setOnClickListener(view -> {
            AppUtils.getInstance().bookmarkClick(getContext(), newsListItem, "", true);
            resetBookmark();
        });

        return rootView;
    }

    private void resetBookmark() {
        boolean isStoryBookmarked = false;
        if (newsListItem != null)
            isStoryBookmarked = SerializeData.getInstance(getActivity()).isBookmarkAvailable(AppUtils.getFilteredStoryId(newsListItem.storyId));

        if (ivBookmark != null) {
            if (isStoryBookmarked) {
                ivBookmark.setImageResource(R.drawable.ic_bookmark_black_24dp);
            } else {
                ivBookmark.setImageResource(R.drawable.ic_bookmark_border_24dp);
            }
        }
    }

    private void headerCheck() {
        try {
            ((NewsBriefActivity) getActivity()).onClickCalled();
//            if (bottomLayout2.getVisibility() == View.VISIBLE) {
//                bottomLayout2.setVisibility(View.GONE);
//            } else {
//                bottomLayout2.setVisibility(View.VISIBLE);
//            }

            if (bottomLayout2.getVisibility() == View.GONE) {
                bottomLayout2.animate()
                        .translationY(0).alpha(1.0f)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                super.onAnimationStart(animation);
                                bottomLayout2.setVisibility(View.VISIBLE);
                                bottomLayout2.setAlpha(0.0f);
                            }
                        });
            } else {
                bottomLayout2.animate()
                        .translationY(bottomLayout2.getHeight()).alpha(0.0f)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                bottomLayout2.setVisibility(View.GONE);
                            }
                        });
            }

        } catch (Exception e) {
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.post(this::setData);
    }

    @Override
    public void onResumeFragment() {
        BusProvider.getInstance().register(this);

        try {
            String souce = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

            Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), categoryInfo.gaScreen + "-" + newsListItem.gTrackUrl, souce, medium, campaign);
        } catch (Exception ignore) {
        }
    }

    @Override
    public void onPauseFragment() {
        BusProvider.getInstance().unregister(this);

        bottomLayout2.animate()
                .translationY(0).alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        bottomLayout2.setVisibility(View.GONE);
                    }
                });
    }

    String desc = "";

    private void setData() {
        if (TextUtils.isEmpty(date)) {
            dateTv.setVisibility(View.GONE);
        } else {
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM");
//            date = simpleDateFormat.format(new Date());
            dateTv.setText(date);
        }

        titleTextView.setText(AppUtils.getInstance().getHomeTitle(newsListItem.iitl_title, AppUtils.getInstance().fromHtml(newsListItem.titleNB), newsListItem.colorCode));
        if (newsListItem.bulletsPointNB != null && newsListItem.bulletsPointNB.length > 0) {
            desc = "";
            for (int i = 0; i < newsListItem.bulletsPointNB.length; i++) {
                desc = desc + newsListItem.bulletsPointNB[i];
            }
//            if (desc.length() > 300) {
////                desc = desc.substring(0, desc.lastIndexOf(" ", 300)) + "...";
//                bulletTextView.setText(AppUtils.getInstance().fromHtml(desc.substring(0, desc.lastIndexOf(" ", 300))));
//            } else {
//                bulletTextView.setText(AppUtils.getInstance().fromHtml(desc));
//            }
//            int i = countLineBreaks(bulletTextView, desc);
//            bulletTextView.setText(AppUtils.getInstance().fromHtml((desc.substring(0, i))));
            bulletTextView.setText(AppUtils.getInstance().fromHtml(desc));
        }

        storyCountTv.setText(mContext.getResources().getString(R.string.news_brief_story_count, newsListItem.listIndex, listSize));

//        storyShareIv.setOnClickListener(v -> BackgroundRequest.getStoryShareUrl(mContext, newsListItem.webUrl, new OnStoryShareUrlListener() {
//                    @Override
//                    public void onUrlFetch(boolean flag, String shareUrl) {
//                        if (flag) {
//                            AppUtils.getInstance().shareClick(mContext, newsListItem.title, shareUrl, newsListItem.gTrackUrl);
//                        } else {
//                            AppUtils.getInstance().shareClick(mContext, newsListItem.title, newsListItem.webUrl, newsListItem.gTrackUrl);
//                        }
//                    }
//                })
//        );

        ImageUtil.setImageBlur(getContext(), newsListItem.image, bottomReadMoreImageView, 0);

        rightArrowImageView.setOnClickListener(view -> {
            int rpos = selectedPosition + 1;
            if (rpos < adapter.getCount())
                viewpager.setCurrentItem(rpos);
        });

        leftArrowImageView.setOnClickListener(view -> {
            int lpos = selectedPosition - 1;
            if (lpos >= 0)
                viewpager.setCurrentItem(lpos);
        });

//        countLineBreaks(bulletTextView, desc);
    }


    int width = 0;
    int height = 0;
    int lineHeight = 0;
    int startPos = 0;
    int lineCount = 0;

    private int countLineBreaks(final TextView view, final String toMeasure) {
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/appfont.ttf");
        view.setTypeface(tf);

        final Paint paint = view.getPaint();
        final int endPos = toMeasure.length();

//        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        width = view.getWidth();
        height = view.getHeight();
        lineHeight = view.getLineHeight();

        int totalLine = height / lineHeight;

        while (startPos < endPos) {
            startPos += paint.breakText(toMeasure.substring(startPos, endPos), true, width, (float[]) null);
            lineCount++;

            if (totalLine <= lineCount)
                break;
        }

        bulletTextView.setText(AppUtils.getInstance().fromHtml(desc.substring(0, startPos)));
//            }
//        });

//        int totalLine = height / lineHeight;

//        while (startPos < endPos) {
//            startPos += paint.breakText(toMeasure.substring(startPos, endPos), true, width, (float[]) null);
//            lineCount++;
//
////            if (totalLine <= lineCount)
////                break;
//        }

        return startPos;
    }


    private void launchArticlePage(NewsListInfo newsListInfo, int position) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            mContext.startActivity(intent);
        } else {
//            newsListInfo.bulletsPoint = null;
            getActivity().startActivityForResult(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo, position), Constants.REQ_CODE_BOTTOM_NAV);
        }
    }

    @Subscribe
    public void responseFromFragmentClick(FragmentItemClickEventWrapper eventWrapper) {
//        launchArticlePage(newsListItem, eventWrapper.getPosition());
        headerCheck();
    }

    @Override
    public void onClick(View view, int position) {
//        launchArticlePage(newsListItem, position);
        headerCheck();
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<NewsPhotoInfo> mnList;

        private ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            mnList = new ArrayList<>();
        }

        void addData(List<NewsPhotoInfo> list) {
            mnList.clear();
            mnList.addAll(list);
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            NewsPhotoInfo newsPhotoInfo = mnList.get(position);
            if (TextUtils.isEmpty(newsPhotoInfo.title) && newsListItem != null)
                newsPhotoInfo.title = newsListItem.title;
            DivyaCommonPhotoFragment divyaCommonPhotoFragment = DivyaCommonPhotoFragment.newInstance(newsPhotoInfo, position, newsPhotoInfo.imageSize, categoryInfo.gaArticle, categoryInfo.gaEventLabel, categoryInfo.displayName, false, color);
            divyaCommonPhotoFragment.setListener(NewsBriefListSubFragment2.this);
            return divyaCommonPhotoFragment;
        }

        @Override
        public int getCount() {
            return mnList.size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }
    }

}
