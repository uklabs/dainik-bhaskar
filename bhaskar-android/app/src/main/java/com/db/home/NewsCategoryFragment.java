package com.db.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.LoginController;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.CatMenuMainFragment;
import com.db.divya_new.common.LoadMoreController;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.divya_new.common.TabController;
import com.db.listeners.FragmentLifecycle;
import com.db.listeners.OnCategoryClickListener;
import com.db.listeners.OnDBVideoClickListener;
import com.db.news.PrefferedCityNewsListFragment;
import com.bhaskar.util.Action;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.bhaskar.view.ui.MainActivity.REQUEST_CODE_CALL_FOR_FOOTER_TAB;


public class NewsCategoryFragment extends Fragment implements TabController.MenuTabController, TabController.SubMenuTabController {

    public static String child_id;
    public List<CategoryInfo> tabMenuList = new ArrayList<>();
    public ViewPagerAdapter adapter;
    public ViewPager pager;
    public View vFlashTopNews;
    public TextView tvHeadingText;
    public WebView webViewTicker;
    private CategoryInfo categoryInfo;
    private OnDBVideoClickListener onDBVideoClickListener;
    private OnCategoryClickListener onCategoryClickListener;
    private ViewPager.OnPageChangeListener onPageChangeListener;
    private TabLayout tabLayout;
    private int currentPos;
    private String displayName = "";
    private String prefixSectionName;

    public static NewsCategoryFragment getInstance(CategoryInfo categoryInfo, String prefixSectionName) {
        NewsCategoryFragment newsFragment = new NewsCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putString(Constants.KeyPair.KEY_PREFIX_SECTION_NAME, prefixSectionName);
        newsFragment.setArguments(bundle);
        return newsFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onDBVideoClickListener = (OnDBVideoClickListener) context;
        } catch (ClassCastException ignored) {
        }
        try {
            onCategoryClickListener = (OnCategoryClickListener) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            prefixSectionName = bundle.getString(Constants.KeyPair.KEY_PREFIX_SECTION_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        tabLayout = getActivity().findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setVisibility(View.VISIBLE);

        pager = view.findViewById(R.id.viewpager);
        vFlashTopNews = view.findViewById(R.id.current_news_layout);
        tvHeadingText = view.findViewById(R.id.marquee_tv);
        webViewTicker = view.findViewById(R.id.webViewTicker);
        webViewTicker.setOnLongClickListener(v -> true);
        webViewTicker.setLongClickable(false);
        webViewTicker.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webViewTicker.getSettings().setJavaScriptEnabled(true);

        if (!AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE, false)) {
            TypedValue tv = new TypedValue();
            int actionBarHeight = 0;
            if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) pager.getLayoutParams();
            params.setMargins(0, 0, 0, actionBarHeight);
            pager.setLayoutParams(params);
            pager.requestLayout();
        }

        onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                onPageSelect(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        };

        pager.addOnPageChangeListener(onPageChangeListener);

        if (categoryInfo != null) {
            displayName = categoryInfo.displayName;
            List<CategoryInfo> categoryInfoArrayList = new ArrayList<>();


            if (categoryInfo.parentId.equalsIgnoreCase("0")) {
                if (categoryInfo.subMenu.size() > 0)
                    categoryInfoArrayList.addAll(categoryInfo.subMenu);
                else
                    categoryInfoArrayList.add(categoryInfo);
                initTabLayout(categoryInfoArrayList);
                pager.post(() -> onPageChangeListener.onPageSelected(pager.getCurrentItem()));
            } else {
                CategoryInfo parentCategory = JsonParser.getInstance().getCategoryInfoBasedOnId(getContext(), categoryInfo.parentId);
                if (parentCategory.subMenu.size() > 0)
                    categoryInfoArrayList.addAll(parentCategory.subMenu);
                else
                    categoryInfoArrayList.add(parentCategory);
                initTabLayout(categoryInfoArrayList);
                pager.setCurrentItem(categoryInfoArrayList.indexOf(categoryInfo));

            }

        }

        return view;
    }

    private void onPageSelect(int position) {
        try {
            if (onDBVideoClickListener != null) {
                onDBVideoClickListener.onDbVideoClick(position);
            }

            if (tabMenuList != null && tabMenuList.size() > 0) {
                CategoryInfo cInfo = tabMenuList.get(position);

                if (cInfo != null) {

                    // Last tab info save
                    if (AppPreferences.getInstance(getActivity()).getBooleanValue(QuickPreferences.LastTabPrefs.TOGGLING_LAST_TAB_ACTIVE, false)) {
                        AppPreferences.getInstance(getActivity()).setStringValue(QuickPreferences.LAST_OPENED_CATEGORY, cInfo.id);
                    }

                    PagerAdapter newsPagerAdapter = pager.getAdapter();
                    if (newsPagerAdapter != null) {

                        String gaScreen = TextUtils.isEmpty(prefixSectionName) ? cInfo.gaScreen : prefixSectionName + "_" + cInfo.gaScreen;
                        String displayName = cInfo.displayName;
                        boolean trackingOn = true;

                        Object fragmentInstance = newsPagerAdapter.instantiateItem(pager, position);
                        if (fragmentInstance instanceof PrefferedCityNewsListFragment) {
                            String city = ((PrefferedCityNewsListFragment) fragmentInstance).getSelectedCity();
                            String citySelectionValue = ((PrefferedCityNewsListFragment) fragmentInstance).getCitySelction();
                            gaScreen = gaScreen + "-" + city + ((TextUtils.isEmpty(citySelectionValue)) ? "" : "_" + citySelectionValue);
                            displayName = displayName + ("/" + city);
                        } else if (fragmentInstance instanceof CatMenuMainFragment) {
                            trackingOn = false;
                        }

                        AppUtils.GLOBAL_DISPLAY_NAME = displayName;

                        if (trackingOn) {
                            String souce = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
                            String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                            String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                            Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), gaScreen, souce, medium, campaign);
                        }

                        String wisdomUrl = TrackingHost.getInstance(getContext()).getWisdomUrl(AppUrls.WISDOM_URL);
                        String domain = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
                        String url = domain + displayName;
                        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
                        String email = "";
                        String mobile = "";
                        if (profileInfo != null) {
                            email = profileInfo.email;
                            mobile = profileInfo.mobile;
                        }

                        Tracking.trackWisdomListingPage(getContext(), wisdomUrl, url, email, mobile, CommonConstants.BHASKAR_APP_ID);
                    }
                }
            }
        } catch (Exception ignored) {
        }

        checkForPauseResumeFragment(position);
    }

    private void checkForPauseResumeFragment(int position) {
        if (pager != null) {
            PagerAdapter newsPagerAdapter = pager.getAdapter();
            if (newsPagerAdapter != null) {
                try {
                    FragmentLifecycle fragmentToShow = (FragmentLifecycle) newsPagerAdapter.instantiateItem(pager, currentPos);
                    fragmentToShow.onPauseFragment();
                } catch (Exception ignored) {
                }
                try {
                    FragmentLifecycle lifecycle = (FragmentLifecycle) newsPagerAdapter.instantiateItem(pager, position);
                    lifecycle.onResumeFragment();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        currentPos = position;
    }

    private void initTabLayout(List<CategoryInfo> categoryInfoArrayList) {
        try {
            adapter = new ViewPagerAdapter(getFragmentManager(), categoryInfoArrayList);
            if (pager != null) {
                pager.setAdapter(adapter);
                tabLayout.setupWithViewPager(pager);
                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        setToTop();
                    }
                });
            }
        } catch (Exception ignored) {
        }
    }

    private void setToTop() {
        try {
            if (pager != null && pager.getAdapter() != null) {
                Object fragmentInstance = pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                if (fragmentInstance instanceof OnMoveTopListener) {
                    ((OnMoveTopListener) fragmentInstance).moveToTop();
                } else if (fragmentInstance instanceof PrefferedCityNewsListFragment) {
                    ((PrefferedCityNewsListFragment) fragmentInstance).moveToTop(true);
                }
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        TabController.getInstance().setSubMenuTabController(this);
        TabController.getInstance().setMenuTabController(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        TabController.getInstance().setMenuTabController(null);
    }

    @Override
    public void onMenuTabClick(String id, String childId, LoadMoreController controller) {
        if (!TextUtils.isEmpty(id)) {
            if (!TextUtils.isEmpty(childId)) {
                child_id = childId;
            } else {
                child_id = "";
            }
            Systr.println("onTitle Click catHomeVH onMenuTabClick");
            boolean isSuccess = goToTab(id, childId);
            if (isSuccess && controller != null) controller.onLoadFinish();
        }
    }

    @Override
    public void onMenuTabClick(String id, LoadMoreController controller) {
        if (!TextUtils.isEmpty(id)) {
            child_id = "";
            boolean isSuccess = goToTab(id, "");
            if (isSuccess && controller != null) controller.onLoadFinish();
        }
    }

    @Override
    public void onMenuTabClick(String id) {
        if (!TextUtils.isEmpty(id)) {
            child_id = "";
            goToTab(id, "");
        }
    }

    @Override
    public void onSubMenuTabClick(String parentId) {
        if (!TextUtils.isEmpty(parentId)) {
            goToTab(parentId, "");
        }
    }

    @Override
    public void onSubMenuTabClick(String childId, LoadMoreController controller) {
        if (!TextUtils.isEmpty(childId)) {
            goToTab(childId, "");
        }
    }

    private boolean goToTab(String id, String child_id) {
        if (pager != null && tabMenuList != null) {
            int index = 0;
            if (TextUtils.isEmpty(child_id)) {
                index = tabMenuList.indexOf(new CategoryInfo(id));
                if (index != -1) {
                    TabController.getInstance().setTabClicked(false);
                    pager.setCurrentItem(index);
                    return true;
                } else
                    return getCatIdAvailableInTabs(id);
            } else return getCatIdAvailableInTabs(id, child_id);
        }

        return false;
    }

    private boolean getCatIdAvailableInTabs(String parentId, String id) {
        child_id = id;
        int index = tabMenuList.indexOf(new CategoryInfo(id));
        if (index != -1) {
            TabController.getInstance().setTabClicked(false);
            Systr.println("onTitle Click getCatIdAvailableInTabs " + index);
            if (pager.getCurrentItem() == index) {
                if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                    TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(id);
                }
            } else
                pager.setCurrentItem(index);
            return true;
        } else {
            CategoryInfo info;
            info = JsonParser.getInstance().getSuperParentCategoryInfoById(getContext(), parentId, id);
            if (info != null) {
                TabController.getInstance().setTabClicked(false);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, info);
                intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, id);
                startActivityForResult(intent, REQUEST_CODE_CALL_FOR_FOOTER_TAB);
            }
        }
        return false;
    }

    private boolean getCatIdAvailableInTabs(String id) {
        CategoryInfo info;
        info = JsonParser.getInstance().getSuperParentCategoryInfoById(getContext(), id);

        if (info != null) {
            TabController.getInstance().setTabClicked(false);
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, info);
            intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, id);
            startActivityForResult(intent, REQUEST_CODE_CALL_FOR_FOOTER_TAB);
        }

        return false;
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private ViewPagerAdapter(FragmentManager manager, List<CategoryInfo> menuList) {
            super(manager);
            tabMenuList = menuList;
        }

        int getSpecificPosition(CategoryInfo categoryInfo) {
            if (tabMenuList != null && tabMenuList.size() > 0) {
                if (tabMenuList.contains(categoryInfo)) {
                    return tabMenuList.indexOf(categoryInfo);
                }
            }
            return -1;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment pagerFragment = null;
            if (tabMenuList.size() > 0) {
                CategoryInfo tabCategoryInfo = tabMenuList.get(position);
                if (Action.CategoryAction.CAT_ACTION_NEWS_BRIEF.equalsIgnoreCase(tabCategoryInfo.action) && onCategoryClickListener != null) {
                    onCategoryClickListener.onActionBarIconUpdate(Action.CategoryAction.CAT_ACTION_NEWS_BRIEF);
                }
                pagerFragment = ActivityUtil.getFragmentByCategoryForMenu(tabCategoryInfo, displayName, prefixSectionName);
            }

            return pagerFragment;
        }

        @Override
        public int getCount() {
            return tabMenuList != null ? tabMenuList.size() : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            try {
                return tabMenuList.get(position).menuName;
            } catch (Exception ignored) {
            }
            return "";
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }


}
