package com.db.home;

import android.content.Context;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.LoginController;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.db.data.models.CategoryInfo;
import com.db.database.DatabaseClient;
import com.db.database.TabInfo;
import com.db.divya_new.common.CatMenuMainFragment;
import com.db.divya_new.common.LoadMoreController;
import com.db.divya_new.common.OnMoveTopListener;
import com.db.divya_new.common.TabController;
import com.db.listeners.FragmentLifecycle;
import com.db.listeners.OnCategoryClickListener;
import com.db.listeners.OnDBVideoClickListener;
import com.db.news.PrefferedCityNewsListFragment;
import com.bhaskar.util.Action;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.bhaskar.util.CssConstants;
import com.db.util.JsonParser;
import com.db.util.OpenNewsLinkInApp;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class NewsFragment extends Fragment implements OnCategoryClickListener, TabController.MenuTabController {

    private List<TabInfo> tabMenuList = new ArrayList<>();

    public ViewPager pager;
    private View vFlashTopNews;
    private TextView tvHeadingText;
    private WebView webViewTicker;
    private TabLayout tabLayout;
    public ImageView ivBreaking;

    private ViewPagerAdapter adapter;

    private OnDBVideoClickListener onDBVideoClickListener;
    private OnCategoryClickListener onCategoryClickListener;
    private ViewPager.OnPageChangeListener onPageChangeListener;

    private int currentPos;
    private boolean showTicker;
    public static String child_id;

    private String prefixSectionName;

    public static NewsFragment getInstance(boolean showTicker, String prefixSectionName) {
        NewsFragment newsFragment = new NewsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("showTicker", showTicker);
        bundle.putString(Constants.KeyPair.KEY_PREFIX_SECTION_NAME, prefixSectionName);
        newsFragment.setArguments(bundle);
        return newsFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onDBVideoClickListener = (OnDBVideoClickListener) context;
        } catch (ClassCastException ignored) {
        }
        try {
            onCategoryClickListener = (OnCategoryClickListener) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            showTicker = bundle.getBoolean("showTicker");
            prefixSectionName = bundle.getString(Constants.KeyPair.KEY_PREFIX_SECTION_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        pager = view.findViewById(R.id.viewpager);
        vFlashTopNews = view.findViewById(R.id.current_news_layout);
        tvHeadingText = view.findViewById(R.id.marquee_tv);
        ivBreaking = view.findViewById(R.id.breaking_iv);
        webViewTicker = view.findViewById(R.id.webViewTicker);
        webViewTicker.setOnLongClickListener(v -> true);
        webViewTicker.setLongClickable(false);
        webViewTicker.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webViewTicker.getSettings().setJavaScriptEnabled(true);

        tabLayout = getActivity().findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setVisibility(View.VISIBLE);

        if (!AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE, false)) {
            TypedValue tv = new TypedValue();
            int actionBarHeight = 0;
            if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) pager.getLayoutParams();
            params.setMargins(0, 0, 0, actionBarHeight);
            pager.setLayoutParams(params);
            pager.requestLayout();
        }

        // Tab listing
        initTabLayout();

        onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                onPageSelect(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        };
        pager.addOnPageChangeListener(onPageChangeListener);

        pager.post(() -> onPageChangeListener.onPageSelected(pager.getCurrentItem()));

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (showTicker)
            getTickerData();
    }

    private void initTabLayout() {
        List<TabInfo> categoryInfoArrayList = JsonParser.getInstance().getTabList(getContext());
        try {
            adapter = new ViewPagerAdapter(getFragmentManager(), categoryInfoArrayList);
            if (pager != null) {
                pager.setAdapter(adapter);
                tabLayout.setupWithViewPager(pager);
                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        setToTop();
                    }
                });
            }
        } catch (Exception ignored) {
        }
    }

    private void setToTop() {
        try {
            if (pager != null && pager.getAdapter() != null) {
                Object fragmentInstance = pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                if (fragmentInstance instanceof OnMoveTopListener) {
                    ((OnMoveTopListener) fragmentInstance).moveToTop();
                } else if (fragmentInstance instanceof PrefferedCityNewsListFragment) {
                    ((PrefferedCityNewsListFragment) fragmentInstance).moveToTop(true);
                }
            }
        } catch (Exception ignored) {
        }
    }

    private void onPageSelect(int position) {
        try {
            if (onDBVideoClickListener != null) {
                onDBVideoClickListener.onDbVideoClick(position);
            }

            if (tabMenuList != null && tabMenuList.size() > 0) {
                TabInfo tabInfo = tabMenuList.get(position);
                CategoryInfo cInfo = DatabaseClient.getInstance(getContext()).getCategoryById(tabInfo.id);

                if (cInfo != null) {
                    // Change tab color
                    int themeColor = AppUtils.getThemeColor(getContext(), cInfo.color);
                    tabLayout.setSelectedTabIndicatorColor(themeColor);
                    tabLayout.setTabTextColors(getActivity().getResources().getColor(R.color.tab_deselect_text_color), themeColor);
                    AppUtils.getInstance().setStatusBarColor(getActivity(), themeColor);

                    // Last tab info save
                    if (AppPreferences.getInstance(getActivity()).getBooleanValue(QuickPreferences.LastTabPrefs.TOGGLING_LAST_TAB_ACTIVE, false)) {
                        AppPreferences.getInstance(getActivity()).setStringValue(QuickPreferences.LAST_OPENED_CATEGORY, cInfo.id);
                    }

                    PagerAdapter newsPagerAdapter = pager.getAdapter();
                    if (newsPagerAdapter != null) {

                        String gaScreen = TextUtils.isEmpty(prefixSectionName) ? cInfo.gaScreen : prefixSectionName + '_' + cInfo.gaScreen;
                        String displayName = cInfo.displayName;
                        boolean trackingOn = true;

                        Object fragmentInstance = newsPagerAdapter.instantiateItem(pager, position);
                        if (fragmentInstance instanceof PrefferedCityNewsListFragment) {
                            String city = ((PrefferedCityNewsListFragment) fragmentInstance).getSelectedCity();
                            String citySelectionValue = ((PrefferedCityNewsListFragment) fragmentInstance).getCitySelction();
                            gaScreen = gaScreen + "-" + city + ((TextUtils.isEmpty(citySelectionValue)) ? "" : "_" + citySelectionValue);
                            displayName = displayName + ("/" + city);
                        } else if (fragmentInstance instanceof CatMenuMainFragment) {
                            if (!TextUtils.isEmpty(child_id)) {
                                if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(child_id);
                                        }
                                    },300);
                                }
                            }

                            trackingOn = false;
                        }

                        AppUtils.GLOBAL_DISPLAY_NAME = displayName;

                        if (trackingOn) {
                            String souce = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
                            String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                            String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                            Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), gaScreen, souce, medium, campaign);
                        }

                        String wisdomUrl = TrackingHost.getInstance(getContext()).getWisdomUrl(AppUrls.WISDOM_URL);
                        String domain = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
                        String url = domain + displayName;
                        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
                        String email = "";
                        String mobile = "";
                        if (profileInfo != null) {
                            email = profileInfo.email;
                            mobile = profileInfo.mobile;
                        }

                        Tracking.trackWisdomListingPage(getContext(), wisdomUrl, url, email, mobile, CommonConstants.BHASKAR_APP_ID);
                    }
                }
            }
        } catch (Exception ignored) {
        }

        checkForPauseResumeFragment(position);
    }

    private void checkForPauseResumeFragment(int position) {
        if (pager != null) {
            PagerAdapter newsPagerAdapter = pager.getAdapter();
            if (newsPagerAdapter != null) {
                try {
                    FragmentLifecycle fragmentToShow = (FragmentLifecycle) newsPagerAdapter.instantiateItem(pager, currentPos);
                    fragmentToShow.onPauseFragment();
                } catch (Exception ignored) {
                }
                try {
                    FragmentLifecycle lifecycle = (FragmentLifecycle) newsPagerAdapter.instantiateItem(pager, position);
                    lifecycle.onResumeFragment();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        currentPos = position;
    }

    private void getTickerData() {
        String feedURL = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.NEWS_TICKER_URL, "");
        if(TextUtils.isEmpty(feedURL)){
            return;
        }
        Systr.println("Ticker feed Url : " + feedURL);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, feedURL, null, response -> {
            try {
                if (response != null) {
                    String fontColor = response.optString("font_color");
                    String bgColor = response.optString("bg_color");
                    JSONArray data = response.optJSONArray("data");

                    String bullet = " <span class=\"dot\"/></span>";
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("&nbsp; &nbsp; &nbsp;");
                    for (int i = 0; i < data.length(); i++) {
                        try {
                            stringBuilder.append("&nbsp; &nbsp;");
                            stringBuilder.append(String.format("%s  %s", bullet, data.optJSONObject(i).optString("content")));
                        } catch (Exception ignored) {
                        }
                    }
                    setLatestNews(stringBuilder.toString(), fontColor, bgColor, data.length() != 0);
                } else {
                    setLatestNews("", "", "", false);
                }

                fetchTickerOnTime();
            } catch (Exception ignored) {
                setLatestNews("", "", "", false);
            }
        }, error -> {
            Systr.println("Ticker Response Error : " + error);
            fetchTickerOnTime();
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(10000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void setLatestNews(final String text, String fontColor, String bgColor, boolean show) {

        webViewTicker.setVerticalScrollBarEnabled(false);
        webViewTicker.setHorizontalScrollBarEnabled(false);

        if (!TextUtils.isEmpty(text) && show) {

            vFlashTopNews.setVisibility(View.VISIBLE);
            tvHeadingText.setText(Html.fromHtml(text));
            webViewTicker.loadDataWithBaseURL("", CssConstants.getTickerContent(text), "text/html; charset=utf-8", null, "");
            tvHeadingText.setSelected(true);
            webViewTicker.setOnLongClickListener(v -> true);
            webViewTicker.setLongClickable(false);
            webViewTicker.setWebViewClient(new DbWebViewClient());
            if (Urls.IS_TESTING) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
            // Below line prevent vibration on Long click
            webViewTicker.setHapticFeedbackEnabled(false);
            // webView set cachMode When Network available or Not
            webViewTicker.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            webViewTicker.getSettings().setUserAgentString(CommonConstants.USER_AGENT);
            // webView method call with java script
            // app setting icon
            // Manage retry option when network not available  or page not load
            webViewTicker.setFocusable(false);
            webViewTicker.setFocusableInTouchMode(false);
            overideTickerImage(fontColor, bgColor);
        } else {
            vFlashTopNews.setVisibility(View.GONE);
        }
    }

    private void overideTickerImage(String fontColor, String bgColor) {
        try {
            if (!TextUtils.isEmpty(bgColor)) {
                ivBreaking.setBackgroundColor(AppUtils.getThemeColor(getContext(), bgColor));
            } else {
                ivBreaking.setBackgroundColor(Objects.requireNonNull(getContext()).getResources().getColor(R.color.latest_red));
            }
            ivBreaking.clearColorFilter();
            if (!TextUtils.isEmpty(fontColor)) {
                ivBreaking.setColorFilter(AppUtils.getThemeColor(getContext(), fontColor), android.graphics.PorterDuff.Mode.SRC_IN);
            } else {
                ivBreaking.setColorFilter(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);

            }
        } catch (Exception ignored) {
        }

    }

    private void fetchTickerOnTime() {
        if (isAdded()) {
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (isAdded()) {
                    if (showTicker)
                        getTickerData();
                }
            }, 90000);
        }
    }

    @Override
    public void onActionBarIconUpdate(String action) {
    }

    public boolean moveToSpecificTab(CategoryInfo categoryInfo) {
        if (pager != null && adapter != null) {
            int index = adapter.getSpecificPosition(categoryInfo);
            if (index >= 0) {
                pager.setCurrentItem(index);
                return true;
            }
        }
        return false;
    }

    public void moveToZeroIndex() {
        if (pager != null && adapter != null) {
            pager.setCurrentItem(0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        TabController.getInstance().setMenuTabController(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        TabController.getInstance().setMenuTabController(null);
    }

    @Override
    public void onMenuTabClick(String id, String childId, LoadMoreController controller) {
        if (!TextUtils.isEmpty(id)) {
            if (!TextUtils.isEmpty(childId)) {
                child_id = childId;
            } else {
                child_id = "";
            }
            boolean isSuccess = goToTab(id, childId);
            if (isSuccess && controller != null) controller.onLoadFinish();
        }
    }

    @Override
    public void onMenuTabClick(String id, LoadMoreController controller) {
        if (!TextUtils.isEmpty(id)) {
            child_id = "";
            boolean isSuccess = goToTab(id, "");
            if (isSuccess && controller != null) controller.onLoadFinish();
        }
    }

    @Override
    public void onMenuTabClick(String id) {
        if (!TextUtils.isEmpty(id)) {
            child_id = "";
            goToTab(id, "");
        }
    }

    private boolean goToTab(String id, String child_id) {
        if (pager != null && tabMenuList != null) {
            int index = 0;
            if (TextUtils.isEmpty(child_id)) {
                index = tabMenuList.indexOf(new TabInfo(id));
                if (index != -1) {
                    TabController.getInstance().setTabClicked(false);
                    pager.setCurrentItem(index);
                    return true;
                } else
                    return getCatIdAvailableInTabs(id);
            } else return getCatIdAvailableInTabs(id, child_id);
        }

        return false;
    }

    private boolean getCatIdAvailableInTabs(String parentId, String id) {
        child_id = id;
        int index = tabMenuList.indexOf(new TabInfo(parentId));
        if (index != -1) {
            pager.setCurrentItem(index);
            TabController.getInstance().setTabClicked(false);
            if (pager.getCurrentItem() == index) {
                if (TabController.getInstance().getSubMenuTabControllerInstance() != null) {
                    TabController.getInstance().getSubMenuTabControllerInstance().onSubMenuTabClick(id);
                }
            } else
                pager.setCurrentItem(index);
        } else {
            CategoryInfo info;
            info = JsonParser.getInstance().getSuperParentCategoryInfoById(getContext(), parentId, id);
            if (info != null) {
                TabController.getInstance().setTabClicked(false);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, info);
                intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, id);
                startActivity(intent);
            }
        }
        return false;
    }

    private boolean getCatIdAvailableInTabs(String id) {
        CategoryInfo info;
        info = JsonParser.getInstance().getSuperParentCategoryInfoById(getContext(), id);

        if (info != null) {
            TabController.getInstance().setTabClicked(false);
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, info);
            intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, id);
            startActivity(intent);
        }

        return false;
    }

    private class DbWebViewClient extends WebViewClient {

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            if (handler != null)
                handler.cancel();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(getContext(), url, Constants.KeyPair.KEY_NEWS_UPDATE, 0, "", null);
            openNewsDetailInApp.openLink();
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private ViewPagerAdapter(FragmentManager manager, List<TabInfo> menuList) {
            super(manager);
            tabMenuList = menuList;
        }

        int getSpecificPosition(CategoryInfo categoryInfo) {
            if (tabMenuList != null && tabMenuList.size() > 0) {
                TabInfo tabInfo = new TabInfo(categoryInfo.id);
                if (tabMenuList.contains(tabInfo)) {
                    return tabMenuList.indexOf(tabInfo);
                }
            }
            return -1;
        }

        @Override
        public Fragment getItem(int position) {

            Fragment pagerFragment = null;
            if (tabMenuList.size() > 0) {
                TabInfo tabInfo = tabMenuList.get(position);
                CategoryInfo tabCategoryInfo = DatabaseClient.getInstance(getContext()).getCategoryById(tabInfo.id);

                try {
                    if (Action.CategoryAction.CAT_ACTION_NEWS_BRIEF.equalsIgnoreCase(tabCategoryInfo.action)) {
                        onCategoryClickListener.onActionBarIconUpdate(Action.CategoryAction.CAT_ACTION_NEWS_BRIEF);
                    }
                } catch (Exception ignore) {
                }
                pagerFragment = ActivityUtil.getFragmentByCategory(tabCategoryInfo, "", prefixSectionName);
            }

            return pagerFragment;
        }

        @Override
        public int getCount() {
            return tabMenuList != null ? tabMenuList.size() : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            try {
                return tabMenuList.get(position).name;
            } catch (Exception ignored) {
            }

            return "";
        }

        @Override
        public int getItemPosition(@NotNull Object object) {
            return POSITION_NONE;
        }
    }
}
