package com.db.home;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.util.LoginController;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.BrandInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.data.models.PrimeBrandInfo;
import com.db.database.DatabaseClient;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.listeners.OnRecyclerTouchListener;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;
import com.db.util.SpacesItemDecoration;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.db.views.WrappingFragmentStatePagerAdapter;
import com.db.views.WrappingViewPager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrimeFragmentNew extends Fragment {

    private WrappingViewPager viewPager;
    private RecyclerView brandGridRecyclerView;
    private RelativeLayout pagerRl;
    private ArrayList<Object> mList;
    private List<BrandInfo> brandInfoArrayList;
    private ViewPagerAdapter adapter;
    private BrandsAdapter brandsAdapter;
    private ImageView rightArrow;
    private ImageView leftArrow;
    private boolean isMarginAllowed;
    private RelativeLayout dataLayout;
    private OnFeedFetchFromServerListener onFeedFetchFromServerListener;

    public static PrimeFragmentNew getInstance(boolean isMarginAllowed) {
        PrimeFragmentNew primeFragment = new PrimeFragmentNew();
        Bundle argument = new Bundle();
        argument.putBoolean("isMarginAllowed", isMarginAllowed);
        primeFragment.setArguments(argument);
        return primeFragment;
    }

    public void setListener(OnFeedFetchFromServerListener onFeedFetchFromServerListener) {
        this.onFeedFetchFromServerListener = onFeedFetchFromServerListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            isMarginAllowed = bundle.getBoolean("isMarginAllowed");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prime, container, false);

        pagerRl = view.findViewById(R.id.pager_rl);
        viewPager = view.findViewById(R.id.viewpager);
        brandGridRecyclerView = view.findViewById(R.id.recycler_view);
        rightArrow = view.findViewById(R.id.right_arrow);
        leftArrow = view.findViewById(R.id.left_arrow);
        dataLayout = view.findViewById(R.id.data_layout);

        mList = new ArrayList<>();

        // insert data
        parseJson();

        adapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if (isMarginAllowed) {
            TypedValue tv = new TypedValue();
            int actionBarHeight = 0;
            if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) dataLayout.getLayoutParams();
            params.setMargins(0, 0, 0, actionBarHeight);
            dataLayout.setLayoutParams(params);
            dataLayout.requestLayout();
        }

        rightArrow.setOnClickListener(v -> {
            int currentIndex = viewPager.getCurrentItem();
            if (currentIndex <= adapter.getCount()) {
                viewPager.setCurrentItem(currentIndex + 1, true);
            }
        });

        leftArrow.setOnClickListener(v -> {
            int currentIndex = viewPager.getCurrentItem();
            if (currentIndex > 0) {
                viewPager.setCurrentItem(currentIndex - 1, true);
            }
        });

        setUpBrandGrid();

        return view;
    }

    private void setUpBrandGrid() {
        SpacesItemDecoration spacesItemDecoration;
        GridLayoutManager mLayoutManager;

        if (brandInfoArrayList.size() > 9) {
            mLayoutManager = new GridLayoutManager(getContext(), 4);
            spacesItemDecoration = new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.brand_grid_spacing));
        } else {
            mLayoutManager = new GridLayoutManager(getContext(), 3);
            spacesItemDecoration = new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.brand_grid_spacing2));

            int margin = (int) AppUtils.getInstance().convertDpToPixel(8, getActivity());
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) brandGridRecyclerView.getLayoutParams();
            marginLayoutParams.setMargins(margin, margin, margin, margin);
            brandGridRecyclerView.setLayoutParams(marginLayoutParams);
        }
        brandGridRecyclerView.addItemDecoration(spacesItemDecoration);
        brandGridRecyclerView.setLayoutManager(mLayoutManager);

        brandsAdapter = new BrandsAdapter(getContext(), brandInfoArrayList);
        brandGridRecyclerView.setAdapter(brandsAdapter);

        brandGridRecyclerView.addOnItemTouchListener(new OnRecyclerTouchListener(getContext(), brandGridRecyclerView, new OnRecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                BrandInfo brandInfo;
                if (position >= 0 && position < brandInfoArrayList.size()) {
                    brandInfo = brandInfoArrayList.get(position);
                } else {
                    brandInfo = brandInfoArrayList.get(0);
                }
                if (brandInfo != null) {
                    CategoryInfo categoryInfo = JsonParser.getInstance().getCategoryInfoById(getContext(), brandInfo.id);
                    if (categoryInfo != null) {
                        onFeedFetchFromServerListener.onDataFetch(true, categoryInfo.id);
//                        ActivityUtil.openCategoryAccordingToAction(getActivity(), categoryInfo);
                        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.Event.TAB, AppFlyerConst.Key.BRAND, categoryInfo.gaEventLabel, AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_CAMPAIGN, ""));
                        CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.PRIME_SCREEN_CLICKS, CTConstant.PROP_NAME.CLICKON, categoryInfo.displayName, LoginController.getUserDataMap());

                        if (brandInfo.isNew.equalsIgnoreCase("1") && !DatabaseClient.getInstance(getContext()).isID_Clicked(brandInfo.id, DatabaseClient.DbConstant.BRAND_TYPE)) {
                            DatabaseClient.getInstance(getContext()).insertClickInfo(brandInfo.id, DatabaseClient.DbConstant.BRAND_TYPE);
                            if (brandsAdapter != null)
                                brandsAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    private void parseJson() {
        brandInfoArrayList = JsonParser.getInstance().getBrandFeedList(getContext());

        List<PrimeBrandInfo> primeInfoList = JsonParser.getInstance().getPrimeBrandFeedList(getActivity());
        for (PrimeBrandInfo info : primeInfoList) {
            switch (info.action) {
                case Constants.PrimeBrandAction.ACTION_IMAGE:
                case Constants.PrimeBrandAction.ACTION_GIF:
                case Constants.PrimeBrandAction.ACTION_WAP:
//                case Constants.PrimeBrandAction.ACTION_YOUTUBE:
                    mList.add(info);
                    break;

                case Constants.PrimeBrandAction.ACTION_NEWS_GALLERY:
                case Constants.PrimeBrandAction.ACTION_PHOTO_GALLERY:
                case Constants.PrimeBrandAction.ACTION_VIDEO_GALLERY:
                    makeJsonObjectFromServer(info);
                    break;
            }
        }

        if (mList.size() > 0) {
            pagerRl.setVisibility(View.VISIBLE);
        }
    }

    private void makeJsonObjectFromServer(final PrimeBrandInfo info) {
        String finalUrl = String.format("%s%s", Urls.APP_FEED_BASE_URL, info.feedUrl);
        if (info.action.equalsIgnoreCase(Constants.PrimeBrandAction.ACTION_VIDEO_GALLERY)) {
            finalUrl += "PG1/";
        }
        Systr.println("PRIME url : " + finalUrl);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, finalUrl, null, response -> {
            try {
                parseFeedData(info, response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Systr.println("PRIME Feed Error : " + error.getMessage()));

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void parseFeedData(PrimeBrandInfo info, JSONObject response) throws JSONException {
        JSONArray feed = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        switch (info.action) {
            case Constants.PrimeBrandAction.ACTION_NEWS_GALLERY:
                NewsListInfo[] newsInfoList = new Gson().fromJson(feed.toString(), NewsListInfo[].class);
                for (NewsListInfo newsInfo : newsInfoList) {
                    PrimeBrandInfo primeInfo = new PrimeBrandInfo(info, newsInfo);
                    mList.add(primeInfo);
                }
                break;

            case Constants.PrimeBrandAction.ACTION_PHOTO_GALLERY:
                PhotoListItemInfo[] photoList = new Gson().fromJson(feed.toString(), PhotoListItemInfo[].class);
                for (PhotoListItemInfo photoInfo : photoList) {
                    PrimeBrandInfo primeInfo = new PrimeBrandInfo(info, photoInfo);
                    mList.add(primeInfo);
                }
                break;

            case Constants.PrimeBrandAction.ACTION_VIDEO_GALLERY:
                VideoInfo[] videoInfoList = new Gson().fromJson(feed.toString(), VideoInfo[].class);
                for (VideoInfo videoInfo : videoInfoList) {
                    PrimeBrandInfo primeInfo = new PrimeBrandInfo(info, videoInfo);
                    mList.add(primeInfo);
                }
                break;
        }
        if (mList.size() > 0) {
            pagerRl.setVisibility(View.VISIBLE);
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    class ViewPagerAdapter extends WrappingFragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
//            if (mList.get(position) instanceof PrimeBrandInfo) {
//                PrimeBrandInfo prime = (PrimeBrandInfo) mList.get(position);
//                if (prime.action.equalsIgnoreCase(Constants.PrimeBrandAction.ACTION_YOUTUBE)) {
//                    return YouTubeVideoPlayFragment.newInstance(prime.feedUrl);
//                }
//            }

            PrimeSubFragmentNew primeSubFragment = PrimeSubFragmentNew.getInstance();
            primeSubFragment.setObject(mList.get(position));
            return primeSubFragment;
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }
    }
}
