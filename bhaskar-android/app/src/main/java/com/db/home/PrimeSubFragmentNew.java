package com.db.home;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.NewsListInfo;
import com.db.data.models.PhotoListItemInfo;
import com.db.data.models.PrimeBrandInfo;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.VideoPlayerActivity;
import com.db.dbvideoPersonalized.detail.VideoPlayerListFragment;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.news.WapV2Activity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrimeSubFragmentNew extends Fragment {

    private Object mObject;

    private ImageView videoIcon;
    private ImageView image;
    private WebView webView;
    private TextView titleTextView;
    private TextView videoTitleTextView;

    public static PrimeSubFragmentNew getInstance() {
        return new PrimeSubFragmentNew();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sub_prime, container, false);

        image = view.findViewById(R.id.news_image);
        videoIcon = view.findViewById(R.id.video_icon_image);
        titleTextView = view.findViewById(R.id.title_textview);
        videoTitleTextView = view.findViewById(R.id.video_title_textview);

        webView = view.findViewById(R.id.webview);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setDomStorageEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setHapticFeedbackEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.getSettings().setUserAgentString(CommonConstants.USER_AGENT);

        webView.setOnTouchListener(new View.OnTouchListener() {
            private float startX;
            private float startY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        startX = event.getX();
                        startY = event.getY();
                        break;
                    case MotionEvent.ACTION_UP:
                        float endX = event.getX();
                        float endY = event.getY();
                        if (AppUtils.getInstance().isAClick(startX, endX, startY, endY) && mObject instanceof PrimeBrandInfo) {
                            PrimeBrandInfo primeBrandInfo = (PrimeBrandInfo) mObject;
                            if (!TextUtils.isEmpty(primeBrandInfo.detailUrl)) {
                                Intent intent = new Intent(getContext(), WapV2Activity.class);
                                intent.putExtra(Constants.KeyPair.KEY_URL, primeBrandInfo.detailUrl);
                                intent.putExtra(Constants.KeyPair.KEY_TITLE, primeBrandInfo.detailUrl);
                                intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(primeBrandInfo.gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : primeBrandInfo.gaScreen) + primeBrandInfo.feedUrl);
                                startActivity(intent);
                            }
                        }
                        break;
                }
                return true;
            }
        });

        view.findViewById(R.id.root_layout).setOnClickListener(view1 -> click(mObject));

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parseFeedData(mObject);
    }

    private void click(Object object) {
        if (object instanceof PrimeBrandInfo) {
            PrimeBrandInfo primeBrandInfo = (PrimeBrandInfo) object;
            switch (primeBrandInfo.action) {
                case Constants.PrimeBrandAction.ACTION_IMAGE:
                case Constants.PrimeBrandAction.ACTION_GIF:
                case Constants.PrimeBrandAction.ACTION_WAP:
                    if (!TextUtils.isEmpty(primeBrandInfo.detailUrl)) {
                        Intent intent = new Intent(getContext(), WapV2Activity.class);
                        intent.putExtra(Constants.KeyPair.KEY_URL, primeBrandInfo.detailUrl);
                        intent.putExtra(Constants.KeyPair.KEY_TITLE, primeBrandInfo.detailUrl);
                        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, (!TextUtils.isEmpty(primeBrandInfo.gaScreen)) ? primeBrandInfo.gaScreen : AppFlyerConst.GAScreen.WEB_VIEW + "-" + primeBrandInfo.feedUrl);
                        startActivity(intent);
                    }
                    break;

//                case Constants.PrimeBrandAction.ACTION_YOUTUBE:
//                    break;

                case Constants.PrimeBrandAction.ACTION_NEWS_GALLERY:
                case Constants.PrimeBrandAction.ACTION_PHOTO_GALLERY:
                case Constants.PrimeBrandAction.ACTION_VIDEO_GALLERY:
                    clickObject(primeBrandInfo.object, primeBrandInfo.detailUrl, primeBrandInfo.feedUrl, primeBrandInfo.gaArticle, primeBrandInfo.gaScreen, primeBrandInfo.gaEventLabel);
                    break;
            }
            Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.PRIME_GALLERY, AppFlyerConst.Key.CLICK, primeBrandInfo.gaEventLabel, AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_CAMPAIGN, ""));

        } else {
            clickObject(object, "", "", "", "", "");
        }
    }

    private void clickObject(Object object, String detailFeedUrl, String feedUrl, String gaArticle, String gaScreen, String gaEventLabel) {
        if (object instanceof NewsListInfo) {
            NewsListInfo newsListInfo = (NewsListInfo) object;
            startActivityForResult(DivyaCommonArticleActivity.getIntent(getContext(), newsListInfo, gaArticle, gaScreen, "", gaEventLabel,""),Constants.REQ_CODE_BOTTOM_NAV);

        } else if (object instanceof PhotoListItemInfo) {
            PhotoListItemInfo photoListItemInfo = (PhotoListItemInfo) object;

            if (!TextUtils.isEmpty(detailFeedUrl)) {
                Intent photoGalleryIntent = new Intent(getContext(), PhotoGalleryActivity.class);
                photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, detailFeedUrl);

                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, photoListItemInfo.rssId);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, photoListItemInfo.gTrackUrl);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_TRACK_URL, photoListItemInfo.trackUrl);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
                startActivity(photoGalleryIntent);
            }

        } else if (object instanceof VideoInfo) {
            VideoInfo videoInfo = (VideoInfo) object;
//            Intent intent = DBVideoPlayerActivity2.getIntent(getContext(), videoInfo, "", AppFlyerConst.DBVideosSource.VIDEO_LIST, 1, detailFeedUrl, feedUrl, gaEventLabel, gaArticle, false, "", "");
//            startActivity(intent);

            ArrayList<VideoInfo> videoList = new ArrayList<>();
            videoList.add(videoInfo);

            Intent intent = VideoPlayerActivity.getIntent(getContext(), 0, 1, feedUrl, AppFlyerConst.DBVideosSource.VIDEO_LIST, gaEventLabel, gaArticle, "", "", VideoPlayerListFragment.TYPE_LIST, VideoPlayerListFragment.THEME_LIGHT);
            VideoPlayerActivity.addListData(videoList);
            startActivity(intent);
        }
    }

    private void parseFeedData(Object object) {
        if (object instanceof PrimeBrandInfo) {
            PrimeBrandInfo primeBrandInfo = (PrimeBrandInfo) object;
            switch (primeBrandInfo.action) {
                case Constants.PrimeBrandAction.ACTION_IMAGE:
                    ImageUtil.setImage(getContext(), primeBrandInfo.feedUrl, image, R.drawable.placeholder);
                    break;

                case Constants.PrimeBrandAction.ACTION_GIF:
                    ImageUtil.setImageGif(getContext(), primeBrandInfo.feedUrl, image, R.drawable.placeholder);
                    break;

                case Constants.PrimeBrandAction.ACTION_WAP:
                    webView.setVisibility(View.VISIBLE);
                    webView.loadUrl(primeBrandInfo.feedUrl);
                    break;

//                case Constants.PrimeBrandAction.ACTION_YOUTUBE:
//                    break;

                case Constants.PrimeBrandAction.ACTION_NEWS_GALLERY:
                case Constants.PrimeBrandAction.ACTION_PHOTO_GALLERY:
                case Constants.PrimeBrandAction.ACTION_VIDEO_GALLERY:
                    setLayoutObject(primeBrandInfo.object);
                    break;
            }
        } else {
            setLayoutObject(object);
        }
    }

    private void setLayoutObject(Object object) {
        if (object instanceof NewsListInfo) {
            NewsListInfo newsListInfo = (NewsListInfo) object;
            ImageUtil.setImage(getContext(), newsListInfo.image, image, R.drawable.water_mark_news_list);
            titleTextView.setVisibility(View.VISIBLE);
            titleTextView.setText(newsListInfo.title);

        } else if (object instanceof PhotoListItemInfo) {
            PhotoListItemInfo photoListItemInfo = (PhotoListItemInfo) object;
            ImageUtil.setImage(getContext(), photoListItemInfo.rssImage, image, R.drawable.water_mark_news_list);

        } else if (object instanceof VideoInfo) {
            VideoInfo videoInfo = (VideoInfo) object;
            ImageUtil.setImage(getContext(), videoInfo.image, image, R.drawable.water_mark_news_list);
            videoIcon.setVisibility(View.VISIBLE);
            videoTitleTextView.setVisibility(View.VISIBLE);
            videoTitleTextView.setText(videoInfo.title);
        }
    }

    public void setObject(Object o) {
        mObject = o;
    }
}
