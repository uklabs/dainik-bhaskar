package com.db.home;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.widget.TextView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.ThemeUtil;

public class RadioActivity extends BaseAppCompatActivity {

    private int color;
    private String title, gaScreen;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_radio);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            color = AppUtils.getThemeColor(RadioActivity.this, bundle.getString(Constants.KeyPair.KEY_COLOR));
            title = bundle.getString(Constants.KeyPair.KEY_TITLE);
            gaScreen=bundle.getString(Constants.KeyPair.KEY_GA_SCREEN);
        }

        if (color == 0) {
            color = AppUtils.getThemeColor(RadioActivity.this, "");
        }

        if(TextUtils.isEmpty(title)){
            title = getResources().getString(R.string.radio);
        }

        handleToolbar();

        Fragment fragment = RadioFragment.newInstance(gaScreen);
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, fragment);
            transaction.commitAllowingStateLoss();
        }


        String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(this, InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);
    }

    private void handleToolbar() {
        AppUtils.getInstance().setStatusBarColor(RadioActivity.this, color);

        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(color);
//        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(title);
    }


//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
//            btn.setImageDrawable(getResources().getDrawable(R.drawable.radio_play));
//        }
//
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        pauseStreamUrl();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        destroyPlayer();
//    }
//
//    void destroyPlayer() {
//        if (mediaPlayer != null) {
//            mediaPlayer.reset();
//            mediaPlayer.release();
//            mediaPlayer = null;
//        }
//    }
//
//    void pauseStreamUrl() {
//        if (mediaPlayer != null) {
//            btn.setImageDrawable(getResources().getDrawable(R.drawable.radio_play));
//            mediaPlayer.pause();
//            playPause = false;
//        }
//    }
//
//    void playStreamUrlFirstTime(Radio radio) {
//        if (mediaPlayer != null && !TextUtils.isEmpty(radio.img)) {
//            btn.setImageDrawable(getResources().getDrawable(R.drawable.radio_pause));
//            if (initialStage) {
//                new Player().execute(radio);
//            } else {
//                if (!mediaPlayer.isPlaying())
//                    mediaPlayer.start();
//            }
//            playPause = true;
//        }
//    }
//
//    void playStreamUrl(Radio radio) {
//        if (mediaPlayer != null && !TextUtils.isEmpty(radio.img)) {
//            btn.setImageDrawable(getResources().getDrawable(R.drawable.radio_pause));
//            destroyPlayer();
//
//            mediaPlayer = new MediaPlayer();
//            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//            new Player().execute(radio);
//            playPause = true;
//        }
//    }
//
//
//    void setStationInTop(Radio radio) {
//        playStreamUrl(radio);
//    }
//
//
//    class Player extends AsyncTask<Radio, Void, Boolean> {
//        Radio radio;
//
//        @Override
//        protected Boolean doInBackground(Radio... _radios) {
//            radio = _radios[0];
//            try {
//                mediaPlayer.setDataSource(radio.stn_url);
//                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                    @Override
//                    public void onCompletion(MediaPlayer mediaPlayer) {
//                        initialStage = true;
//                        playPause = false;
//                        btn.setImageDrawable(getResources().getDrawable(R.drawable.radio_play));
//                        mediaPlayer.stop();
//                        mediaPlayer.reset();
//                    }
//                });
//
//                mediaPlayer.prepare();
//                return true;
//            } catch (Exception error) {
//                pauseStreamUrl();
//            }
//            return false;
//        }
//
//        @Override
//        protected void onPostExecute(Boolean aBoolean) {
//            super.onPostExecute(aBoolean);
//            if (progressDialog.isShowing()) {
//                progressDialog.cancel();
//            }
//            mediaPlayer.start();
//            initialStage = false;
//            if (!aBoolean) {
//                AppUtils.getInstance().showCustomToast(RadioActivity.this, "Unable to play this station. Please play another station.");
//            } else {
//                if (!TextUtils.isEmpty(radio.img)) {
//                    ImageUtil.setImage(RadioActivity.this, radio.img, stationImg, R.drawable.water_mark_brand_category);
//                } else {
//                    stationImg.setImageDrawable(ContextCompat.getDrawable(RadioActivity.this, R.drawable.water_mark_brand_category));
//                }
//                stationTitle.setText(radio.stn_name);
//                bottomStationTitle.setText(radio.stn_name);
//            }
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog.setMessage("Buffering...");
//            progressDialog.show();
//        }
//    }


}
