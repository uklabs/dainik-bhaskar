package com.db.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.Radio;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.CustomHandler;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RadioFragment extends Fragment implements CustomHandler.AppReceiver {
    private static final String TAG = RadioActivity.class.getName();

    private ImageView playBtn, stationImg;
    private TextView stationTitle, bottomStationTitle;
    private ProgressDialog progressDialog;
    private List<Radio> radioItemList = null;
    private ProgressBar progressBar;
    private ImageView radioPlayPauseIV;

    private int selectedIndex = 0;
    private int selectedLangIndex = 0;
    private RadioRecyclerViewDataAdapter radioDataAdapter;
    private CustomHandler handler;
    private String gaScreen;
    private AnimationDrawable animationDrawable;
    private String[] langArr;
    private int[] langIDArr;

    public static RadioFragment newInstance(String gaScreen) {
        RadioFragment fragment = new RadioFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            gaScreen = getArguments().getString(Constants.KeyPair.KEY_GA_SCREEN);
        }
        handler = new CustomHandler(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_radio, container, false);

        progressBar = view.findViewById(R.id.progress_bar);
        playBtn = view.findViewById(R.id.playBtn);
        stationImg = view.findViewById(R.id.station_image);
        stationTitle = view.findViewById(R.id.station_txt);
        bottomStationTitle = view.findViewById(R.id.bottom_station_txt);
        radioPlayPauseIV = view.findViewById(R.id.radio_play_pause_iv);

        animationDrawable = (AnimationDrawable) ContextCompat.getDrawable(getContext(), R.drawable.ic_equalizer_white_36dp);
        radioPlayPauseIV.setImageDrawable(animationDrawable);

        radioItemList = new ArrayList<>();

        selectedIndex = AppPreferences.getInstance(getActivity()).getIntValue(Constants.Radio.RADIO_CHANNEL, 0);
        selectedLangIndex = AppPreferences.getInstance(getActivity()).getIntValue(Constants.Radio.RADIO_LANGUAGE, 0);
        Spinner language_spinner = view.findViewById(R.id.language_spinner);
        langArr = getResources().getStringArray(R.array.radio_lang_array);
        langIDArr = getResources().getIntArray(R.array.radio_lang_ids);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.radio_lang_array, R.layout.layout_radio_language_spinner);
        adapter.setDropDownViewResource(R.layout.layout_radio_dropdown);
        language_spinner.setAdapter(adapter);
        language_spinner.setSelection(selectedLangIndex);
        language_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedLangIndex = position;
                makeJsonObjectRequest(langIDArr[position]);
                Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.RADIO, "lang", langArr[position], "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        progressDialog = new ProgressDialog(new ContextThemeWrapper(getContext(), R.style.CustomFontDialog)) {
            @Override
            public void onBackPressed() {
//                pauseStreamUrl();
                if (progressDialog.isShowing()) {
                    progressDialog.cancel();
                }
            }
        };
        progressDialog.setCancelable(false);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (RadioService.isPlaying) {
                    Intent intent = new Intent(getContext(), RadioService.class);
                    intent.putExtra("stop", true);
                    intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
                    getActivity().startService(intent);

                } else if (radioItemList != null && radioItemList.size() > selectedIndex) {
                    Intent intent = new Intent(getContext(), RadioService.class);
                    intent.putExtra("data", radioItemList.get(selectedIndex));
                    intent.putExtra("handler", new Messenger(handler));
                    intent.putExtra("lang", langArr[selectedLangIndex]);
                    intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
                    getActivity().startService(intent);
                }
            }
        });

        RecyclerView radioRecyclerView = view.findViewById(R.id.card_view_recycler_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        radioRecyclerView.setLayoutManager(layoutManager);

        radioDataAdapter = new RadioRecyclerViewDataAdapter(getContext(), radioItemList, new RadioRecyclerViewDataAdapter.OnClickListener() {
            @Override
            public void onClick(Radio radio, int index) {
                AppPreferences.getInstance(getActivity()).setIntValue(Constants.Radio.RADIO_CHANNEL, index);
                AppPreferences.getInstance(getActivity()).setIntValue(Constants.Radio.RADIO_LANGUAGE, selectedLangIndex);
                selectedIndex = index;
                startRadioPlayService(radio);
            }
        });
        radioRecyclerView.setAdapter(radioDataAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (RadioService.radio != null && RadioService.isPlaying) {
            RadioService.messenger = new Messenger(handler);

            Message msg = new Message();
            msg.obj = RadioService.radio;
            msg.what = RadioService.status;

            onReceiveResult(msg);
        }
    }

    private void makeJsonObjectRequest(int lang_id) {
        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            String finalFeedURL = Urls.APP_FEED_BASE_URL + String.format(Urls.APP_RADIO_URL, lang_id);

            progressBar.setVisibility(View.VISIBLE);

            AppLogs.printDebugLogs(TAG + "RadioList URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalFeedURL,null,
                    response -> {
                        progressBar.setVisibility(View.GONE);
                        try {
                            if(progressDialog!=null && progressDialog.isShowing()){
                                progressDialog.cancel();
                            }
                            onResponseFromServer(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                AppLogs.printVolleyLogs(TAG + "RadioList", "Error: " + error.getMessage());
                Context context = getContext();
                progressBar.setVisibility(View.GONE);
                if (context != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
            Context context = getContext();
            if (context != null) {
                AppUtils.getInstance().showCustomToast(context, context.getString(R.string.no_network_error));
            }
        }
    }

    private void onResponseFromServer(JSONObject response) throws JSONException {
        JSONArray jsonArray = response.getJSONArray("feed");
        radioItemList.clear();
        for (int i = 0; i < jsonArray.length(); i++) {
            Radio radio = new Gson().fromJson(jsonArray.get(i).toString(), Radio.class);
            radioItemList.add(radio);
        }
        radioDataAdapter.notifyDataSetChanged();
        if (radioItemList.size() > 0)
            if (!RadioService.isPlaying) {
                if (radioItemList.size() > selectedIndex) {
                    startRadioPlayService(radioItemList.get(selectedIndex));
                } else {
                    AppPreferences.getInstance(getActivity()).setIntValue(Constants.Radio.RADIO_CHANNEL, 0);
                    selectedIndex = 0;
                    startRadioPlayService(radioItemList.get(selectedIndex));
                }
            }
    }

    private void startRadioPlayService(Radio radio) {
        if (getContext() != null) {
            Intent intent = new Intent(getContext(), RadioService.class);
            intent.putExtra("data", radio);
            intent.putExtra("handler", new Messenger(handler));
            intent.putExtra("lang", langArr[selectedLangIndex]);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
            getActivity().startService(intent);

            Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.RADIO, "station", radio.id + "_" + radio.stn_name, "");
        }
    }

    @Override
    public void onReceiveResult(Message message) {
        if (getActivity() == null || !isAdded()) {
            return;
        }

        Object obj = message.obj;

        if (obj instanceof Radio) {
            Radio radio = (Radio) obj;
            if (!TextUtils.isEmpty(radio.img)) {
                ImageUtil.setImage(getContext(), radio.img, stationImg, R.drawable.water_mark_brand_category);
            } else {
                stationImg.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.water_mark_brand_category));
            }

            stationTitle.setText(radio.stn_name);
            bottomStationTitle.setText(radio.stn_name);
        }

        switch (message.what) {
            case 0:
                playBtn.setImageDrawable(getResources().getDrawable(R.drawable.radio_play));
//                radioPlayPauseIV.setImageResource(R.drawable.radio_music);
                animationDrawable.stop();
                try {
                    if (!progressDialog.isShowing()) {
                        progressDialog.setMessage("Please wait...");
                        progressDialog.show();
                    }
                } catch (Exception e) {
                }
                break;

            case 1:
                try {
                    if (progressDialog.isShowing())
                        progressDialog.cancel();
                } catch (Exception e) {
                }
//                radioPlayPauseIV.setImageResource(R.drawable.radio_music);
                animationDrawable.stop();
                playBtn.setImageDrawable(getResources().getDrawable(R.drawable.radio_play));
                break;

            case 2:
                try {
                    if (progressDialog.isShowing())
                        progressDialog.cancel();
                } catch (Exception e) {
                }
//                ImageUtil.setImageGif(getContext(), radioPlayPauseIV, R.drawable.radio_anim);
                animationDrawable.start();
                playBtn.setImageDrawable(getResources().getDrawable(R.drawable.radio_pause));
                break;
        }
    }
}
