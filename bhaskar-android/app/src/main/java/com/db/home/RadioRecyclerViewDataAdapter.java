package com.db.home;

import android.content.Context;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.Radio;
import com.db.util.ImageUtil;

import java.util.List;

public class RadioRecyclerViewDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface OnClickListener {
        void onClick(Radio radio, int index);
    }

    private Context mContext;
    private List<Radio> radioItemList;
    private OnClickListener listener;

    public RadioRecyclerViewDataAdapter(Context context, List<Radio> radioItemList, OnClickListener listener) {
        this.mContext = context;
        this.radioItemList = radioItemList;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new RadioRecyclerViewItemHolder(layoutInflater.inflate(R.layout.radio_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder hol, int position) {
        if (hol instanceof RadioRecyclerViewItemHolder) {
            RadioRecyclerViewItemHolder holder = (RadioRecyclerViewItemHolder) hol;

            if (radioItemList != null) {
                Radio radioItem = radioItemList.get(position);
                if (radioItem != null) {
                    holder.radioTitleText.setText(radioItem.stn_name);

                    if (!TextUtils.isEmpty(radioItem.img)) {
                        ImageUtil.setImage(mContext, radioItem.img, holder.radioImageView, R.drawable.water_mark_brand_category);
                    } else {
                        holder.radioImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.water_mark_brand_category));
                    }

                    try {
                        int color = Color.parseColor(radioItem.color);
                        holder.colorStirp.setBackgroundColor(color);
                    } catch (Exception e) {
                    }

                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            setStationInTop(radioItem);
                            if (listener != null) {
                                listener.onClick(radioItem, position);
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (radioItemList != null) {
            return radioItemList.size();
        }

        return 0;
    }

    class RadioRecyclerViewItemHolder extends RecyclerView.ViewHolder {

        private TextView radioTitleText;
        private ImageView radioImageView;
        private LinearLayout colorStirp;

        RadioRecyclerViewItemHolder(View itemView) {
            super(itemView);

            radioTitleText = itemView.findViewById(R.id.card_view_image_title);
            radioImageView = itemView.findViewById(R.id.card_view_image);
            colorStirp = itemView.findViewById(R.id.item_color);
        }
    }
}