package com.db.home;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.bhaskar.util.LoginController;
import com.bhaskar.R;
import com.db.audioBulletin.mediaPlayer.MediaPlayerService;
import com.db.data.models.Radio;
import com.db.util.Constants;
import com.db.util.Notificationutil;

import java.io.IOException;

public class RadioService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {

    public static boolean isPlaying = false;
    private MediaPlayer player;
    public static Radio radio;
    public static int status;
    private String gaScreen;
    private int NOTIFICATION_ID = 64803;
    private NotificationManager notificationManager;

    private final IBinder radioBind = new RadioBinder();
    public static Messenger messenger;
    private String lang;
//    private static RadioService radioService;

    public class RadioBinder extends Binder {
        RadioService getService() {
            return RadioService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        isPlaying = false;
        player = new MediaPlayer();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isPlaying = false;
//        radioService = this;
        if (intent != null) {
            gaScreen = intent.getStringExtra(Constants.KeyPair.KEY_GA_SCREEN);
            if (intent.getBooleanExtra("stop", false)) {
                try {
                    CleverTapDB.getInstance(this).cleverTapTrackEvent(this, CTConstant.EVENT_NAME.RADIO, CTConstant.PROP_NAME.STATUS, CTConstant.PROP_VALUE.ENDED, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.STATION_NAME, radio.stn_name));
                } catch (Exception ignore) {
                }
                stopSelf();
            } else if (intent.getBooleanExtra("play", false)) {
                play();
            } else if (intent.getBooleanExtra("pause", false)) {
                pause();
            } else {
                messenger = intent.getParcelableExtra("handler");
                radio = (Radio) intent.getSerializableExtra("data");
                lang = intent.getStringExtra("lang");
                status = 0;
                setMessage(status, radio);
                initMusicPlayer();
                playRadio();
                try {
                    CleverTapDB.getInstance(this).cleverTapTrackEvent(this, CTConstant.EVENT_NAME.RADIO, CTConstant.PROP_NAME.STATUS, CTConstant.PROP_VALUE.STARTED, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.STATION_NAME, radio.stn_name));
                } catch (Exception ignore) {
                }
            }
        }

        return START_STICKY;
    }

    private void setMessage(int status, Object value) {
        Message msg = new Message();
        msg.obj = value;
        msg.what = status;

        if (messenger != null) {
            try {
                messenger.send(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void initMusicPlayer() {
        if (player == null)
            player = new MediaPlayer();

        player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
    }

    public void playRadio() {
        if (player == null) {
            initMusicPlayer();
        }
        player.reset();

        try {
            player.setDataSource(getApplicationContext(), Uri.parse(radio.stn_url));
        } catch (IOException io) {
        } catch (Exception e) {
        }

        player.prepareAsync();
        isPlaying = true;
    }

    public void stopMusicPlayer() {
        isPlaying = false;
        status = 1;
        setMessage(status, radio);

        if (player == null)
            return;

        player.stop();
        player.release();
        player = null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopMusicPlayer();
        return false;
    }

    private void pause() {
        isPlaying = false;
        status = 1;
        setMessage(status, radio);
        stopMusicPlayer();
//        updateNotification(true);
    }

    private void play() {
        if (radio != null) {
            status = 0;
            setMessage(status, radio);
            initMusicPlayer();
            playRadio();
//            updateNotification(false);

        } else {
            stopSelf();
        }
    }

    @Override
    public void onDestroy() {
        isPlaying = false;
        status = 1;
        setMessage(status, radio);
        radio = null;

        try {
            stopMusicPlayer();
            stopForeground(true);
            if (notificationManager != null)
                notificationManager.cancel(NOTIFICATION_ID);
        } catch (Exception e) {
        }

        messenger = null;
        super.onDestroy();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
        isPlaying = true;
        status = 2;
        setMessage(status, radio);

        notifySong(this, this, radio);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    Notification.Builder notificationBuilder;
    RemoteViews notificationView;

    public void notifySong(Context context, final Service service, Radio song) {
        notificationBuilder = new Notification.Builder(context);

        Intent notifyIntent = new Intent(context, RadioActivity.class);
        notifyIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        notifyIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationView = new RemoteViews(context.getPackageName(), R.layout.notification_radio);

        notificationView.setTextViewText(R.id.notification_text_title, song.stn_name);
        notificationView.setTextViewText(R.id.notification_subtext_title, lang);

//        Intent buttonPlayIntent = new Intent(this, NotificationPlayButtonHandler.class);
//        buttonPlayIntent.putExtra("action", "radioPause");
//        PendingIntent buttonPlayPendingIntent = PendingIntent.getBroadcast(context, 0, buttonPlayIntent, 0);
//        notificationView.setOnClickPendingIntent(R.id.notification_button_play, buttonPlayPendingIntent);

        Intent buttonStopIntent = new Intent(this, NotificationStopButtonHandler.class);
        buttonStopIntent.putExtra("action", "radioStop");
        PendingIntent buttonStopPendingIntent = PendingIntent.getBroadcast(context, 0, buttonStopIntent, 0);
        notificationView.setOnClickPendingIntent(R.id.notification_cross_button, buttonStopPendingIntent);

        // Finally... Actually creating the Notification
        notificationBuilder.setContentIntent(pendingIntent)
                .setSmallIcon(Notificationutil.getAppIcon())
                .setTicker("FM : " + song.stn_name)
                .setOngoing(true)
                .setContentTitle(song.stn_name)
                .setContent(notificationView);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(context.getString(R.string.app_name));
        }

        Notification notification = notificationBuilder.build();

        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_NONE;
            String name = context.getString(R.string.app_name);
            String id = context.getString(R.string.app_name);
            NotificationChannel mChannel = new NotificationChannel(id, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        service.startForeground(NOTIFICATION_ID, notification);
    }

    public void updateNotification(boolean isPaused) {
        if ((notificationView == null) || (notificationBuilder == null))
            return;

        int iconID = ((isPaused) ? R.drawable.ic_play_circle_filled_red_96dp : R.drawable.ic_pause_circle_filled_red_96dp);

        notificationView.setImageViewResource(R.id.notification_button_play, iconID);
        notificationBuilder.setContent(notificationView);

        if (isPaused) {
//            radioService.stopForeground(false);
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        } else {
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        }
    }

    public static class NotificationPlayButtonHandler extends BroadcastReceiver {

        public NotificationPlayButtonHandler() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
//            if (isPlaying) {
//                Intent intent1 = new Intent(context, RadioService.class);
//                intent1.putExtra("pause", true);
//                context.startService(intent1);
//            } else {
//                Intent intent1 = new Intent(context, RadioService.class);
//                intent1.putExtra("play", true);
//                context.startService(intent1);
//            }

//            if (radioService != null) {
//                if (isPlaying) {
//                    radioService.pause();
//                } else {
//                    radioService.play();
//                }
//            }
        }
    }

    public static class NotificationStopButtonHandler extends BroadcastReceiver {

        public NotificationStopButtonHandler() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Intent intent1 = new Intent(context, RadioService.class);
            intent1.putExtra("stop", true);
            context.startService(intent1);
//            if (radioService != null)
//                radioService.stopSelf();
        }
    }

    public static void stopTrack(@NonNull Context context) {
        Intent intent = new Intent(context, RadioService.class);
        intent.putExtra("stop",true);
        context.startService(intent);
    }
}
