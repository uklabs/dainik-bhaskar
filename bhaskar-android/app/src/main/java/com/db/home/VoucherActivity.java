package com.db.home;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CouponRaniVoucherInfo;
import com.db.listeners.OnDBVideoClickListener;
import com.db.news.WebViewActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class VoucherActivity extends AppCompatActivity implements OnDBVideoClickListener {
    public boolean isLoading;
    boolean couponCopied = false;
    private ProgressBar progressBar;
    private List<CouponRaniVoucherInfo> couponRaniVoucherInfoList = null;
    private RecyclerView voucherRecyclerView;
    private VoucherAdapter voucherAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String finalUrl;
    private int LOAD_MORE_COUNT = 1;
    private int visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
    private int lastVisibleItem, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(ThemeUtil.getCurrentTheme(this));

        setContentView(R.layout.activity_voucher);

        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_appbar_left_arrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setTitle(R.string.voucher_title);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(VoucherActivity.this, R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(VoucherActivity.this, R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(VoucherActivity.this, R.attr.toolbarBackgroundPrimary));

        voucherRecyclerView = findViewById(R.id.recycler_view);
        voucherRecyclerView.setVisibility(View.GONE);
        voucherRecyclerView.setLayoutManager(new LinearLayoutManager(VoucherActivity.this));

        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(this, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        progressBar.setVisibility(View.VISIBLE);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                LOAD_MORE_COUNT = 1;
                finalUrl = String.format(Urls.COUPON_RANI_VOUCHER_APP_FEED_URL, LOAD_MORE_COUNT);
                couponRaniJsonRequest(true);
            }
        });
        initLoadMore();

        finalUrl = String.format(Urls.COUPON_RANI_VOUCHER_APP_FEED_URL, LOAD_MORE_COUNT);
        couponRaniJsonRequest(true);

        // Tracking
        String source = AppPreferences.getInstance(VoucherActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(VoucherActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(VoucherActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(VoucherActivity.this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GAScreen.APPWALL_COUPON_RANI, source, medium, campaign);

        Systr.println("GA Screen : " + AppFlyerConst.GAScreen.APPWALL_COUPON_RANI + ((campaign.equalsIgnoreCase("direct")) ? "" : campaign));
    }

    private void initLoadMore() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) voucherRecyclerView.getLayoutManager();
        voucherRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        LOAD_MORE_COUNT++;
                        onLoadMore();
                    }
                }
            }
        });

    }

    private void onLoadMore() {
        isLoading = true;
        voucherRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                voucherAdapter.addItem();

            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finalUrl = String.format(Urls.COUPON_RANI_VOUCHER_APP_FEED_URL, LOAD_MORE_COUNT);
                couponRaniJsonRequest(false);
            }
        }, 1000);
    }

    private void couponRaniJsonRequest(final boolean isClearData) {
        AppLogs.printErrorLogs("Feed URL", "== " + finalUrl);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                AppUtils.getInstance().updateApiUrl(finalUrl), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response != null) {
                                AppLogs.printErrorLogs("Feed Response", "== " + response.toString());
                                parseFeedList(response, isClearData);
                            }
                        } catch (Exception e) {
                            //e.printStackTrace();
                        }

                        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d("NewsListFragment", "Error: " + error.getMessage());
                progressBar.setVisibility(View.GONE);

                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                if (error instanceof NoConnectionError || error instanceof NetworkError) {
                    AppUtils.getInstance().showCustomToast(VoucherActivity.this, getResources().getString(R.string.no_network_error));
                } else {
                    AppUtils.getInstance().showCustomToast(VoucherActivity.this, getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(VoucherActivity.this).addToRequestQueue(jsonObjReq);
    }

    private void parseFeedList(JSONObject response, boolean isClearData) {

        if (couponRaniVoucherInfoList == null) {
            couponRaniVoucherInfoList = new ArrayList<>();
        }

        if (isLoading && LOAD_MORE_COUNT > 0) {
            if (voucherAdapter != null) {
                voucherAdapter.removeItem();
            }
        }

        progressBar.setVisibility(View.GONE);
        voucherRecyclerView.setVisibility(View.VISIBLE);

        JSONObject couponsJsonObject = response.optJSONObject("coupons");
        JSONArray couponJsonArray = couponsJsonObject.optJSONArray("coupon");

        Gson gson = new Gson();

        if (couponJsonArray.length() > 0) {
            if (isClearData) {
                couponRaniVoucherInfoList.clear();
            }

            for (int i = 0; i < couponJsonArray.length(); i++) {
                CouponRaniVoucherInfo couponRaniVoucherInfo = gson.fromJson(couponJsonArray.optJSONObject(i).toString(), CouponRaniVoucherInfo.class);
                couponRaniVoucherInfoList.add(couponRaniVoucherInfo);
            }

            if (voucherAdapter == null) {
                voucherAdapter = new VoucherAdapter(VoucherActivity.this, couponRaniVoucherInfoList, VoucherActivity.this);
                voucherRecyclerView.setAdapter(voucherAdapter);
            } else {
                voucherAdapter.setData(couponRaniVoucherInfoList);
            }
        }

        if (couponJsonArray.length() <= 0) {
            if (LOAD_MORE_COUNT > 0) {
                LOAD_MORE_COUNT--;
            }
        }

        visibleThreshold = couponJsonArray.length();
        progressBar.setVisibility(View.GONE);
        isLoading = false;
    }

    @Override
    public void onDbVideoClick(int position) {
        copyCouponCodePopup(voucherAdapter.getItem(position));
    }

    public void copyCouponCodePopup(final CouponRaniVoucherInfo couponRaniVoucherInfo) {
        couponCopied = false;
        final Dialog copyCouponCodeDialog = new Dialog(VoucherActivity.this);
        copyCouponCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        copyCouponCodeDialog.setContentView(R.layout.voucher_code_copy_popup_layout);
        copyCouponCodeDialog.setCancelable(true);
        copyCouponCodeDialog.show();

        final TextView copyCodeTv = copyCouponCodeDialog.findViewById(R.id.copy_code_tv);
        final TextView couponCodeTv = copyCouponCodeDialog.findViewById(R.id.coupon_code_tv);
        TextView voucherTitleTv = copyCouponCodeDialog.findViewById(R.id.voucher_title_tv);
        TextView voucherDescTv = copyCouponCodeDialog.findViewById(R.id.voucher_desc_tv);
        TextView voucherOpenNoteTv = copyCouponCodeDialog.findViewById(R.id.voucher_open_note_tv);

        String name;
        if (couponRaniVoucherInfo.storeName.contains("Coupons")) {
            name = couponRaniVoucherInfo.storeName.substring(0, couponRaniVoucherInfo.storeName.indexOf("Coupons"));
        } else {
            name = couponRaniVoucherInfo.storeName;
        }

        voucherOpenNoteTv.setText(AppUtils.getInstance().fromHtml(getString(R.string.voucher_note, name)));
        couponCodeTv.setText(couponRaniVoucherInfo.code);
        voucherTitleTv.setText(couponRaniVoucherInfo.name);
        voucherDescTv.setText(couponRaniVoucherInfo.description.replace(". ", ".\n -"));

        copyCodeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!couponCopied) {
                    couponCopied = true;
                    String getString = couponCodeTv.getText().toString();
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Dainik Bhaskar", getString);
                    clipboard.setPrimaryClip(clip);
                    AppUtils.getInstance().showCustomToast(VoucherActivity.this, "Coupon Code Copied");
                    copyCodeTv.setText(R.string.voucher_copied);
                }
            }
        });

        voucherOpenNoteTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (couponCopied) {
                    copyCouponCodeDialog.dismiss();
                    couponCopied = false;
                    Intent voucherIntent = new Intent(VoucherActivity.this, WebViewActivity.class);
                    voucherIntent.putExtra(Constants.KeyPair.KEY_URL, couponRaniVoucherInfo.destinationURL);
                    voucherIntent.putExtra(Constants.KeyPair.KEY_TITLE, couponRaniVoucherInfo.storeName);
                    voucherIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAScreen.APPWALL_COUPON_RANI + "-" + couponRaniVoucherInfo.storeName);
                    startActivity(voucherIntent);

                } else {
                    AppUtils.getInstance().showCustomToast(VoucherActivity.this, "Please copy coupon first !");
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
