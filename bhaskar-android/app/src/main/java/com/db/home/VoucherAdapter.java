package com.db.home;

import android.content.Context;
import android.graphics.PorterDuff;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CouponRaniVoucherInfo;
import com.db.listeners.OnDBVideoClickListener;
import com.db.util.AppUtils;
import com.db.util.ImageUtil;

import java.util.ArrayList;
import java.util.List;

class VoucherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int MAIN_LIST = 1;
    private final int VIEW_TYPE_LOADING = 2;

    private OnDBVideoClickListener onDBVideoClickListener;
    private Context mContext;
    private List<CouponRaniVoucherInfo> couponRaniVoucherInfoArrayList = new ArrayList<>();

    VoucherAdapter(Context contexts, List<CouponRaniVoucherInfo> newsFeed, OnDBVideoClickListener onDBVideoClickListener) {
        this.mContext = contexts;
        this.couponRaniVoucherInfoArrayList.clear();
        this.couponRaniVoucherInfoArrayList.addAll(newsFeed);
        this.onDBVideoClickListener = onDBVideoClickListener;
    }

    public void setData(List<CouponRaniVoucherInfo> newsFeed) {
        if (newsFeed != null) {
            couponRaniVoucherInfoArrayList.clear();
            couponRaniVoucherInfoArrayList.addAll(newsFeed);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case MAIN_LIST:
                return new ViewHolder(inflater.inflate(R.layout.coupon_rani_voucher_list_item, parent, false));
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item, parent, false));
            default:
                break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case MAIN_LIST:
                final ViewHolder mainViewHolder = (ViewHolder) holder;
                final CouponRaniVoucherInfo couponRaniVoucherInfo = couponRaniVoucherInfoArrayList.get(mainViewHolder.getAdapterPosition());
                ImageUtil.setImage(mContext, couponRaniVoucherInfo.imageURL, mainViewHolder.voucherIv, R.drawable.water_mark_news_list);

                mainViewHolder.voucherPriceTv.setText(mContext.getString(R.string.voucher_price, mContext.getString(R.string.rupees_symbol), couponRaniVoucherInfo.value));
                mainViewHolder.voucherDescriptionTv.setText(couponRaniVoucherInfo.name);

                mainViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onDBVideoClickListener.onDbVideoClick(mainViewHolder.getAdapterPosition());
                    }
                });
                return;

            default:
                break;
        }
    }

    public CouponRaniVoucherInfo getItem(int position) {
        return couponRaniVoucherInfoArrayList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (couponRaniVoucherInfoArrayList.get(position) == null)
            return VIEW_TYPE_LOADING;
        else
            return MAIN_LIST;
    }

    @Override
    public int getItemCount() {
        return couponRaniVoucherInfoArrayList == null ? 0 : couponRaniVoucherInfoArrayList.size();
    }

    public void addItem() {
        couponRaniVoucherInfoArrayList.add(null);
        notifyDataSetChanged();
    }

    public void removeItem() {
        if (couponRaniVoucherInfoArrayList.size() > 0 && couponRaniVoucherInfoArrayList.get(couponRaniVoucherInfoArrayList.size() - 1) == null) {
            couponRaniVoucherInfoArrayList.remove(couponRaniVoucherInfoArrayList.size() - 1);
        }
        notifyDataSetChanged();
    }


    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView voucherPriceTv, voucherDescriptionTv, voucherExpiryDateTv;
        ImageView voucherIv;

        private ViewHolder(View itemView) {
            super(itemView);
            voucherPriceTv = (TextView) itemView.findViewById(R.id.voucher_price_tv);
            voucherDescriptionTv = (TextView) itemView.findViewById(R.id.voucher_description_tv);
            voucherExpiryDateTv = (TextView) itemView.findViewById(R.id.voucher_expiry_date_tv);
            voucherIv = (ImageView) itemView.findViewById(R.id.voucher_iv);
            itemView.setTag(itemView);
        }

    }

    // View Holders
    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        private LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);

        }
    }
}
