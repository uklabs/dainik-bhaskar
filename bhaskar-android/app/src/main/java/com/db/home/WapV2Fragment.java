package com.db.home;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;

import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.WapRedirectionUrls;
import com.db.listeners.FragmentLifecycle;
import com.bhaskar.util.LoginController;
import com.db.news.WapV2Activity;
import com.db.news.WebFullScreenActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.OpenNewsLinkInApp;
import com.db.util.OpenNewsLinkInAppV3;
import com.db.util.QuickPreferences;
import com.db.views.NestedWebView;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */

public class WapV2Fragment extends Fragment implements FragmentLifecycle {

    private static final int REQUEST_CODE_DOWNLOAD_PERMISSIONS = 11;
    private static final int REQUEST_CODE_UPLOAD_PERMISSIONS = 12;
    private static final int FILE_SELECT_CODE = 101;
    private static final int LOGIN_REQUEST_CODE = 120;

    ValueCallback<Uri[]> mainFilePathCallback;
    boolean refreshBySwipe = false;
    private View view;
    private NestedWebView mWebView;
    private String mainUrl = "";
    private String title = "";
    private boolean isFromPager;
    private String gaScreen;
//    private boolean isFromOnCreate = true;
//    private boolean isNativeContentAllow = false;
    private WapRedirectionUrls wapRedirectionUrls;
    private ProgressBar progressBar;
    private String fileUrl;
    private WebView mWebviewPop;
    private AlertDialog builder;
    private boolean isMainProgress = false;
//    private String gaArticle;
//    private String gaDisplayName;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private boolean hasHeader;

    private boolean isShowLoginDialog;
    private Context mContext;

    public WapV2Fragment() {
        // Required empty public constructor
    }

    public static WapV2Fragment getInstance(CategoryInfo categoryInfo, boolean isFromPager, boolean hasHeader) {
        WapV2Fragment webViewFragment = new WapV2Fragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KeyPair.KEY_URL, categoryInfo.feedUrl);
        bundle.putString(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
        bundle.putBoolean(Constants.KeyPair.KEY_IS_FROM_PAGER, isFromPager);
        bundle.putString(Constants.KeyPair.KEY_TITLE, categoryInfo.menuName);
        bundle.putString(Constants.KeyPair.KEY_EXTRA_VALUES, categoryInfo.extraVlues);
        bundle.putString(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
        bundle.putString(Constants.KeyPair.KEY_GA_DISPLAY_NAME, categoryInfo.displayName);
        bundle.putBoolean(Constants.KeyPair.KEY_HAS_HEADER, hasHeader);
        webViewFragment.setArguments(bundle);
        return webViewFragment;
    }

    public static WapV2Fragment getInstance(String feedUrl, String gaScreen, boolean isFromPager, String title, boolean hasHeader) {
        WapV2Fragment webViewFragment = new WapV2Fragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KeyPair.KEY_URL, feedUrl);
        bundle.putString(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        bundle.putBoolean(Constants.KeyPair.KEY_IS_FROM_PAGER, isFromPager);
        bundle.putString(Constants.KeyPair.KEY_TITLE, title);
        bundle.putBoolean(Constants.KeyPair.KEY_HAS_HEADER, hasHeader);
        webViewFragment.setArguments(bundle);
        return webViewFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                mainUrl = bundle.getString(Constants.KeyPair.KEY_URL, "");
                gaScreen = bundle.getString(Constants.KeyPair.KEY_GA_SCREEN);
//                gaArticle = bundle.getString(Constants.KeyPair.KEY_GA_ARTICLE);
//                gaDisplayName = bundle.getString(Constants.KeyPair.KEY_GA_DISPLAY_NAME);
                isFromPager = bundle.getBoolean(Constants.KeyPair.KEY_IS_FROM_PAGER);
                title = bundle.getString(Constants.KeyPair.KEY_TITLE);
                hasHeader = bundle.getBoolean(Constants.KeyPair.KEY_HAS_HEADER);
                String extraValues = bundle.getString(Constants.KeyPair.KEY_EXTRA_VALUES);
                if (!TextUtils.isEmpty(extraValues)) {
                    JSONObject extraValueJsonObject = new JSONObject(extraValues);
//                    if (extraValueJsonObject.has("native_content_check")) {
//                        isNativeContentAllow = extraValueJsonObject.getString("native_content_check").equalsIgnoreCase("1");
//                    }
                    if (extraValueJsonObject.has("redirect_false_urls")
                            || extraValueJsonObject.has("redirect_false_contains_urls")
                            || extraValueJsonObject.has("redirect_true_urls")
                            || extraValueJsonObject.has("redirect_true_contains_urls")) {
                        wapRedirectionUrls = new Gson().fromJson(extraValueJsonObject.toString(), WapRedirectionUrls.class);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_wap_v2, container, false);
        initView();
//        isFromOnCreate = true;
        setUpWebView();

        mySwipeRefreshLayout.setOnRefreshListener(
                () -> {
                    if (mySwipeRefreshLayout.isRefreshing()) {
                        refreshBySwipe = true;
                        mWebView.reload();
                        new Handler().postDelayed(() -> {
                            mySwipeRefreshLayout.setRefreshing(false);
                            refreshBySwipe = false;
                        }, 1000);
                    }
                }
        );
        return view;
    }

    private void initView() {
        mWebView = view.findViewById(R.id.rl_web_view);
        mySwipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        mySwipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        progressBar.setPadding(0, 0, 0, 0);
        progressBar.setMax(100);
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Tracking
        if (!isFromPager) {
            String source = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            Tracking.trackGAScreen(mContext, InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);

        }
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    public void setUpWebView() {
        try {
            if (mWebView != null) {
                mWebView.setOnKeyListener((v, keyCode, event) -> {
                    if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
                        mWebView.goBack();
                        return true;
                    }
                    return false;
                });
                //Set WebView WebSetting
                WebSettings webSettings = mWebView.getSettings();
                webSettings.setJavaScriptEnabled(true);
                webSettings.setLoadWithOverviewMode(true);
                webSettings.setUseWideViewPort(true);
                webSettings.setDomStorageEnabled(true);
                /*Custom User Agent*/
                String userAgent = webSettings.getUserAgentString();
                userAgent = userAgent + ";CustomUserAgent:" + CommonConstants.USER_AGENT;
                webSettings.setUserAgentString(userAgent);

                webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
                webSettings.setDefaultTextEncodingName("utf-8");
                webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
                webSettings.setMediaPlaybackRequiresUserGesture(false);

                JavaScriptInterface JSInterface = new JavaScriptInterface(getActivity());
                mWebView.addJavascriptInterface(JSInterface, "DivyaJSInterface");

                String APP_CACAHE_DIRNAME = "DB_" + CommonConstants.CHANNEL_ID;
                String cacheDirPath = mContext.getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME;
                webSettings.setAppCachePath(cacheDirPath);
                webSettings.setAppCacheEnabled(true);

                // Set Webview attributes
                mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
                mWebView.setScrollbarFadingEnabled(false);
                mWebView.setVerticalScrollBarEnabled(false);
                mWebView.setHorizontalScrollBarEnabled(false);
                mWebView.setOnLongClickListener(v -> true);
                mWebView.setLongClickable(false);
                mWebView.setHapticFeedbackEnabled(false);
                mWebView.setWebViewClient(new MyWebViewClient());
                mWebView.setWebChromeClient(new DbWebChromeClient());
                mWebView.setDownloadListener((url, userAgent1, contentDisposition, mimetype, contentLength) -> {
                    fileUrl = url;
                    checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
                });
                //Set Params
                RelativeLayout.LayoutParams params;
                if (mainUrl.contains("https")) {
                    params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                } else {
                    params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                }
                params.addRule(RelativeLayout.ALIGN_TOP);
                mWebView.setLayoutParams(params);
                //Load Url
                if (getActivity() != null && NetworkStatus.getInstance().isConnected(getActivity())) {
                    if (hasHeader) {
                        mWebView.loadUrl(mainUrl, AppUtils.getInstance().getWebHeader(mContext));
                    } else {
                        mWebView.loadUrl(mainUrl);
                    }
                } else {
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
                }
            }

        } catch (Resources.NotFoundException ignored) {
        }
        mySwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onPauseFragment() {
        //causing bug thats why removed
//        if (mWebView != null)
//            mWebView.loadUrl("");
//        isFromOnCreate = false;
    }

    @Override
    public void onResumeFragment() {
        //causing bug thats why removed

//        if (!isFromOnCreate) {
//            setValue(15);
//            setUpWebView();
//        }
    }

    public boolean isUrlRedirectFalse(String url) {
        return wapRedirectionUrls != null && wapRedirectionUrls.redirectFalseUrls != null && wapRedirectionUrls.redirectFalseUrls.contains(url);
    }

    public boolean isUrlRedirectFalseContains(String url) {
        if (wapRedirectionUrls != null) {
            for (int i = 0; i < wapRedirectionUrls.redirectFalseContainsUrls.size(); i++) {
                if (url.contains(wapRedirectionUrls.redirectFalseContainsUrls.get(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isUrlRedirectTrue(String url) {
        return wapRedirectionUrls != null && wapRedirectionUrls.redirectTrueUrls != null && wapRedirectionUrls.redirectTrueUrls.contains(url);
    }

    public boolean isUrlRedirectTrueContains(String url) {
        if (wapRedirectionUrls != null) {
            for (int i = 0; i < wapRedirectionUrls.redirectTrueContainsUrls.size(); i++) {
                if (url.contains(wapRedirectionUrls.redirectTrueContainsUrls.get(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean shouldOverrideUrl(WebView view, String url) {
        try {
            if (url.equalsIgnoreCase("javascript:void(0)") || url.equalsIgnoreCase("javscript:void(0)")) {
                return true;
            } else if (url.contains(mContext.getString(R.string.dynamic_link_host1)) || url.contains(mContext.getString(R.string.dynamic_link_host2))) {
                OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(mContext, url, "", 1, "", null);
                openNewsDetailInApp.openLink();
            } else if (url.equals(mainUrl) || isUrlRedirectFalse(url) || isUrlRedirectFalseContains(url)) {
                return false;
            } else if (isUrlRedirectTrue(url) || isUrlRedirectTrueContains(url)) {
                openOutSideWebView(url, title);
            } else if (!TextUtils.isEmpty(url) && (url.startsWith("whatsapp://") || url.startsWith("mailto:") || url.startsWith("tel:"))) {
                if (builder != null) {
                    builder.dismiss();
                }
                view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else if (url.contains("divyabhaskar.co.in") || url.contains("divyabhaskar.com")) {
                OpenNewsLinkInAppV3 openNewsLinkInApp = new OpenNewsLinkInAppV3(mContext, url, Constants.KeyPair.KEY_NEWS_UPDATE, 1);
                openNewsLinkInApp.openLink();
            } else if(url.contains("dbnewshub")){
                return false;
            }else {
                openOutSideWebView(url, title);
            }
        } catch (Exception ignored) {
        }
        return true;
    }

    private void openOutSideWebView(String url, String title) {
        Intent webIntent = new Intent(mContext, WapV2Activity.class);
        webIntent.putExtra(Constants.KeyPair.KEY_URL, url);
        webIntent.putExtra(Constants.KeyPair.KEY_TITLE, title);
        webIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        mContext.startActivity(webIntent);
    }

    public void setValue(int progress) {
        progressBar.setProgress(progress);
        if (progress == 100)
            progressBar.setVisibility(View.GONE);
        else
            progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /*******
     * Permission
     * */
    private void checkForPermission(int type) {
        ArrayList<String> permissionArray = new ArrayList<>();
        if (type == REQUEST_CODE_DOWNLOAD_PERMISSIONS) {
            try {
                if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
            } catch (Exception ignored) {
            }

            if (isAllPermissionProvided(type) && permissionArray.size() > 0) {
                ActivityCompat.requestPermissions(getActivity(), permissionArray.toArray(new String[permissionArray.size()]), REQUEST_CODE_DOWNLOAD_PERMISSIONS);
            } else {
                downloadFile();
            }
        } else if (type == REQUEST_CODE_UPLOAD_PERMISSIONS) {
            try {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                }
            } catch (Exception ignored) {
            }
            if (isAllPermissionProvided(type) && permissionArray.size() > 0) {
                ActivityCompat.requestPermissions(getActivity(), permissionArray.toArray(new String[permissionArray.size()]), REQUEST_CODE_UPLOAD_PERMISSIONS);
            } else {
                showFileChooser();
            }
        }

    }

    private boolean isAllPermissionProvided(int type) {
        if (type == REQUEST_CODE_DOWNLOAD_PERMISSIONS) {
            try {
                if ((ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    return true;
                }
            } catch (Exception ignored) {
            }
        } else if (type == REQUEST_CODE_UPLOAD_PERMISSIONS) {
            try {
                if ((ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    return true;
                }
            } catch (Exception ignored) {
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_DOWNLOAD_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    downloadFile();
                } else {
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.file_download_error));
                }
                break;
            case REQUEST_CODE_UPLOAD_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showFileChooser();
                } else {
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.file_download_error));
                }
                break;
        }
    }

    private void downloadFile() {
        try {
            String[] temp = fileUrl.split("/");
            String fileNameWithExtn = "";
            if (temp.length > 0) {
                fileNameWithExtn = temp[temp.length - 1];
            }
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fileUrl));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileNameWithExtn);
            DownloadManager dm = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
            dm.enqueue(request);
            //To notify the Client that the file is being downloaded
            AppUtils.getInstance().showCustomToast(getActivity(), "Downloading File");
        } catch (Exception e) {
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            if (!Constants.isFileChooserCalled) {
                Constants.isFileChooserCalled = true;
                startActivityForResult(
                        Intent.createChooser(intent, "Select a File to Upload"),
                        FILE_SELECT_CODE);
            }
        } catch (Exception ex) {
            // Potentially direct the user to the Market with a Dialog
            AppUtils.getInstance().showCustomToast(getActivity(), "Please install a File Manager.");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    try {
                        Uri uri = data.getData();
                        if (mainFilePathCallback != null) {
                            Uri[] uris = new Uri[1];
                            uris[0] = uri;
                            mainFilePathCallback.onReceiveValue(uris);
                        }
                        assert uri != null;
                        Log.d("WapV2", "File Uri: " + uri.toString());
                        // Get the path
                        String path = null;

                        path = AppUtils.getInstance().getPath(getActivity(), uri);
                        Log.d("WapV2", "File Path: " + path);
                    } catch (Exception ignored) {
                    }
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                } else {
                    if (mainFilePathCallback != null)
                        mainFilePathCallback.onReceiveValue(null);
                }
                Constants.isFileChooserCalled = false;
                break;

            case LOGIN_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    mWebView.loadUrl(mainUrl, AppUtils.getInstance().getWebHeader(mContext));
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    class MyWebViewClient extends WebViewClient {

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            if (handler != null)
                handler.proceed();
            super.onReceivedSslError(view, handler, error);
        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return shouldOverrideUrl(view, url);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            final Uri uri = request.getUrl();
            String url = uri.toString();
            return shouldOverrideUrl(view, url);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }

    private class DbWebChromeClient extends WebChromeClient {
        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            isMainProgress = true;
            mWebviewPop = new WebView(getActivity());
            mWebviewPop.setVerticalScrollBarEnabled(false);
            mWebviewPop.setHorizontalScrollBarEnabled(false);
            mWebviewPop.setWebViewClient(new MyWebViewClient());
            mWebviewPop.setWebChromeClient(new DbWebChromeClient());
            WebSettings webSettings = mWebviewPop.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setSavePassword(true);
            webSettings.setSaveFormData(true);
            mWebviewPop.setDownloadListener(new DownloadListener() {
                public void onDownloadStart(String url, String userAgent,
                                            String contentDisposition, String mimetype,
                                            long contentLength) {
                    fileUrl = url;
                    checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
                }
            });
            AlertDialog.Builder b = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Light_Dialog);
            b.setTitle("");
            b.setView(mWebviewPop);
            b.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    mWebviewPop.destroy();
                    dialog.dismiss();
                }
            });
            builder = b.create();
            builder.show();
            builder.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.setAcceptThirdPartyCookies(mWebviewPop, true);
            }
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(mWebviewPop);
            resultMsg.sendToTarget();
            return true;


        }

        @Override
        public void onCloseWindow(WebView window) {
            isMainProgress = false;
            try {
                mWebviewPop.destroy();
            } catch (Exception ignored) {

            }
            try {
                builder.dismiss();

            } catch (Exception ignored) {

            }
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {

            if (!isMainProgress) {
                if (newProgress < 15)
                    setValue(15);
                else
                    setValue(newProgress);
            }
            super.onProgressChanged(view, newProgress);
        }

        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            if (filePathCallback != null) {
                mainFilePathCallback = filePathCallback;
                checkForPermission(REQUEST_CODE_UPLOAD_PERMISSIONS);
            }
            return true;
        }
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void downloadFile(String url) {
            fileUrl = url;
            checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
        }

        @JavascriptInterface
        public void fbComplete() {
            try {
                if (builder != null) {
                    builder.dismiss();
                }
            } catch (Exception e) {
            }
        }

        @JavascriptInterface
        public void login(String msg) {
            showLoginDialog(msg);
        }

        @JavascriptInterface
        public void openLink(String url) {
            OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(mContext, url, "", 1, "", null);
            openNewsDetailInApp.openLink();
        }

        @JavascriptInterface
        public void openMenu(String menuId) {
            ActivityUtil.openCategoryAccordingToAction(mContext, JsonParser.getInstance().getCategoryInfoBasedOnId(mContext, menuId), "");
        }

        @JavascriptInterface
        public void openFullPage(String pageUrl) {
            try {
                JSONObject jsonObject = new JSONObject(pageUrl);
                Intent webTV = new Intent(mContext, WebFullScreenActivity.class);
                webTV.putExtra(Constants.KeyPair.KEY_URL, jsonObject.optString("url"));
                webTV.putExtra(Constants.KeyPair.KEY_GA_SCREEN, pageUrl);
                webTV.putExtra(Constants.KeyPair.KEY_HAS_HEADER, jsonObject.optString("isHeader").equalsIgnoreCase("1"));
                mContext.startActivity(webTV);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void appTrackEvent(String jsonData) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                String catName = jsonObject.optString("catName");
                JSONArray eventArray = jsonObject.optJSONArray("events");

                ArrayList<ExtraCTEvent> ctEventArrayList = new ArrayList<>();
                String eventLabel = "";
                String eventValue = "";

                JSONObject jsObj;
                for (int i = 0; i < eventArray.length(); i++) {
                    jsObj = eventArray.getJSONObject(i);
                    if (i == 0) {
                        eventLabel = jsObj.optString("key");
                        eventValue = jsObj.optString("value");
                    } else {
                        if (!TextUtils.isEmpty(jsObj.optString("key"))) {
                            ctEventArrayList.add(new ExtraCTEvent(jsObj.optString("key"), jsObj.optString("value")));
                        }
                    }
                }

                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.SPECIAL_CONTENT, CTConstant.PROP_NAME.CLICKON, eventValue, "");
                CleverTapDB.getInstance(mContext).cleverTapTrackEvent(mContext, catName, eventLabel, eventValue, ctEventArrayList, LoginController.getUserDataMap());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void appTrackScreen(String screenName) {
            String source = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

            Tracking.trackGAScreen(mContext, InitApplication.getInstance().getDefaultTracker(), screenName, source, medium, campaign);
        }
        @JavascriptInterface
        public void rashiDetail(String jsonData) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                String gender = jsonObject.optString("gender");
                String rashiName = jsonObject.optString("rashiName");
                String numerono = jsonObject.optString("numerono");
                Log.e("Event Tracking ", "gender" + gender + "=> rashiName" + rashiName + "=> numerono" + numerono);

                CleverTapDB.getInstance(mContext).updateRashifalValuesOfUser(gender, rashiName, numerono);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void showLoginDialog(String msg) {
        if (isShowLoginDialog) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        final TextView message = new TextView(getContext());
        message.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
        message.setPadding((int) AppUtils.getInstance().convertDpToPixel(8, getContext()), (int) AppUtils.getInstance().convertDpToPixel(16, getContext()), (int) AppUtils.getInstance().convertDpToPixel(8, getContext()), 0);
        message.setText(msg);
        message.setTextSize(16);

        builder.setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        LoginController.loginController().getLoginIntent(getActivity(), TrackingData.getDBId(getContext()), TrackingData.getDeviceId(getContext()), LOGIN_REQUEST_CODE);
//                        startActivityForResult(loginIntent, LOGIN_REQUEST_CODE);
                        isShowLoginDialog = false;
                    }
                })
                .setTitle(getResources().getString(R.string.login))
                .setView(message);

        final AlertDialog alert = builder.create();
        alert.setOnShowListener(dialog -> {
            Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
            btnPositive.setTextSize(16);
            btnPositive.setTypeface(null, Typeface.NORMAL);
        });

        isShowLoginDialog = true;
        alert.show();
    }
}
