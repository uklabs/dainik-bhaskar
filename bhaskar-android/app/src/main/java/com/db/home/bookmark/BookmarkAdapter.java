package com.db.home.bookmark;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.BookmarkSerializedListInfo;
import com.db.util.ImageUtil;
import com.db.util.AppUtils;

import java.util.List;

class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.BookmarkViewHolder> {

    private List<BookmarkSerializedListInfo> bookmarkSerializedList;
    private Context context;
    private BookmarkInteractionListener mListener;

    BookmarkAdapter(Context context, List<BookmarkSerializedListInfo> bookmarkSerializedList, BookmarkInteractionListener bookmarkInteractionListener) {
        this.mListener = bookmarkInteractionListener;
        this.context = context;
        this.bookmarkSerializedList = bookmarkSerializedList;
    }

    @Override
    public BookmarkAdapter.BookmarkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BookmarkViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_hub_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(BookmarkAdapter.BookmarkViewHolder holder, int position) {
        final BookmarkSerializedListInfo bookmarkSerializedListInfo = bookmarkSerializedList.get(position);
        holder.bindContent(bookmarkSerializedListInfo);
        ImageUtil.setImage(context, bookmarkSerializedListInfo.getImage(), holder.thumbIv, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);
        holder.titleTv.setText(bookmarkSerializedListInfo.getTitle());
        holder.titleTv.setText(AppUtils.getInstance().getHomeTitle(bookmarkSerializedListInfo.getIitlTitle(), AppUtils.getInstance().fromHtml(bookmarkSerializedListInfo.getTitle()), bookmarkSerializedListInfo.getColor()));
    }

    @Override
    public int getItemCount() {
        return bookmarkSerializedList != null ? bookmarkSerializedList.size() : 0;
    }


    public void setBookmarkSerializedList(List<BookmarkSerializedListInfo> data) {
        bookmarkSerializedList.clear();
        bookmarkSerializedList.addAll(data);
        notifyDataSetChanged();
    }

    public interface BookmarkInteractionListener {
        void onBookmarkSelection(int position, BookmarkSerializedListInfo bookmarkSerializedListInfo);

        void onDelete(int position, BookmarkSerializedListInfo bookmarkSerializedListInfo);
    }

    class BookmarkViewHolder extends RecyclerView.ViewHolder {

        private TextView titleTv;
        private ImageView thumbIv;
        private ImageView ivDelete;
        private BookmarkSerializedListInfo bookmarkSerializedListInfo;

        private BookmarkViewHolder(View itemView) {
            super(itemView);
            thumbIv = itemView.findViewById(R.id.thumb_image);
            titleTv = itemView.findViewById(R.id.list_item_name);
            ivDelete = itemView.findViewById(R.id.iv_delete);
            ivDelete.setVisibility(View.VISIBLE);

            ivDelete.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onDelete(getAdapterPosition(), bookmarkSerializedListInfo);
                }
            });

            itemView.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onBookmarkSelection(getAdapterPosition(), bookmarkSerializedListInfo);
                }
            });
        }

        private void bindContent(BookmarkSerializedListInfo bookmarkSerializedListInfo) {
            this.bookmarkSerializedListInfo = bookmarkSerializedListInfo;
        }
    }
}