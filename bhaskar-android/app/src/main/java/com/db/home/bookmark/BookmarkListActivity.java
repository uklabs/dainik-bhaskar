package com.db.home.bookmark;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.InitApplication;
import com.db.data.models.BookmarkSerializedListInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.source.remote.SerializeData;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.main.BaseAppCompatActivity;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BookmarkListActivity extends BaseAppCompatActivity {

    private BookmarkAdapter bookmarkAdapter;
    private List<BookmarkSerializedListInfo> bookmarkSerializedFeedList;
    private CategoryInfo mCategoryInfo;

    BookmarkAdapter.BookmarkInteractionListener bookmarkInteractionListener = new BookmarkAdapter.BookmarkInteractionListener() {
        @Override
        public void onBookmarkSelection(int position, BookmarkSerializedListInfo bookmarkSerializedListInfo) {
            if (bookmarkSerializedListInfo != null) {
                String storyType = bookmarkSerializedListInfo.getStorytype();
                if (storyType == null) storyType = "";
                if (storyType.equalsIgnoreCase("V2")) {
                    //divya articles
                    startActivityForResult(DivyaCommonArticleActivity.getIntent(BookmarkListActivity.this,
                            bookmarkSerializedListInfo.getStoryId(), "",
                            bookmarkSerializedListInfo.getGaArticle(), bookmarkSerializedListInfo.getGaScreen(),
                            bookmarkSerializedListInfo.getDisplayName(), bookmarkSerializedListInfo.getGaEventLabel(),
                            AppUtils.getThemeColor("")), Constants.REQ_CODE_BOTTOM_NAV);
                } else {
                    startActivityForResult(DivyaCommonArticleActivity.getIntent(BookmarkListActivity.this, bookmarkSerializedListInfo.getStoryId(), "", AppFlyerConst.BookmarkValue.ARTICLE_VALUE, AppFlyerConst.BookmarkValue.SCREEN_VALUE, bookmarkSerializedListInfo.getDisplayName(), bookmarkSerializedListInfo.getGaEventLabel(), AppUtils.getThemeColor("")), Constants.REQ_CODE_BOTTOM_NAV);
                }
            }
        }

        @Override
        public void onDelete(int position, BookmarkSerializedListInfo bookmarkSerializedListInfo) {
            SerializeData.getInstance(BookmarkListActivity.this).removeBookmark(bookmarkSerializedListInfo.getStoryId());
            bookmarkSerializedFeedList.remove(bookmarkSerializedListInfo);
            bookmarkAdapter.notifyDataSetChanged();
            if (bookmarkAdapter.getItemCount() > 0) {
                findViewById(R.id.blank_text).setVisibility(View.GONE);
            } else {
                findViewById(R.id.blank_text).setVisibility(View.VISIBLE);
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleExtras(getIntent());

        setTheme(ThemeUtil.getCurrentTheme(this));

        setContentView(R.layout.activity_bookmark_list);

        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(v -> BookmarkListActivity.this.finish());
        TextView tvTitle = findViewById(R.id.toolbar_title);
        String title = getString(R.string.bookmark_txt);
        if (mCategoryInfo != null)
            title = mCategoryInfo.menuName;
        tvTitle.setText(title);
        tvTitle.setTextColor(Color.BLACK);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ica_back_black);
            actionBar.setTitle(null);
        }

        ProgressBar progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(this, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);

        bookmarkSerializedFeedList = new ArrayList<>();

        bookmarkSerializedFeedList.addAll(SerializeData.getInstance(this).getBookmarkList());
        Collections.reverse(bookmarkSerializedFeedList);
        RecyclerView recyclerView = findViewById(R.id.bookmark_recycler_view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        // Add the Serialized Bean to tha adapter.
        bookmarkAdapter = new BookmarkAdapter(this, bookmarkSerializedFeedList, bookmarkInteractionListener);

        recyclerView.setAdapter(bookmarkAdapter);
        progressBar.setVisibility(View.GONE);


        // Tracking
        String source = AppPreferences.getInstance(BookmarkListActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(BookmarkListActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(BookmarkListActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(BookmarkListActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GAScreen.BOOKMARK, source, medium, campaign);
        Systr.println("GA Screen : " + AppFlyerConst.GAScreen.BOOKMARK + ((campaign.equalsIgnoreCase("direct")) ? "" : campaign));
    }

    private void handleExtras(Intent intent) {
        if (intent != null) {
            mCategoryInfo = (CategoryInfo) intent.getSerializableExtra(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        List<BookmarkSerializedListInfo> bookmarksSerialized = new ArrayList<>();

        bookmarksSerialized.addAll(SerializeData.getInstance(this).getBookmarkList());
        Collections.reverse(bookmarksSerialized);

        if (bookmarksSerialized.isEmpty()) {
            if (bookmarkAdapter != null) {
                bookmarkAdapter.setBookmarkSerializedList(new ArrayList<>());
            }
            findViewById(R.id.blank_text).setVisibility(View.VISIBLE);
        } else {
            if (bookmarkAdapter != null) {
                bookmarkAdapter.setBookmarkSerializedList(bookmarksSerialized);
            }
            findViewById(R.id.blank_text).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == Constants.REQ_CODE_BOTTOM_NAV && resultCode == RESULT_OK) {
            finish();
        }

    }
}