package com.db.home.fantasyMenu;


import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.fantasyMenu.FantasyMenu;
import com.db.data.models.fantasyMenu.MenuItem;
import com.db.home.fantasyMenu.adapter.FantasticMiddleMenuAdapter;
import com.db.home.fantasyMenu.adapter.FantasyBottomMenuAdapter;
import com.db.home.fantasyMenu.adapter.FantasyTopSectionMenuAdapter;
import com.bhaskar.util.LoginController;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.Action;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.ImageUtil;
import com.db.util.JsonPreferences;
import com.db.util.QuickPreferences;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class FantasyFragment extends Fragment {

    private static final String TAG = FantasyFragment.class.getSimpleName();
    private ImageView ivProfile;
    private TextView tvUserName;
    private ImageView ivMenuOne;
    private ImageView ivMenuTwo;

    private FantasyMenu fantasyMenu;
    private TextView tvTopMenuSection;
    private TextView tvMenuPageTitle;
    private RecyclerView topMenuRv;
    private RecyclerView middleMenuRv;
    private RecyclerView bottomMenuRv;
    private FantasticMiddleMenuAdapter fantasticMiddleMenuAdapter;
    private int mExpandPosition = -1;
    private CopyOnWriteArrayList<MenuItem> headerMenuList;
    private CopyOnWriteArrayList<MenuItem> middleMenuList;
    private CopyOnWriteArrayList<MenuItem> topMenuList;
    private CopyOnWriteArrayList<MenuItem> bottomMenuList;
    private NestedScrollView nestedScrollView;
    private OnActionListener onActionListener;
    private ImageView ivWeatherType;
    private TextView tvSalutationMsg;
    private ViewGroup vWeatherWidget;
    private ProfileInfo mProfileInfo;
    private ImageView ivBack;

    public FantasyFragment() {
        // Required empty public constructor
    }

    public static FantasyFragment getInstance() {
        return new FantasyFragment();
    }

    private static int getNumberCountInclusive(int minRange, int maxRange) {
        return (maxRange - minRange) + 1;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onActionListener = (OnActionListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String fantasyString = JsonPreferences.getInstance(getContext()).getStringValue(QuickPreferences.JsonPreference.FANTASY_MENU_FEED, "");
        Type type = new TypeToken<FantasyMenu>() {
        }.getType();
        fantasyMenu = new Gson().fromJson(fantasyString, type);
        setupDataList(fantasyMenu);
    }

    private void setupDataList(FantasyMenu fantasyMenu) {
        if (fantasyMenu != null) {
            headerMenuList = new CopyOnWriteArrayList<>(fantasyMenu.headerMenu);
            filterListBasedOnActionAndGdpr(headerMenuList, 1);
            topMenuList = new CopyOnWriteArrayList<>(fantasyMenu.topMenu);
            filterListBasedOnActionAndGdpr(topMenuList, 2);
            middleMenuList = new CopyOnWriteArrayList<>(fantasyMenu.middleMenu);
            filterListBasedOnActionAndGdpr(middleMenuList, 3);
            bottomMenuList = new CopyOnWriteArrayList<>(fantasyMenu.bottomMenu);
            filterListBasedOnActionAndGdpr(bottomMenuList, 4);
        } else {
            headerMenuList = new CopyOnWriteArrayList<>();
            topMenuList = new CopyOnWriteArrayList<>();
            middleMenuList = new CopyOnWriteArrayList<>();
            bottomMenuList = new CopyOnWriteArrayList<>();
        }
        mProfileInfo = LoginController.loginController().getCurrentUser();
    }

    private synchronized void filterListBasedOnActionAndGdpr(List<MenuItem> menuList, int type) {
        boolean isBlockCountry = AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        if (type == 1 || type == 2) {
            for (MenuItem menuItem : menuList) {
                //action check and gdpr check
                if (!Action.ALL_HOME_CATEGORY_ACTIONS.contains(menuItem.action) || (menuItem.gdprBlock.equalsIgnoreCase("1") && isBlockCountry)) {
                    menuList.remove(menuItem);
                }
            }
        } else if (type == 3) {
            //middle
            for (MenuItem menuItem : menuList) {
                //remove menuItem if not submenu
                for (MenuItem subMenuItem : menuItem.item) {
                    //action check and gdpr check
                    if (!Action.ALL_HOME_CATEGORY_ACTIONS.contains(subMenuItem.action) || (subMenuItem.gdprBlock.equalsIgnoreCase("1") && isBlockCountry)) {
                        menuItem.item.remove(subMenuItem);
                    }
                }

                if (menuItem.item == null || menuItem.item.isEmpty()) {
                    menuList.remove(menuItem);
                }
            }
        } else if (type == 4) {
            //bottom
            for (MenuItem menuItem : menuList) {
                //check submenu
                if (!Action.ALL_HOME_CATEGORY_ACTIONS.contains(menuItem.action) || (menuItem.gdprBlock.equalsIgnoreCase("1") && isBlockCountry)) {
                    menuList.remove(menuItem);
                } else if (menuItem.subMenu != null && !menuItem.subMenu.isEmpty()) {
                    //check submenu
                    for (MenuItem subMenu : menuItem.subMenu) {
                        if (!Action.ALL_HOME_CATEGORY_ACTIONS.contains(subMenu.action) || (subMenu.gdprBlock.equalsIgnoreCase("1") && isBlockCountry)) {
                            menuItem.subMenu.remove(subMenu);
                        }
                    }
                    if (menuItem.subMenu.isEmpty()) {
                        menuList.remove(menuItem);
                    }
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fantasy, container, false);
        initViews(rootView);
        initFantasyMenu();

        ivBack.setOnClickListener(v -> {
            if (onActionListener != null) {
                onActionListener.onActionClick(null);
            }
        });
        showWeatherCard();
        return rootView;
    }

    private final OnMiddleMenuItemClickListener middleMenuItemClickListener = new OnMiddleMenuItemClickListener() {

        @Override
        public void onMenuItemClick(MenuItem fantasticMenu) {
            onMenuItemClickListener.onMenuItemClick(fantasticMenu);
        }

        @Override
        public void onClickMenuExpand(int expandPosition) {
            MenuItem fantasticMenuItemExp = middleMenuList.get(expandPosition);
            fantasticMenuItemExp.menuStatus = (MENU_STATUS.EXPAND);
            if (expandPosition % 2 != 0) {
                Collections.swap(middleMenuList, expandPosition - 1, expandPosition);
                mExpandPosition = expandPosition - 1;

                int minRange;
                int maxRange;

                minRange = expandPosition - 1;
                if ((expandPosition + 1) < middleMenuList.size()) {
                    maxRange = expandPosition + 1;
                } else {
                    maxRange = expandPosition;
                }

                int itemCount = getNumberCountInclusive(minRange, maxRange);
                fantasticMiddleMenuAdapter.notifyItemRangeChanged(minRange, itemCount);
            } else {
                mExpandPosition = expandPosition;
                fantasticMiddleMenuAdapter.notifyItemChanged(expandPosition);
                if ((expandPosition + 1) < middleMenuList.size()) {
                    fantasticMiddleMenuAdapter.notifyItemChanged(expandPosition + 1);
                }
            }
            for (int index = 0; index < middleMenuList.size(); index++) {
                MenuItem fantasticMenuItem = middleMenuList.get(index);
                if (!fantasticMenuItem.id.equals(fantasticMenuItemExp.id) &&
                        (fantasticMenuItem.menuStatus == FantasyFragment.MENU_STATUS.EXPAND)) {
                    fantasticMenuItem.menuStatus = FantasyFragment.MENU_STATUS.COLLAPSE;
                    fantasticMiddleMenuAdapter.notifyItemChanged(index);
                    break;
                }
            }
        }

        @Override
        public void onClickMenuCollapse(int collapsePosition) {
            MenuItem fantasticMenuItem = middleMenuList.get(collapsePosition);
            fantasticMenuItem.menuStatus = FantasyFragment.MENU_STATUS.COLLAPSE;

            if (collapsePosition % 2 != 0 && (collapsePosition + 1) < middleMenuList.size()) {
                Collections.swap(middleMenuList, collapsePosition, collapsePosition + 1);
            }

            mExpandPosition = -1;

            int minRange = (collapsePosition - 1) < 0 ? collapsePosition : (collapsePosition - 1);
            int maxRange = (collapsePosition + 1) >= middleMenuList.size() ? collapsePosition : (collapsePosition + 1);

            int itemCount = getNumberCountInclusive(minRange, maxRange);
            fantasticMiddleMenuAdapter.notifyItemRangeChanged(minRange, itemCount);
        }

        @Override
        public void onMenuExpended(View expandView) {
            nestedScrollView.post(() -> {
                try {
                    Rect viewToScrollRect = new Rect(); //coordinates to scroll to
                    expandView.getHitRect(viewToScrollRect); //fills viewToScrollRect with coordinates of viewToScroll relative to its parent (LinearLayout)
                    nestedScrollView.requestChildRectangleOnScreen(middleMenuRv, viewToScrollRect, false); //ScrollView will make sure, the given viewToScrollRect is visible
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    };

    private final OnMenuItemClickListener onMenuItemClickListener = this::doOnMenuClick;


    private void showWeatherCard() {
        try {
            FantasyUtil.SalutationType salutationType = FantasyUtil.getSalutationTypeBasedOnTime();
            ivWeatherType.setVisibility(View.VISIBLE);
            ImageUtil.setImage(getContext(), R.drawable.weather_default, ivWeatherType, 0);
            vWeatherWidget.setBackgroundResource(salutationType.getBackResId());
            showUserSalutationMsg(salutationType);
        } catch (Exception ignored) {
        }

    }

    private void showUserProfilePic(ProfileInfo mProfileInfo) {
        if (mProfileInfo != null && mProfileInfo.isUserLoggedIn) {
            tvUserName.setText(mProfileInfo.name);
            ImageUtil.setImage(getContext(), mProfileInfo.profilePic, ivProfile, R.drawable.ic_menu_card_user_profile_placeholder);
        } else {
            ivProfile.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_card_user_profile_placeholder));
            tvUserName.setText("");
            tvSalutationMsg.setText("");
        }
    }

    private void initFantasyMenu() {
        if (fantasyMenu != null) {
            if (!TextUtils.isEmpty(fantasyMenu.labelMenuPageTitle))
                tvMenuPageTitle.setText(fantasyMenu.labelMenuPageTitle);
            tvTopMenuSection.setText(fantasyMenu.labelTopSection);
        } else {
            tvMenuPageTitle.setText(getString(R.string.fmenu_title));
        }
        addHeaderMenus();
        addTopMenuSections(topMenuList);
        if (fantasyMenu != null)
            addMiddleMenus(middleMenuList, fantasyMenu.labelViewMore, fantasyMenu.labelViewLess);
        addBottomMenus(bottomMenuList);
    }

    private void showUserSalutationMsg(FantasyUtil.SalutationType salutationType) {
        String salutationMsg = salutationType.getSalutationMsg();
        tvSalutationMsg.setText(salutationMsg);
    }

    private void addBottomMenus(List<MenuItem> bottomMenuList) {
        if (bottomMenuList != null && !bottomMenuList.isEmpty()) {
            FantasyBottomMenuAdapter fantasticBottomMenuAdapter = new FantasyBottomMenuAdapter(getContext(), bottomMenuList, onMenuItemClickListener);
            bottomMenuRv.setAdapter(fantasticBottomMenuAdapter);
            bottomMenuRv.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,
                    false));
        }
    }

    private void addHeaderMenus() {
        ivMenuOne.setVisibility(View.GONE);
        ivMenuTwo.setVisibility(View.GONE);
        if (headerMenuList != null && !headerMenuList.isEmpty()) {
            for (int index = 0; index < headerMenuList.size(); index++) {
                if (index == 2) {
                    break;
                } else {
                    MenuItem fantasticMenu = headerMenuList.get(index);
                    if (index == 0) {
                        ivMenuOne.setVisibility(View.VISIBLE);
                        ImageUtil.setImage(getContext(), fantasticMenu.icon, ivMenuOne, R.drawable.menu_header_placeholder);

                    } else if (index == 1) {
                        ivMenuTwo.setVisibility(View.VISIBLE);
                        ImageUtil.setImage(getContext(), fantasticMenu.icon, ivMenuTwo, R.drawable.menu_header_placeholder);

                    }
                }
            }
        }
    }


    public void updateLoginView(ProfileInfo profileInfo) {
        this.mProfileInfo = profileInfo;
        showUserProfilePic(mProfileInfo);
        showWeatherCard();
    }

    private void addMiddleMenus(List<MenuItem> middleMenuList, String labelViewMore, String labelViewLess) {
        if (middleMenuList != null && !middleMenuList.isEmpty()) {
            String label = "News";
            fantasticMiddleMenuAdapter = new FantasticMiddleMenuAdapter(getContext(), middleMenuList, middleMenuItemClickListener
                    , labelViewMore, labelViewLess, label);
            middleMenuRv.setAdapter(fantasticMiddleMenuAdapter);
            final int fullSize = 2;
            GridLayoutManager layoutManager = new GridLayoutManager(getContext(), fullSize);
            layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return (position == mExpandPosition) ? fullSize : 1;
                }
            });
            middleMenuRv.setLayoutManager(layoutManager);
        }
    }

    private void addTopMenuSections(List<MenuItem> topMenuList) {
        if (topMenuList != null && !topMenuList.isEmpty()) {
            tvTopMenuSection.setVisibility(View.VISIBLE);
            FantasyTopSectionMenuAdapter fantasticTopSectionMenuAdapter = new FantasyTopSectionMenuAdapter(getContext(),
                    topMenuList, onMenuItemClickListener);
            topMenuRv.setAdapter(fantasticTopSectionMenuAdapter);
            GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
            topMenuRv.setLayoutManager(layoutManager);
        } else {
            tvTopMenuSection.setVisibility(View.GONE);
        }
    }

    private void doOnMenuClick(MenuItem catInfo) {
        if (catInfo != null && onActionListener != null) {
            onActionListener.onActionClick(catInfo);
        }
    }

    private void initViews(View rootView) {
        topMenuRv = rootView.findViewById(R.id.rv_top_menu);
        middleMenuRv = rootView.findViewById(R.id.rv_middle_menu);
        nestedScrollView = rootView.findViewById(R.id.nsv_fmenu);
        bottomMenuRv = rootView.findViewById(R.id.rv_bottom_menu);
        tvUserName = rootView.findViewById(R.id.tv_user_name);
        tvMenuPageTitle = rootView.findViewById(R.id.tv_menu_page_title);
        ivProfile = rootView.findViewById(R.id.iv_user_profile);
        ivMenuOne = rootView.findViewById(R.id.ivMenuOne);
        ivMenuTwo = rootView.findViewById(R.id.ivMenuTwo);
        ivMenuOne.setOnClickListener(this::onMenuOne);
        ivMenuTwo = rootView.findViewById(R.id.ivMenuTwo);
        ivMenuTwo.setOnClickListener(this::onMenuTwo);
        ImageView ivEditProfile = rootView.findViewById(R.id.iv_edit_user);
        ivEditProfile.setOnClickListener(v -> onProfileEdit());
        ivProfile.setOnClickListener(v -> onProfileEdit());

        tvTopMenuSection = rootView.findViewById(R.id.tv_top_section);
        ivWeatherType = rootView.findViewById(R.id.iv_weather_type_img);
        vWeatherWidget = rootView.findViewById(R.id.fl_weather_widget_Card);
        tvSalutationMsg = rootView.findViewById(R.id.tv_salutation_msg);
        ivBack = rootView.findViewById(R.id.iv_back_arrow);
        View vProfile = rootView.findViewById(R.id.vProfileRoot);
        if (AppUtils.isBlockCountry(getContext())) {
            vProfile.setVisibility(View.GONE);
        } else {
            vProfile.setVisibility(View.VISIBLE);
        }

    }

    private void onMenuTwo(View view) {
        try {
            if (onActionListener != null) {
                MenuItem catInfo = headerMenuList.get(1);
                onActionListener.onActionClick(catInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void onMenuOne(View view) {
        try {
            if (onActionListener != null) {
                MenuItem catInfo = headerMenuList.get(0);
                onActionListener.onActionClick(catInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void onProfileEdit() {
        if (onActionListener != null) {
            MenuItem catInfo = new MenuItem();
            catInfo.action = "profile";
            onActionListener.onActionClick(catInfo);
        }
    }

    public enum MENU_STATUS {
        COLLAPSE, EXPAND
    }

    public interface OnMenuItemClickListener {
        void onMenuItemClick(MenuItem catInfo);
    }

    public interface OnMiddleMenuItemClickListener extends OnMenuItemClickListener {
        void onClickMenuExpand(int expendPosition);

        void onClickMenuCollapse(int collapsePosition);

        void onMenuExpended(View expandView);
    }


    public interface OnActionListener {
        void onActionClick(MenuItem catInfo);
    }
}
