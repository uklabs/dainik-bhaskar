package com.db.home.fantasyMenu;

import androidx.annotation.DrawableRes;

import com.bhaskar.R;

import java.util.Calendar;

public class FantasyUtil {

    public interface FantasyConstants{

        // Salutation Messages
        String SALUTATION_GOOD_MORNING = "Good Morning";
        String SALUTATION_GOOD_AFTERNOON = "Good Afternoon";
        String SALUTATION_GOOD_EVENING = "Good Evening";
        String SALUTATION_SWAGAT_HAI = "Welcome";
        String DATE_PATTERN = "dd-MM-yyyy";
    }

    public enum SalutationType {
        MORNING(FantasyConstants.SALUTATION_GOOD_MORNING, R.drawable.weather_morning, R.drawable.back_weather_morning_card),
        AFTERNOON(FantasyConstants.SALUTATION_GOOD_AFTERNOON, R.drawable.weather_afternoon, R.drawable.back_weather_afternoon_card),
        EVENING(FantasyConstants.SALUTATION_GOOD_EVENING,
                R.drawable.weather_evening, R.drawable.back_weather_evening_card),
        NIGHT(FantasyConstants.SALUTATION_SWAGAT_HAI, R.drawable.weather_evening, R.drawable.back_weather_evening_card);

        private String salutationMsg;
        private int iconResId, backResId;

        /***
         *
         * @param salutationMsg
         * @param iconResId
         * @param backResId
         */
        SalutationType(String salutationMsg, @DrawableRes int iconResId, @DrawableRes int backResId) {
            this.salutationMsg = salutationMsg;
            this.iconResId = iconResId;
            this.backResId = backResId;
        }

        public int getBackResId() {
            return backResId;
        }

        public int getIconResId() {
            return iconResId;
        }

        public String getSalutationMsg() {
            return salutationMsg;
        }

        @Override
        public String toString() {
            return "SalutationType{" +
                    "salutationMsg='" + salutationMsg + '\'' +
                    ", iconResId=" + iconResId +
                    ", backResId=" + backResId +
                    '}';
        }
    }

    public static SalutationType getSalutationTypeBasedOnTime() {
        return getGreetings();
    }

    public static SalutationType getGreetings() {
        Calendar calender = Calendar.getInstance();
        int timeOfDay = calender.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 3 && timeOfDay < 12) {
            return SalutationType.MORNING;
        } else if (timeOfDay >= 12 && timeOfDay < 18) {
            return SalutationType.AFTERNOON;

        } else if (timeOfDay >= 0 && timeOfDay < 3) {
            return SalutationType.NIGHT;
        } else {
            return SalutationType.EVENING;
        }
    }

}
