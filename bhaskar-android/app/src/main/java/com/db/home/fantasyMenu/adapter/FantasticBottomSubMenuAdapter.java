package com.db.home.fantasyMenu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.fantasyMenu.MenuItem;
import com.db.home.fantasyMenu.FantasyFragment;

import java.util.ArrayList;
import java.util.List;

public final class FantasticBottomSubMenuAdapter extends RecyclerView.Adapter<FantasticBottomSubMenuAdapter.ViewHolder> {

    private LayoutInflater layoutInflater;
    private List<MenuItem> menuChildList;
    private FantasyFragment.OnMenuItemClickListener onBottomMenuItemClickListener;

    FantasticBottomSubMenuAdapter(Context context, FantasyFragment.OnMenuItemClickListener onBottomMenuItemClickListener) {
        layoutInflater = LayoutInflater.from(context);
        this.onBottomMenuItemClickListener = onBottomMenuItemClickListener;
        menuChildList = new ArrayList<>();
    }

    void setMenuChildList(List<MenuItem> menuChildList) {
        clearMenuChildList();
        this.menuChildList.addAll(menuChildList);
    }

    void clearMenuChildList() {
        this.menuChildList.clear();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.fantastic_bottom_menu_child, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        MenuItem menuItem = menuChildList.get(position);
        holder.menuTitleTv.setText(menuItem.name);
        holder.menuTitleTv.setTag(menuItem);
        holder.menuTitleTv.setOnClickListener(onClickListener);
    }

    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MenuItem fantasticMenu = (MenuItem) v.getTag();
            onBottomMenuItemClickListener.onMenuItemClick(fantasticMenu);
        }
    };

    @Override
    public int getItemCount() {
        return menuChildList.size();
    }

    final class ViewHolder extends RecyclerView.ViewHolder {
        TextView menuTitleTv;

        ViewHolder(View itemView) {
            super(itemView);
            menuTitleTv = itemView.findViewById(R.id.tv_menu_child_title);
        }
    }
}

