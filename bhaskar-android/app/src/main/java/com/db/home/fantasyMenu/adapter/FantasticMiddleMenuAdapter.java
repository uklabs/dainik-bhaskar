package com.db.home.fantasyMenu.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bluehomestudio.animationplus.animation.WidthAnimation;
import com.bhaskar.R;
import com.db.data.models.fantasyMenu.MenuItem;
import com.db.home.fantasyMenu.FantasyFragment;
import com.db.util.AppUtils;

import java.util.List;

public class FantasticMiddleMenuAdapter extends RecyclerView.Adapter<FantasticMiddleMenuAdapter.ViewHolder> {

    private String labelViewLess;
    private String labelViewMore;
    private String menuLabel;
    private Context context;
    private LayoutInflater layoutInflater;
    private List<MenuItem> itemList;
    private FantasyFragment.OnMiddleMenuItemClickListener middleMenuItemClickListener;
    private String TAG = FantasticMiddleMenuAdapter.class.getSimpleName();

    private int collapsedHeadingLineWidth, expandHeadingLineWidth;
    private int collapsedHeadingTextSize, expandHeadingTextSize;

    public FantasticMiddleMenuAdapter(Context context, List<MenuItem> middleMenuList,
                                      FantasyFragment.OnMiddleMenuItemClickListener middleMenuItemClickListener,
                                      String labelViewMore, String labelViewLess, String menuLabel) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.middleMenuItemClickListener = middleMenuItemClickListener;
        this.itemList = middleMenuList;

        this.labelViewLess = labelViewLess;
        this.labelViewMore = labelViewMore;
        this.menuLabel = menuLabel;
        int deviceWidth = AppUtils.getDeviceWidth(context);
        int median = context.getResources().getDimensionPixelSize(R.dimen._10sdp);
        this.collapsedHeadingLineWidth = deviceWidth / 2 - (2 * median);
        this.expandHeadingLineWidth = deviceWidth - (2 * median);

        this.collapsedHeadingTextSize = context.getApplicationContext().getResources().getDimensionPixelSize(R.dimen.fan_menu_regular_txt_size);
        this.expandHeadingTextSize = context.getApplicationContext().getResources().getDimensionPixelSize(R.dimen.fan_menu_heading_txt_size);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.fantastic_menu_middle_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MenuItem fMenu = itemList.get(position);

        GradientDrawable gd = new GradientDrawable();
        gd.setColor(AppUtils.getThemeColor(context, fMenu.color));
        gd.setCornerRadius(25f);

        holder.hLineView.setBackground(gd);
        holder.menuTitleTv.setText(fMenu.name);
        holder.menuItemGl.setTag(fMenu);

        holder.menuItemGl.removeAllViews();
        List<MenuItem> childMenuList = fMenu.item;

        if (childMenuList != null && !childMenuList.isEmpty()) {
            if (fMenu.menuStatus == FantasyFragment.MENU_STATUS.COLLAPSE) {
                Log.d("addCollapsedGridLayout", menuLabel);
                addCollapsedGridLayout(holder.menuItemGl, childMenuList, position);
                holder.menuTitleTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, collapsedHeadingTextSize);
                WidthAnimation heightAnimation = new WidthAnimation(holder.hLineView, 0, collapsedHeadingLineWidth);
                heightAnimation.setDuration(0);
                holder.hLineView.startAnimation(heightAnimation);
            } else {
                Log.d("addExpendedGridLayout", menuLabel);
                addExpendedGridLayout(holder, childMenuList, position);
                holder.menuTitleTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, expandHeadingTextSize);
                WidthAnimation heightAnimation = new WidthAnimation(holder.hLineView, 0, expandHeadingLineWidth);
                heightAnimation.setDuration(750);
                holder.hLineView.startAnimation(heightAnimation);
            }
        }
    }

    private void addCollapsedGridLayout(GridLayout gridLayout, List<MenuItem> menuItemList, int adapterPosition) {
        Log.d(TAG, "addCollapsedGridLayout: " + adapterPosition);
        int menuItemSize = menuItemList.size();
        int index;
        if (menuItemSize < 4) {
            for (index = 0; index < menuItemSize; index++) {
                MenuItem fantasticMenu = menuItemList.get(index);
                View menuChild = getCollapsedGridChild(gridLayout, fantasticMenu, index);
                gridLayout.addView(menuChild);
            }
        } else {
            for (index = 0; index < 3; index++) {
                MenuItem fantasticMenu = menuItemList.get(index);
                View menuChild = getCollapsedGridChild(gridLayout, fantasticMenu, index);
                gridLayout.addView(menuChild);
            }
            View menuChildMore = getViewMoreMenu(gridLayout, index, adapterPosition);
            gridLayout.addView(menuChildMore);
        }
    }


    private View getCollapsedGridChild(GridLayout gridLayout, MenuItem fantasticMenu, int index) {
        GridLayout.LayoutParams gridViewLayoutParams = new GridLayout.LayoutParams();
        gridViewLayoutParams.width = GridLayout.LayoutParams.MATCH_PARENT;
        gridViewLayoutParams.height = GridLayout.LayoutParams.WRAP_CONTENT;

        gridViewLayoutParams.rowSpec = GridLayout.spec(index);
        gridViewLayoutParams.columnSpec = GridLayout.spec(0, 0);

        TextView menuChildTv = (TextView) layoutInflater.inflate(R.layout.fantastic_menu_child_item, gridLayout, false);
        menuChildTv.setLayoutParams(gridViewLayoutParams);
        menuChildTv.setText(fantasticMenu.name);
        menuChildTv.setTag(fantasticMenu);
        menuChildTv.setOnClickListener(onMenuChildClickListener);
        return menuChildTv;
    }


    private void addExpendedGridLayout(ViewHolder holder, List<MenuItem> menuItemList, int adapterPosition) {
        Log.d(TAG, "addExpendedGridLayout: " + adapterPosition);
        GridLayout gridLayout = holder.menuItemGl;
        int menuItemSize = menuItemList.size();
        double r = menuItemSize / 2.0;

        int rowCount = (int) Math.ceil(r);

        int index;
        for (index = 0; index < menuItemSize; index++) {
            MenuItem fantasticMenu = menuItemList.get(index);
            View menuChild = getExpendedGridChild(gridLayout, fantasticMenu, index, rowCount);
            gridLayout.addView(menuChild);
        }
        View menuChildLess;
        int rowIndex;
        if (menuItemSize % 2 == 0) {
            rowIndex = rowCount;
        } else {
            rowIndex = rowCount - 1;
        }
        menuChildLess = getViewLessMenu(gridLayout, rowIndex, adapterPosition);
        gridLayout.addView(menuChildLess);

        middleMenuItemClickListener.onMenuExpended(holder.itemView);
    }

    private View getViewLessMenu(GridLayout gridLayout, int rowIndex, int adapterPosition) {
        Log.d(TAG, "getViewLessMenu: " + adapterPosition);

        if (TextUtils.isEmpty(labelViewLess)) {
            labelViewLess = context.getApplicationContext().getString(R.string.fmenu_view_less);
        }
        return getViewMoreLessMenu(gridLayout, rowIndex, adapterPosition, FantasyFragment.MENU_STATUS.EXPAND, labelViewLess);
    }


    private View getViewMoreMenu(GridLayout gridLayout, int rowIndex, int adapterPosition) {
        Log.d(TAG, "getViewMoreMenu: " + adapterPosition);
        if (TextUtils.isEmpty(labelViewMore)) {
            labelViewMore = context.getString(R.string.fmenu_view_more);
        }
        return getViewMoreLessMenu(gridLayout, rowIndex, adapterPosition, FantasyFragment.MENU_STATUS.COLLAPSE, labelViewMore);
    }

    private View getExpendedGridChild(GridLayout gridLayout, MenuItem fantasticMenu, int index, int rowCount) {
        int rowIndex, columnIndex;
        if (index < rowCount) {
            rowIndex = index;
            columnIndex = 0;
        } else {
            rowIndex = index - rowCount;
            columnIndex = 1;
        }

        GridLayout.LayoutParams gridViewLayoutParams = new GridLayout.LayoutParams();
        gridViewLayoutParams.width = GridLayout.LayoutParams.WRAP_CONTENT;
        gridViewLayoutParams.height = GridLayout.LayoutParams.WRAP_CONTENT;

        gridViewLayoutParams.rowSpec = GridLayout.spec(rowIndex);
        gridViewLayoutParams.columnSpec = GridLayout.spec(columnIndex, 1.f);

        TextView menuChildTv = (TextView) layoutInflater.inflate(R.layout.fantastic_menu_child_item, gridLayout, false);
        menuChildTv.setLayoutParams(gridViewLayoutParams);
        menuChildTv.setText(fantasticMenu.name);
        menuChildTv.setTag(fantasticMenu);
        menuChildTv.setOnClickListener(onMenuChildClickListener);
        return menuChildTv;
    }

    private final View.OnClickListener onMenuChildClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MenuItem fantasticMenu = (MenuItem) v.getTag();
            middleMenuItemClickListener.onMenuItemClick(fantasticMenu);
            Log.d(TAG, "CatInfo: " + fantasticMenu);
        }
    };

    private View getViewMoreLessMenu(GridLayout gridLayout, int rowIndex, int adapterPosition, FantasyFragment.MENU_STATUS currentMenuStatus,
                                     String label) {
        GridLayout.LayoutParams gridViewLayoutParams = new GridLayout.LayoutParams();
        gridViewLayoutParams.width = GridLayout.LayoutParams.WRAP_CONTENT;
        gridViewLayoutParams.height = GridLayout.LayoutParams.WRAP_CONTENT;

        gridViewLayoutParams.rowSpec = GridLayout.spec(rowIndex);
        gridViewLayoutParams.columnSpec = GridLayout.spec(1, 1.f);

        TextView menuChildTv = (TextView) layoutInflater.inflate(R.layout.fantastic_menu_child_more_item,
                gridLayout, false);

//        menuChildTv.setCustomFont(R.string.font_db_hindi_bold);

        menuChildTv.setLayoutParams(gridViewLayoutParams);
        menuChildTv.setText(label);
        menuChildTv.setId(adapterPosition);
        menuChildTv.setTag(currentMenuStatus);
        menuChildTv.setTypeface(Typeface.DEFAULT_BOLD);
        menuChildTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FantasyFragment.MENU_STATUS currentMenuStatus = (FantasyFragment.MENU_STATUS) v.getTag();
                int adapterPosition = v.getId();
                //gridLayout.removeAllViews();
                if (currentMenuStatus == FantasyFragment.MENU_STATUS.EXPAND) {
                    //addCollapsedGridLayout(gridLayout, middleMenuList.get(adapterPosition).getMenuItemList(), adapterPosition);
                    middleMenuItemClickListener.onClickMenuCollapse(adapterPosition);
                } else {
                    //addExpendedGridLayout(gridLayout, middleMenuList.get(adapterPosition).getMenuItemList(), adapterPosition);
                    middleMenuItemClickListener.onClickMenuExpand(adapterPosition);
                }
                /*menuChildTv.setTag(!isExpended);*/
            }
        });
        return menuChildTv;
    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView menuTitleTv;
        private GridLayout menuItemGl;
        private View hLineView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            menuTitleTv = itemView.findViewById(R.id.tv_menu_title);
            menuItemGl = itemView.findViewById(R.id.menu_item_gl);
            hLineView = itemView.findViewById(R.id.view_hline);
        }
    }
}
