package com.db.home.fantasyMenu.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.fantasyMenu.MenuItem;
import com.db.home.fantasyMenu.FantasyFragment;

import java.util.List;

public final class FantasyBottomMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MenuItem> bottomMenuList;
    private LayoutInflater layoutInflater;
    private Context context;
    private FantasyFragment.OnMenuItemClickListener onBottomMenuItemClickListener;
    private Drawable mUpArrowDrawable, mDownArrowDrawable;

    private static final int MENU_WITH_CHILD = 101;
    private static final int MENU_WITHOUT_CHILD = 202;

    public FantasyBottomMenuAdapter(Context context, List<MenuItem> bottomMenuList,
                                    FantasyFragment.OnMenuItemClickListener onBottomMenuItemClickListener) {
        this.layoutInflater = LayoutInflater.from(context);
        this.bottomMenuList = bottomMenuList;
        this.context = context;
        this.onBottomMenuItemClickListener = onBottomMenuItemClickListener;
        this.mUpArrowDrawable = context.getDrawable(R.drawable.ic_fmenu_arrow_up_black_24dp);
        this.mDownArrowDrawable = context.getDrawable(R.drawable.ic_fmenu_arrow_down_black_24dp);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MENU_WITH_CHILD) {
            return new MenuWithChildViewHolder(layoutInflater.inflate(R.layout.fantastic_bottom_menu_group, parent, false));
        } else {
            return new MenuWithOutChildViewHolder(layoutInflater.inflate(R.layout.fantastic_bottom_menu_header, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        MenuItem fantasticMenu = bottomMenuList.get(position);

        MenuWithOutChildViewHolder viewHolder = (MenuWithOutChildViewHolder) holder;

        viewHolder.parentText.setText(fantasticMenu.name);
        viewHolder.menuTitleRl.setTag(fantasticMenu);
        /////viewHolder.menuTitleRl.setId(position);

        int itemViewType = getItemViewType(position);

        if (itemViewType == MENU_WITH_CHILD) {
            MenuWithChildViewHolder childViewHolder = (MenuWithChildViewHolder) holder;
            childViewHolder.ivDropdownArrow.setVisibility(View.VISIBLE);
            List<MenuItem> menuList = fantasticMenu.subMenu;
            int menuChildSize = menuList.size();
            if (fantasticMenu.menuStatus == FantasyFragment.MENU_STATUS.EXPAND) {
                childViewHolder.ivDropdownArrow.setImageDrawable(mUpArrowDrawable);
                //holder.rvSubMenu.setVisibility(View.VISIBLE);

                childViewHolder.fantasticBottomSubMenuAdapter.setMenuChildList(menuList);
                //holder.rvSubMenu.setAdapter(holder.fantasticBottomSubMenuAdapter);
                childViewHolder.fantasticBottomSubMenuAdapter.notifyItemRangeInserted(0, menuChildSize);
            } else {
                childViewHolder.fantasticBottomSubMenuAdapter.clearMenuChildList();
                //holder.rvSubMenu.setAdapter(holder.fantasticBottomSubMenuAdapter);
                //childViewHolder.fantasticBottomSubMenuAdapter.notifyItemRangeRemoved(0, menuChildSize);
                childViewHolder.fantasticBottomSubMenuAdapter.notifyDataSetChanged();
                childViewHolder.ivDropdownArrow.setImageDrawable(mDownArrowDrawable);
                //holder.rvSubMenu.setVisibility(View.GONE);
            }
        } else {
            viewHolder.ivDropdownArrow.setVisibility(View.GONE);
        }

        viewHolder.menuTitleRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuItem fantasticMenuSelected = (MenuItem) view.getTag();

                List<MenuItem> menuList = fantasticMenuSelected.subMenu;
                int itemViewType;
                if (menuList != null && !menuList.isEmpty()) {
                    itemViewType = MENU_WITH_CHILD;
                } else
                    itemViewType = MENU_WITHOUT_CHILD;

                if (itemViewType == MENU_WITH_CHILD) {
                    MenuWithChildViewHolder childViewHolder = (MenuWithChildViewHolder) holder;

                    if (fantasticMenuSelected.menuStatus == FantasyFragment.MENU_STATUS.COLLAPSE) {
                        fantasticMenuSelected.menuStatus = (FantasyFragment.MENU_STATUS.EXPAND);
                        childViewHolder.ivDropdownArrow.setImageDrawable(mUpArrowDrawable);

                        for (int index = 0; index < bottomMenuList.size(); index++) {
                            MenuItem fantasticMenuItem = bottomMenuList.get(index);
                            if (!fantasticMenuItem.id.equals(fantasticMenuSelected.id) &&
                                    (fantasticMenuItem.menuStatus == FantasyFragment.MENU_STATUS.EXPAND)) {
                                fantasticMenuItem.menuStatus = (FantasyFragment.MENU_STATUS.COLLAPSE);
                                notifyItemChanged(index);
                                break;
                            }
                        }
                    } else {
                        fantasticMenuSelected.menuStatus = (FantasyFragment.MENU_STATUS.COLLAPSE);
                        childViewHolder.ivDropdownArrow.setImageDrawable(mDownArrowDrawable);
                    }

                    int indexOfSelected = bottomMenuList.indexOf(fantasticMenuSelected);

                    notifyItemChanged(indexOfSelected);
                } else {
                    onBottomMenuItemClickListener.onMenuItemClick(fantasticMenuSelected);
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        MenuItem fantasticMenu = bottomMenuList.get(position);
        List<MenuItem> menuList = fantasticMenu.subMenu;
        if (menuList != null && !menuList.isEmpty()) {
            return MENU_WITH_CHILD;
        } else
            return MENU_WITHOUT_CHILD;
    }

    @Override
    public int getItemCount() {
        return bottomMenuList.size();
    }

    class MenuWithOutChildViewHolder extends RecyclerView.ViewHolder {
        TextView parentText;
        ImageView ivDropdownArrow;
        ViewGroup menuTitleRl;

        MenuWithOutChildViewHolder(View itemView) {
            super(itemView);
            parentText = itemView.findViewById(R.id.tv_menu_title);
            ivDropdownArrow = itemView.findViewById(R.id.iv_dropdown_arrow);
            menuTitleRl = itemView.findViewById(R.id.rl_menu_title);
        }
    }

    class MenuWithChildViewHolder extends MenuWithOutChildViewHolder {
        private RecyclerView rvSubMenu;
        private FantasticBottomSubMenuAdapter fantasticBottomSubMenuAdapter;

        MenuWithChildViewHolder(View itemView) {
            super(itemView);
            rvSubMenu = itemView.findViewById(R.id.rv_sub_menu);

            rvSubMenu.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL,
                    false));
            fantasticBottomSubMenuAdapter = new FantasticBottomSubMenuAdapter(context, onBottomMenuItemClickListener);
            rvSubMenu.setAdapter(fantasticBottomSubMenuAdapter);
        }
    }
}

