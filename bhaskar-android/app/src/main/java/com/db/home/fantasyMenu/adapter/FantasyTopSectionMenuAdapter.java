package com.db.home.fantasyMenu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.fantasyMenu.MenuItem;
import com.db.home.fantasyMenu.FantasyFragment;
import com.db.util.ImageUtil;

import java.util.List;

public class FantasyTopSectionMenuAdapter extends RecyclerView.Adapter<FantasyTopSectionMenuAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<MenuItem> topMenuList;
    private FantasyFragment.OnMenuItemClickListener onTopMenuItemClickListener;


    public FantasyTopSectionMenuAdapter(Context context, List<MenuItem> topMenuList,
                                        FantasyFragment.OnMenuItemClickListener onTopMenuItemClickListener) {

        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onTopMenuItemClickListener = onTopMenuItemClickListener;
        this.topMenuList = topMenuList;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.fantastic_top_section_menu_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MenuItem topMenu = topMenuList.get(position);
        holder.tvTopSection.setText(topMenu.name);
        // Need PlaceHolder Here
        ImageUtil.setImage(context,topMenu.icon,holder.topMenuIconIv,R.drawable.menu_placeholder_color);
        holder.itemView.setTag(topMenu);
        holder.itemView.setOnClickListener(onClickListener);
    }

    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MenuItem fantasticMenu = (MenuItem) v.getTag();
            onTopMenuItemClickListener.onMenuItemClick(fantasticMenu);
        }
    };



    @Override
    public int getItemCount() {
        return topMenuList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTopSection;
        private ImageView topMenuIconIv;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            topMenuIconIv = itemView.findViewById(R.id.iv_top_menu_icon);
            tvTopSection = itemView.findViewById(R.id.tv_top_section);
        }
    }
}
