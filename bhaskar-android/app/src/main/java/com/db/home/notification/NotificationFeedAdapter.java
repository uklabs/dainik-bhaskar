package com.db.home.notification;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.database.ReadUnreadStatusTable;
import com.db.data.models.NotificationHubItemInfo;
import com.db.util.AppUtils;
import com.bhaskar.util.CssConstants;
import com.db.util.ImageUtil;

import java.util.ArrayList;
import java.util.List;

public class NotificationFeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final OnNotificationFeedItemListener listener;
    private final int VIEW_TYPE_MAIN = 1;
    String displayName;
    private Context mContext;
    private List<NotificationHubItemInfo> mNotificationFeed;
    private ReadUnreadStatusTable readUnreadStatusTable;

    public NotificationFeedAdapter(Context contexts, OnNotificationFeedItemListener listener, String displayName, ReadUnreadStatusTable readUnreadStatusTable) {
        this.mContext = contexts;
        this.listener = listener;
        this.displayName = displayName;
        this.mNotificationFeed = new ArrayList<>();
        this.displayName = displayName;
        this.readUnreadStatusTable = readUnreadStatusTable;
    }

    public void setData(List<NotificationHubItemInfo> newsFeed) {
        if (mNotificationFeed != null && newsFeed != null) {
            mNotificationFeed.clear();
            mNotificationFeed.addAll(newsFeed);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_MAIN:
                return new NotificationViewHolder(inflater.inflate(R.layout.notification_hub_list_item, parent, false));
        }
        return new NotificationViewHolder(inflater.inflate(R.layout.notification_hub_list_item, parent, false));
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        int itemViewType = holder.getItemViewType();
        switch (itemViewType) {
            case VIEW_TYPE_MAIN:
                final NotificationViewHolder notificationViewHolder = (NotificationViewHolder) holder;
                final NotificationHubItemInfo info = mNotificationFeed.get(notificationViewHolder.getAdapterPosition());

                if (info.getVideoFlag() == 1) {
                    notificationViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
                    notificationViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, CssConstants.themeColor));
                } else {
                    notificationViewHolder.imgVideoPlay.setVisibility(View.GONE);
                }

                ImageUtil.setImage(mContext, info.getImage(), notificationViewHolder.ivFeedImage, R.drawable.water_mark_news_list);
                notificationViewHolder.tvTitle.setText(info.getTitle());
                if (readUnreadStatusTable.isStoryWithVersionAvailable(AppUtils.getFilteredStoryId(info.storyId), info.version)) {
                    notificationViewHolder.tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.article_read_color));
                } else {
                    notificationViewHolder.tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.list_title_color));
                }
                if (info.readStatus == 0) {
                    notificationViewHolder.tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.article_title_color));
                } else {
                    notificationViewHolder.tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.article_title_color_read));
                }

                notificationViewHolder.itemView.setOnClickListener(view -> {
                    String filteredStoryId = AppUtils.getFilteredStoryId(info.storyId);
                    if (!readUnreadStatusTable.isStoryWithVersionAvailable(filteredStoryId, info.version)) {
                        readUnreadStatusTable.updateStoryVersion(filteredStoryId, info.version);
                    }
                    if (listener != null) {
                        listener.onItemClick(info, "", position);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_MAIN;
    }

    @Override
    public int getItemCount() {
        return mNotificationFeed == null ? 0 : mNotificationFeed.size();
    }


    public interface OnNotificationFeedItemListener {
        void onItemClick(NotificationHubItemInfo info, String newsUpdateTitle, int indexPosition);
    }

    private class NotificationViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivFeedImage;
        private ImageView imgVideoPlay;
        private TextView tvTitle;
        private ImageView ivDelete;

        private NotificationViewHolder(View itemView) {
            super(itemView);
            ivFeedImage = (ImageView) itemView.findViewById(R.id.thumb_image);
            imgVideoPlay = (ImageView) itemView.findViewById(R.id.img_video_play);
            tvTitle = (TextView) itemView.findViewById(R.id.list_item_name);
            ivDelete = itemView.findViewById(R.id.iv_delete);
            ivDelete.setVisibility(View.GONE);
        }
    }
}
