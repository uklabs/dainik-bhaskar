package com.db.home.notification;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.NotificationStackTable;
import com.db.data.database.ReadUnreadStatusTable;
import com.db.data.models.NotificationHubItemInfo;
import com.db.dbvideo.player.BusProvider;
import com.bhaskar.util.LoginController;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.Notificationutil;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;

import java.util.ArrayList;
import java.util.List;

public class NotificationFragment extends Fragment implements NotificationFeedAdapter.OnNotificationFeedItemListener {

    private static List<NotificationHubItemInfo> notificationFeedList;
    private SwipeRefreshLayout swipeRefreshLayout;
    private NotificationFeedAdapter notificationFeedAdapter;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private String gaArticle;
    private String gaScreen;
    private TextView tvNoDataAvailable;
    private boolean isbackPressed = false;
    private boolean launchFromStack;

    public NotificationFragment() {
    }

    public static NotificationFragment getInstance(boolean launchFromStack, String referrerType, String referrerOrigin, String referrerMedium) {
        NotificationFragment notificationFragment = new NotificationFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("launch", launchFromStack);
        notificationFragment.setArguments(bundle);
        return notificationFragment;
    }

    public static NotificationFragment getInstance(boolean launchFromStack) {
        NotificationFragment notificationFragment = new NotificationFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("launch", launchFromStack);
        notificationFragment.setArguments(bundle);
        return notificationFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BusProvider.getInstance().register(this);
        Bundle arguments = getArguments();
        if (arguments != null) {
            launchFromStack = arguments.getBoolean("launch");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View notificationView = inflater.inflate(R.layout.fragment_notification, container, false);
        tvNoDataAvailable = notificationView.findViewById(R.id.blank_text);
        isbackPressed = false;

        //Clear All Notification from notification Tray and sharedPreference
        Notificationutil.clearNotificationsTray(getActivity());
        Notificationutil.clearNotificationsStack(getActivity());


        recyclerView = notificationView.findViewById(R.id.notification_recycler_view);
        swipeRefreshLayout = notificationView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));

        progressBar = notificationView.findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        ReadUnreadStatusTable readUnreadStatusTable = (ReadUnreadStatusTable) DatabaseHelper.getInstance(getActivity()).getTableObject(ReadUnreadStatusTable.TABLE_NAME);
        notificationFeedAdapter = new NotificationFeedAdapter(getActivity(), this, gaScreen, readUnreadStatusTable);

        recyclerView.setAdapter(notificationFeedAdapter);
        if (notificationFeedList != null && notificationFeedList.size() > 0 && !Notificationutil.isDynamicNotificationAvailable) {
            progressBar.setVisibility(View.GONE);
            tvNoDataAvailable.setVisibility(View.GONE);
            notificationFeedAdapter.setData(notificationFeedList);
            notificationFeedAdapter.notifyDataSetChanged();
        } else {
            parseFeedList();
        }
        swipeRefreshLayout.setOnRefreshListener(this::parseFeedList);

        return notificationView;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.NOTIFICATION_COUNT, 0);
        Intent intent = new Intent(QuickPreferences.NOTI_CUSTOM_EVENT_NAME);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
//        btf_hp.setVisibility(View.GONE);
        if (isbackPressed) {
            if (notificationFeedList == null || notificationFeedList.isEmpty()) {
                NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(getActivity()).getTableObject(NotificationStackTable.TABLE_NAME);
                if (notificationStackTable.getAllNotificationStackTableList().size() != 0) {
                    notificationFeedList = notificationStackTable.getAllNotificationStackTableList();
                }
            }
            notificationFeedAdapter.setData(notificationFeedList);
            notificationFeedAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Rect scrollBounds = new Rect();
        recyclerView.getHitRect(scrollBounds);
        super.onViewCreated(view, savedInstanceState);
        if (launchFromStack) {
            gaArticle = CommonConstants.EVENT_LABEL + "-" + AppFlyerConst.AlertValue.STACK_ARTICLE_VALUE;
            gaScreen = CommonConstants.EVENT_LABEL + "-" + AppFlyerConst.AlertValue.STACK_SCREEN_VALUE;
        } else {
            gaArticle = CommonConstants.EVENT_LABEL + "-" + AppFlyerConst.AlertValue.ARTICLE_VALUE;
            gaScreen = CommonConstants.EVENT_LABEL + "-" + AppFlyerConst.AlertValue.SCREEN_VALUE;
        }

        // Tracking
        String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

        Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);

        String wisdomUrl = TrackingHost.getInstance(getContext()).getWisdomUrl(AppUrls.WISDOM_URL);
        String domain = AppUrls.DOMAIN;
//        String domain = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
        String url = domain + AppFlyerConst.AlertValue.SCREEN_VALUE;
        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
        String email = "";
        String mobile = "";
        if (profileInfo != null) {
            email = profileInfo.email;
            mobile = profileInfo.mobile;
        }
        Tracking.trackWisdomListingPage(getContext(), wisdomUrl, url, email, mobile, CommonConstants.BHASKAR_APP_ID);
        Systr.println("Wisdom : " + url);
    }

    private void parseFeedList() {
        if (notificationFeedList == null) {
            notificationFeedList = new ArrayList<>();
        } else {
            notificationFeedList.clear();
        }


        //Add stack Notification in ArrayList
        NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(getActivity()).getTableObject(NotificationStackTable.TABLE_NAME);
        notificationStackTable.deleteNotificationStackBefore48hrsArrayList();
        notificationFeedList = notificationStackTable.getAllNotificationStackTableList();

        notificationFeedAdapter.setData(notificationFeedList);
        notificationFeedAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

        if (notificationFeedAdapter.getItemCount() > 0) {
            tvNoDataAvailable.setVisibility(View.GONE);
        } else {
            tvNoDataAvailable.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onItemClick(NotificationHubItemInfo info, String newsUpdateTitle, int indexPosition) {

        notificationFeedAdapter.notifyItemChanged(indexPosition);

        if (info.readStatus == 0) {
            String eventType = "2";
            String aurl = "";
            String url = AppUrls.DOMAIN + AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN;

            Tracking.trackWisdomNotification(getContext(), TrackingHost.getInstance(getContext()).getWisdomUrlForNotification(AppUrls.WISDOM_NOTIFICATION_URL), TextUtils.isEmpty(info.catId) ? "1" : info.catId, info.title, Notificationutil.getNewsType(getContext(), info.storyId, (TextUtils.isEmpty(info.catId) ? "1" : info.catId)), eventType, info.storyId, aurl, url, CommonConstants.BHASKAR_APP_ID, info.channelSlno, AppFlyerConst.GAExtra.NOTIFICATION_WIDGET_NOTIFY_HUB);
        }


        Intent detailNews = new Intent(getContext(), AppUtils.getInstance().getArticleTypeClass());
        detailNews.putExtra(Constants.KeyPair.KEY_STORY_ID, info.storyId);
        detailNews.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, CommonConstants.CHANNEL_ID);
        detailNews.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID));
        detailNews.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, newsUpdateTitle);
        detailNews.putExtra(Constants.KeyPair.KEY_LIST_INDEX, indexPosition + 1);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        detailNews.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, AppUrls.DOMAIN);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, gaScreen);
        NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(getActivity()).getTableObject(NotificationStackTable.TABLE_NAME);
        notificationStackTable.updateReadStatus(info.storyId);
        updateLocalListReadStatus(info.storyId);
        detailNews.putExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN, AppFlyerConst.AlertValue.SCREEN_VALUE);
        getActivity().startActivityForResult(detailNews,Constants.REQ_CODE_BOTTOM_NAV);

    }

    private void updateLocalListReadStatus(String storyId) {
        if (notificationFeedList != null && notificationFeedList.contains(new NotificationHubItemInfo(storyId))) {
            notificationFeedList.get(notificationFeedList.indexOf(new NotificationHubItemInfo(storyId))).readStatus = 1;
        }
    }


    @Override
    public void onDestroyView() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        BusProvider.getInstance().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        isbackPressed = true;
        super.onStop();
    }
}
