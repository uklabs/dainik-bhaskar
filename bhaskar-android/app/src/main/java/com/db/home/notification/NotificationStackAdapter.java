package com.db.home.notification;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.NotificationHubItemInfo;
import com.db.listeners.OnItemClickListener;
import com.db.util.ImageUtil;

import java.util.ArrayList;
import java.util.List;


public class NotificationStackAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final OnNotificationStackItemListener listener;
    private final int VIEW_TYPE_MAIN = 1;
    String displayName;
    private Context mContext;
    private List<NotificationHubItemInfo> mNotificationFeed;

    public NotificationStackAdapter(Context contexts, OnNotificationStackItemListener listener, String displayName) {
        this.mContext = contexts;
        this.listener = listener;
        this.displayName = displayName;
        this.mNotificationFeed = new ArrayList<>();
        this.displayName = displayName;
    }

    public void setData(List<NotificationHubItemInfo> newsFeed) {
        mNotificationFeed.clear();
        mNotificationFeed.addAll(newsFeed);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_MAIN:
                return new NotificationViewHolder(inflater.inflate(R.layout.notification_hub_list_item, parent, false));
        }
        return new NotificationViewHolder(inflater.inflate(R.layout.notification_hub_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        int itemViewType = holder.getItemViewType();
        switch (itemViewType) {
            case VIEW_TYPE_MAIN:
                final NotificationViewHolder notificationViewHolder = (NotificationViewHolder) holder;
                final NotificationHubItemInfo info = mNotificationFeed.get(notificationViewHolder.getAdapterPosition());

                if (info.getVideoFlag() == 1) {
                    notificationViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
                } else {
                    notificationViewHolder.imgVideoPlay.setVisibility(View.GONE);
                }

                ImageUtil.setImage(mContext, info.getImage(), notificationViewHolder.ivFeedImage, R.drawable.water_mark_news_list);
                notificationViewHolder.tvTitle.setText(info.getTitle());
                if (info.readStatus == 0) {
                    notificationViewHolder.tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.article_title_color));
                } else {
                    notificationViewHolder.tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.article_title_color_read));

                }
                notificationViewHolder.setClickListener(new OnItemClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        if (listener != null) {
                            listener.onItemClick(info.getStoryId(), "", info.getNotificationId(), position);
                        }
                    }
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_MAIN;
    }

    @Override
    public int getItemCount() {
        return mNotificationFeed == null ? 0 : mNotificationFeed.size();
    }


    public interface OnNotificationStackItemListener {
        void onItemClick(String storyId, String newsUpdateTitle, String notificationId, int position);
    }

    private class ViewHolderAds extends RecyclerView.ViewHolder {
        private ViewHolderAds(View itemView) {
            super(itemView);
        }
    }

    private class NotificationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private ImageView ivFeedImage;
        private ImageView imgVideoPlay;
        private TextView tvTitle;
        private RelativeLayout relativeLayout;

        private ImageView ivDelete;
        private OnItemClickListener clickListener;

        private NotificationViewHolder(View itemView) {
            super(itemView);
            ivFeedImage = (ImageView) itemView.findViewById(R.id.thumb_image);
            imgVideoPlay = (ImageView) itemView.findViewById(R.id.img_video_play);
            tvTitle = (TextView) itemView.findViewById(R.id.list_item_name);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.text_image_layout);

            ivDelete = itemView.findViewById(R.id.iv_delete);
            ivDelete.setVisibility(View.GONE);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        private void setClickListener(OnItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            return true;
        }
    }
}
