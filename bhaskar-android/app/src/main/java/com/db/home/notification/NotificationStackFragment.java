package com.db.home.notification;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.NotificationStackTable;
import com.db.data.models.NotificationHubItemInfo;
import com.bhaskar.util.LoginController;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.Notificationutil;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;

import java.util.ArrayList;
import java.util.List;

public class NotificationStackFragment extends Fragment implements NotificationStackAdapter.OnNotificationStackItemListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private NotificationStackAdapter notificationStackAdapter;
    private ProgressBar progressBar;
    private List<NotificationHubItemInfo> notificationFeedList;
    private RecyclerView recyclerView;
    private String gaArticle;
    private String gaScreen;
    private TextView tvNoDataAvailable;

    public NotificationStackFragment() {
    }

    public static NotificationStackFragment getInstance() {
        return new NotificationStackFragment();
    }

    LinearLayoutManager linearLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View notificationView = inflater.inflate(R.layout.fragment_notification, container, false);
        //Clear All Notification from notification Tray and sharedPreference
        Notificationutil.clearNotificationsTray(getActivity());
        Notificationutil.clearNotificationsStack(getActivity());

        tvNoDataAvailable = notificationView.findViewById(R.id.blank_text);

        recyclerView = notificationView.findViewById(R.id.notification_recycler_view);
        swipeRefreshLayout = notificationView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));

        progressBar = notificationView.findViewById(R.id.progress_bar);

        linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        notificationStackAdapter = new NotificationStackAdapter(getActivity(), this, gaScreen);
        recyclerView.setAdapter(notificationStackAdapter);
        setNotificationStackData();


        swipeRefreshLayout.setOnRefreshListener(() -> {
            notificationFeedList = new ArrayList<>();
            setNotificationStackData();
        });

        return notificationView;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.NOTIFICATION_COUNT, 0);
        Intent intent = new Intent(QuickPreferences.NOTI_CUSTOM_EVENT_NAME);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

        NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(getActivity()).getTableObject(NotificationStackTable.TABLE_NAME);
        notificationFeedList = notificationStackTable.getAllNotificationStackTableList();
        notificationStackAdapter.setData(notificationFeedList);
        notificationStackAdapter.notifyDataSetChanged();

    }

    Rect scrollBounds;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        scrollBounds = new Rect();
        recyclerView.getHitRect(scrollBounds);
        gaArticle = CommonConstants.EVENT_LABEL + "-" + AppFlyerConst.AlertValue.ARTICLE_VALUE;
        gaScreen = CommonConstants.EVENT_LABEL + "-" + AppFlyerConst.AlertValue.SCREEN_VALUE;

        // Tracking
        String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);

        String wisdomUrl = TrackingHost.getInstance(getContext()).getWisdomUrl(AppUrls.WISDOM_URL);
        String domain = AppUrls.DOMAIN;
        String url = domain + AppFlyerConst.AlertValue.SCREEN_VALUE;
        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
        String email = "";
        String mobile = "";
        if(profileInfo!=null){
            email = profileInfo.email;
            mobile = profileInfo.mobile;
        }
        Tracking.trackWisdomListingPage(getContext(), wisdomUrl, url, email, mobile, CommonConstants.BHASKAR_APP_ID);
        Systr.println("Wisdom : " + url);
    }

    private void setNotificationStackData() {
        if (notificationFeedList == null) {
            notificationFeedList = new ArrayList<>();
        } else {
            notificationFeedList.clear();
        }

        NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(getActivity()).getTableObject(NotificationStackTable.TABLE_NAME);
        notificationStackTable.deleteNotificationStackBefore48hrsArrayList();
        notificationFeedList = notificationStackTable.getAllNotificationStackTableList();
        if (notificationFeedList.size() > 0) {
            tvNoDataAvailable.setVisibility(View.GONE);
        } else {
            tvNoDataAvailable.setVisibility(View.VISIBLE);
            tvNoDataAvailable.setText("No new notification");

        }

        notificationStackAdapter.setData(notificationFeedList);
        notificationStackAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onItemClick(String storyId, String newsUpdateTitle, String notificationId, int position) {


        Intent detailNews = new Intent(getContext(), AppUtils.getInstance().getArticleTypeClass());
        detailNews.putExtra(Constants.KeyPair.KEY_STORY_ID, storyId);
        detailNews.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, CommonConstants.CHANNEL_ID);
        detailNews.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID));
        detailNews.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, newsUpdateTitle);
        detailNews.putExtra(Constants.KeyPair.KEY_LIST_INDEX, position + 1);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        detailNews.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, AppUrls.DOMAIN);
        detailNews.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, gaScreen);

        NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(getActivity()).getTableObject(NotificationStackTable.TABLE_NAME);
        notificationStackTable.updateReadStatus(notificationId);

        notificationStackAdapter.notifyDataSetChanged();
        detailNews.putExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN, AppFlyerConst.AlertValue.SCREEN_VALUE);
        startActivity(detailNews);
    }


    @Override
    public void onDestroyView() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        ViewGroup mContainer = getActivity().findViewById(R.id.frame_layout);
        mContainer.removeAllViews();
        super.onDestroyView();
    }
}
