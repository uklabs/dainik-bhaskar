package com.db.homebanner;

import android.content.Context;
import android.net.http.SslError;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.util.LoginController;
import com.bhaskar.R;
import com.db.controller.WebController;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.divya_new.common.TabController;
import com.db.home.ActivityUtil;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.bhaskar.util.Action;
import com.db.util.AppLogs;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.JsonParser;
import com.db.util.OpenNewsLinkInApp;

//Lalit
public class BannerViewHolder extends RecyclerView.ViewHolder implements OnFeedFetchFromServerListener {
    private static final String TAG = "BannerViewHolder";
    public LinearLayout mainLayout;
    public WebView webView;
    public ImageView imageView;
    public ImageView moreIV;
    private BannerInfo mBannerInfo;
    private Context mContext;

    public BannerViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        mainLayout = itemView.findViewById(R.id.cv_homeBanner);
        webView = itemView.findViewById(R.id.wv_homeBanner);
        imageView = itemView.findViewById(R.id.iv_homeBanner);
        moreIV = itemView.findViewById(R.id.more_img);
        WebController.getWebController().setWebViewSettings(mContext, webView);
        setWebViewClient();
    }

    private void setWebViewClient() {
        // webview config
        webView.setWebViewClient(new DbWebViewClient());
        webView.setWebChromeClient(new DbWebChromeClient());
    }


    public void callwebViewOrSetWidgetData(BannerInfo mBannerInfo) {
        if (this.mBannerInfo != null && this.mBannerInfo == mBannerInfo) {
            return;
        }

        this.mBannerInfo = mBannerInfo;

        new Handler().postDelayed(() -> {

            if (!TextUtils.isEmpty(mBannerInfo.bannerUrl)) {
                if (mBannerInfo.enableImgBanner.equalsIgnoreCase("1")) {
                    mainLayout.setVisibility(View.VISIBLE);
                    imageView.setVisibility(View.VISIBLE);
                    webView.setVisibility(View.GONE);
                    try {
                        ImageUtil.setImageOverride(mContext, mBannerInfo.bannerUrl, imageView, 0);
                    } catch (Exception ignored) {
                    }
                    imageView.setOnClickListener(v -> {
                        if (!TextUtils.isEmpty(mBannerInfo.actionUrl)
                                || !(TextUtils.isEmpty(mBannerInfo.actionMenuId) || mBannerInfo.actionMenuId.equalsIgnoreCase("0")))
                            onBannerClick();
                    });
                } else {
                    imageView.setVisibility(View.GONE);
                    webView.setVisibility(View.VISIBLE);
                    webView.loadUrl(mBannerInfo.bannerUrl);
                    mainLayout.setVisibility(View.VISIBLE);
                    if (!TextUtils.isEmpty(mBannerInfo.actionUrl)
                            || !(TextUtils.isEmpty(mBannerInfo.actionMenuId) || mBannerInfo.actionMenuId.equalsIgnoreCase("0")))
                        webView.setOnTouchListener(new View.OnTouchListener() {
                            private float startX;
                            private float startY;

                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                switch (event.getAction()) {
                                    case MotionEvent.ACTION_DOWN:
                                        startX = event.getX();
                                        startY = event.getY();
                                        break;
                                    case MotionEvent.ACTION_UP:
                                        float endX = event.getX();
                                        float endY = event.getY();
                                        if (AppUtils.getInstance().isAClick(startX, endX, startY, endY)) {
                                            onBannerClick();
                                        }
                                        break;
                                }
                                return true;
                            }
                        });
                }
            } else {
                mainLayout.setVisibility(View.GONE);
            }
        }, 0);
    }

    private void onBannerClick() {
        try {
            CleverTapDB.getInstance(mContext).cleverTapTrackEvent(mContext, CTConstant.EVENT_NAME.CONTAINER_CLICKS, CTConstant.PROP_NAME.CONTAINER_NAME, mBannerInfo.gaScreen, LoginController.getUserDataMap());
        } catch (Exception ignore) {
        }

        if (!mBannerInfo.actionMenuId.equalsIgnoreCase("0") && !TextUtils.isEmpty(mBannerInfo.actionMenuId)) {
            bannerClickHandle(mContext, mBannerInfo.actionMenuId);
        } else if (!TextUtils.isEmpty(mBannerInfo.actionUrl)) {
            OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(mContext, mBannerInfo.actionUrl, mBannerInfo.actionName, 1, mBannerInfo.gaScreen, null);
            openNewsDetailInApp.openLink();
        }
    }

    public static void bannerClickHandle(Context mContext, String actionMenuId) {
        CategoryInfo categoryInfo = JsonParser.getInstance().getCategoryInfoById(mContext, actionMenuId);
        if (categoryInfo != null) {
            if (Action.OUTSIDE_OPEN_CATEGORY_ACTIONS.contains(categoryInfo.action)) {
                ActivityUtil.openCategoryActionOutside(mContext, categoryInfo);
            } else {
                if (!categoryInfo.parentId.equalsIgnoreCase("0")) {
                    new Handler().postDelayed(() -> {
                        try {
                            TabController.getInstance().setTabClicked(true);
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(categoryInfo.parentId, categoryInfo.id, null);
                        } catch (Exception e) {
                        }
                    }, 200);
                } else {
                    new Handler().postDelayed(() -> {
                        try {
                            TabController.getInstance().setTabClicked(true);
                            TabController.getInstance().getMenuTabControllerInstance().onMenuTabClick(categoryInfo.id, null);
                        } catch (Exception e) {
                        }
                    }, 200);
                }
            }
        }
    }

    @Override
    public void onDataFetch(boolean flag) {
    }

    @Override
    public void onDataFetch(boolean flag, String sectionName) {
        // Empty Method
    }

    @Override
    public void onDataFetch(boolean flag, int viewType) {
        // Empty Method
    }

    @Override
    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {

    }

    private class DbWebChromeClient extends android.webkit.WebChromeClient {

    }

    private class DbWebViewClient extends WebViewClient {

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            if (handler != null)
                handler.cancel();
        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (!TextUtils.isEmpty(url)) {
                OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(mContext, url, Constants.KeyPair.KEY_NEWS_UPDATE, 1, "", null);
                openNewsDetailInApp.openLink();
            }
            return true;
        }


        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            AppLogs.printDebugLogs("onPageFinished URL", url);
            if (url != null && url.contains("failure=1")) {
                mainLayout.setVisibility(View.GONE);
            } else {
                mainLayout.setVisibility(View.VISIBLE);
            }
            super.onPageFinished(view, url);
        }


    }

}
