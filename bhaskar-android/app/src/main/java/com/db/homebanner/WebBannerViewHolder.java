package com.db.homebanner;

import android.content.Context;
import android.net.http.SslError;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.controller.WebController;
import com.db.data.models.WebBannerInfo;
import com.db.util.AppLogs;
import com.db.util.Constants;
import com.db.util.OpenNewsLinkInAppV2;

public class WebBannerViewHolder extends RecyclerView.ViewHolder {
    public WebView webView;
    private WebBannerInfo webBannerInfo;
    private Context mContext;

    public WebBannerViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        webView = itemView.findViewById(R.id.wv_homeBanner);
        WebController.getWebController().setWebViewSettings(mContext, webView);
        setWebViewClient();
    }

    private void setWebViewClient() {
        // webview config
        webView.setWebViewClient(new DbWebViewClient());
        webView.setWebChromeClient(new DbWebChromeClient());
    }

    public void callwebViewOrSetWidgetData(WebBannerInfo mBannerInfo) {
        if(this.webBannerInfo != null && this.webBannerInfo == mBannerInfo)
            return;
        this.webBannerInfo = mBannerInfo;
        new Handler().postDelayed(() -> {

            if (!TextUtils.isEmpty(mBannerInfo.bannerUrl)) {
                webView.setVisibility(View.VISIBLE);
                webView.loadUrl(mBannerInfo.bannerUrl);
                webView.setVisibility(View.VISIBLE);
            } else {
                webView.setVisibility(View.GONE);
            }
        }, 0);
    }

    private class DbWebChromeClient extends android.webkit.WebChromeClient {

    }

    private class DbWebViewClient extends WebViewClient {

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            if (handler != null)
                handler.cancel();
        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            OpenNewsLinkInAppV2 openNewsLinkInApp = new OpenNewsLinkInAppV2(mContext, url, Constants.KeyPair.KEY_NEWS_UPDATE, 1);
            openNewsLinkInApp.openLink();
            AppLogs.printDebugLogs("webload url", url);
            return true;
        }


        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            AppLogs.printDebugLogs("onPageFinished URL", url);
            if (url != null && url.contains("failure=1")) {
                webView.setVisibility(View.GONE);
            } else {
                webView.setVisibility(View.VISIBLE);
            }

            super.onPageFinished(view, url);
        }


    }

}
