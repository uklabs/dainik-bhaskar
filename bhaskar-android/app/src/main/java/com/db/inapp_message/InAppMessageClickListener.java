package com.db.inapp_message;

import android.content.Context;
import android.util.Log;

import com.db.util.OpenNewsLinkInApp;
import com.google.firebase.inappmessaging.FirebaseInAppMessagingClickListener;
import com.google.firebase.inappmessaging.model.Action;
import com.google.firebase.inappmessaging.model.CampaignMetadata;
import com.google.firebase.inappmessaging.model.InAppMessage;

public class InAppMessageClickListener implements FirebaseInAppMessagingClickListener {
    private Context mContext;

    public InAppMessageClickListener(Context context) {
        mContext = context;
    }

    @Override
    public void messageClicked(InAppMessage inAppMessage, Action action) {
        // Determine which URL the user clicked
        String url = action.getActionUrl();

        // Get general information about the campaign
        CampaignMetadata metadata = inAppMessage.getCampaignMetadata();
        OpenNewsLinkInApp openNewsLinkInApp = new OpenNewsLinkInApp(mContext, url, metadata.getCampaignName(), 0, "", null);
        openNewsLinkInApp.openLink();
        String mesa = "Id=> " + metadata.getCampaignId() + "Name=> " + metadata.getCampaignName() + "msg=> " + metadata.getIsTestMessage();
        Log.e("InAppMessaging", "Data=> " + mesa);
    }
}
