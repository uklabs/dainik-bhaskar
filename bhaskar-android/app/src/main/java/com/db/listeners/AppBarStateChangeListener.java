package com.db.listeners;

import com.google.android.material.appbar.AppBarLayout;

import com.db.util.AppLogs;

public abstract class AppBarStateChangeListener implements AppBarLayout.OnOffsetChangedListener {

    public final static int EXPANDED = 1, COLLAPSED = 2, IDLE = 0;

    private int mCurrentState = IDLE;

    @Override
    public final void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (i == 0) {
            if (mCurrentState != EXPANDED) {
                onStateChanged(appBarLayout, EXPANDED);
            }
            mCurrentState = EXPANDED;
            AppLogs.printDebugLogs("APPBAR", "EXPANDED");
        } else if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
            if (mCurrentState != COLLAPSED) {
                onStateChanged(appBarLayout, COLLAPSED);
            }
            mCurrentState = COLLAPSED;
            AppLogs.printDebugLogs("APPBAR", "COLAPSED");
        } else {
            if (mCurrentState != IDLE) {
                onStateChanged(appBarLayout, IDLE);
            }
            mCurrentState = IDLE;
            AppLogs.printDebugLogs("APPBAR", "IDLE");
        }
    }

    public abstract void onStateChanged(AppBarLayout appBarLayout, int state);
}
