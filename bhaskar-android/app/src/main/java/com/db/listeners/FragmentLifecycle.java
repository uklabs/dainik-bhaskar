package com.db.listeners;

/**
 * Created by DB on 22-08-2017.
 */

public interface FragmentLifecycle {
    void onPauseFragment();

    void onResumeFragment();
}
