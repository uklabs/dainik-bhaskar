package com.db.listeners;

/**
 * Created by DB on 22-08-2017.
 */

public interface FragmentLifecycleArticle {
    void onPauseFragment();

    void onResumeFragment(String prevStoryId, int position);
}
