package com.db.listeners;

import com.db.divya_new.articlePage.ArticleCommonInfo;

public interface OnArticleOperationClickListener {
    void onCommentClick(ArticleCommonInfo articleCommonInfo);

    void onBookmarkClick(ArticleCommonInfo articleCommonInfo);
}
