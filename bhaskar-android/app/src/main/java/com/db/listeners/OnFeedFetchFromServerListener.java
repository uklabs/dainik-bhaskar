package com.db.listeners;


import com.db.data.models.CategoryInfo;

public interface OnFeedFetchFromServerListener {

    void onDataFetch(boolean flag);

    void onDataFetch(boolean flag, String sectionName);

    void onDataFetch(boolean flag, int viewType);

    void onDataFetch(boolean flag, CategoryInfo categoryInfo);
}
