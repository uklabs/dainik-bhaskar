package com.db.listeners;

import android.view.View;

public interface OnItemClickListener {
    void onClick(View view, int position);
}
