package com.db.listeners;


import com.db.data.models.RelatedArticleInfo;

public interface OnRelatedArticleClickListener {

    void OnRelatedArticleClick(String storyId, String channel_Slno, int position, String color, String headerName);

    void OnRelatedArticleClick(RelatedArticleInfo relatedArticleInfo);

}
