package com.db.listeners;


public interface OnStoryShareUrlListener {
    void onUrlFetch(boolean flag, String shareUrl);

}
