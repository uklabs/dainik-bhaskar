package com.db.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.db.core.CoreActivity;
import com.db.divya_new.divyashree.ImageController;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.QuickPreferences;
import com.google.android.play.core.splitcompat.SplitCompat;

import java.io.File;

public abstract class BaseAppCompatActivity extends CoreActivity {
    public static final int FILE_SELECT_CODE_KUNDLI = 102;
    public static final String INTENT_FILTER_ACTIVITY_RESULT = "activity_result";
    public static final String INTENT_FILTER_OVERLAY_RESULT = "activity_result";
    public static final String INTENT_FILTER_LIST_UPDATE = "list_update";
    public static final String IS_APPROVED = "is_approved";
    public static final String FILE_PATH = "file_path";
    public final static int REQUEST_CODE = 901;
    public static int screenSize;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.getInstance().setStatusBarColor(this, ContextCompat.getColor(this, R.color.divya_theme_color));

        Tracking.onCreate(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        SplitCompat.installActivity(this);
    }

    /**
     * file chooser intent
     *
     * @param requestCode
     */
    public void showFileChooser(int requestCode) {
        String[] mimeTypes1 =
                {"application/pdf", "application/msword", "application/vnd.ms-powerpoint", "application/vnd.ms-excel", "text/plain"};

        String[] mimeTypes2 =
                {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                        "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                        "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                        "text/plain",
                        "application/pdf",
                        "application/zip"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes1.length == 1 ? mimeTypes1[0] : "*/*");
            if (mimeTypes1.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes1);
            }
        } else {
            StringBuilder mimeTypesStr = new StringBuilder();
            for (String mimeType : mimeTypes1) {
                mimeTypesStr.append(mimeType).append("|");
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), requestCode);
        } catch (Exception ex) {
            // Potentially direct the user to the Market with a Dialog
            AppUtils.getInstance().showCustomToast(this, "Please install a File Manager.");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkDrawOverlayPermission() {
        /** check if we already  have permission to draw over other apps */
        if (!Settings.canDrawOverlays(this)) {
            /** if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            /** request permission via start activity for result */
            this.startActivityForResult(intent, REQUEST_CODE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {
                case FILE_SELECT_CODE_KUNDLI:
                    if (resultCode == RESULT_OK) {
                        Uri uri = data.getData();
                        if (uri == null)
                            return;
                        try {
                            String filepath = ImageController.getPath(this, uri);
                            fireLocalBroadCast(filepath);
                        } catch (Exception e) {
                            e.printStackTrace();
                            File file = new File(uri.getPath());//create path from uri
                            final String[] split = file.getPath().split(":");//split the path.
                            String filePath = split[1];//assign it to a string(your choice).
                            fireLocalBroadCast(filePath);
                        }
                    }
                    break;
                case REQUEST_CODE:
                    if (Settings.canDrawOverlays(this)) {
                        // continue here - permission was granted
                        fireOverlayLocalBroadCast(true);
                    } else {
                        fireOverlayLocalBroadCast(false);
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppUtils.getInstance().showCustomToast(this, "" + e.getMessage());
        }
    }

    private void fireLocalBroadCast(String filepath) {
        Intent intent = new Intent(INTENT_FILTER_ACTIVITY_RESULT);
        intent.putExtra(FILE_PATH, filepath);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void fireOverlayLocalBroadCast(boolean isApproved) {
        Intent intent = new Intent(INTENT_FILTER_OVERLAY_RESULT);
        intent.putExtra(IS_APPROVED, isApproved);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Tracking.onStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isBlockedCountry = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.onResume(this, isBlockedCountry);
    }

    @Override
    protected void onPause() {
        boolean isBlockedCountry = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        Tracking.onPause(this, isBlockedCountry);
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Tracking.onStop(this);
    }

    @Override
    protected void onDestroy() {
        Tracking.onDestroy(this);
        super.onDestroy();
    }
}
