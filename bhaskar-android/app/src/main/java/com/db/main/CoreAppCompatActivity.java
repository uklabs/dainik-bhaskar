package com.db.main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.Action;
import com.bhaskar.util.LoginController;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.db.data.models.CategoryInfo;
import com.db.data.models.fantasyMenu.MenuItem;
import com.db.database.BottomNavInfo;
import com.db.divya_new.articlePage.DivyaCommonRootArticleFragment;
import com.db.divya_new.articlePage.DivyaCommonRootArticleFragmentVertical;
import com.db.home.ActivityUtil;
import com.db.home.BottomNavGridAdapter;
import com.db.home.NewsFragment;
import com.bhaskar.view.ui.news.NewsListFragment;
import com.db.home.fantasyMenu.FantasyFragment;
import com.db.listeners.OnCategoryClickListener;
import com.db.listeners.OnDBVideoClickListener;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.bhaskar.view.ui.myPage.MeraPageNewsFragment;
import com.db.news.PrefferedCityNewsListFragment;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.FragmentUtil;
import com.db.util.ImageUtil;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;
import com.db.util.ThemeUtil;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static android.view.View.GONE;


public class CoreAppCompatActivity extends BaseAppCompatActivity implements OnCategoryClickListener, OnDBVideoClickListener, OnFeedFetchFromServerListener, FantasyFragment.OnActionListener {

    /*Request Codes*/
    public static final int REQUEST_CODE_CALL_FOR_FOOTER_TAB = 12121;

    /*Bottom Navigation Variables*/
    private static final int MAX_NAVIGATION_ITEMS = 5;

    public int currentBottomPosition = -1;

    public final int LOGIN_REQUEST_CODE = 445;

    /*Native component*/
    public RelativeLayout progressBarLayout;
    boolean isActivityStop = false;

    @SuppressLint("HandlerLeak")
    private ArrayList<BottomNavInfo> bottomNavInfosMain;
    private ArrayList<BottomNavInfo> bottomNavInfosMore;
    /*Fragment instance*/
    private DrawerLayout drawer;
    private View moreLayoutDivider;

    private RecyclerView moreBottomRV;
    private ProfileInfo mProfileInfo;
    private TabLayout bottomTabLayout;


    private boolean bundleIsFromWidget;
    private boolean bundleIsFromPush;
    private boolean bundleIsDeeplink;
    private String bundleFrom;
    private boolean isFirstTime = true;
//    private ViewBottomBehavior viewBottomBehavior;

    public static int IN_APP_UPDATE_REQUEST_CODE = 9090;
    private RelativeLayout frame_data_layout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tracking.onCreate(this);
        setTheme(ThemeUtil.getCurrentTheme(this));
        currentBottomPosition = AppPreferences.getInstance(this).getIntValue("bottomPosition", -1);
        setContentView(R.layout.activity_core);

        getBundleData();
        // Tool Bar
        drawer = findViewById(R.id.drawer_layout);
        frame_data_layout = findViewById(R.id.frame_data_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        DrawerLayout.LayoutParams layoutParams = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        layoutParams.width = displayMetrics.widthPixels;
        navigationView.setLayoutParams(layoutParams);
//        viewBottomBehavior = new ViewBottomBehavior(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void setDrawerArrowDrawable(@NonNull DrawerArrowDrawable drawable) {
                super.setDrawerArrowDrawable(drawable);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                if (newState == DrawerLayout.STATE_DRAGGING && !drawer.isDrawerOpen(GravityCompat.START)) {
                    String campaign = AppPreferences.getInstance(CoreAppCompatActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                    String gAaction = AppFlyerConst.GALabel.DVB_CLICK;
                    Tracking.trackGAEvent(CoreAppCompatActivity.this, ((InitApplication) getApplication()).getDefaultTracker(), AppFlyerConst.GACategory.MENU, gAaction, AppFlyerConst.GALabel.SWIPE, campaign);
                }
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);

        // :: /*initializeViews*/
        initializeViews();

        // :: /*initializeBottomNavigationView*/
        initializeBottomNavigationView();

        mProfileInfo = LoginPreferences.getInstance(this).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        initializeNavigationDrawer();
        boolean isVerticle = AppPreferences.getInstance(this).getBooleanValue(Constants.KeyPair.KEY_IS_VERTICAL, true);
        boolean isMarginAllowed = AppPreferences.getInstance(this).getBooleanValue(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE, false);
        setBottomBehaviour(isMarginAllowed);
        if (isVerticle) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), R.id.frame_layout, DivyaCommonRootArticleFragmentVertical.getInstance(getIntent().getExtras()), false, false);
        } else {
            FragmentUtil.changeFragment(getSupportFragmentManager(), R.id.frame_layout, DivyaCommonRootArticleFragment.getInstance(getIntent().getExtras()), false, false);
        }

        if (bundleIsFromPush || bundleIsDeeplink || bundleIsFromWidget) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            bottomTabLayout.setVisibility(View.GONE);
            AppPreferences.getInstance(this).setBooleanValue("isBottomShowing", false);
            CoordinatorLayout.LayoutParams relativeParams = (CoordinatorLayout.LayoutParams) frame_data_layout.getLayoutParams();
            relativeParams.setMargins(0, 0, 0, 0);  // left, top, right, bottom
            frame_data_layout.setLayoutParams(relativeParams);
        } else {
            if (bottomNavInfosMain.size() > 1) {
                AppPreferences.getInstance(this).setBooleanValue("isBottomShowing", true);
                CoordinatorLayout.LayoutParams relativeParams = (CoordinatorLayout.LayoutParams) frame_data_layout.getLayoutParams();

                int marginBottom = (int) AppUtils.getInstance().convertDpToPixel(56, this);
                relativeParams.setMargins(0, 0, 0, marginBottom);  // left, top, right, bottom
                frame_data_layout.setLayoutParams(relativeParams);
            }
        }
    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            bundleFrom = bundle.getString(Constants.KeyPair.KEY_FROM);
            if (!TextUtils.isEmpty(bundleFrom)) {
                bundleIsDeeplink = bundleFrom.equalsIgnoreCase(Constants.DeepLinking.DEEPLINK);
            }
            bundleIsFromWidget = bundle.getBoolean(Constants.KeyPair.KEY_IS_FROM_APPWIDGET);
            bundleIsFromPush = bundle.getBoolean(Constants.KeyPair.KEY_IS_NOTIFICATION);
            if (bundleIsDeeplink || bundleIsFromWidget || bundleIsFromPush) {
                //saveBottomPosition(-1);
            }
        }
    }

    private void setBottomBehaviour(boolean isMarginAllowed) {
//        LinearLayout yourView = findViewById(R.id.bottom_bar_layout);
//        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) yourView.getLayoutParams();
//        if (isMarginAllowed)
//            params.setBehavior(viewBottomBehavior);
//        else {
//            params.setBehavior(null);
//        }
//        yourView.requestLayout();
    }


    private void initializeViews() {
        progressBarLayout = findViewById(R.id.progress_bar_layout);
        moreBottomRV = findViewById(R.id.more_layout);
        moreLayoutDivider = findViewById(R.id.more_layout_divider);
    }


    private void initializeBottomNavigationView() {
        /* Bottom Navigation */
        ArrayList<TabLayout.Tab> bottomNavigationItems = new ArrayList<>(MAX_NAVIGATION_ITEMS);
        bottomNavInfosMain = new ArrayList<>(MAX_NAVIGATION_ITEMS);

        // init Bottom view
        bottomTabLayout = findViewById(R.id.bottom_navigation);
        bottomTabLayout.removeAllTabs();
        /*Dynamic Bottom Item*/
        boolean isHomeAdded = false;
        List<BottomNavInfo> bottomNavList = JsonParser.getInstance().getBottomNavList(this);
        if (bottomNavList != null && bottomNavList.size() > 0) {
            int dynamicBrandPos = 0;
            for (int indx = 0; indx < bottomNavList.size(); indx++) {
                if (dynamicBrandPos == (isHomeAdded ? MAX_NAVIGATION_ITEMS : (MAX_NAVIGATION_ITEMS - 1))) {
                    break;
                }
                BottomNavInfo brandNavItem = bottomNavList.get(indx);

                if (brandNavItem.action.equalsIgnoreCase(Action.CategoryAction.CAT_MORE_NAV)) {
                    List<BottomNavInfo> bottomNavMoreList = JsonParser.getInstance().getBottomNavMoreList(this);
                    if (bottomNavMoreList.size() > 1) {

                        bottomNavInfosMain.add(dynamicBrandPos, brandNavItem);
                        dynamicBrandPos++;

                        // add bottom more items
                        initializeBottomNavMore(bottomNavMoreList);

                    } else if (bottomNavMoreList.size() == 1) {
                        BottomNavInfo brandNavMoreItem = bottomNavMoreList.get(0);
                        bottomNavInfosMain.add(dynamicBrandPos, brandNavMoreItem);
                        dynamicBrandPos++;
                    }
                } else {
                    if (brandNavItem.action.equalsIgnoreCase(Action.CategoryAction.CAT_HOME_NAV)) {
                        isHomeAdded = true;
                    }
                    bottomNavInfosMain.add(dynamicBrandPos, brandNavItem);
                    dynamicBrandPos++;
                }
            }
        }

        if (!isHomeAdded) {
            BottomNavInfo brandNavItem = new BottomNavInfo();
            brandNavItem.action = Action.CategoryAction.CAT_HOME_NAV;
            brandNavItem.id = "810";
            brandNavItem.name = getResources().getString(R.string.home);
            brandNavItem.icon = "https://i10.dainikbhaskar.com/images/appfeed/nav_icon/Home-1.png";
            brandNavItem.iconActive = "https://i10.dainikbhaskar.com/images/appfeed/nav_icon/Home-2.png";
            bottomNavInfosMain.add(0, brandNavItem);
        } else {
            for (int i = 0; i < bottomNavInfosMain.size(); i++) {
                if (i != 0 && bottomNavInfosMain.get(i).action.equalsIgnoreCase(Action.CategoryAction.CAT_HOME_NAV)) {
                    BottomNavInfo homeBottomNav = new BottomNavInfo(bottomNavInfosMain.get(i));
                    bottomNavInfosMain.remove(homeBottomNav);
                    bottomNavInfosMain.add(0, homeBottomNav);
                }
            }
        }

        for (int index = 0; index < bottomNavInfosMain.size(); index++) {
            bottomNavigationItems.add(createTabIcons(bottomNavInfosMain.get(index), index));
        }

        if (bottomNavigationItems.size() < 2) {
            AppPreferences.getInstance(CoreAppCompatActivity.this).setBooleanValue(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE, true);
            findViewById(R.id.bottom_bar_layout).setVisibility(GONE);
        } else {
            AppPreferences.getInstance(CoreAppCompatActivity.this).setBooleanValue(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE, AppPreferences.getInstance(CoreAppCompatActivity.this).getBooleanValue(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE_SERVER, false));
        }

        bottomTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = bottomNavigationItems.indexOf(tab);
                updateTabUI(tab, bottomNavInfosMain.get(position), true);
                if (!isIndexSelected) {
                    onMainBottomTabClick(position);
                } else {
                    isIndexSelected = false;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int position = bottomNavigationItems.indexOf(tab);
                updateTabUI(tab, bottomNavInfosMain.get(position), false);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int position = bottomNavigationItems.indexOf(tab);
                updateTabUI(tab, bottomNavInfosMain.get(position), true);
                if (!isIndexSelected) {
                    onMainBottomTabClick(position);
                } else {
                    isIndexSelected = false;
                }
            }
        });
        try {
            setItemIndexActiveState(currentBottomPosition);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private TabLayout.Tab createTabIcons(@NonNull BottomNavInfo menuItem, int index) {
        bottomTabLayout.addTab(bottomTabLayout.newTab());
        LinearLayout layout = (LinearLayout) LayoutInflater.from(CoreAppCompatActivity.this).inflate(R.layout.custom_bottom_bar, null);
        TextView tabOne = layout.findViewById(R.id.tab);
        tabOne.setText(menuItem.name);
        ImageView tabImg = layout.findViewById(R.id.tab_img);
        ImageUtil.setImage(CoreAppCompatActivity.this, menuItem.icon, tabImg, 0);
        TabLayout.Tab tab = bottomTabLayout.getTabAt(index);
        if (null != tab) {
            tab.setTag(menuItem);
            tab.setCustomView(layout);
        }
        return tab;
    }

    private void initializeBottomNavMore(List<BottomNavInfo> bottomNavMoreList) {
        bottomNavInfosMore = new ArrayList<>(MAX_NAVIGATION_ITEMS);
        /*Dynamic Bottom Item*/
        if (bottomNavMoreList != null && bottomNavMoreList.size() > 0) {
            int dynamicBrandPos = 0;
            for (int indx = 0; indx < bottomNavMoreList.size(); indx++) {
                if (dynamicBrandPos == (MAX_NAVIGATION_ITEMS)) {
                    break;
                }
                BottomNavInfo brandNavItem = bottomNavMoreList.get(indx);
                if (!brandNavItem.id.equalsIgnoreCase("578")) {
                    bottomNavInfosMore.add(dynamicBrandPos, brandNavItem);
                    dynamicBrandPos++;
                }

            }
        }

        moreBottomRV.setLayoutManager(new GridLayoutManager(CoreAppCompatActivity.this, bottomNavInfosMore.size()));
        moreBottomRV.setAdapter(new BottomNavGridAdapter(bottomNavInfosMore, this::onMoreBottomTabClick, BottomNavGridAdapter.GRID_TYPE));
    }

    boolean isIndexSelected = false;

    private void setItemIndexActiveState(int currentBottomPosition) {
        isIndexSelected = true;
        Objects.requireNonNull(bottomTabLayout.getTabAt(currentBottomPosition)).select();
    }

    private void updateTabUI(@NonNull TabLayout.Tab tab, BottomNavInfo bottomNavInfo, boolean isActive) {
        try {
            int tabTextColor = (isActive) ? ContextCompat.getColor(CoreAppCompatActivity.this, R.color.divya_theme_color) : AppUtils.getThemeColor(CoreAppCompatActivity.this, "#FF777777");
            String imageUrl = (isActive) ? bottomNavInfo.iconActive : bottomNavInfo.icon;
            View customView = tab.getCustomView();
            if (null != customView) {
                TextView tabText = customView.findViewById(R.id.tab);
                ImageView tabImg = customView.findViewById(R.id.tab_img);
                tabText.setTextColor(tabTextColor);
                if (null != tabImg) {
                    ImageUtil.setImage(CoreAppCompatActivity.this, imageUrl, tabImg, 0);
                }
            }
        } catch (Exception ignored) {
        }
    }

    private void onMainBottomTabClick(int position) {
        //saveBottomPosition(position);
        currentBottomPosition = position;
        String action = bottomNavInfosMain.get(position).action;
        if (action.equalsIgnoreCase(Action.CategoryAction.CAT_MORE_NAV)) {
            if (moreBottomRV.getVisibility() == View.VISIBLE) {
                // setItemIndexActiveState(currentBottomPosition);
                moreBottomRV.setVisibility(View.INVISIBLE);
                moreLayoutDivider.setVisibility(View.INVISIBLE);
            } else {
                moreBottomRV.setVisibility(View.VISIBLE);
                moreLayoutDivider.setVisibility(View.VISIBLE);
            }
        } else {
            HashMap<String, Object> dataMap = new HashMap<>();
            dataMap.put("position", position);
            dataMap.put("isBottom", true);
            dataMap.put("isMainBottom", true);
            broadcastMessage(dataMap, action);
        }
    }

    private void broadcastMessage(HashMap<String, Object> dataMap, String action) {
        if (bundleIsFromPush || bundleIsDeeplink) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("isFromArticle", true);
            intent.putExtra("dataMap", dataMap);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Intent intent = new Intent("core_event");
            intent.putExtra("dataMap", dataMap);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            setResult(RESULT_OK);
            if (!Arrays.asList(Action.outsideOpenCateg).contains(action)) {
                finish();
            } else {
                Log.v("TAG", "no finish");
            }
        }

    }


    private void saveBottomPosition(int position) {
        AppPreferences.getInstance(this).setIntValue("bottomPosition", position);
    }

    private int getBottomPosition() {
        return AppPreferences.getInstance(this).getIntValue("bottomPosition", 0);
    }

    private void onMoreBottomTabClick(int position) {
        HashMap<String, Object> dataMap = new HashMap<>();
        dataMap.put("position", position);
        dataMap.put("isBottom", true);
        dataMap.put("isMainBottom", false);
        dataMap.put("bottomPosition", currentBottomPosition);

        broadcastMessage(dataMap, bottomNavInfosMore.get(position).action);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null) {
                final int unmaskedRequestCode = requestCode & 0x0000ffff;
                fragment.onRequestPermissionsResult(unmaskedRequestCode, permissions, grantResults);
            }
        }
    }

    private void closeNavigationDrawer() {
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onActionBarIconUpdate(String action) {
    }

    @Override
    public void onBackPressed() {
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            closeNavigationDrawer();
        } else {
            if (bundleIsFromWidget) {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            } else if ((bundleFrom != null && bundleIsDeeplink) || bundleIsFromPush) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
            finish();
        }
    }

    @Override
    public void onDbVideoClick(int position) {
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IN_APP_UPDATE_REQUEST_CODE) {
            if (resultCode == RESULT_CANCELED) {
                final Date lastShownDate = new Date();
                AppPreferences.getInstance(CoreAppCompatActivity.this).setIntValue(QuickPreferences.APP_UPDATE_SESSION_COUNT, 1);
                AppPreferences.getInstance(CoreAppCompatActivity.this).setLongValue(QuickPreferences.APP_UPDATE_POP_UP_SHOWN_DATE, lastShownDate.getTime());
            }
        } else if (requestCode == MainActivity.REQUEST_CODE_HOME_FEED_CUSTOMIZE) {
            if (resultCode == RESULT_OK) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
                if (fragment instanceof NewsFragment) {
                    ViewPager pager = ((NewsFragment) fragment).pager;
                    if (pager != null && pager.getAdapter() != null) {
                        NewsListFragment newsListFragment = (NewsListFragment) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                        newsListFragment.moveToTop();
                        newsListFragment.swipeRefreshLayout();
                    }
                }
            }
        } else if (requestCode == REQUEST_CODE_CALL_FOR_FOOTER_TAB && resultCode == RESULT_OK) {
            Fragment newsFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
            if (newsFragment instanceof NewsFragment) {
                newsFragment.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == LOGIN_REQUEST_CODE) {
            updateLoginView();
        } else if (requestCode == Constants.REQ_CODE_BOTTOM_NAV && resultCode == RESULT_OK) {
            setResult(Activity.RESULT_OK);
            finish();
        } else {
            if (resultCode == RESULT_OK) {
                Fragment newsFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
                if (newsFragment instanceof NewsFragment) {
                    ViewPager pager = ((NewsFragment) newsFragment).pager;
                    if (pager.getAdapter() != null) {
                        if (pager.getAdapter().instantiateItem(pager, pager.getCurrentItem()) instanceof PrefferedCityNewsListFragment) {
                            PrefferedCityNewsListFragment fragment = (PrefferedCityNewsListFragment) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                            fragment.refreshFragment(null);
                        } else if (pager.getAdapter().instantiateItem(pager, pager.getCurrentItem()) instanceof MeraPageNewsFragment) {
                            MeraPageNewsFragment fragment = (MeraPageNewsFragment) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                            fragment.onActivityResult(requestCode, resultCode, data);
                        }
                    }
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityStop = false;
        updateLoginView();
    }

    private void updateLoginView() {
        mProfileInfo = LoginPreferences.getInstance(CoreAppCompatActivity.this).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        FantasyFragment fantasyFragment = (FantasyFragment) getSupportFragmentManager().findFragmentByTag(FantasyFragment.class.getName());
        if (fantasyFragment != null) {
            fantasyFragment.updateLoginView(mProfileInfo);
        }
    }

    private void initializeNavigationDrawer() {
        FragmentUtil.changeFragment(getSupportFragmentManager(), R.id.root_container, FantasyFragment.getInstance(), false, false);
    }


    @Override
    public void onDataFetch(boolean flag) {
    }

    @Override
    public void onDataFetch(boolean flag, String sectionName) {
    }

    @Override
    public void onDataFetch(boolean flag, int viewType) {
    }

    @Override
    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {
        if (categoryInfo.action.equalsIgnoreCase(Action.CategoryAction.CAT_ACTION_TV)) {
            AppUtils.getInstance().callLiveTV(CoreAppCompatActivity.this, categoryInfo, CoreAppCompatActivity.this);
        }
    }

    @Override
    public void onActionClick(MenuItem menuItem) {
        if (menuItem != null) {
            if (menuItem.action.equals("profile")) {
                LoginController.loginController().getLoginIntent(CoreAppCompatActivity.this, TrackingData.getDBId(this), TrackingData.getDeviceId(this), LOGIN_REQUEST_CODE);
            } else {
                CategoryInfo categoryInfo = JsonParser.getInstance().getCategoryInfoById(CoreAppCompatActivity.this, menuItem.id);
                if (categoryInfo != null) {
                    boolean isOpened = ActivityUtil.openCategoryActionOutside(CoreAppCompatActivity.this, categoryInfo);
                    if (!isOpened) {
                        HashMap<String, Object> dataMap = new HashMap<>();
                        dataMap.put("isBottom", false);
                        dataMap.put("menuItem", menuItem);
                        broadcastMessage(dataMap, categoryInfo.action);
                    }
                }
            }
        }
        closeNavigationDrawer();
    }


}
