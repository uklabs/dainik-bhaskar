package com.db.main;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.util.AppPreferences;
import com.db.util.QuickPreferences;
import com.db.util.ThemeUtil;
import com.db.util.AppUtils;

public class ForceUpdateActivity extends Activity {
    static final int UPDATE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_force_update);

        String updatePopupMessage = AppPreferences.getInstance(ForceUpdateActivity.this).getStringValue(QuickPreferences.UPDATE_POPUP_MESSAGE, "");
        if (!AppUtils.getInstance().isValidString(updatePopupMessage)) {
            updatePopupMessage = getResources().getString(R.string.app_update_popup_default_message);
        }

        ((TextView) findViewById(R.id.txt_dia)).setText(updatePopupMessage);

        TextView btnUpdateNow = findViewById(R.id.textUpdateNow);
        btnUpdateNow.setOnClickListener(v -> {
            try {
                Intent pickContactIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                startActivityForResult(pickContactIntent, UPDATE_REQUEST);
            } catch (Exception e) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == UPDATE_REQUEST) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {

    }
}
