package com.db.main;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.divya_new.jyotishVastu.DivyaRashifalDetailActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppLogs;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.RashifalUtil;
import com.db.util.Systr;
import com.db.util.ThemeUtil;

public class NotificationDialogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_notification_dialog);
        ImageView iv_closePopUp = findViewById(R.id.dialog_confirmation_popup_text_popup_cancelbutton);
        DrawableCompat.setTint(iv_closePopUp.getDrawable(), ContextCompat.getColor(this, R.color.black));

        setData(getIntent());
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setFinishOnTouchOutside(false);


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setData(intent);

    }

    private void setData(Intent mIntent) {
        final String channelId = mIntent.getStringExtra(Constants.KeyPair.KEY_CHANNEL_SNO);
        final String catId = mIntent.getStringExtra(Constants.KeyPair.KEY_CATEGORY_ID);
        final String storyId = mIntent.getStringExtra(Constants.KeyPair.KEY_STORY_ID);
        final String detailUrl = mIntent.getStringExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL);
        final String title = mIntent.getStringExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE);
        final String newsType = mIntent.getStringExtra(Constants.KeyPair.KEY_NEWS_TYPE);
        final String gaScreen = mIntent.getStringExtra(Constants.KeyPair.KEY_GA_SCREEN);
        final String gaArticle = mIntent.getStringExtra(Constants.KeyPair.KEY_GA_ARTICLE);
        final String gaEventLabel = mIntent.getStringExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL);
        final boolean isKeyNotification = mIntent.getBooleanExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, false);
        final boolean clickViaNotification = mIntent.getBooleanExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, false);
        final String domain = mIntent.getStringExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN);
        final String subDomain = mIntent.getStringExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN);
        TextView messageText = findViewById(R.id.dialog_confirmation_popup_text_popup_confirmationpickup_confirmationmessage);
        TextView messageTitle = findViewById(R.id.dialog_confirmation_popup_text_popup_titlemessage);
        final String displayName = mIntent.getStringExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME);
        final String isWapExt = mIntent.getStringExtra(Constants.KeyPair.KEY_IS_WAP_EXT);
        String webUrl = mIntent.getStringExtra(Constants.KeyPair.KEY_WEB_URL);
        if (!webUrl.startsWith("http://") && !webUrl.startsWith("https://")) {
            webUrl = "https://" + webUrl;

        }
        final String mWebUrl = webUrl;


        if (catId.equalsIgnoreCase("4")) {
            messageTitle.setText("Rashifal");
        } else {
            messageTitle.setText("News Update");
        }

        messageText.setText(title);

        TextView readmore_Text = findViewById(R.id.dialog_readmore);
        readmore_Text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tracking
                String eventType = "2";
                String aurl = "";
                String url = AppUrls.DOMAIN + AppFlyerConst.GAExtra.NOTIFICATION_GA_SCREEN;

                Tracking.trackWisdomNotification(NotificationDialogActivity.this, TrackingHost.getInstance(NotificationDialogActivity.this).getWisdomUrlForNotification(AppUrls.WISDOM_NOTIFICATION_URL), catId, title, newsType, eventType, storyId, aurl, url, CommonConstants.BHASKAR_APP_ID, channelId, AppFlyerConst.GAExtra.NOTIFICATION_WIDGET_NOTIFY);
                Systr.println("Wisdom Notification tracking : " + storyId);


                try {
                    if (catId.equalsIgnoreCase("4")) {

                        int indexnumber = RashifalUtil.getRashiIndex(storyId);
                        int rashiNameindexnumber = indexnumber + 2;
                        String rashiName = "";
                        try {
                            if (CommonConstants.CHANNEL_ID.equalsIgnoreCase(Constants.AppIds.DAINIK_BHASKAR)) {
                                String[] rashiArray = getResources().getStringArray(R.array.sunsign_hindi_array);
                                rashiName = rashiArray[rashiNameindexnumber - 1];
                            } else {
                                String[] rashiArray = getResources().getStringArray(R.array.sunsign_english_array);
                                rashiName = rashiArray[rashiNameindexnumber - 1];
                            }
                        } catch (Exception e) {
                            AppLogs.printErrorLogs("RashifalDetails", e.toString());
                        }

                        Intent intent = new Intent(NotificationDialogActivity.this, DivyaRashifalDetailActivity.class);
                        intent.putExtra("indexnumber", (indexnumber + 1));
                        intent.putExtra("rashi.type", "rashi");
                        intent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, AppUrls.RASHIFAL_DETAIL_URL);
                        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
                        intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
                        intent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, domain);

                        intent.putExtra(Constants.KeyPair.KEY_TITLE, rashiName);
                        startActivity(intent);

                    } else if (catId.equalsIgnoreCase("7")) {
                        if (!TextUtils.isEmpty(mWebUrl) && !TextUtils.isEmpty(isWapExt) && isWapExt.equalsIgnoreCase("1")) {
                            try {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mWebUrl));
                                startActivity(browserIntent);
                            } catch (ActivityNotFoundException e) {
                                AppUtils.getInstance().showCustomToast(NotificationDialogActivity.this, "No application can handle this request."
                                        + " Please install a webbrowser");
                                e.printStackTrace();
                            }
                        } else {
                            finish();
                        }

                    } else {
                        Intent intent = new Intent(NotificationDialogActivity.this, AppUtils.getInstance().getArticleTypeClass());
                        intent.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, channelId);
                        intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, isKeyNotification);
                        intent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, clickViaNotification);
                        intent.putExtra(Constants.KeyPair.KEY_STORY_ID, storyId);
                        intent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, catId);
                        intent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, detailUrl);
                        intent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, "");
                        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
                        intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
                        intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
                        intent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, domain);
                        intent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, displayName);
                        intent.putExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN, subDomain);
//                        intent.putExtra(Constants.KeyPair.KEY_COLOR, AppUtils.getThemeColor());
                        startActivity(intent);

                    }

                    finish();

                } catch (Exception e) {
                }
            }
        });
        findViewById(R.id.dialog_confirmation_popup_text_popup_cancelbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (!TextUtils.isEmpty(isWapExt) && isWapExt.equalsIgnoreCase("1")) {
            readmore_Text.setVisibility(View.VISIBLE);

        } else if (TextUtils.isEmpty(storyId)) {
            readmore_Text.setVisibility(View.GONE);
//            final String campaign = AppPreferences.getInstance(NotificationDialogActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
//            Tracking.trackGAEvent(InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, AppFlyerConst.GAAction.CLICK, AppFlyerConst.GALabel.BREAKING_NEWS, campaign);
//            Systr.println("GA Events : " + AppFlyerConst.GACategory.NOTIFICATION + AppFlyerConst.GAAction.CLICK + AppFlyerConst.GALabel.BREAKING_NEWS + campaign);


        } else {
            readmore_Text.setVisibility(View.VISIBLE);
        }
    }
}
