package com.db.main;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.view.ui.splash.SplashActivity;
import com.bumptech.glide.request.target.NotificationTarget;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.QuickNewsTable;
import com.db.data.models.NewsListInfo;
import com.db.dbvideo.player.BusProvider;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.Notificationutil;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuickNewsNotificationService2 extends Service {

    private static String FEED_URL = CommonConstants.APP_WIDGET_STICKY_FEED_URL;
    private boolean isLoading;
    private int paginationCount = 0;
    private boolean isInLoadingNotiState = false;
    private BroadcastReceiver quickNotificationReceiver;
    private BroadcastReceiver connectivityReceiver;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            cancelNotification();
            cancelScheduleService();
            stopForeground(true);
            unRegisterReciever();
            unRegisterConnectivityReciever();
        } catch (Exception e) {
            e.printStackTrace();
        }
        startScheduleService();
        registerBroadCastReciever();
        registerConnectivityReciever();
        notifyPushLoading();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            if (intent.getAction().equals(Constants.QuickReadActions.ACTION_START)) {
                startSticky(intent, intent.getAction());
//                String label = AppFlyerConst.GALabel.ON;
//                String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
//                Tracking.trackGAEvent(this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, AppFlyerConst.GAAction.TOGGLE_STICKY, label, campaign);

            } else if (intent.getAction().equals(Constants.QuickReadActions.ACTION_REFRESH)) {
                cancelScheduleService();
                startScheduleService();
                startSticky(intent, intent.getAction());
            } else if (intent.getAction().equals(Constants.QuickReadActions.ACTION_NEXT)) {
                startSticky(intent, intent.getAction());
            } else if (intent.getAction().equals(Constants.QuickReadActions.ACTION_STOP)) {
                cancelScheduleService();
                stopForeground(true);
                stopSelf();
                AppPreferences.getInstance(this).setIntValue(QuickPreferences.QUICK_READ_STATUS, 0);
                AppPreferences.getInstance(this).setBooleanValue(QuickPreferences.QUICK_READ_STOP_BY_USER, true);
                /*String label = AppFlyerConst.GALabel.OFF;
                String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
                Tracking.trackGAEvent(this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, AppFlyerConst.GAAction.TOGGLE_STICKY, label, campaign);
                */BusProvider.getInstance().post("Off");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }

    private void startSticky(Intent intent, String action) {
        if (!isLoading) {
            int count = intent.getIntExtra("count", 0);
            paginationCount = intent.getIntExtra("paginationCount", 0);

            AppLogs.printErrorLogs("count_in_intent", count + "");

            AppPreferences.getInstance(this).setIntValue(QuickPreferences.QuickNotificationPrefs.COUNT, count);
            AppPreferences.getInstance(this).setIntValue(QuickPreferences.QuickNotificationPrefs.PAGINATION_COUNT, paginationCount);

            FEED_URL = AppPreferences.getInstance(this).getStringValue(QuickPreferences.WidgetPrefs.APP_WIDGET_FEED, FEED_URL);

            QuickNewsTable news_table = (QuickNewsTable) DatabaseHelper.getInstance(this).getTableObject(QuickNewsTable.TABLE_NAME);
            if (action.equalsIgnoreCase(Constants.QuickReadActions.ACTION_REFRESH)) {
                news_table.deleteAllData();
            }

            final ArrayList<NewsListInfo> array_news = news_table.getAllNewsListInfoList();
            String finalFeedURL;
            if (array_news.size() > 0) {
                if (NetworkStatus.getInstance().isConnected(this)) {
                    if (count >= array_news.size()) {
                        paginationCount = paginationCount + 1;
                        if (FEED_URL.endsWith("1/")) {
                            finalFeedURL = AppUtils.getInstance().makeUrlForLoadMore(FEED_URL, paginationCount);
                        } else {
                            finalFeedURL = FEED_URL + "PG" + (paginationCount) + "/";
                        }
                        makeJsonObjectRequest(finalFeedURL);
                    } else if (count < array_news.size()) {
                        isLoading = false;
                        notifyPush(R.mipmap.app_icon, array_news.get(count), count);
                    }
                } else {
                    if (count >= array_news.size()) {
                        AppUtils.getInstance().showCustomToast(this, getResources().getString(R.string.internet_connection_error_));
                    } else {
                        notifyPush(R.mipmap.app_icon, array_news.get(count), count);
                    }
                }
            } else {
                if (NetworkStatus.getInstance().isConnected(this)) {
//                    notifyPushLoading();
                    if (FEED_URL.endsWith("1/")) {
                        finalFeedURL = FEED_URL + "PG1/";
                    } else {
                        paginationCount = paginationCount + 1;
                        finalFeedURL = FEED_URL + "PG" + paginationCount + "/";
                    }
                    makeJsonObjectRequest(finalFeedURL);
                } else {
                    if (array_news.size() > 0 && count < array_news.size()) {
                        notifyPush(R.mipmap.app_icon, array_news.get(count), count);
                    }
                }
            }
        }
    }

    public void notifyPush(int imgRes, NewsListInfo newsInfo, int count) {
        if (!isLoading) {
            count = count + 1;
            AppLogs.printErrorLogs("count", count + "");
            AppLogs.printErrorLogs("newsInfo", newsInfo.title + "");

            String domain = AppUrls.DOMAIN;
            Intent intentNotif = new Intent(this, SplashActivity.class);
            intentNotif.putExtra("count", count);
            intentNotif.putExtra("paginationCount", paginationCount);

            intentNotif.putExtra(Constants.KeyPair.KEY_IS_FROM_QUICK_NOTIFICATION, true);
            intentNotif.putExtra(Constants.KeyPair.KEY_VERSION, newsInfo.version);
            intentNotif.putExtra(Constants.KeyPair.IN_BACKGROUND, InitApplication.getInstance().isAppOpen());
            intentNotif.putExtra(Constants.KeyPair.KEY_IS_FROM_APPWIDGET, true);
            intentNotif.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID));
            intentNotif.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, newsInfo.title);
            intentNotif.putExtra(Constants.KeyPair.KEY_CHANNEL_SNO, CommonConstants.CHANNEL_ID);
            intentNotif.putExtra(Constants.KeyPair.KEY_STORY_ID, newsInfo.storyId);
            intentNotif.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, "1");
            intentNotif.putExtra(Constants.KeyPair.KEY_NEWS_TYPE, "1");
            intentNotif.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, AppFlyerConst.GAExtra.WIDGET_STICKY);
            intentNotif.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAExtra.WIDGET_STICKY);
            intentNotif.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, AppFlyerConst.GAExtra.WIDGET_STICKY_ART);
            intentNotif.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, domain);
            intentNotif.putExtra(Constants.KeyPair.KEY_WISDOM_SUB_DOMAIN, AppFlyerConst.GAExtra.WIDGET_STICKY);
            intentNotif.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendIntent = PendingIntent.getActivity(this, Constants.AppWidgetIDS.STICKY_WIDGET_CLICK_ID, intentNotif, PendingIntent.FLAG_UPDATE_CURRENT);

            final RemoteViews mRemoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification_small);
            mRemoteViews.setImageViewResource(R.id.notif_icon, imgRes);
            mRemoteViews.setTextViewText(R.id.notif_title, newsInfo.title);

            Intent intent1 = new Intent(this, QuickNewsNotificationService2.class);
            intent1.putExtra("count", count);
            intent1.putExtra("paginationCount", paginationCount);
            intent1.setAction(Constants.QuickReadActions.ACTION_NEXT);
            PendingIntent pVolume = PendingIntent.getService(this, Constants.AppWidgetIDS.STICKY_WIDGET_REFRESH_ID, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
            mRemoteViews.setOnClickPendingIntent(R.id.notification_refersh, pVolume);

            Intent intentClose = new Intent(this, QuickNewsNotificationService2.class);
            intentClose.putExtra("count", 0);
            intentClose.putExtra("paginationCount", 0);
            intentClose.setAction(Constants.QuickReadActions.ACTION_STOP);
            PendingIntent pStop = PendingIntent.getService(this, Constants.AppWidgetIDS.STICKY_WIDGET_CLOSE_ID, intentClose, PendingIntent.FLAG_UPDATE_CURRENT);
            mRemoteViews.setOnClickPendingIntent(R.id.notification_close, pStop);

            String imgUrl = "xyz";
            if (!TextUtils.isEmpty(newsInfo.image))
                imgUrl = newsInfo.image;
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            NotificationCompat.Builder mBuilder;
            String id = getResources().getString(R.string.app_name);
            CharSequence ticker = getResources().getString(R.string.app_name);
            mBuilder = new NotificationCompat.Builder(this, id)
                    .setSmallIcon(Notificationutil.getAppIcon())
                    .setAutoCancel(false)
                    .setOngoing(true)
                    .setContentIntent(pendIntent)
                    .setContent(mRemoteViews)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setTicker(ticker);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                String name = getResources().getString(R.string.app_name);
                NotificationChannel mChannel = new NotificationChannel(id, name, importance);
                mNotificationManager.createNotificationChannel(mChannel);
            }
            /*NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String id = getResources().getString(R.string.app_name);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, id);
            CharSequence ticker = getResources().getString(R.string.app_name);
            mBuilder.setSmallIcon(Notificationutil.getAppIcon())
                    .setAutoCancel(false)
                    .setOngoing(true)
                    .setContentIntent(pendIntent)
                    .setContent(mRemoteViews)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setTicker(ticker);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                String name = getResources().getString(R.string.app_name);
                NotificationChannel mChannel = new NotificationChannel(id, name, importance);
                mNotificationManager.createNotificationChannel(mChannel);
            }*/

            final Notification notification = mBuilder.build();

            if (AppPreferences.getInstance(this).getIntValue(QuickPreferences.QUICK_READ_STATUS, 0) == 1) {
                mNotificationManager.notify(Constants.AppWidgetIDS.STICKY_WIDGET_ID, notification);
                NotificationTarget notificationTarget = new NotificationTarget(
                        this,
                        R.id.notif_icon,
                        mRemoteViews,
                        notification,
                        Constants.AppWidgetIDS.STICKY_WIDGET_ID);
                isInLoadingNotiState = false;

                ImageUtil.setImageNotification(getApplicationContext(), imgUrl, notificationTarget, 0);
            } else {
                try {
                    cancelNotification();
                    cancelScheduleService();
                    stopForeground(true);
                    unRegisterReciever();
                    unRegisterConnectivityReciever();
                    stopSelf();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel() {
        String channelId = getResources().getString(R.string.app_name);
        String channelName = getResources().getString(R.string.app_name);
        NotificationChannel chan = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        service.createNotificationChannel(chan);
        return channelId;
    }

    public void notifyPushLoading() {
        String channelId = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel();
        }
        isInLoadingNotiState = true;
        RemoteViews mRemoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification_loading);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId);
        CharSequence ticker = getResources().getString(R.string.app_name);

        mBuilder.setSmallIcon(Notificationutil.getAppIcon())
                .setAutoCancel(false)
                .setOngoing(true)
                .setContent(mRemoteViews)
                .setPriority(Notification.PRIORITY_MAX)
                .setTicker(ticker);


        if (AppPreferences.getInstance(this).getIntValue(QuickPreferences.QUICK_READ_STATUS, 0) == 1)
            startForeground(Constants.AppWidgetIDS.STICKY_WIDGET_ID, mBuilder.build());
        else {
            try {
                cancelNotification();
                cancelScheduleService();
                stopForeground(true);
                unRegisterReciever();
                unRegisterConnectivityReciever();
                stopSelf();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void makeJsonObjectRequest(String finalFeedURL) {
        if (!isLoading && NetworkStatus.getInstance().isConnected(this)) {
            AppLogs.printErrorLogs("final_url", finalFeedURL);
            isLoading = true;
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response != null) {
                                    parseJsonObjectFeedList(response);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    isLoading = false;

                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(this).addToRequestQueue(jsonObjReq);
        }
    }

    private void parseJsonObjectFeedList(JSONObject response) throws JSONException {
        QuickNewsTable news_table = (QuickNewsTable) DatabaseHelper.getInstance(this).getTableObject(QuickNewsTable.TABLE_NAME);
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        List<NewsListInfo> newsInfoList;
        isLoading = false;

        if (resultArray.length() > 0) {
//            for (int i = 0; i < resultArray.length(); i++) {
//                JSONObject feedJsonObject = resultArray.getJSONObject(i);
//                Gson gson = new Gson();
//                NewsListInfo newsFeeds = gson.fromJson(feedJsonObject.toString(), NewsListInfo.class);
//                newsInfoList.add(newsFeeds);
//            }
            try {
                newsInfoList = Arrays.asList(new Gson().fromJson(resultArray.toString(), NewsListInfo[].class));
                AppPreferences.getInstance(this).setIntValue(QuickPreferences.QuickNotificationPrefs.COUNT, 0);
                news_table.deleteAllData();
                news_table.addCommonListItem(newsInfoList);
                startNotification(newsInfoList.get(0));
            } catch (JsonSyntaxException e) {
            }
        } else {
            paginationCount = 0;
            news_table.deleteAllData();
            Intent intent = new Intent(this, QuickNewsNotificationService2.class);
            intent.putExtra("count", 0);
            intent.putExtra("paginationCount", paginationCount);
            intent.setAction(Constants.QuickReadActions.ACTION_START);
            startService(intent);
        }

    }

    private void startNotification(NewsListInfo newsInfo) {
        notifyPush(R.mipmap.app_icon, newsInfo, 0);
    }


    private void startScheduleService() {
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(this, QuickNewsNotificationService2.class);
        intent.putExtra("count", 0);
        intent.putExtra("paginationCount", 0);
        intent.setAction(Constants.QuickReadActions.ACTION_REFRESH);
        PendingIntent pintent = PendingIntent.getService(this, Constants.AppWidgetIDS.STICKY_WIDGET_SCHEDULER_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis() + (60 * 60 * 1000), pintent);
    }

    private void cancelScheduleService() {
        Intent intent = new Intent(this, QuickNewsNotificationService2.class);
        PendingIntent pintent = PendingIntent.getService(this, Constants.AppWidgetIDS.STICKY_WIDGET_SCHEDULER_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pintent);
    }

    private void registerBroadCastReciever() {
        quickNotificationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
                    startNotificationFromBroadcast(context, 1, Constants.QuickReadActions.ACTION_NEXT);
                }
            }
        };
        registerReceiver(quickNotificationReceiver, new IntentFilter(Intent.ACTION_USER_PRESENT));
    }

    private void unRegisterReciever() {
        try {
            if (quickNotificationReceiver != null) {
                unregisterReceiver(quickNotificationReceiver);
            }
        } catch (Exception e) {
            quickNotificationReceiver = null;
        }
    }

    private void startNotificationFromBroadcast(Context context, int increment, String action) {
        int countValue = AppPreferences.getInstance(this).getIntValue(QuickPreferences.QuickNotificationPrefs.COUNT, 0);
        int paginationValue = AppPreferences.getInstance(this).getIntValue(QuickPreferences.QuickNotificationPrefs.PAGINATION_COUNT, 0);
        if (AppPreferences.getInstance(this).getIntValue(QuickPreferences.QUICK_READ_STATUS, 0) == 1) {
            Intent intent = new Intent(this, QuickNewsNotificationService2.class);
            intent.putExtra("count", countValue + increment);
            intent.putExtra("paginationCount", paginationValue);
            intent.setAction(action);
            context.startService(intent);
        }
    }

    private void registerConnectivityReciever() {
        connectivityReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION) && isInLoadingNotiState) {
                    startNotificationFromBroadcast(context, 0, Constants.QuickReadActions.ACTION_START);
                }
            }
        };
        registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void unRegisterConnectivityReciever() {
        try {
            if (connectivityReceiver != null) {
                unregisterReceiver(connectivityReceiver);
            }
        } catch (Exception e) {
            connectivityReceiver = null;
        }
    }

    private void cancelNotification() {
        try {
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(Constants.AppWidgetIDS.STICKY_WIDGET_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegisterReciever();
        unRegisterConnectivityReciever();
    }
}
