package com.db.network;

import android.content.Context;

import com.android.volley.Cache;
import com.db.util.VolleyNetworkSingleton;

import java.io.UnsupportedEncodingException;

public class NetworkUtil {

    public static String volleyCacheRequest(Context context, int method, String url) {
        Cache cache = VolleyNetworkSingleton.getInstance(context).getRequestQueue().getCache();
        Cache.Entry reqEntry = cache.get(method + ":" + url);
        if (reqEntry != null) {
            try {
                return new String(reqEntry.data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public static void volleyInvalidateCache(Context context, String url) {
        VolleyNetworkSingleton.getInstance(context).getRequestQueue().getCache().invalidate(url, true);
    }

    public static void volleyDeleteCache(Context context, int method, String url) {
        VolleyNetworkSingleton.getInstance(context).getRequestQueue().getCache().remove(method + ":" + url);
    }

    public static void volleyClearCache(Context context) {
        VolleyNetworkSingleton.getInstance(context).getRequestQueue().getCache().clear();
    }
}
