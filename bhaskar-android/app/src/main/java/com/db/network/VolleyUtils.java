package com.db.network;

import com.bhaskar.util.CommonConstants;

import java.util.HashMap;
import java.util.Map;

public class VolleyUtils {
    public static VolleyUtils volleyUtils;

    public static VolleyUtils getVolleyUtils() {
        if (volleyUtils == null)
            volleyUtils = new VolleyUtils();
        return volleyUtils;
    }

    private Map<String, String> getHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("api-key", "bh@@$k@r-D");
        headers.put("User-Agent", CommonConstants.USER_AGENT);
        headers.put("encrypt", "0");
        headers.put("Content-Type", "application/json charset=utf-8");
        headers.put("Accept", "application/json");
        return headers;
    }
}
