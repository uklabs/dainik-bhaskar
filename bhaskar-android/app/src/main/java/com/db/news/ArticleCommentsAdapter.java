package com.db.news;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CommentInfo;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class ArticleCommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<CommentInfo> commentInfoList;

    public ArticleCommentsAdapter(Context contexts) {
        this.mContext = contexts;
        commentInfoList = new ArrayList<>();
    }

    public void addItem() {
        commentInfoList.add(null);
        notifyDataSetChanged();
    }

    public void removeItem() {
        commentInfoList.remove(null);
        notifyDataSetChanged();
    }

    public void setData(List<CommentInfo> newsFeed) {
        commentInfoList.clear();
        commentInfoList.addAll(newsFeed);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new CommentViewHolder(inflater.inflate(R.layout.comments_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final CommentViewHolder commentViewHolder = (CommentViewHolder) holder;
        commentViewHolder.tvComment.setText(commentInfoList.get(position).getComment().replace("%20", Constants.Utils.SPACE));

        commentViewHolder.tvCommentingName.setText(commentInfoList.get(position).getName().replace("%20", Constants.Utils.SPACE));

        commentViewHolder.tvTimeAgo.setText(AppUtils.getInstance().getTimeAgo(commentInfoList.get(position).getCreated()));

        if (commentInfoList.get(position).getImage() != null && commentInfoList.get(position).getImage().length() > 0) {

            ImageUtil.setImage(mContext, commentInfoList.get(position).getImage(), commentViewHolder.ivCommentingPerson, 0);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return commentInfoList == null ? 0 : commentInfoList.size();
    }

    private class CommentViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivCommentingPerson;
        private TextView tvCommentingName;
        private TextView tvComment;
        private TextView tvTimeAgo;


        private CommentViewHolder(View itemView) {
            super(itemView);
            ivCommentingPerson = (ImageView) itemView.findViewById(R.id.comments_commenting_person_image);
            tvCommentingName = (TextView) itemView.findViewById(R.id.comments_commenting_person_name_tv);
            tvComment = (TextView) itemView.findViewById(R.id.comments_comment_tv);
            tvTimeAgo = (TextView) itemView.findViewById(R.id.comments_comment_created_time_tv);

            itemView.setTag(itemView);
        }
    }
}