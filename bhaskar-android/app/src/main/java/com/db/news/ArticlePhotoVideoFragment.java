package com.db.news;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.db.data.models.NewsPhotoInfo;
import com.db.util.Constants;
import com.db.util.Systr;

public class ArticlePhotoVideoFragment extends Fragment {
    private NewsPhotoInfo newsPhotoInfo;
    private int position;
    private View rootView;

    public ArticlePhotoVideoFragment() {
        // Required empty public constructor
    }

    public static ArticlePhotoVideoFragment getInstance(int position, NewsPhotoInfo newsPhotoInfo) {
        ArticlePhotoVideoFragment newsFragment = new ArticlePhotoVideoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_PHOTO_DETAIL, newsPhotoInfo);
        bundle.putInt(Constants.KeyPair.KEY_POSITION, position);
        newsFragment.setArguments(bundle);
        return newsFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();
        if (extras != null) {
            newsPhotoInfo = (NewsPhotoInfo) extras.getSerializable(Constants.KeyPair.KEY_PHOTO_DETAIL);
            position = extras.getInt(Constants.KeyPair.KEY_POSITION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_article_photo_video, container, false);
        initializeViews();
        return rootView;
    }

    private void initializeViews() {
    }


    @Override
    public void onStart() {
        super.onStart();
        Systr.println("ON start called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
