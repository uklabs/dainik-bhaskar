package com.db.news;


import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CityInfo;

import java.util.Vector;

public class PreferredCityAdapter extends RecyclerView.Adapter {

    private Vector<CityInfo> cityInfoVector;
    private int selectedPosition = 0;
    private CityListItemClickListener onItemClickListener;


    public PreferredCityAdapter(CityListItemClickListener onItemClickListener) {
        this.cityInfoVector = new Vector<>();
        this.onItemClickListener = onItemClickListener;
    }



    public PreferredCityAdapter() {
        this.cityInfoVector = new Vector<>();
    }

    public void setCityInfoVector(Vector<CityInfo> cityInfos) {
        cityInfoVector.clear();
        cityInfoVector.addAll(cityInfos);
        notifyDataSetChanged();
    }

    public void firstItemSelection() {
        selectedPosition = 0;
        notifyDataSetChanged();
    }

    public void itemSelection(int position) {
        selectedPosition = position;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CityViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.city_lable_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        CityViewHolder cityViewHolder = (CityViewHolder) holder;
        cityViewHolder.cityName.setText(cityInfoVector.get(position).cityHindiName);
        if (position == selectedPosition) {
            cityViewHolder.cityName.setSelected(true);
        } else {
            cityViewHolder.cityName.setSelected(false);
        }
        cityViewHolder.cityName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onCityItemClick(position);
                }
                selectedPosition = position;
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return cityInfoVector != null ? cityInfoVector.size() : 0;
    }

    public interface CityListItemClickListener {
        void onCityItemClick(int cityPosition);
    }

    private class CityViewHolder extends RecyclerView.ViewHolder {
        private TextView cityName;

        private CityViewHolder(View itemView) {
            super(itemView);
            cityName = (TextView) itemView.findViewById(R.id.city_name);
        }
    }

}