package com.db.news;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.database.CityFeedListTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.CityInfo;
import com.db.data.models.RelatedArticleInfo;
import com.db.data.source.remote.SerializeData;
import com.db.dbvideo.player.BusProvider;
import com.db.divya_new.common.TabController;
import com.db.listeners.FragmentLifecycle;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.bhaskar.util.LoginController;
import com.bhaskar.data.model.ProfileInfo;
import com.db.eventWrapper.PersonalizationEventWrapper;
import com.db.preferredcity.CityFeedListAdapter;
import com.db.preferredcity.NamesAdapter;
import com.db.preferredcity.PreferredCityActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.HideShowScrollListener;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.ViewAnimationUtils;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class PrefferedCityNewsListFragment extends Fragment implements OnFeedFetchFromServerListener, PreferredCityAdapter.CityListItemClickListener, FragmentLifecycle, TabController.PreferredCitySearchRefresh {

    public static final int REQUEST_CODE = 30;
    private final String TAG = AppConfig.BaseTag + "." + PrefferedCityNewsListFragment.class.getSimpleName();

    private InitApplication application;
    //Ad layout
    private List<CityInfo> cityInfoArrayList;
    private RecyclerView preferredCityRecyclerView;
    private PreferredCityAdapter preferredCityAdapter;
    private Vector<CityInfo> cityInfoVector;
    private LinearLayoutManager linearLayoutManager;
    private int selectedPosition = -1;
    private CategoryInfo categoryInfo;
    private View view;
    private RelativeLayout search_preferred_city_RL;
    private List<RelatedArticleInfo> cityFeedList = new ArrayList<>();
    //lazy loading
    private boolean isLoading;
    private int LOAD_MORE_COUNT = 0;
    private int visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
    private int lastVisibleItem;
    private int totalItemCount;
    private String finalFeedURL;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CityFeedListAdapter cityFeedListAdapter;
    private RecyclerView cityFeedListRecyclerView;
    private LinearLayoutManager linearLayoutManager1;
    private CityInfo cityInfo;
    private String gaArticle;
    private String gaScreen;
    private String gaEventLabel;
    private String displayName;
    private ProgressBar progressBar;
    private RelativeLayout layoutLoadingItem;
    private TextView noNewsAvailable;
    private ArrayList<Integer> trackingList = new ArrayList<>();
    private boolean isShowing = false;
    private int listIndex = 0;
    private Rect scrollBounds;

    private AutoCompleteTextView selectCityAutocomplete;
    private NamesAdapter cityAutoCompleteListAdapter;
    private View vSearch;
    private View vEditTextSearch;
    private View vSearchSubtitle;
    private View vEditSearch;
    private ImageView ivAddCity;
    private CityInfo outerSelectedCityInfo;
    private List<BannerInfo> bannerInfoList;

    public PrefferedCityNewsListFragment() {
    }

    public static PrefferedCityNewsListFragment newInstance(CategoryInfo categoryInfo, CityInfo cityInfo) {
        PrefferedCityNewsListFragment fragmentFirst = new PrefferedCityNewsListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        bundle.putSerializable(Constants.KeyPair.KEY_CITY_INFO, cityInfo);
        fragmentFirst.setArguments(bundle);
        return fragmentFirst;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);
        Bundle arguments = getArguments();
        if (arguments != null) {
            categoryInfo = (CategoryInfo) arguments.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
            outerSelectedCityInfo = (CityInfo) arguments.getSerializable(Constants.KeyPair.KEY_CITY_INFO);
        }

        application = (InitApplication) getActivity().getApplication();
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
        if(outerSelectedCityInfo == null)
            outerSelectedCityInfo = cityInfo = getSavedCityInfo();
        else
            cityInfo = outerSelectedCityInfo;

        if (categoryInfo != null) {
            gaArticle = categoryInfo.gaArticle;
            gaScreen = categoryInfo.gaScreen;
            displayName = categoryInfo.displayName;
            gaEventLabel = categoryInfo.gaEventLabel;

            if (cityInfo != null) {
                gaScreen += "-" + cityInfo.cityName;
                gaArticle += "-" + cityInfo.cityName;
                displayName += "_" + cityInfo.cityName;
            }
        }
    }

    private CityInfo getSavedCityInfo() {
        String selectedId = AppPreferences.getInstance(getContext()).getStringValue("selectedCityId", "");
        List<CityInfo> cityInfoList = JsonParser.getInstance().getCityFeedList(getContext(), CommonConstants.CHANNEL_ID);
        for (CityInfo cityInfo : cityInfoList) {
            if (cityInfo.cityId.equalsIgnoreCase(selectedId))
                return cityInfo;
        }
        if (!cityInfoList.isEmpty())
            return cityInfoList.get(0);
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_child_preferred_city, container, false);
        initViews();
        overrideTheme();
        //disable add button
        ivAddCity.setVisibility(View.GONE);
        return view;
    }

    private void initViews() {
        preferredCityRecyclerView = view.findViewById(R.id.city_recycler_view);
        //disable on 5.2
        preferredCityRecyclerView.setVisibility(View.GONE);
        vSearch = view.findViewById(R.id.vSearch);
        vEditTextSearch = view.findViewById(R.id.vEditTextSearch);
        ivAddCity = view.findViewById(R.id.add_city_iv);
        vEditSearch = view.findViewById(R.id.vEditSearch);
        vSearchSubtitle = view.findViewById(R.id.vSubTitle);
        selectCityAutocomplete = view.findViewById(R.id.select_city_autocomplete);
        search_preferred_city_RL = view.findViewById(R.id.search_city_RL);
        if (CommonConstants.CHANNEL_ID.equalsIgnoreCase(Constants.AppIds.DIVYA_BHASKAR)) {
            search_preferred_city_RL.setVisibility(View.VISIBLE);
        } else {
            search_preferred_city_RL.setVisibility(View.GONE);
        }

        vSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vSearchSubtitle.setVisibility(View.GONE);
                vEditSearch.setVisibility(View.VISIBLE);
            }
        });

        linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        preferredCityRecyclerView.setLayoutManager(linearLayoutManager);
        preferredCityRecyclerView.scrollToPosition(0);
        preferredCityRecyclerView.setNestedScrollingEnabled(true);


        preferredCityAdapter = new PreferredCityAdapter(this);
        preferredCityRecyclerView.setAdapter(preferredCityAdapter);

        cityInfoVector = JsonParser.getInstance().analyzePreferredCity(getContext(), CommonConstants.CHANNEL_ID);
        selectCityAutocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long l) {
                CityInfo cityInfo = cityAutoCompleteListAdapter.getItem(pos);
                if (cityInfo == null)
                    return;
                //save city
                AppPreferences.getInstance(getContext()).setStringValue("selectedCityId", cityInfo.cityId);
                String cityName = String.format("%s (%s)", cityInfo.cityName, cityInfo.cityHindiName);//cityInfo.cityHindiName;
                selectCityAutocomplete.setHint(cityName);
                String gaScreen = categoryInfo.gaScreen + "-" + cityInfo.cityName; //+ ((TextUtils.isEmpty(cityInfo.citySelectionValue) ? "" : "_" + cityInfo.citySelectionValue));
                String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
                String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);

                cityInfo.citySelectionValue = Constants.CityRajyaSelection.SELECTED;
                clearCitySelection(cityInfoVector);
                if (!cityInfoVector.contains(cityInfo)) {
                    cityInfoVector.add(0, cityInfo);
                }
//                onSavePreferredCityList();
                AppUtils.hideKeyboard(getActivity());
                selectCityAutocomplete.setText("");
                selectCityAutocomplete.clearFocus();
                if (cityInfoVector.contains(cityInfo)) {
                    for (int i = 0; i < cityInfoVector.size(); i++) {
                        if (cityInfoVector.get(i).equals(cityInfo)) {
                            refreshSearchFragment(cityInfoVector, i);
                            return;
                        }
                    }
                } else
                    refreshFragment(cityInfoVector);
            }

        });

        cityInfoArrayList = JsonParser.getInstance().getCityFeedList(getContext(), CommonConstants.CHANNEL_ID);
        setDataToAutoCompleteAdapter();
        preferredCityAdapter.setCityInfoVector(cityInfoVector);

        ivAddCity.setBackgroundColor(Color.TRANSPARENT);

        ivAddCity.setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));

        ivAddCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Systr.println("onclick of Select city");

                Intent intent = new Intent(getContext(), PreferredCityActivity.class);
                intent.putExtra(PreferredCityActivity.CALL_PREFERRED_CITY_HOME, true);
                if (categoryInfo != null) {
                    intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
                    intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                    intent.putExtra(Constants.KeyPair.KEY_COLOR, categoryInfo.color);
                    intent.putExtra(Constants.KeyPair.KEY_TITLE, categoryInfo.menuName);
                    intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
                    intent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, categoryInfo.displayName);
                    intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, categoryInfo.gaEventLabel);
                }
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

//        ----------------------------------------------------------------------------------     //
        progressBar = view.findViewById(R.id.progress_bar);
        layoutLoadingItem = view.findViewById(R.id.layout_loading_item);
        noNewsAvailable = view.findViewById(R.id.error_message_no_data);
        //swipeRefreshLayout();
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));
        cityFeedListRecyclerView = view.findViewById(R.id.city_feed_list_recycler_view);

        linearLayoutManager1 = new LinearLayoutManager(getContext());
        cityFeedListRecyclerView.setLayoutManager(linearLayoutManager1);
        cityFeedListRecyclerView.setHasFixedSize(true);

        cityFeedListAdapter = new CityFeedListAdapter(getContext(), gaScreen, gaArticle, displayName, gaEventLabel);
        cityFeedListAdapter.setThemeColor(categoryInfo.color);
        cityFeedListRecyclerView.setAdapter(cityFeedListAdapter);
        if (CommonConstants.CHANNEL_ID.equalsIgnoreCase(Constants.AppIds.DIVYA_BHASKAR)) {
            cityFeedListRecyclerView.addOnScrollListener(new HideShowScrollListener() {
                @Override
                public void onHide() {
                    ViewAnimationUtils.collapse(search_preferred_city_RL);
                }

                @Override
                public void onShow() {
                    ViewAnimationUtils.expand(search_preferred_city_RL);
                }
            });
        }
    }

    private void setCitySelectionFromAutoComplete(int pos) {
        cityInfo = cityInfoArrayList.get(pos);
        cityInfo.citySelectionValue = Constants.CityRajyaSelection.SELECTED;
        clearCitySelection(cityInfoVector);
        if (!cityInfoVector.contains(cityInfo)) {
            cityInfoVector.add(0, cityInfo);
        }
//                onSavePreferredCityList();
        AppUtils.hideKeyboard(getActivity());
        selectCityAutocomplete.setText("");
        selectCityAutocomplete.clearFocus();
        if (cityInfoVector.contains(cityInfo)) {
            for (int i = 0; i < cityInfoVector.size(); i++) {
                if (cityInfoVector.get(i).equals(cityInfo)) {
                    refreshSearchFragment(cityInfoVector, i);
                    return;
                }
            }
        } else
            refreshFragment(cityInfoVector);
    }

    private void clearCitySelection(Vector<CityInfo> cityInfoVector) {
        for (CityInfo cityInfo : cityInfoVector) {
            cityInfo.citySelectionValue = null;
        }
    }


    private void setDataToAutoCompleteAdapter() {
        if (cityInfoArrayList != null && cityInfoArrayList.size() > 0) {
            cityAutoCompleteListAdapter = new NamesAdapter(getActivity(), R.layout.preferred_city_auto_complete_item, new ArrayList<>(cityInfoArrayList), cityInfoVector);
            selectCityAutocomplete.setThreshold(2);
            selectCityAutocomplete.setAdapter(cityAutoCompleteListAdapter);
        }

    }

    void overrideTheme() {
        vSearch.getBackground().clearColorFilter();
        vSearch.getBackground().setColorFilter(AppUtils.getThemeColor(getContext(), categoryInfo.color), PorterDuff.Mode.MULTIPLY);
        vEditTextSearch.getBackground().clearColorFilter();
        vEditTextSearch.getBackground().setColorFilter(AppUtils.getThemeColor(getContext(), categoryInfo.color), PorterDuff.Mode.MULTIPLY);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.scrollBounds = new Rect();
        this.cityFeedListRecyclerView.getHitRect(scrollBounds);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                swipeRefreshLayout();
            } else {
                removeLoader();
                if (isAdded() && getActivity() != null)
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            }
        });

        linearLayoutManager1 = (LinearLayoutManager) cityFeedListRecyclerView.getLayoutManager();
        cityFeedListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager1.getItemCount();
                        lastVisibleItem = linearLayoutManager1.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            LOAD_MORE_COUNT++;
                            onLoadMore();
                        }
                    }
                }
            }
        });


        if (outerSelectedCityInfo != null) {
            setAutoCompleteHint(outerSelectedCityInfo);
            swipeRefreshLayout();
            trackingList.add(LOAD_MORE_COUNT);
        } else {
            if (cityInfoVector.size() > 0)
                onCityItemClick(0);
        }
    }


    private void swipeRefreshLayout() {
        listIndex = 0;
        LOAD_MORE_COUNT = 0;
        visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
        makeJsonObjectRequest();
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    private void swipeRefreshLayoutWithOutApiHit() {
        LOAD_MORE_COUNT = 0;
        visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
        if (!finalFeedURL.contains("/1/"))
            LOAD_MORE_COUNT++;
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
    }

    private void sendTracking() {
        // Tracking
        if (isShowing) {
            if (cityInfo != null && trackingList != null) {
                while (trackingList.size() > 0) {
                    int value = trackingList.get(0) + 1;
                    trackingList.remove(0);
                    if (value > 1) {
                        value = value - 1;
                        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_DEPTH, displayName, cityInfo.cityName + "_" + AppFlyerConst.GALabel.PG + value, campaign);
                        Systr.println("GA Events : " + AppFlyerConst.GACategory.SCROLL_DEPTH + displayName + cityInfo.cityName + "_" + AppFlyerConst.GALabel.PG + value + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));
                    }
                }
            }
        }
    }

    private void onLoadMore() {
        isLoading = true;
        cityFeedListRecyclerView.post(() -> {
            if (cityFeedListAdapter != null)
                cityFeedListAdapter.addItem();
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                makeJsonObjectRequest();
            }
        }, 1000);
    }

    private void setOfflineCityFeedData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    showProgress();
                    boolean isShowInternetMessage = true;
                    if (cityFeedList == null) {
                        cityFeedList = new ArrayList<>();
                    } else {
                        cityFeedList.clear();
                    }
                    CityFeedListTable cityFeedListTable = (CityFeedListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CityFeedListTable.TABLE_NAME);
                    ArrayList<RelatedArticleInfo> infoList = cityFeedListTable.getAllCityFeedListInfoListAccordingToCity(cityInfo.cityId, getContext());
                    for (RelatedArticleInfo info : infoList) {
                        listIndex += 1;
                        info.listIndex = listIndex;
                    }
                    cityFeedList.addAll(infoList);

                    if (cityFeedList != null && cityFeedList.size() > 0) {
                        isShowInternetMessage = false;
                        addAdsAndWidgetInPrefferdCityList();
                    }
                    noNewsAvailable.setVisibility(View.GONE);
                    cityFeedListRecyclerView.setVisibility(View.VISIBLE);
                    cityFeedListAdapter = new CityFeedListAdapter(getContext(), gaScreen, gaArticle, displayName, gaEventLabel);
                    cityFeedListAdapter.setThemeColor(categoryInfo.color);
                    cityFeedListAdapter.setData(cityFeedList, cityInfo.cityHindiName);
                    cityFeedListRecyclerView.setAdapter(cityFeedListAdapter);
                    cityFeedListAdapter.notifyDataSetChanged();
                    cityFeedListRecyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            moveToTop(false);
                        }
                    });
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            removeLoader();
                        }
                    }, 500);
                    swipeRefreshLayoutWithOutApiHit();
                    checkInternetAndShowToast(isShowInternetMessage);
                } catch (Exception ex) {
                    AppLogs.printErrorLogs(TAG, ex.toString());
                }

            }
        }, Constants.DataBaseHit.DEFAULT_TIME);

    }

    private void checkInternetAndShowToast(Boolean isShowInternetMessage) {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
//                isTokenExpire();
            } else {
                if (isShowInternetMessage)
                    if (isAdded() && getActivity() != null)
                        AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));

            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }

    }

    private void isTokenExpire() {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                CityFeedListTable cityFeedListTable = (CityFeedListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CityFeedListTable.TABLE_NAME);

                String dbEntryCreationTime = cityFeedListTable.getCityFeedListItemCreationTime(cityInfo.cityId);

                if (dbEntryCreationTime != null && !dbEntryCreationTime.equalsIgnoreCase("")) {
                    long dbEntryCreationTimemilliSeconds = Long.parseLong(dbEntryCreationTime);
                    long currentTime = System.currentTimeMillis();

                    if ((currentTime - dbEntryCreationTimemilliSeconds) <= application.getServerTimeoutValue()) {
                        //token not Expire
                        removeLoader();
                        setOfflineCityFeedData();
                    } else {
                        //token Expire
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                removeLoader();
                                firstApiHit();
                            }
                        }, 500L);
                    }

                } else {
                    removeLoader();
                    firstApiHit();
                }
            } else {
//                removeLoader();
                setOfflineCityFeedData();
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }
    }

    private void firstApiHit() {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                AppLogs.printDebugLogs(TAG + "firstApiHit :", "firstApiHit");
                showProgress();
                swipeRefreshLayout();
            } else {
                removeLoader();
                if (isAdded() && getActivity() != null)
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }
    }

    private void makeJsonObjectRequest() {
        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            finalFeedURL = changeFinalUrlForLazyLoading();
            setAutoCompleteHint(cityInfo);
            AppLogs.printErrorLogs(TAG, "Final Feed Url= " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response != null) {
                                    if (!Constants.singleSessionCategoryIdList.contains(cityInfo.cityId)) {
                                        Constants.singleSessionCategoryIdList.add(cityInfo.cityId);
                                    }
                                    parseFeedList(response);
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    if (LOAD_MORE_COUNT >= 0) {
                        LOAD_MORE_COUNT--;
                    }
                    removeLoader();
                    Activity activity = getActivity();
                    if (activity != null && isAdded()) {
                        if (error instanceof NoConnectionError || error instanceof NetworkError) {
                            AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.no_network_error));
                        } else {
                            AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.sorry_error_found_please_try_again_));
                        }
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
            if (isAdded() && getActivity() != null) {
                AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
                removeLoader();
            }
        }
    }

    private void setAutoCompleteHint(CityInfo cityInfo) {
        if (cityInfo != null) {
            String cityName = String.format("%s (%s)", cityInfo.cityName, cityInfo.cityHindiName);//cityInfo.cityHindiName;
            selectCityAutocomplete.setHint(cityName);
        }
    }

    private void parseFeedList(JSONObject response) throws JSONException {
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        int responseSize = resultArray.length();

        if (responseSize > 0 && isAdded()) {

            if (LOAD_MORE_COUNT == 0) {
                cityFeedList.clear();
            }

            noNewsAvailable.setVisibility(View.GONE);
            cityFeedListRecyclerView.setVisibility(View.VISIBLE);

            for (int i = 0; i < responseSize; i++) {
                JSONObject feedJsonObject = resultArray.getJSONObject(i);
                Gson gson = new Gson();
                RelatedArticleInfo newsFeeds = gson.fromJson(feedJsonObject.toString(), RelatedArticleInfo.class);
                listIndex += 1;
                newsFeeds.listIndex = listIndex;
                cityFeedList.add(newsFeeds);
            }

            // Inserting DB CityFeedList in mdb
            final CityFeedListTable cityFeedListTable = (CityFeedListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CityFeedListTable.TABLE_NAME);
            if (LOAD_MORE_COUNT == 0) {
                cityFeedListTable.deleteParticularCityFeedList(cityInfo.cityId);
                cityFeedListTable.addCityFeedListItem(cityInfo, cityFeedList, getContext());
                if (!finalFeedURL.contains("/1/"))
                    LOAD_MORE_COUNT++;

                cityFeedListAdapter = new CityFeedListAdapter(getContext(), gaScreen, gaArticle, displayName, gaEventLabel);
                cityFeedListAdapter.setThemeColor(categoryInfo.color);
                cityFeedListRecyclerView.setAdapter(cityFeedListAdapter);
                addAdsAndWidgetInPrefferdCityList();
            }

            // Tracking
            if (!TextUtils.isEmpty(displayName)) {
                trackingList.add(LOAD_MORE_COUNT);
                sendTracking();
            }

            cityFeedListAdapter.setData(cityFeedList, cityInfo.cityHindiName);
            cityFeedListAdapter.notifyDataSetChanged();
            visibleThreshold += responseSize;

            if (LOAD_MORE_COUNT == 0)
                moveToTop(false);
        } else {
            if (LOAD_MORE_COUNT == 0) {
                noNewsAvailable.setVisibility(View.VISIBLE);
                cityFeedListRecyclerView.setVisibility(View.GONE);
            } else {
                LOAD_MORE_COUNT--;
            }
        }

        removeLoader();
    }


    private void addAdsAndWidgetInPrefferdCityList() {
        for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
            if (!bannerInfoList.get(bannerIndex).isAdded) {
                int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catPos);
                if (bannerPos > -1 && cityFeedList.size() >= bannerPos) {
                    cityFeedList.add(bannerPos, new RelatedArticleInfo(CityFeedListAdapter.VIEW_TYPE_BANNER, bannerInfoList.get(bannerIndex)));
                    bannerInfoList.get(bannerIndex).isAdded = true;
                }
            }
        }
    }

    private String changeFinalUrlForLazyLoading() {
        StringBuilder baseUrl = new StringBuilder(1);
        if (cityInfo != null) {
            baseUrl.append(Urls.APP_FEED_BASE_URL)
                    .append(String.format(Urls.DEFAULT_LIST_URL, CommonConstants.CHANNEL_ID))
                    .append(cityInfo.cityId)
                    .append("/");

            if (LOAD_MORE_COUNT > 0) {
                baseUrl.append("3/PG").append(LOAD_MORE_COUNT);
            } else if (LOAD_MORE_COUNT == 0) {
                baseUrl.append("1/PG1");
            }

            baseUrl.append("/");
            return baseUrl.toString();
        } else {
            return null;
        }
    }

    private void removeLoader() {
        if (LOAD_MORE_COUNT > 0) {
            if (cityFeedListAdapter != null) {
                cityFeedListAdapter.removeItem();
            }
            if (isLoading) {
                isLoading = false;
            }
        }
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        progressBar.setVisibility(View.GONE);
        if (layoutLoadingItem != null)
            layoutLoadingItem.setVisibility(View.GONE);
    }

    private void showProgress() {
        if (progressBar != null) {
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
        progressBar.setVisibility(View.VISIBLE);
        if (layoutLoadingItem != null)
            layoutLoadingItem.setVisibility(View.VISIBLE);
        if (cityFeedListRecyclerView != null)
            cityFeedListRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        super.onStop();
        isShowing = false;
    }

    @Subscribe
    public void NetworkConnectionStateChange(Boolean isConnected) {
        AppLogs.printDebugLogs(TAG + "Preferred getMessage1", "" + isConnected);
        if (isConnected && isVisible()) {
            AppLogs.printDebugLogs(TAG + "Preferred ConnectionC", "" + isConnected);
            showProgress();
            swipeRefreshLayout();
        } else {
        }
    }

    private int getLastSavedCityIndex() {
        String id = AppPreferences.getInstance(getContext()).getStringValue(String.format(QuickPreferences.SELECTED_PREF_CITY, CommonConstants.CHANNEL_ID), Constants.Utils.DEFAULT);
        CityInfo cityInfoBasedOnCityId = new CityInfo(id, Constants.CityPreference.CITY_INFO_BASED_ON_ID);
        if (cityInfoVector != null && cityInfoVector.size() > 0 && cityInfoVector.contains(cityInfoBasedOnCityId)) {
            return cityInfoVector.indexOf(cityInfoBasedOnCityId);
        }
        return 0;
    }

    private void addCityFeedFragment(int position) {
        showProgress();
        selectedPosition = position;
        if (cityInfoVector.size() > 0) {
            cityInfo = getSavedCityInfo();
            gaArticle = categoryInfo.gaArticle;
            gaScreen = categoryInfo.gaScreen;
            displayName = categoryInfo.displayName;
            gaEventLabel = categoryInfo.gaEventLabel;

            gaScreen += "-" + cityInfo.cityName;
            gaArticle += "-" + cityInfo.cityName;
            displayName += "_" + cityInfo.cityName;
            String themeColor = categoryInfo.color;

            if (cityFeedListAdapter != null) {
                cityFeedListAdapter.updateValues(gaScreen, gaArticle, displayName, gaEventLabel);

            }

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!NetworkStatus.getInstance().isConnected(getActivity()) || Constants.singleSessionCategoryIdList.contains(cityInfo.cityId)) {
                        isTokenExpire();
                    } else {
                        swipeRefreshLayout();
                    }
                }
            }, Constants.DataBaseHit.DEFAULT_TIME_IS_TOKEN);
        }
    }

    private void setFragmentShow() {
        isShowing = true;
        sendTracking();
    }

    private void getCityInfoVector() {
        cityInfoVector.addAll(JsonParser.getInstance().analyzePreferredCity(getActivity(), CommonConstants.CHANNEL_ID));

        // save the default value in the Vector
        SerializeData.getInstance(getContext()).setSelectedCityList(CommonConstants.CHANNEL_ID, cityInfoVector);

        cityAutoCompleteListAdapter = new NamesAdapter(getActivity(), R.layout.preferred_city_auto_complete_item, new ArrayList<>(cityInfoArrayList), cityInfoVector);
        selectCityAutocomplete.setThreshold(2);
        selectCityAutocomplete.setAdapter(cityAutoCompleteListAdapter);

        preferredCityAdapter.setCityInfoVector(cityInfoVector);
        setDataToAutoCompleteAdapter();

        // Save the selected City to the Serialized Data.
        if (cityInfoVector.size() > 0) {
            AppPreferences.getInstance(getContext()).setStringValue(String.format(QuickPreferences.SELECTED_PREF_CITY, CommonConstants.CHANNEL_ID), cityInfoVector.get(0).cityId);
        }
        int lastSelectionPosition = getLastSavedCityIndex();
        addCityFeedFragment(lastSelectionPosition);
        preferredCityAdapter.itemSelection(lastSelectionPosition);
    }

//    @Override
//    public void onDataFetch(boolean flag, BrandInfo brandInfo) {// Empty method.
//    }

    @Override
    public void onDataFetch(boolean flag) {
        cityInfoArrayList = JsonParser.getInstance().getCityFeedList(getContext(), CommonConstants.CHANNEL_ID);
        if (cityInfoArrayList != null && !cityInfoArrayList.isEmpty()) {
            getCityInfoVector();
        }
    }

    @Override
    public void onDataFetch(boolean flag, String sectionName) {
    }

    @Override
    public void onDataFetch(boolean flag, int viewType) {

    }

    @Override
    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {

    }

    public void refreshFragment(Vector<CityInfo> cityInfoVector) {
        if (linearLayoutManager != null && cityInfoVector != null) {
            preferredCityAdapter.setCityInfoVector(cityInfoVector);

            int lastSelectionPosition = getLastSavedCityIndex();
            addCityFeedFragment(lastSelectionPosition);

            linearLayoutManager.scrollToPosition(selectedPosition);
            preferredCityAdapter.itemSelection(0);
            onCityItemClick(0);
        }
        moveToTop(false);
    }

    public void refreshSearchFragment(Vector<CityInfo> cityInfoVector, int position) {
        if (linearLayoutManager != null && cityInfoVector != null) {
            preferredCityAdapter.setCityInfoVector(cityInfoVector);

            //int lastSelectionPosition = getLastSavedCityIndex();
            addCityFeedFragment(position);

            linearLayoutManager.scrollToPosition(position);
            preferredCityAdapter.itemSelection(position);
            onCityItemClick(position);
        }
        moveToTop(false);
    }

    public void moveToTop(boolean isSmoothScroll) {
        try {
            if (cityFeedListRecyclerView != null) {
                if (isSmoothScroll) {
                    cityFeedListRecyclerView.smoothScrollToPosition(0);
                } else {
                    cityFeedListRecyclerView.scrollToPosition(0);
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onCityItemClick(int cityPosition) {
        Systr.println("oncityitemclick" + cityPosition);

        // Save the selected City to the Serialized Data.
        AppPreferences.getInstance(getContext()).setStringValue(String.format(QuickPreferences.SELECTED_PREF_CITY, CommonConstants.CHANNEL_ID), cityInfoVector.get(cityPosition).cityId);

        int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
        int lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
        int centerPosition = (firstVisibleItemPosition + lastVisibleItemPosition) / 2;

        if (cityPosition > centerPosition) {
            preferredCityRecyclerView.smoothScrollToPosition(cityPosition + 1);
        } else if (cityPosition < centerPosition) {
            preferredCityRecyclerView.smoothScrollToPosition(cityPosition);
        }
        if (cityPosition == selectedPosition) {
            // set the position to top of the news List.
            moveToTop(true);
        } else {
            listIndex = 0;
            LOAD_MORE_COUNT = 0;
            visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
            addCityFeedFragment(cityPosition);

            setFragmentShow();

            // Tracking
            if (categoryInfo != null && cityInfoVector != null && cityInfoVector.size() > 0) {
                CityInfo selectedCityInfo = cityInfoVector.get(cityPosition);
                String gaScreen = categoryInfo.gaScreen + "-" + selectedCityInfo.cityName;// + ((TextUtils.isEmpty(selectedCityInfo.citySelectionValue) ? "" : "_" + selectedCityInfo.citySelectionValue));

                String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
                String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);

                String wisdomUrl = TrackingHost.getInstance(getContext()).getWisdomUrl(AppUrls.WISDOM_URL);
                String domain = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
                String subDomain = categoryInfo.displayName + "/" + selectedCityInfo.cityName;
                AppUtils.GLOBAL_DISPLAY_NAME = subDomain;
                String url = domain + subDomain;
                ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
                String email = "";
                String mobile = "";
                if (profileInfo != null) {
                    email = profileInfo.email;
                    mobile = profileInfo.mobile;
                }
                Tracking.trackWisdomListingPage(getContext(), wisdomUrl, url, email, mobile, CommonConstants.BHASKAR_APP_ID);
                Systr.println("Wisdom : " + url);
            }
        }
    }

    public String getSelectedCity() {
        if (cityInfoVector != null && cityInfoVector.size() > 0 && selectedPosition >= 0)
            return cityInfoVector.get(selectedPosition).cityName;

        return "";
    }

    public String getCitySelction() {
        if (cityInfoVector != null && cityInfoVector.size() > 0 && selectedPosition >= 0)
            return cityInfoVector.get(selectedPosition).citySelectionValue;
        return "";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void refreshData(PersonalizationEventWrapper eventWrapper) {
        if (eventWrapper.getRefreshScreen() == 1) {
            if (view != null) {
                initViews();
            }
        }
    }

    @Override
    public void onPauseFragment() {
    }

    @Override
    public void onResumeFragment() {
        outerSelectedCityInfo = cityInfo = getSavedCityInfo();
        setFragmentShow();
        swipeRefreshLayout();
//        TabController.getInstance().setPreferredCitySearchRefresh(this);
//        updatePreferredCity();
    }

    @Override
    public void RefreshList() {
        refreshFragment(cityInfoVector);
    }
}