package com.db.news;

import android.content.Context;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.RelatedArticleInfo;
import com.db.listeners.OnItemClickListener;
import com.db.listeners.OnRelatedArticleClickListener;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;

import java.util.ArrayList;
import java.util.List;


public class RecommendationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int RECOMMENDED_ARTICLE_NORMAL_IMAGE_ITEM = 1;
    private final int RECOMMENDED_ARTICLE_BIG_IMAGE_ITEM = 2;
    private final int RECOMMENDED_ARTICLE_VIEW_TYPE_THUMB = 3;

    public List<RelatedArticleInfo> recommendedArticles;
    private boolean recommendationDefaultLayout = false;
    private Context mContext;
    private OnRelatedArticleClickListener onRelatedArticleClickListener;
    private boolean isNewArticle;
    private String color;

    public RecommendationAdapter(Context context, OnRelatedArticleClickListener onRelatedArticleClickListener, List<RelatedArticleInfo> recommendedData, boolean isDayNight, boolean isOnFirstWall) {
        this.mContext = context;
        this.onRelatedArticleClickListener = onRelatedArticleClickListener;
        this.recommendedArticles = new ArrayList<>();
        if (recommendedData != null) {
            recommendedArticles = recommendedData;
        }
        recommendationDefaultLayout = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.RECOMMENDATIONS_LIST_LAYOUT, false);
    }

    public void setData(ArrayList<RelatedArticleInfo> data) {
        this.recommendedArticles.clear();
        this.recommendedArticles.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case RECOMMENDED_ARTICLE_VIEW_TYPE_THUMB:
                return new RelatedArticleViewHolder(inflater.inflate(R.layout.recommendation_thumb_type, parent, false));
            case RECOMMENDED_ARTICLE_NORMAL_IMAGE_ITEM:
                return new RelatedArticleViewHolder(inflater.inflate(R.layout.recommendation_article_normal_image_item, parent, false));
            case RECOMMENDED_ARTICLE_BIG_IMAGE_ITEM:
                return new BigImageViewHolder(inflater.inflate(R.layout.recomendation_article_big_image_item, parent, false));
            default:
                break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        boolean isNightMode = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.IS_NIGHT_MODE, false);
        switch (holder.getItemViewType()) {
            case RECOMMENDED_ARTICLE_NORMAL_IMAGE_ITEM:
                final RelatedArticleViewHolder relatedArticleViewHolder = (RelatedArticleViewHolder) holder;
                final RelatedArticleInfo info = recommendedArticles.get(position);
                AppUtils.getInstance().setFontSize(mContext, relatedArticleViewHolder.titleTextView, Constants.Font.CAT_SMALL_LARGE);

                relatedArticleViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(info.iitlTitle, AppUtils.getInstance().fromHtml(info.title), color));
                relatedArticleViewHolder.titleTextView.setTextColor(ContextCompat.getColor(mContext, R.color.article_title_color));
                relatedArticleViewHolder.rowLayout.findViewById(R.id.related_article_recycler_view_item_rl).setBackgroundColor(Color.WHITE);
                relatedArticleViewHolder.itemSeparator.setBackgroundColor(Color.LTGRAY);

                ImageUtil.setImage(mContext, info.image, relatedArticleViewHolder.ivThumb, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);
                if (info != null && info.videoFlag != null && info.videoFlag.equalsIgnoreCase("1")) {
                    relatedArticleViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
                } else {
                    relatedArticleViewHolder.imgVideoPlay.setVisibility(View.GONE);
                }

                relatedArticleViewHolder.setClickListener((view, position1) -> {
                    if (onRelatedArticleClickListener != null) {
                        onRelatedArticleClickListener.OnRelatedArticleClick(info);
                    }
                });
                break;
            case RECOMMENDED_ARTICLE_VIEW_TYPE_THUMB:
                final RelatedArticleViewHolder relatedArticleViewHolder1 = (RelatedArticleViewHolder) holder;
                final RelatedArticleInfo info1 = recommendedArticles.get(position);
                AppUtils.getInstance().setFontSize(mContext, relatedArticleViewHolder1.titleTextView, Constants.Font.CAT_SMALL);

                if (!TextUtils.isEmpty(info1.iitlTitle)) {
                    relatedArticleViewHolder1.titleTextView.setText(AppUtils.getInstance().getHomeTitle(info1.iitlTitle, AppUtils.getInstance().fromHtml(info1.title), color));
                } else {
                    if (!TextUtils.isEmpty(info1.title))
                        relatedArticleViewHolder1.titleTextView.setText(AppUtils.getInstance().fromHtml(info1.title));
                }
                relatedArticleViewHolder1.titleTextView.setTextColor(Color.parseColor("#333333"));
                relatedArticleViewHolder1.rowLayout.setBackgroundColor(Color.parseColor("#EFF3F6"));


                ImageUtil.setImage(mContext, info1.image, relatedArticleViewHolder1.ivThumb, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);

                if (info1.videoFlag != null && info1.videoFlag.equalsIgnoreCase("1")) {
                    relatedArticleViewHolder1.imgVideoPlay.setVisibility(View.VISIBLE);
                } else {
                    relatedArticleViewHolder1.imgVideoPlay.setVisibility(View.GONE);
                }

                relatedArticleViewHolder1.setClickListener((view, position12) -> {
                    if (onRelatedArticleClickListener != null) {
                        onRelatedArticleClickListener.OnRelatedArticleClick(info1);
                    }
                });
                break;

            case RECOMMENDED_ARTICLE_BIG_IMAGE_ITEM:
                final BigImageViewHolder bigImageViewHolder = (BigImageViewHolder) holder;
                final RelatedArticleInfo relatedArticleInfo = recommendedArticles.get(position);
                AppUtils.getInstance().setFontSize(mContext, bigImageViewHolder.titleTextView, Constants.Font.CAT_MEDIUM_LARGE);
                bigImageViewHolder.titleTextView.setText(relatedArticleInfo.title);

                float ratio = AppUtils.getInstance().parseImageRatio(relatedArticleInfo.imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
                AppUtils.getInstance().setImageViewSizeWithAspectRatio(bigImageViewHolder.ivThumb, ratio, 0, mContext);
                bigImageViewHolder.titleTextView.setTextColor(ContextCompat.getColor(mContext, R.color.article_title_color));
                bigImageViewHolder.rowLayout.setBackgroundColor(Color.WHITE);
                bigImageViewHolder.itemSeparator.setBackgroundColor(Color.LTGRAY);

                ImageUtil.setImage(mContext, relatedArticleInfo.image, bigImageViewHolder.ivThumb, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);


                if (relatedArticleInfo.videoFlag.equalsIgnoreCase("1")) {
                    bigImageViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
                    bigImageViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext,color));
                } else {
                    bigImageViewHolder.imgVideoPlay.setVisibility(View.GONE);
                }

                bigImageViewHolder.setClickListener((view, position13) -> {
                    if (onRelatedArticleClickListener != null) {
                        onRelatedArticleClickListener.OnRelatedArticleClick(relatedArticleInfo);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        int adCount = 0;
        return (recommendedArticles != null && recommendedArticles.size() > (15 + adCount)) ? (15 + adCount) : recommendedArticles.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isNewArticle) {
            return RECOMMENDED_ARTICLE_VIEW_TYPE_THUMB;
        } else {
            return (!recommendationDefaultLayout) ? RECOMMENDED_ARTICLE_NORMAL_IMAGE_ITEM : (recommendedArticles.get(position).bigImage == 0) ? RECOMMENDED_ARTICLE_NORMAL_IMAGE_ITEM : RECOMMENDED_ARTICLE_BIG_IMAGE_ITEM;
        }
    }

    public void useInNewArticle(boolean isNewArticle, String color) {
        this.isNewArticle = isNewArticle;
        this.color = color;
    }

    private class RelatedArticleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private ImageView ivThumb;
        private TextView titleTextView;
        private RelativeLayout rowLayout;
        private OnItemClickListener clickListener;
        private ImageView imgVideoPlay;
        private View itemSeparator;

        private RelatedArticleViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.list_item_name);
            ivThumb = itemView.findViewById(R.id.thumb_image);
            rowLayout = itemView.findViewById(R.id.related_article_recycler_view_item_rl);
            imgVideoPlay = itemView.findViewById(R.id.img_video_play);
            imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext,color));
            itemSeparator = itemView.findViewById(R.id.item_separator);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        private void setClickListener(OnItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
            return true;
        }
    }


    private class BigImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private LinearLayout rowLayout;
        private ImageView ivThumb;
        private TextView titleTextView;
        private ImageView imgVideoPlay;
        private View itemSeparator;
        private OnItemClickListener clickListener;

        private BigImageViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.list_item_name);
            ivThumb = itemView.findViewById(R.id.thumb_image);
            imgVideoPlay = itemView.findViewById(R.id.img_video_play);
            rowLayout = itemView.findViewById(R.id.row_layout);
            itemSeparator = itemView.findViewById(R.id.item_separator);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        private void setClickListener(OnItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getAdapterPosition());
            return true;
        }
    }
}