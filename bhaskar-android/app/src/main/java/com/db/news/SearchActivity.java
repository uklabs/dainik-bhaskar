package com.db.news;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.db.home.CommonListAdapter;
import com.bhaskar.util.LoginController;
import com.bhaskar.data.model.ProfileInfo;
import com.db.main.BaseAppCompatActivity;
import com.db.search.SearchFragmentDialog;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.ThemeUtil;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SearchActivity extends BaseAppCompatActivity implements SearchFragmentDialog.SearchItemListener {

    private String finalFeedURL, searchKeyword;
    private CommonListAdapter newsListAdapter;
    private ArrayList<NewsListInfo> newsInfoList;
    private ProgressBar progressBar;
    private TextView noRecordFound;
    private RecyclerView recyclerView;
    private CategoryInfo customAdaPrmsDisplayNameInfo = new CategoryInfo();

    private String mGaScreen;
    private String mGaArticle;
    private String mDisplayName;
    private int LOAD_MORE_COUNT = 1;
    private LinearLayoutManager linearLayoutManager;
    private SearchFragmentDialog searchFragmentDialog;
    private TextView tvTitle;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_search);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ica_back_black);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        try {
            getSupportActionBar().setShowHideAnimationEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        getSupportActionBar().setTitle(null);
        tvTitle = findViewById(R.id.toolbar_title);
        tvTitle.setTextColor(Color.BLACK);
        noRecordFound = findViewById(R.id.no_record_found);

        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(SearchActivity.this, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(Constants.KeyPair.KEY_KEYWORD)) {
            searchKeyword = extras.getString(Constants.KeyPair.KEY_KEYWORD);
            mGaScreen = extras.getString(Constants.KeyPair.KEY_GA_SCREEN);
            mGaArticle = extras.getString(Constants.KeyPair.KEY_GA_ARTICLE);
            mDisplayName = extras.getString(Constants.KeyPair.KEY_GA_DISPLAY_NAME);
        }

        customAdaPrmsDisplayNameInfo.displayName = "Search";

        recyclerView = findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        startSearching();
    }

    public void setHeaderTitle(String headerTitle) {
        tvTitle.setText(headerTitle);
    }

    private void startSearching() {
        progressBar.setVisibility(View.VISIBLE);
        noRecordFound.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);

        setHeaderTitle(searchKeyword);

        finalFeedURL = String.format(Urls.SEARCH_NEWS_API_URL, CommonConstants.CHANNEL_ID, searchKeyword);
        newsListAdapter = new CommonListAdapter(SearchActivity.this, customAdaPrmsDisplayNameInfo.gaScreen, customAdaPrmsDisplayNameInfo.color, customAdaPrmsDisplayNameInfo);
        SpacesDecoration spacesDecoration = new SpacesDecoration(0, 0, 2, 0);
        recyclerView.addItemDecoration(spacesDecoration);
        recyclerView.setAdapter(newsListAdapter);

        customAdaPrmsDisplayNameInfo.gaScreen = mGaScreen + "-" + searchKeyword;
        customAdaPrmsDisplayNameInfo.gaArticle = mGaArticle + "-" + searchKeyword;
        customAdaPrmsDisplayNameInfo.displayName = mDisplayName + "_" + searchKeyword;

        if (NetworkStatus.getInstance().isConnected(this)) {
            makeJsonArrayRequest();
        } else {
            AppUtils.getInstance().showCustomToast(this, getResources().getString(R.string.no_network_error));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.ac_search_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_search:
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                searchFragmentDialog = SearchFragmentDialog.newInstance();
                searchFragmentDialog.registerListener(SearchActivity.this);
                transaction.addToBackStack("searchdialog");
                searchFragmentDialog.show(transaction, "searchdialog");
                break;

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private void makeJsonArrayRequest() {
        AppLogs.printErrorLogs("Feed URL", "== " + AppUtils.getInstance().updateApiUrl(finalFeedURL));

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                response -> {

                    if (LOAD_MORE_COUNT == 1) {
                        String gaScreen = mGaScreen + "-" + searchKeyword;

                        if (newsListAdapter != null) {
                            newsListAdapter.updateGA(gaScreen);
                        }
                        String source = AppPreferences.getInstance(SearchActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
                        String medium = AppPreferences.getInstance(SearchActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                        String campaign = AppPreferences.getInstance(SearchActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAScreen(SearchActivity.this, InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);

                        String wisdomUrl = TrackingHost.getInstance(SearchActivity.this).getWisdomUrl(AppUrls.WISDOM_URL);
                        String domain = AppPreferences.getInstance(SearchActivity.this).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
                        String url = domain + gaScreen;
                        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
                        String email = "";
                        String mobile = "";
                        if (profileInfo != null) {
                            email = profileInfo.email;
                            mobile = profileInfo.mobile;
                        }
                        Tracking.trackWisdomListingPage(SearchActivity.this, wisdomUrl, url, email, mobile, CommonConstants.BHASKAR_APP_ID);
                    }

                    try {
                        if (!TextUtils.isEmpty(response.toString()) && response.length() > 0) {
                            noRecordFound.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            parseJsonArrayFeedList(response);
                            AppLogs.printErrorLogs("Feed Response", "== " + response.toString());
                        } else {
                            noRecordFound.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        noRecordFound.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                    progressBar.setVisibility(View.GONE);
                },
                error -> {
                    try {
                        AppUtils.getInstance().showCustomToast(SearchActivity.this, getString(R.string.sorry_error_found_please_try_again_));
                        noRecordFound.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);

                    } catch (Exception e) {
                        AppLogs.printErrorLogs("Error", "= " + e.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonArrayRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void parseJsonArrayFeedList(JSONArray response) {
        if (newsInfoList == null) {
            newsInfoList = new ArrayList<>();
        }
        newsInfoList.clear();
        newsInfoList.addAll(Arrays.asList(new Gson().fromJson(response.toString(), NewsListInfo[].class)));
        newsListAdapter.setData(newsInfoList, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID));
        newsListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onItemClicked(String searchKeyword) {
        this.searchKeyword = searchKeyword;
        startSearching();
        if (searchFragmentDialog != null) {
            searchFragmentDialog.dismiss();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == Constants.REQ_CODE_BOTTOM_NAV) {
            setResult(Activity.RESULT_OK);
            finish();
        }
    }
}
