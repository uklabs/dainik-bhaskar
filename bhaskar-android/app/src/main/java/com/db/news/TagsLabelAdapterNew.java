package com.db.news;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.view.ui.MainActivity;
import com.bhaskar.R;
import com.db.divya_new.articlePage.DivyaArticleInteraction;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;

public class TagsLabelAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int MAIN_LIST = 1;
    private String[] mDataSet;
    private Activity mContext;
    private DivyaArticleInteraction onTagClickListener;

    public TagsLabelAdapterNew(Activity context, DivyaArticleInteraction onTagClickListener, String tagsDataSet) {
        mDataSet = new String[0];
        this.mContext = context;
        this.onTagClickListener = onTagClickListener;
        mDataSet = tagsDataSet.split(",");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case MAIN_LIST:
                return new ViewHolder(inflater.inflate(R.layout.tags_label_item_view, parent, false));
            default:
                break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case MAIN_LIST:
                final ViewHolder mainViewHolder = (ViewHolder) holder;
                mainViewHolder.tagsName.setText(String.format("%s", mDataSet[position]));
                AppUtils.getInstance().setArticleFontSize(mContext, mainViewHolder.tagsName, Constants.Font.CAT_SMALL_LARGE);

                mainViewHolder.tagsName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String searchKeyword = mDataSet[mainViewHolder.getAdapterPosition()];
                        String gaArticle = AppFlyerConst.GAExtra.TAG_GA_ARTICLE;
                        String gaScreen = AppFlyerConst.GAExtra.TAG_GA_SCREEN;

                        Intent searchIntent = new Intent(mContext, SearchActivity.class);
                        searchIntent.putExtra(Constants.KeyPair.KEY_KEYWORD, searchKeyword);
                        searchIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
                        searchIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
                        searchIntent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, gaScreen);
                        searchIntent.putExtra(Constants.KeyPair.KEY_IS_SEARCH, false);
                        if (onTagClickListener != null) {
                            onTagClickListener.onTagItemClicked();
                        }
                        mContext.startActivityForResult(searchIntent, Constants.REQ_CODE_BOTTOM_NAV);
                    }
                });
                return;

            default:
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        return MAIN_LIST;
    }


    @Override
    public int getItemCount() {
        return mDataSet.length;
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tagsName;

        private ViewHolder(View itemView) {
            super(itemView);
            tagsName = (TextView) itemView.findViewById(R.id.tag_name);

            itemView.setTag(itemView);
        }
    }
}