package com.db.news;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bhaskar.util.LoginController;
import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.home.ActivityUtil;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.OpenNewsLinkInApp;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.db.util.VideoEnabledWebChromeClient;
import com.db.views.VideoEnabledWebView;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;


public class WapV2Activity extends BaseAppCompatActivity implements VideoEnabledWebChromeClient.WebClientCallbackListener {

    private static final int REQUEST_CODE_DOWNLOAD_PERMISSIONS = 11;
    private static final int REQUEST_CODE_UPLOAD_PERMISSIONS = 12;
    private static final int FILE_SELECT_CODE = 101;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    ValueCallback<Uri[]> mainFilePathCallback;
    private VideoEnabledWebView myWebView;
    private ProgressBar progressBar;
    private String mainUrl, title;
    private String fileUrl;
    private String gaScreen;
    private boolean bundleIsFromPush;
    private TextView headerTv;
    private WebView mWebviewPop;
    private AlertDialog builder;
    private boolean isMainProgress = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_wap_v2);

        /*Tool Bar*/
        headerTv = findViewById(R.id.header_tv);
        ImageView closeIv = findViewById(R.id.close_iv);
        closeIv.setOnClickListener(v -> handleBackActivity());
        Tracker tracker = ((InitApplication) getApplication()).getDefaultTracker();
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mainUrl = intent.getStringExtra(Constants.KeyPair.KEY_URL);
            mainUrl = mainUrl.replace("http-", "https:");
            mainUrl = mainUrl.replace("https-", "https:");

            if (!TextUtils.isEmpty(mainUrl)) {
                AppLogs.printDebugLogs("ACTION URL", mainUrl);

            }
            title = intent.getStringExtra(Constants.KeyPair.KEY_TITLE);
            gaScreen = intent.getStringExtra(Constants.KeyPair.KEY_GA_SCREEN);
            bundleIsFromPush = intent.getBooleanExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, false);
        }
//        headerTv.setText(mainUrl);
        progressBar = findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(WapV2Activity.this, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        progressBar.setPadding(0, 0, 0, 0);
        progressBar.setMax(100);

//        setUpWebView();

        // Tracking
        String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(this, tracker, gaScreen, source, medium, campaign);
    }

    private void handleBackActivity() {
        if (bundleIsFromPush) {
            Intent intent = new Intent(WapV2Activity.this, MainActivity.class);
            startActivity(intent);
        }
        finish();
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    public void setUpWebView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        myWebView = findViewById(R.id.webview);

        View nonVideoLayout = findViewById(R.id.nonVideoLayout);
        ViewGroup videoLayout = findViewById(R.id.videoLayout);
        @SuppressLint("InflateParams") View loadingView = getLayoutInflater().inflate(R.layout.view_loading_video, null);

        VideoEnabledWebChromeClient videoEnabledWebChromeClient = new VideoEnabledWebChromeClient(nonVideoLayout, videoLayout, loadingView, myWebView);

        videoEnabledWebChromeClient.setOnWebClientListener(this);
        videoEnabledWebChromeClient.setOnToggledFullscreen(fullscreen -> {
            changeOrientation(fullscreen);
            myWebView.loadUrl("javascript:isVideoMode('" + fullscreen + "')");
            if (fullscreen) {
                WindowManager.LayoutParams attrs = getWindow().getAttributes();
                attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
                attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                getWindow().setAttributes(attrs);
                if (Build.VERSION.SDK_INT >= 19) {
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE);
                }
            } else {
                WindowManager.LayoutParams attrs = getWindow().getAttributes();
                attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
                attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                getWindow().setAttributes(attrs);
                if (Build.VERSION.SDK_INT >= 19) {
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                }
            }
        });

        myWebView.setWebChromeClient(videoEnabledWebChromeClient);
//        myWebView.setWebChromeClient(new WebViewActivity.UriChromeClient());

        WebSettings webSettings = myWebView.getSettings();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webSettings.setMediaPlaybackRequiresUserGesture(false);
        }
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        /*Custom User Agent*/
        String userAgent = webSettings.getUserAgentString();
        userAgent = userAgent + ";CustomUserAgent:" + CommonConstants.USER_AGENT;
        webSettings.setUserAgentString(userAgent);
        webSettings.setMediaPlaybackRequiresUserGesture(false);
        JavaScriptInterface JSInterface = new JavaScriptInterface(this);
        myWebView.addJavascriptInterface(JSInterface, "DivyaJSInterface");

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.setAcceptThirdPartyCookies(myWebView, true);
        }

        if (!TextUtils.isEmpty(title) && title.equalsIgnoreCase("Voucher")) {
            myWebView.loadData(mainUrl, "text/html", "utf-8");
        } else {
            myWebView.loadUrl(mainUrl);
        }
        myWebView.setWebViewClient(new MyWebViewClient());

        // download listener
        myWebView.setDownloadListener((url, userAgent1, contentDisposition, mimetype, contentLength) -> {
            fileUrl = url;
            checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
        });

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        myWebView.setLayoutParams(new RelativeLayout.LayoutParams(width, height - 100));
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            findViewById(R.id.actionBar).setVisibility(View.GONE);
        } else {
            findViewById(R.id.actionBar).setVisibility(View.VISIBLE);
        }
    }

    public void changeOrientation(boolean fullscreen) {
        if (!fullscreen) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (myWebView == null) {
            setUpWebView();
        }
    }

    public void setValue(int progress) {
        progressBar.setProgress(progress);
        if (progress == 100)
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onPause() {
        super.onPause();
        if (myWebView != null) {
            myWebView.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myWebView != null) {
            myWebView.onResume();
        }
    }

    @Override
    protected void onStop() {
        try {
            Class.forName("android.webkit.WebView")
                    .getMethod("onPause", (Class[]) null)
                    .invoke(myWebView, (Object[]) null);

        } catch (ClassNotFoundException ignored) {
        } catch (NoSuchMethodException ignored) {
        } catch (InvocationTargetException ignored) {
        } catch (IllegalAccessException ignored) {
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myWebView != null) {
            myWebView.destroy();
            myWebView = null;
        }
    }

    /*******
     * Permission
     * */
    private void checkForPermission(int type) {
        ArrayList<String> permissionArray = new ArrayList<>();
        if (type == REQUEST_CODE_DOWNLOAD_PERMISSIONS) {
            try {
                if (ContextCompat.checkSelfPermission(WapV2Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
            } catch (Exception ignored) {
            }
            if (isAllPermissionProvided(type) && permissionArray.size() > 0) {
                ActivityCompat.requestPermissions(WapV2Activity.this, permissionArray.toArray(new String[permissionArray.size()]), REQUEST_CODE_DOWNLOAD_PERMISSIONS);
            } else {
                downloadFile();
            }
        } else {
            try {
                if (ContextCompat.checkSelfPermission(WapV2Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                }
            } catch (Exception ignored) {
            }
            if (isAllPermissionProvided(type) && permissionArray.size() > 0) {
                ActivityCompat.requestPermissions(WapV2Activity.this, permissionArray.toArray(new String[permissionArray.size()]), REQUEST_CODE_UPLOAD_PERMISSIONS);
            } else {
                showFileChooser();
            }
        }

    }

    private boolean isAllPermissionProvided(int type) {
        if (type == REQUEST_CODE_DOWNLOAD_PERMISSIONS) {
            try {
                if ((ContextCompat.checkSelfPermission(WapV2Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    return true;
                }
            } catch (Exception ignored) {
            }
        } else {
            try {
                if ((ContextCompat.checkSelfPermission(WapV2Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    return true;
                }
            } catch (Exception ignored) {
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_DOWNLOAD_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    downloadFile();
                } else {
                    AppUtils.getInstance().showCustomToast(WapV2Activity.this, getResources().getString(R.string.file_download_error));
                }
                break;
            case REQUEST_CODE_UPLOAD_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showFileChooser();
                } else {
                    AppUtils.getInstance().showCustomToast(WapV2Activity.this, getResources().getString(R.string.file_download_error));
                }
                break;
        }
    }

    private void downloadFile() {
        try {
            String[] temp = fileUrl.split("/");
            String fileNameWithExtn = "";
            if (temp.length > 0) {
                fileNameWithExtn = temp[temp.length - 1];
            }
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fileUrl));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileNameWithExtn);
            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            assert dm != null;
            dm.enqueue(request);
            //To notify the Client that the file is being downloaded
            AppUtils.getInstance().showCustomToast(getApplicationContext(), "Downloading File");
        } catch (Exception e) {
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        handleBackActivity();
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (Exception ex) {
            // Potentially direct the user to the Market with a Dialog
            AppUtils.getInstance().showCustomToast(WapV2Activity.this, "Please install a File Manager.");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    try {
                        // Get the Uri of the selected file
                        Uri uri = data.getData();
                        if (mainFilePathCallback != null) {
                            Uri[] uris = new Uri[1];
                            uris[0] = uri;
                            mainFilePathCallback.onReceiveValue(uris);
                        }
                        // Get the path
                        String path = null;

                        path = AppUtils.getInstance().getPath(WapV2Activity.this, uri);
                        Log.d("WapV2", "File Path: " + path);
                    } catch (Exception ignored) {
                    }
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                } else {
                    if (mainFilePathCallback != null) {
                        mainFilePathCallback.onReceiveValue(null);
                    }
                }
                break;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void showWeb(Message resultMsg) {
        isMainProgress = true;
        mWebviewPop = new WebView(WapV2Activity.this);
        mWebviewPop.setVerticalScrollBarEnabled(false);
        mWebviewPop.setHorizontalScrollBarEnabled(false);
        mWebviewPop.setWebViewClient(new MyWebViewClient());
        mWebviewPop.setWebChromeClient(new VideoEnabledWebChromeClient());
        WebSettings webSettings = mWebviewPop.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSavePassword(true);
        webSettings.setSaveFormData(true);

        mWebviewPop.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            fileUrl = url;
            checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
        });
        AlertDialog.Builder b = new AlertDialog.Builder(WapV2Activity.this, R.style.Theme_AppCompat_Light_Dialog);
        b.setTitle("");
        b.setView(mWebviewPop);
        b.setPositiveButton("Close", (dialog, id) -> {
            mWebviewPop.destroy();
            dialog.dismiss();
        });
        builder = b.create();
        builder.show();
        builder.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.setAcceptThirdPartyCookies(mWebviewPop, true);
        }
        WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
        transport.setWebView(mWebviewPop);
        resultMsg.sendToTarget();
    }

    @Override
    public void onCloseWindow(WebView window) {
        isMainProgress = false;
        try {
            if (mWebviewPop != null)
                mWebviewPop.destroy();
        } catch (Exception ignored) {
        }
        try {
            builder.dismiss();
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        if (!isMainProgress) {
            progressBar.setVisibility(View.VISIBLE);
            if (newProgress < 15)
                setValue(15);
            else
                setValue(newProgress);
        }
    }

    @Override
    public void onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
        showWeb(resultMsg);
    }

    @Override
    public void onCheckForPermission(ValueCallback<Uri[]> filePathCallback) {
        if (filePathCallback != null) {
            mainFilePathCallback = filePathCallback;
            checkForPermission(REQUEST_CODE_UPLOAD_PERMISSIONS);
        }
    }

    class MyWebViewClient extends WebViewClient {

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Systr.println("WEB VIEW : " + request.getUrl());
            String url = request.getUrl().toString();
            try {
                if (url.startsWith("whatsapp://") || url.startsWith("mailto:") || url.startsWith("tel:")) {
                    if (builder != null) {
                        builder.dismiss();
                    }
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
                    OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(WapV2Activity.this, url, "", 1, "", null);
                    openNewsDetailInApp.openLink();
                    return true;
                }

            } catch (Exception e) {
                AppUtils.getInstance().showCustomToast(WapV2Activity.this, WapV2Activity.this.getResources().getString(R.string.sorry_error_found_please_try_again_));
            }
            if (!TextUtils.isEmpty(url)) {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Systr.println("WEB VIEW : " + url);
            try {
                if (url != null && (url.startsWith("whatsapp://") || url.startsWith("mailto:") || url.startsWith("tel:"))) {
                    if (builder != null) {
                        builder.dismiss();
                    }
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
                    OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(WapV2Activity.this, url, "", 1, "", null);
                    openNewsDetailInApp.openLink();
                    return true;
                }
            } catch (Exception e) {
                AppUtils.getInstance().showCustomToast(WapV2Activity.this, WapV2Activity.this.getResources().getString(R.string.sorry_error_found_please_try_again_));
            }
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            Systr.println("WEB VIEW : " + error);
            if (handler != null)
                handler.proceed();
            super.onReceivedSslError(view, handler, error);
        }
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void setdata(final String title) {
            try {
                runOnUiThread(() -> headerTv.setText(title));
            } catch (Exception e) {
            }
        }

        @JavascriptInterface
        public void downloadFile(String url) {
            fileUrl = url;
            checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
        }

        @JavascriptInterface
        public void fbComplete() {
            Systr.println("fb completed called");
            try {
                if (builder != null) {
                    builder.dismiss();
                }
            } catch (Exception e) {
            }
        }

        @JavascriptInterface
        public void openLink(String url) {
            OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(mContext, url, "", 0, "", null);
            openNewsDetailInApp.openLink();
        }

        @JavascriptInterface
        public void openMenu(String menuId) {
            ActivityUtil.openCategoryAccordingToAction(mContext, JsonParser.getInstance().getCategoryInfoBasedOnId(mContext, menuId), "");
        }

        @JavascriptInterface
        public void openFullPage(String pageUrl) {
            try {
                JSONObject jsonObject = new JSONObject(pageUrl);
                Intent webTV = new Intent(mContext, WebFullScreenActivity.class);
                webTV.putExtra(Constants.KeyPair.KEY_URL, jsonObject.optString("url"));
                webTV.putExtra(Constants.KeyPair.KEY_GA_SCREEN, pageUrl);
                webTV.putExtra(Constants.KeyPair.KEY_HAS_HEADER, jsonObject.optString("isHeader").equalsIgnoreCase("1"));
                mContext.startActivity(webTV);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void closeScreen() {
            WapV2Activity.this.finish();
        }

        @JavascriptInterface
        public void appTrackEvent(String jsonData) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                String catName = jsonObject.optString("catName");
                JSONArray eventArray = jsonObject.optJSONArray("events");

                ArrayList<ExtraCTEvent> ctEventArrayList = new ArrayList<>();
                String eventLabel = "";
                String eventValue = "";

                JSONObject jsObj;
                for (int i = 0; i < eventArray.length(); i++) {
                    jsObj = eventArray.getJSONObject(i);
                    if (i == 0) {
                        eventLabel = jsObj.optString("key");
                        eventValue = jsObj.optString("value");
                    } else {
                        if (!TextUtils.isEmpty(jsObj.optString("key"))) {
                            ctEventArrayList.add(new ExtraCTEvent(jsObj.optString("key"), jsObj.optString("value")));
                        }
                    }
                }

                Tracking.trackGAEvent(WapV2Activity.this, InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.SPECIAL_CONTENT, CTConstant.PROP_NAME.CLICKON, eventValue, "");
                CleverTapDB.getInstance(WapV2Activity.this).cleverTapTrackEvent(WapV2Activity.this, catName, eventLabel, eventValue, ctEventArrayList, LoginController.getUserDataMap());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void appTrackScreen(String screenName) {
            String source = AppPreferences.getInstance(WapV2Activity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(WapV2Activity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(WapV2Activity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

            Tracking.trackGAScreen(WapV2Activity.this, InitApplication.getInstance().getDefaultTracker(), screenName, source, medium, campaign);
        }
        @JavascriptInterface
        public void rashiDetail(String jsonData) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                String gender = jsonObject.optString("gender");
                String rashiName = jsonObject.optString("rashiName");
                String numerono = jsonObject.optString("numerono");
                Log.e("Event Tracking ", "gender" + gender + "=> rashiName" + rashiName + "=> numerono" + numerono);

                CleverTapDB.getInstance(WapV2Activity.this).updateRashifalValuesOfUser(gender, rashiName, numerono);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
