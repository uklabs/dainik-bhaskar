package com.db.news;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.home.ActivityUtil;
import com.bhaskar.util.LoginController;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.OpenNewsLinkInApp;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.db.util.VideoEnabledWebChromeClient;
import com.db.views.VideoEnabledWebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

import static android.view.ViewGroup.FOCUS_BLOCK_DESCENDANTS;

public class WebFullScreenActivity extends BaseAppCompatActivity {
    private static final int REQUEST_CODE_DOWNLOAD_PERMISSIONS = 11;
    private static final int REQUEST_CODE_UPLOAD_PERMISSIONS = 12;
    private static final int FILE_SELECT_CODE = 101;
    private static final int LOGIN_REQUEST_CODE = 120;

    private String mainUrl;

    private boolean isMainProgress = false;
    private WebView mWebviewPop;
    private AlertDialog builder;
    ValueCallback<Uri[]> mainFilePathCallback;

    private VideoEnabledWebView myWebView;
    private String gaScreen, extraValues;
    private boolean hasHeader;
    private String fileUrl;
    private boolean isShowLoginDialog;
    private boolean isTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_web_fullscreen);

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mainUrl = intent.getStringExtra(Constants.KeyPair.KEY_URL);
            gaScreen = intent.getStringExtra(Constants.KeyPair.KEY_GA_SCREEN);
            extraValues = intent.getStringExtra(Constants.KeyPair.KEY_EXTRA_VALUES);
            hasHeader = intent.getBooleanExtra(Constants.KeyPair.KEY_HAS_HEADER, false);
            isTV = intent.getBooleanExtra(Constants.KeyPair.KEY_IS_TV,false);
        }
        if (!TextUtils.isEmpty(extraValues)) {
            try {
                JSONObject jsonObject = new JSONObject(extraValues);
                hasHeader = jsonObject.optString("has_header").equalsIgnoreCase("1");
            } catch (JSONException ignored) {

            }
        }
        setUpWebView();

        String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(this, InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void setUpWebView() {
        myWebView = findViewById(R.id.webView);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        if (CommonConstants.CHANNEL_ID.equalsIgnoreCase(Constants.AppIds.DAINIK_BHASKAR)) {
            myWebView.addJavascriptInterface(new JavaScriptInterface(), "DBAnalyticsWebInterface");
        }else{
            myWebView.addJavascriptInterface(new JavaScriptInterface(), "DivyaJSInterface");
        }
        myWebView.getSettings().setLoadWithOverviewMode(true);
        myWebView.getSettings().setSupportZoom(false);
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        myWebView.getSettings().setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            myWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        myWebView.setFocusable(false);
        myWebView.setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);
        myWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);


        // Initialize the VideoEnabledWebChromeClient and set event handlers
        View nonVideoLayout = findViewById(R.id.nonVideoLayout); // Your own view, read class comments
        ViewGroup videoLayout = findViewById(R.id.videoLayout); // Your own view, read class comments
        //noinspection all
        View loadingView = getLayoutInflater().inflate(R.layout.view_loading_video, null); // Your own view, read class comments
        // See all available constructors...
        // Subscribe to standard events, such as onProgressChanged()...
        VideoEnabledWebChromeClient webChromeClient = new VideoEnabledWebChromeClient(nonVideoLayout, videoLayout, loadingView, myWebView) // See all available constructors...
        {
            // Subscribe to standard events, such as onProgressChanged()...
            @Override
            public void onProgressChanged(WebView view, int progress) {
            }

            @Override
            public Bitmap getDefaultVideoPoster() {
                return Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
            }

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                isMainProgress = true;
                mWebviewPop = new WebView(WebFullScreenActivity.this);
                mWebviewPop.setVerticalScrollBarEnabled(false);
                mWebviewPop.setHorizontalScrollBarEnabled(false);
                mWebviewPop.setWebViewClient(new WebViewClient());
                mWebviewPop.setWebChromeClient(new WebChromeClient());
                mWebviewPop.getSettings().setJavaScriptEnabled(true);
                mWebviewPop.getSettings().setSavePassword(true);
                mWebviewPop.getSettings().setSaveFormData(true);
                AlertDialog.Builder b = new AlertDialog.Builder(WebFullScreenActivity.this, R.style.Theme_AppCompat_Light_Dialog);
                b.setTitle("");
                b.setView(mWebviewPop);
                b.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        mWebviewPop.destroy();
                        dialog.dismiss();
                    }
                });
                builder = b.create();
                builder.show();
                builder.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                CookieManager cookieManager = CookieManager.getInstance();
                cookieManager.setAcceptCookie(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cookieManager.setAcceptThirdPartyCookies(mWebviewPop, true);
                }
                WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                transport.setWebView(mWebviewPop);
                resultMsg.sendToTarget();
                return true;


            }

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                mainFilePathCallback = filePathCallback;
                checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
                return true;
            }


            @Override
            public void onCloseWindow(WebView window) {
                isMainProgress = false;
                try {
                    mWebviewPop.destroy();
                } catch (Exception e) {

                }
                try {
                    builder.dismiss();

                } catch (Exception e) {

                }
            }
        };
        webChromeClient.setOnToggledFullscreen(fullscreen -> {
            // Your code to handle the full-screen change, for example showing and hiding the title bar. Example:
            if (fullscreen) {
                WindowManager.LayoutParams attrs = getWindow().getAttributes();
                attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
                attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                getWindow().setAttributes(attrs);
                //noinspection all
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else {
                WindowManager.LayoutParams attrs = getWindow().getAttributes();
                attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
                attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                getWindow().setAttributes(attrs);
                //noinspection all
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

        });
        myWebView.setWebChromeClient(webChromeClient);
        // Call private class InsideWebViewClient

        // Navigate anywhere you want, but consider that this classes have only been tested on YouTube's mobile site
        if (hasHeader) {
            myWebView.loadUrl(mainUrl, AppUtils.getInstance().getWebHeader(WebFullScreenActivity.this));
        } else {
            myWebView.loadUrl(mainUrl);
        }


    }

    @Override
    public void onBackPressed() {
        if (!myWebView.canGoBack()) {
            super.onBackPressed();
        } else {
            myWebView.goBack();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myWebView != null) {
            myWebView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (myWebView != null) {
            myWebView.onPause();
        }
    }

    @Override
    protected void onStop() {
        try {
            Class.forName("android.webkit.WebView")
                    .getMethod("onPause", (Class[]) null)
                    .invoke(myWebView, (Object[]) null);

        } catch (ClassNotFoundException cnfe) {
        } catch (NoSuchMethodException nsme) {
        } catch (InvocationTargetException ite) {
        } catch (IllegalAccessException iae) {
        }
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        myWebView.destroy();
    }

    public class JavaScriptInterface {

        JavaScriptInterface() {
        }

        @JavascriptInterface
        public void openLink(String url) {
            OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(WebFullScreenActivity.this, url, "", 1, "", null);
            openNewsDetailInApp.setFlag(false);
            openNewsDetailInApp.openLink();
        }

        @JavascriptInterface
        public void openMenu(String menuId) {
            Toast.makeText(WebFullScreenActivity.this, "Menu= " + menuId, Toast.LENGTH_SHORT).show();
            ActivityUtil.openCategoryAccordingToAction(WebFullScreenActivity.this, JsonParser.getInstance().getCategoryInfoBasedOnId(WebFullScreenActivity.this, menuId), "");
        }

        @JavascriptInterface
        public void openFullPage(String pageUrl) {
            try {
                JSONObject jsonObject = new JSONObject(pageUrl);
                Intent webTV = new Intent(WebFullScreenActivity.this, WebFullScreenActivity.class);
                webTV.putExtra(Constants.KeyPair.KEY_URL, jsonObject.optString("url"));
                webTV.putExtra(Constants.KeyPair.KEY_GA_SCREEN, pageUrl);
                webTV.putExtra(Constants.KeyPair.KEY_HAS_HEADER, jsonObject.optString("isHeader").equalsIgnoreCase("1"));
                startActivity(webTV);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void closeScreen() {
            WebFullScreenActivity.this.finish();
        }

        @JavascriptInterface
        public void performAction(String json, String jsonObject) {
            try {
                JSONObject jo = new JSONObject(jsonObject);
                String linkType = jo.getString("linkType");
                if (linkType.equalsIgnoreCase("closeWebView"))
                    WebFullScreenActivity.this.finish();
                else if(linkType.equalsIgnoreCase("news")){
//                    OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(WebFullScreenActivity.this, url, "", 1, "", null);
//                    openNewsDetailInApp.openLink();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @JavascriptInterface
        public void downloadFile(String url) {
            fileUrl = url;
            checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
        }

        @JavascriptInterface
        public void fbComplete() {
            Systr.println("fb completed called");
            try {
                if (builder != null) {
                    builder.dismiss();
                }
            } catch (Exception e) {
            }
        }

        @JavascriptInterface
        public void login(String msg) {
            showLoginDialog(msg);
        }

        @JavascriptInterface
        public void appTrackEvent(String jsonData) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                String catName = jsonObject.optString("catName");
                JSONArray eventArray = jsonObject.optJSONArray("events");

                ArrayList<ExtraCTEvent> ctEventArrayList = new ArrayList<>();
                String eventLabel = "";
                String eventValue = "";

                JSONObject jsObj;
                for (int i = 0; i < eventArray.length(); i++) {
                    jsObj = eventArray.getJSONObject(i);
                    if (i == 0) {
                        eventLabel = jsObj.optString("key");
                        eventValue = jsObj.optString("value");
                    } else {
                        if (!TextUtils.isEmpty(jsObj.optString("key"))) {
                            ctEventArrayList.add(new ExtraCTEvent(jsObj.optString("key"), jsObj.optString("value")));
                        }
                    }
                }

                Tracking.trackGAEvent(WebFullScreenActivity.this, InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.SPECIAL_CONTENT, CTConstant.PROP_NAME.CLICKON, eventValue, "");
                CleverTapDB.getInstance(WebFullScreenActivity.this).cleverTapTrackEvent(WebFullScreenActivity.this, catName, eventLabel, eventValue, ctEventArrayList, LoginController.getUserDataMap());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void appTrackScreen(String screenName) {
            String source = AppPreferences.getInstance(WebFullScreenActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(WebFullScreenActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(WebFullScreenActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

            Tracking.trackGAScreen(WebFullScreenActivity.this, InitApplication.getInstance().getDefaultTracker(), screenName, source, medium, campaign);
        }

        @JavascriptInterface
        public void rashiDetail(String jsonData) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                String gender = jsonObject.optString("gender");
                String rashiName = jsonObject.optString("rashiName");
                String numerono = jsonObject.optString("numerono");
                Log.e("Event Tracking ", "gender => " + gender + "  rashiName => " + rashiName + "  numerono => " + numerono);

                CleverTapDB.getInstance(WebFullScreenActivity.this).updateRashifalValuesOfUser(gender, rashiName, numerono);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void showLoginDialog(String msg) {
        if (isShowLoginDialog) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(WebFullScreenActivity.this);

        final TextView message = new TextView(WebFullScreenActivity.this);
        message.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
        message.setPadding((int) AppUtils.getInstance().convertDpToPixel(8, WebFullScreenActivity.this), (int) AppUtils.getInstance().convertDpToPixel(16, WebFullScreenActivity.this), (int) AppUtils.getInstance().convertDpToPixel(8, WebFullScreenActivity.this), 0);
        message.setText(msg);
        message.setTextSize(16);

        builder.setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), (dialog, id) -> {
                    LoginController.loginController().getLoginIntent(WebFullScreenActivity.this, TrackingData.getDBId(WebFullScreenActivity.this), TrackingData.getDeviceId(WebFullScreenActivity.this), LOGIN_REQUEST_CODE);
//                    startActivityForResult(loginIntent, LOGIN_REQUEST_CODE);
                    isShowLoginDialog = false;
                })
                .setTitle(getResources().getString(R.string.login))
                .setView(message);

        final AlertDialog alert = builder.create();
        alert.setOnShowListener(dialog -> {
            Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
            btnPositive.setTextSize(16);
            btnPositive.setTypeface(null, Typeface.NORMAL);
        });

        isShowLoginDialog = true;
        alert.show();
    }

    private void checkForPermission(int type) {
        ArrayList<String> permissionArray = new ArrayList<>();
        if (type == REQUEST_CODE_DOWNLOAD_PERMISSIONS) {
            try {
                if (ContextCompat.checkSelfPermission(WebFullScreenActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
            } catch (Exception e) {
            }
            if (!isAllPermissionProvided(type) && permissionArray.size() > 0) {
                ActivityCompat.requestPermissions(WebFullScreenActivity.this, permissionArray.toArray(new String[permissionArray.size()]), REQUEST_CODE_DOWNLOAD_PERMISSIONS);
            } else {
                downloadFile();
            }
        } else {
            try {
                if (ContextCompat.checkSelfPermission(WebFullScreenActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                }
            } catch (Exception e) {
            }
            if (!isAllPermissionProvided(type) && permissionArray.size() > 0) {
                ActivityCompat.requestPermissions(WebFullScreenActivity.this, permissionArray.toArray(new String[permissionArray.size()]), REQUEST_CODE_UPLOAD_PERMISSIONS);
            } else {
                showFileChooser();
            }
        }
    }

    private boolean isAllPermissionProvided(int type) {
        try {
            if ((ContextCompat.checkSelfPermission(WebFullScreenActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                return false;
            }
        } catch (Exception e) {
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_DOWNLOAD_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    downloadFile();
                } else {
                    AppUtils.getInstance().showCustomToast(this, getResources().getString(R.string.file_download_error));
                }
                break;
            case REQUEST_CODE_UPLOAD_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showFileChooser();
                } else {
                    AppUtils.getInstance().showCustomToast(this, getResources().getString(R.string.file_download_error));
                }
                break;
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            AppUtils.getInstance().showCustomToast(this, "Please install a File Manager.");
        }
    }

    private void downloadFile() {
        try {
            String[] temp = fileUrl.split("/");
            String fileNameWithExtn = "";
            if (temp.length > 0) {
                fileNameWithExtn = temp[temp.length - 1];
            }
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fileUrl));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileNameWithExtn);
            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            dm.enqueue(request);
            //To notify the Client that the file is being downloaded
            AppUtils.getInstance().showCustomToast(getApplicationContext(), "Downloading File");
        } catch (Exception e) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    try {
                        // Get the Uri of the selected file
                        Uri uri = data.getData();
                        if (mainFilePathCallback != null) {
                            Uri[] uris = new Uri[1];
                            uris[0] = uri;
                            mainFilePathCallback.onReceiveValue(uris);
                        }
                        // Get the path
                        String path = null;

                        path = AppUtils.getInstance().getPath(WebFullScreenActivity.this, uri);
                        Log.d("WapV2", "File Path: " + path);
                    } catch (Exception ignored) {
                    }
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                } else {
                    if (mainFilePathCallback != null) {
                        mainFilePathCallback.onReceiveValue(null);
                    }
                }
                break;

            case LOGIN_REQUEST_CODE:
                if (requestCode == RESULT_OK) {
                    myWebView.loadUrl(mainUrl, AppUtils.getInstance().getWebHeader(WebFullScreenActivity.this));
                }
                break;

        }
        if(requestCode == Constants.REQ_CODE_BOTTOM_NAV && RESULT_OK==resultCode){
            finish();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }

}





