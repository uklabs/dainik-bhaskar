package com.db.news;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bhaskar.view.ui.MainActivity;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.home.ActivityUtil;
import com.bhaskar.util.LoginController;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.OpenNewsLinkInApp;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.ThemeUtil;
import com.google.android.gms.analytics.Tracker;
import com.google.android.material.appbar.AppBarLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;


public class WebViewActivity extends BaseAppCompatActivity {
    private static final int REQUEST_CODE_DOWNLOAD_PERMISSIONS = 11;
    private static final int REQUEST_CODE_UPLOAD_PERMISSIONS = 12;
    private static final int FILE_SELECT_CODE = 101;
    private static final int LOGIN_REQUEST_CODE = 120;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    AppBarLayout appbar;
    Toolbar toolbar;
    InitApplication initApplication;
    ValueCallback<Uri[]> mainFilePathCallback;
    private WebView myWebView;
    private ProgressBar progressBar;
    private String mainUrl, title;
    private String fileUrl;
    private String gaScreen;
    private Tracker tracker;
    private boolean bundleIsFromPush;
    private boolean isMainProgress = false;
    private WebView mWebviewPop;
    private AlertDialog builder;
    private JavaScriptInterface JSInterface;
    private boolean hasHeader;

    private boolean isShowLoginDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initApplication = (InitApplication) getApplication();

        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_web_activirty);

        setToolbar();
        tracker = ((InitApplication) getApplication()).getDefaultTracker();
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mainUrl = intent.getStringExtra(Constants.KeyPair.KEY_URL);
            mainUrl = mainUrl.replace("http-", "https:");
            mainUrl = mainUrl.replace("https-", "https:");

            if (!TextUtils.isEmpty(mainUrl)) {
                AppLogs.printDebugLogs("ACTION URL", mainUrl);

            }
            title = intent.getStringExtra(Constants.KeyPair.KEY_TITLE);
            gaScreen = intent.getStringExtra(Constants.KeyPair.KEY_GA_SCREEN);
            bundleIsFromPush = intent.getBooleanExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, false);
            hasHeader = intent.getBooleanExtra(Constants.KeyPair.KEY_HAS_HEADER, false);
        }
        getSupportActionBar().setTitle(title);

        progressBar = findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(WebViewActivity.this, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        progressBar.setPadding(0, 0, 0, 0);
        progressBar.setMax(100);

//        setUpWebView();

        // Tracking
        String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(this, tracker, gaScreen, source, medium, campaign);

        initApplication = (InitApplication) getApplication();

        appbar = findViewById(R.id.appbar);
    }

    private void setToolbar() {
        /*Tool Bar*/
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(AppUtils.fetchAttributeColor(this, R.attr.toolbarIconColor));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleBackActivity();
            }
        });
    }

    private void handleBackActivity() {
        if (bundleIsFromPush) {
            Intent intent = new Intent(WebViewActivity.this, MainActivity.class);
            startActivity(intent);
        }
        finish();
    }

    public void setUpWebView() {

        myWebView = findViewById(R.id.webview);
        myWebView.setWebChromeClient(new UriChromeClient());
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        /*Custom User Agent*/
        String userAgent = webSettings.getUserAgentString();
        userAgent = userAgent + ";CustomUserAgent:" + CommonConstants.USER_AGENT;
        webSettings.setUserAgentString(userAgent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webSettings.setMediaPlaybackRequiresUserGesture(false);
        }

        JSInterface = new JavaScriptInterface(this);
        myWebView.addJavascriptInterface(JSInterface, "DivyaJSInterface");

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.setAcceptThirdPartyCookies(myWebView, true);
        }

        if (title.equalsIgnoreCase("Voucher")) {
            myWebView.loadData(mainUrl, "text/html", "utf-8");
        } else {
            if (hasHeader) {
                myWebView.loadUrl(mainUrl, AppUtils.getInstance().getWebHeader(WebViewActivity.this));
            } else {
                myWebView.loadUrl(mainUrl);
            }
        }
        myWebView.setWebViewClient(new MyWebViewClient());

        // download listener
        myWebView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {
                fileUrl = url;
                checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (myWebView == null) {
            setUpWebView();
        }
    }

    public void setValue(int progress) {
        progressBar.setProgress(progress);
        if (progress == 100)
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onPause() {
        super.onPause();
        if (myWebView != null) {
            myWebView.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myWebView != null) {
            myWebView.onResume();
        }
    }

    @Override
    protected void onStop() {
        try {
            Class.forName("android.webkit.WebView")
                    .getMethod("onPause", (Class[]) null)
                    .invoke(myWebView, (Object[]) null);

        } catch (ClassNotFoundException cnfe) {
        } catch (NoSuchMethodException nsme) {
        } catch (InvocationTargetException ite) {
        } catch (IllegalAccessException iae) {
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myWebView.destroy();
    }

    private void checkForPermission(int type) {
        ArrayList<String> permissionArray = new ArrayList<>();
        if (type == REQUEST_CODE_DOWNLOAD_PERMISSIONS) {
            try {
                if (ContextCompat.checkSelfPermission(WebViewActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
            } catch (Exception e) {
            }
            if (!isAllPermissionProvided(type) && permissionArray.size() > 0) {
                ActivityCompat.requestPermissions(WebViewActivity.this, permissionArray.toArray(new String[permissionArray.size()]), REQUEST_CODE_DOWNLOAD_PERMISSIONS);
            } else {
                downloadFile();
            }
        } else {
            try {
                if (ContextCompat.checkSelfPermission(WebViewActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                }
            } catch (Exception e) {
            }
            if (!isAllPermissionProvided(type) && permissionArray.size() > 0) {
                ActivityCompat.requestPermissions(WebViewActivity.this, permissionArray.toArray(new String[permissionArray.size()]), REQUEST_CODE_UPLOAD_PERMISSIONS);
            } else {
                showFileChooser();
            }
        }
    }

    private boolean isAllPermissionProvided(int type) {
        try {
            if ((ContextCompat.checkSelfPermission(WebViewActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                return false;
            }
        } catch (Exception e) {
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_DOWNLOAD_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    downloadFile();
                } else {
                    AppUtils.getInstance().showCustomToast(this, getResources().getString(R.string.file_download_error));
                }
                break;
            case REQUEST_CODE_UPLOAD_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showFileChooser();
                } else {
                    AppUtils.getInstance().showCustomToast(this, getResources().getString(R.string.file_download_error));
                }
                break;
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            AppUtils.getInstance().showCustomToast(this, "Please install a File Manager.");
        }
    }

    private void downloadFile() {
        try {
            String[] temp = fileUrl.split("/");
            String fileNameWithExtn = "";
            if (temp.length > 0) {
                fileNameWithExtn = temp[temp.length - 1];
            }
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fileUrl));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileNameWithExtn);
            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            dm.enqueue(request);
            //To notify the Client that the file is being downloaded
            AppUtils.getInstance().showCustomToast(getApplicationContext(), "Downloading File");
        } catch (Exception e) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    try {
                        // Get the Uri of the selected file
                        Uri uri = data.getData();
                        if (mainFilePathCallback != null) {
                            Uri[] uris = new Uri[1];
                            uris[0] = uri;
                            mainFilePathCallback.onReceiveValue(uris);
                        }
                        // Get the path
                        String path = null;

                        path = AppUtils.getInstance().getPath(WebViewActivity.this, uri);
                        Log.d("WapV2", "File Path: " + path);
                    } catch (Exception ignored) {
                    }
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                } else {
                    if (mainFilePathCallback != null) {
                        mainFilePathCallback.onReceiveValue(null);
                    }
                }
                break;

            case LOGIN_REQUEST_CODE:
                if (requestCode == RESULT_OK) {
                    myWebView.loadUrl(mainUrl, AppUtils.getInstance().getWebHeader(WebViewActivity.this));
                }
                break;

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        handleBackActivity();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        myWebView.setLayoutParams(new RelativeLayout.LayoutParams(width, height - 100));
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            findViewById(R.id.app_bar_layout).setVisibility(View.GONE);
        } else {
            findViewById(R.id.app_bar_layout).setVisibility(View.VISIBLE);
        }
    }

    public void changeOrientation(boolean fullscreen) {
        if (!fullscreen) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
    }

    class UriChromeClient extends WebChromeClient {
        @Override
        public Bitmap getDefaultVideoPoster() {
            return Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            isMainProgress = true;
            mWebviewPop = new WebView(WebViewActivity.this);
            mWebviewPop.setVerticalScrollBarEnabled(false);
            mWebviewPop.setHorizontalScrollBarEnabled(false);
            mWebviewPop.setWebViewClient(new WebViewActivity.MyWebViewClient());
            mWebviewPop.setWebChromeClient(new WebViewActivity.UriChromeClient());
            mWebviewPop.getSettings().setJavaScriptEnabled(true);
            mWebviewPop.getSettings().setSavePassword(true);
            mWebviewPop.getSettings().setSaveFormData(true);
            AlertDialog.Builder b = new AlertDialog.Builder(WebViewActivity.this, R.style.Theme_AppCompat_Light_Dialog);
            b.setTitle("");
            b.setView(mWebviewPop);
            b.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    mWebviewPop.destroy();
                    dialog.dismiss();
                }
            });
            builder = b.create();
            builder.show();
            builder.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.setAcceptThirdPartyCookies(mWebviewPop, true);
            }
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(mWebviewPop);
            resultMsg.sendToTarget();
            return true;


        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (!isMainProgress) {
                progressBar.setVisibility(View.VISIBLE);
                if (newProgress < 15)
                    setValue(15);
                else
                    setValue(newProgress);
            }
            super.onProgressChanged(view, newProgress);
        }

        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            mainFilePathCallback = filePathCallback;
            checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
            return true;
        }


        @Override
        public void onCloseWindow(WebView window) {
            isMainProgress = false;
            try {
                mWebviewPop.destroy();
            } catch (Exception e) {

            }
            try {
                builder.dismiss();

            } catch (Exception e) {

            }
        }

    }

    public class MyWebViewClient extends WebViewClient {

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            try {
                if (!TextUtils.isEmpty(url)) {
                    if (url.contains("dbnewshub")) {
                        return false;
                    } else if (url.startsWith("whatsapp://") || url.startsWith("mailto:") || url.startsWith("tel:")) {
                        if (builder != null) {
                            builder.dismiss();
                        }
                        view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        return true;
                    } else {
                        OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(WebViewActivity.this, url, "", 1, "", null);
                        openNewsDetailInApp.openLink();
                        return true;
                    }
                }
            } catch (Exception e) {
                AppUtils.getInstance().showCustomToast(WebViewActivity.this, WebViewActivity.this.getResources().getString(R.string.sorry_error_found_please_try_again_));
            }

            view.loadUrl(url);
            return false;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            try {
                if (!TextUtils.isEmpty(url)) {
                    if (url.contains("dbnewshub")) {
                        return false;
                    } else if (url.startsWith("whatsapp://") || url.startsWith("mailto:") || url.startsWith("tel:")) {
                        if (builder != null) {
                            builder.dismiss();
                        }
                        view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        return true;
                    } else {
                        OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(WebViewActivity.this, url, "", 1, "", null);
                        openNewsDetailInApp.openLink();
                        return true;
                    }
                }
            } catch (Exception e) {
                AppUtils.getInstance().showCustomToast(WebViewActivity.this, WebViewActivity.this.getResources().getString(R.string.sorry_error_found_please_try_again_));
            }
            view.loadUrl(url);
            return true;
        }
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void setdata(String title) {
            if (!TextUtils.isEmpty(title) && getSupportActionBar() != null)
                getSupportActionBar().setTitle(title);
        }

        @JavascriptInterface
        public void downloadFile(String url) {
            fileUrl = url;
            checkForPermission(REQUEST_CODE_DOWNLOAD_PERMISSIONS);
        }

        @JavascriptInterface
        public void fbComplete() {
            Systr.println("fb completed called");
            try {
                if (builder != null) {
                    builder.dismiss();
                }
            } catch (Exception e) {
            }
        }

        @JavascriptInterface
        public void login(String msg) {
            showLoginDialog(msg);
        }

        @JavascriptInterface
        public void openLink(String url) {
            OpenNewsLinkInApp openNewsDetailInApp = new OpenNewsLinkInApp(mContext, url, "", 1, "", null);
            openNewsDetailInApp.openLink();
        }

        @JavascriptInterface
        public void openMenu(String menuId) {
            ActivityUtil.openCategoryAccordingToAction(mContext, JsonParser.getInstance().getCategoryInfoBasedOnId(mContext, menuId), "");
        }

        @JavascriptInterface
        public void openFullPage(String pageUrl) {
            try {
                JSONObject jsonObject = new JSONObject(pageUrl);
                Intent webTV = new Intent(mContext, WebFullScreenActivity.class);
                webTV.putExtra(Constants.KeyPair.KEY_URL, jsonObject.optString("url"));
                webTV.putExtra(Constants.KeyPair.KEY_GA_SCREEN, pageUrl);
                webTV.putExtra(Constants.KeyPair.KEY_HAS_HEADER, jsonObject.optString("isHeader").equalsIgnoreCase("1"));
                mContext.startActivity(webTV);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void closeScreen() {
            WebViewActivity.this.finish();
        }

        @JavascriptInterface
        public void appTrackEvent(String jsonData) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                String catName = jsonObject.optString("catName");
                JSONArray eventArray = jsonObject.optJSONArray("events");

                ArrayList<ExtraCTEvent> ctEventArrayList = new ArrayList<>();
                String eventLabel = "";
                String eventValue = "";

                JSONObject jsObj;
                for (int i = 0; i < eventArray.length(); i++) {
                    jsObj = eventArray.getJSONObject(i);
                    if (i == 0) {
                        eventLabel = jsObj.optString("key");
                        eventValue = jsObj.optString("value");
                    } else {
                        if (!TextUtils.isEmpty(jsObj.optString("key"))) {
                            ctEventArrayList.add(new ExtraCTEvent(jsObj.optString("key"), jsObj.optString("value")));
                        }
                    }
                }

                Tracking.trackGAEvent(WebViewActivity.this, InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.SPECIAL_CONTENT, CTConstant.PROP_NAME.CLICKON, eventValue, "");
                CleverTapDB.getInstance(WebViewActivity.this).cleverTapTrackEvent(WebViewActivity.this, catName, eventLabel, eventValue, ctEventArrayList, LoginController.getUserDataMap());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void appTrackScreen(String screenName) {
            String source = AppPreferences.getInstance(WebViewActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(WebViewActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(WebViewActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");

            Tracking.trackGAScreen(WebViewActivity.this, InitApplication.getInstance().getDefaultTracker(), screenName, source, medium, campaign);
        }

        @JavascriptInterface
        public void rashiDetail(String jsonData) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                String gender = jsonObject.optString("gender");
                String rashiName = jsonObject.optString("rashiName");
                String numerono = jsonObject.optString("numerono");
                Log.e("Event Tracking ", "gender" + gender + "=> rashiName" + rashiName + "=> numerono" + numerono);

                CleverTapDB.getInstance(WebViewActivity.this).updateRashifalValuesOfUser(gender, rashiName, numerono);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void showLoginDialog(String msg) {
        if (isShowLoginDialog) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(WebViewActivity.this);

        final TextView message = new TextView(WebViewActivity.this);
        message.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
        message.setPadding((int) AppUtils.getInstance().convertDpToPixel(8, WebViewActivity.this), (int) AppUtils.getInstance().convertDpToPixel(16, WebViewActivity.this), (int) AppUtils.getInstance().convertDpToPixel(8, WebViewActivity.this), 0);
        message.setText(msg);
        message.setTextSize(16);

        builder.setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), (dialog, id) -> {
                    LoginController.loginController().getLoginIntent(WebViewActivity.this, TrackingData.getDBId(WebViewActivity.this), TrackingData.getDeviceId(WebViewActivity.this), LOGIN_REQUEST_CODE);
//                    startActivityForResult(loginIntent, LOGIN_REQUEST_CODE);
                    isShowLoginDialog = false;
                })
                .setTitle(getResources().getString(R.string.login))
                .setView(message);

        final AlertDialog alert = builder.create();
        alert.setOnShowListener(dialog -> {
            Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
            btnPositive.setTextSize(16);
            btnPositive.setTypeface(null, Typeface.NORMAL);
        });

        isShowLoginDialog = true;
        alert.show();
    }
}
