package com.db.news;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.ThemeUtil;

public class WebViewGameActivity extends BaseAppCompatActivity {

    private WebView myWebView;
    private String mainUrl;
    private String gaScreen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_web_activity_2);

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mainUrl = intent.getStringExtra(Constants.KeyPair.KEY_URL);
            gaScreen = intent.getStringExtra(Constants.KeyPair.KEY_GA_SCREEN);
        }

        setUpWebView();


        String source = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(this, InitApplication.getInstance().getDefaultTracker(), gaScreen, source, medium, campaign);
    }

    public void setUpWebView() {
        myWebView = findViewById(R.id.webview);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        myWebView.clearCache(true);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.setWebChromeClient(new WebChromeClient());
        myWebView.loadUrl(mainUrl);
    }

    @Override
    public void onBackPressed() {
        if (!myWebView.canGoBack()) {
            super.onBackPressed();
        } else {
            myWebView.goBack();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myWebView != null) {
            myWebView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (myWebView != null) {
            myWebView.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myWebView.destroy();
    }
}
