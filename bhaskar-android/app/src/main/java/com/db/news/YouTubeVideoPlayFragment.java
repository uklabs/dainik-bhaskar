package com.db.news;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by DB on 10/24/2017.
 */

public class YouTubeVideoPlayFragment extends Fragment {
    String videoUrl;
    String video_id;
    public String KeyValue = "AIzaSyDM_cV0tl8xTVTJbdiLPDkoqnGXP2Qt6Nc";

    public static Fragment newInstance(String iframe) {
        YouTubeVideoPlayFragment fragment = new YouTubeVideoPlayFragment();
        Bundle bundle = new Bundle();
        bundle.putString("iframe", iframe);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = layoutInflater.inflate(R.layout.fragment_youtube_video, viewGroup, false);
        Bundle bundle1 = getArguments();
        if (bundle1 != null) {
            videoUrl = bundle1.getString("iframe");
        }
        videoUrl = "https://www.youtube.com/watch?v=UZbRmle_Oqc";
        if (!TextUtils.isEmpty(videoUrl)) {
//            int start_image_index = videoUrl.indexOf("src=\"") + 5;
//            int end_image_index = videoUrl.indexOf("\"", start_image_index);
//            String image_url = videoUrl.substring(start_image_index, end_image_index);
            video_id = getYouTubeId(videoUrl);

        }

        YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_player_view, youTubePlayerFragment).commit();

        youTubePlayerFragment.initialize(KeyValue, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {

                player.setPlayerStateChangeListener(playerStateChangeListener);
                player.setPlaybackEventListener(playbackEventListener);

                if (!wasRestored) {
                    player.cueVideo(video_id);
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error) {

            }
        });


        return view;
    }

    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {

        @Override
        public void onBuffering(boolean arg0) {

            // Systr.printLog("onBuffering",arg0+"");
        }

        @Override
        public void onPaused() {

            // Systr.printLog("onPaused","onpaused");

        }

        @Override
        public void onPlaying() {
            //   Systr.printLog("onPlaying","onPlaying");

        }

        @Override
        public void onSeekTo(int arg0) {
            //  Systr.printLog("onSeekTo",arg0+"");

        }

        @Override
        public void onStopped() {

            //Systr.printLog("onStopped","onStopped"+"");
        }


    };

    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {

        @Override
        public void onAdStarted() {

        }

        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {

        }

        @Override
        public void onLoaded(String arg0) {
            //Systr.printLog("onLoaded",arg0);

        }

        @Override
        public void onLoading() {

        }

        @Override
        public void onVideoEnded() {

        }

        @Override
        public void onVideoStarted() {

        }


    };

    private String getYouTubeId(String youTubeUrl) {
        String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if (matcher.find()) {
            return matcher.group();
        } else {
            return "error";
        }
    }

}
