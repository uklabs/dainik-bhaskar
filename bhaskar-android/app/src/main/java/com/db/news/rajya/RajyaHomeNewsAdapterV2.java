package com.db.news.rajya;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.ads.AdPlacementController;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.NativeAdViewHolder2;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.database.DatabaseClient;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.homebanner.BannerViewHolder;
import com.db.news.WapV2Activity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.views.JustifiedTextView;
import com.db.views.viewholder.BigImageListViewHolder;
import com.db.views.viewholder.RecListViewHolder;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class RajyaHomeNewsAdapterV2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements NativeListAdController2.onAdNotifyListener {

    private final int VIEW_TYPE_BIG_IMAGE = 0;
    private final int VIEW_TYPE_REC = 2;
    public static final int VIEW_TYPE_SEARCH = 3;
    public static final int VIEW_TYPE_BANNER = 4;
    private final int VIEW_TYPE_NATIVE_ADS = 26;

    private Context mContext;
    private List<Object> newsListInfoList;
    private String categoryID;
    private String color;

    public RajyaHomeNewsAdapterV2(Context context, List<Object> newsListInfos) {
        this.mContext = context;
        this.newsListInfoList = new ArrayList<>();
        this.newsListInfoList.addAll(newsListInfos);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_BIG_IMAGE:
                return new BigImageListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image, parent, false));
            case VIEW_TYPE_REC:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_rec_item, parent, false));
            case VIEW_TYPE_SEARCH:
                return new SearchViewHolderV2(inflater.inflate(R.layout.layout_search_city, parent, false));
            case VIEW_TYPE_NATIVE_ADS:
                return new NativeAdViewHolder2(inflater.inflate(R.layout.ad_layout_native_listing_view, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            default:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_rec_item, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_SEARCH:
                bindSearchViewHolder((SearchViewHolderV2) holder, position);
                break;
            case VIEW_TYPE_BIG_IMAGE:
                onBindBigNewsViewHolder((BigImageListViewHolder) holder, position);
                break;
            case VIEW_TYPE_REC:
                onBindRecNewsViewHolder((RecListViewHolder) holder, position);
                break;
            case VIEW_TYPE_NATIVE_ADS:
                showNativeAds((NativeAdViewHolder2) holder, position);
                break;
            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                NewsListInfo newsListInfo = (NewsListInfo) newsListInfoList.get(position);
                bannerViewHolder.callwebViewOrSetWidgetData(newsListInfo.mBannerInfo);
                break;
        }
    }

    private void showNativeAds(NativeAdViewHolder2 holder, int pos) {
        NewsListInfo adInfo = (NewsListInfo) newsListInfoList.get(pos);

        UnifiedNativeAd ad = NativeListAdController2.getInstance(mContext).getAdForHome(adInfo.adUnit, adInfo.adType, pos, categoryID, new WeakReference<>(this));
        holder.populateNativeAdView(ad);
    }

    private void bindSearchViewHolder(SearchViewHolderV2 holder, int position) {
        holder.setData(categoryID, color);
    }

    @Override
    public void onNotifyAd() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        NewsListInfo info = (NewsListInfo) newsListInfoList.get(position);
        if (info.type > 0) {
            return info.type;
        } else if (info.isAd) {
            return VIEW_TYPE_NATIVE_ADS;
        } else {
            switch (info.bigImage) {
                case 1:
                    return VIEW_TYPE_BIG_IMAGE;
                case 2:
                    return VIEW_TYPE_REC;
                default:
                    return -1;
            }
        }
    }

    @Override
    public int getItemCount() {
        return newsListInfoList.size();
    }

    private void onBindRecNewsViewHolder(RecListViewHolder holder, int position) {
        final NewsListInfo recNewsInfo = (NewsListInfo) newsListInfoList.get(position);
        holder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(recNewsInfo.iitl_title, AppUtils.getInstance().fromHtml(recNewsInfo.title), recNewsInfo.colorCode));

        /*Exclusive*/
        if (!TextUtils.isEmpty(recNewsInfo.exclusive)) {
            holder.exclusiveLayout.setVisibility(View.VISIBLE);
            holder.exclusiveTv.setText(recNewsInfo.exclusive);
        } else {
            holder.exclusiveLayout.setVisibility(View.GONE);
        }

        /*Image*/
        ImageUtil.setImage(mContext, recNewsInfo.image, holder.ivThumb, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);

        /*Video Flag*/
        if (recNewsInfo.videoFlag == 1) {
            holder.imgVideoPlay.setVisibility(View.VISIBLE);
            holder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, recNewsInfo.colorCode));
        } else {
            holder.imgVideoPlay.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(view -> launchArticlePage(recNewsInfo, position));
        holder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
            @Override
            public void onBookMark() {
                CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(categoryID);
                if (categoryInfo != null) {
                    AppUtils.getInstance().bookmarkClick(mContext, recNewsInfo, categoryInfo.feedUrl, color, false);
                }
            }

            @Override
            public void onShare() {
                AppUtils.getInstance().shareClick(mContext, recNewsInfo.title, recNewsInfo.webUrl, recNewsInfo.gTrackUrl, false);
            }
        }));
    }

    private void onBindBigNewsViewHolder(BigImageListViewHolder viewHolder, int position) {
        final NewsListInfo bigNewsInfo = (NewsListInfo) newsListInfoList.get(position);

        if (!TextUtils.isEmpty(bigNewsInfo.exclusive)) {
            viewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            viewHolder.exclusiveTv.setText(bigNewsInfo.exclusive);
        } else {
            viewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
        }

        /*bullet point*/
        if (bigNewsInfo.bulletsPoint != null && bigNewsInfo.bulletsPoint.length > 0) {
            viewHolder.bulletLayout.removeAllViews();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            for (int i = 0; i < bigNewsInfo.bulletsPoint.length; i++) {
                View item = inflater.inflate(R.layout.layout_bullet_type, null);
                String desc = bigNewsInfo.bulletsPoint[i];
                ((JustifiedTextView) item.findViewById(R.id.bullet_tv)).setText(AppUtils.getInstance().fromHtml(desc));
                ((ImageView) item.findViewById(R.id.iv_bullet_circle)).setColorFilter(AppUtils.getThemeColor(mContext, bigNewsInfo.colorCode));
                viewHolder.bulletLayout.addView(item);
            }

            viewHolder.bulletLayout.setVisibility(View.VISIBLE);
        } else {
            viewHolder.bulletLayout.setVisibility(View.GONE);
        }

        /*Exclusive*/
        if (!TextUtils.isEmpty(bigNewsInfo.exclusive)) {
            viewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            viewHolder.exclusiveTv.setText(bigNewsInfo.exclusive);
        } else {
            viewHolder.exclusiveLayout.setVisibility(View.GONE);
        }
        float ratio = AppUtils.getInstance().parseImageRatio(bigNewsInfo.imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
        AppUtils.getInstance().setImageViewSizeWithAspectRatio(viewHolder.ivThumb, ratio, 0, mContext);
        ImageUtil.setImage(mContext, bigNewsInfo.image, viewHolder.ivThumb, R.drawable.water_mark_news_detail, R.drawable.water_mark_big_news_detail_error);
        if (bigNewsInfo.videoFlag == 1) {
            viewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
            viewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, bigNewsInfo.colorCode));
        } else {
            viewHolder.imgVideoPlay.setVisibility(View.GONE);
        }

        viewHolder.itemView.setOnClickListener(v -> launchArticlePage(bigNewsInfo, position));

        try {

            viewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(categoryID);
                    if (categoryInfo != null) {
                        AppUtils.getInstance().bookmarkClick(mContext, bigNewsInfo, categoryInfo.detailUrl, color, false);
                    }
                }

                @Override
                public void onShare() {
                    AppUtils.getInstance().shareClick(mContext, bigNewsInfo.title, bigNewsInfo.webUrl, bigNewsInfo.gTrackUrl, false);
                }
            }));

            viewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(bigNewsInfo.iitl_title, AppUtils.getInstance().fromHtml(bigNewsInfo.title), bigNewsInfo.colorCode));

        } catch (Exception ex) {
        }
    }

    private void launchArticlePage(NewsListInfo newsListInfo, int position) {
        CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryById(categoryID);
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            if (categoryInfo != null) {
                intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(categoryInfo.gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : categoryInfo.gaScreen) + newsListInfo.webUrl);
            } else {
                intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAScreen.WEB_VIEW + "-" + newsListInfo.webUrl);
            }
            mContext.startActivity(intent);

        } else if (newsListInfo.isOpenGallery) {
            if (categoryInfo != null) {
                Intent photoGalleryIntent = new Intent(mContext, PhotoGalleryActivity.class);
                photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, categoryInfo.detailUrl);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, categoryInfo.detailUrl);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, newsListInfo.storyId);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
                mContext.startActivity(photoGalleryIntent);
            }
        } else {
            mContext.startActivity(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, categoryInfo, position));
        }
    }

    public void setData(String categoryID, String color) {
        this.categoryID = categoryID;
        this.color = color;
        notifyDataSetChanged();
    }

    public void addItems(List<Object> founderList, boolean isClear) {
        if (isClear) {
            this.newsListInfoList.clear();
        }
        this.newsListInfoList.addAll(founderList);
        AdPlacementController.getInstance().addHomeListingNativeAdsForRajya(newsListInfoList, mContext);
        notifyDataSetChanged();
    }
}
