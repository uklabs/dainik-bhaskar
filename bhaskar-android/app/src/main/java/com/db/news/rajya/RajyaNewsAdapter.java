package com.db.news.rajya;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.AdController;
import com.bhaskar.appscommon.ads.AdInfo;
import com.bhaskar.appscommon.ads.BannerAdViewHolder;
import com.bhaskar.appscommon.ads.NativeAdViewHolder2;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.RajyaInfo;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.divya_new.common.viewholder.BlankViewHolder;
import com.db.homebanner.BannerViewHolder;
import com.db.news.WapV2Activity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.util.Action;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.QuickPreferences;
import com.db.views.JustifiedTextView;
import com.db.views.viewholder.BigImageListViewHolder;
import com.db.views.viewholder.LoadingViewHolder;
import com.db.views.viewholder.RecListViewHolder;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.List;

public class RajyaNewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdController.OnAdListener {

    private final int VIEW_TYPE_BIG_IMAGE = 0;
    private final int VIEW_TYPE_REC = 2;
    private final int VIEW_TYPE_SEARCH = 3;
    public static final int VIEW_TYPE_LOADING = 4;
    private final static int VIEW_TYPE_AD = 25;
    private final int VIEW_TYPE_NATIVE_ADS = 26;
    private static final int VIEW_TYPE_BANNER = 10;

    private final boolean shouldShowSearch;
    private Context mContext;
    private List<Object> newsListInfoList;
    private RajyaInfo rajyaInfo;
    private CategoryInfo categoryInfo;

    public RajyaNewsAdapter(Context context, List<Object> newsListInfos, boolean shouldShowSearch) {
        this.mContext = context;
        this.shouldShowSearch = shouldShowSearch;
        this.newsListInfoList = newsListInfos;
    }

    private boolean isBannerLoaded;

    @Override
    public void onBannerLoaded() {
        isBannerLoaded = true;
        notifyItemChanged(0);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_BIG_IMAGE:
                return new BigImageListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image, parent, false));
            case VIEW_TYPE_REC:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_rec_item, parent, false));
            case VIEW_TYPE_SEARCH:
                return new SearchViewHolder(inflater.inflate(R.layout.layout_search_city, parent, false));
            case VIEW_TYPE_NATIVE_ADS:
                return new NativeAdViewHolder2(inflater.inflate(R.layout.ad_layout_native_listing_view, parent, false));
            case VIEW_TYPE_AD:
                return new BannerAdViewHolder(inflater.inflate(R.layout.ad_layout_banner_with_height, parent, false));
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            default:
                return new BlankViewHolder(inflater.inflate(R.layout.listitem_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BigImageListViewHolder) {
            onBindBigNewsViewHolder((BigImageListViewHolder) holder, position);
        } else if (holder instanceof RecListViewHolder) {
            onBindRecNewsViewHolder((RecListViewHolder) holder, position);
        } else if (holder instanceof SearchViewHolder) {
            bindSearchViewHolder((SearchViewHolder) holder, position);
        } else if (holder instanceof NativeAdViewHolder2) {
            showNativeAds((NativeAdViewHolder2) holder, position);
        } else if (holder instanceof BannerAdViewHolder) {
//            BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
//            adViewHolder.addAds(((AdInfo) newsListInfoList.get(position)).bannerAdView);
            BannerAdViewHolder adViewHolder = (BannerAdViewHolder) holder;
            if (((AdInfo) newsListInfoList.get(position)).catType == 1) {
                if (isBannerLoaded || !AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, false)) {
                    adViewHolder.addAds(((AdInfo) newsListInfoList.get(position)).bannerAdView);

                } else {
                    ImageView imageView = new ImageView(mContext);
                    imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.getInstance().convertDpToPixel(50, mContext)));
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    imageView.setAdjustViewBounds(true);
                    imageView.setClickable(true);
                    String homeBannerUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, "");
                    ImageUtil.setImage(mContext, homeBannerUrl, imageView, 0);
                    adViewHolder.addAdsUrl(imageView);

                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AppUtils.getInstance().clickOnAdBanner(mContext);
                        }
                    });
                }
            } else {
                adViewHolder.addAds(((AdInfo) newsListInfoList.get(position)).bannerAdView);
            }
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
            loadingViewHolder.progressBar.setIndeterminate(true);
            loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
        } else if (holder instanceof BannerViewHolder) {
            BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
            bannerViewHolder.callwebViewOrSetWidgetData((BannerInfo) newsListInfoList.get(holder.getAdapterPosition()));
        }
    }

    private void showNativeAds(NativeAdViewHolder2 holder, int pos) {
        NewsListInfo adInfo = (NewsListInfo) newsListInfoList.get(pos);
        UnifiedNativeAd ad = NativeListAdController2.getInstance(mContext).getAd(adInfo.adUnit, pos);
        holder.populateNativeAdView(ad);
    }

    private void bindSearchViewHolder(SearchViewHolder holder, int position) {
        holder.setData(categoryInfo);
    }


    public NewsListInfo getItemAtPosition(int position) {
        if (position >= 0 && position < newsListInfoList.size()) {
            if (newsListInfoList.get(position) instanceof NewsListInfo) {
                return (NewsListInfo) newsListInfoList.get(position);
            }
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {
        Object object = newsListInfoList.get(position);
        if (position > 12) {
            object = newsListInfoList.get(position);
        }
        if (object instanceof BannerInfo) {
            return VIEW_TYPE_BANNER;
        } else if (object instanceof AdInfo) {
            return VIEW_TYPE_AD;
        } else if (object instanceof NewsListInfo) {
            NewsListInfo newsListInfo = (NewsListInfo) object;
            if (newsListInfo.bigImage == 1) {
                return VIEW_TYPE_BIG_IMAGE;
            } else if (newsListInfo.isAd) {
                return VIEW_TYPE_NATIVE_ADS;
            } else {
                return VIEW_TYPE_REC;
            }
        } else if (object.equals(Action.CategoryAction.CAT_ACTION_SEARCH_WIDGET)) {
            return VIEW_TYPE_SEARCH;
        } else if (object.equals(RajyaNewsAdapter.VIEW_TYPE_LOADING)) {
            return VIEW_TYPE_LOADING;
        }
        return -1;
    }


    @Override
    public int getItemCount() {
        return newsListInfoList.size();
    }


    private void onBindRecNewsViewHolder(RecListViewHolder holder, int position) {
        final NewsListInfo recNewsInfo = (NewsListInfo) newsListInfoList.get(position);
        String color = (categoryInfo == null) ? "" : categoryInfo.color;
        holder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(recNewsInfo.iitl_title, AppUtils.getInstance().fromHtml(recNewsInfo.title), recNewsInfo.colorCode));

        /*Exclusive*/
        if (!TextUtils.isEmpty(recNewsInfo.exclusive)) {
            holder.exclusiveLayout.setVisibility(View.VISIBLE);
            holder.exclusiveTv.setText(recNewsInfo.exclusive);
        } else {
            holder.exclusiveLayout.setVisibility(View.GONE);
        }
        /*Image*/
        ImageUtil.setImage(mContext, recNewsInfo.image, holder.ivThumb, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);

        /*Video Flag*/
        if (recNewsInfo.videoFlag == 1) {
            holder.imgVideoPlay.setVisibility(View.VISIBLE);
            holder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, recNewsInfo.colorCode));
        } else {
            holder.imgVideoPlay.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(view -> launchArticlePage(recNewsInfo));
        holder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
            @Override
            public void onBookMark() {
                AppUtils.getInstance().bookmarkClick(mContext, recNewsInfo, rajyaInfo.feedUrl, color, false);
            }

            @Override
            public void onShare() {
                AppUtils.getInstance().shareClick(mContext, recNewsInfo.title, recNewsInfo.webUrl, recNewsInfo.gTrackUrl, false);
            }
        }));
    }

    private void onBindBigNewsViewHolder(BigImageListViewHolder viewHolder, int position) {
        final NewsListInfo bigNewsInfo = (NewsListInfo) newsListInfoList.get(position);
        String color = (categoryInfo == null) ? "" : categoryInfo.color;
        if (!TextUtils.isEmpty(bigNewsInfo.exclusive)) {
            viewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            viewHolder.exclusiveTv.setText(bigNewsInfo.exclusive);
        } else {
            viewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
        }

        /*bullet point*/
        if (bigNewsInfo.bulletsPoint != null && bigNewsInfo.bulletsPoint.length > 0) {
            viewHolder.bulletLayout.removeAllViews();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            for (int i = 0; i < bigNewsInfo.bulletsPoint.length; i++) {
                View item = inflater.inflate(R.layout.layout_bullet_type, null);
                String desc = bigNewsInfo.bulletsPoint[i];
                ((JustifiedTextView) item.findViewById(R.id.bullet_tv)).setText(AppUtils.getInstance().fromHtml(desc));
                ((ImageView) item.findViewById(R.id.iv_bullet_circle)).setColorFilter(AppUtils.getThemeColor(mContext, bigNewsInfo.colorCode));
                viewHolder.bulletLayout.addView(item);
            }

            viewHolder.bulletLayout.setVisibility(View.VISIBLE);
        } else {
            viewHolder.bulletLayout.setVisibility(View.GONE);
        }
        /*Exclusive*/
        if (!TextUtils.isEmpty(bigNewsInfo.exclusive)) {
            viewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            viewHolder.exclusiveTv.setText(bigNewsInfo.exclusive);
        } else {
            viewHolder.exclusiveLayout.setVisibility(View.GONE);
        }
        float ratio = AppUtils.getInstance().parseImageRatio(bigNewsInfo.imageSize, Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
        AppUtils.getInstance().setImageViewSizeWithAspectRatio(viewHolder.ivThumb, ratio, 0, mContext);
        ImageUtil.setImage(mContext, bigNewsInfo.image, viewHolder.ivThumb, R.drawable.water_mark_news_detail, R.drawable.water_mark_big_news_detail_error);
        if (bigNewsInfo.videoFlag == 1) {
            viewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
            viewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, bigNewsInfo.colorCode));
        } else {
            viewHolder.imgVideoPlay.setVisibility(View.GONE);
        }

        viewHolder.itemView.setOnClickListener(v -> launchArticlePage(bigNewsInfo));

        try {
            viewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
                @Override
                public void onBookMark() {
                    AppUtils.getInstance().bookmarkClick(mContext, bigNewsInfo, rajyaInfo.detailURL, color, false);
                }

                @Override
                public void onShare() {
                    AppUtils.getInstance().shareClick(mContext, bigNewsInfo.title, bigNewsInfo.webUrl, bigNewsInfo.gTrackUrl, false);
                }
            }));
            viewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(bigNewsInfo.iitl_title, AppUtils.getInstance().fromHtml(bigNewsInfo.title), bigNewsInfo.colorCode));
        } catch (Exception ex) {
            AppLogs.printDebugLogs("VIEW_TYPE_BIG_IMAGE exception", ex.toString());
        }
    }

    private String getSubTitle(String newsSubTitle) {
        String subTitle = "";
        try {
            subTitle = AppUtils.getInstance().removeAndReplaceHtmlSymbols(newsSubTitle);
        } catch (Exception ex) {
            AppLogs.printDebugLogs("", ex.toString());
        }

        if (subTitle != null && subTitle.length() > 120) {
            subTitle = subTitle.substring(0, 120);
        }
        return subTitle;
    }


    private void launchArticlePage(NewsListInfo newsListInfo) {
        if (newsListInfo.isWebView) {
            Intent intent = new Intent(mContext, WapV2Activity.class);
            intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
            intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(categoryInfo.gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : categoryInfo.gaScreen) + newsListInfo.webUrl);
            mContext.startActivity(intent);

        } else if (newsListInfo.isOpenGallery) {
            Intent photoGalleryIntent = new Intent(mContext, PhotoGalleryActivity.class);
            photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, categoryInfo.detailUrl);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, newsListInfo.storyId);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
            photoGalleryIntent.putExtra(Constants.KeyPair.KEY_GA_G_TRACK_URL, newsListInfo.gTrackUrl);
            mContext.startActivity(photoGalleryIntent);

        } else {
            mContext.startActivity(DivyaCommonArticleActivity.getIntent(mContext, newsListInfo, rajyaInfo.gaArticle, rajyaInfo.gaScreen, rajyaInfo.displayName, rajyaInfo.gaEventLabel,categoryInfo.menuName));
        }
    }

    public void setData(RajyaInfo rajyaInfo, CategoryInfo categoryInfo) {
        isBannerLoaded = false;
        this.rajyaInfo = rajyaInfo;
        this.categoryInfo = categoryInfo;
        notifyDataSetChanged();
    }

    public void addItems(List<Object> founderList, boolean isClear) {
        if (isClear) {
            this.newsListInfoList.clear();
        }
        this.newsListInfoList.addAll(founderList);
        notifyDataSetChanged();
    }
}
