package com.db.news.rajya;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.ads.AdPlacementController;
import com.db.ads.NativeListAdController2;
import com.bhaskar.appscommon.ads.AdController;
import com.db.data.models.BannerInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.RajyaInfo;
import com.db.dbvideoPersonalized.SpacesDecoration;
import com.bhaskar.appscommon.tracking.Tracking;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RajyaNewsListFragment extends Fragment implements NativeListAdController2.onAdNotifyListener {
    private final String TAG = AppConfig.BaseTag + "." + RajyaNewsListFragment.class.getSimpleName();

    private CategoryInfo categoryInfo;
    private int LOAD_MORE_COUNT = 1;
    private boolean isLoading;
    private ProgressBar progressBar;
    private RajyaInfo rajyaInfo;
    private Button btnRetry;
    private RecyclerView recyclerView;
    private int totalItemCount;
    private int lastVisibleItem;
    private int visibleThreshold = 5;
    private RajyaNewsAdapter rajyaNewsAdapter;
    private List<Object> newsInfoList;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<BannerInfo> bannerInfoList;

    public static RajyaNewsListFragment newInstance(CategoryInfo categoryInfo) {
        RajyaNewsListFragment fragmentFirst = new RajyaNewsListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        fragmentFirst.setArguments(bundle);
        return fragmentFirst;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
        /*Banner*/
        bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rajya_list_news, container, false);

        swipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh_layout);
        progressBar = rootView.findViewById(R.id.progressBar);
        btnRetry = rootView.findViewById(R.id.btn_retry);
        recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(() -> {

            LOAD_MORE_COUNT = 1;
            getRajyaNewsList(rajyaInfo, true);

            bannerInfoList = JsonParser.getInstance().getBannersToAdd(categoryInfo.id);
        });

        btnRetry.setOnClickListener(v -> {
            btnRetry.setVisibility(View.GONE);
            getRajyaNewsList(rajyaInfo, true);
        });


        newsInfoList = new ArrayList<>();
        rajyaNewsAdapter = new RajyaNewsAdapter(getContext(), newsInfoList, true);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        SpacesDecoration dividerItemDecoration = new SpacesDecoration(0, 0, 2, 0);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(rajyaNewsAdapter);
        setInfiniteScrolling(linearLayoutManager);
        rajyaInfo = new RajyaInfo(categoryInfo.id, categoryInfo.menuName, categoryInfo.feedUrl, categoryInfo.detailUrl, categoryInfo.gaScreen, categoryInfo.gaArticle, categoryInfo.gaEventLabel, categoryInfo.displayName);
        getRajyaNewsList(rajyaInfo, true);
        sendTracking(rajyaInfo.gaScreen);

        return rootView;
    }

    private void addBannerAtPosition(List<Object> list) {
        for (int bannerIndex = 0; bannerIndex < bannerInfoList.size(); bannerIndex++) {
            int bannerPos = Integer.parseInt(bannerInfoList.get(bannerIndex).catHomePos);
            if (!bannerInfoList.get(bannerIndex).isAdded && bannerPos > -1 && list.size() >= bannerPos) {
                list.add(bannerPos, bannerInfoList.get(bannerIndex));
                bannerInfoList.get(bannerIndex).isAdded = true;
            }
        }
    }

    private void setInfiniteScrolling(LinearLayoutManager linearLayoutManager) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        LOAD_MORE_COUNT++;
                        getRajyaNewsList(rajyaInfo, false);
                    }
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (rajyaNewsAdapter != null) {
                    int i = linearLayoutManager.findFirstVisibleItemPosition();
                    int j = linearLayoutManager.findLastVisibleItemPosition();
                    for (int k = i; k <= j; k++) {
                        NewsListInfo itemAtPosition = rajyaNewsAdapter.getItemAtPosition(k);
                        if (itemAtPosition != null && itemAtPosition.isAd) {
//                            NativeListAdController.getInstance(getContext()).getAdRequest(itemAtPosition.adUnit, itemAtPosition.adType, k, new WeakReference<NativeListAdController.onAdNotifyListener>(NewsListFragment.this));
                            NativeListAdController2.getInstance(getContext()).getAdRequest(itemAtPosition.adUnit, itemAtPosition.adType, k, new WeakReference<NativeListAdController2.onAdNotifyListener>(RajyaNewsListFragment.this));
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onNotifyAd() {
        if (rajyaNewsAdapter != null) {
            rajyaNewsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void sendTracking(String gaScreen) {
        String souce = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), gaScreen, souce, medium, campaign);
    }

    private void setRajyaListAdapter(List<Object> newsInfoList, boolean isClear) {
        rajyaNewsAdapter.setData(rajyaInfo, categoryInfo);
        if (isClear) {
            this.newsInfoList.clear();
            //no more search widget
            //this.newsInfoList.add(Action.CategoryAction.CAT_ACTION_SEARCH_WIDGET); // for search
            addBannerAtPosition(newsInfoList);
            if (AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.AdPref.ATF_LIST_TOGGLE, false)) {
                this.newsInfoList.add(0, AdController.showATFForAllListing(getContext(), rajyaNewsAdapter));
            }
            this.newsInfoList.addAll(newsInfoList);
            //for loader
            this.newsInfoList.add(RajyaNewsAdapter.VIEW_TYPE_LOADING);
            recyclerView.setAdapter(rajyaNewsAdapter);
        } else {
            //remove last loader
            removeLastItemFromTheList(this.newsInfoList);
            addBannerAtPosition(newsInfoList);
            this.newsInfoList.addAll(newsInfoList);
            this.newsInfoList.add(RajyaNewsAdapter.VIEW_TYPE_LOADING);
            rajyaNewsAdapter.notifyDataSetChanged();
        }

    }

    private void getRajyaNewsList(RajyaInfo rajyaInfo, boolean isClear) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            if (rajyaInfo == null)
                return;
            isLoading = true;
            progressBar.setVisibility(View.VISIBLE);
            String finalFeedURL = createFeedUrl(rajyaInfo);
            AppLogs.printDebugLogs(TAG + "Rajya Feed URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalFeedURL, null,
                    response -> {
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        btnRetry.setVisibility(View.GONE);
                        try {
                            if (response != null) {
                                JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
                                int length = resultArray.length();
                                int count = response.getInt("count");
                                if (length > 0) {
                                    List<Object> founderList = new ArrayList<>(Arrays.asList(new Gson().fromJson(String.valueOf(resultArray), NewsListInfo[].class)));
                                    if (LOAD_MORE_COUNT < 5) {
                                        founderList = AdPlacementController.getInstance().addNewsListingNativeAdsWithObject(founderList, getContext());
                                    }
                                    setRajyaListAdapter(founderList, isClear);
                                } else {
                                    //remove last loader
                                    removeLastItemFromTheList(this.newsInfoList);
                                    if (LOAD_MORE_COUNT > 1) {
                                        LOAD_MORE_COUNT--;
                                    }
                                }
                            }
                        } catch (Exception e) {
                            AppLogs.printErrorLogs(TAG + "RajyaNewsList", "Error: " + e);
                            if (LOAD_MORE_COUNT > 1) {
                                LOAD_MORE_COUNT--;
                            }
                        }
                    }, error -> {
                btnRetry.setVisibility(View.VISIBLE);
                isLoading = false;
                progressBar.setVisibility(View.GONE);
                Context context = getContext();
                if (context != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
            if (getContext() != null) {
                AppUtils.getInstance().showCustomToast(getContext(), getContext().getString(R.string.no_network_error));
                btnRetry.setVisibility(View.VISIBLE);
            }
        }
    }

    private void removeLastItemFromTheList(List<Object> newsInfoList) {
        if (newsInfoList != null) {
            if (newsInfoList.size() > 0) {
                int position = newsInfoList.size() - 1;
                if (newsInfoList.get(position).equals(RajyaNewsAdapter.VIEW_TYPE_LOADING)) {
                    newsInfoList.remove(position);
                    rajyaNewsAdapter.notifyItemRemoved(position);
                }

            }
        }

    }

    private String createFeedUrl(RajyaInfo rajyaInfo) {
        String feedURL = "";
        //city
        if (LOAD_MORE_COUNT > 1) {
            feedURL = AppUtils.getInstance().makeUrlForLoadMore(Urls.APP_FEED_BASE_URL + rajyaInfo.feedUrl, LOAD_MORE_COUNT);
        } else if (LOAD_MORE_COUNT == 1) {
            feedURL = Urls.APP_FEED_BASE_URL + rajyaInfo.feedUrl + "PG" + LOAD_MORE_COUNT + "/";
        }
        return feedURL;
    }


    private ArrayList<Object> parseRajyaInfoList(ArrayList<CategoryInfo> rajyaList) {
        ArrayList<Object> rajyaInfos = new ArrayList<>();
        for (CategoryInfo categoryInfo : rajyaList) {
            rajyaInfos.add(new RajyaInfo(categoryInfo.id, categoryInfo.menuName, categoryInfo.feedUrl, categoryInfo.detailUrl, categoryInfo.gaScreen, categoryInfo.gaArticle, categoryInfo.gaEventLabel, categoryInfo.displayName));
        }
        return rajyaInfos;
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Subscribe
    public void NetworkConnectionStateChange(Boolean isConnected) {
        AppLogs.printDebugLogs(TAG + "Rajya getMessage1", "" + isConnected);
    }


}