package com.db.news.rajya;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.CityInfo;
import com.db.preferredcity.NamesAdapter;
import com.db.preferredcity.PreferredCityActivity;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class SearchViewHolder extends RecyclerView.ViewHolder {
    private final Context context;
    private final View vSearch;
    private final View vEditTextSearch;
    private AutoCompleteTextView autoCompleteTextView;

    private List<CityInfo> cityInfoArrayList;

    private NamesAdapter cityAutoCompleteListAdapter;
    private CategoryInfo categoryInfo;



    public SearchViewHolder(View itemView) {
        super(itemView);
        this.context = itemView.getContext();

        cityInfoArrayList = JsonParser.getInstance().getCityFeedList(context, CommonConstants.CHANNEL_ID);
        vSearch = itemView.findViewById(R.id.vSearch);
        vEditTextSearch = itemView.findViewById(R.id.vEditTextSearch);
        autoCompleteTextView = itemView.findViewById(R.id.select_city_autocomplete);

        autoCompleteTextView.setOnItemClickListener((parent, view, pos, l) -> onCitySelect(view, pos));
        setDataToAutoCompleteAdapter();

    }

    private void overrideThemeColor(CategoryInfo categoryInfo) {
        setTextViewDrawableColor(autoCompleteTextView, AppUtils.getThemeColor(context, categoryInfo.color));
        vSearch.getBackground().clearColorFilter();
        vSearch.getBackground().setColorFilter(AppUtils.getThemeColor(context, categoryInfo.color), PorterDuff.Mode.MULTIPLY);
        vEditTextSearch.getBackground().clearColorFilter();
        vEditTextSearch.getBackground().setColorFilter(AppUtils.getThemeColor(context, categoryInfo.color), PorterDuff.Mode.MULTIPLY);
    }


    private void setDataToAutoCompleteAdapter() {
        if (cityInfoArrayList != null && cityInfoArrayList.size() > 0) {
            cityAutoCompleteListAdapter = new NamesAdapter(context, R.layout.preferred_city_auto_complete_item, new ArrayList<>(cityInfoArrayList), new Vector<>());
            autoCompleteTextView.setThreshold(2);
            autoCompleteTextView.setAdapter(cityAutoCompleteListAdapter);
        }
    }

    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.clearColorFilter();
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(textView.getContext(), color), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    public void setData(CategoryInfo categoryInfo) {
        if (categoryInfo != null) {
            this.categoryInfo = categoryInfo;
            overrideThemeColor(categoryInfo);
        }
    }


    private void onCitySelect(View view, int pos) {
        CityInfo cityInfo = cityAutoCompleteListAdapter.getItem(pos);
        if (cityInfo == null) {
            return;
        }
        cityInfo.citySelectionValue = Constants.CityRajyaSelection.SELECTED;
        AppUtils.hideKeyboard(context);
        autoCompleteTextView.setText("");
        autoCompleteTextView.clearFocus();

        Intent intent = new Intent(context, PreferredCityActivity.class);
        intent.putExtra(PreferredCityActivity.CALL_PREFERRED_CITY_HOME, false);
        intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
        intent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, categoryInfo.displayName);
        intent.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, categoryInfo.gaEventLabel);
        intent.putExtra(Constants.KeyPair.KEY_COLOR, categoryInfo.color);
        intent.putExtra(Constants.KeyPair.KEY_TITLE, categoryInfo.menuName);
        intent.putExtra(Constants.KeyPair.KEY_CATEGORY_INFO, categoryInfo);
        intent.putExtra(Constants.KeyPair.KEY_CITY_INFO, cityInfo);
        context.startActivity(intent);
    }


}
