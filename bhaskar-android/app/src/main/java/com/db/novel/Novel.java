package com.db.novel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Novel implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("author_name")
    @Expose
    public String author_name;
    @SerializedName("author_img")
    @Expose
    public String author_img;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("cover_image")
    @Expose
    public String cover_image;
    @SerializedName("chapter_cnt")
    @Expose
    public String chapter_cnt;
    @SerializedName("preface")
    @Expose
    public String preface;
    @SerializedName("author_description")
    @Expose
    public String author_description;

}
