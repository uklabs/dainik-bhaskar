package com.db.novel;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.main.BaseAppCompatActivity;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.ThemeUtil;

public class NovelActivity extends BaseAppCompatActivity {

    private int color;
    private String title, feedUrl, detailFeedUrl;
    private String colorString;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_novel);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            color = AppUtils.getThemeColor(NovelActivity.this, bundle.getString(Constants.KeyPair.KEY_COLOR));
            colorString = bundle.getString(Constants.KeyPair.KEY_COLOR);
            title = bundle.getString(Constants.KeyPair.KEY_TITLE);
            feedUrl = bundle.getString(Constants.KeyPair.KEY_FEED_URL);
            detailFeedUrl = bundle.getString(Constants.KeyPair.KEY_DETAIL_FEED_URL);

        }
        AppUtils.getInstance().setStatusBarColor(NovelActivity.this, color);
        handleToolbar();

//        Fragment fragment = NovelFragment.newInstance(color, colorString, title, feedUrl, detailFeedUrl);
        Fragment fragment = NovelFragment.newInstance(null);
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, fragment);
            transaction.commitAllowingStateLoss();
        }
    }

    private void handleToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(color);
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(title);
    }
}
