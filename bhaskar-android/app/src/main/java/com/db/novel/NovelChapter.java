package com.db.novel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NovelChapter implements Serializable {
    @SerializedName("chapter_id")
    @Expose
    public String chapter_id;
    @SerializedName("page_count")
    @Expose
    public String page_count;
    @SerializedName("chapter_image")
    @Expose
    public String chapter_image;
    @SerializedName("chapter_title")
    @Expose
    public String chapter_title;
    @SerializedName("chapter_preface")
    @Expose
    public String chapter_preface;
    @SerializedName("g_track_url")
    @Expose
    public String g_track_url;
}
