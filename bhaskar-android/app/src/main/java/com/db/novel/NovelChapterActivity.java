package com.db.novel;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import com.db.util.AppUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.ThemeUtil;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NovelChapterActivity extends BaseAppCompatActivity {
    private static final String TAG = NovelChapterActivity.class.getName();
    private int color;
    private String title, detailFeedUrl;
    private Novel novel;
    private ProgressBar progressBar;
    private List<NovelChapter> chapterItemList = null;
    private NovelChapterAdapter chapterAdapter;
    TabLayout tabLayout;
    private Menu rootMenu;
    private Tracker tracker;
    ViewPager viewPager;
    AppBarLayout appbar;
    String gaScreen, gaArticle, gaEventLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_novel_chapter);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            color = AppUtils.getThemeColor(this, bundle.getString(Constants.KeyPair.KEY_COLOR));
            title = bundle.getString(Constants.KeyPair.KEY_TITLE);
            detailFeedUrl = bundle.getString(Constants.KeyPair.KEY_DETAIL_FEED_URL);
            novel = (Novel) bundle.getSerializable("novel_object");
            gaScreen = bundle.getString(Constants.KeyPair.KEY_GA_SCREEN);
            gaArticle = bundle.getString(Constants.KeyPair.KEY_GA_ARTICLE);
            gaEventLabel = bundle.getString(Constants.KeyPair.KEY_GA_EVENT_LABEL);
        }
        AppUtils.getInstance().setStatusBarColor(NovelChapterActivity.this, color);
        handleToolbar();
        tracker = ((InitApplication) getApplication()).getDefaultTracker();
        progressBar = findViewById(R.id.progress_bar);
        chapterItemList = new ArrayList<>();
        viewPager = findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                String source = AppPreferences.getInstance(NovelChapterActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
                String medium = AppPreferences.getInstance(NovelChapterActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
                String campaign = AppPreferences.getInstance(NovelChapterActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAScreen(NovelChapterActivity.this, InitApplication.getInstance().getDefaultTracker(), gaArticle + "-" + chapterItemList.get(position).g_track_url, source, medium, campaign);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        chapterAdapter = new NovelChapterAdapter(getSupportFragmentManager());
        viewPager.setAdapter(chapterAdapter);
        appbar = findViewById(R.id.appbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            appbar.setElevation(0);
        }
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setVisibility(View.VISIBLE);
        tabLayout.setupWithViewPager(viewPager);
        setTabLayoutColor();
        makeJsonObjectRequest(novel.id);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rashifal_menu, menu);
        rootMenu = menu;
        resetFont();
        return true;
    }

    private void resetFont() {
        int lastFontSize = AppPreferences.getInstance(NovelChapterActivity.this).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
        if (lastFontSize == Constants.Font.MEDIUM) {
            rootMenu.findItem(R.id.action_font).setIcon(R.drawable.arc_small_font);
        } else {
            rootMenu.findItem(R.id.action_font).setIcon(R.drawable.arc_large_font);
        }
    }

    int currentPos = 0;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_font:

                int lastFontSize = AppPreferences.getInstance(NovelChapterActivity.this).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
                if (lastFontSize == Constants.Font.MEDIUM) {
                    AppPreferences.getInstance(NovelChapterActivity.this).setIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.LARGE);
                } else {
                    AppPreferences.getInstance(NovelChapterActivity.this).setIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
                }
                resetFont();
                List<Fragment> novelChapterFragmentList = chapterAdapter.getVisibleFragments(tabLayout.getSelectedTabPosition());
                if (novelChapterFragmentList != null) {
                    for (int i = 0; i < novelChapterFragmentList.size(); i++) {
                        ((NovelChapterFragment) novelChapterFragmentList.get(i)).setFontSize();
                    }
                }
                // Tracking
                String campaign = AppPreferences.getInstance(NovelChapterActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                String value = (lastFontSize == Constants.Font.MEDIUM ? AppFlyerConst.GALabel.LARGE : AppFlyerConst.GALabel.MEDIUM);
                Tracking.trackGAEvent(NovelChapterActivity.this, tracker, AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.FONT, value, campaign);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void makeJsonObjectRequest(String novel_id) {
        if (NetworkStatus.getInstance().isConnected(NovelChapterActivity.this)) {
            String finalFeedURL = Urls.APP_FEED_BASE_URL + detailFeedUrl + novel_id + "/";
            progressBar.setVisibility(View.VISIBLE);
            AppLogs.printDebugLogs(TAG + "ChapterList URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalFeedURL, null,
                    response -> {
                        progressBar.setVisibility(View.GONE);
                        try {
                            onResponseFromServer(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                AppLogs.printVolleyLogs(TAG + "ChapterList", "Error: " + error.getMessage());
                Context context = NovelChapterActivity.this;
                progressBar.setVisibility(View.GONE);
                if (context != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(NovelChapterActivity.this).addToRequestQueue(jsonObjReq);
        } else {
            Context context = NovelChapterActivity.this;
            if (context != null) {
                AppUtils.getInstance().showCustomToast(context, context.getString(R.string.no_network_error));
            }
            progressBar.setVisibility(View.GONE);
        }
    }


    private void onResponseFromServer(JSONObject response) throws JSONException {
        JSONArray jsonArray = response.getJSONArray("feed");
        chapterItemList.clear();
        for (int i = 0; i < jsonArray.length(); i++) {
            NovelChapter chapter = new Gson().fromJson(jsonArray.get(i).toString(), NovelChapter.class);
            chapterItemList.add(chapter);
        }
        if (chapterItemList.size() <= 1) {
            tabLayout.setVisibility(View.GONE);
        }
        chapterAdapter.notifyDataSetChanged();
        if (chapterAdapter.getCount() > 0) {
            String source = AppPreferences.getInstance(NovelChapterActivity.this).getStringValue(QuickPreferences.UTM_SOURCE, "");
            String medium = AppPreferences.getInstance(NovelChapterActivity.this).getStringValue(QuickPreferences.UTM_MEDIUM, "");
            String campaign = AppPreferences.getInstance(NovelChapterActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            Tracking.trackGAScreen(NovelChapterActivity.this, InitApplication.getInstance().getDefaultTracker(), gaArticle + "-" + chapterItemList.get(0).g_track_url, source, medium, campaign);
        }
    }


    private void handleToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(color);
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(title);
    }

    private void setTabLayoutColor() {
        tabLayout.setTabTextColors(getResources().getColor(R.color.tab_layout_text_color), getResources().getColor(R.color.tab_layout_text_color_selected));
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.tab_indicator_color));
        tabLayout.setBackgroundColor(color);
    }

    private class NovelChapterAdapter extends FragmentPagerAdapter {
        private FragmentManager fm;

        NovelChapterAdapter(FragmentManager fm) {
            super(fm);
            this.fm = fm;
        }

        @Override
        public int getCount() {
            return (chapterItemList != null) ? chapterItemList.size() : 0;
        }

        @Override
        public Fragment getItem(int position) {
            return NovelChapterFragment.newInstance(chapterItemList.get(position), detailFeedUrl, novel.id);
        }

        public List<Fragment> getVisibleFragments(int position) {
          /* List<Fragment> fragments = fm.getFragments();
           ArrayList<Fragment> visibleFragments = new ArrayList<>();
            if (fragments != null) {
                for (Fragment fragment : fragments) {
                    if (fragment != null && fragment.isVisible())
                        visibleFragments.add(fragment);
                }
            }*/
           /*if(visibleFragments.size()>2){
                return visibleFragments.get(1);
            }
            return visibleFragments.get(0);*/
            //   return visibleFragments.get(position);
            return fm.getFragments();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Chapter " + (position + 1);
        }

    }


}