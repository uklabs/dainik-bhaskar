package com.db.novel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NovelChapterContent implements Serializable {
    @SerializedName("content_id")
    @Expose
    public String content_id;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("g_track_url")
    @Expose
    public String g_track_url;
}
