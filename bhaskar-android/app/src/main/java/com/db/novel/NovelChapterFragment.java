package com.db.novel;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.AdController;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.bhaskar.util.CssConstants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NovelChapterFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private static final String TAG = NovelChapterFragment.class.getName();
    private String detailUrl, novel_id;
    private NovelChapter mNovelChapter;
    private ProgressBar progressBar;
    private WebView webView;
    private WebSettings webSettings;
    private String pageContent = "";

    public static NovelChapterFragment newInstance(NovelChapter novelChapter, String detailUrl, String novel_id) {
        NovelChapterFragment fragment = new NovelChapterFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PAGE, novelChapter);
        args.putString(Constants.KeyPair.KEY_DETAIL_FEED_URL, detailUrl);
        args.putString("novel_id", novel_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNovelChapter = (NovelChapter) getArguments().getSerializable(ARG_PAGE);
        detailUrl = getArguments().getString(Constants.KeyPair.KEY_DETAIL_FEED_URL);
        novel_id = getArguments().getString("novel_id");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_novel, container, false);
        webView = view.findViewById(R.id.content_web_view);
        progressBar = view.findViewById(R.id.progress_bar);

        if (AppPreferences.getInstance(getActivity()).getBooleanValue(QuickPreferences.AdPref.ATF_ARTICLE_TOGGLE, false)) {
            AdController.showArticleATF(getContext(), view.findViewById(R.id.ads_view_top));
        } else {
            view.findViewById(R.id.ads_view_top).setVisibility(View.GONE);
        }
        if (AppPreferences.getInstance(getActivity()).getBooleanValue(QuickPreferences.AdPref.BTF_ROS_ARTICLE_TOGGLE, false)) {
            AdController.showArticleBTF(getContext(), view.findViewById(R.id.ads_view_bottom));
        } else {
            view.findViewById(R.id.ads_view_bottom).setVisibility(View.GONE);
        }

        makeJsonObjectRequest(novel_id, mNovelChapter.chapter_id);
        return view;
    }


    private void makeJsonObjectRequest(String novel_id, String chapter_id) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            String finalFeedURL = Urls.APP_FEED_BASE_URL + detailUrl + novel_id + "/" + chapter_id + "/";

            progressBar.setVisibility(View.VISIBLE);

            AppLogs.printDebugLogs(TAG + "ChapterPageList URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalFeedURL,null,
                    response -> {
                        progressBar.setVisibility(View.GONE);
                        try {
                            if (isAdded())
                                onResponseFromServer(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                AppLogs.printVolleyLogs(TAG + "ChapterPageList", "Error: " + error.getMessage());
                Context context = getContext();
                progressBar.setVisibility(View.GONE);
                if (context != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
            Context context = getContext();
            if (context != null) {
                AppUtils.getInstance().showCustomToast(context, context.getString(R.string.no_network_error));
            }
            progressBar.setVisibility(View.GONE);

        }
    }


    private void onResponseFromServer(JSONObject response) throws Exception {
        JSONArray jsonArray = response.getJSONArray("feed");
        String gTrackUrl = response.optString("g_track_url");
        for (int i = 0; i < jsonArray.length(); i++) {
            NovelChapterContent content = new Gson().fromJson(jsonArray.get(i).toString(), NovelChapterContent.class);
            if (i == 0) {
                pageContent = content.description;
            } else {
                pageContent = pageContent + "<br/>" + content.description;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        webSettings = webView.getSettings();
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        int fontSize;
        int lastFontSize = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
        if (lastFontSize == Constants.Font.MEDIUM) {
            fontSize = (int) getResources().getDimension(R.dimen.novel_normal);
        } else {
            fontSize = (int) getResources().getDimension(R.dimen.novel_big);
        }
        webSettings.setDefaultFontSize(fontSize);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setAppCacheEnabled(false);
        webSettings.setBlockNetworkImage(false);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setGeolocationEnabled(false);
        webSettings.setNeedInitialFocus(false);
        webSettings.setSaveFormData(false);

        webView.loadDataWithBaseURL("file:///android_asset/", CssConstants.getContentWithFontFamily_Novel(pageContent), "text/html; charset=utf-8", null, "");
    }

    public void setFontSize() {
        if (null != webSettings && null != webView) {
            int fontSize;
            int lastFontSize = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);
            if (lastFontSize == Constants.Font.MEDIUM) {
                fontSize = (int) getResources().getDimension(R.dimen.novel_normal);
            } else {
                fontSize = (int) getResources().getDimension(R.dimen.novel_big);
            }
            webSettings.setDefaultFontSize(fontSize);
        }
    }
}