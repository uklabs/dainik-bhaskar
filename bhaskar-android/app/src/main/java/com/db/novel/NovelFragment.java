package com.db.novel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.util.AppLogs;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.NetworkStatus;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NovelFragment extends Fragment {

    private static final String TAG = NovelActivity.class.getName();
    NovelRecyclerViewDataAdapter novelDataAdapter;
    // private String title, feedUrl, detailFeedUrl;
    private List<Novel> novelItemList = null;
    private ProgressBar progressBar;
    // private String color;
    private int totalItemCount;
    private int lastVisibleItem;
    private int visibleItemCount;
    private boolean isLoading;
    private int pastVisibleItems;
    private int LOADMORE = 1;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean shouldAutoLoad = true;
    CategoryInfo categoryInfo;

    public static NovelFragment newInstance(CategoryInfo categoryInfo) {
        NovelFragment fragment = new NovelFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("object", categoryInfo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryInfo = (CategoryInfo) bundle.getSerializable("object");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_novel_khani, container, false);

        progressBar = view.findViewById(R.id.progress_bar);
        novelItemList = new ArrayList<>();

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));
        RecyclerView radioRecyclerView = view.findViewById(R.id.card_view_recycler_list);
        //GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, RecyclerView.VERTICAL);
        //LinearLayoutManager layoutManager= new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false);
        radioRecyclerView.setLayoutManager(layoutManager);
        novelDataAdapter = new NovelRecyclerViewDataAdapter(novelItemList);
        radioRecyclerView.setAdapter(novelDataAdapter);
        makeJsonObjectRequest(false);
        radioRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    int[] firstVisibleItems = null;
                    firstVisibleItems = layoutManager.findFirstVisibleItemPositions(firstVisibleItems);
                    if (firstVisibleItems != null && firstVisibleItems.length > 0) {
                        pastVisibleItems = firstVisibleItems[0];
                    }

                    if (!isLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount && shouldAutoLoad) {
                            isLoading = false;
                            Log.d("tag", "LOAD NEXT ITEM");

                            makeJsonObjectRequest(false);
                        }
                    }
                }

            }
        });

        swipeRefreshLayout.setOnRefreshListener(() -> {
            LOADMORE = 1;
            makeJsonObjectRequest(true);
        });

        return view;
    }

    private void makeJsonObjectRequest(boolean isClear) {
        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            String finalFeedURL = Urls.APP_FEED_BASE_URL + categoryInfo.feedUrl + "PG" + LOADMORE + "/";

            if (!isClear)
                progressBar.setVisibility(View.VISIBLE);

            isLoading = true;
            AppLogs.printDebugLogs(TAG + "NovelList URL", "== " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, finalFeedURL, null,
                    response -> {
                        isLoading = false;
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        try {
                            onResponseFromServer(response, isClear);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                AppLogs.printVolleyLogs(TAG + "NovelList", "Error: " + error.getMessage());

                isLoading = false;
                Context context = getContext();
                progressBar.setVisibility(View.GONE);
                if (context != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.no_network_error));
                    } else {
                        AppUtils.getInstance().showCustomToast(context, context.getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
            Context context = getContext();
            if (context != null) {
                AppUtils.getInstance().showCustomToast(context, context.getString(R.string.no_network_error));
            }
           /* if (offeringAdapter.getItemCount() == 0) {
                btnRetry.setVisibility(View.VISIBLE);
            }*/
            progressBar.setVisibility(View.GONE);
        }
    }

    private void onResponseFromServer(JSONObject response, boolean isClear) throws Exception {
        int count = response.optInt("count");
        JSONArray jsonArray = response.getJSONArray("feed");
        int length = jsonArray.length();
        if (length < count) {
            shouldAutoLoad = false;
        } else {
            shouldAutoLoad = true;
            LOADMORE++;
        }
        if (isClear)
            novelItemList.clear();
        for (int i = 0; i < jsonArray.length(); i++) {
            Novel novel = new Gson().fromJson(jsonArray.get(i).toString(), Novel.class);
            novelItemList.add(novel);
//            novelDataAdapter.notifyItemInserted(novelDataAdapter.getItemCount());
        }
        // novelDataAdapter.notifyItemInserted(novelDataAdapter.getItemCount());


        novelDataAdapter.notifyDataSetChanged();
    }

    public void setFragmentShow() {
    }

    public class NovelRecyclerViewDataAdapter extends RecyclerView.Adapter<NovelRecyclerViewItemHolder> {

        private List<Novel> novelItemList;

        NovelRecyclerViewDataAdapter(List<Novel> novelItemList) {
            this.novelItemList = novelItemList;
        }

        @Override
        public NovelRecyclerViewItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View novelItemView = layoutInflater.inflate(R.layout.novel_list_item, parent, false);
            NovelRecyclerViewItemHolder ret = new NovelRecyclerViewItemHolder(novelItemView);
            return ret;
        }

        @Override
        public void onBindViewHolder(NovelRecyclerViewItemHolder holder, int position) {
            if (novelItemList != null) {
                Novel novelItem = novelItemList.get(position);
                if (novelItem != null) {
                    holder.getEditorTitleText().setText(novelItem.author_name);
                    holder.getNovelTitleText().setText(AppUtils.getInstance().getHomeTitle(novelItem.title + " / ", AppUtils.getInstance().fromHtml(novelItem.preface), categoryInfo.color));
                    if (!TextUtils.isEmpty(novelItem.cover_image)) {
                        ImageUtil.setImageWithServerImageDimention(getContext(), novelItem.cover_image, holder.getNovelImageView(), R.drawable.water_mark_vertical);
                    } else {
                        holder.getNovelImageView().setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.water_mark_contest_list));
                    }

                    holder.itemView.setOnClickListener(v -> {
                        Intent intentKahaniya = new Intent(getContext(), NovelChapterActivity.class);
                        intentKahaniya.putExtra(Constants.KeyPair.KEY_DETAIL_FEED_URL, categoryInfo.detailUrl);
                        intentKahaniya.putExtra(Constants.KeyPair.KEY_TITLE, novelItem.title);
                        intentKahaniya.putExtra(Constants.KeyPair.KEY_COLOR, categoryInfo.color);
                        intentKahaniya.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                        intentKahaniya.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, categoryInfo.gaArticle);
                        intentKahaniya.putExtra(Constants.KeyPair.KEY_GA_EVENT_LABEL, categoryInfo.gaEventLabel);
                        intentKahaniya.putExtra("novel_object", novelItem);
                        startActivity(intentKahaniya);
                    });
                }
            }
        }

        @Override
        public int getItemCount() {
            int ret = 0;
            if (novelItemList != null) {
                ret = novelItemList.size();
            }
            return ret;
        }
    }

    public class NovelRecyclerViewItemHolder extends RecyclerView.ViewHolder {

        private TextView editorTitleText = null;

        private TextView novelTitleText = null;

        private ImageView novelImageView = null;

        public NovelRecyclerViewItemHolder(View itemView) {
            super(itemView);

            if (itemView != null) {
                editorTitleText = itemView.findViewById(R.id.editor_name);
                novelTitleText = itemView.findViewById(R.id.novel_name);
                novelImageView = itemView.findViewById(R.id.card_view_image);
            }
        }

        public TextView getEditorTitleText() {
            return editorTitleText;
        }


        public TextView getNovelTitleText() {
            return novelTitleText;
        }

        public ImageView getNovelImageView() {
            return novelImageView;
        }

    }
}
