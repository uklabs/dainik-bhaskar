package com.db.preferredcity;

import android.content.Context;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CityInfo;
import com.db.util.AppLogs;

import java.util.ArrayList;
import java.util.List;

public class CityAutoCompleteListAdapter extends ArrayAdapter<CityInfo> {
    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter cityNameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((CityInfo) resultValue).cityName;
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (CityInfo cityInfo : tempItems) {
                    if (cityInfo.cityName.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(cityInfo);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<CityInfo> filterList = (ArrayList<CityInfo>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (CityInfo cityInfo : filterList) {
                    CityAutoCompleteListAdapter.this.add(cityInfo);
                    CityAutoCompleteListAdapter.this.notifyDataSetChanged();
                }
            } else if (constraint == null) {
                CityAutoCompleteListAdapter.this.addAll(filterList);
            }
        }
    };
    private Context context;
    private int resource;
    private ArrayList<CityInfo> items, tempItems, suggestions;

    public CityAutoCompleteListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CityInfo> cityInfoList) {
        super(context, resource, cityInfoList);
        this.context = context;
        this.resource = resource;
        this.items = new ArrayList<>(cityInfoList);
        this.tempItems = new ArrayList<>(cityInfoList);
        suggestions = new ArrayList<>();
    }

    public int getCount() {
        return items.size();
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resource, parent, false);
        }
        TextView lblName = (TextView) convertView.findViewById(R.id.textview);
        CityInfo cityInfo = items.get(position);
        if (cityInfo != null) {
            lblName.setText(cityInfo.cityName);
        }
        AppLogs.printErrorLogs("PRE CITY", "CityAutoCompleteListAdapter= " + cityInfo);
        return convertView;
    }


//    @Override
//    public Filter getFilter() {
//        return cityNameFilter;
//    }

    @Nullable
    @Override
    public CityInfo getItem(int position) {
        return items.get(position);
    }
}
