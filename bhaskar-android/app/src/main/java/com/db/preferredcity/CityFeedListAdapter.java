package com.db.preferredcity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.NewsListInfo;
import com.db.data.models.RelatedArticleInfo;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.homebanner.BannerViewHolder;
import com.db.homebanner.WebBannerViewHolder;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.Urls;
import com.db.views.JustifiedTextView;
import com.db.views.viewholder.BigImageListViewHolder;
import com.db.views.viewholder.RecListViewHolder;

import java.util.ArrayList;
import java.util.List;


public class CityFeedListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int MAIN_LIST_WITH_IMAGE = 0;
    private final int MAIN_LIST_REC = 1;
    private final int VIEW_TYPE_LOADING = 2;
    public static final int VIEW_TYPE_BANNER = 3;
    public static final int VIEW_TYPE_WEB_BANNER = 4;

    private List<RelatedArticleInfo> mNewsFeed;
    private String displayName;
    private Context mContext;
    private String gaScreen;
    private String gaArticle;
    private String gaEventLabel;
    private String themeColor;

    public CityFeedListAdapter(Context context, String gaScreen, String gaArticle, String displayName, String gaEventLabel) {
        this.mContext = context;
        this.mNewsFeed = new ArrayList<>();
        this.displayName = displayName;
        this.gaArticle = gaArticle;
        this.gaScreen = gaScreen;
        this.gaEventLabel = gaEventLabel;
    }

    public CityFeedListAdapter(Context context) {
        this.mContext = context;
        this.mNewsFeed = new ArrayList<>();
    }

    public void updateValues(String gaScreen, String gaArticle, String displayName, String gaEventLabel) {
        this.displayName = displayName;
        this.gaArticle = gaArticle;
        this.gaScreen = gaScreen;
        this.gaEventLabel = gaEventLabel;
    }

    public void setData(List<RelatedArticleInfo> newsFeed, String title) {
        mNewsFeed.clear();
        mNewsFeed.addAll(newsFeed);
        notifyDataSetChanged();
    }

    public void setDataOnLoadMore(List<RelatedArticleInfo> newsFeed) {
        mNewsFeed.addAll(newsFeed);
        notifyDataSetChanged();
    }

    public void addItem() {
        mNewsFeed.add(null);
        notifyDataSetChanged();
    }

    public void removeItem() {
        if (!mNewsFeed.isEmpty()) {
            mNewsFeed.remove(mNewsFeed.size() - 1);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case MAIN_LIST_WITH_IMAGE:
                return new BigImageListViewHolder(inflater.inflate(R.layout.recyclerlist_item_with_big_image, parent, false));
            case MAIN_LIST_REC:
                return new RecListViewHolder(inflater.inflate(R.layout.recyclerlist_rec_item, parent, false));
            case VIEW_TYPE_LOADING:
                return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item, parent, false));
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(inflater.inflate(R.layout.layout_banner, parent, false));
            case VIEW_TYPE_WEB_BANNER:
                return new WebBannerViewHolder(inflater.inflate(R.layout.layout_web_banner, parent, false));
            default:
                break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case MAIN_LIST_WITH_IMAGE:
                createBigImageHolder(holder);
                break;

            case MAIN_LIST_REC:
                createRecImageHolder(holder);
                break;

            case VIEW_TYPE_LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
                break;
            case VIEW_TYPE_BANNER:
                BannerViewHolder bannerViewHolder = (BannerViewHolder) holder;
                bannerViewHolder.callwebViewOrSetWidgetData(mNewsFeed.get(holder.getAdapterPosition()).bannerInfo);
                break;
//            case VIEW_TYPE_WEB_BANNER:
//                WebBannerViewHolder webBannerViewHolder = (WebBannerViewHolder) holder;
//                webBannerViewHolder.callwebViewOrSetWidgetData(((RelatedArticleInfo) mNewsFeed.get(webBannerViewHolder.getAdapterPosition())).webBannerInfo);
            default:
                break;
        }
    }

    private void createBigImageHolder(RecyclerView.ViewHolder holder) {
        final BigImageListViewHolder bigImageViewHolder = (BigImageListViewHolder) holder;
        final RelatedArticleInfo bigNewsInfo = mNewsFeed.get(bigImageViewHolder.getAdapterPosition());

        bigImageViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(bigNewsInfo.iitlTitle, AppUtils.getInstance().fromHtml(bigNewsInfo.title), themeColor));
        AppUtils.getInstance().setImageViewSizeWithAspectRatio(bigImageViewHolder.ivThumb, Constants.ImageRatios.ARTICLE_DETAIL_RATIO, 0, mContext);
        ImageUtil.setImage(mContext, bigNewsInfo.image, bigImageViewHolder.ivThumb, R.drawable.water_mark_news_detail, R.drawable.water_mark_news_detail_error);

        /*bullet point*/
        if (bigNewsInfo.bulletsPoint != null && bigNewsInfo.bulletsPoint.length > 0) {
            bigImageViewHolder.bulletLayout.removeAllViews();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            for (int i = 0; i < bigNewsInfo.bulletsPoint.length; i++) {
                View item = inflater.inflate(R.layout.layout_bullet_type, null);
                String desc = bigNewsInfo.bulletsPoint[i];
                ((JustifiedTextView) item.findViewById(R.id.bullet_tv)).setText(AppUtils.getInstance().fromHtml(desc));
                ((ImageView) item.findViewById(R.id.iv_bullet_circle)).setColorFilter(AppUtils.getThemeColor(mContext, bigNewsInfo.colorCode));
                bigImageViewHolder.bulletLayout.addView(item);
            }

            bigImageViewHolder.bulletLayout.setVisibility(View.VISIBLE);
        } else {
            bigImageViewHolder.bulletLayout.setVisibility(View.GONE);
        }
        /*Exclusive*/
        if (!TextUtils.isEmpty(bigNewsInfo.exclusive)) {
            bigImageViewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            bigImageViewHolder.exclusiveTv.setText(bigNewsInfo.exclusive);
        } else {
            bigImageViewHolder.exclusiveLayout.setVisibility(View.GONE);
        }
        bigImageViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = DivyaCommonArticleActivity.getIntent(mContext, bigNewsInfo.storyId, bigNewsInfo.headerName, gaArticle, gaScreen, displayName, gaEventLabel, bigNewsInfo.colorCode);
                if(mContext instanceof Activity){
                    ((Activity)mContext).startActivityForResult(intent,Constants.REQ_CODE_BOTTOM_NAV);
                }else{
                    mContext.startActivity(intent);
                }
            }
        });

        bigImageViewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
            @Override
            public void onBookMark() {
                AppUtils.getInstance().bookmarkClick(mContext, bigNewsInfo, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID),themeColor, false);
            }

            @Override
            public void onShare() {
                AppUtils.getInstance().shareClick(mContext, bigNewsInfo.title, bigNewsInfo.webopenurl, bigNewsInfo.gTrackUrl, false);
            }
        }));
    }

    private void createRecImageHolder(RecyclerView.ViewHolder holder) {
        final RecListViewHolder relatedArticleViewHolder = (RecListViewHolder) holder;
        final RelatedArticleInfo recNewsInfo = mNewsFeed.get(relatedArticleViewHolder.getAdapterPosition());

        relatedArticleViewHolder.moreIV.setOnClickListener(v -> AppUtils.showMoreOptionsV2(mContext, v, new MoreOptionInterface() {
            @Override
            public void onBookMark() {
                AppUtils.getInstance().bookmarkClick(mContext, recNewsInfo, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID),themeColor, false);
            }

            @Override
            public void onShare() {
                AppUtils.getInstance().shareClick(mContext, recNewsInfo.title, recNewsInfo.webopenurl, recNewsInfo.gTrackUrl, false);
            }
        }));
        relatedArticleViewHolder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(recNewsInfo.iitlTitle, AppUtils.getInstance().fromHtml(recNewsInfo.title), themeColor));

        ImageUtil.setImage(mContext, recNewsInfo.image, relatedArticleViewHolder.ivThumb, R.drawable.water_mark_news_list, R.drawable.water_mark_news_detail_error);
        if (recNewsInfo.videoFlag.equalsIgnoreCase("1")) {
            relatedArticleViewHolder.imgVideoPlay.setVisibility(View.VISIBLE);
            relatedArticleViewHolder.imgVideoPlay.setColorFilter(AppUtils.getThemeColor(mContext, themeColor));
        } else {
            relatedArticleViewHolder.imgVideoPlay.setVisibility(View.GONE);
        }
        /*Exclusive*/
        if (!TextUtils.isEmpty(recNewsInfo.exclusive)) {
            relatedArticleViewHolder.exclusiveLayout.setVisibility(View.VISIBLE);
            relatedArticleViewHolder.exclusiveTv.setText(recNewsInfo.exclusive);
        } else {
            relatedArticleViewHolder.exclusiveLayout.setVisibility(View.GONE);
        }

        relatedArticleViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = DivyaCommonArticleActivity.getIntent(mContext, recNewsInfo.storyId, recNewsInfo.headerName, gaArticle, gaScreen, displayName, gaEventLabel, recNewsInfo.colorCode);
                if(mContext instanceof Activity){
                    ((Activity)mContext).startActivityForResult(intent, Constants.REQ_CODE_BOTTOM_NAV);
                }else{
                    mContext.startActivity(intent);
                }
            }
        });

    }


    @Override
    public int getItemViewType(int position) {
        if (mNewsFeed.get(position) == null)
            return VIEW_TYPE_LOADING;
        else {
            int viewType;
            if (mNewsFeed.get(position).type > 0)
                return mNewsFeed.get(position).type;
            else {
                switch (mNewsFeed.get(position).bigImage) {
                    case 1:
                        viewType = MAIN_LIST_WITH_IMAGE;
                        break;
                    case 2:
                    default:
                        viewType = MAIN_LIST_REC;
                        break;
                }
            }
            return viewType;
        }
    }

    @Override
    public int getItemCount() {
        return mNewsFeed == null ? 0 : mNewsFeed.size();
    }

    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
        notifyDataSetChanged();
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        private LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(mContext, R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        }
    }
}