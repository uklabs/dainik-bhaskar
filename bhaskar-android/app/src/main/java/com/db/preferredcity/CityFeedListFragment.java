package com.db.preferredcity;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.database.CityFeedListTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.models.CityInfo;
import com.db.data.models.RelatedArticleInfo;
import com.db.dbvideo.player.BusProvider;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CityFeedListFragment extends Fragment {
    private final String TAG = AppConfig.BaseTag + "." + CityFeedListFragment.class.getSimpleName();
    InitApplication application;
    private List<RelatedArticleInfo> cityFeedList = new ArrayList<>();
    //lazy loading
    private boolean isLoading;
    private int LOAD_MORE_COUNT = 0;
    private int visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
    private int lastVisibleItem;
    private int totalItemCount;
    private String finalFeedURL;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CityFeedListAdapter cityFeedListAdapter;
    private RecyclerView cityFeedListRecyclerView;
    private CityInfo cityInfo;
    private String gaArticle;
    private String gaScreen;
    private String displayName;
    private LinearLayoutManager linearLayoutManager1;
    private ProgressBar progressBar;
    private TextView noNewsAvailable;
    private ArrayList<Integer> trackingList = new ArrayList<>();
    private boolean isShowing = false;
    private String gaEventLabel;
    private String themeColor;

    public CityFeedListFragment() {
    }

    public static CityFeedListFragment getInstance(CityInfo cityInfo, String gaArticle, String gaScreen, String displayName, String gaEventLabel, String themeColor) {

        CityFeedListFragment cityFeedListFragment = new CityFeedListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CITY_INFO, cityInfo);
        bundle.putString(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        bundle.putString(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        bundle.putString(Constants.KeyPair.KEY_GA_DISPLAY_NAME, displayName);
        bundle.putString(Constants.KeyPair.KEY_GA_EVENT_LABEL, gaEventLabel);
        bundle.putString(Constants.Themes.THEME_COLOR, themeColor);
        cityFeedListFragment.setArguments(bundle);
        return cityFeedListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            cityInfo = (CityInfo) bundle.getSerializable(Constants.KeyPair.KEY_CITY_INFO);
            gaArticle = bundle.getString(Constants.KeyPair.KEY_GA_ARTICLE);
            gaScreen = bundle.getString(Constants.KeyPair.KEY_GA_SCREEN);
            gaEventLabel = bundle.getString(Constants.KeyPair.KEY_GA_EVENT_LABEL);
            displayName = bundle.getString(Constants.KeyPair.KEY_GA_DISPLAY_NAME);
            themeColor = bundle.getString(Constants.Themes.THEME_COLOR);
        }
        BusProvider.getInstance().register(this);

        Activity activity = getActivity();
        if (activity != null) {
            application = (InitApplication) activity.getApplication();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        AppLogs.printErrorLogs(TAG, "" + isVisibleToUser);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_city_feed_list, container, false);
        progressBar = rootView.findViewById(R.id.progress_bar);
        noNewsAvailable = rootView.findViewById(R.id.error_message_no_data);
        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary),
                AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));
        cityFeedListRecyclerView = rootView.findViewById(R.id.city_feed_list_recycler_view);

        linearLayoutManager1 = new LinearLayoutManager(getContext());
        cityFeedListRecyclerView.setLayoutManager(linearLayoutManager1);
        cityFeedListRecyclerView.setHasFixedSize(true);

        if (cityInfo != null) {
            gaScreen += "-" + cityInfo.cityName;
            gaArticle += "-" + cityInfo.cityName;
            displayName += "_" + cityInfo.cityName;
        }
        cityFeedListAdapter = new CityFeedListAdapter(getContext(), gaScreen, gaArticle, displayName, gaEventLabel);
        cityFeedListAdapter.setThemeColor(themeColor);
        cityFeedListRecyclerView.setAdapter(cityFeedListAdapter);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!NetworkStatus.getInstance().isConnected(getActivity()) || Constants.singleSessionCategoryIdList.contains(cityInfo.cityId)) {
                    isTokenExpire();
                } else {
                    swipeRefreshLayout();
                }
            }
        }, Constants.DataBaseHit.DEFAULT_TIME_IS_TOKEN);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    swipeRefreshLayout();
                } else {
                    removeLoader();
                    if (isAdded() && getActivity() != null)
                        AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));

                }

            }
        });

        linearLayoutManager1 = (LinearLayoutManager) cityFeedListRecyclerView.getLayoutManager();
        cityFeedListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(getActivity())) {
                    if (dy > 0) {
                        totalItemCount = linearLayoutManager1.getItemCount();
                        lastVisibleItem = linearLayoutManager1.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            LOAD_MORE_COUNT++;
                            onLoadMore();
                        }
                    }
                }
            }
        });
    }

    private void swipeRefreshLayout() {
        LOAD_MORE_COUNT = 0;
        visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
        makeJsonObjectRequest();
    }

    private void swipeRefreshLayoutWithOutApiHit() {
        LOAD_MORE_COUNT = 0;
        visibleThreshold = Constants.AppCommon.DEFAULT_VISIBLE_THRESHOLD;
        if (!finalFeedURL.contains("/1/"))
            LOAD_MORE_COUNT++;
    }

    public void setFragmentShow() {
        isShowing = true;
        sendTracking();
    }

    private void sendTracking() {
        // Tracking
        if (isShowing) {
            if (cityInfo != null && trackingList != null) {
                while (trackingList.size() > 0) {
                    int value = trackingList.get(0) + 1;
                    trackingList.remove(0);
                    if (value > 1) {
                        value = value - 1;
                        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SCROLL_DEPTH, displayName, cityInfo.cityName + "_" + AppFlyerConst.GALabel.PG + value, campaign);
                        Systr.println("GA Events : " + AppFlyerConst.GACategory.SCROLL_DEPTH + displayName + cityInfo.cityName + "_" + AppFlyerConst.GALabel.PG + value + (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct") ? campaign : ""));
                    }
                }
            }
        }
    }

    private void onLoadMore() {

        isLoading = true;
        cityFeedListRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (cityFeedListAdapter != null)
                    cityFeedListAdapter.addItem();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                makeJsonObjectRequest();
            }
        }, 1000);
    }

    private void setOfflineCityFeedData() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    showProgress();
                    Boolean isShowInternetMessage = true;
                    if (cityFeedList == null) {
                        cityFeedList = new ArrayList<>();
                    } else {
                        cityFeedList.clear();
                    }
                    CityFeedListTable cityFeedListTable = (CityFeedListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CityFeedListTable.TABLE_NAME);
                    cityFeedList.addAll(cityFeedListTable.getAllCityFeedListInfoListAccordingToCity(cityInfo.cityId, getContext()));

                    if (cityFeedList != null && cityFeedList.size() > 0) {
                        isShowInternetMessage = false;
                        addAdsAndWidgetInCityFeedList();
                    }
                    cityFeedListAdapter.setData(cityFeedList, cityInfo.cityHindiName);
                    cityFeedListAdapter.notifyDataSetChanged();
                    removeLoader();
                    swipeRefreshLayoutWithOutApiHit();
                    checkInternetAndShowToast(isShowInternetMessage);
                } catch (Exception ex) {
                    AppLogs.printErrorLogs(TAG, ex.toString());
                }

            }
        }, Constants.DataBaseHit.DEFAULT_TIME);

    }

    private void checkInternetAndShowToast(Boolean isShowInternetMessage) {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
//                isTokenExpire();
            } else {
                if (isShowInternetMessage)
                    if (isAdded() && getActivity() != null)
                        AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));

            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }

    }

    private void isTokenExpire() {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                CityFeedListTable cityFeedListTable = (CityFeedListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CityFeedListTable.TABLE_NAME);

                String dbEntryCreationTime = cityFeedListTable.getCityFeedListItemCreationTime(cityInfo.cityId);

                if (dbEntryCreationTime != null && !dbEntryCreationTime.equalsIgnoreCase("")) {
                    long dbEntryCreationTimemilliSeconds = Long.parseLong(dbEntryCreationTime);
                    long currentTime = System.currentTimeMillis();

                    if ((currentTime - dbEntryCreationTimemilliSeconds) <= application.getServerTimeoutValue()) {
                        //token not Expire
                        removeLoader();
                        setOfflineCityFeedData();
                    } else {
                        //token Expire
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                removeLoader();
                                firstApiHit();
                            }
                        }, 500L);
                    }

                } else {
                    removeLoader();
                    firstApiHit();
                }
            } else {
                removeLoader();
                setOfflineCityFeedData();
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }
    }

    private void firstApiHit() {
        try {
            if (NetworkStatus.getInstance().isConnected(getActivity())) {
                AppLogs.printDebugLogs(TAG + "firstApiHit :", "firstApiHit");
                showProgress();
                swipeRefreshLayout();
            } else {
                removeLoader();
                if (isAdded() && getActivity() != null)
                    AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
            }
        } catch (Exception ex) {
            AppLogs.printErrorLogs(TAG, ex.toString());
        }
    }

    private void makeJsonObjectRequest() {
        if (NetworkStatus.getInstance().isConnected(getActivity())) {
            finalFeedURL = changeFinalUrlForLazyLoading();
            AppLogs.printErrorLogs(TAG, "Final Feed Url= " + finalFeedURL);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    AppUtils.getInstance().updateApiUrl(finalFeedURL), null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response != null) {
                                    if (!Constants.singleSessionCategoryIdList.contains(cityInfo.cityId)) {
                                        Constants.singleSessionCategoryIdList.add(cityInfo.cityId);
                                    }
                                    parseFeedList(response);
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    if (LOAD_MORE_COUNT >= 0) {
                        LOAD_MORE_COUNT--;
                    }
                    removeLoader();
                    Activity activity = getActivity();
                    if (activity != null && isAdded()) {
                        if (error instanceof NoConnectionError || error instanceof NetworkError) {
                            AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.no_network_error));
                        } else {

                            AppUtils.getInstance().showCustomToast(activity, getResources().getString(R.string.sorry_error_found_please_try_again_));
                        }
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
            if (isAdded() && getActivity() != null) {
                AppUtils.getInstance().showCustomToast(getActivity(), getResources().getString(R.string.no_network_error));
                removeLoader();
            }
        }
    }

    private void parseFeedList(JSONObject response) throws JSONException {
        JSONArray resultArray = response.getJSONArray(Constants.KeyPair.KEY_FEED);
        int responseSize = resultArray.length();

        if (responseSize > 0 && isAdded()) {

            if (LOAD_MORE_COUNT == 0) {
                cityFeedList.clear();
            }

            noNewsAvailable.setVisibility(View.GONE);
            cityFeedListRecyclerView.setVisibility(View.VISIBLE);
            for (int i = 0; i < responseSize; i++) {
                JSONObject feedJsonObject = resultArray.getJSONObject(i);
                Gson gson = new Gson();
                RelatedArticleInfo newsFeeds = gson.fromJson(feedJsonObject.toString(), RelatedArticleInfo.class);
                cityFeedList.add(newsFeeds);
            }

            // Inserting DB CityFeedList in mdb
            final CityFeedListTable cityFeedListTable = (CityFeedListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(CityFeedListTable.TABLE_NAME);
            if (LOAD_MORE_COUNT == 0) {
                cityFeedListTable.deleteParticularCityFeedList(cityInfo.cityId);
                cityFeedListTable.addCityFeedListItem(cityInfo, cityFeedList, getContext());
                if (!finalFeedURL.contains("/1/"))
                    LOAD_MORE_COUNT++;
            }

            // Tracking
            if (!TextUtils.isEmpty(displayName)) {
                trackingList.add(LOAD_MORE_COUNT);
                sendTracking();
            }

            visibleThreshold += responseSize;
            addAdsAndWidgetInCityFeedList();
            cityFeedListAdapter.setData(cityFeedList, cityInfo.cityHindiName);
            cityFeedListAdapter.notifyDataSetChanged();
        } else {
            if (LOAD_MORE_COUNT == 0) {
                noNewsAvailable.setVisibility(View.VISIBLE);
                cityFeedListRecyclerView.setVisibility(View.GONE);
            } else {
                LOAD_MORE_COUNT--;
            }
        }

        removeLoader();
    }

    private void addAdsAndWidgetInCityFeedList() {
        /*Native Ads*/

    }

    private String changeFinalUrlForLazyLoading() {
        StringBuilder baseUrl = new StringBuilder(1);
        baseUrl.append(Urls.APP_FEED_BASE_URL)
                .append(String.format(Urls.DEFAULT_LIST_URL, CommonConstants.CHANNEL_ID))
                .append(cityInfo.cityId)
                .append("/");

        if (LOAD_MORE_COUNT > 0) {
            baseUrl.append("3/PG").append(LOAD_MORE_COUNT);
        } else if (LOAD_MORE_COUNT == 0) {
            baseUrl.append("1/PG1");
        }

        baseUrl.append("/");
        return baseUrl.toString();
    }

    private void removeLoader() {
        if (LOAD_MORE_COUNT > 0) {
            if (cityFeedListAdapter != null) {
                cityFeedListAdapter.removeItem();
            }
            if (isLoading) {
                isLoading = false;
            }
        }
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
    }

    private void showProgress() {
        if (progressBar != null) {
            progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
            progressBar.setVisibility(View.VISIBLE);
        }

    }

    public void moveToTop() {
        try {
            if (cityFeedListRecyclerView != null) {
                cityFeedListRecyclerView.smoothScrollToPosition(0);
            }
        } catch (Exception e) {
        }
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Subscribe
    public void NetworkConnectionStateChange(Boolean isConnected) {
        AppLogs.printDebugLogs(TAG + "Preferred getMessage1", "" + isConnected);
        if (isConnected && isVisible()) {
            AppLogs.printDebugLogs(TAG + "Preferred ConnectionC", "" + isConnected);
//            showProgress();
//            swipeRefreshLayout();
        } else {
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isShowing = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }
}
