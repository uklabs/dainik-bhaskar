package com.db.preferredcity;

import android.widget.Filter;

import com.db.data.models.CityInfo;

import java.util.ArrayList;

public class CityInfoAutoCompleteFilter extends Filter {

    private OnFilterNotifyListener listener;
    private ArrayList<CityInfo> items, tempItems, suggestions;
    public CityInfoAutoCompleteFilter(ArrayList<CityInfo> accountEmailList, OnFilterNotifyListener listener) {
        this.items = new ArrayList<>(accountEmailList);
        this.tempItems = new ArrayList<>(accountEmailList);
        this.suggestions = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public CharSequence convertResultToString(Object resultValue) {
        String str = ((CityInfo) resultValue).cityName;
        return str;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        if (constraint != null) {
            suggestions.clear();
            for (CityInfo cityInfo : tempItems) {
                if (cityInfo.cityName.toLowerCase().contains(constraint.toString().toLowerCase())) {
                    suggestions.add(cityInfo);
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = suggestions;
            filterResults.count = suggestions.size();
            return filterResults;
        } else {
            return new FilterResults();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        if (listener != null) {
            listener.onFilterPublishResult(constraint, (ArrayList<CityInfo>) results.values);
        }
    }

    public interface OnFilterNotifyListener {
        void onFilterPublishResult(CharSequence constraint, ArrayList<CityInfo> result);
    }
}
