package com.db.preferredcity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CityInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class NamesAdapter extends ArrayAdapter<CityInfo> {

    private Context context;
    private int resource;
    private List<CityInfo> items, tempItems, suggestions;
    private Vector<CityInfo> cityInfoVector;
    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    private Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((CityInfo) resultValue).cityName;
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (CityInfo names : tempItems) {
                    if (names.cityName.toLowerCase().contains(constraint.toString().toLowerCase()) || names.cityHindiName.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(names);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            try {
                if (results != null && results.count > 0) {
                    List<CityInfo> filterList = (ArrayList<CityInfo>) results.values;
                    clear();
                    if (filterList != null) {
                        for (CityInfo names : filterList) {
                            add(names);
                            notifyDataSetChanged();
                        }
                    }
                    filterList.clear();
                }
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    };

    public NamesAdapter(Context context, int resource, List<CityInfo> clone, Vector<CityInfo> items) {
        super(context, resource, clone);
        this.context = context;
        this.resource = resource;
        this.items = clone;
        cityInfoVector = items;
        tempItems = new ArrayList<>(clone); // this makes the difference.
        suggestions = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resource, parent, false);
        }
        CityInfo cityName = items.get(position);
        if (cityName != null) {
            TextView lblName = view.findViewById(R.id.textview);
            if (lblName != null)
                lblName.setText(String.format("%s (%s)",cityName.cityName,cityName.cityHindiName));
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }
}