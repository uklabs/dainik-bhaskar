package com.db.preferredcity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


import com.bhaskar.util.CommonConstants;
import com.db.util.JsonParser;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.CityInfo;
import com.db.main.BaseAppCompatActivity;
import com.db.news.PrefferedCityNewsListFragment;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.ThemeUtil;
import com.db.util.AppUtils;

import java.util.Vector;


public class PreferredCityActivity extends BaseAppCompatActivity implements OnFinishListener {
    public static final String CALL_PREFERRED_CITY_HOME = "call_preferred_city_home";
    private static final int SELECT_CITY_FRAGMENT = 1;
    public static final int CITY_FEED_FRAGMENT = 2;
    private AppBarLayout appbar;
    private int showCurrentFragment = SELECT_CITY_FRAGMENT;
    private AppBarLayout.LayoutParams appBarLayoutParams;
    private Fragment fragment = null;
    private boolean callSelectCityFragment;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private Button editItem1;
    private Button saveItem1;
    private CityInfo cityInfo;
    private String title;
    private String themeColor;
    private CategoryInfo categoryInfo;
    private TextView toolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_preferred_city);
        toolbar = findViewById(R.id.toolbar);
        tabLayout = findViewById(R.id.tab_layout);
        toolbarTitle = findViewById(R.id.toolbar_title);

        setSupportActionBar(toolbar);
        appBarLayoutParams = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back);

        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        saveItem1 = toolbar.findViewById(R.id.saveBtn);
        editItem1 = toolbar.findViewById(R.id.editBtn);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(CALL_PREFERRED_CITY_HOME)) {
                callSelectCityFragment = intent.getBooleanExtra(CALL_PREFERRED_CITY_HOME, false);
            }

            themeColor = intent.getStringExtra(Constants.KeyPair.KEY_COLOR);
            cityInfo = (CityInfo) intent.getSerializableExtra(Constants.KeyPair.KEY_CITY_INFO);
            title = intent.getStringExtra(Constants.KeyPair.KEY_TITLE);
            categoryInfo = (CategoryInfo) intent.getSerializableExtra(Constants.KeyPair.KEY_CATEGORY_INFO);
        }

        appbar = findViewById(R.id.appbar);
        overrideThemeColor(themeColor);
    }

    private void overrideThemeColor(String color) {
        if (!TextUtils.isEmpty(color)) {
            appbar.setBackgroundColor(AppUtils.getThemeColor(this, color));
            tabLayout.setTabTextColors(getResources().getColor(R.color.tab_layout_text_color), getResources().getColor(R.color.tab_layout_text_color_selected));
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.tab_indicator_color));
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            toolbar.setNavigationIcon(R.drawable.ica_back);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back);
            AppUtils.getInstance().setStatusBarColor(this, color);
            appbar.setBackgroundColor(AppUtils.getThemeColor(this, color));
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Vector<CityInfo> cityInfoVector = JsonParser.getInstance().analyzePreferredCity(PreferredCityActivity.this, CommonConstants.CHANNEL_ID);
        if (cityInfoVector.size() > 0) {
            showCurrentFragment = CITY_FEED_FRAGMENT;
        } else {
            showCurrentFragment = SELECT_CITY_FRAGMENT;
        }
        handleSaveEditButtons();
        addFragment(showCurrentFragment, callSelectCityFragment);
    }

    public void addFragment(int id, boolean isFinish) {
        showCurrentFragment = id;
        switch (id) {
            case SELECT_CITY_FRAGMENT:
                fragment = SelectPreferredCityFragment.getInstance(isFinish);

                if (!TextUtils.isEmpty(title))
                    toolbarTitle.setText(title);
                else
                    toolbarTitle.setText(R.string.preferred_city);

                appBarLayoutParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
                findViewById(R.id.tab_layout).setVisibility(View.GONE);
                AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
                params.setScrollFlags(0);
                if (editItem1 != null)
                    editItem1.setVisibility(View.GONE);
                if (saveItem1 != null)
                    saveItem1.setVisibility(View.GONE);
                break;

            case CITY_FEED_FRAGMENT:
                fragment = PrefferedCityNewsListFragment.newInstance(categoryInfo, cityInfo);
                if (!TextUtils.isEmpty(title))
                    toolbarTitle.setText(title);
                else
                    toolbarTitle.setText(R.string.preferred_city);
                appBarLayoutParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
                tabLayout.setVisibility(View.GONE);
                if (editItem1 != null)
                    editItem1.setVisibility(View.GONE);
                if (saveItem1 != null)
                    saveItem1.setVisibility(View.GONE);
                break;
        }

        try {
            if (fragment != null) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.preferred_city_frame_layout, fragment);
                transaction.commitAllowingStateLoss();
            }
        } catch (IllegalStateException e) {
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        if (!callSelectCityFragment && getSupportFragmentManager().findFragmentById(R.id.preferred_city_frame_layout) instanceof SelectPreferredCityFragment) {
            saveItem1.setVisibility(View.GONE);
            showCurrentFragment = CITY_FEED_FRAGMENT;
            addFragment(showCurrentFragment, false);
        } else {
            super.onBackPressed();
        }
    }

    private void handleSaveEditButtons() {
        int theme = AppPreferences.getInstance(this).getIntValue(Constants.Themes.THEME_COLOR, Constants.Themes.THEME_DEFAULT);
        if (theme == Constants.Themes.THEME_DARK_GRAY) {
            saveItem1.setTextColor(ContextCompat.getColor(this, R.color.save_btn_txt_def));
            editItem1.setTextColor(ContextCompat.getColor(this, R.color.save_btn_txt_def));
            saveItem1.setBackgroundResource(R.drawable.pref_city_save_btn_bg_def);
            editItem1.setBackgroundResource(R.drawable.pref_city_save_btn_bg_def);
        } else {
            saveItem1.setTextColor(ContextCompat.getColor(this, R.color.save_btn_txt_theme));
            editItem1.setTextColor(ContextCompat.getColor(this, R.color.save_btn_txt_theme));
            saveItem1.setBackgroundResource(R.drawable.pref_city_save_btn_bg_theme);
            editItem1.setBackgroundResource(R.drawable.pref_city_save_btn_bg_theme);
        }
        if (showCurrentFragment == CITY_FEED_FRAGMENT) {
//            editItem1.setVisibility(View.VISIBLE);
            editItem1.setVisibility(View.GONE);
            saveItem1.setVisibility(View.GONE);
        } else {
            editItem1.setVisibility(View.GONE);
//            saveItem1.setVisibility(View.VISIBLE);
            saveItem1.setVisibility(View.GONE);
        }

        editItem1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editItem1.setVisibility(View.GONE);
                showCurrentFragment = SELECT_CITY_FRAGMENT;
                addFragment(showCurrentFragment, false);
            }
        });

        saveItem1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // hide keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (view == null)
                    view = new View(PreferredCityActivity.this);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                boolean isSaveSuccess = false;
                Fragment lastFragment = getSupportFragmentManager().findFragmentById(R.id.preferred_city_frame_layout);
                if (lastFragment instanceof SelectPreferredCityFragment) {
                    isSaveSuccess = ((SelectPreferredCityFragment) lastFragment).onSavePreferredCityList();
                }
                if (isSaveSuccess) {
                    saveItem1.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onFinish() {
        setResult(RESULT_OK);
        finish();
    }
}
