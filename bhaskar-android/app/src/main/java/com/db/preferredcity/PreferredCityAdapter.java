package com.db.preferredcity;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bhaskar.R;
import com.db.data.models.CityInfo;
import com.db.util.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class PreferredCityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private Vector<CityInfo> cityInfoVector;
    private List<CityInfo> cityInfoArrayList;
    private List<AutoCompleteTextView> listAutoTV;

    public PreferredCityAdapter(Context context, Vector<CityInfo> cityInfoVector, List<CityInfo> cityInfoArrayList) {
        this.mContext = context;
        this.cityInfoVector = cityInfoVector;
        this.cityInfoArrayList = cityInfoArrayList;
        listAutoTV = new ArrayList<>();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new SelectCityHolder(inflater.inflate(R.layout.preferred_city_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final SelectCityHolder mainViewHolder = (SelectCityHolder) holder;

        mainViewHolder.tvSNo.setText(position + 1 + ".");
        if (cityInfoArrayList != null && cityInfoVector.size() > position) {
            mainViewHolder.ibDelete.setVisibility(View.VISIBLE);
            mainViewHolder.tvCity.setText(cityInfoVector.get(position).cityName);
//            mainViewHolder.tvCity.setEnabled(false);
        } else {
            mainViewHolder.ibDelete.setVisibility(View.INVISIBLE);
            mainViewHolder.tvCity.setText("");
//            mainViewHolder.tvCity.setEnabled(true);
        }
        mainViewHolder.tvCity.setEnabled(true);

        if (!listAutoTV.contains(mainViewHolder.tvCity) && listAutoTV.size() < cityInfoVector.size()) {
            listAutoTV.add(mainViewHolder.tvCity);
        }

        final NamesAdapter cityAutoCompleteListAdapter = new NamesAdapter(mContext, R.layout.preferred_city_auto_complete_item, new ArrayList<>(cityInfoArrayList), cityInfoVector);
        mainViewHolder.tvCity.setThreshold(2);
        mainViewHolder.tvCity.setAdapter(cityAutoCompleteListAdapter);
//        mainViewHolder.tvCity.setOnDismissListener(new AutoCompleteTextView.OnDismissListener() {
//            @Override
//            public void onDismiss() {
//                mainViewHolder.tvCity.setText("");
//            }
//        });
        mainViewHolder.tvCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long l) {
                CityInfo cityInfo = cityAutoCompleteListAdapter.getItem(pos);
                cityInfo.citySelectionValue = Constants.CityRajyaSelection.SELECTED;
                if (position < cityInfoVector.size()) {

                    cityInfoVector.remove(position);
                    cityInfoVector.add(position, cityInfo);

                } else {
                    cityInfoVector.add(cityInfoVector.size(), cityInfo);
                }
                notifyDataSetChanged();
            }
        });

        mainViewHolder.ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityInfoVector.size() > position) {
                    cityInfoVector.remove(position);
                    listAutoTV.remove(position);
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return cityInfoVector.size() + 1;
    }


    private class SelectCityHolder extends RecyclerView.ViewHolder {

        private AutoCompleteTextView tvCity;
        private TextView tvSNo;
        private ImageButton ibDelete;

        SelectCityHolder(View itemView) {
            super(itemView);
            tvSNo = (TextView) itemView.findViewById(R.id.preferred_city_item_no_tv);
            ibDelete = (ImageButton) itemView.findViewById(R.id.preferred_city_item_delete_ib);
            tvCity = (AutoCompleteTextView) itemView.findViewById(R.id.preferred_city_item_auto_complete_tv);
        }
    }
}