package com.db.preferredcity;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.CityInfo;
import com.db.data.source.remote.SerializeData;
import com.db.data.source.server.BackgroundRequest;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.JsonPreferences;
import com.db.util.QuickPreferences;

import java.util.List;
import java.util.Vector;

public class SelectPreferredCityFragment extends Fragment implements OnFeedFetchFromServerListener {

    private static final String IS_FINISH = "IS_FINISH";
    private PreferredCityAdapter preferredCityListAdapter;

    private Vector<CityInfo> cityInfoVector;

    private boolean isFinish;

    private OnFinishListener onFinishWhenSaveListener;
    private RecyclerView cityRecyclerView;
    private RelativeLayout loadingLayout;

    public static SelectPreferredCityFragment getInstance(boolean isFinish) {
        SelectPreferredCityFragment selectPreferredCityFragment = new SelectPreferredCityFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_FINISH, isFinish);
        selectPreferredCityFragment.setArguments(bundle);
        return selectPreferredCityFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            isFinish = bundle.getBoolean(IS_FINISH);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onFinishWhenSaveListener = (OnFinishListener) context;
        } catch (ClassCastException ex) {

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_select_preferred_city, container, false);
        loadingLayout = rootView.findViewById(R.id.loadingLayout);
        cityRecyclerView = rootView.findViewById(R.id.preferred_city_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        cityRecyclerView.setLayoutManager(layoutManager);

        new GetCityData().execute();

        return rootView;
    }

    public boolean onSavePreferredCityList() {
        if (cityInfoVector != null && !cityInfoVector.isEmpty()) {
            SerializeData.getInstance(getContext()).setSelectedCityList(CommonConstants.CHANNEL_ID, cityInfoVector);

            if (isFinish) {
                onFinishWhenSaveListener.onFinish();
                return true;
            } else {
                ((PreferredCityActivity) getActivity()).addFragment(PreferredCityActivity.CITY_FEED_FRAGMENT, false);
                return true;
            }
        } else {
            AppUtils.getInstance().showCustomToast(getContext(), getString(R.string.enter_city_error_msg));
            return false;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

//    @Override
//    public void onDataFetch(boolean flag, BrandInfo brandInfo) {
//    }

    @Override
    public void onDataFetch(boolean flag) {
        if (flag) {
            List<CityInfo> cityInfoArrayList = JsonParser.getInstance().getCityFeedList(getContext(), CommonConstants.CHANNEL_ID);
            if (cityInfoArrayList != null && !cityInfoArrayList.isEmpty()) {
                preferredCityListAdapter = new PreferredCityAdapter(getContext(), cityInfoVector, cityInfoArrayList);
                cityRecyclerView.setAdapter(preferredCityListAdapter);
            } else {
            }
        }
    }

    @Override
    public void onDataFetch(boolean flag, String sectionName) {
    }

    @Override
    public void onDataFetch(boolean flag, int viewType) {
    }

    @Override
    public void onDataFetch(boolean flag, CategoryInfo categoryInfo) {

    }

    class GetCityData extends AsyncTask<Void, Void, List<CityInfo>> {

        @Override
        protected void onPreExecute() {
            cityRecyclerView.setVisibility(View.GONE);
            loadingLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<CityInfo> doInBackground(Void... params) {
            cityInfoVector = new Vector<>(SerializeData.getInstance(getActivity()).getSelectedCityList(CommonConstants.CHANNEL_ID));
            List<CityInfo> cityInfoArrayList = JsonParser.getInstance().getCityFeedList(getActivity(), CommonConstants.CHANNEL_ID);
            return cityInfoArrayList;
        }

        @Override
        protected void onPostExecute(List<CityInfo> cityInfoArrayList) {
            try {
                int cityVersion = JsonPreferences.getInstance(getContext()).getIntValue(String.format(QuickPreferences.JsonPreference.PREFERRED_CITY_FEED_VERSION, CommonConstants.CHANNEL_ID), 0);
                int latestCityVersion = JsonParser.getInstance().getVersionOfParticularAPI(getContext(), Constants.ApiName.CITY_LIST_API);
                if (cityInfoArrayList != null && !cityInfoArrayList.isEmpty() && cityVersion == latestCityVersion) {
                    preferredCityListAdapter = new PreferredCityAdapter(getActivity(), cityInfoVector, cityInfoArrayList);
                    cityRecyclerView.setAdapter(preferredCityListAdapter);
                } else {
                    BackgroundRequest.fetchPreferredCityFeedFromServer(getContext(), CommonConstants.CHANNEL_ID, SelectPreferredCityFragment.this);
                }
                cityRecyclerView.setVisibility(View.VISIBLE);
                loadingLayout.setVisibility(View.GONE);
            } catch (Exception e) {
            }
        }
    }
}
