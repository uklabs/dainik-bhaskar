package com.db.providers

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bhaskar.R
import com.db.InitApplication
import com.db.core.CoreActivity
import com.db.core.CoreProvider
import com.db.data.models.PromoWidgetModel
import com.db.database.DatabaseClient
import com.db.home.ActivityUtil
import com.db.util.AppPreferences
import com.db.util.DbSharedPref
import com.db.util.ImageUtil
import com.db.util.OnDragTouchListener

class NewsDetailDragUiProvider(private val coreActivity: CoreActivity) : CoreProvider(coreActivity.dbApp) {

    companion object {
        private val TAG = NewsDetailDragUiProvider::class.java.simpleName
    }

    @SuppressLint("ClickableViewAccessibility")
    fun init(fragmentView: View, promoWidgetModel: PromoWidgetModel, isFromVerticalLayout: Boolean) {

        try {
            val viewGroupContainer = fragmentView.findViewById<ViewGroup>(R.id.container_news_detail_pager)
            val dragFl = LayoutInflater.from(dbApp).inflate(R.layout.layout_news_detail_draggable, viewGroupContainer,
                    false) as FrameLayout

            viewGroupContainer.addView(dragFl)

            val marginReg = dbApp.resources.getDimensionPixelSize(R.dimen._10sdp)
            var marginBottom = 0
            marginBottom = if (isFromVerticalLayout) {
                dbApp.resources.getDimensionPixelSize(R.dimen._35sdp)
            } else {
                dbApp.resources.getDimensionPixelSize(R.dimen._75sdp)
            }
            val dragParams = dragFl.layoutParams as RelativeLayout.LayoutParams
            dragParams.setMargins(0, 0, marginReg, marginBottom)
            setDataToUI(dragFl, promoWidgetModel)

            val onDragTouchListener = OnDragTouchListener(dragFl, viewGroupContainer)

            onDragTouchListener.setOnDragViewClickListener(View.OnClickListener {
                try {
                    val menuId = promoWidgetModel.menuId
                    if (TextUtils.isEmpty(menuId) || menuId.equals("0", true)) {
                        if (!TextUtils.isEmpty(promoWidgetModel.linkUrl)) {
                            ActivityUtil.getInstance().handleDynamicLink(coreActivity, promoWidgetModel.linkUrl)
                        } else {
                            //Do not add control
                        }
                    } else {
                        val categoryInfo = DatabaseClient.getInstance(dbApp).getCategoryById(menuId)
                        ActivityUtil.openCategoryAccordingToAction(coreActivity, categoryInfo, "")
                    }
                } catch (e: Exception) {
                    Log.e(TAG, "OnDragView OnClick(): "+e.message)
                }
            })

            dragFl.setOnTouchListener(onDragTouchListener)

            dragFl.findViewById<View>(R.id.iv_cancel).setOnClickListener {
                try {
                    viewGroupContainer.removeView(dragFl)
                    DbSharedPref.setPromoWidgetVisibilityStatus(dbApp, false)
                } catch (e: Exception) {
                    Log.e(TAG, "dragFl OnClick(): "+e.message)
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, "init(): "+e.message)
        }
    }

    private fun setDataToUI(dragFl: FrameLayout, promoWidgetModel: PromoWidgetModel) {
        try {
            val promoIv = dragFl.findViewById<ImageView>(R.id.iv_promotional)
            promoIv.visibility = View.VISIBLE
            ImageUtil.setImage(dbApp, promoWidgetModel.iconUrl, promoIv, R.mipmap.app_icon)
        } catch (e: Exception) {
            Log.e(TAG, "setDataToUI(): "+e.message)
        }
    }
}