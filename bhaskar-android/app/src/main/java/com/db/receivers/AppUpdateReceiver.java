package com.db.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.db.util.AppPreferences;
import com.db.util.QuickPreferences;
import com.db.util.Systr;

public class AppUpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Systr.println("AppUpdateReceiver : Intent: " + intent.getAction());

        // Reset the display rate us popup data.
        AppPreferences.getInstance(context).setIntValue(QuickPreferences.APP_RATEUS_SESSION_COUNT, 0);
        AppPreferences.getInstance(context).setBooleanValue(QuickPreferences.IS_APP_RATED, false);

    }
}
