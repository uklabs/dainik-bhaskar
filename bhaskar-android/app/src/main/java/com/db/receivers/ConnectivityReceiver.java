package com.db.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.db.InitApplication;
import com.db.dbvideo.player.BusProvider;

/**
 * Created by Imran Husain on 6/6/2017.
 * <p>
 * this receiver is use to check the Internet connection is available or not
 */

public class ConnectivityReceiver
        extends BroadcastReceiver {

    public ConnectivityReceiver() {
        super();
    }

    /**
     * For Manual Internet Check
     *
     * @return boolean value
     */
    public static boolean isConnected() {
        ConnectivityManager
                cm = (ConnectivityManager) InitApplication.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onReceive(Context context, Intent arg1) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            Boolean isConnected = activeNetwork != null && activeNetwork.isAvailable() && activeNetwork.isConnectedOrConnecting();
//            Log.d("Inside On recieve", "" + isConnected);

            BusProvider.getInstance().post(isConnected);
        } catch (Exception e) {
        }
    }


}