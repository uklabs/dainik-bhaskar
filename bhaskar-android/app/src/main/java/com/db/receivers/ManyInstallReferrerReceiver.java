package com.db.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.db.util.AppConfig;
import com.db.util.AppLogs;
import com.db.util.AppPreferences;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.google.android.gms.analytics.CampaignTrackingReceiver;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;


public class ManyInstallReferrerReceiver extends BroadcastReceiver {

    private final String TAG = AppConfig.BaseTag + "." + ManyInstallReferrerReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String data = intent.getStringExtra("referrer");
            Systr.println("Referrer => " + data);
            try {
                data = URLDecoder.decode(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                data = null;
            }
            if (data != null) {
                Map<String, String> paramsMap = new HashMap<>();
                for (String s : data.split("&")) {
                    paramsMap.put(s.split("=")[0], s.split("=")[1]);
                    AppLogs.printErrorLogs(TAG + s.split("=")[0], s.split("=")[1]);
                }
                String utm_source = paramsMap.get(QuickPreferences.UTM_SOURCE);
                String utm_medium = paramsMap.get(QuickPreferences.UTM_MEDIUM);
                String utm_campaign = paramsMap.get(QuickPreferences.UTM_CAMPAIGN);

                AppPreferences prefs = AppPreferences.getInstance(context);
                if (prefs.getStringValue(QuickPreferences.UTM_SOURCE, "").length() == 0 || prefs.getStringValue(QuickPreferences.UTM_CAMPAIGN, "").equalsIgnoreCase(QuickPreferences.UTM_DIRECT)) {

                    if (!utm_source.contains("(not set)")) {
                        prefs.setStringValue(QuickPreferences.UTM_SOURCE_TEMP, utm_source);

                    }
                    if (!utm_medium.contains("(not set)")) {
                        prefs.setStringValue(QuickPreferences.UTM_MEDIUM_TEMP, utm_medium);

                    }
                    if (!utm_campaign.contains("(not set)")) {
                        prefs.setStringValue(QuickPreferences.UTM_CAMPAIGN_TEMP, utm_campaign);
                    }
                }

                new CampaignTrackingReceiver().onReceive(context, intent);
            }
        } catch (Exception ignored) {
        }
    }
}