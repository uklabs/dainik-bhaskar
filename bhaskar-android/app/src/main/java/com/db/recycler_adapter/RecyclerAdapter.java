/*
 * Copyright © 2018, DB Corp,
 *
 */

package com.db.recycler_adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;

/**
 * Helper class to easily work with Android's RecyclerView.Adapter.
 *
 * @author Aleksandar Gotev
 */
public final class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapterViewHolder>
        implements RecyclerAdapterNotifier {

    private LinkedHashMap<String, Integer> typeIds;
    private LinkedHashMap<Integer, AdapterItem> types;
    private List<AdapterItem> itemsList;
    private AdapterItem emptyItem;
    private final int emptyItemId = 0;

    private List<AdapterItem> filtered;
    private boolean showFiltered;


    /**
     * Property for selecting the type animation for item transition when scrolling.
     * By default value is -1, means no animation to be applied.
     */
    private int itemAnimationType = -1;

    public static final int ANIMATION_TYPE_LIST_ITEM = 0;

    public static final int ANIMATION_TYPE_BRIEF_ITEM = 1;

    private int lastPosition = -1;

    /**
     * Creates a new recyclerAdapter
     */
    public RecyclerAdapter() {
        typeIds = new LinkedHashMap<>();
        types = new LinkedHashMap<>();
        itemsList = new ArrayList<>();
        emptyItem = null;
    }


    private List<AdapterItem> getItems() {
        return showFiltered ? filtered : itemsList;
    }


    /**
     * Adds a new item to this adapter
     *
     * @param item item to add
     * @return {@link RecyclerAdapter}
     */
    public RecyclerAdapter add(AdapterItem item) {
        registerItemType(item);
        getItems().add(item);
        removeEmptyItemIfItHasBeenConfigured();

        notifyItemInserted(getItems().size() - 1);
        return this;
    }

    /**
     * Gets the position of an item in an adapter.
     * <p>
     * For the method to work properly, all the items has to override the
     * {@link AdapterItem#equals(Object)} and {@link AdapterItem#hashCode()} methods and
     * implement the required business logic code to detect if two instances are referring to the
     * same item (plus some other changes). Check the example in {@link RecyclerAdapter#add(AdapterItem)}
     *
     * @param item item object
     * @return the item's position or -1 if the item does not exist
     */
    public int getItemPosition(AdapterItem item) {
        return getItems().indexOf(item);
    }


    /**
     * @param item
     * @param position
     */
    public void updateItemAtPosition(AdapterItem item, int position) {
        getItems().set(position, item);
        notifyItemChanged(position);
    }

    /**
     * Syncs the internal list of items with a list passed as parameter.
     * Adds, updates or deletes internal items, with RecyclerView animations.
     * <p>
     * For the sync to work properly, all the items has to override the
     * {@link AdapterItem#equals(Object)} and {@link AdapterItem#hashCode()} methods and
     * implement the required business logic code to detect if two instances are referring to the
     * same item. Check the example in {@link RecyclerAdapter#add(AdapterItem)}.
     * If two instances are referring to the same item, you can decide if the item should be
     * replaced by the new one, by implementing {@link AdapterItem#hasToBeReplacedBy(AdapterItem)}.
     * Check hasToBeReplacedBy method JavaDoc for more information.
     *
     * @param newItems list of new items. Passing a null or empty list will result in
     *                 {@link RecyclerAdapter#clear()} method call.
     * @return {@link RecyclerAdapter}
     */
    RecyclerAdapter syncWithItems(final List<? extends AdapterItem> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            clear();
            return this;
        }

        ListIterator<AdapterItem> iterator = getItems().listIterator();

        while (iterator.hasNext()) {
            int internalListIndex = iterator.nextIndex();
            AdapterItem item = iterator.next();

            int indexInNewItemsList = newItems.indexOf(item);
            // if the item does not exist in the new list, it means it has been deleted
            if (indexInNewItemsList < 0) {
                iterator.remove();
                notifyItemRemoved(internalListIndex);
            } else { // the item exists in the new list
                AdapterItem newItem = newItems.get(indexInNewItemsList);
                if (item.hasToBeReplacedBy(newItem)) { // the item needs to be updated
                    updateItemAtPosition(newItem, internalListIndex);
                }
                newItems.remove(indexInNewItemsList);
            }
        }

        for (AdapterItem newItem : newItems) {
            add(newItem);
        }

        return this;
    }

    /**
     * Removes an item from the adapter.
     * <p>
     * For the remove to work properly, all the items has to override the
     * {@link AdapterItem#equals(Object)} and {@link AdapterItem#hashCode()} methods.
     *
     * @param item item to remove
     * @return true if the item has been correctly removed or false if the item does not exist
     */
    public boolean removeItem(AdapterItem item) {
        int itemIndex = getItems().indexOf(item);

        if (itemIndex < 0) {
            return false;
        }

        return removeItemAtPosition(itemIndex);
    }

    /**
     * Adds a new item to this adapter
     *
     * @param item     item to add
     * @param position position at which to add the element. The item previously at
     *                 (position) will be at (position + 1) and the same for all the subsequent
     *                 elements
     * @return {@link RecyclerAdapter}
     */
    public RecyclerAdapter addAtPosition(AdapterItem item, int position) {
        if (position >= 0) {
            registerItemType(item);
            getItems().add(position, item);
            removeEmptyItemIfItHasBeenConfigured();

            notifyItemInserted(position);
            return this;
        }
        return null;
    }

    private void registerItemType(AdapterItem item) {
        final String className = item.getClass().getName();
        if (!typeIds.containsKey(className)) {
            int viewId = View.generateViewId();

            ////int viewId = className.hashCode();

            typeIds.put(className, viewId);
            types.put(viewId, item);
        }
    }

    private void removeEmptyItemIfItHasBeenConfigured() {
        // this is necessary to prevent IndexOutOfBoundsException on RecyclerView when the
        // first item gets added and an empty item has been configured
        if (getItems().size() == 1 && emptyItem != null) {
            notifyItemRemoved(0);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (adapterIsEmptyAndEmptyItemIsDefined()) {
            return emptyItemId;
        }

        AdapterItem item = getItems().get(position);
        String className = item.getClass().getName();
        return typeIds.get(className);
    }

    public int getItemViewType(String className) {
        if (adapterIsEmptyAndEmptyItemIsDefined()) {
            return emptyItemId;
        }
        return typeIds.get(className);
    }

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    /**
     * @param parent
     * @return
     */
    private LayoutInflater getLayoutInflater(ViewGroup parent) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(mContext);
        }
        return mLayoutInflater;
    }

    @Override
    public RecyclerAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            AdapterItem item;

            if (adapterIsEmptyAndEmptyItemIsDefined() && viewType == emptyItemId) {
                item = emptyItem;
            } else {
                item = types.get(viewType);
            }

            View view = getLayoutInflater(parent).inflate(item.getLayoutId(), parent, false);

            //         ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(ctx), item.getLayoutId(), parent, false);

            return item.getViewHolder(view, this);

        } catch (NoSuchMethodException exc) {
            Log.e(getClass().getSimpleName(), "onCreateViewHolder error: you should declare " +
                    "a constructor like this in your ViewHolder: " +
                    "public RecyclerAdapterViewHolder(View itemView, RecyclerAdapterNotifier adapter)");
            return null;

        } catch (IllegalAccessException exc) {
            Log.e(getClass().getSimpleName(), "Your ViewHolder class in " +
                    types.get(viewType).getClass().getName() + " should be public!");
            return null;

        } catch (Exception exc) {
            Log.e(getClass().getSimpleName(), "onCreateViewHolder error", exc);
            return null;
        }
    }


    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(@NonNull RecyclerAdapterViewHolder holder, int position) {
        if (adapterIsEmptyAndEmptyItemIsDefined()) {
            emptyItem.bindData(holder, emptyItem.getData(), position);
        } else {
            AdapterItem adapterItem = getItems().get(position);
            adapterItem.bindData(holder, adapterItem.getData(), position);
        }
    }

    /**
     * Set animation type for item transition when scrolling.
     *
     * @param type Animation Type
     */
    public void setItemAnimationType(int type) {
        itemAnimationType = type;
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerAdapterViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }


    @Override
    public int getItemCount() {
        if (adapterIsEmptyAndEmptyItemIsDefined())
            return 1;

        return getItems().size();
    }

    @Override
    public void sendEvent(RecyclerAdapterViewHolder holder, Bundle data) {
        int position = holder.getAdapterPosition();

        if (position < 0 || position >= getItems().size())
            return;

        if (getItems().get(position).onEvent(position, data))
            notifyItemChanged(position);
    }

    /**
     * Removes all the items with a certain class from this adapter and automatically notifies changes.
     *
     * @param clazz class of the items to be removed
     */
    public void removeAllItemsWithClass(Class<? extends AdapterItem> clazz) {
        removeAllItemsWithClass(clazz, new RemoveListener() {
            @Override
            public boolean hasToBeRemoved(AdapterItem item) {
                return true;
            }
        });
    }

    /**
     * Removes all the items with a certain class from this adapter and automatically notifies changes.
     *
     * @param clazz    class of the items to be removed
     * @param listener listener invoked for every item that is found. If the callback returns true,
     *                 the item will be removed. If it returns false, the item will not be removed
     */
    public void removeAllItemsWithClass(Class<? extends AdapterItem> clazz, RemoveListener listener) {
        if (clazz == null)
            throw new IllegalArgumentException("The class of the items can't be null!");

        if (listener == null)
            throw new IllegalArgumentException("RemoveListener can't be null!");

        if (getItems().isEmpty())
            return;

        ListIterator<AdapterItem> iterator = getItems().listIterator();
        int index;
        while (iterator.hasNext()) {
            index = iterator.nextIndex();
            AdapterItem item = iterator.next();
            if (item.getClass().getName().equals(clazz.getName()) && listener.hasToBeRemoved(item)) {
                iterator.remove();
                notifyItemRemoved(index);
            }
        }

        Integer id = typeIds.get(clazz.getName());
        if (id != null) {
            typeIds.remove(clazz.getName());
            types.remove(id);
        }
    }

    /**
     * Gets the last item with a given class, together with its position.
     *
     * @param clazz class of the item to search
     * @return Pair with position and AdapterItem or null if the adapter is empty or no items
     * exists with the given class
     */
    public Pair<Integer, AdapterItem> getLastItemWithClass(Class<? extends AdapterItem> clazz) {
        if (clazz == null)
            throw new IllegalArgumentException("The class of the items can't be null!");

        if (getItems().isEmpty())
            return null;

        for (int i = getItems().size() - 1; i >= 0; i--) {
            if (getItems().get(i).getClass().getName().equals(clazz.getName())) {
                return new Pair<>(i, getItems().get(i));
            }
        }

        return null;
    }


    /**
     * Removes an item in a certain position. Does nothing if the adapter is empty or if the
     * position specified is out of adapter bounds.
     *
     * @param position position to be removed
     * @return true if the item has been removed, false if it doesn't exist or the position
     * is out of bounds
     */
    public boolean removeItemAtPosition(int position) {
        if (getItems().isEmpty() || position < 0 || position >= getItems().size())
            return false;

        getItems().remove(position);
        notifyItemRemoved(position);

        return true;
    }

    /**
     * Gets an item at a given position.
     *
     * @param position item position
     * @return {@link AdapterItem} or null if the adapter is empty or the position is out of bounds
     */
    public AdapterItem getItemAtPosition(int position) {
        if (getItems().isEmpty() || position < 0 || position >= getItems().size())
            return null;

        return getItems().get(position);
    }

    /**
     * Clears all the elements in the adapter.
     */
    public void clear() {
        int itemsSize = getItems().size();
        getItems().clear();
        if (itemsSize > 0) {
            notifyItemRangeRemoved(0, itemsSize);
        }
    }

    private boolean adapterIsEmptyAndEmptyItemIsDefined() {
        return getItems().isEmpty() && emptyItem != null;
    }


    /**
     * Removes and Clears all the elements in the adapter.
     */
    public void clearAndRemove() {
        int itemsSize = getItems().size();
        if (itemsSize > 0) {
            if (!typeIds.isEmpty()) {
                List<String> classNames = new ArrayList(typeIds.keySet());
                for (String key : classNames) {
                    try {
                        removeAllItemsWithClass((Class<? extends AdapterItem>) Class.forName(key));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        getItems().clear();
        if (itemsSize > 0) {
            notifyItemRangeRemoved(0, itemsSize);
        }
    }


    /**
     * Filters this adapter with a given search term and shows only the items which
     * matches it.
     * <p>
     * For the filter to work properly, each item must override the
     * {@link AdapterItem#onFilter(String)} method and provide custom implementation.
     *
     * @param searchTerm search term
     */
    public void filter(final String searchTerm) {
        if (itemsList == null || itemsList.isEmpty()) {
            return;
        }

        if (searchTerm == null || searchTerm.isEmpty()) {
            showFiltered = false;
            notifyDataSetChanged();
            return;
        }

        if (filtered == null) {
            filtered = new ArrayList<>();
        } else {
            filtered.clear();
        }

        for (AdapterItem item : itemsList) {
            if (item.onFilter(searchTerm)) {
                filtered.add(item);
            }
        }

        showFiltered = true;
        notifyDataSetChanged();
    }


    @Override
    public void onViewRecycled(@NonNull RecyclerAdapterViewHolder holder) {
        try {
            super.onViewRecycled(holder);
            int position = holder.getAdapterPosition();
            if (getItems() != null && position < getItems().size() && position > -1) {
                AdapterItem adapterItem = getItems().get(position);
                if (null != adapterItem) {
                    adapterItem.onViewRecycled(holder);
                }
            }
        } catch (Exception e) {
            Log.e("onViewRecycled", e.getMessage() + "");
        }
    }
}