/*
 * Copyright © 2018, DB Corp,
 *
 */

package com.db.recycler_adapter;

import android.os.Bundle;

/**
 * Contains methods to notify the adapter.
 *
 * @author Aleksandar Gotev
 */
public interface RecyclerAdapterNotifier {
    void sendEvent(RecyclerAdapterViewHolder holder, Bundle data);

}
