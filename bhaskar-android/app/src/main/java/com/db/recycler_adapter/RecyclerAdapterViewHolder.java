/*
 * Copyright © 2018, DB Corp,
 *
 */

package com.db.recycler_adapter;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;

/**
 * Base ViewHolder class to extend in subclasses.
 *
 * @author Aleksandar Gotev
 */
public abstract class RecyclerAdapterViewHolder extends RecyclerView.ViewHolder {

    private final WeakReference<RecyclerAdapterNotifier> adapter;

    /***
     *
     * @param view
     * @param adapter
     */
    public RecyclerAdapterViewHolder(View view, RecyclerAdapterNotifier adapter) {
        super(view);
        this.adapter = new WeakReference<>(adapter);

    }


    protected final View findViewById(int id) {
        return itemView.findViewById(id);
    }


}
