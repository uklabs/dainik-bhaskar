/*
 * Copyright © 2018, DB Corp,
 *
 */

package com.db.recycler_adapter;

/**
 * Listener invoked for every element that is going to be removed.
 *
 * @author Aleksandar Gotev
 */

public interface RemoveListener {
    boolean hasToBeRemoved(AdapterItem item);
}
