package com.db.search;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.NewsSearchHistory;
import com.db.util.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class SearchFragmentDialog extends DialogFragment {

    private ProgressBar progressBar;
    private View rootView;
    private ImageView back;
    private RecyclerView searchContentRecyclerView;
    private SearchNewsHistoryAdapter searchNewsAdapter;
    private EditText searchEditText;
    private SearchItemListener listener;

    public static SearchFragmentDialog newInstance() {
        SearchFragmentDialog f = new SearchFragmentDialog();
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (SearchItemListener) context;
        } catch (ClassCastException e) {
        }
    }

    public void registerListener(SearchItemListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.search_fragment_dialog, container, false);

        CardView actionBarLayout = rootView.findViewById(R.id.action_bar_layout);
        actionBarLayout.setBackgroundColor(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundAccent));

        searchContentRecyclerView = rootView.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        searchContentRecyclerView.setLayoutManager(layoutManager);

        searchEditText = rootView.findViewById(R.id.et_search);

        searchEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (!TextUtils.isEmpty(searchEditText.getText().toString())) {
                    AppUtils.getInstance().saveSearchHistory(getActivity(), searchEditText.getText().toString());

                    if (listener != null) {
                        listener.onItemClicked(searchEditText.getText().toString());
                    }
                }
                return true;
            }
            return false;
        });

        back = rootView.findViewById(R.id.back);
        back.setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarIconColor), PorterDuff.Mode.SRC_IN);
        searchEditText.setTextColor(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarIconColor));
        searchEditText.setHintTextColor(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarIconColor));
        back.setOnClickListener(view -> getDialog().dismiss());

        progressBar = rootView.findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        initializePreviousSearch();
        return rootView;
    }

    private void initializePreviousSearch() {
        new getNewsSearchHistory().execute();
        searchEditText.setHint(R.string.search_hint);
    }

    List<String> newsSearchHistoryList;

    private class getNewsSearchHistory extends AsyncTask<Void, Void, List<String>> {

        @Override
        protected List<String> doInBackground(Void... voids) {
            final NewsSearchHistory newsSearchHistory = (NewsSearchHistory) DatabaseHelper.getInstance(getActivity()).getTableObject(NewsSearchHistory.TABLE_NAME);
            if (newsSearchHistoryList == null) {
                newsSearchHistoryList = new ArrayList<>();
            }
            newsSearchHistoryList = newsSearchHistory.getAllSearchHistory();
            return newsSearchHistoryList;
        }

        @Override
        protected void onPostExecute(List<String> stringList) {
            try {
                if (stringList != null && !stringList.isEmpty()) {
                    searchNewsAdapter = new SearchNewsHistoryAdapter(getActivity(), stringList, listener);
                    searchContentRecyclerView.setAdapter(searchNewsAdapter);
                    searchContentRecyclerView.setVisibility(View.VISIBLE);
                }
                progressBar.setVisibility(View.GONE);
            } catch (Exception e) {

            }
        }
    }

    public interface SearchItemListener {
        void onItemClicked(String searchKeyword);
    }

}