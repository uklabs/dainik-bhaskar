package com.db.search;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.R;

import java.util.List;

/**
 * Created by lalgos1 on 29-01-2018.
 */

public class SearchNewsHistoryAdapter extends RecyclerView.Adapter<SearchNewsHistoryAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    private List<String> searchList;
    private SearchFragmentDialog.SearchItemListener listener;

    public SearchNewsHistoryAdapter(Context context, List<String> searchList, SearchFragmentDialog.SearchItemListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.searchList = searchList;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.video_provider_search_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.ivSearchArrow.setVisibility(View.VISIBLE);
        holder.ivSearchTime.setVisibility(View.VISIBLE);
        holder.name.setText(searchList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClicked(searchList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView ivSearchTime, ivSearchArrow;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            ivSearchTime = view.findViewById(R.id.iv_search_time);
            ivSearchArrow = view.findViewById(R.id.iv_search_arrow);
        }
    }
}
