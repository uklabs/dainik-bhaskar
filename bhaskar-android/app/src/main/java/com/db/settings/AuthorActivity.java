package com.db.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.db.main.BaseAppCompatActivity;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bhaskar.R;

import java.util.List;

public class AuthorActivity extends BaseAppCompatActivity implements OnFragUpdateListener {

    private String[] tabList;
    private ViewPager pager;

    public static Intent getIntent(Context context) {
        return new Intent(context, AuthorActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author);
        tabList = new String[]{
                getString(R.string.favourite_author_txt),getString(R.string.all_author_txt)
        };
        /*Set Action bar*/
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(AppUtils.fetchAttributeColor(this, R.attr.toolbarIconColor));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> AuthorActivity.this.finish());

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back_black);
            getSupportActionBar().setTitle(null);

            TextView tvTitle = findViewById(R.id.toolbar_title);
            tvTitle.setTextColor(Color.BLACK);
            tvTitle.setText(getString(R.string.author_txt));
        } catch (Exception ignored) {
        }

        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager = findViewById(R.id.viewpager);
        TabLayout tabLayout = findViewById(R.id.tab_layout);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null)
                tab.setCustomView(pagerAdapter.getTabView(AuthorActivity.this));
        }

        tabLayout.setupWithViewPager(pager);
        pager.setOffscreenPageLimit(1);

        pager.setAdapter(pagerAdapter);
    }

    @Override
    public void onFragUpdate(int type, int position) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        Object fragmentInstance = fragments.get(position);
        if (fragmentInstance instanceof AuthorFragment) {
            ((AuthorFragment) fragmentInstance).refreshPage(type);
        }

    }

    @Override
    public void onSwitchFragment(int position) {
        try {
            pager.setCurrentItem(position);
        } catch (Exception e) {
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return AuthorFragment.newInstance(AuthorFragment.AUTHOR_LIST_FOLLOWED);

            return AuthorFragment.newInstance(AuthorFragment.AUTHOR_LIST_ALL);
        }

        @Override
        public int getCount() {
            return tabList.length;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabList[position];
        }

        View getTabView(Context context) {
            return LayoutInflater.from(context).
                    inflate(R.layout.tab_author_layout, null);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if( requestCode== Constants.REQ_CODE_BOTTOM_NAV){
            setResult(Activity.RESULT_OK);
            finish();
        }
    }
}
