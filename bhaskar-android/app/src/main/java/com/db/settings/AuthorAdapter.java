package com.db.settings;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.AuthorInfo;
import com.db.util.Constants;
import com.db.util.ImageUtil;

import java.util.ArrayList;

public class AuthorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int pageType;

    public ArrayList<AuthorInfo> getList() {
        if (null != mList) {
            return mList;
        }
        return null;
    }

    void clearCheckList() {
        checkedIds.clear();
    }

    public interface OnAuthorCheckedListener {
        void onCheck(int pageType, boolean isChecked, AuthorInfo info);
    }

    private Activity context;
    private ArrayList<AuthorInfo> mList;
    private OnAuthorCheckedListener listener;

    private ArrayList<String> checkedIds;

    AuthorAdapter(Activity context, int pageType, OnAuthorCheckedListener listener) {
        this.pageType = pageType;
        mList = new ArrayList<>();
        checkedIds = new ArrayList<>();

        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_author_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.nameTv.setText(mList.get(position).title);

            ImageUtil.setImage(context, mList.get(position).image, viewHolder.picIv, R.drawable.profilepic);

            if (checkedIds.contains(mList.get(position).id)) {
                viewHolder.checkBox.setChecked(true);
            } else {
                viewHolder.checkBox.setChecked(false);
            }

            viewHolder.checkBox.setOnClickListener(view -> {
                if (listener != null) {
                    boolean contains = checkedIds.contains(mList.get(position).id);
                    if (contains) {
                        checkedIds.remove(mList.get(position).id);
                    } else {
                        checkedIds.add(mList.get(position).id);
                    }

                    listener.onCheck(pageType, !contains, mList.get(position));
                    notifyDataSetChanged();
                }
            });

            viewHolder.itemView.setOnClickListener(view -> context.startActivityForResult(AuthorNewsListActivity.getIntent(context, mList.get(position).id, mList.get(position).title), Constants.REQ_CODE_BOTTOM_NAV));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    void setCheckList(ArrayList<String> list) {
        checkedIds.addAll(list);
        notifyDataSetChanged();
    }

    public void addList(ArrayList<AuthorInfo> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameTv;
        CheckBox checkBox;
        ImageView picIv;

        ViewHolder(View itemView) {
            super(itemView);

            nameTv = itemView.findViewById(R.id.name_tv);
            checkBox = itemView.findViewById(R.id.checkbox);
            picIv = itemView.findViewById(R.id.ivProfile);
        }
    }
}
