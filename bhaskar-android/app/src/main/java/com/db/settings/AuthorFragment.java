package com.db.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.util.LoginController;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.database.AuthorListTable;
import com.db.data.database.DatabaseHelper;
import com.db.data.models.AuthorInfo;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.db.util.AppPreferences;
import com.bhaskar.util.CommonConstants;
import com.db.util.QuickPreferences;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;

public class AuthorFragment extends Fragment implements AuthorAdapter.OnAuthorCheckedListener {

    public static final int AUTHOR_LIST_FOLLOWED = 1;
    public static final int AUTHOR_LIST_ALL = 2;
    private OnFragUpdateListener onUpdateListener;
    private AuthorAdapter adapter;
    private int type;
    private LinearLayout followButtonLayout;
    private Button followButton;

    public static AuthorFragment newInstance(int type) {
        AuthorFragment authorFragment = new AuthorFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        authorFragment.setArguments(bundle);

        return authorFragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onUpdateListener = (OnFragUpdateListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null) {
            type = arguments.getInt("type");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_author, container, false);

        followButtonLayout = view.findViewById(R.id.follow_button_layout);
        followButton = view.findViewById(R.id.follow_button);

        SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setEnabled(false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));

        adapter = new AuthorAdapter(getActivity(), type, this);
        recyclerView.setAdapter(adapter);
        if (type == AUTHOR_LIST_FOLLOWED) {
            fetchDataFromOffline();
        } else {
            fetchDataFromServer();
        }

        return view;
    }

    private void fetchDataFromOffline() {
        AuthorListTable authorListTable = (AuthorListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(AuthorListTable.TABLE_NAME);
        ArrayList<AuthorInfo> authorList = authorListTable.getAuthorList();
        adapter.addList(authorList);

        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < authorList.size(); i++) {
            list.add(authorList.get(i).id);
        }
        adapter.setCheckList(list);

        if (authorList.size() > 0) {
            followButtonLayout.setVisibility(View.GONE);
        } else {
            followButtonLayout.setVisibility(View.VISIBLE);
            followButton.setOnClickListener(view -> {
                if (onUpdateListener != null) {
                    onUpdateListener.onSwitchFragment(1);
                }
            });
        }
    }

    private void fetchDataFromServer() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, String.format(Urls.AUTHOR_LIST_URL, CommonConstants.CHANNEL_ID), null, response -> {
            Systr.println("Author Response : " + response.toString());

            JSONArray data = response.optJSONArray("data");
            if (data != null) {
                ArrayList<AuthorInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(data.toString(), AuthorInfo[].class)));
                adapter.addList(list);

                AuthorListTable authorListTable = (AuthorListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(AuthorListTable.TABLE_NAME);
                ArrayList<String> checkedList = authorListTable.getAuthorIdsList();
                adapter.setCheckList(checkedList);
            }
        }, error -> {

        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(15000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }


    @Override
    public void onCheck(int type, boolean isChecked, AuthorInfo info) {
        AuthorListTable authorListTable = (AuthorListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(AuthorListTable.TABLE_NAME);
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        if (isChecked) {
            authorListTable.addAuthor(info);
            Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.AUTHORS, CTConstant.PROP_VALUE.FOLLOW, info.name, campaign);
            CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.AUTHORS, CTConstant.PROP_NAME.STATUS, CTConstant.PROP_VALUE.FOLLOW, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.AUTHOR_NAME, info.name));
        } else {
            authorListTable.deleteAuthor(info);
            Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.AUTHORS, CTConstant.PROP_VALUE.UNFOLLOW, info.name, campaign);
            CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.AUTHORS, CTConstant.PROP_NAME.STATUS, CTConstant.PROP_VALUE.UNFOLLOW, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.AUTHOR_NAME, info.name));
        }

        if (type == AUTHOR_LIST_FOLLOWED) {
            refreshPage(type);
            if (onUpdateListener != null) {
                onUpdateListener.onFragUpdate(AUTHOR_LIST_ALL, 1);
            }
        } else {
            if (onUpdateListener != null) {
                onUpdateListener.onFragUpdate(AUTHOR_LIST_FOLLOWED, 0);
            }
        }
    }

    public void refreshPage(int type) {
        adapter.clearCheckList();
        if (type == AUTHOR_LIST_FOLLOWED) {
            fetchDataFromOffline();
        } else {
            AuthorListTable authorListTable = (AuthorListTable) DatabaseHelper.getInstance(getActivity()).getTableObject(AuthorListTable.TABLE_NAME);
            ArrayList<String> checkedList = authorListTable.getAuthorIdsList();
            adapter.setCheckList(checkedList);
        }
    }
}
