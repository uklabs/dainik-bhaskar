package com.db.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.home.CommonListAdapter;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.Systr;
import com.db.util.Urls;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class AuthorNewsListActivity extends BaseAppCompatActivity {

    private CommonListAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    private final static int visibleThreshold = 5;
    private boolean isLoading;
    private int LOAD_MORE_COUNT = 1;

    private ArrayList<NewsListInfo> mList;

    private String feedurl;
    private String finalFeedURL;

    private String title;
    private String id;

    public static Intent getIntent(Context context, String id, String title) {
        Intent intent = new Intent(context, AuthorNewsListActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("title", title);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author_newslist);

        mList = new ArrayList<>();
        feedurl = Urls.APP_FEED_BASE_URL + String.format(Urls.DEFAULT_LIST_URL, CommonConstants.CHANNEL_ID);

        Intent intent = getIntent();
        if (intent != null) {
            id = intent.getStringExtra("id");
            title = intent.getStringExtra("title");
        }

        feedurl += id + "/1/";

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(AppUtils.fetchAttributeColor(this, R.attr.toolbarIconColor));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> AuthorNewsListActivity.this.finish());

        try {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back_black);
            getSupportActionBar().setTitle(null);

            TextView tvTitle = findViewById(R.id.toolbar_title);
            tvTitle.setTextColor(Color.BLACK);
            tvTitle.setText(TextUtils.isEmpty(title) ? "Author" : title);
        } catch (Exception ignored) {
        }

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        CategoryInfo categoryInfo = new CategoryInfo();
        categoryInfo.gaArticle = AppFlyerConst.GAScreen.AUTHOR_GA_ARTICLE;
        adapter = new CommonListAdapter(this, "", "", categoryInfo);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (NetworkStatus.getInstance().isConnected(AuthorNewsListActivity.this)) {
                    if (dy > 0) {
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            LOAD_MORE_COUNT++;
                            onLoadMore();
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(@NotNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        fetchData();
    }

    private void fetchData() {
        finalFeedURL = feedurl + "PG" + LOAD_MORE_COUNT + "/";
        fetchDataFromServer(true);
    }

    private void onLoadMore() {
        isLoading = true;

        new Handler().postDelayed(() -> {
            finalFeedURL = AppUtils.getInstance().makeUrlForLoadMore(feedurl, LOAD_MORE_COUNT);
            fetchDataFromServer(false);
        }, 1000);
    }

    private void fetchDataFromServer(boolean isClear) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, finalFeedURL, null, response -> {
            Systr.println("Author newslist Response : " + response.toString());

            try {
                JSONArray data = response.getJSONArray(Constants.KeyPair.KEY_FEED);
                if (data != null) {
                    if (isClear) {
                        mList.clear();
                    }
                    mList.addAll(new ArrayList<>(Arrays.asList(new Gson().fromJson(data.toString(), NewsListInfo[].class))));
                    adapter.setData(mList, String.format(Urls.DEFAULT_DETAIL_URL, CommonConstants.CHANNEL_ID));
                    adapter.notifyDataSetChanged();


                    isLoading = false;
                    LOAD_MORE_COUNT++;
                }
            } catch (Exception ignored) {
            }
        }, error -> isLoading = false);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(15000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if( requestCode==Constants.REQ_CODE_BOTTOM_NAV){
            setResult(Activity.RESULT_OK);
            finish();
        }
    }
}
