package com.db.settings;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bhaskar.appscommon.tracking.OnRequestListener;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.atracker.ATracker;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.util.LoginController;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JobUtil;
import com.db.util.Notificationutil;
import com.db.util.QuickPreferences;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class NotificationSettingFragment extends Fragment implements View.OnClickListener, View.OnTouchListener, TimePickerDialog.OnTimeSetListener {

    private View mView;
    private TextView tvFromTime, tvToTime;
    private boolean IsClickOnFrom = false;

    public static NotificationSettingFragment getInstance() {
        return new NotificationSettingFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GAScreen.SETTINGS_NOTIFICATION, source, medium, campaign);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_notification_setting, container, false);
        initializeViews();
        setClickListener();
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intializeAllView();
    }

    private void setClickListener() {
        mView.findViewById(R.id.rl_notificationStack).setOnClickListener(this);
        mView.findViewById(R.id.rl_do_not_disturb).setOnClickListener(this);
        mView.findViewById(R.id.rl_sound).setOnClickListener(this);
        mView.findViewById(R.id.rl_vibrate).setOnClickListener(this);
        mView.findViewById(R.id.notification_rl).setOnClickListener(this);
        mView.findViewById(R.id.important_rl).setOnClickListener(this);
        mView.findViewById(R.id.all_notifications_rl).setOnClickListener(this);
        mView.findViewById(R.id.morning_brief_rl).setOnClickListener(this);
        mView.findViewById(R.id.evening_roundup_rl).setOnClickListener(this);
        tvFromTime.setOnTouchListener(this);
        tvToTime.setOnTouchListener(this);
    }

    private void initializeViews() {
        tvFromTime = mView.findViewById(R.id.tv_from);
        tvToTime = mView.findViewById(R.id.tv_to);

        CardView notRcvNotification = mView.findViewById(R.id.not_receiving_notification);
        if (JobUtil.getNotificationIssueDevice(getActivity())) {
            notRcvNotification.setVisibility(View.VISIBLE);

            notRcvNotification.setOnClickListener(view -> {
                JobUtil.autoStart(getActivity());

                String campaign = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
                Tracking.trackGAEvent(getActivity(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, AppFlyerConst.GAAction.CLICK, AppFlyerConst.GALabel.NOT_RECEIVE_NOTIFICATION, campaign);
            });

        } else {
            notRcvNotification.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void sendTracking(int status, String gaAction) {
        /*Tracking*/
        String campaign = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
        @ATracker.ToggleStatus final int nValue = (status == ATracker.ToggleStatus.ON) ? ATracker.ToggleStatus.OFF : ATracker.ToggleStatus.ON;
        String label = (nValue == ATracker.ToggleStatus.ON) ? AppFlyerConst.GALabel.ON : AppFlyerConst.GALabel.OFF;
        Tracking.trackGAEvent(getActivity(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, gaAction, label, campaign);
    }

    @Override
    public void onClick(View v) {
        String campaign = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
        @ATracker.ToggleStatus int notificationValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);
        switch (v.getId()) {
            case R.id.notification_rl:
                v.setClickable(false);
                sendTracking(notificationValue, AppFlyerConst.GAAction.TOGGLE_NOTIFICATION);
                notificationToggle(R.id.notification_rl);
                CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.NOTIFICATION_STATUS, CTConstant.PROP_NAME.STATUS, (notificationValue == ATracker.ToggleStatus.ON) ? CTConstant.PROP_VALUE.OFF : CTConstant.PROP_VALUE.ON, LoginController.getUserDataMap());
                break;

            case R.id.all_notifications_rl:
                if (notificationValue == ATracker.ToggleStatus.ON) {
                    @ATracker.ToggleStatus int allNotificationValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.ALL_NOTIFICATION_STATUS, Notificationutil.NOTIFICATION_DEFAULT_VALUE);
                    if (allNotificationValue == ATracker.ToggleStatus.ON) {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.ALL_NOTIFICATION_STATUS, ATracker.ToggleStatus.OFF);
                        ((SwitchCompat) mView.findViewById(R.id.all_notifications_toggle)).setChecked(false);
                    } else {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.ALL_NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);
                        ((SwitchCompat) mView.findViewById(R.id.all_notifications_toggle)).setChecked(true);
                    }
                    sendTracking(allNotificationValue, AppFlyerConst.GAAction.TOGGLE_ALL);
                    notificationToggle(R.id.all_notifications_rl);
                }
                break;
            case R.id.important_rl:
                @ATracker.ToggleStatus int impNotificationValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.IMPORTANT_NOTIFICATION_STATUS, Notificationutil.NOTIFICATION_DEFAULT_VALUE);
                AppUtils.getInstance().showCustomToast(getContext(), "You can't turn off important Notifications");
                //sendTracking(impNotificationValue, AppFlyerConst.GAAction.TOGGLE_IMPORTANT);
                break;
            case R.id.morning_brief_rl:
                if (notificationValue == ATracker.ToggleStatus.ON) {
                    @ATracker.ToggleStatus int morningBriefValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.MORNING_BRIEF_STATUS, Notificationutil.NOTIFICATION_DEFAULT_VALUE);
                    if (morningBriefValue == ATracker.ToggleStatus.ON) {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.MORNING_BRIEF_STATUS, ATracker.ToggleStatus.OFF);
                        ((SwitchCompat) mView.findViewById(R.id.morning_brief_toggle)).setChecked(false);
                    } else {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.MORNING_BRIEF_STATUS, ATracker.ToggleStatus.ON);
                        ((SwitchCompat) mView.findViewById(R.id.morning_brief_toggle)).setChecked(true);
                    }
                    changeTextColor(getContext(), morningBriefValue != ATracker.ToggleStatus.ON, mView.findViewById(R.id.morning_brief_tv));
                    sendTracking(morningBriefValue, AppFlyerConst.GAAction.TOGGLE_MORNINIG_BRIEF);
                }
                notificationToggle(R.id.morning_brief_rl);
                break;
            case R.id.evening_roundup_rl:
                if (notificationValue == ATracker.ToggleStatus.ON) {
                    @ATracker.ToggleStatus int eveningRoundupValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.EVENING_ROUNDUP_STATUS, Notificationutil.NOTIFICATION_DEFAULT_VALUE);
                    if (eveningRoundupValue == ATracker.ToggleStatus.ON) {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.EVENING_ROUNDUP_STATUS, ATracker.ToggleStatus.OFF);
                        ((SwitchCompat) mView.findViewById(R.id.evening_roundup_toggle)).setChecked(false);
                    } else {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.EVENING_ROUNDUP_STATUS, ATracker.ToggleStatus.ON);
                        ((SwitchCompat) mView.findViewById(R.id.evening_roundup_toggle)).setChecked(true);
                    }
                    changeTextColor(getContext(), eveningRoundupValue != ATracker.ToggleStatus.ON, mView.findViewById(R.id.evening_roundup_tv));
                    sendTracking(eveningRoundupValue, AppFlyerConst.GAAction.TOGGLE_EVENING_ROUNDUP);
                }
                notificationToggle(R.id.evening_roundup_rl);
                break;
            case R.id.rl_notificationStack:
                @ATracker.ToggleStatus int notificationStackValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.NOTIFICATION_NEW_STACK_STATUS, Notificationutil.NOTIFICATION_DEFAULT_VALUE);
                if (notificationValue == ATracker.ToggleStatus.ON) {
                    if (notificationStackValue == ATracker.ToggleStatus.ON) {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.NOTIFICATION_NEW_STACK_STATUS, ATracker.ToggleStatus.OFF);
                        ((SwitchCompat) mView.findViewById(R.id.sc_notificationStack)).setChecked(false);
                    } else {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.NOTIFICATION_NEW_STACK_STATUS, ATracker.ToggleStatus.ON);
                        ((SwitchCompat) mView.findViewById(R.id.sc_notificationStack)).setChecked(true);
                    }
                    sendTracking(notificationStackValue, AppFlyerConst.GAAction.TOGGLE_STACK);
                }

                break;
            case R.id.rl_do_not_disturb:
                if (notificationValue == ATracker.ToggleStatus.ON) {
                    @ATracker.ToggleStatus int doNotDisturbValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.DO_NOT_DISTURB_STATUS, ATracker.ToggleStatus.OFF);
                    if (doNotDisturbValue == ATracker.ToggleStatus.ON) {
                        mView.findViewById(R.id.dnd_layout).setVisibility(View.GONE);
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.DO_NOT_DISTURB_STATUS, ATracker.ToggleStatus.OFF);
                        ((SwitchCompat) mView.findViewById(R.id.sc_do_not_disturb)).setChecked(false);
                    } else {
                        mView.findViewById(R.id.dnd_layout).setVisibility(View.VISIBLE);
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.DO_NOT_DISTURB_STATUS, ATracker.ToggleStatus.ON);
                        ((SwitchCompat) mView.findViewById(R.id.sc_do_not_disturb)).setChecked(true);
                    }
                    sendTracking(doNotDisturbValue, AppFlyerConst.GAAction.DND);
                    CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.DND, CTConstant.PROP_NAME.STATUS, (doNotDisturbValue == ATracker.ToggleStatus.ON) ? CTConstant.PROP_VALUE.OFF : CTConstant.PROP_VALUE.ON, LoginController.getUserDataMap());
                }
                break;

            case R.id.rl_sound:
                @ATracker.ToggleStatus int soundValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.SOUND_STATUS, ATracker.ToggleStatus.ON);
                if (notificationValue == ATracker.ToggleStatus.ON) {
                    if (soundValue == ATracker.ToggleStatus.ON) {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.SOUND_STATUS, ATracker.ToggleStatus.OFF);
                        ((SwitchCompat) mView.findViewById(R.id.sc_sound)).setChecked(false);
                    } else {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.SOUND_STATUS, ATracker.ToggleStatus.ON);
                        ((SwitchCompat) mView.findViewById(R.id.sc_sound)).setChecked(true);
                    }
                    sendTracking(soundValue, AppFlyerConst.GAAction.SOUND);
                }

                break;

            case R.id.rl_vibrate:
                if (notificationValue == ATracker.ToggleStatus.ON) {
                    @ATracker.ToggleStatus int vibrateValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.VIBRATE_STATUS, ATracker.ToggleStatus.OFF);
                    if (vibrateValue == ATracker.ToggleStatus.ON) {

                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.VIBRATE_STATUS, ATracker.ToggleStatus.OFF);
                        ((SwitchCompat) mView.findViewById(R.id.sc_vibrate)).setChecked(false);
                        Tracking.trackGAEvent(getActivity(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, AppFlyerConst.GAAction.VIBRATE, AppFlyerConst.GALabel.OFF, campaign);

                    } else {
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.VIBRATE_STATUS, ATracker.ToggleStatus.ON);
                        ((SwitchCompat) mView.findViewById(R.id.sc_vibrate)).setChecked(true);
                        Tracking.trackGAEvent(getActivity(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, AppFlyerConst.GAAction.VIBRATE, AppFlyerConst.GALabel.ON, campaign);
                    }
                }

                break;
        }
    }


    private void notificationToggle(final int id) {
        final String campaign = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
        String source = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_SOURCE, QuickPreferences.UTM_DIRECT);
        String medium = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_MEDIUM, QuickPreferences.UTM_DIRECT);

        @ATracker.ToggleStatus int notificationValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);

        String allValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.ALL_NOTIFICATION_STATUS, ATracker.ToggleStatus.ON) + "";
        String morningValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.MORNING_BRIEF_STATUS, ATracker.ToggleStatus.ON) + "";
        String eveningValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.EVENING_ROUNDUP_STATUS, ATracker.ToggleStatus.ON) + "";

        if (id == R.id.notification_rl) {
            notificationValue = (notificationValue == ATracker.ToggleStatus.ON) ? ATracker.ToggleStatus.OFF : ATracker.ToggleStatus.ON;
            allValue = notificationValue + "";
            morningValue = notificationValue + "";
            eveningValue = notificationValue + "";
        }

        String url = TrackingHost.getInstance(getActivity()).getToggleNotifyUrl(AppUrls.TOGGLE_NOTIFY_URL);
        @ATracker.ToggleStatus final int nValue = notificationValue;
        Tracking.notifyToggle(getActivity(), CommonConstants.BHASKAR_APP_ID, url, nValue, ATracker.ToggleType.OTHER, source, medium, campaign, new OnRequestListener() {
            @Override
            public void onSuccess() {
                if (id == R.id.notification_rl) {
                    AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.NOTIFICATION_STATUS, nValue);
                    ((SwitchCompat) mView.findViewById(R.id.notification_switch_button)).setChecked(nValue == ATracker.ToggleStatus.ON);
                    mView.findViewById(R.id.notification_rl).setClickable(true);
                    if (nValue == ATracker.ToggleStatus.OFF) {
                        resetAllValueToDefault(false);
                    } else {
                        resetAllValueToDefault(true);
                        switch (id) {
                            /*More Settings*/
                            case R.id.rl_notificationStack:
                                AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.NOTIFICATION_NEW_STACK_STATUS, ATracker.ToggleStatus.ON);
                                ((SwitchCompat) mView.findViewById(R.id.sc_notificationStack)).setChecked(true);
                                break;

                            case R.id.rl_do_not_disturb:
                                AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.DO_NOT_DISTURB_STATUS, ATracker.ToggleStatus.ON);
                                ((SwitchCompat) mView.findViewById(R.id.sc_do_not_disturb)).setChecked(true);
                                break;

                            case R.id.rl_sound:
                                AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.SOUND_STATUS, ATracker.ToggleStatus.ON);
                                ((SwitchCompat) mView.findViewById(R.id.sc_sound)).setChecked(true);
                                break;

                            case R.id.rl_vibrate:
                                AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.VIBRATE_STATUS, ATracker.ToggleStatus.ON);
                                ((SwitchCompat) mView.findViewById(R.id.sc_vibrate)).setChecked(true);
                                break;
                        }
                    }
                }

            }

            @Override
            public void onFailed(String message) {
                Activity activity = getActivity();
                if (activity != null && isAdded()) {
                    mView.findViewById(R.id.notification_rl).setClickable(true);
                    AppUtils.getInstance().showCustomToast(activity, message);
                }
            }
        }, allValue, "1", morningValue, eveningValue);
    }

    private void resetAllValueToDefault(Boolean isNotificationActive) {

        if (isNotificationActive) {
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.ALL_NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.IMPORTANT_NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.MORNING_BRIEF_STATUS, ATracker.ToggleStatus.ON);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.EVENING_ROUNDUP_STATUS, ATracker.ToggleStatus.ON);

            //Switch Button
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.NOTIFICATION_NEW_STACK_STATUS, ATracker.ToggleStatus.ON);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.DO_NOT_DISTURB_STATUS, ATracker.ToggleStatus.OFF);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.SOUND_STATUS, ATracker.ToggleStatus.ON);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.VIBRATE_STATUS, ATracker.ToggleStatus.OFF);
            //Spinner
            //DND From and To time
            AppPreferences.getInstance(getActivity()).setStringValue(QuickPreferences.DO_NOT_DISTURB_FROM_TIME, Constants.DefaultSettingValue.DEFAULT_FROM_TIME);
            AppPreferences.getInstance(getActivity()).setStringValue(QuickPreferences.DO_NOT_DISTURB_TO_TIME, Constants.DefaultSettingValue.DEFAULT_TO_TIME);

        } else {
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.ALL_NOTIFICATION_STATUS, ATracker.ToggleStatus.OFF);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.IMPORTANT_NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.MORNING_BRIEF_STATUS, ATracker.ToggleStatus.OFF);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.EVENING_ROUNDUP_STATUS, ATracker.ToggleStatus.OFF);

            //Switch Button
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.NOTIFICATION_NEW_STACK_STATUS, ATracker.ToggleStatus.OFF);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.DO_NOT_DISTURB_STATUS, ATracker.ToggleStatus.OFF);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.SOUND_STATUS, ATracker.ToggleStatus.OFF);
            AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.VIBRATE_STATUS, ATracker.ToggleStatus.OFF);
            //Spinner
            //DND From and To time
            AppPreferences.getInstance(getActivity()).setStringValue(QuickPreferences.DO_NOT_DISTURB_FROM_TIME, Constants.DefaultSettingValue.DEFAULT_FROM_TIME);
            AppPreferences.getInstance(getActivity()).setStringValue(QuickPreferences.DO_NOT_DISTURB_TO_TIME, Constants.DefaultSettingValue.DEFAULT_TO_TIME);
        }
        intializeAllView();
    }

    private void intializeAllView() {
        Context context = getContext();
        if (context != null) {
            setToggle(context);
            initializeFromAndToTime();
        }
    }

    private void setToggle(Context context) {
        int notifyToggle = AppPreferences.getInstance(context).getIntValue(QuickPreferences.NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);
        ((SwitchCompat) mView.findViewById(R.id.notification_switch_button)).setChecked(notifyToggle == ATracker.ToggleStatus.ON);
        int colorCode;

        // ============
        if (notifyToggle == 1) {
            //Switch Button
            /*ALl*/
            int allNotiStatus = AppPreferences.getInstance(context).getIntValue(QuickPreferences.ALL_NOTIFICATION_STATUS, Notificationutil.NOTIFICATION_DEFAULT_VALUE);
            ((SwitchCompat) mView.findViewById(R.id.all_notifications_toggle)).setChecked(allNotiStatus == ATracker.ToggleStatus.ON);

            /*Morning*/
            int MorningBriefStatus = AppPreferences.getInstance(context).getIntValue(QuickPreferences.MORNING_BRIEF_STATUS, Notificationutil.NOTIFICATION_DEFAULT_VALUE);
            ((SwitchCompat) mView.findViewById(R.id.morning_brief_toggle)).setChecked(MorningBriefStatus == ATracker.ToggleStatus.ON);

            /*Evening*/
            int EveningRoundupStatus = AppPreferences.getInstance(context).getIntValue(QuickPreferences.EVENING_ROUNDUP_STATUS, Notificationutil.NOTIFICATION_DEFAULT_VALUE);
            ((SwitchCompat) mView.findViewById(R.id.evening_roundup_toggle)).setChecked(EveningRoundupStatus == ATracker.ToggleStatus.ON);

            /*Stack from baseappcontrol chaeck*/
            boolean stackNotification = Notificationutil.getNotificationStackStatus(getActivity());
            ((SwitchCompat) mView.findViewById(R.id.sc_notificationStack)).setChecked(stackNotification);

            /*DND*/
            int doNotDisturb = AppPreferences.getInstance(context).getIntValue(QuickPreferences.DO_NOT_DISTURB_STATUS, ATracker.ToggleStatus.OFF);
            if (doNotDisturb == ATracker.ToggleStatus.ON) {
                mView.findViewById(R.id.dnd_layout).setVisibility(View.VISIBLE);
            } else {
                mView.findViewById(R.id.dnd_layout).setVisibility(View.GONE);
            }
            ((SwitchCompat) mView.findViewById(R.id.sc_do_not_disturb)).setChecked(doNotDisturb == ATracker.ToggleStatus.ON);

            /*Sound*/
            int sound = AppPreferences.getInstance(context).getIntValue(QuickPreferences.SOUND_STATUS, ATracker.ToggleStatus.ON);
            ((SwitchCompat) mView.findViewById(R.id.sc_sound)).setChecked(sound == ATracker.ToggleStatus.ON);

            /*Vibrate*/
            int vibrate = AppPreferences.getInstance(context).getIntValue(QuickPreferences.VIBRATE_STATUS, ATracker.ToggleStatus.OFF);
            ((SwitchCompat) mView.findViewById(R.id.sc_vibrate)).setChecked(vibrate == ATracker.ToggleStatus.ON);

            /*Text Color*/
            colorCode = R.color.notification_text_color_small;
        } else {

            /*ALl*/
            ((SwitchCompat) mView.findViewById(R.id.all_notifications_toggle)).setChecked(false);

            /*Morning*/
            ((SwitchCompat) mView.findViewById(R.id.morning_brief_toggle)).setChecked(false);

            /*Evening*/
            ((SwitchCompat) mView.findViewById(R.id.evening_roundup_toggle)).setChecked(false);

            /*Stack*/
            ((SwitchCompat) mView.findViewById(R.id.sc_notificationStack)).setChecked(false);

            /*DND*/
            ((SwitchCompat) mView.findViewById(R.id.sc_do_not_disturb)).setChecked(false);
            mView.findViewById(R.id.dnd_layout).setVisibility(View.GONE);

            /*Sound*/
            ((SwitchCompat) mView.findViewById(R.id.sc_sound)).setChecked(false);

            /*Vibrate*/
            ((SwitchCompat) mView.findViewById(R.id.sc_vibrate)).setChecked(false);

            /*Text Color*/
            colorCode = R.color.notification_text_color_small_translucent;
        }

        ((TextView) mView.findViewById(R.id.all_notifications_tv)).setTextColor(ContextCompat.getColor(context, colorCode));
        ((TextView) mView.findViewById(R.id.morning_brief_tv)).setTextColor(ContextCompat.getColor(context, colorCode));
        ((TextView) mView.findViewById(R.id.evening_roundup_tv)).setTextColor(ContextCompat.getColor(context, colorCode));
        ((TextView) mView.findViewById(R.id.tv_notificationStack)).setTextColor(ContextCompat.getColor(context, colorCode));
        ((TextView) mView.findViewById(R.id.tv_do_not_disturb)).setTextColor(ContextCompat.getColor(context, colorCode));
        ((TextView) mView.findViewById(R.id.tv_from_title)).setTextColor(ContextCompat.getColor(context, colorCode));
        ((TextView) mView.findViewById(R.id.tv_to_title)).setTextColor(ContextCompat.getColor(context, colorCode));
        ((TextView) mView.findViewById(R.id.tv_sound)).setTextColor(ContextCompat.getColor(context, colorCode));
        ((TextView) mView.findViewById(R.id.tv_vibrate)).setTextColor(ContextCompat.getColor(context, colorCode));
    }

    private void initializeFromAndToTime() {
        String fromTime = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.DO_NOT_DISTURB_FROM_TIME, Constants.DefaultSettingValue.DEFAULT_FROM_TIME);
        tvFromTime.setText(fromTime);

        String toTime = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.DO_NOT_DISTURB_TO_TIME, Constants.DefaultSettingValue.DEFAULT_TO_TIME);
        tvToTime.setText(toTime);
    }


    private void changeTextColor(Context context, boolean isActive, TextView textView) {
        int colorCode = (isActive) ? R.color.notification_text_color_small : R.color.notification_text_color_small_translucent;
        textView.setTextColor(ContextCompat.getColor(context, colorCode));
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        int doNotDisturbToggle = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.DO_NOT_DISTURB_STATUS, ATracker.ToggleStatus.OFF);

        if ((doNotDisturbToggle == ATracker.ToggleStatus.ON)) {

            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    Date fromTime_12Hr = null;
                    Date toTime_12Hr = null;
                    SimpleDateFormat displayFormat = new SimpleDateFormat("h:mm aa");
                    try {
                        fromTime_12Hr = displayFormat.parse(tvFromTime.getText().toString().trim());
                        toTime_12Hr = displayFormat.parse(tvToTime.getText().toString().trim());
                    } catch (Exception ex) {
                    }

                    if (v.equals(tvFromTime)) {
                        tvFromTime.setError(null);
                        IsClickOnFrom = true;
                        SetCalender(DateToCalendar(fromTime_12Hr));
                    } else {
                        IsClickOnFrom = false;
                        tvToTime.setError(null);
                        SetCalender(DateToCalendar(toTime_12Hr));
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    break;
            }
            return true;
        } else {
            return true;
        }
    }

    public int SetCalender(Calendar cal) {

        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(this,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                false
        );

        if (IsClickOnFrom) {
            tpd.setTitle("FROM");
        } else {
            tpd.setTitle("TO");
        }
        tpd.setThemeDark(false);
        tpd.vibrate(true);
        tpd.dismissOnPause(true);
//        tpd.enableSeconds(true);
        tpd.enableMinutes(true);
        tpd.setAccentColor(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary));
//Limit Set
        tpd.setTimeInterval(1, 15, 10);

        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
            }
        });
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");

        return 1;
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;
        String time = hourString + ":" + minuteString + ":" + secondString;


        String time12hrformat = "";
        SimpleDateFormat inFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat outFormat = new SimpleDateFormat("h:mm aa");

        try {
            String campaign = AppPreferences.getInstance(getActivity()).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
            time12hrformat = outFormat.format(inFormat.parse(time));
            String time24HrsFormat = hourOfDay + ":" + minute + ":" + second;
            if (IsClickOnFrom) {

                tvFromTime.setText(time12hrformat);
                AppPreferences.getInstance(getActivity()).setStringValue(QuickPreferences.DO_NOT_DISTURB_FROM_TIME, time12hrformat);
//                Tracking.trackGAEvent(InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, AppFlyerConst.GAAction.DND_FROM, "" + time24HrsFormat, campaign);
            } else {
                if (time12hrformat.length() > 0)
                    tvToTime.setText(time12hrformat);
                AppPreferences.getInstance(getActivity()).setStringValue(QuickPreferences.DO_NOT_DISTURB_TO_TIME, time12hrformat);
//                Tracking.trackGAEvent(InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, AppFlyerConst.GAAction.DND_TO, "" + time24HrsFormat, campaign);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static Calendar DateToCalendar(Date date) {

        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return cal;
        } else {
            Calendar cal = Calendar.getInstance(Locale.getDefault());
            return cal;
        }

    }
}
