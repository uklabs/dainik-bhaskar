package com.db.settings;

public interface OnFragUpdateListener {
    void onFragUpdate(int type, int position);

    void onSwitchFragment(int position);
}
