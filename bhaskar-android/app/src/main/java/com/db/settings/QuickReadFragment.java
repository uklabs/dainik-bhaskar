package com.db.settings;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.main.QuickNewsNotificationService2;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.db.util.Constants;
import com.db.util.NetworkStatus;
import com.db.util.QuickPreferences;


public class QuickReadFragment extends Fragment {


    public static QuickReadFragment getInstance() {
        return new QuickReadFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GAScreen.SETTINGS_QUICKREAD, source, medium, campaign);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_quickread_setting, container, false);

        RelativeLayout quickread_lv = mView.findViewById(R.id.quickread_rl);
        SwitchCompat quick_read_toggle = mView.findViewById(R.id.quickread_switch_button);
        if (AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.QUICK_READ_STATUS, 0) == 1) {
            quick_read_toggle.setChecked(true);
            startNotificationIfClosed(getActivity());
        } else {
            NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(Constants.AppWidgetIDS.STICKY_WIDGET_ID);
            quick_read_toggle.setChecked(false);
        }

        quickread_lv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkToggleStatus = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.QUICK_READ_STATUS, 0);
                if (checkToggleStatus == 1) {
                    quick_read_toggle.setChecked(false);
                    stopStickyNotification();
                    AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.QUICK_READ_STATUS, 0);
                    AppPreferences.getInstance(getActivity()).setBooleanValue(QuickPreferences.QUICK_READ_STOP_BY_USER, true);

                    String label = AppFlyerConst.GALabel.OFF;
                    String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
                    Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, AppFlyerConst.GAAction.TOGGLE_STICKY, label, campaign);
                } else {
                    if (NetworkStatus.getInstance().isConnected(getActivity())) {
                        quick_read_toggle.setChecked(true);
                        startStickyNotification();
                        AppPreferences.getInstance(getActivity()).setIntValue(QuickPreferences.QUICK_READ_STATUS, 1);
                        String label = AppFlyerConst.GALabel.ON;
                        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
                        Tracking.trackGAEvent(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, AppFlyerConst.GAAction.TOGGLE_STICKY, label, campaign);

                    } else {
                        AppUtils.getInstance().showCustomToast(getActivity(), getActivity().getResources().getString(R.string.internet_connection_error_));
                    }
                }
            }
        });


        return mView;
    }


    private void startStickyNotification() {
        Intent intent = new Intent(getActivity(), QuickNewsNotificationService2.class);
        intent.putExtra("count", 0);
        intent.putExtra("paginationCount", 0);
        intent.setAction(Constants.QuickReadActions.ACTION_START);
        getActivity().startService(intent);

    }

    private void stopStickyNotification() {
        Intent intent = new Intent(getActivity(), QuickNewsNotificationService2.class);
        intent.putExtra("count", 0);
        intent.putExtra("paginationCount", 0);
        intent.setAction(Constants.QuickReadActions.ACTION_STOP);
        getActivity().startService(intent);

    }

    private void startNotificationIfClosed(Context context) {
        int countValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.QuickNotificationPrefs.COUNT, 0);
        int paginationValue = AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.QuickNotificationPrefs.PAGINATION_COUNT, 0);
        if (AppPreferences.getInstance(getActivity()).getIntValue(QuickPreferences.QUICK_READ_STATUS, 0) == 1) {
//            ResultReceiver resultReceiver = new MyResultReceiver(new Handler());
            Intent intent = new Intent(getActivity(), QuickNewsNotificationService2.class);
            intent.putExtra("count", countValue);
            intent.putExtra("paginationCount", paginationValue);
//            intent.putExtra("result_receiver",resultReceiver);
            intent.setAction(Constants.QuickReadActions.ACTION_NEXT);
            context.startService(intent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
