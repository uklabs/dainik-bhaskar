package com.db.settings;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.appintro.activities.AppIntroDashboardActivity;
import com.db.appintro.utils.ChatIntroConst;
import com.db.data.models.CategoryInfo;
import com.db.main.BaseAppCompatActivity;
import com.bhaskar.view.ui.myPage.FragmentDetailActivity;
import com.db.news.WebViewActivity;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.QuickPreferences;
import com.db.util.ThemeUtil;
import com.db.util.Urls;


public class SettingActivity extends BaseAppCompatActivity implements SettingCallbacks {

    private TextView tvTitle;
    private CategoryInfo mCategoryInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleExtras(getIntent());
        /*Set App Theme*/
        setTheme(ThemeUtil.getCurrentTheme(this));
        setContentView(R.layout.activity_setting);

        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(AppUtils.fetchAttributeColor(this, R.attr.toolbarIconColor));
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception ignored) {

        }
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ica_back_black);
        toolbar.setNavigationOnClickListener(v -> handleBackActivity());
        getSupportActionBar().setTitle(null);
        tvTitle = findViewById(R.id.toolbar_title);
        tvTitle.setTextColor(Color.BLACK);

        /*Set Fragment*/
        SettingV2Fragment fragment = SettingV2Fragment.getInstance(mCategoryInfo);
        fragment.setListener(SettingActivity.this);
        replaceFragment(fragment, false);

    }

    private void handleExtras(Intent intent) {
        if (intent != null) {
            mCategoryInfo = (CategoryInfo) intent.getSerializableExtra(Constants.KeyPair.KEY_CATEGORY_INFO);
        }
    }

    public void setHeaderTitle(String headerTitle) {
        tvTitle.setText(headerTitle);
    }

    private void replaceFragment(Fragment fragment, boolean isAddBackToStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.add(R.id.fragment_container, fragment);
        if (isAddBackToStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        }
        transaction.commitAllowingStateLoss();
    }


    private void handleBackActivity() {
        int fSize = getSupportFragmentManager().getBackStackEntryCount();
        if (fSize >= 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        handleBackActivity();
    }

    @Override
    public void onSettingClick(SettingInfo settingInfo) {
        String campaign = AppPreferences.getInstance(SettingActivity.this).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
        Fragment fragment = null;
        switch (settingInfo.settingAction) {
            case Constants.SettingAction.MERA_PAGE:
                startActivity(FragmentDetailActivity.getIntent(this, mCategoryInfo, true, Constants.SettingAction.MERA_PAGE));
                break;
            case Constants.SettingAction.NOTIFICATION:
                startActivity(FragmentDetailActivity.getIntent(this, mCategoryInfo, true, Constants.SettingAction.NOTIFICATION));
                break;
            case Constants.SettingAction.QUICK_READ:
                startActivity(FragmentDetailActivity.getIntent(this, mCategoryInfo, true, Constants.SettingAction.QUICK_READ));
                break;
            case Constants.SettingAction.AUTHOR_LIST:
                startActivityForResult(AuthorActivity.getIntent(this), Constants.REQ_CODE_BOTTOM_NAV);
                break;
            case Constants.SettingAction.VIDEO_SETTING:
                startActivity(FragmentDetailActivity.getIntent(this, mCategoryInfo, true, Constants.SettingAction.VIDEO_SETTING));
                break;
            case Constants.SettingAction.TUTORIALS:
                startActivity(AppIntroDashboardActivity.getIntent(this, ChatIntroConst.NavigationFlow.NAVIGATE_FROM_HOME, AppFlyerConst.GAScreen.SETTINGS_TUTORIAL));
                break;
            case Constants.SettingAction.SHARE_APP:
                Tracking.trackGAEvent(this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SOCIAL, AppFlyerConst.GAAction.CLICK, AppFlyerConst.GALabel.APP_SHARE, campaign);
                AppUtils.shareApp(SettingActivity.this);
                break;
            case Constants.SettingAction.FEEDBACK:
                Tracking.trackGAEvent(this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SOCIAL, AppFlyerConst.GAAction.CLICK, AppFlyerConst.GALabel.FEEDBACK, campaign);
                AppUtils.getInstance().sendEmail(SettingActivity.this, "");
                break;
            case Constants.SettingAction.RATE_US:
                Tracking.trackGAEvent(this, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.SOCIAL, AppFlyerConst.GAAction.CLICK, AppFlyerConst.GALabel.RATE_US, campaign);
                AppUtils.getInstance().openPlayStore(this);
                break;
            case Constants.SettingAction.ABOUT_US:
                Intent aboutUsIntent = new Intent(SettingActivity.this, WebViewActivity.class);
                aboutUsIntent.putExtra(Constants.KeyPair.KEY_URL, String.format(Urls.ABOUT_US_URL, CommonConstants.CHANNEL_ID));
                aboutUsIntent.putExtra(Constants.KeyPair.KEY_TITLE, settingInfo.settingName);
                aboutUsIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAScreen.ABOUT_US);
                startActivity(aboutUsIntent);
                break;
            case Constants.SettingAction.TERMS_AND_CONDITION:
                Intent privacyIntent = new Intent(SettingActivity.this, WebViewActivity.class);
                privacyIntent.putExtra(Constants.KeyPair.KEY_URL, String.format(Urls.TERMS_AND_CONDITION_URL, CommonConstants.CHANNEL_ID));
                privacyIntent.putExtra(Constants.KeyPair.KEY_TITLE, settingInfo.settingName);
                privacyIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAScreen.PRIVACY_POLICY);
                startActivity(privacyIntent);
                break;

        }
        if (null != fragment) {
            replaceFragment(fragment, true);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQ_CODE_BOTTOM_NAV) {
            setResult(RESULT_OK);
            finish();
        }

    }
}
