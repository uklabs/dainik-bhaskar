package com.db.settings;

public interface SettingCallbacks {
    void onSettingClick(SettingInfo settingInfo);
}
