package com.db.settings;

import java.util.Objects;

class SettingInfo {

    SettingInfo(String settingName, int settingIcon, String settingAction) {
        this.settingName = settingName;
        this.settingIcon = settingIcon;
        this.settingAction = settingAction;
    }

    String settingName;
    int settingIcon;
    String settingAction;


}
