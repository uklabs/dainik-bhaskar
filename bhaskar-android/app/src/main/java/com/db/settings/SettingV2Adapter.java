package com.db.settings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.atracker.ATracker;
import com.db.util.AppPreferences;
import com.db.util.Constants;
import com.db.util.QuickPreferences;

import java.util.List;

public class SettingV2Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<SettingInfo> mList;
    private SettingCallbacks settingCallbacks;
    private Context context;
    SettingV2Adapter(Context context, List<SettingInfo> mList, SettingCallbacks settingCallbacks) {
        this.mList = mList;
        this.context = context;
        this.settingCallbacks = settingCallbacks;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new SettingViewHolder(inflater.inflate(R.layout.setting_list_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SettingViewHolder) {
            SettingViewHolder settingViewHolder = (SettingViewHolder) holder;
            String text = mList.get(position).settingName;
            String action = mList.get(position).settingAction;


            if(action.equalsIgnoreCase(Constants.SettingAction.VERTICAL_SWIPE)){
                settingViewHolder.switchButton.setVisibility(View.VISIBLE);
                settingViewHolder.itemView.setOnClickListener(v->{
                    onVerticleSwipeClicked(settingViewHolder);
                });

                settingViewHolder.switchButton.setClickable(false);
            }else{
                settingViewHolder.ivRightArrow.setVisibility(View.VISIBLE);
                settingViewHolder.itemView.setOnClickListener(v -> {
                    if (null != settingCallbacks) {
                        settingCallbacks.onSettingClick(mList.get(position));
                    }
                });
            }
            settingViewHolder.settingName.setText(text);
            settingViewHolder.settingIcon.setImageResource(mList.get(position).settingIcon);

            boolean isVertical = AppPreferences.getInstance(context).getBooleanValue(Constants.KeyPair.KEY_IS_VERTICAL,true);
            settingViewHolder.switchButton.setChecked(isVertical);
        }

    }

    private void onVerticleSwipeClicked(SettingViewHolder settingViewHolder){
        boolean isChecked = settingViewHolder.switchButton.isChecked();
        settingViewHolder.switchButton.setChecked(!isChecked);
        if(isChecked){
            AppPreferences.getInstance(context).setBooleanValue(Constants.KeyPair.KEY_IS_VERTICAL,false);

        }else{
            AppPreferences.getInstance(context).setBooleanValue(Constants.KeyPair.KEY_IS_VERTICAL,true);

        }
        sendTracking((isChecked?0:1), AppFlyerConst.GAAction.TOGGLE_VERTICAL_SWIPE);
    }

    private void sendTracking(int status, String gaAction) {
        /*Tracking*/
        String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, QuickPreferences.UTM_DIRECT);
        @ATracker.ToggleStatus final int nValue = (status == ATracker.ToggleStatus.ON) ? ATracker.ToggleStatus.ON : ATracker.ToggleStatus.OFF;
        String label = (nValue == ATracker.ToggleStatus.ON) ? AppFlyerConst.GALabel.ON : AppFlyerConst.GALabel.OFF;
        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.NOTIFICATION, gaAction, label, campaign);
    }



    @Override
    public int getItemCount() {
        return mList.size();
    }

    class SettingViewHolder extends RecyclerView.ViewHolder {
        private TextView settingName;
        private ImageView settingIcon;
        private ImageView ivRightArrow;
        private Switch switchButton;

        SettingViewHolder(View itemView) {
            super(itemView);
            settingName = itemView.findViewById(R.id.setting_name);
            settingIcon = itemView.findViewById(R.id.setting_icon);
            ivRightArrow = itemView.findViewById(R.id.right_arrow);
            switchButton = itemView.findViewById(R.id.switchButton);
            itemView.setTag(itemView);
        }



    }
}
