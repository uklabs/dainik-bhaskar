package com.db.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.bhaskar.util.Action;
import com.db.util.AppPreferences;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.Constants;
import com.db.util.JsonParser;
import com.db.util.QuickPreferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class SettingV2Fragment extends Fragment {

    private List<SettingInfo> settingList = new ArrayList<>();
    private SettingCallbacks settingCallbacks;
    private CategoryInfo categoryInfo;

    public static SettingV2Fragment getInstance(CategoryInfo categoryInfo) {
        SettingV2Fragment fragment = new SettingV2Fragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KeyPair.KEY_CATEGORY_INFO,categoryInfo);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null){
            categoryInfo = (CategoryInfo) bundle.getSerializable(Constants.KeyPair.KEY_CATEGORY_INFO);
        }

        String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GAScreen.SETTINGS, source, medium, campaign);
        settingList = SettingsHelper.getSettingsHelper().getAllSettingList(getContext(), CommonConstants.CHANNEL_ID);
        /*My Page Check*/
        if (JsonParser.getInstance().isCatActionAvailable(getContext(), Action.CategoryAction.CAT_ACTION_MERA_PAGE)) {
//            //insert my page
            settingList.add(1,new SettingInfo(getString(R.string.setting_mera_page), R.drawable.ic_settings_mera_page, Constants.SettingAction.MERA_PAGE));
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_setting_v2, container, false);

        TextView dbId = mView.findViewById(R.id.tv_db_id);
        TextView appVersion = mView.findViewById(R.id.tv_app_version);
        dbId.setText(AppUtils.getInstance().fromHtml(getString(R.string.setting_db_id, TrackingData.getDBId(getActivity())).trim()));
        appVersion.setText(AppUtils.getInstance().fromHtml(getString(R.string.app_version_str, TrackingUtils.getAppVersion(getContext()))));

        RecyclerView settingsRecyclerView = mView.findViewById(R.id.settingRv);
        settingsRecyclerView.setNestedScrollingEnabled(false);
        settingsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        settingsRecyclerView.setAdapter(new SettingV2Adapter(getContext(),settingList, settingCallbacks));
        return mView;
    }

    public void setListener(SettingCallbacks settingCallbacks) {
        this.settingCallbacks = settingCallbacks;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(categoryInfo!=null)
            ((SettingActivity) Objects.requireNonNull(getActivity())).setHeaderTitle(categoryInfo.menuName);
    }
}
