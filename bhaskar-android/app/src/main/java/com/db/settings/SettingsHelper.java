package com.db.settings;

import android.content.Context;

import com.bhaskar.R;
import com.db.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class SettingsHelper {
    private static SettingsHelper settingsHelper;

    public static SettingsHelper getSettingsHelper() {
        if (null == settingsHelper) {
            settingsHelper = new SettingsHelper();
        }
        return settingsHelper;
    }


    public List<SettingInfo> getAllSettingList(Context context, String appId) {
        List<SettingInfo> settingInfoList = new ArrayList<>();
        settingInfoList.add(new SettingInfo(context.getString(R.string.settings_vertical_swipe), R.drawable.ic_vertical_swipe, Constants.SettingAction.VERTICAL_SWIPE));
        settingInfoList.add(new SettingInfo(context.getString(R.string.setting_notification), R.drawable.ic_settings_notification, Constants.SettingAction.NOTIFICATION));
        settingInfoList.add(new SettingInfo(context.getString(R.string.quick_read), R.drawable.ic_settings_quick_reads, Constants.SettingAction.QUICK_READ));
        if (appId.equalsIgnoreCase(Constants.AppIds.DIVYA_BHASKAR)) {
            settingInfoList.add(new SettingInfo(context.getString(R.string.setting_author_list), R.drawable.ic_settings_author_list, Constants.SettingAction.AUTHOR_LIST));
        }
        settingInfoList.add(new SettingInfo(context.getString(R.string.setting_video_setting), R.drawable.ic_settings_video_settings, Constants.SettingAction.VIDEO_SETTING));
        settingInfoList.add(new SettingInfo(context.getString(R.string.setting_tutorials), R.drawable.ic_settings_tutorials, Constants.SettingAction.TUTORIALS));
        settingInfoList.add(new SettingInfo(context.getString(R.string.setting_share_app), R.drawable.ic_settings_share_app, Constants.SettingAction.SHARE_APP));
        settingInfoList.add(new SettingInfo(context.getString(R.string.setting_feedback), R.drawable.ic_settings_feedback, Constants.SettingAction.FEEDBACK));
        settingInfoList.add(new SettingInfo(context.getString(R.string.setting_rateus), R.drawable.ic_settings_rate_us, Constants.SettingAction.RATE_US));
        settingInfoList.add(new SettingInfo(context.getString(R.string.setting_about_us), R.drawable.ic_settings_about_us, Constants.SettingAction.ABOUT_US));
        settingInfoList.add(new SettingInfo(context.getString(R.string.setting_terms), R.drawable.ic_settings_terms_conditions, Constants.SettingAction.TERMS_AND_CONDITION));
        return settingInfoList;
    }
}
