package com.db.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import com.db.InitApplication;
import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.AppPreferences;
import com.db.util.QuickPreferences;


public class VideoSettingFragment extends Fragment {


    public static VideoSettingFragment getInstance() {
        return new VideoSettingFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String source = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_SOURCE, "");
        String medium = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_MEDIUM, "");
        String campaign = AppPreferences.getInstance(getContext()).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAScreen(getContext(), InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GAScreen.SETTINGS_VIDEO, source, medium, campaign);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_video_setting, container, false);

        RadioGroup radioGroup = mView.findViewById(R.id.radioGroup);
        int selectedIndex = AppPreferences.getInstance(getContext()).getIntValue(QuickPreferences.VIDOE_SETTING, 0);
        RadioButton radioSexButton = mView.findViewById(getSelectedID(selectedIndex));
        radioSexButton.setChecked(true);

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> AppPreferences.getInstance(getContext()).setIntValue(QuickPreferences.VIDOE_SETTING, getSelectedIndex(checkedId)));

        SwitchCompat wifi_toggle= mView.findViewById(R.id.wifi_toggle);
        boolean wifiState = AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.VIDOE_SETTING_WIFI, true);
        wifi_toggle.setChecked(wifiState);
        wifi_toggle.setOnCheckedChangeListener((buttonView, isChecked) -> AppPreferences.getInstance(getContext()).setBooleanValue(QuickPreferences.VIDOE_SETTING_WIFI, isChecked));





        return mView;
    }

    private int getSelectedIndex(int checkedId){
        int index = 0;
        switch (checkedId) {
            case R.id.radioButton1:
                index = 0;
                break;

            case R.id.radioButton2:
                index = 1;
                break;

            case R.id.radioButton3:
                index = 2;
                break;

            case R.id.radioButton4:
                index = 3;
                break;
        }
        return index;
    }

    private int getSelectedID(int index){
        int selectedId= R.id.radioButton1;
        switch (index) {
            case 0:
                selectedId= R.id.radioButton1;
                break;
            case 1:
                selectedId= R.id.radioButton2;
                break;
            case 2:
                selectedId= R.id.radioButton3;
                break;
            case 3:
                selectedId= R.id.radioButton4;
                break;
        }
        return selectedId;
    }
    @Override
    public void onResume() {
        super.onResume();

    }
}
