package com.db.splash;

/**
 * Created by eftahm1 on 13-12-2017.
 */

public interface SplashCallbackListener {
        void onSplashProgress(boolean show,SplashFragmentCallbackListener splashFragmentCallbackListener);
        void onSplashPrepared(SplashFragmentCallbackListener splashFragmentCallbackListener);
        void onSplashComplete(SplashFragmentCallbackListener splashFragmentCallbackListener);
        void onSplashFailed(SplashFragmentCallbackListener splashFragmentCallbackListener);
}
