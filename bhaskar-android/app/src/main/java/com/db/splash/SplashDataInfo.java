package com.db.splash;

import com.bhaskar.appscommon.ads.AdsConstants;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Lalit Goswami on 05-12-2017.
 */

public class SplashDataInfo implements Serializable {

    public static int SPLASH_BANNER_TYPE_WEBVIEW = 0;
    public static int SPLASH_BANNER_TYPE_IMAGE = 1;
    @SerializedName("splash_id")
    public String splashId;

    @SerializedName("splash_url")
    public String splashUrl;

    @SerializedName("splash_type")
    public String splashType;

    @SerializedName("action_url")
    public String splashActionUrl;

    @SerializedName("banner_url")
    public String splashBannerUrl;

    @SerializedName("like_count")
    public String splashLikeCount;

    @SerializedName("dislike_count")
    public String splashDislikeCount;

    @SerializedName("time")
    public String splashTime;

    @SerializedName("enable_img_banner")
    public String enable_img_banner;

    @SerializedName("action_text")
    public String action_text;

    @SerializedName("action_active")
    public String action_active;

    @SerializedName("action_url_name")
    public String action_url_name;

    @SerializedName("session_count")
    public String session_count;

    public int getSplashCount() {
        return AdsConstants.getIntFromString(session_count);
    }

    @Override
    public String toString() {
        return "\nSplashDataInfo{" +
                "splashId='" + splashId + '\'' +
                ", splashUrl='" + splashUrl + '\'' +
                ", splashType='" + splashType + '\'' +
                ", splashActionUrl='" + splashActionUrl + '\'' +
                ", splashBannerUrl='" + splashBannerUrl + '\'' +
                ", splashLikeCount='" + splashLikeCount + '\'' +
                ", splashDislikeCount='" + splashDislikeCount + '\'' +
                ", splashTime='" + splashTime + '\'' +
                ", enable_img_banner='" + enable_img_banner + '\'' +
                ", action_text='" + action_text + '\'' +
                ", action_active='" + action_active + '\'' +
                '}';
    }

    public boolean isBottomActionButtons() {
        return AdsConstants.getIntFromString(action_active) == 1;
    }

    public boolean getBannerTypeImage() {
        return AdsConstants.getIntFromString(enable_img_banner) == SPLASH_BANNER_TYPE_IMAGE;
    }
}
