package com.db.splash;

/**
 * Created by eftahm1 on 13-12-2017.
 */

public interface SplashFragmentCallbackListener {
    void setCallBackListener(SplashCallbackListener splashCallbackListener);
    void stopFunctionality();
}
