package com.db.splash;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bhaskar.R;
import com.db.util.Constants;
import com.db.util.ImageUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashGifFragment extends Fragment /*implements SplashFragmentCallbackListener*/ {

    private ImageView gifSplash;

    public SplashGifFragment() {
    }

    public static SplashGifFragment getInstance() {
        SplashGifFragment splashGifFragment = new SplashGifFragment();
        return splashGifFragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_splash_gif, container, false);
        gifSplash = rootView.findViewById(R.id.giv_splash);
        ImageUtil.setImageGif(getActivity(), Constants.splashDataInfo.splashUrl, gifSplash, 0);
//        showGifImage(getActivity(), Constants.splashDataInfo.splashUrl, gifSplash);
        return rootView;
    }

    boolean isLoadedCalled = false;

//    private void showGifImage(final Context context, String url, final GifImageView gifImageView) {
//        if (!TextUtils.isEmpty(url)) {
//            if (url.endsWith(".gif")) {
//                new GIFDownloader() {
//                    @Override
//                    protected void onPostExecute(final byte[] bytes) {
//                        gifImageView.setBytes(bytes);
//                        gifImageView.startAnimation();
//                        if (!isLoadedCalled && splashCallbackListener != null)
//                            splashCallbackListener.onSplashPrepared(SplashGifFragment.this);
//                    }
//
//                    @Override
//                    protected void onCancelled() {
//                        super.onCancelled();
//                        if (splashCallbackListener != null)
//                            splashCallbackListener.onSplashFailed(SplashGifFragment.this);
//                    }
//                }.execute(url);
//
//            } else if (splashCallbackListener != null)
//                splashCallbackListener.onSplashFailed(this);
//            gifImageView.setVisibility(View.VISIBLE);
//        }
//    }


//    private SplashCallbackListener splashCallbackListener;

//    @Override
//    public void setCallBackListener(SplashCallbackListener splashCallbackListener) {
//        this.splashCallbackListener = splashCallbackListener;
//    }

//    @Override
//    public void stopFunctionality() {
//        if (gifSplash != null)
//            gifSplash.stopAnimation();
//    }
}
