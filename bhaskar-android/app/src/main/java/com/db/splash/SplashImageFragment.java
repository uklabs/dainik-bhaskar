package com.db.splash;


import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bhaskar.R;
import com.db.util.Constants;
import com.db.util.ImageUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashImageFragment extends Fragment implements SplashFragmentCallbackListener {

    private ImageView ivSplash;

    public static SplashImageFragment getInstance() {
        SplashImageFragment splashImageFragment = new SplashImageFragment();

        return splashImageFragment;
    }

    public SplashImageFragment() {
        // Required empty public constructor
    }


    boolean isAlreadySecondTime = false;
    boolean isLoadedCalled = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash_image, container, false);
        ivSplash = rootView.findViewById(R.id.iv_splash);
        if (splashCallbackListener != null)
            splashCallbackListener.onSplashProgress(true, SplashImageFragment.this);

        if (Constants.splashDataInfo != null && !TextUtils.isEmpty(Constants.splashDataInfo.splashUrl)) {
            try {
                Glide.with(this)
                        .asBitmap()
                        .load(ImageUtil.HttpHeader.getUrl(Constants.splashDataInfo.splashUrl))
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .listener(new RequestListener<Bitmap>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                if (!isAlreadySecondTime) {
                                    isAlreadySecondTime = true;

                                } else if (splashCallbackListener != null) {
                                    splashCallbackListener.onSplashProgress(false, SplashImageFragment.this);
                                    splashCallbackListener.onSplashFailed(SplashImageFragment.this);
                                }
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                if (!isLoadedCalled && splashCallbackListener != null) {
                                    isLoadedCalled = true;
                                    splashCallbackListener.onSplashProgress(false, SplashImageFragment.this);
                                    splashCallbackListener.onSplashPrepared(SplashImageFragment.this);
                                }
                                return false;
                            }
                        }).into(ivSplash);
            } catch (Exception e) {
            }
        }
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    //Callback
    private SplashCallbackListener splashCallbackListener;

    @Override
    public void setCallBackListener(SplashCallbackListener splashCallbackListener) {
        this.splashCallbackListener = splashCallbackListener;
    }

    @Override
    public void stopFunctionality() {

    }
}
