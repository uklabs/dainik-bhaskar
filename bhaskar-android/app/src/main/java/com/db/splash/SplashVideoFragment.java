package com.db.splash;


import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import com.bhaskar.R;
import com.db.util.Systr;

public class SplashVideoFragment extends Fragment implements SplashFragmentCallbackListener {

    private VideoView videoview;
    private MediaPlayer mediaPlayer;
    private boolean isLoadedCalled = false;
    private String videoUrl;

    public static SplashVideoFragment getInstance(String videoUrl) {
        SplashVideoFragment splashVideoFragment = new SplashVideoFragment();

        Bundle bundle = new Bundle();
        bundle.putString("video_url", videoUrl);
        splashVideoFragment.setArguments(bundle);

        return splashVideoFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null) {
            videoUrl = arguments.getString("video_url");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash_video, container, false);
        videoview = rootView.findViewById(R.id.video_view);
        return rootView;
    }

    public void setVolume(boolean soundOnOff) {
        if (mediaPlayer != null)
            if (soundOnOff)
                mediaPlayer.setVolume(1.0f, 1.0f);
            else
                mediaPlayer.setVolume(0.0f, 0.0f);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (splashCallbackListener != null)
            splashCallbackListener.onSplashProgress(true, SplashVideoFragment.this);
        try {
            Uri video = Uri.parse(videoUrl);
            videoview.setMediaController(null);
            videoview.setVideoURI(video);
            videoview.requestFocus();

            videoview.setOnPreparedListener(new OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer = mp;
                    if (!isLoadedCalled && splashCallbackListener != null) {
                        isLoadedCalled = true;
                        splashCallbackListener.onSplashProgress(false, SplashVideoFragment.this);
                        splashCallbackListener.onSplashPrepared(SplashVideoFragment.this);
                    }
                    videoview.setVisibility(View.VISIBLE);
                    videoview.start();
                }
            });

            videoview.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int i, int i1) {
                    mediaPlayer = mp;
                    if (splashCallbackListener != null) {
                        splashCallbackListener.onSplashProgress(false, SplashVideoFragment.this);
                        splashCallbackListener.onSplashFailed(SplashVideoFragment.this);
                    }
                    Systr.println("Error==");
                    return true;
                }
            });

            videoview.setOnCompletionListener(mediaPlayer -> {
                currentPosition = -1;
                if (splashCallbackListener != null)
                    splashCallbackListener.onSplashComplete(SplashVideoFragment.this);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    int currentPosition = -1;

    @Override
    public void onPause() {
        super.onPause();
        if (videoview != null) {
            currentPosition = videoview.getCurrentPosition();
            videoview.pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (videoview != null && currentPosition != -1) {
            videoview.seekTo(currentPosition);
            videoview.start();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        videoview = null;
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    //Callback
    private SplashCallbackListener splashCallbackListener;

    @Override
    public void setCallBackListener(SplashCallbackListener splashCallbackListener) {
        this.splashCallbackListener = splashCallbackListener;
    }

    @Override
    public void stopFunctionality() {
        if (videoview != null) {
            videoview.suspend();
        }
    }
}
