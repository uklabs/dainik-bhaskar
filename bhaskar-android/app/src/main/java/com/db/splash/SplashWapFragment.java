package com.db.splash;


import android.graphics.PorterDuff;
import android.net.http.SslError;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.bhaskar.R;
import com.db.util.AppLogs;
import com.db.util.AppUtils;
import com.db.util.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashWapFragment extends Fragment implements SplashFragmentCallbackListener{
    private WebView splashWebView;
    private ProgressBar progressBar;

    public SplashWapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash_wap, container, false);
        splashWebView = rootView.findViewById(R.id.webview);
        progressBar = rootView.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(AppUtils.fetchAttributeColor(getActivity(), R.attr.toolbarBackgroundPrimary), PorterDuff.Mode.MULTIPLY);
        progressBar.setPadding(0, 0, 0, 0);
        progressBar.setMax(100);
        setWebView(splashWebView, Constants.splashDataInfo.splashUrl);

        return rootView;
    }

    private void setWebView(WebView topBannerWebView, String url)
    {
        WebSettings webSettings = topBannerWebView.getSettings();
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);

        topBannerWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        topBannerWebView.setScrollbarFadingEnabled(false);
        topBannerWebView.setVerticalScrollBarEnabled(false);
        topBannerWebView.setHorizontalScrollBarEnabled(false);
        topBannerWebView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        topBannerWebView.setLongClickable(false);
        topBannerWebView.setWebViewClient(new DbWebViewClient());
        topBannerWebView.setWebChromeClient(new MyWebViewClientOther());

        //Below line prevent vibration0 on Long click
        topBannerWebView.setHapticFeedbackEnabled(false);
        //webView set cachMode When Network available or Not
        topBannerWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        //Manage retry option when network not available  or page not load
        topBannerWebView.loadUrl(url);
        topBannerWebView.setFocusable(false);
        topBannerWebView.setFocusableInTouchMode(false);
    }

    public static SplashWapFragment getInstance() {
        SplashWapFragment splashWapFragment = new SplashWapFragment();
        return splashWapFragment;
    }

    boolean isLoadedCalled=false;
    private class DbWebViewClient extends WebViewClient {

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            if (handler != null)
                handler.cancel();
        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            AppLogs.printDebugLogs("webload url", url);
            view.loadUrl(url);
            progressBar.setVisibility(View.VISIBLE);
            return true;
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if(!isLoadedCalled && splashCallbackListener!=null) {
                isLoadedCalled=true;
                splashCallbackListener.onSplashPrepared(SplashWapFragment.this);
            }
        }


    }

    public void setValue(int progress) {
        progressBar.setProgress(progress);
        if (progress == 100)
            progressBar.setVisibility(View.GONE);
    }

    private class MyWebViewClientOther extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            progressBar.setVisibility(View.VISIBLE);
            if (newProgress < 15)
                setValue(15);
            else
                setValue(newProgress);
            super.onProgressChanged(view, newProgress);
        }
    }

    private SplashCallbackListener splashCallbackListener;
    @Override
    public void setCallBackListener(SplashCallbackListener splashCallbackListener) {
        this.splashCallbackListener = splashCallbackListener;
    }
    @Override
    public void stopFunctionality() {

    }
}
