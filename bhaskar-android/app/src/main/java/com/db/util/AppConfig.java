package com.db.util;

/**
 * Class used to set the configuration of a project
 */
public interface AppConfig {

    /**
     * The basic tag for the application log
     */
    String BaseTag = "DainikBhaskar";


}
