package com.db.util;

import com.google.gson.Gson;

public final class AppGson {
    private Gson gsonInstance;
    private static AppGson appGson;

    public static AppGson getAppGson() {
        if (appGson == null)
            appGson = new AppGson();
        return appGson;
    }

    private AppGson() {
        gsonInstance = new Gson();
    }

    public Gson getGsonInstance() {
        return gsonInstance;
    }

}
