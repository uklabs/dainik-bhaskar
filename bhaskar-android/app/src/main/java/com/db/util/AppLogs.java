package com.db.util;

import android.util.Log;

import com.android.volley.VolleyLog;

public class AppLogs {
    public static void printErrorLogs(String source, String value) {
//        Log.e(source, value);
    }

    public static void printAdsErrorLogs(String source, String value) {
//        Log.e(source, value);
    }

    public static void printDebugLogs(String source, String value) {
        Log.d(source, value);
    }

    public static void printVolleyLogs(String source, String value) {
//        VolleyLog.e(source, value);
    }

    public static void printVerboseLogs(String source, String value) {
//        Log.d(source, value);
    }
}
