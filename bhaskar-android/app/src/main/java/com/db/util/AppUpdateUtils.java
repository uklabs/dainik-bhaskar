package com.db.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.view.Window;
import android.widget.TextView;

import com.bhaskar.view.ui.MainActivity;
import com.bhaskar.R;
import com.db.main.ForceUpdateActivity;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

import java.util.Date;

public class AppUpdateUtils {
    private static AppUpdateUtils appUpdateUtils;
    private Activity homeActivity;
    private AppUpdateManager appUpdateManager;

    public static AppUpdateUtils getAppUpdateUtils(Activity activity) {
        if (appUpdateUtils == null)
            appUpdateUtils = new AppUpdateUtils(activity);
        return appUpdateUtils;
    }

    private AppUpdateUtils(Activity activity) {
        homeActivity = activity;
    }

    public void initAppUpdate() {
        // fetch value from preference
        int curAppVersion = AppPreferences.getInstance(homeActivity).getIntValue(QuickPreferences.APP_CURRENT_VERSION, 0);
        int appUpdateVersion = AppPreferences.getInstance(homeActivity).getIntValue(QuickPreferences.UPDATE_POPUP_VERSION_CODE, 0);

        int sessionCount = AppPreferences.getInstance(homeActivity).getIntValue(QuickPreferences.APP_UPDATE_SESSION_COUNT, 0);
        long lastPopupShownDate = AppPreferences.getInstance(homeActivity).getLongValue(QuickPreferences.APP_UPDATE_POP_UP_SHOWN_DATE, (long) 0);

        //fetch value from preference for update popup
        if (curAppVersion < appUpdateVersion) {
            if (sessionCount > 0) {
                AppPreferences.getInstance(homeActivity).setIntValue(QuickPreferences.APP_UPDATE_SESSION_COUNT, sessionCount + 1);
            } else {
                Date lastShownDate = new Date();
                AppPreferences.getInstance(homeActivity).setLongValue(QuickPreferences.APP_UPDATE_POP_UP_SHOWN_DATE, lastShownDate.getTime());
            }

            long threshold = Constants.AppCommon.UPDATE_POPUP_DAYS_COUNT * 24 * 60 * 60 * 1000L;
            long today = new Date().getTime();

            if (AppPreferences.getInstance(homeActivity).getIntValue(QuickPreferences.FORCE_UPDATE_VERSION, 0) == 1) {
                homeActivity.startActivity(new Intent(homeActivity, ForceUpdateActivity.class));
//                homeActivity.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            } else if (sessionCount % Constants.AppCommon.UPDATE_POPUP_SESSION_COUNT == 0 || (today - lastPopupShownDate >= threshold)) {
                checkAndLaunchUpdate();
            }
        }
    }

    private void checkAndLaunchUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(homeActivity);

        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                try {
                    onRegister();
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.FLEXIBLE, homeActivity, MainActivity.IN_APP_UPDATE_REQUEST_CODE);
                } catch (IntentSender.SendIntentException ignored) {

                }
            } else if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                showUpdatePopup();
            }
        });
    }

    private final InstallStateUpdatedListener updatedListener = installState -> {
        try {
            if (installState.installStatus() == InstallStatus.DOWNLOADED) {
                showUpdatePopup();
            } else if (installState.installStatus() == InstallStatus.CANCELED) {
                homeActivity.finish();
            }
        } catch (Exception ignored) {
        }
    };


    private void showUpdatePopup() {

        final Dialog updateVersionDialog = new Dialog(homeActivity);
        updateVersionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        updateVersionDialog.setContentView(R.layout.app_update_popup_layout);
        updateVersionDialog.setCancelable(false);
        if (!homeActivity.isFinishing()) {
            updateVersionDialog.show();
        }
        TextView btnRemindMeLater = updateVersionDialog.findViewById(R.id.remind_later_tv);

        TextView btnUpdateNow = updateVersionDialog.findViewById(R.id.update_now_tv);
        TextView txt_description = updateVersionDialog.findViewById(R.id.txt_dia);

        String updatePopupMessage = AppPreferences.getInstance(homeActivity).getStringValue(QuickPreferences.UPDATE_POPUP_INSTALL_MESSAGE, "");
        if (!AppUtils.getInstance().isValidString(updatePopupMessage)) {
            txt_description.setText(homeActivity.getResources().getString(R.string.app_update_popup_default_message));
        } else {
            txt_description.setText(AppPreferences.getInstance(homeActivity).getStringValue(QuickPreferences.UPDATE_POPUP_MESSAGE, ""));
        }

        btnRemindMeLater.setOnClickListener(v -> {
            final Date lastShownDate = new Date();
            // store last pop up shown date
            AppPreferences.getInstance(homeActivity).setLongValue(QuickPreferences.APP_UPDATE_POP_UP_SHOWN_DATE, lastShownDate.getTime());
            // set session count 0
            AppPreferences.getInstance(homeActivity).setIntValue(QuickPreferences.APP_UPDATE_SESSION_COUNT, 1);
            updateVersionDialog.dismiss();
        });
        btnUpdateNow.setOnClickListener(v -> {
            try {
                AppPreferences.getInstance(homeActivity).setIntValue(QuickPreferences.APP_UPDATE_SESSION_COUNT, 0);
                appUpdateManager.completeUpdate();
            } catch (Exception ignored) {
            }

            updateVersionDialog.dismiss();
        });
    }

    public void onRegister() {
        appUpdateManager.registerListener(updatedListener);
    }

    public void onUnregister() {
        if (appUpdateManager != null) {
            appUpdateManager.unregisterListener(updatedListener);
            appUpdateManager = null;
        }

    }
}
