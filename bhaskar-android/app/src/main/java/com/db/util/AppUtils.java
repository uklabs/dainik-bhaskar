package com.db.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.R;
import com.bhaskar.appscommon.ads.consents.Consents;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.local.prefs.SWAppPreferences;
import com.bhaskar.data.model.Offer;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.AppUrls;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.util.CssConstants;
import com.bhaskar.util.LoginController;
import com.bhaskar.util.ShareWinController;
import com.db.InitApplication;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.NewsSearchHistory;
import com.db.data.models.ArticleDetailInfo;
import com.db.data.models.BookmarkSerializedListInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.data.models.RelatedArticleInfo;
import com.db.data.source.remote.SerializeData;
import com.db.data.source.server.BackgroundRequest;
import com.db.dbvideoPersonalized.data.VideoInfo;
import com.db.dbvideoPersonalized.detail.VideoPlayerActivity;
import com.db.dbvideoPersonalized.detail.VideoPlayerListFragment;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.articlePage.DivyaPartraitVideoActivity;
import com.db.home.ActivityUtil;
import com.db.listeners.OnDownloadListener;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.main.BaseAppCompatActivity;
import com.db.main.CoreAppCompatActivity;
import com.db.news.WapV2Activity;
import com.db.news.WebFullScreenActivity;
import com.db.news.WebViewActivity;
import com.bhaskar.view.ui.photoGallery.PhotoGalleryActivity;
import com.db.util.animation.ParallaxPagerTransformer;
import com.db.views.JustifiedTextView;
import com.db.views.MySpannable;
import com.github.jksiezni.permissive.PermissionsGrantedListener;
import com.github.jksiezni.permissive.PermissionsRefusedListener;
import com.github.jksiezni.permissive.Permissive;
import com.google.ads.consent.ConsentStatus;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AppUtils {

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    public static String GLOBAL_DISPLAY_NAME;
    public static boolean isBlockCountry = true;
    private static AppUtils uniqueInstance;

    public static AppUtils getInstance() {
        if (uniqueInstance == null) {
            synchronized (AppUtils.class) {
                uniqueInstance = new AppUtils();
            }
        }
        return uniqueInstance;
    }

    public static int fetchAttributeColor(Context context, int attr) {
        if (context != null) {
            TypedValue typedValue = new TypedValue();

            TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]{attr});
            int color = a.getColor(0, 0);
            a.recycle();
            return color;
        } else {
            return R.color.dark_grey;
        }
    }

    public static int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage = (((double) currentSeconds) / totalSeconds) * 100;

        // return percentage
        return percentage.intValue();
    }

    public static int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = totalDuration / 1000;
        currentDuration = (int) ((((double) progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {
        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                try {
                    ViewTreeObserver obs = tv.getViewTreeObserver();
                    obs.removeGlobalOnLayoutListener(this);
                    if (maxLine == 0) {
                        int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                        String text = tv.getText().subSequence(0, lineEndIndex - (expandText.length() * 3) + 1) + " " + expandText;
                        tv.setText(text);
                        tv.setMovementMethod(LinkMovementMethod.getInstance());
                        tv.setText(addClickablePartTextViewResizable(
                                Html.fromHtml(tv.getText().toString()), tv,
                                maxLine, expandText, viewMore),
                                TextView.BufferType.SPANNABLE);
                    } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                        int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                        String text = tv.getText().subSequence(0, lineEndIndex - (expandText.length() * 3) + 1) + " " + expandText;
                        tv.setText(text);
                        tv.setMovementMethod(LinkMovementMethod.getInstance());
                        tv.setText(addClickablePartTextViewResizable(
                                Html.fromHtml(tv.getText().toString()), tv,
                                maxLine, expandText, viewMore),
                                TextView.BufferType.SPANNABLE);
                        tv.setMaxLines(maxLine);
                    } else {
                        int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                        String text;
                        if (expandText.equals("Less")) {
                            text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                        } else {
                            text = tv.getText().subSequence(0, lineEndIndex) + " ";
                        }

                        if (!text.equals(" More")) {
                            tv.setMaxLines(15);
                            tv.setText(text);
                            tv.setMovementMethod(LinkMovementMethod.getInstance());
                            tv.setText(
                                    addClickablePartTextViewResizable(
                                            Html.fromHtml(tv.getText().toString()),
                                            tv, lineEndIndex, expandText, viewMore),
                                    TextView.BufferType.SPANNABLE);
                        }
                    }
                } catch (Exception e) {
                }
            }
        });
    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(
            final Spanned strSpanned, final TextView tv, final int maxLine,
            final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new MySpannable(true) {
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        tv.setMaxLines(15);
                        makeTextViewResizable(tv, -1, "Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        tv.setMaxLines(3);
                        makeTextViewResizable(tv, 3, "More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText)
                    + spanableText.length(), 0);

        }
        return ssb;

    }

    private static void convertListToString(Context context, List<String> list) {
        String data = "";
        for (int i = (list.size() - 5); i < list.size(); i++) {
            data = data + list.get(i) + ",";
        }
        AppPreferences.getInstance(context).setStringValue(QuickPreferences.DOUBLE_NOTIFICATION_DATA, data);
    }

    public static void shareApp(Context activity) {
        String message = activity.getResources().getString(R.string.app_share_message);
        String messageLink = CommonConstants.APP_SHARE_URL;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String body = message + " \n" + messageLink;
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, activity.getString(R.string.app_share_subject));
        activity.startActivity(Intent.createChooser(intent, "Share"));
    }

    public static void shareDbOriginal(Context activity) {
        String sbjct = "એક્સક્લુસિવ સમાચારો, કોલમ અને એનાલિસિસ માત્ર DB ઓરિજિનલ્સ પર";
        String messageLink = "https://divyabhaskar.page.link/db-originals";
        String message = "દિવ્ય ભાસ્કર APP પર મેળવો પ્રતિષ્ઠિત લેખકોની કોલમ, એક્સક્લુસિવ સમાચારો, ફોટોઝ, વીડિયોઝ જેવી અનેક ખૂબીઓ";
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String body = sbjct + " \n" + messageLink + "\n" + message;
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, sbjct);
        activity.startActivity(Intent.createChooser(intent, "Share DbOriginal"));
    }

    public static String getFilteredStoryId(String story_id) {
        if (!TextUtils.isEmpty(story_id)) {
            if (story_id.contains("00001")) {
                String[] arr = story_id.split("00001");
                if (arr != null && arr.length > 1) {
                    story_id = arr[0];
                }
            } else if (story_id.contains("@")) {
                String[] arr = story_id.split("@");
                if (arr != null && arr.length > 1) {
                    story_id = arr[0];
                }
            }
        } else {
            story_id = "";
        }
        return story_id;
    }

    public static boolean isBlockCountry(Context context) {
        isBlockCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        return isBlockCountry;
    }

    public static boolean isAdEnabled(Context context) {
        boolean isAdEnabled = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.ADS_CONSENT, false);
        if (!isAdEnabled && isBlockCountry(context))
            Consents.getInstance(CommonConstants.DFP_PUBLISHER_ID_FOR_CONSENT).setConsentStatus(context, ConsentStatus.NON_PERSONALIZED);
        else {
            Consents.getInstance(CommonConstants.DFP_PUBLISHER_ID_FOR_CONSENT).setConsentStatus(context, ConsentStatus.PERSONALIZED);
        }
        return isAdEnabled;
    }

    public static ProgressDialog generateProgressDialog(Context context, boolean cancelable) {
        ProgressDialog progressDialog = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            progressDialog = new ProgressDialog(context, R.style.ProgressTheme);
        else
            progressDialog = new ProgressDialog(context, R.style.AlertDialog_Holo);
        progressDialog.setCancelable(cancelable);
        return progressDialog;
    }

    public static void hideKeyboard(Context context) {
        try {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.e("AppUtils", "Sigh, cant even hide keyboard " + e.getMessage());
        }
    }

    public static int getSpan() {
        switch (BaseAppCompatActivity.screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                return 4;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return 3;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return 3;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return 3;
            case Configuration.SCREENLAYOUT_SIZE_UNDEFINED:
                return 3;
            case Configuration.SCREENLAYOUT_SIZE_MASK:
                return 3;
            default:
                return 3;
        }
    }

    public static void downloadFile(final Activity context, final String fileUrl, final OnDownloadListener listener) {

        new Permissive.Request(Manifest.permission.WRITE_EXTERNAL_STORAGE).whenPermissionsGranted(new PermissionsGrantedListener() {
            @Override
            public void onPermissionsGranted(String[] permissions) throws SecurityException {
                try {
                    String[] temp = fileUrl.split("/");
                    String fileNameWithExtn = "";
                    if (temp.length > 0) {
                        fileNameWithExtn = temp[temp.length - 1];
                    }
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fileUrl));
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileNameWithExtn);
                    DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                    dm.enqueue(request);

                    //To notify the Client that the file is being downloaded
                    AppUtils.getInstance().showCustomToast(context, "Downloading File");

                    if (listener != null) {
                        listener.onStart();
                    }

                } catch (Exception ignored) {
                }
            }
        }).whenPermissionsRefused(new PermissionsRefusedListener() {
            @Override
            public void onPermissionsRefused(String[] permissions) {
                AppUtils.getInstance().showCustomToast(context, "Please provide write permission.");
            }
        }).execute(context);
    }

    public static PopupWindow showMoreOptionsV2(final Context context, final View v, final MoreOptionInterface moreOptionInterface) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_menu_layout, null);
        TextView tvBookMark = view.findViewById(R.id.tvBookMark);
        TextView tvShare = view.findViewById(R.id.tvShare);
        final PopupWindow popupWindow = new PopupWindow(view, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(v);

        tvBookMark.setOnClickListener(v1 -> {
            if (moreOptionInterface != null) {
                popupWindow.dismiss();
                moreOptionInterface.onBookMark();
            }
        });
        tvShare.setOnClickListener(v12 -> {
            if (moreOptionInterface != null) {
                popupWindow.dismiss();
                moreOptionInterface.onShare();
            }
        });

        return popupWindow;
    }

    public static String getThemeColor(String color) {
        try {
            Color.parseColor(color.trim());
            return color;
        } catch (Exception e) {
        }

        return CssConstants.themeColor;
    }

    public static int getThemeColor(Context context, String color) {
        if (context != null) {

            try {
                return Color.parseColor(color.trim());
            } catch (Exception e) {
                return ContextCompat.getColor(context, R.color.divya_theme_color);
            }
        }

        return Color.parseColor(CssConstants.themeColor);
    }

    public void showCustomToast(Context context, String message) {
        try {
            View layout = LayoutInflater.from(context).inflate(R.layout.layout_custom_toast, null);
            TextView text = layout.findViewById(R.id.toast_textview);
            text.setText(message);

            Toast toast = new Toast(context);
//            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } catch (Exception e) {
        }
    }

    public void sendEmail(Context context, String subject) {
        try {
            if (subject.equalsIgnoreCase("")) {
                subject = context.getString(R.string.feedback_mail_subject) + TrackingUtils.getAppVersion(context);
            }

            Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
            sendIntent.setType("message/rfc822");
            sendIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{CommonConstants.FEED_BACK_EMAIL_ID});
            sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
            sendIntent.putExtra(android.content.Intent.EXTRA_TEXT, getPhoneInfo(context));
            context.startActivity(Intent.createChooser(sendIntent, "Send email"));

        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public String getPhoneInfo(Context context) {
        if (AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true)) {
            return "";
        } else {
            String str = "App Version : " + TrackingUtils.getAppVersion(context) + "\nSdk : " + Build.VERSION.SDK_INT + "\nBrand : " + Build.BRAND + "\nModel : " + Build.MODEL + "\nDBId : " + TrackingData.getDBId(context) + "\n-------------------------------\n\n";
            return str;
        }
    }

    public String removeAndReplaceHtmlSymbols(String htmlString) {
        if (!TextUtils.isEmpty(htmlString)) {
            htmlString = htmlString.replace("&amp;", "&");
            htmlString = htmlString.replace("&lt;", "<");
            htmlString = htmlString.replace("lt;", "<");
            htmlString = htmlString.replace("'", "\"");
            htmlString = htmlString.replace("\r", "");
            htmlString = htmlString.replace("&gt;", ">");
            htmlString = htmlString.replace("gt;", ">");
            htmlString = htmlString.replace("&nbsp;", " ");
            htmlString = htmlString.replace("nbsp;", " ");
            htmlString = htmlString.replace("\n", "<br>");
            htmlString = htmlString.replace("<div>", "");
            htmlString = htmlString.replace("</div>", "");

        }
        return htmlString;
    }

    public String handleBreakTagInDescription(String data) {
        String finalData = data.trim();
        finalData = finalData.replaceAll("<br> <br>", "<br><br>");
        finalData = finalData.replaceAll("<br>  <br>", "<br><br>");
        finalData = finalData.replaceAll("<br><br><br><br><br><br>", "<br><br>");
        finalData = finalData.replaceAll("<br><br><br><br><br>", "<br><br>");
        finalData = finalData.replaceAll("<br><br><br><br>", "<br><br>");
        finalData = finalData.replaceAll("<br><br><br>", "<br><br>");
        return finalData;
    }

    public MovementMethod privateMovementMethod(final Context context) {
        return new MovementMethod() {

            @Override
            public boolean onTrackballEvent(TextView widget, Spannable text,
                                            MotionEvent event) {
                return false;
            }

            @Override
            public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
                final int action = event.getAction();
                if (action == MotionEvent.ACTION_UP) {
                    final int x = (int) event.getX() - widget.getTotalPaddingLeft() + widget.getScrollX();
                    final int y = (int) event.getY() - widget.getTotalPaddingTop() + widget.getScrollY();
                    final Layout layout = widget.getLayout();
                    final int line = layout.getLineForVertical(y);
                    final int off = layout.getOffsetForHorizontal(line, x);
                    final ClickableSpan[] C = buffer.getSpans(off, off, ClickableSpan.class);
                    if (C.length != 0) {
                        URLSpan span = (URLSpan) C[0];
                        OpenNewsLinkInAppV2 openNewsLinkInApp = new OpenNewsLinkInAppV2(context, span.getURL(), Constants.KeyPair.KEY_NEWS_UPDATE, 1);
                        openNewsLinkInApp.openLink();
                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onTakeFocus(TextView widget, Spannable text, int direction) {// empty Method
            }

            @Override
            public boolean onKeyUp(TextView widget, Spannable text, int keyCode,
                                   KeyEvent event) {
                return false;
            }

            @Override
            public boolean onKeyOther(TextView view, Spannable text, KeyEvent event) {
                return false;
            }

            @Override
            public boolean onKeyDown(TextView widget, Spannable text, int keyCode,
                                     KeyEvent event) {
                return false;
            }

            @Override
            public boolean onGenericMotionEvent(TextView widget, Spannable text,
                                                MotionEvent event) {
                return false;
            }

            @Override
            public void initialize(TextView widget, Spannable text) {
                widget.setLinkTextColor(Color.parseColor("#0000EE"));
            }

            @Override
            public boolean canSelectArbitrarily() {
                return false;
            }
        };
    }


    public MovementMethod privacyMovementMethod(final Context context, final String title) {
        return new MovementMethod() {

            @Override
            public boolean onTrackballEvent(TextView widget, Spannable text,
                                            MotionEvent event) {
                return false;
            }

            @Override
            public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
                final int action = event.getAction();
                if (action == MotionEvent.ACTION_UP) {
                    final int x = (int) event.getX() - widget.getTotalPaddingLeft() + widget.getScrollX();
                    final int y = (int) event.getY() - widget.getTotalPaddingTop() + widget.getScrollY();
                    final Layout layout = widget.getLayout();
                    final int line = layout.getLineForVertical(y);
                    final int off = layout.getOffsetForHorizontal(line, x);
                    final ClickableSpan[] C = buffer.getSpans(off, off, ClickableSpan.class);
                    if (C.length != 0) {
                        URLSpan span = (URLSpan) C[0];
                        Intent intent = new Intent(context, WebViewActivity.class);
                        intent.putExtra(Constants.KeyPair.KEY_URL, span.getURL());
                        intent.putExtra(Constants.KeyPair.KEY_TITLE, title);
                        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAScreen.WEB_VIEW + "-" + span.getURL());
                        context.startActivity(intent);

                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onTakeFocus(TextView widget, Spannable text, int direction) {// empty Method
            }

            @Override
            public boolean onKeyUp(TextView widget, Spannable text, int keyCode,
                                   KeyEvent event) {
                return false;
            }

            @Override
            public boolean onKeyOther(TextView view, Spannable text, KeyEvent event) {
                return false;
            }

            @Override
            public boolean onKeyDown(TextView widget, Spannable text, int keyCode,
                                     KeyEvent event) {
                return false;
            }

            @Override
            public boolean onGenericMotionEvent(TextView widget, Spannable text,
                                                MotionEvent event) {
                return false;
            }

            @Override
            public void initialize(TextView widget, Spannable text) {
                widget.setLinkTextColor(Color.parseColor("#000000"));
            }

            @Override
            public boolean canSelectArbitrarily() {
                return false;
            }
        };
    }

    public String getTimeAgo(String pub_date) {
        long now = System.currentTimeMillis();
        long time = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = sdf.parse(pub_date);
            time = date.getTime();
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }


        if (time > now || time <= 0) {
            time = now;
        }

        // localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a min ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " min ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hr ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hr ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

    @SuppressWarnings("deprecation")
    public Spanned fromHtml(String html) {
        html = removeAndReplaceHtmlSymbols(html);
        html = handleBreakTagInDescription(html);
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public Spanned getHomeTitle(String iitlTitle, Spanned secSent, String iitlColor) {
        iitlColor = AppUtils.getThemeColor(iitlColor);
        String title = "";
        String defaultArticleColor = "#000000";
        if (TextUtils.isEmpty(iitlTitle)) {
            title = String.format("<font color=%s>%s</font>", defaultArticleColor, secSent);
        } else {
            title = String.format("<font color=%s>%s</font> <font color=%s>%s</font>", iitlColor, iitlTitle, defaultArticleColor, secSent);
        }
        return Html.fromHtml(title);
    }

    public Spanned getHomeTitleNew(String firstSent, Spanned secSent, String iitlColor) {
        String title = "";
        iitlColor = AppUtils.getThemeColor(iitlColor);
        String defaultArticleColor = "#FFFFFF";
        if (firstSent == null || firstSent.isEmpty()) {
            title = String.format("<font color=%s>%s</font>", defaultArticleColor, secSent);
        } else {
            title = String.format("<font color=%s>%s</font> <font color=%s>%s</font>", iitlColor, firstSent, defaultArticleColor, secSent);
        }
        return Html.fromHtml(title);
    }

    public Spanned getHomeTitle(String firstSent, Spanned secSent, String iitlColor, String articleColor) {
        String title = "";
        String defaultIITLColor = CssConstants.themeColor;
        String defaultArticleColor = "#1a1a1a";
        if (iitlColor != null && !iitlColor.isEmpty())
            defaultIITLColor = iitlColor;
        if (articleColor != null && !articleColor.isEmpty())
            defaultArticleColor = articleColor;
        if (firstSent == null || firstSent.isEmpty()) {
            title = String.format("<font color=%s>%s</font>", defaultArticleColor, secSent);
        } else {
            title = String.format("<font color=%s>%s</font> <font color=%s>%s</font>", defaultIITLColor, firstSent, defaultArticleColor, secSent);
        }
        return Html.fromHtml(title);
    }

    public void shareClick(Context mContext, String title, String webUrl, String gTrackUrl, boolean fromDetail) {
        BackgroundRequest.getStoryShareUrl(mContext, webUrl, (flag, shareUrl) -> {
            String shareLink = (flag) ? shareUrl : webUrl;
            shareArticle(mContext, shareLink, title, gTrackUrl, fromDetail);
        });
    }

    public void bookmarkClick(Context mContext, Object object, String feedDetailUrl, boolean fromDetail) {
        if (object instanceof NewsListInfo) {
            NewsListInfo newsListInfo = (NewsListInfo) object;
            boolean isStoryBookmarked = SerializeData.getInstance(mContext).isBookmarkAvailable(AppUtils.getFilteredStoryId(newsListInfo.storyId));
            if (isStoryBookmarked) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getString(R.string.bookmark_already_added));
            } else {

                BookmarkSerializedListInfo bookmarkListInfo = new BookmarkSerializedListInfo(AppUtils.getFilteredStoryId(newsListInfo.storyId));
                bookmarkListInfo.setVideoFlag(isVideo(newsListInfo));
                bookmarkListInfo.setImage(newsListInfo.image);
                bookmarkListInfo.setTitle(newsListInfo.title);
                bookmarkListInfo.setStorytype("DB");
                bookmarkListInfo.setDetailUrl(feedDetailUrl);
                SerializeData.getInstance(mContext).saveBookmark(bookmarkListInfo);

                // Tracking
                String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.BOOKMARK, newsListInfo.gTrackUrl, campaign);

                CleverTapDB.getInstance(mContext).cleverTapTrackEvent(mContext, CTConstant.EVENT_NAME.BOOKMARKED, CTConstant.PROP_NAME.CONTENT_URL, newsListInfo.gTrackUrl, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.BOOKMARKED_THROUGH, fromDetail ? CTConstant.PROP_VALUE.STORY_DETAIL : CTConstant.PROP_VALUE.THREE_DOTS));

                AppUtils.getInstance().showCustomToast(mContext, mContext.getString(R.string.bookmark_added));
            }

        } else if (object instanceof RelatedArticleInfo) {
            RelatedArticleInfo articleInfo = (RelatedArticleInfo) object;
            boolean isStoryBookmarked = SerializeData.getInstance(mContext).isBookmarkAvailable(AppUtils.getFilteredStoryId(articleInfo.storyId));
            if (isStoryBookmarked) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getString(R.string.bookmark_already_added));
            } else {
                BookmarkSerializedListInfo bookmarkListInfo = new BookmarkSerializedListInfo(AppUtils.getFilteredStoryId(articleInfo.storyId));
                bookmarkListInfo.setVideoFlag(isVideo(articleInfo));
                bookmarkListInfo.setImage(articleInfo.image);
                bookmarkListInfo.setSlugIntro(articleInfo.slugIntro);
                bookmarkListInfo.setTitle(articleInfo.title);
                bookmarkListInfo.setStorytype("DB");
                bookmarkListInfo.setDetailUrl(feedDetailUrl);
                SerializeData.getInstance(mContext).saveBookmark(bookmarkListInfo);

                // Tracking
                String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.BOOKMARK, articleInfo.gTrackUrl, campaign);

                CleverTapDB.getInstance(mContext).cleverTapTrackEvent(mContext, CTConstant.EVENT_NAME.BOOKMARKED, CTConstant.PROP_NAME.CONTENT_URL, articleInfo.gTrackUrl, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.BOOKMARKED_THROUGH, fromDetail ? CTConstant.PROP_VALUE.STORY_DETAIL : CTConstant.PROP_VALUE.THREE_DOTS));

                AppUtils.getInstance().showCustomToast(mContext, mContext.getString(R.string.bookmark_added));
            }
        }
    }

    public void bookmarkClick(Context mContext, Object object, String feedDetailUrl, String color, boolean fromDetail) {
        if (TextUtils.isEmpty(color)) {
            color = "";
        }

        if (object instanceof NewsListInfo) {
            NewsListInfo newsListInfo = (NewsListInfo) object;
            boolean isStoryBookmarked = SerializeData.getInstance(mContext).isBookmarkAvailable(AppUtils.getFilteredStoryId(newsListInfo.storyId));
            if (isStoryBookmarked) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getString(R.string.bookmark_already_added));
            } else {

                BookmarkSerializedListInfo bookmarkListInfo = new BookmarkSerializedListInfo(AppUtils.getFilteredStoryId(newsListInfo.storyId));
                bookmarkListInfo.setVideoFlag(isVideo(newsListInfo));
                bookmarkListInfo.setImage(newsListInfo.image);
                bookmarkListInfo.setTitle(newsListInfo.title);
                bookmarkListInfo.setIitlTitle(newsListInfo.iitl_title);
                bookmarkListInfo.setColor(color);
                bookmarkListInfo.setStorytype("DB");
                bookmarkListInfo.setDetailUrl(feedDetailUrl);
                SerializeData.getInstance(mContext).saveBookmark(bookmarkListInfo);

                // Tracking
                String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.BOOKMARK, newsListInfo.gTrackUrl, campaign);

                CleverTapDB.getInstance(mContext).cleverTapTrackEvent(mContext, CTConstant.EVENT_NAME.BOOKMARKED, CTConstant.PROP_NAME.CONTENT_URL, newsListInfo.gTrackUrl, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.BOOKMARKED_THROUGH, fromDetail ? CTConstant.PROP_VALUE.STORY_DETAIL : CTConstant.PROP_VALUE.THREE_DOTS));

                AppUtils.getInstance().showCustomToast(mContext, mContext.getString(R.string.bookmark_added));
            }

        } else if (object instanceof RelatedArticleInfo) {
            RelatedArticleInfo articleInfo = (RelatedArticleInfo) object;
            boolean isStoryBookmarked = SerializeData.getInstance(mContext).isBookmarkAvailable(AppUtils.getFilteredStoryId(articleInfo.storyId));
            if (isStoryBookmarked) {
                AppUtils.getInstance().showCustomToast(mContext, mContext.getString(R.string.bookmark_already_added));
            } else {
                BookmarkSerializedListInfo bookmarkListInfo = new BookmarkSerializedListInfo(AppUtils.getFilteredStoryId(articleInfo.storyId));
                bookmarkListInfo.setVideoFlag(isVideo(articleInfo));
                bookmarkListInfo.setImage(articleInfo.image);
                bookmarkListInfo.setSlugIntro(articleInfo.slugIntro);
                bookmarkListInfo.setTitle(articleInfo.title);
                bookmarkListInfo.setIitlTitle(articleInfo.iitlTitle);
                bookmarkListInfo.setColor(color);
                bookmarkListInfo.setStorytype("DB");
                bookmarkListInfo.setDetailUrl(feedDetailUrl);
                SerializeData.getInstance(mContext).saveBookmark(bookmarkListInfo);

                // Tracking
                String campaign = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
                Tracking.trackGAEvent(mContext, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.BOOKMARK, articleInfo.gTrackUrl, campaign);

                CleverTapDB.getInstance(mContext).cleverTapTrackEvent(mContext, CTConstant.EVENT_NAME.BOOKMARKED, CTConstant.PROP_NAME.CONTENT_URL, articleInfo.gTrackUrl, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.BOOKMARKED_THROUGH, fromDetail ? CTConstant.PROP_VALUE.STORY_DETAIL : CTConstant.PROP_VALUE.THREE_DOTS));

                AppUtils.getInstance().showCustomToast(mContext, mContext.getString(R.string.bookmark_added));
            }
        }
    }

    public int isVideo(NewsListInfo newsListInfo) {
        if (newsListInfo != null) {
            if (newsListInfo.videoFlag == 1) {
                return 1;
            }
        }
        return 0;
    }

    public int isVideo(RelatedArticleInfo info) {
        if (info != null) {
            if (info.videoFlag.equalsIgnoreCase("1")) {
                return 1;
            }
        }
        return 0;
    }

    public void shareArticle(Context context, String url, String title, String trackUrl, boolean fromDetail) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        StringBuilder shareContent = new StringBuilder(1);
        shareContent.append(title)
                .append("\n")
                .append(url);

        shareIntent.putExtra(Intent.EXTRA_TEXT, shareContent.toString());
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Check out this Article!");
        shareIntent.setType("text/plain");
        if (context != null) {
            context.startActivity(Intent.createChooser(shareIntent, "Share via"));
        }

        // Tracking
        String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
        Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.SHARE, trackUrl, campaign);

        CleverTapDB.getInstance(context).cleverTapTrackEvent(context, CTConstant.EVENT_NAME.CONTENT_SHARED, CTConstant.PROP_NAME.NEWS_URL, trackUrl, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.SHARED_THROUGH, fromDetail ? CTConstant.PROP_VALUE.STORY_DETAIL : CTConstant.PROP_VALUE.THREE_DOTS));
    }

    public void setFontSize(Context context, TextView textView, int cat) {
        int size = AppPreferences.getInstance(context).getIntValue(QuickPreferences.FONT_SIZE, Constants.Font.MEDIUM);

        float fontSize = 10;
        try {
            switch (size) {
                case Constants.Font.SMALL:
                    switch (cat) {
                        case Constants.Font.CAT_X_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.small_x_large_text_size);
                            break;
                        case Constants.Font.CAT_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.small_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.small_medium_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM:
                            fontSize = context.getResources().getDimension(R.dimen.small_medium_text_size);
                            break;
                        case Constants.Font.CAT_SMALL_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.small_small_large_text_size);
                            break;
                        case Constants.Font.CAT_SMALL:
                            fontSize = context.getResources().getDimension(R.dimen.small_small_text_size);
                            break;
                        case Constants.Font.CAT_PUB_DATE:
                            fontSize = context.getResources().getDimension(R.dimen.small_pubdate_text_size);
                            break;
                    }
                    break;
                case Constants.Font.MEDIUM:

                    switch (cat) {
                        case Constants.Font.CAT_X_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_x_large_text_size);
                            break;
                        case Constants.Font.CAT_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_medium_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM:
                            fontSize = context.getResources().getDimension(R.dimen.medium_medium_text_size);
                            break;
                        case Constants.Font.CAT_SMALL_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_small_large_text_size);
                            break;
                        case Constants.Font.CAT_SMALL:
                            fontSize = context.getResources().getDimension(R.dimen.medium_small_text_size);
                            break;

                        case Constants.Font.CAT_PUB_DATE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_pubdate_text_size);
                            break;
                    }
                    break;

                case Constants.Font.LARGE:

                    switch (cat) {
                        case Constants.Font.CAT_X_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_x_large_text_size);
                            break;
                        case Constants.Font.CAT_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_medium_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM:
                            fontSize = context.getResources().getDimension(R.dimen.large_medium_text_size);
                            break;
                        case Constants.Font.CAT_SMALL_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_small_large_text_size);
                            break;
                        case Constants.Font.CAT_SMALL:
                            fontSize = context.getResources().getDimension(R.dimen.large_small_text_size);
                            break;
                        case Constants.Font.CAT_PUB_DATE:
                            fontSize = context.getResources().getDimension(R.dimen.large_pubdate_text_size);
                            break;
                    }
                    break;


            }
        } catch (Exception e) {

        }
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
    }

    public void setArticleFontSize(Context context, TextView textView, int cat) {
        int size = AppPreferences.getInstance(context).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);

        float fontSize = 10;
        try {
            switch (size) {
                case Constants.Font.MEDIUM:

                    switch (cat) {
                        case Constants.Font.CAT_X_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_x_large_text_size);

                            break;
                        case Constants.Font.CAT_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_medium_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM:
                            fontSize = context.getResources().getDimension(R.dimen.medium_medium_text_size);
                            break;
                        case Constants.Font.CAT_SMALL_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_small_large_text_size);
                            break;
                        case Constants.Font.CAT_SMALL:
                            fontSize = context.getResources().getDimension(R.dimen.medium_small_text_size);
                            break;

                        case Constants.Font.CAT_PUB_DATE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_pubdate_text_size);
                            break;
                    }
                    break;

                case Constants.Font.LARGE:

                    switch (cat) {
                        case Constants.Font.CAT_X_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_x_large_text_size);
                            break;
                        case Constants.Font.CAT_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_medium_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM:
                            fontSize = context.getResources().getDimension(R.dimen.large_medium_text_size);
                            break;
                        case Constants.Font.CAT_SMALL_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_small_large_text_size);
                            break;
                        case Constants.Font.CAT_SMALL:
                            fontSize = context.getResources().getDimension(R.dimen.large_small_text_size);
                            break;
                        case Constants.Font.CAT_PUB_DATE:
                            fontSize = context.getResources().getDimension(R.dimen.large_pubdate_text_size);
                            break;
                    }
                    break;


            }
        } catch (Exception e) {

        }
        if (textView != null)
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
    }

    public void setArticleFontSizeJustify(Context context, JustifiedTextView textView, int cat) {
        int size = AppPreferences.getInstance(context).getIntValue(QuickPreferences.ARTICLE_FONT_SIZE, Constants.Font.MEDIUM);

        float fontSize = 10;
        try {
            switch (size) {
                case Constants.Font.MEDIUM:

                    switch (cat) {
                        case Constants.Font.CAT_X_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_x_large_text_size);

                            break;
                        case Constants.Font.CAT_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_medium_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM:
                            fontSize = context.getResources().getDimension(R.dimen.medium_medium_text_size);
                            break;
                        case Constants.Font.CAT_SMALL_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_small_large_text_size);
                            break;
                        case Constants.Font.CAT_SMALL:
                            fontSize = context.getResources().getDimension(R.dimen.medium_small_text_size);
                            break;

                        case Constants.Font.CAT_PUB_DATE:
                            fontSize = context.getResources().getDimension(R.dimen.medium_pubdate_text_size);
                            break;
                    }
                    break;

                case Constants.Font.LARGE:

                    switch (cat) {
                        case Constants.Font.CAT_X_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_x_large_text_size);
                            break;
                        case Constants.Font.CAT_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_medium_large_text_size);
                            break;
                        case Constants.Font.CAT_MEDIUM:
                            fontSize = context.getResources().getDimension(R.dimen.large_medium_text_size);
                            break;
                        case Constants.Font.CAT_SMALL_LARGE:
                            fontSize = context.getResources().getDimension(R.dimen.large_small_large_text_size);
                            break;
                        case Constants.Font.CAT_SMALL:
                            fontSize = context.getResources().getDimension(R.dimen.large_small_text_size);
                            break;
                        case Constants.Font.CAT_PUB_DATE:
                            fontSize = context.getResources().getDimension(R.dimen.large_pubdate_text_size);
                            break;
                    }
                    break;


            }
        } catch (Exception e) {

        }
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
    }

    public String getSIPrefixForCount(int count) {
        String siPrefix;

        try {
            int value = (count / 1000000000);
            if (value > 0) {
                siPrefix = String.format(Locale.getDefault(), "%.1fB", (count / 1000000000.0f));
            } else {
                value = (count / 1000000);
                if (value > 0) {
                    siPrefix = String.format(Locale.getDefault(), "%.1fM", (count / 1000000.0f));
                } else {
                    value = (count / 1000);
                    if (value > 0) {
                        siPrefix = String.format(Locale.getDefault(), "%.1fK", (count / 1000.0f));
                    } else {
                        siPrefix = String.format(Locale.getDefault(), "%d", count);
                    }
                }
            }

        } catch (Exception e) {
            siPrefix = "" + count;
        }

        return siPrefix;
    }

    public String getSIPrefixForCount(String scount) {
        int count = 0;
        try {
            count = Integer.valueOf(scount);
        } catch (NumberFormatException e) {
        }

        String siPrefix;

        try {
            int value = (count / 1000000000);
            if (value > 0) {
                siPrefix = String.format(Locale.getDefault(), "%.1fB", (count / 1000000000.0f));
            } else {
                value = (count / 1000000);
                if (value > 0) {
                    siPrefix = String.format(Locale.getDefault(), "%.1fM", (count / 1000000.0f));
                } else {
                    value = (count / 1000);
                    if (value > 0) {
                        siPrefix = String.format(Locale.getDefault(), "%.1fK", (count / 1000.0f));
                    } else {
                        siPrefix = String.format(Locale.getDefault(), "%d", count);
                    }
                }
            }

        } catch (Exception e) {
            siPrefix = "" + count;
        }

        return siPrefix;
    }

    public void openNewsDetail(Context context, String storyId, String wisdomDomain, int isBackground, String menuName, String gaScreen, String gaArticle, String gaDisplayName, boolean useFlag) {

        // setData from in intent to know from where this details activity is called in order to handle back
        Intent intent = new Intent(context, AppUtils.getInstance().getArticleTypeClass());
        if (useFlag)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (isBackground == 0)
            intent.putExtra(Constants.KeyPair.KEY_FROM, Constants.DeepLinking.DEEPLINK);
        intent.putExtra(Constants.KeyPair.KEY_STORY_ID, storyId);
        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
        intent.putExtra(Constants.KeyPair.KEY_GA_ARTICLE, gaArticle);
        intent.putExtra(Constants.KeyPair.KEY_GA_DISPLAY_NAME, gaDisplayName);
        intent.putExtra(Constants.KeyPair.KEY_NEWS_DETAIL_TITLE, menuName);
        intent.putExtra(Constants.KeyPair.KEY_WISDOM_DOMAIN, wisdomDomain);
        intent.putExtra(Constants.KeyPair.IN_BACKGROUND, isBackground);
        if (context instanceof Activity) {
            ((Activity) context).startActivityForResult(intent, Constants.REQ_CODE_BOTTOM_NAV);
        } else {
            context.startActivity(intent);
        }
    }

    public void openVideo(Context mContext, String id, boolean isBackground) {
        String fetchUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.APP_VIDEO_NOTIFICATION_URL, AppUrls.PUSH_VIDEO_PREFIX);
//        String videoFeedUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.VideoPref.SETTINGS_VIDEO_FEED_URL_V2, "") + CommonConstants.CHANNEL_ID + "/";
        String videoFeedUrl = AppUrls.VIDEO_RECOMMENDATION_URL + CommonConstants.CHANNEL_ID + "/";
        if (!TextUtils.isEmpty(fetchUrl)) {
            Systr.println("Notification video url : " + fetchUrl);
            fetchUrl = fetchUrl + CommonConstants.CHANNEL_ID + "/" + id + "/";
            Systr.println("Video fetch url : " + fetchUrl);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, fetchUrl, null,
                    response -> {
                        Systr.println("Notification DBVideo : " + response);
                        try {
                            if (response != null && response.length() > 0) {
                                VideoInfo videoInfo = new Gson().fromJson(response.toString(), VideoInfo.class);
                                if (videoInfo != null) {
                                    if (videoInfo.portraitVideo == 1) {
                                        Intent intent = DivyaPartraitVideoActivity.getIntent(mContext, videoInfo, AppFlyerConst.GAExtra.SHARE_GA_SCREEN, AppFlyerConst.DBVideosSource.VIDEO_SHARE, 1, "", AppFlyerConst.GAExtra.SHARE_GA_ARTICLE);
                                        intent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, isBackground);
                                        mContext.startActivity(intent);
                                    } else {
                                        ArrayList<VideoInfo> videoList = new ArrayList<>();
                                        videoList.add(videoInfo);

                                        String feedUrl = videoFeedUrl + videoInfo.storyId + "/";
                                        Intent intent = VideoPlayerActivity.getIntent(mContext, 0, 1, feedUrl, AppFlyerConst.DBVideosSource.VIDEO_SHARE, "", AppFlyerConst.GAExtra.SHARE_GA_ARTICLE, AppFlyerConst.GAExtra.SHARE_GA_SCREEN, "", VideoPlayerListFragment.TYPE_LIST, VideoPlayerListFragment.THEME_LIGHT);
                                        VideoPlayerActivity.addListData(videoList);
                                        intent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, isBackground);
                                        mContext.startActivity(intent);
                                    }
                                }
                            }
                        } catch (Exception e) {
                        }
                    },
                    error -> Systr.println("Video Error : " + error)) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
        }
    }

    public void openDBOriginalVideo(Context mContext, String url, String gaScreen, String gaArticle) {
        String fetchUrl = Urls.APP_FEED_BASE_URL + url + "PG1/";

        Systr.println("Video fetch url : " + fetchUrl);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, fetchUrl, null,
                response -> {
                    Systr.println("Notification DBVideo : " + response);
                    try {
                        if (response != null) {
                            JSONArray array = response.getJSONArray("feed");
                            ArrayList<VideoInfo> videoList = new ArrayList<>(Arrays.asList(new Gson().fromJson(array.toString(), VideoInfo[].class)));
                            Intent intent = VideoPlayerActivity.getIntent(mContext, 0, 2, url, AppFlyerConst.DBVideosSource.VIDEO_LIST, "", gaScreen, gaArticle, "", VideoPlayerListFragment.TYPE_GRID, VideoPlayerListFragment.THEME_DARK);
                            VideoPlayerActivity.addListData(videoList);
                            mContext.startActivity(intent);
                        }
                    } catch (Exception e) {
                    }
                },
                error -> Systr.println("Video Error : " + error)) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommonConstants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(mContext).addToRequestQueue(jsonObjReq);
    }

    public void openPhotoGallery(Context mContext, String id, boolean isComeFromNotification) {
        Intent photoGalleryIntent = new Intent(mContext, PhotoGalleryActivity.class);
        photoGalleryIntent.putExtra(Constants.PhotoGalleryActions.PHOTO_GALLERY_ACTION, Constants.PhotoGalleryActions.GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS);
        photoGalleryIntent.putExtra(Constants.KeyPair.KEY_POSITION, 0);
        photoGalleryIntent.putExtra(Constants.KeyPair.KEY_ID, id);
        photoGalleryIntent.putExtra(Constants.KeyPair.KEY_CLICK_VIA_NOTIFICATION, isComeFromNotification);
        mContext.startActivity(photoGalleryIntent);
    }

    public boolean isValidString(String string) {
        return !TextUtils.isEmpty(string) && !string.trim().equalsIgnoreCase("null");
    }


    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public String updateApiUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            String updatedUrl = url.trim();

            updatedUrl = updatedUrl.replace(" ", "%20");
            return updatedUrl;
        } else {
            return url;
        }
    }

    public String makeUrlForLoadMore(String baseUrl, int count) {
        String finalUrl = "";
        if (baseUrl.endsWith("/1/")) {
            finalUrl = baseUrl.substring(0, baseUrl.length() - 2);
            finalUrl = finalUrl + "3/" + "PG" + count + "/";
        } else {
            finalUrl = baseUrl + "PG" + count + "/";
        }
        return finalUrl;
    }

    public boolean checkDoubleNotification(Context context, String storyId) {
        try {
            String data = AppPreferences.getInstance(context).getStringValue(QuickPreferences.DOUBLE_NOTIFICATION_DATA, "");
            if (data.length() > 1) {
                if ((data.charAt(data.length() - 1) + "").equalsIgnoreCase(",")) {
                    data = data.substring(0, data.length() - 1);
                }
            } else {
                return false;
            }

            String[] arr = data.split(",");
            List<String> list = new ArrayList<>();
            Collections.addAll(list, arr);
            if (list.size() > Constants.NOTIFICATION_ID_HARD_LIMIT)
                convertListToString(context, list);

            if (list != null) {
                return list.contains(storyId);
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void setImageViewSizeWithAspectRatio(final ImageView imgView, int width, float heightRatio) {
        if (width > 0) {
            imgView.getLayoutParams().height = (int) (width * heightRatio);
            imgView.getLayoutParams().width = width;
            imgView.requestLayout();
        }
    }

    public void setImageViewSizeWithAspectRatio(final View imgView, float heightRatio) {
        if (Constants.DEVICE_WIDTH == 0) {
            Constants.DEVICE_WIDTH = Resources.getSystem().getDisplayMetrics().widthPixels;
        }
        imgView.getLayoutParams().height = (int) (Constants.DEVICE_WIDTH * heightRatio);
        imgView.getLayoutParams().width = Constants.DEVICE_WIDTH;
        imgView.requestLayout();
    }

    public void setViewSizeWithAspectRatioOrMax43(final View view, float heightRatio) {
        if (Constants.DEVICE_WIDTH == 0) {
            Constants.DEVICE_WIDTH = Resources.getSystem().getDisplayMetrics().widthPixels;
        }
        int layoutHeight = (int) (Constants.DEVICE_WIDTH * heightRatio);
        int layoutMaxHeight = (int) (Constants.DEVICE_WIDTH * Constants.ImageRatios.NEWS_BRIEF_RATIO);

        view.getLayoutParams().height = layoutMaxHeight > layoutHeight ? layoutHeight : layoutMaxHeight;
        view.getLayoutParams().width = Constants.DEVICE_WIDTH;
        view.requestLayout();
    }

    public void setImageViewHalfSizeWithAspectRatio(final ImageView imgView, float heightRatio, int totalPaddingInDP, Context context) {
        if (Constants.DEVICE_WIDTH == 0) {
            Constants.DEVICE_WIDTH = Resources.getSystem().getDisplayMetrics().widthPixels;
        }
        int width = (Constants.DEVICE_WIDTH / 2) - (int) AppUtils.getInstance().convertDpToPixel(totalPaddingInDP, context);
        imgView.getLayoutParams().height = (int) (width * heightRatio);
        imgView.getLayoutParams().width = width;
        imgView.requestLayout();
    }

    public float parseImageRatio(String size, float defaultRatio) {
        float ratio = defaultRatio;
        if (!TextUtils.isEmpty(size) && size.contains("x")) {
            String[] xy = size.split("x");
            try {
                int w = Integer.parseInt(xy[0].trim());
                int h = Integer.parseInt(xy[1].trim());
                ratio = (float) h / (float) w;
            } catch (Exception e) {
                ratio = defaultRatio;
            }
        }
        return ratio;
    }

    public boolean isAClick(float startX, float endX, float startY, float endY) {
        int CLICK_ACTION_THRESHOLD = 200;
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        return !(differenceX > CLICK_ACTION_THRESHOLD/* =5 */ || differenceY > CLICK_ACTION_THRESHOLD);
    }

    public void setImageViewSizeWithAspectRatio(final ImageView imgView, float heightRatio, int totalPaddingInDP, Context context) {
        if (Constants.DEVICE_WIDTH == 0) {
            Constants.DEVICE_WIDTH = Resources.getSystem().getDisplayMetrics().widthPixels;
        }
        int width = Constants.DEVICE_WIDTH - (int) AppUtils.getInstance().convertDpToPixel(totalPaddingInDP, context);
        imgView.getLayoutParams().height = (int) (width * heightRatio);
        imgView.getLayoutParams().width = width;
        imgView.requestLayout();
    }


    public Bitmap rotate(Bitmap b, int degrees) {
        if (degrees != 0 && b != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);

            try {
                Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);

                if (b != b2) {
                    b.recycle();
                    b = b2;
                }
            } catch (OutOfMemoryError ex) {
                throw ex;
            }
        }
        return b;
    }

    public Class getArticleTypeClass() {
        return CoreAppCompatActivity.class;
    }

    public String getPath(Context context, Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    public void setStatusBarColor(Activity activity, int color) {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = activity.getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color);
            }
        } catch (Exception e) {
        }
    }

    public void setStatusBarColor(Activity context, String color) {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = context.getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(AppUtils.getThemeColor(context, color));
            }
        } catch (Exception ignored) {
        }
    }

    public void saveSearchHistory(Activity activity, String searchKeyword) {
        final NewsSearchHistory newsSearchHistory = (NewsSearchHistory) DatabaseHelper.getInstance(activity).getTableObject(NewsSearchHistory.TABLE_NAME);
        if (!newsSearchHistory.keywordExist(searchKeyword)) {
            newsSearchHistory.updateSearchTime(searchKeyword);
        } else {
            newsSearchHistory.insertSearchKeyword(searchKeyword);
        }
    }

    public void startOffer(Activity context, Offer offers, boolean shouldShowCoupon) {
        if (offers == null) {
            Toast.makeText(context, "Some error occurred Please Try again", Toast.LENGTH_SHORT).show();
            return;
        }
        ShareWinController.shareWinController().getDashboardIntent(context, offers, shouldShowCoupon, InitApplication.getInstance().getDefaultTracker());
    }

    public void startWapOffer(Activity context, Offer offers) {
        if (offers == null) {
            Toast.makeText(context, "Some error occurred Please Try again", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent webIntent = new Intent(context, WapV2Activity.class);
        webIntent.putExtra(Constants.KeyPair.KEY_URL, offers.webUrl);
        webIntent.putExtra(Constants.KeyPair.KEY_TITLE, offers.name);
        webIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, offers.gaScreen);


        context.startActivity(webIntent);
    }

    public void postUserReferral(Context context, String dbID, String deviceID, String refCode, String appId, String offerID) {
        if (NetworkStatus.getInstance().isConnected(context)) {
            JSONObject requestObject = new JSONObject();
            try {
                requestObject.put("db_id", dbID);
                requestObject.put("device_id", deviceID);
                requestObject.put("ref_code", refCode);
                requestObject.put("app_id", appId);
                requestObject.put("sw_id", offerID);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String SW_FEED_BASE_URL = InitApplication.getInstance().getSWBaseUrl("");
            String finalFeedURL = SW_FEED_BASE_URL + Urls.SHARE_WIN_POST_REF;
            Systr.println(String.format(" Feed API %s Request params %s", finalFeedURL, requestObject.toString()));
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, finalFeedURL, requestObject,
                    response -> {
                        Systr.println(String.format(" Feed API %s response params %s", finalFeedURL, response.toString()));
                        String status = response.optString("status");
                        if (status.equalsIgnoreCase("Success")) {
                            SWAppPreferences.getInstance(context).setBooleanValue(SWAppPreferences.Keys.REFF_SENT, true);
                        }

                    }, error -> {
                Systr.println(String.format(" Feed API %s error params %s", finalFeedURL, error.getMessage()));
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return getNetworkHeader(context);
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);

        } else {
            showCustomToast(context, context.getString(R.string.internet_connection_error_));
        }
    }

    static class MyWebViewClient extends WebViewClient {
        @SuppressLint("StaticFieldLeak")
        private static MyWebViewClient myWebViewClient;
        private Context context;

        MyWebViewClient(Context mContext) {
            context = mContext;
        }

        public static MyWebViewClient getInstance(Context context) {
            if (myWebViewClient == null)
                myWebViewClient = new MyWebViewClient(context);
            return myWebViewClient;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            OpenNewsLinkInAppV2 openNewsLinkInApp = new OpenNewsLinkInAppV2(context, url, Constants.KeyPair.KEY_NEWS_UPDATE, 1);
            openNewsLinkInApp.openLink();
            return true;
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            OpenNewsLinkInAppV2 openNewsLinkInApp = new OpenNewsLinkInAppV2(context, request.getUrl().toString(), Constants.KeyPair.KEY_NEWS_UPDATE, 1);
            openNewsLinkInApp.openLink();
            return true;
        }

    }

    public CharSequence trim(CharSequence s) {
        try {
            int start = 0;
            int end = s.length() - 1;
            while (start < end && Character.isWhitespace(s.charAt(start))) {
                start++;
            }

            while (end > start && Character.isWhitespace(s.charAt(end - 1))) {
                end--;
            }

            return s.subSequence(start, end);
        } catch (Exception e) {
        }

        return s;
    }

    public void callLiveTV(Context context, CategoryInfo categoryInfo, OnFeedFetchFromServerListener onFeedFetchFromServerListener) {
        String partnerID = AppPreferences.getInstance(context).getStringValue(QuickPreferences.LiveTv.PARTNER_ID, "");
        String userID = AppPreferences.getInstance(context).getStringValue(QuickPreferences.LiveTv.USER_ID, "");
        String token = AppPreferences.getInstance(context).getStringValue(QuickPreferences.LiveTv.TOKEN, "");
        String expiry = AppPreferences.getInstance(context).getStringValue(QuickPreferences.LiveTv.EXPIRY, "");
        if (!TextUtils.isEmpty(expiry) && !TextUtils.isEmpty(userID) && !TextUtils.isEmpty(partnerID) && !TextUtils.isEmpty(token)) {
            Systr.println("Expiry " + Long.valueOf(expiry) + " Current" + System.currentTimeMillis());
            if (Long.valueOf(expiry) > (System.currentTimeMillis() / 1000)) {
                String finalURL = AppUrls.LIVE_TV_BASEURL + "partner/" + partnerID + "/" + userID + "/" + token;
                Intent webTV = new Intent(context, WebFullScreenActivity.class);
                webTV.putExtra(Constants.KeyPair.KEY_URL, finalURL);
                webTV.putExtra(Constants.KeyPair.KEY_TITLE, categoryInfo.menuName);
                webTV.putExtra(Constants.KeyPair.KEY_GA_SCREEN, categoryInfo.gaScreen);
                webTV.putExtra(Constants.KeyPair.KEY_IS_TV, true);
                context.startActivity(webTV);
            } else if (onFeedFetchFromServerListener != null) {
                BackgroundRequest.getLiveTvToken(context, onFeedFetchFromServerListener, categoryInfo);
            }
        } else if (onFeedFetchFromServerListener != null) {
            BackgroundRequest.getLiveTvToken(context, onFeedFetchFromServerListener, categoryInfo);
        }
    }

    public void openChromeTab(Context context, String url, String tabColor) {
        try {
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.setToolbarColor(AppUtils.getThemeColor(context, tabColor));
            builder.setShowTitle(true);
            CustomTabsIntent customTabsIntent = builder.build();
            String packageName = CustomTabsHelper.getPackageNameToUse(context, url);
            if (packageName != null) {
                customTabsIntent.intent.setPackage(packageName);
            }
            customTabsIntent.launchUrl(context, Uri.parse(url));
        } catch (Exception e) {
        }
    }

    public Map<String, String> getWebHeader(Context context) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("app-id", CommonConstants.BHASKAR_APP_ID);
        headerMap.put("device-id", TrackingData.getDeviceId(context));
        headerMap.put("db-id", TrackingData.getDBId(context));
        String uid = "";
        try {
            uid = LoginController.loginController().getCurrentUser().uID;
        } catch (Exception ignore) {
        }
        headerMap.put("user-id", uid);
        return headerMap;
    }

    public void clickOnAdBanner(Context mContext) {
        String actionUrl = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_ACTION_URL, "");
        String actionName = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_ACTION_NAME, "");
        String gaScreen = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_GA_SCREEN, "");
        String menuId = AppPreferences.getInstance(mContext).getStringValue(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_ACTION_MENU_ID, "");

        if (!TextUtils.isEmpty(actionUrl)) {
            Intent webIntent = new Intent(mContext, WapV2Activity.class);
            webIntent.putExtra(Constants.KeyPair.KEY_URL, actionUrl);
            webIntent.putExtra(Constants.KeyPair.KEY_TITLE, actionName);
            webIntent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, gaScreen);
            mContext.startActivity(webIntent);
        } else {
            CategoryInfo categoryInfoById = JsonParser.getInstance().getCategoryInfoById(mContext, menuId);
            ActivityUtil.openCategoryAccordingToAction(mContext, categoryInfoById, "");
        }
    }

    public int postHashCode(String finalStoryUrl) {
        int hash = 0;
        try {
            if (!TextUtils.isEmpty(finalStoryUrl)) {
                for (int i = 0; i < finalStoryUrl.length(); i++) {
                    hash = ((hash << 5) - hash) + finalStoryUrl.codePointAt(i);
                    hash = hash & hash;
                }
            }
        } catch (Exception e) {

        }
        return hash;
    }

    public static ParallaxPagerTransformer getParallaxPagerTransform(int resId, int borderWidth, float swipeSpeed) {
        ParallaxPagerTransformer transformer = new ParallaxPagerTransformer(resId);
        transformer.setBorderWidth(borderWidth);
        transformer.setSpeed(swipeSpeed);
        return transformer;
    }

    public int stringToInt(String versionString) {
        try {
            return Integer.parseInt(versionString);
        } catch (Exception e) {
            return 0;
        }
    }

    public static GradientDrawable getDbOrgGradient() {
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[]{0xEE000000, 0x00000000, 0x00000000, 0xEE000000});
        gd.setCornerRadius(0f);
        return gd;
    }

    public static int[] getDeviceWidthAndHeight(@NonNull Activity activity) {
        int[] wh = new int[2];
        try {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay()
                    .getMetrics(displaymetrics);
            wh[0] = displaymetrics.widthPixels;
            wh[1] = displaymetrics.heightPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wh;
    }

    public static HashMap<String, String> parseExtendedDynamicURL(String extendedUrl) {
        HashMap<String, String> dataMap = new HashMap<>();
        if (extendedUrl.contains("firebase_dynamic_link=1")) {
            /*New Logic*/
            if (extendedUrl.contains("start=1&") && extendedUrl.contains("&end=1")) {
                int startIndex = extendedUrl.indexOf("start=1&");
                int endIndex = extendedUrl.indexOf("&end=1");

                String appData = extendedUrl.substring(startIndex + 7, endIndex);
                if (appData.contains("&")) {
                    String[] dataArray = appData.split("&");
                    for (String key : dataArray) {
                        if (!TextUtils.isEmpty(key)) {
                            String[] keyValue = key.split("=");
                            if (!TextUtils.isEmpty(keyValue[1]))
                                switch (keyValue[0]) {
                                    case "url":
                                        dataMap.put("url", keyValue[1]);
                                        break;
                                    case "brand_id":
                                        dataMap.put("brand_id", keyValue[1]);
                                        break;
                                    case "menu_id":
                                        dataMap.put("menu_id", keyValue[1]);
                                        break;
                                }
                        }
                    }
                }
            }
        }
        dataMap.put("extended_url", extendedUrl);
        return dataMap;
    }

    public static int getDeviceWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        return width;
    }
    public static int getDeviceHeight(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int heightPixels = displayMetrics.heightPixels;
        return heightPixels;
    }

    public HashMap<String, String> getNetworkHeader(Context context) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("api-key", "bh@@$k@r-D");
        headers.put("device-type", "ANDROID");
        headers.put("content-type", "application/json charset=utf-8");
        headers.put("accept", "application/json");
        headers.put("app-id", CommonConstants.BHASKAR_APP_ID);
        headers.put("db-id", LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.DB_ID, String.class));
        headers.put("device-id", LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.DEVICE_ID, String.class));
        ProfileInfo profileInfo = LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        headers.put("uuid", (profileInfo == null ? null : profileInfo.uuID));
        headers.put("uid", (profileInfo == null ? null : profileInfo.uID));
        Systr.println(String.format(" Feed API Header %s", headers.toString()));
        return headers;
    }

    public void openPlayStore(Context context) {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public static boolean isAppRunningOnEmulator() {
        return (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.HARDWARE.contains("goldfish")
                || Build.HARDWARE.contains("ranchu")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || Build.PRODUCT.contains("sdk_google")
                || Build.PRODUCT.contains("google_sdk")
                || Build.PRODUCT.contains("sdk")
                || Build.PRODUCT.contains("sdk_x86")
                || Build.PRODUCT.contains("vbox86p")
                || Build.PRODUCT.contains("emulator")
                || Build.PRODUCT.contains("simulator");
    }

    /**
     * Checks if is my service running.
     *
     * @return true, if is my service running
     */
    public static boolean isMyServiceRunning(@NonNull Context context,
                                             String serviceClassName) {
        ActivityManager manager = (ActivityManager) context
                .getSystemService(Activity.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {

            String serviceName = service.service.getClassName();
            if (serviceClassName.equals(serviceName)) {
                return true;
            }
        }
        return false;
    }
}