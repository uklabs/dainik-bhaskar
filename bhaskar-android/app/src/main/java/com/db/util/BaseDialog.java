package com.db.util;

import android.app.Activity;
import android.app.Dialog;

import androidx.annotation.NonNull;

public abstract class BaseDialog extends Dialog {

    protected Activity activityContext;

    /**
     * @param activityContext
     */
    public BaseDialog(@NonNull Activity activityContext) {
        super(activityContext);
        this.activityContext = activityContext;
    }

    /**
     * @param activityContext
     * @param themeResId
     */
    public BaseDialog(@NonNull Activity activityContext, int themeResId) {

        super(activityContext, themeResId );
        this.activityContext = activityContext;
    }

    @Override
    public void show() {
        if (activityContext != null && !activityContext.isFinishing()) {
            super.show();
        }
    }

    @Override
    public void dismiss() {
        if (activityContext != null && !activityContext.isFinishing()) {
            super.dismiss();
        }
    }
}
