package com.db.util;

import androidx.annotation.IntDef;

import com.bhaskar.BuildConfig;
import com.db.divya_new.data.GuruvaniGuruTabInfo;
import com.db.splash.SplashDataInfo;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

public class Constants {

    public final static String ACTIVE_TRUE = "1";

    public final static int NOTIFICATION_ID_HARD_LIMIT = 50;
    public final static int NOTIFICATION_APPEND_ID = 1564565;
    public final static int NOTIFICATION_CAT_CATEGORY = 1564566;
    public final static int NOTIFICATION_CAT_ID_URL = 1564567;
    public static int DEVICE_WIDTH = 0;
    public static int DEVICE_HEIGHT = 0;
    public static long FEED_CACHE_TIME = 5;
    public static String BREAKING_NEWS = "hide";
    public static ArrayList<String> singleSessionCategoryIdList = new ArrayList<>();
    public static int MIN_HEIGHT_UNIT = 50;

    /*Splash Data*/
    public static SplashDataInfo splashDataInfo;

    /*WapV2 */
    public static boolean isFileChooserCalled = false;
    public static boolean IS_OVERLAY_PERMISSION_ASKED = false;

    public static GuruvaniGuruTabInfo guruTabInfo;

    /*ReadUnread Date*/
    public static String currentDate;
    public static int REQ_CODE_BOTTOM_NAV = 10244;

    public interface NewsBulletin{
        String KEY_NEWS_BULLETIN_LIST = "key_news_bulletin_list";
        String KEY_SELECTED_NEWS_BULLETIN_INDEX = "key_selected_news_bulletin_index";
        String KEY_NEWS_BULLETIN_FRESH_LAUNCH = "key_news_bulletin_fresh_launch";
        String IS_FIRST_LAUNCH = "is_first_launch";
        String KEY_NEWS_BULLETIN_ID = "key_bulletin_id";
        String ACTION_STOP_TRACKS = "ACTION_STOP_TRACKS";
        String ACTION_BROADCAST_PLAYBACK_CONTROL = BuildConfig.APPLICATION_ID + "MediaPlayerService";
        String KEY_AUDIO_PLAYBACK_EVENT = "key_audio_playback_event";
    }

    public interface AppIds {
        String DAINIK_BHASKAR = "521";
        String DIVYA_BHASKAR = "960";
        String MONEY_BHASKAR = "1463";
        String DIVYA_MARATHI = "5483";
    }

    public interface Font {
        int SMALL = 1;
        int MEDIUM = 2;
        int LARGE = 3;

        int CAT_X_LARGE = 6;
        int CAT_LARGE = 5;

        int CAT_MEDIUM_LARGE = 4;
        int CAT_MEDIUM = 3;

        int CAT_SMALL_LARGE = 2;
        int CAT_SMALL = 1;

        int CAT_PUB_DATE = 0;
    }

    public interface KeyPair {
        String KEY_DATA = "data";
        String KEY_SPLASH_DATA = "splash_data";
        String KEY_FEED = "feed";
        String KEY_ACTION = "action";
        String KEY_STORY_ID = "story_id";
        String KEY_MENU_ID = "menu_id";
        String KEY_WEB_URL = "web_url";
        String KEY_WEB_TITLE = "web_Title";
        String KEY_IS_WAP_EXT = "is_Wap_Ext";
        String KEY_NAME = "name";
        String KEY_IS_MIN_SET = "isMinSet";
        String KEY_IS_BANNER_DYNAMIC_LINK = "isBannerDynamicLink";

        String KEY_DEEP_LINK = "deep_link";
        String KEY_SL_ID = "sl_id";

        String KEY_NEWS_DETAIL = "newsDetail";
        String KEY_DETAIL_URL = "detail_url";
        String KEY_DISPLAY_NAME = "display_name";
        String KEY_ID = "id";
        String KEY_LIST_INFO = "listInfo";
        String KEY_CATEGORY_INFO = "categoryInfo";
        String KEY_CHANNEL_SNO = "channel_slno";
        String KEY_CATEGORY_ID = "categoryId";
        String KEY_PHOTO_DETAIL = "photoDetail";
        String KEY_FROM = "from";
        String KEY_DETAIL_FEED_URL = "detailFeedUrl";
        String KEY_POSITION = "position";
        String KEY_VIDEO_SECTION_LABEL = "section_label";
        String KEY_VIDEO_SECTION_LABEL_PROVIDER = "section_label_provider";
        String KEY_NEWS_DETAIL_TITLE = "newsDetailTitle";

        String KEY_IS_FROM_APPWIDGET = "is_from_appwidget";
        String KEY_IS_FROM_QUICK_NOTIFICATION = "is_from_quick_notification";
        String KEY_IS_NOTIFICATION = "is_notification_message";
        String KEY_NEWS_TYPE = "notification_type";
        String KEY_VERSION = "version";
        String KEY_TODAY_DATE = "today_date";

        String KEY_GA_ARTICLE = "gaArticle";
        String KEY_GA_SCREEN = "gaScreen";
        String KEY_GA_G_TRACK_URL = "gaGTrackUrl";
        String KEY_GA_TRACK_URL = "gaTrackUrl";
        String KEY_GA_DISPLAY_NAME = "gaDisaplayName";
        String KEY_CLICK_VIA_NOTIFICATION = "clickViaNotification";
        String KEY_URL = "Url";
        String KEY_IS_FROM_PAGER = "isFromPager";
        String KEY_TITLE = "title";
        String KEY_IITL_TITLE = "iitlTitle";
        String KEY_CITY_INFO = "cityInfo";
        String KEY_WISDOM_DOMAIN = "wisdomDomain";
        String IN_BACKGROUND = "inBackground";
        String KEY_WISDOM_SUB_DOMAIN = "wisdomSubDomain";
        String KEY_IS_SEARCH = "isSearch";
        String KEY_SOURCE = "Source";
        String KEY_VISIBLE = "visible";
        String KEY_COLOR = "color";

        String KEY_FEED_URL = "feed_Url";
        String KEY_GA_EVENT_LABEL = "ga_event_label";
        String KEY_SELECTED_MY_PAGE_IDS = "SelectedCategoryIdss";

        String KEY_FRAGMENT_VALUE = "fragmentValue";

        String KEY_FEED_URL_IS_PERSONALIZED = "feedUrlIsPersonalized";
        String KEY_IS_VIDEO_NOTIFICATION = "videoNotification";
        String KEY_DBVIDEO_INFO = "videoInfo";
        String KEY_SECTION_LABEL = "sectionLabel";
        String LAUNCH_FROM_STACK_NOTIFICATION = "lauchFromStackNotification";
        String KEY_LINK = "detailLink";
        String KEY_IS_STACK_NOTIFICATION = "isStackNotification";
        String KEY_CAMPAIGN_ID = "campaignId";
        // QUIZ Contest FAQ Keys.
        String KEY_FAQ_URL = "faq_url";
        String KEY_FAQ_TITLE = "faq_title";
        ////////wall///////
        String KEY_NEWS_UPDATE = "";

        String KEY_NOTIFY = "notify";
        String KEY_KEYWORD = "keyword";
        String KEY_LIST_INDEX = "listIndex";
        String KEY_EXTRA_VALUES = "extraValues";

        String KEY_COUNT = "count";
        String KEY_PARENT_ID = "parentId";
        String KEY_SUB_TITLE = "mSubTitle";
        String KEY_IS_FROM_BOOKMARK = "isFromBookmark";
        String KEY_IS_STORY_V2 = "Story_V2";
        String KEY_BULLETS = "bulletArticle";
        String KEY_PUB_DATE = "pubDate";
        String KEY_PROVIDER_NAME = "providerName";
        String KEY_PROVIDER_LOGO = "providerLogo";
        String KEY_HAS_HEADER = "hasHeader";
        String IS_FIRST_TIME = "isFirstTime";
        String KEY_NEXT_PAGE_COUNT = "nextPageCount";
        String KEY_IS_TV = "isTV";

        String KEY_IS_VERTICAL = "isVertical";
        String KEY_PCAT_ID = "pCatId";
        String KEY_PREFIX_SECTION_NAME = "PrefixSectionName";
        String KEY_ICON = "icon";

    }

    public interface SelfieKeyPair {
        String KEY_FEED_OTHER = "other";

        String KEY_ACTION = "action";
        String KEY_SELFIE_KEY = "selfie_key";

        String KEY_SELFIE_FEED_URL = "selfie_feed_url";
        String KEY_SELFIE_UPLOAD_URL = "selfie_upload_url";

        String KEY_IS_CONTEST_ACTIVE = "selfie_is_contest_active";

        String KEY_CONTEST_TYPE = "contest_type";
        String KEY_FOLDER_NAME = "folder_name";

        String KEY_CONTEST_EXTRAS = "contest_extras";

        String KEY_SELFIE_GALLERY_LIST_DATA = "selfie_list_data";
        String KEY_SELFIE_PRROFILE_LIST = "selfie_profile_list_data";
        String KEY_SELFIE_POSITION = "selfie_position";
        String KEY_GA_ARTICLE = "gaArticle";
    }

    public interface ContestAction {
        String KEY_ACTION_NEW = "Action-New";
        String KEY_ACTION_TOP = "Action-Top";
        String KEY_ACTION_WINNER = "Action-Winner";
        String KEY_ACTION_SEARCH = "Action-Search";
        String KEY_ACTION_OTHER = "Action-Other";

    }

    public interface ContestMenuAction {
        String ACTION_MAIL = "MAIL";
        String ACTION_WAP = "WAP";
    }

    public interface SettingAction {
        String MERA_PAGE = "ActionMeraPage";
        String NOTIFICATION = "ActionNotification";
        String AUTHOR_LIST = "ActionAuthorList";
        String VIDEO_SETTING = "ActionVideoSetting";
        String TUTORIALS = "ActionTutorials";
        String SHARE_APP = "ActionShareApp";
        String FEEDBACK = "ActionFeedback";
        String RATE_US = "ActionRateus";
        String ABOUT_US = "ActionAboutUs";
        String TERMS_AND_CONDITION = "ActionTermAndCondi";
        String QUICK_READ = "ActionQuickRead";
        String VERTICAL_SWIPE = "VerticalSwipe";
    }

    public interface PrimeBrandAction {
        String ACTION_IMAGE = "Image";
        String ACTION_GIF = "Gif";
        String ACTION_WAP = "Wap";
        String ACTION_NEWS_GALLERY = "NewsGallery";
        String ACTION_PHOTO_GALLERY = "PhotoGallery";
        String ACTION_VIDEO_GALLERY = "VideoGallery";
    }

    public interface BaseName {
        String HOME = "Home";
        String BRAND = "Brand";
        String NAV = "Nav";
    }

    public interface Utils {
        String DEFAULT = "";
        String FORMAT_YYYY_MM_DD = "yyyy-MM-dd";//2017-02-17
        String FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";//2017-02-17 10:16:00
        String FORMAT_MMM_DD_YYYY_H_MM_A_T = "MMM dd, yyyy, h:mm a";//Jan 16, 2017, 11:23 AM IST
        String FORMAT_MMMM_DD_YYYY = "MMMM dd, yyyy";//January 16, 2017
        String SPACE = " ";
        String FORMATE_SEPARATOR = "#@#";
    }

    public interface DeepLinking {
        String DEEPLINK = "deeplink";
    }

    public interface AppHitChoice {
        String API_HIT = "API_HIT";
        int APP_VERSION_API = 1;
        int APP_CATEGORY_API = 2;
        int APP_CITY_API = 3;
        int APP_HOST_API = 4;
        int BASE_APP_CONTROL_API = 5;
    }

    public interface AppCommon {
        int UPDATE_POPUP_SESSION_COUNT = 7;
        int UPDATE_POPUP_DAYS_COUNT = 5;
        int DEFAULT_VISIBLE_THRESHOLD = 5;
    }

    public interface ArticleDetailContentKeys {
        String ARTICLE_READ_MORE = "article_read_more";
        String ARTICLE_FIRST_PARAGRAPH = "article_first_paragraph";
        String ARTICLE_NORMAL = "article_normal";
        String ARTICLE_TABLE = "article_table";
    }

    public interface PhotoGalleryActions {
        String PHOTO_GALLERY_ACTION = "photoGalleryAction";
        short GALLERY_FOR_DETAIL = 0;
        short GALLERY_FOR_PHOTO_GALLERY = 2;
        short GALLERY_FOR_MOVIE_REVIEW_PREVIEW = 3;
        short GALLERY_FOR_PHOTO_GALLERY_FROM_NEWS = 5;
        short GALLERY_FOR_ORGINIAL_PHOTO_GALLERY = 6;
    }

    public interface ApiName {
        String API_NAME = "api_name";
        String API_VERSION = "api_version";
        String HOST_API = "host";
        String CATEGORIES_API = "categories";
        String CITY_LIST_API = "citylist";
        String BASE_APP_CONTROL_API = "base_appcontrol";
        String GA_EVENT_ENABLE = "ga_event_enable_v2";
        String CSS_API = "article_version";
        String APP_TUTORIAL = "app_tutorials_V2";
        String GA_SCROLL_TEST = "GA_Scroll_Test_V2";
        String HOME_PAGINATION = "home_speed_pagination";
        String HOME_LOGO = "home_logo";
        String ALL_CAT_HOME = "all_cat_home";
    }

    public interface CityPreference {
        short CITY_INFO_BASED_ON_ID = 0;
        short CITY_INFO_BASED_ON_STATE_NAME = 1;
        short CITY_INFO_BASED_ON_CITY_NAME = 2;
    }

    public interface ImageRatios {
        float ARTICLE_DETAIL_RATIO = 0.775f;
        float NEWS_BRIEF_RATIO = 0.75f;
        float DBVIDEOS_RATIO = 0.562f;
        float PHOTO_GALLARY_RATIO = 0.865f;
        float FLICKER_RATIO = 0.674f;
    }

    public interface DefaultSettingValue {
        String DEFAULT_FROM_TIME = "11:00 p.m";
        String DEFAULT_TO_TIME = "07:00 a.m";
    }

    public interface Themes {
        int THEME_DEFAULT = 4;
        String THEME_COLOR = "theme_color";

        int THEME_MARINE = 1;
        int THEME_TURQUOISE = 2;
        int THEME_ORANGE = 3;
        int THEME_DARK_GRAY = 4;
        int THEME_CITRUS = 5;
    }

    public interface DataBaseHit {
        int DEFAULT_TIME_IS_TOKEN = 200;
        int DEFAULT_TIME = 0;
    }

    public interface CityRajyaSelection {
        String DEFAULT = "default";
        String FETCHED = "fetched";
        String SELECTED = "selected";
    }


    public interface AppWidgetType {
        int APP_WIDGET_TYPE = 0;
        int LIST_WIDGET_TYPE = 1;
    }

    public interface AppWidgetIDS {
        int APP_WIDGET_ID_1 = 131313;
        int APP_WIDGET_ID_2 = 141414;
        int APP_WIDGET_AUTO_REFRESH_ID = 1414141;
        int APP_WIDGET_REFRESH_ID = 1414142;
        int LIST_WIDGET_ID = 212121;
        int LIST_WIDGET_REFRESH_ID = 2121211;
        int LIST_WIDGET_AUTO_REFRESH_ID = 2121212;
        int STICKY_WIDGET_ID = 7887;
        int STICKY_WIDGET_CLICK_ID = 121212;
        int STICKY_WIDGET_REFRESH_ID = 1212121;
        int STICKY_WIDGET_SCHEDULER_ID = 1212122;
        int STICKY_WIDGET_CLOSE_ID = 1212123;
    }

    public interface QuickReadActions {
        String ACTION_START = "startQuickRead";
        String ACTION_STOP = "stopQuickRead";
        String ACTION_REFRESH = "refreshQuickRead";
        String ACTION_NEXT = "nextQuickRead";
    }


    public interface SelfiContest {
        String DIRECTORY_NAME = "SelfieContest";
    }

    public interface Radio {
        String RADIO_CHANNEL = "RadioChannel";
        String RADIO_LANGUAGE = "RadioLang";
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({BooleanConst.VIDEO_FOLLOW, BooleanConst.VIDEO_LIKE, BooleanConst.ARTICLE_NEWS_LIKE, BooleanConst.SELFIE_ABUSE, BooleanConst.SELFIE_LIKE, BooleanConst.ARTICLE_COMMENT_LIKE, BooleanConst.ARTICLE_COMMENT_REPORT_ABUSE, BooleanConst.ELECTION_MLA_LIKE, BooleanConst.SPLASH_CONTENT_LIKE, BooleanConst.SPLASH_CONTENT_DISLIKE, BooleanConst.SPLASH_CONTENT_SHARE, BooleanConst.SPLASH_CONTENT_READ_MORE})
    public @interface BooleanConst {
        int VIDEO_FOLLOW = 1;
        int VIDEO_LIKE = 2;
        int SELFIE_ABUSE = 3;
        int SELFIE_LIKE = 4;

        int ARTICLE_NEWS_LIKE = 5;
        int ARTICLE_COMMENT_LIKE = 6;

        int ARTICLE_COMMENT_REPORT_ABUSE = 7;
        int ELECTION_MLA_LIKE = 8;

        int SPLASH_CONTENT_LIKE = 9;
        int SPLASH_CONTENT_DISLIKE = 10;
        int SPLASH_CONTENT_SHARE = 11;
        int SPLASH_CONTENT_READ_MORE = 12;
    }

    public interface SplashType {
        int SPLASH_TYPE_IMAGE = 0;
        int SPLASH_TYPE_HTML = 1;
        int SPLASH_TYPE_GIF = 2;
        int SPLASH_TYPE_VIDEO = 3;
    }

    public interface HomeRecreateVariables {
        String BOTTOM_POS = "Bottom_Pos";
        String IS_TAB_VISIBLE = "TabVisible";
        String ACTION_BAR_TEXT = "actionBarText";
    }

    public interface AppLanguage {
        String KEY_HINDI = "hindi";
        String KEY_ENGLISH = "english";
        String KEY_GUJRATI = "gujarati";
    }

    public interface AppStaticJson {
        String HOST_JSON = "host.json";
        String BASE_APP_CONTROL_JSON = "base_appcontrol.json";
        String CATEGORY_JSON = "category.json";
        String CITY_JSON = "city.json";
    }

    public interface RatingAction {
        String ACTION_RATING = "rating";
        String ACTION_COMMENT = "comment";
        String ACTION_SINGLLE_CHECK = "singlecheck";
        String ACTION_RATE = "rate";
    }

    public interface IntentExtra {
        String TITLE = "title";
        String ID = "id";
        String CHILD_TAB_ID = "child_tab_id";
    }

    public interface IntentFilterType {
        String FILTER_TITLE = "intent_title";
        String FILTER_FOR_ACTIVITY = "intent_for_activity";
    }

    public interface ViewHolderType {
        String VIEW_HOLDER_STORY = "view_holder_story";
        String VIEW_HOLDER_NOVEL = "view_holder_novel";
        String VIEW_HOLDER_SONGS = "view_holder_songs";
    }

}
