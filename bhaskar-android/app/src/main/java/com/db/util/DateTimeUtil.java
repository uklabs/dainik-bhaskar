package com.db.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtil {
    public static final String NEWS_BULLETIN_DATE_DISPLAY_FORMAT = "dd-MM-yyyy";
    public static final String NEWS_BULLETIN_DATE_FORMAT = "EEE, MM/dd/yyyy";
    public static final String NEWS_BULLETIN_DATE_REQUEST_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String MONTH_FORMAT = "MMM";
    public static final String DATE_FORMAT = "dd";

    public static String getFormattedDateString(String date, String currentDateFormat,
                                                String requiredDateFormat) {
        try {
            SimpleDateFormat currentFormat = new SimpleDateFormat(currentDateFormat, Locale.ENGLISH);
            SimpleDateFormat requiredFormat = new SimpleDateFormat(requiredDateFormat, Locale.ENGLISH);
            return getFormattedDate(currentFormat.parse(date), requiredFormat);
        } catch (ParseException e) {
            Log.d("", e.getLocalizedMessage());
        }
        return date;
    }

    private static String getFormattedDate(Date date, SimpleDateFormat requiredFormat) {
        return requiredFormat.format(date);
    }

    public static String getStringFromDate(Date date, String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
        return getFormattedDate(date, format);
    }

}
