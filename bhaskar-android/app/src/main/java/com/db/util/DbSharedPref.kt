package com.db.util

import android.content.Context
import com.db.data.models.PromoWidgetModel

class DbSharedPref {

    companion object {
        fun isPromoWidgetModelVisible(context: Context): Boolean {
            return AppPreferences.getInstance(context)
                    .getBooleanValue(QuickPreferences.KEY_PROMO_WIDGET_VISIBILITY_STATUS, true)
        }

        fun setPromoWidgetVisibilityStatus(context: Context, willBeVisible: Boolean) {
            AppPreferences.getInstance(context)
                    .setBooleanValue(QuickPreferences.KEY_PROMO_WIDGET_VISIBILITY_STATUS, willBeVisible)
        }

        fun getPromoWidgetModel(context: Context): PromoWidgetModel? {
            return AppPreferences.getInstance(context).getObjectFromGsonString(QuickPreferences.KEY_PROMO_WIDGET_MODEL,
                    PromoWidgetModel::class.java)
        }

        fun isDrawOverOtherAppsPermissionAsked(context: Context): Boolean {
            return AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.KEY_DRAW_OVER_OTHER_APPS, false)
        }

        fun setDrawOverOtherAppsPermissionAsked(context: Context) {
            AppPreferences.getInstance(context).setBooleanValue(QuickPreferences.KEY_DRAW_OVER_OTHER_APPS, true)
        }
    }
}