package com.db.util;

import android.app.Activity;

public interface DialogResponseInterface {
	void doOnPositiveBtnClick(Activity activity);
	void doOnNegativeBtnClick(Activity activity);
}
