package com.db.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.widget.ImageSwitcher;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bhaskar.util.CommonConstants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.AppWidgetTarget;
import com.bumptech.glide.request.target.NotificationTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.bhaskar.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class ImageUtil {

    public static final String defaultURL = "";

    public static void setImage(Context context, String imageUrl, ImageView imageView, int placeholder) {
        try {
            if (imageUrl.endsWith(".gif")) {
                setImageGif(context, imageUrl, imageView, placeholder);
            } else {
                setImageJpeg(context, imageUrl, imageView, placeholder);
            }
        } catch (Exception ignored) {
            setImageJpeg(context, imageUrl, imageView, placeholder);
        }
    }

    public static void setImageWithThumbnail(Context context, String imageUrl, String thumbnailUrl, ImageView imageView, int placeholder, int error) {
        try {
            if (imageUrl.endsWith(".gif")) {
                setImageGif(context, imageUrl, imageView, placeholder);
            } else {
                setImageJpegWithThumbnail(context, imageUrl, thumbnailUrl, imageView, placeholder, error);
            }
        } catch (Exception ignored) {
            setImageJpeg(context, imageUrl, imageView, placeholder);
        }
    }

    private static void setImageJpegWithThumbnail(Context context, String imageUrl, String thumbnailUrl, ImageView imageView, int placeholder, int error) {
        try {
            if (context == null)
                return;

            RequestBuilder<Drawable> thumbnailRequest = Glide
                    .with(context)
                    .load(thumbnailUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);

            GlideUrl url = HttpHeader.getUrl(imageUrl);

            if (placeholder == 0) {
                Glide.with(context)
                        .load(url == null ? defaultURL : url)
                        .thumbnail(thumbnailRequest)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            } else {
                Glide.with(context)
                        .load(url == null ? defaultURL : url)
                        .thumbnail(thumbnailRequest)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(placeholder)
                        .error(error)
                        .into(imageView);
            }
        } catch (Exception ignored) {
        }
    }


    private static void setImageJpeg(Context context, String imageUrl, ImageView imageView, int placeholder) {
        try {
            if (context == null)
                return;

            GlideUrl url = HttpHeader.getUrl(imageUrl);

            if (placeholder == 0) {
                Glide.with(context)
                        .load(url == null ? defaultURL : url)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            } else {
                Glide.with(context)
                        .load(url == null ? defaultURL : url)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(placeholder)
                        .error(placeholder)
                        .into(imageView);
            }
        } catch (Exception ignored) {
        }
    }

    public static void setImage(Context context, String imageUrl, ImageView imageView, int placeholder, int error) {
        try {
            if (imageUrl.endsWith(".gif")) {
                setImageGif(context, imageUrl, imageView, placeholder, error);
            } else {
                setImageJpeg(context, imageUrl, imageView, placeholder, error);
            }
        } catch (Exception ignored) {
            setImageJpeg(context, imageUrl, imageView, placeholder, error);
        }
    }

    private static void setImageJpeg(Context context, String imageUrl, ImageView imageView, int placeholder, int error) {
        try {
            if (context == null)
                return;

            GlideUrl url = HttpHeader.getUrl(imageUrl);

            if (placeholder == 0) {
                Glide.with(context)
                        .load(url == null ? defaultURL : url)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            } else {
                Glide.with(context)
                        .load(url == null ? defaultURL : url)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(placeholder)
                        .error(error)
                        .into(imageView);
            }
        } catch (Exception ignored) {
        }
    }

    public static void setImageWithServerImageDimention(Context context, String imageUrl, ImageView imageView, int placeholder) {
        try {
            imageView.layout(0, 0, 0, 0);
            if (context == null)
                return;

            GlideUrl url = HttpHeader.getUrl(imageUrl);

            if (placeholder == 0) {
                Glide.with(context)
                        .load(url == null ? defaultURL : url)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            } else {
                Glide.with(context)
                        .load(url == null ? defaultURL : url)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(placeholder)
                        .error(placeholder)
                        .into(imageView);
            }
        } catch (Exception ignored) {
        }
    }

    public static void setImage(Context context, int resId, ImageView imageView, int placeholder) {
        try {
            if (context == null)
                return;

            if (placeholder == 0) {
                Glide.with(context)
                        .load(resId)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            } else {
                Glide.with(context)
                        .load(resId)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(placeholder)
                        .error(placeholder)
                        .into(imageView);
            }
        } catch (Exception ignored) {
        }
    }

    public static void setImageBlur(Context context, String imageUrl, ImageView imageView, int placeholder) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .load(url == null ? defaultURL : url)
                    .placeholder(placeholder)
                    .error(placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transform(new jp.wasabeef.glide.transformations.BlurTransformation(30, 2))
                    .into(imageView);
        } catch (Exception ignored) {
        }
    }

    public static void setImageOverride(Context context, String imageUrl, ImageView imageView, int placeholder) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .load(url == null ? defaultURL : url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(com.bumptech.glide.request.target.Target.SIZE_ORIGINAL, com.bumptech.glide.request.target.Target.SIZE_ORIGINAL)
                    .into(imageView);
        } catch (Exception ignored) {
        }
    }

    public static void setImageNotification(Context context, String imageUrl, NotificationTarget imageView, int placeholder) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asBitmap()
                    .load(url == null ? defaultURL : url)
                    .placeholder(placeholder)
                    .error(placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        } catch (Exception ignored) {
        }
    }

    public static void setImageGif(Context context, String imageUrl, ImageView gifImageView, int i) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asGif()
                    .load(url == null ? defaultURL : url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(i)
                    .into(gifImageView);
        } catch (Exception ignored) {
        }
    }

    public static void setImageGif(Context context, String imageUrl, ImageView gifImageView, int i, int error) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asGif()
                    .load(url == null ? defaultURL : url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(i)
                    .error(error)
                    .into(gifImageView);
        } catch (Exception ignored) {
        }
    }

    public static void setImageWidget(Context context, String imageUrl, AppWidgetTarget imageView, int placeholder) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asBitmap()
                    .load(url == null ? defaultURL : url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        } catch (Exception ignored) {
        }
    }

//    public static void setImageAfterDownload(final Context context, String imageUrl, final ImageView imageView, int placeholder) {
//        try {
//            GlideUrl url = HttpHeader.getUrl(imageUrl);
//
//            GlideApp.with(context)
//                    .asBitmap()
//                    .load(url == null ? defaultURL : url)
//                    .placeholder(placeholder)
//                    .into(new BitmapImageViewTarget(imageView) {
//                        @Override
//                        protected void setResource(Bitmap resource) {
//                            imageView.setImageBitmap(resource);
//                        }
//                    });
//        } catch (Exception ignored) {
//        }
//    }

    public static void shareImageAfterDownload(final Context context, String imageUrl, String subject) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asBitmap()
                    .load(url == null ? defaultURL : url)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("image/*");
                            i.putExtra(Intent.EXTRA_SUBJECT, subject);
                            i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(context, resource));
                            context.startActivity(Intent.createChooser(i, "Share Image"));
                        }
                    });
        } catch (Exception ignored) {
        }
    }

    public static Uri getLocalBitmapUri(Context context, Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "db_share_image_" + System.currentTimeMillis() + ".jpg");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static void setImageImageSwitcher(final Context context, String imageUrl, final ImageSwitcher imageView, int placeholder) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asBitmap()
                    .load(url == null ? defaultURL : url)
                    .placeholder(placeholder)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            imageView.setImageDrawable(new BitmapDrawable(resource));
                        }
                    });
        } catch (Exception ignored) {
        }
    }

//    public static class HttpHeader {
//
//        public static final String USER_AGENT = CommonConstants.USER_AGENT;
////        public static final String APP_AGENT = "XYZ";
//
//
//        public static GlideUrl getUrl(String url) {
//            try {
//                if (TextUtils.isEmpty(url)) {
//                    return null;
//                } else return new GlideUrl(url, new LazyHeaders.Builder()
//                        .addHeader("User-Agent", USER_AGENT)
////                    .addHeader("App-agent", APP_AGENT)
//                        .build());
//            } catch (Exception e) {
//                return new GlideUrl(url);
//            }
//
//        }
//    }


    public static class HttpHeader {
        private static final String USER_AGENT = CommonConstants.USER_AGENT;
        private static LazyHeaders lazyHeaders;

        public static GlideUrl getUrl(String url) {
            try {
                if (lazyHeaders == null) {
                    lazyHeaders = new LazyHeaders.Builder().addHeader("User-Agent", USER_AGENT).build();
                }

                return new GlideUrl(url, lazyHeaders);
            } catch (Exception e) {
                try {
                    return new GlideUrl(url);
                } catch (Exception ignored) {
                }
            }

            return null;
        }
    }

    /***
     *
     * @param context
     * @param imageURL
     * @return
     */
    public static Bitmap getBitmap(@NonNull Context context, @NonNull String imageURL) {
        Bitmap bitmap = null;
        try {
            Drawable drawable = Glide.
                    with(context).
                    load(imageURL).
                    into(100, 100).
                    get();
            if (drawable instanceof BitmapDrawable) {
                bitmap = ((BitmapDrawable) drawable).getBitmap();
            } else if (drawable instanceof GifDrawable) {
                bitmap = ((GifDrawable) drawable).getFirstFrame();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
