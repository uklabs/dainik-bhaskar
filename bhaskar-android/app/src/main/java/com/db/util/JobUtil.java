package com.db.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import com.bhaskar.R;
import com.db.home.AutoStartDialogActivity;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class JobUtil {

    public static boolean getNotificationIssueDevice(Context context) {
        String manufacturer = android.os.Build.MANUFACTURER;
        manufacturer = manufacturer.toLowerCase();

        if (manufacturer.equalsIgnoreCase("oppo") || manufacturer.equalsIgnoreCase("xiaomi_0")) {
            return true;

        } else {
            Intent intent = new Intent();
            switch (manufacturer) {
                case "xiaomi":
                    intent.setClassName("com.miui.securitycenter", "com.miui.appmanager.ApplicationsDetailsActivity");
                    break;

                case "vivo":
                    intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
                    break;
            }

            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                return true;
            }
        }

        return false;
    }

    public static void autoStart(Context context) {
        String manufacturer = android.os.Build.MANUFACTURER;
        manufacturer = manufacturer.toLowerCase();

        if (manufacturer.equalsIgnoreCase("xiaomi_0") || manufacturer.equalsIgnoreCase("oppo")) {
//            Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + context.getPackageName()));
//            myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
//            myAppSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            context.startActivity(myAppSettings);
            Intent intent = new Intent(context, AutoStartDialogActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("img_scroll", false);
            intent.putExtra("launch_settings", true);
            context.startActivity(intent);

        } else {
            boolean launch = false;

            Intent intent = new Intent();
            switch (manufacturer) {
                case "xiaomi":
//                    intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
//                    intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.appmanager.ApplicationsDetailsActivity"));
                    intent.setClassName("com.miui.securitycenter", "com.miui.appmanager.ApplicationsDetailsActivity");
                    intent.putExtra("package_name", context.getPackageName());
                    intent.putExtra("package_label", context.getString(R.string.app_name));
                    break;

                case "oppo":
                    intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
//                    intent.setClassName("com.android.settings", "com.android.settings.applications.InstalledAppDetails");
//                    intent.putExtra("package_name", context.getPackageName());
//                    intent.putExtra("package_label", context.getString(R.string.app_name));
                    break;

                case "vivo":
                    intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
                    intent.putExtra("package_name", context.getPackageName());
                    intent.putExtra("package_label", context.getString(R.string.app_name));
                    break;
            }

            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                launch = true;
                context.startActivity(intent);
            }

            if (launch) {
                final String finalManufacturer = manufacturer;
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(context, AutoStartDialogActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("img_scroll", !finalManufacturer.equalsIgnoreCase("xiaomi"));
                        intent.putExtra("launch_settings", false);
                        context.startActivity(intent);
                    }
                }, 1000);
            }
        }
    }
}
