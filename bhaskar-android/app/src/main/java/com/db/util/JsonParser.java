package com.db.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.data.model.Offer;
import com.bhaskar.util.Action;
import com.bhaskar.util.CommonConstants;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.SelectedCityListTable;
import com.db.data.models.BannerInfo;
import com.db.data.models.BrandInfo;
import com.db.data.models.CategoryInfo;
import com.db.data.models.CityInfo;
import com.db.data.models.PrimeBrandInfo;
import com.db.database.BottomNavInfo;
import com.db.database.DatabaseClient;
import com.db.database.HeaderNavInfo;
import com.db.database.TabInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import static com.db.util.Constants.KeyPair.KEY_DATA;
import static com.db.util.Constants.KeyPair.KEY_VERSION;

public class JsonParser {

    public static Map<String, List<CityInfo>> cityListMap = new HashMap<>();
    private static JsonParser jsonParser = null;


    private JsonParser() {
    }

    public static JsonParser getInstance() {
        if (jsonParser == null)
            jsonParser = new JsonParser();
        return jsonParser;
    }


    public List<BrandInfo> getBrandFeedList(Context mContext) {
        boolean isGdprBlockCountry = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        return DatabaseClient.getInstance(mContext).getBrandList(isGdprBlockCountry);
    }

    public List<PrimeBrandInfo> getPrimeBrandFeedList(Context mContext) {
        return DatabaseClient.getInstance(mContext).getPrimeBrandList();
    }

    public boolean isCatActionAvailable(Context context, String actionName) {
        return DatabaseClient.getInstance(context).isCategoryActionAvailable(actionName);
    }

    public CategoryInfo getCategoryInfoBasedOnId(Context mContext, String categoryId) {
        CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryByIdAndBrandId(categoryId);
        return (categoryInfo == null) ? new CategoryInfo() : categoryInfo;
    }

    public CategoryInfo getCategoryInfoBasedOnAction(Context mContext, String action) {
        boolean isBlockCountry = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        CategoryInfo categoryInfo = DatabaseClient.getInstance(mContext).getCategoryByAction(isBlockCountry, action);
        return (categoryInfo == null) ? new CategoryInfo() : categoryInfo;
    }

    public boolean isCategoryAvailable(Context mContext, String categoryId) {
        boolean isBlockCountry = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        return DatabaseClient.getInstance(mContext).isCategoryIdAvailable(isBlockCountry, categoryId);
    }

    public CategoryInfo getCategoryInfoById(Context context, String categoryId) {
        return DatabaseClient.getInstance(context).getCategoryById(categoryId);
    }

    public CategoryInfo getCategoryInfoByIdForBottom(Context context, String categoryId) {
        return DatabaseClient.getInstance(context).getCategoryInfoByIdForBottom(categoryId);
    }

    public CategoryInfo getSuperParentCategoryInfoById(Context context, String parentId, String categoryId) {
        CategoryInfo superCategoryInfo = DatabaseClient.getInstance(context).getCategoryById(parentId);
        return superCategoryInfo;
    }

    public CategoryInfo getSuperParentCategoryInfoById(Context context, String categoryId) {
        CategoryInfo superCategoryInfo = DatabaseClient.getInstance(context).getSuperParentCategoryById(categoryId);
        return superCategoryInfo;
    }


    public List<CityInfo> getCityFeedList(Context mContext, String channelId) {
        try {
            if (!cityListMap.containsKey(channelId)) {
                List<CityInfo> cityInfoList;
                String jsonString = JsonPreferences.getInstance(mContext).getStringValue(String.format(QuickPreferences.JsonPreference.PREFERRED_CITY_FEED_JSON, channelId), "");
                jsonString = jsonString.replaceAll("\r", "");
                jsonString = jsonString.replaceAll("\n", "");
                if (!TextUtils.isEmpty(jsonString)) {
                    try {
                        JSONArray jsonArray = new JSONObject(jsonString).getJSONArray(Constants.KeyPair.KEY_DATA);
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<CityInfo>>() {
                        }.getType();
                        cityInfoList = gson.fromJson(jsonArray.toString(), type);
                        cityListMap.put(channelId, cityInfoList);
                    } catch (Exception ignored) {
                    }
                }
            }
        } catch (Exception ignored) {

        }
        if (cityListMap == null) {
            cityListMap = new HashMap<>();
            return new ArrayList<>();
        } else if (cityListMap.containsKey(channelId))
            return cityListMap.get(channelId);
        else
            return new ArrayList<>();
    }

    public void prepareCityList(final Context context, final String channelId) {
        @SuppressLint("StaticFieldLeak")
        class PrepareCityData extends AsyncTask<Void, Void, List<CityInfo>> {

            @Override
            protected void onPreExecute() {
            }

            @Override
            protected List<CityInfo> doInBackground(Void... params) {
                return JsonParser.getInstance().getCityFeedList(context, channelId);
            }

            @Override
            protected void onPostExecute(List<CityInfo> cityInfoArrayList) {
            }
        }
        new PrepareCityData().execute();

        boolean isPreferredCitySet = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.IS_PREFERRED_CITY_SET, false);
        if (!isPreferredCitySet)
            setDefaultCities(context);

    }

    private void setDefaultCities(Context context) {
        Vector<CityInfo> defaultCities = analyzePreferredCity(context, CommonConstants.CHANNEL_ID);
        SelectedCityListTable selectedCityListTable = (SelectedCityListTable) DatabaseHelper.getInstance(context).getTableObject(SelectedCityListTable.TABLE_NAME);
        selectedCityListTable.deleteAllData();
        selectedCityListTable.addselectedCityListItem(CommonConstants.CHANNEL_ID, defaultCities);
        AppPreferences.getInstance(context).setBooleanValue(QuickPreferences.IS_PREFERRED_CITY_SET, true);
    }

    public Vector<CityInfo> analyzePreferredCity(Context context, String channelId) {
        Vector<CityInfo> cityInfoVector = new Vector<>();
        List<CityInfo> cityInfoArrayList = JsonParser.getInstance().getCityFeedList(context, channelId);
        try {
            String preferredCity = AppPreferences.getInstance(context).getStringValue(String.format(QuickPreferences.DEFAULT_PREFERRED_CITY_V2, channelId), "");
            JSONArray jsonArray = new JSONArray(preferredCity);

            CityInfo cityInfo;
            String id;
            for (int i = 0; i < jsonArray.length(); i++) {
                id = jsonArray.getString(i);

                cityInfo = new CityInfo(id, Constants.CityPreference.CITY_INFO_BASED_ON_ID);
                int indexOf = cityInfoArrayList.indexOf(cityInfo);
                if (indexOf >= 0) {
                    cityInfoVector.add(cityInfoArrayList.get(indexOf));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cityInfoVector;
    }

    public JSONObject getJsonObjectFromAssets(Context context, String fileName) throws
            IOException, JSONException {
        AssetManager manager = context.getAssets();
        InputStream file = manager.open(fileName);
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new JSONObject(new String(formArray));
    }

    public int getVersionOfParticularAPI(Context context, String apiName) {
        int apiVersion = 0;
        String appVersionJsonString = JsonPreferences.getInstance(context).getStringValue(QuickPreferences.JsonPreference.APP_VERSION_JSON, "");
        try {
            if (!appVersionJsonString.isEmpty()) {
                JSONObject appVersionJsonObject = new JSONObject(appVersionJsonString);
                JSONArray jsonElements = appVersionJsonObject.getJSONArray(KEY_DATA);
                for (int i = 0; i < jsonElements.length(); i++) {
                    JSONObject jsonObject = jsonElements.getJSONObject(i);
                    if (jsonObject.optString(Constants.ApiName.API_NAME).equalsIgnoreCase(apiName)) {
                        try {
                            apiVersion = Integer.parseInt(jsonObject.optString(Constants.ApiName.API_VERSION));
                        } catch (NumberFormatException e) {
                            apiVersion = 0;
                        }
                        break;
                    }
                }
            }
        } catch (Exception ignored) {

        }
        return apiVersion;
    }



    public void parseHostJson(Context context, JSONObject baseJsonObject) {
        SharedPreferences appPreferences = context.getSharedPreferences(AppPreferences.NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = appPreferences.edit();
        try {
            JSONObject jsonObject;
            if (baseJsonObject.has(KEY_DATA)) {
                jsonObject = baseJsonObject.getJSONObject(KEY_DATA);
            } else {
                jsonObject = baseJsonObject;
            }

            if (jsonObject.has("log_feed")) {
                edit.putString(QuickPreferences.APP_LOG_FEED_BASE_URL, jsonObject.optJSONObject("log_feed").optString("value"));
            }
            if (jsonObject.has("feeds")) {
                edit.putString(QuickPreferences.APP_FEED_BASE_URL, jsonObject.optJSONObject("feeds").optString("value"));
            }


            /*GA_UAID*/
            if (jsonObject.has("ga")) {
                JSONObject appWallJsonObject = jsonObject.getJSONObject("ga");
                if (appWallJsonObject != null) {
                    String gaUaid = appWallJsonObject.optString("uaid");
                    edit.putString(QuickPreferences.GA_UAID, gaUaid);
                    TrackingHost.getInstance(context).setGoogleAnalyticsId(gaUaid);
                }
            }

            if (jsonObject.has("comscore")) {
                String secretId = jsonObject.getJSONObject("comscore").optString("secret_code");
                String publisherId = jsonObject.getJSONObject("comscore").optString("account");
                TrackingHost.getInstance(context).setComscoreIds(secretId, publisherId);
            }
            if (jsonObject.has("image_notify")) {
                edit.putString(QuickPreferences.APP_IMAGE_NOTIFY_URL, jsonObject.optJSONObject("image_notify").optString("value"));
            }
            if (jsonObject.has("video_notify")) {
                edit.putString(QuickPreferences.APP_VIDEO_NOTIFICATION_URL, jsonObject.optJSONObject("video_notify").optString("value"));
            }
            if (jsonObject.has("tracking_wisdom")) {
                TrackingHost.getInstance(context).setWisdomUrl(jsonObject.optJSONObject("tracking_wisdom").optString("value"));
            }
            if (jsonObject.has("tracking_wisdom_notification")) {
                TrackingHost.getInstance(context).setWisdomNotificationUrl(jsonObject.optJSONObject("tracking_wisdom_notification").optString("value"));
            }
            if (jsonObject.has("tracking_vendor_notification")) {
                TrackingHost.getInstance(context).setBluePiWisdomNotificationUrl(jsonObject.optJSONObject("tracking_vendor_notification").optString("value"));
            }
            if (jsonObject.has("appsource_wisdom")) {
                TrackingHost.getInstance(context).setWisdomAppSourceUrl(jsonObject.optJSONObject("appsource_wisdom").optString("value"));
            }
            if (jsonObject.has("login_feed")) {
                edit.putString(QuickPreferences.APP_LOGIN_FEED_URL, jsonObject.optJSONObject("login_feed").optString("value"));
            }
            if (jsonObject.has("new_brand_icon")) {
                edit.putString(QuickPreferences.JsonPreference.PRIME_NEW_ICON_URL, jsonObject.optJSONObject("new_brand_icon").optString("url"));
            }
            if (jsonObject.has("article_action")) {
                edit.putString(QuickPreferences.JsonPreference.ARTICLE_ACTION_URL, jsonObject.optJSONObject("article_action").optString("value"));
            }
            if (jsonObject.has("contest_selfie_detail_url")) {
                edit.putString(QuickPreferences.JsonPreference.CONTEST_SELFIE_DETAIL_URL, jsonObject.optJSONObject("contest_selfie_detail_url").optString("value"));
            }
            if (jsonObject.has("tracking_wisdom_video")) {
                edit.putString(QuickPreferences.AD_IMPRESSION_TRACK_URL, jsonObject.optJSONObject("tracking_wisdom_video").optString("value"));
            }
            if (jsonObject.has("get_apps")) {
                edit.putString(QuickPreferences.GET_APPS_URL, jsonObject.optJSONObject("get_apps").optString("value"));
            }
            if (jsonObject.has("impression_click_tracking")) {
                edit.putString(QuickPreferences.IMPRESSION_TRACKING_URL, jsonObject.optJSONObject("impression_click_tracking").optString("value"));
            }
            if (jsonObject.has("news_flash")) {
                edit.putString(QuickPreferences.NEWS_TICKER_URL, jsonObject.optJSONObject("news_flash").optString("value"));
            }
            if (jsonObject.has("event_feed")) {
                edit.putString(QuickPreferences.EVENT_FEED_BASE_URL, jsonObject.optJSONObject("event_feed").optString("value"));
            }

            edit.commit();
        } catch (Exception ignored) {
        }
    }

    public void parseBaseAppControlJson(Context context, JSONObject baseJsonObject) {
        SharedPreferences preferences = context.getSharedPreferences(AppPreferences.NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        JSONObject jsonObject;
        try {
            if (baseJsonObject.has(KEY_DATA)) {
                jsonObject = baseJsonObject.getJSONObject(KEY_DATA);
            } else {
                jsonObject = baseJsonObject;
            }

            /* App Update */
            if (jsonObject.has("update_popup")) {
                JSONObject updatePopupJsonObject = jsonObject.getJSONObject("update_popup");
                if (updatePopupJsonObject != null) {
                    String app_version = updatePopupJsonObject.optString("version");
                    String force_update = updatePopupJsonObject.optString("force_update");
                    String message = updatePopupJsonObject.optString("message");
                    String installMessage = updatePopupJsonObject.optString("install_message");
                    editor.putInt(QuickPreferences.UPDATE_POPUP_VERSION_CODE, Integer.parseInt(app_version));
                    editor.putInt(QuickPreferences.FORCE_UPDATE_VERSION, Integer.parseInt(force_update));
                    editor.putString(QuickPreferences.UPDATE_POPUP_MESSAGE, message);
                    editor.putString(QuickPreferences.UPDATE_POPUP_INSTALL_MESSAGE, installMessage);
                }
            }

            /*Preferred city defaults*/
            if (jsonObject.has("preferredcity")) {
                JSONObject preferredCityV2JsonObject = jsonObject.optJSONObject("preferredcity");
                Iterator<String> keys = preferredCityV2JsonObject.keys();
                while (keys.hasNext()) {
                    String keyName = keys.next();
                    editor.putString(String.format(QuickPreferences.DEFAULT_PREFERRED_CITY_V2, keyName), preferredCityV2JsonObject.optString(keyName));
                }
            }

            /*Feed Changes*/
            if (jsonObject.has("network")) {
                JSONObject feedJsonObject = jsonObject.getJSONObject("network");
                int cacheTime;
                try {
                    cacheTime = Integer.valueOf(feedJsonObject.optString("cache_time").trim());
                } catch (NumberFormatException e) {
                    cacheTime = 5;
                }
                Constants.FEED_CACHE_TIME = cacheTime * 60 * 1000;
                editor.putInt(QuickPreferences.FEED_CACHE_TIME, cacheTime);
            }

            /*Bottom Nav Hide/Show Toggle*/
            if (jsonObject.has("bottom_nav")) {
                JSONObject bottomNavJsonObject = jsonObject.getJSONObject("bottom_nav");
                if (bottomNavJsonObject != null) {
                    boolean isActive = (bottomNavJsonObject.optString("hideable").equalsIgnoreCase("1"));
                    editor.putBoolean(QuickPreferences.TOGGLING_BOTTOM_NAV_HIDEABLE_SERVER, isActive);
                }
            }

            /*Video Auto Play Toggle*/
            if (jsonObject.has("video_auto_play")) {
                JSONObject videoAutoPlayJsonObject = jsonObject.getJSONObject("video_auto_play");
                if (videoAutoPlayJsonObject != null) {
                    boolean isActive = (videoAutoPlayJsonObject.optString("autoNextPlay").equalsIgnoreCase("1"));
                    editor.putBoolean(QuickPreferences.TOGGLING_VIDEO_AUTO_NEXT_PLAY, isActive);
                }
            }

            /*ButtonTopWidget*/
            if (jsonObject.has("button_top_widget")) {
                JSONObject buttonTopWidgetJO = jsonObject.optJSONObject("button_top_widget");
                if (buttonTopWidgetJO != null) {
                    boolean isActive = (buttonTopWidgetJO.optString("isactive").equalsIgnoreCase("1"));
                    editor.putBoolean(QuickPreferences.ButtonTopWGTPref.IS_ACTIVE, isActive);
                    String bannerUrl = buttonTopWidgetJO.optString("banner_url");
                    String addedMenu = buttonTopWidgetJO.optString("added_on_menu");
                    editor.putString(QuickPreferences.ButtonTopWGTPref.BANNER_URL, bannerUrl);
                    editor.putString(QuickPreferences.ButtonTopWGTPref.ADDED_MENU, addedMenu);
                }
            }

            /*Article Iframe*/
            if (jsonObject.has("article_iframe")) {
                JSONObject homeIframeJsonObject = jsonObject.getJSONObject("article_iframe");
                if (homeIframeJsonObject != null) {
                    int article_iframe_pos = 0;
                    try {
                        article_iframe_pos = Integer.parseInt(homeIframeJsonObject.optString("placement_pos"));
                    } catch (NumberFormatException ignored) {
                    }
                    String article_iframe_url = homeIframeJsonObject.optString("url");
                    String article_iframe_action_url = homeIframeJsonObject.optString("action_url");
                    String article_iframe_action_name = homeIframeJsonObject.optString("action_name");
                    String article_iframe_ga_event = homeIframeJsonObject.optString("ga_event");
                    String article_iframe_ga_screen = homeIframeJsonObject.optString("ga_screen");
                    String isActive = homeIframeJsonObject.optString("isactive");
                    boolean article_iframe_isActive = (isActive.equalsIgnoreCase("1"));
                    String islogin_req = homeIframeJsonObject.optString("islogin_req");
                    boolean article_iframe_login_req = (islogin_req.equalsIgnoreCase("1"));
                    editor.putInt(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_POSITION, article_iframe_pos);
                    editor.putString(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_URL, article_iframe_url);
                    editor.putString(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_ACTION_URL, article_iframe_action_url);
                    editor.putString(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_ACTION_NAME, article_iframe_action_name);
                    editor.putString(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_GA_EVENT, article_iframe_ga_event);
                    editor.putString(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_GA_SCREEN, article_iframe_ga_screen);
                    editor.putBoolean(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_IS_ACTIVE, article_iframe_isActive);
                    editor.putBoolean(QuickPreferences.WidgetPrefs.ARTICLE_IFRAME_LOGIN_REQUIRED, article_iframe_login_req);
                }
            }

            /*Notification Stack*/
            if (jsonObject.has("stack")) {
                JSONObject stackJsonObject = jsonObject.getJSONObject("stack");
                if (stackJsonObject != null) {
                    boolean isStackActive = stackJsonObject.optString("isactive").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.StackPrefs.TOGGLING_STACK_ACTIVE, isStackActive);
                }
            }
            /*Mega Offer*/
            if (jsonObject.has("mega_offer")) {
                JSONObject stackJsonObject = jsonObject.getJSONObject("mega_offer");
                if (stackJsonObject != null) {
                    boolean isStackActive = stackJsonObject.optString("is_active").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.TOGGLING_MEGA_OFFER, isStackActive);
                }
            }
            /*home Banner*/

            if (jsonObject.has("ATFHP_Banner")) {
                JSONObject atfhpBannerObject = jsonObject.getJSONObject("ATFHP_Banner");
                if (atfhpBannerObject != null) {
                    boolean isATFHPBannerActive = atfhpBannerObject.optString("isactive").equalsIgnoreCase("1");
                    String ATFHP_BannerActionUrl = atfhpBannerObject.optString("action_url");
                    String ATFHP_BannerActionName = atfhpBannerObject.optString("action_name");
                    String ATFHP_BannerActionBrandId = atfhpBannerObject.optString("action_brand_id");
                    String ATFHP_BannerActionMenuId = atfhpBannerObject.optString("action_menu_id");
                    String ATFHP_BannerGaEvent = atfhpBannerObject.optString("ga_event");
                    String ATFHP_BannerGaScreen = atfhpBannerObject.optString("ga_screen");
                    String ATFHP_BannerUrl = atfhpBannerObject.optString("banner_url");
                    boolean isImageBannerEnable = atfhpBannerObject.optString("enable_img_banner").equalsIgnoreCase("1");

                    editor.putBoolean(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_ACTIVE, isATFHPBannerActive);
                    editor.putBoolean(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_IS_IMG_ENABLE, isImageBannerEnable);
                    editor.putString(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_ACTION_URL, ATFHP_BannerActionUrl);
                    editor.putString(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_ACTION_NAME, ATFHP_BannerActionName);
                    editor.putString(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_ACTION_BRAND_ID, ATFHP_BannerActionBrandId);
                    editor.putString(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_ACTION_MENU_ID, ATFHP_BannerActionMenuId);
                    editor.putString(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_GA_EVENT, ATFHP_BannerGaEvent);
                    editor.putString(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_GA_SCREEN, ATFHP_BannerGaScreen);
                    editor.putString(QuickPreferences.ATF_HP_BannerPrefs.ATFHP_BANNER_URL, ATFHP_BannerUrl);
                }
            }

            /*video*/
            if (jsonObject.has("video")) {
                JSONObject videoObject = jsonObject.getJSONObject("video");
                if (videoObject != null) {
                    String providerUrl = videoObject.optString("provider_url");
                    String feedUrl_v2 = videoObject.optString("feed_url");
                    String detailUrl_v2 = videoObject.optString("detail_url");

                    String videoActionUrl = videoObject.optString("video_action_url");
                    boolean isFeedUrlPersonalized = videoObject.optString("is_feed_url_personalized").equalsIgnoreCase("1");

                    editor.putString(QuickPreferences.VideoPref.SETTINGS_VIDEO_PROVIDER_URL, providerUrl);
//                    editor.putString(QuickPreferences.VideoPref.SETTINGS_VIDEO_FEED_URL_V2, feedUrl_v2);
                    editor.putString(QuickPreferences.VideoPref.SETTINGS_VIDEO_DETAIL_URL_V2, detailUrl_v2);

                    editor.putString(QuickPreferences.VideoPref.SETTINGS_VIDEO_ACTION, videoActionUrl);
                    editor.putBoolean(QuickPreferences.VideoPref.SETTINGS_IS_VIDEO_FEED_URL_PERSONALIZED, isFeedUrlPersonalized);
                }
            }

            /*Home Last open Tab*/
            if (jsonObject.has("home_last_pref")) {
                JSONObject lastOpenTabJsonObject = jsonObject.getJSONObject("home_last_pref");
                if (lastOpenTabJsonObject != null) {
                    boolean isLastTabOpenActive = lastOpenTabJsonObject.optString("is_active").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.LastTabPrefs.TOGGLING_LAST_TAB_ACTIVE, isLastTabOpenActive);
                }
            }

            /*Facebook login check*/
            if (jsonObject.has("login_fb_btn")) {
                JSONObject lastOpenTabJsonObject = jsonObject.getJSONObject("login_fb_btn");
                if (lastOpenTabJsonObject != null) {
                    boolean isDisabled = lastOpenTabJsonObject.optString("isdisable").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.IS_FACEBOOK_LOGIN_DISABLED, isDisabled);
                }
            }

            /* Widget Control */
            if (jsonObject.has("widgets_control")) {
                JSONObject widgetsControlJsonObject = jsonObject.getJSONObject("widgets_control");
                if (widgetsControlJsonObject != null) {
                    String app_widget_feed = widgetsControlJsonObject.optString("app_widget_feed");
                    editor.putString(QuickPreferences.WidgetPrefs.APP_WIDGET_FEED, app_widget_feed);
                }
            }

            /*flicker data*/
            if (jsonObject.has("flicker_widget")) {
                JSONObject flickerJsonObject = jsonObject.optJSONObject("flicker_widget");
                if (flickerJsonObject != null) {
                    boolean isFlickerActive = flickerJsonObject.optString("isactive").equalsIgnoreCase("1");
                    String flickerApi = flickerJsonObject.optString("api");
                    String flickerAddedOnMenu = flickerJsonObject.optString("added_on_menu");
                    String flickerAddedOnHome = flickerJsonObject.optString("added_on_home");
                    String color = flickerJsonObject.optString("bg_color");
                    String textColor = flickerJsonObject.optString("txt_color");
                    String subjectColor = flickerJsonObject.optString("subject_color");
                    int flickerHomePlacementPos = Integer.parseInt(flickerJsonObject.optString("placement_pos_home"));
                    int flickerMenuPlacementPos = Integer.parseInt(flickerJsonObject.optString("placement_pos_menu"));

                    editor.putBoolean(QuickPreferences.FlickerPrefs.FLICKER_IS_ACTIVE, isFlickerActive);
                    editor.putString(QuickPreferences.FlickerPrefs.FLICKER_API, flickerApi);
                    editor.putString(QuickPreferences.FlickerPrefs.FLICKER_ADDED_ON_MENU, flickerAddedOnMenu);
                    editor.putString(QuickPreferences.FlickerPrefs.FLICKER_ADDED_ON_HOME, flickerAddedOnHome);
                    editor.putInt(QuickPreferences.FlickerPrefs.FLICKER_HOME_POS, flickerHomePlacementPos);
                    editor.putInt(QuickPreferences.FlickerPrefs.FLICKER_MENU_POS, flickerMenuPlacementPos);
                    editor.putString(QuickPreferences.FlickerPrefs.FLICKER_COLOR, color);
                    editor.putString(QuickPreferences.FlickerPrefs.FLICKER_TEXT_COLOR, textColor);
                    editor.putString(QuickPreferences.FlickerPrefs.FLICKER_SUBJECT_COLOR, subjectColor);
                }
            }

            /*Stock widget data*/
            if (jsonObject.has("stock_widget")) {
                JSONObject stockJsonObject = jsonObject.optJSONObject("stock_widget");
                if (stockJsonObject != null) {
                    boolean isActive = stockJsonObject.optString("isactive").equalsIgnoreCase("1");
                    String stockApi = stockJsonObject.optString("api");
                    String stockAddedOnMenu = stockJsonObject.optString("added_on_menu");
                    String stockAddedOnHome = stockJsonObject.optString("added_on_home");
                    int stockHomePlacementPos = 0;
                    if (!TextUtils.isEmpty(stockJsonObject.optString("placement_pos_home"))) {
                        stockHomePlacementPos = Integer.parseInt(stockJsonObject.optString("placement_pos_home"));
                    }

                    int stockMenuPlacementPos = 0;
                    if (!TextUtils.isEmpty(stockJsonObject.optString("placement_pos_menu"))) {
                        stockMenuPlacementPos = Integer.parseInt(stockJsonObject.optString("placement_pos_menu"));
                    }

                    editor.putBoolean(QuickPreferences.StockPrefs.STOCK_IS_ACTIVE, isActive);
                    editor.putString(QuickPreferences.StockPrefs.STOCK_API, stockApi);
                    editor.putString(QuickPreferences.StockPrefs.STOCK_ADDED_ON_HOME, stockAddedOnHome);
                    editor.putString(QuickPreferences.StockPrefs.STOCK_ADDED_ON_MENU, stockAddedOnMenu);
                    editor.putInt(QuickPreferences.StockPrefs.STOCK_HOME_POS, stockHomePlacementPos);
                    editor.putInt(QuickPreferences.StockPrefs.STOCK_MENU_POS, stockMenuPlacementPos);
                }
            }
            /*Offer version Stack*/
            if (jsonObject.has("offer_popup")) {
                JSONObject stackJsonObject = jsonObject.getJSONObject("offer_popup");
                if (stackJsonObject != null) {
                    String version = stackJsonObject.optString("version");
                    editor.putString(Action.CategoryAction.CAT_OFFER_VERSION, version);
                }
            }

            /*ads control*/
            if (jsonObject.has("ads_control")) {
                JSONObject adsJson = jsonObject.getJSONObject("ads_control");
                if (adsJson != null) {
                    boolean isVideoAd = adsJson.optString("video").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.AdPref.VIDEO_TOGGLE, isVideoAd);

                    String listPos = adsJson.optString("list_pos");
                    try {
                        String[] listPosArr = listPos.split(",");
                        for (int i = 0; i < listPosArr.length; i++) {
                            switch (i) {
                                case 0:
                                    editor.putInt(QuickPreferences.AdPref.NATIVE_POSITION_1, Integer.valueOf(listPosArr[i]));
                                    break;
                                case 1:
                                    editor.putInt(QuickPreferences.AdPref.NATIVE_POSITION_2, Integer.valueOf(listPosArr[i]));
                                    break;
                                case 2:
                                    editor.putInt(QuickPreferences.AdPref.NATIVE_POSITION_3, Integer.valueOf(listPosArr[i]));
                                    break;
                            }
                        }
                    } catch (Exception e) {
                    }

                    boolean isBannerListAd = adsJson.optString("banner_list_active").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.AdPref.BANNER_LIST_TOGGLE, isBannerListAd);

                    boolean isATFListAd = adsJson.optString("atf_list_active").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.AdPref.ATF_LIST_TOGGLE, isATFListAd);

                    boolean isATFArticleAd = adsJson.optString("atf_article_active").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.AdPref.ATF_ARTICLE_TOGGLE, isATFArticleAd);

                    boolean isBtfRosListAd = adsJson.optString("btf_ros_list_active").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.AdPref.BTF_ROS_LIST_TOGGLE, isBtfRosListAd);

                    boolean isBtfRosArticleAd = adsJson.optString("btf_ros_article_active").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.AdPref.BTF_ROS_ARTICLE_TOGGLE, isBtfRosArticleAd);

                    boolean isVideoBannerAd = adsJson.optString("video_banner").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.AdPref.VIDEO_BANNER_TOGGLE, isVideoBannerAd);

                    boolean isPhotoBannerAd = adsJson.optString("photo_banner").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.AdPref.PHOTO_BANNER_TOGGLE, isPhotoBannerAd);

                    boolean isExitBannerAd = adsJson.optString("exit_banner").equalsIgnoreCase("1");
                    editor.putBoolean(QuickPreferences.AdPref.EXIT_BANNER_TOGGLE, isExitBannerAd);
                }
            }

            if(jsonObject.has("promo_widget")){
                JSONObject promoWidget = jsonObject.optJSONObject("promo_widget");
                editor.putString(QuickPreferences.KEY_PROMO_WIDGET_MODEL, promoWidget.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    private ArrayList<BannerInfo> bannerInfoList = null;

    public void parseBannerV2(Context context) {
        if (bannerInfoList == null) {
            String baseAppControlData = JsonPreferences.getInstance(context).getStringValue(QuickPreferences.JsonPreference.BASE_APP_CONTROL_JSON, "");
            try {
                JSONObject jsonObject = new JSONObject(baseAppControlData);
                JSONArray bannerArray = jsonObject.getJSONArray("bannerV2");
                Type founderListType = new TypeToken<ArrayList<BannerInfo>>() {
                }.getType();
                bannerInfoList = new Gson().fromJson(bannerArray.toString(), founderListType);
            } catch (JSONException ignored) {
            }
        }

        ArrayList<BannerInfo> tempBannerInfos = new ArrayList<>();
        if (bannerInfoList != null && bannerInfoList.size() > 0) {
            for (BannerInfo mBannerInfo : bannerInfoList) {
                if (isBannerHaveToAdd(context, mBannerInfo)) {
                    tempBannerInfos.add(new BannerInfo(mBannerInfo));
                }
            }
            bannerInfoList.clear();
            bannerInfoList.addAll(tempBannerInfos);
        }
    }

    private boolean isBannerHaveToAdd(Context context, BannerInfo mBannerInfo) {
        if (!(TextUtils.isEmpty(mBannerInfo.actionMenuId) || mBannerInfo.actionMenuId.equalsIgnoreCase("0"))) {
            return isCategoryAvailable(context, mBannerInfo.actionMenuId);
        } else {
            return true;
        }
    }

    public ArrayList<BannerInfo> getBannersToAdd(String catId) {
        ArrayList<BannerInfo> bannerInfos = new ArrayList<>();
        if (bannerInfoList != null) {
            bannerInfos = new ArrayList<>();
            for (BannerInfo mBannerInfo : bannerInfoList) {
                if (mBannerInfo.addedOnMenu.equalsIgnoreCase(catId) || mBannerInfo.addedOnMenu.equalsIgnoreCase("0")) {
                    bannerInfos.add(new BannerInfo(mBannerInfo));
                }
            }
        }
        return bannerInfos;
    }


    public void appKill() {
        jsonParser = null;
    }


    public boolean isOfferActionAvailable(Context context) {
        boolean isGdprBlockCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        if (isGdprBlockCountry)
            return false;

        return isCatActionAvailable(context, Action.CategoryAction.CAT_ACTION_OFFER);
    }


    public List<Offer> getAvailableOffers(String jsonString) {
        List<Offer> availableOffers = new ArrayList<>();
        if (!TextUtils.isEmpty(jsonString)) {
            try {
                JSONArray jsonArray = new JSONObject(jsonString).getJSONArray(Constants.KeyPair.KEY_DATA);
                Gson gson = new Gson();
                Type type = new TypeToken<List<Offer>>() {
                }.getType();
                List<Offer> tempBrandInfoList = gson.fromJson(jsonArray.toString(), type);

                for (Offer availableOffer : tempBrandInfoList) {
                    String action = availableOffer.action;
                    if (Action.ALL_OFFER_ACTION.contains(action)) {
                        availableOffers.add(availableOffer);
                    }
                }
            } catch (Exception ignored) {
            }
        }
        return availableOffers;
    }


    public List<TabInfo> getTabList(Context mContext) {
        boolean isBlockCountry = AppPreferences.getInstance(mContext).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        List<TabInfo> tabInfos = DatabaseClient.getInstance(mContext).getTabList(isBlockCountry);

        return tabInfos;
    }

    public List<HeaderNavInfo> getHeaderNavList(Context context) {
        boolean isBlockCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        return DatabaseClient.getInstance(context).getHeaderNavList(isBlockCountry);
    }

    public List<BottomNavInfo> getBottomNavList(Context context) {
        boolean isBlockCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        return DatabaseClient.getInstance(context).getBottomNavList(isBlockCountry);
    }

    public List<BottomNavInfo> getBottomNavMoreList(Context context) {
        boolean isBlockCountry = AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, true);
        return DatabaseClient.getInstance(context).getBottomNavMoreList(isBlockCountry);
    }

    public void storeDefaultJson(Context context) {

        //Host Json
        JSONObject hostJsonObject = null;
        try {
            hostJsonObject = JsonParser.getInstance().getJsonObjectFromAssets(context, Constants.AppStaticJson.HOST_JSON);
            if (hostJsonObject != null) {
                parseAndSaveJson(context, hostJsonObject, Constants.AppHitChoice.APP_HOST_API);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //Base App Control
        JSONObject baseAppControlJsonObject = null;
        try {
            baseAppControlJsonObject = JsonParser.getInstance().getJsonObjectFromAssets(context, Constants.AppStaticJson.BASE_APP_CONTROL_JSON);
            if (baseAppControlJsonObject != null) {
                parseAndSaveJson(context, hostJsonObject, Constants.AppHitChoice.BASE_APP_CONTROL_API);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Category Feed parse
        try {
            JSONObject categoryJsonObject = JsonParser.getInstance().getJsonObjectFromAssets(context, Constants.AppStaticJson.CATEGORY_JSON);
            parseAndSaveJson(context, categoryJsonObject, Constants.AppHitChoice.APP_CATEGORY_API);
        } catch (Exception e) {
        }

        //City Json
        JSONObject cityJsonObject = null;
        try {
            cityJsonObject = JsonParser.getInstance().getJsonObjectFromAssets(context, Constants.AppStaticJson.CITY_JSON);
            if (cityJsonObject != null) {
                parseAndSaveJson(context, cityJsonObject, Constants.AppHitChoice.APP_CITY_API);
            }
        } catch (Exception e) {
        }

        AppPreferences.getInstance(context).setBooleanValue(QuickPreferences.IS_STATIC_JSON_STORED, true);
    }

    public void parseAndSaveJson(Context context, JSONObject response, int appHitValue) {
        switch (appHitValue) {

            case Constants.AppHitChoice.BASE_APP_CONTROL_API:
                JsonPreferences.getInstance(context).setIntValue(QuickPreferences.JsonPreference.BASE_APP_CONTROL_VERSION, AppUtils.getInstance().stringToInt(response.optString(KEY_VERSION)));
                JsonPreferences.getInstance(context).setStringValue(QuickPreferences.JsonPreference.BASE_APP_CONTROL_JSON, response.toString());
                try {
                    JsonParser.getInstance().parseBaseAppControlJson(context, response);
                } catch (Exception e) {
                }
                break;


            case Constants.AppHitChoice.APP_CATEGORY_API:
                int version = AppUtils.getInstance().stringToInt(response.optString(KEY_VERSION));
                JSONObject dataObject = response.optJSONObject(Constants.KeyPair.KEY_DATA);

                // Fantasy Menu
                JSONObject fantasyMenuObject = dataObject.optJSONObject("fantasy_menu");
                JsonPreferences.getInstance(context).setStringValue(QuickPreferences.JsonPreference.FANTASY_MENU_FEED, fantasyMenuObject.toString());

                // Category List
                JSONArray categoryArray = dataObject.optJSONArray("menu");
                CategoryInfo[] catList = new Gson().fromJson(categoryArray.toString(), CategoryInfo[].class);
                DatabaseClient.getInstance(context).deleteAllCategory();
                DatabaseClient.getInstance(context).insertCategory(catList);

                // Header List
                JSONArray headerArray = dataObject.optJSONArray("home_header");
                HeaderNavInfo[] headerList = new Gson().fromJson(headerArray.toString(), HeaderNavInfo[].class);
                DatabaseClient.getInstance(context).deleteAllHeaderNav();
                DatabaseClient.getInstance(context).insertHeaderNav(headerList);

                // Footer List
                JSONArray footerArray = dataObject.optJSONArray("home_footer");
                BottomNavInfo[] footerList = new Gson().fromJson(footerArray.toString(), BottomNavInfo[].class);
                DatabaseClient.getInstance(context).deleteAllBottomNav();
                DatabaseClient.getInstance(context).insertBottomNav(footerList);

                // Footer More List
                JSONArray footerMoreArray = dataObject.optJSONArray("home_more");
                BottomNavInfo[] footerMoreList = new Gson().fromJson(footerMoreArray.toString(), BottomNavInfo[].class);
                DatabaseClient.getInstance(context).deleteAllBottomNavMore();
                DatabaseClient.getInstance(context).insertBottomNavMore(footerMoreList);

                // Tab List
                JSONArray tabArray = dataObject.optJSONArray("home_tab");
                TabInfo[] tabList = new Gson().fromJson(tabArray.toString(), TabInfo[].class);
                DatabaseClient.getInstance(context).deleteAllTab();
                DatabaseClient.getInstance(context).insertTab(tabList);

                // Prime Brand List
                JSONArray brandArray = dataObject.optJSONArray("prime");
                BrandInfo[] brandList = new Gson().fromJson(brandArray.toString(), BrandInfo[].class);
                DatabaseClient.getInstance(context).deleteAllBrand();
                DatabaseClient.getInstance(context).insertBrand(brandList);

                // Prime Gallery List
                JSONArray primeBrandArray = dataObject.optJSONArray("prime_gallery");
                PrimeBrandInfo[] primeBrandList = new Gson().fromJson(primeBrandArray.toString(), PrimeBrandInfo[].class);
                DatabaseClient.getInstance(context).deleteAllPrimeBrand();
                DatabaseClient.getInstance(context).insertPrimeBrand(primeBrandList);

                JsonPreferences.getInstance(context).setIntValue(String.format(QuickPreferences.JsonPreference.CATEGORY_FEED_VERSION, CommonConstants.CHANNEL_ID), version);
                break;

            case Constants.AppHitChoice.APP_CITY_API:
                JsonPreferences.getInstance(context).setIntValue(String.format(QuickPreferences.JsonPreference.PREFERRED_CITY_FEED_VERSION, CommonConstants.CHANNEL_ID), AppUtils.getInstance().stringToInt(response.optString(KEY_VERSION)));
                JsonPreferences.getInstance(context).setStringValue(String.format(QuickPreferences.JsonPreference.PREFERRED_CITY_FEED_JSON, CommonConstants.CHANNEL_ID), response.toString());
                break;

            case Constants.AppHitChoice.APP_HOST_API:
                JsonPreferences.getInstance(context).setIntValue(QuickPreferences.JsonPreference.APP_HOST_VERSION, AppUtils.getInstance().stringToInt(response.optString(KEY_VERSION)));
                JsonPreferences.getInstance(context).setStringValue(QuickPreferences.JsonPreference.APP_HOST_JSON, response.toString());
                JsonParser.getInstance().parseHostJson(context, response);
                break;

        }
    }
}
