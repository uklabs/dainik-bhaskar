package com.db.util;

import android.content.Context;
import android.content.SharedPreferences;

public class JsonPreferences {

    public static final String NAME = "DB_Json_Preference";
    private static JsonPreferences instance = null;
    private SharedPreferences preferences = null;

    private JsonPreferences(Context ctx) {
        if (ctx != null) {
            preferences = ctx.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        }
    }

    public static JsonPreferences getInstance(Context ctx) {
        if (instance == null) {
            instance = new JsonPreferences(ctx);
        }
        return instance;
    }

    public void setIntValue(String key, int value) {
        if (preferences != null)
            preferences.edit().putInt(key, value).commit();
    }

    public int getIntValue(String key, int defaultValue) {
        if (preferences != null)
            return preferences.getInt(key, defaultValue);
        else
            return defaultValue;
    }

    public void setStringValue(String key, String value) {
        if (preferences != null)
            preferences.edit().putString(key, value).commit();
    }

    public String getStringValue(String key, String defaultValue) {
        if (preferences != null)
            return preferences.getString(key, defaultValue);
        else
            return defaultValue;
    }

    public void setBooleanValue(String key, Boolean value) {
        if (preferences != null)
            preferences.edit().putBoolean(key, value).commit();
    }

    public Boolean getBooleanValue(String key, Boolean defaultValue) {
        if (preferences != null)
            return preferences.getBoolean(key, defaultValue);
        else
            return defaultValue;
    }

    public void setFloatValue(String key, Float value) {
        if (preferences != null)
            preferences.edit().putFloat(key, value).commit();
    }

    public Float getFlotValue(String key, Float defaultValue) {
        if (preferences != null)
            return preferences.getFloat(key, defaultValue);
        else
            return defaultValue;
    }

    public void setLongValue(String key, Long value) {
        if (preferences != null)
            preferences.edit().putLong(key, value).commit();
    }

    public long getLongValue(String key, Long defaultValue) {
        if (preferences != null)
            return preferences.getLong(key, defaultValue);
        else
            return defaultValue;
    }
}
