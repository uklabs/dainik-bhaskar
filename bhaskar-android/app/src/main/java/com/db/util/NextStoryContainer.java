package com.db.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class NextStoryContainer extends LinearLayout {

    public NextStoryContainer(@NonNull Context context) {
        super(context);
    }

    public NextStoryContainer(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }

    public NextStoryContainer(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private float y1;
    private float compareDelta = -200f;

    private OnScrollRefreshListener onScrollRefreshListener;

    /***
     *
     * @param onScrollRefreshListener
     */
    public void detectBottomToTopSwipe(@NonNull OnScrollRefreshListener onScrollRefreshListener) {
        this.onScrollRefreshListener = onScrollRefreshListener;

        /*setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onScrollRefreshListener.onBottomToTopSwiped();
            }
        });*/
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            getParent().requestDisallowInterceptTouchEvent(true);
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    y1 = event.getY();
                    return true;
                case MotionEvent.ACTION_MOVE:
                    return true;// super.onTouchEvent(event);
                case MotionEvent.ACTION_CANCEL:
                    return true;

                case MotionEvent.ACTION_UP:
                    float y2 = event.getY();
                    float deltaY = y2 - y1;

                    ////Log.d("StoryCardView", "onTouchEvent: deltaY:compareDelta " + deltaY + " : " + compareDelta);

                    if (deltaY < compareDelta) {
                        if (onScrollRefreshListener != null) {
                            onScrollRefreshListener.onBottomToTopSwiped();
                            getParent().requestDisallowInterceptTouchEvent(false);
                        }
                    }
                    return true;

            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return super.onTouchEvent(event);
        }
        ////return super.onTouchEvent(event);
    }
}
