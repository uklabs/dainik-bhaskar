package com.db.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.bhaskar.util.AppUrls;
import com.bhaskar.util.CommonConstants;
import com.db.InitApplication;
import com.bhaskar.R;
import com.db.data.database.DatabaseHelper;
import com.db.data.database.NotificationStackTable;
import com.db.data.models.NotificationHubItemInfo;
import com.db.data.source.server.fcm.FirebaseNotificationConstants;
import com.bhaskar.appscommon.tracking.atracker.ATracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by DB on 8/26/2017.
 */

public class Notificationutil {
    private static final String RASHIFAL_CAT_ID = "4";
    private static final int NOTIFICATION_STACK_MAX_LIMIT = 5;
    private static final int NOTIFICATION_TYPE_BIG_IMAGE = 3;
    public static int NOTIFICATION_STACK_MAX_LIMIT_SHOW = 99;
    public static int NOTIFICATION_STACK_DEFAULT_VALUE = ATracker.ToggleStatus.OFF;
    public static int NOTIFICATION_DEFAULT_VALUE = ATracker.ToggleStatus.ON;
    public static boolean isDynamicNotificationAvailable = false;
    private static Notificationutil uniqueInstance;

    public static Notificationutil getInstance() {
        if (uniqueInstance == null) {
            synchronized (Notificationutil.class) {
                uniqueInstance = new Notificationutil();
            }
        }
        return uniqueInstance;
    }

    public static boolean getRashifalNotificationStatus(Context context) {
        if (context != null) {
            int soundStatus = AppPreferences.getInstance(context).getIntValue(QuickPreferences.RASHIFAL_STATUS, ATracker.ToggleStatus.OFF);
            if (soundStatus == ATracker.ToggleStatus.ON) {
                Systr.println("Rashifal Notification tracking true");
                return true;
            }
        }
        return false;
    }

    public static boolean getNotificationStatus(Context context) {
        if (context != null) {
            int notificationStatus = AppPreferences.getInstance(context).getIntValue(QuickPreferences.NOTIFICATION_STATUS, ATracker.ToggleStatus.ON);
            if (notificationStatus == ATracker.ToggleStatus.ON) {
                Systr.println("Notification tracking true");
                return true;
            }
        }
        return false;
    }

    public static boolean getNotificationStackStatus(Context context) {


        if (context != null) {
//            @ATracker.ToggleStatus int IsnotificationStackStatusChange = AppPreferences.getInstance(context).getIntValue(QuickPreferences.NOTIFICATION_NEW_STACK_STATUS, Notificationutil.NOTIFICATION_DEFAULT_VALUE);
//            int IsnotificationStackStatusChange = AppPreferences.getInstance(context).getIntValue(QuickPreferences.NOTIFICATION_IS_USER_STACK_CHANGE, NOTIFICATION_STACK_DEFAULT_VALUE);
//            if (IsnotificationStackStatusChange == ATracker.ToggleStatus.ON) {
            int notificationLocalStackStatus = AppPreferences.getInstance(context).getIntValue(QuickPreferences.NOTIFICATION_NEW_STACK_STATUS, NOTIFICATION_STACK_DEFAULT_VALUE);
            if (notificationLocalStackStatus == ATracker.ToggleStatus.ON) {
                Systr.println("Notification tracking true");
                return true;
            } else {
                return false;
            }
//        } else
//            return AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.StackPrefs.TOGGLING_STACK_ACTIVE, false);
        }
        return false;
    }

    public static boolean getStackVisibilityStatus(Context context) {
        if (context != null) {
            return AppPreferences.getInstance(context).getBooleanValue(QuickPreferences.StackPrefs.TOGGLING_STACK_ACTIVE, false);
        }
        return false;
    }


    public static boolean getNotificationSoundStatus(Context context) {
        if (context != null) {
            int soundStatus = AppPreferences.getInstance(context).getIntValue(QuickPreferences.SOUND_STATUS, ATracker.ToggleStatus.ON);
            if (soundStatus == ATracker.ToggleStatus.ON) {
                Systr.println("getNotificationSoundStatus tracking true");
                return true;
            }
        }
        return false;
    }

    public static boolean getNotificationVibrateStatus(Context context) {
        if (context != null) {
            int vibrateStatus = AppPreferences.getInstance(context).getIntValue(QuickPreferences.VIBRATE_STATUS, ATracker.ToggleStatus.OFF);
            if (vibrateStatus == ATracker.ToggleStatus.ON) {
                Systr.println("vibrateStatus tracking true");
                return true;
            }
        }
        return false;
    }

    public static boolean getNotificationDNDStatus(Context context) {
        if (context != null) {
            int doNotDisturb = AppPreferences.getInstance(context).getIntValue(QuickPreferences.DO_NOT_DISTURB_STATUS, ATracker.ToggleStatus.OFF);
            if (doNotDisturb == ATracker.ToggleStatus.ON) {
                Systr.println("DNDStatus tracking true");
                return true;
            }
        }
        return false;
    }

/*
    public static boolean checkFrequencyCount(Context context) {
        if (context != null) {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            //Set Current date in preferences
            Calendar c = Calendar.getInstance();
            String todayDate = df.format(c.getTime());
            String dailyFrequencyMainCount = AppPreferences.getInstance(context).getStringValue(QuickPreferences.DAILY_FREQUENCY_COUNT, "15");
            String preferencedate = AppPreferences.getInstance(context).getStringValue(QuickPreferences.DAILY_FREQUENCY_CURRENT_DATE, todayDate);
            int dailyFrequencyPerdayCount = AppPreferences.getInstance(context).getIntValue(QuickPreferences.DAILY_FREQUENCY_PER_DAY_COUNT, 0);
            Date strPrefrerenceDate = null;
            Date todayDateObject = null;
            try {
                strPrefrerenceDate = df.parse(preferencedate);
                todayDateObject = df.parse(todayDate);
            } catch (Exception ex) {
            }

            if (todayDateObject != null && strPrefrerenceDate != null && todayDateObject.compareTo(strPrefrerenceDate) == 0) {
                try {
                    if (dailyFrequencyPerdayCount < Integer.parseInt(dailyFrequencyMainCount)) {
                        dailyFrequencyPerdayCount++;
                        AppPreferences.getInstance(context).setStringValue(QuickPreferences.DAILY_FREQUENCY_CURRENT_DATE, preferencedate);
                        AppPreferences.getInstance(context).setIntValue(QuickPreferences.DAILY_FREQUENCY_PER_DAY_COUNT, dailyFrequencyPerdayCount);
                        AppLogs.printDebugLogs("Notification Date", "Same Day" + preferencedate);
                        return true;
                    }
                } catch (Exception ex) {

                }


            } else {
                AppPreferences.getInstance(context).setStringValue(QuickPreferences.DAILY_FREQUENCY_CURRENT_DATE, todayDate);
                //dailyFrequencyPerdayCount = 1;
                AppPreferences.getInstance(context).setIntValue(QuickPreferences.DAILY_FREQUENCY_PER_DAY_COUNT, 1);
                AppLogs.printDebugLogs("Notification Date today", "Not Same Day" + todayDate + ":" + strPrefrerenceDate);
                AppLogs.printDebugLogs("Notification Date preference", "Not Same Day" + preferencedate + new Date());
                return true;
            }

        }
        return false;
    }
*/

    public static boolean checkNotificationDNDTiming(Context context) {
        if (context != null) {
            Calendar cal = Calendar.getInstance();
            int doNotDisturb = AppPreferences.getInstance(context).getIntValue(QuickPreferences.DO_NOT_DISTURB_STATUS, ATracker.ToggleStatus.OFF);
            if (doNotDisturb == ATracker.ToggleStatus.ON) {
                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");

                String dNDFromTime = AppPreferences.getInstance(context).getStringValue(QuickPreferences.DO_NOT_DISTURB_FROM_TIME, Constants.DefaultSettingValue.DEFAULT_FROM_TIME);
                String dNDToTime = AppPreferences.getInstance(context).getStringValue(QuickPreferences.DO_NOT_DISTURB_TO_TIME, Constants.DefaultSettingValue.DEFAULT_TO_TIME);
                String currentTime = (parseFormat.format(cal.getTime()));

                Date datedNDFromTime = null;
                Date datedNDToTime = null;
                Date dateCurrentTime = null;
                try {
                    datedNDFromTime = parseFormat.parse(dNDFromTime);
                    datedNDToTime = parseFormat.parse(dNDToTime);
                    dateCurrentTime = parseFormat.parse(currentTime);
                    if ((datedNDFromTime.compareTo(datedNDToTime) == 0) || isTimeBetweenTwoTime(datedNDFromTime, datedNDToTime, dateCurrentTime)) {
                        AppLogs.printDebugLogs("DND value in range", "datedNDFromTime" + displayFormat.format(datedNDFromTime) + ":" + "datedNDToTime" + displayFormat.format(datedNDToTime) + ":" + "dateCurrentTime" + displayFormat.format(dateCurrentTime) + ":");
                        return true;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }
        return false;
    }

    public static boolean isTimeBetweenTwoTime(Date startTime, Date stopTime, Date currentTime) {
        //Start Time
        Calendar StartTime = Calendar.getInstance();
        StartTime.setTime(startTime);
        //Current Time
        Calendar CurrentTime = Calendar.getInstance();
        CurrentTime.setTime(currentTime);
        //Stop Time
        Calendar StopTime = Calendar.getInstance();
        StopTime.setTime(stopTime);

        if (stopTime.compareTo(startTime) < 0) {
            if (CurrentTime.compareTo(StopTime) < 0) {
                CurrentTime.add(Calendar.DATE, 1);
            }
            StopTime.add(Calendar.DATE, 1);
        }
        return CurrentTime.compareTo(StartTime) >= 0 && CurrentTime.compareTo(StopTime) < 0;
    }

    public static int getAppIcon() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            return R.mipmap.ic_notification_small;
        } else {
            return R.mipmap.app_icon;
        }
    }


    public static Bitmap getRashiBitmap(Context context, String rashi) {
        return BitmapFactory.decodeResource(context.getResources(), RashifalUtil.getRashiImageFromName(rashi));
    }

    public static void increaseNotificationsAlertCount(Context context) {
        if (context != null) {
//            if (currentFragmentIndex != VIDEO_BLTN_FRAGMENT) {
//                int count = AppPreferences.getInstance(context).getIntValue(QuickPreferences.NOTIFICATION_COUNT, 0);
//                count++;
//                AppPreferences.getInstance(context).setIntValue(QuickPreferences.NOTIFICATION_COUNT, count);
//
//                Intent intent = new Intent(QuickPreferences.NOTI_CUSTOM_EVENT_NAME);
//                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//            }

        }
    }

    public static void decreaseNotificationsAlertCount(Context context) {
        if (context != null) {
            int count = AppPreferences.getInstance(context).getIntValue(QuickPreferences.NOTIFICATION_COUNT, 0);
            if (count > 0) {
                count--;
            }

            AppPreferences.getInstance(context).setIntValue(QuickPreferences.NOTIFICATION_COUNT, count);
        }
    }

    public static void addDainikNotificationStackItemIntoDatabase(Context context, final String message, String storyId, String image, String channelId, int videoFlag, String cat_id) {
        if (context != null) {
            long when = System.currentTimeMillis();
            NotificationHubItemInfo notificationHubItemInfo = new NotificationHubItemInfo();
            notificationHubItemInfo.trackUrl = "";
            notificationHubItemInfo.catId = cat_id;
            notificationHubItemInfo.title = message;
            notificationHubItemInfo.image = image;
            notificationHubItemInfo.menuId = "";
            notificationHubItemInfo.storyId = storyId;
            notificationHubItemInfo.channelSlno = channelId;
            notificationHubItemInfo.slugIntro = "";
            notificationHubItemInfo.pubDate = "";
            notificationHubItemInfo.videoFlag = videoFlag;
            notificationHubItemInfo.gTrackUrl = "";
            notificationHubItemInfo.version = "";
            notificationHubItemInfo.readStatus = 0;
            notificationHubItemInfo.notificationId = Long.toString(when);
            NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(context).getTableObject(NotificationStackTable.TABLE_NAME);
            String result = notificationStackTable.insertDataFromPush(notificationHubItemInfo);
        }
    }

    public static int getNotificationsStackCount(Context context) {
        int notifcationStackCount = 0;

        if (context != null) {
            NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(context).getTableObject(NotificationStackTable.TABLE_NAME);
            ArrayList<NotificationHubItemInfo> notificationHubItemInfoArrayList = new ArrayList<>();
            notificationHubItemInfoArrayList = notificationStackTable.getAllNotificationTrayList();
            if (notificationHubItemInfoArrayList != null) {
                notifcationStackCount = notificationHubItemInfoArrayList.size();
            }
        }
        return notifcationStackCount;

    }

    // Clears notification tray messages
    public static void clearNotificationsTray(Context context) {
        NotificationManager notificationManager = (NotificationManager) InitApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Constants.NOTIFICATION_APPEND_ID);

    }

    public static void clearparticluarNotification(Context context, int notificationId) {
        NotificationManager notificationManager = (NotificationManager) InitApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationId);

    }

    public static void clearNotificationsStack(Context context) {
        if (context != null) {
            NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(context).getTableObject(NotificationStackTable.TABLE_NAME);
            List<NotificationHubItemInfo> NotificationHubItemInfo = notificationStackTable.getAllNotificationTrayList();
            notificationStackTable.updateNotificationOpenStatus(NotificationHubItemInfo);
        }
    }

    public static String getChannel(Context context, String catId) {
        String channel = CommonConstants.CHANNEL_NEWS;
        if (catId != null && context != null) {
            if (catId.equals("1")) {
                channel = CommonConstants.CHANNEL_NEWS;
            } else if (catId.equals("2")) {
                channel = CommonConstants.CHANNEL_BUSINESS;
            } else if (catId.equals("3")) {
                channel = CommonConstants.CHANNEL_RELIGION;
            }
        }
        return channel;
    }

    public static String getImage(Context context, String storyId, String catId, int notificationType, String channelId) {
        String image = "";
        try {
            if (catId.equalsIgnoreCase(RASHIFAL_CAT_ID)) /*for Rashifal*/ {
                image = catId;
            } else if (catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_BRAND_AND_CATEGORY)
                    || catId.equalsIgnoreCase(FirebaseNotificationConstants.NotificationCatIdType.CAT_ID_URL)) {
                image = "";
            } else {
                if (!TextUtils.isEmpty(storyId)) {
                    image = AppPreferences.getInstance(context).getStringValue(QuickPreferences.APP_IMAGE_NOTIFY_URL, AppUrls.PUSH_IMAGE_PREFIX) + channelId + "/" + storyId + "/";
                    if (notificationType == NOTIFICATION_TYPE_BIG_IMAGE)
                        image = image + "?bigimage=1";
                }
            }
        } catch (Exception ignored) {

        }
        return image;
    }


    public static String getNewsType(Context context, String storyId, String catId) {
        String newsType = "1";
        if (context != null) {
            if (TextUtils.isEmpty(storyId) || storyId.equalsIgnoreCase("0")) {
                newsType = "2";
            } else if (!TextUtils.isEmpty(catId) && catId.equalsIgnoreCase(RASHIFAL_CAT_ID)) {
                newsType = "3";
            }
        }
        return newsType;
    }

    public static String getDomain(Context context) {
        return AppUrls.DOMAIN;
    }

    public static float getImageFactor(Resources r) {
        DisplayMetrics metrics = r.getDisplayMetrics();
        return metrics.density / 3f;
    }

    public static void generateStackNotification(Context context, NotificationCompat.Builder notificationBuilder, String message) {
        Bitmap appIconBitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.app_icon);

        int counttotalMessage = 0;
        int counttotalMessagemorethanfive = 0;
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        //Add stack Notification in ArrayList
        NotificationStackTable notificationStackTable = (NotificationStackTable) DatabaseHelper.getInstance(context).getTableObject(NotificationStackTable.TABLE_NAME);
        notificationStackTable.deleteNotificationStackBefore48hrsArrayList();
        List<NotificationHubItemInfo> NotificationHubItemInfo = notificationStackTable.getAllNotificationTrayList();
        if (NotificationHubItemInfo != null) {
            for (int i = NotificationHubItemInfo.size() - 1; i >= 0; i--) {
                if (counttotalMessage < NOTIFICATION_STACK_MAX_LIMIT) {
                    inboxStyle.addLine(NotificationHubItemInfo.get(i).getTitle());
                }
                counttotalMessage++;
            }
        }

        if (counttotalMessage > NOTIFICATION_STACK_MAX_LIMIT) {
            counttotalMessagemorethanfive = counttotalMessage - 5;
            if (counttotalMessagemorethanfive > NOTIFICATION_STACK_MAX_LIMIT_SHOW) {
                counttotalMessagemorethanfive = NOTIFICATION_STACK_MAX_LIMIT_SHOW;
            }
            inboxStyle.setSummaryText("+" + counttotalMessagemorethanfive + " " + context.getResources().getString(R.string.more_alerts));

        }
        Bitmap aBitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.app_icon);

        notificationBuilder.setSmallIcon(R.mipmap.ic_notification_small);
        notificationBuilder.setLargeIcon(aBitmap);
        if (counttotalMessage > 1) {
            notificationBuilder.setContentTitle(context.getResources().getString(R.string.app_name));
            notificationBuilder.setContentText(counttotalMessage + " " + context.getResources().getString(R.string.breaking_news_alerts));
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_MAX);
            notificationBuilder.setStyle(inboxStyle);
        } else {
            //Depends on Requirement
            notificationBuilder.setLargeIcon(appIconBitmap);
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_MAX);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                    .bigText(message)
            );
        }

    }


    //push type 1
    public static void generateCustomNotification(Context context, NotificationCompat.Builder notificationBuilder, Bitmap downloadedBitmap, String message, boolean isVideo) {
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.layout_expanded_notification);
        contentView.setTextViewText(R.id.notification_title, message);

        if (downloadedBitmap != null) {
            contentView.setImageViewBitmap(R.id.notification_image, downloadedBitmap);
            if (isVideo){
                try{
                    contentView.setViewVisibility(R.id.ivVideoFlag, View.VISIBLE);
                    contentView.setImageViewResource(R.id.ivVideoFlag,R.drawable.ic_video_play_flag);
                }catch (Exception ignored){

                }

            }
            notificationBuilder
                    .setCustomContentView(contentView)
                    .build();
        } else {
            contentView.setViewVisibility(R.id.notification_image, View.GONE);
            contentView.setViewVisibility(R.id.vRightView, View.GONE);
            notificationBuilder
                    .setCustomContentView(contentView)
                    .build();
        }

    }

    //pushtype 2
    public static void generateCustomBigSizeNotification(Context context, NotificationCompat.Builder notificationBuilder, Bitmap downloadedBitmap, String message, boolean isVideo) {
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.layout_expanded_notification);
        contentView.setTextViewText(R.id.notification_title, message);

        if (downloadedBitmap != null) {
            contentView.setImageViewBitmap(R.id.notification_image, downloadedBitmap);

            if (isVideo){
                try{
                    contentView.setViewVisibility(R.id.ivVideoFlag, View.VISIBLE);
                    contentView.setImageViewResource(R.id.ivVideoFlag,R.drawable.ic_video_play_flag);
                }catch (Exception ignored){}
            }

            RemoteViews notificationLayoutExpanded = new RemoteViews(context.getPackageName(), R.layout.layout_expanded_notification);
            notificationLayoutExpanded.setTextViewText(R.id.notification_title, message);
            notificationLayoutExpanded.setImageViewBitmap(R.id.notification_image, downloadedBitmap);
            notificationLayoutExpanded.setImageViewBitmap(R.id.notification_big_image, downloadedBitmap);
            if (isVideo){
                try{
                    notificationLayoutExpanded.setViewVisibility(R.id.ivVideoFlagBig, View.VISIBLE);
                    notificationLayoutExpanded.setImageViewResource(R.id.ivVideoFlagBig,R.drawable.ic_video_play_flag);
                }catch (Exception ignored){}
            }


            notificationLayoutExpanded.setViewVisibility(R.id.big_image_container, View.VISIBLE);

            notificationBuilder
                    .setCustomContentView(contentView)
                    .setCustomBigContentView(notificationLayoutExpanded)
                    .build();
        } else {
            contentView.setViewVisibility(R.id.notification_image, View.GONE);
            contentView.setViewVisibility(R.id.vRightView, View.GONE);
            notificationBuilder
                    .setCustomContentView(contentView)
                    .build();
        }

    }


    //pushtype 3
    public static void generateCustomMultilineNotification(Context context, NotificationCompat.Builder notificationBuilder, Bitmap downloadedBitmap, String message, boolean isVideo) {
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.layout_expanded_notification);
        contentView.setTextViewText(R.id.notification_title, message);

        if (downloadedBitmap != null) {
            contentView.setImageViewBitmap(R.id.notification_image, downloadedBitmap);

            if (isVideo){
                try{
                    contentView.setViewVisibility(R.id.ivVideoFlag, View.VISIBLE);
                    contentView.setImageViewResource(R.id.ivVideoFlag,R.drawable.ic_video_play_flag);
                }catch (Exception ignored){}
            }

//            notificationBuilder
//                    .setCustomContentView(contentView)
//                    .setLargeIcon(downloadedBitmap)
//                    .setStyle(new NotificationCompat.BigTextStyle()
//                            .bigText(message))
//                    .build();

            RemoteViews notificationLayoutExpanded = new RemoteViews(context.getPackageName(), R.layout.layout_expanded_multi_notification);
            notificationLayoutExpanded.setTextViewText(R.id.notification_title, message);
            notificationLayoutExpanded.setImageViewBitmap(R.id.notification_image, downloadedBitmap);
            if (isVideo){
                try{
                    notificationLayoutExpanded.setViewVisibility(R.id.ivVideoFlag, View.VISIBLE);
                    notificationLayoutExpanded.setImageViewResource(R.id.ivVideoFlag,R.drawable.ic_video_play_flag);
                }catch (Exception ignored){}
            }
//            notificationLayoutExpanded.setImageViewBitmap(R.id.notification_big_image, downloadedBitmap);
//            notificationLayoutExpanded.setViewVisibility(R.id.big_image_container, View.VISIBLE);
//            notificationLayoutExpanded.setViewVisibility(R.id.notification_big_image, View.GONE);

            notificationBuilder
                    .setCustomContentView(contentView)
                    .setCustomBigContentView(notificationLayoutExpanded)
                    .build();

        } else {
            contentView.setViewVisibility(R.id.notification_image, View.GONE);
            contentView.setViewVisibility(R.id.vRightView, View.GONE);
            notificationBuilder
                    .setCustomContentView(contentView)
                    .build();
        }

    }

    //pushtype 4
    public static void generateDefaultNotification(NotificationCompat.Builder notificationBuilder, Bitmap downloadedBitmap) {
        if (downloadedBitmap != null) {
            notificationBuilder
                    .setLargeIcon(downloadedBitmap);
        }


    }

    //push type 5
    public static void generateDefaultBigImageNotification(NotificationCompat.Builder notificationBuilder, Bitmap downloadedBitmap) {
        if (downloadedBitmap != null) {
            notificationBuilder
                    .setLargeIcon(downloadedBitmap)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(downloadedBitmap)
                            .bigLargeIcon(null));
        }

    }

    //pushtype 6
    public static void generateDefaultMultiLineNotification(NotificationCompat.Builder notificationBuilder, Bitmap downloadedBitmap, String message) {
        if (downloadedBitmap != null) {
            notificationBuilder
                    .setLargeIcon(downloadedBitmap)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message));
        } else {
            notificationBuilder
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message));
        }

    }


    public static Notification setNotificationProperty(Context context, Notification notification) {
        if (context != null && notification != null) {
            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            if (Notificationutil.getNotificationSoundStatus(context)) {
                Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                if (uri != null) {
                    notification.sound = uri;
                }
            } else {
                notification.sound = null;
            }
            if (Notificationutil.getNotificationVibrateStatus(context)) {
                notification.vibrate = new long[]{0, 100, 200, 300};
            } else {
                notification.vibrate = new long[]{-1};
            }
            notification.defaults |= Notification.DEFAULT_LIGHTS;
        }


        return notification;
    }


}
