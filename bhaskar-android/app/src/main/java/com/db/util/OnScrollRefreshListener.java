package com.db.util;

public interface OnScrollRefreshListener {
    void onBottomToTopSwiped();

    void onTopToBottomSwiped();
}
