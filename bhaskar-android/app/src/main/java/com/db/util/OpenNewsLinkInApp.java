package com.db.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.util.AppUrls;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.view.ui.MainActivity;
import com.bhaskar.BuildConfig;
import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.database.DatabaseClient;
import com.db.home.ActivityUtil;
import com.db.listeners.OnFeedFetchFromServerListener;
import com.db.news.WebViewActivity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OpenNewsLinkInApp {
    private Context context;
    private String url;
    private String menuName;
    // isBackground = 0  true
    private int isBackground;
    private String gaScreen;
    private OnFeedFetchFromServerListener onFeedFetchFromServerListener;
    private boolean useFlag = true;
    private OpenNewsLinkCallback openNewsLinkCallback;

    public OpenNewsLinkInApp(Context context, String url, String menuName, int isBackground, String gaScreen, OnFeedFetchFromServerListener onFeedFetchFromServerListener) {
        this.context = context;
        if (url.contains("\\\""))
            url = url.replaceAll("\\\\\"", "");
        this.url = url;
        this.menuName = (!TextUtils.isEmpty(menuName)) ? menuName : Constants.KeyPair.KEY_NEWS_UPDATE;
        this.isBackground = isBackground;
        this.gaScreen = gaScreen;
        this.onFeedFetchFromServerListener = onFeedFetchFromServerListener;
    }

    public void setFlag(boolean useFlag) {
        this.useFlag = useFlag;
    }

    public void openLink() {
        if (NetworkStatus.getInstance().isConnected(context)) {
            if (url.contains(context.getString(R.string.dynamic_link_host1)) || url.contains(context.getString(R.string.dynamic_link_host2))) {
                handleDynamicLink(context, url);
            } else if (this.url.contains("firebase_dynamic_link=1")) {
                /*New Logic*/
                if (this.url.contains("start=1&") && this.url.contains("&end=1")) {
                    int startIndex = this.url.indexOf("start=1&");
                    int endIndex = this.url.indexOf("&end=1");

                    String appData = this.url.substring(startIndex + 7, endIndex);
                    String dynamicLinkUrl = "", menuId = "";
                    if (appData.contains("&")) {
                        String[] dataArray = appData.split("&");
                        for (String key : dataArray) {
                            if (!TextUtils.isEmpty(key)) {
                                String[] keyValue = key.split("=");
                                if (!TextUtils.isEmpty(keyValue[1]))
                                    switch (keyValue[0]) {
                                        case "url":
                                            dynamicLinkUrl = keyValue[1];
                                            break;
                                        case "menu_id":
                                            menuId = keyValue[1];
                                            break;
                                    }
                            }
                        }
                    } else {
                        dynamicLinkUrl = appData.length() > "url=".length() ? appData.substring(appData.indexOf("url="), appData.length() - 1) : "";
                    }

                    if (!TextUtils.isEmpty(dynamicLinkUrl)) {
                        String localUrl = Urls.AR_LINK_URL_INFO_V3 + dynamicLinkUrl + Urls.NEWS_URL_COMMON_SUFFIX;
                        new ArLinkTask(context, localUrl, dynamicLinkUrl).makeJsonObjectRequest();
                    } else {
                        if (isBackground != 0) {
                            CategoryInfo categoryInfo = DatabaseClient.getInstance(context).getCategoryById(menuId);
                            ActivityUtil.openCategoryAccordingToAction(context, categoryInfo, "");
                        } else {
                            Intent homeIntent = new Intent(context, MainActivity.class);
                            if (!TextUtils.isEmpty(menuId)) {
                                homeIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, menuId);
                            }
                            context.startActivity(homeIntent);
                        }
                        if(openNewsLinkCallback!=null){
                            openNewsLinkCallback.onCallback(1);
                        }
                    }
                } else {
                    Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);
                    if(openNewsLinkCallback!=null){
                        openNewsLinkCallback.onCallback(1);
                    }
                }
            } else if (this.url.contains("play.google.com") || this.url.contains("itunes.apple.com")) {
                Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
                if(openNewsLinkCallback!=null){
                    openNewsLinkCallback.onCallback(1);
                }
            } else {
                String url = Urls.AR_LINK_URL_INFO_V3 + this.url + Urls.NEWS_URL_COMMON_SUFFIX;
                Systr.println("feed : "+url);
                new ArLinkTask(context, url, this.url).makeJsonObjectRequest();
            }
        }
    }

    private void handleDynamicLink(Context context, String dynamicURL) {
        Task<PendingDynamicLinkData> dynamicLink = FirebaseDynamicLinks.getInstance().getDynamicLink(Uri.parse(dynamicURL));
        dynamicLink.addOnSuccessListener(command -> {
            if (command != null) {
                this.url = command.getLink().toString();
                int minVersion = command.getMinimumAppVersion();
                if (minVersion > BuildConfig.VERSION_CODE) {
                    //open playstore
                    context.startActivity(command.getUpdateAppIntent(context));
                    return;
                }
                openLink();
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(dynamicURL));
                context.startActivity(intent);
            }
        });
    }

    private void openWebviewActivity(Context context, String url) {
        Intent intent = new Intent(context, WebViewActivity.class);
        if (isBackground == 0)
            intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, true);
        intent.putExtra(Constants.KeyPair.KEY_URL, url);
        intent.putExtra(Constants.KeyPair.KEY_TITLE, menuName);
        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAScreen.WEB_VIEW + "-" + gaScreen + "-" + url);
        intent.putExtra(Constants.KeyPair.KEY_HAS_HEADER, true);
        context.startActivity(intent);
        if(openNewsLinkCallback!=null){
            openNewsLinkCallback.onCallback(1);
        }
    }

    public void setCallback(OpenNewsLinkCallback openNewsLinkCallback) {
        this.openNewsLinkCallback = openNewsLinkCallback;
    }

    public class ArLinkTask {
        Context ctx;
        String url;
        String mailUrl;

        ArLinkTask(Context context, String url, String mailUrl) {
            this.ctx = context;
            this.url = url;
            this.mailUrl = mailUrl;
        }

        private void makeJsonObjectRequest() {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(url), null, response -> {
                if (!TextUtils.isEmpty(response.toString())) {
                    processResponse(response);
                } else {
                    openWebviewActivity(context, mailUrl);
                }
            }, error -> {
                openWebviewActivity(context, mailUrl);
                if (onFeedFetchFromServerListener != null) {
                    onFeedFetchFromServerListener.onDataFetch(true);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
        }

        private void processResponse(JSONObject jsonObject) {
            try {
                String channelSlno = jsonObject.getString("channel_slno");
                if (!channelSlno.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
                    openWebviewActivity(context, mailUrl);
                } else {
                    String id = jsonObject.getString("id");
                    String news_type = jsonObject.getString("news_type");

                    if (news_type.equalsIgnoreCase("News")) {
                        String wisdomDomain = AppUrls.DOMAIN;
                        AppUtils.getInstance().openNewsDetail(context, id, wisdomDomain, isBackground, menuName, gaScreen, AppFlyerConst.GAScreen.OPEN_LINK_GA_ARTICLE, "", useFlag);
                    } else if (news_type.equalsIgnoreCase("Photo")) {
                        AppUtils.getInstance().openPhotoGallery(context, id, isBackground == 0);
                    } else if (news_type.equalsIgnoreCase("Video")) {
                        AppUtils.getInstance().openVideo(context, id, isBackground == 0);
                    }
                }
                if (onFeedFetchFromServerListener != null) {
                    onFeedFetchFromServerListener.onDataFetch(true);
                }
                if(openNewsLinkCallback!=null){
                    openNewsLinkCallback.onCallback(1);
                }
            } catch (JSONException e) {
            }
        }
    }

    public interface OpenNewsLinkCallback{
        void onCallback(int status);
    }


}
