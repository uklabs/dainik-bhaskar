package com.db.util;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.util.AppUrls;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.view.ui.MainActivity;
import com.db.news.WapV2Activity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OpenNewsLinkInAppV3 {
    private Context context;
    private String url;
    private String menuName;
    private int isBackground;
    private String gaScreen;

    public OpenNewsLinkInAppV3(Context context, String url, String menuName, int isBackground) {
        this.context = context;
        this.url = url;
        this.menuName = (!TextUtils.isEmpty(menuName)) ? menuName : Constants.KeyPair.KEY_NEWS_UPDATE;
        this.isBackground = isBackground;
        this.gaScreen = "LINK-CLICK";
    }

    public void openLink() {
        if (NetworkStatus.getInstance().isConnected(context)) {
            if (this.url.contains("firebase_dynamic_link=1")) {
                Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
            } else {
                String url = Urls.AR_LINK_URL_INFO_V3 + this.url + Urls.NEWS_URL_COMMON_SUFFIX;
                new ArLinkTask(context, url, this.url).makeJsonObjectRequest();
            }
        }
    }

    private void openContestPage(Context context, String appBrandId, String appMenuId, String appContestType, String appSelfieId, int isBackground) {
        Intent homeIntent = new Intent(context, MainActivity.class);
        homeIntent.putExtra(Constants.KeyPair.KEY_CATEGORY_ID, appMenuId);
        context.startActivity(homeIntent);
    }

    private void openWebviewActivity(Context context, String url) {
        Intent intent = new Intent(context, WapV2Activity.class);
        if (isBackground == 0)
            intent.putExtra(Constants.KeyPair.KEY_IS_NOTIFICATION, true);
        intent.putExtra(Constants.KeyPair.KEY_URL, url);
        intent.putExtra(Constants.KeyPair.KEY_TITLE, menuName);
        intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAScreen.WEB_VIEW + "-" + gaScreen + "-" + url);
        context.startActivity(intent);
    }

    public class ArLinkTask {
        Context ctx;
        String url;
        String mailUrl;

        public ArLinkTask(Context context, String url, String mailUrl) {
            this.ctx = context;
            this.url = url;
            this.mailUrl = mailUrl;
        }

        private void makeJsonObjectRequest() {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppUtils.getInstance().updateApiUrl(url), null, response -> {
                if (!TextUtils.isEmpty(response.toString())) {
                    processResponse(response);
                } else {
                    openWebviewActivity(context, mailUrl);
                }
            }, error -> {
                openWebviewActivity(context, mailUrl);
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", CommonConstants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
        }

        private void processResponse(JSONObject jsonObject) {
            try {
                String channelSlno = jsonObject.getString("channel_slno");
                if (!channelSlno.equalsIgnoreCase(CommonConstants.CHANNEL_ID)) {
                    openWebviewActivity(context, mailUrl);
                } else {
                    String id = jsonObject.getString("id");
                    String news_type = jsonObject.getString("news_type");

                    if (news_type.equalsIgnoreCase("News")) {
                        String wisdomDomain = AppUrls.DOMAIN;
                        AppUtils.getInstance().openNewsDetail(context, id, wisdomDomain, isBackground, menuName, gaScreen, AppFlyerConst.GAScreen.OPEN_LINK_GA_ARTICLE, "", true);
                    } else if (news_type.equalsIgnoreCase("Photo")) {
                        AppUtils.getInstance().openPhotoGallery(context, id, isBackground == 0);
                    } else if (news_type.equalsIgnoreCase("Video")) {
                        AppUtils.getInstance().openVideo(context, id, isBackground == 0);
                    }
                }
            } catch (JSONException e) {
            }
        }
    }
}
