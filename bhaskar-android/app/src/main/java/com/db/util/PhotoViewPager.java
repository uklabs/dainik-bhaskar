package com.db.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.db.views.WrappingViewPager;

public class PhotoViewPager extends WrappingViewPager {


    public PhotoViewPager(@NonNull Context context) {
        super(context);
    }

    public PhotoViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        if(getAdapter()!=null && getAdapter().getCount()==1){
            return false;
        }

        return super.onInterceptTouchEvent(e);

//        return false;
    }

}
