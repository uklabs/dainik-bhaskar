package com.db.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.view.Window;

import com.bhaskar.R;

public class ProgressbarDialog {

    private ProgressDialog progressDialog;

    public ProgressbarDialog(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            } else {
                progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStylePre);
            }
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        } catch (Exception e) {
        }
    }

    public void show(Activity activity, String message, boolean cancelable) {
        try {
            if (progressDialog != null && !progressDialog.isShowing() && !activity.isFinishing()) {
                progressDialog.setCancelable(cancelable);
                progressDialog.setMessage(message);
                progressDialog.show();
            }
        } catch (IllegalArgumentException e) {
        }
    }

    public void show(String message, boolean cancelable) {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.setCancelable(cancelable);
                progressDialog.setMessage(message);
                progressDialog.show();
            }
        } catch (IllegalArgumentException e) {
        }
    }

    public void dismiss() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
        }
    }
}
