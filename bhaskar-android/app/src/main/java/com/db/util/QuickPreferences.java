package com.db.util;


public class QuickPreferences {

    public static final String KEY_DRAW_OVER_OTHER_APPS = "key_draw_over_other_apps";
    public static final String KEY_PROMO_WIDGET_VISIBILITY_STATUS = "key_promo_widget_visibility_status";

    public static final String KEY_PROMO_WIDGET_MODEL = "key_promo_widget_model";

    public static final String APP_VERSION = "app_version";

    public static final String SPLASH_VIDEO_SOUND = "splash_video_sound";

    /*// App Prerference*/
    public static final String IS_STATIC_JSON_STORED = "home_feed_json_available_5.0.1";

    public static final String IS_PREFERRED_CITY_SET = "is_preferred_city_set";
    public static final String DEFAULT_PREFERRED_CITY = "default_preferred_city_%s";
    public static final String DEFAULT_PREFERRED_CITY_V2 = "default_preferred_city_v2_%s";
    public static final String SELECTED_PREF_CITY = "selected_pref_city_%s";
    public static final String IS_NIGHT_MODE = "is_day_night_pref_not_night_mode";
    public static final String FONT_SIZE = "font_size_pref";
    public static final String ARTICLE_FONT_SIZE = "article_font_size_pref";

    // App Wall
    public static final String RECOMMENDATIONS_LIST_LAYOUT = "list_layout";
    // Toggling Video Ads
    public static final String TOGGLING_MEGA_OFFER = "toggling_mega_offer";
    // Toggling ScreenDepth
    public static final String TOGGLING_ARTICLE_RATING_TOP = "articleRatingToggleTop";
    // Toggling Bottom Navigation Bar
    public static final String TOGGLING_BOTTOM_NAV_HIDEABLE_SERVER = "bottomNavHideable";
    public static final String TOGGLING_BOTTOM_NAV_HIDEABLE = "bottomNavHideableTemp";

    // Toggling Auto Video playing
    public static final String TOGGLING_VIDEO_AUTO_NEXT_PLAY = "videoAutoNextPlay";

    // Native Ads Flags
    public static final String APP_UPDATE_POP_UP_SHOWN_DATE = "updatePopupShownDate";

    //update popup constants
    public static final String APP_UPDATE_SESSION_COUNT = "updatePopupSessionCountV2";
    public static final String APP_RATEUS_SESSION_COUNT = "rateUsPopupSessionCountV2";
    public static final String APP_CURRENT_VERSION = "appCurrentVersionV2";
    public static final String UPDATE_POPUP_VERSION_CODE = "update_popup_versionV2";
    public static final String FORCE_UPDATE_VERSION = "force_update_versionV2";
    public static final String UPDATE_POPUP_MESSAGE = "force_update_messageV2";
    public static final String UPDATE_POPUP_INSTALL_MESSAGE = "update_install_messageV2";
    //rate app
    public static final String IS_APP_RATED = "isAppRated";
    public static final String IS_APP_LAUNCED_FISRT_TIME = "isAppLaunchedFirstTime";
    public static final String APP_INSTALL_DATE = "appInstalledDate";
    public static final String APP_INSTALL_DATE_IN_FORMAT = "appInstallDateInFormat";


    // UTM Campaign PREFERENCE
    public static final String UTM_SOURCE = "utm_source";
    public static final String UTM_MEDIUM = "utm_medium";
    public static final String UTM_CAMPAIGN = "utm_campaign";
    public static final String UTM_DIRECT = "direct";

    public static final String UTM_SOURCE_TEMP = "utm_source_temp";
    public static final String UTM_MEDIUM_TEMP = "utm_medium_temp";
    public static final String UTM_CAMPAIGN_TEMP = "utm_campaign_temp";

    // Notification & Rashifal
    public static final String NOTIFICATION_STATUS = "notificationStatus";

    public static final String RASHIFAL_STATUS = "rashifalStatus";
    public static final String DOUBLE_NOTIFICATION_DATA = "doubleNotificationData";


    public static final String ALL_NOTIFICATION_STATUS = "AllNotificationStatus";
    public static final String IMPORTANT_NOTIFICATION_STATUS = "ImportantNotificationStatus";
    public static final String MORNING_BRIEF_STATUS = "MorningBriefStatus";
    public static final String EVENING_ROUNDUP_STATUS = "EveningRoundupStatus";

    // DoNotDisturb Sound & Vibrate
    public static final String NOTIFICATION_NEW_STACK_STATUS = "newNotificationStackStatusV2";
    public static final String DO_NOT_DISTURB_STATUS = "doNotDisturbStatus";
    public static final String DO_NOT_DISTURB_FROM_TIME = "doNotDisturbFromTime";
    public static final String DO_NOT_DISTURB_TO_TIME = "doNotDisturbToTime";
    public static final String SOUND_STATUS = "soundStatus";
    public static final String VIBRATE_STATUS = "vibrateStatus";
    // DailyFrequency Count
    public static final String DAILY_FREQUENCY_COUNT = "dailyFrequencyCount";
    public static final String DAILY_FREQUENCY_PER_DAY_COUNT = "dailyFrequencyPerDayCount";
    public static final String DAILY_FREQUENCY_CURRENT_DATE = "dailyFrequencyCurrentDate";
    /* Host Preference*/
    public static final String APP_FEED_BASE_URL = "appFeedBaseUrlV3";
    public static final String APP_LOG_FEED_BASE_URL = "appLogFeedBaseUrlV3";
    public static final String APP_IMAGE_NOTIFY_URL = "appImageNotifyUrlV3";
    public static final String APP_VIDEO_NOTIFICATION_URL = "appVideoNotificationUrlV3";
    public static final String APP_LOGIN_FEED_URL = "appLoginFeedUrl";
    public static final String GA_UAID = "gaUaid";
    public static final String SELECTED_CHANNEL_EVENT_LABEL = "channelEventLabel";
    public static final String SELECTED_CHANNEL_EVENT_LABEL_TEMP = "channelEventLabelTemp";
    public static final String BASE_NAME = "baseName";
    public static final String VIDOE_SETTING = "videoSetting";
    public static final String VIDOE_SETTING_WIFI = "videoSettingWifi";
    //Notification custom event
    public static final String NOTI_CUSTOM_EVENT_NAME = "notification_custom_event";
    public static final String NOTIFICATION_COUNT = "notification_count";
    public static final String WISDOM_DOMAIN = "wisdomDomain";
    public static final String WISDOM_DOMAIN_TEMP = "wisdomDomainTemp";
    /*Feed dynamic Values */
    public static final String FEED_CACHE_TIME = "feed_cache_time";


    public static final String FOLLOW_LIST_UPDATE_FIRST_TIME = "followListUpdateFirstTime";
    public static final String AD_IMPRESSION_TRACK_URL = "addImpressionTrackUrl";
    public static final String GET_APPS_URL = "getAppsUrlV3";
    public static final String READ_MORE_COUNT = "readMoreCount";
    public static final String IMPRESSION_TRACKING_URL = "fRecommTrackingUrl";
    public static final String NEWS_TICKER_URL = "NewsTickerUrl";
    public static final String RECOMMENDATION_CONSENT_SENT = "recommendationConsentSent";
    public static final String TUTORIAL_ENABLE = "tutorial_enable";
    public static final String GA_SCROLL_TEST_ENABLE = "ga_scroll_test_enable";
    public static final String HOME_PAGINATION_ENABLE = "home_pagination_enable";
    public static final String EVENT_FEED_BASE_URL = "event_feed";
    public static final String NOTIFICATION_RECEIVE_TIME_MILLIS = "notificationReceiveMillis";
    public static final String NOTIFICATION_AUTOSTART_POPUP_SHOW = "notificationAutoStartPopupShow";

    //AppWidget Prefs
    public static final String APP_WIDGET_KEY_CURRENT_POS = "appWidgetCurrntPos";

    //CleverTap
    public static final String IS_TOKEN_UPLOADED = "is_token_uploaded";

    // Tab Reording
    public static String TAB_REORDER = "tabreoder";
    public static String QUICK_READ_STATUS = "quick_read_status";
    public static String QUICK_READ_STOP_BY_USER = "quick_read_stop_by_user";

    //Splash data
    public static String SPLASH_COUNT = "splash_count";

    public static final String SESSION_TIME_STAMP = "session_time_stamp";

    public static final String SHOW_TUTORIAL = "show_tutorial";

    //Facebook login
    public static final String IS_FACEBOOK_LOGIN_DISABLED = "facebook_login_disabled";

    public interface JsonPreference {
        /*// Json Preference*/
        String PRIME_NEW_ICON_URL = "brand_new_icon_url";

        String CONTEST_SELFIE_DETAIL_URL = "contest_selfie_detail_url";
        String ARTICLE_ACTION_URL = "article_action_urlV3";

        String CATEGORY_FEED_VERSION = "category_feed_version_V3_%s";

        //app control
        String BASE_APP_CONTROL_JSON = "base_app_control_json_V3";
        String BASE_APP_CONTROL_VERSION = "base_app_control_version_V3";

        // app host
        String APP_HOST_JSON = "app_host_json";
        String APP_HOST_VERSION = "app_host_version";

        // app version
        String APP_VERSION_JSON = "app_version_jsonV2";
        String APP_VERSION_VERSION = "app_version_versionV2";
        String APP_TODAY_DATE = "app_today_date";

        // preferred city
        String PREFERRED_CITY_FEED_JSON = "preferred_city_feed_json_%s";
        String PREFERRED_CITY_FEED_VERSION = "preferred_city_feed_version_%s";

        String FANTASY_MENU_FEED = "fantasy_menu_feed";

    }

    public interface SerializePreference {
        String PREFERED_SELECTED_CITY_JSON = "preffered_selected_city_json";
        String BOOKMARKS_JSON_ARRAY = "bookmarks_json_array";
    }

    public interface ButtonTopWGTPref {
        String IS_ACTIVE = "BTW_IsActive";
        String BANNER_URL = "BTW_bannerUrl";
        String ADDED_MENU = "BTW_addedMenu";
    }

    public interface WidgetPrefs {
        String APP_WIDGET_FEED = "aw_feed";

        String ARTICLE_IFRAME_POSITION = "article_iframe_position";
        String ARTICLE_IFRAME_URL = "article_iframe_url";
        String ARTICLE_IFRAME_ACTION_URL = "article_iframe_action_url";
        String ARTICLE_IFRAME_ACTION_NAME = "article_iframe_action_name";
        String ARTICLE_IFRAME_GA_EVENT = "article_iframe_ga_event";
        String ARTICLE_IFRAME_GA_SCREEN = "article_iframe_ga_screen";
        String ARTICLE_IFRAME_IS_ACTIVE = "article_iframe_isActive";
        String ARTICLE_IFRAME_LOGIN_REQUIRED = "article_iframe_login_required";
    }

    public interface QuickNotificationPrefs {
        String COUNT = "count";
        String PAGINATION_COUNT = "pagination_count";
    }

    public interface DBQuizPrefs {
        String EVENT_USER_ID = "event_user_id";
        String TOGGLING_CONTEST_ACTIVE = "toggling_contest";
    }

    public interface HomeLogo {
        String IS_HOME_LOGO_ACTIVE = "isHomeLogoActive";
        String HOME_LOGO_URL = "homeLogoUrl";
    }

    public interface StackPrefs {
        String TOGGLING_STACK_ACTIVE = "stack_id_control";
    }

    public interface LastTabPrefs {
        String TOGGLING_LAST_TAB_ACTIVE = "toggling_last_tab_open";
    }


    public interface ATF_HP_BannerPrefs {
        String ATFHP_BANNER_IS_ACTIVE = "ATFHPBannerIsActive";
        String ATFHP_BANNER_ACTION_URL = "ATFHPBannerActionUrl";
        String ATFHP_BANNER_ACTION_NAME = "ATFHPBannerActionName";
        String ATFHP_BANNER_ACTION_BRAND_ID = "ATFHPBannerBrandId";
        String ATFHP_BANNER_ACTION_MENU_ID = "ATFHPBannerMenuId";
        String ATFHP_BANNER_GA_EVENT = "ATFHPBannerGAEvent";
        String ATFHP_BANNER_GA_SCREEN = "ATFHPBannerGAScreen";
        String ATFHP_BANNER_URL = "ATFHPBannerUrl";
        String ATFHP_BANNER_IS_IMG_ENABLE = "ATFHPBannerIsImgEnabled";
    }

    public interface FlickerPrefs {
        String FLICKER_IS_ACTIVE = "flickerIsActive";
        String FLICKER_API = "flickerApi";
        String FLICKER_ADDED_ON_MENU = "flickerAddedOnMenu";
        String FLICKER_ADDED_ON_HOME = "flickerAddedOnHome";
        String FLICKER_HOME_POS = "flickerHomePosition";
        String FLICKER_MENU_POS = "flickerMenuPosition";
        String FLICKER_COLOR = "flickerColor";
        String FLICKER_TEXT_COLOR = "flickerTextColor";
        String FLICKER_SUBJECT_COLOR = "subjectColor";
    }


    public interface StockPrefs {
        String STOCK_IS_ACTIVE = "stockIsActive";
        String STOCK_API = "stockApi";
        String STOCK_ADDED_ON_MENU = "stockAddedOnMenu";
        String STOCK_ADDED_ON_HOME = "stockAddedOnHome";
        String STOCK_HOME_POS = "stockHomePosition";
        String STOCK_MENU_POS = "stockMenuPosition";

    }

    public static final String LAST_OPENED_CATEGORY = "lastOpenedCategoryId";

    public interface VideoPref {
        String SETTINGS_VIDEO_ACTION = "video_action";
        String SETTINGS_VIDEO_PROVIDER_URL = "settingsVideoProviderUrl";
        String SETTINGS_VIDEO_FEED_URL_V2 = "settingsVideoFeedUrlV2";
        String SETTINGS_VIDEO_DETAIL_URL_V2 = "settingsVideoDetailUrlV2";
        String SETTINGS_IS_VIDEO_FEED_URL_PERSONALIZED = "settingsIsVideoFeedUrlPersonalized";

        String GDRP_FB_ENABLE = "gdpr_fb_enable";
    }


    public interface ActiveUser {
        String ACTIVE_USER_NOTIFY = "activeUserNotify";
        String ACTIVE_USER_ARTICLE = "activeUserArticle";
        String GA_CLIENT_ID = "gaClientId";
    }

    public interface GdprConst {
        String IS_BLOCKED_COUNTRY = "isBlockedCountry";
        String GDPR_CONSENT_ACCEPTED = "gdprConsentAccepted";
        String ADS_CONSENT = "adsConsent";
        String GA_CONSENT = "gaConsent";
        String RECOMMENDATION_CONSENT = "recommendationConsent";

        String IS_FAIL_VERSION = "isFailVersion";
        String IS_SEND_TO_GA = "isSendToGA";
    }

    public interface DivyaNew {
        String VIDEO_REC_URL = "videoRecUrl";
    }

    public interface CSSData {
        String CSS_DATA = "articleCssData";
        String CSS_VERSION = "articleCssVersion";
    }

    public interface LiveTv {
        String PARTNER_ID = "livetv_partnerId";
        String TOKEN = "livetv_token";
        String USER_ID = "livetv_userId";
        String EXPIRY = "livetv_expiry";
    }

    public interface AdPref {
        String NATIVE_POSITION_1 = "native_pos_1";
        String NATIVE_POSITION_2 = "native_pos_2";
        String NATIVE_POSITION_3 = "native_pos_3";

        String BANNER_LIST_TOGGLE = "banner_list_toggle";
        String ATF_LIST_TOGGLE = "atf_list_toggle";
        String ATF_ARTICLE_TOGGLE = "atf_article_toggle";

        String BTF_ROS_ARTICLE_TOGGLE = "btf_ros_article_toggle";
        String BTF_ROS_LIST_TOGGLE = "btf_ros_list_toggle";

        String VIDEO_TOGGLE = "videoToggleAds";
        String VIDEO_BANNER_TOGGLE = "video_banner_toggle";
        String PHOTO_BANNER_TOGGLE = "photo_banner_toggle";
        String EXIT_BANNER_TOGGLE = "exit_banner_toggle";
    }

    public interface FeedData {
        String HOME_PAGE = "home_page_data";
        String HOME_PAGE_PG1 = "home_page_data_pg1";

        String NETWORK_TYPE = "network_type";
    }
}
