package com.db.util;

import com.bhaskar.R;

/**
 * Created by DB on 20-04-2017.
 */

public class RashifalUtil {

    private static final String[] mRashis = {"aries", "taurus", "gemini", "cancer",
            "leo", "virgo", "libra", "scorpio", "sagittarius", "capricorn",
            "aquarius", "pisces"};


    private static int[] rashiImageArray = {R.drawable.mesh_v2,
            R.drawable.vrishk_v2, R.drawable.mithun_v2, R.drawable.karka_v2,
            R.drawable.singh_v2, R.drawable.kanya_v2, R.drawable.tula_v2,
            R.drawable.vrushchik_v2, R.drawable.dhanu_v2, R.drawable.makar_v2,
            R.drawable.kumbha_v2, R.drawable.meen_v2};


    private static int getRashiImage(int index) {
        return rashiImageArray[index];
    }

    private static int getItemPostion(String[] array, String item) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equalsIgnoreCase(item))
                return i;
        }
        return -1;
    }

    static int getRashiImageFromName(String engName) {
        int pos = getItemPostion(mRashis, engName);
        if (pos < 0) {
            return R.mipmap.app_icon;
        }
        return getRashiImage(pos);
    }

    public static int getRashiIndex(String engName) {
        return getItemPostion(mRashis, engName);
    }
}
