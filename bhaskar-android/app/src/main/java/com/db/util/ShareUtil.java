package com.db.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.bhaskar.util.LoginController;
import com.db.InitApplication;
import com.bhaskar.R;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by DB on 11-03-2017.
 */

public class ShareUtil {

//    private static String msg = "For more latest update, download\n" + CommonConstants.APP_SHARE_URL;
    private static String msg = "";

    // FB Share
    public static boolean facebookShare(Activity mActivity, String title, String link) {
        ArrayList appList = getSharableAppList(mActivity);
        String body = title + "\n" + link + "\n\n" + msg;

        for (int i = 0; i < appList.size(); i++) {
            ResolveInfo resolveInfo = (ResolveInfo) appList.get(i);
            if (resolveInfo.activityInfo.packageName.contains("com.facebook.katana")) {
                Intent selectedIntent = new Intent("android.intent.action.SEND");
                selectedIntent.setPackage(resolveInfo.activityInfo.packageName);
                selectedIntent.putExtra("android.intent.extra.TEXT", body);
                selectedIntent.setType("text/plain");
                mActivity.startActivity(selectedIntent);
                return true;
            }
        }

        return false;
    }

    // Tweeter Share
    public static void tweeterShare(Activity mActivity, String title, String link) {
        try {
            String body = title + "\n" + link;
            String encodedBody = body;
            try {
                encodedBody = URLEncoder.encode(body, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }

            Intent i = new Intent();
            i.putExtra("android.intent.extra.TEXT", body);
            i.setAction("android.intent.action.VIEW");
            i.setData(Uri.parse("https://twitter.com/intent/tweet?text=" + encodedBody));
            mActivity.startActivity(i);
        } catch (ActivityNotFoundException e) {
            AppUtils.getInstance().showCustomToast(mActivity, "Tweeter not found.");
        }
    }

    // Gplus Share
    public static void gPlusShare(Activity mActivity, String title, String link) {
        ArrayList appList = getSharableAppList(mActivity);
        String body = title + "\n" + link + "\n\n" + msg;

        for (int i = 0; i < appList.size(); i++) {
            ResolveInfo resolveInfo = (ResolveInfo) appList.get(i);
            if (resolveInfo.activityInfo.packageName.contains("com.google.android.apps.plus")) {
                Intent selectedIntent = new Intent("android.intent.action.SEND");
                selectedIntent.setPackage(resolveInfo.activityInfo.packageName);
                selectedIntent.putExtra("android.intent.extra.TEXT", body);
                selectedIntent.putExtra("android.intent.extra.SUBJECT", "");
                selectedIntent.setType("text/plain");
                mActivity.startActivity(selectedIntent);
                break;
            }
        }
    }

    // Email Share
    public static void emailShare(Activity mActivity, String title, String link) {
        try {
            String body = title + "\n" + link + "\n\n" + msg;

            Intent email = new Intent("android.intent.action.SENDTO");
            email.setData(Uri.parse("mailto:"));
            email.putExtra("android.intent.extra.SUBJECT", mActivity.getResources().getString(R.string.checkout_this_app));
            email.putExtra("android.intent.extra.TEXT", body);
            mActivity.startActivity(Intent.createChooser(email, "Choose an Email client :"));
        } catch (ActivityNotFoundException e) {
            AppUtils.getInstance().showCustomToast(mActivity, "Email not found.");
        }
    }

    // Whatsapp Share
    public static boolean whatsappShare(Activity mActivity, String title, String link) {
        try {
            String body = title + "\n" + link + "\n\n" + msg;

            Intent whatsappIntent = new Intent("android.intent.action.SEND");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.setType("text/plain");
            whatsappIntent.putExtra("android.intent.extra.TEXT", body);
            mActivity.startActivity(whatsappIntent);
            return true;
        } catch (Exception e) {
            AppUtils.getInstance().showCustomToast(mActivity, "WhatsApp not found.");
        }

        return false;
    }

    public static ArrayList getSharableAppList(Context mContext) {
        PackageManager packageManager = mContext.getPackageManager();
        Intent queryIntent = new Intent("android.intent.action.SEND");
        queryIntent.setType("text/plain");
        return (ArrayList) packageManager.queryIntentActivities(queryIntent, 0);
    }

    public static void shareDefault(Context context, String message, String subject) {
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, message);
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            context.startActivity(Intent.createChooser(intent, "Share"));
        } catch (Exception e) {

        }
    }

    public static void shareDefault(Context context, String title, String subject, String link, String trackUrl, boolean fromDetail) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            StringBuilder shareContent = new StringBuilder(1);
            shareContent.append(title)
                    .append("\n")
                    .append(link);
//                    .append("\n\n")
//                    .append(msg);

            shareIntent.putExtra(Intent.EXTRA_TEXT, shareContent.toString());
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
            shareIntent.setType("text/plain");
            context.startActivity(Intent.createChooser(shareIntent, "Share via"));

            String campaign = AppPreferences.getInstance(context).getStringValue(QuickPreferences.UTM_CAMPAIGN, "");
            Tracking.trackGAEvent(context, InitApplication.getInstance().getDefaultTracker(), AppFlyerConst.GACategory.ARTICLE_EVENT, AppFlyerConst.GAAction.SHARE, trackUrl, campaign);

            CleverTapDB.getInstance(context).cleverTapTrackEvent(context, CTConstant.EVENT_NAME.CONTENT_SHARED, CTConstant.PROP_NAME.NEWS_URL, trackUrl, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.SHARED_THROUGH, fromDetail ? CTConstant.PROP_VALUE.STORY_DETAIL : CTConstant.PROP_VALUE.THREE_DOTS));
        } catch (Exception e) {
        }
    }
}
