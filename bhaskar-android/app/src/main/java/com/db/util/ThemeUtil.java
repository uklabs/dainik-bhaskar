package com.db.util;

import android.content.Context;

import com.bhaskar.R;


/**
 * Created by DB on 07-12-2016.
 */

public class ThemeUtil {

    public static int getCurrentTheme(Context context) {
        switch (AppPreferences.getInstance(context).getIntValue(Constants.Themes.THEME_COLOR, Constants.Themes.THEME_DEFAULT)) {
            case Constants.Themes.THEME_MARINE:
                return R.style.AppTheme_NoActionBar_Marine;

            case Constants.Themes.THEME_TURQUOISE:
                return R.style.AppTheme_NoActionBar_Turquoise;

            case Constants.Themes.THEME_ORANGE:
                return R.style.AppTheme_NoActionBar_Orange;

            case Constants.Themes.THEME_DARK_GRAY:
                return R.style.AppTheme_NoActionBar_DarkGray;

            case Constants.Themes.THEME_CITRUS:
                return R.style.AppTheme_NoActionBar_Citrus;
        }

        return R.style.AppTheme_NoActionBar_DarkGray;
    }
}
