package com.db.util;

import com.bhaskar.util.AppUrls;
import com.db.InitApplication;

public class Urls {

    public static final boolean IS_TESTING = true;

    //App Base Urls
    public static final String APP_FEED_BASE_URL = AppUrls.FEED_BASE_URL;
    private static final String APP_LOG_FEED_BASE_URL = AppUrls.LOG_FEED_BASE_URL;
    private static final String BASE_LOGIN_URL = AppUrls.BASE_URL;

    // Video Base Urls
    public static final String DEFAULT_VIDEO_ACTION = AppUrls.DEFAULT_VIDEO_ACTION;
    public static final String DEFAULT_VIDEO_PROVIDER = AppUrls.DEFAULT_VIDEO_PROVIDER;
    public static final String DEFAULT_DIVYA_VIDEO_REC_URL = AppUrls.DEFAULT_DIVYA_VIDEO_REC_URL;

    /* Log Feed Hit API's */
    /*{App Id}*/
    public static String APP_VERSION_URL = APP_LOG_FEED_BASE_URL + "version/%s/" + (IS_TESTING ? "?testing=1" : "");
    /*{App Id}*/
    public static String APP_HOST_URL = APP_LOG_FEED_BASE_URL + "host/%s/android/" + (IS_TESTING ? "?testing=1" : "");
    /*{App Id}*/
    public static String BASE_APP_CONTROL_URL = APP_LOG_FEED_BASE_URL + "baseappcontrolV4/%s/android/" + (IS_TESTING ? "?testing=1" : "");
    /*{App Id}*/
    public static String APP_CATEGORY_URL = APP_LOG_FEED_BASE_URL + "categoryV4/%s/android/" + (IS_TESTING ? "?testing=1" : "");
    /* {Channel Id} */
    public static final String APP_CITY_URL = APP_LOG_FEED_BASE_URL + "citylist/%s/";

    /*{channel id}*/
    public static final String DEFAULT_DETAIL_URL = "NewsV3/%s/";
    public static final String DEFAULT_LIST_URL = "News/%s/";
    public static final String RASHIFAL_WIDGET_URL = "JVDailyRashifal/%s/";
    public static final String FLICKER_WIDGET_URL = "FlickerNews/%s/";

    /* Coupon Rani API Url*/
    /*{LOAD MORE COUNT}*/
    public static final String COUPON_RANI_VOUCHER_APP_FEED_URL = "https://appfeedlight.bhaskar.com/couponrani/PG/%s/";

    /*{ Channel Id, search Keyword}*/
    public static final String SEARCH_NEWS_API_URL = AppUrls.FEED_BASE_URL + "keyword/%s/?q=%s/";

    /*{Channel Id, storyLink}*/
    public static final String ARTICLE_SHARE_URL = AppUrls.FEED_BASE_URL + "ArticleDynamicUrl/%s/";
    /*{Channel Id}*/
    public static final String RASHIFAL_ITEMS_URL = AppUrls.FEED_BASE_URL + "Rashifal/%s/";


    static final String AR_LINK_URL_INFO_V3 = AppUrls.FEED_BASE_URL + "UrlInfoV3/";
    static final String NEWS_URL_COMMON_SUFFIX = "/";

    ///*Login*/
    private static final String BASE_URL_WEB_API = BASE_LOGIN_URL + "webapiV3/";

    /*{App Id}*/
    public static final String APP_OFFER_URL = "Offers/%s/" + (IS_TESTING ? "?testing=1" : "");

    /*{Lang Id}*/
    public static final String APP_RADIO_URL = "RadioList/%s/";
    public static final String APP_DB_COLUMN_URL = Urls.APP_FEED_BASE_URL + "RasdharDailyCollmunList/%s/";
    public static final String APP_QUIZ_URL = Urls.APP_FEED_BASE_URL + "QuizSubAnswer/";

    //public static String LIVETV_LOGIN = "http://119.81.201.166:8099/partner/api/v3/register/user";
    public static String LIVETV_LOGIN = "https://ext.yupptv.in/partner/api/v3/register/user";

    /*{App Id}*/
    public static String AUTHOR_LIST_URL = AppUrls.FEED_BASE_URL + "DBColumnEditorList/%s/";

    public static String APP_SET_ACTIVE_USER = AppUrls.FEED_BASE_URL + "activeUser/";

    public static String RECOMMENDATION_CONSENT_URL = BASE_URL_WEB_API + "user_notification/";

    /*{css version}*/
    public static String CSS_API_URL = "https://i10.dainikbhaskar.com/images/appfeed/css/articleCss_%s_1.css?v%s";

    /*{channel_id},{app version}*/
    public static String TUTORIAL_URL = APP_LOG_FEED_BASE_URL + "AppTutorial/%s/android/%s/";

    public static String BASE_COMMENT_URL = "https://cldmum.bhaskar.com/comment/";
    /*{channelId}, {postId} */
    public static String RETRIEVE_ALL_COMMENTS = BASE_COMMENT_URL + "ttl/cmtgetcomment/%s/%s/";
    /*{channelId} */
    public static String ABOUT_US_URL = "https://i10.dainikbhaskar.com/images/appfeed/appUrl/%s/about_us.html";
    /*{channelId} */
    public static String TERMS_AND_CONDITION_URL = "https://i10.dainikbhaskar.com/images/appfeed/appUrl/%s/app-terms-conditions.html";

    static final String SHARE_WIN_POST_REF = "sharewin/postData/";
    private static String SW_DEFAULT_BASE_URL = "https://cldmum.bhaskar.com/appFeedV3/";
    public static String SW_BASE_URL = (InitApplication.getInstance() == null) ? SW_DEFAULT_BASE_URL : InitApplication.getInstance().getSWBaseUrl(SW_DEFAULT_BASE_URL);
}
