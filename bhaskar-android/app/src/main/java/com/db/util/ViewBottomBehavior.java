package com.db.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.db.util.bottom.TranslateAnimateHelper;
import com.db.util.bottom.ViewBehavior;


public class ViewBottomBehavior extends ViewBehavior {

    public ViewBottomBehavior(Context context) {
        super(context, null);
    }

    public ViewBottomBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return true;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        if (canInit) {
            canInit = false;
            mAnimateHelper = TranslateAnimateHelper.get(child);
            mAnimateHelper.setStartY(child.getY());
            mAnimateHelper.setMode(TranslateAnimateHelper.MODE_BOTTOM);
        }
        return super.onDependentViewChanged(parent, child, dependency);
    }


    @Override
    protected void onNestPreScrollInit(View child) {
    }

}
