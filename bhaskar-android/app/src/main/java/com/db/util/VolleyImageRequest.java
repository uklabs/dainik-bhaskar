package com.db.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by DB on 20-04-2017.
 */

public class VolleyImageRequest extends Request<Bitmap> {

    public static final String PARAMS_ENCODING = "UTF-8";
    private static final String TAG = VolleyImageRequest.class.getSimpleName();
    private final Response.Listener listener; // the response listener
    private Priority mPriority = Priority.NORMAL;

    public VolleyImageRequest(int requestMethod, String url, Response.Listener responseListener, Response.ErrorListener errorListener) {
        super(requestMethod, url, errorListener);
        this.listener = responseListener;
    }

    public static String getEncodedURL(String path, Map<String, String> params) {
        String url = path;
        if (params != null) {
            StringBuilder query = new StringBuilder();
            try {
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    query.append(URLEncoder.encode(entry.getKey(), PARAMS_ENCODING));
                    query.append("=");
                    query.append(URLEncoder.encode(entry.getValue(), PARAMS_ENCODING));
                    query.append("&");
                }
            } catch (UnsupportedEncodingException uee) {
                throw new RuntimeException("Encoding not supported: " + PARAMS_ENCODING, uee);
            }
            if (url.contains("&")) {
                url = url + "&" + query;
            } else {
                url = url + "?" + query;
            }
        }

        return url;
    }

    @Override
    protected void deliverResponse(Bitmap jsonObject) {
        listener.onResponse(jsonObject);
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(response.data, 0, response.data.length);
        return Response.success(bitmap, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected String getParamsEncoding() {
        return super.getParamsEncoding();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<>();
        params.put("Authorization", String.format("Basic %s", Base64.encodeToString(String.format("%s:%s", "admin", "admin").getBytes(), Base64.DEFAULT)));
        return params;
    }

    @Override
    public Priority getPriority() {
        return mPriority;
    }

    public void setPriority(Priority priority) {
        mPriority = priority;
    }
}

