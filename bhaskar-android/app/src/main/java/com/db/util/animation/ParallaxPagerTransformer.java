package com.db.util.animation;

import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

public class ParallaxPagerTransformer implements ViewPager.PageTransformer {

    private int mResId, mBorderWidth = 0;
    private float mSwipeSpeed = 0.2f;

    public ParallaxPagerTransformer(int id) {
        this.mResId = id;
    }

    @Override
    public void transformPage(@NonNull View view, float position) {
        try {
            View parallaxView = view.findViewById(mResId);
            if (parallaxView != null) {
                if (position > -1 && position < 1) {
                    float width = parallaxView.getWidth();
                    parallaxView.setTranslationX(-(position * width * mSwipeSpeed));
                    float sc = ((float) view.getWidth() - mBorderWidth) / view.getWidth();
                    if (position == 0) {
                        view.setScaleX(1);
                        view.setScaleY(1);
                    } else {
                        view.setScaleX(sc);
                        view.setScaleY(sc);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("transformPage", "" + e.getMessage());
        }
    }

    public void setBorderWidth(int px) {
        mBorderWidth = px;
    }

    public void setSpeed(float speed) {
        this.mSwipeSpeed = speed;
    }
}
