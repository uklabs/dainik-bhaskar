package com.db.views;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.R;

public class DBSplashTimeTicker extends RelativeLayout {

    public DBSplashTimeTicker(Context context) {
        super(context);
        createItems(context);
    }

    public DBSplashTimeTicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        createItems(context);
    }

    public DBSplashTimeTicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createItems(context);
    }

    private CountDownTimer countDownTimer;
    private long totalMillis;
    private long intervalText;
    private long intervalProgresBar;
    private ProgressBar progressBarCircle;
    private TextView timerText;

    private void createItems(Context context) {
        View rootView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.db_time_ticker, null);
        progressBarCircle = rootView.findViewById(R.id.progressBarCircle);
        timerText = rootView.findViewById(R.id.textViewTime);
        addView(rootView);
    }

    public void setValuesAndStartTimer(long totalMillis, long intervalValuesText, long intervalProgresBar) {
        this.totalMillis = totalMillis;
        this.intervalText = intervalValuesText;
        this.intervalProgresBar = intervalProgresBar;
        this.progressBarCircle.setMax((int) (totalMillis / intervalProgresBar));
        this.progressBarCircle.setProgress((int) (totalMillis / intervalProgresBar));
        this.countDownTimer = new CustomCountDownTimer(totalMillis, intervalProgresBar);
        this.countDownTimer.start();
    }


    boolean isPause = false;
    long remainingMillisOnPause = 0;

    public void onPause() {
        isPause = true;
    }

    public void onResume() {
        isPause = false;
        if (remainingMillisOnPause > 0) {
            cancel();
            long seconds = (remainingMillisOnPause / intervalText);
            long progres = (remainingMillisOnPause / intervalProgresBar);
            timerText.setText("Skip\n" + seconds);
            progressBarCircle.setProgress((int) (progres));
            countDownTimer = new CustomCountDownTimer(remainingMillisOnPause, intervalProgresBar);
            countDownTimer.start();
        }
    }

    public void cancel() {
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    private CountDownCallback countDownCallback;

    public void setCallback(CountDownCallback countDownCallback) {
        this.countDownCallback = countDownCallback;
    }

    public void stop() {
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    private class CustomCountDownTimer extends CountDownTimer {
        public CustomCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (countDownCallback != null)
                countDownCallback.onTick(millisUntilFinished);
            remainingMillisOnPause = millisUntilFinished;
            if (isPause) {
                cancel();
            }
            long seconds = (millisUntilFinished / intervalText);
            long progres = (millisUntilFinished / intervalProgresBar);
            timerText.setText("Skip\n" + seconds);
            progressBarCircle.setProgress((int) (progres));
            Log.e("TICKING", "TCKING:" + millisUntilFinished + "," + seconds);
        }

        @Override
        public void onFinish() {
            Log.e("FINISH", "FNISHED");
            if (countDownCallback != null)
                countDownCallback.onFinish();
            timerText.setText("Skip");
            progressBarCircle.setProgress(0);
        }
    }

    public void setTimerFinished() {
        progressBarCircle.setProgress(0);
        remainingMillisOnPause = 0;
        setText("Skip");
    }

    public void setText(String text) {
        if (timerText != null && text != null)
            timerText.setText(text);
    }

    public interface CountDownCallback {
        void onTick(long millisUntilFinished);

        void onFinish();
    }
}
