package com.db.views;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;

import com.bhaskar.R;

public class RecyclerViewMaxHeight extends RecyclerView {

    private final int maxHeight;

    public RecyclerViewMaxHeight(Context context) {
        this(context, null);
    }

    public RecyclerViewMaxHeight(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecyclerViewMaxHeight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RecyclerViewMaxHeight);
            maxHeight = typedArray.getDimensionPixelSize(R.styleable.RecyclerViewMaxHeight_maxHeight, Integer.MAX_VALUE);
            typedArray.recycle();
        } else {
            maxHeight = 0;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int measuredHeight = MeasureSpec.getSize(heightMeasureSpec);
        if (maxHeight > 0 && maxHeight < measuredHeight) {
            int measureMode = MeasureSpec.getMode(heightMeasureSpec);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(maxHeight, measureMode);
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
