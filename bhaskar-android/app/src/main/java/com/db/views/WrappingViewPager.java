package com.db.views;

import android.content.Context;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/**
 * ViewPager with dynamic height support. For basic usage, replace {@link ViewPager} with {@link WrappingViewPager}
 * in your layout file, and set its height property to {@code wrap_content}.
 * <p>
 * You also have to make your adapter inform the {@link WrappingViewPager} of every page change:
 * the easiest way to achieve this is to override {@link PagerAdapter#setPrimaryItem(ViewGroup, int, Object)}
 * and call {@link WrappingViewPager onPageChanged(View)}. To avoid unnecessary calls, only do this
 * when the page is changed, instead of the old one being reselected. For a basic example of this,
 * see how it is implemented in the library's own {@link WrappingFragmentStatePagerAdapter}.
 *
 * @author Santeri Elo
 * @author Abhishek V (http://stackoverflow.com/a/32410274)
 * @author Vihaan Verma (http://stackoverflow.com/a/32488566)
 * @since 14-06-2016
 */
public class WrappingViewPager extends ViewPager {
    boolean isPagingEnabled;
    private View mCurrentView;

    public WrappingViewPager(Context context) {
        super(context);
    }

    public void setPagingEnabled(boolean isPagingEnabled) {
        this.isPagingEnabled = isPagingEnabled;
    }

    public WrappingViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        try {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            int height = 0;
            if (mCurrentView != null) {
                for (int i = 0; i < getChildCount(); i++) {
                    mCurrentView.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
                    int h = mCurrentView.getMeasuredHeight();
                    if (h > height) height = h;
                }
                if (height != 0) {
                    heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
                }
            }
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } catch (Exception ex) {

        }
    }

    public void onPageChanged(View view) {
        mCurrentView = view;
        requestLayout();
    }
}