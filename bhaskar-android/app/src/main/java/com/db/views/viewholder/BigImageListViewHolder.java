package com.db.views.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.R;

/**
 * Created by DB on 9/22/2017.
 */

public class BigImageListViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivThumb;
    public ImageView moreIV;
    public TextView titleTextView, exclusiveTv;
    public ImageView imgVideoPlay;
    public RelativeLayout exclusiveLayout;
    public LinearLayout bulletLayout;

    public BigImageListViewHolder(View itemView) {
        super(itemView);

        titleTextView = itemView.findViewById(R.id.title_tv);

        titleTextView.setVisibility(View.VISIBLE);
        ivThumb = itemView.findViewById(R.id.thumb_image);
        imgVideoPlay = itemView.findViewById(R.id.img_video_play);
        bulletLayout = itemView.findViewById(R.id.bullet_layout);
        exclusiveLayout = itemView.findViewById(R.id.exclusive_layout);
        exclusiveTv = itemView.findViewById(R.id.exclusive_tv);
        moreIV = itemView.findViewById(R.id.more_img);

        itemView.setTag(itemView);
    }
}
