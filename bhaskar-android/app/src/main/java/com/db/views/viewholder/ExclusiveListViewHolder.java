package com.db.views.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.R;

/**
 * Created by DB on 9/22/2017.
 */

public class ExclusiveListViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivMore;
    public ImageView ivAuthor;
    public TextView tvHeader;
    public View vHeader;
    public TextView tvAuthor;
    public TextView tvTitle;
    public View vHeaderDivider;
    public ImageView ivDiagonal;
    public View vContainer;


    public ExclusiveListViewHolder(View itemView) {
        super(itemView);
        ivMore = itemView.findViewById(R.id.ivMore);
        vContainer = itemView.findViewById(R.id.vContainer);
        ivDiagonal = itemView.findViewById(R.id.ivDiagonal);
        ivAuthor = itemView.findViewById(R.id.ivAuthor);
        tvHeader = itemView.findViewById(R.id.tvHeader);
        vHeader = itemView.findViewById(R.id.vHeader);
        tvAuthor = itemView.findViewById(R.id.tvAuthor);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        vHeaderDivider = itemView.findViewById(R.id.vHeaderDivider);
        itemView.setTag(itemView);
    }

}