package com.db.views.viewholder;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.database.DatabaseClient;
import com.db.divya_new.MoreOptionInterface;
import com.db.divya_new.articlePage.DivyaCommonArticleActivity;
import com.db.news.WapV2Activity;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.db.util.Constants;
import com.db.util.ImageUtil;
import com.db.util.AppUtils;

import java.util.List;

public class GridFourViewHolderNewV2 extends RecyclerView.ViewHolder {

    public RecyclerView recyclerView;

    public GridFourViewHolderNewV2(View itemView) {
        super(itemView);
        recyclerView = itemView.findViewById(R.id.recycler_view);
        itemView.setTag(itemView);
    }

    public static class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
        private List<NewsListInfo> newsListInfos;
        private Context context;
        private String catId;

        public Adapter(Context context, List<NewsListInfo> newsListInfos, String catId) {
            this.newsListInfos = newsListInfos;
            this.context = context;
            this.catId = catId;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new ViewHolder(inflater.inflate(R.layout.recyclerlist_item_staggered, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            final NewsListInfo info1 = newsListInfos.get(position);

            float ratio = (position % 2 == 0) ? AppUtils.getInstance().parseImageRatio("16x25", Constants.ImageRatios.ARTICLE_DETAIL_RATIO) : AppUtils.getInstance().parseImageRatio("16x13", Constants.ImageRatios.ARTICLE_DETAIL_RATIO);
            AppUtils.getInstance().setImageViewHalfSizeWithAspectRatio(holder.ivThumb, ratio, 20, context);
            if (info1.image == null) {
                info1.image = "";
            }
            ImageUtil.setImage(context, info1.image, holder.ivThumb, position % 2 == 0 ? R.drawable.water_mark_vertical : R.drawable.water_mark_news_detail);

            if (info1.videoFlag == 1) {
                holder.ivVideoPlay.setVisibility(View.VISIBLE);
                holder.ivVideoPlay.setColorFilter(AppUtils.getThemeColor(context, info1.colorCode));
            } else {
                holder.ivVideoPlay.setVisibility(View.GONE);
            }

            /*Exclusive*/
            if (!TextUtils.isEmpty(info1.exclusive)) {
                holder.exclusiveLayout.setVisibility(View.VISIBLE);
                holder.exclusiveTv.setText(info1.exclusive);
            } else {
                holder.exclusiveLayout.setVisibility(View.GONE);
            }

            holder.carLayout.setOnClickListener(v -> {
                launchArticlePage(info1, position);
                holder.titleTextView.setTextColor(ContextCompat.getColor(context, R.color.article_read_color));
            });

            // first card
            try {
                holder.ivMore.setOnClickListener(v -> AppUtils.showMoreOptionsV2(context, v, new MoreOptionInterface() {
                    @Override
                    public void onBookMark() {
                        CategoryInfo categoryInfo = DatabaseClient.getInstance(context).getCategoryById(catId);
                        if (categoryInfo != null)
                            AppUtils.getInstance().bookmarkClick(context, info1, categoryInfo.detailUrl, categoryInfo.color, false);
                    }

                    @Override
                    public void onShare() {
                        AppUtils.getInstance().shareClick(context, info1.title, info1.webUrl, info1.gTrackUrl, false);
                    }
                }));

                holder.titleTextView.setText(AppUtils.getInstance().getHomeTitle(info1.iitl_title, AppUtils.getInstance().fromHtml(info1.title), info1.colorCode));
            } catch (Exception ignore) {
            }
        }

        @Override
        public int getItemCount() {
            return newsListInfos.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            ImageView ivMore, ivThumb, ivVideoPlay;
            TextView titleTextView;
            CardView carLayout;
            RelativeLayout exclusiveLayout;
            TextView exclusiveTv;

            ViewHolder(View itemView) {
                super(itemView);
                carLayout = itemView.findViewById(R.id.card_layout);
                ivMore = itemView.findViewById(R.id.more_img_1);
                titleTextView = itemView.findViewById(R.id.list_item_name_1);
                ivThumb = itemView.findViewById(R.id.thumb_image_1);
                ivVideoPlay = itemView.findViewById(R.id.img_video_play_1);
                exclusiveLayout = itemView.findViewById(R.id.exclusive_layout);
                exclusiveTv = itemView.findViewById(R.id.exclusive_tv);
            }
        }

        private void launchArticlePage(NewsListInfo newsListInfo, int position) {
            CategoryInfo categoryInfo = DatabaseClient.getInstance(context).getCategoryById(catId);
            if (newsListInfo.isWebView) {
                Intent intent = new Intent(context, WapV2Activity.class);
                intent.putExtra(Constants.KeyPair.KEY_URL, newsListInfo.webUrl);
                intent.putExtra(Constants.KeyPair.KEY_TITLE, newsListInfo.webUrl);
                if (categoryInfo != null) {
                    intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, ((TextUtils.isEmpty(categoryInfo.gaScreen)) ? AppFlyerConst.GAScreen.WEB_VIEW + "-" : categoryInfo.gaScreen) + newsListInfo.webUrl);
                } else {
                    intent.putExtra(Constants.KeyPair.KEY_GA_SCREEN, AppFlyerConst.GAScreen.WEB_VIEW + "-" + newsListInfo.webUrl);
                }
                context.startActivity(intent);
            } else {
                context.startActivity(DivyaCommonArticleActivity.getIntent(context, newsListInfo, categoryInfo, position));
            }
        }
    }
}
