package com.db.views.viewholder;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.R;

public class GuardianListViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivMore_1, ivThumb_1, ivVideoPlay_1;
    public TextView titleTextView_1;
    public CardView carLayout_1;

    public ImageView ivMore_2, ivThumb_2, ivVideoPlay_2;
    public TextView titleTextView_2;
    public CardView carLayout_2;
    public RelativeLayout exclusiveLayout1, exclusiveLayout2;
    public TextView exclusiveTv1, exclusiveTv2;

    public GuardianListViewHolder(View itemView) {
        super(itemView);

        carLayout_1 = itemView.findViewById(R.id.card_layout_1);
        ivMore_1 = itemView.findViewById(R.id.more_img_1);
        titleTextView_1 = itemView.findViewById(R.id.list_item_name_1);
        ivThumb_1 = itemView.findViewById(R.id.thumb_image_1);
        ivVideoPlay_1 = itemView.findViewById(R.id.img_video_play_1);

        carLayout_2 = itemView.findViewById(R.id.card_layout_2);
        ivMore_2 = itemView.findViewById(R.id.more_img_2);
        titleTextView_2 = itemView.findViewById(R.id.list_item_name_2);
        ivThumb_2 = itemView.findViewById(R.id.thumb_image_2);
        ivVideoPlay_2 = itemView.findViewById(R.id.img_video_play_2);
        exclusiveLayout1 = itemView.findViewById(R.id.exclusive_layout1);
        exclusiveTv1 = itemView.findViewById(R.id.exclusive_tv1);
        exclusiveLayout2 = itemView.findViewById(R.id.exclusive_layout2);
        exclusiveTv2 = itemView.findViewById(R.id.exclusive_tv2);
        itemView.setTag(itemView);
    }
}
