package com.db.views.viewholder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bhaskar.R;
import com.db.data.models.CategoryInfo;
import com.db.data.models.NewsListInfo;
import com.db.db_original.NewsGalleryFragment;
import com.db.util.AppUtils;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by DB on 9/22/2017.
 */

public class NewsGalleryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ViewPager viewPagerNews;
    private TextView pagerCountTv;
    private ImageView ivBackArrow;
    private ImageView ivForwardArrow;
    private Context context;
    private FragmentManager fragmentManager;
    private CategoryInfo categoryInfo;
    private List<NewsListInfo> galleryData;
    private int selectedPosition;

    public NewsGalleryViewHolder(View itemView, Context context, FragmentManager fragmentManager, CategoryInfo categoryInfo, int viewPagerHeight) {
        super(itemView);
        viewPagerNews = itemView.findViewById(R.id.view_pager_news);
        pagerCountTv = itemView.findViewById(R.id.tv_pager_count);
        ivBackArrow = itemView.findViewById(R.id.iv_back_arrow);
        ivForwardArrow = itemView.findViewById(R.id.iv_forward_arrow);
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.categoryInfo = categoryInfo;
        try {
            ViewGroup.LayoutParams params = viewPagerNews.getLayoutParams();
            params.height = viewPagerHeight;
        } catch (Exception ignored) {
        }
        ivForwardArrow.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);

        if (galleryData != null && galleryData.size() == 1) {
            itemView.findViewById(R.id.container_news_photo_counter).setVisibility(View.GONE);
        } else {
            itemView.findViewById(R.id.container_news_photo_counter).setVisibility(View.VISIBLE);
        }
        itemView.setTag(itemView);
    }


    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            selectedPosition = position;

            ivBackArrow.setVisibility(View.VISIBLE);
            ivForwardArrow.setVisibility(View.VISIBLE);
            if (position == 0) {
                ivBackArrow.setVisibility(View.GONE);
            }
            if (position == galleryData.size() - 1) {
                ivForwardArrow.setVisibility(View.GONE);
            }

            pagerCountTv.setText(context.getString(R.string.label_pager_count, "" + (selectedPosition + 1), "" + galleryData.size()));
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    public void setData(List<NewsListInfo> galleryData) {
        this.galleryData = galleryData;
        ivBackArrow.setVisibility(View.GONE);
        ivForwardArrow.setVisibility(View.GONE);
        viewPagerNews.addOnPageChangeListener(onPageChangeListener);
        viewPagerNews.setPageTransformer(false, AppUtils.getParallaxPagerTransform(R.id.article_iv, 0, 0.5f));
        viewPagerNews.setAdapter(new NewsGalleryPagerAdapter(fragmentManager));
        viewPagerNews.setCurrentItem(0);
        if (galleryData != null && galleryData.size() > 1) {
            ivBackArrow.setVisibility(View.GONE);
            ivForwardArrow.setVisibility(View.VISIBLE);

        } else {
            ivBackArrow.setVisibility(View.GONE);
            ivForwardArrow.setVisibility(View.GONE);
        }
        assert galleryData != null;
        pagerCountTv.setText(context.getString(R.string.label_pager_count, "" + (selectedPosition + 1), "" + galleryData.size()));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back_arrow:
                int lpos = selectedPosition - 1;
                if (lpos >= 0)
                    viewPagerNews.setCurrentItem(lpos);
                break;
            case R.id.iv_forward_arrow:
                int rpos = selectedPosition + 1;
                if (rpos < galleryData.size())
                    viewPagerNews.setCurrentItem(rpos);
                break;

        }
    }


    private class NewsGalleryPagerAdapter extends FragmentStatePagerAdapter {

        NewsGalleryPagerAdapter(@NonNull FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return galleryData.size();
        }

        @NotNull
        @Override
        public Fragment getItem(int position) {
            return NewsGalleryFragment.newInstance(galleryData.get(position), position, categoryInfo);
        }
    }
}
