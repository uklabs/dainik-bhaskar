package com.db.views.viewholder;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.R;

/**
 * Created by DB on 9/22/2017.
 */

public class RecListViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivThumb;
    public TextView titleTextView;
    public CardView rowLayout;
    public ImageView imgVideoPlay;
    public ImageView moreIV;
    public RelativeLayout exclusiveLayout;
    public TextView exclusiveTv;

    public RecListViewHolder(View itemView) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.list_item_name);
        ivThumb = itemView.findViewById(R.id.thumb_image);
        rowLayout = itemView.findViewById(R.id.related_article_recycler_view_item_rl);
        moreIV = itemView.findViewById(R.id.more_img);
        imgVideoPlay = itemView.findViewById(R.id.img_video_play);
        exclusiveLayout = itemView.findViewById(R.id.exclusive_layout);
        exclusiveTv = itemView.findViewById(R.id.exclusive_tv);
        itemView.setTag(itemView);
    }

}
