package com.bhaskar.util;

public class CommonConstants {
    public static final String CHANNEL_ID = "5483";
    public static final String BHASKAR_APP_ID = "20";

    public static String CONTENT_URL_VALUE = "https://divyamarathi.bhaskar.com/";
    public static final String TRACKING_PREFIX = "DVM";
    public static final String FEED_BACK_EMAIL_ID = "mobileapp@dainikbhaskar.com";

    public static final String EVENT_LABEL = "dvm";

    public static final String CHANNEL_NEWS = "5483";
    public static final String CHANNEL_BUSINESS = "5558";
    public static final String CHANNEL_RELIGION = "5494";

    public static final String APP_SHARE_URL = "https://divyamarathi.page.link/appshare";

    //Ads Publisher App ID
    public static final String ADMOB_APP_ID = "ca-app-pub-3985955077200088~8965948255";
    public static final String DFP_PUBLISHER_ID_FOR_CONSENT = "131958808";

    /*app widget & sticky api feed url*/
    public static String APP_WIDGET_STICKY_FEED_URL = "https://appfeedlight.bhaskar.com/appFeedV3/News/5483/11884/1/PG1/";
    public static final String USER_AGENT = "DivyaMarathiAndroid";

    public static String rashifalTabTitles[] = new String[]{"Daily", "Weekly", "Monthly", "Yearly"};
    public static String rashifalTabTitlesEnglish[] = new String[]{"Daily", "Weekly", "Monthly", "Yearly"};
}
