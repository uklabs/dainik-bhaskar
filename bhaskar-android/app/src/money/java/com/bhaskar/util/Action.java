package com.bhaskar.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static com.bhaskar.util.Action.CategoryAction.*;

public class Action {


    /************
     * Categories
     * ************************************************************/
    public interface CategoryAction {

        //Web view action
        String CAT_ACTION_WAP = "WAP";
        String CAT_ACTION_WAP_USER = "WAP_USER";
        String CAT_ACTION_WAP_V3 = "WAP_V3";
        String CAT_ACTION_FULL_WAP = "WAP_FULL";
        String CAT_ACTION_CHROME = "Chrome";
        String CAT_ACTION_CHROME_TAB = "Chrome_Tab";
        String CAT_ACTION_TV = "WAP_TV";

        // List action
        String CAT_ACTION_NEWS = "News";
        String CAT_ACTION_CITY_SELECTION = "CitySelection";
        String CAT_ACTION_CITY_SELECTION_V2 = "CitySelection_V2";
        String CAT_ACTION_PHOTO_GALLERY = "Photo Gallery";
        String CAT_ACTION_MOVIE_REVIEW = "Movie Review";
        String CAT_ACTION_MOVIE_PREVIEW = "Movie Preview";
        String CAT_ACTION_VIDEO_GALLERY_V3 = "VideoGallery_V3";
        String CAT_ACTION_RAJYA = "Rajya_V3";
        String CAT_ACTION_RAJYA_HOME = "Rajya_Home";
        String CAT_ACTION_RAJYA_LIST = "Rajya_List";
        String CAT_ACTION_ALL_CAT_HOME = "All_Cat_Home";
        String CAT_ACTION_CAT_MENU = "Cat_Menu";
        String CAT_ACTION_GURUVANI_GURU = "GuruwaniGuru";
        String CAT_ACTION_GURUVANI_TOPIC = "GuruwaniTopic";
        String CAT_ACTION_STORY_LIST = "Story";
        String CAT_ACTION_STORY_LIST_V2 = "Story_V2";
        String CAT_ACTION_CAT_HOME = "Cat_Home";
        String CAT_ACTION_RASHIFAL_V2 = "Rashifal_V2";
        String CAT_ACTION_RATNA = "Ratna";
        String CAT_ACTION_SONGS = "Songs";
        String CAT_ACTION_EPAPER = "epaper";
        String CAT_ACTION_BULLETIN = "Bulletin";
        String CAT_ACTION_DB_COLUMN = "Daily_Column";
        String CAT_ACTION_OFFER = "Offer";
        String CAT_ACTION_KAHANIYA = "Kahaniya";
        String CAT_ACTION_EXCLUSIVE = "Exclusive";
        String CAT_ACTION_MERA_PAGE = "MyPage";
        String CAT_ACTION_DAILY_QUIZ = "Daily_Quiz";
        String CAT_ACTION_SEARCH_WIDGET = "Search_Widget";
        String CAT_ACTION_VIDEO_SECTION = "VideoSection";
        String CAT_ACTION_VIDEO_SECTION_LIST = "VideoSectionList";

        // Outside
        String CAT_ACTION_RADIO = "Radio";
        String CAT_ACTION_NEWS_BRIEF = "Brief";

        String CAT_ACTION_DB_ORG = "DBOriginal";
        String CAT_ACTION_DB_ORG_NEWS = "DBOriginalNews";
        String CAT_ACTION_DB_ORG_COLUMN = "DBOriginalColumn";
        String CAT_ACTION_DB_ORG_PHOTO = "DBOriginalPhoto";
        String CAT_ACTION_DB_ORG_VIDEO = "DBOriginalVideo";

        String CAT_ACTION_SEPARATOR = "Separator";
        String CAT_ACTION_NOTIFICATION_CENTER = "NotificationCenter";
        String CAT_ACTION_BOOKMARK = "Bookmark";
        String CAT_ACTION_SETTINGS = "Settings";
        String CAT_ACTION_SHARE_APP = "ShareApp";

        // Offers
        String CAT_ACTION_OFFER_SHARE_WIN = "ShareWin_V2";
        String CAT_ACTION_OFFER_COUPON = "xyzLaunch";
        String CAT_ACTION_OFFER_MEGA_OFFER = "xyzMegaOffer";
        String CAT_ACTION_OFFER_TAMBOLA = "Tambola_V2";
        String CAT_ACTION_OFFER_EMPTY = "Empty";
        String CAT_OFFER_VERSION = "Offer_version";

        // Bottom navigation
        String CAT_HOME_NAV = "Nav_Home";
        String CAT_PRIME_NAV = "Nav_Prime";
        String CAT_MORE_NAV = "Nav_More";
    }

    public static String[] allCateg;
    public static List<String> ALL_HOME_CATEGORY_ACTIONS;

    public static String[] allBottomCateg;
    public static List<String> ALL_HOME_CATEGORY_BOTTOM_ACTIONS;

    static {
        allCateg = new String[]{CAT_ACTION_NEWS, CAT_ACTION_CITY_SELECTION,CAT_ACTION_CITY_SELECTION_V2, CAT_ACTION_PHOTO_GALLERY, CAT_ACTION_WAP, CAT_ACTION_WAP_V3, CAT_ACTION_WAP_USER, CAT_ACTION_MOVIE_PREVIEW, CAT_ACTION_MOVIE_REVIEW, CAT_ACTION_RAJYA,
                CAT_ACTION_ALL_CAT_HOME, CAT_ACTION_CAT_MENU, CAT_ACTION_VIDEO_GALLERY_V3, CAT_ACTION_STORY_LIST, CAT_ACTION_STORY_LIST_V2, CAT_ACTION_CAT_HOME,
                CAT_ACTION_GURUVANI_GURU, CAT_ACTION_KAHANIYA, CAT_ACTION_GURUVANI_TOPIC, CAT_ACTION_RASHIFAL_V2, CAT_ACTION_RATNA,  CAT_ACTION_SONGS, CAT_ACTION_EPAPER, CAT_ACTION_BULLETIN,
                CAT_ACTION_NEWS_BRIEF, CAT_ACTION_OFFER, CAT_ACTION_DB_COLUMN, CAT_ACTION_EXCLUSIVE, CAT_ACTION_DB_COLUMN, CAT_ACTION_CHROME, CAT_ACTION_CHROME_TAB, CAT_ACTION_TV, CAT_ACTION_SEPARATOR, CAT_ACTION_NOTIFICATION_CENTER,
                CAT_ACTION_BOOKMARK, CAT_ACTION_SETTINGS, CAT_ACTION_SHARE_APP, CAT_ACTION_MERA_PAGE, CAT_ACTION_DAILY_QUIZ, CAT_ACTION_RAJYA_HOME, CAT_ACTION_RAJYA_LIST, CAT_ACTION_RADIO,
                 CAT_ACTION_OFFER_TAMBOLA, CAT_ACTION_OFFER_MEGA_OFFER, CAT_ACTION_FULL_WAP, CAT_ACTION_DB_ORG, CAT_ACTION_DB_ORG_NEWS, CAT_ACTION_DB_ORG_COLUMN, CAT_ACTION_DB_ORG_PHOTO, CAT_ACTION_DB_ORG_VIDEO, CAT_ACTION_VIDEO_SECTION,CAT_ACTION_VIDEO_SECTION_LIST};

        ALL_HOME_CATEGORY_ACTIONS = Arrays.asList(allCateg);

        ALL_HOME_CATEGORY_BOTTOM_ACTIONS = new ArrayList<>();
        ALL_HOME_CATEGORY_BOTTOM_ACTIONS.addAll(ALL_HOME_CATEGORY_ACTIONS);
        ALL_HOME_CATEGORY_BOTTOM_ACTIONS.add(CAT_HOME_NAV);
        ALL_HOME_CATEGORY_BOTTOM_ACTIONS.add(CAT_MORE_NAV);
        ALL_HOME_CATEGORY_BOTTOM_ACTIONS.add(CAT_PRIME_NAV);

        allBottomCateg = ALL_HOME_CATEGORY_BOTTOM_ACTIONS.toArray(new String[0]);
    }

    // Outside open categories
    public static String[] outsideOpenCateg = new String[]{CAT_ACTION_RADIO, CAT_ACTION_EPAPER, CAT_ACTION_SETTINGS, CAT_ACTION_BOOKMARK, CAT_ACTION_NOTIFICATION_CENTER, CAT_ACTION_TV, CAT_ACTION_CHROME, CAT_ACTION_WAP, CAT_ACTION_CHROME_TAB,
            CAT_ACTION_OFFER_MEGA_OFFER, CAT_ACTION_OFFER_TAMBOLA, CAT_ACTION_OFFER, CAT_ACTION_SHARE_APP, CAT_ACTION_FULL_WAP, CAT_ACTION_DB_ORG, CAT_ACTION_NEWS_BRIEF, CAT_ACTION_BULLETIN};

    public static final List<String> OUTSIDE_OPEN_CATEGORY_ACTIONS = Arrays.asList(outsideOpenCateg);

    // Offers
    public static String[] allOffer = new String[]{CAT_ACTION_OFFER_SHARE_WIN, CAT_ACTION_OFFER_COUPON, CAT_ACTION_OFFER_EMPTY, CAT_ACTION_WAP, CAT_ACTION_OFFER_MEGA_OFFER, CAT_ACTION_OFFER_TAMBOLA};

    public static final List<String> ALL_OFFER_ACTION = Arrays.asList(allOffer);

    // Db Original
    public static String[] allDbOriginal = new String[]{CAT_ACTION_DB_ORG, CAT_ACTION_DB_ORG_NEWS, CAT_ACTION_DB_ORG_COLUMN, CAT_ACTION_DB_ORG_PHOTO, CAT_ACTION_DB_ORG_VIDEO, CAT_ACTION_NEWS, CAT_ACTION_VIDEO_GALLERY_V3, CAT_ACTION_PHOTO_GALLERY};

    public static final List<String> ALL_DB_ORIGINAL_ACTION = Arrays.asList(allDbOriginal);
}
