package com.bhaskar.util;

import android.content.Context;

public class CssConstants {

    public static String themeColor = "#46853d";
    public static String fontSizeRegular = "18px";
    public static String fontSizeLarge = "20px";
    public static String lineHeightRegular = "26px";
    public static String lineHeightLarge = "28px";
    public static String anchorColor = "#6F9A6A";


    public static String getTickerContent(String data) {
        return "<html><head>"
                + "<style action=\"text/css\">" +
                " .dot {\n" +
                "  height: 9px;\n" +
                "  width: 9px;\n" +
                "  background-color: #46853d;\n" +
                "  border-radius: 50%;\n" +
                "  display: inline-block;\n" +
                "}"
                + "@font-face { font-family: \"DivyaBhaskar2\"; src: url('file:///android_asset/fonts/articlefont.ttf');} body { font-family:\"DivyaBhaskar2\"}"
                + "body{ font-size:16px; color:#000;line-height:20px;padding:0px;}"
                + "</style>"
                + "<body>"
                + "<marquee behavior=\"scroll\" direction=\"left\" width = \"100%\" height = \"100%\" style=\" position: absolute; text-align: center;\">"
                + data
                + "</marquee>"
                + "</body></html>";
    }

    public static String getStockTickerContent(String data) {
        return "<html><head>"
                + "<style action=\"text/css\">" +
                " .dot {\n" +
                "  height: 9px;\n" +
                "  width: 9px;\n" +
                "  background-color: #e56d00;\n" +
                "  border-radius: 50%;\n" +
                "  display: inline-block;\n" +
                "}"
                + "@font-face { font-family: \"DivyaBhaskar2\"; src: url('file:///android_asset/fonts/appfont.ttf');} body { font-family:\"DivyaBhaskar2\"}"
                + "body{ font-size:15px; color:#000;line-height:20px;padding:0px;}"
                + "</style>"
                + "<body>"
                + "<marquee scrolldelay=\"100\" direction=\"left\" behavior=\"scroll\" width = \"100%\" height = \"100%\" style=\" position: absolute; text-align: center;\">"
                + data
                + "</marquee>"
                + "</body></html>";
    }

    public static String getContentWithFontFamily(String data) {
        return "<html><head>"
                + "<style action=\"text/css\">"
                + "@font-face { font-family: \"DivyaBhaskar2\"; src: url('file:///android_asset/fonts/articlefont.ttf');} body { font-family:\"DivyaBhaskar2\"}"
                + "body{ background-color: #FFFFFF; text-align:justify; margin:0px 15px 0px 15px; }"
                + "</style></head>"
                + "<body>"
                + data
                + "</body></html>";

    }

    public static String getContentWithFontFamily(String data, String cssData) {
        return "<html><head>"
                + "<style action=\"text/css\">"
                + "@font-face { font-family: \"DivyaBhaskar2\"; src: url('file:///android_asset/fonts/articlefont.ttf');} body { font-family:\"DivyaBhaskar2\"}"
                + "body{ background-color: #FFFFFF; text-align:justify; margin:0px 15px 0px 15px; }"
                + cssData
                + "</style></head>"
                + "<body>"
                + data
                + "</body></html>";
    }

    public static String getContentWithFontFamily_Novel(String data) {
        return "<html><head>"
                + "<style action=\"text/css\">"
                + "@font-face { font-family: \"DivyaBhaskar2\"; src: url('file:///android_asset/fonts/articlefont.ttf');} body { font-family:\"DivyaBhaskar2\"}"
                + "body{ background-color: #FFFFFF; text-align:justify; margin:15px 15px 15px 15px; }"
                + "</style></head>"
                + "<body>"
                + data
                + "</body></html>";
    }


    public static String getContentWithFontFamily(Context context, String data, String cssData) {
        String fontData = "appfont.ttf";
//        String appFontValue = AppPreferences.getInstance(context).getStringValue("App_font_Value", "appFont1");
//        if (appFontValue.equalsIgnoreCase("appFont1")) {
//            fontData = "appfont.ttf";
//        } else if (appFontValue.equalsIgnoreCase("appFont2")) {
//            fontData = "HindVadodara-Regular.ttf";
//        } else if (appFontValue.equalsIgnoreCase("appFont3")) {
//            fontData = "MuktaVaani-Regular.ttf";
//        } else if (appFontValue.equalsIgnoreCase("appFont4")) {
//            fontData = "NotoSans-Regular_0.ttf";
//        } else if (appFontValue.equalsIgnoreCase("appFont5")) {
//            fontData = "lohitgujarati.ttf";
//        } else if (appFontValue.equalsIgnoreCase("appFont6")) {
//            fontData = "SHRUTI.TTF";
//        }
        return "<html><head>"
                + "<style action=\"text/css\">"
                + "@font-face { font-family: \"DivyaBhaskar2\"; src: url('file:///android_asset/fonts/" +
                fontData +
                "');} body { font-family:\"DivyaBhaskar2\"}"
                + "body{ background-color: #FFFFFF; text-align:justify; margin:0px 15px 0px 15px; }"
                + cssData
                + "</style></head>"
                + "<body>"
                + data
                + "</body></html>";
    }

    public static String getVideoContent(String data) {
        return "<html><head>"
                + "</head>"
                + "<body>"
                + data
                + "</body></html>";
    }



    /*default css*/
    public static String defaultCssData = "*{margin:0px; padding:0px; font-size: font_size; line-height: line_height; }ul, ol{ list-style-type:none; }\n" +
            ".artticleTxt p{ padding-bottom:5px; }\n" +
            ".artticleTxt{ text-align: justify; margin-top:5px; }\n" +
            "a { text-decoration: underline; color: anchor_color; }\n" +
            ".artticleTxt ul,.artticleTxt ol{ padding-left: 20px; margin:10px 0px; }\n" +
            ".artticleTxt ul li{  position:relative; }\n" +
            ".artticleTxt ul li:before{content:\"\";height:7px;width:7px;border-radius:50%;position:absolute;left:-15px;top:9px;background:app_color;}\n" +
            ".pointersTxt h4{ border-bottom:2px solid app_color; text-align: center; margin:10px 0px; padding-bottom: 3px; }\n" +
            ".pointersTxt h5{ text-align: center; margin: 5px 0px; color: app_color; }\n" +
            ".artticleTxt h3 { color:#000; padding:5px 0px;}\n" +
            ".pointersTxt ul > li{ border-bottom:1px solid #e8e8e8; padding-bottom: 10px; }\n" +
            ".pointersTxt ul > li + li{ margin-top: 10px; }\n" +
            ".pointerTxt{ position: relative; padding-left: 20px; }\n" +
            ".pointerTxt .increse{ position:absolute; top:2px; left: 0px; font-weight:bold; color: app_color; }\n" +
            ".artTag{ margin-top: 15px;}\n" +
            ".artTag li figure{clear:both; margin-bottom: 10px;}\n" +
            ".artTag li{ border-bottom: 1px solid #e8e8e8; padding-bottom:10px; }\n" +
            ".artTag li+li{ margin-top:10px; }\n" +
            ".artTag li span.circle{ background: app_color; color: #fff; border-radius: 50%; font-weight: bold; display: flex; width: 33px; height: 33px; float: left; align-items: center; justify-content: space-around; margin-bottom: 10px; }\n" +
            ".artTag li figure img{ vertical-align:middle; width:100%;}\n" +
            ".artTag li h3{ width: calc(100% - 45px); float: right; margin-bottom: 10px;}\n" +
            "table { border-collapse: collapse; border: solid 1px #ddd; width: 100% !important; white-space: normal; margin:10px 0px; }\n" +
            "table td { padding: 5px; border: solid 1px #ddd; word-wrap: break-word; }\n" +
            ".scrollablecont{color:#000;  padding:10px 10px 0px 10px; }";
}
