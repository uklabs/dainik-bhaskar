package com.bhaskar.appscommon.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.appscommon.tracking.util.Systr;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;

public class AdController {

    public interface OnAdListener {
        void onBannerLoaded();
    }

    private static final int AD_SIZE_320_50 = 1;
    private static final int AD_SIZE_300_250 = 2;

    private static final int AD_TYPE_EXIT_POPUP = 1;
    private static final int AD_TYPE_ARTICLE_ATF = 2;
    private static final int AD_TYPE_ARTICLE_BTF = 3;
    private static final int AD_TYPE_ARTICLE_BTF_50 = 4;
    private static final int AD_TYPE_HOME_LISTING_ATF = 5;
    private static final int AD_TYPE_HOME_LISTING_BTF = 6;
    private static final int AD_TYPE_ALL_LISTING_ATF = 7;
    private static final int AD_TYPE_PHOTO_GALLERY_BTF = 8;
    private static final int AD_TYPE_VIDEO_GALLERY = 9;

    // Live
//    private static final String AD_UNIT_Exit_Popup = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_300x250_Exit";
//    private static final String AD_UNIT_Article_ATF = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_AS_TOP_320x50";
//    private static final String AD_UNIT_Article_BTF = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_AS_BOTTOM_300x250";
//    private static final String AD_UNIT_Article_BTF_50 = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_AS_BOTTOM_320x50";
//
//    private static final String AD_UNIT_Home_Listing_ATF = "/28928716/Divyabhaskar_APP/Home/DVB_APP_AOS_HP_Top_320x50";
//    private static final String AD_UNIT_Home_Listing_BTF = "/28928716/Divyabhaskar_APP/Home/DVB_APP_AOS_HP_Bottom_300x250";
//
//    private static final String AD_UNIT_All_Listing_ATF = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_AL_TOP_320x50";
//
//    private static final String AD_UNIT_Photo_Gallery_BTF = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_Photo_Gallery_320x50";
//
//    private static final String AD_UNIT_VIDEO_GALLERY = "/28928716/Divyabhaskar_APP/ROS/DVB_APP_AOS_ROS_Video_Gallery_320x50";


    // Testing
//    private static final String AD_UNIT_Article_ATF = "/6499/example/banner";
//    private static final String AD_UNIT_Article_BTF = "ca-app-pub-3940256099942544/6300978111";
//    private static final String AD_UNIT_NATIVE_LISTING = "/6499/example/native";
//    private static final String AD_UNIT_VIDEO_GALLERY = "/6499/example/banner";

    // Exit Popup
//    public static void showExitPopup(Context context, ViewGroup adLayout) {
//        if (adLayout != null)
//            showBannerAd(context, adLayout, AD_SIZE_300_250, AD_TYPE_EXIT_POPUP);
//    }

    // Article page top
    public static void showArticleATF(Context context, ViewGroup adLayout) {
        if (adLayout != null)
            showBannerAd2(context, adLayout, AD_SIZE_320_50, AD_TYPE_ARTICLE_ATF);
    }

    // Article page bottom
    public static void showArticleBTF(Context context, ViewGroup adLayout) {
        if (adLayout != null)
            showBannerAd(context, adLayout, AD_SIZE_300_250, AD_TYPE_ARTICLE_BTF);
    }

    // Article page bottom flicker
    public static void showArticleBTF_50(Context context, ViewGroup adLayout) {
        if (adLayout != null)
            showBannerAd(context, adLayout, AD_SIZE_320_50, AD_TYPE_ARTICLE_BTF_50);
    }

    // Article page top
    public static AdInfo showArticleATFForListing(Context context) {
        return showBannerAd(context, AD_SIZE_320_50, AD_TYPE_ARTICLE_ATF, null);
    }

    // Article page bottom
    public static AdInfo showArticleBTFForListing(Context context) {
        return showBannerAd(context, AD_SIZE_300_250, AD_TYPE_ARTICLE_BTF, null);
    }

    // Home listing top
    public static AdInfo showATFForHomeListing(Context context, OnAdListener listener) {
        AdInfo adInfo = showBannerAd(context, AD_SIZE_320_50, AD_TYPE_HOME_LISTING_ATF, listener);
        adInfo.catType = 1;
        return adInfo;
    }

    // Home listing bottom
    public static AdInfo showBTFForHomeListing(Context context) {
        return showBannerAd(context, AD_SIZE_300_250, AD_TYPE_HOME_LISTING_BTF, null);
    }

    // All page listing top
    public static AdInfo showATFForAllListing(Context context, OnAdListener listener) {
        AdInfo adInfo = showBannerAd(context, AD_SIZE_320_50, AD_TYPE_ALL_LISTING_ATF, listener);
        adInfo.catType = 1;
        return adInfo;
    }

    // Photo Gallery bottom
    public static void showPhotoGalleryBTF(Context context, ViewGroup adLayout) {
        if (adLayout != null)
            showBannerAd(context, adLayout, AD_SIZE_320_50, AD_TYPE_PHOTO_GALLERY_BTF);
    }

    // Video Gallery middle below player
    public static void showVideoGalleryBanner(Context context, ViewGroup adLayout) {
        if (adLayout != null)
            showBannerAd(context, adLayout, AD_SIZE_320_50, AD_TYPE_VIDEO_GALLERY);
    }


    private static void showBannerAd(Context context, final ViewGroup adLayout, final int adSize, final int adUnit) {
        try {
//            adLayout.setVisibility(View.GONE);

            PublisherAdView adView = getAdView(context, adSize, adUnit);

            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    Systr.println("ADS : Loaded : AdUnit : " + adUnit + ", AdSize : " + adSize);
                    adLayout.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    Systr.println("ADS : Failed : AdUnit : " + adUnit + ", AdSize : " + adSize + ", Error Code : " + i);
                    adLayout.setVisibility(View.GONE);
                }
            });

            PublisherAdRequest.Builder builder = new PublisherAdRequest.Builder();
            builder.addTestDevice("64EF4FFB20599B749E1E5C9DE67C079D");

            Systr.println("ADS : Banner Custom Params : AdUnit : " + adUnit + ", GDPR : " + AdsConstants.gdpr_blocked_boolean);
            builder.addCustomTargeting("gdpr_blocked", AdsConstants.gdpr_blocked_boolean ? "true" : "false");
//        if (AdsConstants.gdpr_blocked_boolean) {
//            Bundle bundle = new Bundle();
//            bundle.putString("npa", "1");
//            builder.addNetworkExtrasBundle(AdMobAdapter.class, bundle);
//        }

            adView.loadAd(builder.build());

            adLayout.removeAllViews();
            adLayout.addView(adView);
        } catch (Exception e) {
        }
    }

    private static void showBannerAd2(Context context, final ViewGroup adLayout, final int adSize, final int adUnit) {
        try {
            final PublisherAdView adView = getAdView(context, adSize, adUnit);
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    Systr.println("ADS : Loaded : AdUnit : " + adUnit + ", AdSize : " + adSize);
                    adLayout.setVisibility(View.VISIBLE);
                    adLayout.removeAllViews();
                    adLayout.addView(adView);
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    Systr.println("ADS : Failed : AdUnit : " + adUnit + ", AdSize : " + adSize + ", Error Code : " + i);
                }
            });

            Systr.println("ADS : Banner Custom Params : AdUnit : " + adUnit + ", GDPR : " + AdsConstants.gdpr_blocked_boolean);

            PublisherAdRequest.Builder builder = new PublisherAdRequest.Builder();
            builder.addTestDevice("64EF4FFB20599B749E1E5C9DE67C079D");
            builder.addCustomTargeting("gdpr_blocked", AdsConstants.gdpr_blocked_boolean ? "true" : "false");
            adView.loadAd(builder.build());

        } catch (Exception e) {
        }
    }

    private static AdInfo showBannerAd(Context context, final int adSize, final int adUnit, final OnAdListener listener) {
        final AdInfo adInfo = new AdInfo();
        adInfo.type = AdsConstants.AD_TYPE_BANNER;

        try {
            PublisherAdView adView = getAdView(context, adSize, adUnit);
            adInfo.adUnitId = adView.getAdUnitId();

            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    Systr.println("ADS : Loaded : AdUnit : " + adUnit + ", AdSize : " + adSize);
                    if (listener != null) {
                        listener.onBannerLoaded();
                    }
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    Systr.println("ADS : Failed : AdUnit : " + adUnit + ", AdSize : " + adSize + ", Error Code : " + i);
                    adInfo.bannerAdView = null;
                }
            });

            PublisherAdRequest.Builder builder = new PublisherAdRequest.Builder();
            builder.addTestDevice("64EF4FFB20599B749E1E5C9DE67C079D");

            Systr.println("ADS : Banner Custom Params : AdUnit : " + adUnit + ", GDPR : " + AdsConstants.gdpr_blocked_boolean);
            builder.addCustomTargeting("gdpr_blocked", AdsConstants.gdpr_blocked_boolean ? "true" : "false");
//        if (AdsConstants.gdpr_blocked_boolean) {
//            Bundle bundle = new Bundle();
//            bundle.putString("npa", "1");
//            builder.addNetworkExtrasBundle(AdMobAdapter.class, bundle);
//        }

            adView.loadAd(builder.build());

            adInfo.bannerAdView = adView;
        } catch (Exception e) {
        }

        return adInfo;
    }

    private static PublisherAdView getAdView(Context context, int adSize, int adUnit) {
        final PublisherAdView adView = new PublisherAdView(context);

        switch (adSize) {
            case AD_SIZE_320_50:
                adView.setAdSizes(AdSize.BANNER);
                break;

            case AD_SIZE_300_250:
                adView.setAdSizes(AdSize.MEDIUM_RECTANGLE, new AdSize(336, 280), new AdSize(250, 250), new AdSize(200, 200));
                break;
        }

        switch (adUnit) {
            case AD_TYPE_ARTICLE_ATF:
                adView.setAdUnitId(AdUnitConst.AD_UNIT_Article_ATF);
                break;

            case AD_TYPE_ARTICLE_BTF:
                adView.setAdUnitId(AdUnitConst.AD_UNIT_Article_BTF);
                break;

            case AD_TYPE_ARTICLE_BTF_50:
                adView.setAdUnitId(AdUnitConst.AD_UNIT_Article_BTF_50);
                break;

            case AD_TYPE_PHOTO_GALLERY_BTF:
                adView.setAdUnitId(AdUnitConst.AD_UNIT_Photo_Gallery_BTF);
                break;

            case AD_TYPE_VIDEO_GALLERY:
                adView.setAdUnitId(AdUnitConst.AD_UNIT_VIDEO_GALLERY);
                break;

            case AD_TYPE_HOME_LISTING_ATF:
                adView.setAdUnitId(AdUnitConst.AD_UNIT_Home_Listing_ATF);
                break;

            case AD_TYPE_HOME_LISTING_BTF:
                adView.setAdUnitId(AdUnitConst.AD_UNIT_Home_Listing_BTF);
                break;

            case AD_TYPE_ALL_LISTING_ATF:
                adView.setAdUnitId(AdUnitConst.AD_UNIT_All_Listing_ATF);
                break;

            case AD_TYPE_EXIT_POPUP:
                adView.setAdUnitId(AdUnitConst.AD_UNIT_Exit_Popup);
                break;
        }

        return adView;
    }


    public static PublisherAdView exitAdView;

    public static void loadExitPopupAd(Context context) {
        try {
            exitAdView = getAdView(context, AD_SIZE_300_250, AD_TYPE_EXIT_POPUP);

            PublisherAdRequest.Builder builder = new PublisherAdRequest.Builder();
            builder.addCustomTargeting("gdpr_blocked", AdsConstants.gdpr_blocked_boolean ? "true" : "false");

            exitAdView.loadAd(builder.build());

        } catch (Exception e) {
        }
    }
}
