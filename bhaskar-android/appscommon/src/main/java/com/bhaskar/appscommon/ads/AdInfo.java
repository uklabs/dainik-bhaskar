package com.bhaskar.appscommon.ads;

import com.google.android.gms.ads.doubleclick.PublisherAdView;

public class AdInfo {

    public String adUnitId;
    public int position;
    public int type;

    public PublisherAdView bannerAdView;
    public int catType;
}
