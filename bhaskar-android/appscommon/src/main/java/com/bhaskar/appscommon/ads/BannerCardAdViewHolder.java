package com.bhaskar.appscommon.ads;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.doubleclick.PublisherAdView;

public class BannerCardAdViewHolder extends RecyclerView.ViewHolder {

    RelativeLayout adLayout;
    CardView adCardView;

    public BannerCardAdViewHolder(View view) {
        super(view);

        adLayout = view.findViewById(com.bhaskar.appscommon.R.id.adRelativeLayout);
        adCardView = view.findViewById(com.bhaskar.appscommon.R.id.adCardView);
    }

    public void addAds(PublisherAdView adView) {
        try {
            if (adLayout != null) {
                adLayout.removeAllViews();

                if (adView != null) {
                    ViewParent parent = adView.getParent();
                    ViewGroup pr = (ViewGroup) parent;
                    if (pr != null)
                        pr.removeView(adView);
                    adView.setFocusable(false);

                    adLayout.addView(adView);
                    adLayout.setVisibility(View.VISIBLE);
                    adCardView.setVisibility(View.VISIBLE);

                } else {
                    if (adCardView != null)
                        adCardView.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            if (adCardView != null) {
                adCardView.setVisibility(View.GONE);
            }
        }
    }
}
