package com.bhaskar.appscommon.ads;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class NativeAdViewHolder extends RecyclerView.ViewHolder {

    public RelativeLayout adLayout;
    public CardView adCardLayout;

    public NativeAdViewHolder(View view) {
        super(view);

        adLayout = view.findViewById(com.bhaskar.appscommon.R.id.adRelativeLayout);
        adCardLayout = view.findViewById(com.bhaskar.appscommon.R.id.adCardView);
    }

    public void addAds(View adView) {
        try {
            if (adLayout != null) {

                if (adView != null) {
                    ViewParent parent = adView.getParent();
                    ViewGroup pr = (ViewGroup) parent;
                    if (pr != null)
                        pr.removeView(adView);
                    adView.setFocusable(false);
                }

                adLayout.removeAllViews();
                adLayout.addView(adView);
                adLayout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
        }
    }
}
