package com.bhaskar.appscommon.ads;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

public class NativeAdViewHolder2 extends RecyclerView.ViewHolder {

    private UnifiedNativeAdView adView;
    private CardView adCardView;

    TextView headingTV;
    TextView bodyTV;
    TextView installTV;
    ImageView iconIV;

    public NativeAdViewHolder2(View view) {
        super(view);

        adCardView = view.findViewById(com.bhaskar.appscommon.R.id.adCardView);
        adView = view.findViewById(com.bhaskar.appscommon.R.id.unified_adview);
        headingTV = adView.findViewById(com.bhaskar.appscommon.R.id.ad_headline);
        adView.setHeadlineView(headingTV);
        bodyTV = adView.findViewById(com.bhaskar.appscommon.R.id.ad_body);
        adView.setBodyView(bodyTV);
        installTV = adView.findViewById(com.bhaskar.appscommon.R.id.install_tv);
        adView.setCallToActionView(installTV);
        iconIV = adView.findViewById(com.bhaskar.appscommon.R.id.ad_app_icon);
        adView.setIconView(iconIV);
    }

    public void populateNativeAdView(UnifiedNativeAd nativeAd) {
        try {
            if (nativeAd == null) {
                adCardView.setVisibility(View.GONE);
                return;
            }

            headingTV.setText(nativeAd.getHeadline());
            bodyTV.setText(nativeAd.getBody());
            installTV.setText(nativeAd.getCallToAction());

            NativeAd.Image icon = nativeAd.getIcon();
            if (icon == null) {
                iconIV.setVisibility(View.INVISIBLE);
            } else {
                iconIV.setImageDrawable(icon.getDrawable());
                iconIV.setVisibility(View.VISIBLE);
            }

            // Assign native ad object to the native view.
            adView.setNativeAd(nativeAd);
            adCardView.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            if (adCardView != null) {
                adCardView.setVisibility(View.GONE);
            }
        }
    }
}
