package com.bhaskar.appscommon.ads.consents;

import android.content.Context;

import com.google.ads.consent.AdProvider;
import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentFormListener;
import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.consent.DebugGeography;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class Consents {
    private static Consents consents;
    private String dfpPublisherId;
    private String dfpPublisherIdArray[];
    private boolean requestLocationInEeaOrUnknown = false;
    private boolean isCalled = false;
    private boolean isConsentInfoUpdated = false;

    public boolean isConsentInfoUpdated() {
        return isConsentInfoUpdated;
    }

    public boolean isCalled() {
        return isCalled;
    }

    public boolean isRequestLocationInEeaOrUnknown() {
        return requestLocationInEeaOrUnknown;
    }

    public Consents(String dfpPublisherId) {
        this.dfpPublisherId = dfpPublisherId;
        dfpPublisherIdArray = new String[]{dfpPublisherId};
    }

    public static Consents getInstance(String dfpPublisherId) {
        if (consents == null)
            consents = new Consents(dfpPublisherId);
        return consents;
    }

    public void setConsentStatus(Context context, ConsentStatus consentStatus) {
        ConsentInformation.getInstance(context).setConsentStatus(consentStatus);
    }

    public List getAdProviders(Context context) {
        List<AdProvider> adProviders = ConsentInformation.getInstance(context).getAdProviders();
        return adProviders;
    }

    public void checkForConsentUpdate(final Context context) {
        ConsentInformation consentInformation = ConsentInformation.getInstance(context);
        consentInformation.requestConsentInfoUpdate(dfpPublisherIdArray, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                requestLocationInEeaOrUnknown = ConsentInformation.getInstance(context).isRequestLocationInEeaOrUnknown();
                isCalled = true;
                isConsentInfoUpdated = true;
            }

            @Override
            public void onFailedToUpdateConsentInfo(String errorDescription) {
                isCalled = true;
                isConsentInfoUpdated = false;
            }
        });
    }

    public void setDebugEeaTrue(Context context) {
        ConsentInformation.getInstance(context).addTestDevice("86E810CB74BED24C19C74FCD4365DD4A");
        ConsentInformation.getInstance(context).addTestDevice("8F58C470B8FE7842C6B838BA677851F7");
        ConsentInformation.getInstance(context).addTestDevice("6802CC27AEB9B45692915B32A2C9E278");
        ConsentInformation.getInstance(context).addTestDevice("6605F77AED430F0E4A05CC903F306B8C");
        ConsentInformation.getInstance(context).addTestDevice("BCFFF981ABFAFE91E9A407C146989F1C");
        ConsentInformation.getInstance(context).addTestDevice("FA8E78F9D1194CB20BDC08E717240D9D");
        ConsentInformation.getInstance(context).addTestDevice("87267D81E6A4F74A51A9AF8D63198683");
        ConsentInformation.getInstance(context).addTestDevice("FADC4669ACBED15FA30ECC40EDA3BD76");
        ConsentInformation.getInstance(context).addTestDevice("33BE2250B43518CCDA7DE426D04EE231");
        ConsentInformation.getInstance(context).setDebugGeography(DebugGeography.DEBUG_GEOGRAPHY_EEA);
    }

    private ConsentForm form;

    private void requestConsentForm(final Context context) {
        URL privacyUrl = null;
        try {
            privacyUrl = new URL("https://m.divyabhaskar.co.in/privacy-policy/");
        } catch (MalformedURLException e) {
        }
        form = new ConsentForm.Builder(context, privacyUrl)
                .withListener(new ConsentFormListener() {
                    @Override
                    public void onConsentFormLoaded() {
                        if (form != null)
                            form.show();
                    }

                    @Override
                    public void onConsentFormOpened() {
                    }

                    @Override
                    public void onConsentFormClosed(ConsentStatus consentStatus, Boolean userPrefersAdFree) {
                    }

                    @Override
                    public void onConsentFormError(String errorDescription) {
                    }
                })
                .withNonPersonalizedAdsOption()
                .withPersonalizedAdsOption()
                .build();

        form.load();
    }
}
