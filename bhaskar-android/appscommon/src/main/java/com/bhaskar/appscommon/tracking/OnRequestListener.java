package com.bhaskar.appscommon.tracking;

/**
 * Created by DB on 24-03-2017.
 */

public interface OnRequestListener {
    void onSuccess();

    void onFailed(String message);
}
