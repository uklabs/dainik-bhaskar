package com.bhaskar.appscommon.tracking;

import android.content.Context;
import android.text.TextUtils;

import com.bhaskar.appscommon.tracking.util.TrackingPreferences;
import com.bhaskar.appscommon.tracking.util.TrackingQuickPreferences;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;

/**
 * Created by DB on 24-03-2017.
 */

public class TrackingData {

    public static String getDeviceId(Context context) {
        return TrackingUtils.getDeviceId(context);
    }

    public static String getDBId(Context context) {
//        String dbId = TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.DB_ID, getDeviceId(context));
//        if (TextUtils.isEmpty(dbId)) {
//            return getDeviceId(context);
//        }
//        return dbId;
        String dbId = TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.DB_ID, "");
        return dbId;
    }

    public static String getSessionId(Context context) {
        return TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.SESSION_ID, "");
    }

    public static String getCity(Context context) {
        return TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.CITY, "");
    }

    public static String getState(Context context) {
        return TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.STATE, "");
    }

    public static String getCountry(Context context) {
        return TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.COUNTRY, "");
    }

    public static String getDbORDeviceId(Context context) {
        String dbId = getDBId(context);
        if (TextUtils.isEmpty(dbId)) {
            dbId = getDeviceId(context);
        }

        return dbId;
    }

    public static String getFirstInstallDate(Context context) {
        return TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.FIRST_INSTALL_DATE, "");
    }

    public static String getAppInstallDate(Context context) {
        return TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.APP_INSTALL_DATE, "");
    }

    public static void setAppInstallDate(Context context, String date) {
        TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.APP_INSTALL_DATE, date);
    }

    public static String getGoogleAdvertisingId(Context context) {
        return TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.AD_ID, "");
    }

    public static void setGoogleAdvertisingId(final Context context) {
        String adId = TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.AD_ID, "");

        if (TextUtils.isEmpty(adId)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String googleAdvId = AdvertisingIdClient.getAdvertisingIdInfo(context).getId();
                        TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.AD_ID, googleAdvId);
                    } catch (Exception e) {
                    }
                }
            }).start();
        }
    }
}
