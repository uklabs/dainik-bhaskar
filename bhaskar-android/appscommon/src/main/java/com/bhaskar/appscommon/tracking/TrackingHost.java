package com.bhaskar.appscommon.tracking;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class TrackingHost {

    private static final String NAME = "TrackingHost";

    private static final String KEY_SESSION_URL = "sessionUrl";
    private static final String KEY_LOCATION_URL = "locationUrl";
    private static final String KEY_TOGGLE_NOTIFY_URL = "toggleNotifyUrl";
    private static final String KEY_RASHIFAL_REGISTRATION_URL = "rashifalRegisUrl";
    private static final String KEY_WISDOM_URL = "wisdomUrl";
    private static final String KEY_WISDOM_NOTIFICATION_URL = "wisdomNotificationUrl";
    private static final String KEY_BLUE_PI_WISDOM_NOTIFICATION_URL = "bluepiWisdomNotificationUrl";
    private static final String KEY_WISDOM_APP_SOURCE_URL = "wisdomAppSource";

    private static final String KEY_SCREEN_VIEW_COUNT = "screenViewCount";

    private static final String IS_APP_SOURCE_HIT = "isAppSourceHit";
    private static final String KEY_FCM_TOKEN = "fcm_token_id";
    private static final String KEY_EVENT_TRACKING_ENABLE = "eventTrackingEnable";
    private static final String KEY_IS_BLOCKED_COUNTRY = "t_isBlockedCountry";

    private static TrackingHost instance = null;
    private static SharedPreferences preferences = null;

    private OnScreenCountListener mScreenCountListener;

    private TrackingHost(Context ctx) {
        createPref(ctx);
    }

    private static void createPref(Context ctx) {
        if (ctx != null) {
            preferences = ctx.getSharedPreferences(NAME, Context.MODE_PRIVATE);
            preferences.edit().putInt(KEY_SCREEN_VIEW_COUNT, 0);
        }
    }

    public static TrackingHost getInstance(Context ctx) {
        if (instance == null) {
            instance = new TrackingHost(ctx);
        }
        if (preferences == null) {
            createPref(ctx);
        }

        return instance;
    }

    /*******
     * URLs
     * ***/

    public void registerCountReceiver(OnScreenCountListener listener) {
        mScreenCountListener = listener;
    }

    public void increaseScreenCount() {
        if (preferences != null) {
            int count = preferences.getInt(KEY_SCREEN_VIEW_COUNT, 0);
            count += 1;

            if (mScreenCountListener != null) {
                mScreenCountListener.onScreenCount(count);
            }
            preferences.edit().putInt(KEY_SCREEN_VIEW_COUNT, count).commit();
        }
    }

    public void setSessionUrl(String url) {
        if (preferences != null) {
            preferences.edit().putString(KEY_SESSION_URL, url).commit();
        }
    }

    public void setLocationUrl(String url) {
        if (preferences != null) {
            preferences.edit().putString(KEY_LOCATION_URL, url).commit();
        }
    }

    public void setToggleNotifyUrl(String url) {
        if (preferences != null) {
            preferences.edit().putString(KEY_TOGGLE_NOTIFY_URL, url).commit();
        }
    }

    public void setRashifalRegistrationUrl(String url) {
        if (preferences != null) {
            preferences.edit().putString(KEY_RASHIFAL_REGISTRATION_URL, url).commit();
        }
    }

    public void setWisdomUrl(String url) {
        if (preferences != null) {
            preferences.edit().putString(KEY_WISDOM_URL, url).commit();
        }
    }

    public void setWisdomNotificationUrl(String url) {
        if (preferences != null) {
            preferences.edit().putString(KEY_WISDOM_NOTIFICATION_URL, url).commit();
        }
    }

    public void setBluePiWisdomNotificationUrl(String url) {
        if (preferences != null) {
            preferences.edit().putString(KEY_BLUE_PI_WISDOM_NOTIFICATION_URL, url).commit();
        }
    }

    public void setWisdomAppSourceUrl(String url) {
        if (preferences != null) {
            preferences.edit().putString(KEY_WISDOM_APP_SOURCE_URL, url).commit();
        }
    }

    public void setFCMToken(String token) {
        if (preferences != null) {
            preferences.edit().putString(KEY_FCM_TOKEN, token).commit();
        }
    }

    public String getSesstionUrl(String defaultUrl) {
        if (preferences != null) {
            String url = preferences.getString(KEY_SESSION_URL, defaultUrl);
            if (TextUtils.isEmpty(url)) {
                return defaultUrl;
            }

            return url;
        }

        return defaultUrl;
    }

    public String getLocationUrl(String defaultUrl) {
        if (preferences != null) {
            String url = preferences.getString(KEY_LOCATION_URL, defaultUrl);
            if (TextUtils.isEmpty(url)) {
                return defaultUrl;
            }

            return url;
        }

        return defaultUrl;
    }

    public String getToggleNotifyUrl(String defaultUrl) {
        if (preferences != null) {
            String url = preferences.getString(KEY_TOGGLE_NOTIFY_URL, defaultUrl);
            if (TextUtils.isEmpty(url)) {
                return defaultUrl;
            }

            return url;
        }

        return defaultUrl;
    }

    public String getRashifalRegistrationUrl(String defaultUrl) {
        if (preferences != null) {
            String url = preferences.getString(KEY_RASHIFAL_REGISTRATION_URL, defaultUrl);
            if (TextUtils.isEmpty(url)) {
                return defaultUrl;
            }

            return url;
        }

        return defaultUrl;
    }

    public String getWisdomUrl(String defaultUrl) {
        if (preferences != null) {
            String url = preferences.getString(KEY_WISDOM_URL, defaultUrl);
            if (TextUtils.isEmpty(url)) {
                return defaultUrl;
            }

            return url;
        }

        return defaultUrl;
    }

    public String getWisdomUrlForNotification(String defaultUrl) {
        if (preferences != null) {
            String url = preferences.getString(KEY_WISDOM_NOTIFICATION_URL, defaultUrl);
            if (TextUtils.isEmpty(url)) {
                return defaultUrl;
            }

            return url;
        }

        return defaultUrl;
    }

    public String getBluePiWisdomUrlForNotification(String defaultUrl) {
        if (preferences != null) {
            String url = preferences.getString(KEY_BLUE_PI_WISDOM_NOTIFICATION_URL, defaultUrl);
            if (TextUtils.isEmpty(url)) {
                return defaultUrl;
            }

            return url;
        }

        return defaultUrl;
    }


    public String getWisdomAppSourceUrl(String defaultUrl) {
        if (preferences != null) {
            String url = preferences.getString(KEY_WISDOM_APP_SOURCE_URL, defaultUrl);
            if (TextUtils.isEmpty(url)) {
                return defaultUrl;
            }

            return url;
        }

        return defaultUrl;
    }

    public String getFCMToken() {
        if (preferences != null) {
            return preferences.getString(KEY_FCM_TOKEN, "");
        }

        return "";
    }

    public boolean getAppSourceHit() {
        if (preferences != null) {
            return preferences.getBoolean(IS_APP_SOURCE_HIT, false);
        }

        return false;
    }

    public void setAppSourceHit(boolean value) {
        if (preferences != null) {
            preferences.edit().putBoolean(IS_APP_SOURCE_HIT, value).commit();
        }
    }

    /*******
     * IDs
     * ***/

    private static final String KEY_COMSCORE_SECRET_ID = "comscoreSecretId";
    private static final String KEY_COMSCORE_PUBLISHER_ID = "comscorePublisherId";

    private static final String KEY_APPSFLYER_DEV_ID = "appsflyerDevId";
    private static final String KEY_GCM_PROJECT_NUMBER = "gcmProjectNumber";

    private static final String KEY_GOOGLE_ANALYTICS_ID = "googleAnalyticsId";
    private static final String KEY_LOTAME_ID = "lotameId";

    public void setComscoreIds(String comscoreSecretid, String comscorePublisherId) {
        if (preferences != null) {
            preferences.edit().putString(KEY_COMSCORE_SECRET_ID, comscoreSecretid).commit();
            preferences.edit().putString(KEY_COMSCORE_PUBLISHER_ID, comscorePublisherId).commit();
        }
    }

    public void setAppsflyerDevId(String devId) {
        if (preferences != null) {
            preferences.edit().putString(KEY_APPSFLYER_DEV_ID, devId).commit();
        }
    }

    public void setGcmProjectNumber(String projectNumber) {
        if (preferences != null) {
            preferences.edit().putString(KEY_GCM_PROJECT_NUMBER, projectNumber).commit();
        }
    }

    public void setGoogleAnalyticsId(String gaId) {
        if (preferences != null) {
            preferences.edit().putString(KEY_GOOGLE_ANALYTICS_ID, gaId).commit();
        }
    }

    public void setLotameId(int lotameId) {
        if (preferences != null) {
            preferences.edit().putInt(KEY_LOTAME_ID, lotameId).commit();
        }
    }

    public String getComscoreSecretId(String defaultSecretId) {
        if (preferences != null) {
            String id = preferences.getString(KEY_COMSCORE_SECRET_ID, defaultSecretId);
            if (TextUtils.isEmpty(id)) {
                return defaultSecretId;
            }

            return id;
        }

        return defaultSecretId;
    }

    public String getComscorePublisherId(String defaultPublisherId) {
        if (preferences != null) {
            String id = preferences.getString(KEY_COMSCORE_PUBLISHER_ID, defaultPublisherId);
            if (TextUtils.isEmpty(id)) {
                return defaultPublisherId;
            }

            return id;
        }

        return defaultPublisherId;
    }

    public String getAppsflyerDevId(String defaultDevId) {
        if (preferences != null) {
            String id = preferences.getString(KEY_APPSFLYER_DEV_ID, defaultDevId);
            if (TextUtils.isEmpty(id)) {
                return defaultDevId;
            }

            return id;
        }

        return defaultDevId;
    }

    public String getGcmProjectNumber(String defaultGCMProjectNum) {
        if (preferences != null) {
            String id = preferences.getString(KEY_GCM_PROJECT_NUMBER, defaultGCMProjectNum);
            if (TextUtils.isEmpty(id)) {
                return defaultGCMProjectNum;
            }

            return id;
        }

        return defaultGCMProjectNum;
    }

    public String getGoogleAnalyticsId(String defaultGAId) {
        if (preferences != null) {
            String id = preferences.getString(KEY_GOOGLE_ANALYTICS_ID, defaultGAId);
            if (TextUtils.isEmpty(id)) {
                return defaultGAId;
            }

            return id;
        }

        return defaultGAId;
    }

    public int getLotameId(int defaultLotameId) {
        if (preferences != null) {
            int id = preferences.getInt(KEY_LOTAME_ID, defaultLotameId);
            if (id < 1) {
                return defaultLotameId;
            }

            return id;
        }

        return defaultLotameId;
    }

    public void setEventTrackingEnabled(boolean enable) {
        if (preferences != null) {
            preferences.edit().putBoolean(KEY_EVENT_TRACKING_ENABLE, enable).commit();
        }
    }

    public boolean getEventTrackingEnabled() {
        if (preferences != null) {
            return preferences.getBoolean(KEY_EVENT_TRACKING_ENABLE, false);
        }

        return true;
    }

    public void setIsBlockedCountry(boolean enable) {
        if (preferences != null) {
            preferences.edit().putBoolean(KEY_IS_BLOCKED_COUNTRY, enable).commit();
        }
    }

    public boolean getIsBlockedCountry() {
        if (preferences != null) {
            return preferences.getBoolean(KEY_IS_BLOCKED_COUNTRY, false);
        }

        return true;
    }
}
