package com.bhaskar.appscommon.tracking.atracker;

import android.content.Context;
import android.os.Build;
import androidx.annotation.IntDef;
import androidx.annotation.StringDef;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.appscommon.tracking.OnApiRequestListener;
import com.bhaskar.appscommon.tracking.OnRequestListener;
import com.bhaskar.appscommon.R;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.util.Systr;
import com.bhaskar.appscommon.tracking.util.TrackingPreferences;
import com.bhaskar.appscommon.tracking.util.TrackingQuickPreferences;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by DB on 22-03-2017.
 */

public class ATracker {

    private static final String OPERATING_SYSTEM = "android";

    @IntDef({ToggleStatus.ON, ToggleStatus.OFF})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ToggleStatus {
        int ON = 1;
        int OFF = 0;
    }

    @StringDef({ToggleType.RASHIFAL, ToggleType.OTHER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ToggleType {
        String RASHIFAL = "rashifal";
        String OTHER = "other";
    }

    public static void sessionTracking(final Context context, String sessionUrl, String token, String appId, String email, final OnApiRequestListener listener) {

        JSONObject jsonObject = new JSONObject();
        try {
//            jsonObject.put("cn", TrackingUtils.getNetwork(context));
//            jsonObject.put("op:", TrackingUtils.getOperatorName(context));
//            jsonObject.put("ss", TrackingUtils.getScreenSize(context));
            jsonObject.put("did", TrackingData.getDeviceId(context));
//            jsonObject.put("br", Build.BRAND);
//            jsonObject.put("md", Build.MODEL);
            jsonObject.put("ov", String.valueOf(Build.VERSION.SDK_INT));
            jsonObject.put("os", OPERATING_SYSTEM);
            jsonObject.put("app_ver", TrackingUtils.getAppVersion(context));
            jsonObject.put("app_vc", String.valueOf(TrackingUtils.getAppVersionCode(context)));

            jsonObject.put("tk", token);
            jsonObject.put("aid", appId);
//            jsonObject.put("em", email);
        } catch (JSONException e) {
        }

        Systr.println("Session Send Json : " + jsonObject.toString());

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, sessionUrl, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Systr.println("Session Response : " + response);
                parseJsonSession(context, response);

                if (listener != null) {
                    listener.onComplete();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Session Error : " + error);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                return headers;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(14000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }

    private static void parseJsonSession(Context context, JSONObject jsonObject) {
        try {
            String status = jsonObject.optString("status");
            if (status.equals("Success")) {
                String dbId = jsonObject.optString("dev_id");
                String sessionId = jsonObject.optString("session_id");

                TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.DB_ID, dbId);
                TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.SESSION_ID, sessionId);
            }
        } catch (Exception e) {
        }
    }

    public static void rashifalTracking(final Context context, String rashifalRegisUrl, String name, String dob, String sunshine, String token, String appId, String email, final OnRequestListener listener) {

        String dbId = TrackingData.getDBId(context);
        if (TextUtils.isEmpty(dbId)) {
            if (listener != null) {
                listener.onFailed(context.getResources().getString(R.string.sorry_error_found_please_try_again));
            }
            return;
        }

        final JSONObject jsonObject = new JSONObject();
        try {
//            jsonObject.put("cn", TrackingUtils.getNetwork(context));
//            jsonObject.put("op:", TrackingUtils.getOperatorName(context));
//            jsonObject.put("ss", TrackingUtils.getScreenSize(context));
            jsonObject.put("did", TrackingData.getDBId(context));
            jsonObject.put("device_os_id", TrackingData.getDeviceId(context));
//            jsonObject.put("br", Build.BRAND);
//            jsonObject.put("md", Build.MODEL);
            jsonObject.put("ov", String.valueOf(Build.VERSION.SDK_INT));
            jsonObject.put("os", OPERATING_SYSTEM);
//            jsonObject.put("count", TrackingData.getCountry(context));
//            jsonObject.put("stat", TrackingData.getState(context));
//            jsonObject.put("city", TrackingData.getCity(context));
            jsonObject.put("app_ver", TrackingUtils.getAppVersion(context));
            jsonObject.put("app_vc", String.valueOf(TrackingUtils.getAppVersionCode(context)));

            jsonObject.put("tk", token);
            jsonObject.put("aid", appId);
            jsonObject.put("nam", name);
            jsonObject.put("dob", dob);
            jsonObject.put("sun", sunshine);
//            jsonObject.put("em", email);

        } catch (JSONException e) {
        }

        Systr.println("Rashifal Send Json : " + jsonObject.toString());

//        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, rashifalRegisUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                Systr.println("Rashifal Response : " + response);
//
//                String string = response.optString("status");
//                if (!TextUtils.isEmpty(string) && string.equals("Success")) {
//                    if (listener != null) {
//                        Systr.println("Rashifal onSuccess called");
//                        listener.onSuccess();
//                    }
//                } else {
//                    String message = response.optString("msg");
//                    if (listener != null) {
//                        Systr.println("Rashifal onfailed called");
//                        listener.onFailed(message);
//                    }
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Systr.println("Rashifal Error : " + error);
//
//                if (listener != null) {
//                    Systr.println("Rashifal error OnFailed called");
//                    listener.onFailed(context.getResources().getString(R.string.rashifal_registration_failed));
//                }
//            }
//        }) {
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("api-key", "bh@@$k@r-D");
//                return headers;
//            }
//        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, rashifalRegisUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                Systr.println("Rashifal Response : " + res);

                try {
                    JSONObject response = new JSONObject(res);
                    String string = response.optString("status");
                    if (!TextUtils.isEmpty(string) && string.equals("Success")) {
                        if (listener != null) {
                            Systr.println("Rashifal onSuccess called");
                            listener.onSuccess();
                        }
                    } else {
                        String message = response.optString("msg");
                        if (listener != null) {
                            Systr.println("Rashifal onfailed called");
                            listener.onFailed(message);
                        }
                    }
                } catch (Exception e) {
                    if (listener != null) {
                        Systr.println("Rashifal onfailed called");
                        listener.onFailed(context.getResources().getString(R.string.sorry_error_found_please_try_again));
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Rashifal Error : " + error);

                if (listener != null) {
                    Systr.println("Rashifal error OnFailed called");
                    listener.onFailed(context.getResources().getString(R.string.rashifal_registration_failed));
                }
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", "1");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                String json = jsonObject.toString();
                String data = TrackingUtils.encryptString(json);
                Map<String, String> params = new HashMap<String, String>();
                Systr.println("Rashifal json : " + data);
                params.put("edata", data);
                return params;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(14000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }

    public static void toggleTracking(final Context context, String toggleUrl, @ToggleStatus int toggleStatus, @ToggleType String toggleType, String token, String appId, String email, String source, String medium, String campaign, final OnRequestListener listener, String allType, String breakType, String mrngType, String evenType) {

        String dbId = TrackingData.getDBId(context);
        if (TextUtils.isEmpty(dbId)) {
            if (listener != null) {
                listener.onFailed(context.getResources().getString(R.string.sorry_error_found_please_try_again));
            }
            return;
        }

        final JSONObject jsonObject = new JSONObject();
        try {
//            jsonObject.put("cn", TrackingUtils.getNetwork(context));
//            jsonObject.put("op:", TrackingUtils.getOperatorName(context));
//            jsonObject.put("ss", TrackingUtils.getScreenSize(context));
            jsonObject.put("did", dbId);
            jsonObject.put("device_os_id", TrackingData.getDeviceId(context));
//            jsonObject.put("br", Build.BRAND);
//            jsonObject.put("md", Build.MODEL);
            jsonObject.put("ov", String.valueOf(Build.VERSION.SDK_INT));
            jsonObject.put("os", OPERATING_SYSTEM);
//            jsonObject.put("count", TrackingData.getCountry(context));
//            jsonObject.put("stat", TrackingData.getState(context));
//            jsonObject.put("city", TrackingData.getCity(context));
            jsonObject.put("app_ver", TrackingUtils.getAppVersion(context));
            jsonObject.put("app_vc", String.valueOf(TrackingUtils.getAppVersionCode(context)));

            jsonObject.put("tk", token);
            jsonObject.put("aid", appId);
//            jsonObject.put("em", email);
            jsonObject.put("sta", String.valueOf(toggleStatus));
            jsonObject.put("typ", toggleType);

            /*New Notification Type*/
            jsonObject.put("ntyp_all", allType);    // For All Update
            jsonObject.put("ntyp_break", breakType);    // For Critical/Breaking Update
            jsonObject.put("ntyp_mrng", mrngType);    // For Morning Brief Update
            jsonObject.put("ntyp_eve", evenType);    // For Evening Roundup Update

            jsonObject.put("source", source);
            jsonObject.put("medium", medium);
            jsonObject.put("campaign", campaign);

        } catch (JSONException e) {
        }

        Systr.println("ToggleStatus send json : " + jsonObject.toString());

//        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, toggleUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                Systr.println("ToggleStatus Response : " + response);
//
//                String string = response.optString("status");
//                if (!TextUtils.isEmpty(string) && string.equals("Success")) {
//                    if (listener != null) {
//                        Systr.println("ToggleStatus onSuccess called");
//                        listener.onSuccess();
//                    }
//                } else {
//                    String message = response.optString("msg");
//                    if (listener != null) {
//                        Systr.println("ToggleStatus onFailed called");
//                        listener.onFailed(message);
//                    }
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Systr.println("ToggleStatus Error : " + error);
//
//                if (listener != null) {
//                    Systr.println("ToggleStatus error onFailed called");
//                    listener.onFailed(context.getResources().getString(R.string.sorry_error_found_please_try_again));
//                }
//            }
//        }) {
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("api-key", "bh@@$k@r-D");
//                return headers;
//            }
//        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, toggleUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                Systr.println("ToggleStatus Response : " + res);
                try {
                    JSONObject response = new JSONObject(res);

                    String string = response.optString("status");
                    if (!TextUtils.isEmpty(string) && string.equals("Success")) {
                        if (listener != null) {
                            Systr.println("ToggleStatus onSuccess called");
                            listener.onSuccess();
                        }
                    } else {
                        String message = response.optString("msg");
                        if (listener != null) {
                            Systr.println("ToggleStatus onFailed called");
                            listener.onFailed(message);
                        }
                    }
                } catch (Exception e) {
                    if (listener != null) {
                        Systr.println("ToggleStatus onFailed called");
                        listener.onFailed(context.getResources().getString(R.string.sorry_error_found_please_try_again));
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("ToggleStatus Error : " + error);

                if (listener != null) {
                    Systr.println("ToggleStatus error onFailed called");
                    listener.onFailed(context.getResources().getString(R.string.sorry_error_found_please_try_again));
                }
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", "1");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                String json = jsonObject.toString();
                String data = TrackingUtils.encryptString(json);
                Map<String, String> params = new HashMap<String, String>();
                Systr.println("Toggle json : " + data);
                params.put("edata", data);
                return params;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(14000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }
}
