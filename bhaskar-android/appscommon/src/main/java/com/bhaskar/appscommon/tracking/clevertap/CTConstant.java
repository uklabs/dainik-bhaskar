package com.bhaskar.appscommon.tracking.clevertap;

public interface CTConstant {

    String NOT_AVAILABLE = "NA";
    String DBID = "DBID";
    String DEVICE_ID = "DeviceID";
    String ADID = "ADID";
    String PLATFORM = "Platform";
    String APP_VERSION = "AppVersion";
    String STATUS = "LoginStatus";
    String LOGGED_IN = "LoggedIn";
    String GUEST = "Guest";
    String EMAIL = "Email";
    String PHONE = "Phone";
    String GENDER = "UserGender";
    String RASHI = "Rashi";
    String NUMERO_NUM = "NumeroNo";


    interface EVENT_NAME {

        String APPINTRO = "AppIntro";
        //        String HOME_SCREEN_ACTION = "HomeScreenActions";
        String SPECIAL_CONTENT = "SpecialContent";
//        String NOTIFICATION = "NotificationStatus";

        String API = "API";
        //        String EVENT_AUDIO = "AudioEvents";
        String EVENT_TAMBOLA = "BingoEvents";

        String VIDEO_WATCHED = "VideoWatched";
        String ACCOUNT_ACTIONS = "AccountActions";
        String CONTENT_SHARED = "ContentShared";
        String BOOKMARKED = "Bookmarked";
        String NOTIFICATION_STATUS = "NotificationStatus";
        String DND = "DND";
        String AUTHORS = "Authors";
        String COMMENTS = "Comments";
        String PRIME_SCREEN_CLICKS = "PrimeScreenClicks";
        String BOTTOM_BAR_CLICKS = "BottomBarClicks";
        String MERA_PAGE = "MeraPage";
        String HOME_TOP_RIGHT_ICONS_CLICK = "HomeTopRightIconsClick";
        String SIDE_MENU_CLICKS = "SideMenuClicks";
        String NEXT_STORY = "NextStory";
        String RADIO = "Radio";
        String CONTAINER_CLICKS = "ContainerClicks";
        String ARTICLE = "Article";
        String VIDEO_GALLERY_CLICKS = "VideoGalleryClicks";
    }

    interface PROP_NAME {

        String MEGA_OFFER_POINTS = "MegaOfferPoints";
        String MEGA_OFFER_MONTH = "MegaOfferMonth";
        String MEGA_OFFER_VISITS = "MegaOfferVisits";
        String CLICKON = "ClickOn";
        String REGISTRATION = "Registration";
        String CLICKED_ITEM_NAME = "ClickedItemName";

        String VIDEO_TITLE = "VideoTitle";
        String ACTION_TYPE = "ActionType";
        String ACTION_MEDIUM = "ActionMedium";
        String NEWS_URL = "NewsURL";
        String SHARED_THROUGH = "SharedThrough";
        String CONTENT_URL = "ContentURL";
        String BOOKMARKED_THROUGH = "BookmarkedThrough";
        String STATUS = "Status";
        String AUTHOR_NAME = "Author Name";
        String CATEGORIES_NAME = "CategoriesName";
        String ICON_NAME = "IconName";
        String NEXT_STORY_URL = "NextStoryURL";
        String STATION_NAME = "StationName";
        String CONTAINER_NAME = "ContainerName";
        String VIEW = "View";

        interface Tambola {
            String QR_SCAN = "QR_Scan";
            String QR_CODE = "QR_Code";
        }
    }


    interface PROP_VALUE {
        String HOME = "Home";
        String SKIP = "Skip";
        String GOT_IT = "Got It";
        String SAVE = "Save";
        String FAIL = "Fail";
        String SUCCESS = "Success";
        String FAILED = "Failed";
        String ICON = "Icon";
        String MOBILE = "Mobile";
        String TAMBOLA = "Bingo";

        String VIDEO_STARTED = "VideoStarted";
        String VIDEO_END = "VideoEnd";

        String LOGOUT = "LogOut";
        String LOGIN = "LogIn";
        String SIGNUP = "Registration";

        String THREE_DOTS = "ThreeDots";
        String STORY_DETAIL = "StoryDetail";

        String ON = "On";
        String OFF = "Off";

        String FOLLOW = "Follow";
        String UNFOLLOW = "Unfollow";
        String PROFILE = "Profile";

        String CANCEL_BUTTON_CLICKS = "CancelButtonClicks";
        String SWIPE_UP_BY_USER = "SwipeUpByUser";
        String AUTO_SWIPE_UP = "AutoSwipeUp";

        String STARTED = "Started";
        String ENDED = "Ended";
        String SEARCH = "Search";
        String REFRESH = "Refresh";
        String COMPLETE = "Complete";

        interface Tambola {
            String SCREEN_REGISTRATION = "DB_Bingo_RegistrationScreen";
            String SCREEN_DASHBOARD = "DB_Bingo_HomeScreen";
            String SCREEN_SCANNER = "DB_Bingo_QRscanScreen";
            String SCREEN_TODAY_NUMBER = "DB_Bingo_TodaysNumberScreen";
            String SCREEN_WINNERS = "DB_Bingo_Winners";
            String SCREEN_ALL_NUMBERS = "DB_Bingo_AllNumbers";
            String SCREEN_USER_INFO = "DB_Bingo_MyInfo";
            String SCREEN_GAME_RULES = "DB_Bingo_GamesRules";
            String SCREEN_HELP_LINE = "DB_Bingo_GamesHelpLine";
            String INVITE_FRIENDS = "InviteFriends";
            String SCAN_QR = "ScanQR";
            String CLAIM_STATUS = "ClaimStatus";
            String WINNERS = "Winners";
            String ALL_NUMBERS = "AllNumbers";
            String MYINFO = "MyInfo";
            String GAME_RULES = "GamesRules";
            String GAME_HELP_LINE = "GamesHelpLine";
        }

        interface MegaOffer {
            String SCREEN_CHOOSE_YOUR_OFFER = "ChooseYourOffer";
            String SCREEN_MEGA_OFFER = "MegaOffer";
            String SCREEN_OFFER_DETAILS = "Details";
            String SCREEN_CONGRATS_POPUP = "CongrtPopUp";
        }
    }

    interface LABEL {
        String TAMBOLA = "Bingo";
        String NOTIFICATION = "Notification";
    }
}
