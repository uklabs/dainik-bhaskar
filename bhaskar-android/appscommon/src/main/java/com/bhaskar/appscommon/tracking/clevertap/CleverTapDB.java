package com.bhaskar.appscommon.tracking.clevertap;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.clevertap.android.sdk.CleverTapAPI;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.ADID;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.APP_VERSION;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.DBID;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.DEVICE_ID;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.EMAIL;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.GENDER;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.GUEST;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.LOGGED_IN;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.NOT_AVAILABLE;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.NUMERO_NUM;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.PHONE;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.PLATFORM;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.RASHI;
import static com.bhaskar.appscommon.tracking.clevertap.CTConstant.STATUS;

public class CleverTapDB {

    private final String TAG = CleverTapDB.class.getSimpleName();
    private CleverTapAPI clevertapDefaultInstance;
    private static CleverTapDB instance;

    private CleverTapDB(Context application) {
        initCleverTap(application);
    }

    private void initCleverTap(Context application) {
        clevertapDefaultInstance = CleverTapAPI.getDefaultInstance(application);
    }

    public static CleverTapDB getInstance(Context application) {
        if (instance == null) {
            instance = new CleverTapDB(application);
        }

        return instance;
    }

    public void registerLifeCycle(Application application) {
        ActivityLifecycleCallback.register(application);
    }

    private HashMap<String, Object> getDefaultCleverTapParams(Context context) {
        HashMap<String, Object> prodViewedAction = new HashMap<>();
        if (!TrackingHost.getInstance(context).getIsBlockedCountry()) {
            prodViewedAction.put(DEVICE_ID, TrackingData.getDeviceId(context));
            prodViewedAction.put(ADID, TrackingData.getGoogleAdvertisingId(context));
        }
        prodViewedAction.put(PLATFORM, "Android");
        prodViewedAction.put(APP_VERSION, TrackingUtils.getAppVersion(context));

        String userDBId = TrackingData.getDBId(context);
        if (TextUtils.isEmpty(userDBId)) {
            userDBId = NOT_AVAILABLE;
        }
        prodViewedAction.put(DBID, userDBId);

        return prodViewedAction;
    }

    public void loginProfileForLoginUser(Context context, String userEmailId, @NonNull String userPhoneNo, String userName, String photo, @NonNull String loginType, String fbAuth, String googleAuth) {
        Map<String, Object> profileUpdate = getCleverTapMapForLoginUser(context, userEmailId, userPhoneNo, userName, photo, loginType, fbAuth, googleAuth);
        clevertapDefaultInstance.onUserLogin(profileUpdate);
    }

    public void updateProfileForLoginUser(Context context, String userEmailId, @NonNull String userPhoneNo, String userName, String photo, @NonNull String loginType, String fbAuth, String googleAuth) {
        Map<String, Object> profileUpdate = getCleverTapMapForLoginUser(context, userEmailId, userPhoneNo, userName, photo, loginType, fbAuth, googleAuth);
        clevertapDefaultInstance.pushProfile(profileUpdate);
    }

    public void updateProfileForLogoutUser() {
        Map<String, Object> profileUpdate = new HashMap<>();
        profileUpdate.put(STATUS, GUEST);
        clevertapDefaultInstance.pushProfile(profileUpdate);
    }

    public void updateRashifalValuesOfUser(String gender, String rashiName, String numeroNumber) {
        Map<String, Object> profileUpdate = new HashMap<>();
        profileUpdate.put(GENDER, gender);
        profileUpdate.put(RASHI, rashiName);
        profileUpdate.put(NUMERO_NUM, numeroNumber);

        clevertapDefaultInstance.pushProfile(profileUpdate);

        Log.d(TAG, "Gender: " + gender + ", Rashi: " + rashiName + ", Numero: " + numeroNumber);
    }

    private Map<String, Object> getCleverTapMapForLoginUser(Context context, String userEmailId, String userPhoneNo, String userName, String photo, @NonNull String loginType, String fbAuth, String googleAuth) {

        if (TextUtils.isEmpty(userPhoneNo)) {
            userPhoneNo = NOT_AVAILABLE;
        }
        if (TextUtils.isEmpty(userName)) {
            userName = NOT_AVAILABLE;
        }
        if (TextUtils.isEmpty(photo)) {
            photo = NOT_AVAILABLE;
        }

        Map<String, Object> profileUpdate = new HashMap<>();
        try {
            profileUpdate.putAll(getDefaultCleverTapParams(context));
            profileUpdate.put(STATUS, LOGGED_IN);
            profileUpdate.put(PROFILE.PHONE, userPhoneNo);
            profileUpdate.put(PROFILE.IDENTITY, userPhoneNo);
            profileUpdate.put(PROFILE.NAME, userName);
            profileUpdate.put(PROFILE.PHOTO_URL, photo);
//            profileUpdate.put(PROFILE.LOGIN_TYPE, loginType);

            if (!TextUtils.isEmpty(userEmailId)) {
                profileUpdate.put(PROFILE.EMAIL, userEmailId);
            }

            switch (loginType) {
                case "F":
                    if (!TextUtils.isEmpty(fbAuth)) {
                        profileUpdate.put(PROFILE.FBID, fbAuth);
                    }
                    break;

                case "G":
                    if (!TextUtils.isEmpty(googleAuth)) {
                        profileUpdate.put(PROFILE.GPID, googleAuth);
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return profileUpdate;
    }

    public void cleverTapTrackPageView(@NonNull Context context, String screenName) {
        if (TextUtils.isEmpty(screenName)) {
            return;
        }
        screenName = screenName.replaceAll("\\s+", "");

        if (TextUtils.isEmpty(screenName))
            return;

        try {
            Map<String, Object> prodViewedAction = new HashMap<>();
            prodViewedAction.putAll(getDefaultCleverTapParams(context));
            prodViewedAction.put(PageView.SCREEN_NAME, screenName);

            CleverTapAPI.getDefaultInstance(context.getApplicationContext()).pushEvent(Actions.SCREEN_VIEW, prodViewedAction);

            Log.d(TAG, "eventName: " + Actions.SCREEN_VIEW + " >> screen name: " + screenName);
        } catch (Exception e) {
        }
    }


    public void cleverTapTrackEvent(@NonNull Context context, String eventName, String eventLabel, String eventValue, HashMap<String, Object> dataMap) {
        if (TextUtils.isEmpty(eventName) || TextUtils.isEmpty(eventLabel) || TextUtils.isEmpty(eventValue))
            return;
        cleverTapTrackEvent(context, eventName, eventLabel, eventValue, new ArrayList<ExtraCTEvent>(), dataMap);
    }

    public void cleverTapTrackEvent(@NonNull Context context, String eventName, String eventLabel, String eventValue,
                                    List<ExtraCTEvent> cleverTapExtraEvents, HashMap<String, Object> dataMap) {
        /*if (BuildConfig.DEBUG) {
            return;
        }*/

        if (TextUtils.isEmpty(eventName) || TextUtils.isEmpty(eventLabel) || TextUtils.isEmpty(eventValue))
            return;

        try {
            CleverTapAPI ct = CleverTapAPI.getDefaultInstance(context);

            Map<String, Object> prodViewedAction = new HashMap<>();
            prodViewedAction.putAll(getDefaultCleverTapParams(context));
            if (dataMap != null && dataMap.size() > 0) {
                prodViewedAction.putAll(getDefaultCleverTapParamsForUser(context, dataMap));
            }

            prodViewedAction.put(eventLabel, eventValue);

            if (cleverTapExtraEvents != null && !cleverTapExtraEvents.isEmpty()) {
                for (ExtraCTEvent cleverTapExtraEvent : cleverTapExtraEvents) {
                    prodViewedAction.put(cleverTapExtraEvent.getEventLabel(), cleverTapExtraEvent.getEventValue());
                }
            }
            ct.pushEvent(eventName, prodViewedAction);
                /*if (BuildConfig.DEBUG) {
                    Toast.makeText(context, "eventName: " + eventName + " >> eventLabel: " + eventLabel + " >> eventValue: " + eventValue
                            + " >> cleverTapExtraEvents: " + cleverTapExtraEvents, Toast.LENGTH_LONG).show();
                }*/
            Log.d(TAG, "cleverTapTrackEvent()");
            Log.d(TAG, "eventName: " + eventName + " >> eventLabel: " + eventLabel + " >> eventValue: " + eventValue
                    + " >> cleverTapExtraEvents: " + cleverTapExtraEvents);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }


    public void cleverTapTrackEvent(@NonNull Context context, String eventName, String eventLabel, String eventValue, HashMap<String, Object> dataMap,
                                    ExtraCTEvent... cleverTapExtraEvents) {
        if (TextUtils.isEmpty(eventName) || TextUtils.isEmpty(eventLabel) || TextUtils.isEmpty(eventValue))
            return;
        if (cleverTapExtraEvents != null) {
            cleverTapTrackEvent(context, eventName, eventLabel, eventValue, Arrays.asList(cleverTapExtraEvents), dataMap);
        } else {
            cleverTapTrackEvent(context, eventName, eventLabel, eventValue, new ArrayList<ExtraCTEvent>(), dataMap);
        }
    }

    private HashMap<String, Object> getDefaultCleverTapParamsForUser(@NonNull Context context, HashMap<String, Object> dataMap) {
        HashMap<String, Object> prodViewedAction = new HashMap<>();
        try {
            if (dataMap.containsKey("isUserLoggedIn") && (boolean) dataMap.get("isUserLoggedIn")) {
                prodViewedAction.put(STATUS, LOGGED_IN);
            } else {
                prodViewedAction.put(STATUS, GUEST);
            }

            String emailId = (dataMap.containsKey("email")) ? (String) dataMap.get("email") : "";
            if (!TextUtils.isEmpty(emailId)) {
                prodViewedAction.put(EMAIL, emailId);
            }
            String mobile = (dataMap.containsKey("mobile")) ? (String) dataMap.get("mobile") : "";


            if (TextUtils.isEmpty(mobile)) {
                mobile = NOT_AVAILABLE;
            } else {
                mobile = mobile.contains("+91") ? mobile : "+91" + mobile;
            }
            prodViewedAction.put(PHONE, mobile);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
        Log.d(TAG, "getDefaultCleverTapParamsForUser()");
        return prodViewedAction;
    }


    interface PROFILE {
        String NAME = "Name";
        String IDENTITY = "Identity";
        String EMAIL = "Email";
        String PHONE = "Phone";
        String GENDER = "Gender";
        String DOB = "DOB";
        String AGE = "Age";
        String PHOTO_URL = "Photo";
        String FBID = "FBID";
        String GPID = "GPID";
//        String LOGIN_TYPE = "loginType";
    }

    interface Actions {
        String SCREEN_VIEW = "Screen View";
    }

    interface PageView {
        String SCREEN_NAME = "ScreenName";
    }
}
