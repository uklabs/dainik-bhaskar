package com.bhaskar.appscommon.tracking.clevertap;


public class ExtraCTEvent {

    /***
     *
     * @param eventLabel
     * @param eventValue
     */
    public ExtraCTEvent(String eventLabel, Object eventValue) {
        this.eventLabel = eventLabel;
        this.eventValue = eventValue;
    }

    private String eventLabel;
    private Object eventValue;

    public String getEventLabel() {
        return eventLabel;
    }

    public Object getEventValue() {
        return eventValue;
    }

    @Override
    public String toString() {
        return "ExtraCTEvent{" +
                "eventLabel='" + eventLabel + '\'' +
                ", eventValue='" + eventValue + '\'' +
                '}';
    }
}