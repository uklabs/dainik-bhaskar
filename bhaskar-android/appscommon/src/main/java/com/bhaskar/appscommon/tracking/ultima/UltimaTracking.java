package com.bhaskar.appscommon.tracking.ultima;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.util.Systr;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;

import java.util.HashMap;

/**
 * Created by DB on 13-04-2017.
 */

public class UltimaTracking {

    private static final String OPERATING_SYSTEM = "android";
    private static String ultimaId;
    private static String ultimaAdId;

    public interface VIDEO_QUARTILE {
        int PLAY_10 = 1;
        int PLAY_20 = 2;
        int PLAY_30 = 3;
        int PLAY_40 = 4;
        int PLAY_50 = 5;
        int PLAY_60 = 6;
        int PLAY_70 = 7;
        int PLAY_80 = 8;
        int PLAY_90 = 9;
        int PLAY_100 = 10;
    }

    public static void sendUltimaTrackingToServer(Context context, String ultimaTrack, String appTrack) {
        ultimaId = "";

        String url = ultimaTrack;
        url += "&p_sessionID=" + TrackingData.getDBId(context);
        url += "&sess_id=" + TrackingData.getSessionId(context);
        url += "&country=" + TrackingData.getCountry(context);
        url += "&state=" + TrackingData.getState(context);
        url += "&city=" + TrackingData.getCity(context);
        url += "&os=" + OPERATING_SYSTEM + Build.VERSION.SDK_INT;
        url += "&browser_ver=" + TrackingUtils.getAppVersion(context);
        url += "&device_type=2";
        url += "&tm=" + System.currentTimeMillis();
        url += "&app_track=" + appTrack;

//        final String fUrl = url.replace(" ", "%20");
        final String fUrl = TrackingUtils.encode(url);
        Systr.println("Ultima Send url : " + fUrl);

        RequestQueue queue = VolleyNetworkSingleton.getInstance(context).getRequestQueue();
        StringRequest request = new StringRequest(Request.Method.GET, fUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Ultima Response : " + response);

                if (!TextUtils.isEmpty(response)) {
                    String[] str = response.split("-:-");
                    if (str.length > 0) {
                        ultimaId = str[0];
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Ultima Error : " + error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(8000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    public static void sendUltimaTrackingToServer(Context context, String ultimaTrack, int quartile) {

        String url = ultimaTrack;

        url += "&id=" + ultimaId;
        url += "&quartile=" + quartile;
        url += "&mode=1";
        url += "&p_sessionID=" + TrackingData.getDBId(context);
        url += "&sess_id=" + TrackingData.getSessionId(context);
        url += "&country=" + TrackingData.getCountry(context);
        url += "&state=" + TrackingData.getState(context);
        url += "&city=" + TrackingData.getCity(context);
        url += "&os=" + OPERATING_SYSTEM + Build.VERSION.SDK_INT;
        url += "&browser_ver=" + TrackingUtils.getAppVersion(context);
        url += "&device_type=2";

//        String fUrl = url.replace(" ", "%20");
        String fUrl = TrackingUtils.encode(url);

        Systr.println("Ultima Send url : " + fUrl);

        RequestQueue queue = VolleyNetworkSingleton.getInstance(context).getRequestQueue();
        StringRequest request = new StringRequest(Request.Method.GET, fUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Ultima Response : " + response);

                if (!TextUtils.isEmpty(response)) {
                    String[] str = response.split("-:-");
                    if (str.length > 0) {
                        ultimaId = str[0];
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Ultima Error : " + error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(8000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }


    private static HashMap<String, String> adUrls = new HashMap<>();

    public static void sendUltimaAdRequestTrackingToServer(final Context context, String ultimaTrack, String appTrack, String channelId, final String videoId) {
        ultimaAdId = "";

        String url = ultimaTrack;
        url += "?p_sessionID=" + TrackingData.getDBId(context);
        url += "&sess_id=" + TrackingData.getSessionId(context);
        url += "&channel_id=" + channelId;
        url += "&video_id=" + videoId;
        url += "&screen=" + TrackingUtils.getScreenSize(context);
        url += "&app_track=" + appTrack;
        url += "&device_type=2&rec=0&replay=0&domaintype=a&adtype=1&ref_d=appshare&width=0";
        url += "&os=" + OPERATING_SYSTEM + Build.VERSION.SDK_INT;
        url += "&tm=" + System.currentTimeMillis();

        final String fUrl = TrackingUtils.encode(url);
        Systr.println("Ultima Ad Send url : " + fUrl);

        RequestQueue queue = VolleyNetworkSingleton.getInstance(context).getRequestQueue();
        StringRequest request = new StringRequest(Request.Method.GET, fUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Ultima Ad Response : " + response);

                if (!TextUtils.isEmpty(response)) {
                    String[] str = response.split("-:-");
                    if (str.length > 0) {
                        ultimaAdId = str[0];

                        if (adUrls.size() > 0) {
                            String url = adUrls.get(videoId);
                            adUrls.remove(videoId);

                            sendUltimaAdTrackingToServer(context, url);
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Ultima Ad Error : " + error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(8000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    public static void sendUltimaAdTrackingToServer(Context context, String ultimaTrack, String appTrack, String channelId, String videoId, int quarType, String errorValue) {
        String url = ultimaTrack;

//        url += "?p_sessionID=" + TrackingData.getDBId(context);
//        url += "&sess_id=" + TrackingData.getSessionId(context);
//        url += "&channel_id=" + channelId;
//        url += "&video_id=" + videoId;
//        url += "&screen=" + TrackingUtils.getScreenSize(context);
//        url += "&device_type=2&rec=0&replay=0&domaintype=a&adtype=1&ref_d=appshare&width=0";
//        url += "&os=" + OPERATING_SYSTEM + Build.VERSION.SDK_INT;
        url += "?mode=1";
        url += "&tm=" + System.currentTimeMillis();

        if (quarType == 0) {
            url += "&error_code=1";
            url += "&error_type=" + errorValue;
        } else if (quarType == 2) {
            url += "&skipped=1";
        } else if (quarType == 3) {
            url += "&completed=1";
        }

        if (TextUtils.isEmpty(ultimaAdId)) {
            adUrls.put(videoId, url);
        } else {
            sendUltimaAdTrackingToServer(context, url);
        }
    }

    private static void sendUltimaAdTrackingToServer(Context context, String url) {
        url += "&id=" + ultimaAdId;

        String fUrl = TrackingUtils.encode(url);
        Systr.println("Ultima Ad Send url : " + fUrl);

        RequestQueue queue = VolleyNetworkSingleton.getInstance(context).getRequestQueue();
        StringRequest request = new StringRequest(Request.Method.GET, fUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Ultima Ad Response : " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Ultima Ad Error : " + error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(8000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }
}
