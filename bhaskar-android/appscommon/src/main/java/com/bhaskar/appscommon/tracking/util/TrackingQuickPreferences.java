package com.bhaskar.appscommon.tracking.util;


public class TrackingQuickPreferences {

    public static final String HAS_LOCATION = "t_hasLocation";

    public static final String CITY = "t_city";
    public static final String STATE = "t_state";
    public static final String COUNTRY = "t_country";
    public static final String SCREEN_SIZE = "t_screenSize";
    public static final String DB_ID = "t_dbId";
    public static final String DEVICE_ID = "t_deviceId";
    public static final String SESSION_ID = "t_sessionId";
    public static final String PRIMARY_EMAIL = "t_primaryEmail";
    public static final String FIRST_CALL_NOTIFY_TOGGLE = "t_firstCallNotifyToggle";
    public static final String APP_VERSION_FOR_NOTIFY_TOGGLE = "t_appVersionNotify";
    public static final String FIRST_INSTALL_DATE = "t_firstInstallDate";
    public static final String APP_LIST_SENT_APP_VERSION = "t_appListSentAppVersion";
    public static final String APP_INSTALL_DATE = "t_appinstalldate";
    public static final String AD_ID = "t_adid";
}
