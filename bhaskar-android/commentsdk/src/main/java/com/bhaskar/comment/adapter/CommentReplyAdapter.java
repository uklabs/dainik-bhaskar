package com.bhaskar.comment.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bhaskar.comment.R;
import com.bhaskar.comment.util.BooleanExpressionTable;
import com.bhaskar.comment.handler.ArticleCommentCallback;
import com.bhaskar.comment.handler.CommentActionHandler;
import com.bhaskar.comment.model.ArticleCommentInfo;
import com.bhaskar.comment.ui.ReplyActivity;
import com.bhaskar.comment.util.CommentConstant;
import com.bhaskar.comment.util.CommentPreference;
import com.bhaskar.comment.util.CommentUtils;
import com.bhaskar.comment.util.ImageUtil;
import com.bhaskar.comment.views.CircleImageView;
import com.db.util.NetworkStatus;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lalit Goswami on 07-11-2017.
 */

public class CommentReplyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /*View Type*/
    private final int VIEW_TYPE_COMMENT = 1;
    private final int VIEW_TYPE_LOAD_MORE = 2;

    private List<ArticleCommentInfo> articleCommentInfoArrayList;
    private Context mContext;
    private ArticleCommentCallback articleCommentCallback;
    private int loadMoreCount, totalComments;
    private boolean isForReply;
    private BooleanExpressionTable booleanExpressionTable;
    private boolean isNightMode, isAgree;
    private String storyId, storyUrl, storyTitle, host;
    private CommentActionHandler commentActionHandler;

    public CommentReplyAdapter(Context context, boolean isForReply, ArticleCommentCallback articleCommentCallback, CommentActionHandler commentActionHandler, String storyId, String storyTitle, String storyUrl, String host, boolean isAgree, BooleanExpressionTable booleanExpressionTable) {
        mContext = context;
        this.articleCommentInfoArrayList = new ArrayList<>();
        this.articleCommentCallback = articleCommentCallback;
        this.booleanExpressionTable = booleanExpressionTable;
        this.commentActionHandler = commentActionHandler;
        this.isForReply = isForReply;
        this.storyId = storyId;
        this.storyTitle = storyTitle;
        this.storyUrl = storyUrl;
        this.host = host;
        this.isAgree = isAgree;
    }

    public void setData(List<ArticleCommentInfo> articleCommentInfoArrayList, int loadMoreCount, int totalComments) {
        if (articleCommentInfoArrayList != null) {
            this.articleCommentInfoArrayList.clear();
            this.articleCommentInfoArrayList.addAll(articleCommentInfoArrayList);
        }
        this.loadMoreCount = loadMoreCount;
        this.totalComments = totalComments;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == VIEW_TYPE_LOAD_MORE) {
            return new LoadMoreViewHolder(inflater.inflate(R.layout.view_comment_load_more, parent, false));
        } else {
            return new CommentViewHolder(inflater.inflate(R.layout.view_user_comment, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType() == VIEW_TYPE_COMMENT) {
            final CommentViewHolder commentViewHolder = (CommentViewHolder) holder;
            final ArticleCommentInfo articleCommentInfo = articleCommentInfoArrayList.get(position);

            /*Set Day night Theme*/
            if (this.isNightMode) {
                commentViewHolder.commentBgId.setBackground(mContext.getResources().getDrawable(R.drawable.comment_night_background));
                commentViewHolder.tvUserName.setTextColor(ContextCompat.getColor(mContext, R.color.cmt_head_text_night_color));
                commentViewHolder.tvCommentDate.setTextColor(ContextCompat.getColor(mContext, R.color.cmt_date_text_night_color));
                commentViewHolder.tvComment.setTextColor(ContextCompat.getColor(mContext, R.color.cmt_comment_night_color));
                commentViewHolder.ivReply.setColorFilter(ContextCompat.getColor(mContext, R.color.white));
                //set Like

                if (booleanExpressionTable != null && booleanExpressionTable.isBoolean(articleCommentInfo.id, CommentConstant.BooleanConst.ARTICLE_COMMENT_LIKE)) {
                    commentViewHolder.tvLikeCount.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_enable_night_color));
                } else {
                    commentViewHolder.tvLikeCount.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_disable_night_color));
                }

                /*set Report Abuse */
                if (booleanExpressionTable != null && booleanExpressionTable.isBoolean(articleCommentInfo.id, CommentConstant.BooleanConst.ARTICLE_COMMENT_REPORT_ABUSE)) {
                    commentViewHolder.tvReport.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_enable_night_color));
                } else {
                    commentViewHolder.tvReport.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_disable_night_color));
                }
                commentViewHolder.tvReplies.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_disable_night_color));
                commentViewHolder.tvDelete.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_disable_night_color));
            } else {
                commentViewHolder.commentBgId.setBackground(mContext.getResources().getDrawable(R.drawable.comment_background));
                commentViewHolder.tvUserName.setTextColor(ContextCompat.getColor(mContext, R.color.cmt_head_text_day_color));
                commentViewHolder.tvCommentDate.setTextColor(ContextCompat.getColor(mContext, R.color.cmt_head_text_day_color));
                commentViewHolder.tvComment.setTextColor(ContextCompat.getColor(mContext, R.color.cmt_date_text_day_color));
                commentViewHolder.ivReply.setColorFilter(ContextCompat.getColor(mContext, R.color.action_icon_disable_day_color));

                //Like
                if (booleanExpressionTable != null && booleanExpressionTable.isBoolean(articleCommentInfo.id, CommentConstant.BooleanConst.ARTICLE_COMMENT_LIKE)) {
                    commentViewHolder.tvLikeCount.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_enable_day_color));
                } else {
                    commentViewHolder.tvLikeCount.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_disable_day_color));
                }
                /*set Report Abuse */
                if (booleanExpressionTable != null && booleanExpressionTable.isBoolean(articleCommentInfo.id, CommentConstant.BooleanConst.ARTICLE_COMMENT_REPORT_ABUSE)) {
                    commentViewHolder.tvReport.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_enable_day_color));
                } else {
                    commentViewHolder.tvReport.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_disable_day_color));
                }

                commentViewHolder.tvReplies.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_disable_day_color));
                commentViewHolder.tvDelete.setTextColor(ContextCompat.getColor(mContext, R.color.action_icon_disable_day_color));
            }

            /*set Date */
            if (!TextUtils.isEmpty(articleCommentInfo.dateTime)) {
                commentViewHolder.tvCommentDate.setVisibility(View.VISIBLE);
                try {
                    commentViewHolder.tvCommentDate.setText(CommentUtils.formatToYesterdayOrToday(articleCommentInfo.dateTime));
                } catch (ParseException ignored) {

                }
            } else {
                commentViewHolder.tvCommentDate.setVisibility(View.GONE);
            }

            /*set User Name*/
            if (!TextUtils.isEmpty(articleCommentInfo.userName))
                commentViewHolder.tvUserName.setText(articleCommentInfo.userName);

            /*setLike Count*/
            if (articleCommentInfo.likeCount > 0)
                commentViewHolder.tvLikeCount.setText(mContext.getString(R.string.like_with_count, CommentUtils.getFinalCount(articleCommentInfo.likeCount)));
            else
                commentViewHolder.tvLikeCount.setText(mContext.getString(R.string.like));

            /*Set Report Abuse */
            if (articleCommentInfo.isSameUser || !TextUtils.isEmpty(articleCommentInfo.userId) && articleCommentInfo.userId.equalsIgnoreCase(CommentPreference.getInstance(mContext).getStringValue(CommentConstant.CommentPref.USER_ID, ""))) {
                commentViewHolder.tvReportLayout.setVisibility(View.GONE);
                commentViewHolder.tvDeleteLayout.setVisibility(View.VISIBLE);
            } else {
                commentViewHolder.tvReportLayout.setVisibility(View.VISIBLE);
                commentViewHolder.tvDeleteLayout.setVisibility(View.GONE);
                if (booleanExpressionTable != null && booleanExpressionTable.isBoolean(articleCommentInfo.id, CommentConstant.BooleanConst.ARTICLE_COMMENT_REPORT_ABUSE)) {
                    commentViewHolder.ivReport.setColorFilter(ContextCompat.getColor(mContext, R.color.action_icon_enable_day_color));
                    if (articleCommentInfo.reportAbuseCount > 0)
                        commentViewHolder.tvReport.setText(mContext.getString(R.string.already_report_count, CommentUtils.getSIPrefixForCount(articleCommentInfo.reportAbuseCount)));
                    else
                        commentViewHolder.tvReport.setText(mContext.getString(R.string.already_report));
                } else {
                    if (articleCommentInfo.reportAbuseCount > 0)
                        commentViewHolder.tvReport.setText(mContext.getString(R.string.report_count, CommentUtils.getSIPrefixForCount(articleCommentInfo.reportAbuseCount)));
                    else
                        commentViewHolder.tvReport.setText(mContext.getString(R.string.report));

                    commentViewHolder.ivReport.setColorFilter(ContextCompat.getColor(mContext, R.color.action_icon_disable_day_color));
                    commentViewHolder.tvReportLayout.setVisibility(View.VISIBLE);
                    commentViewHolder.tvReportLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (NetworkStatus.getInstance().isConnected(mContext)) {
                                if (CommentPreference.getInstance(mContext).getBooleanValue(CommentConstant.CommentPref.IS_COMMENT_LOG_IN, false)) {
                                    if (booleanExpressionTable != null && !booleanExpressionTable.isBoolean(articleCommentInfo.id, CommentConstant.BooleanConst.ARTICLE_COMMENT_REPORT_ABUSE)) {
                                        articleCommentCallback.actionClick(CommentConstant.API_ACTION_TYPE.API_REPORT, position);
                                    } else {
                                        if (mContext != null)
                                            CommentUtils.showCustomToast(mContext, mContext.getString(R.string.already_reported));
                                    }
                                } else {
                                    if (commentActionHandler != null)
                                        commentActionHandler.onLoginClick();
                                }
                            } else {
                                if (mContext != null)
                                    CommentUtils.showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                            }
                        }
                    });
                }
            }

            /*delete Comment*/
            commentViewHolder.tvDeleteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkStatus.getInstance().isConnected(mContext)) {
                        articleCommentCallback.actionClick(CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT, position);
                    } else {
                        if (mContext != null)
                            CommentUtils.showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                    }
                }
            });

            /*set Reply Count*/
            if (isForReply) {
                if (commentViewHolder.tvReportLayout.getVisibility() == View.VISIBLE)
                    commentViewHolder.tvRepliesLayout.setVisibility(View.GONE);
                else
                    commentViewHolder.tvRepliesLayout.setVisibility(View.INVISIBLE);
                commentViewHolder.replies_separator.setVisibility(View.GONE);
            } else {
                commentViewHolder.tvRepliesLayout.setVisibility(View.VISIBLE);
                if (articleCommentInfo.replyCount > 0)
                    commentViewHolder.tvReplies.setText(mContext.getString(R.string.replies_with_count, CommentUtils.getFinalCount(articleCommentInfo.replyCount)));
                else
                    commentViewHolder.tvReplies.setText(mContext.getString(R.string.replies));
            }

            /*set Description*/
            if (!TextUtils.isEmpty(articleCommentInfo.description)) {
                commentViewHolder.tvComment.setText(Html.fromHtml(articleCommentInfo.description));
                commentViewHolder.tvComment.setMovementMethod(CommentUtils.privacyMovementMethod(mContext, commentActionHandler));
            }

            if (booleanExpressionTable != null && booleanExpressionTable.isBoolean(articleCommentInfo.id, CommentConstant.BooleanConst.ARTICLE_COMMENT_LIKE)) {
                commentViewHolder.ivLike.setColorFilter(ContextCompat.getColor(mContext, R.color.action_icon_enable_day_color));
            } else {
                commentViewHolder.ivLike.setColorFilter(ContextCompat.getColor(mContext, R.color.action_icon_disable_day_color));
                commentViewHolder.tvLikeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (NetworkStatus.getInstance().isConnected(mContext)) {
                            if (booleanExpressionTable != null && !booleanExpressionTable.isBoolean(articleCommentInfo.id, CommentConstant.BooleanConst.ARTICLE_COMMENT_LIKE)) {
                                articleCommentCallback.actionClick(CommentConstant.API_ACTION_TYPE.API_LIKE, position);
                            } else {
                                if (mContext != null)
                                    CommentUtils.showCustomToast(mContext, mContext.getString(R.string.already_liked));
                            }
                        } else {
                            if (mContext != null)
                                CommentUtils.showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                        }
                    }
                });
            }

            /*set Profile Pic*/
            String userImage = articleCommentInfo.userImage;
            if (TextUtils.isEmpty(userImage)) {
                userImage = "xyz";
            }

            if (isNightMode) {
                commentViewHolder.ivUserImage.setCircleBackgroundColor(ContextCompat.getColor(mContext, R.color.grey));
                commentViewHolder.ivUserImage.setBorderColor(ContextCompat.getColor(mContext, R.color.white));
            } else {
                commentViewHolder.ivUserImage.setCircleBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
                commentViewHolder.ivUserImage.setBorderColor(ContextCompat.getColor(mContext, R.color.grey));
            }

            ImageUtil.setImage(mContext, userImage, commentViewHolder.ivUserImage, com.bhaskar.R.drawable.default_pic);

            if (!isForReply) {
                commentViewHolder.tvRepliesLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (NetworkStatus.getInstance().isConnected(mContext)) {
                            if (CommentPreference.getInstance(mContext).getBooleanValue(CommentConstant.CommentPref.IS_COMMENT_LOG_IN, false)) {
                                Intent replyIntent = new Intent(mContext, ReplyActivity.class);
                                replyIntent.putExtra("post_id", articleCommentInfo.postId);
                                replyIntent.putExtra("channel_slno", articleCommentInfo.channelSlNo);
                                replyIntent.putExtra("load_more_size", loadMoreCount);
                                replyIntent.putExtra("comment_id", articleCommentInfo.id);
                                replyIntent.putExtra("story_id", storyId);
                                replyIntent.putExtra("story_url", storyUrl);
                                replyIntent.putExtra("story_title", storyTitle);
                                replyIntent.putExtra("host", host);
                                replyIntent.putExtra("is_agree", isAgree);
                                replyIntent.putExtra("in_reply_user_id", articleCommentInfo.userId);
                                replyIntent.putExtra("in_reply_comment_id", articleCommentInfo.id);
                                ((AppCompatActivity) mContext).startActivityForResult(replyIntent, ReplyActivity.REPLY_REQUEST_CODE);
//                                if (mContext instanceof AppCompatActivity)
//                                    ((AppCompatActivity) mContext).overridePendingTransition(com.bhaskar.R.anim.slide_from_right, com.bhaskar.R.anim.slide_to_left);
                            } else {
                                if (commentActionHandler != null)
                                    commentActionHandler.onLoginClick();
                            }
                        } else {
                            if (mContext != null)
                                CommentUtils.showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));
                        }
                    }
                });
            }

        } else if (holder.getItemViewType() == VIEW_TYPE_LOAD_MORE) {
            final LoadMoreViewHolder loadMoreViewHolder = (LoadMoreViewHolder) holder;
            loadMoreViewHolder.btLoadMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkStatus.getInstance().isConnected(mContext)) {
                        articleCommentCallback.onLoadMore(articleCommentInfoArrayList.get(position - 1).dateTime);
                    } else {
                        CommentUtils.showCustomToast(mContext, mContext.getResources().getString(R.string.no_network_error));

                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        int viewLoadMore = (isForReply) ? 0 : (articleCommentInfoArrayList != null && articleCommentInfoArrayList.size() > 0) ? (articleCommentInfoArrayList.size() < totalComments) ? 1 : 0 : 0;
        return (articleCommentInfoArrayList != null && articleCommentInfoArrayList.size() > 0) ? articleCommentInfoArrayList.size() + viewLoadMore : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (articleCommentInfoArrayList.size() > position) {
            return VIEW_TYPE_COMMENT;
        } else {
            return VIEW_TYPE_LOAD_MORE;
        }
    }

    public void setDayNightMode(boolean dayNightMode) {
        this.isNightMode = dayNightMode;
    }

    public void removeLoadMoreItem() {

    }

    public void deleteComment(int index) {
        articleCommentInfoArrayList.remove(index);
        totalComments = totalComments - 1;
        notifyDataSetChanged();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout commentBgId;
        private CircleImageView ivUserImage;
        private TextView tvLikeCount, tvUserName, tvComment, tvReplies, tvReport, tvCommentDate, tvDelete;
        private RelativeLayout tvReportLayout, tvLikeLayout, tvRepliesLayout, tvDeleteLayout;
        private View replies_separator;
        private ImageView ivLike, ivReport, ivReply;

        CommentViewHolder(View itemView) {
            super(itemView);
            commentBgId = itemView.findViewById(R.id.comment_bg_id);
            ivUserImage = itemView.findViewById(R.id.iv_user_image);
            tvLikeLayout = itemView.findViewById(R.id.tv_like_layout);
            tvDeleteLayout = itemView.findViewById(R.id.tv_delete_layout);
            tvReportLayout = itemView.findViewById(R.id.tv_report_layout);
            tvRepliesLayout = itemView.findViewById(R.id.tv_replies_layout);
            tvUserName = itemView.findViewById(R.id.tv_user_name);
            tvComment = itemView.findViewById(R.id.tv_comment);
            tvLikeCount = itemView.findViewById(R.id.tv_like_count);
            tvReplies = itemView.findViewById(R.id.tv_replies);
            tvReport = itemView.findViewById(R.id.tv_report);
            ivReport = itemView.findViewById(R.id.civ_report);
            replies_separator = itemView.findViewById(R.id.replies_separator);
            ivLike = itemView.findViewById(R.id.civ_like);
            tvCommentDate = itemView.findViewById(R.id.comment_date_tv);
            tvDelete = itemView.findViewById(R.id.tv_delete);
            ivReply = itemView.findViewById(R.id.civ_reply);

        }
    }

    public class LoadMoreViewHolder extends RecyclerView.ViewHolder {
        private Button btLoadMore;

        LoadMoreViewHolder(View itemView) {
            super(itemView);
            btLoadMore = itemView.findViewById(R.id.bt_load_more);
        }
    }
}
