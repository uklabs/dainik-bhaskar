package com.bhaskar.comment.handler;

/**
 * Created by Lalit Goswami on 07-11-2017.
 */

public interface ArticleCommentCallback {
    void onSuccess(int actionType);

    void onFailure(int actionType);

    void onLoadMore(String dateTime);

    void actionClick(int apiLike, int position);
}
