package com.bhaskar.comment.handler;

import java.io.Serializable;

/**
 * Created by Lalit Goswami on 06-11-2017.
 */

public interface CommentActionHandler extends Serializable {
    void onLoginClick();

    void userIngestAfterLogin(int apiAction, ArticleCommentCallback articleCommentCallback);

    void userIngestWithoutLogin(String userName, String userEmail, int apiAction, ArticleCommentCallback articleCommentCallback);

    void onExpand();

    void openLink(String link);

    void updateHeaderTitle(String count);

}
