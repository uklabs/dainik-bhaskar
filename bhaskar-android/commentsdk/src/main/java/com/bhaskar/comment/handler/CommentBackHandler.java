package com.bhaskar.comment.handler;

import java.io.Serializable;

/**
 * Created by Lalit Goswami on 06-11-2017.
 */

public interface CommentBackHandler extends Serializable {

    void getComment();

}
