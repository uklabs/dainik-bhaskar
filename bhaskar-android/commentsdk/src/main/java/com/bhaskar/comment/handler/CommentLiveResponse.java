package com.bhaskar.comment.handler;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DB on 11/11/2017.
 */

public class CommentLiveResponse {

    @SerializedName("status")
    private int statusValue;
    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(int statusValue) {
        this.statusValue = statusValue;
    }
}
