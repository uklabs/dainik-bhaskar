package com.bhaskar.comment.handler;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DB on 11/11/2017.
 */

public class CommentResponse {

    @SerializedName("live")
    private String LiveValue;

    public String getLiveValue() {
        return LiveValue;
    }

    public void setLiveValue(String liveValue) {
        LiveValue = liveValue;
    }
}
