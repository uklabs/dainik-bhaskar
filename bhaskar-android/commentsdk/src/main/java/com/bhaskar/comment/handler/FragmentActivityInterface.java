package com.bhaskar.comment.handler;

import java.io.Serializable;

/**
 * Created by lalgos1 on 26-02-2018.
 */

public interface FragmentActivityInterface extends Serializable {
    void onLoginClick();

    void userIngestAfterLogin(int apiAction, ArticleCommentCallback articleCommentCallback);

    void userIngestWithoutLogin(int apiAction, ArticleCommentCallback articleCommentCallback);
}
