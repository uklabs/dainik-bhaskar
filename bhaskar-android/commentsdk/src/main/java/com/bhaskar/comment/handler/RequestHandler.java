package com.bhaskar.comment.handler;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.comment.util.CommentUtils;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.LoginController;
import com.bhaskar.comment.model.ArticleCommentInfo;
import com.bhaskar.comment.util.CommentConstant;
import com.bhaskar.comment.util.CommentPreference;
import com.bhaskar.comment.util.CommentUrls;
import com.db.util.Systr;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lalit Goswami on 17-10-2017.
 */

public class RequestHandler {

    public static void postUserData(Context context, String accountType, String birth_date, String city, String country, String email, String gender,
                                    String hometown, String host, String mobile, String userId, String sessionId, String image,
                                    String location, String name, String state, final int apiAction, final ArticleCommentCallback successFailureCallback) {
        Systr.println("User Ingest");
        /*used in Comment/Reply Ingestion*/
        CommentPreference.getInstance(context).setBooleanValue(CommentConstant.CommentPref.IS_COMMENT_LOG_IN, true);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_NAME, name);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_IMAGE, image);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_EMAIL, email);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_ID, userId);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.SESSION_ID, sessionId);
        CommentPreference.getInstance(context).setBooleanValue(CommentConstant.CommentPref.IS_BY_ANONYMOUS, false);

        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.ACCOUNT_TYPE, accountType);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_GENDER, gender);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.BIRTH_DATE, birth_date);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_MOBILE, mobile);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_HOMETOWN, hometown);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_LOCATION, location);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_CITY, city);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_STATE, state);
        CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_COUNTRY, country);


        JSONObject allElements = new JSONObject();
        try {
            allElements.put("account_type", accountType);
            allElements.put("birth_date", birth_date);
            allElements.put("city", city);
            allElements.put("country", country);
            allElements.put("email", email);
            allElements.put("gender", gender);
            allElements.put("hometown", hometown);
            allElements.put("host", host);
            allElements.put("mobile", mobile);
            allElements.put("id", userId);
            allElements.put("session_id", sessionId);
            allElements.put("image", image);
            allElements.put("location", location);
            allElements.put("name", name);
            allElements.put("state", state);
        } catch (JSONException ignored) {

        }
        Systr.println("User Ingest Data = " + allElements);
        final JSONArray elements = new JSONArray();
        elements.put(allElements);

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, CommentUrls.COMMENT_USER_DATA_INGEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                try {
                    JSONObject response = new JSONObject(decryptString(res));
                    if (!TextUtils.isEmpty(response.toString())) {
                        if (response.has("live")) {
//                            CommentResponse commentResponse = parseCommentResponseExtraData(response.toString());
//                            CommentLiveResponse commentLiveResponse = parseCommentLiveResponseExtraData(commentResponse.getLiveValue());

                            // int statusCode = commentLiveResponse.getStatusValue();
                            int statusCode = response.getJSONObject("live").optInt("status");

                            if (statusCode == 200) {
                                if (successFailureCallback != null) {
                                    successFailureCallback.onSuccess(apiAction);
                                }
                            } else {
                                if (successFailureCallback != null) {
                                    successFailureCallback.onFailure(apiAction);
                                }
                            }

                        } else {
                            if (successFailureCallback != null) {
                                successFailureCallback.onFailure(apiAction);
                            }
                        }
                    } else {
                        if (successFailureCallback != null) {
                            successFailureCallback.onFailure(apiAction);
                        }
                    }
                } catch (Exception e) {
                    if (successFailureCallback != null) {
                        successFailureCallback.onFailure(apiAction);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (successFailureCallback != null) {
                    successFailureCallback.onFailure(apiAction);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommentConstant.USER_AGENT);
                headers.put("encrypt", "1");
//                headers.put("Content-Type", "application/json charset=utf-8");
//                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                String json = elements.toString();
                String data = encryptString(json);
                Map<String, String> params = new HashMap<>();
                params.put("edata", data);
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(CommentUtils.TIMEOUT_VALUE, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }

    public static void postComment(Context context, int postId, String storyid, String title, String description, String channelSlno, String url,
                                   boolean isReply, String host, boolean isAgree, final int apiAction, final ArticleCommentCallback successFailureCallback, final ArticleCommentInfo articleCommentInfo) {
        Systr.println("Post Comment");


        String userId = CommentPreference.getInstance(context).getStringValue(CommentConstant.CommentPref.USER_ID, "");
        String email = CommentPreference.getInstance(context).getStringValue(CommentConstant.CommentPref.USER_EMAIL, "");
        String name = CommentPreference.getInstance(context).getStringValue(CommentConstant.CommentPref.USER_NAME, "");
        String image = CommentPreference.getInstance(context).getStringValue(CommentConstant.CommentPref.USER_IMAGE, "");
        String sessionId = CommentPreference.getInstance(context).getStringValue(CommentConstant.CommentPref.SESSION_ID, "");
        boolean isByAnonymous = CommentPreference.getInstance(context).getBooleanValue(CommentConstant.CommentPref.IS_BY_ANONYMOUS, false);

        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
        if (profileInfo != null) {
            userId = profileInfo.uID;
            CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_ID, userId);
            email = profileInfo.email;
            CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_EMAIL, email);
            name = profileInfo.name;
            CommentPreference.getInstance(context).setStringValue(CommentConstant.CommentPref.USER_NAME, name);

            image = profileInfo.profilePic;
        }

        /*create comment info*/
        articleCommentInfo.userId = userId;
        articleCommentInfo.isSameUser = true;
        articleCommentInfo.email = email;
        articleCommentInfo.channelSlNo = channelSlno;
        articleCommentInfo.description = description;
        articleCommentInfo.userName = name;
        articleCommentInfo.userImage = image;

        final JSONObject allElements = new JSONObject();
        try {
            allElements.put("post_id", postId);
            allElements.put("storyid", storyid);
            allElements.put("title", title);
            allElements.put("user_id", userId);
            allElements.put("email", email);
            allElements.put("description", description);
            allElements.put("channel_slno", channelSlno);
            allElements.put("name", name);
            allElements.put("image", image);
            allElements.put("url", url);
            allElements.put("isReply", isReply);
            allElements.put("isAgree", isAgree);
            allElements.put("isByAnonymousUser", isByAnonymous);
            allElements.put("session_id", sessionId);
            allElements.put("status", 1);
            allElements.put("host", host);
        } catch (JSONException ignored) {
        }

        Systr.println("Comment Ingest Data = " + allElements);

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, CommentUrls.COMMENT_INGEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                try {
                    JSONObject response = new JSONObject(decryptString(res));
                    if (!TextUtils.isEmpty(response.toString())) {
                        if (response.has("live")) {
                            int statusCode = response.getJSONObject("live").optInt("status");
                            if (statusCode == 200) {
                                articleCommentInfo.id = response.getJSONObject("live").optString("id");
                                if (successFailureCallback != null) {
                                    successFailureCallback.onSuccess(apiAction);
                                }
                            } else {
                                if (successFailureCallback != null) {
                                    successFailureCallback.onFailure(apiAction);
                                }
                            }

                        } else {
                            if (successFailureCallback != null) {
                                successFailureCallback.onFailure(apiAction);
                            }
                        }
                    } else {
                        if (successFailureCallback != null) {
                            successFailureCallback.onFailure(apiAction);
                        }
                    }
                } catch (Exception e) {
                    if (successFailureCallback != null) {
                        successFailureCallback.onFailure(apiAction);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (successFailureCallback != null) {
                    successFailureCallback.onFailure(apiAction);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommentConstant.USER_AGENT);
                headers.put("encrypt", "1");
//                headers.put("Content-Type", "application/json charset=utf-8");
//                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                String json = allElements.toString();
                String data = encryptString(json);
                Map<String, String> params = new HashMap<>();
                params.put("edata", data);
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(CommentUtils.TIMEOUT_VALUE, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static CommentResponse parseCommentResponseExtraData(String json) {
        JSONObject extaData = null;
        try {
            if (!TextUtils.isEmpty(json)) {
                extaData = new JSONObject(json);
                return new Gson().fromJson(extaData.toString(), CommentResponse.class);
            } else {
                return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static CommentLiveResponse parseCommentLiveResponseExtraData(String json) {
        JSONObject extaData = null;
        try {
            if (!TextUtils.isEmpty(json)) {
                extaData = new JSONObject(json);
                return new Gson().fromJson(extaData.toString(), CommentLiveResponse.class);
            } else {
                return null;

            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void postCommentReply(Context context, int postId, String storyid, String title, String description, String channelSlno, String url,
                                        boolean isReply, String inReplyToCommentId,
                                        String inReplyToUserId, String host, boolean isAgree, final int apiAction, final ArticleCommentCallback successFailureCallback, final ArticleCommentInfo articleCommentInfo) {
        Systr.println("Post Reply");

        String userId = CommentPreference.getInstance(context).getStringValue(CommentConstant.CommentPref.USER_ID, "");
        String email = CommentPreference.getInstance(context).getStringValue(CommentConstant.CommentPref.USER_EMAIL, "");
        String name = CommentPreference.getInstance(context).getStringValue(CommentConstant.CommentPref.USER_NAME, "");
        String image = CommentPreference.getInstance(context).getStringValue(CommentConstant.CommentPref.USER_IMAGE, "");
        String sessionId = CommentPreference.getInstance(context).getStringValue(CommentConstant.CommentPref.SESSION_ID, "");
        boolean isByAnonymousUser = CommentPreference.getInstance(context).getBooleanValue(CommentConstant.CommentPref.IS_BY_ANONYMOUS, false);

        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
        if (profileInfo != null) {
            userId = profileInfo.uID;
            email = profileInfo.email;
            name = profileInfo.name;
            image = profileInfo.profilePic;
        }

        /*create comment info*/
        articleCommentInfo.userId = userId;
        articleCommentInfo.email = email;
        articleCommentInfo.channelSlNo = channelSlno;
        articleCommentInfo.description = description;
        articleCommentInfo.userName = name;
        articleCommentInfo.userImage = image;

        final JSONObject allElements = new JSONObject();
        try {
            allElements.put("post_id", postId);
            allElements.put("storyid", storyid);
            allElements.put("title", title);
            allElements.put("user_id", userId);
            allElements.put("email", email);
            allElements.put("description", description);
            allElements.put("channel_slno", channelSlno);
            allElements.put("name", name);
            allElements.put("image", image);
            allElements.put("url", url);
            allElements.put("isReply", isReply);
            allElements.put("isAgree", isAgree);
            allElements.put("isByAnonymousUser", isByAnonymousUser);
            allElements.put("session_id", sessionId);
            allElements.put("in_reply_to_comment_id", inReplyToCommentId);
            allElements.put("in_reply_to_user_id", inReplyToUserId);
            allElements.put("status", 1);
            allElements.put("host", host);
        } catch (JSONException ignored) {

        }

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, CommentUrls.REPLY_INGEST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                try {
                    JSONObject response = new JSONObject(decryptString(res));
                    if (!TextUtils.isEmpty(response.toString())) {
                        if (response.has("live")) {
//
//                            CommentResponse commentResponse = parseCommentResponseExtraData(response.toString());
//                            CommentLiveResponse commentLiveResponse = parseCommentLiveResponseExtraData(commentResponse.getLiveValue());
//

                            int statusCode = response.getJSONObject("live").optInt("status");
                            if (statusCode == 200) {
                                String commentId = response.getJSONObject("live").optString("id");
                                articleCommentInfo.id = commentId;
                                if (successFailureCallback != null) {
                                    successFailureCallback.onSuccess(apiAction);
                                }
                            } else {
                                if (successFailureCallback != null) {
                                    successFailureCallback.onFailure(apiAction);
                                }
                            }

                        } else {
                            if (successFailureCallback != null) {
                                successFailureCallback.onFailure(apiAction);
                            }
                        }
                    } else {
                        if (successFailureCallback != null) {
                            successFailureCallback.onFailure(apiAction);
                        }
                    }
                } catch (Exception e) {
                    if (successFailureCallback != null) {
                        successFailureCallback.onFailure(apiAction);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommentConstant.USER_AGENT);
                headers.put("encrypt", "1");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                String json = allElements.toString();
                String data = encryptString(json);
                Map<String, String> params = new HashMap<>();
                params.put("edata", data);
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(CommentUtils.TIMEOUT_VALUE, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }


    public static void likeReportCount(Context context, String postId, String id, String channelSlNo, String action, final int apiAction, final ArticleCommentCallback successFailureCallback) {
        Systr.println("Action = " + apiAction);
        JSONObject allElements = new JSONObject();
        try {
            allElements.put("id", id);
            allElements.put("event", action);
            allElements.put("post_id", postId);
            allElements.put("channel_slno", channelSlNo);
        } catch (Exception ignored) {
        }
        String finalUrl = (action.equalsIgnoreCase(CommentConstant.LikeReportActionType.LIKE_ACTION) ? CommentUrls.COMMENT_LIKE_COUNT_URL : CommentUrls.COMMENT_REPORT_ABUSE_URL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, allElements, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (!TextUtils.isEmpty(response.toString())) {
                        if (response.has("live")) {
//                            CommentResponse commentResponse = parseCommentResponseExtraData(response.toString());
//                            CommentLiveResponse commentLiveResponse = parseCommentLiveResponseExtraData(commentResponse.getLiveValue());

                            int statusCode = response.getJSONObject("live").optInt("status");
                            if (statusCode == 200) {
                                if (successFailureCallback != null) {
                                    successFailureCallback.onSuccess(apiAction);
                                }
                            } else {
                                if (successFailureCallback != null) {
                                    successFailureCallback.onFailure(apiAction);
                                }
                            }

                        } else {
                            if (successFailureCallback != null) {
                                successFailureCallback.onFailure(apiAction);
                            }
                        }
                    } else {
                        if (successFailureCallback != null) {
                            successFailureCallback.onFailure(apiAction);
                        }
                    }
                } catch (Exception e) {
                    if (successFailureCallback != null) {
                        successFailureCallback.onFailure(apiAction);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (successFailureCallback != null) {
                    successFailureCallback.onFailure(apiAction);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommentConstant.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(CommentUtils.TIMEOUT_VALUE, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void deleteComment(Context context, String postId, String id, String userId, String sessionId, final int apiAction, final ArticleCommentCallback successFailureCallback) {
        Systr.println("Action = " + apiAction);
        final JSONObject allElements = new JSONObject();
        try {
            allElements.put("id", id);
            allElements.put("user_id", userId);
            allElements.put("session_id", sessionId);
            allElements.put("post_id", postId);

        } catch (Exception ignored) {
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CommentUrls.COMMENT_DELETE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                try {
                    JSONObject response = new JSONObject(decryptString(res));
                    if (!TextUtils.isEmpty(response.toString())) {
                        int statusCode = response.getInt("status");
                        if (statusCode == 200) {
                            if (successFailureCallback != null) {
                                successFailureCallback.onSuccess(apiAction);
                            }
                        } else {
                            if (successFailureCallback != null) {
                                successFailureCallback.onFailure(apiAction);
                            }
                        }

                    } else {
                        if (successFailureCallback != null) {
                            successFailureCallback.onFailure(apiAction);
                        }
                    }
                } catch (Exception e) {
                    if (successFailureCallback != null) {
                        successFailureCallback.onFailure(apiAction);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Error" + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommentConstant.USER_AGENT);
                headers.put("encrypt", "1");
//                headers.put("Content-Type", "application/json charset=utf-8");
//                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                String json = allElements.toString();
                String data = encryptString(json);
                Map<String, String> params = new HashMap<>();
                params.put("edata", data);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(CommentUtils.TIMEOUT_VALUE, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public static String decryptString(String str) {
        try {
            return new String(Base64.decode(str, Base64.DEFAULT), "UTF-8");
        } catch (Exception e) {
        }

        return "";
    }

    public static String encryptString(String str) {
        try {
            return Base64.encodeToString(str.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (Exception e) {
        }

        return "";
    }
}
