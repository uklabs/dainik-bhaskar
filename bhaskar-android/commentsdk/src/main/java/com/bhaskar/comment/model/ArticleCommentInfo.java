package com.bhaskar.comment.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Lalit Goswami on 04-11-2017.
 */

public class ArticleCommentInfo {

    @SerializedName("channel_slno")
    public String channelSlNo;

    @SerializedName("datetime")
    public String dateTime;

    @SerializedName("description")
    public String description;

    @SerializedName("email")
    public String email;

    @SerializedName("id")
    public String id;

    @SerializedName("in_reply_to_comment_id")
    public String inReplyToCommentId;

    @SerializedName("in_reply_to_user_id")
    public String inReplyToUserId;

    @SerializedName("like_count")
    public int likeCount;

    @SerializedName("user_id")
    public String userId;

    @SerializedName("name")
    public String userName;

    @SerializedName("image")
    public String userImage;

    @SerializedName("post_id")
    public String postId;

    @SerializedName("reply_count")
    public int replyCount;

    @SerializedName("report_abuse_count")
    public int reportAbuseCount;

    @SerializedName("url")
    public String url;


    @SerializedName("isReply")
    public boolean isReply;

    @SerializedName("modified_description")
    public String modifiedDescription;

    @SerializedName("isAgree")
    public boolean isAgree;

    public boolean isLiked = false;
    public boolean isReported = false;
    public boolean isSameUser = false;

    public ArticleCommentInfo() {
    }

    public ArticleCommentInfo(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArticleCommentInfo that = (ArticleCommentInfo) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

}
