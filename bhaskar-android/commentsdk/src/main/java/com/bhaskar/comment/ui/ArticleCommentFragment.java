package com.bhaskar.comment.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.comment.util.CommentUtils;
import com.bhaskar.util.LoginController;
import com.bhaskar.comment.R;
import com.bhaskar.comment.adapter.CommentReplyAdapter;
import com.bhaskar.comment.util.BooleanExpressionTable;
import com.bhaskar.comment.util.DatabaseHelper;
import com.bhaskar.comment.handler.ArticleCommentCallback;
import com.bhaskar.comment.handler.CommentActionHandler;
import com.bhaskar.comment.handler.CommentBackHandler;
import com.bhaskar.comment.handler.RequestHandler;
import com.bhaskar.comment.model.ArticleCommentInfo;
import com.bhaskar.comment.util.CommentConstant;
import com.bhaskar.comment.util.CommentPreference;
import com.db.util.Systr;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.db.util.Urls.RETRIEVE_ALL_COMMENTS;

/**
 * Created by Lalit Goswami on 06-11-2017.
 */

public class ArticleCommentFragment extends Fragment implements View.OnClickListener, ArticleCommentCallback, Serializable, CommentBackHandler {
    private boolean isCommentCollapsed = true;
    private BooleanExpressionTable booleanExpressionTable;
    private boolean isExpandable = false;
    private LinearLayout commentRootLayout;
    private ImageView commentStatus;
    private EditText etCommentBox;
    private CommentReplyAdapter commentReplyAdapter;
    private CommentActionHandler commentActionHandler;
    private List<ArticleCommentInfo> articleCommentInfoList;
    private String storyUrl, channelSlno;
    private String host, storyId, storyTitle;
    private String commentId;
    private int postId, loadMoreCount;
    private int totalComments;
    private boolean isCommentLogin, isAgree = true, isNightMode;
    private ArticleCommentInfo articleCommentInfo = new ArticleCommentInfo();
    private View view;
    private int IDLE_MODE = 0;
    private int POST_MODE = 1;
    private int postStatus = IDLE_MODE;
    private int likeStatus = IDLE_MODE;
    private int reportStatus = IDLE_MODE;
    private int deleteStatus = IDLE_MODE;
    private int loadMoreStatus = IDLE_MODE;

    public static ArticleCommentFragment getInstance(boolean isLogin, String storyUrl, String channelSlno, int size, String host, String storyId, String storyTitle, boolean isNightMode) {
        ArticleCommentFragment articleCommentFragment = new ArticleCommentFragment();
        Bundle argument = new Bundle();
        argument.putBoolean("is_login", isLogin);
        argument.putString("story_url", storyUrl);
        argument.putString("channel_slno", channelSlno);
        argument.putInt("loadMoreCount", size);
        argument.putString("host", host);
        argument.putString("story_id", storyId);
        argument.putString("story_title", storyTitle);
        argument.putBoolean("is_night_mode", isNightMode);
        articleCommentFragment.setArguments(argument);
        return articleCommentFragment;
    }

    public void setExpandableModeOn(boolean bo) {
        isExpandable = bo;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        commentActionHandler = (CommentActionHandler) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        boolean isLogin = false;
        if (bundle != null) {
            storyUrl = bundle.getString("story_url");
            isLogin = bundle.getBoolean("is_login");
            channelSlno = bundle.getString("channel_slno");
            loadMoreCount = bundle.getInt("loadMoreCount");
            host = bundle.getString("host");
            storyId = bundle.getString("story_id");
            storyTitle = bundle.getString("story_title");
            isNightMode = bundle.getBoolean("is_night_mode");
        }
        CommentPreference.getInstance(getContext()).setBooleanValue(CommentConstant.CommentPref.IS_COMMENT_LOG_IN, isLogin);
        isCommentLogin = CommentPreference.getInstance(getContext()).getBooleanValue(CommentConstant.CommentPref.IS_COMMENT_LOG_IN, false);
        /*if user login in app but not in comment*/
        if (isCommentLogin) {
            commentActionHandler.userIngestAfterLogin(CommentConstant.API_ACTION_TYPE.API_USER_INGEST, this);
        }

        booleanExpressionTable = (BooleanExpressionTable) DatabaseHelper.getInstance(getContext()).getTableObject(BooleanExpressionTable.TABLE_NAME);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_root_comment, container, false);
        initView(view);
        isExpandable = true;
        setDayNightMode(isNightMode);
        postId = CommentUtils.postHashCode(storyUrl);
        Log.e("POST_ID", "=>" + postId);
        if (articleCommentInfoList != null) {
            articleCommentInfoList.clear();
        }
        if (postId != 0)
            getAllArticleComments(channelSlno, postId, loadMoreCount, "", false);

        if (isExpandable) {
            expandCommentView();
            commentStatus.setVisibility(View.GONE);
        }
        return view;
    }

    private void initView(final View view) {
        /*Find view by id*/
        commentRootLayout = view.findViewById(R.id.comment_root_layout);
        ImageView commentPost = view.findViewById(R.id.comment_post);
        commentStatus = view.findViewById(R.id.comment_status);
        etCommentBox = view.findViewById(R.id.et_comment_box);
        RecyclerView commentRecyclerView = view.findViewById(R.id.comment_recycler_view);
        /*set UI*/
        etCommentBox.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                etCommentBox.setFocusable(true);
                etCommentBox.setFocusableInTouchMode(true);
                return false;
            }
        });

        etCommentBox.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        etCommentBox.setLongClickable(false);

        etCommentBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etCommentBox.getText().length() <= 1000 && getContext() != null) {
//                    tvWordCounter.setText(getString(R.string.character_counter_text, etCommentBox.getText().length()));
                } else {
                    CommentUtils.showCustomToast(getContext(), "Limit Over");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        /*set Click Listener*/
        commentPost.setOnClickListener(this);

        /*set Adapter*/
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        commentRecyclerView.setNestedScrollingEnabled(false);
        commentRecyclerView.setLayoutManager(linearLayoutManager);
        commentReplyAdapter = new CommentReplyAdapter(getActivity(), false, this, commentActionHandler, storyId, storyTitle, storyUrl, host, isAgree, booleanExpressionTable);
        commentRecyclerView.setAdapter(commentReplyAdapter);
    }

    private void expandCommentView() {
        if (isCommentCollapsed && view != null) {
            isCommentCollapsed = false;
            CommentUtils.expand(commentRootLayout);
            commentStatus.setImageResource(R.drawable.cmt_ic_collapse);
        }
    }

    public boolean isCommentExpanded() {
        return !isCommentCollapsed;
    }

    private void getAllArticleComments(String channelId, int postId, int size, String dateTime, final boolean isLoadMore) {
        String finalUrl = String.format(RETRIEVE_ALL_COMMENTS, channelId, postId);
        if (isLoadMore)
            finalUrl = finalUrl + size + "/" + dateTime + "/";
        Systr.println("URL = " + finalUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (!TextUtils.isEmpty(RequestHandler.decryptString(response))) {
                        parseArticleComments(new JSONObject(RequestHandler.decryptString(response)), isLoadMore);
                    }
                } catch (Exception ignored) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Activity activity = getActivity();
                if (activity != null && isAdded()) {
                    if (error instanceof NetworkError) {
                        CommentUtils.showCustomToast(activity, getResources().getString(R.string.no_network_error));
                    } else {
                        CommentUtils.showCustomToast(activity, getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommentConstant.USER_AGENT);
                headers.put("encrypt", "1");
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    private void parseArticleComments(JSONObject response, boolean isLoadMore) throws JSONException {
        if (!isLoadMore) {
            totalComments = response.getInt("totalParentComments");
            if (totalComments > 0 && getContext() != null) {
                if (commentActionHandler != null) {
                    if (totalComments > 1) {
                        commentActionHandler.updateHeaderTitle(getString(R.string.comments_heading_with_count, CommentUtils.getSIPrefixForCount(totalComments)));
                    } else {
                        commentActionHandler.updateHeaderTitle(getString(R.string.commen_heading_with_count, CommentUtils.getSIPrefixForCount(totalComments)));
                    }
                }

            }
        } else {
            if (loadMoreStatus == POST_MODE) {
                loadMoreStatus = IDLE_MODE;
            }
        }

        JSONArray jsonArray = response.getJSONArray("comments");
        if (articleCommentInfoList == null) {
            articleCommentInfoList = new ArrayList<>();
        } else {
            articleCommentInfoList.clear();
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<ArticleCommentInfo>>() {
        }.getType();
        List<ArticleCommentInfo> tempList = gson.fromJson(jsonArray.toString(), type);
        articleCommentInfoList.addAll(tempList);
        commentReplyAdapter.setData(articleCommentInfoList, loadMoreCount, totalComments);
        commentReplyAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.comment_post) {
            isCommentLogin = LoginController.loginController().isUserLoggedIn();
            if (isCommentLogin) {
                postComment();
            } else {
                if (commentActionHandler != null) commentActionHandler.onLoginClick();
            }
        }
    }

    private void postComment() {
        if (postStatus == POST_MODE) {
            if (getContext() != null)
                CommentUtils.showCustomToast(getContext(), getContext().getString(R.string.comment_post_error));
        } else {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            if (!TextUtils.isEmpty(etCommentBox.getText().toString().trim())) {
                if (!isCommentLogin && commentActionHandler != null) {
                    commentActionHandler.userIngestWithoutLogin("", "", CommentConstant.API_ACTION_TYPE.API_USER_INGEST, this);
                }
                postStatus = POST_MODE;
                RequestHandler.postComment(getContext(), postId, storyId, storyTitle, etCommentBox.getText().toString().trim(), channelSlno, storyUrl, false, host, isAgree, CommentConstant.API_ACTION_TYPE.API_COMMENT_POST, this, articleCommentInfo);
            } else {
//                if (getContext() != null)
//                    CommentUtils.showCustomToast(getContext(), getContext().getString(R.string.comment_error));
            }
        }
    }

    @Override
    public void onSuccess(int actionType) {
        switch (actionType) {
            case CommentConstant.API_ACTION_TYPE.API_USER_INGEST:
                isCommentLogin = CommentPreference.getInstance(getContext()).getBooleanValue(CommentConstant.CommentPref.IS_COMMENT_LOG_IN, false);
                postComment();
                break;

            case CommentConstant.API_ACTION_TYPE.API_COMMENT_POST:
                if (postStatus == POST_MODE) {
                    try {
                        CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.COMMENTS, CTConstant.PROP_NAME.NEWS_URL, storyUrl, LoginController.getUserDataMap());
                    } catch (Exception ignore) {
                    }

                    postStatus = IDLE_MODE;
                    if (articleCommentInfoList == null)
                        articleCommentInfoList = new ArrayList<>();
                    articleCommentInfoList.add(0, articleCommentInfo);
                    commentReplyAdapter.setData(articleCommentInfoList, loadMoreCount, totalComments);
                    commentReplyAdapter.notifyDataSetChanged();
                    etCommentBox.setText("");
                    articleCommentInfo = new ArticleCommentInfo();
                    totalComments++;
                    if (totalComments > 0) {
                        if (commentActionHandler != null) {
                            if (totalComments > 1) {
                                commentActionHandler.updateHeaderTitle(getString(R.string.comments_heading_with_count, CommentUtils.getSIPrefixForCount(totalComments)));
                            } else {
                                commentActionHandler.updateHeaderTitle(getString(R.string.commen_heading_with_count, CommentUtils.getSIPrefixForCount(totalComments)));
                            }
                        }

                    }
                }
                break;

            case CommentConstant.API_ACTION_TYPE.API_LIKE:
                if (likeStatus == POST_MODE) {
                    likeStatus = IDLE_MODE;
                    booleanExpressionTable.updateBooleanValue(commentId, CommentConstant.BooleanConst.ARTICLE_COMMENT_LIKE, true);
                    if (articleCommentInfoList != null && commentReplyAdapter != null && !TextUtils.isEmpty(commentId)) {
                        commentReplyAdapter.notifyItemChanged(articleCommentInfoList.indexOf(new ArticleCommentInfo(commentId)));
                    }
                }
                break;

            case CommentConstant.API_ACTION_TYPE.API_REPORT:
                if (reportStatus == POST_MODE) {
                    reportStatus = IDLE_MODE;
                    booleanExpressionTable.updateBooleanValue(commentId, CommentConstant.BooleanConst.ARTICLE_COMMENT_REPORT_ABUSE, true);
                    if (commentReplyAdapter != null && articleCommentInfoList != null) {
                        commentReplyAdapter.notifyItemChanged(articleCommentInfoList.indexOf(new ArticleCommentInfo(commentId)));
                    }
                }
                break;

            case CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT:
                if (deleteStatus == POST_MODE) {
                    deleteStatus = IDLE_MODE;
                    totalComments--;
                    if (totalComments > 0) {
                        if (commentActionHandler != null) {
                            if (totalComments > 1) {
                                commentActionHandler.updateHeaderTitle(getString(R.string.comments_heading_with_count, CommentUtils.getSIPrefixForCount(totalComments)));
                            } else {
                                commentActionHandler.updateHeaderTitle(getString(R.string.commen_heading_with_count, CommentUtils.getSIPrefixForCount(totalComments)));
                            }
                        }
                    }
                    if (commentReplyAdapter != null && articleCommentInfoList != null) {
                        int index = articleCommentInfoList.indexOf(new ArticleCommentInfo(commentId));
                        articleCommentInfoList.remove(index);
                        commentReplyAdapter.deleteComment(index);
                    }
                }
                break;
        }
    }

    @Override
    public void onFailure(int actionType) {
        switch (actionType) {
            case CommentConstant.API_ACTION_TYPE.API_USER_INGEST:
                break;
            case CommentConstant.API_ACTION_TYPE.API_COMMENT_POST:
                postStatus = IDLE_MODE;
                break;
            case CommentConstant.API_ACTION_TYPE.API_LIKE:
                likeStatus = IDLE_MODE;
                break;
            case CommentConstant.API_ACTION_TYPE.API_REPORT:
                reportStatus = IDLE_MODE;
                break;
            case CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT:
                deleteStatus = IDLE_MODE;
                break;
        }
    }

    @Override
    public void onLoadMore(String dateTime) {
        int total = (articleCommentInfoList != null) ? totalComments - articleCommentInfoList.size() : 0;
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        CommentLoadMoreActivity commentLoadMoreFragmentDialog = CommentLoadMoreActivity.newInstance(channelSlno, postId, loadMoreCount, dateTime, totalComments, storyId, storyTitle, storyUrl, host, isNightMode);
        commentLoadMoreFragmentDialog.registerListener(commentActionHandler);
        commentLoadMoreFragmentDialog.registerBackListener(ArticleCommentFragment.this);
        commentLoadMoreFragmentDialog.setOldCommentsFirst(articleCommentInfoList);
        transaction.addToBackStack("commentLoadMore");
        commentLoadMoreFragmentDialog.show(transaction, "commentLoadMore");
    }

    @Override
    public void actionClick(int apiLike, int position) {
        if (articleCommentInfoList != null && articleCommentInfoList.size() > position && commentReplyAdapter != null) {
            commentId = articleCommentInfoList.get(position).id;
            switch (apiLike) {
                case CommentConstant.API_ACTION_TYPE.API_LIKE:
                    if (!TextUtils.isEmpty(commentId) && likeStatus == IDLE_MODE) {
                        likeStatus = POST_MODE;
                        articleCommentInfoList.get(position).likeCount++;
                        articleCommentInfoList.get(position).isLiked = true;
                        commentReplyAdapter.notifyItemChanged(position);
                        RequestHandler.likeReportCount(getContext(), articleCommentInfoList.get(position).postId, commentId, channelSlno, CommentConstant.LikeReportActionType.LIKE_ACTION, CommentConstant.API_ACTION_TYPE.API_LIKE, this);
                    }
                    break;
                case CommentConstant.API_ACTION_TYPE.API_REPORT:
                    if (!TextUtils.isEmpty(commentId) && reportStatus == IDLE_MODE) {
                        reportStatus = POST_MODE;
                        articleCommentInfoList.get(position).reportAbuseCount++;
                        articleCommentInfoList.get(position).isReported = true;
                        commentReplyAdapter.notifyItemChanged(position);
                        RequestHandler.likeReportCount(getContext(), articleCommentInfoList.get(position).postId, commentId, channelSlno, CommentConstant.LikeReportActionType.REPORT_ACTION, CommentConstant.API_ACTION_TYPE.API_REPORT, this);
                    }
                    break;
                case CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT:
                    if (deleteStatus == IDLE_MODE) {
                        deleteStatus = POST_MODE;
                        String userId = CommentPreference.getInstance(getContext()).getStringValue(CommentConstant.CommentPref.USER_ID, "");
                        String sessionId = CommentPreference.getInstance(getContext()).getStringValue(CommentConstant.CommentPref.SESSION_ID, "");
                        RequestHandler.deleteComment(getContext(), articleCommentInfoList.get(position).postId, commentId, userId, sessionId, CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT, this);
                    }
                    break;
            }
        }
    }


    public void refreshFragment(boolean isLogin) {
        if (commentActionHandler != null) {
            commentActionHandler.userIngestAfterLogin(CommentConstant.API_ACTION_TYPE.API_USER_INGEST, this);
        }
        isCommentLogin = isLogin;//CommentPreference.getInstance(mContext).getBooleanValue(CommentConstant.CommentPref.IS_COMMENT_LOG_IN, false);
        if (isCommentLogin && commentReplyAdapter != null)
            commentReplyAdapter.notifyDataSetChanged();

        if (isCommentLogin) {
            postComment();
        }
    }

    public void updateReplyCount(String commentId, int replyCount) {
        if (articleCommentInfoList.contains(new ArticleCommentInfo(commentId))) {
            int itemPosition = articleCommentInfoList.indexOf(new ArticleCommentInfo(commentId));
            int totalReplyCount = articleCommentInfoList.get(itemPosition).replyCount + replyCount;
            articleCommentInfoList.get(itemPosition).replyCount = totalReplyCount;
            commentReplyAdapter.notifyItemChanged(itemPosition);
        }
    }

    public void setDayNightMode(boolean isNightMode) {
        if (getContext() != null && view != null) {
            this.isNightMode = isNightMode;
            if (this.isNightMode) {
                view.findViewById(R.id.ll_main_layout).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.black));
                ((TextView) view.findViewById(R.id.tv_commment_head)).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.cmt_head_bg_night_color));
                ((TextView) view.findViewById(R.id.tv_commment_head)).setTextColor(ContextCompat.getColor(getContext(), R.color.cmt_head_text_night_color));
            } else {
                view.findViewById(R.id.ll_main_layout).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
                ((TextView) view.findViewById(R.id.tv_commment_head)).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.cmt_head_bg_day_color));
                ((TextView) view.findViewById(R.id.tv_commment_head)).setTextColor(ContextCompat.getColor(getContext(), R.color.cmt_head_text_day_color));
            }
        }
        if (commentActionHandler != null) {
            commentReplyAdapter.setDayNightMode(isNightMode);
            commentReplyAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void hideKeyBoard() {
        if (getView() != null && getActivity() != null) {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

    @Override
    public void getComment() {
        if (postId != 0)
            getAllArticleComments(channelSlno, postId, loadMoreCount, "", false);
    }
}
