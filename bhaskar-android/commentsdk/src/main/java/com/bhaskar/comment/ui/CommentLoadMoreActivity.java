package com.bhaskar.comment.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.comment.R;
import com.bhaskar.comment.adapter.CommentReplyAdapter;
import com.bhaskar.comment.util.BooleanExpressionTable;
import com.bhaskar.comment.util.CommentUtils;
import com.bhaskar.comment.util.DatabaseHelper;
import com.bhaskar.comment.handler.ArticleCommentCallback;
import com.bhaskar.comment.handler.CommentActionHandler;
import com.bhaskar.comment.handler.CommentBackHandler;
import com.bhaskar.comment.handler.RequestHandler;
import com.bhaskar.comment.model.ArticleCommentInfo;
import com.bhaskar.comment.util.CommentConstant;
import com.bhaskar.comment.util.CommentPreference;
import com.db.util.Systr;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.db.util.Urls.RETRIEVE_ALL_COMMENTS;

public class CommentLoadMoreActivity extends DialogFragment implements ArticleCommentCallback {

    CommentActionHandler commentActionHandler;
    private int IDLE_MODE = 0;
    private int POST_MODE = 1;
    private int postStatus = IDLE_MODE;
    private int likeStatus = IDLE_MODE;
    private int reportStatus = IDLE_MODE;
    private int deleteStatus = IDLE_MODE;
    private int loadMoreStatus = IDLE_MODE;
    private ArticleCommentInfo articleCommentInfo = new ArticleCommentInfo();
    private RecyclerView commentRecyclerView;
    private CommentReplyAdapter commentReplyAdapter;
    private String channelSlno, dateTime, storyId, storyTitle, storyUrl, host;
    private int postId, loadMoreCount, totalComments;
    private List<ArticleCommentInfo> articleCommentInfoList;
    private List<ArticleCommentInfo> oldArticleCommentInfoList;
    private BooleanExpressionTable booleanExpressionTable;
    private boolean isNightMode;
    private String commentId;
    private boolean isCommentLogin;
    private EditText etCommentBox;
    private ImageView commentPost;
    private Toolbar toolbar;
    private View rootView;
    private ImageView back;
    private List<ArticleCommentInfo> oldCommentsFirst;
    private CommentBackHandler commentBackHandler;

    public static CommentLoadMoreActivity newInstance(String channelSlno, int postId, int loadMoreCount, String dateTime, int total, String storyId, String storyTitle, String storyUrl, String host, boolean isNightMode) {
        CommentLoadMoreActivity f = new CommentLoadMoreActivity();
        Bundle loadMoreIntent = new Bundle();
        loadMoreIntent.putString("channelId", channelSlno);
        loadMoreIntent.putInt("postId", postId);
        loadMoreIntent.putInt("loadMoreCount", loadMoreCount);
        loadMoreIntent.putString("dateTime", dateTime);

        loadMoreIntent.putInt("totalComments", total);
        loadMoreIntent.putString("storyId", storyId);
        loadMoreIntent.putString("storyTitle", storyTitle);
        loadMoreIntent.putString("storyUrl", storyUrl);
        loadMoreIntent.putString("host", host);
        loadMoreIntent.putBoolean("isNightMode", isNightMode);

        f.setArguments(loadMoreIntent);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, com.bhaskar.R.style.AppTheme);

        /*Tool Bar*/
        Bundle extras = getArguments();
        if (extras != null) {
            channelSlno = extras.getString("channelId");
            postId = extras.getInt("postId");
            dateTime = extras.getString("dateTime");
            loadMoreCount = extras.getInt("loadMoreCount");
            totalComments = extras.getInt("totalComments");
            storyId = extras.getString("storyId");
            storyTitle = extras.getString("storyTitle");
            storyUrl = extras.getString("storyUrl");
            host = extras.getString("host");
            isNightMode = extras.getBoolean("isNightMode");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_comment_load_more, container, false);
        commentRecyclerView = (RecyclerView) rootView.findViewById(R.id.comment_recycler_view);
        etCommentBox = (EditText) rootView.findViewById(R.id.et_comment_box);
        commentPost = (ImageView) rootView.findViewById(R.id.post_reply);

        back = (ImageView) rootView.findViewById(R.id.back);
        //back.setColorFilter(CommentUtils.fetchAttributeColor(getActivity(), R.attr.toolbarIconColor), PorterDuff.Mode.SRC_IN);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
                if (commentBackHandler != null) {
                    commentBackHandler.getComment();
                }
            }
        });
        commentPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCommentLogin) {
                    postComment();
                } else {
                    if (commentActionHandler != null) {
                        commentActionHandler.onLoginClick();
                    }
                }
            }
        });
        etCommentBox.setLongClickable(false);
        etCommentBox.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        isCommentLogin = CommentPreference.getInstance(getActivity()).getBooleanValue(CommentConstant.CommentPref.IS_COMMENT_LOG_IN, false);
        booleanExpressionTable = (BooleanExpressionTable) DatabaseHelper.getInstance(getActivity()).getTableObject(BooleanExpressionTable.TABLE_NAME);
        /*set Adapter*/
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        commentRecyclerView.setNestedScrollingEnabled(false);
        commentRecyclerView.setLayoutManager(linearLayoutManager);
        commentReplyAdapter = new CommentReplyAdapter(getActivity(), false, this, commentActionHandler, storyId, storyTitle, storyUrl, host, true, booleanExpressionTable);

        commentRecyclerView.setAdapter(commentReplyAdapter);

        //set old comments
        commentReplyAdapter.setData(articleCommentInfoList, loadMoreCount, totalComments);

        setDayNightMode(isNightMode);
        getAllArticleComments(channelSlno, postId, loadMoreCount, dateTime, true);
        return rootView;
    }

    public void getAllArticleComments(String channelId, int postId, int size, String dateTime, final boolean isLoadMore) {
        String finalUrl = String.format(RETRIEVE_ALL_COMMENTS, channelId, postId);
        if (isLoadMore)
            finalUrl = finalUrl + size + "/" + dateTime + "/";
        Systr.println("URL = " + finalUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (!TextUtils.isEmpty(response)) {
                        parseArticleComments(new JSONObject(RequestHandler.decryptString(response)), isLoadMore);
                    }
                } catch (Exception ignored) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Activity activity = getActivity();
                if (activity != null && isAdded()) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        CommentUtils.showCustomToast(activity, getResources().getString(R.string.no_network_error));
                    } else {
                        CommentUtils.showCustomToast(activity, getResources().getString(R.string.sorry_error_found_please_try_again_));
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommentConstant.USER_AGENT);
                headers.put("encrypt", "1");
//                headers.put("Content-Type", "application/json charset=utf-8");
//                headers.put("Accept", "application/json");
                return headers;
            }
        };
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
//                finalUrl,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (!TextUtils.isEmpty(response.toString())) {
//                                parseArticleComments(response, isLoadMore);
//                            }
//                        } catch (Exception ignored) {
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Activity activity = getActivity();
//                if (activity != null && isAdded()) {
//                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
//                        Toast.makeText(activity, getResources().getString(R.string.no_network_error), Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(activity, getResources().getString(R.string.sorry_error_found_please_try_again_), Toast.LENGTH_LONG).show();
//                    }
//                }
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("api-key", "bh@@$k@r-D");
//                headers.put("User-Agent", CommentConstant.USER_AGENT);
//                headers.put("encrypt", "0");
//                headers.put("Content-Type", "application/json charset=utf-8");
//                headers.put("Accept", "application/json");
//                return headers;
//            }
//        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

    private void parseArticleComments(JSONObject response, boolean isLoadMore) throws JSONException {

        if (loadMoreStatus == POST_MODE) {
            loadMoreStatus = IDLE_MODE;
        }

        JSONArray jsonArray = response.getJSONArray("comments");

        List<ArticleCommentInfo> tempList = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<ArticleCommentInfo>>() {
        }.getType());
        if (tempList.size() == 0) {
            commentReplyAdapter.removeLoadMoreItem();
        } else {
            articleCommentInfoList.addAll(tempList);
            commentReplyAdapter.setData(articleCommentInfoList, loadMoreCount, totalComments);
        }
        commentReplyAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccess(int actionType) {
        switch (actionType) {
            case CommentConstant.API_ACTION_TYPE.API_USER_INGEST:
                isCommentLogin = CommentPreference.getInstance(getActivity()).getBooleanValue(CommentConstant.CommentPref.IS_COMMENT_LOG_IN, false);
                break;
            case CommentConstant.API_ACTION_TYPE.API_COMMENT_POST:
                if (postStatus == POST_MODE) {
                    postStatus = IDLE_MODE;
                    articleCommentInfoList.add(0, articleCommentInfo);
                    commentReplyAdapter.setData(articleCommentInfoList, loadMoreCount, totalComments);
                    commentReplyAdapter.notifyDataSetChanged();
                    etCommentBox.setText("");
                    articleCommentInfo = new ArticleCommentInfo();
                    totalComments++;
                }
                break;
            case CommentConstant.API_ACTION_TYPE.API_LIKE:
                if (likeStatus == POST_MODE) {
                    likeStatus = IDLE_MODE;
                    booleanExpressionTable.updateBooleanValue(commentId, CommentConstant.BooleanConst.ARTICLE_COMMENT_LIKE, true);
                    if (articleCommentInfoList != null && commentReplyAdapter != null && !TextUtils.isEmpty(commentId)) {
                        commentReplyAdapter.notifyItemChanged(articleCommentInfoList.indexOf(new ArticleCommentInfo(commentId)));
                    }
                }
                break;
            case CommentConstant.API_ACTION_TYPE.API_REPORT:
                if (reportStatus == POST_MODE) {
                    reportStatus = IDLE_MODE;
                    booleanExpressionTable.updateBooleanValue(commentId, CommentConstant.BooleanConst.ARTICLE_COMMENT_REPORT_ABUSE, true);
                    if (commentReplyAdapter != null && articleCommentInfoList != null) {
                        commentReplyAdapter.notifyItemChanged(articleCommentInfoList.indexOf(new ArticleCommentInfo(commentId)));
                    }
                }
                break;
            case CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT:
                if (deleteStatus == POST_MODE) {
                    deleteStatus = IDLE_MODE;
                    if (commentReplyAdapter != null && articleCommentInfoList != null) {
                        int index = articleCommentInfoList.indexOf(new ArticleCommentInfo(commentId));
                        articleCommentInfoList.remove(index);
                        oldArticleCommentInfoList.remove(index);
                        commentReplyAdapter.deleteComment(index);
                    }
                }
                break;
        }
    }

    @Override
    public void onFailure(int actionType) {
        switch (actionType) {
            case CommentConstant.API_ACTION_TYPE.API_USER_INGEST:
                break;
            case CommentConstant.API_ACTION_TYPE.API_COMMENT_POST:
                postStatus = IDLE_MODE;
                break;
            case CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT:
                deleteStatus = IDLE_MODE;
                break;
        }
    }

    @Override
    public void onLoadMore(String dateTime) {
        if (loadMoreStatus == IDLE_MODE) {
            loadMoreStatus = POST_MODE;
            getAllArticleComments(channelSlno, postId, loadMoreCount, dateTime, true);
        }
    }

    @Override
    public void actionClick(int apiLike, int position) {
        if (commentReplyAdapter != null && articleCommentInfoList != null && articleCommentInfoList.size() > position) {
            commentId = articleCommentInfoList.get(position).id;
            switch (apiLike) {
                case CommentConstant.API_ACTION_TYPE.API_LIKE:
                    if (!TextUtils.isEmpty(commentId) && likeStatus == IDLE_MODE) {
                        likeStatus = POST_MODE;
                        articleCommentInfoList.get(position).likeCount++;
                        articleCommentInfoList.get(position).isLiked = true;
                        commentReplyAdapter.notifyItemChanged(position);
                        RequestHandler.likeReportCount(getActivity(), articleCommentInfoList.get(position).postId, commentId, channelSlno, CommentConstant.LikeReportActionType.LIKE_ACTION, CommentConstant.API_ACTION_TYPE.API_LIKE, this);
                    }
                    break;
                case CommentConstant.API_ACTION_TYPE.API_REPORT:
                    if (reportStatus == IDLE_MODE) {
                        reportStatus = POST_MODE;
                        articleCommentInfoList.get(position).reportAbuseCount++;
                        articleCommentInfoList.get(position).isReported = true;
                        commentReplyAdapter.notifyItemChanged(position);
                        RequestHandler.likeReportCount(getActivity(), articleCommentInfoList.get(position).postId, commentId, channelSlno, CommentConstant.LikeReportActionType.REPORT_ACTION, CommentConstant.API_ACTION_TYPE.API_REPORT, this);
                    }
                    break;
                case CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT:
                    if (deleteStatus == IDLE_MODE) {
                        deleteStatus = POST_MODE;
                        String userId = CommentPreference.getInstance(getContext()).getStringValue(CommentConstant.CommentPref.USER_ID, "");
                        String sessionId = CommentPreference.getInstance(getContext()).getStringValue(CommentConstant.CommentPref.SESSION_ID, "");
                        RequestHandler.deleteComment(getActivity(), articleCommentInfoList.get(position).postId, commentId, userId, sessionId, CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT, this);
                    }
                    break;
            }
        }
    }

    public void setDayNightMode(boolean isNightMode) {
        this.isNightMode = isNightMode;
        if (this.isNightMode) {
            rootView.findViewById(R.id.ll_main_layout).setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.black));
        } else {
            rootView.findViewById(R.id.ll_main_layout).setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
        }
        commentReplyAdapter.setDayNightMode(isNightMode);
        commentReplyAdapter.notifyDataSetChanged();
    }

    private void postComment() {
        if (postStatus == POST_MODE) {
            if (getActivity() != null)
                CommentUtils.showCustomToast(getActivity(), getActivity().getString(R.string.comment_post_error));
        } else {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            if (!TextUtils.isEmpty(etCommentBox.getText().toString().trim())) {
                if (!isCommentLogin && commentActionHandler != null) {
                    commentActionHandler.userIngestWithoutLogin("", "", CommentConstant.API_ACTION_TYPE.API_USER_INGEST, this);
                }
                postStatus = POST_MODE;
                RequestHandler.postComment(getActivity(), postId, storyId, storyTitle, etCommentBox.getText().toString().trim(), channelSlno, storyUrl, false, host, true, CommentConstant.API_ACTION_TYPE.API_COMMENT_POST, this, articleCommentInfo);
            } else {
                if (getActivity() != null)
                    CommentUtils.showCustomToast(getActivity(), getActivity().getString(R.string.comment_error));
            }
        }
    }

    public void registerListener(CommentActionHandler listener) {
        this.commentActionHandler = listener;
    }

    public void setOldCommentsFirst(List<ArticleCommentInfo> oldCommentsFirst) {
        if (articleCommentInfoList == null) {
            articleCommentInfoList = new ArrayList<>();
        }
        oldArticleCommentInfoList = oldCommentsFirst;
        articleCommentInfoList.addAll(oldCommentsFirst);
    }

    public void registerBackListener(CommentBackHandler commentBackHandler) {
        this.commentBackHandler = commentBackHandler;
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(android.content.DialogInterface dialog, int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    getDialog().dismiss();
                    if (commentBackHandler != null) {
                        commentBackHandler.getComment();
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });
    }
}
