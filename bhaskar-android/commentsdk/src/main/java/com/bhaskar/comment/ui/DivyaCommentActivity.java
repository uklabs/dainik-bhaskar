package com.bhaskar.comment.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.comment.R;
import com.bhaskar.comment.handler.ArticleCommentCallback;
import com.bhaskar.comment.handler.CommentActionHandler;
import com.bhaskar.comment.handler.RequestHandler;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.CommonConstants;
import com.bhaskar.util.LoginController;
import com.db.util.AppPreferences;
import com.bhaskar.util.AppUrls;
import com.db.util.AppUtils;
import com.bhaskar.util.CommonConstants;
import com.db.util.OpenNewsLinkInApp;
import com.db.util.QuickPreferences;

public class DivyaCommentActivity extends BaseActivity implements CommentActionHandler {

    private String storyUrl;

    private String channelSlno;
    private String host;
    private String storyId;
    private String storyTitle;
    private boolean isLogin;
    private int loadMoreCount;
    private String themeColor;
    private TextView tvTitle;


    public static Intent getIntent(Context context, boolean isLogin, String storyUrl, String channelSlno, int size, String host, String storyId, String title, String color) {
        Intent intent = new Intent(context, DivyaCommentActivity.class);
        intent.putExtra("is_login", isLogin);
        intent.putExtra("story_url", storyUrl);
        intent.putExtra("channel_slno", channelSlno);
        intent.putExtra("loadMoreCount", size);
        intent.putExtra("host", host);
        intent.putExtra("story_id", storyId);
        intent.putExtra("story_title", title);
        intent.putExtra("color", color);
        return intent;
    }

    /**
     * @param fragmentManager
     * @param id
     * @param fragment
     * @param saveInBackstack
     * @param animate
     */
    public static void changeFragment(FragmentManager fragmentManager, int id, Fragment fragment, boolean saveInBackstack, boolean animate) {
        String backStateName = fragment.getClass().getName();
        try {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (animate) {
                transaction.setCustomAnimations(com.bhaskar.R.anim.enter_from_right, com.bhaskar.R.anim.exit_to_left, com.bhaskar.R.anim.enter_from_left, com.bhaskar.R.anim.exit_to_right);
            }
            transaction.replace(id, fragment, backStateName);
            if (saveInBackstack) {
                transaction.addToBackStack(backStateName);
            }

            transaction.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_root_container);
        handleExtras();
        handleToolbar();
        AppUtils.getInstance().setStatusBarColor(this, themeColor);
        ArticleCommentFragment articleCommentFragment = ArticleCommentFragment.getInstance(isLogin, storyUrl, CommonConstants.CHANNEL_ID, loadMoreCount, host, storyId, storyTitle, false);
        //articleCommentFragment.setExpandableModeOn(false);
        changeFragment(getSupportFragmentManager(), R.id.rootContainer, articleCommentFragment, false, false);
    }

    private void handleExtras() {
        if (getIntent() != null) {
            storyUrl = getIntent().getStringExtra("story_url");
            isLogin = getIntent().getBooleanExtra("is_login", false);
            channelSlno = getIntent().getStringExtra("channel_slno");
            loadMoreCount = getIntent().getIntExtra("loadMoreCount", 0);
            host = getIntent().getStringExtra("host");
            storyId = getIntent().getStringExtra("story_id");
            storyTitle = getIntent().getStringExtra("story_title");
            themeColor = getIntent().getStringExtra("color");
        }
    }

    private void handleToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(AppUtils.getThemeColor(this, themeColor));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(com.bhaskar.R.drawable.ica_back);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        tvTitle = toolbar.findViewById(R.id.toolbar_title);
        tvTitle.setText("Comment (0)");
//        tvTitle.setText(storyTitle);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ArticleCommentFragment articleCommentFragment = (ArticleCommentFragment) getSupportFragmentManager().findFragmentByTag(ArticleCommentFragment.class.getName());
        if (requestCode == LoginController.LOGIN_POPUP_REQUEST_CODE) {
            boolean isLogin = LoginController.loginController().isUserLoggedIn();
            if (articleCommentFragment != null) {
                articleCommentFragment.refreshFragment(isLogin);
            }
        } else if (requestCode == ReplyActivity.REPLY_REQUEST_CODE && resultCode == ReplyActivity.REPLY_RESULT_CODE) {
            if (data != null) {
                int replyCount = data.getIntExtra("ReplyCount", 0);
                String commentId = data.getStringExtra("CommentId");
                if (articleCommentFragment != null) {
                    articleCommentFragment.updateReplyCount(commentId, replyCount);
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onLoginClick() {
        ArticleCommentFragment articleCommentFragment = (ArticleCommentFragment) getSupportFragmentManager().findFragmentByTag(ArticleCommentFragment.class.getName());
        boolean isUserLoginIn = LoginController.loginController().isUserLoggedIn();
        if (!isUserLoginIn) {
            LoginController.loginController().getLoginIntent(DivyaCommentActivity.this, TrackingData.getDBId(this), TrackingData.getDeviceId(this), LoginController.LOGIN_POPUP_REQUEST_CODE);
        } else {
            if (articleCommentFragment != null) {
                articleCommentFragment.refreshFragment(isUserLoginIn);
            }
        }
    }

    @Override
    public void userIngestAfterLogin(int apiAction, ArticleCommentCallback articleCommentCallback) {
        String dbId = TrackingData.getDBId(this);
        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
        if (profileInfo == null)
            return;
        String signupId = profileInfo.uID;
        String name = profileInfo.name;
        String email = profileInfo.email;
        String profile_pic = profileInfo.profilePic;
        String city = profileInfo.city;
        String gender = profileInfo.gender;
        String host = AppPreferences.getInstance(this).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
        String accountType = profileInfo.loginType;
        /*User Ingest Request*/
        RequestHandler.postUserData(this, accountType, "", city, "India", email, gender, "", host, "", signupId, dbId, profile_pic, "", name, "", apiAction, articleCommentCallback);

    }

    @Override
    public void userIngestWithoutLogin(String userName, String userEmail, int apiAction, ArticleCommentCallback articleCommentCallback) {
        String dbId = TrackingData.getDBId(this);
        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
        String city = (profileInfo == null) ? "" : profileInfo.city;
        String gender = (profileInfo == null) ? "" : profileInfo.gender;
        String host = AppPreferences.getInstance(this).getStringValue(QuickPreferences.WISDOM_DOMAIN, AppUrls.DOMAIN);
        /*User Ingest Request*/
        RequestHandler.postUserData(this, "email", "", city, "India", userEmail, gender, "", host, "", userEmail, dbId, "", "", userName, "", apiAction, articleCommentCallback);

    }

    @Override
    public void onExpand() {

    }

    @Override
    public void openLink(String link) {
        OpenNewsLinkInApp openNewsLinkInApp = new OpenNewsLinkInApp(DivyaCommentActivity.this, link, "", 1, "", null);
        openNewsLinkInApp.openLink();
    }

    @Override
    public void updateHeaderTitle(String countTxt) {
        if (!TextUtils.isEmpty(countTxt)) {
            tvTitle.setText(countTxt);
        } else {
            tvTitle.setText("Comment (0)");
        }
    }


}
