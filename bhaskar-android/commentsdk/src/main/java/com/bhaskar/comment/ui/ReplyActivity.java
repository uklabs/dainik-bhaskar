package com.bhaskar.comment.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.comment.R;
import com.bhaskar.comment.adapter.CommentReplyAdapter;
import com.bhaskar.comment.handler.ArticleCommentCallback;
import com.bhaskar.comment.handler.RequestHandler;
import com.bhaskar.comment.model.ArticleCommentInfo;
import com.bhaskar.comment.util.BooleanExpressionTable;
import com.bhaskar.comment.util.CommentConstant;
import com.bhaskar.comment.util.CommentPreference;
import com.bhaskar.comment.util.CommentUtils;
import com.bhaskar.comment.util.DatabaseHelper;
import com.bhaskar.comment.util.ThemeUtil;
import com.bhaskar.comment.util.CommentUrls;
import com.bhaskar.util.LoginController;
import com.db.util.AppUtils;
import com.db.util.VolleyNetworkSingleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReplyActivity extends BaseActivity implements View.OnClickListener, ArticleCommentCallback {

    public static final int REPLY_REQUEST_CODE = 9090;
    public static final int REPLY_RESULT_CODE = 9191;
    int position;
    private EditText etCommentBox;
    private RecyclerView replyRecyclerView;
    private CommentReplyAdapter commentReplyAdapter;
    private ImageView postReply;
    private List<ArticleCommentInfo> articleCommentInfoList;
    private String channelSlno, commentId;
    private int postId, loadMoreCount;
    private boolean isCommentLogin;
    private ArticleCommentInfo articleCommentInfo = new ArticleCommentInfo();
    private String storyId, storyTitle, storyUrl, host;
    private boolean isAgree;
    private String inReplyUserId, inReplyCommentId;
    private String themeColor;
    private int IDLE_MODE = 0;
    private int POST_MODE = 1;
    private int postStatus = IDLE_MODE;
    private int deleteStatus = IDLE_MODE;
    private BooleanExpressionTable booleanExpressionTable;
    private int replyCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeUtil.getCurrentTheme(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply);

        /*get Bundle Extras*/
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("post_id"))
                try {
                    postId = Integer.parseInt(extras.getString("post_id"));
                } catch (NumberFormatException ignored) {
                }
            loadMoreCount = extras.getInt("load_more_size");
            channelSlno = extras.getString("channel_slno");
            commentId = extras.getString("comment_id");
            storyId = extras.getString("story_id");
            storyUrl = extras.getString("story_url");
            storyTitle = extras.getString("story_title");
            host = extras.getString("host");
            isAgree = extras.getBoolean("is_agree");
            inReplyUserId = extras.getString("in_reply_user_id");
            inReplyCommentId = extras.getString("in_reply_comment_id");
            themeColor = getIntent().getStringExtra("color");
        }

        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(AppUtils.getThemeColor(this, themeColor));
        toolbar.setTitle("Replies");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(v -> handleBack());

        booleanExpressionTable = (BooleanExpressionTable) DatabaseHelper.getInstance(ReplyActivity.this).getTableObject(BooleanExpressionTable.TABLE_NAME);
        isCommentLogin = CommentPreference.getInstance(ReplyActivity.this).getBooleanValue(CommentConstant.CommentPref.IS_COMMENT_LOG_IN, false);
        initViews();
        if (articleCommentInfoList == null)
            getAllRepliesOnComment(channelSlno, postId, commentId, loadMoreCount, "", false);
    }

    private void handleBack() {
        if (replyCount > 0) {
            Intent replyCountIntent = new Intent();
            replyCountIntent.putExtra("ReplyCount", replyCount);
            replyCountIntent.putExtra("CommentId", commentId);
            setResult(REPLY_RESULT_CODE, replyCountIntent);
        }
        finish();
//        overridePendingTransition(com.bhaskar.R.anim.slide_from_left, com.bhaskar.R.anim.slide_to_right);
    }

    private void initViews() {
        postReply = (ImageView) findViewById(R.id.post_reply);
        etCommentBox = (EditText) findViewById(R.id.et_comment_box);
        replyRecyclerView = (RecyclerView) findViewById(R.id.reply_recycler_view);

        postReply.setOnClickListener(this);
        etCommentBox.setLongClickable(false);
        etCommentBox.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        etCommentBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etCommentBox.getText().length() <= 1000) {
                } else {
                    CommentUtils.showCustomToast(ReplyActivity.this, "Limit Over");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        /*set Adapter*/
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ReplyActivity.this);
        replyRecyclerView.setNestedScrollingEnabled(false);
        replyRecyclerView.setLayoutManager(linearLayoutManager);
        commentReplyAdapter = new CommentReplyAdapter(ReplyActivity.this, true, this, null, storyId, storyTitle, storyUrl, host, isAgree, booleanExpressionTable);
        replyRecyclerView.setAdapter(commentReplyAdapter);
    }

    public void getAllRepliesOnComment(String channelSlno, int postId, String commentId, int size, String dateTime, boolean isLoadMore) {
        String finalUrl = String.format(CommentUrls.RETRIEVE_ALL_REPLIES, channelSlno, postId, commentId);
        if (isLoadMore)
            finalUrl = finalUrl + size + "/" + dateTime + "/";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (!TextUtils.isEmpty(response)) {
                        parseArticleComments(new JSONObject(RequestHandler.decryptString(response)));
                    }
                } catch (Exception ignored) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NoConnectionError || error instanceof NetworkError) {
                    CommentUtils.showCustomToast(ReplyActivity.this, getResources().getString(R.string.no_network_error));
                } else {
                    CommentUtils.showCustomToast(ReplyActivity.this, getResources().getString(R.string.sorry_error_found_please_try_again_));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", CommentConstant.USER_AGENT);
                headers.put("encrypt", "1");
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(ReplyActivity.this).addToRequestQueue(stringRequest);
    }

    private void parseArticleComments(JSONObject response) throws JSONException {
        JSONArray jsonArray = response.getJSONArray("comments");
        if (articleCommentInfoList == null) {
            articleCommentInfoList = new ArrayList<>();
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<ArticleCommentInfo>>() {
        }.getType();
        List<ArticleCommentInfo> tempList = gson.fromJson(jsonArray.toString(), type);
        articleCommentInfoList.addAll(tempList);
        commentReplyAdapter.setData(articleCommentInfoList, loadMoreCount, 0);
        commentReplyAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccess(int actionType) {
        switch (actionType) {
            case CommentConstant.API_ACTION_TYPE.API_REPLY_POST:
                if (postStatus == POST_MODE) {
                    replyCount++;
                    postStatus = IDLE_MODE;
                    articleCommentInfoList.add(0, articleCommentInfo);
                    commentReplyAdapter.setData(articleCommentInfoList, loadMoreCount, 0);
                    commentReplyAdapter.notifyDataSetChanged();
                    etCommentBox.setText("");
                    articleCommentInfo = new ArticleCommentInfo();
                }
                break;
            case CommentConstant.API_ACTION_TYPE.API_LIKE:
                if (articleCommentInfoList != null && commentReplyAdapter != null && !TextUtils.isEmpty(commentId)) {
                    booleanExpressionTable.updateBooleanValue(commentId, CommentConstant.BooleanConst.ARTICLE_COMMENT_LIKE, true);
                    commentReplyAdapter.notifyItemChanged(articleCommentInfoList.indexOf(new ArticleCommentInfo(commentId)));
                }
                break;
            case CommentConstant.API_ACTION_TYPE.API_REPORT:
                if (commentReplyAdapter != null && articleCommentInfoList != null) {
                    booleanExpressionTable.updateBooleanValue(commentId, CommentConstant.BooleanConst.ARTICLE_COMMENT_REPORT_ABUSE, true);
                    commentReplyAdapter.notifyItemChanged(articleCommentInfoList.indexOf(new ArticleCommentInfo(commentId)));
                }
                break;
            case CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT:
                if (deleteStatus == POST_MODE) {
                    deleteStatus = IDLE_MODE;
                    if (commentReplyAdapter != null && articleCommentInfoList != null) {
                        int index = articleCommentInfoList.indexOf(new ArticleCommentInfo(commentId));
                        articleCommentInfoList.remove(index);
                        commentReplyAdapter.deleteComment(index);
                    }
                }
                break;
        }
    }

    @Override
    public void onFailure(int actionType) {
        switch (actionType) {
            case CommentConstant.API_ACTION_TYPE.API_USER_INGEST:
                break;
            case CommentConstant.API_ACTION_TYPE.API_COMMENT_POST:
                postStatus = IDLE_MODE;
                break;
            case CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT:
                deleteStatus = IDLE_MODE;
                break;
        }
    }

    @Override
    public void onLoadMore(String dateTime) {
//        getAllArticleComments(channelSlno, postId, loadMoreCount, dateTime, true, false);
    }

    @Override
    public void actionClick(int apiLike, int position) {
        this.position = position;
        if (articleCommentInfoList != null && articleCommentInfoList.size() > position && commentReplyAdapter != null) {
            commentId = articleCommentInfoList.get(position).id;
            switch (apiLike) {
                case CommentConstant.API_ACTION_TYPE.API_LIKE:
                    if (!TextUtils.isEmpty(commentId)) {
                        articleCommentInfoList.get(position).likeCount++;
                        articleCommentInfoList.get(position).isLiked = true;
                        commentReplyAdapter.notifyItemChanged(position);
                    }
                    RequestHandler.likeReportCount(ReplyActivity.this, articleCommentInfoList.get(position).postId, commentId, channelSlno, CommentConstant.LikeReportActionType.LIKE_ACTION, CommentConstant.API_ACTION_TYPE.API_LIKE, this);
                    break;
                case CommentConstant.API_ACTION_TYPE.API_REPORT:
                    articleCommentInfoList.get(position).reportAbuseCount++;
                    articleCommentInfoList.get(position).isReported = true;
                    commentReplyAdapter.notifyItemChanged(position);
                    RequestHandler.likeReportCount(ReplyActivity.this, articleCommentInfoList.get(position).postId, commentId, channelSlno, CommentConstant.LikeReportActionType.REPORT_ACTION, CommentConstant.API_ACTION_TYPE.API_REPORT, this);
                    break;
                case CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT:
                    if (deleteStatus == IDLE_MODE) {
                        deleteStatus = POST_MODE;
                        String userId = CommentPreference.getInstance(ReplyActivity.this).getStringValue(CommentConstant.CommentPref.USER_ID, "");
                        String sessionId = CommentPreference.getInstance(ReplyActivity.this).getStringValue(CommentConstant.CommentPref.SESSION_ID, "");
                        RequestHandler.deleteComment(ReplyActivity.this, articleCommentInfoList.get(position).postId, commentId, userId, sessionId, CommentConstant.API_ACTION_TYPE.API_DELETE_COMMENT, this);
                    }
                    break;
            }
        }

    }

    private void postComment() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        if (!TextUtils.isEmpty(etCommentBox.getText().toString().trim())) {
            postStatus = POST_MODE;
            RequestHandler.postCommentReply(ReplyActivity.this, postId, storyId, storyTitle, etCommentBox.getText().toString().trim(), channelSlno, storyUrl, true, inReplyCommentId, inReplyUserId, host, true, CommentConstant.API_ACTION_TYPE.API_REPLY_POST, this, articleCommentInfo);
        } else {
            CommentUtils.showCustomToast(ReplyActivity.this, ReplyActivity.this.getString(R.string.comment_error));
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.post_reply) {
            isCommentLogin = LoginController.loginController().isUserLoggedIn();
            if (isCommentLogin && postStatus == IDLE_MODE) {
                postComment();
            }
        }
    }

    @Override
    public void onBackPressed() {
        handleBack();
    }
}
