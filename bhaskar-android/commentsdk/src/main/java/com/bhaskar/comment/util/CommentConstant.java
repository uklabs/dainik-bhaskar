package com.bhaskar.comment.util;

import androidx.annotation.IntDef;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import com.bhaskar.util.CommonConstants;
/**
 * Created by Lalit Goswami on 27-10-2017.
 */

public class CommentConstant {

    public static String USER_AGENT = CommonConstants.USER_AGENT;

    public interface CommentPref {
        String IS_COMMENT_LOG_IN = "isUserLoggedIn";
        String IS_BY_ANONYMOUS = "isByAnonymous";
        String ACCOUNT_TYPE = "accountType";
        String USER_ID = "userId";
        String SESSION_ID = "session_id";

        String USER_NAME = "userName";
        String USER_IMAGE = "userImage";
        String USER_GENDER = "userGender";
        String USER_EMAIL = "userEmail";
        String BIRTH_DATE = "birthDate";
        String USER_MOBILE = "userMobile";
        String USER_HOMETOWN = "homeTown";
        String USER_LOCATION = "location";
        String USER_CITY = "city";
        String USER_STATE = "state";
        String USER_COUNTRY = "country";
    }

    public interface LikeReportActionType {
        String LIKE_ACTION = "like";
        String REPORT_ACTION = "abuse";
        String DELETE_ACTION = "delete";
    }

    public interface API_ACTION_TYPE {
        int API_LIKE = 1;
        int API_REPORT = 2;
        int API_COMMENT_POST = 3;
        int API_REPLY_POST = 4;
        int API_USER_INGEST = 5;
        int API_DELETE_COMMENT = 6;
    }


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({BooleanConst.ARTICLE_COMMENT_LIKE, BooleanConst.ARTICLE_COMMENT_REPORT_ABUSE})
    public @interface BooleanConst {
        int ARTICLE_COMMENT_LIKE = 1;
        int ARTICLE_COMMENT_REPORT_ABUSE = 2;
    }
}
