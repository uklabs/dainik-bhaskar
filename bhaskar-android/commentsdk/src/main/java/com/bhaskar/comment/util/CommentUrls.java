package com.bhaskar.comment.util;

import static com.db.util.Urls.BASE_COMMENT_URL;

/**
 * Created by Lalit Goswami on 26-10-2017.
 */

public class CommentUrls {


    public static String COMMENT_USER_DATA_INGEST_URL = BASE_COMMENT_URL + "cmtuserinfo/";//post
    public static String COMMENT_INGEST_URL = BASE_COMMENT_URL + "cmtusercomment/";//post
    public static String REPLY_INGEST_URL = BASE_COMMENT_URL + "cmtuserreply/";//post

    public static String COMMENT_LIKE_COUNT_URL = BASE_COMMENT_URL + "cmtuserlike/";//post
    public static String COMMENT_REPORT_ABUSE_URL = BASE_COMMENT_URL + "cmtuserabuse/";//post

    public static String COMMENT_DELETE_URL = BASE_COMMENT_URL + "cmtdeletecomment/";//post

    /*{channelId}, {postId} ,{size}*/
    public static String REPLY_LOAD_MORE_URL = BASE_COMMENT_URL + "ttl/cmtgetreply/%s/%s/%s/";

    /*{channelId}, {postId} ,{commentId}*/
    public static String RETRIEVE_ALL_REPLIES = BASE_COMMENT_URL + "ttl/cmtgetreply/%s/%s/%s/";
}

