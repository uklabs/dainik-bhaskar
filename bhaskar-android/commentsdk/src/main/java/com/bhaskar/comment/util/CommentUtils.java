package com.bhaskar.comment.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.Uri;
import android.text.Layout;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.TextView;
import android.widget.Toast;

import com.bhaskar.comment.R;
import com.bhaskar.comment.handler.CommentActionHandler;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Lalit Goswami on 26-10-2017.
 */

public class CommentUtils {
    public static final int TIMEOUT_VALUE = 12000;

    public static int postHashCode(String finalStoryUrl) {
        int hash = 0;
        try {
            if (!TextUtils.isEmpty(finalStoryUrl)) {
                for (int i = 0; i < finalStoryUrl.length(); i++) {
                    hash = ((hash << 5) - hash) + finalStoryUrl.codePointAt(i);
                    hash = hash & hash;
                }
            }
        } catch (Exception e) {

        }
        return hash;
    }

    public static int postHashCodeWIthOpera(String url) {
        int hash = 0;
        try {
            String[] urlArray = url.split(".com");
            if (urlArray.length > 1) {
                int endIndex = urlArray[1].indexOf("?");
                if (endIndex == -1)
                    endIndex = urlArray[1].length() - 1;
                String finalStoryUrl = urlArray[1].substring(0, endIndex);
                if (!TextUtils.isEmpty(finalStoryUrl)) {
                    for (int i = 0; i < finalStoryUrl.length(); i++) {
                        hash = ((hash << 5) - hash) + finalStoryUrl.codePointAt(i);
                        hash = hash & hash;
                    }
                }
            }
        } catch (Exception e) {

        }
        return hash;
    }

    public static int fetchAttributeColor(Context context, int attr) {
        if (context != null) {
            TypedValue typedValue = new TypedValue();

            TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]{attr});
            int color = a.getColor(0, 0);
            a.recycle();
            return color;
        } else {
            return R.color.grey;
        }

    }

    public static String getFinalCount(int likeCount) {
        if (likeCount > 99) {
            return "99+";
        } else {
            return "" + likeCount;
        }
    }

    public static String getSIPrefixForCount(int count) {
        String siPrefix;

        try {
            int value = (count / 1000000000);
            if (value > 0) {
                siPrefix = String.format(Locale.getDefault(), "%.1fB", (count / 1000000000.0f));
            } else {
                value = (count / 1000000);
                if (value > 0) {
                    siPrefix = String.format(Locale.getDefault(), "%.1fM", (count / 1000000.0f));
                } else {
                    value = (count / 1000);
                    if (value > 0) {
                        siPrefix = String.format(Locale.getDefault(), "%.1fK", (count / 1000.0f));
                    } else {
                        siPrefix = String.format(Locale.getDefault(), "%d", count);
                    }
                }
            }

        } catch (Exception e) {
            siPrefix = "" + count;
        }

        return siPrefix;
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targtetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targtetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (targtetHeight / v.getContext().getResources().getDisplayMetrics().density));
//        a.setDuration(250);
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
//        a.setDuration(250);
        v.startAnimation(a);
    }


    //2018-03-05T18:44:27Z
    public static String formatToYesterdayOrToday(String date) throws ParseException {
        Date dateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(date);//2018-03-10T15:21:52Z
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("h:mm a");

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today, " + timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday, " + timeFormatter.format(dateTime);
        } else {
            return getDateAndTime(date);
        }
    }

    private static String getDateAndTime(String startDateString) {
        String currentDateAndTime = startDateString;
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            Date startDate = formatter.parse(startDateString);
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            currentDateAndTime = sdf.format(startDate);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return currentDateAndTime;
    }

    public static void showCustomToast(Context context, String message) {
        try {
            View layout = LayoutInflater.from(context).inflate(R.layout.comment_layout_custom_toast, null);
            TextView text = layout.findViewById(R.id.toast_textview);
            text.setText(message);

            Toast toast = new Toast(context);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } catch (Exception e) {
        }
    }

    public String getDateAndTimeForComment() {
        String convertedDateAndTime = "";
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a z");
        convertedDateAndTime = formatter.format(new Date(System.currentTimeMillis()));
        return convertedDateAndTime;
    }

    public static MovementMethod privacyMovementMethod(final Context context, final CommentActionHandler commentActionHandler) {
        return new MovementMethod() {

            @Override
            public boolean onTrackballEvent(TextView widget, Spannable text,
                                            MotionEvent event) {
                return false;
            }

            @Override
            public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
                final int action = event.getAction();
                if (action == MotionEvent.ACTION_UP) {
                    final int x = (int) event.getX() - widget.getTotalPaddingLeft() + widget.getScrollX();
                    final int y = (int) event.getY() - widget.getTotalPaddingTop() + widget.getScrollY();
                    final Layout layout = widget.getLayout();
                    final int line = layout.getLineForVertical(y);
                    final int off = layout.getOffsetForHorizontal(line, x);
                    final ClickableSpan[] C = buffer.getSpans(off, off, ClickableSpan.class);
                    if (C.length != 0) {
                        URLSpan span = (URLSpan) C[0];
                        String url = span.getURL();
                        if (!url.startsWith("http://") && !url.startsWith("https://"))
                            url = "http://" + url;
                        try {
                            if (commentActionHandler != null) {
                                commentActionHandler.openLink(url);
                            } else {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                context.startActivity(browserIntent);
                            }
                        } catch (ActivityNotFoundException ignored) {
                        }
                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onTakeFocus(TextView widget, Spannable text, int direction) {// empty Method
            }

            @Override
            public boolean onKeyUp(TextView widget, Spannable text, int keyCode,
                                   KeyEvent event) {
                return false;
            }

            @Override
            public boolean onKeyOther(TextView view, Spannable text, KeyEvent event) {
                return false;
            }

            @Override
            public boolean onKeyDown(TextView widget, Spannable text, int keyCode,
                                     KeyEvent event) {
                return false;
            }

            @Override
            public boolean onGenericMotionEvent(TextView widget, Spannable text,
                                                MotionEvent event) {
                return false;
            }

            @Override
            public void initialize(TextView widget, Spannable text) {
                widget.setLinkTextColor(Color.parseColor("#000000"));
            }

            @Override
            public boolean canSelectArbitrarily() {
                return false;
            }
        };
    }
}