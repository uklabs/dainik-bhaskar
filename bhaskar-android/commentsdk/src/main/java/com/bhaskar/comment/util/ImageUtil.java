package com.bhaskar.comment.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.target.AppWidgetTarget;
import com.bumptech.glide.request.target.NotificationTarget;

public class ImageUtil {

    public static final String defaultURL = "";

    public static void setImage(Context context, String imageUrl, ImageView imageView, int placeholder) {
        try {
            if (context == null)
                return;

            GlideUrl url = HttpHeader.getUrl(imageUrl);

            if (placeholder == 0) {
                Glide.with(context)
                        .asBitmap()
                        .load(url == null ? defaultURL : url)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .into(imageView);
            } else {
                Glide.with(context)
                        .asBitmap()
                        .load(url == null ? defaultURL : url)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .placeholder(placeholder)
                        .error(placeholder)
                        .into(imageView);
            }
        } catch (Exception ignored) {
        }
    }

    public static void setImage(Context context, int resId, ImageView imageView, int placeholder) {
        try {
            if (context == null)
                return;

            if (placeholder == 0) {
                Glide.with(context)
                        .asBitmap()
                        .load(resId)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .into(imageView);
            } else {
                Glide.with(context)
                        .asBitmap()
                        .load(resId)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .placeholder(placeholder)
                        .error(placeholder)
                        .into(imageView);
            }
        } catch (Exception ignored) {
        }
    }

    public static void setImageNew(Context context, String imageUrl, ImageView imageView, int placeholder) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asBitmap()
                    .load(url == null ? defaultURL : url)
                    .placeholder(placeholder)
                    .error(placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .transform(new jp.wasabeef.glide.transformations.BlurTransformation())
                    .into(imageView);
        } catch (Exception ignored) {
        }
    }

    public static void setImageOverride(Context context, String imageUrl, ImageView imageView, int placeholder) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asBitmap()
                    .load(url == null ? defaultURL : url)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .override(com.bumptech.glide.request.target.Target.SIZE_ORIGINAL, com.bumptech.glide.request.target.Target.SIZE_ORIGINAL)
                    .into(imageView);
        } catch (Exception ignored) {
        }
    }

    public static void setImageNotification(Context context, String imageUrl, NotificationTarget imageView, int placeholder) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asBitmap()
                    .load(url == null ? defaultURL : url)
                    .placeholder(placeholder)
                    .error(placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .into(imageView);
        } catch (Exception ignored) {
        }
    }

    public static void setImageGif(Context context, String imageUrl, ImageView gifImageView, int i) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asGif()
                    .load(url == null ? defaultURL : url)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .into(gifImageView);
        } catch (Exception ignored) {
        }
    }

    public static void setImageWidget(Context context, String imageUrl, AppWidgetTarget imageView, int placeholder) {
        try {
            GlideUrl url = HttpHeader.getUrl(imageUrl);

            Glide.with(context)
                    .asBitmap()
                    .load(url == null ? defaultURL : url)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .into(imageView);
        } catch (Exception ignored) {
        }
    }

    public static class HttpHeader {
        private static final String USER_AGENT = CommentConstant.USER_AGENT;
        private static LazyHeaders lazyHeaders;

        public static GlideUrl getUrl(String url) {
            try {
                if (lazyHeaders == null) {
                    lazyHeaders = new LazyHeaders.Builder().addHeader("User-Agent", USER_AGENT).build();
                }

                return new GlideUrl(url, lazyHeaders);
            } catch (Exception e) {
                try {
                    return new GlideUrl(url);
                } catch (Exception ignored) {
                }
            }

            return null;
        }
    }
}
