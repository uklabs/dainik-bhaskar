package com.db.coupanmanagement.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.db.coupanmanagement.BuildConfig;
import com.db.coupanmanagement.R;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.model.APIModel;
import com.db.coupanmanagement.utils.GoogleAnalyticUtils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by hp on 30-05-2018.
 */

public abstract class BaseActivity extends AppCompatActivity {

    /**
     * This method will be used to replace fragment
     *
     * @param fragment
     */
    protected void showFragment(Fragment fragment, Bundle extras) {
        try {
            String name = fragment.getClass().getSimpleName();
            // Below code commented for Drashlytics crash fix on 22-09-2018.
//        boolean fragmentPopped = getSupportFragmentManager().popBackStackImmediate(name, CouponManagementConstants.FRAGMENT_BACK_STACK_FLAG);
//        if (!fragmentPopped) {
            fragment.setArguments(extras);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_left, R.anim.slide_left_exit, R.anim.slide_right_exit, R.anim.slide_right);
            fragmentTransaction.add(R.id.main_container, fragment, name);
            fragmentTransaction.addToBackStack(name);
            fragmentTransaction.commitAllowingStateLoss(); // Replaced commit() due to IllegalStateException crash
        } catch (Exception e) {
            Log.d(BaseActivity.class.getName(), e.getMessage() + "");
        }
//        }
    }

    public interface APIModelProvider {
        APIModel getAPIModel();

        void getAPIModel(Intent intent);
    }

    /**
     * This method can be used to track any user event in all the screens.
     *
     * @param category Name of category
     * @param action   Name of action
     * @param label    Name of label
     * @param value    Value of event
     */
    public void trackEvent(APIModel apiModel, String category, String action, String label, long value) {
        try {
            StringBuilder builder = new StringBuilder();
            // Send an event to GA
            Tracker tracker = new GoogleAnalyticUtils().getGoogleAnalyticTracker(getApplication());
            if (tracker != null) {
                HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();
                if (apiModel != null) {
                    // Set Login Status
                    if (!apiModel.isUserLoggedIn()) {
                        builder.append(" Dimension7->LoginStatus: " + CouponManagementConstants.GALabel.GUEST);
                        eventBuilder.setCustomDimension(CouponManagementConstants.GADimensions.LOGIN_STATUS, CouponManagementConstants.GALabel.GUEST);
                    } else {
                        builder.append(" Dimension7->LoginStatus: " + CouponManagementConstants.GALabel.LOGGED_IN);
                        eventBuilder.setCustomDimension(CouponManagementConstants.GADimensions.LOGIN_STATUS, CouponManagementConstants.GALabel.LOGGED_IN);
                    }
                    // Set UID
                    builder.append(" Dimension8->UserId: ").append(apiModel.getuId());
                    eventBuilder.setCustomDimension(CouponManagementConstants.GADimensions.DB_USER_ID, apiModel.getuId());
                    // Set DBId
                    if (!TextUtils.isEmpty(apiModel.getDbId())) {
                        builder.append(" Dimension8->DBId: ").append(apiModel.getDbId());
                        eventBuilder.setCustomDimension(CouponManagementConstants.GADimensions.DB_ID, apiModel.getDbId());
                    } else {
                        builder.append(" Dimension8->DBId: ").append("NA");
                        eventBuilder.setCustomDimension(CouponManagementConstants.GADimensions.DB_ID, "NA");

                    }
                }
                eventBuilder.setCategory(category).setAction(action).setLabel(label).setValue(value);
                // Send tracker
                tracker.send(eventBuilder.build());
//                Log.d(CouponManagementConstants.GA_TAG, "Category: " + category + " Action: " + action + " Label: " + label + " Value: " + value + " " + builder.toString());

//                if (BuildConfig.DEBUG) {
//                    Toast.makeText(getApplication(), "Category: " + category + "\nAction: " + action + "\nLabel: " + label + "\nValue: " + value, Toast.LENGTH_LONG).show();
//                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method will be used to track next screen navigation
     *
     * @param apiModel          APIModel
     * @param currentScreenName Name of current screen
     * @param nextScreenName    Name of next screen
     */
    public void trackEventForNextScreen(APIModel apiModel, String currentScreenName, String nextScreenName) {
        try {
            StringBuilder builder = new StringBuilder();
            // Send an event to GA
            Tracker tracker = new GoogleAnalyticUtils().getGoogleAnalyticTracker(getApplication());
            if (tracker != null) {
                HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();
                if (apiModel != null) {
                    // Set Login Status
                    if (!apiModel.isUserLoggedIn()) {
                        builder.append(" Dimension7->LoginStatus: " + CouponManagementConstants.GALabel.GUEST);
                        eventBuilder.setCustomDimension(CouponManagementConstants.GADimensions.LOGIN_STATUS, CouponManagementConstants.GALabel.GUEST);
                    } else {
                        builder.append(" Dimension7->LoginStatus: " + CouponManagementConstants.GALabel.LOGGED_IN);
                        eventBuilder.setCustomDimension(CouponManagementConstants.GADimensions.LOGIN_STATUS, CouponManagementConstants.GALabel.LOGGED_IN);
                    }
                    // Set UID
                    builder.append(" Dimension8->UserId: ").append(apiModel.getuId());
                    eventBuilder.setCustomDimension(CouponManagementConstants.GADimensions.DB_USER_ID, apiModel.getuId());
                    // Set DBId
                    if (!TextUtils.isEmpty(apiModel.getDbId())) {
                        builder.append(" Dimension8->DBId: ").append(apiModel.getDbId());
                        eventBuilder.setCustomDimension(CouponManagementConstants.GADimensions.DB_ID, apiModel.getDbId());

                    } else {
                        builder.append(" Dimension8->DBId: ").append("NA");
                        eventBuilder.setCustomDimension(CouponManagementConstants.GADimensions.DB_ID, "NA");

                    }
                }
                eventBuilder.setCategory(CouponManagementConstants.GAEventCategory.NEXT_SCREEN).setAction(CouponManagementConstants.GA_CURRENT_SCREEN_TAG + currentScreenName).setLabel(CouponManagementConstants.GA_NEXT_SCREEN_TAG + nextScreenName);
                // Send tracker
                tracker.send(eventBuilder.build());
                Log.d(CouponManagementConstants.GA_TAG, "Category: " + CouponManagementConstants.GAEventCategory.NEXT_SCREEN + " Action: " + CouponManagementConstants.GA_CURRENT_SCREEN_TAG + currentScreenName + " Label: " + CouponManagementConstants.GA_NEXT_SCREEN_TAG + nextScreenName + " " + builder.toString());

                if (BuildConfig.DEBUG) {
                    Toast.makeText(getApplication(), "Category: " + CouponManagementConstants.GAEventCategory.NEXT_SCREEN + "\nAction: " + CouponManagementConstants.GA_CURRENT_SCREEN_TAG + currentScreenName + "\nLabel: " + CouponManagementConstants.GA_NEXT_SCREEN_TAG + nextScreenName, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method can be used to track current & previous screen.
     *
     * @param apiModel           APIModel
     * @param categoryName       Name of category
     * @param currentScreenName  Name of current screen
     * @param previousScreenName Name of previous screen
     */
    public void trackPageView(APIModel apiModel, String categoryName, String currentScreenName, String previousScreenName) {
        try {
            StringBuilder builder = new StringBuilder();
            Tracker tracker = new GoogleAnalyticUtils().getGoogleAnalyticTracker(getApplication());
            if (tracker != null) {
                HitBuilders.ScreenViewBuilder screenViewBuilder = new HitBuilders.ScreenViewBuilder();
                if (apiModel != null) {
                    // Set Login Status
                    if (!apiModel.isUserLoggedIn()) {
                        builder.append(" Dimension7->LoginStatus: " + CouponManagementConstants.GALabel.GUEST);
                        screenViewBuilder.setCustomDimension(CouponManagementConstants.GADimensions.LOGIN_STATUS, CouponManagementConstants.GALabel.GUEST);
                    } else {
                        builder.append(" Dimension7->LoginStatus: " + CouponManagementConstants.GALabel.LOGGED_IN);
                        screenViewBuilder.setCustomDimension(CouponManagementConstants.GADimensions.LOGIN_STATUS, CouponManagementConstants.GALabel.LOGGED_IN);
                    }
                    // Set UID
                    builder.append(" Dimension8->UserId: ").append(apiModel.getuId());
                    screenViewBuilder.setCustomDimension(CouponManagementConstants.GADimensions.DB_USER_ID, apiModel.getuId());
                    // Set DBId
                    if (!TextUtils.isEmpty(apiModel.getDbId())) {
                        builder.append(" Dimension8->DBId: ").append(apiModel.getDbId());
                        screenViewBuilder.setCustomDimension(CouponManagementConstants.GADimensions.DB_ID, apiModel.getDbId());
                    } else {
                        builder.append(" Dimension8->DBId: ").append("NA");
                        screenViewBuilder.setCustomDimension(CouponManagementConstants.GADimensions.DB_ID, "NA");

                    }
                }
                // Set screen name
                tracker.setScreenName(currentScreenName);
                screenViewBuilder.setCustomDimension(CouponManagementConstants.GADimensions.CONTENT_CATEGORY, categoryName);
                // Set previous & next screen name on screen load
                screenViewBuilder.setCustomDimension(CouponManagementConstants.GADimensions.PREVIOUS_SCREEN, previousScreenName);
                // Send tracker
                tracker.send(screenViewBuilder.build());

//                Log.d(CouponManagementConstants.GA_TAG, "Category: " + categoryName + " PreviousScreenName: " + previousScreenName + " NextScreenName: " + currentScreenName + " " + builder.toString());
//                Log.d(CouponManagementConstants.GA_TAG, " Screen: " + currentScreenName + ", Custom Dimension :- Category: " + categoryName + ", PreviousScreenName: " + previousScreenName);

//                if (BuildConfig.DEBUG) {
//                    Toast.makeText(getApplication(), "Category: " + categoryName + "\nPreviousScreenName: " + previousScreenName + "\nNextScreenName: " + currentScreenName, Toast.LENGTH_LONG).show();
//                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
//        CommonUtils.runGc();
        super.onDestroy();
    }
}
