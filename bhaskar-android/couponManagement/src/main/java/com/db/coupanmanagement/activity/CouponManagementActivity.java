package com.db.coupanmanagement.activity;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.dialog.CouponDialogUtils;
import com.db.coupanmanagement.fragment.AvailableOffersWalkThroughFragment;
import com.db.coupanmanagement.fragment.BaseFragment;
import com.db.coupanmanagement.fragment.MonthlyOffersFragment;
import com.db.coupanmanagement.model.APIModel;
import com.db.coupanmanagement.network.NetworkStateReceiver;
import com.db.coupanmanagement.utils.CouponAppPreferences;
import com.db.coupanmanagement.utils.LocationProviderUtils;
import com.bhaskar.data.model.ProfileInfo;

import java.util.Objects;

public class CouponManagementActivity extends BaseActivity implements NetworkStateReceiver.NetworkStateReceiverListener, BaseActivity.APIModelProvider {

    private Toolbar toolbar;
    private NetworkStateReceiver mNetworkStateReceiver;
    private LocationProviderUtils mLocationProviderUtils;
    private APIModel mApiModel;

    @Override
    public APIModel getAPIModel() {
        return mApiModel;
    }

    @Override
    public void getAPIModel(Intent intent) {
//        mApiModel = (APIModel) Objects.requireNonNull(intent.getExtras()).getSerializable(CouponManagementConstants.APIModelData.API_MODEL);
        ProfileInfo profileInfo = (ProfileInfo) Objects.requireNonNull(intent.getExtras()).getSerializable("userData");
        mApiModel.setEmailId(profileInfo.email);
        mApiModel.setMobileNo(profileInfo.mobile);
        mApiModel.setuId(profileInfo.uID);
        mApiModel.setUuId(profileInfo.uuID);
        mApiModel.setUserLoggedIn(profileInfo.isUserLoggedIn);
    }

    private void handleToolbar() {
        /*Tool Bar*/
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.cca_back);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_layout);

        /*tool bar*//*
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setToolbarBackIconVisible(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());*/
        handleToolbar();

        // Get data from bundle
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String uId = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_UID);
            String uuId = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_UUID);
            String emailId = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_EMAIL_ID);
            String dbId = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_DB_ID);
            dbId = (dbId == null ? "" : dbId);
            String authToken = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_AUTH_TOKEN);
            authToken = (authToken == null ? "" : authToken);
            String mobileNo = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_MOBILE_NO);
            String deviceId = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_DEVICE_ID);
            String appId = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_APP_ID);
            String appKey = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_APP_KEY);
            boolean isComingFromHamburgerMenu = extras.getBoolean(CouponManagementConstants.GetBundleKeys.KEY_IS_COMING_FROM_HAMBURGER_MENU);
            String toolBarTitle = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_TOOLBAR_TITLE);
            boolean isUserLoggedIn = extras.getBoolean(CouponManagementConstants.GetBundleKeys.KEY_IS_USER_LOGGED_IN);
            mApiModel = new APIModel(uId, uuId, emailId, dbId, authToken, mobileNo, deviceId, appId, appKey, isComingFromHamburgerMenu, isUserLoggedIn);
            // Load available offers
            BaseFragment availableOffersFragment = new AvailableOffersWalkThroughFragment();
            addFragment(availableOffersFragment, extras);
            setToolbarTItle(toolBarTitle);
        }

        mNetworkStateReceiver = new NetworkStateReceiver();
//        initLocationProvider();
    }

    /**
     * This method will be used to show/hide toolbar back icon
     *
     * @param isVisible
     */
    public void setToolbarBackIconVisible(boolean isVisible) {
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(isVisible);
    }

    /**
     * This method will be used to set title of toolbar
     *
     * @param title
     */
    public void setToolbarTItle(String title) {
        toolbar.setTitle(title);
    }

    public void addFragment(BaseFragment fragment, Bundle extras) {
        fragment.setAPIModelProvider(this);
        showFragment(fragment, extras);
    }

    public void refreshMonthlyOffers() {
        setToolbarBackIconVisible(true);
        onBackPressed();
        // MonthlyOffersFragment
        BaseFragment monthlyOffersFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(CouponManagementConstants.GetFragmentName.MONTHLY_OFFERS_FRAGMENT);
        if (monthlyOffersFragment != null && monthlyOffersFragment.isVisible()) {
            monthlyOffersFragment.fetchDataFromServer();
        }
    }

    @Override
    public void networkAvailable() {
        // AvailableOffersFragment
        BaseFragment availableOffersFragment = getAvailableWalkThroughFragment();
        if (availableOffersFragment != null && availableOffersFragment.isVisible()) {
            availableOffersFragment.setAPIModelProvider(this);
            availableOffersFragment.networkConnectivitySuccess();
        }

        // MonthlyOffersFragment
        BaseFragment monthlyOffersFragment = getMonthlyOfferFragment();
        if (monthlyOffersFragment != null && monthlyOffersFragment.isVisible()) {
            monthlyOffersFragment.setAPIModelProvider(this);
            monthlyOffersFragment.networkConnectivitySuccess();
        }

        // MonthlyCouponsFragment
        BaseFragment monthlyCouponsFragment = getMonthlyCouponFragment();
        if (monthlyCouponsFragment != null && monthlyCouponsFragment.isVisible()) {
            monthlyCouponsFragment.setAPIModelProvider(this);
            monthlyCouponsFragment.networkConnectivitySuccess();
        }

        // ClaimedCouponCodeFragment
        BaseFragment claimedCouponCodeFragment = getClaimedCouponFragment();
        if (claimedCouponCodeFragment != null && claimedCouponCodeFragment.isVisible()) {
            claimedCouponCodeFragment.setAPIModelProvider(this);
            claimedCouponCodeFragment.networkConnectivitySuccess();
        }
    }

    @Override
    public void networkUnavailable() {
        CouponDialogUtils.showToastMessage(CouponManagementActivity.this, getString(R.string.network_unavailable));
        // AvailableOffersFragment
        BaseFragment availableOffersFragment = getAvailableWalkThroughFragment();
        if (availableOffersFragment != null && availableOffersFragment.isVisible()) {
            availableOffersFragment.setAPIModelProvider(this);
            availableOffersFragment.networkConnectivityError();
        }

        // MonthlyOffersFragment
        BaseFragment monthlyOffersFragment = getMonthlyOfferFragment();
        if (monthlyOffersFragment != null && monthlyOffersFragment.isVisible()) {
            monthlyOffersFragment.setAPIModelProvider(this);
            monthlyOffersFragment.networkConnectivityError();
        }

        // MonthlyCouponsFragment
        BaseFragment monthlyCouponsFragment = getMonthlyCouponFragment();
        if (monthlyCouponsFragment != null && monthlyCouponsFragment.isVisible()) {
            monthlyCouponsFragment.setAPIModelProvider(this);
            monthlyCouponsFragment.networkConnectivityError();
        }

        // ClaimedCouponCodeFragment
        BaseFragment claimedCouponCodeFragment = getClaimedCouponFragment();
        if (claimedCouponCodeFragment != null && claimedCouponCodeFragment.isVisible()) {
            claimedCouponCodeFragment.setAPIModelProvider(this);
            claimedCouponCodeFragment.networkConnectivityError();
        }
    }

    private BaseFragment getAvailableWalkThroughFragment() {
        return (BaseFragment) getSupportFragmentManager().findFragmentByTag(CouponManagementConstants.GetFragmentName.AVAILABLE_OFFERS_WALK_THROUGH_FRAGMENT);
    }

    private BaseFragment getMonthlyOfferFragment() {
        return (BaseFragment) getSupportFragmentManager().findFragmentByTag(CouponManagementConstants.GetFragmentName.MONTHLY_OFFERS_FRAGMENT);
    }

    private BaseFragment getMonthlyCouponFragment() {
        return (BaseFragment) getSupportFragmentManager().findFragmentByTag(CouponManagementConstants.GetFragmentName.MONTHLY_COUPONS_FRAGMENT);
    }

    private BaseFragment getClaimedCouponFragment() {
        return (BaseFragment) getSupportFragmentManager().findFragmentByTag(CouponManagementConstants.GetFragmentName.CLAIMED_COUPONS_FRAGMENT);
    }

    /**
     * This method will be used to register network receiver
     */
    private void registerNetworkReceiver() {
        mNetworkStateReceiver.addListener(this);
        this.registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     * This method will be used to unregister network receiver
     */
    private void unRegisterNetworkReceiver() {
        mNetworkStateReceiver.removeListener(this);
        this.unregisterReceiver(mNetworkStateReceiver);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                boolean isComingFromHamburgerMenu = extras.getBoolean(CouponManagementConstants.GetBundleKeys.KEY_IS_COMING_FROM_HAMBURGER_MENU);
                if (isComingFromHamburgerMenu) {
                    finish();
                } else {
                    // Show dialog
                    showNormalMessageDialog();
                }
            } else {
                finish();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerNetworkReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegisterNetworkReceiver();
    }

    /**
     * This method will be used to navigate to coupon dashboard
     *
     * @param context
     * @param authToken
     * @param uId
     * @param uuId
     * @param dbId
     */
    public static void startCouponManagementActivity(Context context, String authToken, String uId, String uuId, String emailId, String dbId, String mobileNo, String deviceId, String appId, String appKey, boolean isComingFromHamburgerMenu, String toolbarTitle, boolean isUserLoggedIn) {
        // Clear preference for mobile number popup
        String prefEmailId = CouponAppPreferences.getInstance(context).getStringValue(CouponManagementConstants.GetPreferenceKeys.EMAILID, "");
        if (!prefEmailId.equalsIgnoreCase(emailId)) {
            CouponAppPreferences.getInstance(context).clearPref();
        }
        Bundle extras = new Bundle();
        dbId = (dbId == null ? "" : dbId);
        authToken = (authToken == null ? "" : authToken);
        extras.putString(CouponManagementConstants.GetBundleKeys.KEY_AUTH_TOKEN, authToken);
        extras.putString(CouponManagementConstants.GetBundleKeys.KEY_DB_ID, dbId);
        extras.putString(CouponManagementConstants.GetBundleKeys.KEY_UID, uId);
        extras.putString(CouponManagementConstants.GetBundleKeys.KEY_UUID, uuId);
        extras.putString(CouponManagementConstants.GetBundleKeys.KEY_EMAIL_ID, emailId);
        extras.putString(CouponManagementConstants.GetBundleKeys.KEY_MOBILE_NO, mobileNo);
        extras.putString(CouponManagementConstants.GetBundleKeys.KEY_DEVICE_ID, deviceId);
        extras.putString(CouponManagementConstants.GetBundleKeys.KEY_APP_ID, appId);
        extras.putString(CouponManagementConstants.GetBundleKeys.KEY_APP_KEY, appKey);
        extras.putBoolean(CouponManagementConstants.GetBundleKeys.KEY_IS_COMING_FROM_HAMBURGER_MENU, isComingFromHamburgerMenu);
        extras.putString(CouponManagementConstants.GetBundleKeys.KEY_TOOLBAR_TITLE, toolbarTitle);
        extras.putBoolean(CouponManagementConstants.GetBundleKeys.KEY_IS_USER_LOGGED_IN, isUserLoggedIn);

        ActivityOptions options = ActivityOptions.makeCustomAnimation(context, R.anim.slide_left, R.anim.slide_left);
        Intent intent = new Intent(context, CouponManagementActivity.class);
        intent.putExtras(extras);
        context.startActivity(intent, options.toBundle());
    }

    /**
     * This method will be used to ask permission to users for accessing user's information
     */
    public void initLocationProvider(MonthlyOffersFragment.OnLocationPermissionGrantedInterface onLocationPermissionGrantedInterface) {
        if (null == mLocationProviderUtils) {
            mLocationProviderUtils = new LocationProviderUtils(new LocationProviderUtils.OnLocationFoundListener() {
                @Override
                public void onGetLatitudeLongitude(String latitude, String longitude, String city) {
                    Log.w("LAT-LONG", "Location: " + latitude + " >> " + longitude + " >> " + city);
                    // Put data in SharedPreferences so that we can use these lat/long on get coupons api
                    CouponAppPreferences.getInstance(CouponManagementActivity.this).setStringValue(CouponManagementConstants.GetPreferenceKeys.LATTITUDE, latitude);
                    CouponAppPreferences.getInstance(CouponManagementActivity.this).setStringValue(CouponManagementConstants.GetPreferenceKeys.LONGITUDE, longitude);
                    CouponAppPreferences.getInstance(CouponManagementActivity.this).setStringValue(CouponManagementConstants.GetPreferenceKeys.CITY, city);
                    // MonthlyCouponsFragment
//                    BaseFragment monthlyOffersFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(CouponManagementConstants.GetFragmentName.MONTHLY_OFFERS_FRAGMENT);
//                    if (monthlyOffersFragment != null && monthlyOffersFragment.isVisible()) {
                    if (onLocationPermissionGrantedInterface != null) {
                        // Location permission granted
                        onLocationPermissionGrantedInterface.onLocationDataFound();
                    }
//                    }
                }

                @Override
                public void onLocationNotFound() {
                    if (onLocationPermissionGrantedInterface != null) {
                        // Location permission not granted
                        onLocationPermissionGrantedInterface.onLocationDataNotFound();
                    }
                }
            });
        }
        mLocationProviderUtils.initForLocationPermission(this);
    }

    /**
     * This method will be used to ask permission to users for accessing user's information
     */
//    public void initReadSmsProvider(MonthlyOffersFragment.OnReadSmsPermissionGrantedInterface onReadSmsPermissionGrantedInterface) {
//        mLocationProviderUtils = new LocationProviderUtils(() -> {
//            BaseFragment monthlyOffersFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(CouponManagementConstants.GetFragmentName.MONTHLY_OFFERS_FRAGMENT);
//            if (monthlyOffersFragment != null && monthlyOffersFragment.isVisible()) {
//                if (onReadSmsPermissionGrantedInterface != null) {
//                    // Read sms permission granted
//                    onReadSmsPermissionGrantedInterface.onReadSmsPermissionGranted();
//                }
//            }
//        });
//        mLocationProviderUtils.initForReadSmsPermission(this);
//    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (null != mLocationProviderUtils)
            mLocationProviderUtils.onRequestPermissionsResult(requestCode, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (null != mLocationProviderUtils)
            mLocationProviderUtils.onActivityResult(requestCode, resultCode);
    }

    @Override
    protected void onDestroy() {
        if (null != mLocationProviderUtils) {
            mLocationProviderUtils.dispose();
        }
        super.onDestroy();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.coupon_menu, menu);
//        updateMenu(menu);
//        return true;
//    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        updateMenu(menu);
//        super.onPrepareOptionsMenu(menu);
//        return true;
//    }

//    private void updateMenu(Menu menu) {
//        if (mApiModel != null && mApiModel.isUserLoggedIn()) {
//            menu.getItem(0).setVisible(false); // Hide "Skip" menu
//            menu.getItem(1).setVisible(true); // Show "Home" menu
//        } else {
//            menu.getItem(1).setVisible(false); // Hide "Home" menu
//            menu.getItem(0).setVisible(true); // Show "Skip" menu
//        }
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.menu_skip) {
//            // Show dialog
//            showNormalMessageDialog();
//        }
//        if (id == R.id.menu_home) {
//            // navigate to home screen
////            navigateToHomeScreen(this);
//            finish();
//        }
//        return false;
//    }

    /**
     * This method is used to show normal message when user will skip from coupon or back press when coming from first launch
     */
    private void showNormalMessageDialog() {
        boolean exitDialogCount = CouponAppPreferences.getInstance(CouponManagementActivity.this).getBooleanValue(CouponManagementConstants.GetPreferenceKeys.EXIT_DIALOG_COUNT, false);
        if (!exitDialogCount) {
            new CouponDialogUtils().showNormalMessageWithOneActionButtonDialog(this, getString(R.string.coupon_skip), getString(R.string.coupon_skip_message), false, getString(R.string.ok), new CouponDialogUtils.SingleButtonDialogResponsesInterface() {
                @Override
                public void doOnOkButtonClick(Activity activity) {
                    CouponAppPreferences.getInstance(CouponManagementActivity.this).setBooleanValue(CouponManagementConstants.GetPreferenceKeys.EXIT_DIALOG_COUNT, true);
                    finish();
                    navigateToLoginScreen(CouponManagementActivity.this);
                }
            });
        } else {
            finish();
        }
    }

    /**
     * This method will be used to navigate to login screen
     *
     * @param activity
     */
    private void navigateToLoginScreen(Activity activity) {
        try {
            Intent intent = new Intent(activity, Class.forName(CouponManagementConstants.APP_ONBOARDING_ACTIVITY));
            intent.putExtra(CouponManagementConstants.GetBundleKeys.KEY_LAUNCH_LOGIN_TYPE, CouponManagementConstants.COUPON_FLAG_SKIP);
            startActivity(intent);
            activity.finish();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method will be used to navigate to home screen
     *
     * @param activity
     */
    private void navigateToHomeScreen(Activity activity) {
        try {
            Intent myIntent = new Intent(activity, Class.forName(CouponManagementConstants.APP_HOME_ACTIVITY));
            startActivity(myIntent);
            activity.finish();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
