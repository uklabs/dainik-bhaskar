package com.db.coupanmanagement.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.fragment.RedeemCouponCodeFragment;

import java.util.Objects;

public class RedirectToVenderPortalActivity extends BaseActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_layout);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Fragment fragment = new RedeemCouponCodeFragment();
            addFragment(fragment, bundle);
            setToolbarTItle(getString(R.string.offers_hindi));
        }
    }

    /**
     * This method will be used to set title of toolbar
     *
     * @param title
     */
    public void setToolbarTItle(String title) {
        toolbar.setTitle(title);
    }

    public void addFragment(Fragment fragment, Bundle extras) {
        showFragment(fragment, extras);
    }
}