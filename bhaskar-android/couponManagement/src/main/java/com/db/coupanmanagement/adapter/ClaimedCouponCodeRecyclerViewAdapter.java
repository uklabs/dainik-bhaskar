package com.db.coupanmanagement.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.db.coupanmanagement.R;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.dialog.CouponDialogUtils;
import com.db.coupanmanagement.dialog.OfflineCouponCodeDialog;
import com.db.coupanmanagement.model.ClaimedCouponDetailsModel;
import com.db.coupanmanagement.utils.Utils;

import java.util.List;

/**
 * Created by hp on 21-05-2018.
 */

public class ClaimedCouponCodeRecyclerViewAdapter extends RecyclerView.Adapter<ClaimedCouponCodeRecyclerViewAdapter.ClaimedCouponViewHolder> {

    private List<ClaimedCouponDetailsModel> mClaimedCouponDetailsModelList;
    private Context mContext;
    private GetCouponDetails mGetCouponDetailsInterface;

    public class ClaimedCouponViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivCoupon;
        private TextView tvVendorName, tvCouponAmount, tvExpiryDate, tvMinPurchase, tvTermsCondition, tvCouponCode, tvBtnViewCode, tvCouponType, tvCodeDekhLiyaHai;
        private LinearLayout linCodeDekhin, linCodeDekhLiyaHai;

        ClaimedCouponViewHolder(View itemView) {
            super(itemView);
            ivCoupon = itemView.findViewById(R.id.iv_brand_logo);
            tvVendorName = itemView.findViewById(R.id.tv_vendor_name);
            tvCouponAmount = itemView.findViewById(R.id.tv_coupon_amount);
            tvExpiryDate = itemView.findViewById(R.id.tv_expiry_date);
            tvMinPurchase = itemView.findViewById(R.id.tv_min_purchase);
            tvTermsCondition = itemView.findViewById(R.id.tv_terms_condition);
            tvBtnViewCode = itemView.findViewById(R.id.tv_btn_view_code);
            tvCouponCode = itemView.findViewById(R.id.tv_coupon_code);
            tvCouponType = itemView.findViewById(R.id.tv_coupon_type);
            tvCodeDekhLiyaHai = itemView.findViewById(R.id.tv_code_dekh_liya_hai);
            linCodeDekhin = itemView.findViewById(R.id.lin_code_dekhein);
            linCodeDekhLiyaHai = itemView.findViewById(R.id.lin_code_dekh_liya_hai);
        }
    }

    public ClaimedCouponCodeRecyclerViewAdapter(Context context, List<ClaimedCouponDetailsModel> claimedCouponDetailsModelList, GetCouponDetails getCouponDetails) {
        this.mContext = context;
        this.mClaimedCouponDetailsModelList = claimedCouponDetailsModelList;
        this.mGetCouponDetailsInterface = getCouponDetails;
    }

    @NonNull
    @Override
    public ClaimedCouponViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_fragment_claimed_coupan_code_item_layout, parent, false);
        return new ClaimedCouponViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ClaimedCouponViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final ClaimedCouponDetailsModel claimedCouponDetailsModel = mClaimedCouponDetailsModelList.get(position);
        holder.tvVendorName.setText(claimedCouponDetailsModel.getVendorName());
        String couponAmount = Utils.formatFloatValue(mContext, claimedCouponDetailsModel.getCouponAmount());
        holder.tvCouponAmount.setText(couponAmount);
        String validity = mContext.getString(R.string.expiry_date) + " - " + Utils.parseDateToddMMMyyyy(claimedCouponDetailsModel.getCouponExpiryDate());
        SpannableString validitySpannable = Utils.getHighLightedText(ContextCompat.getColor(mContext, R.color.coupon_dull_blue_color), validity, "-");
        holder.tvExpiryDate.setText(validitySpannable, TextView.BufferType.SPANNABLE);

        // Check if valid_on is coming then show valid_on text then show minimumPurchaseAmount
        if (TextUtils.isEmpty(claimedCouponDetailsModel.getValidOn())) {
            String minimumPurchase = mContext.getString(R.string.minimum_purchase_amount) + " " + Math.round(Float.parseFloat(claimedCouponDetailsModel.getMinimumPurchaseAmount()));
            SpannableString minimumPurchaseSpannable = Utils.getHighLightedText(ContextCompat.getColor(mContext, R.color.coupon_dull_blue_color), minimumPurchase, ":");

            holder.tvMinPurchase.setText(minimumPurchaseSpannable, TextView.BufferType.SPANNABLE);
        } else {
            String validOn = mContext.getString(R.string.valid_on_title) + claimedCouponDetailsModel.getValidOn();
            holder.tvMinPurchase.setText(validOn);
        }

        // Set type of coupon i.e Online/Offline
        if (claimedCouponDetailsModel.getIsOffline() == CouponManagementConstants.OfflineCouponData.IS_OFFLINE_COUPON) { // Online Coupon = 2
            holder.tvCouponType.setText(mContext.getString(R.string.online_coupon_title));
            holder.tvCouponType.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_green_color));
        } else { // Offline Coupon = 1
            holder.tvCouponType.setText(mContext.getString(R.string.offline_coupon_title));
            holder.tvCouponType.setTextColor(ContextCompat.getColor(mContext, R.color.color_red));

            // Hide code dekhein button if already offline viewed in front of shopkeeper
            if (claimedCouponDetailsModel.getIsOfflineCouponViewed() == CouponManagementConstants.OfflineCouponData.OFFLINE_COUPON_VIEWED) {
                holder.linCodeDekhin.setVisibility(View.GONE);
                holder.linCodeDekhLiyaHai.setVisibility(View.VISIBLE);
            }
        }

        holder.tvBtnViewCode.setOnClickListener(v -> {
            if (holder.tvBtnViewCode.getText().toString().trim().equalsIgnoreCase(mContext.getString(R.string.btn_view_code))) {
                if (claimedCouponDetailsModel.getIsOffline() == CouponManagementConstants.OfflineCouponData.IS_OFFLINE_COUPON) { // Online coupon = 2
                    holder.tvBtnViewCode.setText(mContext.getString(R.string.btn_copy));
                    holder.tvCouponCode.setText(Utils.decodeResponse(claimedCouponDetailsModel.getCouponCode()));
                } else { // Offline coupon = 1
                    // Show offline coupon code dekhein dialog
                    mGetCouponDetailsInterface.onClickOfflineCouponCodeDekhein(claimedCouponDetailsModel, holder, position, (itemPosition, holder1) -> {
                        holder1.linCodeDekhin.setVisibility(View.GONE);
                        holder1.linCodeDekhLiyaHai.setVisibility(View.VISIBLE);
                        // Set offline coupon viewed to 1 for not to show coupon code next time
                        mClaimedCouponDetailsModelList.get(itemPosition).setIsOfflineCouponViewed(CouponManagementConstants.OfflineCouponData.OFFLINE_COUPON_VIEWED);
                    });
                }
            } else if (holder.tvBtnViewCode.getText().toString().trim().equalsIgnoreCase(mContext.getString(R.string.btn_copy))) {
                new CouponDialogUtils().copyToClipBoard(mContext, Utils.decodeResponse(claimedCouponDetailsModel.getCouponCode()));
                mGetCouponDetailsInterface.onCodeDekheinClickListner(claimedCouponDetailsModel);
            } else {
                new CouponDialogUtils().copyToClipBoard(mContext, Utils.decodeResponse(claimedCouponDetailsModel.getCouponCode()));
            }
        });

        Glide.with(mContext)
                .load(claimedCouponDetailsModel.getVendorImage())
                .into(holder.ivCoupon);

        holder.tvTermsCondition.setOnClickListener(v -> mGetCouponDetailsInterface.onTermAndConditionClickListner(claimedCouponDetailsModel));

        holder.tvCodeDekhLiyaHai.setOnClickListener(view -> mGetCouponDetailsInterface.onClickCodeDekhLiyaHai());
    }

    @Override
    public int getItemCount() {
        return mClaimedCouponDetailsModelList.size() > 0 ? mClaimedCouponDetailsModelList.size() : 0;
    }

    public interface GetCouponDetails {
        void onCodeDekheinClickListner(ClaimedCouponDetailsModel couponDetailsModel);

        void onTermAndConditionClickListner(ClaimedCouponDetailsModel couponDetailsModel);

        void onClickOfflineCouponCodeDekhein(ClaimedCouponDetailsModel couponDetailsModel, ClaimedCouponViewHolder holder, int itemPosition, OfflineCouponCodeDialog.OnClickCodeDikhaDiyaInterface clickCodeDikhaDiyaInterface);

        void onClickCodeDekhLiyaHai();
    }
}
