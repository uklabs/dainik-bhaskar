package com.db.coupanmanagement.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.db.coupanmanagement.R;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.model.ClaimedCouponDetailsModel;
import com.db.coupanmanagement.utils.Utils;

import java.util.List;

/**
 * Created by hp on 21-05-2018.
 */

public class MonthlyCouponsRecyclerViewAdapter extends RecyclerView.Adapter<MonthlyCouponsRecyclerViewAdapter.MonthlyCouponsViewHolder> {
    private List<ClaimedCouponDetailsModel> mCouponModelList;
    private Context mContext;
    private GetCouponDetails mGetCouponDetails;

    class MonthlyCouponsViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout linCouponParent;
        private ImageView ivCoupon, ivMinus, ivPlus, ivHelp;
        private TextView tvVendorName, tvCouponAmount, tvNoOfCoupon, tvCouponValidity, tvCouponTermsAndCondition, tvMinPurchase, tvCouponType;

        MonthlyCouponsViewHolder(View itemView) {
            super(itemView);
            linCouponParent = itemView.findViewById(R.id.lin_coupon_parent);
            ivCoupon = itemView.findViewById(R.id.iv_brand_coupon);
            ivMinus = itemView.findViewById(R.id.iv_minus);
            ivPlus = itemView.findViewById(R.id.iv_plus);
            ivHelp = itemView.findViewById(R.id.iv_help);
            tvVendorName = itemView.findViewById(R.id.tv_vendor_name);
            tvCouponAmount = itemView.findViewById(R.id.tv_coupon_amount);
            tvCouponValidity = itemView.findViewById(R.id.tv_expiry_date);
            tvNoOfCoupon = itemView.findViewById(R.id.tv_no_of_coupon);
            tvNoOfCoupon.setText("0");
            tvCouponTermsAndCondition = itemView.findViewById(R.id.tv_terms_condition);
            tvMinPurchase = itemView.findViewById(R.id.tv_min_purchase);
            tvCouponType = itemView.findViewById(R.id.tv_coupon_type);
        }
    }

    public MonthlyCouponsRecyclerViewAdapter(Context context, List<ClaimedCouponDetailsModel> couponModelList, GetCouponDetails getCouponDetails) {
        this.mContext = context;
        this.mCouponModelList = couponModelList;
        this.mGetCouponDetails = getCouponDetails;
    }

    @NonNull
    @Override
    public MonthlyCouponsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_monthly_coupans_item_layout, parent, false);
        return new MonthlyCouponsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MonthlyCouponsViewHolder holder, final int position) {
        final ClaimedCouponDetailsModel couponDetailsModel = mCouponModelList.get(position);
        SpannableString validitySpannable;
        String validity = mContext.getString(R.string.coupon_expiry_date) + " - " + Utils.parseDateToddMMMyyyy(couponDetailsModel.getCouponExpiryDate());
//        String minimumPurchase = mContext.getString(R.string.minimum_purchase_amount) + " " + Math.round(Float.parseFloat(couponDetailsModel.getMinimumPurchaseAmount()));

        holder.tvVendorName.setText(couponDetailsModel.getVendorName());

        SpannableString minimumPurchaseSpannable;
        if (couponDetailsModel.getQty() > 0) {
            holder.ivMinus.setEnabled(true);
            holder.ivMinus.setAlpha(1.0f);
            float totalAmount = couponDetailsModel.getCouponAmount() * couponDetailsModel.getQty();
            String concatenatedAmount = Math.round(couponDetailsModel.getCouponAmount()) + " x " + couponDetailsModel.getQty() + " = " + Math.round(totalAmount);
            holder.tvCouponAmount.setText(concatenatedAmount);
            holder.linCouponParent.setBackgroundColor(ContextCompat.getColor(mContext, R.color.tranparent_coupon_background));
            validitySpannable = Utils.getHighLightedText(ContextCompat.getColor(mContext, android.R.color.white), validity, "-");

            // Check if valid_on is coming then show valid_on text then show minimumPurchaseAmount
            if (TextUtils.isEmpty(couponDetailsModel.getValidOn())) {
                String minimumPurchase = mContext.getString(R.string.minimum_purchase_amount) + " " + Math.round(Float.parseFloat(couponDetailsModel.getMinimumPurchaseAmount()));
                minimumPurchaseSpannable = Utils.getHighLightedText(ContextCompat.getColor(mContext, android.R.color.white), minimumPurchase, ":");
                holder.tvMinPurchase.setText(minimumPurchaseSpannable, TextView.BufferType.SPANNABLE);
            } else {
                String validOn = mContext.getString(R.string.valid_on_title) + couponDetailsModel.getValidOn();
                holder.tvMinPurchase.setText(validOn);
            }
        } else {
            holder.ivMinus.setEnabled(false);
            holder.ivMinus.setAlpha(0.3f);
            SpannableString couponAmount = Utils.getCouponAmount(mContext, ContextCompat.getColor(mContext, R.color.tab_selection), couponDetailsModel.getCouponAmount());
            holder.tvCouponAmount.setText(couponAmount, TextView.BufferType.SPANNABLE);
            holder.linCouponParent.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.white));
            validitySpannable = Utils.getHighLightedText(ContextCompat.getColor(mContext, R.color.coupon_dull_blue_color), validity, "-");

            // Check if valid_on is coming then show valid_on text then show minimumPurchaseAmount
            if (TextUtils.isEmpty(couponDetailsModel.getValidOn())) {
                String minimumPurchase = mContext.getString(R.string.minimum_purchase_amount) + " " + Math.round(Float.parseFloat(couponDetailsModel.getMinimumPurchaseAmount()));
                minimumPurchaseSpannable = Utils.getHighLightedText(ContextCompat.getColor(mContext, R.color.coupon_dull_blue_color), minimumPurchase, ":");
                holder.tvMinPurchase.setText(minimumPurchaseSpannable, TextView.BufferType.SPANNABLE);
            } else {
                String validOn = mContext.getString(R.string.valid_on_title) + couponDetailsModel.getValidOn();
                holder.tvMinPurchase.setText(validOn);
            }
        }

        if (couponDetailsModel.isClaimable()) {
            holder.ivPlus.setEnabled(true);
            holder.ivPlus.setAlpha(1.0f);
        } else {
            holder.ivPlus.setEnabled(false);
            holder.ivPlus.setAlpha(0.3f);
        }

        holder.tvCouponValidity.setText(validitySpannable, TextView.BufferType.SPANNABLE);

        holder.tvNoOfCoupon.setText(String.valueOf(couponDetailsModel.getQty()));
        Glide.with(mContext)
                .load(couponDetailsModel.getVendorImage())
                .into(holder.ivCoupon);

        // Set type of coupon i.e Online/Offline
        if (couponDetailsModel.getIsOffline() == CouponManagementConstants.OfflineCouponData.IS_OFFLINE_COUPON) { // Online coupon = 2
            holder.tvCouponType.setText(mContext.getString(R.string.online_coupon_title));
            holder.tvCouponType.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_green_color));
        } else { // Offline coupon = 1
            holder.tvCouponType.setText(mContext.getString(R.string.offline_coupon_title));
            holder.tvCouponType.setTextColor(ContextCompat.getColor(mContext, R.color.color_red));
        }

        holder.tvCouponTermsAndCondition.setOnClickListener(v -> mGetCouponDetails.onTermAndConditionClickListner(couponDetailsModel));

        holder.ivMinus.setOnClickListener(v -> {
            if (couponDetailsModel.getQty() > 0) {
                int noOfCoupon = couponDetailsModel.getQty() - 1;
                couponDetailsModel.setQty(noOfCoupon);
                mGetCouponDetails.onUpdateCart(couponDetailsModel, position);
            }
        });

        holder.ivPlus.setOnClickListener(v -> {
            int noOfCoupon = couponDetailsModel.getQty() + 1;
            couponDetailsModel.setQty(noOfCoupon);
            mGetCouponDetails.onUpdateCart(couponDetailsModel, position);
        });

        holder.tvCouponType.setOnClickListener(view -> mGetCouponDetails.onOnlineOfflineCouponClick(couponDetailsModel, position));

        holder.ivHelp.setOnClickListener(view -> mGetCouponDetails.onOnlineOfflineCouponClick(couponDetailsModel, position));
    }

    @Override
    public int getItemCount() {
        return mCouponModelList.size() > 0 ? mCouponModelList.size() : 0;
    }

    public interface GetCouponDetails {
        void onTermAndConditionClickListner(ClaimedCouponDetailsModel couponDetailsModel);

        void onUpdateCart(ClaimedCouponDetailsModel couponDetailsModel, int position);

        void onOnlineOfflineCouponClick(ClaimedCouponDetailsModel couponDetailsModel, int position);
    }
}
