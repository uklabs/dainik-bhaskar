package com.db.coupanmanagement.adapter;

import android.app.Activity;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.activity.CouponManagementActivity;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.dialog.CouponDialogUtils;
import com.db.coupanmanagement.model.MonthlyOfferPlanModel;
import com.db.coupanmanagement.utils.Utils;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by hp on 21-05-2018.
 */

public class MonthlyOffersRecyclerViewAdapter extends RecyclerView.Adapter<MonthlyOffersRecyclerViewAdapter.CouponsViewHolder> {

    private List<MonthlyOfferPlanModel> mMonthlyOfferPlanModelsList;
    private Activity mContext;
    private GetCouponDetails mGetCouponDetailsInterface;
    private String mCurrentDateTime;

    class CouponsViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relCoupon;
        private LinearLayout linOfferItemParent, linCountDown;
        private TextView tvMonth, tvCouponAmount, tvCouponValidity, tvCouponCountDownDays, tvCouponCountDownHours, tvCouponCountDownMinute, tvCouponCountDownSecond;
        private Button btnDetails;
        private ImageView ivStatus;
        private CountDownTimer countDownTimer;

        CouponsViewHolder(View itemView) {
            super(itemView);
            relCoupon = itemView.findViewById(R.id.rel_coupon);
            linOfferItemParent = itemView.findViewById(R.id.lin_offer_item_parent);
            linCountDown = itemView.findViewById(R.id.lin_countdown);
            tvMonth = itemView.findViewById(R.id.tv_month);
            tvCouponAmount = itemView.findViewById(R.id.tv_coupon_amount);
            tvCouponValidity = itemView.findViewById(R.id.tv_coupon_validity);
            tvCouponCountDownDays = itemView.findViewById(R.id.tv_days);
            tvCouponCountDownHours = itemView.findViewById(R.id.tv_hours);
            tvCouponCountDownMinute = itemView.findViewById(R.id.tv_minute);
            tvCouponCountDownSecond = itemView.findViewById(R.id.tv_second);
            btnDetails = itemView.findViewById(R.id.btn_details);
            ivStatus = itemView.findViewById(R.id.iv_status);

            countDownTimer = null;
        }
    }

    public MonthlyOffersRecyclerViewAdapter(Activity context, List<MonthlyOfferPlanModel> monthlyOfferPlanModelsList, String currentDateTime, GetCouponDetails getCouponDetailsInterface) {
        this.mContext = context;
        this.mMonthlyOfferPlanModelsList = monthlyOfferPlanModelsList;
        this.mCurrentDateTime = currentDateTime;
        this.mGetCouponDetailsInterface = getCouponDetailsInterface;
    }

    @NonNull
    @Override
    public CouponsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_monthly_offers_item_layout, parent, false);
        return new CouponsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CouponsViewHolder holder, int position) {
        holder.btnDetails.setVisibility(View.VISIBLE);

        final SpannableString[] claimedCouponAmountSpannable = {null};
        SpannableString claimedCouponDateSpannable;

        final MonthlyOfferPlanModel monthlyOfferPlanModel = mMonthlyOfferPlanModelsList.get(position);
        holder.tvMonth.setText(monthlyOfferPlanModel.getMonthName());
        holder.tvCouponAmount.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlck));
        holder.tvMonth.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlck));
        holder.tvCouponValidity.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_dark_blue_color));

        String couponDate;
        switch (monthlyOfferPlanModel.getMonthlyPlanStatusForUser()) {
            case 1:// Future offers to claim
                holder.btnDetails.setVisibility(View.GONE);
                holder.linCountDown.setVisibility(View.VISIBLE);

                // Get time in millis for running countdown
                long currentDateTimeInMillis = Utils.getMillisecondFromDateTime(mCurrentDateTime);
                long futureDateTimeInMillis = Utils.getMillisecondFromDateTime(monthlyOfferPlanModel.getPlanStartDate());
                long difference = futureDateTimeInMillis - currentDateTimeInMillis;
                long noOfDays = TimeUnit.HOURS.toDays(TimeUnit.MILLISECONDS.toHours(difference));
                // Get number of days in a month
                int noOfDaysInAMonth = Utils.getNoOfDaysInAMonth();
                if (noOfDays < noOfDaysInAMonth) {
                    holder.countDownTimer = new CountDownTimer(difference, CouponManagementConstants.CountDownTimerFormat.COUNTDOWN_TIMER_INTERVAL) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            if (mContext instanceof CouponManagementActivity) {
                                CouponManagementActivity couponManagementActivity = (CouponManagementActivity) mContext;
                                couponManagementActivity.runOnUiThread(() -> {
                                    // Days
                                    holder.tvCouponCountDownDays.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, noOfDays));
                                    // Hours
                                    long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished));
                                    holder.tvCouponCountDownHours.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, hours));
                                    // Minutes
                                    long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
                                    holder.tvCouponCountDownMinute.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, minutes));
                                    // Seconds
                                    long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                                    holder.tvCouponCountDownSecond.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, seconds));
                                });
                            }
                        }

                        @Override
                        public void onFinish() {
                            if (mContext instanceof CouponManagementActivity) {
                                CouponManagementActivity couponManagementActivity = (CouponManagementActivity) mContext;
                                couponManagementActivity.runOnUiThread(() -> {
                                    holder.tvCouponCountDownDays.setText(mContext.getString(R.string.zero_zero));
                                    holder.tvCouponCountDownHours.setText(mContext.getString(R.string.zero_zero));
                                    holder.tvCouponCountDownMinute.setText(mContext.getString(R.string.zero_zero));
                                    holder.tvCouponCountDownSecond.setText(mContext.getString(R.string.zero_zero));
                                    stopCountdown(holder.countDownTimer);
                                    holder.linCountDown.setVisibility(View.GONE);
                                    holder.btnDetails.setVisibility(View.VISIBLE);
                                    holder.tvMonth.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlck));
                                    monthlyOfferPlanModel.setMonthlyPlanStatusForUser(2);
                                    claimedCouponAmountSpannable[0] = renderReadyToClaimOffer(holder, monthlyOfferPlanModel);
                                });
                            }
                        }
                    }.start();
                } else {
                    // When noOfDays>30 then we'll show only days in timer without countdown
                    // Days
                    holder.tvCouponCountDownDays.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, noOfDays));
                    // Hours
                    holder.tvCouponCountDownHours.setText(mContext.getString(R.string.zero_zero));
                    // Minutes
                    holder.tvCouponCountDownMinute.setText(mContext.getString(R.string.zero_zero));
                    // Second
                    holder.tvCouponCountDownSecond.setText(mContext.getString(R.string.zero_zero));
                }

                holder.relCoupon.setAlpha(1f);
                holder.ivStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.coupon_clock));
                holder.ivStatus.setVisibility(View.VISIBLE);
                claimedCouponAmountSpannable[0] = Utils.getCouponAmount(mContext, ContextCompat.getColor(mContext, R.color.tab_selection), monthlyOfferPlanModel.getOfferAmount());

                holder.tvMonth.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_grey_color));

                holder.tvCouponAmount.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_dark_blue_color));

                holder.tvCouponValidity.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_grey_color));
                couponDate = Utils.parseDateToddMMMyyyy(monthlyOfferPlanModel.getPlanStartDate()) + " " + mContext.getString(R.string.available_from);
                holder.tvCouponValidity.setText(couponDate);
                holder.relCoupon.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhte));

                String finalCouponDate = couponDate;
                holder.linOfferItemParent.setOnClickListener(v -> {
                    // Show countdown timer dialog
                    if (noOfDays < noOfDaysInAMonth) {
                        new CouponDialogUtils().prepareCouponCountDownDialog(mContext, monthlyOfferPlanModel.getMonthName(), monthlyOfferPlanModel.getOfferAmount(), monthlyOfferPlanModel.getPlanStartDate(), finalCouponDate, mCurrentDateTime, true);
                    } else {
                        new CouponDialogUtils().prepareCouponCountDownDialog(mContext, monthlyOfferPlanModel.getMonthName(), monthlyOfferPlanModel.getOfferAmount(), monthlyOfferPlanModel.getPlanStartDate(), finalCouponDate, mCurrentDateTime, false);
                    }
                });
                break;
            case 2:// Ready to claim offers
                claimedCouponAmountSpannable[0] = renderReadyToClaimOffer(holder, monthlyOfferPlanModel);
                break;
            case 3:// Already claimed offers
                holder.linCountDown.setVisibility(View.GONE);
                if (((CouponManagementActivity) mContext).getAPIModel().isUserLoggedIn()) {
                    holder.btnDetails.setText(mContext.getString(R.string.details));
                    holder.btnDetails.setBackgroundResource(R.drawable.ripple_effect_green);
                    holder.btnDetails.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_green_color));
                    holder.tvCouponValidity.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_dark_blue_color));
                    holder.relCoupon.setAlpha(1f);

                    holder.ivStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.coupon_verified));
                    holder.ivStatus.setVisibility(View.VISIBLE);
                    DrawableCompat.setTint(holder.ivStatus.getDrawable(), ContextCompat.getColor(mContext, R.color.coupon_green_color));

                    claimedCouponAmountSpannable[0] = Utils.getCouponAmount(mContext, ContextCompat.getColor(mContext, R.color.coupon_green_color), monthlyOfferPlanModel.getOfferAmount());
                    holder.tvCouponAmount.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_dark_blue_color));

                    couponDate = mContext.getString(R.string.claimed_date) + " - " + Utils.parseClaimedDateToddMMMyyyy(monthlyOfferPlanModel.getUserClaimedDate());
                    claimedCouponDateSpannable = Utils.getHighLightedText(ContextCompat.getColor(mContext, R.color.coupon_dull_blue_color), couponDate, "-");
                    holder.tvCouponValidity.setText(claimedCouponDateSpannable, TextView.BufferType.SPANNABLE);
                    holder.relCoupon.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhte));
                } else {
                    claimedCouponAmountSpannable[0] = renderReadyToClaimOffer(holder, monthlyOfferPlanModel);
                }
                break;
            case 4:// Expired offers
                holder.linCountDown.setVisibility(View.GONE);

                if (((CouponManagementActivity) mContext).getAPIModel().isUserLoggedIn()) {
                    holder.btnDetails.setText(mContext.getString(R.string.details));
                } else {
                    holder.btnDetails.setText(mContext.getString(R.string.login_to_claim));
                }
                holder.btnDetails.setBackgroundResource(R.drawable.ripple_effect_grey);
                holder.btnDetails.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_grey_color));
                holder.relCoupon.setAlpha(0.3f);

                claimedCouponAmountSpannable[0] = Utils.getCouponAmount(mContext, ContextCompat.getColor(mContext, R.color.coupon_grey_color), monthlyOfferPlanModel.getOfferAmount());

                holder.tvCouponAmount.setTextColor(ContextCompat.getColor(mContext, R.color.coupon_dark_blue_color));

                holder.tvCouponValidity.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlck));
                String expiryDate = mContext.getString(R.string.expired_plan_message, Utils.parseDateToddMMMyyyy(monthlyOfferPlanModel.getExpiryDate()));
                holder.tvCouponValidity.setText(expiryDate);
                holder.ivStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.coupon_error));
                holder.ivStatus.setVisibility(View.VISIBLE);

                holder.relCoupon.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhte));

                holder.linOfferItemParent.setOnClickListener(v -> {
                    // Show expired offer message dialog
                    new CouponDialogUtils().prepareExpiredMessageDialog(mContext, monthlyOfferPlanModel.getMonthName(), monthlyOfferPlanModel.getOfferAmount(), expiryDate);
                });
                break;
        }
        holder.tvCouponAmount.setText(claimedCouponAmountSpannable[0], TextView.BufferType.SPANNABLE);

        holder.btnDetails.setOnClickListener(v -> {
            if (monthlyOfferPlanModel.getMonthlyPlanStatusForUser() == 2) {
                mGetCouponDetailsInterface.onClaimButtonCLickListner(monthlyOfferPlanModel);
            } else if (monthlyOfferPlanModel.getMonthlyPlanStatusForUser() == 3) {
                if (((CouponManagementActivity) mContext).getAPIModel().isUserLoggedIn()) {
                    mGetCouponDetailsInterface.onDetailsButtonCLickListner(monthlyOfferPlanModel);
                } else {
                    mGetCouponDetailsInterface.onClaimButtonCLickListner(monthlyOfferPlanModel);
                }
            }
        });
    }

    /**
     * This method will be used to render ready to claim offer
     *
     * @param holder
     * @param monthlyOfferPlanModel
     * @return
     */
    private SpannableString renderReadyToClaimOffer(CouponsViewHolder holder, MonthlyOfferPlanModel monthlyOfferPlanModel) {
        SpannableString claimedCouponAmountSpannable, claimedCouponDateSpannable;

        holder.linCountDown.setVisibility(View.GONE);

        holder.btnDetails.setEnabled(true);
        if (((CouponManagementActivity) mContext).getAPIModel().isUserLoggedIn())
            holder.btnDetails.setText(mContext.getString(R.string.claim));
        else
            holder.btnDetails.setText(mContext.getString(R.string.login_to_claim));

        holder.btnDetails.setBackgroundResource(R.drawable.ripple_effect_white);
        holder.btnDetails.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhte));
        holder.tvCouponValidity.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlck));
        holder.relCoupon.setAlpha(1f);

        holder.ivStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.coupon_verified));
        holder.ivStatus.setVisibility(View.INVISIBLE);
        DrawableCompat.setTint(holder.ivStatus.getDrawable(), ContextCompat.getColor(mContext, R.color.coupon_grey_color));

        claimedCouponAmountSpannable = Utils.getCouponAmount(mContext, ContextCompat.getColor(mContext, R.color.colorWhte), monthlyOfferPlanModel.getOfferAmount());

        holder.tvCouponAmount.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhte));

        String couponDate = mContext.getString(R.string.expiry_date) + " - " + Utils.parseDateToddMMMyyyy(monthlyOfferPlanModel.getExpiryDate());
        claimedCouponDateSpannable = Utils.getHighLightedText(ContextCompat.getColor(mContext, R.color.colorWhte), couponDate, "-");
        holder.tvCouponValidity.setText(claimedCouponDateSpannable, TextView.BufferType.SPANNABLE);
        holder.relCoupon.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorCouponPrimary));
        return claimedCouponAmountSpannable;
    }

    //Stop Countdown method
    private void stopCountdown(CountDownTimer countDownTimer) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }


    @Override
    public int getItemCount() {
        return mMonthlyOfferPlanModelsList.size() > 0 ? mMonthlyOfferPlanModelsList.size() : 0;
    }

    public interface GetCouponDetails {
        void onDetailsButtonCLickListner(MonthlyOfferPlanModel monthlyOfferPlanModel);

        void onClaimButtonCLickListner(MonthlyOfferPlanModel monthlyOfferPlanModel);
    }
}
