package com.db.coupanmanagement.constant;

/**
 * Created by hp on 25-06-2018.
 */

public interface CouponManagementConstants {

    String URL_COUPON_BASE = "https://cldmum.bhaskar.com/";
//    String URL_COUPON_BASE = "http://digital.dbnewshub.com/";
//    String URL_COUPON_BASE = "http://stage-digital.dbnewshub.com/";

    int API_SUCCESS = 200; // Success code
    int API_INTERNAL_SERVER_ERROR = 500;
    int API_BAD_REQUEST = 400; // Bad request
    int API_UNAUTHORIZED_ACCESS = 401; // Unauthorised access
    int API_NOT_FOUND = 404; // Error not found
    int RESEND_OTP = 60000; // Resend OTP enable time (01 min)
    int FRAGMENT_BACK_STACK_FLAG = 1;
    String COPY_CODE_LABLE = "copy_code";
    String DIALOG_TAG = "dialog";
    String BEARER = "Bearer ";
    int PERMISSIONS_REQUEST_LOCATION = 1021;
    //    int PERMISSIONS_REQUEST_READ_SMS = 1022;
    int AUTO_VERIFY_OTP_DELAY = 1000;
    int LOGIN_TO_CLAIM_COUPON_REQUEST = 1023;
    int COUPON_FLAG = 1000;
    int COUPON_FLAG_SKIP = 1001;
    String DAINIK_BHASKAR_OTP_SENDER = "BHASKR";
    String APP_HOME_ACTIVITY = "com.db.MainActivity";
    String APP_ONBOARDING_ACTIVITY = "com.db.MainActivity";
    int deviceWidth = 240;
    int deviceHeight = 320;
    int deviceWidthToBe = 50;
    String PREF_KEY_UNIQUE_DEVICE_ID = "unique_device_id_for_first_launch";
    String IS_GUEST_USER_API_DONE = "is_guest_user_api_done";
    int OTP_MESSAGE_LENGTH = 6;

    interface HeaderKeys {
        String KEY_AUTHORIZATION = "Authorization";
        String KEY_CHANNEL = "Channel";
        String KEY_DEVICE_TYPE = "Device-Type";
        String KEY_ACCEPT = "Accept";
        String KEY_DB_ID = "DB-Id";
        String KEY_CONTENT_TYPE = "Content-Type";
        String KEY_X_CSRF_TOKEN = "X-CSRF-Token";
        String KEY_UUID = "uuid";
        String KEY_APP_ID = "app_id";
        String KEY_APP_KEY = "api-key";
        String KEY_UID = "uid";
        String KEY_DEVICE_ID = "device-id";
    }

    interface HeaderValues {
        String VALUE_CHANNEL = "MOBILE";
        String VALUE_DEVICE_TYPE = "ANDROID";
        String VALUE_ACCEPT = "application/json";
        String VALUE_CONTENT_TYPE = "application/json";
    }

    interface NetworkRequestEndPoint {
        String GUEST_USER = "coupon/guestUsers/";
        String OFFERS = "coupon/offers/";
        String PLANS = "coupon/plans/";
        String COUPONS = "coupon/coupons/";
        String CLAIMED_REWARD_DETAILS = "coupon/user-claimed-reward-details/";
        String GET_COUPON_CODES = "coupon/get-coupon-codes/";
        String GET_OTP = "coupon/api/sms/otp/";
        String OTP_VERIFICATION = "coupon/api/sms/otp/verification/";
        String CONFIRM_OFFLINE_COUPON = "coupon/user-confirm-offline-coupon/";
        String INSTALL_USER_FIRST_LAUNCH = "coupon/register-guest";
        String URL_GET_COUPON_AVAILABLE = "coupon/check-offer-user-status?q=applaunch&uuid=";
    }

    interface APIModelData {
        String API_MODEL = "api_model";
    }

    interface GetFragmentName {
        String AVAILABLE_OFFERS_WALK_THROUGH_FRAGMENT = "AvailableOffersWalkThroughFragment";
        String MONTHLY_OFFERS_FRAGMENT = "MonthlyOffersFragment";
        String MONTHLY_COUPONS_FRAGMENT = "MonthlyCouponsFragment";
        String CLAIMED_COUPONS_FRAGMENT = "ClaimedCouponCodeFragment";
    }

    interface GetPreferenceKeys {
        String LATTITUDE = "latitude";
        String LONGITUDE = "longitude";
        String CITY = "city";
        String EMAILID = "email_id";
        String MOBILE_NUMBER = "mobile_number";
        String EXIT_DIALOG_COUNT = "exitDialogCount";
    }

    interface GetBundleKeys {
        String DESCRIPTION = "description";
        String TERM_AND_CONDITION = "term_and_condition";
        String FAQ_URL = "faq_url";
        String AVAILABLE_OFFER_MODEL = "available_offer_model";
        String TITLE = "title";
        String OFFER_ID = "offer_id";
        String PLAN_ID = "plan_id";
        String MONTH_ID = "month_id";
        String YEAR = "year";
        String REDIRECT_LINK = "redirect_link";
        String IMAGE_URL = "image_url";
        String OFFER_MONTH_AMOUNT = "offer_month_amount";
        String OFFER_AMOUNT = "offer_amount";
        String MONTH_NAME = "month_name";
        // Below keys will be used in coupon flow when we'll navigate from splash -> chatintro -> coupon
        String KEY_UID = "key_uid";
        String KEY_UUID = "key_uuid";
        String KEY_EMAIL_ID = "key_email_id";
        String KEY_DB_ID = "key_db_id";
        String KEY_AUTH_TOKEN = "key_auth_token";
        String KEY_MOBILE_NO = "key_mobile_no";
        String KEY_DEVICE_ID = "key_device_id";
        String KEY_IS_COMING_FROM_HAMBURGER_MENU = "key_is_coming_from_hamburger_menu";
        String KEY_LAUNCH_LOGIN_TYPE = "launch_login_type";
        String KEY_TOOLBAR_TITLE = "key_toolbar_title";
        String KEY_IS_USER_LOGGED_IN = "key_is_user_logged_in";
        String KEY_APP_ID = "app_id";
        String KEY_APP_KEY = "app_key";
    }

    interface CountDownTimerFormat {
        String MONTHLY_OFFER_COUNTDOWN_FORMAT = "%02d";
        String RESEND_OTP_COUNTDOWN_FORMAT = "%02d:%02d";
        int COUNTDOWN_TIMER_INTERVAL = 1000;
    }

    interface DateTimeFormats {
        String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
        String DD_MMMM_YYYY = "dd MMMM yyyy";
        String YYYY_MM_DD = "yyyy-MM-dd";
        String HI_LOCALE = "hi";
        String GU_LOCALE = "gu";
        String IN_COUNTRY = "in";
    }

    interface OfflineCouponData {
        int OFFLINE_COUPON_VIEWED = 1; // 1 for offline coupon. Already viewed by user & shopkeeper
        int IS_OFFLINE_COUPON = 2; // Online Coupon = 1, Offline Coupon = 2
    }

    // Google Analytics Constants
    String GA_TAG = "Tracking : GA";
    String GA_APPLICATION_METHOD = "getDefaultTracker";
    String GA_CURRENT_SCREEN_TAG = "CurrentScreen_";
    String GA_NEXT_SCREEN_TAG = "NextScreen_";

    interface GAEventCategory {
        String MONTHLY_OFFERS = "MonthlyOffers";
        String NEXT_SCREEN = "NextScreen";
    }

    interface GAAction {
        String REWARD_ASSIGNED = "RewardsAssigned";
        String OFFER_CLAIMED = "OfferClaimed";
        String LOGIN_TO_CLAIM = "LoginToClaim";
    }

    interface GALabel {
        String SUCCESSFUL = "Successful";
        String LOGGED_IN = "LoggedIn";
        String GUEST = "Guest";
    }

    interface GAScreenName {
        String HOME = "Home";
        String MERA_PAGE = "Mera Page";
        String MYOFFERS = "MyOffers";
        String MYOFFERS_MORE_INFO = "MyOffers_MoreInfo";
        String MYOFFERS_TERM_AND_CONDITIONS = "MyOffers_TermsAndConditions";
        String MYOFFERS_VIEW_OFFERS = "MyOffers_ViewOffers";
        String MYOFFERS_MONTHLY_OFFERS = "MyOffers_MonthlyOffers";
        String MYOFFERS_CLAIM_OFFERS = "MyOffers_ClaimOffer";
        String MYOFFERS_SMS_PERMISSION = "MyOffers_ClaimOffer_smsPermission";
        String MYOFFERS_SMS_PERMISSION_OK = "MyOffers_ClaimOffer_smsPermission_OK";
        String MYOFFERS_ENTER_MOBILE_NUMBER = "MyOffers_ClaimOffer_EnterMobileNumber";
        String MYOFFERS_ENTER_MOBILE_NUMBER_CANCEL = "MyOffers_ClaimOffer_EnterMobileNumber_Cancel";
        String MYOFFERS_ENTER_MOBILE_NUMBER_OK = "MyOffers_ClaimOffer_EnterMobileNumber_OK";
        String MYOFFERS_ENTER_OTP = "MyOffers_ClaimOffer_EnterOTP";
        String MYOFFERS_ENTER_OTP_RESEND = "MyOffers_ClaimOffer_EnterOTP_Resend";
        String MYOFFERS_ENTER_OTP_CANCEL = "MyOffers_ClaimOffer_EnterOTP_Cancel";
        String MYOFFERS_ENTER_OTP_OK = "MyOffers_ClaimOffer_EnterOTP_OK";
        String MYOFFERS_CLAIMED_SUCCESSFULLY = "MyOffers_ClaimedSuccessfully";
    }

    interface GAScreenCategory {
        String MYOFFERS = "MyOffers";
        String MYOFFERS_CLAIM_OFFER = "MyOffers_ClaimOffer";
        String MYOFFERS_CLAIMED_SUCCESSFULLY = "MyOffers_ClaimedSuccessfully";
    }

    interface GADimensions {
        int CONTENT_CATEGORY = 6;
        int LOGIN_STATUS = 7;
        int DB_USER_ID = 8; // send uid
        int PREVIOUS_SCREEN = 9;
        int CURRENT_NEXT_SCREEN = 10;
        int DB_ID = 12; // Send dbId
    }
}
