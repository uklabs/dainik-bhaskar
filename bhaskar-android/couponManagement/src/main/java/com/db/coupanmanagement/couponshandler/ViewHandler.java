package com.db.coupanmanagement.couponshandler;

/**
 * Created by hp on 21-06-2018.
 */

public interface ViewHandler {
    void networkConnectivityError();

    void networkConnectivitySuccess();
}
