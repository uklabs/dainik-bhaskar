package com.db.coupanmanagement.couponshandler;

import android.content.Context;

/**
 * Created by hp on 20-06-2018.
 */

public interface ViewInteractor {
    interface OnNetworkConnetivityListener {
        void onNetworkConnectionError();

        void onNetworkConnectionSuccess();
    }

    void isNetworkConnectivityAvailable(Context context, OnNetworkConnetivityListener listener);
}
