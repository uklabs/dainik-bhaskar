package com.db.coupanmanagement.couponshandler;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by hp on 20-06-2018.
 */

public class ViewInteractorImpl implements ViewInteractor {
    @Override
    public void isNetworkConnectivityAvailable(Context context, OnNetworkConnetivityListener listener) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            if (cm.getActiveNetworkInfo() != null) {
                listener.onNetworkConnectionSuccess();
            } else {
                listener.onNetworkConnectionError();
            }
        }
    }
}
