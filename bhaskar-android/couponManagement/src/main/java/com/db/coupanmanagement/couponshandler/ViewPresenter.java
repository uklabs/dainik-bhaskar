package com.db.coupanmanagement.couponshandler;

import android.content.Context;

/**
 * Created by hp on 21-06-2018.
 */

public interface ViewPresenter {
    void validateNetworkConnectivity(Context context);
}
