package com.db.coupanmanagement.couponshandler;

import android.content.Context;

/**
 * Created by hp on 21-06-2018.
 */

public class ViewPresenterImpl implements ViewPresenter, ViewInteractor.OnNetworkConnetivityListener {

    private ViewHandler mViewHandler;
    private ViewInteractor mViewInteractor;

    public ViewPresenterImpl(ViewHandler viewHandler, ViewInteractor viewInteractor) {
        this.mViewHandler = viewHandler;
        this.mViewInteractor = viewInteractor;
    }

    @Override
    public void validateNetworkConnectivity(Context context) {
        if (mViewHandler != null)
            mViewInteractor.isNetworkConnectivityAvailable(context, this);
    }

    @Override
    public void onNetworkConnectionError() {
        if (mViewHandler != null)
            mViewHandler.networkConnectivityError();
    }

    @Override
    public void onNetworkConnectionSuccess() {
        if (mViewHandler != null)
            mViewHandler.networkConnectivitySuccess();
    }
}
