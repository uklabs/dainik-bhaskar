package com.db.coupanmanagement.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.db.coupanmanagement.R;
import com.plattysoft.leonids.ParticleSystem;

import java.util.Objects;

public class CouponClaimSuccessDialog extends Dialog {

    private String mTitle, mMessage, mBtnText;
    private OnTheekHaiClickListener mOnTheekHaiClickListener;

    /**
     * @param context
     */
    public CouponClaimSuccessDialog(@NonNull Context context, String title, String message, String btnText, OnTheekHaiClickListener onTheekHaiClickListener) {
        super(context, R.style.CouponClaimSuccessDialog);
        mTitle = title;
        mMessage = message;
        mBtnText = btnText;
        mOnTheekHaiClickListener = onTheekHaiClickListener;
        setCancelable(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_coupon_claim_success, null);

        setContentView(view);

        TextView tvTitle = findViewById(R.id.tv_title);
        tvTitle.setText(mTitle);
        TextView tvMessage = findViewById(R.id.tv_message);
        tvMessage.setText(mMessage);
        TextView tvTheekHai = findViewById(R.id.tv_theek_hai);
        tvTheekHai.setText(mBtnText);

        tvTheekHai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnTheekHaiClickListener != null) {
                    mOnTheekHaiClickListener.onTheekHai();
                }
                dismiss();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Start particle animation
                startParticleAnimation(getContext(), view);
            }
        }, 1000);
    }

    /**
     * This method is used to show particle animation
     */
    private void startParticleAnimation(Context context, View view) {
        final ParticleSystem psTopRight = new ParticleSystem((ViewGroup) view, 80, Objects.requireNonNull(ContextCompat.getDrawable(context, R.drawable.ic_heart_1)), 10000);
        psTopRight.setSpeedModuleAndAngleRange(0f, 0.1f, 180, 180).setRotationSpeed(141)
                .setAcceleration(0.000050f, 90)
                .emit(view.findViewById(R.id.emiter_top_right), 8);

        final ParticleSystem psTopLeft = new ParticleSystem((ViewGroup) view, 80, Objects.requireNonNull(ContextCompat.getDrawable(context, R.drawable.ic_heart_4)), 10000);
        psTopLeft.setSpeedModuleAndAngleRange(0f, 0.1f, 0, 0)
                .setRotationSpeed(141).setAcceleration(0.000050f, 90)
                .emit(view.findViewById(R.id.emiter_top_left), 8);

        final ParticleSystem psBottomRight = new ParticleSystem((ViewGroup) view, 80, Objects.requireNonNull(ContextCompat.getDrawable(context, R.drawable.ic_heart_3)), 10000);
        psBottomRight.setSpeedModuleAndAngleRange(0f, 0.1f, 180, 180)
                .setRotationSpeed(141).setAcceleration(0.000050f, -90)
                .emit(view.findViewById(R.id.emiter_bottom_right), 8);

        final ParticleSystem psBottomLeft = new ParticleSystem((ViewGroup) view, 80, Objects.requireNonNull(ContextCompat.getDrawable(context, R.drawable.ic_heart_2)), 10000);
        psBottomLeft.setSpeedModuleAndAngleRange(0f, 0.1f, 0, 0)
                .setRotationSpeed(141).setAcceleration(0.000050f, -90)
                .emit(view.findViewById(R.id.emiter_bottom_left), 8);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                psTopRight.stopEmitting();
                psTopLeft.stopEmitting();
                psBottomRight.stopEmitting();
                psBottomLeft.stopEmitting();
            }
        }, 20000);
    }

    public interface OnTheekHaiClickListener {
        void onTheekHai();
    }
}
