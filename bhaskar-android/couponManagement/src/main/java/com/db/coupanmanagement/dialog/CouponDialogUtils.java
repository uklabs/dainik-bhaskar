package com.db.coupanmanagement.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.activity.CouponManagementActivity;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.utils.Utils;

import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class CouponDialogUtils {

    public void showErrorDialog(CouponManagementActivity activity, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.coupon_app_name));
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok_msg_btn), (dialog, which) -> {
        });
        if (!activity.isFinishing()) {
            builder.show();
        }
    }

    /**
     * This method will be sued to show single action button with normal message
     *
     * @param activity
     * @param title
     * @param message
     * @param isCancelable
     * @param btnText
     * @param singleButtonDialogResponsesInterface
     */
    public void showNormalMessageWithOneActionButtonDialog(final Activity activity,
                                                           final String title, final String message,
                                                           final boolean isCancelable,
                                                           final String btnText,
                                                           final SingleButtonDialogResponsesInterface singleButtonDialogResponsesInterface) {

        try {
            Dialog normalMessageDialog = new Dialog(activity);
            normalMessageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            normalMessageDialog.setCancelable(isCancelable);
            Objects.requireNonNull(normalMessageDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
            normalMessageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            normalMessageDialog.setContentView(R.layout.dialog_normal_message_with_one_action_button);

            ImageView ivClose = normalMessageDialog.findViewById(R.id.iv_close);
            TextView tvTitle = normalMessageDialog.findViewById(R.id.tv_title);
            tvTitle.setText(title);
            TextView tvMessage = normalMessageDialog.findViewById(R.id.tv_normal_message);
            tvMessage.setText(message);
            Button btnOk = normalMessageDialog.findViewById(R.id.btn_ok);
//            btnOk.setText(btnText);

            if (!normalMessageDialog.isShowing())
                normalMessageDialog.show();

            ivClose.setOnClickListener(view -> {
                normalMessageDialog.dismiss();
            });

            btnOk.setOnClickListener(view -> {
                if (singleButtonDialogResponsesInterface != null) {
                    singleButtonDialogResponsesInterface.doOnOkButtonClick(activity);
                }
                normalMessageDialog.dismiss();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface SingleButtonDialogResponsesInterface {
        void doOnOkButtonClick(Activity activity);
    }

    /**
     * This method will be sued to show two action buttons with normal message
     *
     * @param activity
     * @param title
     * @param message
     * @param isCancelable
     * @param btn1Text
     * @param btn2Text
     * @param normalMessageDialogResponsesInterface
     */
    public void showNormalMessageWithTwoActionButtonDialog(final Activity activity,
                                                           final String title, final String message,
                                                           final boolean isCancelable,
                                                           final String btn1Text, final String btn2Text,
                                                           final NormalMessageDialogResponsesInterface normalMessageDialogResponsesInterface) {

        try {
            final Dialog normalMessageDialog = new Dialog(activity);
            normalMessageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            normalMessageDialog.setCancelable(isCancelable);
            Objects.requireNonNull(normalMessageDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
            normalMessageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            normalMessageDialog.setContentView(R.layout.dialog_normal_message_with_two_action_button);

//            ImageView ivClose = normalMessageDialog.findViewById(R.id.iv_close);
            TextView tvTitle = normalMessageDialog.findViewById(R.id.tv_title);
            tvTitle.setText(title);
            TextView tvMessage = normalMessageDialog.findViewById(R.id.tv_normal_message);
            tvMessage.setText(message);
            Button btnTurnOn = normalMessageDialog.findViewById(R.id.btn_turn_on);
            btnTurnOn.setText(btn1Text);
            Button btnSkip = normalMessageDialog.findViewById(R.id.btn_skip);
            btnSkip.setText(btn2Text);

            if (!normalMessageDialog.isShowing())
                normalMessageDialog.show();

            /*ivClose.setOnClickListener(view -> {
                normalMessageDialog.dismiss();
            });*/

            btnTurnOn.setOnClickListener(view -> {
                if (normalMessageDialogResponsesInterface != null) {
                    normalMessageDialogResponsesInterface.doOnOkButtonClick(activity);
                }
                normalMessageDialog.dismiss();
            });

            btnSkip.setOnClickListener(view -> {
                if (normalMessageDialogResponsesInterface != null) {
                    normalMessageDialogResponsesInterface.doOnSkipButtonClick(activity);
                }
                normalMessageDialog.dismiss();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface NormalMessageDialogResponsesInterface {
        void doOnOkButtonClick(Activity activity);

        void doOnSkipButtonClick(Activity activity);
    }

    /**
     * This method will be used to prepare dialog
     */
    public void prepareTermsAndConditionDialog(Activity context, Bundle extras) {
        final Dialog termsAndConditionDialog = new Dialog(context);
        termsAndConditionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        termsAndConditionDialog.setCancelable(true);
        Objects.requireNonNull(termsAndConditionDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        termsAndConditionDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        termsAndConditionDialog.setContentView(R.layout.dialog_terms_and_condition);
        ImageView ivClose = termsAndConditionDialog.findViewById(R.id.iv_close);
        TextView tvTermsAndCondition = termsAndConditionDialog.findViewById(R.id.tv_terms_and_condition);
        TextView tvDescription = termsAndConditionDialog.findViewById(R.id.tv_description);

        if (extras != null) {
            String description = extras.getString(CouponManagementConstants.GetBundleKeys.DESCRIPTION);
            String termsAndCondition = extras.getString(CouponManagementConstants.GetBundleKeys.TERM_AND_CONDITION);

            Spanned spannedDescriptionText = Html.fromHtml("<b><u>" + context.getString(R.string.description) + ":" + "</u></b> " + description + "");
            tvDescription.setText(spannedDescriptionText);
            Spanned spannedTermsAndConditionText = Html.fromHtml("<b><u>" + context.getString(R.string.term_and_condition) + ":" + "</u></b> " + termsAndCondition + "");
            tvTermsAndCondition.setText(spannedTermsAndConditionText);
        }

        if (!termsAndConditionDialog.isShowing()) {
            termsAndConditionDialog.show();
        }

        ivClose.setOnClickListener(v -> termsAndConditionDialog.dismiss());
    }

    /**
     * This method will be used to prepare dialog
     */
    public void prepareTermsAndConditionDialogForFaq(Activity context, Bundle extras) {
        final Dialog termsAndConditionDialog = new Dialog(context);
        termsAndConditionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        termsAndConditionDialog.setCancelable(true);
        Objects.requireNonNull(termsAndConditionDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        termsAndConditionDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        termsAndConditionDialog.setContentView(R.layout.dialog_terms_and_condition_faq);
        ImageView ivClose = termsAndConditionDialog.findViewById(R.id.iv_close);
        TextView tvTermsAndCondition = termsAndConditionDialog.findViewById(R.id.tv_terms_and_condition);
        TextView tvDescription = termsAndConditionDialog.findViewById(R.id.tv_description);

        if (extras != null) {
            String description = extras.getString(CouponManagementConstants.GetBundleKeys.DESCRIPTION);
            String termsAndCondition = extras.getString(CouponManagementConstants.GetBundleKeys.TERM_AND_CONDITION);
            String faqUrl = extras.getString(CouponManagementConstants.GetBundleKeys.FAQ_URL);

            Spanned spannedDescriptionText = Html.fromHtml("<b><u>" + context.getString(R.string.description) + ":" + "</u></b> " + description + "");
            tvDescription.setText(spannedDescriptionText);
            Spanned spannedTermsAndConditionText = Html.fromHtml("<b><u>" + context.getString(R.string.term_and_condition) + ":" + "</u></b> " + termsAndCondition + "");
            tvTermsAndCondition.setText(spannedTermsAndConditionText);

            if (!termsAndConditionDialog.isShowing()) {
                termsAndConditionDialog.show();
            }

            if (!TextUtils.isEmpty(faqUrl)) {
                TextView tvSahayata = termsAndConditionDialog.findViewById(R.id.tv_sahayata);
                tvSahayata.setVisibility(View.VISIBLE);

                tvSahayata.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                            builder.setToolbarColor(ContextCompat.getColor(context, R.color.text_color_orange));
                            CustomTabsIntent customTabsIntent = builder.build();
                            customTabsIntent.launchUrl(context, Uri.parse(faqUrl));
                        } catch (Exception e) {
                            Log.e("Error", "Error: " + e.getMessage() + " >> " + faqUrl);
                        }
                    }
                });
            }
        }

        ivClose.setOnClickListener(v -> termsAndConditionDialog.dismiss());
    }

    private CountDownTimer countDownTimer;

    /**
     * This method will be used to prepare dialog
     *
     * @param context
     * @param monthName
     * @param couponAmount
     * @param planStartDate
     * @param availableFrom
     * @param currentDateTime
     * @param isCountDownOn
     */
    public void prepareCouponCountDownDialog(Activity context, String monthName, float couponAmount, String planStartDate, String availableFrom, String currentDateTime, boolean isCountDownOn) {
        final Dialog countDownDialog = new Dialog(context);
        countDownDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(countDownDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        countDownDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        countDownDialog.setContentView(R.layout.dialog_coupon_countdown);
        TextView tvCouponMonth = countDownDialog.findViewById(R.id.tv_month);
        TextView tvCouponAmount = countDownDialog.findViewById(R.id.tv_coupon_amount);
        TextView tvDays = countDownDialog.findViewById(R.id.tv_days);
        TextView tvHours = countDownDialog.findViewById(R.id.tv_hours);
        TextView tvMinutes = countDownDialog.findViewById(R.id.tv_minute);
        TextView tvSecond = countDownDialog.findViewById(R.id.tv_second);
        TextView tvAvailableFrom = countDownDialog.findViewById(R.id.tv_coupon_available_from);
        ImageView ivClose = countDownDialog.findViewById(R.id.iv_close);

        tvCouponMonth.setText(monthName);
        tvCouponAmount.setText(Utils.formatFloatValue(context, couponAmount));
        tvAvailableFrom.setText(availableFrom);

        // Get time in millis for running countdown
        long currentDateTimeInMillis = Utils.getMillisecondFromDateTime(currentDateTime);
        long futureDateTimeInMillis = Utils.getMillisecondFromDateTime(planStartDate);
        long difference = futureDateTimeInMillis - currentDateTimeInMillis;
        if (isCountDownOn) {
            countDownTimer = new CountDownTimer(difference, CouponManagementConstants.CountDownTimerFormat.COUNTDOWN_TIMER_INTERVAL) {
                @Override
                public void onTick(long millisUntilFinished) {
                    if (context instanceof CouponManagementActivity) {
                        CouponManagementActivity couponManagementActivity = (CouponManagementActivity) context;
                        couponManagementActivity.runOnUiThread(() -> {
                            // Days
                            long days = TimeUnit.HOURS.toDays(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
                            tvDays.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, days));
                            // Hours
                            long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished));
                            tvHours.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, hours));
                            // Minutes
                            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
                            tvMinutes.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, minutes));
                            // Seconds
                            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                            tvSecond.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, seconds));
                        });
                    }
                }

                @Override
                public void onFinish() {
                    if (context instanceof CouponManagementActivity) {
                        CouponManagementActivity couponManagementActivity = (CouponManagementActivity) context;
                        couponManagementActivity.runOnUiThread(() -> {
                            tvDays.setText(context.getString(R.string.zero_zero));
                            tvHours.setText(context.getString(R.string.zero_zero));
                            tvMinutes.setText(context.getString(R.string.zero_zero));
                            tvSecond.setText(context.getString(R.string.zero_zero));
                            stopCountdown();
                        });
                    }
                }
            }.start();
        } else {
            // Days
            long days = TimeUnit.HOURS.toDays(TimeUnit.MILLISECONDS.toHours(difference));
            tvDays.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, days));
            // Hours
            tvHours.setText(context.getString(R.string.zero_zero));
            // Minutes
            tvMinutes.setText(context.getString(R.string.zero_zero));
            // Seconds
            tvSecond.setText(context.getString(R.string.zero_zero));
        }

        if (!countDownDialog.isShowing())
            countDownDialog.show();

        ivClose.setOnClickListener(v -> {
            stopCountdown();
            countDownDialog.dismiss();
        });
    }

    /**
     * This method will be used to show expired offer plan message
     *
     * @param context
     * @param monthName
     * @param couponAmount
     * @param expiredMessage
     */
    public void prepareExpiredMessageDialog(Activity context, String monthName, float couponAmount, String expiredMessage) {
        final Dialog expiredMessageDialog = new Dialog(context);
        expiredMessageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(expiredMessageDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        expiredMessageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        expiredMessageDialog.setContentView(R.layout.dialog_expired_message);
        TextView tvCouponMonth = expiredMessageDialog.findViewById(R.id.tv_month);
        TextView tvCouponAmount = expiredMessageDialog.findViewById(R.id.tv_coupon_amount);
        TextView tvExpiredMessage = expiredMessageDialog.findViewById(R.id.tv_expired_message);
        ImageView ivClose = expiredMessageDialog.findViewById(R.id.iv_close);

        tvCouponMonth.setText(monthName);
        tvCouponAmount.setText(Utils.formatFloatValue(context, couponAmount));
        tvExpiredMessage.setText(expiredMessage);

        if (!expiredMessageDialog.isShowing())
            expiredMessageDialog.show();

        ivClose.setOnClickListener(v -> expiredMessageDialog.dismiss());
    }

    /**
     * This method will be used to show online/offline coupon message
     *
     * @param context
     * @param isOffline
     */
    public void prepareOnlineOfflineCouponMessageDialog(Activity context, int isOffline) {
        final Dialog onLineOfflineCouponMessageDialog = new Dialog(context);
        onLineOfflineCouponMessageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(onLineOfflineCouponMessageDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        onLineOfflineCouponMessageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        onLineOfflineCouponMessageDialog.setContentView(R.layout.dialog_online_offline_coupon_message);
        TextView tvCouponType = onLineOfflineCouponMessageDialog.findViewById(R.id.tv_coupon_type);
        TextView tvCouponTypeMessage = onLineOfflineCouponMessageDialog.findViewById(R.id.tv_coupon_type_message);
        ImageView ivClose = onLineOfflineCouponMessageDialog.findViewById(R.id.iv_close);
        Button btnOk = onLineOfflineCouponMessageDialog.findViewById(R.id.btn_ok);

        if (isOffline == 2) { // Online coupon = 2
            tvCouponType.setText(context.getString(R.string.online_coupon_title));
            tvCouponTypeMessage.setText(context.getString(R.string.online_coupon_msg));
        } else { // Offline coupon = 1
            tvCouponType.setText(context.getString(R.string.offline_coupon_title));
            tvCouponTypeMessage.setText(context.getString(R.string.offline_coupon_msg));
        }

        if (!onLineOfflineCouponMessageDialog.isShowing())
            onLineOfflineCouponMessageDialog.show();

        ivClose.setOnClickListener(v -> onLineOfflineCouponMessageDialog.dismiss());

        btnOk.setOnClickListener(v -> onLineOfflineCouponMessageDialog.dismiss());
    }

    /**
     * This method will be used to show online/offline coupon message
     *
     * @param context
     */
    public void prepareCodeDekhLiyaHaiDialog(Activity context) {
        final Dialog codeDekhLiyaHaiDialog = new Dialog(context);
        codeDekhLiyaHaiDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(codeDekhLiyaHaiDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        codeDekhLiyaHaiDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        codeDekhLiyaHaiDialog.setContentView(R.layout.dialog_online_offline_coupon_message);
        TextView tvCouponType = codeDekhLiyaHaiDialog.findViewById(R.id.tv_coupon_type);
        TextView tvCouponTypeMessage = codeDekhLiyaHaiDialog.findViewById(R.id.tv_coupon_type_message);
        ImageView ivClose = codeDekhLiyaHaiDialog.findViewById(R.id.iv_close);
        Button btnOk = codeDekhLiyaHaiDialog.findViewById(R.id.btn_ok);

        tvCouponType.setText(context.getString(R.string.offline_coupon_title));
        tvCouponTypeMessage.setText(context.getString(R.string.code_dekh_diye_msg));

        if (!codeDekhLiyaHaiDialog.isShowing())
            codeDekhLiyaHaiDialog.show();

        ivClose.setOnClickListener(v -> codeDekhLiyaHaiDialog.dismiss());

        btnOk.setOnClickListener(v -> codeDekhLiyaHaiDialog.dismiss());
    }

    //Stop Countdown method
    private void stopCountdown() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    /**
     * This method will be used to copy code to clipboard
     *
     * @param context
     * @param codeToBeCopied
     */
    public void copyToClipBoard(Context context, String codeToBeCopied) {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText(CouponManagementConstants.COPY_CODE_LABLE, codeToBeCopied);
        if (clipboardManager != null) {
            clipboardManager.setPrimaryClip(clipData);
        }
        CouponDialogUtils.showToastMessage(context, context.getString(R.string.copy_to_clipboard));
    }

    /**
     * This method will be used to show toast message
     *
     * @param message
     */
    public static void showToastMessage(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
        }
    }
}
