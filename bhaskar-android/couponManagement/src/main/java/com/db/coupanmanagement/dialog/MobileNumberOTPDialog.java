package com.db.coupanmanagement.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.activity.BaseActivity;
import com.db.coupanmanagement.activity.CouponManagementActivity;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.model.CouponMobileNumberOTPModel;
import com.db.coupanmanagement.model.CouponMobileNumberOTPVerificationModel;
import com.db.coupanmanagement.model.CouponOTPVerificationRequestParam;
import com.db.coupanmanagement.model.GetMobileNumberOTPRequestParam;
import com.db.coupanmanagement.network.CouponManagementAPIClient;
import com.db.coupanmanagement.network.CouponManagementAPIInterface;
import com.db.coupanmanagement.smsreceiver.SmsOtpReceiver;
import com.db.coupanmanagement.utils.CouponAppPreferences;

import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hp on 14-07-2018.
 */

public class MobileNumberOTPDialog implements View.OnKeyListener, TextView.OnEditorActionListener {
    private ProgressBar mProgressBar;
    private String mMobileNo;
    private int mOTP;
    private LinearLayout mLinCountDown;
    private TextView mTvMinute, mTvSecond;
    private EditText mEtOTP1, mEtOTP2, mEtOTP3, mEtOTP4, mEtOTP5, mEtOTP6;
    private Button mBtnTheekHai, mBtnClear, mBtnResendOTP;
    private ImageView mIvMobileNumberStatus;
    private CouponManagementActivity mActivity;
    private CountDownTimer mCountDownTimer;
    private OnOTPVerifiedListner mListner;
    private String mUniqueDeviceId;

    public MobileNumberOTPDialog(CouponManagementActivity activity) {
        this.mActivity = activity;
    }

    /**
     * This method will be used to prepare dialog
     */
    public void showEnterMobileNumberOTPDialog(BaseActivity.APIModelProvider apiModelProvider, OnOTPVerifiedListner listner) {
        mListner = listner;

        final Dialog mobileNumberOTPDialog = new Dialog(mActivity);
        mobileNumberOTPDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mobileNumberOTPDialog.setCancelable(false);
        Objects.requireNonNull(mobileNumberOTPDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        mobileNumberOTPDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mobileNumberOTPDialog.setContentView(R.layout.dialog_enter_mobile_number_otp);

        // Get device id from preference
        mUniqueDeviceId = CouponAppPreferences.getInstance(mActivity).getStringValue(CouponManagementConstants.PREF_KEY_UNIQUE_DEVICE_ID, "");

        mLinCountDown = mobileNumberOTPDialog.findViewById(R.id.lin_countdown);
        mTvMinute = mobileNumberOTPDialog.findViewById(R.id.tv_minute);
        mTvSecond = mobileNumberOTPDialog.findViewById(R.id.tv_second);

        mProgressBar = mobileNumberOTPDialog.findViewById(R.id.progress_bar);
        mProgressBar.setVisibility(View.GONE);

        // Initialize references for first layout which will ask for mobile number
        LinearLayout linMobileNumber = mobileNumberOTPDialog.findViewById(R.id.lin_mobile_number);
        linMobileNumber.setVisibility(View.VISIBLE);

        mIvMobileNumberStatus = mobileNumberOTPDialog.findViewById(R.id.iv_mobile_number_status);
        mIvMobileNumberStatus.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.cb_cross));

        TextView tvCountryCode = mobileNumberOTPDialog.findViewById(R.id.tv_country_code);
        tvCountryCode.setText(mActivity.getString(R.string.country_code));

        EditText etMobileNumber = mobileNumberOTPDialog.findViewById(R.id.et_mobile_number);

        mBtnClear = mobileNumberOTPDialog.findViewById(R.id.btn_clear);

        etMobileNumber.addTextChangedListener(new MobileNumberEditTextWatcher(etMobileNumber));

        etMobileNumber.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                tvCountryCode.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.bottom_line_highlighted_background));
        });

        mBtnClear.setOnClickListener(v -> etMobileNumber.setText(""));

        // Initialize references for second layout which will ask for OTP(One Time Password)
        LinearLayout linOTP = mobileNumberOTPDialog.findViewById(R.id.lin_otp);
        linOTP.setVisibility(View.GONE);
        mEtOTP1 = mobileNumberOTPDialog.findViewById(R.id.et_otp_1);
        mEtOTP2 = mobileNumberOTPDialog.findViewById(R.id.et_otp_2);
        mEtOTP3 = mobileNumberOTPDialog.findViewById(R.id.et_otp_3);
        mEtOTP4 = mobileNumberOTPDialog.findViewById(R.id.et_otp_4);
        mEtOTP5 = mobileNumberOTPDialog.findViewById(R.id.et_otp_5);
        mEtOTP6 = mobileNumberOTPDialog.findViewById(R.id.et_otp_6);

        // Initialize references for final layout which has common buttons for both the above layouts
        mBtnResendOTP = mobileNumberOTPDialog.findViewById(R.id.btn_resend_otp);
        mBtnResendOTP.setVisibility(View.GONE);
        // Disable resend otp button
        disableResendOtpButton();
        Button btnRaddKarein = mobileNumberOTPDialog.findViewById(R.id.btn_radd_karein);
        mBtnTheekHai = mobileNumberOTPDialog.findViewById(R.id.btn_theek_hai);

        mEtOTP1.addTextChangedListener(new OTPEditTextWatcher(mEtOTP1));
        mEtOTP1.setOnKeyListener(this);
        mEtOTP1.setOnEditorActionListener(this);

        mEtOTP2.addTextChangedListener(new OTPEditTextWatcher(mEtOTP2));
        mEtOTP2.setOnKeyListener(this);
        mEtOTP2.setOnEditorActionListener(this);

        mEtOTP3.addTextChangedListener(new OTPEditTextWatcher(mEtOTP3));
        mEtOTP3.setOnKeyListener(this);
        mEtOTP3.setOnEditorActionListener(this);

        mEtOTP4.addTextChangedListener(new OTPEditTextWatcher(mEtOTP4));
        mEtOTP4.setOnKeyListener(this);
        mEtOTP4.setOnEditorActionListener(this);

        mEtOTP5.addTextChangedListener(new OTPEditTextWatcher(mEtOTP5));
        mEtOTP5.setOnKeyListener(this);
        mEtOTP5.setOnEditorActionListener(this);

        mEtOTP6.addTextChangedListener(new OTPEditTextWatcher(mEtOTP6));
        mEtOTP6.setOnKeyListener(this);
        mEtOTP6.setOnEditorActionListener(this);

        if (!mobileNumberOTPDialog.isShowing())
            mobileNumberOTPDialog.show();

        // Track current & previous screen
        mActivity.trackPageView(apiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS_CLAIM_OFFER, CouponManagementConstants.GAScreenName.MYOFFERS_MONTHLY_OFFERS, CouponManagementConstants.GAScreenName.MYOFFERS_ENTER_MOBILE_NUMBER);

        // Check receiver to get SMS results
        SmsOtpReceiver.bindListener(messageText -> {
            if (mobileNumberOTPDialog.isShowing()) {
                //                Log.w("onMessageReceived", "SMS: " + messageText);
                if (messageText.length() != 0) {
                    // Blank otp fields before setting sms otp
                    mEtOTP1.setText("");
                    mEtOTP2.setText("");
                    mEtOTP3.setText("");
                    mEtOTP4.setText("");
                    mEtOTP5.setText("");
                    mEtOTP6.setText("");

                    mEtOTP1.setText(String.valueOf(messageText.charAt(0)));
                    mEtOTP2.setText(String.valueOf(messageText.charAt(1)));
                    mEtOTP3.setText(String.valueOf(messageText.charAt(2)));
                    mEtOTP4.setText(String.valueOf(messageText.charAt(3)));
                    mEtOTP5.setText(String.valueOf(messageText.charAt(4)));
                    mEtOTP6.setText(String.valueOf(messageText.charAt(5)));
                    mEtOTP6.clearFocus();

                    // Verify otp if sms otp is equal to get mobile number response otp
                    if (mOTP != 0) {
                        if (Integer.parseInt(messageText) == mOTP) {
                            int enteredOTP = Integer.parseInt(mEtOTP1.getText().toString() + mEtOTP2.getText().toString() + mEtOTP3.getText().toString() + mEtOTP4.getText().toString() + mEtOTP5.getText().toString() + mEtOTP6.getText().toString());
                            // Delay for some sec to display otp set on screen
                            new Handler().postDelayed(() -> {
                                // Auto verify OTP
                                verifyMobileNumberOTP(mBtnTheekHai, apiModelProvider, enteredOTP, mobileNumberOTPDialog);
                            }, CouponManagementConstants.AUTO_VERIFY_OTP_DELAY);
                        }
                    }
                }
            }
        });

        mobileNumberOTPDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                SmsOtpReceiver.unbindListener();
            }
        });

        mBtnResendOTP.setOnClickListener(v -> {
            // Track current & previous screen
            mActivity.trackPageView(apiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS_CLAIM_OFFER, CouponManagementConstants.GAScreenName.MYOFFERS_ENTER_OTP_RESEND, CouponManagementConstants.GAScreenName.MYOFFERS_ENTER_MOBILE_NUMBER);
            // Disable resend otp button
            disableResendOtpButton();
            // Start count down timer
            startCountDownTimer(mActivity);
            getOTPForMobileNumber(v, mMobileNo, linMobileNumber, linOTP, apiModelProvider);
        });

        btnRaddKarein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (linMobileNumber.getVisibility() == View.VISIBLE) {
                    // Track current & previous screen
                    mActivity.trackPageView(apiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS_CLAIM_OFFER, CouponManagementConstants.GAScreenName.MYOFFERS_ENTER_MOBILE_NUMBER_CANCEL, CouponManagementConstants.GAScreenName.MYOFFERS_MONTHLY_OFFERS);
                } else {
                    // Track current & previous screen
                    mActivity.trackPageView(apiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS_CLAIM_OFFER, CouponManagementConstants.GAScreenName.MYOFFERS_ENTER_OTP_CANCEL, CouponManagementConstants.GAScreenName.MYOFFERS_MONTHLY_OFFERS);
                }
                if (!mActivity.isFinishing()) {
                    mobileNumberOTPDialog.dismiss();
                }
            }
        });

        mBtnTheekHai.setOnClickListener(v -> {
            int enteredOTP = 0;
            if (linMobileNumber.getVisibility() == View.VISIBLE) {
                // Track current & previous screen
                mActivity.trackPageView(apiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS_CLAIM_OFFER, CouponManagementConstants.GAScreenName.MYOFFERS_ENTER_MOBILE_NUMBER_OK, CouponManagementConstants.GAScreenName.MYOFFERS_MONTHLY_OFFERS);

                mMobileNo = etMobileNumber.getText().toString();
                if (mMobileNo.length() == 10) {
                    // Get OTP for entered mobile number
                    getOTPForMobileNumber(v, mMobileNo, linMobileNumber, linOTP, apiModelProvider);
                } else {
                    showToastMessage(mActivity, mActivity.getString(R.string.enter_correct_mobile_number));
                }
            } else if (linOTP.getVisibility() == View.VISIBLE) {
                // Track current & previous screen
                mActivity.trackPageView(apiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS_CLAIM_OFFER, CouponManagementConstants.GAScreenName.MYOFFERS_ENTER_OTP_OK, CouponManagementConstants.GAScreenName.MYOFFERS_MONTHLY_OFFERS);

                if (isEmptyEditText(new EditText[]{mEtOTP1, mEtOTP2, mEtOTP3, mEtOTP4, mEtOTP5, mEtOTP6})) {
                    showToastMessage(mActivity, mActivity.getString(R.string.enter_otp));
                } else if (mOTP != 0) {
                    enteredOTP = Integer.parseInt(mEtOTP1.getText().toString() + mEtOTP2.getText().toString() + mEtOTP3.getText().toString() + mEtOTP4.getText().toString() + mEtOTP5.getText().toString() + mEtOTP6.getText().toString());
                    if (mOTP != enteredOTP) {
                        showToastMessage(mActivity, mActivity.getString(R.string.enter_correct_otp));
                    } else {
                        // Verify OTP for entered mobile number
                        verifyMobileNumberOTP(v, apiModelProvider, enteredOTP, mobileNumberOTPDialog);
                    }
                } else {
                    // Verify OTP for entered mobile number
                    verifyMobileNumberOTP(v, apiModelProvider, enteredOTP, mobileNumberOTPDialog);
                }
            } else {
                if (!mActivity.isFinishing()) {
                    mobileNumberOTPDialog.dismiss();
                }
            }
        });
    }

    /**
     * This method is used to disable resend otp button
     */
    private void disableResendOtpButton() {
        mBtnResendOTP.setEnabled(false);
        mBtnResendOTP.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_color));
        mBtnResendOTP.setAlpha(0.5f);
    }

    /**
     * This method will be used to get otp for mobile number
     *
     * @param v
     * @param mobileNo
     * @param linMobileNumber
     * @param linOTP
     * @param apiModelProvider
     */
    private void getOTPForMobileNumber(View v, String mobileNo, LinearLayout linMobileNumber, LinearLayout linOTP, BaseActivity.APIModelProvider apiModelProvider) {
        mBtnTheekHai.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);

        InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);

        GetMobileNumberOTPRequestParam requestParam = new GetMobileNumberOTPRequestParam(mobileNo);

        CouponManagementAPIInterface apiInterface = CouponManagementAPIClient.getClient(apiModelProvider.getAPIModel().getAuthToken(), apiModelProvider.getAPIModel().getDbId(), apiModelProvider.getAPIModel().getUuId(), apiModelProvider.getAPIModel().getuId(), apiModelProvider.getAPIModel().getAppId(), apiModelProvider.getAPIModel().getAppKey()).create(CouponManagementAPIInterface.class);
        Call<CouponMobileNumberOTPModel> call = apiInterface.doPostCouponMobileNumberOTP(requestParam);

        call.enqueue(new Callback<CouponMobileNumberOTPModel>() {
            @Override
            public void onResponse(@NonNull Call<CouponMobileNumberOTPModel> call, @NonNull Response<CouponMobileNumberOTPModel> response) {
                CouponMobileNumberOTPModel mobileNumberOTPModel = response.body();
                if (mobileNumberOTPModel != null) {
                    if (mobileNumberOTPModel.getCode() == CouponManagementConstants.API_SUCCESS) {
                        mBtnTheekHai.setVisibility(View.VISIBLE);
                        mProgressBar.setVisibility(View.GONE);

                        if (linOTP.getVisibility() != View.VISIBLE) {
                            // Track current & previous screen
                            mActivity.trackPageView(apiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS_CLAIM_OFFER, CouponManagementConstants.GAScreenName.MYOFFERS_ENTER_OTP, CouponManagementConstants.GAScreenName.MYOFFERS_ENTER_MOBILE_NUMBER);
                            if (inputManager != null) {
                                inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                            }
                            linMobileNumber.setVisibility(View.GONE);
                            Animation animation = AnimationUtils.loadAnimation(mActivity, R.anim.slide_left_exit);
                            linMobileNumber.startAnimation(animation);
                            linOTP.setVisibility(View.VISIBLE);
                            animation = AnimationUtils.loadAnimation(mActivity, R.anim.slide_left);
                            linOTP.startAnimation(animation);
                        }
                        mOTP = mobileNumberOTPModel.getOTP();
                        mBtnResendOTP.setVisibility(View.VISIBLE);
                        // Disable resend otp button
                        disableResendOtpButton();
                        // Start count down timer
                        startCountDownTimer(mActivity);
                    } else if (mobileNumberOTPModel.getCode() == CouponManagementConstants.API_BAD_REQUEST || mobileNumberOTPModel.getCode() == CouponManagementConstants.API_INTERNAL_SERVER_ERROR || mobileNumberOTPModel.getCode() == CouponManagementConstants.API_NOT_FOUND) {
                        // Show metwork error
                        showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_otp));
                    } else if (mobileNumberOTPModel.getCode() == CouponManagementConstants.API_UNAUTHORIZED_ACCESS) {
                        // Show metwork error
                        showNetworkErrorMessage(mActivity.getString(R.string.please_login));
                    } else {
                        // Show metwork error
                        showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_otp));
                    }
                } else {
                    // Show metwork error
                    showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_otp));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CouponMobileNumberOTPModel> call, @NonNull Throwable t) {
                // Show metwork error
                showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_otp));
            }
        });
    }

    /**
     * This method will will be used to verify otp for mobile
     *
     * @param v
     * @param apiModelProvider
     * @param enteredOTP
     * @param mobileNumberOTPDialog
     */
    private void verifyMobileNumberOTP(View v, BaseActivity.APIModelProvider apiModelProvider, int enteredOTP, Dialog mobileNumberOTPDialog) {
        mBtnTheekHai.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);

        InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        CouponOTPVerificationRequestParam requestParam = new CouponOTPVerificationRequestParam(mMobileNo, enteredOTP);
        CouponManagementAPIInterface apiInterface = CouponManagementAPIClient.getClient(apiModelProvider.getAPIModel().getAuthToken(), apiModelProvider.getAPIModel().getDbId(), apiModelProvider.getAPIModel().getUuId(), apiModelProvider.getAPIModel().getuId(), apiModelProvider.getAPIModel().getAppId(), apiModelProvider.getAPIModel().getAppKey()).create(CouponManagementAPIInterface.class);
        Call<CouponMobileNumberOTPVerificationModel> call = apiInterface.doPostCouponMobileNumberOTPVerification(requestParam);

        call.enqueue(new Callback<CouponMobileNumberOTPVerificationModel>() {
            @Override
            public void onResponse(@NonNull Call<CouponMobileNumberOTPVerificationModel> call, @NonNull Response<CouponMobileNumberOTPVerificationModel> response) {
                CouponMobileNumberOTPVerificationModel mobileNumberOTPVerificationModel = response.body();
                if (mobileNumberOTPVerificationModel != null) {
                    if (mobileNumberOTPVerificationModel.getCode() == CouponManagementConstants.API_SUCCESS) {
                        mBtnTheekHai.setVisibility(View.VISIBLE);
                        mProgressBar.setVisibility(View.GONE);

                        if (inputManager != null && v != null) {
                            inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                        }
                        showToastMessage(mActivity, mActivity.getString(R.string.your_mobile_number_is_registered));
                        if (!mActivity.isFinishing()) {
                            mobileNumberOTPDialog.dismiss();
                        }
                        // Put data in SharedPreferences
                        CouponAppPreferences.getInstance(mActivity).setStringValue(CouponManagementConstants.GetPreferenceKeys.MOBILE_NUMBER, mMobileNo);
                        CouponAppPreferences.getInstance(mActivity).setStringValue(CouponManagementConstants.GetPreferenceKeys.EMAILID, apiModelProvider.getAPIModel().getEmailId());
                        // Call listner to move forward
                        if (mListner != null) {
                            mListner.onOTPVerified();
                        }
                    } else if (mobileNumberOTPVerificationModel.getCode() == CouponManagementConstants.API_BAD_REQUEST || mobileNumberOTPVerificationModel.getCode() == CouponManagementConstants.API_INTERNAL_SERVER_ERROR || mobileNumberOTPVerificationModel.getCode() == CouponManagementConstants.API_NOT_FOUND) {
                        // Show metwork error
                        showNetworkErrorMessage(mActivity.getString(R.string.unable_to_verify_otp));
                    } else if (mobileNumberOTPVerificationModel.getCode() == CouponManagementConstants.API_UNAUTHORIZED_ACCESS) {
                        // Show metwork error
                        showNetworkErrorMessage(mActivity.getString(R.string.please_login));
                    } else {
                        // Show metwork error
                        showNetworkErrorMessage(mActivity.getString(R.string.unable_to_verify_otp));
                    }
                } else {
                    // Show metwork error
                    showNetworkErrorMessage(mActivity.getString(R.string.unable_to_verify_otp));
                }
            }

            @Override
            public void onFailure(Call<CouponMobileNumberOTPVerificationModel> call, Throwable t) {
                // Show metwork error
                showNetworkErrorMessage(mActivity.getString(R.string.unable_to_verify_otp));
            }
        });
    }

    /**
     * This method will be used to show network message
     */
    private void showNetworkErrorMessage(String message) {
        mBtnTheekHai.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        showToastMessage(mActivity, message);
    }

    /**
     * This method is used to start countdown timer
     *
     * @param context
     */
    private void startCountDownTimer(Context context) {
        mLinCountDown.setVisibility(View.VISIBLE);
        mLinCountDown.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left));

        mCountDownTimer = new CountDownTimer(CouponManagementConstants.RESEND_OTP, CouponManagementConstants.CountDownTimerFormat.COUNTDOWN_TIMER_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                // Minutes
                long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
                mTvMinute.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, minutes));
                // Seconds
                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                mTvSecond.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, seconds));
            }

            @Override
            public void onFinish() {
                mTvMinute.setText(context.getString(R.string.zero_zero));
                mTvSecond.setText(context.getString(R.string.zero_zero));
                mLinCountDown.setVisibility(View.GONE);
                mLinCountDown.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right));
                mBtnResendOTP.setEnabled(true);
                mBtnResendOTP.setTextColor(ContextCompat.getColor(mActivity, android.R.color.holo_blue_dark));
                mBtnResendOTP.setAlpha(1.0f);
                if (mCountDownTimer != null) {
                    mCountDownTimer.cancel();
                    mCountDownTimer = null;
                }
            }
        }.start();
    }

    /**
     * This method basically check the empty edittext
     *
     * @param editText
     * @return
     */
    private boolean isEmptyEditText(EditText[] editText) {
        for (EditText et : editText) {
            return TextUtils.isEmpty(et.getText().toString());
        }
        return false;
    }

    /**
     * This method will be used to show toast message
     *
     * @param message
     */
    private static void showToastMessage(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            int id = view.getId();
            if (id == R.id.et_otp_1) {
                mEtOTP1.setText("");
                return true;
            } else if (id == R.id.et_otp_2) {
                mEtOTP1.requestFocus();
                mEtOTP2.setText("");
                return true;
            } else if (id == R.id.et_otp_3) {
                mEtOTP2.requestFocus();
                mEtOTP3.setText("");
                return true;
            } else if (id == R.id.et_otp_4) {
                mEtOTP3.requestFocus();
                mEtOTP4.setText("");
                return true;
            } else if (id == R.id.et_otp_5) {
                mEtOTP4.requestFocus();
                mEtOTP5.setText("");
                return true;
            } else if (id == R.id.et_otp_6) {
                mEtOTP5.requestFocus();
                mEtOTP6.setText("");
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
        if (keyCode == EditorInfo.IME_ACTION_NEXT) {
            int id = textView.getId();
            if (id == R.id.et_otp_1) {
                mEtOTP2.requestFocus();
                return true;
            } else if (id == R.id.et_otp_2) {
                mEtOTP3.requestFocus();
                return true;
            } else if (id == R.id.et_otp_3) {
                mEtOTP4.requestFocus();
                return true;
            } else if (id == R.id.et_otp_4) {
                mEtOTP5.requestFocus();
                return true;
            } else if (id == R.id.et_otp_5) {
                mEtOTP6.requestFocus();
                return true;
            }
        }
        return false;
    }

    private class OTPEditTextWatcher implements TextWatcher {

        private View view;

        private OTPEditTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int id = view.getId();
            if (id == R.id.et_otp_1) {
                if (text.length() == 1)
                    mEtOTP2.requestFocus();
            } else if (id == R.id.et_otp_2) {
                if (text.length() == 1)
                    mEtOTP3.requestFocus();
            } else if (id == R.id.et_otp_3) {
                if (text.length() == 1)
                    mEtOTP4.requestFocus();
            } else if (id == R.id.et_otp_4) {
                if (text.length() == 1)
                    mEtOTP5.requestFocus();
            } else if (id == R.id.et_otp_5) {
                if (text.length() == 1)
                    mEtOTP6.requestFocus();
            }
        }
    }

    private class MobileNumberEditTextWatcher implements TextWatcher {

        private View view;

        private MobileNumberEditTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            if (text.length() == 10)
                mIvMobileNumberStatus.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.cb_tick));
            else
                mIvMobileNumberStatus.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.cb_cross));
            int id = view.getId();
            if (id == R.id.et_mobile_number) {
                if (text.length() == 0)
                    mBtnClear.setVisibility(View.GONE);
                else
                    mBtnClear.setVisibility(View.VISIBLE);
            }
        }
    }

    public interface OnOTPVerifiedListner {
        void onOTPVerified();
    }
}
