package com.db.coupanmanagement.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.activity.BaseActivity;
import com.db.coupanmanagement.adapter.ClaimedCouponCodeRecyclerViewAdapter;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.model.ClaimedCouponDetailsModel;
import com.db.coupanmanagement.model.ConfirmOfflineCouponRequestParams;
import com.db.coupanmanagement.model.GetOfflineClaimedCouponDataModel;
import com.db.coupanmanagement.model.GetOfflineClaimedCouponDetailModel;
import com.db.coupanmanagement.network.CouponManagementAPIClient;
import com.db.coupanmanagement.network.CouponManagementAPIInterface;
import com.db.coupanmanagement.utils.CouponAppPreferences;
import com.db.coupanmanagement.utils.Utils;

import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.WindowManager.LayoutParams.FLAG_SECURE;

/**
 * Created by hp on 02-08-2018.
 */

public class OfflineCouponCodeDialog implements View.OnClickListener {
    private Context mContext;
    private Dialog mOfflineCouponCodeDekheinDialog;
    private LinearLayout mLinOfflineCodeDekhein1, mLinOfflineCodeDekhein2, mLinOfflineCodeDekhein3;
    private TextView mTvMinute, mTvSecond, mTvOfflineCouponCode;
    private Button mBtnConfirm;
    private ProgressBar mProgressBar;
    private ClaimedCouponDetailsModel mClaimedCouponDetailsModel;
    private BaseActivity.APIModelProvider mApiModelProvider;
    private OnClickCodeDikhaDiyaInterface mOnClickCodeDikhaDiyaInterface;
    private ClaimedCouponCodeRecyclerViewAdapter.ClaimedCouponViewHolder mHolder;
    private int mItemPosition;
    private String mUniqueDeviceId;

    /**
     * This method will be used to prepare dialog
     */
    public void prepareOfflineCodeDekheinDialog(Activity context, ClaimedCouponDetailsModel claimedCouponDetailsModel, BaseActivity.APIModelProvider apiModelProvider, ClaimedCouponCodeRecyclerViewAdapter.ClaimedCouponViewHolder holder, int itemPosition, OnClickCodeDikhaDiyaInterface clickCodeDikhaDiyaInterface) {
        mContext = context;
        mClaimedCouponDetailsModel = claimedCouponDetailsModel;
        mApiModelProvider = apiModelProvider;
        mOnClickCodeDikhaDiyaInterface = clickCodeDikhaDiyaInterface;
        mHolder = holder;
        mItemPosition = itemPosition;

        mOfflineCouponCodeDekheinDialog = new Dialog(context);
        mOfflineCouponCodeDekheinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(mOfflineCouponCodeDekheinDialog.getWindow()).setFlags(FLAG_SECURE, FLAG_SECURE);
        Objects.requireNonNull(mOfflineCouponCodeDekheinDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        mOfflineCouponCodeDekheinDialog.setCancelable(false);
        mOfflineCouponCodeDekheinDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mOfflineCouponCodeDekheinDialog.setContentView(R.layout.dialog_offline_coupon_code_dekhein);

        // Get device id from preference
        mUniqueDeviceId = CouponAppPreferences.getInstance(mContext).getStringValue(CouponManagementConstants.PREF_KEY_UNIQUE_DEVICE_ID, "");

        ImageView ivClose = mOfflineCouponCodeDekheinDialog.findViewById(R.id.iv_close);

        // Offline coupon code message layout
        mLinOfflineCodeDekhein1 = mOfflineCouponCodeDekheinDialog.findViewById(R.id.lin_offline_code_dekhein1);
        TextView tvOfflineCouponMessage1 = mOfflineCouponCodeDekheinDialog.findViewById(R.id.tv_offline_coupon_message1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            tvOfflineCouponMessage1.setText(Html.fromHtml(claimedCouponDetailsModel.getIsOfflineMessage(), Html.FROM_HTML_MODE_LEGACY));
        else
            tvOfflineCouponMessage1.setText(Html.fromHtml(claimedCouponDetailsModel.getIsOfflineMessage()));

        Button btnBadMeinDekhein = mOfflineCouponCodeDekheinDialog.findViewById(R.id.btn_baad_mein_dekhein);
        Button btnCodeDekhein = mOfflineCouponCodeDekheinDialog.findViewById(R.id.btn_code_dekhein);
        mLinOfflineCodeDekhein1.setVisibility(View.VISIBLE);

        // Confirmation offline coupon layout
        mLinOfflineCodeDekhein2 = mOfflineCouponCodeDekheinDialog.findViewById(R.id.lin_offline_code_dekhein2);
        mLinOfflineCodeDekhein2.setVisibility(View.GONE);
        TextView tvOfflineCouponMessage2 = mOfflineCouponCodeDekheinDialog.findViewById(R.id.tv_offline_coupon_message2);
        tvOfflineCouponMessage2.setText(claimedCouponDetailsModel.getIsOfflineConfirmMessage());
        mBtnConfirm = mOfflineCouponCodeDekheinDialog.findViewById(R.id.btn_confirm);
        mProgressBar = mOfflineCouponCodeDekheinDialog.findViewById(R.id.progress_bar);
        mProgressBar.setVisibility(View.GONE);

        // Offline coupon code layout
        mLinOfflineCodeDekhein3 = mOfflineCouponCodeDekheinDialog.findViewById(R.id.lin_offline_code_dekhein3);
        mLinOfflineCodeDekhein3.setVisibility(View.GONE);
        mTvMinute = mOfflineCouponCodeDekheinDialog.findViewById(R.id.tv_minute);
        mTvSecond = mOfflineCouponCodeDekheinDialog.findViewById(R.id.tv_second);
        mTvOfflineCouponCode = mOfflineCouponCodeDekheinDialog.findViewById(R.id.tv_offline_claimed_coupon_code);
        Button btnCodeDikhaDIya = mOfflineCouponCodeDekheinDialog.findViewById(R.id.btn_code_dikha_diya);

        if (claimedCouponDetailsModel.getIsOfflineCouponViewed() == CouponManagementConstants.OfflineCouponData.OFFLINE_COUPON_VIEWED) {
            mLinOfflineCodeDekhein1.setVisibility(View.GONE);
            new CouponDialogUtils().prepareCodeDekhLiyaHaiDialog(context);
            if (mOnClickCodeDikhaDiyaInterface != null) {
                mOnClickCodeDikhaDiyaInterface.onClickCodeDikhaDiyaConfirm(mItemPosition, mHolder);
            }
        } else {
            mLinOfflineCodeDekhein1.setVisibility(View.VISIBLE);
            if (!mOfflineCouponCodeDekheinDialog.isShowing())
                mOfflineCouponCodeDekheinDialog.show();
        }

        ivClose.setOnClickListener(this);
        btnBadMeinDekhein.setOnClickListener(this);
        btnCodeDekhein.setOnClickListener(this);
        mBtnConfirm.setOnClickListener(this);
        btnCodeDikhaDIya.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.iv_close) {
            if (mLinOfflineCodeDekhein3.getVisibility() == View.VISIBLE && mOnClickCodeDikhaDiyaInterface != null) {
                mOnClickCodeDikhaDiyaInterface.onClickCodeDikhaDiyaConfirm(mItemPosition, mHolder);
            }
            mOfflineCouponCodeDekheinDialog.dismiss();
        } else if (id == R.id.btn_baad_mein_dekhein) {
            mOfflineCouponCodeDekheinDialog.dismiss();
        } else if (id == R.id.btn_code_dekhein) {
            getConfirmationMessage(mContext);
        } else if (id == R.id.btn_confirm) {
            getOfflineCouponDetail(mContext);
        } else if (id == R.id.btn_code_dikha_diya) {
            mOfflineCouponCodeDekheinDialog.dismiss();
        } else {
            mOfflineCouponCodeDekheinDialog.dismiss();
        }
    }

    /**
     * This method will be used to show offline coupon code confirmation data
     *
     * @param context
     */
    private void getConfirmationMessage(Context context) {
        mLinOfflineCodeDekhein1.setVisibility(View.GONE);
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_left_exit);
        mLinOfflineCodeDekhein1.startAnimation(animation);
        mLinOfflineCodeDekhein2.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.slide_left);
        mLinOfflineCodeDekhein2.startAnimation(animation);
    }

    private void getOfflineCouponDetail(Context context) {
        mBtnConfirm.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        ConfirmOfflineCouponRequestParams requestParam = new ConfirmOfflineCouponRequestParams(mClaimedCouponDetailsModel.getCouponId());
        CouponManagementAPIInterface apiInterface = CouponManagementAPIClient.getClient(mApiModelProvider.getAPIModel().getAuthToken(), mApiModelProvider.getAPIModel().getDbId(), mApiModelProvider.getAPIModel().getUuId(), mApiModelProvider.getAPIModel().getuId(), mApiModelProvider.getAPIModel().getAppId(), mApiModelProvider.getAPIModel().getAppKey()).create(CouponManagementAPIInterface.class);
        Call<GetOfflineClaimedCouponDataModel> call = apiInterface.doPostGetOfflineCouponDetail(requestParam);
        call.enqueue(new Callback<GetOfflineClaimedCouponDataModel>() {
            @Override
            public void onResponse(@NonNull Call<GetOfflineClaimedCouponDataModel> call, @NonNull Response<GetOfflineClaimedCouponDataModel> response) {
                mProgressBar.setVisibility(View.GONE);
                GetOfflineClaimedCouponDataModel getOfflineClaimedCouponDataModel = response.body();
                if (getOfflineClaimedCouponDataModel != null) {
                    if (getOfflineClaimedCouponDataModel.getCode() == CouponManagementConstants.API_SUCCESS) {
                        if (mOnClickCodeDikhaDiyaInterface != null) {
                            mOnClickCodeDikhaDiyaInterface.onClickCodeDikhaDiyaConfirm(mItemPosition, mHolder);
                        }
                        setOfflineCouponDetail(context, getOfflineClaimedCouponDataModel.getGetOfflineClaimedCouponDetailModel());
                    } else if (getOfflineClaimedCouponDataModel.getCode() == CouponManagementConstants.API_BAD_REQUEST || getOfflineClaimedCouponDataModel.getCode() == CouponManagementConstants.API_INTERNAL_SERVER_ERROR || getOfflineClaimedCouponDataModel.getCode() == CouponManagementConstants.API_NOT_FOUND) {
                        // Show metwork error
                        showNetworkErrorMessage(context.getString(R.string.unable_to_load_offline_coupon_code));
                    } else if (getOfflineClaimedCouponDataModel.getCode() == CouponManagementConstants.API_UNAUTHORIZED_ACCESS) {
                        // Show metwork error
                        showNetworkErrorMessage(context.getString(R.string.please_login));
                    } else {
                        // Show metwork error
                        showNetworkErrorMessage(context.getString(R.string.unable_to_load_offline_coupon_code));
                    }
                } else {
                    // Show metwork error
                    showNetworkErrorMessage(context.getString(R.string.unable_to_load_offline_coupon_code));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetOfflineClaimedCouponDataModel> call, @NonNull Throwable t) {
                // Show metwork error
                showNetworkErrorMessage(context.getString(R.string.unable_to_load_offline_coupon_code));
            }
        });
    }

    /**
     * This method will be used to show network message
     */
    private void showNetworkErrorMessage(String message) {
        mBtnConfirm.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        showToastMessage(mContext, message);
    }

    /**
     * This method will be used to show offline coupon code confirmation data
     *
     * @param context
     */
    private void setOfflineCouponDetail(Context context, GetOfflineClaimedCouponDetailModel getOfflineClaimedCouponDetailModel) {
        mLinOfflineCodeDekhein2.setVisibility(View.GONE);
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_left_exit);
        mLinOfflineCodeDekhein2.startAnimation(animation);
        mLinOfflineCodeDekhein3.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.slide_left);
        mLinOfflineCodeDekhein3.startAnimation(animation);

        // Start count down timer
        startCountDownTimer(getOfflineClaimedCouponDetailModel);
        mTvOfflineCouponCode.setText(Utils.decodeResponse(getOfflineClaimedCouponDetailModel.getCouponCode()));
    }

    /**
     * This method is used to start countdown timer
     *
     * @param detailModel
     */
    private void startCountDownTimer(GetOfflineClaimedCouponDetailModel detailModel) {
        new CountDownTimer(detailModel.getDisappearInterval(), CouponManagementConstants.CountDownTimerFormat.COUNTDOWN_TIMER_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                // Minutes
                long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
                mTvMinute.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, minutes));
                // Seconds
                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                mTvSecond.setText(String.format(Locale.getDefault(), CouponManagementConstants.CountDownTimerFormat.MONTHLY_OFFER_COUNTDOWN_FORMAT, seconds));
            }

            @Override
            public void onFinish() {
                if (mOnClickCodeDikhaDiyaInterface != null) {
                    mOnClickCodeDikhaDiyaInterface.onClickCodeDikhaDiyaConfirm(mItemPosition, mHolder);
                }
                mOfflineCouponCodeDekheinDialog.dismiss();
            }
        }.start();
    }

    /**
     * This method will be used to show toast message
     *
     * @param message
     */
    private void showToastMessage(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    public interface OnClickCodeDikhaDiyaInterface {
        void onClickCodeDikhaDiyaConfirm(int itemPosition, ClaimedCouponCodeRecyclerViewAdapter.ClaimedCouponViewHolder holder);
    }
}
