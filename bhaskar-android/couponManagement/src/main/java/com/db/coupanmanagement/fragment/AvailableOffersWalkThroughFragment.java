package com.db.coupanmanagement.fragment;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.db.coupanmanagement.R;
import com.db.coupanmanagement.activity.CouponManagementActivity;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.dialog.CouponDialogUtils;
import com.db.coupanmanagement.model.AvailableOfferListModel;
import com.db.coupanmanagement.model.AvailableOfferModel;
import com.db.coupanmanagement.model.GuestUserModel;
import com.db.coupanmanagement.model.GuestUserRequestParam;
import com.db.coupanmanagement.network.CouponManagementAPIInterface;
import com.db.coupanmanagement.utils.CouponAppPreferences;
import com.db.coupanmanagement.utils.Utils;
import com.viewpagerindicator.LinePageIndicator;

import java.util.List;
import java.util.Objects;

import retrofit2.Callback;
import retrofit2.Response;

import static com.db.coupanmanagement.utils.Utils.errorLogger;

/**
 * Created by hp on 15-05-2018.
 */

public class AvailableOffersWalkThroughFragment extends BaseFragment implements ViewPager.OnPageChangeListener {
    private static final String TAG = CouponManagementConstants.GetFragmentName.AVAILABLE_OFFERS_WALK_THROUGH_FRAGMENT;
    private ViewPager mPager;
    private LinearLayout mLinProgress;
    private TextView mTvNoOffersAvailable;
    private List<AvailableOfferModel> mAvailableOfferModelList;
    private RelativeLayout mRlContainer;
    private LinePageIndicator mTitleIndicator;
    private ImageView mIvOfferBackground;
    private CouponManagementActivity mActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_available_offers_walkthrough, container, false);
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        mActivity = (CouponManagementActivity) context;
    }

    @Override
    public void onViewCreated(@NonNull View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        //Set the pager with an adapter
        mPager = rootView.findViewById(R.id.tutorial_pager);
        mTitleIndicator = rootView.findViewById(R.id.line_indicator);
        mLinProgress = rootView.findViewById(R.id.lin_progress);
        mTvNoOffersAvailable = rootView.findViewById(R.id.tv_no_offers_available);
        mRlContainer = rootView.findViewById(R.id.rl_container);
        mIvOfferBackground = rootView.findViewById(R.id.iv_offer_background);

        mPager.setOnPageChangeListener(this);
        rootView.setOnClickListener(null);

        if (mAvailableOfferModelList == null) {
            fetchDataFromServer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            // Track current screen
            trackGAScreen();
            Bundle extras = getArguments();
            if (extras != null) {
                String title = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_TOOLBAR_TITLE);
                if (title != null && !title.isEmpty()) {
                    mActivity.setToolbarTItle(extras.getString(CouponManagementConstants.GetBundleKeys.KEY_TOOLBAR_TITLE));
                } else {
                    mActivity.setToolbarTItle(mActivity.getString(R.string.available_offers));
                }
                // Show toolbar back icon
                boolean isComingFromHamburgerMenu = extras.getBoolean(CouponManagementConstants.GetBundleKeys.KEY_IS_COMING_FROM_HAMBURGER_MENU, false);
                mActivity.setToolbarBackIconVisible(true);
            }
        }
    }

    private void trackGAScreen() {
        // Track current screen
        if (mApiModelProvider != null) {
            if (mApiModelProvider.getAPIModel() != null && mApiModelProvider.getAPIModel().isComingFromHamburgerMenu()) {
                mActivity.trackPageView(mApiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS, CouponManagementConstants.GAScreenName.MYOFFERS, CouponManagementConstants.GAScreenName.HOME);
            } else {
                mActivity.trackPageView(mApiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS, CouponManagementConstants.GAScreenName.MYOFFERS, CouponManagementConstants.GAScreenName.MERA_PAGE);
            }
        }
    }


    @Override
    public void fetchDataFromServer(String... data) {

        if (Utils.isNetworkConnected(mActivity)) {
            mLinProgress.setVisibility(View.VISIBLE);
            boolean isGuestHitDone = CouponAppPreferences.getInstance(getContext()).getBooleanValue(CouponManagementConstants.IS_GUEST_USER_API_DONE, false);
            if (!isGuestHitDone) {
                makeGuestUserApiHit();
            } else {
                makeNetworkApiCall();
            }

        } else {
            networkConnectivityError();
        }
    }

    private void makeGuestUserApiHit() {
        CouponManagementAPIInterface apiInterface = getAPIInterface();
        String make = Build.BRAND;
        String model = Build.MODEL;
        String version = String.valueOf(Build.VERSION.SDK_INT);
        String os = "ANDROID";
        String deviceId = (null != mActivity) ? mActivity.getAPIModel().getDeviceId() : "";

        GuestUserRequestParam networkRequestParam = new GuestUserRequestParam(model, make, os, version, deviceId);
        if (null != apiInterface) {
            retrofit2.Call<GuestUserModel> call = apiInterface.doPostGuestUser(networkRequestParam);
            call.enqueue(new Callback<GuestUserModel>() {
                @Override
                public void onResponse(@NonNull retrofit2.Call<GuestUserModel> call, @NonNull Response<GuestUserModel> response) {
                    if (response.code() == 200) {
                        CouponAppPreferences.getInstance(getContext()).setBooleanValue(CouponManagementConstants.IS_GUEST_USER_API_DONE, true);
                        makeNetworkApiCall();
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<GuestUserModel> call, Throwable t) {
                    Log.e("", "" + t.getMessage());
                }

            });
        }
    }

    private void makeNetworkApiCall() {
        CouponManagementAPIInterface apiInterface = getAPIInterface();
        if (null != apiInterface) {
            retrofit2.Call<AvailableOfferListModel> call = apiInterface.doPostAvailableOffersList();
            call.enqueue(new Callback<AvailableOfferListModel>() {
                @Override
                public void onResponse(@NonNull retrofit2.Call<AvailableOfferListModel> call, @NonNull Response<AvailableOfferListModel> response) {
                    AvailableOfferListModel availableOfferListModel = response.body();
                    mLinProgress.setVisibility(View.GONE);
                    if (null != availableOfferListModel) {
                        if (availableOfferListModel.getCode() == CouponManagementConstants.API_SUCCESS) {
                            if (null != availableOfferListModel.getData()) {
                                if (!availableOfferListModel.isShowMessage()) {
                                    // Render data on UI
                                    renderAvailableOffers(availableOfferListModel);
                                } else {
                                    // Show coming message to user
                                    showNormalMessageDialog(availableOfferListModel.getPopupMessage());
                                }
                            } else {
                                mRlContainer.setVisibility(View.GONE);
                                mTvNoOffersAvailable.setText(mActivity.getString(R.string.no_offers_available));
                                mTvNoOffersAvailable.setVisibility(View.VISIBLE);
                            }
                        } else if (availableOfferListModel.getCode() == CouponManagementConstants.API_BAD_REQUEST || availableOfferListModel.getCode() == CouponManagementConstants.API_INTERNAL_SERVER_ERROR || availableOfferListModel.getCode() == CouponManagementConstants.API_NOT_FOUND) {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_available_offers));
                        } else if (availableOfferListModel.getCode() == CouponManagementConstants.API_UNAUTHORIZED_ACCESS) {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.please_login));
                        } else {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_available_offers));
                        }
                    } else {
                        // Show metwork error
                        showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_available_offers));
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<AvailableOfferListModel> call, Throwable t) {
                    errorLogger(TAG, t.getMessage());
                    // Show metwork error
                    showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_available_offers));
                }

            });
        }
    }

    /**
     * This method will be used to show network message
     */
    private void showNetworkErrorMessage(String message) {
        mLinProgress.setVisibility(View.GONE);
        mRlContainer.setVisibility(View.GONE);
        mTvNoOffersAvailable.setVisibility(View.VISIBLE);
        mTvNoOffersAvailable.setText(message);
    }

    /**
     * This method will be used to render available offers data
     *
     * @param availableOfferListModel
     */
    private void renderAvailableOffers(AvailableOfferListModel availableOfferListModel) {
        mTvNoOffersAvailable.setVisibility(View.GONE);
        mRlContainer.setVisibility(View.VISIBLE);
        mAvailableOfferModelList = availableOfferListModel.getData();
        //set data on list
        if (null != mAvailableOfferModelList && !mAvailableOfferModelList.isEmpty()) {
            mPager.setOffscreenPageLimit(mAvailableOfferModelList.size()); // Helps to keep fragment alive, otherwise I will have to load again images
            PagerAdapter pagerAdapter = new WalkThroughPagerAdapter(Objects.requireNonNull(mActivity).getSupportFragmentManager(), mAvailableOfferModelList);
            mPager.setAdapter(pagerAdapter);

            //Bind the title indicator to the adapter
            mTitleIndicator.setViewPager(mPager);
        }
    }

    @Override
    public void networkConnectivityError() {
        if (null == mAvailableOfferModelList) {
            mRlContainer.setVisibility(View.GONE);
            mTvNoOffersAvailable.setVisibility(View.VISIBLE);
            mTvNoOffersAvailable.setText(mActivity.getString(R.string.network_unavailable));
        }
    }

    @Override
    public void networkConnectivitySuccess() {
        if (mAvailableOfferModelList == null) {
            fetchDataFromServer();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        Glide.with(Objects.requireNonNull(mActivity))
                .load(mAvailableOfferModelList.get(position).getBackgroundImage())
                .into(mIvOfferBackground);
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    public class WalkThroughPagerAdapter extends FragmentStatePagerAdapter {
        List<AvailableOfferModel> _availableOfferModelList;

        WalkThroughPagerAdapter(FragmentManager fm, List<AvailableOfferModel> availableOfferModelList) {
            super(fm);
            this._availableOfferModelList = availableOfferModelList;
        }

        @Override
        public Fragment getItem(int position) {
            if (getArguments() != null) {
                Bundle extras = new Bundle();
                extras.putAll(getArguments());
                return WalkThroughFragment.newInstance(_availableOfferModelList.get(position), extras);
            }
            return null;
        }

        @Override
        public int getCount() {
            return _availableOfferModelList.size();
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }

    /**
     * This method is used to show normal message
     */
    private void showNormalMessageDialog(String message) {
        new CouponDialogUtils().showNormalMessageWithOneActionButtonDialog(mActivity, mActivity.getString(R.string.notice), message, false, mActivity.getString(R.string.ok), new CouponDialogUtils.SingleButtonDialogResponsesInterface() {
            @Override
            public void doOnOkButtonClick(Activity activity) {

            }
        });
    }
}


