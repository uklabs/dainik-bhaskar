package com.db.coupanmanagement.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.db.coupanmanagement.activity.BaseActivity;
import com.db.coupanmanagement.couponshandler.ViewHandler;
import com.db.coupanmanagement.couponshandler.ViewInteractorImpl;
import com.db.coupanmanagement.couponshandler.ViewPresenter;
import com.db.coupanmanagement.couponshandler.ViewPresenterImpl;
import com.db.coupanmanagement.network.CouponManagementAPIClient;
import com.db.coupanmanagement.network.CouponManagementAPIInterface;

/**
 * Created by hp on 21-06-2018.
 */

public abstract class BaseDialogFragment extends DialogFragment implements ViewHandler {

    protected ViewPresenter mViewPresenter;
    protected BaseActivity.APIModelProvider mApiModelProvider;


    /**
     * This method will be used to get data from bundles & render on UI
     */
    protected abstract void fetchDataFromServer(String... data);

    protected Context mContext;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = getActivity();
        // Get device id from preference

    }

   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        // Get device id from preference
        SharedPreferences preferences = mContext.getSharedPreferences(CouponManagementConstants.PREF_KEY_DEVICE_ID_SHARED_NAME, Context.MODE_PRIVATE);
        mUniqueDeviceId = preferences.getString(CouponManagementConstants.PREF_KEY_UNIQUE_DEVICE_ID, "");
    }*/

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewPresenter = new ViewPresenterImpl(this, new ViewInteractorImpl());
        mViewPresenter.validateNetworkConnectivity(mContext);
    }

    public void setAPIModelProvider(BaseActivity.APIModelProvider apiModelProvider) {
        this.mApiModelProvider = apiModelProvider;
    }

    protected CouponManagementAPIInterface getAPIInterface() {
        if (mApiModelProvider != null) {
            return CouponManagementAPIClient.getClient(mApiModelProvider.getAPIModel().getAuthToken(), mApiModelProvider.getAPIModel().getDbId(), mApiModelProvider.getAPIModel().getUuId(), mApiModelProvider.getAPIModel().getuId(), mApiModelProvider.getAPIModel().getAppId(),mApiModelProvider.getAPIModel().getAppKey()).create(CouponManagementAPIInterface.class);
        } else {
            return null;
        }
    }
}
