package com.db.coupanmanagement.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bhaskar.utils.CommonUtils;
import com.db.coupanmanagement.activity.BaseActivity;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.network.CouponManagementAPIClient;
import com.db.coupanmanagement.network.CouponManagementAPIInterface;
import com.db.coupanmanagement.utils.CouponAppPreferences;

/**
 * Created by hp on 21-06-2018.
 */

public abstract class BaseFragment extends Fragment/* implements ViewHandler*/ {

    protected BaseActivity.APIModelProvider mApiModelProvider;
    private String mUniqueDeviceId;

    /**
     * This method will be used to get data from bundles & render on UI
     */
    public abstract void fetchDataFromServer(String... data);

    public abstract void networkConnectivityError();

    public abstract void networkConnectivitySuccess();

    protected Context mContext;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = getActivity();
        // Get device id from preference
        mUniqueDeviceId = CouponAppPreferences.getInstance(mContext).getStringValue(CouponManagementConstants.PREF_KEY_UNIQUE_DEVICE_ID, "");
    }

  /*  @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        // Get device id from preference
        SharedPreferences preferences = mContext.getSharedPreferences(CouponManagementConstants.PREF_KEY_DEVICE_ID_SHARED_NAME, Context.MODE_PRIVATE);
        mUniqueDeviceId = preferences.getString(CouponManagementConstants.PREF_KEY_UNIQUE_DEVICE_ID, "");
    }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*mViewPresenter = new ViewPresenterImpl(this, new ViewInteractorImpl());
        mViewPresenter.validateNetworkConnectivity(mContext);*/
    }

    public void setAPIModelProvider(BaseActivity.APIModelProvider apiModelProvider) {
        this.mApiModelProvider = apiModelProvider;
    }

    protected CouponManagementAPIInterface getAPIInterface() {
        try {
            return CouponManagementAPIClient.getClient(mApiModelProvider.getAPIModel().getAuthToken(), mApiModelProvider.getAPIModel().getDbId(), mApiModelProvider.getAPIModel().getUuId(), mApiModelProvider.getAPIModel().getuId(), mApiModelProvider.getAPIModel().getAppId(),mApiModelProvider.getAPIModel().getAppKey()).create(CouponManagementAPIInterface.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onDestroy() {
        CommonUtils.runGc();
        super.onDestroy();
    }
}
