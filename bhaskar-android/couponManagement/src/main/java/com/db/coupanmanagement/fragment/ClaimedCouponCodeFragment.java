package com.db.coupanmanagement.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.activity.CouponManagementActivity;
import com.db.coupanmanagement.adapter.ClaimedCouponCodeRecyclerViewAdapter;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.dialog.CouponDialogUtils;
import com.db.coupanmanagement.dialog.OfflineCouponCodeDialog;
import com.db.coupanmanagement.model.ClaimedCouponDetailListModel;
import com.db.coupanmanagement.model.ClaimedCouponDetailsModel;
import com.db.coupanmanagement.model.GetClaimedCouponsRequestParam;
import com.db.coupanmanagement.network.CouponManagementAPIInterface;
import com.db.coupanmanagement.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.db.coupanmanagement.utils.Utils.errorLogger;

/**
 * Created by hp on 01-06-2018.
 */

public class ClaimedCouponCodeFragment extends BaseDialogFragment implements ClaimedCouponCodeRecyclerViewAdapter.GetCouponDetails {
    private static final String TAG = CouponManagementConstants.GetFragmentName.CLAIMED_COUPONS_FRAGMENT;
    private RecyclerView mRecyclerView;
    private LinearLayout mLinProgress;
    private List<ClaimedCouponDetailsModel> mClaimedCouponDetailsModelList = new ArrayList<>();
    private TextView mTvNoClaimedCouponsAvailable, tvMonth;
    private CouponManagementActivity mActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        mActivity = (CouponManagementActivity) context;
        Log.w("onAttach", "mActivity: " + mActivity);
    }

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_claimed_coupon_code_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        tvMonth = rootView.findViewById(R.id.tv_month);
        mRecyclerView = rootView.findViewById(R.id.rv_claimed_coupons);
        mLinProgress = rootView.findViewById(R.id.lin_progress);
        ImageView icClose = rootView.findViewById(R.id.iv_close);
        mTvNoClaimedCouponsAvailable = rootView.findViewById(R.id.tv_no_claimed_coupons_available);
        icClose.setOnClickListener(v -> dismiss());
        rootView.setOnClickListener(null);

        fetchDataFromServer();
    }

    @Override
    protected void fetchDataFromServer(String... data) {
        if (Utils.isNetworkConnected(mActivity)) {
            Bundle extras = getArguments();
            if (null != extras) {
                int offerId = extras.getInt(CouponManagementConstants.GetBundleKeys.OFFER_ID);
                int planId = extras.getInt(CouponManagementConstants.GetBundleKeys.PLAN_ID);
                int monthId = extras.getInt(CouponManagementConstants.GetBundleKeys.MONTH_ID);
                int year = extras.getInt(CouponManagementConstants.GetBundleKeys.YEAR);
                getClaimedCoupons(offerId, planId, monthId, year);
            }
        } else {
            networkConnectivityError();
        }
    }

    @Override
    public void onCodeDekheinClickListner(ClaimedCouponDetailsModel claimedCouponDetailsModel) {
        if (null != mActivity) {
            try {
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(ContextCompat.getColor(mActivity, R.color.text_color_orange));
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(mActivity, Uri.parse(claimedCouponDetailsModel.getRedirectUrl()));
            } catch (Exception e) {
                Log.e("Error", "Error: " + e.getMessage() + " >> " + claimedCouponDetailsModel.getRedirectUrl());
            }
        }
    }

    @Override
    public void onTermAndConditionClickListner(ClaimedCouponDetailsModel couponDetailsModel) {
        Bundle extras = getArguments();
        if (null != extras) {
            extras.putString(CouponManagementConstants.GetBundleKeys.IMAGE_URL, couponDetailsModel.getVendorImage());
            extras.putString(CouponManagementConstants.GetBundleKeys.DESCRIPTION, couponDetailsModel.getCouponDescription());
            extras.putString(CouponManagementConstants.GetBundleKeys.TERM_AND_CONDITION, couponDetailsModel.getTermAndCondition());
            if (null != mActivity) {
                // Track current & next screen
                mActivity.trackEventForNextScreen(mApiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenName.MYOFFERS_CLAIM_OFFERS, CouponManagementConstants.GAScreenName.MYOFFERS_TERM_AND_CONDITIONS);
                new CouponDialogUtils().prepareTermsAndConditionDialog(mActivity, extras);
            }
        }
    }

    @Override
    public void onClickOfflineCouponCodeDekhein(ClaimedCouponDetailsModel couponDetailsModel, ClaimedCouponCodeRecyclerViewAdapter.ClaimedCouponViewHolder holder, int itemPosition, OfflineCouponCodeDialog.OnClickCodeDikhaDiyaInterface clickCodeDikhaDiyaInterface) {
        new OfflineCouponCodeDialog().prepareOfflineCodeDekheinDialog(mActivity, couponDetailsModel, mApiModelProvider, holder, itemPosition, clickCodeDikhaDiyaInterface);
    }

    @Override
    public void onClickCodeDekhLiyaHai() {
        new CouponDialogUtils().prepareCodeDekhLiyaHaiDialog(mActivity);
    }

    /**
     * This method will be used to render data after data loading
     */
    private void renderUIWithData() {
        ClaimedCouponCodeRecyclerViewAdapter availableCouponsRecyclerViewAdapter = new ClaimedCouponCodeRecyclerViewAdapter(mActivity, mClaimedCouponDetailsModelList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(availableCouponsRecyclerViewAdapter);
        // Get device dimensions
        int[] deviceDimensions = Utils.getDeviceDimensions(Objects.requireNonNull(mActivity));
        if (deviceDimensions.length > 0) {
            mRecyclerView.setMinimumWidth(deviceDimensions[0] - CouponManagementConstants.deviceWidthToBe);
            mRecyclerView.setMinimumHeight(CouponManagementConstants.deviceHeight);
        } else {
            mRecyclerView.setMinimumWidth(CouponManagementConstants.deviceWidth);
            mRecyclerView.setMinimumHeight(CouponManagementConstants.deviceHeight);
        }
    }

    /**
     * Prepare list with static data
     */
    private void getClaimedCoupons(int offerId, int planId, int monthId, int year) {
        mLinProgress.setVisibility(View.VISIBLE);
        GetClaimedCouponsRequestParam networkRequestParam = new GetClaimedCouponsRequestParam(offerId, planId, monthId, year);
        CouponManagementAPIInterface apiInterface = getAPIInterface();
        if (null != apiInterface) {
            Call<ClaimedCouponDetailListModel> call = apiInterface.doPostClaimedCouponList(networkRequestParam);
            call.enqueue(new Callback<ClaimedCouponDetailListModel>() {
                @Override
                public void onResponse(@NonNull Call<ClaimedCouponDetailListModel> call, @NonNull Response<ClaimedCouponDetailListModel> response) {
                    ClaimedCouponDetailListModel claimedCouponDetailListModel = response.body();
                    mLinProgress.setVisibility(View.GONE);
                    if (null != claimedCouponDetailListModel) {
                        if (claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_SUCCESS) {
                            if (null != claimedCouponDetailListModel.getData()) {
                                renderClaimedCoupons(claimedCouponDetailListModel);
                            } else {
                                tvMonth.setVisibility(View.INVISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                                mTvNoClaimedCouponsAvailable.setText(mActivity.getString(R.string.no_claimed_coupons_available));
                                mTvNoClaimedCouponsAvailable.setVisibility(View.VISIBLE);
                            }
                        } else if (claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_BAD_REQUEST || claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_INTERNAL_SERVER_ERROR || claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_NOT_FOUND) {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_claimed_coupons));
                        } else if (claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_UNAUTHORIZED_ACCESS) {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.please_login));
                        } else {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_claimed_coupons));
                        }
                    } else {
                        // Show metwork error
                        showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_claimed_coupons));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ClaimedCouponDetailListModel> call, @NonNull Throwable t) {
                    errorLogger(TAG, t.getMessage());
                    // Show metwork error
                    showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_claimed_coupons));
                }
            });
        }
    }

    /**
     * This method will be used to show network message
     */
    private void showNetworkErrorMessage(String message) {
        mLinProgress.setVisibility(View.GONE);
        mLinProgress.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        tvMonth.setVisibility(View.INVISIBLE);
        mTvNoClaimedCouponsAvailable.setVisibility(View.VISIBLE);
        mTvNoClaimedCouponsAvailable.setText(message);
    }

    /**
     * This method will be used to render claimed coupons data
     *
     * @param claimedCouponDetailListModel
     */
    private void renderClaimedCoupons(ClaimedCouponDetailListModel claimedCouponDetailListModel) {
        mTvNoClaimedCouponsAvailable.setVisibility(View.GONE);
        tvMonth.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
        // Set month
        String couponMonthName = claimedCouponDetailListModel.getData().getMonthName();
        int couponYear = claimedCouponDetailListModel.getData().getYear();
        String month = couponMonthName + " " + couponYear;
        tvMonth.setText(month);
        // Render data
        mClaimedCouponDetailsModelList = claimedCouponDetailListModel.getData().getCoupons();
        if (null != mClaimedCouponDetailsModelList && !mClaimedCouponDetailsModelList.isEmpty()) {
            renderUIWithData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != mActivity) {
            mActivity.setToolbarTItle(mActivity.getString(R.string.claimed_coupons));
            // Show toolbar back icon
            mActivity.setToolbarBackIconVisible(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Bundle extras = getArguments();
        if (null != extras) {
            if (mActivity != null) {
                mActivity.setToolbarTItle(extras.getString(CouponManagementConstants.GetBundleKeys.TITLE));
                // Show toolbar back icon
                mActivity.setToolbarBackIconVisible(true);
            }
        }
    }

    @Override
    public void networkConnectivityError() {
        if (mClaimedCouponDetailsModelList.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            tvMonth.setVisibility(View.INVISIBLE);
            mTvNoClaimedCouponsAvailable.setVisibility(View.VISIBLE);
            mTvNoClaimedCouponsAvailable.setText(mActivity.getString(R.string.network_unavailable));
        }
    }

    @Override
    public void networkConnectivitySuccess() {
        if (mClaimedCouponDetailsModelList.isEmpty()) {
            fetchDataFromServer();
        }
    }
}
