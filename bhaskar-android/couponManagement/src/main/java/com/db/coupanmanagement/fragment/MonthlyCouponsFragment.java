package com.db.coupanmanagement.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.activity.CouponManagementActivity;
import com.db.coupanmanagement.adapter.MonthlyCouponsRecyclerViewAdapter;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.dialog.CouponClaimSuccessDialog;
import com.db.coupanmanagement.dialog.CouponDialogUtils;
import com.db.coupanmanagement.model.ClaimCouponRequestParam;
import com.db.coupanmanagement.model.ClaimedCouponDetailListModel;
import com.db.coupanmanagement.model.ClaimedCouponDetailsModel;
import com.db.coupanmanagement.model.CouponRedeem;
import com.db.coupanmanagement.model.MonthlyOfferDetailsRequestParam;
import com.db.coupanmanagement.network.CouponManagementAPIInterface;
import com.db.coupanmanagement.recyclerviewitemdecorator.RecyclerViewDividerItemDecoration;
import com.db.coupanmanagement.utils.CouponAppPreferences;
import com.db.coupanmanagement.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.db.coupanmanagement.utils.Utils.errorLogger;

/**
 * Created by hp on 15-05-2018.
 */

public class MonthlyCouponsFragment extends BaseFragment implements MonthlyCouponsRecyclerViewAdapter.GetCouponDetails, View.OnClickListener {
    private static final String TAG = CouponManagementConstants.GetFragmentName.MONTHLY_COUPONS_FRAGMENT;
    private Set<ClaimedCouponDetailsModel> mcartCouponModels = new HashSet<>();
    private List<ClaimedCouponDetailsModel> mCouponModelList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private LinearLayout mLinProgress, mLinMonthlyCouponParent;
    private TextView mTvMonthName, mtvTotalMonthlyReward;
    private TextView mTvClaimAmount, mTvNoCouponsAvailable;
    private MonthlyCouponsRecyclerViewAdapter mMonthlyCouponsRecyclerViewAdapter;
    private float monthlyTotalAmount;
    private CouponManagementActivity mActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_monthly_coupans_layout, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (CouponManagementActivity) activity;
        Log.w("onAttach", "mActivity: " + mActivity);
    }

   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (CouponManagementActivity) context;
        Log.w("onAttach", "mActivity: " + mActivity);
    }*/

    @Override
    public void onViewCreated(@NonNull View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        mLinMonthlyCouponParent = rootView.findViewById(R.id.lin_monthly_coupon_parent);
        mTvMonthName = rootView.findViewById(R.id.tv_month_name);
        mtvTotalMonthlyReward = rootView.findViewById(R.id.tv_total_monthly_reward);
        mTvClaimAmount = rootView.findViewById(R.id.tv_claim_amount);
        mTvClaimAmount.setVisibility(View.GONE);
        mTvClaimAmount.setOnClickListener(this);
        mRecyclerView = rootView.findViewById(R.id.rv_monthly_coupons);
        mRecyclerView.setNestedScrollingEnabled(false);
        mLinProgress = rootView.findViewById(R.id.lin_progress);
        mTvNoCouponsAvailable = rootView.findViewById(R.id.tv_no_coupons_available);
        mTvNoCouponsAvailable.setVisibility(View.GONE);

        // Show toolbar back icon
        Objects.requireNonNull(mActivity).setToolbarBackIconVisible(true);

        rootView.setOnClickListener(null);

        extractLatLong();
    }

    @Override
    public void fetchDataFromServer(String... data) {
        Bundle extras = getArguments();
        if (null != extras) {
            int monthId = extras.getInt(CouponManagementConstants.GetBundleKeys.MONTH_ID);
            int offerId = extras.getInt(CouponManagementConstants.GetBundleKeys.OFFER_ID);
            int planId = extras.getInt(CouponManagementConstants.GetBundleKeys.PLAN_ID);

            monthlyTotalAmount = extras.getFloat(CouponManagementConstants.GetBundleKeys.OFFER_MONTH_AMOUNT);
            String monthName = extras.getString(CouponManagementConstants.GetBundleKeys.MONTH_NAME);
            mtvTotalMonthlyReward.setText(Utils.formatFloatValue(Objects.requireNonNull(mActivity), monthlyTotalAmount));
            String totalReward = mActivity.getString(R.string.total) + " " + monthName + " " + mActivity.getString(R.string.reward);
            mTvMonthName.setText(totalReward);
            getCouponList(offerId, planId, monthId, data[0], data[1], data[2]);
        }
    }

    @Override
    public void onTermAndConditionClickListner(ClaimedCouponDetailsModel couponDetailsModel) {
        Bundle extras = getArguments();
        if (null != extras) {
            extras.putString(CouponManagementConstants.GetBundleKeys.IMAGE_URL, couponDetailsModel.getVendorImage());
            extras.putString(CouponManagementConstants.GetBundleKeys.DESCRIPTION, couponDetailsModel.getCouponDescription());
            extras.putString(CouponManagementConstants.GetBundleKeys.TERM_AND_CONDITION, couponDetailsModel.getTermAndCondition());
            if (null != mActivity) {
                // Track current & next screen
                mActivity.trackEventForNextScreen(mApiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenName.MYOFFERS_CLAIM_OFFERS, CouponManagementConstants.GAScreenName.MYOFFERS_TERM_AND_CONDITIONS);
                new CouponDialogUtils().prepareTermsAndConditionDialog(mActivity, extras);
            }
        }
    }

    @Override
    public void onUpdateCart(ClaimedCouponDetailsModel couponDetailsModel, int position) {
        if (couponDetailsModel.getQty() == 0) {
            mcartCouponModels.remove(couponDetailsModel);
            float redeemed_amount = getTotalRedeemedAmount(mcartCouponModels);
            couponDetailsModel.setClaimable(true);
            mCouponModelList.set(position, couponDetailsModel);
            renderUIWithData(mCouponModelList);
            if (redeemed_amount > 0) {
                mTvClaimAmount.setVisibility(View.VISIBLE);
            } else {
                mTvClaimAmount.setVisibility(View.GONE);
            }
            String redeemedAmount = mActivity.getString(R.string.claim) + " " + Utils.formatFloatValue(Objects.requireNonNull(mActivity), redeemed_amount);
            mTvClaimAmount.setText(redeemedAmount);
            // Compare & enable unselected coupon icon
            float totalSelectedAmountForClaim = getTotalRedeemedAmount(mcartCouponModels);
            if (totalSelectedAmountForClaim < monthlyTotalAmount) {
                enableUnselectedCoupons();
            }
        } else {
            Set<ClaimedCouponDetailsModel> tempCartCouponModels = new HashSet<>();
            tempCartCouponModels.addAll(mcartCouponModels);
            tempCartCouponModels.add(couponDetailsModel);
            float redeemed_amount = getTotalRedeemedAmount(tempCartCouponModels);
            if (redeemed_amount > monthlyTotalAmount) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle(mActivity.getString(R.string.coupon_app_name));
                builder.setMessage(mActivity.getString(R.string.exceed_redeem_amount_msg));
                builder.setPositiveButton(mActivity.getString(R.string.ok_msg_btn), null);
                builder.show();
                couponDetailsModel.setQty(couponDetailsModel.getQty() - 1);
                couponDetailsModel.setClaimable(false);
                mCouponModelList.set(position, couponDetailsModel);
                renderUIWithData(mCouponModelList);
            } else if (getTotalAmountForVender(tempCartCouponModels, couponDetailsModel.getVendorId()) > couponDetailsModel.getDealAmount()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle(mActivity.getString(R.string.coupon_app_name));
                String msg = mActivity.getString(R.string.coupon_claim_exceed, couponDetailsModel.getVendorName(), Utils.formatFloatValue(Objects.requireNonNull(mActivity), couponDetailsModel.getDealAmount()));
//                String msg = mActivity.getString(R.string.you) + " " + couponDetailsModel.getVendorName() + " " + mActivity.getString(R.string.ke) + " " + Utils.formatFloatValue(Objects.requireNonNull(mActivity), couponDetailsModel.getDealAmount()) + " " + mActivity.getString(R.string.msg_vender_redeemed_amount);
                builder.setMessage(msg);
                builder.setPositiveButton(mActivity.getString(R.string.ok_msg_btn), null);
                builder.show();
                couponDetailsModel.setQty(couponDetailsModel.getQty() - 1);
                couponDetailsModel.setClaimable(false);
                mCouponModelList.set(position, couponDetailsModel);
                renderUIWithData(mCouponModelList);
            } else {
                couponDetailsModel.setClaimable(true);
                mCouponModelList.set(position, couponDetailsModel);
                mcartCouponModels = tempCartCouponModels;
                renderUIWithData(mCouponModelList);

                if (redeemed_amount > 0) {
                    mTvClaimAmount.setVisibility(View.VISIBLE);
                } else {
                    mTvClaimAmount.setVisibility(View.GONE);
                }
                String redeemedAmount = mActivity.getString(R.string.claim) + " " + Utils.formatFloatValue(Objects.requireNonNull(mActivity), redeemed_amount);
                mTvClaimAmount.setText(redeemedAmount);
                // Compare & enable unselected coupon icon
                float totalSelectedAmountForClaim = getTotalRedeemedAmount(mcartCouponModels);
                if (totalSelectedAmountForClaim < monthlyTotalAmount) {
                    enableUnselectedCoupons();
                }
            }
        }
    }

    /**
     * This method will be used to enable unselected coupons disable icon
     */
    private void enableUnselectedCoupons() {
        for (ClaimedCouponDetailsModel detailsModel : mCouponModelList) {
            if (detailsModel.getQty() == 0) {
                detailsModel.setClaimable(true);
            }
        }
        mMonthlyCouponsRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onOnlineOfflineCouponClick(ClaimedCouponDetailsModel couponDetailsModel, int position) {
        new CouponDialogUtils().prepareOnlineOfflineCouponMessageDialog(mActivity, couponDetailsModel.getIsOffline());
    }

    /**
     * This method will be used to render data after data loading
     */
    private void renderUIWithData(List<ClaimedCouponDetailsModel> couponModelList) {
        if (null == mMonthlyCouponsRecyclerViewAdapter) {
            mMonthlyCouponsRecyclerViewAdapter = new MonthlyCouponsRecyclerViewAdapter(mActivity, couponModelList, MonthlyCouponsFragment.this);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(layoutManager);
            mRecyclerView.setAdapter(mMonthlyCouponsRecyclerViewAdapter);
        } else {
            mMonthlyCouponsRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

    public float getTotalAmountForVender(Set<ClaimedCouponDetailsModel> couponModelList, int venderId) {
        float total = 0;
        for (ClaimedCouponDetailsModel couponDetailsModel : couponModelList) {
            if (couponDetailsModel.getVendorId() == venderId) {
                total = total + (couponDetailsModel.getQty() * couponDetailsModel.getCouponAmount());
            }
        }
        return total;
    }


    public float getTotalRedeemedAmount(Set<ClaimedCouponDetailsModel> couponModelList) {
        float total = 0;
        for (ClaimedCouponDetailsModel couponDetailsModel : couponModelList) {
            total = total + (couponDetailsModel.getQty() * couponDetailsModel.getCouponAmount());
        }
        return total;
    }

    // API call for get coupon list acording  to month.
    private void getCouponList(int offerId, int planId, int monthId, String latitude, String longitude, String city) {
        mLinProgress.setVisibility(View.VISIBLE);
        mLinMonthlyCouponParent.setVisibility(View.VISIBLE);
        MonthlyOfferDetailsRequestParam networkRequestParam = new MonthlyOfferDetailsRequestParam(offerId, planId, monthId, latitude, longitude, city);
        CouponManagementAPIInterface apiInterface = getAPIInterface();
        if (null != apiInterface) {
            Call<ClaimedCouponDetailListModel> call = apiInterface.doPostMonthlyOffersDetail(networkRequestParam);
            call.enqueue(new Callback<ClaimedCouponDetailListModel>() {
                @Override
                public void onResponse(@NonNull Call<ClaimedCouponDetailListModel> call, @NonNull Response<ClaimedCouponDetailListModel> response) {
                    ClaimedCouponDetailListModel claimedCouponDetailListModel = response.body();
                    mLinProgress.setVisibility(View.GONE);
                    if (null != claimedCouponDetailListModel) {
                        if (claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_SUCCESS) {
                            if (null != claimedCouponDetailListModel.getData()) {
                                renderMonthlyCoupons(claimedCouponDetailListModel);
                            } else {
                                mLinMonthlyCouponParent.setVisibility(View.GONE);
                                mTvNoCouponsAvailable.setText(mActivity.getString(R.string.no_coupons_available));
                                mTvNoCouponsAvailable.setVisibility(View.VISIBLE);
                            }
                        } else if (claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_BAD_REQUEST || claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_INTERNAL_SERVER_ERROR || claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_NOT_FOUND) {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_maasik_coupons));
                        } else if (claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_UNAUTHORIZED_ACCESS) {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.please_login));
                        } else {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_maasik_coupons));
                        }
                    } else {
                        // Show metwork error
                        showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_maasik_coupons));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ClaimedCouponDetailListModel> call, @NonNull Throwable t) {
                    errorLogger(TAG, t.getMessage());
                    // Show metwork error
                    showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_maasik_coupons));
                }
            });
        }
    }

    /**
     * This method will be used to show network message
     */
    private void showNetworkErrorMessage(String message) {
        mLinProgress.setVisibility(View.GONE);
        mLinMonthlyCouponParent.setVisibility(View.GONE);
        mTvNoCouponsAvailable.setVisibility(View.VISIBLE);
        mTvNoCouponsAvailable.setText(message);
    }

    /**
     * This method will be used to render monthly coupons data
     *
     * @param claimedCouponDetailListModel
     */
    private void renderMonthlyCoupons(ClaimedCouponDetailListModel claimedCouponDetailListModel) {
        mTvNoCouponsAvailable.setVisibility(View.GONE);
        mTvMonthName.setVisibility(View.VISIBLE);
        mtvTotalMonthlyReward.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mCouponModelList.clear();
        mCouponModelList.addAll(claimedCouponDetailListModel.getData().getCoupons());
        if (mCouponModelList.size() > 0) {
            mMonthlyCouponsRecyclerViewAdapter = new MonthlyCouponsRecyclerViewAdapter(mActivity, mCouponModelList, MonthlyCouponsFragment.this);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(layoutManager);
            mRecyclerView.addItemDecoration(new RecyclerViewDividerItemDecoration(ContextCompat.getDrawable(Objects.requireNonNull(mActivity), R.drawable.dotted_separator)));
            mRecyclerView.setAdapter(mMonthlyCouponsRecyclerViewAdapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != mActivity) {
            // Track GA current & previous screen
            if (mApiModelProvider != null) {
                mActivity.trackPageView(mApiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS,
                        CouponManagementConstants.GAScreenName.MYOFFERS_CLAIM_OFFERS, CouponManagementConstants.GAScreenName.MYOFFERS_MONTHLY_OFFERS);
            }

            mActivity.setToolbarTItle(mActivity.getString(R.string.maasik_coupons));
            // Show toolbar back icon
            mActivity.setToolbarBackIconVisible(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Bundle extras = getArguments();
        if (null != extras) {
            if (null != mActivity) {
                mActivity.setToolbarTItle(extras.getString(CouponManagementConstants.GetBundleKeys.TITLE));
                // Show toolbar back icon
                mActivity.setToolbarBackIconVisible(true);
            }
        }
    }

    // claim button click for api call
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_claim_amount) {
            List<CouponRedeem> couponRedeemList = new ArrayList<>();
            for (ClaimedCouponDetailsModel couponDetailsModel : mcartCouponModels) {
                CouponRedeem couponRedeem1 = new CouponRedeem();
                couponRedeem1.setAmount(couponDetailsModel.getCouponAmount());
                couponRedeem1.setQuantity(couponDetailsModel.getQty());
                couponRedeem1.setVendorId(couponDetailsModel.getVendorId());
                couponRedeem1.setIsOffline(couponDetailsModel.getIsOffline());
                couponRedeemList.add(couponRedeem1);
            }

            // Compare total amount selected by user for claim with monthlyTotalAmount
            float totalSelectedAmountForClaim = getTotalRedeemedAmount(mcartCouponModels);
            if (totalSelectedAmountForClaim < monthlyTotalAmount) {
                new CouponDialogUtils().showErrorDialog(mActivity, mActivity.getString(R.string.below_redeem_amount_msg, Utils.formatFloatValue(Objects.requireNonNull(mActivity), totalSelectedAmountForClaim), Utils.formatFloatValue(Objects.requireNonNull(mActivity), monthlyTotalAmount)));
            } else {
                mTvClaimAmount.setEnabled(false);
                mTvClaimAmount.setAlpha(0.4f);
                // Call claim coupon API
                callClaimCouponAPI(couponRedeemList, totalSelectedAmountForClaim);
            }
        }
    }

    private void callClaimCouponAPI(List<CouponRedeem> couponRedeemList, float totalSelectedAmountForClaim) {
        Bundle extras = getArguments();
        if (null != extras) {
            int monthId = extras.getInt(CouponManagementConstants.GetBundleKeys.MONTH_ID);
            int offerId = extras.getInt(CouponManagementConstants.GetBundleKeys.OFFER_ID);
            int planId = extras.getInt(CouponManagementConstants.GetBundleKeys.PLAN_ID);
            int year = extras.getInt(CouponManagementConstants.GetBundleKeys.YEAR);
            ClaimCouponRequestParam requestParam = new ClaimCouponRequestParam(offerId, planId, monthId, year, couponRedeemList);
            // Claim selected coupons
            claimSelectedCoupon(requestParam, totalSelectedAmountForClaim);
        }
    }

    /**
     * api call for claim coupon
     */
    private void claimSelectedCoupon(final ClaimCouponRequestParam requestParam, float totalSelectedAmountForClaim) {
        if (Utils.isNetworkConnected(mActivity)) {
            mLinProgress.setVisibility(View.VISIBLE);
            CouponManagementAPIInterface apiInterface = getAPIInterface();
            if (null != apiInterface) {
                Call<ClaimedCouponDetailListModel> call = apiInterface.doPostClaimCoupon(requestParam);
                call.enqueue(new Callback<ClaimedCouponDetailListModel>() {
                    @Override
                    public void onResponse(@NonNull Call<ClaimedCouponDetailListModel> call, @NonNull Response<ClaimedCouponDetailListModel> response) {
                        ClaimedCouponDetailListModel claimedCouponDetailListModel = response.body();
                        mLinProgress.setVisibility(View.GONE);
                        if (null != claimedCouponDetailListModel) {
                            if (claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_SUCCESS) {
                                if (null != claimedCouponDetailListModel.getData()) {
                                    Bundle extras = getArguments();
                                    if (null != extras) {
                                        String monthName = extras.getString(CouponManagementConstants.GetBundleKeys.MONTH_NAME);
                                        // Track GA Event
                                        if (null != mActivity && null != mApiModelProvider.getAPIModel()) {
                                            mActivity.trackEvent(mApiModelProvider.getAPIModel(), CouponManagementConstants.GAEventCategory.MONTHLY_OFFERS, CouponManagementConstants.GAAction.OFFER_CLAIMED, monthName, Utils.formatFloatValueForGA(totalSelectedAmountForClaim));
                                            // Track current & previous screen
                                            mActivity.trackPageView(mApiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS_CLAIMED_SUCCESSFULLY, CouponManagementConstants.GAScreenName.MYOFFERS_CLAIMED_SUCCESSFULLY, CouponManagementConstants.GAScreenName.MYOFFERS_CLAIMED_SUCCESSFULLY);
                                        }
                                    }
                                    enableClaimAmountButton();
                                    // Show claimed coupon dialog
                                    showSucessDialog(mActivity.getString(R.string.msg_sucess_claimed_coupon), requestParam);
                                } else {
                                    // Show metwork error
                                    enableClaimAmountButton();
                                    mRecyclerView.getAdapter().notifyDataSetChanged();
                                    new CouponDialogUtils().showErrorDialog(mActivity, claimedCouponDetailListModel.getMessage());
                                }
                            } else if (claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_BAD_REQUEST || claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_INTERNAL_SERVER_ERROR || claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_NOT_FOUND) {
                                enableClaimAmountButton();
                                // Show metwork error
                                new CouponDialogUtils().showErrorDialog(mActivity, mActivity.getString(R.string.unable_to_claim_maasik_coupons));
                            } else if (claimedCouponDetailListModel.getCode() == CouponManagementConstants.API_UNAUTHORIZED_ACCESS) {
                                enableClaimAmountButton();
                                // Show metwork error
                                new CouponDialogUtils().showErrorDialog(mActivity, mActivity.getString(R.string.please_login));
                            } else {
                                enableClaimAmountButton();
                                // Show metwork error
                                new CouponDialogUtils().showErrorDialog(mActivity, mActivity.getString(R.string.unable_to_claim_maasik_coupons));
                            }
                        } else {
                            enableClaimAmountButton();
                            // Show metwork error
                            new CouponDialogUtils().showErrorDialog(mActivity, mActivity.getString(R.string.unable_to_claim_maasik_coupons));
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ClaimedCouponDetailListModel> call, @NonNull Throwable t) {
                        errorLogger(TAG, t.getMessage());
                        enableClaimAmountButton();
                        mLinProgress.setVisibility(View.GONE);
                        new CouponDialogUtils().showErrorDialog(mActivity, mActivity.getString(R.string.unable_to_claim_maasik_coupons));
                    }
                });
            }
        } else {
            enableClaimAmountButton();
            CouponDialogUtils.showToastMessage(mActivity, mActivity.getString(R.string.network_unavailable));
        }
    }

    /**
     * This method will enable claim amount button
     */
    private void enableClaimAmountButton() {
        mTvClaimAmount.setEnabled(true);
        mTvClaimAmount.setAlpha(1.0f);
    }

    /**
     * This method will be used to show coupon claimed success dialog
     *
     * @param message
     * @param requestParam
     */
    public void showSucessDialog(String message, final ClaimCouponRequestParam requestParam) {
        if (!mActivity.isFinishing()) {
            new CouponClaimSuccessDialog(Objects.requireNonNull(mActivity), mActivity.getString(R.string.coupon_app_name), message, mActivity.getString(R.string.ok_msg_btn), new CouponClaimSuccessDialog.OnTheekHaiClickListener() {
                @Override
                public void onTheekHai() {
                    Objects.requireNonNull(mActivity).refreshMonthlyOffers();
                    Bundle extras = getArguments();
                    if (null != extras) {
                        extras.putInt(CouponManagementConstants.GetBundleKeys.OFFER_ID, requestParam.getOfferId());
                        extras.putInt(CouponManagementConstants.GetBundleKeys.PLAN_ID, requestParam.getPlanId());
                        extras.putInt(CouponManagementConstants.GetBundleKeys.MONTH_ID, requestParam.getMonthId());
                        extras.putInt(CouponManagementConstants.GetBundleKeys.YEAR, requestParam.getYear());
                        extras.putString(CouponManagementConstants.GetBundleKeys.TITLE, mActivity.getString(R.string.maasik_coupons));
                        if (null != mActivity) {
                            FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
                            Fragment prev = mActivity.getSupportFragmentManager().findFragmentByTag(CouponManagementConstants.DIALOG_TAG);
                            if (prev != null) {
                                transaction.remove(prev);
                            }
                            transaction.addToBackStack(null);
                            BaseDialogFragment dialogFragment = new ClaimedCouponCodeFragment();
                            dialogFragment.setArguments(extras);
                            dialogFragment.setAPIModelProvider(mActivity);
                            dialogFragment.show(transaction, CouponManagementConstants.DIALOG_TAG);
                        }
                    }
                }
            }).show();
        }
    }

    @Override
    public void networkConnectivityError() {
        if (null == mMonthlyCouponsRecyclerViewAdapter) {
            mLinMonthlyCouponParent.setVisibility(View.GONE);
            mTvMonthName.setVisibility(View.GONE);
            mtvTotalMonthlyReward.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            mTvNoCouponsAvailable.setVisibility(View.VISIBLE);
            mTvNoCouponsAvailable.setText(mActivity.getString(R.string.network_unavailable));
        }
    }

    @Override
    public void networkConnectivitySuccess() {
        extractLatLong();
    }

    private void extractLatLong() {
        if (Utils.isNetworkConnected(mActivity)) {
            if (mCouponModelList.isEmpty()) {
                String latitude = CouponAppPreferences.getInstance(mContext).getStringValue(CouponManagementConstants.GetPreferenceKeys.LATTITUDE, "");
                String longitude = CouponAppPreferences.getInstance(mContext).getStringValue(CouponManagementConstants.GetPreferenceKeys.LONGITUDE, "");
                String city = CouponAppPreferences.getInstance(mContext).getStringValue(CouponManagementConstants.GetPreferenceKeys.CITY, "");
                if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
                    fetchDataFromServer(latitude, longitude, city);
                } else {
                    fetchDataFromServer("", "", ""); // latitude, longitude, city - in case of no data found
                }
            }
        } else {
            networkConnectivityError();
        }
    }
}
