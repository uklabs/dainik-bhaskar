package com.db.coupanmanagement.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.activity.CouponManagementActivity;
import com.db.coupanmanagement.adapter.MonthlyOffersRecyclerViewAdapter;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.dialog.CouponDialogUtils;
import com.db.coupanmanagement.dialog.MobileNumberOTPDialog;
import com.db.coupanmanagement.model.AvailableOfferModel;
import com.db.coupanmanagement.model.MonthlyOfferListModel;
import com.db.coupanmanagement.model.MonthlyOfferListRequestParam;
import com.db.coupanmanagement.model.MonthlyOfferModel;
import com.db.coupanmanagement.model.MonthlyOfferPlanModel;
import com.db.coupanmanagement.network.CouponManagementAPIInterface;
import com.db.coupanmanagement.utils.CouponAppPreferences;
import com.db.coupanmanagement.utils.LocationProviderUtils;
import com.db.coupanmanagement.utils.Utils;
import com.bhaskar.util.LoginController;
import com.bhaskar.appscommon.tracking.TrackingData;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.db.coupanmanagement.utils.Utils.errorLogger;

/**
 * Created by hp on 30-05-2018.
 */

public class MonthlyOffersFragment extends BaseFragment implements MonthlyOffersRecyclerViewAdapter.GetCouponDetails {
    private static final String TAG = CouponManagementConstants.GetFragmentName.MONTHLY_OFFERS_FRAGMENT;
    private RecyclerView mRecyclerView;
    private MonthlyOfferModel mCurrentActiveMonthlyPlan;
    private LinearLayout mLinProgress;
    private TextView mTvNoOffersAvailable, mTvTotalRewards, mTvClaimedAmount, mTvExpiredAmount, mTvWaitingAmount;
    private NestedScrollView mNestedScrollview;
    private MonthlyOfferPlanModel mMonthlyOfferPlanModel;
    private CouponManagementActivity mActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_monthly_offers_layout, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (CouponManagementActivity) activity;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = view.findViewById(R.id.rv_monthly_offers);
        mRecyclerView.setNestedScrollingEnabled(false);
        mLinProgress = view.findViewById(R.id.lin_progress);
        mNestedScrollview = view.findViewById(R.id.nested_scrollview);
        mTvNoOffersAvailable = view.findViewById(R.id.tv_no_monthly_offers_available);
        mTvTotalRewards = view.findViewById(R.id.tv_total_rewards);
        mTvClaimedAmount = view.findViewById(R.id.tv_claimed_amount);
        mTvExpiredAmount = view.findViewById(R.id.tv_expired_amount);
        mTvWaitingAmount = view.findViewById(R.id.tv_waiting_amount);

        // Show toolbar back icon
        mActivity.setToolbarBackIconVisible(true);

        view.setOnClickListener(null);

        fetchDataFromServer();
    }

    @Override
    public void fetchDataFromServer(String... data) {
        if (Utils.isNetworkConnected(mActivity)) {
            Bundle extras = getArguments();
            if (null != extras) {
                AvailableOfferModel availableOfferModel = (AvailableOfferModel) extras.getSerializable(CouponManagementConstants.GetBundleKeys.AVAILABLE_OFFER_MODEL);
                if (availableOfferModel != null) {
                    getOffersDetails(availableOfferModel.getOfferId());
                }
            }
        } else {
            networkConnectivityError();
        }
    }

    @Override
    public void onDetailsButtonCLickListner(MonthlyOfferPlanModel monthlyOfferPlanModel) {
        mMonthlyOfferPlanModel = monthlyOfferPlanModel;
        Bundle extras = getArguments();
        if (null != extras) {
            extras.putInt(CouponManagementConstants.GetBundleKeys.OFFER_ID, mCurrentActiveMonthlyPlan.getOfferId());
            extras.putInt(CouponManagementConstants.GetBundleKeys.PLAN_ID, mCurrentActiveMonthlyPlan.getOfferPlanId());
            extras.putInt(CouponManagementConstants.GetBundleKeys.MONTH_ID, monthlyOfferPlanModel.getMonthId());
            extras.putInt(CouponManagementConstants.GetBundleKeys.YEAR, monthlyOfferPlanModel.getYear());
            extras.putString(CouponManagementConstants.GetBundleKeys.TITLE, mActivity.getString(R.string.maasik_offers));
            if (null != mActivity) {
                FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
                Fragment prev = mActivity.getSupportFragmentManager().findFragmentByTag(CouponManagementConstants.DIALOG_TAG);
                if (null != prev) {
                    transaction.remove(prev);
                }
                transaction.addToBackStack(null);
                BaseDialogFragment dialogFragment = new ClaimedCouponCodeFragment();
                dialogFragment.setArguments(extras);
                dialogFragment.setAPIModelProvider(mActivity);
                dialogFragment.show(transaction, CouponManagementConstants.DIALOG_TAG);
            }
        }
    }

    @Override
    public void onClaimButtonCLickListner(MonthlyOfferPlanModel monthlyOfferPlanModel) {
        mMonthlyOfferPlanModel = monthlyOfferPlanModel;
        if (null != mApiModelProvider) {
            if (mApiModelProvider.getAPIModel().isUserLoggedIn()) {
                // Track Screen
                if (null != mActivity) {
                    // Track GA current & next screen
                    mActivity.trackEventForNextScreen(mApiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenName.MYOFFERS_MONTHLY_OFFERS, CouponManagementConstants.GAScreenName.MYOFFERS_CLAIM_OFFERS);
                }
                // Check location permission
                checkLocationAccess();
            } else {
                startActivityForResult(LoginController.loginController().getLoginIntent(getContext(), TrackingData.getDBId(getContext()), TrackingData.getDeviceId(getContext())), CouponManagementConstants.LOGIN_TO_CLAIM_COUPON_REQUEST);
            }
        }
    }

    /**
     * This method will be used to check location access of user
     */
    private void checkLocationAccess() {
        // Show a message to take location permission from user
        if (!LocationProviderUtils.checkLocationPermissions(mActivity)) {
            new CouponDialogUtils().showNormalMessageWithTwoActionButtonDialog(mActivity, mActivity.getString(R.string.location_permission_title), mActivity.getString(R.string.location_permission_message), false, mActivity.getString(R.string.turn_on), mActivity.getString(R.string.coupon_skip), new CouponDialogUtils.NormalMessageDialogResponsesInterface() {
                @Override
                public void doOnOkButtonClick(Activity activity) {
                    // Get location details
                    getLocationData();
                }

                @Override
                public void doOnSkipButtonClick(Activity activity) {
                    checkUserMobileNumberForSmsPermission();
                }
            });
        } else {
            // Get location details
            getLocationData();
        }
    }

    /**
     * This method will be used to get location data
     */
    private void getLocationData() {
        Objects.requireNonNull(mActivity).initLocationProvider(new OnLocationPermissionGrantedInterface() {
            @Override
            public void onLocationDataFound() {
                checkUserMobileNumberForSmsPermission();
            }

            @Override
            public void onLocationDataNotFound() {
                checkUserMobileNumberForSmsPermission();
            }
        });
    }

    /**
     * This method is used to check sms permission
     */
    private void checkUserMobileNumberForSmsPermission() {
        String mobileNumber = mApiModelProvider.getAPIModel().getMobileNo();
        if (!TextUtils.isEmpty(mobileNumber)) {
            navigateToMonthlyCoupon(mMonthlyOfferPlanModel);
        } else {
            String prefMobileNumber = CouponAppPreferences.getInstance(mContext).getStringValue(CouponManagementConstants.GetPreferenceKeys.MOBILE_NUMBER, "");
            String prefEmailId = CouponAppPreferences.getInstance(mContext).getStringValue(CouponManagementConstants.GetPreferenceKeys.EMAILID, "");
            if (!prefEmailId.equalsIgnoreCase(mApiModelProvider.getAPIModel().getEmailId()) && TextUtils.isEmpty(prefMobileNumber)) {
                new MobileNumberOTPDialog(mActivity).showEnterMobileNumberOTPDialog(mApiModelProvider, new MobileNumberOTPDialog.OnOTPVerifiedListner() {
                    @Override
                    public void onOTPVerified() {
                        navigateToMonthlyCoupon(mMonthlyOfferPlanModel);
                    }
                });
            } else {
                navigateToMonthlyCoupon(mMonthlyOfferPlanModel);
            }
        }
    }

    /**
     * This methof will be used to navigate to monthly coupon
     *
     * @param monthlyOfferPlanModel
     */
    private void navigateToMonthlyCoupon(MonthlyOfferPlanModel monthlyOfferPlanModel) {
        Bundle extras = getArguments();
        if (null != extras) {
            extras.putInt(CouponManagementConstants.GetBundleKeys.MONTH_ID, monthlyOfferPlanModel.getMonthId());
            extras.putInt(CouponManagementConstants.GetBundleKeys.OFFER_ID, mCurrentActiveMonthlyPlan.getOfferId());
            extras.putInt(CouponManagementConstants.GetBundleKeys.PLAN_ID, mCurrentActiveMonthlyPlan.getOfferPlanId());
            extras.putFloat(CouponManagementConstants.GetBundleKeys.OFFER_AMOUNT, mCurrentActiveMonthlyPlan.getMainOfferAmount());
            extras.putFloat(CouponManagementConstants.GetBundleKeys.OFFER_MONTH_AMOUNT, monthlyOfferPlanModel.getOfferAmount());
            extras.putString(CouponManagementConstants.GetBundleKeys.MONTH_NAME, monthlyOfferPlanModel.getMonthName());
            extras.putInt(CouponManagementConstants.GetBundleKeys.YEAR, monthlyOfferPlanModel.getYear());
            extras.putString(CouponManagementConstants.GetBundleKeys.TITLE, mActivity.getString(R.string.maasik_offers));
            if (mActivity != null) {
                mActivity.addFragment(new MonthlyCouponsFragment(), extras);
            }
        }
    }

    // API call for get available offers monthly details
    private void getOffersDetails(int offerId) {
        mLinProgress.setVisibility(View.VISIBLE);
        MonthlyOfferListRequestParam networkRequestParam = new MonthlyOfferListRequestParam(offerId);
        CouponManagementAPIInterface apiInterface = getAPIInterface();
        if (null != apiInterface) {
            Call<MonthlyOfferListModel> call = apiInterface.doPostMonthlyOffersList(networkRequestParam);
            call.enqueue(new Callback<MonthlyOfferListModel>() {
                @Override
                public void onResponse(@NonNull Call<MonthlyOfferListModel> call, @NonNull Response<MonthlyOfferListModel> response) {
                    MonthlyOfferListModel monthlyOfferListModel = response.body();
                    mLinProgress.setVisibility(View.GONE);
                    if (null != monthlyOfferListModel) {
                        if (monthlyOfferListModel.getCode() == CouponManagementConstants.API_SUCCESS) {
                            if (null != monthlyOfferListModel.getData()) {
                                renderMonthlyOffers(monthlyOfferListModel);
                            } else {
                                mNestedScrollview.setVisibility(View.GONE);
                                mTvNoOffersAvailable.setText(mActivity.getString(R.string.no_offers_available));
                                mTvNoOffersAvailable.setVisibility(View.VISIBLE);
                            }
                        } else if (monthlyOfferListModel.getCode() == CouponManagementConstants.API_BAD_REQUEST || monthlyOfferListModel.getCode() == CouponManagementConstants.API_INTERNAL_SERVER_ERROR || monthlyOfferListModel.getCode() == CouponManagementConstants.API_NOT_FOUND) {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_maasik_offers));
                        } else if (monthlyOfferListModel.getCode() == CouponManagementConstants.API_UNAUTHORIZED_ACCESS) {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.please_login));
                        } else {
                            // Show metwork error
                            showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_maasik_offers));
                        }
                    } else {
                        // Show metwork error
                        showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_maasik_offers));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MonthlyOfferListModel> call, @NonNull Throwable t) {
                    errorLogger(TAG, t.getMessage());
                    // Show metwork error
                    showNetworkErrorMessage(mActivity.getString(R.string.unable_to_load_maasik_offers));
                }
            });
        }
    }

    /**
     * This method will be used to show network message
     */
    private void showNetworkErrorMessage(String message) {
        mLinProgress.setVisibility(View.GONE);
        mNestedScrollview.setVisibility(View.GONE);
        mTvNoOffersAvailable.setVisibility(View.VISIBLE);
        mTvNoOffersAvailable.setText(message);
    }

    /**
     * This method will be used to render monthly offers data
     *
     * @param monthlyOfferListModel
     */
    private void renderMonthlyOffers(MonthlyOfferListModel monthlyOfferListModel) {
        mTvNoOffersAvailable.setVisibility(View.GONE);
        mNestedScrollview.setVisibility(View.VISIBLE);
        mCurrentActiveMonthlyPlan = monthlyOfferListModel.getData();
        if (mCurrentActiveMonthlyPlan != null && !mCurrentActiveMonthlyPlan.getMonthlyOfferPlanModels().isEmpty()) {
            // Set the total reward amount
            String totalReward = Utils.formatFloatValue(Objects.requireNonNull(mActivity), mCurrentActiveMonthlyPlan.getMainOfferAmount());
            mTvTotalRewards.setText(totalReward);

            // Get all calculated amount for monthly plans
            String claimedAmount = mActivity.getString(R.string.rupee_sign) + " " + mCurrentActiveMonthlyPlan.getUserClaimedAmount();
            String expiredAmount = mActivity.getString(R.string.rupee_sign) + " " + mCurrentActiveMonthlyPlan.getUserExpiredAmount();
            String waitingAmount = mActivity.getString(R.string.rupee_sign) + " " + mCurrentActiveMonthlyPlan.getUserAmountInWaiting();
            mTvClaimedAmount.setText(claimedAmount);
            mTvExpiredAmount.setText(String.valueOf(expiredAmount));
            mTvWaitingAmount.setText(String.valueOf(waitingAmount));

            // Render data for showing all available monthly offers
            MonthlyOffersRecyclerViewAdapter availableCouponsRecyclerViewAdapter = new MonthlyOffersRecyclerViewAdapter(mActivity, mCurrentActiveMonthlyPlan.getMonthlyOfferPlanModels(), mCurrentActiveMonthlyPlan.getCurrentDateTime(), MonthlyOffersFragment.this);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(layoutManager);
            mRecyclerView.setAdapter(availableCouponsRecyclerViewAdapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != mActivity) {
            if (mApiModelProvider != null) {
                // Track GA current & previous screen
                mActivity.trackPageView(mApiModelProvider.getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS,
                        CouponManagementConstants.GAScreenName.MYOFFERS_MONTHLY_OFFERS, CouponManagementConstants.GAScreenName.MYOFFERS);
            }
            // Set toolbar title
            mActivity.setToolbarTItle(mActivity.getString(R.string.maasik_offers));
            // Show toolbar back icon
            mActivity.setToolbarBackIconVisible(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Bundle extras = getArguments();
        if (null != extras) {
            if (null != mActivity) {
                String title = extras.getString(CouponManagementConstants.GetBundleKeys.KEY_TOOLBAR_TITLE);
                if (null != title && !title.isEmpty()) {
                    mActivity.setToolbarTItle(extras.getString(CouponManagementConstants.GetBundleKeys.KEY_TOOLBAR_TITLE));
                } else {
                    mActivity.setToolbarTItle(mActivity.getString(R.string.available_offers));
                }
                // Show toolbar back icon
//                boolean isComingFromHamburgerMenu = extras.getBoolean(CouponManagementConstants.GetBundleKeys.KEY_IS_COMING_FROM_HAMBURGER_MENU, false);
                mActivity.setToolbarBackIconVisible(true);
            }
        }
    }

    @Override
    public void networkConnectivityError() {
        if (null == mCurrentActiveMonthlyPlan) {
            mNestedScrollview.setVisibility(View.GONE);
            mTvNoOffersAvailable.setVisibility(View.VISIBLE);
            mTvNoOffersAvailable.setText(mActivity.getString(R.string.network_unavailable));
        }
    }

    @Override
    public void networkConnectivitySuccess() {
        if (null == mCurrentActiveMonthlyPlan) {
            fetchDataFromServer();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CouponManagementConstants.LOGIN_TO_CLAIM_COUPON_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                // Track GA Event
                if (mActivity != null) {
                    mActivity.trackEvent(mActivity.getAPIModel(), CouponManagementConstants.GAEventCategory.MONTHLY_OFFERS, CouponManagementConstants.GAAction.LOGIN_TO_CLAIM, CouponManagementConstants.GALabel.SUCCESSFUL, 0);

                    // Read API Model
                    mActivity.getAPIModel(intent);
                    // Scroll to top
                    Utils.scrollToItemPosition(0, mRecyclerView, mNestedScrollview);
                    // Refresh menu after logged in
                    Objects.requireNonNull(mActivity).invalidateOptionsMenu();
                    // Reload monthly offers
                    fetchDataFromServer();
                }
            }
        }
    }

    // This interface will be used to show enter mobile number dialog
    public interface OnReadSmsPermissionGrantedInterface {
        void onReadSmsPermissionGranted();
    }

    // This interface will be used to show enter mobile number dialog
    public interface OnLocationPermissionGrantedInterface {
        void onLocationDataFound();

        void onLocationDataNotFound();
    }
}
