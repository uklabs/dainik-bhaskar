package com.db.coupanmanagement.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.constant.CouponManagementConstants;

/**
 * Created by hp on 30-05-2018.
 */

public class RedeemCouponCodeFragment extends Fragment {
    private WebView mWvRedeemCoupon;
    private LinearLayout mLinProgress;

    @SuppressLint("SetJavaScriptEnabled")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_redeem_coupon_code_layout, container, false);

        mWvRedeemCoupon = rootView.findViewById(R.id.wv_redeem_coupon);
        mLinProgress = rootView.findViewById(R.id.lin_progress);
        mWvRedeemCoupon.getSettings().setJavaScriptEnabled(true);
        mWvRedeemCoupon.getSettings().setBuiltInZoomControls(true);
        mWvRedeemCoupon.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {

                if (progress == 100) {
                    mLinProgress.setVisibility(View.GONE);

                } else {
                    mLinProgress.setVisibility(View.VISIBLE);

                }
            }
        });
        rootView.setOnClickListener(null);
        loadArgumentData();
        return rootView;
    }

    /**
     * This method will be used to get data from bundles & render on UI
     */
    public void loadArgumentData() {
        if (getArguments() != null) {
            mWvRedeemCoupon.loadUrl(getArguments().getString(CouponManagementConstants.GetBundleKeys.REDIRECT_LINK));
        }
    }
}
