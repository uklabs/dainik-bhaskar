package com.db.coupanmanagement.fragment;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.db.coupanmanagement.R;
import com.db.coupanmanagement.activity.BaseActivity;
import com.db.coupanmanagement.activity.CouponManagementActivity;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.dialog.CouponDialogUtils;
import com.db.coupanmanagement.model.AvailableOfferModel;
import com.db.coupanmanagement.utils.Utils;

import java.util.Objects;

import static com.db.coupanmanagement.utils.Utils.parseDateToddMMMyyyy;


/**
 * Created by hp on 15-05-2018.
 */

public class WalkThroughFragment extends BaseFragment implements View.OnClickListener {

    private AvailableOfferModel mAvailableOfferModel;
    private TextView mTvOfferName, mTvOfferAmount, mTvOfferStartDate, mTvOfferEndDate; /*mTvDescription, */
    private ImageView mIvOffer;
    private TextView mTvDescription;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.walkthrough, container, false);
    }

    @Override
    public void fetchDataFromServer(String... data) {
        if (getArguments() != null) {
            mAvailableOfferModel = (AvailableOfferModel) getArguments().getSerializable(CouponManagementConstants.GetBundleKeys.AVAILABLE_OFFER_MODEL);
            if (mAvailableOfferModel != null) {
                renderDataOnViews(mAvailableOfferModel);
            }
        }
    }

    @Override
    public void networkConnectivityError() {
    }

    @Override
    public void networkConnectivitySuccess() {
    }

    // set data on views
    private void renderDataOnViews(AvailableOfferModel availableOfferModel) {
        String amount = Utils.formatFloatValue(Objects.requireNonNull(getActivity()), availableOfferModel.getMaxOfferAmount());
        mTvOfferAmount.setText(amount);
        mTvOfferName.setText(availableOfferModel.getOfferName());
        mTvOfferStartDate.setText(parseDateToddMMMyyyy(availableOfferModel.getOfferStartDate()));
        mTvOfferEndDate.setText(parseDateToddMMMyyyy(availableOfferModel.getOfferEndDate()));
        try {
//            if (availableOfferModel.getDescription().length() < 240) {
//                mTvDescription.setTrimLength(availableOfferModel.getDescription().length());
//            }
            mTvDescription.setText(Html.fromHtml(availableOfferModel.getDescription()));
        } catch (Exception ignored) {
        }
        // Load offer image
        Glide.with(Objects.requireNonNull(getActivity())).load(availableOfferModel.getOfferImage()).into(mIvOffer);
    }

    @Override
    public void onViewCreated(@NonNull View rootview, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootview, savedInstanceState);
        mTvOfferName = rootview.findViewById(R.id.tv_offer_name);
        mTvOfferAmount = rootview.findViewById(R.id.tv_offer_amount);
        mTvOfferStartDate = rootview.findViewById(R.id.tv_offer_start_date);
        mTvOfferEndDate = rootview.findViewById(R.id.tv_offer_end_date);
        mIvOffer = rootview.findViewById(R.id.iv_offer);
        mTvDescription = rootview.findViewById(R.id.tv_description);
        TextView tvBtnViewOffer = rootview.findViewById(R.id.tv_btn_view_offer);
        TextView tvBtnMoreInfo = rootview.findViewById(R.id.tv_btn_more_info);
        TextView tvSahayata = rootview.findViewById(R.id.tv_sahayata);

        tvBtnViewOffer.setOnClickListener(this);
        tvBtnMoreInfo.setOnClickListener(this);
        tvSahayata.setOnClickListener(this);
        rootview.setOnClickListener(null);
        // Render data
        fetchDataFromServer();
    }

    public static WalkThroughFragment newInstance(AvailableOfferModel availableOfferModel, Bundle extras) {
        final WalkThroughFragment fragment = new WalkThroughFragment();
        extras.putSerializable(CouponManagementConstants.GetBundleKeys.AVAILABLE_OFFER_MODEL, availableOfferModel);
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_btn_view_offer) {
            // Track Screen
            if (getActivity() != null && ((CouponManagementActivity) getActivity()).getAPIModel() != null) {
                // Track GA current & previous screen
                ((BaseActivity) getActivity()).trackPageView(((CouponManagementActivity) getActivity()).getAPIModel(), CouponManagementConstants.GAScreenCategory.MYOFFERS, CouponManagementConstants.GAScreenName.MYOFFERS_VIEW_OFFERS, CouponManagementConstants.GAScreenName.MYOFFERS);
                // Track GA current & next screen
                ((BaseActivity) getActivity()).trackEventForNextScreen(((CouponManagementActivity) getActivity()).getAPIModel(), CouponManagementConstants.GAScreenName.MYOFFERS, CouponManagementConstants.GAScreenName.MYOFFERS_MONTHLY_OFFERS);
            }
            // Navigate to monthly offer
            navigateToMonthlyOffer();
        } else if (v.getId() == R.id.tv_btn_more_info) {
            Bundle extras = getArguments();
            if (extras != null) {
                extras.putString(CouponManagementConstants.GetBundleKeys.IMAGE_URL, mAvailableOfferModel.getOfferImage());
                extras.putString(CouponManagementConstants.GetBundleKeys.DESCRIPTION, mAvailableOfferModel.getDescription());
                extras.putString(CouponManagementConstants.GetBundleKeys.TERM_AND_CONDITION, mAvailableOfferModel.getTermCondition());
                extras.putString(CouponManagementConstants.GetBundleKeys.FAQ_URL, mAvailableOfferModel.getFaqUrl());
                if (getActivity() != null && ((CouponManagementActivity) getActivity()).getAPIModel() != null) {
                    // Track current & next screen
                    ((BaseActivity) getActivity()).trackEventForNextScreen(((CouponManagementActivity) getActivity()).getAPIModel(), CouponManagementConstants.GAScreenName.MYOFFERS_MONTHLY_OFFERS, CouponManagementConstants.GAScreenName.MYOFFERS_MORE_INFO);
                    new CouponDialogUtils().prepareTermsAndConditionDialogForFaq(getActivity(), extras);
                }
            }
        } else if (v.getId() == R.id.tv_sahayata) {
            if (!TextUtils.isEmpty(mAvailableOfferModel.getFaqUrl())) {
                try {
                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                    builder.setToolbarColor(ContextCompat.getColor(mContext, R.color.text_color_orange));
                    CustomTabsIntent customTabsIntent = builder.build();
                    customTabsIntent.launchUrl(mContext, Uri.parse(mAvailableOfferModel.getFaqUrl()));
                } catch (Exception e) {
                    Log.e("Error", "Error: " + e.getMessage() + " >> " + mAvailableOfferModel.getFaqUrl());
                }
            }
        }
    }

    /**
     * This method will be used to navigate to monthly offers
     */
    private void navigateToMonthlyOffer() {
        Bundle extras = getArguments();
        if (extras != null) {
            if (getActivity() != null) {
                ((CouponManagementActivity) getActivity()).addFragment(new MonthlyOffersFragment(), extras);
            }
        }
    }

    @Override
    public void onResume() {
        // Track GA Event
        if (mAvailableOfferModel != null && getActivity() != null && ((CouponManagementActivity) getActivity()).getAPIModel() != null) {
            int amount = Utils.formatFloatValueForGA(mAvailableOfferModel.getMaxOfferAmount());
            ((BaseActivity) getActivity()).trackEvent(((CouponManagementActivity) getActivity()).getAPIModel(), CouponManagementConstants.GAEventCategory.MONTHLY_OFFERS, CouponManagementConstants.GAAction.REWARD_ASSIGNED, CouponManagementConstants.GALabel.SUCCESSFUL, amount);
        }
        super.onResume();
    }
}
