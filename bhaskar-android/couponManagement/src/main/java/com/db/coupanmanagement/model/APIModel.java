package com.db.coupanmanagement.model;

import java.io.Serializable;

/**
 * Created by hp on 22-06-2018.
 */

public class APIModel implements Serializable {

    private String uId;
    private String uuId;
    private String emailId;
    private String dbId;
    private String authToken;
    private String mobileNo;
    private String appId;
    private String appKey;


    private String deviceId;
    private boolean isComingFromHamburgerMenu, isUserLoggedIn;

    public APIModel(String uId, String uuId, String emailId, String dbId, String authToken, String mobileNo, String deviceId, String appId,String appKey, boolean isComingFromHamburgerMenu, boolean isUserLoggedIn) {
        this.uId = uId;
        this.uuId = uuId;
        this.emailId = emailId;
        this.dbId = dbId;
        this.authToken = authToken;
        this.mobileNo = mobileNo;
        this.deviceId = deviceId;
        this.appId = appId;
        this.appKey = appKey;
        this.isComingFromHamburgerMenu = isComingFromHamburgerMenu;
        this.isUserLoggedIn = isUserLoggedIn;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getUuId() {
        return uuId;
    }

    public void setUuId(String uuId) {
        this.uuId = uuId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getDbId() {
        return dbId;
    }

    public void setDbId(String dbId) {
        this.dbId = dbId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public boolean isComingFromHamburgerMenu() {
        return isComingFromHamburgerMenu;
    }

    public void setComingFromHamburgerMenu(boolean comingFromHamburgerMenu) {
        isComingFromHamburgerMenu = comingFromHamburgerMenu;
    }

    public boolean isUserLoggedIn() {
        return isUserLoggedIn;
    }

    public void setUserLoggedIn(boolean userLoggedIn) {
        isUserLoggedIn = userLoggedIn;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    @Override
    public String toString() {
        return "APIModel{" +
                "uId='" + uId + '\'' +
                ", uuId='" + uuId + '\'' +
                ", emailId='" + emailId + '\'' +
                ", dbId='" + dbId + '\'' +
                ", authToken='" + authToken + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", isComingFromHamburgerMenu=" + isComingFromHamburgerMenu +
                ", isUserLoggedIn=" + isUserLoggedIn +
                '}';
    }
}
