
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AvailableOfferModel implements Serializable {

    @SerializedName("offer_id")
    @Expose
    private int offerId;

    @SerializedName("offer_name")
    @Expose
    private String offerName;

    @SerializedName("max_offer_amount")
    @Expose
    private Float maxOfferAmount;
    @SerializedName("offer_image")
    @Expose
    private String offerImage;
    @SerializedName("offer_start_date")
    @Expose
    private String offerStartDate;
    @SerializedName("offer_end_date")
    @Expose
    private String offerEndDate;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("status_id")
    @Expose
    private int statusId;
    @SerializedName("status_name")
    @Expose
    private String statusName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("term_condition")
    @Expose
    private String termCondition;
    @SerializedName("user_plan_message")
    @Expose
    private String userPlanMessage;
    @SerializedName("background_image")
    @Expose
    private String backgroundImage;
    @SerializedName("faq_url")
    @Expose
    private String faqUrl;


    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }


    public String getOfferImage() {
        return offerImage;
    }

    public void setOfferImage(String offerImage) {
        this.offerImage = offerImage;
    }

    public String getOfferStartDate() {
        return offerStartDate;
    }

    public void setOfferStartDate(String offerStartDate) {
        this.offerStartDate = offerStartDate;
    }

    public String getOfferEndDate() {
        return offerEndDate;
    }

    public void setOfferEndDate(String offerEndDate) {
        this.offerEndDate = offerEndDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTermCondition() {
        return termCondition;
    }

    public void setTermCondition(String termCondition) {
        this.termCondition = termCondition;
    }

    public Float getMaxOfferAmount() {
        return maxOfferAmount == null ? 0.0f : maxOfferAmount;
    }

    public void setMaxOfferAmount(Float maxOfferAmount) {
        this.maxOfferAmount = maxOfferAmount;
    }

    public String getUserPlanMessage() {
        return userPlanMessage == null ? "-----" : userPlanMessage;
    }

    public void setUserPlanMessage(String userPlanMessage) {
        this.userPlanMessage = userPlanMessage;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getFaqUrl() {
        return faqUrl;
    }

    public void setFaqUrl(String faqUrl) {
        this.faqUrl = faqUrl;
    }
}
