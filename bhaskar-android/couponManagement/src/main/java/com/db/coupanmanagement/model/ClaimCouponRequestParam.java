
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClaimCouponRequestParam {

    @SerializedName("offer_id")
    @Expose
    private int offerId;
    @SerializedName("plan_id")
    @Expose
    private int planId;
    @SerializedName("month_id")
    @Expose
    private int monthId;
    @SerializedName("year")
    @Expose
    private int year;
    @SerializedName("coupon_redeem")
    @Expose
    private List<CouponRedeem> couponRedeem = null;

    public ClaimCouponRequestParam(int offerId, int planId, int monthId, int year, List<CouponRedeem> couponRedeem) {
        this.offerId = offerId;
        this.planId = planId;
        this.monthId = monthId;
        this.year = year;
        this.couponRedeem = couponRedeem;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public int getMonthId() {
        return monthId;
    }

    public void setMonthId(int monthId) {
        this.monthId = monthId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<CouponRedeem> getCouponRedeem() {
        return couponRedeem;
    }

    public void setCouponRedeem(List<CouponRedeem> couponRedeem) {
        this.couponRedeem = couponRedeem;
    }

}
