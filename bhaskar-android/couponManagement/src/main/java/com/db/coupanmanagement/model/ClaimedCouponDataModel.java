
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClaimedCouponDataModel {

    @SerializedName("plan_id")
    @Expose
    private int planId;
    @SerializedName("month_id")
    @Expose
    private int monthId;
    @SerializedName("plan_status")
    @Expose
    private boolean planStatus;
    @SerializedName("offer_id")
    @Expose
    private int offerId;
    @SerializedName("month_name")
    @Expose
    private String monthName;
    @SerializedName("year")
    @Expose
    private int year;
    @SerializedName("offer_amount")
    @Expose
    private String offerAmount;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;
    @SerializedName("offer_name")
    @Expose
    private String offerName;
    @SerializedName("plan_start_date")
    @Expose
    private String planStartDate;
    @SerializedName("main_offer_amount")
    @Expose
    private String mainOfferAmount;

    @SerializedName("redirect_url")
    @Expose
    private String redirectUrl;


    @SerializedName("coupons")
    @Expose
    private List<ClaimedCouponDetailsModel> coupons = null;

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public int getMonthId() {
        return monthId;
    }

    public void setMonthId(int monthId) {
        this.monthId = monthId;
    }

    public boolean getPlanStatus() {
        return planStatus;
    }

    public void setPlanStatus(boolean planStatus) {
        this.planStatus = planStatus;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getOfferAmount() {
        return offerAmount;
    }

    public void setOfferAmount(String offerAmount) {
        this.offerAmount = offerAmount;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public String getMainOfferAmount() {
        return mainOfferAmount;
    }

    public void setMainOfferAmount(String mainOfferAmount) {
        this.mainOfferAmount = mainOfferAmount;
    }

    public List<ClaimedCouponDetailsModel> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<ClaimedCouponDetailsModel> coupons) {
        this.coupons = coupons;
    }

    public boolean isPlanStatus() {
        return planStatus;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
