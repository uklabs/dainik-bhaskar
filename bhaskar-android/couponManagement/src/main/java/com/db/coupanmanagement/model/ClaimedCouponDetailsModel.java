
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClaimedCouponDetailsModel {

    @SerializedName("vendor_id")
    @Expose
    private int vendorId;
    @SerializedName("vendor_name")
    @Expose
    private String vendorName;
    @SerializedName("vendor_image")
    @Expose
    private String vendorImage;
    @SerializedName("deal_amount")
    @Expose
    private float dealAmount;
    @SerializedName("coupon_id")
    @Expose
    private int couponId;
    @SerializedName("coupon_amount")
    @Expose
    private float couponAmount;
    @SerializedName("coupon_count")
    @Expose
    private int couponCount;
    @SerializedName("coupon_code")
    @Expose
    private String couponCode;
    @SerializedName("minimum_purchase_amount")
    @Expose
    private String minimumPurchaseAmount;
    @SerializedName("coupon_description")
    @Expose
    private String couponDescription;
    @SerializedName("term_and_condition")
    @Expose
    private String termAndCondition;
    @SerializedName("coupon_start_date")
    @Expose
    private String couponStartDate;
    @SerializedName("coupon_expiry_date")
    @Expose
    private String couponExpiryDate;
    @SerializedName("status_id")
    @Expose
    private int statusId;
    @SerializedName("status_name")
    @Expose
    private String status_name;
    @SerializedName("qty")
    @Expose
    private int qty;
    @SerializedName("redirect_url")
    @Expose
    private String redirectUrl;
    @SerializedName("is_offline_coupon_viewed")
    @Expose
    private int isOfflineCouponViewed;
    @SerializedName("is_offline")
    @Expose
    private int isOffline;
    @SerializedName("is_offline_message")
    @Expose
    private String isOfflineMessage;
    @SerializedName("is_offline_confirm_message")
    @Expose
    private String isOfflineConfirmMessage;
    @SerializedName("valid_on")
    @Expose
    private String validOn;
    private boolean isClaimable = true;

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorImage() {
        return vendorImage;
    }

    public void setVendorImage(String vendorImage) {
        this.vendorImage = vendorImage;
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public float getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(float couponAmount) {
        this.couponAmount = couponAmount;
    }

    public float getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(float dealAmount) {
        this.dealAmount = dealAmount;
    }

    public int getCouponCount() {
        return couponCount;
    }

    public void setCouponCount(int couponCount) {
        this.couponCount = couponCount;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getMinimumPurchaseAmount() {
        return minimumPurchaseAmount;
    }

    public void setMinimumPurchaseAmount(String minimumPurchaseAmount) {
        this.minimumPurchaseAmount = minimumPurchaseAmount;
    }

    public String getCouponDescription() {
        return couponDescription;
    }

    public void setCouponDescription(String couponDescription) {
        this.couponDescription = couponDescription;
    }

    public String getTermAndCondition() {
        return termAndCondition;
    }

    public void setTermAndCondition(String termAndCondition) {
        this.termAndCondition = termAndCondition;
    }

    public String getCouponStartDate() {
        return couponStartDate;
    }

    public void setCouponStartDate(String couponStartDate) {
        this.couponStartDate = couponStartDate;
    }

    public String getCouponExpiryDate() {
        return couponExpiryDate;
    }

    public void setCouponExpiryDate(String couponExpiryDate) {
        this.couponExpiryDate = couponExpiryDate;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public int getIsOfflineCouponViewed() {
        return isOfflineCouponViewed;
    }

    public void setIsOfflineCouponViewed(int isOfflineCouponViewed) {
        this.isOfflineCouponViewed = isOfflineCouponViewed;
    }

    public int getIsOffline() {
        return isOffline;
    }

    public void setIsOffline(int isOffline) {
        this.isOffline = isOffline;
    }

    public String getIsOfflineMessage() {
        return isOfflineMessage;
    }

    public void setIsOfflineMessage(String isOfflineMessage) {
        this.isOfflineMessage = isOfflineMessage;
    }

    public String getIsOfflineConfirmMessage() {
        return isOfflineConfirmMessage;
    }

    public void setIsOfflineConfirmMessage(String isOfflineConfirmMessage) {
        this.isOfflineConfirmMessage = isOfflineConfirmMessage;
    }

    public boolean isClaimable() {
        return isClaimable;
    }

    public void setClaimable(boolean claimable) {
        isClaimable = claimable;
    }

    public String getValidOn() {
        return validOn;
    }

    public void setValidOn(String validOn) {
        this.validOn = validOn;
    }
}
