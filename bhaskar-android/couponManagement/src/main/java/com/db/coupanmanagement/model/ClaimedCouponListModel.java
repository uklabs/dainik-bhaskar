
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClaimedCouponListModel {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ClaimedCouponDataModel claimedCouponDataModel;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ClaimedCouponDataModel getClaimedCouponDataModel() {
        return claimedCouponDataModel;
    }

    public void setClaimedCouponDataModel(ClaimedCouponDataModel claimedCouponDataModel) {
        this.claimedCouponDataModel = claimedCouponDataModel;
    }
}
