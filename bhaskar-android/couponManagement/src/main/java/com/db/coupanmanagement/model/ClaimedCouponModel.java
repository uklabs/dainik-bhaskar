
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClaimedCouponModel {

    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("year")
    @Expose
    private int year;
    @SerializedName("claimedCouponDetailsModels")
    @Expose
    private List<ClaimedCouponDetailsModel> claimedCouponDetailsModels = null;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<ClaimedCouponDetailsModel> getClaimedCouponDetailsModels() {
        return claimedCouponDetailsModels;
    }

    public void setClaimedCouponDetailsModels(List<ClaimedCouponDetailsModel> claimedCouponDetailsModels) {
        this.claimedCouponDetailsModels = claimedCouponDetailsModels;
    }

}
