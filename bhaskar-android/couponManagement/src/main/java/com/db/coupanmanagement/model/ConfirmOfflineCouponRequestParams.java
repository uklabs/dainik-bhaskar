package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfirmOfflineCouponRequestParams {

    @SerializedName("coupon_id")
    @Expose
    private int couponId;

    public ConfirmOfflineCouponRequestParams(int couponId) {
        this.couponId = couponId;
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }
}
