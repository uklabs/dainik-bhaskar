
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponOTPVerificationRequestParam {

    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("otp")
    @Expose
    private int otp;

    public CouponOTPVerificationRequestParam(String mobileNumber, int otp) {
        this.mobileNumber = mobileNumber;
        this.otp = otp;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }
}
