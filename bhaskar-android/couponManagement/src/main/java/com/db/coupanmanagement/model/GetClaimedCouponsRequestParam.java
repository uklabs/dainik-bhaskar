
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetClaimedCouponsRequestParam {

    @SerializedName("offer_id")
    @Expose
    private int offerId;
    @SerializedName("plan_id")
    @Expose
    private int planId;
    @SerializedName("month_id")
    @Expose
    private int monthId;
    @SerializedName("year")
    @Expose
    private int year;

    public GetClaimedCouponsRequestParam(int offerId, int planId, int monthId, int year) {
        this.offerId = offerId;
        this.planId = planId;
        this.monthId = monthId;
        this.year = year;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public int getMonthId() {
        return monthId;
    }

    public void setMonthId(int monthId) {
        this.monthId = monthId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
