package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetOfflineClaimedCouponDataModel {
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private GetOfflineClaimedCouponDetailModel getOfflineClaimedCouponDetailModel;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GetOfflineClaimedCouponDetailModel getGetOfflineClaimedCouponDetailModel() {
        return getOfflineClaimedCouponDetailModel;
    }

    public void setGetOfflineClaimedCouponDetailModel(GetOfflineClaimedCouponDetailModel getOfflineClaimedCouponDetailModel) {
        this.getOfflineClaimedCouponDetailModel = getOfflineClaimedCouponDetailModel;
    }
}
