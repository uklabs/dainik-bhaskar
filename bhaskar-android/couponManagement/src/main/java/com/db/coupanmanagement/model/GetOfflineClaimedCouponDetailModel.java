package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetOfflineClaimedCouponDetailModel {
    @SerializedName("coupon_id")
    @Expose
    private String couponId;
    @SerializedName("coupon_code")
    @Expose
    private String couponCode;
    @SerializedName("disappear_interval")
    @Expose
    private long disappearInterval;

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public long getDisappearInterval() {
        return disappearInterval;
    }

    public void setDisappearInterval(long disappearInterval) {
        this.disappearInterval = disappearInterval;
    }
}
