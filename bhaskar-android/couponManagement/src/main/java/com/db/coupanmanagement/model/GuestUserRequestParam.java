
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuestUserRequestParam {
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("os")
    @Expose
    private String os;
    @SerializedName("osVersion")
    @Expose
    private String osVersion;

    @SerializedName("device_id")
    @Expose
    private String deviceId;

    public GuestUserRequestParam(String model, String make, String os, String osVersion, String deviceId) {
        this.model = model;
        this.make = make;
        this.os = os;
        this.osVersion = osVersion;
        this.deviceId = deviceId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

}
