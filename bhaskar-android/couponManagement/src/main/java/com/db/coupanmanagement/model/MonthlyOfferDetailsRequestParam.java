
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MonthlyOfferDetailsRequestParam {

    @SerializedName("offer_id")
    @Expose
    private int offerId;
    @SerializedName("plan_id")
    @Expose
    private int planId;
    @SerializedName("month_id")
    @Expose
    private int monthId;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("city")
    @Expose
    private String city;

    public MonthlyOfferDetailsRequestParam(int offerId, int planId, int monthId, String latitude, String longitude, String city) {
        this.offerId = offerId;
        this.planId = planId;
        this.monthId = monthId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public int getMonthId() {
        return monthId;
    }

    public void setMonthId(int monthId) {
        this.monthId = monthId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "MonthlyOfferDetailsRequestParam{" +
                "offerId=" + offerId +
                ", planId=" + planId +
                ", monthId=" + monthId +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
