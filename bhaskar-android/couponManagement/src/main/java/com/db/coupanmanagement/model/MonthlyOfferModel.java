
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MonthlyOfferModel {


    @SerializedName("user_plan_status")
    @Expose
    private boolean userPlanStatus;

    @SerializedName("today_plan_status")
    @Expose
    private boolean todayPlanStatus;
    @SerializedName("plan_month")
    @Expose
    private String planMonth;
    @SerializedName("offer_id")
    @Expose
    private int offerId;
    @SerializedName("offer_name")
    @Expose
    private String offerName;
    @SerializedName("user_claimed_amount")
    @Expose
    private int userClaimedAmount;
    @SerializedName("user_expired_amount")
    @Expose
    private int userExpiredAmount;
    @SerializedName("user_amount_in_waiting")
    @Expose
    private int userAmountInWaiting;
    @SerializedName("offer_plan_id")
    @Expose
    private int offerPlanId;
    @SerializedName("plan_start_date")
    @Expose
    private String planStartDate;
    @SerializedName("main_offer_amount")
    @Expose
    private float mainOfferAmount;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("plan_term_condition")
    @Expose
    private String planTermCondition;
    @SerializedName("status_id")
    @Expose
    private int statusId;
    @SerializedName("status_name")
    @Expose
    private String statusName;
    @SerializedName("current_date_time")
    @Expose
    private String currentDateTime;
    @SerializedName("plans")
    @Expose
    private List<MonthlyOfferPlanModel> monthlyOfferPlanModels = null;

    public boolean isUserPlanStatus() {
        return userPlanStatus;
    }

    public void setUserPlanStatus(boolean userPlanStatus) {
        this.userPlanStatus = userPlanStatus;
    }

    public boolean isTodayPlanStatus() {
        return todayPlanStatus;
    }

    public void setTodayPlanStatus(boolean todayPlanStatus) {
        this.todayPlanStatus = todayPlanStatus;
    }

    public String getPlanMonth() {
        return planMonth;
    }

    public void setPlanMonth(String planMonth) {
        this.planMonth = planMonth;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public int getUserClaimedAmount() {
        return userClaimedAmount;
    }

    public void setUserClaimedAmount(int userClaimedAmount) {
        this.userClaimedAmount = userClaimedAmount;
    }

    public int getUserExpiredAmount() {
        return userExpiredAmount;
    }

    public void setUserExpiredAmount(int userExpiredAmount) {
        this.userExpiredAmount = userExpiredAmount;
    }

    public int getUserAmountInWaiting() {
        return userAmountInWaiting;
    }

    public void setUserAmountInWaiting(int userAmountInWaiting) {
        this.userAmountInWaiting = userAmountInWaiting;
    }

    public int getOfferPlanId() {
        return offerPlanId;
    }

    public void setOfferPlanId(int offerPlanId) {
        this.offerPlanId = offerPlanId;
    }

    public String getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public float getMainOfferAmount() {
        return mainOfferAmount;
    }

    public void setMainOfferAmount(float mainOfferAmount) {
        this.mainOfferAmount = mainOfferAmount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlanTermCondition() {
        return planTermCondition;
    }

    public void setPlanTermCondition(String planTermCondition) {
        this.planTermCondition = planTermCondition;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getCurrentDateTime() {
        return currentDateTime;
    }

    public void setCurrentDateTime(String currentDateTime) {
        this.currentDateTime = currentDateTime;
    }

    public List<MonthlyOfferPlanModel> getMonthlyOfferPlanModels() {
        return monthlyOfferPlanModels;
    }

    public void setMonthlyOfferPlanModels(List<MonthlyOfferPlanModel> monthlyOfferPlanModels) {
        this.monthlyOfferPlanModels = monthlyOfferPlanModels;
    }
}
