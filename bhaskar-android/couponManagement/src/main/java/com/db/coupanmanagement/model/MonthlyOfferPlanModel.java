
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MonthlyOfferPlanModel implements Serializable {

    @SerializedName("month_id")
    @Expose
    private int monthId;
    @SerializedName("month_name")
    @Expose
    private String monthName;
    @SerializedName("year")
    @Expose
    private int year;
    @SerializedName("offer_amount")
    @Expose
    private float offerAmount;
    @SerializedName("plan_start_date")
    @Expose
    private String planStartDate;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;
    @SerializedName("plan_status")
    @Expose
    private boolean planStatus;
    @SerializedName("is_user_plan_taken")
    @Expose
    private boolean isUserPlanTaken;
    @SerializedName("user_claimed_date")
    @Expose
    private String userClaimedDate;
    @SerializedName("monthly_plan_status_for_user")
    @Expose
    private int monthlyPlanStatusForUser;

    public int getMonthId() {
        return monthId;
    }

    public void setMonthId(int monthId) {
        this.monthId = monthId;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getOfferAmount() {
        return offerAmount;
    }

    public void setOfferAmount(float offerAmount) {
        this.offerAmount = offerAmount;
    }

    public String getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean getPlanStatus() {
        return planStatus;
    }

    public void setPlanStatus(boolean planStatus) {
        this.planStatus = planStatus;
    }

    public boolean getIsUserPlanTaken() {
        return isUserPlanTaken;
    }

    public void setIsUserPlanTaken(boolean isUserPlanTaken) {
        this.isUserPlanTaken = isUserPlanTaken;
    }

    public boolean isPlanStatus() {
        return planStatus;
    }

    public boolean isUserPlanTaken() {
        return isUserPlanTaken;
    }

    public void setUserPlanTaken(boolean userPlanTaken) {
        isUserPlanTaken = userPlanTaken;
    }

    public String getUserClaimedDate() {
        return userClaimedDate;
    }

    public void setUserClaimedDate(String userClaimedDate) {
        this.userClaimedDate = userClaimedDate;
    }

    public int getMonthlyPlanStatusForUser() {
        return monthlyPlanStatusForUser;
    }

    public void setMonthlyPlanStatusForUser(int monthlyPlanStatusForUser) {
        this.monthlyPlanStatusForUser = monthlyPlanStatusForUser;
    }
}
