
package com.db.coupanmanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NetworkRequestParam {

    @SerializedName("uid")
    @Expose
    private int uId;
    @SerializedName("uuid")
    @Expose
    private String uuId;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("dbid")
    @Expose
    private String dbId;

    public NetworkRequestParam(int uId, String uuId, String deviceId, String dbId) {
        this.uId = uId;
        this.uuId = uuId;
        this.deviceId = deviceId;
        this.dbId = dbId;
    }

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getUuId() {
        return uuId;
    }

    public void setUuId(String uuId) {
        this.uuId = uuId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDbId() {
        return dbId;
    }

    public void setDbId(String dbId) {
        this.dbId = dbId;
    }
}
