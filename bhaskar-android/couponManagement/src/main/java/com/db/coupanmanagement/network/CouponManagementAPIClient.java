package com.db.coupanmanagement.network;


import android.text.TextUtils;

import com.db.coupanmanagement.BuildConfig;
import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hp on 02-06-2018.
 */

public class CouponManagementAPIClient implements CouponManagementConstants.HeaderKeys, CouponManagementConstants.HeaderValues {

    public static Retrofit getClient(String authToken, final String dbId, final String uuid, final String uid, final String appId, final String appKey) {
        final String finalAuthToken;
        if (TextUtils.isEmpty(authToken)) {
            finalAuthToken = "";
        } else {
            finalAuthToken = CouponManagementConstants.BEARER + authToken;
        }
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            // set your desired log level
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            // set your desired log level
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    Request.Builder builder = request.newBuilder()
                            .addHeader(KEY_ACCEPT, VALUE_ACCEPT)
                            .addHeader(KEY_CHANNEL, VALUE_CHANNEL)
                            .addHeader(KEY_DEVICE_TYPE, VALUE_DEVICE_TYPE)
                            .addHeader(KEY_CONTENT_TYPE, VALUE_CONTENT_TYPE)
                            .addHeader(KEY_AUTHORIZATION, finalAuthToken)
                            .addHeader(KEY_DB_ID, dbId)
                            .addHeader(KEY_DEVICE_ID, dbId)
                            .addHeader(KEY_X_CSRF_TOKEN, "")
                            .addHeader(KEY_UUID, uuid)
                            .addHeader(KEY_APP_ID, appId)
                            .addHeader(KEY_APP_KEY, appKey)
                            .addHeader(KEY_UID, uid)
                            .method(request.method(), request.body());
                    return chain.proceed(builder.build());
                })
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .addNetworkInterceptor(new StethoInterceptor())
                .build();

        return new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(CouponManagementConstants.URL_COUPON_BASE)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient()
                        .create()))
                .build();
    }
}
