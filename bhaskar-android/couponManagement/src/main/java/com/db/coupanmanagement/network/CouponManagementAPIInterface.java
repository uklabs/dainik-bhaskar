package com.db.coupanmanagement.network;

import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.model.AvailableOfferListModel;
import com.db.coupanmanagement.model.ClaimCouponRequestParam;
import com.db.coupanmanagement.model.ClaimedCouponDetailListModel;
import com.db.coupanmanagement.model.ConfirmOfflineCouponRequestParams;
import com.db.coupanmanagement.model.CouponMobileNumberOTPModel;
import com.db.coupanmanagement.model.CouponMobileNumberOTPVerificationModel;
import com.db.coupanmanagement.model.CouponOTPVerificationRequestParam;
import com.db.coupanmanagement.model.GetClaimedCouponsRequestParam;
import com.db.coupanmanagement.model.GetMobileNumberOTPRequestParam;
import com.db.coupanmanagement.model.GetOfflineClaimedCouponDataModel;
import com.db.coupanmanagement.model.GuestUserModel;
import com.db.coupanmanagement.model.GuestUserRequestParam;
import com.db.coupanmanagement.model.MonthlyOfferDetailsRequestParam;
import com.db.coupanmanagement.model.MonthlyOfferListModel;
import com.db.coupanmanagement.model.MonthlyOfferListRequestParam;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by hp on 02-06-2018.
 */

public interface CouponManagementAPIInterface {
    @POST(CouponManagementConstants.NetworkRequestEndPoint.GUEST_USER)
    Call<GuestUserModel> doPostGuestUser(@Body GuestUserRequestParam requestParam);

    @POST(CouponManagementConstants.NetworkRequestEndPoint.OFFERS)
    Call<AvailableOfferListModel> doPostAvailableOffersList();

    @POST(CouponManagementConstants.NetworkRequestEndPoint.PLANS)
    Call<MonthlyOfferListModel> doPostMonthlyOffersList(@Body MonthlyOfferListRequestParam requestParam);

    @POST(CouponManagementConstants.NetworkRequestEndPoint.COUPONS)
    Call<ClaimedCouponDetailListModel> doPostMonthlyOffersDetail(@Body MonthlyOfferDetailsRequestParam requestParam);

    @POST(CouponManagementConstants.NetworkRequestEndPoint.CLAIMED_REWARD_DETAILS)
    Call<ClaimedCouponDetailListModel> doPostClaimedCouponList(@Body GetClaimedCouponsRequestParam requestParam);

    @POST(CouponManagementConstants.NetworkRequestEndPoint.GET_COUPON_CODES)
    Call<ClaimedCouponDetailListModel> doPostClaimCoupon(@Body ClaimCouponRequestParam requestParam);

    @POST(CouponManagementConstants.NetworkRequestEndPoint.GET_OTP)
    Call<CouponMobileNumberOTPModel> doPostCouponMobileNumberOTP(@Body GetMobileNumberOTPRequestParam requestParam);

    @POST(CouponManagementConstants.NetworkRequestEndPoint.OTP_VERIFICATION)
    Call<CouponMobileNumberOTPVerificationModel> doPostCouponMobileNumberOTPVerification(@Body CouponOTPVerificationRequestParam requestParam);

    @POST(CouponManagementConstants.NetworkRequestEndPoint.CONFIRM_OFFLINE_COUPON)
    Call<GetOfflineClaimedCouponDataModel> doPostGetOfflineCouponDetail(@Body ConfirmOfflineCouponRequestParams requestParam);
}
