package com.db.coupanmanagement.smsreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;

import com.db.coupanmanagement.constant.CouponManagementConstants;

public class SmsOtpReceiver extends BroadcastReceiver {

    private final String TAG = "SmsOtpReceiver";
    private static SmsOtpReceiverListener mSmsOtpReceiverListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            if (mSmsOtpReceiverListener != null) {
                Bundle data = intent.getExtras();

                Object[] pdus = new Object[0];
                if (data != null) {
                    pdus = (Object[]) data.get("pdus");
                }

                if (pdus != null) {
                    for (Object pdu : pdus) {
                        SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
                        String sender = smsMessage.getDisplayOriginatingAddress();
                        //You must check here if the sender is your provider and not another one with same text.
                        if (sender.contains(CouponManagementConstants.DAINIK_BHASKAR_OTP_SENDER)) {
                            String messageBody = smsMessage.getMessageBody();
                            //Pass on the text to our listener.
                            messageBody = messageBody.replaceAll("[^0-9]", "");
                            if (!TextUtils.isEmpty(messageBody) && mSmsOtpReceiverListener != null &&
                                    messageBody.length() == CouponManagementConstants.OTP_MESSAGE_LENGTH) {
                                mSmsOtpReceiverListener.onMessageReceived(messageBody);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }

    public static void bindListener(SmsOtpReceiverListener listener) {
        mSmsOtpReceiverListener = listener;
    }

    public static void unbindListener() {
        mSmsOtpReceiverListener = null;
    }

    public interface SmsOtpReceiverListener {
        void onMessageReceived(String messageText);
    }
}
