package com.db.coupanmanagement.utils;

import android.app.Application;
import android.util.Log;

import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.google.android.gms.analytics.Tracker;

import java.lang.reflect.Method;

public class GoogleAnalyticUtils {

    public Tracker getGoogleAnalyticTracker(Application application) {
        Tracker tracker = null;
        try {
            Method method = application.getClass().getMethod(CouponManagementConstants.GA_APPLICATION_METHOD, (Class<?>[]) null);
            Object object = null;
            if (method != null) {
                object = method.invoke(application, (Object[]) null);
                if (object != null) {
                    tracker = (Tracker) object;
                }
            }
        } catch (Exception e) {
            Log.w(CouponManagementConstants.GA_TAG, "Can't get GA tracker: " + e.getMessage());
        }
        return tracker;
    }
}
