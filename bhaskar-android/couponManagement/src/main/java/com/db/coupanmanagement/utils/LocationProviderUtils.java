package com.db.coupanmanagement.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.util.Log;

import com.db.coupanmanagement.constant.CouponManagementConstants;
import com.db.coupanmanagement.dialog.CouponDialogUtils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LocationProviderUtils {
    private final String TAG = LocationProviderUtils.class.getSimpleName();
    private FusedLocationProviderClient mFusedLocationClient;
    private OnLocationFoundListener mOnLocationFoundListener;
    private final int REQUEST_CHECK_SETTINGS = 102;
    private Activity mActivity;

    public interface OnLocationFoundListener {
        void onGetLatitudeLongitude(String latitude, String longitude, String city);

        void onLocationNotFound();
    }

    /**
     * @param mOnLocationFoundListener
     */
    public LocationProviderUtils(OnLocationFoundListener mOnLocationFoundListener) {
        this.mOnLocationFoundListener = mOnLocationFoundListener;

    }

    public void initForLocationPermission(Activity activity) {
        this.mActivity = activity;
        isLocationSettingsImplemented = false;
        if (!checkLocationPermissions(activity))
            startLocationPermissionRequest(activity);
        else
            getLastLocation();
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mActivity);
        mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                try {
                    if (task.isSuccessful() && task.getResult() != null) {
                        Location location = task.getResult();
                        String latitude = String.valueOf(location.getLatitude());
                        String longitude = String.valueOf(location.getLongitude());
                        List<Address> address = getAddressFromLatLong(mActivity, latitude, longitude);
                        String city = "";
                        if (address != null && address.size() > 0) {
                            city = address.get(0).getLocality();
                        }
                        if (mOnLocationFoundListener != null) {
                            mOnLocationFoundListener.onGetLatitudeLongitude(latitude, longitude, city);
                        }
                    } else {
                        if (!isLocationSettingsImplemented) {
                            implementLocationSettings();
                        } else {
                            if (mOnLocationFoundListener != null) {
                                mOnLocationFoundListener.onLocationNotFound();
                            }
                            Log.w(TAG, "getLastLocation:exception: " + task.getException());
                            //CouponDialogUtils.showToastMessage(mActivity.getApplicationContext(), "Location not found. Please try again.");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (mOnLocationFoundListener != null) {
                        mOnLocationFoundListener.onLocationNotFound();
                        //CouponDialogUtils.showToastMessage(mActivity.getApplicationContext(), "Location not found. Please try again.");
                    }
                    Log.w(TAG, "getLastLocation:exception: " + e.getMessage());
                }
            }
        });
    }

    private boolean isLocationSettingsImplemented = false;

    /**
     * Return the current state of the permissions needed.
     */
    public static boolean checkLocationPermissions(Activity activity) {
        int permissionStateCoarse = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
        int permissionStateFine = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);

        return permissionStateCoarse == PackageManager.PERMISSION_GRANTED && permissionStateFine == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest(Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            activity.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, CouponManagementConstants.PERMISSIONS_REQUEST_LOCATION);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull int[] grantResults) {
        if (requestCode == CouponManagementConstants.PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                if (mOnLocationFoundListener != null) {
                    mOnLocationFoundListener.onLocationNotFound();
                }
                Log.w(TAG, "User interaction was cancelled.");
                CouponDialogUtils.showToastMessage(mActivity.getApplicationContext(), "Location permission not granted.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
            } else {
                if (mOnLocationFoundListener != null) {
                    mOnLocationFoundListener.onLocationNotFound();
                }
                CouponDialogUtils.showToastMessage(mActivity.getApplicationContext(), "Location permission not granted.");
            }
        }
    }

    private void implementLocationSettings() {
        isLocationSettingsImplemented = true;
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        Task<LocationSettingsResponse> responseTask = LocationServices.getSettingsClient(mActivity).checkLocationSettings(builder.build());

        responseTask.addOnSuccessListener(locationSettingsResponse -> getLastLocation());

        responseTask.addOnFailureListener(e -> {
            int statusCode = ((ApiException) e).getStatusCode();
            switch (statusCode) {
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    Log.w(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                            "location settings ");
                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the
                        // result in onActivityResult().
                        ResolvableApiException rae = (ResolvableApiException) e;
                        rae.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sie) {
                        Log.w(TAG, "PendingIntent unable to execute request.");
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    String errorMessage = "Location settings are inadequate, and cannot be " +
                            "fixed here. Fix in Settings.";
                    Log.w(TAG, errorMessage);
                    CouponDialogUtils.showToastMessage(mActivity.getApplicationContext(), errorMessage);
            }
        });
    }

    /**
     * @param requestCode
     * @param resultCode
     */
    public void onActivityResult(int requestCode, int resultCode) {
        /*final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);*/
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        new android.os.Handler(mActivity.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getLastLocation();
                            }
                        }, 3000);
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        //CouponDialogUtils.showToastMessage(mActivity.getApplicationContext(), "Location not Found. Please try again.");
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    /**
     * @param ctx
     * @param latitude
     * @param longitude
     * @return
     */
    private static List<Address> getAddressFromLatLong(Context ctx, String latitude, String longitude) {
        if (ctx == null) return null;
        Geocoder geocoder;
        List<Address> addresses = new ArrayList<>();
        geocoder = new Geocoder(ctx, Locale.getDefault());
        try {
            Double location_lat = Double.parseDouble(latitude);
            Double location_long = Double.parseDouble(longitude);
            addresses = geocoder.getFromLocation(location_lat, location_long, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return addresses;
        }
    }

    public void dispose() {
        mFusedLocationClient = null;
        mOnLocationFoundListener = null;
    }
}
