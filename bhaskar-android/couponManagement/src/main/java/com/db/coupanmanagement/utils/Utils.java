package com.db.coupanmanagement.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.os.Handler;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;

import com.db.coupanmanagement.R;
import com.db.coupanmanagement.constant.CouponManagementConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Utils {

    /**
     * This method will be used to parse datetime in dd MMMM yyyy(10 फरवरी 2018)
     *
     * @param time
     * @return
     */
    public static String parseDateToddMMMyyyy(String time) {
        String inputPattern = CouponManagementConstants.DateTimeFormats.YYYY_MM_DD_HH_MM_SS;
        String outputPattern = CouponManagementConstants.DateTimeFormats.DD_MMMM_YYYY;
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, new Locale(CouponManagementConstants.DateTimeFormats.GU_LOCALE, CouponManagementConstants.DateTimeFormats.IN_COUNTRY));
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, new Locale(CouponManagementConstants.DateTimeFormats.GU_LOCALE, CouponManagementConstants.DateTimeFormats.IN_COUNTRY));

        Date date;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * This method will be used to parse date in dd MMMM yyyy(10 फरवरी 2018)
     *
     * @param time
     * @return
     */
    public static String parseClaimedDateToddMMMyyyy(String time) {
        String inputPattern = CouponManagementConstants.DateTimeFormats.YYYY_MM_DD;
        String outputPattern = CouponManagementConstants.DateTimeFormats.DD_MMMM_YYYY;
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, new Locale(CouponManagementConstants.DateTimeFormats.GU_LOCALE, CouponManagementConstants.DateTimeFormats.IN_COUNTRY));
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, new Locale(CouponManagementConstants.DateTimeFormats.GU_LOCALE, CouponManagementConstants.DateTimeFormats.IN_COUNTRY));

        Date date;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * This method will be used to get millisecond from datetime
     *
     * @param dateTime
     * @return
     */
    public static long getMillisecondFromDateTime(String dateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat(CouponManagementConstants.DateTimeFormats.YYYY_MM_DD_HH_MM_SS, new Locale(CouponManagementConstants.DateTimeFormats.HI_LOCALE, CouponManagementConstants.DateTimeFormats.IN_COUNTRY));
        try {
            Date date = sdf.parse(dateTime);
            long timeInMilliseconds = date.getTime();
            System.out.println("Date in millisecond: " + timeInMilliseconds);
            return timeInMilliseconds;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * This method will be used to get day of month
     *
     * @return
     */
    public static int getNoOfDaysInAMonth() {
        return new GregorianCalendar().getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static void errorLogger(String tag, String errrorMsg) {
        Log.e(tag + "", errrorMsg + "");
    }

    /**
     * This method will be used to format float value upto 2 decimal value
     *
     * @param context
     * @param value
     * @return
     */
    public static String formatFloatValue(Context context, float value) {
//        return context.getResources().getString(R.string.rupee_sign) + " " + String.format(Locale.getDefault(), "%.02f", value) + "/-";
        return context.getResources().getString(R.string.rupee_sign) + " " + Math.round(value);

    }

    /**
     * This method will be used to format float value for GA
     *
     * @param value
     * @return
     */
    public static int formatFloatValueForGA(float value) {
        return Math.round(value);

    }

    /**
     * This method will be used to check internet connection before network API call
     *
     * @param context
     * @return
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm != null && cm.getActiveNetworkInfo() != null;
    }

    /**
     * This method will be used to decode Base64 response
     *
     * @param response
     * @return
     */
    public static String decodeResponse(String response) {
        return new String(Base64.decode(response, Base64.DEFAULT));
    }

    /**
     * This method will be used to get coupon amount with different-2 color rupee sign
     *
     * @param color
     * @param offerAmount
     * @return
     */
    public static SpannableString getCouponAmount(Context context, int color, float offerAmount) {
        String rupeeSign = context.getString(R.string.rupee_sign) + " " + String.valueOf(Math.round(offerAmount));
        SpannableString spannableString = new SpannableString(rupeeSign);
        spannableString.setSpan(new ForegroundColorSpan(color), 0, 1, 0);
        return spannableString;
    }

    /**
     * This method will be used to create spannable string for claimed coupon date
     *
     * @param color
     * @param text
     * @param separator
     * @return
     */
    public static SpannableString getHighLightedText(int color, String text, String separator) {
        SpannableString spannableString = new SpannableString(text);
        int endIndex = text.indexOf(separator);
        String subString = text.substring(0, endIndex);
        spannableString.setSpan(new ForegroundColorSpan(color), 0, subString.length(), 0);
        return spannableString;
    }

    /**
     * This method will be used to scroll recycler view on top after loading content again
     *
     * @param position
     * @param recyclerView
     * @param nestedScrollView
     */
    public static void scrollToItemPosition(int position, RecyclerView recyclerView, NestedScrollView nestedScrollView) {
        if (recyclerView != null) {
            new Handler().post(() -> {
                recyclerView.scrollToPosition(position);
                if (position == 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    if (layoutManager != null)
                        layoutManager.scrollToPositionWithOffset(0, 0);
                    nestedScrollView.fullScroll(View.FOCUS_UP);
                }
            });
        }
    }

    /**
     * This method will be used to get dimension of device
     *
     * @return
     */
    public static int[] getDeviceDimensions(Activity activity) {
        int[] deviceDimensions;
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        deviceDimensions = new int[]{point.x, point.y};
        return deviceDimensions;
    }
}
