package com.bhaskar.epaper.utils;

/**
 * Created by db on 10/3/2016.
 */
public class PreferncesConstant {
    public static final String BHASKAR_APP_ID = "521";
    public static String setDateAsDefult = "setdafaultdate";
    public static String setAsDefaultData = "setasdefault";
    public static String setDateAsMax = "setDateAsMax";
    public static String defaultStateForEPaper = "MP";


    public interface AppIds {
        String DAINIK_BHASKAR = "521";
        String DIVYA_BHASKAR = "960";
        String MONEY_BHASKAR = "1463";
        String DIVYA_MARATHI = "5483";
    }

}
