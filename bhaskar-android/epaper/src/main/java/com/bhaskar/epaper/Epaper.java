package com.bhaskar.epaper;

import com.bhaskar.epaper.services.BaseServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by db on 9/15/2016.
 */
public class Epaper {

    public static String BASEURL = "https://appfeedlight.bhaskar.com/epaper/";
    public static String IMGBASEURL = "https://i10.dainikbhaskar.com/epaper/splashimg.png";
    public static String NO_IMG_FOUND = "https://i10.dainikbhaskar.com/epaper/nopicture.png";
    public static final String IS_IMAGE_SAVE = BASEURL + "isImageSave/";

    public static String USER_AGENT;
    private static Epaper mInstance;
    BaseServices services;

    public Epaper() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
//                .connectTimeout(25, TimeUnit.SECONDS)
//                .readTimeout(25, TimeUnit.SECONDS)
//                .writeTimeout(25, TimeUnit.SECONDS)
//                .addInterceptor(interceptor)
//                .build();
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(BASEURL).
//                        addConverterFactory(GsonConverterFactory.create(gson)).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASEURL)
//                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        services = retrofit.create(BaseServices.class);
    }

    public static synchronized Epaper getInstance() {
        if (mInstance == null) {
            mInstance = new Epaper();
        }
        return mInstance;
    }


    public BaseServices getServices() {
        return services;
    }
}
