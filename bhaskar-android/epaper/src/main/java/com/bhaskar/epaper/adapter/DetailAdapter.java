package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.model.MasterPageModel;
import com.bhaskar.epaper.ui.DetailSwipeActivity;
import com.bhaskar.epaper.utils.ImageUtil;

import java.util.List;

/**
 * Created by db on 5/2/2016.
 */
public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.ViewHolder> {

    public List<MasterPageModel> listdata;
    Activity context;

    public DetailAdapter(Activity context, List<MasterPageModel> listdata) {
        this.listdata = listdata;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        Drawable img = context.getResources().getDrawable(listdata.get(position).img);
//        holder.itemCategorytxt.setText(listdata.get(position).title);
//        Log.e("title", "=" + listdata.get(position).title);
//        holder.itemImageView.setImageDrawable(img);
        holder.itemCategorytxt.setText(listdata.get(position).pageno + " " + listdata.get(position).page);
        ImageUtil.setImage(context,listdata.get(position).imagethumb,R.drawable.watermark_epaper,holder.itemImageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  MyCustomDialog fragment1 = new MyCustomDialog(listdata, position);
                EditionDetailActivity act = (EditionDetailActivity) context;
                FragmentManager fm = act.getSupportFragmentManager();
                fragment1.show(fm, "");*/
                context.startActivity(new Intent(context, DetailSwipeActivity.class).putExtra("position", position));

            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemCategorytxt;
        ImageView itemImageView;
        RelativeLayout mainCardView;
        RelativeLayout ll_category_home;

        public ViewHolder(View itemView) {
            super(itemView);
            mainCardView = (RelativeLayout) itemView.findViewById(R.id.mainCardView);
            ll_category_home = (RelativeLayout) itemView.findViewById(R.id.ll_category_home);

            itemCategorytxt = (TextView) itemView.findViewById(R.id.item_category_txt);
            itemImageView = (ImageView) itemView.findViewById(R.id.item_category_image);
        }
    }


   /* public void setCustomDailog(int position) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.dailoglayout);

        ImageView closebtn = (ImageView) dialog.findViewById(R.id.closeimgeview);
        closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ViewPager viewpager = (ViewPager) dialog.findViewById(R.id.dailog_viewpager);
        EditionDetailActivity act = (EditionDetailActivity) context;
        viewpager.setAdapter(new PagerAdapter(act.getSupportFragmentManager(), listdata));
        viewpager.setCurrentItem(position);
        dialog.show();
    }
*/

}


/*

if (listdata.get(position).getColor_dec().contains("1")) {
        Log.i("declaration", "=>" + listdata.get(position).getColor_dec());
        holder.color_yes.setChecked(true);
        } else {
        holder.color_no.setChecked(true);
        }

final ImageDataList imgDataModel = listdata.get(position);

        if (imgDataModel.color_dec.contains("1")) {
        Log.i("declaration", "=>" + imgDataModel.color_dec);
        holder.color_yes.setChecked(true);
        } else {
        holder.color_no.setChecked(true);
        }

        if (imgDataModel.reg_dec.equals("1")) {
        Log.i("declaration", "=>" + imgDataModel.reg_dec);
        holder.reg_yes.setChecked(true);
        } else {
        holder.reg_no.setChecked(true);
        }

        if (imgDataModel.center_dec.equals("1")) {
        Log.i("declaration", "=>" + imgDataModel.center_dec);
        holder.center_yes.setChecked(true);
        } else {
        holder.center_no.setChecked(true);
        }*/
