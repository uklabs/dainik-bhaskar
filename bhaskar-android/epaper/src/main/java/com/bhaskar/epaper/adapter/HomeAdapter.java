package com.bhaskar.epaper.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.homeutils.HomeContent;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.List;

/**
 * Created by db on 5/2/2016.
 */
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    public List<String> listdata;
    Activity context;
    Epaper epaper;
    EpaperPrefernces epaperPrefernces;

    public HomeAdapter(Activity context, List<String> listdata, Epaper epaper, EpaperPrefernces epaperPrefernces) {
        this.listdata = listdata;
        this.context = context;
        this.epaper = epaper;
        this.epaperPrefernces = epaperPrefernces;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.title.setText(listdata.get(position));
        List<MasterModel> model = (List<MasterModel>) new HomeContent(context).getElementByIndex(position);
        holder.CategoryList.setAdapter(new HomeItemAdapter(context, listdata.get(position).toString(), model, epaperPrefernces, epaper));

        YoYo.with(Techniques.Shake)
                .duration(700)
                .playOn(holder.sideIndication);

    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        RecyclerView CategoryList;
        ImageView sideIndication;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.item_main_title);
            CategoryList = (RecyclerView) itemView.findViewById(R.id.home_list_item);
            CategoryList.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
            sideIndication = (ImageView) itemView.findViewById(R.id.sideindication);

        }
    }
}



/*

if (listdata.get(position).getColor_dec().contains("1")) {
        Log.i("declaration", "=>" + listdata.get(position).getColor_dec());
        holder.color_yes.setChecked(true);
        } else {
        holder.color_no.setChecked(true);
        }

final ImageDataList imgDataModel = listdata.get(position);

        if (imgDataModel.color_dec.contains("1")) {
        Log.i("declaration", "=>" + imgDataModel.color_dec);
        holder.color_yes.setChecked(true);
        } else {
        holder.color_no.setChecked(true);
        }

        if (imgDataModel.reg_dec.equals("1")) {
        Log.i("declaration", "=>" + imgDataModel.reg_dec);
        holder.reg_yes.setChecked(true);
        } else {
        holder.reg_no.setChecked(true);
        }

        if (imgDataModel.center_dec.equals("1")) {
        Log.i("declaration", "=>" + imgDataModel.center_dec);
        holder.center_yes.setChecked(true);
        } else {
        holder.center_no.setChecked(true);
        }*/
