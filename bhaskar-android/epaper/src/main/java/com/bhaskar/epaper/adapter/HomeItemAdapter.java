package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.ui.SubMasterActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtil;

import java.util.List;

/**
 * Created by db on 5/2/2016.
 */
public class HomeItemAdapter extends RecyclerView.Adapter<HomeItemAdapter.ViewHolder> {

    public List<MasterModel> listdata;
    public String statename;
    Context context;

    Epaper mApp;
    EpaperPrefernces mPref;

    public HomeItemAdapter(Activity context, List<MasterModel> listdata) {
        this.listdata = listdata;
        this.context = context;
        mApp = Epaper.getInstance();
        mPref = EpaperPrefernces.getInstance(context);

    }

    public HomeItemAdapter(Context context, String statename, List<MasterModel> listdata, EpaperPrefernces prefernces, Epaper epaper) {
        this.listdata = listdata;
        this.statename = statename;
        this.context = context;
        mApp = epaper;
        mPref = prefernces;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        String imgThumb = Epaper.NO_IMG_FOUND;
        if (listdata.get(position) != null) {
            holder.itemCategorytxt.setText(listdata.get(position).description);
            imgThumb = listdata.get(position).imagethumb;
        } else {
            holder.itemCategorytxt.setText("unavailable");
        }

        try {
            ImageUtil.setImage(context, imgThumb, R.drawable.watermark_epaper, holder.itemImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(
                        new Intent(context, SubMasterActivity.class)
                                .putExtra("statename", statename)
                                .putExtra("editioncode", listdata.get(position).editioncode)
                                .putExtra("cityname", listdata.get(position).description)
                );
            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemCategorytxt;
        TextView itemCategoryBookmark;
        ImageView itemImageView;
        RelativeLayout mainCardView;
        RelativeLayout ll_category_home;
        CardView cardview;

        public ViewHolder(View itemView) {
            super(itemView);
            mainCardView = itemView.findViewById(R.id.mainCardView);
            ll_category_home = itemView.findViewById(R.id.ll_category_home);
            itemCategorytxt = itemView.findViewById(R.id.item_category_txt);
            itemCategoryBookmark = itemView.findViewById(R.id.item_category_bookmark);
            cardview = itemView.findViewById(R.id.cardview);
            itemImageView = itemView.findViewById(R.id.item_category_image);
            if (!TextUtils.isEmpty(statename)) {
                itemCategoryBookmark.setVisibility(View.GONE);
            }
        }
    }
}

