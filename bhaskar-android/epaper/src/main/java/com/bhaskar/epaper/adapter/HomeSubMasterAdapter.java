package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.model.EditionModel;
import com.bhaskar.epaper.model.SubMasterModel;
import com.bhaskar.epaper.ui.EditionDetailActivity;
import com.bhaskar.epaper.ui.SubMasterActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtil;
import com.bhaskar.epaper.utils.SerializeData;
import com.bhaskar.epaper.utils.Utility;

import java.util.ArrayList;

/**
 * Created by db on 5/2/2016.
 */
public class HomeSubMasterAdapter extends RecyclerView.Adapter<HomeSubMasterAdapter.ViewHolder> {

    public ArrayList<SubMasterModel> listdata;
    Activity context;
    String statename,cityName;
    Epaper mApp;
    EpaperPrefernces mPref;

    public HomeSubMasterAdapter(Activity context, ArrayList<SubMasterModel> listdata) {
        this.listdata = listdata;
        this.context = context;
    }

    public HomeSubMasterAdapter(Activity context, ArrayList<SubMasterModel> filterEdition, String stateName, String cityName) {
        this.listdata = filterEdition;
        this.context = context;
        this.statename = stateName;
        this.cityName=cityName;
        mApp = Epaper.getInstance();
        mPref = EpaperPrefernces.getInstance(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (listdata.get(position) != null) {
            holder.itemCategorytxt.setText(listdata.get(position).description);
            ImageUtil.setImage(context, listdata.get(position).imagethumb, R.drawable.watermark_epaper, holder.itemImageView);
        } else {
            holder.itemCategorytxt.setText("unavailable");
            ImageUtil.setImage(context, Epaper.NO_IMG_FOUND, R.drawable.watermark_epaper, holder.itemImageView);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(statename)) {
                    context.startActivity(new Intent(context, EditionDetailActivity.class)
                            .putExtra("statename", statename)
                            .putExtra("cityname", cityName)
                            .putParcelableArrayListExtra("pages", listdata)
                            .putExtra("editioncode", listdata.get(position).editioncode)
                            .putExtra("editionname", listdata.get(position).description));
                }

            }
        });

        holder.itemCategoryBookmark.setVisibility(View.GONE);
        holder.itemCategoryBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.e("clicked", "clicked");
                setMyEdition(listdata.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public void setMyEdition(SubMasterModel sm) {

        SubMasterActivity activity = (SubMasterActivity) context;
        final ProgressDialog pd = new ProgressDialog(activity);
        pd.setMessage("Please Wait ..");
        pd.show();
        EditionModel editionModel = new EditionModel();
        editionModel.edtname = sm.description;
        editionModel.org_path = sm.imagepath;
        editionModel.thumb_path = sm.imagepath;
        editionModel.large_path = sm.imagelarge;
        editionModel.edtcode = sm.editioncode;
        editionModel.center = statename;
        editionModel.state = statename;

        if (!SerializeData.getInstance(activity).isMyEditionAvailable(sm.editioncode)) {
            SerializeData.getInstance(activity).saveMyEdition(editionModel);
            Utility.setToast(activity, "Edition Successfully Saved..");
        } else {
            Utility.setToast(activity, "Edition Already Exist..");
        }
        pd.dismiss();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemCategorytxt;
        ImageView itemImageView;
        RelativeLayout mainCardView;
        RelativeLayout ll_category_home;
        TextView itemCategoryBookmark;

        public ViewHolder(View itemView) {
            super(itemView);
            mainCardView = (RelativeLayout) itemView.findViewById(R.id.mainCardView);
            ll_category_home = (RelativeLayout) itemView.findViewById(R.id.ll_category_home);

            itemCategorytxt = (TextView) itemView.findViewById(R.id.item_category_txt);
            itemImageView = (ImageView) itemView.findViewById(R.id.item_category_image);
            itemCategoryBookmark = (TextView) itemView.findViewById(R.id.item_category_bookmark);
        }
    }

}
