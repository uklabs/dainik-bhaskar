package com.bhaskar.epaper.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.bhaskar.epaper.fragments.MagazinePagerFragment;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.utils.PreferncesConstant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MagazinePagerAdapter extends FragmentPagerAdapter {

    private List<MagazineMasterModel> GujratiModelList = new ArrayList<>();
    private List<MagazineMasterModel> MarathiModelList = new ArrayList<>();
    private List<MagazineMasterModel> HindiModelList = new ArrayList<>();
    private List<String> title = new ArrayList<>();
    private Map<String, List<MagazineMasterModel>> map = new HashMap<>();

    public MagazinePagerAdapter(FragmentManager fm, List<MagazineMasterModel> pages) {
        super(fm);
        setMasterDetail(pages);
    }

    private void setMasterDetail(List<MagazineMasterModel> pages) {
        for (MagazineMasterModel mmm : pages) {
            if (mmm != null) {
                switch (mmm.getMagazine()) {
                    case "1":
                        HindiModelList.add(mmm);
                        break;
                    case "2":
                        GujratiModelList.add(mmm);
                        break;
                    default:
                        MarathiModelList.add(mmm);
                        break;
                }
            }
        }
        switch (PreferncesConstant.BHASKAR_APP_ID) {

            case PreferncesConstant.AppIds.DIVYA_MARATHI:
                map.put("Marathi", MarathiModelList);
                title.add("Marathi");

                map.put("Hindi", HindiModelList);
                title.add("Hindi");

                map.put("Gujarati", GujratiModelList);
                title.add("Gujarati");
                break;

            case PreferncesConstant.AppIds.DIVYA_BHASKAR:
                map.put("Gujarati", GujratiModelList);
                title.add("Gujarati");

                map.put("Hindi", HindiModelList);
                title.add("Hindi");

                map.put("Marathi", MarathiModelList);
                title.add("Marathi");
                break;

            case PreferncesConstant.AppIds.MONEY_BHASKAR:
            default:
                map.put("Hindi", HindiModelList);
                title.add("Hindi");

                map.put("Gujarati", GujratiModelList);
                title.add("Gujarati");

                map.put("Marathi", MarathiModelList);
                title.add("Marathi");
                break;
        }
    }

    @Override
    public Fragment getItem(int position) {
        return new MagazinePagerFragment(map.get(title.get(position)), title.get(position));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title.get(position);
    }

    @Override
    public int getCount() {
        return map.size();
    }

}