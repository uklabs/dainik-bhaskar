package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.ui.ConnectionDetector;
import com.bhaskar.epaper.ui.MagazineDetailActivity;
import com.bhaskar.epaper.ui.MagazineSubActivity;
import com.bhaskar.epaper.utils.ImageUtil;

import java.util.List;

/**
 * Created by db on 5/2/2016.
 */
public class MagazineRecyclerAdapter extends RecyclerView.Adapter<MagazineRecyclerAdapter.ViewHolder> {

    public List<MagazineMasterModel> listdata;
    Activity context;
    Boolean isClickable;
    String magazineLang;
    public MagazineRecyclerAdapter(Activity context, List<MagazineMasterModel> listdata,String magazineLang, Boolean isClickable) {
        this.listdata = listdata;
        this.context = context;
        this.isClickable = isClickable;
        this.magazineLang=magazineLang;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_home, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (listdata.get(position) != null) {
            holder.itemCategorytxt.setText(listdata.get(position).getDescription());
            ImageUtil.setImage(context, listdata.get(position).getImagethumb(), R.drawable.watermark_epaper, holder.itemImageView);

            holder.itemCategoryBookmark.setText(listdata.get(position).getPagedate());
        } else {
            holder.itemCategorytxt.setText("unavailable");
            ImageUtil.setImage(context, Epaper.NO_IMG_FOUND, R.drawable.watermark_epaper, holder.itemImageView);
            holder.itemCategoryBookmark.setText("");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isConnectingToInternet(context)) {
                    if (isClickable) {
                        context.startActivity(new Intent(context, MagazineSubActivity.class).putExtra("editioncode", listdata.get(position).getEditioncode()).putExtra("magazinelang",magazineLang).putExtra("magazinename",listdata.get(position).getDescription()));
                    } else {
                        context.startActivity(new Intent(context, MagazineDetailActivity.class).putExtra("editioncode", listdata.get(position).getEditioncode()).putExtra("editiondate", listdata.get(position).getPagedate()).putExtra("magazine", listdata.get(position).getMagazine()).putExtra("magazinelang",magazineLang).putExtra("magazinename",listdata.get(position).getDescription()));
                    }
                } else {
                    Toast.makeText(context, "Please Check Network Connection", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemCategorytxt;
        ImageView itemImageView;
        RelativeLayout mainCardView;
        RelativeLayout ll_category_home;
        TextView itemCategoryBookmark;

        public ViewHolder(View itemView) {
            super(itemView);
            mainCardView = itemView.findViewById(R.id.mainCardView);
            ll_category_home = itemView.findViewById(R.id.ll_category_home);
            itemCategorytxt = itemView.findViewById(R.id.item_category_txt);
            itemImageView = itemView.findViewById(R.id.item_category_image);
            itemCategoryBookmark = itemView.findViewById(R.id.item_category_bookmark);
            itemCategoryBookmark.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_calendar_new), null, null, null);
        }

    }
}