package com.bhaskar.epaper.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.bhaskar.epaper.fragments.MasterPageStateFragment;

import java.util.List;

public class MasterPageStateAdapter extends FragmentStatePagerAdapter {

    List<String> statename;

    public MasterPageStateAdapter(FragmentManager fm, List<String> Statename) {
        super(fm);
        this.statename = Statename;
    }

    @Override
    public Fragment getItem(int position) {
        return new MasterPageStateFragment(statename.get(position));

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return statename.get(position);
    }

    @Override
    public int getCount() {
        return statename.size();
    }
}