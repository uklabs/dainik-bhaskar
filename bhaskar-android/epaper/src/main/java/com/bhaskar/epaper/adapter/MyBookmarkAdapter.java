package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.ui.ImageZoomActivity;
import com.bhaskar.epaper.ui.MyBookmarkActivity;
import com.bhaskar.epaper.utils.ImageUtil;
import com.bhaskar.epaper.utils.SerializeData;
import com.bhaskar.epaper.utils.Utility;

import java.util.List;

/**
 * Created by db on 5/2/2016.
 */
public class MyBookmarkAdapter extends RecyclerView.Adapter<MyBookmarkAdapter.ViewHolder> {

    public List<MasterModel> listdata;
    Activity context;

    public MyBookmarkAdapter(Activity context, List<MasterModel> listdata) {
        this.listdata = listdata;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mybookmark_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        String url = listdata.get(position).imagethumb.replace(" ", "%20");
        try {
            ImageUtil.setImage(context, url, R.drawable.watermark_epaper, holder.imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.item_bookmark_name_txt.setText(listdata.get(position).description);
        holder.item_bookmark_edition_txt.setText("Page No:" + listdata.get(position).editioncode);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageZoomActivity.clickModel = listdata.get(position);
                context.startActivity(new Intent(context, ImageZoomActivity.class)
                        .putExtra("largeUrl", listdata.get(position).imagepath)
                        .putExtra("editioncode", listdata.get(position).editioncode)
                        .putExtra("editionname", listdata.get(position).description)
                );
            }
        });
        holder.delete_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeBookMark(listdata.get(position), position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public void removeBookMark(MasterModel masterModel, final int position) {
        final MyBookmarkActivity activity = (MyBookmarkActivity) context;
        final ProgressDialog pd = new ProgressDialog(activity);
        pd.setMessage("Please Wait ..");
        pd.show();
        if (SerializeData.getInstance(activity).isBookmarkAvailable(masterModel.imagepath)) {
            SerializeData.getInstance(activity).removeBookmark(masterModel.imagepath);
            Utility.setToast(activity, "Bookmark Removed");
            removeItemPostion(position);
        } else {
            Utility.setToast(activity, "Bookmark not available");
        }
        pd.dismiss();
    }

    public void removeItemPostion(int position) {
        if (listdata.size() > 0) {
            listdata.remove(position);
        }
        MyBookmarkAdapter.this.notifyItemRemoved(position);
        MyBookmarkAdapter.this.notifyItemRangeChanged(position, listdata.size());
        MyBookmarkAdapter.this.notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView item_bookmark_name_txt, item_bookmark_edition_txt, delete_txt;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.item_category_image);
            item_bookmark_name_txt = itemView.findViewById(R.id.item_bookmark_name_txt);
            item_bookmark_edition_txt = itemView.findViewById(R.id.item_bookmark_edition_txt);
            delete_txt = itemView.findViewById(R.id.delete_txt);
        }
    }
}



