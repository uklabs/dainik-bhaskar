package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.model.EditionModel;
import com.bhaskar.epaper.ui.EditionDetailActivity;
import com.bhaskar.epaper.ui.MyEditionActivity;
import com.bhaskar.epaper.utils.ImageUtil;
import com.bhaskar.epaper.utils.SerializeData;
import com.bhaskar.epaper.utils.Utility;

import java.util.List;

/**
 * Created by db on 5/2/2016.
 */
public class MyEditionAdapter extends RecyclerView.Adapter<MyEditionAdapter.ViewHolder> {

    public List<EditionModel> listdata;
    Activity context;


    public MyEditionAdapter(Activity context, List<EditionModel> listdata) {
        this.listdata = listdata;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mybookmark_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        String url = listdata.get(position).getThumb_path().replace(" ", "%20");
        try {
            if (url.isEmpty()) {
                url = Epaper.NO_IMG_FOUND;
            }
            ImageUtil.setImage(context, url, R.drawable.watermark_epaper, holder.imageView);

        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.item_bookmark_name_txt.setText(listdata.get(position).getEdtname());
        holder.item_bookmark_edition_txt.setText(listdata.get(position).getState());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, EditionDetailActivity.class).putExtra("statename", listdata.get(position).getState())
                        .putExtra("editioncode", listdata.get(position).edtcode).putExtra("currentdate", "currentdate"));
            }
        });

        holder.delete_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeMyEdition(listdata.get(position), position);
//                Utility.setToast(context, "");
            }
        });

        Utility.logE("image", "=>" + listdata.get(position).getThumb_path());
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public void removeMyEdition(EditionModel editionModel, final int position) {
        final MyEditionActivity activity = (MyEditionActivity) context;

        final ProgressDialog pd = new ProgressDialog(activity);
        pd.setMessage("Please Wait ..");
        pd.show();

        if (SerializeData.getInstance(activity).isMyEditionAvailable(editionModel.edtcode)) {
            SerializeData.getInstance(activity).removeBookmark(editionModel.edtcode);
            Utility.setToast(activity, "Edition Successfully Removed..");
            removeItemPostion(position);
        } else {
            Utility.setToast(activity, "Edition Not Exist..");
        }
        pd.dismiss();
    }

    public void removeItemPostion(int position) {
        listdata.remove(position);
        MyEditionAdapter.this.notifyItemRemoved(position);
        MyEditionAdapter.this.notifyItemRangeChanged(position, listdata.size());
        MyEditionAdapter.this.notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView item_bookmark_name_txt, item_bookmark_edition_txt, delete_txt;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.item_category_image);
            item_bookmark_name_txt = (TextView) itemView.findViewById(R.id.item_bookmark_name_txt);
            item_bookmark_edition_txt = (TextView) itemView.findViewById(R.id.item_bookmark_edition_txt);
            delete_txt = (TextView) itemView.findViewById(R.id.delete_txt);

        }
    }
}



