package com.bhaskar.epaper.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.bhaskar.epaper.fragments.ViewPagerFragment;
import com.bhaskar.epaper.model.MasterPageModel;

import java.util.List;

public class PagerAdapter extends FragmentPagerAdapter {

    private final List<MasterPageModel> pages;

    public PagerAdapter(FragmentManager fm, List<MasterPageModel> pages) {
        super(fm);
        this.pages = pages;
    }


    @Override
    public Fragment getItem(int position) {
        return new ViewPagerFragment(pages.get(position));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        int pageitem = position;
        pageitem = pageitem + 1;
        return "Page " + pageitem;
    }

    @Override
    public int getCount() {
        return pages.size();
    }
}