package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.bhaskar.epaper.model.SearchModel;
import com.bhaskar.epaper.model.SubMasterModel;
import com.bhaskar.epaper.ui.EditionDetailActivity;
import com.bhaskar.epaper.ui.EpaperSplashActivity;
import com.bhaskar.epaper.utils.DateUtility;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends ArrayAdapter<SearchModel> {

    Activity context;
    int resource, textViewResourceId;
    List<SearchModel> items, tempItems, suggestions;
    ArrayList<SubMasterModel> subMasterModels;

    public SearchAdapter(Activity context, List<SearchModel> items) {
        super(context, 0, items);
        this.context = context;
        this.items = items;
        tempItems = new ArrayList<>(items); // this makes the difference.
        suggestions = new ArrayList<>();
        subMasterModels = new ArrayList<>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        final SearchModel searchdata = items.get(position);
        if (searchdata != null) {
            TextView lblName = (TextView) view.findViewById(android.R.id.text1);
            lblName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subMasterModels.clear();
                    for (SearchModel searchModel : tempItems) {
                        if (searchdata.getState().equals(searchModel.getState())) {
                            subMasterModels.add(new SubMasterModel(searchModel.getDescription(), searchModel.getEditioncode(), searchModel.getEditioncode(), searchModel.getState(), "", "", ""));
                        }
                    }
                    if (!TextUtils.isEmpty(EpaperSplashActivity.getDate)) {
                        context.startActivity(new Intent(context, EditionDetailActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                .putParcelableArrayListExtra("pages", subMasterModels)
                                .putExtra("statename", searchdata.getState())
                                .putExtra("editionname", searchdata.getDescription())
                                .putExtra("editioncode", searchdata.getEditioncode())
                                .putExtra("searchdate", EpaperSplashActivity.getDate));

                    } else {

                        context.startActivity(new Intent(context, EditionDetailActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                .putParcelableArrayListExtra("pages", subMasterModels)
                                .putExtra("statename", searchdata.getState())
                                .putExtra("editionname", searchdata.getDescription())
                                .putExtra("editioncode", searchdata.getEditioncode())
                                .putExtra("searchdate", DateUtility.setDateForAll));

                    }

                }
            });
            if (lblName != null)
                lblName.setText(searchdata.getEdition() + "    (" + searchdata.getState() + ")");
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((SearchModel) resultValue).getEdition();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (SearchModel people : tempItems) {
                    if (people.getEdition().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(people);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<SearchModel> filterList = (ArrayList<SearchModel>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (SearchModel people : filterList) {
                    add(people);
                    notifyDataSetChanged();
                }
            }
        }
    };
}
