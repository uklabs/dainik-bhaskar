package com.bhaskar.epaper.epaperv2.util;

public class QuickPreferences {

    public static String USER_LOGGED_IN_INTENT = "user_logged_in_intent";

    //update popup constants

    public interface ContactInfo {
        String ADDRESS_1 = "address1";
        String ADDRESS_2 = "address2";
        String FEEDBACK = "feedback";
        String SEND_EMAIL = "sendemail";
    }

    public interface Urls {
        String SUBS_INFO = "subInfoUrl";
        String SUBS_HISTORY = "subsHistoryUrl";
        String IS_USER_SUBSCRIBED = "isUserSubscribedUrl";
    }
}
