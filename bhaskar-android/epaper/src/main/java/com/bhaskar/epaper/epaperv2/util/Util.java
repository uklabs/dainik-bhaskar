package com.bhaskar.epaper.epaperv2.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.text.TextUtils;
import android.util.Base64;

import com.bhaskar.epaper.R;

public class Util {

    private static String autoGenerateId;

    public static boolean isEmailValid(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static void sendEmail(Context context, String subject, String email) {
        try {
            if (TextUtils.isEmpty(subject)) {
                subject = context.getString(R.string.feedback_mail_subject) + getAppVersion(context);
            }
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("message/rfc822");
            sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "");
            context.startActivity(Intent.createChooser(sendIntent, "Send email"));
        } catch (Exception e) {
        }
    }

    public static String getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null)
                return packageInfo.versionName;
        } catch (Exception e) {
        }

        return "5.0";
    }


    public static String encryptString(String str) {
        try {
            return Base64.encodeToString(str.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (Exception e) {
        }

        return "";
    }

    public static String decryptString(String str) {
        try {
            return new String(Base64.decode(str, Base64.DEFAULT), "UTF-8");
        } catch (Exception ignored) {
        }

        return "";
    }

    /**********************************************************************
     *  Call in Application
     ***********************/

    public static void initialiseComscore(Context applicationContext, String publisherId, String secretId) {
        // ComScore
//        PublisherConfiguration myPublisherConfig = new PublisherConfiguration.Builder()
//                .publisherId(publisherId)
//                .publisherSecret(secretId)
//                .vce(false)
//                .build();
//        Analytics.getConfiguration().addClient(myPublisherConfig);
//        if (applicationContext != null) {
//            Analytics.start(applicationContext);
//        }
    }

    public static void onResume(Context context) {
        // Comscore
//        Analytics.notifyEnterForeground();
    }

    public static void onPause(Context context) {
        // Comscore
//        Analytics.notifyExitForeground();
    }

    public static String getAutoGenerateId() {

        return autoGenerateId;
    }
}
