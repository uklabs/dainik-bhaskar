package com.bhaskar.epaper.epaperv2.util;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyNetworkSingleton {
    private static VolleyNetworkSingleton mInstance;
    Context mContext;
    private RequestQueue mRequestQueue;

    private VolleyNetworkSingleton(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized VolleyNetworkSingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyNetworkSingleton(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setShouldCache(false);
        getRequestQueue().add(req);
    }
}
