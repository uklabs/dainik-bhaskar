package com.bhaskar.epaper.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.imagezoom.ImageViewTouch;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.ui.ImageCropperActivity;
import com.bhaskar.epaper.ui.ImageCropperShareActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtility;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

import org.json.JSONException;
import org.json.JSONObject;

public class MagazinePageDetailViewpager extends Fragment {


    //    FloatingActionButton save_menu_btn;
//    FloatingActionButton share_menu_btn;
//    FloatingActionButton book_menu_btn;
//    FloatingActionButton crop_menu_btn;
//    FloatingActionMenu menuItem;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeLayout;
    TextView txt_loader;

    Epaper mApp;
    EpaperPrefernces mPref;
    MagazineMasterModel masteDetailModel;
    Bitmap imagebitmap = null;
    private ProgressDialog pd;

    public MagazinePageDetailViewpager() {
    }

    @SuppressLint("ValidFragment")
    public MagazinePageDetailViewpager(MagazineMasterModel magazineMasterModel) {
        this.masteDetailModel = magazineMasterModel;
    }

    private void initViews(View rootView) {
        swipeLayout = rootView.findViewById(R.id.swipeLayout);
        txt_loader = rootView.findViewById(R.id.txt_loader);
        progressBar = rootView.findViewById(R.id.progress);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pagerlayout, container, false);
        v.setSaveFromParentEnabled(false);
        initViews(v);
        mApp = Epaper.getInstance();
        mPref = EpaperPrefernces.getInstance(getActivity());

//        menuItem.removeMenuButton(book_menu_btn);
        final ImageViewTouch image = v.findViewById(R.id.pager_imageview);
//==================================== UNIVERSAL IMAGE ===========================
        final DisplayImageOptions opts = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.NONE).bitmapConfig(Bitmap.Config.ARGB_4444)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getActivity())
                .defaultDisplayImageOptions(opts)
                .memoryCache(new WeakMemoryCache())
                .build();

        if (!ImageLoader.getInstance().isInited())
            ImageLoader.getInstance().init(config);

        loadImage(image, opts);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadImage(image, opts);
            }
        });

        v.findViewById(R.id.crop_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                IsImageSaveAPI("crop");
              /*  if (ImageCropperShareActivity.isShowTime) {
                    ImageCropperShareActivity.imagebitmap = imagebitmap;
                    String description = *//*getResources().getString(R.string.dainikbhaskar) + "\n Edition: " + *//*masteDetailModel.getDescription() + " Page " + masteDetailModel.getPageNo();
                    Log.e("ImageURL Magazine", masteDetailModel.getImagepath());

                    startActivity(new Intent(getActivity(), ImageCropperShareActivity.class)
                            .putExtra("imgpath", masteDetailModel.getImagelarge())
                            .putExtra("pageno", masteDetailModel.getPageNo())
                            .putExtra("description", description)
                            .putExtra("editioncode", masteDetailModel.getEditioncode())
                            .putExtra("editionname", masteDetailModel.getDescription())
                    );
                } else {
                    if (imagebitmap != null) {
                        ImageCropperActivity.imagebitmap = imagebitmap;
                        String description = *//*getResources().getString(R.string.dainikbhaskar) + "\n Edition: " + *//*masteDetailModel.getDescription() + " Page " + masteDetailModel.getPageNo();
                        startActivity(new Intent(getActivity(), ImageCropperActivity.class).putExtra("imgpath", masteDetailModel.getImagelarge()).putExtra("pageno", masteDetailModel.getPageNo()).putExtra("description", description).putExtra("editionname", masteDetailModel.getMagazine()));
                    } else {
                        Utility.setToast(getActivity(), "Please Wait For Image Download");
                    }
                }*/

            }
        });
/*
        save_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebitmap != null) {
                    ImageUtility.saveImage(imagebitmap, getActivity());
                } else {
                    Utility.setToast(getActivity(), "Please Wait For Image Download");
                }
            }
        });

        share_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (imagebitmap != null) {
                    String description = getResources().getString(R.string.dainikbhaskar) + "\n Edition" + masteDetailModel.getDescription() + " Page No:" + masteDetailModel.getPagedate();
                    ImageUtility.saveSharedImage(imagebitmap, getActivity(), description);
                } else {
                    Utility.setToast(getActivity(), "Please Wait For Image Download");
                }

            }
        });
*/


        return v;
    }

    public void loadImage(ImageViewTouch image, DisplayImageOptions options) {
        if (masteDetailModel == null) {
            return;
        }

        ImageLoader.getInstance().displayImage(masteDetailModel.getImagepath(), image, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                if (swipeLayout.isRefreshing()) {
                    swipeLayout.setRefreshing(false);
                }

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
                Throwable cause = failReason.getCause();
                String message = cause != null ? cause.getMessage() : failReason.getType().name();

                if (isAdded() && getActivity() != null) {
                    Toast.makeText(getActivity(), "Error: " + message, Toast.LENGTH_LONG).show();
                    swipeLayout.setRefreshing(true);
                    swipeLayout.setEnabled(true);
                }
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
               /* pbPercent.setVisibility(View.GONE);
                tvPerc.setVisibility(View.GONE);*/
                imagebitmap = loadedImage;
                progressBar.setVisibility(View.GONE);
                txt_loader.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                swipeLayout.setRefreshing(false);
                swipeLayout.setEnabled(false);

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
//                Toast.makeText(getActivity(), this.getClass().getSimpleName() + " : OnLoadingCancelled", Toast.LENGTH_LONG).show();
//                doBar();
                if (swipeLayout.isRefreshing()) {
                    swipeLayout.setRefreshing(false);
                }

            }
            // NOTE: .cacheOnDisk(true) must be set in the options or this Listener won't work
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
                int percent = (current * 100) / 1112000;
                progressBar.setProgress(percent);

            }
        });
    }

    //API IS IMAGE SAVE
    public void IsImageSaveAPI(final String chkClick)
    {
        setProgressbar();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id",TrackingData.getDBId(getContext()));
            jsonObject.put("img", imagebitmap);
            jsonObject.put("type", chkClick);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Epaper.IS_IMAGE_SAVE, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (pd != null && !getActivity().isFinishing()) {
                    pd.dismiss();
                }

                try {
                    String img = response.getString("img");
                    //String img = "";
                    if (img != null) {
                        if (!img.equalsIgnoreCase("")) {
                            if (imagebitmap != null) {
                                if (ImageCropperShareActivity.isShowTime) {
                                    ImageCropperShareActivity.imagebitmap = imagebitmap;
                                    String description = /*getResources().getString(R.string.dainikbhaskar) + "\n Edition: " + */masteDetailModel.getDescription() + " Page " + masteDetailModel.getPageNo();
                                    Log.e("ImageURL Magazine", masteDetailModel.getImagepath());

                                    startActivity(new Intent(getActivity(), ImageCropperShareActivity.class)
                                            .putExtra("imgpath", masteDetailModel.getImagelarge())
                                            .putExtra("pageno", masteDetailModel.getPageNo())
                                            .putExtra("description", description)
                                            .putExtra("editioncode", masteDetailModel.getEditioncode())
                                            .putExtra("editionname", masteDetailModel.getDescription())
                                    );
                                } else {
                                    if (imagebitmap != null) {
                                        ImageCropperActivity.imagebitmap = imagebitmap;
                                        String description = /*getResources().getString(R.string.dainikbhaskar) + "\n Edition: " + */masteDetailModel.getDescription() + " Page " + masteDetailModel.getPageNo();
                                        startActivity(new Intent(getActivity(), ImageCropperActivity.class).putExtra("imgpath", masteDetailModel.getImagelarge()).putExtra("pageno", masteDetailModel.getPageNo()).putExtra("description", description).putExtra("editionname", masteDetailModel.getMagazine()));
                                    } else {
                                        Utility.setToast(getActivity(), "Please Wait For Image Download");
                                    }
                                }

                            } else {
                                showSaveImgDialog(chkClick, response.getString("message"));
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(getActivity());
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void setProgressbar() {
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait");
        pd.setCanceledOnTouchOutside(false);
        if (!getActivity().isFinishing()) {
            pd.show();
        }
    }

    private void showSaveImgDialog(String chkClick, String message) {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.save_image_dialog, null);
        exitDialogBuilder.setView(dialogView);
        TextView mszTxt = dialogView.findViewById(R.id.mszTxt);
        TextView titleDialog = dialogView.findViewById(R.id.titleDialog);
        TextView okBtn = dialogView.findViewById(R.id.okBtn);
        final AlertDialog mszDialog = exitDialogBuilder.create();
        mszDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mszDialog.setCancelable(true);
        String upperString = chkClick.substring(0, 1).toUpperCase() + chkClick.substring(1);
        titleDialog.setText(upperString + " Epaper");
        mszTxt.setText(message);
        if (okBtn != null)
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mszDialog.dismiss();

                }
            });

        mszDialog.show();
    }

}
