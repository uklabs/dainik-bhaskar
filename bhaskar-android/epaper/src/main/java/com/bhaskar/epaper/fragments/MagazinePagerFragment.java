package com.bhaskar.epaper.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MagazineRecyclerAdapter;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MagazinePagerFragment extends Fragment {

    List<MagazineMasterModel> modelList = new ArrayList<>();
    String magazineLang;
    EpaperPrefernces mPref;
    Epaper mApp;
    MagazineRecyclerAdapter magazineRecyclerAdapter;
    public MagazinePagerFragment() {
    }

    @SuppressLint("ValidFragment")
    public MagazinePagerFragment(List<MagazineMasterModel> pages, String magazineLang) {
        this.modelList = pages;
        this.magazineLang=magazineLang;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_sub_magazine_master, container, false);
        v.setSaveFromParentEnabled(false);
        mApp = Epaper.getInstance();
        mPref = EpaperPrefernces.getInstance(getActivity());
        RecyclerView recycler = v.findViewById(R.id.homeRecyclerList);
        int columns = Utility.calculateNoOfColumns(getActivity());
        recycler.setLayoutManager(new GridLayoutManager(getActivity(), columns));
        Collections.sort(modelList, new Comparator<MagazineMasterModel>() {
            public int compare(MagazineMasterModel o1, MagazineMasterModel o2) {
                if (o1 == null || o1.getPagedate() == null || o2 == null || o2.getPagedate() == null)
                    return 0;
                return o1.getPagedate().compareTo(o2.getPagedate());
            }
        });
        Collections.reverse(modelList);
        magazineRecyclerAdapter=new MagazineRecyclerAdapter(getActivity(), modelList, magazineLang,true);
        recycler.setAdapter(magazineRecyclerAdapter);


        return v;
    }

}