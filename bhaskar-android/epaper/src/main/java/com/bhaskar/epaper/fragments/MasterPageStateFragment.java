package com.bhaskar.epaper.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.HomeItemAdapter;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.homeutils.HomeContent;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.ui.ConnectionDetector;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.epaper.utils.XMLParser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class MasterPageStateFragment extends Fragment {

    EpaperPrefernces mPref;
    Epaper epaper;
    String statename;
    List<MasterModel> stateEditionList = new ArrayList<>();
    RecyclerView recycler;
    ProgressBar progress;

    public MasterPageStateFragment() {
    }

    @SuppressLint("ValidFragment")
    public MasterPageStateFragment(String Statename) {
        this.statename = Statename;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_state_master, container, false);
        v.setSaveFromParentEnabled(false);
        mPref = EpaperPrefernces.getInstance(getActivity());
        epaper = Epaper.getInstance();

        recycler = v.findViewById(R.id.homeRecyclerList);
        progress = v.findViewById(R.id.progress);
        progress.setVisibility(View.VISIBLE);

        int columns = Utility.calculateNoOfColumns(getActivity());
        recycler.setLayoutManager(new GridLayoutManager(getActivity(), columns));
        callStateWiseLoadData();
        return v;
    }

    private void callStateWiseLoadData() {
        String dateSetter = DateUtility.setDateForAll;
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            try {
                String filename = "jx-master/xml/" + statename.toUpperCase() + "/" + dateSetter + "/";
                String url = Urls.BASE_URL + filename;
                StringRequest request = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String res) {
                        progress.setVisibility(View.GONE);
                        try {
                            setXmlParsingMaster(res, statename);
                        } catch (Exception ignore) {
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            progress.setVisibility(View.GONE);
                            Toast toast = Toast.makeText(getActivity(), "NO Data Found", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.setNetworkErrorToast(getActivity());
        }
    }


    private void setXmlParsingMaster(String xmlres, String state) {
        XMLParser xmlparser = new XMLParser();

        Document doc = xmlparser.getDomElement(xmlres);
        NodeList nl = doc.getElementsByTagName("data");
        stateEditionList.clear();

        if (nl.getLength() > 0) {
            for (int i = 0; i < nl.getLength(); i++) {
                Element e = (Element) nl.item(i);
                String editioncode = xmlparser.getValue(e, "editioncode"); // name child value
                String description = xmlparser.getValue(e, "description"); // cost child value
                String priorty = xmlparser.getValue(e, "priorty"); // description child value
                String imagepath = xmlparser.getValue(e, "imagepath"); // description child value
                String imagethumb = xmlparser.getValue(e, "imagethumb"); // description child value
                String imagelarge = xmlparser.getValue(e, "imagelarge"); // description child value
                stateEditionList.add(new MasterModel(editioncode, description, priorty, imagepath, imagethumb, imagelarge));
            }
        }
        String listString = new Gson().toJson(
                stateEditionList,
                new TypeToken<List<MasterModel>>() {
                }.getType());
        recycler.setAdapter(new HomeItemAdapter(getActivity(), state, stateEditionList, mPref, epaper));
        HomeContent.listMain.put(state, stateEditionList);
    }

}