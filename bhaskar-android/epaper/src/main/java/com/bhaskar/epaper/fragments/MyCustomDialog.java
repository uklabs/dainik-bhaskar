package com.bhaskar.epaper.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.PagerAdapter;
import com.bhaskar.epaper.model.MasterPageModel;

import java.util.List;

public class MyCustomDialog extends DialogFragment {
    Button mButton;
    EditText mEditText;
    onSubmitListener mListener;
    String text = "";
    public static List<MasterPageModel> listdata;
    int position;
    ViewPager viewpager;

    public MyCustomDialog(){}

    @SuppressLint("ValidFragment")
    public MyCustomDialog(List<MasterPageModel> listdata, int position) {
        this.position = position;
    }

    @SuppressLint("ValidFragment")
    public MyCustomDialog(int position) {
        this.position = position;
    }

    interface onSubmitListener {
        void setOnSubmitListener(String arg);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View dialog = inflater.inflate(R.layout.activity_detail_swipe, container,false);

    /*    final Dialog dailog = getDialog();
//        dailog.setTitle(getArguments().getString("title"));

        dailog.getWindow().setBackgroundDrawable(null);
//        dailog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dailog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dailog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
*/
       /* ImageView closebtn = (ImageView) dialog.findViewById(R.id.closeimgeview);
        closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                dailog.dismiss();
            }
        });
*/
        viewpager = (ViewPager) dialog.findViewById(R.id.dailog_viewpager);

        viewpager.setAdapter(new PagerAdapter(getChildFragmentManager(), listdata));
        viewpager.setCurrentItem(position);

        return dialog;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

/*
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.dailoglayout);

        ImageView closebtn = (ImageView) dialog.findViewById(R.id.closeimgeview);
        closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        viewpager = (ViewPager) dialog.findViewById(R.id.dailog_viewpager);
        EditionDetailActivity act = (EditionDetailActivity) getActivity();
        viewpager.setAdapter(new PagerAdapter(act.getSupportFragmentManager(), listdata));
        viewpager.setCurrentItem(position);

        return dialog;
    }*/
}