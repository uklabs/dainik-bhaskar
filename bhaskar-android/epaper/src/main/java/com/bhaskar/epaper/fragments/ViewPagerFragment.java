package com.bhaskar.epaper.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.imagezoom.ImageViewTouch;
import com.bhaskar.epaper.model.MasterPageModel;
import com.bhaskar.epaper.ui.ImageCropperActivity;
import com.bhaskar.epaper.ui.ImageCropperShareActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Utility;
import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

import org.json.JSONException;
import org.json.JSONObject;

public class ViewPagerFragment extends Fragment {

    ProgressBar progressBar;
    TextView txt_loader;
    SwipeRefreshLayout swipeLayout;
    ImageView imageview;
    ImageView imgPlaceholder;

    Epaper mApp;
    ImageViewTouch image;
    EpaperPrefernces mPref;
    Bitmap imagebitmap = null;
    private String imgThumb = "";
    MasterPageModel masteDetailModel;
    private ProgressDialog pd;

    @SuppressLint("ValidFragment")
    public ViewPagerFragment(MasterPageModel questionItem) {
        this.masteDetailModel = questionItem;
    }

    public ViewPagerFragment() {
    }

    private void initViews(View rootView) {
        imgPlaceholder = rootView.findViewById(R.id.imgPlaceholder);
        imageview = rootView.findViewById(R.id.imageview);
        swipeLayout = rootView.findViewById(R.id.swipeLayout);
        txt_loader = rootView.findViewById(R.id.txt_loader);
        progressBar = rootView.findViewById(R.id.progress);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pagerlayout, container, false);
        v.setSaveFromParentEnabled(false);

        initViews(v);
        mApp = Epaper.getInstance();
        mPref = EpaperPrefernces.getInstance(getActivity());
        image = v.findViewById(R.id.pager_imageview);
        imageview.setOnTouchListener(new ImageMatrixTouchHandler(v.getContext()));

        progressBar.bringToFront();
        txt_loader.bringToFront();
        v.findViewById(R.id.crop_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                IsImageSaveAPI("crop");
                /*if (ImageCropperShareActivity.isShowTime) {
                    ImageCropperShareActivity.imagebitmap = imagebitmap;
                    String description = masteDetailModel.description + " Page " + masteDetailModel.pageno;
                    startActivity(
                            new Intent(getActivity(), ImageCropperShareActivity.class)
                                    .putExtra("editioncode", masteDetailModel.editioncode)
                                    .putExtra("pageno", masteDetailModel.pageno)
                                    .putExtra("description", description)
                                    .putExtra("editionname", masteDetailModel.description)
                                    .putExtra("imgpath", masteDetailModel.imagepath)
                    );
                } else {
                    if (imagebitmap != null) {
                        ImageCropperActivity.imagebitmap = imagebitmap;
                        String description = masteDetailModel.description + " Page " + masteDetailModel.pageno;
                        startActivity(
                                new Intent(getActivity(), ImageCropperActivity.class)
                                        .putExtra("pageno", masteDetailModel.pageno)
                                        .putExtra("description", description)
                                        .putExtra("editionname", masteDetailModel.description)
                                        .putExtra("imgpath", masteDetailModel.imagepath)
                        );
                    } else {
                        Utility.setToast(getActivity(), "Please Wait For Image Download");
                    }
                }*/
            }
        });


        final DisplayImageOptions opts = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.NONE)
                .displayer(new FadeInBitmapDisplayer(300))
                .bitmapConfig(Bitmap.Config.ARGB_4444)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getActivity())
                .defaultDisplayImageOptions(opts)
                .memoryCache(new WeakMemoryCache())
                .build();

        if (!ImageLoader.getInstance().isInited())
            ImageLoader.getInstance().init(config);

        loadImages(image, opts);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadImages(image, opts);
            }
        });

        //==========================
        return v;
    }

    public void loadImages(final ImageView image, DisplayImageOptions opts) {
        txt_loader.setText("Please wait...");

        String imgPath = "http://app.dbclmatrix.com/nopicture.png", tmpImgThumb = "http://app.dbclmatrix.com/nopicture.png";

        if (masteDetailModel != null) {
            if (masteDetailModel.imagepath != null) {
                imgPath = masteDetailModel.imagepath;
            } else if (masteDetailModel.imagelarge != null) {
                imgPath = masteDetailModel.imagelarge;
            }
            if (masteDetailModel.imagethumb != null) {
                tmpImgThumb = masteDetailModel.imagethumb;
            }
        }

        imgThumb = tmpImgThumb;
        ImageLoader.getInstance().displayImage(imgPath, image, opts, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {

//                Log.e("image", "=>" + imageUri);
                if (swipeLayout.isRefreshing()) {
                    swipeLayout.setRefreshing(false);
                }

                /*preview*/
                imgPlaceholder.setVisibility(View.VISIBLE);

                progressBar.setVisibility(View.VISIBLE);
                Glide.with(getActivity()).load(imgThumb).into(imgPlaceholder);
                /*preview*/
                txt_loader.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                @SuppressWarnings("ThrowableResultOfMethodCallIgnored")

                Throwable cause = failReason.getCause();
                String message = cause != null ? cause.getMessage() : failReason.getType().name();

                /*preview*/
                txt_loader.setVisibility(View.GONE);
                imgPlaceholder.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                imagebitmap = loadedImage;

                image.setImageBitmap(imagebitmap);
                image.setVisibility(View.VISIBLE);

                swipeLayout.setRefreshing(false);
                swipeLayout.setEnabled(false);

                /*preview*/
                txt_loader.setVisibility(View.GONE);
                imgPlaceholder.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                imgPlaceholder.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
            }
        });
    }

    /*private void setBookmarkdata() {
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait ...");
        pd.setCanceledOnTouchOutside(false);

        MasterModel bookmarkIOnfo = new MasterModel();
        bookmarkIOnfo.id = TrackingUtils.getAutoGenerateId();
        bookmarkIOnfo.editioncode = masteDetailModel.editioncode;
        bookmarkIOnfo.description = masteDetailModel.description;
        bookmarkIOnfo.priorty = masteDetailModel.pageno;
        bookmarkIOnfo.imagepath = masteDetailModel.imagepath;
        bookmarkIOnfo.imagethumb = masteDetailModel.imagethumb;
        bookmarkIOnfo.imagelarge = masteDetailModel.imagelarge;


        if (!SerializeData.getInstance(getActivity()).isBookmarkAvailable(bookmarkIOnfo.imagepath)) {
            SerializeData.getInstance(getActivity()).saveBookmark(bookmarkIOnfo);
            Utility.setToast(getActivity(), "Successfully Bookmarked");
        } else {
            Utility.setToast(getActivity(), "Bookmark Already Exist...");
        }

    }*/

    //API IS IMAGE SAVE
    public void IsImageSaveAPI(final String chkClick)
    {
        setProgressbar();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", TrackingData.getDBId(getContext()));
            jsonObject.put("img", imgThumb);
            jsonObject.put("type", chkClick);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Epaper.IS_IMAGE_SAVE, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (pd != null && !getActivity().isFinishing()) {
                    pd.dismiss();
                }

                try {
                    String img = response.getString("img");
                    //String img = "";
                    if (img != null) {
                        if (!img.equalsIgnoreCase("")) {
                            if (imagebitmap != null) {
                                if (ImageCropperShareActivity.isShowTime) {
                                    ImageCropperShareActivity.imagebitmap = imagebitmap;
                                    String description = masteDetailModel.description + " Page " + masteDetailModel.pageno;
                                    startActivity(
                                            new Intent(getActivity(), ImageCropperShareActivity.class)
                                                    .putExtra("editioncode", masteDetailModel.editioncode)
                                                    .putExtra("pageno", masteDetailModel.pageno)
                                                    .putExtra("description", description)
                                                    .putExtra("editionname", masteDetailModel.description)
                                                    .putExtra("imgpath", masteDetailModel.imagepath)
                                    );
                                } else {
                                    if (imagebitmap != null) {
                                        ImageCropperActivity.imagebitmap = imagebitmap;
                                        String description = masteDetailModel.description + " Page " + masteDetailModel.pageno;
                                        startActivity(
                                                new Intent(getActivity(), ImageCropperActivity.class)
                                                        .putExtra("pageno", masteDetailModel.pageno)
                                                        .putExtra("description", description)
                                                        .putExtra("editionname", masteDetailModel.description)
                                                        .putExtra("imgpath", masteDetailModel.imagepath)
                                        );
                                    } else {
                                        Utility.setToast(getActivity(), "Please Wait For Image Download");
                                    }
                                }
                            } else {
                                showSaveImgDialog(chkClick, response.getString("message"));
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(getActivity());
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void setProgressbar() {
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait");
        pd.setCanceledOnTouchOutside(false);
        if (!getActivity().isFinishing()) {
            pd.show();
        }
    }

    private void showSaveImgDialog(String chkClick, String message) {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.save_image_dialog, null);
        exitDialogBuilder.setView(dialogView);
        TextView mszTxt = dialogView.findViewById(R.id.mszTxt);
        TextView titleDialog = dialogView.findViewById(R.id.titleDialog);
        TextView okBtn = dialogView.findViewById(R.id.okBtn);
        final AlertDialog mszDialog = exitDialogBuilder.create();
        mszDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mszDialog.setCancelable(true);
        String upperString = chkClick.substring(0, 1).toUpperCase() + chkClick.substring(1);
        titleDialog.setText(upperString + " Epaper");
        mszTxt.setText(message);
        if (okBtn != null)
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mszDialog.dismiss();

                }
            });

        mszDialog.show();
    }
}
