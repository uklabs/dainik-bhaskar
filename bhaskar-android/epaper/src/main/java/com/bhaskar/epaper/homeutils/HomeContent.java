package com.bhaskar.epaper.homeutils;

import android.content.Context;

import com.bhaskar.epaper.model.MasterModel;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by db on 8/29/2R.drawable.ic_mytodo16.
 */
public class HomeContent implements Serializable {

    private Context context;
    public static LinkedHashMap<String, List<MasterModel>> listMain = new LinkedHashMap<>();

    public Object getElementByIndex(int index) {
        return listMain.get(listMain.keySet().toArray()[index]);
    }

    public HomeContent(Context context) {
        this.context = context;
    }
}
