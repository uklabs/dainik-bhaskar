package com.bhaskar.epaper.homeutils;

/**
 * Created by db on 8/29/2016.
 */
public class HomeModel {
    public String title;
    public int img;
    public Class activity;
    public String value;
    public int color;


    public HomeModel(String title, int img) {
        this.title = title;
        this.img = img;
    }

    public HomeModel(String title, int img, Class activity, int color) {
        this.title = title;
        this.img = img;
        this.activity = activity;
        this.color = color;
    }

    public HomeModel(String title, int img, Class activity, String value, int color) {
        this.title = title;
        this.img = img;
        this.activity = activity;
        this.value = value;
        this.color = color;
    }
}
