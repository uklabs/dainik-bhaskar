package com.bhaskar.epaper.model;

/**
 * Created by db on 11/5/2016.
 */
public class EditionModel {
    public String id;
    public String state;
    public String center;
    public String edtname;
    public String edtcode;
    public String org_path;
    public String thumb_path;
    public String large_path;

    public EditionModel(String edtcode) {
        this.edtcode = edtcode;
    }

    public EditionModel(String id, String state, String center, String edtname, String edtcode, String org_path, String thumb_path, String large_path) {
        this.id = id;
        this.state = state;
        this.center = center;
        this.edtname = edtname;
        this.edtcode = edtcode;
        this.org_path = org_path;
        this.thumb_path = thumb_path;
        this.large_path = large_path;
    }

    public EditionModel(String id, String state, String center, String edtname, String edtcode) {
        this.id = id;
        this.state = state;
        this.center = center;
        this.edtname = edtname;
        this.edtcode = edtcode;
    }

    public EditionModel() {

    }

    public String getOrg_path() {
        return org_path;
    }

    public void setOrg_path(String org_path) {
        this.org_path = org_path;
    }

    public String getThumb_path() {
        return thumb_path;
    }

    public void setThumb_path(String thumb_path) {
        this.thumb_path = thumb_path;
    }

    public String getLarge_path() {
        return large_path;
    }

    public void setLarge_path(String large_path) {
        this.large_path = large_path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getEdtname() {
        return edtname;
    }

    public void setEdtname(String edtname) {
        this.edtname = edtname;
    }

    public String getEdtcode() {
        return edtcode;
    }

    public void setEdtcode(String edtcode) {
        this.edtcode = edtcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EditionModel that = (EditionModel) o;
        return (edtcode != null ? edtcode.equals(that.edtcode) : that.edtcode == null);
    }
}
