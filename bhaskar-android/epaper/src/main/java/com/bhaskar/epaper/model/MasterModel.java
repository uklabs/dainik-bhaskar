package com.bhaskar.epaper.model;

import java.io.Serializable;

/**
 * Created by db on 9/15/2016.
 */
public class MasterModel implements Serializable {
    public String editioncode;
    public String description;
    public String priorty;
    public String imagepath;
    public String imagethumb;
    public String imagelarge;
    public String id;

    public MasterModel(String imagepath) {
        this.imagepath = imagepath;
    }

    public MasterModel(String editioncode, String description, String priorty, String imagepath, String imagethumb, String imagelarge) {
        this.editioncode = editioncode;
        this.description = description;
        this.priorty = priorty;
        this.imagepath = imagepath;
        this.imagethumb = imagethumb;
        this.imagelarge = imagelarge;
    }

    public MasterModel(String priorty, String description, String imagepath, String imagethumb, String imagelarge) {
        this.priorty = priorty;
        this.description = description;
        this.imagepath = imagepath;
        this.imagethumb = imagethumb;
        this.imagelarge = imagelarge;
    }

    public MasterModel(String editioncode, String description, String imagepath, String imagethumb, String imagelarge, String id, String str) {
        this.editioncode = editioncode;
        this.description = description;
        this.imagepath = imagepath;
        this.imagethumb = imagethumb;
        this.imagelarge = imagelarge;
        this.id = id;
    }

    public MasterModel() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MasterModel that = (MasterModel) o;
        return (imagepath != null ? imagepath.equals(that.imagepath) : that.imagepath == null);
    }
}
