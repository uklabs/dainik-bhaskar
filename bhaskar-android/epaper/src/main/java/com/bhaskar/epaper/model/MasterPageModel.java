package com.bhaskar.epaper.model;

/**
 * Created by db on 9/15/2016.
 */
public class MasterPageModel {
    public String editioncode;
    public String pageno;
    public String page;
    public String priorty;
    public String description;
    public String imagepath;
    public String imagethumb;
    public String imagelarge;
    public String imageSize;


    public MasterPageModel(String editioncode, String pageno, String page, String priorty, String imagepath, String imagethumb, String imagelarge, String descrption) {
        this.editioncode = editioncode;
        this.pageno = pageno;
        this.page = page;
        this.priorty = priorty;
        this.imagepath = imagepath;
        this.imagethumb = imagethumb;
        this.imagelarge = imagelarge;
        this.description = descrption;
    }

    /*public MasterPageModel(String editioncode, String pageno, String page, String priorty, String imagepath, String imagethumb, String imagelarge, String imageSize) {
        this.editioncode = editioncode;
        this.pageno = pageno;
        this.page = page;
        this.priorty = priorty;
        this.imagepath = imagepath;
        this.imagethumb = imagethumb;
        this.imagelarge = imagelarge;
        this.imageSize = imageSize;
    }*/
}
