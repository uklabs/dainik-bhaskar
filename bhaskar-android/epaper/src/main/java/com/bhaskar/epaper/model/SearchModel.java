package com.bhaskar.epaper.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by db on 12/29/2016.
 */
public class SearchModel implements Parcelable {

    @SerializedName("edition")
    @Expose
    private String edition;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("editioncode")
    @Expose
    private String editioncode;
    @SerializedName("state")
    @Expose
    private String state;

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEditioncode() {
        return editioncode;
    }

    public void setEditioncode(String editioncode) {
        this.editioncode = editioncode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.edition);
        dest.writeString(this.description);
        dest.writeString(this.editioncode);
        dest.writeString(this.state);

    }

    public SearchModel() {
    }

    protected SearchModel(Parcel in) {

        this.edition = in.readString();
        this.description = in.readString();
        this.editioncode = in.readString();
        this.state = in.readString();

    }

    public static final Creator<SearchModel> CREATOR = new Creator<SearchModel>() {

        @Override
        public SearchModel createFromParcel(Parcel source) {
            return new SearchModel(source);
        }

        @Override
        public SearchModel[] newArray(int size) {
            return new SearchModel[size];
        }
    };

}
