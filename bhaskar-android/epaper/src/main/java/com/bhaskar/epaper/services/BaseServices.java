package com.bhaskar.epaper.services;


import com.bhaskar.epaper.model.MagazineMasterModel;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


/**
 * Created by db on 3/21/2016.
 */
public interface BaseServices {

//
//    @FormUrlEncoded
//    @POST("/view_myedition.php")
//    void getMyEditionData(@Field("email") String email, Callback<JsonElement> callback);



//    @GET("datajson.php")
//    Call<ArrayList<SearchModel>>  getSearchData();


    @GET("magazine-master.json.php")
    Call<ArrayList<MagazineMasterModel>> getMagazineMasterData();


    @GET("magazine-submaster.json.php")
    Call<ArrayList<MagazineMasterModel>> getMagazineSubMasterData();


    @GET("magazine-details.json.php")
    Call<ArrayList<MagazineMasterModel>> getMagazineDetailData();


    @Multipart
    @POST("set_best_story.php")
    Call<JSONObject> setBestStory(@Part("comment") String comment, @Part("edition_code") String edition_code, @Part("edition_name") String edition_name, @Part("img_origanal") String img_org_path, @Part("img_cropped") RequestBody file);

}
