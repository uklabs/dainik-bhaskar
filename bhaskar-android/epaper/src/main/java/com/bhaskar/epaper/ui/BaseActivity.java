package com.bhaskar.epaper.ui;

import android.os.Bundle;

import com.bhaskar.epaper.epaperv2.util.Util;
import com.db.main.BaseAppCompatActivity;

public class BaseActivity extends BaseAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Util.onResume(BaseActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Util.onPause(BaseActivity.this);
    }
}
