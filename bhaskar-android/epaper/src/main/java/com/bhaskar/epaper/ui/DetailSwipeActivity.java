package com.bhaskar.epaper.ui;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.PagerAdapter;
import com.bhaskar.epaper.adapter.SearchAdapter;
import com.bhaskar.epaper.fragments.MyCustomDialog;
import com.bhaskar.epaper.model.SearchModel;
import com.bhaskar.epaper.model.SubMasterModel;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.appscommon.tracking.Tracking;

import java.util.ArrayList;
import java.util.Collections;


public class DetailSwipeActivity extends BaseActivity {

    public ArrayList<SubMasterModel> listdata;
    public ArrayList<SearchModel> searchList;
    ViewPager viewpager;
    TabLayout tabs;
    ImageView leftimg;
    ImageView rightimg;
    ImageView homeIcon;
    TabLayout edition_tabs;
    AutoCompleteTextView searchTxt;
    String description, state, cityName;
    EpaperPrefernces pref;

    private void initViews() {
        viewpager = findViewById(R.id.dailog_viewpager);
        tabs = findViewById(R.id.tabs);
        leftimg = findViewById(R.id.dailog_left_arrow_img);
        rightimg = findViewById(R.id.dailog_right_arrow_img);
        homeIcon = findViewById(R.id.home_icon);
        edition_tabs = findViewById(R.id.edition_tabs);
        searchTxt = findViewById(R.id.autoCompleteTxt);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_swipe);
        initViews();
        int position = getIntent().getIntExtra("position", 0);
        description = getIntent().getStringExtra("editionname");
        state = getIntent().getStringExtra("state");
        cityName = getIntent().getStringExtra("cityname");
        pref = EpaperPrefernces.getInstance(DetailSwipeActivity.this);

        if (getIntent().getParcelableArrayListExtra("pages") != null) {
            listdata = getIntent().getParcelableArrayListExtra("pages");
        }
        if (getIntent().getParcelableArrayListExtra("searchList") != null) {
            searchList = getIntent().getParcelableArrayListExtra("searchList");
        }

        viewpager = findViewById(R.id.dailog_viewpager);

        if (MyCustomDialog.listdata != null && MyCustomDialog.listdata.size() > 0) {
            viewpager.setAdapter(new PagerAdapter(getSupportFragmentManager(), MyCustomDialog.listdata));
            Tracking.trackGAScreen(DetailSwipeActivity.this, EpaperSplashActivity.getInstance(DetailSwipeActivity.this).defaultTracker, EpaperSplashActivity.getInstance(DetailSwipeActivity.this).modulePrefix+"-"+state+"-"+cityName+"-"+description+"-PAGE1", EpaperSplashActivity.getInstance(DetailSwipeActivity.this).source, EpaperSplashActivity.getInstance(DetailSwipeActivity.this).medium, EpaperSplashActivity.getInstance(DetailSwipeActivity.this).campaign);
        }
        viewpager.setOffscreenPageLimit(1);
        viewpager.setCurrentItem(position);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Tracking.trackGAScreen(DetailSwipeActivity.this, EpaperSplashActivity.getInstance(DetailSwipeActivity.this).defaultTracker, EpaperSplashActivity.getInstance(DetailSwipeActivity.this).modulePrefix+"-"+state+"-"+cityName+"-"+description+"-PAGE"+(position+1), EpaperSplashActivity.getInstance(DetailSwipeActivity.this).source, EpaperSplashActivity.getInstance(DetailSwipeActivity.this).medium, EpaperSplashActivity.getInstance(DetailSwipeActivity.this).campaign);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        tabs.setupWithViewPager(viewpager);

        if (listdata != null && listdata.size() > 0) {
            for (int i = 0; i < listdata.size(); i++) {
                if (!TextUtils.isEmpty(description)) {
                    if (description.contains(listdata.get(i).description)) {
                        Collections.swap(listdata, i, 0);
                    }
                }
            }

            searchTxt.setVisibility(View.GONE);
            edition_tabs.setVisibility(View.VISIBLE);

        } else if (searchList != null && searchList.size() > 0) {
            searchTxt.setVisibility(View.VISIBLE);
            edition_tabs.setVisibility(View.GONE);
            setAutoCompleteText();

        } else {
            searchTxt.setVisibility(View.GONE);
            edition_tabs.setVisibility(View.GONE);
        }

        if (edition_tabs.getVisibility() == View.VISIBLE) {
            for (int i = 0; i < listdata.size(); i++) {
                edition_tabs.addTab(edition_tabs.newTab().setText(listdata.get(i).description));
            }
        }

        edition_tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                startActivity(new Intent(DetailSwipeActivity.this, EditionDetailActivity.class).putExtra("statename", state)
                        .putExtra("position", 0)
                        .putExtra("editionname", listdata.get(tab.getPosition()).description)
                        .putExtra("editioncode", listdata.get(tab.getPosition()).editioncode)
                        .putExtra("cityname", cityName)
                        .putParcelableArrayListExtra("pages", listdata)
                );
                finish();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                startActivity(new Intent(
                        DetailSwipeActivity.this, EditionDetailActivity.class)
                        .putExtra("statename", state)
                        .putExtra("position", 0)
                        .putExtra("editionname", listdata.get(tab.getPosition()).description)
                        .putExtra("editioncode", listdata.get(tab.getPosition()).editioncode)
                        .putParcelableArrayListExtra("pages", listdata)
                );
                finish();
            }
        });

//        new FBAdUtility(this).loadBannerAd(adview);
//        new AdUtility(this).loadBannerAd((AdView) findViewById(R.id.adView));

        homeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EpaperPrefernces.getInstance(getApplicationContext()).setHome();
            }
        });

        rightimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int viewpagercurrent = viewpager.getCurrentItem();
                if (viewpagercurrent < MyCustomDialog.listdata.size()) {
                    viewpager.setCurrentItem(++viewpagercurrent);
                }
            }
        });

        leftimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int viewpagercurrent = viewpager.getCurrentItem();
                if (viewpagercurrent > 0) {
                    viewpager.setCurrentItem(--viewpagercurrent);
                }
            }
        });
    }

    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, fragment);
        fragmentTransaction.commit();
    }

    public void setAutoCompleteText() {
        searchTxt.clearFocus();

        if (searchList != null && searchList.size() > 0) {
            searchTxt.setVisibility(View.VISIBLE);
            searchTxt.setThreshold(2);
            searchTxt.setAdapter(new SearchAdapter(DetailSwipeActivity.this, searchList));
        } else {
            searchTxt.setVisibility(View.GONE);
            searchTxt.setText("No Network Connection");
            searchTxt.setEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
