package com.bhaskar.epaper.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.fragments.MyCustomDialog;
import com.bhaskar.epaper.model.MasterPageModel;
import com.bhaskar.epaper.model.SearchModel;
import com.bhaskar.epaper.model.SubMasterModel;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.epaper.utils.XMLParser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class EditionDetailActivity extends BaseActivity {

    public ArrayList<SubMasterModel> listdata;
    public ArrayList<SearchModel> searchList;
    Epaper epaper;
    String stateName,cityName;
    String geteditioncode;
    String currentDate;
    String searchDate;
    String description;
    RecyclerView homeRecyclerList;
    RecyclerView stateSuggetion;
    SwipeRefreshLayout swipeLayout;
    ProgressBar progressBar;

    private void initViews() {
        progressBar = findViewById(R.id.progress);
        swipeLayout = findViewById(R.id.swipeLayout);
        stateSuggetion = findViewById(R.id.stateSuggetion);
        homeRecyclerList = findViewById(R.id.homeRecyclerList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_master);
        initViews();
        setToolbar();


        epaper = Epaper.getInstance();
        stateName = getIntent().getStringExtra("statename"); //"RAJSATHAN";//
        geteditioncode = getIntent().getStringExtra("editioncode");//"14";
        cityName = getIntent().getStringExtra("cityname");
        currentDate = getIntent().getStringExtra("currentdate");
        searchDate = getIntent().getStringExtra("searchdate");
        description = getIntent().getStringExtra("editionname");


        if (getIntent().getParcelableArrayListExtra("pages") != null) {
            listdata = getIntent().getParcelableArrayListExtra("pages");
        }
        if (getIntent().getParcelableArrayListExtra("searchList") != null) {
            searchList = getIntent().getParcelableArrayListExtra("searchList");
        }
//        Log.e("State", "=>" + stateName + "====" + geteditioncode + "==" + description + "==" + currentDate + "===" + DateUtility.setDateForAll);

        callXmlsetter();
    }

    private void setToolbar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_action_bar_back);
//        upArrow.setColorFilter(getResources().getColor(R.color.md_grey_100), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        setToolbarImage();
    }

    public void setToolbarImage() {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    private void callXmlsetter() {
        progressBar.setVisibility(View.VISIBLE);
        if (swipeLayout.isRefreshing()) {
            swipeLayout.setRefreshing(false);
        }

        if (ConnectionDetector.isConnectingToInternet(this)) {
            String filename = null;

            if (!TextUtils.isEmpty(currentDate)) {
                filename = "jx-edtdetails/xml/" + stateName + "/" + DateUtility.getDate() + "/";
            } else {
                filename = "jx-edtdetails/xml/" + stateName + "/" + DateUtility.setDateForAll + "/";
            }

            if (!TextUtils.isEmpty(searchDate)) {
                filename = "jx-edtdetails/xml/" + stateName + "/" + searchDate + "/";
            }

            Log.d("filename", "=>" + filename);


            String url = Urls.BASE_URL + filename;
            StringRequest request = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String res) {
                    setXmlParsingMaster(res);
                    progressBar.setVisibility(View.GONE);
                    if (swipeLayout.isRefreshing()) {
                        swipeLayout.setRefreshing(false);
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_SHORT).show();
                    swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            callXmlsetter();
                        }
                    });

                    if (swipeLayout.isRefreshing()) {
                        swipeLayout.setRefreshing(false);
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(EditionDetailActivity.this).addToRequestQueue(request);
        } else {
            progressBar.setVisibility(View.GONE);
            if (swipeLayout.isRefreshing()) {
                swipeLayout.setRefreshing(false);
            }

            Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void setXmlParsingMaster(String xmlres) {
        XMLParser xmlparser = new XMLParser();
        Document doc = xmlparser.getDomElement(xmlres);
        NodeList nl = doc.getElementsByTagName("data");
        List<MasterPageModel> stateEditionList = new ArrayList<>();
        for (int i = 0; i < nl.getLength(); i++) {
            Element e = (Element) nl.item(i);
            String editioncode = xmlparser.getValue(e, "editioncode"); // name child value
            String pageno = xmlparser.getValue(e, "pageno"); // name child value
            String page = xmlparser.getValue(e, "page"); // cost child value
            String priorty = xmlparser.getValue(e, "priorty"); // editionname child value
            String imagepath = xmlparser.getValue(e, "imagepath"); // editionname child value
            String imagethumb = xmlparser.getValue(e, "imagethumb"); // editionname child value
            String imagelarge = xmlparser.getValue(e, "imagelarge"); // editionname child value
//            Log.e("descriptiondata", "=>" + description + "---" + imagepath);
            stateEditionList.add(new MasterPageModel(editioncode, pageno, page, priorty, imagepath, imagethumb, imagelarge, description));
        }
        setAdapter(stateEditionList);
    }

    private void setAdapter(List<MasterPageModel> stateEditionList) {
        int columno = Utility.calculateNoOfColumns(EditionDetailActivity.this);
        homeRecyclerList.setLayoutManager(new GridLayoutManager(this, columno));
        List<MasterPageModel> filterEdition = new ArrayList<>();

        for (MasterPageModel pgm : stateEditionList) {
            Log.d("Edition", pgm.editioncode + "===" + geteditioncode + "===" + pgm.description + "===" + description);
            if (pgm.editioncode.equals(geteditioncode)) {
                filterEdition.add(pgm);
            }
        }

        MyCustomDialog.listdata = filterEdition;
//        homeRecyclerList.setAdapter(new DetailAdapter(this, filterEdition));
        homeRecyclerList.setItemAnimator(new DefaultItemAnimator());
        Intent intent = new Intent(EditionDetailActivity.this, DetailSwipeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("editionname", description);
        intent.putExtra("position", 0);
        intent.putParcelableArrayListExtra("searchList", searchList);
        intent.putParcelableArrayListExtra("pages", listdata);
        intent.putExtra("state", stateName);
        intent.putExtra("cityname", cityName);

        startActivity(intent);
//        overridePendingTransition(0, 0);
        finish();
    }

}
