package com.bhaskar.epaper.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.bhaskar.epaper.R;

public class EpaperMainActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_epaper_main);

        EpaperSplashActivity.getInstance(this).startEpaperInitialization(null, "", "", "", "", new EpaperSplashActivity.OnFetchListener() {
            @Override
            public void onFetchDone() {
                finish();
            }
        });
    }
}
