package com.bhaskar.epaper.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.homeutils.HomeContent;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.PreferncesConstant;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.epaper.utils.XMLParser;
import com.google.android.gms.analytics.Tracker;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EpaperSplashActivity {

    public interface OnFetchListener {
        void onFetchDone();
    }

    private OnFetchListener onFetchListener;

    private final static String DEFAULT_WS = "jx-default/xml/";
    private static EpaperSplashActivity epaperSplashActivity;

    private String setDate = "";
    private EpaperPrefernces pref;
    private boolean mCurrDate = true;
    private Context context;
    private ProgressDialog progressDialog;
    private OnChangeDateListener onChangeDateListener;

    Epaper mApp;
    public static String getDate = null;
    public String modulePrefix, source, medium, campaign;
    public Tracker defaultTracker;

    public EpaperSplashActivity(Context context) {
        this.context = context;
        progressDialog = generateProgressDialog(context, true);
    }

    public static EpaperSplashActivity getInstance(Context context) {
        if (epaperSplashActivity == null) {
            synchronized (EpaperSplashActivity.class) {
                if (epaperSplashActivity == null)
                    epaperSplashActivity = new EpaperSplashActivity(context);
            }
        }
        return epaperSplashActivity;
    }

    private void getDefaultDate(final String date) {
        if (ConnectionDetector.isConnectingToInternet(context)) {
            String url = Urls.BASE_URL + date;
            StringRequest request = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String res) {
                    try {
                        if (date.equals(DEFAULT_WS)) {
                            setDateXmlParsing(res, null);
                        } else {
                            setDateXmlParsing(res, DEFAULT_WS);
                        }
                    } catch (Exception ignored) {
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    showProgress(false);
                    if (error.networkResponse != null && error.networkResponse.statusCode == 404) {
                        String preDate = getPreviousDate();
                        pref.setStringValue(PreferncesConstant.setDateAsDefult, preDate);
                        getDefaultDate(DEFAULT_WS + preDate + "/");
                    } else {
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(request);
        } else {
            Utility.setNetworkErrorToast(context);
        }
    }

    public void changeDateLoad(String date, OnChangeDateListener onChangeDateListener) {
        this.onChangeDateListener = onChangeDateListener;
        if (!TextUtils.isEmpty(date)) {
            setDate = getDate = date;
            mCurrDate = false;
            pref.setStringValue(PreferncesConstant.setDateAsMax, setDate);
            pref.setStringValue(PreferncesConstant.setDateAsDefult, getDate);
            if (TextUtils.isEmpty(pref.getStringValue(PreferncesConstant.setAsDefaultData, ""))) {
                pref.setStringValue(PreferncesConstant.setAsDefaultData, PreferncesConstant.defaultStateForEPaper);
            }
            getDefaultDate(DEFAULT_WS + date);
        }
    }

    private void showProgress(boolean show) {
        try {
            if (show) {
                if (progressDialog == null)
                    progressDialog = generateProgressDialog(context, false);
                progressDialog.show();
            } else {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        } catch (Exception ignored) {
        }
    }

    private void setDateXmlParsing(String xmlres, @Nullable final String chkDate) {
        XMLParser xmlparser = new XMLParser();
        Document doc = xmlparser.getDomElement(xmlres);
        NodeList nl = doc.getElementsByTagName("data");
        if (null == chkDate) {
            Element e = (Element) nl.item(0);
            setDate = xmlparser.getValue(e, "date"); // name child value
            getDate = setDate; // name child value
            DateUtility.setDateForAll = setDate;/*get the previous date*/
            pref.setStringValue(PreferncesConstant.setDateAsDefult, setDate);
            pref.setStringValue(PreferncesConstant.setDateAsMax, setDate);
            String wsURL = DEFAULT_WS + getDate + "/";
            getDefaultDate(wsURL);

        } else {
            pref.setStringValue(PreferncesConstant.setDateAsDefult, setDate);
            NodeList state = doc.getElementsByTagName("state");
            Constant.state.clear();
            if (state.getLength() > 0) {
                for (int i = 0; i < state.getLength(); i++) {
                    Element e1 = (Element) state.item(i);
                    String statename = xmlparser.getValue(e1, "statename");
                    Constant.state.add(statename);
                }
                String setAsDefaultString = pref.getStringValue(PreferncesConstant.setAsDefaultData, "");
                if (Constant.state != null && Constant.state.size() > 0 && !TextUtils.isEmpty(setAsDefaultString)) {
                    if (Constant.state.contains(setAsDefaultString)) {
                        Constant.state.remove(setAsDefaultString);
                        Constant.state.add(0, setAsDefaultString);
                    }
                }
                DateUtility.setDateForAll = setDate;
//                showProgress(false);
                startMasterActivity();

            } else {
                String preDate = getPreviousDate();
                pref.setStringValue(PreferncesConstant.setDateAsDefult, preDate);
                getDefaultDate(DEFAULT_WS + preDate + "/");
            }
        }
    }

    private String getPreviousDate() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return dateFormatter.format(cal.getTime());
    }

    private void startMasterActivity() {
        Intent intent = new Intent(context, MasterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        context.startActivity(intent);

        if (onFetchListener != null) {
            onFetchListener.onFetchDone();
        }
    }

    public void startEpaperInitialization(Tracker defaultTracker, String modulePrefix, String source, String medium, String campaign, OnFetchListener onFetchListener) {
        mCurrDate = true;
//        showProgress(true);
        this.defaultTracker = defaultTracker;
        this.modulePrefix = modulePrefix;
        this.source = source;
        this.medium = medium;
        this.campaign = campaign;
        this.onFetchListener = onFetchListener;
        onChangeDateListener = null;
        pref = EpaperPrefernces.getInstance(context);
        mApp = Epaper.getInstance();
        if (TextUtils.isEmpty(pref.getStringValue(PreferncesConstant.setAsDefaultData, ""))) {
            pref.setStringValue(PreferncesConstant.setAsDefaultData, PreferncesConstant.defaultStateForEPaper);
        }
        HomeContent.listMain.clear();

        /*default date is current date*/
        if (mCurrDate) {
            getDefaultDate(DEFAULT_WS);
        } else {
            String date = DEFAULT_WS + getDate + "/";
            pref.setStringValue(PreferncesConstant.setDateAsDefult, getDate);
            getDefaultDate(date);
        }
    }

    public ProgressDialog generateProgressDialog(Context context, boolean cancelable) {
        ProgressDialog progressDialog = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            progressDialog = new ProgressDialog(context, R.style.ProgressThemeLogin);
        else
            progressDialog = new ProgressDialog(context, R.style.AlertDialog_Holo_Login);
        //progressDialog.setMessage("Loading..");
        progressDialog.setCancelable(cancelable);
        return progressDialog;
    }
}
