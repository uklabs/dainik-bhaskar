package com.bhaskar.epaper.ui;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.my_cropper.CropImageView;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtility;
import com.bhaskar.epaper.utils.PreferncesConstant;
import com.bhaskar.epaper.utils.Utility;

public class ImageCropperActivity extends BaseActivity {

    public static Bitmap imagebitmap;
    CropImageView cropImageView;
    ImageView setCropperImage;
    Button crop_btn;
    Button share_btn;
    Button save_btn;
    Button reset_btn;
    RelativeLayout fullRelativeImage;
    HorizontalScrollView scrollView;
    ScrollView layRelViewHolder;
    LinearLayout bottom_ll;
    RelativeLayout relativeImage;
    TextView editionDetailTxt;
    ImageView logo_banner;
    String imageUrl;
    String editionname;
    String description;
    String pageno;
    String state;
    String imgpath;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_cropper);
        initViews();
        imageUrl = getIntent().getStringExtra("largeUrl");
        System.out.print("Image URL " + imageUrl);
        editionname = getIntent().getStringExtra("editionname");
        description = getIntent().getStringExtra("description");
        pageno = getIntent().getStringExtra("pageno");
        imgpath = getIntent().getStringExtra("imgpath");

        String setasDate = EpaperPrefernces.getInstance(ImageCropperActivity.this).getStringValue(PreferncesConstant.setDateAsDefult, "");
        editionDetailTxt.setText(DateUtility.parseDateToddMMyyyy(setasDate) + "\n" + description);
        if (!TextUtils.isEmpty(imgpath)) {
            if (imgpath.startsWith("http://digitalimages.bhaskar.com/gujarat") || imgpath.contains("/gujarat/")) {
                logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.divyabhaskar_logo));
            } else if (imgpath.startsWith("http://digitalimages.bhaskar.com/divyamarathi") || imgpath.contains("/divyamarathi/")) {
                logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.divyamarathi_logo));
            } else {
                logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.dainikbhaskar_logo));
            }
        }

        setToolbar();

        if (TextUtils.isEmpty(imageUrl)) {
            try {
                cropImageView.setImageBitmap(imagebitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
            setCropperImage.setImageBitmap(imagebitmap);
            setAccessVisibility(false);
        } else {
            cropImageView.setVisibility(View.GONE);
            setCropperImage.setVisibility(View.GONE);

            Log.i("imageUrl", "=>" + imageUrl);
            cropImageView.setImageUriAsync(Uri.parse(imageUrl));
            cropImageView.setVisibility(View.VISIBLE);
            setAccessVisibility(false);
        }

        crop_btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            relativeImage.setVisibility(View.VISIBLE);
                            fullRelativeImage.setVisibility(View.GONE);
                            setAccessVisibility(true);

                        } catch (Exception e) {
                            Utility.setToast(ImageCropperActivity.this, "Please Work Properly");
                            e.printStackTrace();
                        }
                    }
                });

        reset_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setCropperImage.setVisibility(View.GONE);
                cropImageView.setVisibility(View.VISIBLE);
                fullRelativeImage.setVisibility(View.VISIBLE);
                relativeImage.setVisibility(View.GONE);

                try {
                    cropImageView.setImageBitmap(imagebitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                setAccessVisibility(false);
            }
        });

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layRelViewHolder.setVisibility(View.GONE);
//                adview.setVisibility(View.GONE);
                bottom_ll.setVisibility(View.GONE);
                //scrollView.setVisibility(View.GONE);
                Bitmap bt = ImageUtility.getBitmapFromView(relativeImage);
                ImageUtility.saveImage(bt, ImageCropperActivity.this);

                layRelViewHolder.setVisibility(View.VISIBLE);
//                adview.setVisibility(View.VISIBLE);
                bottom_ll.setVisibility(View.VISIBLE);
                //scrollView.setVisibility(View.VISIBLE);
            }
        });

        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bt = ImageUtility.getBitmapFromView(relativeImage);
                ImageUtility.saveSharedImage(bt, ImageCropperActivity.this, description);
            }
        });
    }

    private void initViews() {
        cropImageView = findViewById(R.id.cropImageView);
        setCropperImage = findViewById(R.id.setCropperImage);
        crop_btn = findViewById(R.id.crop_activity_btn);
        share_btn = findViewById(R.id.share_activity_btn);
        save_btn = findViewById(R.id.save_activity_btn);
        reset_btn = findViewById(R.id.reset_activity_btn);
        fullRelativeImage = findViewById(R.id.fullRelative_image);

        scrollView = findViewById(R.id.scrollView);
        layRelViewHolder = findViewById(R.id.layRelViewHolder);
        bottom_ll = findViewById(R.id.bottom_ll);
        relativeImage = findViewById(R.id.relative_image);
        editionDetailTxt = findViewById(R.id.edition_detail_txt);
        logo_banner = findViewById(R.id.logo_banner);

    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_action_bar_back);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowHomeEnabled(true);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeAsUpIndicator(upArrow);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    public void setAccessVisibility(Boolean flag) {

        if (flag) {
            save_btn.setVisibility(View.VISIBLE);
            share_btn.setVisibility(View.VISIBLE);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    2.0f
            );

            crop_btn.setLayoutParams(layoutParams);
            reset_btn.setLayoutParams(layoutParams);
            save_btn.setLayoutParams(layoutParams);
            share_btn.setLayoutParams(layoutParams);
            cropImageView.setVisibility(View.VISIBLE);
            setCropperImage.setVisibility(View.VISIBLE);
            setCropperImage.setImageBitmap(cropImageView.getCroppedImage());
        } else {

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    4.0f
            );

            crop_btn.setLayoutParams(layoutParams);
            reset_btn.setLayoutParams(layoutParams);

            share_btn.setVisibility(View.GONE);
            save_btn.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Epaper.getInstance().trackScreenView("ImageCropperActivity");
    }
}
