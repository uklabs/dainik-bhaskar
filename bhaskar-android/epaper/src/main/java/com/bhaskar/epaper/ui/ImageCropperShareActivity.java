package com.bhaskar.epaper.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.my_cropper.CropImageView;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtility;
import com.bhaskar.epaper.utils.PreferncesConstant;
import com.bhaskar.epaper.utils.Utility;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageCropperShareActivity extends BaseActivity {

    public static Bitmap imagebitmap;
    public static boolean isShowTime = false;
    CropImageView cropImageView;
    ImageView setCropperImage;
    Button crop_btn;
    Button share_btn;
    Button save_btn;
    Button reset_btn;
    RelativeLayout fullRelativeImage;
    HorizontalScrollView scrollView;
    ScrollView layRelViewHolder;
    LinearLayout bottom_ll;
    RelativeLayout relativeImage;
    TextView editionDetailTxt;
    ImageView logo_banner;
    EditText edtComment;
    LinearLayout layCommnetContainer;
    String imageUrl;
    String editionname;
    String editioncode;
    String description;
    String pageno;
    String state;
    String imgpath;
    ImageCropperShareActivity mActivity;
    ProgressDialog progressDialog;
    private Epaper mApp;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_cropper_share);
        mActivity = this;

        mApp = Epaper.getInstance();
        initViews();
        System.out.print("Image URL " + imageUrl);

        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(false);

        layCommnetContainer.bringToFront();
        edtComment.setSelected(false);

        imageUrl = getIntent().getStringExtra("largeUrl");
        editionname = getIntent().hasExtra("editionname") ? getIntent().getStringExtra("editionname") : "";
        editioncode = getIntent().hasExtra("editioncode") ? getIntent().getStringExtra("editioncode") : "";
        description = getIntent().getStringExtra("description");
        pageno = getIntent().getStringExtra("pageno");
        imgpath = getIntent().getStringExtra("imgpath");

        Toast.makeText(this, "editionname :" + editionname + " , editioncode:" + editioncode, Toast.LENGTH_LONG).show();

        String setasDate = EpaperPrefernces.getInstance(mActivity).getStringValue(PreferncesConstant.setDateAsDefult, "");
        editionDetailTxt.setText(DateUtility.parseDateToddMMyyyy(setasDate) + "\n" + description);

//        if (imgpath != null) {
//            if (imgpath.startsWith("http://digitalimages.bhaskar.com/gujarat")) {
        logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.divyabhaskar_logo));
//            } else if (imgpath.startsWith("http://digitalimages.bhaskar.com/divyamarathi")) {
//                logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.divya_logo));
//            } else {
//                logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.dainik_logo));
//            }
//        }

        setToolbar();

//        final AdUtility adUtility = new AdUtility(this);
//
//        adUtility.loadBannerAd(adview);

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                adUtility.loadInterstitial();
//            }
//        }, 72000);

        if (TextUtils.isEmpty(imageUrl)) {
            try {
                cropImageView.setImageBitmap(imagebitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
            setCropperImage.setImageBitmap(imagebitmap);
            setAccessVisibility(false);
        } else {

            cropImageView.setVisibility(View.GONE);
            setCropperImage.setVisibility(View.GONE);
            Log.i("imageUrl", "=>" + imageUrl);
            cropImageView.setImageUriAsync(Uri.parse(imageUrl));
            cropImageView.setVisibility(View.VISIBLE);
            setAccessVisibility(false);

        }


        crop_btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {

                            relativeImage.setVisibility(View.VISIBLE);
                            fullRelativeImage.setVisibility(View.GONE);
                            setAccessVisibility(true);

                        } catch (Exception e) {

                            Utility.setToast(mActivity, "Please Work Properly");
                            e.printStackTrace();

                        }


                    }
                });


        reset_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setCropperImage.setVisibility(View.GONE);
                cropImageView.setVisibility(View.VISIBLE);
                fullRelativeImage.setVisibility(View.VISIBLE);
                relativeImage.setVisibility(View.GONE);

                try {
                    cropImageView.setImageBitmap(imagebitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                setAccessVisibility(false);
            }
        });

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layRelViewHolder.setVisibility(View.GONE);
//                adview.setVisibility(View.GONE);
                bottom_ll.setVisibility(View.GONE);
                //scrollView.setVisibility(View.GONE);
                Bitmap bt = ImageUtility.getBitmapFromView(relativeImage);
                ImageUtility.saveImage(bt, mActivity);

                layRelViewHolder.setVisibility(View.VISIBLE);
//                adview.setVisibility(View.VISIBLE);
                bottom_ll.setVisibility(View.VISIBLE);
                String comments = edtComment.getText().toString();


                setBestStory(comments, editioncode, editionname, imageUrl, getBitmapToFilePath(bt));


                //scrollView.setVisibility(View.VISIBLE);
            }
        });

        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bt = ImageUtility.getBitmapFromView(relativeImage);
                ImageUtility.saveSharedImage(bt, mActivity, description);
            }
        });

    }

    private void initViews() {
        cropImageView = findViewById(R.id.cropImageView);
        setCropperImage = findViewById(R.id.setCropperImage);
        crop_btn = findViewById(R.id.crop_activity_btn);
        share_btn = findViewById(R.id.share_activity_btn);
        save_btn = findViewById(R.id.save_activity_btn);
        reset_btn = findViewById(R.id.reset_activity_btn);
        fullRelativeImage = findViewById(R.id.fullRelative_image);

        scrollView = findViewById(R.id.scrollView);
        layRelViewHolder = findViewById(R.id.layRelViewHolder);
        bottom_ll = findViewById(R.id.bottom_ll);
        relativeImage = findViewById(R.id.relative_image);
        editionDetailTxt = findViewById(R.id.edition_detail_txt);
        logo_banner = findViewById(R.id.logo_banner);
        edtComment = findViewById(R.id.edtComment);
        layCommnetContainer = findViewById(R.id.layCommnetContainer);

    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("E-Paper");
    }


    public void setAccessVisibility(Boolean flag) {


        if (flag) {

            save_btn.setVisibility(View.VISIBLE);
            share_btn.setVisibility(View.VISIBLE);
            layCommnetContainer.setVisibility(View.VISIBLE);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    2.0f
            );

            crop_btn.setLayoutParams(layoutParams);
            reset_btn.setLayoutParams(layoutParams);
            save_btn.setLayoutParams(layoutParams);
            share_btn.setLayoutParams(layoutParams);
            cropImageView.setVisibility(View.VISIBLE);
            setCropperImage.setVisibility(View.VISIBLE);
            setCropperImage.setImageBitmap(cropImageView.getCroppedImage());


        } else {

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    4.0f
            );

            crop_btn.setLayoutParams(layoutParams);
            reset_btn.setLayoutParams(layoutParams);
            layCommnetContainer.setVisibility(View.GONE);

            share_btn.setVisibility(View.GONE);
            save_btn.setVisibility(View.GONE);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
//        Epaper.getInstance().trackScreenView("ImageCropperActivity");
    }


    private String getBitmapToFilePath(Bitmap bitmap) {

        String path = "";
        File myDir;

        if (ImageUtility.hasStorage(true)) {

            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            myDir = new File(root + "/" + getString(R.string.foldername));

        } else {

            ContextWrapper cw = new ContextWrapper(this);
            myDir = cw.getDir(getString(R.string.foldername), Context.MODE_PRIVATE);
        }

        myDir.mkdirs();

        Random generator = new Random();
        int n = 100000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);

        if (file.exists())
            file.delete();

        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            //ImageUtility.galleryAddPic(file.getAbsolutePath(), this);
            out.flush();
            out.close();
            path = file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }


    /*private void setBestStoryOld(String comment, String edition_code, String edition_name, String img_org_path, String filePath) {
        Log.d("filePath", filePath);
        final File imageFile = new File(filePath);
        RequestBody typedFile;
        if (imageFile != null) {
            Log.i("sendfile", "=>" + imageFile.getAbsolutePath());
            try {

                //typedFile = new TypedFile("multipart/form-data", new File(imageFile.getAbsolutePath()));
                typedFile = RequestBody.create(MediaType.parse("image/*"), imageFile.getAbsolutePath());
                progressDialog.show();
                mApp.getServices().setBestStory(comment, edition_code, edition_name, img_org_path, typedFile, new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        if (response.body() != null) {
                            Log.d("setBestStory", response.toString());
                            Toast.makeText(mActivity, response.toString(), Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                        if (imageFile.exists()) {
                            imageFile.delete();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        Toast.makeText(mActivity, t.getMessage(), Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                        if (imageFile.exists()) {
                            imageFile.delete();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }*/

    private void setBestStory(String comment, String edition_code, String edition_name, String img_org_path, String filePath) {


        Log.d("filePath", filePath);
        final File imageFile = new File(filePath);
        Log.i("sendfile", "=>" + imageFile.getAbsolutePath());
        try {
            progressDialog.show();
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), imageFile.getAbsolutePath());
            Call<JSONObject> call = mApp.getServices().setBestStory(comment, edition_code, edition_name, img_org_path, requestBody);
            call.enqueue(new Callback<JSONObject>() {
                @Override
                public void onResponse(@NonNull Call<JSONObject> call, @NonNull Response<JSONObject> response) {
                    if (response.body() != null) {
                        Log.d("setBestStory", response.toString());
                        Toast.makeText(mActivity, response.toString(), Toast.LENGTH_LONG).show();
                    }
                    progressDialog.dismiss();
                    if (imageFile.exists()) {
                        imageFile.delete();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<JSONObject> call, @NonNull Throwable t) {
                    Toast.makeText(mActivity, t.getMessage(), Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                    if (imageFile.exists()) {
                        imageFile.delete();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
