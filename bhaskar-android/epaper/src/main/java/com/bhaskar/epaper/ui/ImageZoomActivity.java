package com.bhaskar.epaper.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.imagezoom.ImageViewTouch;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtility;
import com.bhaskar.epaper.utils.SerializeData;
import com.bhaskar.epaper.utils.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.bhaskar.epaper.fab.FloatingActionButton;

public class ImageZoomActivity extends BaseActivity {

    public static MasterModel clickModel;
    FloatingActionButton crop_menu_btn;
    FloatingActionButton save_menu_btn;
    FloatingActionButton share_menu_btn;
    FloatingActionButton book_menu_btn;
    Epaper mApp;
    EpaperPrefernces mPref;
    String imageUrl, editioncode, editionname;
    Bitmap imagebitmap = null;
    ImageViewTouch image;
    Target mTarget;

    private void initViews() {
        crop_menu_btn = findViewById(R.id.crop_menu_btn);
        book_menu_btn = findViewById(R.id.book_menu_btn);
        share_menu_btn = findViewById(R.id.share_menu_btn);
        save_menu_btn = findViewById(R.id.save_menu_btn);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagezoom);
        initViews();
        setToolbar();

        imageUrl = getIntent().getStringExtra("largeUrl");

        editioncode = getIntent().getStringExtra("editioncode");
        editionname = getIntent().getStringExtra("editionname");

//        imageUrl = "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSpQgSz-aG4cX1EmyuBcKQKokszgyBPJmS_OHZMZXObGC6eZDrItw";
        mPref = EpaperPrefernces.getInstance(ImageZoomActivity.this);

        image = (ImageViewTouch) findViewById(R.id.pager_imageview);

//        book_menu_btn.setVisibility(View.GONE);

        String url = imageUrl.replace(" ", "%20");
//        Log.e("Zoom activity 1", url);
        loadImage(url);

        book_menu_btn.setVisibility(View.GONE);


        crop_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebitmap != null) {

                    if (ImageCropperShareActivity.isShowTime) {
                        ImageCropperShareActivity.imagebitmap = imagebitmap;
                        Intent intent = new Intent(ImageZoomActivity.this, ImageCropperShareActivity.class);
                        intent.putExtra("imgpath", imageUrl);
                        intent.putExtra("editioncode", editioncode);
                        intent.putExtra("editionname", editionname);
                        startActivity(intent);
                    } else {
                        ImageCropperActivity.imagebitmap = imagebitmap;
                        Intent intent = new Intent(ImageZoomActivity.this, ImageCropperActivity.class);
                        intent.putExtra("imgpath", imageUrl);
                        startActivity(intent);
                    }

                    //startActivity(new Intent(ImageZoomActivity.this, ImageCropperActivity.class));
                } else {
                    Utility.setToast(ImageZoomActivity.this, "Please Wait For Image Download");
                }
            }
        });

        save_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebitmap != null) {
//                    ImageUtility.saveImage(ImageCropperActivity.imagebitmap, ImageZoomActivity.this);
                    ImageUtility.saveImage(imagebitmap, ImageZoomActivity.this);
                } else {
                    Utility.setToast(ImageZoomActivity.this, "Please Wait For Image Download");
                }
            }
        });

        share_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebitmap != null) {
//                    ImageUtility.saveSharedImage(ImageCropperActivity.imagebitmap, ImageZoomActivity.this);
                    ImageUtility.saveSharedImage(imagebitmap, ImageZoomActivity.this);
                } else {
                    Utility.setToast(ImageZoomActivity.this, "Please Wait For Image Download");
                }

            }
        });

        book_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickModel != null) {
                    removeBookMark(clickModel.imagepath);
                }


            }
        });
    }

    public void loadImage(String imgUrl) {

        mTarget=new SimpleTarget<Bitmap>() {

            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                image.setImageBitmap(resource);
                ImageCropperActivity.imagebitmap = resource;
                imagebitmap = resource;
            }

            @Override
            public void onLoadStarted(@Nullable Drawable placeholder) {
                super.onLoadStarted(placeholder);

                ImageCropperActivity.imagebitmap = null;
                imagebitmap = null;
                image.setImageDrawable(getResources().getDrawable(R.drawable.img));
            }

            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                ImageCropperActivity.imagebitmap = null;
                imagebitmap = null;
            }
        };

        Glide.with(this)
                .load(imgUrl)
                .into(mTarget);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_action_bar_back);
//        upArrow.setColorFilter(getResources().getColor(R.color.md_grey_100), PorterDuff.Mode.SRC_ATOP);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    public void removeBookMark(String imagePath) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please Wait ..");
        pd.show();

        if (!SerializeData.getInstance(ImageZoomActivity.this).isBookmarkAvailable(imagePath)) {
            SerializeData.getInstance(ImageZoomActivity.this).removeBookmark(imagePath);
        }
    }
}
