package com.bhaskar.epaper.ui;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MagazinePageDetailAdapter;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.appscommon.tracking.Tracking;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MagazineDetailActivity extends ParentActivity {
    public static final int LOGIN_REQUEST_CODE = 1010;
    public static final int SUBSCRIBE_REQUEST_CODE = 1020;
    private ProgressBar progress;
    private ViewPager viewpager;
    private TabLayout tabs;
    private ImageView home_icon;
    private String editioncode, editiondate, magazine, magazineLang,magazineName;
    private EpaperPrefernces pref;
    private MagazinePageDetailAdapter magazinePageDetailAdapter;
    private void initViews() {
        home_icon = findViewById(R.id.home_icon);
        tabs = findViewById(R.id.tabs);
        viewpager = findViewById(R.id.dailog_viewpager);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                String gaScreen= EpaperSplashActivity.getInstance(MagazineDetailActivity.this).modulePrefix+"-"+MagazineMasterActivity.magazinePrefix+"-"+magazineLang+"-"+magazineName+"-PAGE"+(position+1);
                Tracking.trackGAScreen(MagazineDetailActivity.this, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).defaultTracker, gaScreen, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).source, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).medium, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).campaign);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        progress = findViewById(R.id.progress);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_swipe);
        initViews();
        //        setToolbar();
        progress.setVisibility(View.VISIBLE);
//        new AdUtility(this).loadBannerAd(adView);
        editioncode = getIntent().getStringExtra("editioncode");
        editiondate = getIntent().getStringExtra("editiondate");
        magazine = getIntent().getStringExtra("magazine");
        magazineLang = getIntent().getStringExtra("magazinelang");
        magazineName = getIntent().getStringExtra("magazinename");
//        Log.e("EditionCode", "=>" + editioncode + "editiondate" + editiondate);

        pref = EpaperPrefernces.getInstance(MagazineDetailActivity.this);
        if (ConnectionDetector.isConnectingToInternet(this)) {
            getDetailMagazineData();
        } else {
            Utility.setNetworkErrorToast(this);
        }

        home_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EpaperPrefernces.getInstance(getApplicationContext()).setHome();
            }
        });
    }

    private void getDetailMagazineData() {
        Call<ArrayList<MagazineMasterModel>> call = mApp.getServices().getMagazineDetailData();
        call.enqueue(new Callback<ArrayList<MagazineMasterModel>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<MagazineMasterModel>> call, @NonNull Response<ArrayList<MagazineMasterModel>> response) {
                progress.setVisibility(View.GONE);
                Type listType = new TypeToken<List<MagazineMasterModel>>() {
                }.getType();
                List<MagazineMasterModel> magazinemodel = response.body();
                List<MagazineMasterModel> MagazineDetailList = new ArrayList<>();
                for (MagazineMasterModel mmm : magazinemodel) {
                    if (mmm.getEditioncode().equals(editioncode) && mmm.getPagedate().equals(editiondate)) {
                        MagazineDetailList.add(mmm);
//                        Log.e("magazine_Detail", "=>" + mmm.getDescription() + "====" + mmm.getPagedate());
                    }
                }
                magazinePageDetailAdapter=new MagazinePageDetailAdapter(getSupportFragmentManager(), MagazineDetailList);
                viewpager.setAdapter(magazinePageDetailAdapter);
                tabs.setupWithViewPager(viewpager);

                if(magazinePageDetailAdapter.getCount()>0){
                    String gaScreen= EpaperSplashActivity.getInstance(MagazineDetailActivity.this).modulePrefix+"-"+MagazineMasterActivity.magazinePrefix+"-"+magazineLang+"-"+magazineName+"-PAGE1";
                    Tracking.trackGAScreen(MagazineDetailActivity.this, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).defaultTracker, gaScreen, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).source, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).medium, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).campaign);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<MagazineMasterModel>> call, @NonNull Throwable t) {
                Utility.setToast(MagazineDetailActivity.this, t.getMessage());
            }
        });

    }

}
