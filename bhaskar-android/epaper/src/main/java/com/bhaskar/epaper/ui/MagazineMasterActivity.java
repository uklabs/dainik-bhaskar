package com.bhaskar.epaper.ui;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ProgressBar;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MagazinePagerAdapter;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.appscommon.tracking.Tracking;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MagazineMasterActivity extends ParentActivity {

    private ViewPager magazine_viewpager;
    private TabLayout tabs;
    private ProgressBar progress;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static final String magazinePrefix= "MAGAZINE";
    MagazinePagerAdapter magazinePagerAdapter;

    private void initViews() {
        magazine_viewpager = findViewById(R.id.magazine_viewpager);

        magazine_viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
//                if (isShowSubscripionLoginDialog() && position > 0)
//                    showSubscriptionAndLoginDialog();
                Tracking.trackGAScreen(MagazineMasterActivity.this, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).defaultTracker, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).modulePrefix+"-"+magazinePrefix+"-"+magazinePagerAdapter.getPageTitle(position), EpaperSplashActivity.getInstance(MagazineMasterActivity.this).source, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).medium, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).campaign);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        tabs = findViewById(R.id.tabs);
        progress = findViewById(R.id.progress);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magazine);
        initViews();

        setToolbar();
        if (ConnectionDetector.isConnectingToInternet(this)) {
            getMasterMagazineData();
        } else {
            Utility.setNetworkErrorToast(this);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (ConnectionDetector.isConnectingToInternet(MagazineMasterActivity.this)) {
                    getMasterMagazineData();
                } else {
                    Utility.setNetworkErrorToast(MagazineMasterActivity.this);
                }
            }
        });
    }

    public void getMasterMagazineData() {
        Call<ArrayList<MagazineMasterModel>> call = mApp.getServices().getMagazineMasterData();
        call.enqueue(new Callback<ArrayList<MagazineMasterModel>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<MagazineMasterModel>> call, @NonNull Response<ArrayList<MagazineMasterModel>> response) {

                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                progress.setVisibility(View.GONE);
                List<MagazineMasterModel> magazinemodel = response.body();
                magazinePagerAdapter= new MagazinePagerAdapter(getSupportFragmentManager(), magazinemodel);
                magazine_viewpager.setAdapter(magazinePagerAdapter);
                tabs.setupWithViewPager(magazine_viewpager);
                if(magazinePagerAdapter.getCount()>0){
                    Tracking.trackGAScreen(MagazineMasterActivity.this, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).defaultTracker, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).modulePrefix+"-"+magazinePrefix+"-"+magazinePagerAdapter.getPageTitle(0), EpaperSplashActivity.getInstance(MagazineMasterActivity.this).source, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).medium, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).campaign);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MagazineMasterModel>> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}
