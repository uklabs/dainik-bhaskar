package com.bhaskar.epaper.ui;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MagazineRecyclerAdapter;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.appscommon.tracking.Tracking;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MagazineSubActivity extends ParentActivity {

    RecyclerView recyclerView;

    ProgressBar progress;

    SwipeRefreshLayout swipeLayout;

    String editioncode,magazineLang, magazineName;

    private void initViews() {
        swipeLayout = findViewById(R.id.swipeLayout);
        recyclerView = findViewById(R.id.homeRecyclerList);
        progress = findViewById(R.id.progress);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_master);
        initViews();
        setToolbar();
        int columno = Utility.calculateNoOfColumns(MagazineSubActivity.this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, columno));
        editioncode = getIntent().getStringExtra("editioncode");//"14";
        magazineLang = getIntent().getStringExtra("magazinelang");
        magazineName = getIntent().getStringExtra("magazinename");
//        new AdUtility(MagazineSubActivity.this).loadInterstitial();
        getSubMagazineDetail();

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSubMagazineDetail();
            }
        });

    }

    public void getSubMagazineDetail() {
        Call<ArrayList<MagazineMasterModel>> call = mApp.getServices().getMagazineSubMasterData();
        call.enqueue(new Callback<ArrayList<MagazineMasterModel>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<MagazineMasterModel>> call, @NonNull Response<ArrayList<MagazineMasterModel>> response) {
                List<MagazineMasterModel> magazinemodel = response.body();
                setRecyclerView(magazinemodel);

                if (swipeLayout.isRefreshing()) {
                    swipeLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<MagazineMasterModel>> call, @NonNull Throwable t) {
                if (swipeLayout.isRefreshing()) {
                    swipeLayout.setRefreshing(false);
                }
                Utility.setNetworkErrorToast(MagazineSubActivity.this);
            }
        });

    }

    private void setRecyclerView(List<MagazineMasterModel> magazinemodel) {
        if (magazinemodel == null) {
            Utility.setToast(this, "No Data Found");
        } else {
            List<MagazineMasterModel> MagazineDetailList = new ArrayList<>();
            for (MagazineMasterModel mmm : magazinemodel) {
                if (mmm.getEditioncode().equals(editioncode)) {
                    MagazineDetailList.add(mmm);
//                    Log.e("Jsonresponse", "=>" + mmm.getMagazine());
                }
            }
            progress.setVisibility(View.GONE);
            recyclerView.setAdapter(new MagazineRecyclerAdapter(MagazineSubActivity.this, MagazineDetailList, magazineLang,false));

            String gaScreen= EpaperSplashActivity.getInstance(MagazineSubActivity.this).modulePrefix+"-"+ MagazineMasterActivity.magazinePrefix+"-"+magazineLang+"-"+magazineName;
            Tracking.trackGAScreen(MagazineSubActivity.this, EpaperSplashActivity.getInstance(MagazineSubActivity.this).defaultTracker, gaScreen, EpaperSplashActivity.getInstance(MagazineSubActivity.this).source, EpaperSplashActivity.getInstance(MagazineSubActivity.this).medium, EpaperSplashActivity.getInstance(MagazineSubActivity.this).campaign);
        }
    }

}
