package com.bhaskar.epaper.ui;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.HomeAdapter;
import com.bhaskar.epaper.adapter.MasterPageStateAdapter;
import com.bhaskar.epaper.homeutils.HomeContent;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.OnFeedFetchFromServerListener;
import com.bhaskar.epaper.utils.PreferncesConstant;
import com.bhaskar.appscommon.tracking.Tracking;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MasterActivity extends BaseActivity implements OnFeedFetchFromServerListener, OnChangeDateListener {

    private RecyclerView homeRecyclerList;
    private ProgressBar progressBar;
    private TextView ivMagazine;
    private ViewPager masterPageViewpager;
    private TabLayout masterPageTabs;
    private Toolbar toolbar;
    private EpaperPrefernces pref;
    private Epaper mApp;
    private MasterActivity mActivity;

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        masterPageTabs = findViewById(R.id.master_page_tabs);
        masterPageViewpager = findViewById(R.id.master_page_viewpager);
        ivMagazine = findViewById(R.id.ivMagazine);
        ivMagazine.setVisibility(View.VISIBLE);
        progressBar = findViewById(R.id.progress);
        homeRecyclerList = findViewById(R.id.homeRecyclerList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_main);

        mActivity = this;
        pref = EpaperPrefernces.getInstance(mActivity);
        mApp = Epaper.getInstance();
        setUp();
        progressBar.setVisibility(View.GONE);
    }

    private void setUp() {
        initViews();
        setToolbar();
        setHomeRecyclerData();
    }

    private void setToolbar() {
        toolbar.setTitleTextColor(getResources().getColor(R.color.md_black_1000));

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        final Drawable upArrow = getResources().getDrawable(R.drawable.eca_back);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
    }

    private void setHomeRecyclerData() {
        homeRecyclerList.setLayoutManager(new LinearLayoutManager(mActivity));
        ArrayList<String> keyList = new ArrayList<>(HomeContent.listMain.keySet());

        // to check whether state list has the data or not else the hit will be made in background thread
        if (Constant.state != null && Constant.state.size() > 0) {
            initPageAdapter();
        } else {
            EpaperSplashActivity.getInstance(MasterActivity.this).startEpaperInitialization(EpaperSplashActivity.getInstance(this).defaultTracker, EpaperSplashActivity.getInstance(MasterActivity.this).modulePrefix, EpaperSplashActivity.getInstance(this).source, EpaperSplashActivity.getInstance(this).medium, EpaperSplashActivity.getInstance(this).campaign, null);
        }

        homeRecyclerList.setAdapter(new HomeAdapter(mActivity, keyList, mApp, pref));
        ivMagazine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, MagazineMasterActivity.class));
            }
        });
    }

    MasterPageStateAdapter adapter;

    private void initPageAdapter() {
        adapter = new MasterPageStateAdapter(getSupportFragmentManager(), Constant.state);
        masterPageViewpager.setAdapter(adapter);
        masterPageTabs.setupWithViewPager(masterPageViewpager);
        masterPageViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Tracking.trackGAScreen(MasterActivity.this, EpaperSplashActivity.getInstance(MasterActivity.this).defaultTracker, EpaperSplashActivity.getInstance(MasterActivity.this).modulePrefix + "-" + String.valueOf(adapter.getPageTitle(position)), EpaperSplashActivity.getInstance(MasterActivity.this).source, EpaperSplashActivity.getInstance(MasterActivity.this).medium, EpaperSplashActivity.getInstance(MasterActivity.this).campaign);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        if (adapter.getCount() > 0)
            Tracking.trackGAScreen(MasterActivity.this, EpaperSplashActivity.getInstance(MasterActivity.this).defaultTracker, EpaperSplashActivity.getInstance(MasterActivity.this).modulePrefix + "-" + String.valueOf(adapter.getPageTitle(0)), EpaperSplashActivity.getInstance(MasterActivity.this).source, EpaperSplashActivity.getInstance(MasterActivity.this).medium, EpaperSplashActivity.getInstance(MasterActivity.this).campaign);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.date) {
            onDate();
        } else if (i == R.id.magazines) {
            onMagazines();
        } else if (i == R.id.edition) {
            onMyEditions();
        } else if (i == R.id.bookmarks) {
            onBookmarks();
        }
        return super.onOptionsItemSelected(item);
    }

    private void onMagazines() {
        Intent intent = new Intent(this, MagazineMasterActivity.class);
        startActivity(intent);
    }

    private void onDate() {
        callDatePicker();
    }

    public void callDatePicker() {
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        final Calendar calendarGlobal = Calendar.getInstance();

        DatePickerDialog dpd = new DatePickerDialog(mActivity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                calendarGlobal.set(Calendar.MONTH, monthOfYear);
                calendarGlobal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                calendarGlobal.set(Calendar.YEAR, year);
                String strDate = dateFormatter.format(calendarGlobal.getTime());
                EpaperSplashActivity.getInstance(MasterActivity.this).changeDateLoad(strDate, MasterActivity.this);

            }
        }, year, month, day);

        Calendar calNow = Calendar.getInstance();
        // adding -1 month
        calNow.add(Calendar.MONTH, -12);

        dpd.getDatePicker().setMinDate(calNow.getTimeInMillis());
        dpd.getDatePicker().setMaxDate(getMaxDate().getTime());

        dpd.show();
    }

    public Date getMaxDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            if (!TextUtils.isEmpty(pref.getStringValue(PreferncesConstant.setDateAsMax, ""))) {
                String currDate = pref.getStringValue(PreferncesConstant.setDateAsMax, "");
                Date date = simpleDateFormat.parse(currDate);
                return date;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Date();
    }

    private void onMyEditions() {
        startActivity(new Intent(mActivity, MyEditionActivity.class));
    }

    private void onBookmarks() {
        startActivity(new Intent(mActivity, MyBookmarkActivity.class));
    }

    @Override
    public void onDataFetch(boolean flag) {
        if (flag && Constant.state != null && Constant.state.size() > 0) {
            initPageAdapter();
        }
    }

    @Override
    public void onChangeDateDone() {
        finish();
        recreate();
    }
}
