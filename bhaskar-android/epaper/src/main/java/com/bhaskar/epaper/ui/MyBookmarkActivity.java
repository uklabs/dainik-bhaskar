package com.bhaskar.epaper.ui;

import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MyBookmarkAdapter;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.SerializeData;
import com.bhaskar.epaper.utils.Utility;

import java.util.List;


public class MyBookmarkActivity extends BaseActivity {

    RecyclerView recyclerView;

    Toolbar toolbar;

    ProgressBar progress;

    Epaper mApp;
    EpaperPrefernces mPref;


    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        progress = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.homeRecyclerList);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bookmark);
        initViews();        setToolbar();
        mApp = Epaper.getInstance();
        mPref = EpaperPrefernces.getInstance(MyBookmarkActivity.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(MyBookmarkActivity.this));
//        AdUtility adUtility = new AdUtility(this);
//        adUtility.loadBannerAd((AdView) findViewById(R.id.adView));
    }

    private void setRecyclerAdapter() {
        List<MasterModel> stateEditionList = SerializeData.getInstance(MyBookmarkActivity.this).getBookmarkList();
        if (stateEditionList != null && stateEditionList.size() > 0) {
            progress.setVisibility(View.GONE);
            int columno = Utility.calculateNoOfColumns(this);
            recyclerView.setLayoutManager(new GridLayoutManager(this, columno));
            recyclerView.setAdapter(new MyBookmarkAdapter(MyBookmarkActivity.this, stateEditionList));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
        } else {
            Utility.setToast(this, "No Bookmark Available");
        }
        progress.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRecyclerAdapter();
//        Epaper.getInstance().trackScreenView("MyBookmarkActivity");
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("My Bookmarks");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


}
