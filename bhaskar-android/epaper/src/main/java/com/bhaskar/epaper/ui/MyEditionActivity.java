package com.bhaskar.epaper.ui;

import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MyEditionAdapter;
import com.bhaskar.epaper.model.EditionModel;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.SerializeData;

import java.util.List;


public class MyEditionActivity extends BaseActivity {

    RecyclerView recyclerView;
    Toolbar toolbar;
    ProgressBar progress;

    Epaper mApp;
    EpaperPrefernces mPref;

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        progress = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.homeRecyclerList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bookmark);

        initViews();
        setToolbar();
        mApp = Epaper.getInstance();
        mPref = EpaperPrefernces.getInstance(MyEditionActivity.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(MyEditionActivity.this));

//        AdUtility adUtility = new AdUtility(this);
//        adUtility.loadBannerAd((AdView) findViewById(R.id.adView));

    }

    @Override
    protected void onResume() {
        super.onResume();
        setRecyclerAdapter();
//        Epaper.getInstance().trackScreenView("MyEditionActivity");
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyEditionActivity.this.finish();
            }
        });

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Edition");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void setRecyclerAdapter() {

        List<EditionModel> setEditionModelList = SerializeData.getInstance(MyEditionActivity.this).getMyEditionList();
        if (setEditionModelList != null && setEditionModelList.size() > 0) {
            recyclerView.setAdapter(new MyEditionAdapter(MyEditionActivity.this, setEditionModelList));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
        }
        progress.setVisibility(View.GONE);

    }

    public String getImagePath(String imageurl) {
        if (imageurl.contains("epaper.dbpost.com")) {
            String imagepath = "http://epaper.dbpost.com/epaper_images/";
            String[] arrayName = imageurl.split("/");
            String stateName = null;
            int sizeofdate = getDateEditionSize(arrayName[6]);
            if (sizeofdate <= 10) {
                stateName = (getDateEditionSize(DateUtility.getMyEditionCurrentDate()) - 1) + arrayName[arrayName.length - 1].substring(1);
            } else if (sizeofdate > 10) {
                stateName = (getDateEditionSize(DateUtility.getMyEditionCurrentDate()) - 1) + arrayName[arrayName.length - 1].substring(2);
            }
            return imagepath + arrayName[4] + "/epaperimages/" + DateUtility.getMyEditionCurrentDate() + "/" + stateName;
        } else if (imageurl.contains("dnaindia.com")) {
            return "http://app.dbclmatrix.com/dna.jpg";
        } else {
            String imagepath = "http://digitalimages.bhaskar.com/";
            String[] arrayName = imageurl.split("/");
            String stateName = null;// = arrayName[arrayName.length - 1];
            int sizeofdate = getDateEditionSize(arrayName[5]);
            if (sizeofdate <= 10) {
                stateName = (getDateEditionSize(DateUtility.getMyEditionCurrentDate()) - 1) + arrayName[arrayName.length - 1].substring(1);
            } else if (sizeofdate > 10) {
                stateName = (getDateEditionSize(DateUtility.getMyEditionCurrentDate()) - 1) + arrayName[arrayName.length - 1].substring(2);
            }
            return imagepath + arrayName[3] + "/EpaperImages/" + DateUtility.getMyEditionCurrentDate() + "/" + stateName;
        }
    }

    public int getDateEditionSize(String date) {
        return Integer.parseInt(date.substring(0, 2));
    }

}
