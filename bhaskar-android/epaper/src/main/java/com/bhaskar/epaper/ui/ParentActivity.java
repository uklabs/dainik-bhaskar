package com.bhaskar.epaper.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.utils.EpaperPrefernces;

public class ParentActivity extends BaseActivity {
    Epaper mApp;
    EpaperPrefernces epref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApp = Epaper.getInstance();
        epref = EpaperPrefernces.getInstance(ParentActivity.this);
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Crop & Share");
//        toolbar.setTitle("MAGAZINE");
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_action_bar_back);
//        upArrow.setColorFilter(getResources().getColor(R.color.md_grey_100), PorterDuff.Mode.SRC_ATOP);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
        setToolbarImage();
    }

    public void setToolbarImage() {
//        ImageView dainik = (ImageView) findViewById(R.id.dainikbhaskar_img);
//        ImageView divya = (ImageView) findViewById(R.id.divyabhaskar_img);
//        ImageView marathi = (ImageView) findViewById(R.id.divyamarathi_img);
//
//        dainik.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(ParentActivity.this, WebActivity.class).putExtra("key", "https://m.bhaskar.com/"));
//            }
//        });
//
//        divya.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(ParentActivity.this, WebActivity.class).putExtra("key", "https://m.divyabhaskar.co.in/"));
//            }
//        });
//
//        marathi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(ParentActivity.this, WebActivity.class).putExtra("key", "http://m.divyamarathi.bhaskar.com/"));
//            }
//        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Util.onResume(ParentActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Util.onPause(ParentActivity.this);
    }
}
