package com.bhaskar.epaper.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.HomeItemAdapter;
import com.bhaskar.epaper.adapter.HomeSubMasterAdapter;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.homeutils.HomeContent;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.model.SubMasterModel;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.epaper.utils.XMLParser;
import com.bhaskar.appscommon.tracking.Tracking;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;


public class SubMasterActivity extends BaseActivity {

    public static List<SubMasterModel> filterEditionList = new ArrayList<>();
    Epaper epaper;
    String stateName, editioncode, cityName;
    RecyclerView homeRecyclerList;
    SwipeRefreshLayout swipeLayout;
    RecyclerView stateSuggetion;
    ProgressBar progressBar;

    private void initViews() {
        swipeLayout = findViewById(R.id.swipeLayout);
        progressBar = findViewById(R.id.progress);
        stateSuggetion = findViewById(R.id.stateSuggetion);
        homeRecyclerList = findViewById(R.id.homeRecyclerList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_master);
        initViews();
        handleDeepLinks(getIntent());


        setToolbar();
        setStateSuggestion();

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callXmlsetter();
            }
        });
//        AdUtility adUtility = new AdUtility(this);
//        adUtility.loadBannerAd((AdView) findViewById(R.id.adView));

        epaper = Epaper.getInstance();
        stateName = getIntent().getStringExtra("statename"); //"RAJSATHAN";//
        editioncode = getIntent().getStringExtra("editioncode");//"14";
        cityName = getIntent().getStringExtra("cityname");//"14";
        callXmlsetter();


    }

    private void handleDeepLinks(Intent intent) {

        if (intent != null) {
            String action = intent.getAction();
            String data = intent.getDataString();
            if (Intent.ACTION_MAIN.equals(action) && data != null) {

//                String recipeId = data.substring(data.lastIndexOf("/") + 1);
                Toast.makeText(this, data, Toast.LENGTH_LONG).show();

            }
        } else {
            Toast.makeText(this, "No data", Toast.LENGTH_LONG).show();
        }

    }

    private void setStateSuggestion() {
        List<MasterModel> modelList = HomeContent.listMain.get(stateName);
        HomeItemAdapter suggestionAdapter = new HomeItemAdapter(SubMasterActivity.this, modelList);

    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_action_bar_back);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowHomeEnabled(true);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeAsUpIndicator(upArrow);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    private void callXmlsetter() {
        progressBar.setVisibility(View.VISIBLE);

        if (ConnectionDetector.isConnectingToInternet(this)) {

            String filename = "jx-submaster/xml/" + stateName + "/" + DateUtility.setDateForAll + "/";
            Log.d("filename", "=>" + filename);

            String url = Urls.BASE_URL + filename;
            StringRequest request = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String res) {
                    Log.d("filename", "=>" + res);
                    setXmlParsingMaster(res);
                    progressBar.setVisibility(View.GONE);

                    if (swipeLayout.isRefreshing()) {
                        swipeLayout.setRefreshing(false);
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_SHORT).show();
                    if (swipeLayout.isRefreshing()) {
                        swipeLayout.setRefreshing(false);
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(SubMasterActivity.this).addToRequestQueue(request);

        } else {
            progressBar.setVisibility(View.GONE);

            Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void setXmlParsingMaster(String xmlres) {
        XMLParser xmlparser = new XMLParser();
        Document doc = xmlparser.getDomElement(xmlres);
        NodeList nl = doc.getElementsByTagName("data");
        List<SubMasterModel> stateEditionList = new ArrayList<>();
        for (int i = 0; i < nl.getLength(); i++) {
            Element e = (Element) nl.item(i);
//            String description = xmlparser.getValue(e, "editionname"); // cost child value
            String editioncode = xmlparser.getValue(e, "editioncode"); // cost child value
            String priorty = xmlparser.getValue(e, "priorty"); // editionname child value
            String parent = xmlparser.getValue(e, "parent"); // name child value
            String imagepath = xmlparser.getValue(e, "imagepath"); // editionname child value
            String imagethumb = xmlparser.getValue(e, "imagethumb"); // editionname child value
            String imagelarge = xmlparser.getValue(e, "imagelarge"); // editionname child value
            String description = xmlparser.getValue(e, "description"); // editionname child value
//            Log.e("edition", "=>" + editioncode);
            stateEditionList.add(new SubMasterModel(description, priorty, editioncode, parent, imagepath, imagethumb, imagelarge));
        }
        setAdapter(stateEditionList);

    }


    private void setAdapter(List<SubMasterModel> stateEditionList) {
        int columno = Utility.calculateNoOfColumns(this);

        homeRecyclerList.setLayoutManager(new GridLayoutManager(this, columno));
        ArrayList<SubMasterModel> filterEdition = new ArrayList<>();
        filterEditionList.clear();
        for (SubMasterModel sm : stateEditionList) {
//            Log.d("Edition", sm.parent + "===" + editioncode);
            if (sm.parent.equals(editioncode)) {
                filterEdition.add(sm);
                filterEditionList.add(sm);
            }
        }

        homeRecyclerList.setAdapter(new HomeSubMasterAdapter(SubMasterActivity.this, filterEdition, stateName,cityName));
        homeRecyclerList.setItemAnimator(new DefaultItemAnimator());

        Tracking.trackGAScreen(SubMasterActivity.this, EpaperSplashActivity.getInstance(SubMasterActivity.this).defaultTracker, EpaperSplashActivity.getInstance(SubMasterActivity.this).modulePrefix+"-"+stateName+"-"+cityName, EpaperSplashActivity.getInstance(SubMasterActivity.this).source, EpaperSplashActivity.getInstance(SubMasterActivity.this).medium, EpaperSplashActivity.getInstance(SubMasterActivity.this).campaign);
    }

}
