package com.bhaskar.epaper.utils;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class AuthConst {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ResultType.SUCCESS, ResultType.SKIP, ResultType.FAIL})
    public @interface ResultType {
        int SUCCESS = 1;
        int SKIP = 2;
        int FAIL = 3;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({LoginType.FB, LoginType.GOOGLE, LoginType.MANUAL, LoginType.FB_MANUAL})
    public @interface LoginType {
        int MANUAL = 1;
        int GOOGLE = 2;
        int FB = 3;
        int FB_MANUAL = 4;
    }
}
