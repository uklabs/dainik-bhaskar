package com.bhaskar.epaper.utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by db on 9/23/2016.
 */
public class DateUtility {

    public static String setDateForAll;

    public static String getDate() {
        final Calendar c = Calendar.getInstance();

        Log.i("result", "=>afterselection" + c.getTime());
        DateFormat mSDF = new SimpleDateFormat("yyyy-MM-dd");
//        c.add(Calendar.DATE, -2); // its for previous date.
        Log.i("result", "=>afterselection" + mSDF.format(c.getTime()));

        return mSDF.format(c.getTime()).toString();

    }

    public static String getMyEditionCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String setPreviousDateFormate() {
        final Calendar c = Calendar.getInstance();
        Log.i("result", "=>afterselection" + c.getTime());
        DateFormat mSDF = new SimpleDateFormat("yyyy-MM-dd");
//        Log.d("failer", "=>" + previous);
//        Integer previousday = Integer.parseInt(previous);
//        c.add(Calendar.DATE, -previousday);

        /*switch (previous) {
            case "1":
                c.add(Calendar.DATE, -1);
                break;
            case "2":
                c.add(Calendar.DATE, -2);
                break;
            case "3":
                c.add(Calendar.DATE, -3);
                break;
            case "4":
                c.add(Calendar.DATE, -4);
                break;
        }*/

//        c.add(Calendar.DATE, -1); // its for previous date.
        Log.i("result", "=>afterselection" + mSDF.format(c.getTime()));

        return mSDF.format(c.getTime()).toString();
    }

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
