package com.bhaskar.epaper.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bhaskar.epaper.Epaper;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.request.RequestOptions;

public class ImageUtil {

    private static final String defaultURL = "";

    public static void setImage(Context context, String imageUrl, int ic_placeholder, ImageView imageView) {
        try {
            if (context == null)
                return;

            GlideUrl url = HttpHeader.getUrl(imageUrl);

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(ic_placeholder);

            if (ic_placeholder == 0) {
                Glide.with(context)
                        .asBitmap()
                        .load(url == null ? defaultURL : url)
                        .into(imageView);
            } else {
                Glide.with(context)
                        .setDefaultRequestOptions(requestOptions)
                        .asBitmap()
                        .load(url == null ? defaultURL : url)
                        .into(imageView);
            }
        } catch (Exception ignored) {
        }
    }

    public static class HttpHeader {
        private static final String USER_AGENT = Epaper.USER_AGENT;
        private static LazyHeaders lazyHeaders;

        public static GlideUrl getUrl(String url) {
            try {
                if (lazyHeaders == null) {
                    lazyHeaders = new LazyHeaders.Builder().addHeader("User-Agent", USER_AGENT).build();
                }

                return new GlideUrl(url, lazyHeaders);
            } catch (Exception e) {
                try {
                    return new GlideUrl(url);
                } catch (Exception ignored) {
                }
            }

            return null;
        }
    }
}
