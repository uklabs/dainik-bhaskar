package com.bhaskar.epaper.utils;

import android.content.Context;

import com.bhaskar.epaper.model.EditionModel;
import com.bhaskar.epaper.model.MasterModel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SerializeData implements Serializable {

    private static final String FILE_NAME = "EpaperDataFileV1.0";
    private static SerializeData serializeData;
    private static Context mContext;

    private List<MasterModel> bookmarkListInfoList;
    private List<EditionModel> editionInfoList;

    private SerializeData() {
        bookmarkListInfoList = new ArrayList<>();
        editionInfoList = new ArrayList<>();
    }

    // create instance
    public static SerializeData getInstance(Context context) {
        SerializeData.mContext = context;
        if (serializeData == null) {
            synchronized (SerializeData.class) {
                SerializeData fileInstance = getFileInstance(context);
                if (fileInstance == null) {
                    if (serializeData == null) {
                        serializeData = new SerializeData();

                        save(context);
                    }
                } else {
                    serializeData = fileInstance;
                }
            }
        }
        return serializeData;
    }

    private synchronized static void save(Context context) {
        try {
            if (context != null) {
                FileOutputStream fileOut = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(serializeData);
                out.close();
                fileOut.close();
            }

        } catch (FileNotFoundException e) {
            //e.printStackTrace();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private synchronized static SerializeData getFileInstance(Context context) {
        SerializeData serializeData = null;
        try {
            if (context != null) {
                FileInputStream fileIn = context.openFileInput(FILE_NAME);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                serializeData = (SerializeData) in.readObject();
                in.close();
                fileIn.close();
            }
        } catch (IOException i) {
        } catch (ClassNotFoundException c) {
        }
        return serializeData;
    }

    ////////////////////////////////////BookMark Method/////////////////////////////////////////////////////
    public void saveBookmark(MasterModel bookmarkListInfo) {
        if (!bookmarkListInfoList.contains(bookmarkListInfo)) {
            bookmarkListInfoList.add(bookmarkListInfo);
        }
        save(mContext);
    }

    public void removeBookmark(String editionCode) {
        if (bookmarkListInfoList.remove(new MasterModel(editionCode))) {
            save(mContext);
        }
    }

    public List<MasterModel> getBookmarkList() {
        return bookmarkListInfoList;
    }

    public boolean isBookmarkAvailable(String imagePath) {
        return bookmarkListInfoList.contains(new MasterModel(imagePath));
    }

    ////////////////////////////////////Edition Method/////////////////////////////////////////////////////
    public void saveMyEdition(EditionModel editionModel) {
        if (!editionInfoList.contains(editionModel)) {
            editionInfoList.add(editionModel);
        }
        save(mContext);
    }

    public void removeMyEdition(String editioncode) {
        if (editionInfoList.remove(new EditionModel(editioncode))) {
            save(mContext);
        }
    }

    public List<EditionModel> getMyEditionList() {
        return editionInfoList;
    }

    public boolean isMyEditionAvailable(String editioncode) {
        return editionInfoList.contains(new EditionModel(editioncode));
    }


}
