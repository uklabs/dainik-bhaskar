package com.bhaskar.epaper.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;

import org.jsoup.Jsoup;

/**
 * Created by db on 1/4/2017.
 */
public class UpdateChecker {

    Activity act;

    public void getVersionCode(Activity act) {
        this.act = act;
        new GetVersionCode(act).execute();
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {
        Activity act;

        public GetVersionCode(Activity act) {
            this.act = act;
        }

        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + act.getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {

                try {
                    String currentVersion = act.getPackageManager().getPackageInfo(act.getPackageName(), 0).versionName;
                    Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
                    if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                        //show dialog
                        Log.d("update", "LaunchDailog");
                        launchUpdateDailog();
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }


            }

        }
    }

    private void launchUpdateDailog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(act);
        builder1.setMessage("Update Available" + "\n" + "Check out the latest version of app!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Update",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        act.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + act.getPackageName())));
                    }
                });

        builder1.setNegativeButton(
                "Later",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
