package com.bhaskar.epaper.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;



/**
 * Created by db on 10/1/2016.
 */
public class Utility {

    public static int getDeviceHeight(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int height = displayMetrics.heightPixels;
        return height;
    }

    public static int getDeviceWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        return width;
    }


    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 160);
        return noOfColumns;
    }

    public static void logE(String msg, String msg1) {
//        Log.e(msg, msg1);
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr != null) {
            NetworkInfo[] info = conMgr.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static void getKeyHash(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash :", Base64.encodeToString(md.digest(), Base64.DEFAULT)); //print Keyhash.
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }

    public static void setToast(Context context, String msg) {
        try {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setNetworkErrorToast(Context context) {
        try {
            Toast.makeText(context, "Please Check your Network", Toast.LENGTH_SHORT).show();
        } catch (Exception ignored) {
        }
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


//    public static String[] getPhoneNumber(Context context) {
//        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        String[] getPhone = new String[3];
//        getPhone[0] = tMgr.getLine1Number();
//        getPhone[1] = tMgr.getSimOperator();
//        getPhone[2] = tMgr.getSimOperatorName();
//
//        return getPhone;
//    }


//    public static String convertResponseToString(Response response) {
//        //Try to get response body
//        BufferedReader reader = null;
//        StringBuilder sb = new StringBuilder();
//        try {
//
//            reader = new BufferedReader(new InputStreamReader(response.getBody().in()));
//            String line;
//
//            try {
//                while ((line = reader.readLine()) != null) {
//                    sb.append(line);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        String result = sb.toString();
//        return result;
//    }

    public static boolean isValidString(String string) {
        return !TextUtils.isEmpty(string) && !string.trim().equalsIgnoreCase("null");
    }

    public static String phoneNumberMask(String number) {
        String pre = number.substring(0, 2);
        String all = number.substring(2, number.length() - 2);
        String post = number.substring(number.length() - 2, number.length());

        String maskedNumber = pre + all.replaceAll("(?s).", "X") + post;

        return maskedNumber;
    }

//    public static boolean isUserLoggedIn(Context context) {
//        boolean status;
//        EpaperPrefernces pref = EpaperPrefernces.getInstance(context);
//        if (pref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIPTION_LOGIN_ENABLE, false)) {
//            if (pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
//                status = true;
//            } else {
//                status = false;
//            }
//        } else {
//            status = false;
//        }
//        return status;
//    }
}
