package com.bhaskar.login;

public class CommonLoginConstants {
    public static final String APP_ID = "960";
    public static final String DEVICE_TYPE = "ANDROID";
    public static final String APP_KEY = "bh@@$k@r-D";
    public static final String CONTENT_TYPE = "application/json charset=utf-8";
    public static final String CONTENT_TYPE_MULTIPART = " multipart/form-data";
    public static final String BHASKAR_APP_ID = "4";
    public static final String EVENT_LABEL = "dvb";
    public static final String COUNTRY_CODE = "91";

    /*app widget & sticky api feed url*/
    public static String USER_AGENT = "DivyaBhaskarAndroid";


    public interface AppIds {
        String DAINIK_BHASKAR = "521";
        String DIVYA_BHASKAR = "960";
        String MONEY_BHASKAR = "1463";
        String DIVYA_MARATHI = "5483";
    }


}
