package com.bhaskar.login;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bhaskar.login.utility.LoginUtil;
import com.db.main.BaseAppCompatActivity;

public abstract class BaseActivity extends BaseAppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginUtil.getInstance().setStatusBarColor(this, ContextCompat.getColor(this, com.bhaskar.login.R.color.login_theme_color));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
