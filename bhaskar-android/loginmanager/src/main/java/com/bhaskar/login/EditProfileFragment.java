package com.bhaskar.login;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.LoginController;
import com.bhaskar.login.model.UploadImage;
import com.bhaskar.login.network.ApiService;
import com.bhaskar.login.network.ServiceGenerator;
import com.bhaskar.login.utility.LoginUrls;
import com.bhaskar.login.utility.LoginUtil;
import com.bhaskar.login.utility.Systr;
import com.bhaskar.login.utility.TextInputAutoCompleteTextView;
import com.db.util.NetworkStatus;
import com.db.util.VolleyNetworkSingleton;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class EditProfileFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_IMAGE_REQUEST_CODE = 200;
    private static final int REQUEST_CODES_PERMISSIONS = 10;
    private static final int REQUEST_CODE_OTP = 2343;
    private SwitchListener switchListener;
    private ImageView ivEditImage;
    private ImageView ivProfile;

    private TextView edtDbId;
    private EditText edtEmail;
    private EditText edtName;
    private EditText edtMobile;
    private TextInputAutoCompleteTextView edtState;
    private EditText edtCountry;
    private EditText edtCity;
    private EditText edtPincode;

    private TextInputLayout tilEmail;
    private TextInputLayout tilName;
    private TextInputLayout tilMobile;

    private Button btnSave;
    private Uri fileUri;
    private ProfileInfo mProfileInfo;
    private ProfileInfo mLocalProfileInfo;

    private String picturePath;
    private String mDBid;
    private boolean isCamera;
    private String OTP;
    private List<String> stateList;
    private ProgressDialog progressDialog;

    private RadioButton rbAgeA;
    private RadioButton rbAgeB;
    private RadioButton rbAgeC;
    private RadioButton rbAgeD;
    private RadioButton rbAgeE;
    private RadioButton rbAgeF;
    private RadioButton rbMale;
    private RadioButton rbFemale;

    public static EditProfileFragment newInstance(ProfileInfo profileInfo) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(LoginPreferences.Keys.CURRENT_USER, profileInfo);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProfileInfo = (ProfileInfo) getArguments().getSerializable(LoginPreferences.Keys.CURRENT_USER);
            if (mProfileInfo != null) {
                mLocalProfileInfo = new ProfileInfo(mProfileInfo);
            }
            mDBid = LoginPreferences.getInstance(getContext()).getDataFromPreferences(LoginPreferences.Keys.DB_ID, String.class);
            stateList = Arrays.asList(getResources().getStringArray(com.bhaskar.login.R.array.india_states));

        }
        progressDialog = LoginUtil.getInstance().generateProgressDialog(getContext(), false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            switchListener = (SwitchListener) context;
        } catch (ClassCastException ignored) {
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.bhaskar.login.R.layout.fragment_edit_profile, container, false);
        initViews(view);
        updateUI(mProfileInfo);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoginController.loginController().sendGAScreen(getContext(), AppFlyerConst.GAScreen.EDIT_PROFILE);
    }

    @Override
    public void onResume() {
        super.onResume();
        Objects.requireNonNull(getActivity()).setTitle("Edit Profile");
    }

    private void updateUI(ProfileInfo mProfileInfo) {
        if (mProfileInfo == null)
            return;
        String db_id = LoginPreferences.getInstance(getContext()).getDataFromPreferences(LoginPreferences.Keys.DB_ID, String.class);
        String dbIDText = String.format("DB ID: %s", db_id);
        edtDbId.setText(dbIDText);
        edtName.setText(String.valueOf(mProfileInfo.name));
        edtEmail.setText(String.valueOf(mProfileInfo.email));
        edtMobile.setText(String.valueOf(mProfileInfo.mobile));
        edtPincode.setText(String.valueOf(mProfileInfo.zipcode));
        edtCountry.setText(String.valueOf(mProfileInfo.country));
        edtCity.setText(String.valueOf(mProfileInfo.city));
        edtState.setText(String.valueOf(mProfileInfo.state));
        setImage(mProfileInfo.profilePic, ivProfile);
        String age = TextUtils.isEmpty(mProfileInfo.dob) ? "" : mProfileInfo.dob;
        setAgeRadioSelection(age);
        String gender = TextUtils.isEmpty(mProfileInfo.gender) ? "" : mProfileInfo.gender;
        if (gender.equalsIgnoreCase("F")) {
            rbFemale.setChecked(true);
            rbMale.setChecked(false);
        } else {
            rbFemale.setChecked(false);
            rbMale.setChecked(true);
        }
    }

    private void setAgeRadioSelection(String dob) {
        clearAgeCheck();

        int age = 0;
        try {
            age = Integer.parseInt(dob);
        } catch (Exception e) {
        }

        if (age < 18) {
            rbAgeA.setChecked(true);
        } else if (age < 25) {
            rbAgeB.setChecked(true);
        } else if (age < 35) {
            rbAgeC.setChecked(true);
        } else if (age < 45) {
            rbAgeD.setChecked(true);
        } else if (age < 55) {
            rbAgeE.setChecked(true);
        } else {
            rbAgeF.setChecked(true);
        }
    }

    private void setImage(String url, ImageView ivProfile) {
        if (!TextUtils.isEmpty(url)) {
            ImageRequest request = new ImageRequest(
                    url, ivProfile::setImageBitmap, 100,
                    100, ImageView.ScaleType.CENTER, Bitmap.Config.RGB_565, error -> ivProfile.setImageResource(com.bhaskar.R.drawable.img_placeholder_profilepic));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        clearAgeCheck();
        buttonView.setChecked(isChecked);
    }

    void clearAgeCheck() {
        rbAgeA.setChecked(false);
        rbAgeB.setChecked(false);
        rbAgeC.setChecked(false);
        rbAgeD.setChecked(false);
        rbAgeE.setChecked(false);
        rbAgeF.setChecked(false);
    }

    private void initViews(View view) {
        edtDbId = view.findViewById(com.bhaskar.login.R.id.tvDBId);
        ivEditImage = view.findViewById(com.bhaskar.login.R.id.ivEditImage);
        ivProfile = view.findViewById(com.bhaskar.login.R.id.ivProfile);
        edtEmail = view.findViewById(com.bhaskar.login.R.id.edtEmail);
        edtMobile = view.findViewById(com.bhaskar.login.R.id.edtMobile);
        edtName = view.findViewById(com.bhaskar.login.R.id.edtName);
        edtMobile = view.findViewById(com.bhaskar.login.R.id.edtMobile);
        edtState = view.findViewById(com.bhaskar.login.R.id.edtState);
        //Creating the instance of ArrayAdapter containing list of fruit names
        ArrayAdapter<String> adapter = new ArrayAdapter<>
                (Objects.requireNonNull(getContext()), com.bhaskar.login.R.layout.select_dialog_item, stateList);
        edtState.setAdapter(adapter);
        edtState.setThreshold(2);
        // edtAge.setKeyListener(null);
        edtCountry = view.findViewById(com.bhaskar.login.R.id.edtCountry);
        edtCity = view.findViewById(com.bhaskar.login.R.id.edtCity);
        edtPincode = view.findViewById(com.bhaskar.login.R.id.edtPincode);
        btnSave = view.findViewById(com.bhaskar.login.R.id.btnSave);
        tilName = view.findViewById(com.bhaskar.login.R.id.tilName);
        tilMobile = view.findViewById(com.bhaskar.login.R.id.tilMobile);
        tilEmail = view.findViewById(com.bhaskar.login.R.id.tilEmail);
        btnSave.setOnClickListener(v -> onSaveProfile(null));
        ivEditImage.setOnClickListener(v -> onEditProfile());

        rbAgeA = view.findViewById(com.bhaskar.login.R.id.rbAgeA);
        rbAgeB = view.findViewById(com.bhaskar.login.R.id.rbAgeB);
        rbAgeC = view.findViewById(com.bhaskar.login.R.id.rbAgeC);
        rbAgeD = view.findViewById(com.bhaskar.login.R.id.rbAgeD);
        rbAgeE = view.findViewById(com.bhaskar.login.R.id.rbAgeE);
        rbAgeF = view.findViewById(com.bhaskar.login.R.id.rbAgeF);
        rbMale = view.findViewById(com.bhaskar.login.R.id.rbMale);
        rbFemale = view.findViewById(com.bhaskar.login.R.id.rbFemale);

        rbAgeA.setOnCheckedChangeListener(this);
        rbAgeB.setOnCheckedChangeListener(this);
        rbAgeC.setOnCheckedChangeListener(this);
        rbAgeD.setOnCheckedChangeListener(this);
        rbAgeE.setOnCheckedChangeListener(this);
        rbAgeF.setOnCheckedChangeListener(this);

    }

    void showOptions() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(com.bhaskar.login.R.layout.layout_options, null);
        TextView tvRemove = dialogView.findViewById(com.bhaskar.login.R.id.tvRemove);
        if (TextUtils.isEmpty(mProfileInfo.profilePic)) {
            tvRemove.setVisibility(View.GONE);
        } else {
            tvRemove.setVisibility(View.VISIBLE);
        }
        builderSingle.setView(dialogView);
        builderSingle.setCancelable(true);
        AlertDialog optionDialog = builderSingle.create();
        dialogView.findViewById(com.bhaskar.login.R.id.tvCamera).setOnClickListener(v -> {
            optionDialog.dismiss();
            openBCCamera();
        });
        dialogView.findViewById(com.bhaskar.login.R.id.tvGallery).setOnClickListener(v -> {
            optionDialog.dismiss();
            openBCGallary();
        });
        dialogView.findViewById(com.bhaskar.login.R.id.tvRemove).setOnClickListener(v -> {
            optionDialog.dismiss();
            removeImage();
        });

        optionDialog.show();

    }

    private void removeImage() {
        ivProfile.setImageResource(com.bhaskar.R.drawable.img_placeholder_profilepic);
        mProfileInfo.profilePic = "";
    }

    private void onEditProfile() {
        showOptions();
    }

    private void openBCCamera() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (isAllPermissionProvided()) {
                openCamera();
            } else {
                checkForPermission(true);
            }
        } else {
            openCamera();
        }
    }

    private void openBCGallary() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (isAllPermissionProvided()) {
                openGallery();
            } else {
                checkForPermission(false);
            }
        } else {
            openGallery();
        }
    }

    private void checkForPermission(boolean isCamera) {
        this.isCamera = isCamera;
        ArrayList<String> perArray = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            perArray.add(Manifest.permission.CAMERA);
        }
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            perArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            perArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!isAllPermissionProvided() && perArray.size() > 0) {
            requestPermissions(perArray.toArray(new String[perArray.size()]), REQUEST_CODES_PERMISSIONS);

        } else {
            if (isCamera) {
                openCamera();
            } else {
                openGallery();
            }
        }
    }

    private void openGallery() {
        try {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, GALLERY_IMAGE_REQUEST_CODE);
        } catch (ActivityNotFoundException ex) {
            LoginUtil.getInstance().showCustomToast(getContext(), "There is no Application installed on your device to upload an image.");
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private boolean isAllPermissionProvided() {
        return (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void onSaveProfile(Dialog dialog) {
        if (!verifyContent()) {
            return;
        }

        LoginUtil.getInstance().hideKeyboard(getContext());
        if (isMobileNumberChanged()) {
            showOTPDialog(LoginUtil.getInstance().getProperText(edtMobile));
            //startActivityForResult(OtpDialogActivity.getIntent(getContext(), LoginUtil.getInstance().getProperText(edtMobile), mProfileInfo.countryCode, mProfileInfo.appId,mProfileInfo,OtpDialogActivity.TYPE_EDIT), REQUEST_CODE_OTP);

        } else {
            JSONObject requestParams = getUpdateProfileRequestParams();
            makeJSONobject(requestParams, dialog);

        }

    }

    private void showOTPDialog(String mobile) {
//        OTPDailogFragment otpDailogFragment = OTPDailogFragment.getInstance(mobile, false);
//        otpDailogFragment.setCancelable(false);
//        otpDailogFragment.setOTPInteractionListener(new OTPDailogFragment.OTPInteraction() {
//            @Override
//            public void onDone(String mobile, String otp, Dialog dialog) {
//                mProfileInfo.mobile = mobile;
//                OTP = otp;
//                onSaveProfile(dialog);
//            }
//
//            @Override
//            public void onCancel(Dialog dialog) {
//                dialog.dismiss();
//                showProgress(false);
//            }
//        });
//        if (isAdded())
//            otpDailogFragment.show(getChildFragmentManager(), OTPDailogFragment.class.getSimpleName());

        if (switchListener != null) {
            OtpVerificationFragment verificationFragment = OtpVerificationFragment.newInstance(mobile, "0");
            verificationFragment.setListener((mobile1, otp) -> {
                mProfileInfo.mobile = mobile1;
                OTP = otp;
                onSaveProfile(null);
            });
            switchListener.onSwitchFragment(verificationFragment, true, true);
        }
    }

    public void makeJSONobject(JSONObject requestParams, Dialog dialog) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            showProgress(true);
            String feedURL = LoginUrls.LOGIN_FEED_BASE_URL + LoginUrls.UPDATE_PROFILE_URL;
            Systr.println(String.format(" Feed API %s Request params %s", feedURL, requestParams.toString()));
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, feedURL, requestParams,
                    response -> {
                        showProgress(false);
                        Systr.println(String.format(" Feed API %s Response params %s", feedURL, response.toString()));
                        if (response.optString("status").equalsIgnoreCase("Success")) {
                            String message = response.optString("msg");
                            if (dialog != null && isAdded())
                                dialog.dismiss();
                            parseUserResponse(response.optJSONObject("data"));
                            // LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SUCCESS,AppFlyerConst.GALabel.UPDATE);
                            LoginUtil.getInstance().showCustomToast(getContext(), message);
                            try {
                                getActivity().onBackPressed();
                            } catch (Exception e) {
                            }
                        } else if (response.optString("status").equalsIgnoreCase("Fail")) {
                            String message = response.optString("msg");
                            LoginUtil.getInstance().showCustomToast(getContext(), message);
                            mProfileInfo = mLocalProfileInfo;
                        }
                        Log.v("TAG", "" + response);
                    }, error -> {
                Context context = getContext();
                Systr.println(String.format(" Feed API %s Error params %s", feedURL, error.getMessage()));
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.UPDATE);
                showProgress(false);
                if (context != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        LoginUtil.getInstance().showCustomToast(context, context.getResources().getString(com.bhaskar.login.R.string.no_network_error));
                    } else {
                        LoginUtil.getInstance().showCustomToast(context, context.getResources().getString(com.bhaskar.login.R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return LoginUtil.getInstance().getNetworkHeader(getContext());
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
        } else {
            if (getContext() != null) {
                LoginUtil.getInstance().showCustomToast(getContext(), getContext().getString(com.bhaskar.login.R.string.no_network_error));
            }
            showProgress(false);
        }


    }

    public void makeJSONobject(JSONObject requestParams) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            showProgress(true);
            String feedURL = LoginUrls.LOGIN_FEED_BASE_URL + LoginUrls.UPDATE_PROFILE_URL;
            Systr.println(String.format(" Feed API %s Request params %s", feedURL, requestParams.toString()));
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, feedURL, requestParams,
                    response -> {
                        showProgress(false);
                        Systr.println(String.format(" Feed API %s Response params %s", feedURL, response.toString()));
                        if (response.optString("status").equalsIgnoreCase("Success")) {
                            String message = response.optString("msg");

                            parseUserResponse(response.optJSONObject("data"));
                            // LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SUCCESS,AppFlyerConst.GALabel.UPDATE);
                            LoginUtil.getInstance().showCustomToast(getContext(), message);
                            try {
                                getFragmentManager().popBackStack();
                            } catch (Exception e) {
                            }
                        } else if (response.optString("status").equalsIgnoreCase("Fail")) {
                            String message = response.optString("msg");
                            LoginUtil.getInstance().showCustomToast(getContext(), message);
                        }
                        Log.v("TAG", "" + response);
                    }, error -> {
                Context context = getContext();
                Systr.println(String.format(" Feed API %s Error params %s", feedURL, error.getMessage()));
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.UPDATE);
                showProgress(false);
                if (context != null) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        LoginUtil.getInstance().showCustomToast(context, context.getResources().getString(com.bhaskar.login.R.string.no_network_error));
                    } else {
                        LoginUtil.getInstance().showCustomToast(context, context.getResources().getString(com.bhaskar.login.R.string.sorry_error_found_please_try_again_));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return LoginUtil.getInstance().getNetworkHeader(getContext());
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);
        } else {
            if (getContext() != null) {
                LoginUtil.getInstance().showCustomToast(getContext(), getContext().getString(com.bhaskar.login.R.string.no_network_error));
            }
            showProgress(false);
        }
    }

    private boolean isMobileNumberChanged() {
        return !LoginUtil.getInstance().getProperText(edtMobile).equalsIgnoreCase(mProfileInfo.mobile);
    }

    private boolean verifyContent() {
        String email = LoginUtil.getInstance().getProperText(edtEmail);
        String name = LoginUtil.getInstance().getProperText(edtName);
        String phone = LoginUtil.getInstance().getProperText(edtMobile);
        if (TextUtils.isEmpty(name)) {
            tilName.setError(getResources().getString(com.bhaskar.login.R.string.name_cant_blank));
            return false;
        } else if (name.length() < 3) {
            tilName.setError(getResources().getString(com.bhaskar.login.R.string.name_length));
            return false;
        } else {
            tilName.setError(null);
        }


        if (TextUtils.isEmpty(email)) {
            tilEmail.setError(null);
        } else if (!LoginUtil.getInstance().isEmailValid(email)) {
            tilEmail.setError(getResources().getString(com.bhaskar.login.R.string.email_valid));
            return false;
        } else {
            tilEmail.setError(null);
        }

        if (TextUtils.isEmpty(phone) || phone.length() != 10) {
            tilMobile.setError(getResources().getString(com.bhaskar.login.R.string.enter_valid_mobile_number));
            return false;
        } else {
            tilMobile.setError(null);
        }
        return true;
    }

    private String getGender() {
        return (rbMale.isChecked()) ? "M" : "F";
    }

    private String getAge() {
        String age = "17";
        if (rbAgeA.isChecked()) {
            age = "17";
        } else if (rbAgeB.isChecked()) {
            age = "18";
        } else if (rbAgeC.isChecked()) {
            age = "25";
        } else if (rbAgeD.isChecked()) {
            age = "35";
        } else if (rbAgeE.isChecked()) {
            age = "45";
        } else if (rbAgeF.isChecked()) {
            age = "55";
        }
        return age;
    }

    private JSONObject getUpdateProfileRequestParams() {
        JSONObject jsonObject = new JSONObject();
        try {
            String age = getAge();
            String gender = getGender();
            //"state":"UP","city":"Delhi","zipcode":"91","gender":"M","otp":"5613","dob":"2019-01-11"}
            jsonObject.put("email", LoginUtil.getInstance().getProperText(edtEmail));
            jsonObject.put("name", LoginUtil.getInstance().getProperText(edtName));
            jsonObject.put("dob", age);
            jsonObject.put("gender", gender);
            jsonObject.put("app_id", CommonLoginConstants.BHASKAR_APP_ID);
            jsonObject.put("country_code", CommonLoginConstants.COUNTRY_CODE);
            jsonObject.put("country", LoginUtil.getInstance().getProperText(edtCountry));
            jsonObject.put("state", LoginUtil.getInstance().getProperText(edtState));
            jsonObject.put("city", LoginUtil.getInstance().getProperText(edtCity));
            jsonObject.put("zipcode", LoginUtil.getInstance().getProperText(edtPincode));
            if (!TextUtils.isEmpty(mProfileInfo.profilePic))
                jsonObject.put("user_pic", mProfileInfo.profilePic);
            if (!TextUtils.isEmpty(OTP)) {
                jsonObject.put("otp", OTP);
                jsonObject.put("mobile", LoginUtil.getInstance().getProperText(edtMobile));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void parseUserResponse(JSONObject response) {
        try {
            mProfileInfo.uID = response.optString("uid");
            mProfileInfo.uuID = response.optString("uuid");
            mProfileInfo.name = response.optString("name");
            mProfileInfo.email = response.optString("email");
            mProfileInfo.mobile = response.optString("mobile");
            mProfileInfo.dob = response.optString("dob");
            mProfileInfo.gender = response.optString("gender");
            mProfileInfo.city = response.optString("city");
            mProfileInfo.state = response.optString("state");
            mProfileInfo.country = response.optString("country");
            mProfileInfo.zipcode = response.optString("zipcode");
            mProfileInfo.profilePic = response.optString("user_pic");
            LoginPreferences.getInstance(getContext()).saveDataIntoPreferences(mProfileInfo, LoginPreferences.Keys.CURRENT_USER);

            CleverTapDB.getInstance(getContext()).updateProfileForLoginUser(getContext(), mProfileInfo.email, mProfileInfo.mobile, mProfileInfo.name, mProfileInfo.profilePic, mProfileInfo.loginType, mProfileInfo.fbAuth, mProfileInfo.googleAuth);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgress(boolean show) {
        if (isAdded()) {
            if (show) {
                if (progressDialog != null) progressDialog.show();
            } else {
                if (progressDialog != null) progressDialog.dismiss();
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (fileUri != null) {
                    picturePath = fileUri.getPath();
                    previewImage();
                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                LoginUtil.getInstance().showCustomToast(getContext(), "User cancelled image capture");
            } else {
                // failed to capture image
                LoginUtil.getInstance().showCustomToast(getContext(), "Sorry! Failed to capture image");
            }
        } else if (requestCode == GALLERY_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            try {
                Uri imageUri = getPickImageResultUri(Objects.requireNonNull(getContext()), data);
                picturePath = getRealPathFromURI(imageUri);
                previewImage();
            } catch (Exception e) {
                LoginUtil.getInstance().showCustomToast(getContext(), "Something went wrong !!!");
            }
        }
    }

    private void previewImage() {

        mProfileInfo.profilePic = picturePath;
        File file = new File(picturePath);
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap rotatedBitmap = null;

        try {
            ExifInterface ei = new ExifInterface(picturePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bitmap;
            }
        } catch (IOException e) {

        }
        ImageSetter myTask = new ImageSetter();

        if (rotatedBitmap != null) {
            File newFile = savebitmap("ProfileImage", rotatedBitmap);
            myTask.execute(newFile);
        } else {
            myTask.execute(file);
        }


    }

    private File savebitmap(String filename, Bitmap bitmap) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        ContextWrapper cw = new ContextWrapper(getActivity());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        OutputStream outStream = null;
        File file = new File(directory, filename + ".jpg");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".jpg");
        }
        try {
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODES_PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && isCamera) {
                openBCCamera();
            } else if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && !isCamera) {
                openBCGallary();
            }
        }

    }


    private void uploadFileToServerNew(String filePath) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            try {
                File imageFile = new File(filePath);
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse("image/.jpg"),
                                imageFile);
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("user_pic", imageFile.getName(), requestFile);
                ApiService apiInterface = ServiceGenerator.getClient(getContext())
                        .create(ApiService.class);

                final Call<UploadImage> call = apiInterface.upload(body);
                call.enqueue(new Callback<UploadImage>() {
                    @Override
                    public void onResponse(Call<UploadImage> call,
                                           Response<UploadImage> response) {
                        showProgress(false);
                        UploadImage responseBody = response.body();
                        if (responseBody != null) {

                            String message = responseBody.getMsg();
                            if (responseBody.getStatus().equalsIgnoreCase("Success")) {
                                mProfileInfo.profilePic = responseBody.getFilename();
                                LoginUtil.getInstance().showCustomToast(getContext(), "Please save profile to update image");
                            } else {
                                LoginUtil.getInstance().showCustomToast(getContext(), message + "");

                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<UploadImage> call, Throwable t) {
                        showProgress(false);
                        Log.e("Upload error:", t.getMessage());
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
                showProgress(false);
            }
        } else {
            if (getContext() != null) {
                LoginUtil.getInstance().showCustomToast(getContext(), getContext().getString(com.bhaskar.login.R.string.no_network_error));
            }
            showProgress(false);
        }


    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private File getOutputMediaFile(int type) {
        File mediaStorageDir = null;
        if (type == 1) {
            // External sdcard location
            mediaStorageDir = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ProfilePicture");
        }
        // Create the storage directory if it does not exist
        if (mediaStorageDir != null && !mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {

                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    /**
     * @param context used to access Android APIs, like content resolve, it is your activity/fragment/widget.
     * @param data    the returned data of the  activity result
     */
    public Uri getPickImageResultUri(@NonNull Context context, @Nullable Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera || data.getData() == null ? getCaptureImageOutputUri(context) : data.getData();
    }

    /**
     * Get URI to image received from capture  by camera.
     *
     * @param context used to access Android APIs, like content resolve, it is your activity/fragment/widget.
     */
    public Uri getCaptureImageOutputUri(@NonNull Context context) {
        Uri outputFileUri = null;
        File getImage = context.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    class ImageSetter extends AsyncTask<File, String, File> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(true);
        }

        @Override
        protected File doInBackground(File... params) {
            File file = params[0];
            return LoginUtil.getInstance().compressImage(file);
        }

        @Override
        protected void onPostExecute(File file) {
            if (file != null) {
                picturePath = file.getAbsolutePath();
                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                ivProfile.setImageBitmap(bitmap);
                uploadFileToServerNew(picturePath);
            }
        }
    }


}
