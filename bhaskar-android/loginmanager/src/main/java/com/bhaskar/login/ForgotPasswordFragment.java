package com.bhaskar.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.util.LoginController;
import com.bhaskar.login.utility.LoginUrls;
import com.bhaskar.login.utility.LoginUtil;
import com.bhaskar.login.utility.Systr;
import com.db.util.NetworkStatus;
import com.db.util.VolleyNetworkSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;


public class ForgotPasswordFragment extends Fragment {
    private Button btnSubmit;
    private EditText edtMobile;
    private SwitchListener switchListener;
    private ProgressDialog progressDialog;

    public static ForgotPasswordFragment newInstance() {
        ForgotPasswordFragment loginFragment = new ForgotPasswordFragment();
        return loginFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            switchListener = (SwitchListener) context;
        } catch (ClassCastException ignored) {
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = LoginUtil.getInstance().generateProgressDialog(getContext(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((LoginCommonActivity) Objects.requireNonNull(getActivity())).setTitle("Forgot Password");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.bhaskar.login.R.layout.fragment_forgot_password, container, false);
//        TextView tvHeaderTitle = view.findViewById(R.id.tv_heading_title);
//        tvHeaderTitle.setText("Forgot Password");
        btnSubmit = view.findViewById(com.bhaskar.login.R.id.btnSubmit);
        edtMobile = view.findViewById(com.bhaskar.login.R.id.edtMobile);
        btnSubmit.setOnClickListener(v -> {
            String mobile = LoginUtil.getInstance().getProperText(edtMobile);
            if (TextUtils.isEmpty(mobile) || mobile.length() != 10) {
                edtMobile.setError(getResources().getString(com.bhaskar.login.R.string.enter_valid_mobile_number));
                return;
            } else {
                edtMobile.setError(null);
            }
            if (TextUtils.isEmpty(mobile)) {
                edtMobile.setError("Field can't be empty");
            } else {
                edtMobile.setError(null);
            }
            getOTP(mobile);
        });
        (view.findViewById(com.bhaskar.login.R.id.iv_back)).setOnClickListener(view1 -> {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
            } else {
                getActivity().finish();
            }
        });


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoginController.loginController().sendGAScreen(getContext(), AppFlyerConst.GAScreen.FORGOT_PASSWORD);
    }

    private void getOTP(String phone) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            showProgress(true);
            JSONObject requestObject = getRequestObject(phone);
            String feedURL = LoginUrls.LOGIN_FEED_BASE_URL + LoginUrls.OTP_REQUEST_URL;
            Systr.println(String.format(" Feed API %s Request params %s", feedURL, requestObject.toString()));
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, feedURL, requestObject, response -> {
                showProgress(false);
                Systr.println(String.format(" Feed API %s Response params %s", feedURL, response.toString()));
                try {
                    //{"status":"Success","code":"200","data":[],"msg":9511}
                    String status = response.optString("status");
                    String msg = response.optString("msg");
                    if (status.equalsIgnoreCase("Fail")) {
                        LoginUtil.getInstance().showCustomToast(getContext(), msg);
                    } else if (status.equalsIgnoreCase("Success")) {
                        LoginUtil.getInstance().showCustomToast(getContext(), msg);
                        if (switchListener != null)
                            switchListener.onSwitchFragment(OtpVerificationFragment.newInstance(phone, "1"), true, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> {
                if (isAdded()) {
                    showProgress(false);
                    Systr.println(String.format(" Feed API %s Response params %s", feedURL, error.getMessage()));
                    LoginUtil.getInstance().showCustomToast(getContext(), getString(com.bhaskar.login.R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return LoginUtil.getInstance().getNetworkHeader(getContext());
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
        } else {
            showProgress(false);
            LoginUtil.getInstance().showCustomToast(getContext(), getString(com.bhaskar.login.R.string.internet_connection_error_));
        }
    }

    private JSONObject getRequestObject(String email) {
        JSONObject jsonObject = new JSONObject();
        try {
            //{"app_id":"4","mobile":"9810270278","country_code":"91"}
            jsonObject.put("mobile", email);
            jsonObject.put("app_id",CommonLoginConstants.BHASKAR_APP_ID);
            jsonObject.put("country_code", CommonLoginConstants.COUNTRY_CODE);
            jsonObject.put("is_forgot", "1");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void showProgress(boolean show) {
        if (isAdded()) {
            if (show) {
                if (progressDialog != null) progressDialog.show();
            } else {
                if (progressDialog != null) progressDialog.dismiss();
            }
        }
    }


}
