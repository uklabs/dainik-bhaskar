package com.bhaskar.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.util.LoginController;
import com.bhaskar.login.utility.LoginUrls;
import com.bhaskar.login.utility.LoginUtil;
import com.bhaskar.login.utility.Systr;
import com.db.util.NetworkStatus;
import com.db.util.VolleyNetworkSingleton;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

public class ForgotPasswordVerificationFragment extends Fragment {

    private EditText edtPassword;
    private EditText edtVerifyPassword;
    private TextInputLayout tilPassword;
    private TextInputLayout tilVerifyPassword;
    private String mPhone;
    private ProgressDialog progressDialog;
    private String otp;

    public static ForgotPasswordVerificationFragment newInstance(String phone, String otp) {
        ForgotPasswordVerificationFragment fragment = new ForgotPasswordVerificationFragment();
        Bundle bundle = new Bundle();
        bundle.putString("phone", phone);
        bundle.putString("otp", otp);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPhone = getArguments().getString("phone");
            otp = getArguments().getString("otp");
        }
        progressDialog = LoginUtil.getInstance().generateProgressDialog(getContext(), false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.bhaskar.login.R.layout.fragment_forgot_password_verification, container, false);
        Button btnSubmit = view.findViewById(com.bhaskar.login.R.id.btnSubmit);
        edtPassword = view.findViewById(com.bhaskar.login.R.id.edtPassword);
        edtVerifyPassword = view.findViewById(com.bhaskar.login.R.id.edtVerifyPassword);
        tilVerifyPassword = view.findViewById(com.bhaskar.login.R.id.tilVerifyPassword);
        tilPassword = view.findViewById(com.bhaskar.login.R.id.tilPassword);
        btnSubmit.setOnClickListener(v -> {
            if (verifyCredentials()) {
                onSubmitForgotPassword();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoginController.loginController().sendGAScreen(getContext(), AppFlyerConst.GAScreen.VERIFICATION_SCREEN);
    }


    private JSONObject getForgotPasswordRequestObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            //{"mobile":"9810270278","pass":"munna@123","otp":"","app_id":"4"}
            jsonObject.put("mobile", mPhone);
            jsonObject.put("pass", LoginUtil.getInstance().getProperText(edtPassword));
            jsonObject.put("otp", otp);
            jsonObject.put("app_id", LoginPreferences.getInstance(getContext()).getDataFromPreferences(LoginPreferences.Keys.APP_ID, String.class));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    private boolean verifyCredentials() {
        String password = LoginUtil.getInstance().getProperText(edtPassword);
        String verifyPassword = LoginUtil.getInstance().getProperText(edtVerifyPassword);
        if (TextUtils.isEmpty(password)) {
            tilPassword.setError(getResources().getString(com.bhaskar.login.R.string.pass_cant_blank));
            return false;
        } else if (password.length() < 4) {
            tilPassword.setError(getResources().getString(com.bhaskar.login.R.string.pass_length));
            return false;
        } else {
            tilPassword.setError(null);
        }

        if (!password.equals(verifyPassword)) {
            tilVerifyPassword.setError(getResources().getString(com.bhaskar.login.R.string.confirm_pass_match));
            return false;
        } else {
            tilVerifyPassword.setError(null);
        }
        return true;
    }

    private void onSubmitForgotPassword() {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            showProgress(true);
            JSONObject requestObject = getForgotPasswordRequestObject();
            String feedURL = LoginUrls.LOGIN_FEED_BASE_URL + LoginUrls.FORGOT_PASSWORD_URL;
            Systr.println(String.format(" Feed API %s Request params %s", feedURL, requestObject.toString()));
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, feedURL, requestObject, response -> {
                //{"status":"Success","code":"200","data":[],"msg":"Your password has been reset successfully."}
                Systr.println(String.format(" Feed API %s Response params %s", feedURL, response.toString()));
                showProgress(false);
                String status = response.optString("status");
                String msg = response.optString("msg");
                if (status.equalsIgnoreCase("Success")) {
                    LoginUtil.getInstance().showCustomToast(getContext(), msg);
                    Objects.requireNonNull(getActivity()).finish();
                } else {
                    LoginUtil.getInstance().showCustomToast(getContext(), msg);
                }
            }, error -> {
                if (isAdded()) {
                    Systr.println(String.format(" Feed API %s Response params %s", feedURL, error.getMessage()));
                    showProgress(false);
                    LoginUtil.getInstance().showCustomToast(getContext(), getString(com.bhaskar.login.R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return LoginUtil.getInstance().getNetworkHeader(getContext());
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
        } else {
            showProgress(false);
            LoginUtil.getInstance().showCustomToast(getContext(), getString(com.bhaskar.login.R.string.internet_connection_error_));

        }
    }


    private void showProgress(boolean show) {
        if (isAdded()) {
            if (show) {
                if (progressDialog != null) progressDialog.show();
            } else {
                if (progressDialog != null) progressDialog.dismiss();
            }
        }
    }

}
