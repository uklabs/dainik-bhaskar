package com.bhaskar.login;

public interface FragmentLifecycleLogin {
    void onPauseFragment();

    void onResumeFragment();
}
