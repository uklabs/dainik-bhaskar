package com.bhaskar.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.login.utility.FragmentUtil;

public class LoginCommonActivity extends BaseActivity implements SwitchListener {

    public static String FORGOT_PASSWORD = "forgotPassword";
    public static String PERSONAL_INFO = "personalInfo";
    public static String ENTER_OTP = "enterOTP";
    public static String RESET_PASSWORD = "resetPassword";

    private String type;
    private ProfileInfo profileInfo;

    public static Intent getIntent(Context context, String type, ProfileInfo profileInfo) {
        Intent intent = new Intent(context, LoginCommonActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("profile", profileInfo);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.bhaskar.login.R.layout.activity_login_common);
        handleExtras(getIntent());
        handleToolbar();
        findViewById(com.bhaskar.login.R.id.skip_tv).setVisibility(View.GONE);
        if (type.equalsIgnoreCase(FORGOT_PASSWORD)) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), com.bhaskar.login.R.id.rootContainer, ForgotPasswordFragment.newInstance(), false, false);
        } else if (type.equalsIgnoreCase(PERSONAL_INFO)) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), com.bhaskar.login.R.id.rootContainer, PersonalInfoFragment.newInstance(profileInfo), false, false);
            findViewById(com.bhaskar.login.R.id.skip_tv).setVisibility(View.VISIBLE);
        } else if (type.equalsIgnoreCase(ENTER_OTP)) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), com.bhaskar.login.R.id.rootContainer, OtpVerificationFragment.newInstance("", "0"), false, false);
        } else if (type.equalsIgnoreCase(RESET_PASSWORD)) {
            FragmentUtil.changeFragment(getSupportFragmentManager(), com.bhaskar.login.R.id.rootContainer, ForgotPasswordVerificationFragment.newInstance("", ""), false, false);
        }

        findViewById(com.bhaskar.login.R.id.skip_tv).setOnClickListener(v -> finish());
    }

    private void handleToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(com.bhaskar.login.R.id.toolbar);
        toolbar.setBackgroundColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setTitle("");
        toolbarTitle = toolbar.findViewById(com.bhaskar.login.R.id.toolbar_title);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    TextView toolbarTitle;

    public void setTitle(String title) {
        toolbarTitle.setText(title);
    }


    private void handleExtras(Intent intent) {
        if (intent != null) {
            type = intent.getStringExtra("type");
            profileInfo = (ProfileInfo) intent.getSerializableExtra("profile");
        }
    }


    @Override
    public void onSwitchFragment(Fragment fragment, boolean addToStack, boolean animate) {
        FragmentUtil.changeFragment(getSupportFragmentManager(), com.bhaskar.login.R.id.rootContainer, fragment, addToStack, animate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
//        getMenuInflater().inflate(R.menu.login, menu);
//
//        if (type.equalsIgnoreCase("PersonalInfo")) {
//            menu.findItem(R.id.skip).setVisible(true);
//
//        } else {
//            menu.findItem(R.id.skip).setVisible(false);
//        }
//
//        int positionOfMenuItem = 0; // or whatever...
//        MenuItem item = menu.getItem(positionOfMenuItem);
//        SpannableString s = new SpannableString(menu.getItem(positionOfMenuItem).getTitle().toString());
//        s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimaryLogin)), 0, s.length(), 0);
//        item.setTitle(s);
        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int i = item.getItemId();
//        if (i == R.id.skip) {
//            finish();
//        }
//        return super.onOptionsItemSelected(item);
//    }
}
