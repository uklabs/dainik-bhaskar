package com.bhaskar.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.login.utility.LoginUtil;
import com.bhaskar.util.LoginController;
import com.google.android.gms.auth.api.credentials.Credential;

import static com.bhaskar.login.LoginManagerActivity.RC_MOBILE_NO_PICKER;

public class LoginFragment extends Fragment implements FragmentLifecycleLogin {

    private EditText edtMobile;
    private EditText edtPassword;
    private LoginInteraction loginListener;
    private boolean isPasswordVisible = false;

    public static LoginFragment newInstance() {
        LoginFragment loginFragment = new LoginFragment();
        Bundle bundle = new Bundle();
        loginFragment.setArguments(bundle);

        return loginFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginInteraction) {
            loginListener = (LoginInteraction) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.bhaskar.login.R.layout.fragment_login, container, false);
        edtMobile = view.findViewById(com.bhaskar.login.R.id.edtMobile);
        edtPassword = view.findViewById(com.bhaskar.login.R.id.edtPassword);

        view.findViewById(com.bhaskar.login.R.id.iv_pass_visible).setOnClickListener(v -> {
            isPasswordVisible = !isPasswordVisible;
            if (isPasswordVisible) {
                edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                ((ImageView) view.findViewById(com.bhaskar.login.R.id.iv_pass_visible)).setImageResource(com.bhaskar.login.R.drawable.ic_visibility_on);
            } else {
                edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                ((ImageView) view.findViewById(com.bhaskar.login.R.id.iv_pass_visible)).setImageResource(com.bhaskar.login.R.drawable.ic_visibility_off);
            }
            edtPassword.setTypeface(Typeface.DEFAULT);
        });
        view.findViewById(com.bhaskar.login.R.id.btnLogin).setOnClickListener(view13 -> {
            String mobile = LoginUtil.getInstance().getProperText(edtMobile);
            String password = LoginUtil.getInstance().getProperText(edtPassword);
            manualValues(mobile, password);
        });

        view.findViewById(com.bhaskar.login.R.id.tvForgotPassword).setOnClickListener(view14 -> {
            startActivity(LoginCommonActivity.getIntent(getContext(), LoginCommonActivity.FORGOT_PASSWORD, null));
        });
        ((TextView) view.findViewById(com.bhaskar.login.R.id.tvSignup)).setText(Html.fromHtml(getContext().getResources().getString(com.bhaskar.login.R.string.please_sign_up)));
        view.findViewById(com.bhaskar.login.R.id.tvSignup).setOnClickListener(view15 -> {
            if (loginListener != null)
                loginListener.onSwitch(0);
        });

        view.findViewById(com.bhaskar.login.R.id.vGmail).setOnClickListener(view1 -> {
            if (loginListener != null) {
                loginListener.onGoogle();
            }
        });

        view.findViewById(com.bhaskar.login.R.id.vFacebook).setOnClickListener(view12 -> {
            if (loginListener != null) {
                loginListener.onFacebook();
            }
        });

        edtMobile.setOnFocusChangeListener((v, hasFocus) -> {
            if (v.isFocused()) {
                if (loginListener != null)
                    loginListener.onMobileNo(LoginFragment.this);
            }
        });

        LoginUtil.getInstance().hideKeyboard(getContext());
        hideFacebook(view);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_MOBILE_NO_PICKER) {
            if (resultCode == Activity.RESULT_OK) {
                setMobileNumber(data);
            }
            edtMobile.setOnFocusChangeListener(null);
        }
    }

    private void setMobileNumber(Intent data) {
        try {
            Credential cred = data.getParcelableExtra(Credential.EXTRA_KEY);
            String mobileNo = cred.getId();
            if (!TextUtils.isEmpty(mobileNo)) {
                mobileNo = mobileNo.trim().replace(" ", "");
                mobileNo = mobileNo.substring(mobileNo.length() - 10);
                edtMobile.setText(mobileNo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideFacebook(View view) {
        boolean isFacebookDisabled = LoginPreferences.getInstance(getContext()).getBooleanValue(LoginPreferences.Keys.IS_FACEBOOK_LOGIN_DISABLED, false);

        if (isFacebookDisabled) {
            view.findViewById(com.bhaskar.login.R.id.vFacebook).setVisibility(View.GONE);
        }
    }


    private void manualValues(String mobile, String pass) {
        LoginUtil.getInstance().hideKeyboard(getContext());
        if (TextUtils.isEmpty(mobile) || mobile.length() != 10) {
            edtMobile.setError(getResources().getString(com.bhaskar.login.R.string.enter_valid_mobile_number));
            return;
        } else {
            edtMobile.setError(null);
        }

        if (TextUtils.isEmpty(pass)) {
            edtPassword.setError(getResources().getString(com.bhaskar.login.R.string.pass_cant_blank));
            return;
        } else if (pass.length() < 4) {
            edtPassword.setError(getResources().getString(com.bhaskar.login.R.string.pass_length));
            return;
        } else {
            edtPassword.setError(null);
        }

        ProfileInfo profileInfo = new ProfileInfo();
        profileInfo.loginType = LoginController.LoginType.MANUAL;
        profileInfo.mobile = mobile;
        profileInfo.password = pass;
        if (loginListener != null)
            loginListener.onLoginServer(profileInfo);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {
        LoginController.loginController().sendGAScreen(getContext(), AppFlyerConst.GAScreen.LOGIN);

    }

    public interface LoginInteraction {
        void onLoginServer(ProfileInfo profileInfo);

        void onFacebook();

        void onGoogle();

        void onSwitch(int type);

        void onMobileNo(Fragment fragment);
    }

}
