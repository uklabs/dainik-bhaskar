package com.bhaskar.login;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.login.utility.FragmentUtil;
import com.bhaskar.login.utility.LoginUrls;
import com.bhaskar.login.utility.LoginUtil;
import com.bhaskar.login.utility.Systr;
import com.bhaskar.util.LoginController;
import com.db.util.NetworkStatus;
import com.db.util.VolleyNetworkSingleton;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Map;

public class LoginManagerActivity extends BaseActivity implements LoginFragment.LoginInteraction, SignupFragment.SignupInteraction, GoogleApiClient.OnConnectionFailedListener, MobileNumberFragment.OnMobileNumberListener, OtpVerificationFragment.OnOtpListener {

    private static final String TAG = LoginManagerActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 8000;
    public static final int RC_MOBILE_NO_PICKER = 8001;
    // Google Authentication
    boolean flag = false;
    private String googleTokenId;
    private boolean isShowDialog;
    private ViewPager viewPager;
    private ProgressDialog progressDialog;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;
//    private TextView headerTitle;

    private ProfileInfo profileInfo;
    private SectionPagerAdapter sectionPagerAdapter;
    private Toolbar toolbar;

    public static Intent getIntent(Context context, String googleTokenId, String dbID, String deviceID) {
        Intent intent = new Intent(context, LoginManagerActivity.class);
        intent.putExtra("googleTokenId", googleTokenId);
        intent.putExtra("dbID", dbID);
        intent.putExtra("deviceID", deviceID);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.bhaskar.login.R.layout.activity_login_manager);
        TabLayout tabLayout = findViewById(com.bhaskar.login.R.id.tabLayout);

        viewPager = findViewById(com.bhaskar.login.R.id.viewPager);
        progressDialog = LoginUtil.getInstance().generateProgressDialog(this, false);
        sectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());

        handleToolbar();
        handleExtras();
        initGoogleAuth();
        initFacebookSdk();

        ProfileInfo profileInfo = LoginPreferences.getInstance(this).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        if (profileInfo == null || !profileInfo.isUserLoggedIn) {
            viewPager.setAdapter(sectionPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.setVisibility(View.GONE);
//            headerTitle.setText(getString(com.bhaskar.login.R.string.sign_up));
            toolbar.setTitle(getString(com.bhaskar.login.R.string.sign_up));
        } else {
            startActivity(ProfileManagerActivity.getIntent(this));
            finishActivity();
        }

        viewPager.addOnPageChangeListener(onPageChangeListener);
        viewPager.post(() -> onPageChangeListener.onPageSelected(0));

        showConsentAlertDialog();
        LoginUtil.getInstance().hideKeyboard(this);

    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            checkForPauseResumeFragment(position);
            setHeaderTitle(position);
            LoginUtil.getInstance().hideKeyboard(LoginManagerActivity.this);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void setHeaderTitle(int position) {
        try {
            if (sectionPagerAdapter != null)
                toolbar.setTitle(sectionPagerAdapter.getPageTitle(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showMobileNoPickerUI(Fragment context) {
        try {
            HintRequest hintRequest = new HintRequest.Builder()
                    .setHintPickerConfig(new CredentialPickerConfig.Builder()
                            .setShowCancelButton(true)
                            .build())
                    .setPhoneNumberIdentifierSupported(true)
                    .build();

            PendingIntent intent =
                    Auth.CredentialsApi.getHintPickerIntent(mGoogleApiClient, hintRequest);
            try {
                context.startIntentSenderForResult(intent.getIntentSender(), RC_MOBILE_NO_PICKER, null, 0, 0, 0, null);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void checkForPauseResumeFragment(int position) {
        if (viewPager != null) {
            try {
                SectionPagerAdapter newsPagerAdapter = (SectionPagerAdapter) viewPager.getAdapter();
                if (newsPagerAdapter != null) {
                    try {
                        FragmentLifecycleLogin fragmentToShow = (FragmentLifecycleLogin) newsPagerAdapter.instantiateItem(viewPager, position);
                        fragmentToShow.onPauseFragment();
                    } catch (Exception ignored) {
                    }
                    try {
                        FragmentLifecycleLogin lifecycle = (FragmentLifecycleLogin) newsPagerAdapter.instantiateItem(viewPager, position);
                        lifecycle.onResumeFragment();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception ignore) {
            }
        }
    }


    private void handleToolbar() {
        toolbar = findViewById(com.bhaskar.login.R.id.toolbar);
        toolbar.setBackgroundColor(Color.WHITE);
//        toolbar.setTitleTextColor(Color.BLACK);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void handleExtras() {
        Intent intent = getIntent();
        if (intent != null) {
            googleTokenId = intent.getStringExtra("googleTokenId");
            String dbID = intent.getStringExtra("dbID");
            String deviceID = intent.getStringExtra("deviceID");

            LoginPreferences.getInstance(this).saveDataIntoPreferences(dbID, LoginPreferences.Keys.DB_ID);
            LoginPreferences.getInstance(this).saveDataIntoPreferences(deviceID, LoginPreferences.Keys.DEVICE_ID);
        }
    }

    private void initGoogleAuth() {
        if (!TextUtils.isEmpty(googleTokenId)) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(googleTokenId)
                    .requestEmail()
                    .build();

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addApi(Auth.CREDENTIALS_API)
                    .build();

            mAuth = FirebaseAuth.getInstance();
            mAuthListener = firebaseAuth -> {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null && !flag) {
                    flag = true;
                    firebaseValues(user);
                }
            };
        } else {
            LoginUtil.getInstance().showCustomToast(this, "Something went wrong please try again later!");
        }

    }

    private void signInWithGoogle() {
        flag = false;
        if (mAuth != null && mAuthListener != null) {
            mAuth.addAuthStateListener(mAuthListener);
            mAuth.signOut();
        }
        if (mGoogleApiClient != null) {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            mGoogleApiClient.clearDefaultAccountAndReconnect();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    // Facebook Authentication
    private void initFacebookSdk() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Context context = LoginManagerActivity.this;
                        LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.FB);
                        if (context != null)
                            fail(context.getResources().getString(com.bhaskar.login.R.string.cancel), LoginController.ResultType.SKIP, LoginController.LoginType.FB);
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Context context = LoginManagerActivity.this;
                        LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.FB);
                        if (context != null)
                            fail(context.getResources().getString(com.bhaskar.login.R.string.error), LoginController.ResultType.FAIL, LoginController.LoginType.FB);
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken accessToken) {
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        //Sign in success update UI with the signed-in user's info
                        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> facebookValues(object, response));
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender");
                        request.setParameters(parameters);
                        request.executeAsync();
                    } else {
                        //If sign in fails display a message
                        Context context = LoginManagerActivity.this;
                        LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.FB);
                        if (context != null)
                            fail(context.getResources().getString(com.bhaskar.login.R.string.error), LoginController.ResultType.FAIL, LoginController.LoginType.FB);

                    }
                });

    }

    private void facebookValues(JSONObject object, GraphResponse response) {
        System.out.println(response.toString());
        try {
            String signUpId = object.getString("id");
            String name = object.getString("name");
            String gender = "";
            try {
                gender = object.getString("gender");
            } catch (Exception e) {
            }

            String email = "";
            try {
                email = object.getString("email");
            } catch (JSONException e) {
            }

            String profilePic = "https://graph.facebook.com/" + signUpId + "/picture";

            Systr.println("Id: " + signUpId + ", name: " + name + ", gender: " + gender + ", email: " + email + ", pic : " + profilePic);

            profileInfo = new ProfileInfo();
            profileInfo.loginType = LoginController.LoginType.FB;
            profileInfo.fbAuth = signUpId;
            profileInfo.name = name;
            profileInfo.email = email;
            profileInfo.gender = gender;
            profileInfo.profilePic = profilePic;
            LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.FB);

            checkForLoginFromServer(profileInfo);

        } catch (Exception e) {
            e.printStackTrace();
            LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.FB);

            Context context = LoginManagerActivity.this;
            if (context != null)
                fail(context.getResources().getString(com.bhaskar.login.R.string.login_failed_please_try_again), LoginController.ResultType.FAIL, LoginController.LoginType.FB);
        }
    }

    private void fail(String msg, int resultType, String loginType) {
        LoginUtil.getInstance().showCustomToast(this, msg);
    }

    private void checkForLoginFromServer(final ProfileInfo profileInfo) {
        if (NetworkStatus.getInstance().isConnected(this)) {
            showProgress(true);
            JSONObject requestObject = new JSONObject();
            try {
                requestObject.put("login_type", profileInfo.loginType);
                requestObject.put("fb_auth", profileInfo.fbAuth);
                requestObject.put("g_auth", profileInfo.googleAuth);
                requestObject.put("mobile", profileInfo.mobile);
                requestObject.put("pass", profileInfo.password);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String feedURL = LoginUrls.LOGIN_FEED_BASE_URL + LoginUrls.LOGIN_URL;
            Systr.println(String.format(" Feed API %s Request params %s", feedURL, requestObject.toString()));

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, feedURL, requestObject, response -> {
                Systr.println(String.format(" Feed API %s Response params %s", feedURL, response.toString()));
                showProgress(false);
                parseLoginJson(response, profileInfo);
            }, error -> {
                showProgress(false);
                Systr.println(String.format(" Feed API %s error params %s", feedURL, error.getMessage()));
                LoginUtil.getInstance().showCustomToast(this, getString(com.bhaskar.login.R.string.sorry_error_found_please_try_again_));
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return LoginUtil.getInstance().getNetworkHeader(LoginManagerActivity.this);
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(this).addToRequestQueue(request);
        } else {
            showProgress(false);
            if (this != null)
                LoginUtil.getInstance().showCustomToast(this, getString(com.bhaskar.login.R.string.internet_connection_error_));
        }
    }

    private void parseLoginJson(JSONObject response, ProfileInfo profileInfo) {
        String status = response.optString("status");
        String message = response.optString("msg");

        if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("Success")) {
            try {
                JSONObject dataJsonObject = response.getJSONObject("data");
                profileInfo.uID = dataJsonObject.optString("uid");
                profileInfo.uuID = dataJsonObject.optString("uuid");
                profileInfo.name = dataJsonObject.optString("name");
                profileInfo.email = dataJsonObject.optString("email");
                profileInfo.mobile = dataJsonObject.optString("mobile");
                profileInfo.countryCode = dataJsonObject.optString("country_code");
                profileInfo.country = dataJsonObject.optString("country");
                profileInfo.state = dataJsonObject.optString("state");
                profileInfo.city = dataJsonObject.optString("city");
                profileInfo.zipcode = dataJsonObject.optString("zipcode");
                profileInfo.gender = dataJsonObject.optString("gender");
                profileInfo.dob = dataJsonObject.optString("dob");
                profileInfo.profilePic = dataJsonObject.optString("user_pic");

                profileInfo.isUserLoggedIn = true;
                LoginPreferences.getInstance(this).saveDataIntoPreferences(profileInfo, LoginPreferences.Keys.CURRENT_USER);
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.LOGIN, LoginController.loginController().getLoginType(profileInfo));
                CleverTapDB.getInstance(LoginManagerActivity.this).loginProfileForLoginUser(LoginManagerActivity.this, profileInfo.email, profileInfo.mobile, profileInfo.name, profileInfo.profilePic, profileInfo.loginType, profileInfo.fbAuth, profileInfo.googleAuth);
                CleverTapDB.getInstance(LoginManagerActivity.this).cleverTapTrackEvent(LoginManagerActivity.this, CTConstant.EVENT_NAME.ACCOUNT_ACTIONS, CTConstant.PROP_NAME.ACTION_TYPE, CTConstant.PROP_VALUE.LOGIN, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.ACTION_MEDIUM, LoginController.loginController().getLoginType(profileInfo)));

                LoginUtil.getInstance().showCustomToast(this, message);
                finishActivity();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            if (profileInfo.loginType.equalsIgnoreCase(LoginController.LoginType.FB) || profileInfo.loginType.equalsIgnoreCase(LoginController.LoginType.GOOGLE)) {
                showMobileNumberDialog();
            } else {
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, LoginController.loginController().getLoginType(profileInfo));
                LoginUtil.getInstance().showCustomToast(this, message);
            }
        }
    }

    void finishActivity() {
        finish();
    }

    private void showMobileNumberDialog() {
        findViewById(com.bhaskar.login.R.id.tab_root_layout).setVisibility(View.GONE);
        findViewById(com.bhaskar.login.R.id.frame_layout).setVisibility(View.VISIBLE);
        FragmentUtil.changeFragment(getSupportFragmentManager(), com.bhaskar.login.R.id.frame_layout, MobileNumberFragment.newInstance(), true, false);
    }

    private void showProgress(boolean show) {
        if (show) {
            if (progressDialog != null && !progressDialog.isShowing() && !isFinishing()) {
                progressDialog.show();
            }
        } else {
            if (progressDialog != null && progressDialog.isShowing()) {
                if (!isFinishing())
                    progressDialog.cancel();
            }
        }
    }

    private void signupRequestToServer(ProfileInfo profileInfo, String otp) {
        if (NetworkStatus.getInstance().isConnected(LoginManagerActivity.this)) {
            JSONObject requestObject = new JSONObject();
            showProgress(true);
            try {
                requestObject.put("app_id", CommonLoginConstants.BHASKAR_APP_ID);
                requestObject.put("login_type", profileInfo.loginType);
                requestObject.put("mobile", profileInfo.mobile);
                requestObject.put("country_code", profileInfo.countryCode);
                requestObject.put("pass", profileInfo.password);
                requestObject.put("fb_auth", profileInfo.fbAuth);
                requestObject.put("g_auth", profileInfo.googleAuth);
                requestObject.put("name", profileInfo.name);
                requestObject.put("email", profileInfo.email);
                requestObject.put("gender", profileInfo.gender);
                requestObject.put("otp", otp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String feedURL = LoginUrls.LOGIN_FEED_BASE_URL + LoginUrls.SIGNUP_URL;
            Systr.println(String.format(" Feed API %s Request params %s", feedURL, requestObject.toString()));
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, feedURL, requestObject, response -> {
                showProgress(false);
                Systr.println(String.format(" Feed API %s Response params %s", feedURL, response.toString()));
                parseSignupJson(response, profileInfo);
            }, error -> {
                Systr.println(String.format(" Feed API %s Error %s", feedURL, error.getMessage()));
                showProgress(false);
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.OTP);
                LoginUtil.getInstance().showCustomToast(LoginManagerActivity.this, getString(com.bhaskar.login.R.string.sorry_error_found_please_try_again_));
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return LoginUtil.getInstance().getNetworkHeader(LoginManagerActivity.this);
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(LoginManagerActivity.this).addToRequestQueue(request);
        } else {
            showProgress(false);
            LoginUtil.getInstance().showCustomToast(LoginManagerActivity.this, getString(com.bhaskar.login.R.string.internet_connection_error_));
        }
    }

    private void parseSignupJson(JSONObject response, ProfileInfo profileInfo) {
        String status = response.optString("status");
        String message = response.optString("msg");

        if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("Success")) {
            try {
                JSONObject dataJsonObject = response.getJSONObject("data");
                profileInfo.uID = dataJsonObject.optString("uid");
                profileInfo.uuID = dataJsonObject.optString("uuid");
                profileInfo.name = dataJsonObject.optString("name");
                profileInfo.email = dataJsonObject.optString("email");
                profileInfo.mobile = dataJsonObject.optString("mobile");
                profileInfo.countryCode = dataJsonObject.optString("country_code");
                profileInfo.country = dataJsonObject.optString("country");
                profileInfo.state = dataJsonObject.optString("state");
                profileInfo.city = dataJsonObject.optString("city");
                profileInfo.zipcode = dataJsonObject.optString("zipcode");
                profileInfo.gender = dataJsonObject.optString("gender");
                profileInfo.dob = dataJsonObject.optString("dob");
                profileInfo.profilePic = dataJsonObject.optString("user_pic");
                profileInfo.isUserLoggedIn = true;

                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.OTP);
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.REGISTER, LoginController.loginController().getLoginType(profileInfo));
                LoginPreferences.getInstance(LoginManagerActivity.this).saveDataIntoPreferences(profileInfo, LoginPreferences.Keys.CURRENT_USER);
                LoginUtil.getInstance().showCustomToast(LoginManagerActivity.this, message);

                CleverTapDB.getInstance(LoginManagerActivity.this).loginProfileForLoginUser(LoginManagerActivity.this, profileInfo.email, profileInfo.mobile, profileInfo.name, profileInfo.profilePic, profileInfo.loginType, profileInfo.fbAuth, profileInfo.googleAuth);
                CleverTapDB.getInstance(LoginManagerActivity.this).cleverTapTrackEvent(LoginManagerActivity.this, CTConstant.EVENT_NAME.ACCOUNT_ACTIONS, CTConstant.PROP_NAME.ACTION_TYPE, CTConstant.PROP_VALUE.SIGNUP, LoginController.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.ACTION_MEDIUM, LoginController.loginController().getLoginType(profileInfo)));

                startActivity(LoginCommonActivity.getIntent(LoginManagerActivity.this, LoginCommonActivity.PERSONAL_INFO, profileInfo));
                finishActivity();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.OTP);
            LoginUtil.getInstance().showCustomToast(LoginManagerActivity.this, message);
        }
    }


    private void showConsentAlertDialog() {
        if (isShowDialog || LoginPreferences.getInstance(LoginManagerActivity.this).getBooleanValue(LoginPreferences.Keys.CONSENT_ACCEPT, false)) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(com.bhaskar.login.R.layout.layout_gdpr_login_permission_dialog, null);
        builder.setView(view);
        builder.setCancelable(false);

        final AlertDialog alert = builder.create();
        alert.setOnShowListener(dialog -> {
        });

        view.findViewById(com.bhaskar.login.R.id.cancel_tv).setOnClickListener(view12 -> {
            alert.dismiss();
            finishActivity();
        });

        view.findViewById(com.bhaskar.login.R.id.allow_tv).setOnClickListener(view1 -> {
            LoginPreferences.getInstance(LoginManagerActivity.this).setBooleanValue(LoginPreferences.Keys.CONSENT_ACCEPT, true);
            alert.dismiss();
        });

        isShowDialog = true;
        alert.show();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            findViewById(com.bhaskar.login.R.id.frame_layout).setVisibility(View.GONE);
            findViewById(com.bhaskar.login.R.id.tab_root_layout).setVisibility(View.VISIBLE);
        } else {
            ProfileInfo profileInfo = LoginPreferences.getInstance(LoginManagerActivity.this).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
            if (profileInfo != null && profileInfo.isUserLoggedIn) {
                Intent intent = new Intent();
                intent.putExtra("userData", profileInfo);
                setResult(Activity.RESULT_OK, intent);
                finishActivity();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result != null && result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else if (result != null && !result.isSuccess()) {
                LoginUtil.getInstance().showCustomToast(LoginManagerActivity.this, getResources().getString(com.bhaskar.login.R.string.login_failed_please_try_again));
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.GOOGLE);
            } else {
                fail(getResources().getString(com.bhaskar.login.R.string.login_failed_please_try_again), LoginController.ResultType.FAIL, LoginController.LoginType.GOOGLE);
            }
        } else {
            if (callbackManager != null) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        if (mAuth != null) {
            AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
            mAuth.signInWithCredential(credential).addOnCompleteListener(LoginManagerActivity.this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.GOOGLE);
                        fail(getResources().getString(com.bhaskar.login.R.string.authentication_failed), LoginController.ResultType.FAIL, LoginController.LoginType.GOOGLE);
                    }
                }
            });
        }
    }

    private void firebaseValues(FirebaseUser user) {
        Uri uri = user.getPhotoUrl();
        String profilePic = "";
        if (uri != null) {
            profilePic = uri.toString();
        }

        String signUpId = user.getUid();
        String name = user.getDisplayName();
        String email = user.getEmail();

        profileInfo = new ProfileInfo();
        profileInfo.loginType = LoginController.LoginType.GOOGLE;
        profileInfo.googleAuth = signUpId;
        profileInfo.name = name;
        profileInfo.email = email;
        profileInfo.profilePic = profilePic;
        LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.GOOGLE);

        checkForLoginFromServer(profileInfo);
    }

    @Override
    public void onDoneOTP(String mobile, String otp) {
        if (profileInfo != null) {
            profileInfo.mobile = mobile;
            signupRequestToServer(profileInfo, otp);
        }
    }

    @Override
    public void onInputMobileNumber(String mobile) {
        findViewById(com.bhaskar.login.R.id.tab_root_layout).setVisibility(View.GONE);
        findViewById(com.bhaskar.login.R.id.frame_layout).setVisibility(View.VISIBLE);
        FragmentUtil.changeFragment(getSupportFragmentManager(), com.bhaskar.login.R.id.frame_layout, OtpVerificationFragment.newInstance(mobile, "0"), true, false);
    }

    @Override
    public void onLoginServer(ProfileInfo profileInfo) {
        this.profileInfo = profileInfo;
        checkForLoginFromServer(profileInfo);
    }

    @Override
    public void onSignupServer(ProfileInfo profileInfo) {
        this.profileInfo = profileInfo;
        onInputMobileNumber(profileInfo.mobile);
    }

    @Override
    public void onFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    @Override
    public void onGoogle() {
        boolean isEnabled = true;
        if (isEnabled) {
            if (NetworkStatus.getInstance().isConnected(LoginManagerActivity.this)) {
                signInWithGoogle();
            } else {
                LoginUtil.getInstance().showCustomToast(LoginManagerActivity.this, getResources().getString(com.bhaskar.login.R.string.no_network_error));
            }
        } else {
            LoginUtil.getInstance().showCustomToast(LoginManagerActivity.this, getResources().getString(com.bhaskar.login.R.string.accept_tnc));
        }
    }

    @Override
    public void onSwitch(int type) {
        viewPager.setCurrentItem(type);
    }

    @Override
    public void onMobileNo(Fragment fragment) {
        showMobileNoPickerUI(fragment);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.GOOGLE);
        fail(getResources().getString(com.bhaskar.login.R.string.google_play_service_error), LoginController.ResultType.FAIL, LoginController.LoginType.GOOGLE);
    }

    @Override
    public void onDestroy() {
        if (mAuth != null && mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

        if (mGoogleApiClient != null) {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }

        super.onDestroy();
    }

    class SectionPagerAdapter extends FragmentPagerAdapter {

        SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return SignupFragment.newInstance();
                default:
                    return LoginFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(com.bhaskar.login.R.string.sign_up);
                default:
                    return getString(com.bhaskar.login.R.string.login_txt);
            }
        }
    }
}
