package com.bhaskar.login;

import android.content.Context;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.bhaskar.login.utility.LoginUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class MobileNumberFragment extends Fragment {

    public interface OnMobileNumberListener {
        void onInputMobileNumber(String mobile);
    }

    private OnMobileNumberListener mListener;

    public MobileNumberFragment() {
    }

    public static MobileNumberFragment newInstance() {
        MobileNumberFragment fragment = new MobileNumberFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (OnMobileNumberListener) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(com.bhaskar.login.R.layout.fragment_mobile_number, container, false);

        EditText edtMobile = view.findViewById(com.bhaskar.login.R.id.edtMobile);
        Button btnContinue = view.findViewById(com.bhaskar.login.R.id.btnContinue);
        TextInputLayout tilMobile = view.findViewById(com.bhaskar.login.R.id.tilMobile);

        btnContinue.setOnClickListener(v -> {
            String mobile = LoginUtil.getInstance().getProperText(edtMobile);
            if (TextUtils.isEmpty(mobile) || mobile.length() != 10) {
                tilMobile.setError(getString(com.bhaskar.login.R.string.mobile_error));
                return;
            } else {
                tilMobile.setError(null);
            }

            if (mListener != null)
                mListener.onInputMobileNumber(mobile);
        });

        return view;
    }
}
