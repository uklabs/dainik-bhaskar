package com.bhaskar.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.util.LoginController;
import com.bhaskar.login.utility.LoginUrls;
import com.bhaskar.login.utility.LoginUtil;
import com.bhaskar.login.utility.Systr;
import com.db.util.NetworkStatus;
import com.db.util.VolleyNetworkSingleton;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtpVerificationFragment extends Fragment {

    private TextView otpMsgTv;
    private String isForgot;

    public void setListener(OnOtpListener listener) {
        mListener = listener;
    }

    public interface OnOtpListener {
        void onDoneOTP(String mobile, String otp);
    }

    private EditText edtOTP1;
    private EditText edtOTP2;
    private EditText edtOTP3;
    private EditText edtOTP4;

    private TextView tvMin;
    private TextView tvSec;
    private TextView tvResendOTP;
    private Button verifyButton;

    private ProgressBar progressBar;
    private View vTimer;
    private CountDownTimer timer;

    private String mPhone;
    private boolean shouldSendTracking = true;

    private OnOtpListener mListener;
    private SwitchListener switchListener;

    public OtpVerificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (OnOtpListener) context;
        } catch (Exception e) {
        }
        try {
            switchListener = (SwitchListener) context;
        } catch (ClassCastException ignored) {
        }

    }

    public static OtpVerificationFragment newInstance(String mobileNumber, String isForgot) {
        OtpVerificationFragment fragment = new OtpVerificationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LoginPreferences.Keys.MOBILE, mobileNumber);
        bundle.putString(LoginPreferences.Keys.IS_FORGOT, isForgot);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPhone = getArguments().getString(LoginPreferences.Keys.MOBILE);
            isForgot = getArguments().getString(LoginPreferences.Keys.IS_FORGOT);
        }
        startSmsRetriver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(com.bhaskar.login.R.layout.fragment_otp_verfication, container, false);

        progressBar = rootView.findViewById(com.bhaskar.login.R.id.progressBar);
        edtOTP1 = rootView.findViewById(com.bhaskar.login.R.id.edtOTP1);
        edtOTP2 = rootView.findViewById(com.bhaskar.login.R.id.edtOTP2);
        edtOTP3 = rootView.findViewById(com.bhaskar.login.R.id.edtOTP3);
        edtOTP4 = rootView.findViewById(com.bhaskar.login.R.id.edtOTP4);
        tvMin = rootView.findViewById(com.bhaskar.login.R.id.tvMin);
        tvSec = rootView.findViewById(com.bhaskar.login.R.id.tvSec);
        tvResendOTP = rootView.findViewById(com.bhaskar.login.R.id.tvResendOTP);
        verifyButton = rootView.findViewById(com.bhaskar.login.R.id.btnLogin);
        otpMsgTv = rootView.findViewById(com.bhaskar.login.R.id.msg_tv);
        vTimer = rootView.findViewById(com.bhaskar.login.R.id.timer_layout);

        tvResendOTP.setOnClickListener(v -> {
            tvResendOTP.setEnabled(false);
            tvResendOTP.setTextColor(Color.DKGRAY);
            sendOtpRequestToServer();
        });

        verifyButton.setOnClickListener(v -> {
            showProgress(false);
            onDoneOTP();
        });

        otpMsgTv.setText(Objects.requireNonNull(getContext()).getResources().getString(com.bhaskar.login.R.string.otp_verification_text, mPhone));
        setTextWatcherInView();

        if (TextUtils.isEmpty(isForgot) || !isForgot.equalsIgnoreCase("1")) {
            sendOtpRequestToServer();
        } else {
            showTimer(true);
        }

        return rootView;
    }

    private void setTextWatcherInView() {
        edtOTP1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String txt = String.valueOf(s);
                if (txt.length() > 0) {
                    edtOTP2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edtOTP2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String txt = String.valueOf(s);
                if (txt.length() > 0) {
                    edtOTP3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edtOTP3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String txt = String.valueOf(s);
                if (txt.length() > 0) {
                    edtOTP4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void clearOTP() {
        edtOTP1.setText("");
        edtOTP2.setText("");
        edtOTP3.setText("");
        edtOTP4.setText("");
        edtOTP1.requestFocus();
    }

    private void sendOtpRequestToServer() {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            showProgress(true);

            JSONObject requestObj = getRequestParams(mPhone);
            String feedURL = LoginUrls.LOGIN_FEED_BASE_URL + LoginUrls.OTP_REQUEST_URL;
            Systr.println(String.format(" Feed API %s Request params %s", feedURL, requestObj.toString()));

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, feedURL, requestObj, response -> {
                if (isAdded()) {
                    Systr.println(String.format(" Feed API %s Response params %s", feedURL, response.toString()));
                    showProgress(false);
                    String status = response.optString("status");
                    String msg = response.optString("msg");
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("Success")) {
                        showTimer(true);
                    } else {
                        clearOTP();
                        LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.OTP);
                    }
                    LoginUtil.getInstance().showCustomToast(getContext(), msg);
                }

            }, error -> {
                Systr.println(String.format(" Feed API %s Error params %s", feedURL, error.getMessage()));

                if (shouldSendTracking)
                    LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.OTP);

                if (isAdded()) {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        LoginUtil.getInstance().showCustomToast(getContext(), getResources().getString(com.bhaskar.login.R.string.no_network_error));
                    } else {
                        LoginUtil.getInstance().showCustomToast(getContext(), getResources().getString(com.bhaskar.login.R.string.sorry_error_found_please_try_again_) + " \n Error: " + error.getMessage());
                    }
                    showProgress(false);

                    tvResendOTP.setEnabled(true);
                    tvResendOTP.setTextColor(Color.BLUE);
                    showTimer(false);
                }

            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return LoginUtil.getInstance().getNetworkHeader(getContext());
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);

        } else {
            showProgress(false);
            LoginUtil.getInstance().showCustomToast(getContext(), getString(com.bhaskar.login.R.string.internet_connection_error_));
        }
    }

    private void showTimer(boolean b) {
        if (b) {
            vTimer.setVisibility(View.VISIBLE);
            startTimer();
        } else {
            vTimer.setVisibility(View.GONE);
            stopTimer();
        }
    }

    private JSONObject getRequestParams(String mobile) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("app_id", CommonLoginConstants.BHASKAR_APP_ID);
            jsonObject.put("mobile", mobile);
            jsonObject.put("country_code", CommonLoginConstants.COUNTRY_CODE);
            jsonObject.put("is_forgot", isForgot);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void showProgress(boolean b) {
        if (isAdded()) {
            if (b) {
                progressBar.setVisibility(View.VISIBLE);
                vTimer.setVisibility(View.INVISIBLE);
            } else {
                progressBar.setVisibility(View.INVISIBLE);
                vTimer.setVisibility(View.VISIBLE);
            }
        }
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }

    private void startTimer() {
        final DecimalFormat formatter = new DecimalFormat("00");

        timer = new CountDownTimer(120000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long min = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                long sec = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                tvMin.setText(formatter.format(min));
                tvSec.setText(formatter.format(sec));
            }

            @Override
            public void onFinish() {
                showTimer(false);
                tvSec.setText("00");
                tvMin.setText("00");
                tvResendOTP.setEnabled(true);
                tvResendOTP.setTextColor(Color.BLUE);
            }
        }.start();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (timer != null) {
            timer.cancel();
        }
    }

    private void startSmsRetriver() {
        // Get an instance of SmsRetrieverClient, used to start listening for a matching// SMS message.
        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());

        // Starts SmsRetriever, which waits for ONE matching SMS message until timeout
        // (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
        // action SmsRetriever#SMS_RETRIEVED_ACTION.
        Task<Void> task = client.startSmsRetriever();

        // Listen for success/failure of the start Task. If in a background thread, this
        // can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Successfully started retriever, expect broadcast intent
                // ...
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
            }
        });
    }

    private void onDoneOTP() {
        String otp = getEnteredOTP();
        if (!TextUtils.isEmpty(otp)) {
            if (!TextUtils.isEmpty(isForgot) && isForgot.equalsIgnoreCase("1")) {
                if (switchListener != null)
                    switchListener.onSwitchFragment(ForgotPasswordVerificationFragment.newInstance(mPhone, otp), true, true);
            } else {
                if (mListener != null)
                    mListener.onDoneOTP(mPhone, otp);
            }
        } else {
            clearOTP();
            LoginUtil.getInstance().showCustomToast(getContext(), "Please enter OTP to proceed!");
        }
    }

    private String getEnteredOTP() {
        return String.format("%s%s%s%s", LoginUtil.getInstance().getProperText(edtOTP1), LoginUtil.getInstance().getProperText(edtOTP2), LoginUtil.getInstance().getProperText(edtOTP3), LoginUtil.getInstance().getProperText(edtOTP4));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getActivity().registerReceiver(receiverSms, new IntentFilter("com.google.android.gms.auth.api.phone.SMS_RETRIEVED"));
        }
    }

    @Override
    public void onDestroy() {
        try {
            if (receiverSms != null) {
                getActivity().unregisterReceiver(receiverSms);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private BroadcastReceiver receiverSms = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {

            if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                Bundle extras = intent.getExtras();
                Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

                switch (status.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS:
                        // Get SMS message contents
                        String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                        // Extract one-time code from the message and complete verification
                        // by sending the code back to your server.
                        Pattern p = Pattern.compile("\\b\\d{4}\\b");
                        Matcher m = p.matcher(message);
                        String finalOtp = "";
                        while (m.find()) {
                            finalOtp = m.group(0);
                        }

                        edtOTP1.setText("" + finalOtp.charAt(0));
                        edtOTP2.setText("" + finalOtp.charAt(1));
                        edtOTP3.setText("" + finalOtp.charAt(2));
                        edtOTP4.setText("" + finalOtp.charAt(3));

                        new Handler().postDelayed(() -> {
                            showProgress(false);
                            onDoneOTP();
                        }, 1000);
                        break;

                    case CommonStatusCodes.TIMEOUT:
                        // Waiting for SMS timed out (5 minutes)
                        // Handle the error ...
                        break;
                }
            }
        }
    };
}
