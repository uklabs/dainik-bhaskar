package com.bhaskar.login;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.LoginController;
import com.bhaskar.login.utility.LoginUrls;
import com.bhaskar.login.utility.LoginUtil;
import com.bhaskar.login.utility.Systr;
import com.db.util.NetworkStatus;
import com.db.util.VolleyNetworkSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

public class PersonalInfoFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    private EditText edtEmail;
    private EditText edtCity;
    private View vProgress;
    private ProfileInfo profileInfo;

    private RadioButton rbAgeA;
    private RadioButton rbAgeB;
    private RadioButton rbAgeC;
    private RadioButton rbAgeD;
    private RadioButton rbAgeE;
    private RadioButton rbAgeF;
    private RadioButton rbMale;
    private RadioButton rbFemale;

    public static PersonalInfoFragment newInstance(ProfileInfo mProfileInfo) {
        PersonalInfoFragment fragment = new PersonalInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(LoginPreferences.Keys.CURRENT_USER, mProfileInfo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            profileInfo = (ProfileInfo) getArguments().getSerializable(LoginPreferences.Keys.CURRENT_USER);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((LoginCommonActivity) Objects.requireNonNull(getActivity())).setTitle("Personal Info");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.bhaskar.login.R.layout.fragment_personal_info, container, false);
        Button btnContinue = view.findViewById(com.bhaskar.login.R.id.btnContinue);
        vProgress = view.findViewById(com.bhaskar.login.R.id.vProgress);
        rbAgeA = view.findViewById(com.bhaskar.login.R.id.rbAgeA);
        rbAgeB = view.findViewById(com.bhaskar.login.R.id.rbAgeB);
        rbAgeC = view.findViewById(com.bhaskar.login.R.id.rbAgeC);
        rbAgeD = view.findViewById(com.bhaskar.login.R.id.rbAgeD);
        rbAgeE = view.findViewById(com.bhaskar.login.R.id.rbAgeE);
        rbAgeF = view.findViewById(com.bhaskar.login.R.id.rbAgeF);
        rbMale = view.findViewById(com.bhaskar.login.R.id.rbMale);
        rbFemale = view.findViewById(com.bhaskar.login.R.id.rbFemale);

        rbAgeA.setOnCheckedChangeListener(this);
        rbAgeB.setOnCheckedChangeListener(this);
        rbAgeC.setOnCheckedChangeListener(this);
        rbAgeD.setOnCheckedChangeListener(this);
        rbAgeE.setOnCheckedChangeListener(this);
        rbAgeF.setOnCheckedChangeListener(this);

        edtEmail = view.findViewById(com.bhaskar.login.R.id.edtEmail);
        edtCity = view.findViewById(com.bhaskar.login.R.id.edtCity);


        btnContinue.setOnClickListener(v -> {
            updateUserProfile();
        });

        setData();

        return view;
    }

    void clearAgeCheck() {
        rbAgeA.setChecked(false);
        rbAgeB.setChecked(false);
        rbAgeC.setChecked(false);
        rbAgeD.setChecked(false);
        rbAgeE.setChecked(false);
        rbAgeF.setChecked(false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoginController.loginController().sendGAScreen(getContext(), AppFlyerConst.GAScreen.PERSONAL_INFO_SCREEN);
    }

    private void finishInfo() {
        LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.UPDATE);
        Objects.requireNonNull(getActivity()).onBackPressed();
    }

    private void setData() {
        if (profileInfo != null) {
            edtEmail.setText(TextUtils.isEmpty(profileInfo.email) ? "" : profileInfo.email);
            edtCity.setText(TextUtils.isEmpty(profileInfo.city) ? "" : profileInfo.city);
            String age = TextUtils.isEmpty(profileInfo.dob) ? "" : profileInfo.dob;
            setAgeRadioSelection(age);
            String gender = TextUtils.isEmpty(profileInfo.gender) ? "" : profileInfo.gender;
            if (gender.equalsIgnoreCase("Female")) {
                rbFemale.setChecked(true);
                rbMale.setChecked(false);
            } else {
                rbFemale.setChecked(false);
                rbMale.setChecked(true);
            }
        }
    }

    private void setAgeRadioSelection(String dob) {
        clearAgeCheck();

        int age = 0;
        try {
            age = Integer.parseInt(dob);
        } catch (Exception e) {
        }

        if (age < 18) {
            rbAgeA.setChecked(true);
        } else if (age < 25) {
            rbAgeB.setChecked(true);
        } else if (age < 35) {
            rbAgeC.setChecked(true);
        } else if (age < 45) {
            rbAgeD.setChecked(true);
        } else if (age < 55) {
            rbAgeE.setChecked(true);
        } else {
            rbAgeF.setChecked(true);
        }
    }


    private void updateUserProfile() {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            String email = LoginUtil.getInstance().getProperText(edtEmail);
            String city = LoginUtil.getInstance().getProperText(edtCity);
            String age = getAge();
            String gender = getGender();

            profileInfo.email = email;
            profileInfo.dob = age;
            profileInfo.city = city;
            profileInfo.gender = gender;
            showProgress(true);
            JSONObject requestObject = getUpdateProfileRequestParamas();

            String feedURL = LoginUrls.LOGIN_FEED_BASE_URL + LoginUrls.UPDATE_PROFILE_URL;
            Systr.println(String.format(" Feed API %s Request params %s", feedURL, requestObject.toString()));
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, feedURL, requestObject, response -> {
                Systr.println(String.format(" Feed API %s Response params %s", feedURL, response));
                showProgress(false);
                parseUserJson(response, profileInfo);
            }, error -> {
                if (isAdded()) {
                    LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.UPDATE);
                    Systr.println(String.format(" Feed API %s error params %s", feedURL, error.getMessage()));
                    showProgress(false);
                    LoginUtil.getInstance().showCustomToast(getContext(), getString(com.bhaskar.login.R.string.sorry_error_found_please_try_again_));
                }

            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return LoginUtil.getInstance().getNetworkHeader(getContext());
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
        } else {
            showProgress(false);
            LoginUtil.getInstance().showCustomToast(getContext(), getString(com.bhaskar.login.R.string.internet_connection_error_));
        }
    }

    private String getGender() {
        return (rbMale.isChecked()) ? "M" : "F";
    }

    private String getAge() {
        String age = "17";
        if (rbAgeA.isChecked()) {
            age = "17";
        } else if (rbAgeB.isChecked()) {
            age = "18";
        } else if (rbAgeC.isChecked()) {
            age = "25";
        } else if (rbAgeD.isChecked()) {
            age = "35";
        } else if (rbAgeE.isChecked()) {
            age = "45";
        } else if (rbAgeF.isChecked()) {
            age = "55";
        }
        return age;
    }

    private JSONObject getUpdateProfileRequestParamas() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", profileInfo.name);
            jsonObject.put("email", profileInfo.email);
            jsonObject.put("app_id", CommonLoginConstants.BHASKAR_APP_ID);
            jsonObject.put("country_code", profileInfo.countryCode);
            jsonObject.put("pass", profileInfo.password);
            jsonObject.put("login_type", profileInfo.loginType);
            jsonObject.put("gender", profileInfo.gender);
            jsonObject.put("dob", profileInfo.dob);
            jsonObject.put("city", profileInfo.city);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    private void parseUserJson(JSONObject response, ProfileInfo profileInfo) {
        String status = response.optString("status");
        String message = response.optString("msg");
        if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("Success")) {
            try {
                JSONObject dataJsonObject = response.getJSONObject("data");
                profileInfo.uID = dataJsonObject.optString("uid");
                profileInfo.uuID = dataJsonObject.optString("uuid");
                profileInfo.name = dataJsonObject.optString("name");
                profileInfo.email = dataJsonObject.optString("email");
                profileInfo.mobile = dataJsonObject.optString("mobile");
                profileInfo.countryCode = dataJsonObject.optString("country_code");
                profileInfo.country = dataJsonObject.optString("country");
                profileInfo.state = dataJsonObject.optString("state");
                profileInfo.city = dataJsonObject.optString("city");
                profileInfo.zipcode = dataJsonObject.optString("zipcode");
                profileInfo.gender = dataJsonObject.optString("gender");
                profileInfo.dob = dataJsonObject.optString("dob");
                profileInfo.profilePic = dataJsonObject.optString("user_pic");
                profileInfo.isUserLoggedIn = true;
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.UPDATE);
                LoginPreferences.getInstance(getContext()).saveDataIntoPreferences(profileInfo, LoginPreferences.Keys.CURRENT_USER);
                LoginUtil.getInstance().showCustomToast(getContext(), message);

                Objects.requireNonNull(getActivity()).onBackPressed();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            LoginUtil.getInstance().showCustomToast(getContext(), message);
        }
    }

    private void showProgress(boolean show) {
        if (isAdded()) {
            if (show) {
                vProgress.setVisibility(View.VISIBLE);
            } else {
                vProgress.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        clearAgeCheck();
        buttonView.setChecked(isChecked);
    }
}
