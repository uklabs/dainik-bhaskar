package com.bhaskar.login;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.toolbox.ImageRequest;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.LoginController;
import com.bhaskar.login.utility.LoginUtil;
import com.bhaskar.view.ui.splash.SplashActivity;
import com.db.util.AppPreferences;
import com.db.util.QuickPreferences;
import com.db.util.VolleyNetworkSingleton;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Calendar;
import java.util.Objects;

public class ProfileFragment extends Fragment {
    private SwitchListener switchListener;
    private ProfileInfo profileInfo;
    private String mDBId;
    private TextView tvGreetings;

    public static ProfileFragment newInstance(ProfileInfo profileInfo) {
        ProfileFragment loginFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(LoginPreferences.Keys.CURRENT_USER, profileInfo);
        loginFragment.setArguments(bundle);
        return loginFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            profileInfo = (ProfileInfo) getArguments().getSerializable(LoginPreferences.Keys.CURRENT_USER);
            mDBId = LoginPreferences.getInstance(getContext()).getStringValue(LoginPreferences.Keys.DB_ID, "");
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            switchListener = (SwitchListener) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Objects.requireNonNull(getActivity()).setTitle("Profile");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.bhaskar.login.R.layout.fragment_profile, container, false);

        updateUI(view);
        view.findViewById(com.bhaskar.login.R.id.vSignOut).setOnClickListener(v -> logoutUser());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoginController.loginController().sendGAScreen(getContext(), AppFlyerConst.GAScreen.PROFILE);
    }


    private void logoutUser() {
        LoginUtil.getInstance().showConfirmDialog(getContext(), "Are your sure you want to logout?", (dialog, which) -> {
            LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.LOGOUT, LoginController.loginController().getLoginType(profileInfo));
            logout(getContext());
            Objects.requireNonNull(getActivity()).finish();

            CleverTapDB.getInstance(getContext()).updateProfileForLogoutUser();
            CleverTapDB.getInstance(getContext()).cleverTapTrackEvent(getContext(), CTConstant.EVENT_NAME.ACCOUNT_ACTIONS, CTConstant.PROP_NAME.ACTION_TYPE, CTConstant.PROP_VALUE.LOGOUT, LoginController.getUserDataMap());

        }, (dialog, which) -> {
            dialog.dismiss();
        });
    }

    public void logout(Context context) {
        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
        try {
            FirebaseAuth.getInstance().signOut();
        } catch (Exception e) {
        }
        try {
            LoginManager.getInstance().logOut();
        } catch (Exception e) {
        }
        switch (profileInfo.loginType) {
            case LoginController.LoginType.GOOGLE:
                break;
            case LoginController.LoginType.FB:
                break;
            case LoginController.LoginType.MANUAL:
                break;
            default:
                break;
        }

        profileInfo.isUserLoggedIn = false;
        profileInfo.loginType = LoginController.LoginType.MANUAL;
        LoginPreferences.getInstance(context).saveDataIntoPreferences(profileInfo, LoginPreferences.Keys.CURRENT_USER);
    }

    private void updateUI(View view) {
        profileInfo = LoginPreferences.getInstance(getContext()).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        tvGreetings = view.findViewById(com.bhaskar.login.R.id.tvGreetings);
        TextView tvName = view.findViewById(com.bhaskar.login.R.id.tvName);
        TextView tvEmail = view.findViewById(com.bhaskar.login.R.id.tvEmail);
        TextView tvDBId = view.findViewById(com.bhaskar.login.R.id.tvDBId);
        TextView tvPhone = view.findViewById(com.bhaskar.login.R.id.edtPhone);
        TextView tvCity = view.findViewById(com.bhaskar.login.R.id.tvCity);
        ImageView ivProfileImage = view.findViewById(com.bhaskar.login.R.id.ivProfile);
        ImageView ivGoogle = view.findViewById(com.bhaskar.login.R.id.ivGoogle);
        ImageView ivFacebook = view.findViewById(com.bhaskar.login.R.id.ivFacebook);
        setText(tvName, profileInfo.name);
        setText(tvEmail, profileInfo.email);
        String db_id = LoginPreferences.getInstance(getContext()).getDataFromPreferences(LoginPreferences.Keys.DB_ID, String.class);
        String dbIDText = String.format("DB ID: %s", db_id);
        tvDBId.setText(dbIDText);
        setText(tvPhone, profileInfo.mobile);
        String city = profileInfo.city;
        if (!TextUtils.isEmpty(profileInfo.state)) {
            city = city + ", " + profileInfo.state;
        }
        setText(tvCity, city.trim());
        String greetings = getGreetingText();
        tvGreetings.setText(String.format("%s, %s", greetings, profileInfo.name));
        if (!TextUtils.isEmpty(profileInfo.profilePic)) {
            ImageRequest request = new ImageRequest(
                    profileInfo.profilePic, response -> ivProfileImage.setImageBitmap(response), 100,
                    100, ImageView.ScaleType.CENTER, Bitmap.Config.RGB_565, error -> ivProfileImage.setImageResource(com.bhaskar.R.drawable.img_placeholder_profilepic));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
        }

        view.findViewById(com.bhaskar.login.R.id.ivEditProfile).setOnClickListener(v -> {
            if (switchListener != null)
                switchListener.onSwitchFragment(EditProfileFragment.newInstance(profileInfo), true, true);
        });


        if (TextUtils.isEmpty(profileInfo.googleAuth)) {
            ivGoogle.setImageResource(com.bhaskar.login.R.drawable.ic_google_login_uncheck);
        } else {
            ivGoogle.setImageResource(com.bhaskar.login.R.drawable.ic_google_login_check);
        }
        boolean isFacebookLoginDisabled = AppPreferences.getInstance(getContext()).getBooleanValue(QuickPreferences.IS_FACEBOOK_LOGIN_DISABLED, false);
        if (isFacebookLoginDisabled) {
            if (TextUtils.isEmpty(profileInfo.fbAuth)) {
                ivFacebook.setImageResource(com.bhaskar.login.R.drawable.ic_fb_login_uncheck);
            } else {
                ivFacebook.setImageResource(com.bhaskar.login.R.drawable.ic_fb_login_check);
            }
        } else {
            ivFacebook.setVisibility(View.GONE);
        }
    }

    private String getGreetingText() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay < 12) {
            return "Good Morning";
        } else if (timeOfDay < 18) {
            return "Good Afternoon";
        } else if (timeOfDay < 21) {
            return "Good Evening";
        } else if (timeOfDay < 24) {
            return "Good Evening";
        }
        return "Hello";
    }

    private void setText(TextView tvTextView, String text) {
        if (TextUtils.isEmpty(text)) {
            tvTextView.setVisibility(View.GONE);
        } else if (text.equalsIgnoreCase(",")) {
            tvTextView.setVisibility(View.GONE);
        } else {
            tvTextView.setVisibility(View.VISIBLE);
            tvTextView.setText(text);

        }
    }
}
