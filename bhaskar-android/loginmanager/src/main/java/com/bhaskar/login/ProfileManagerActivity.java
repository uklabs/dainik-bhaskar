package com.bhaskar.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.login.utility.FragmentUtil;

public class ProfileManagerActivity extends BaseActivity implements SwitchListener {

    private Toolbar toolbar;

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, ProfileManagerActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.bhaskar.login.R.layout.activity_profile_manager);
        ProfileInfo profileInfo = LoginPreferences.getInstance(this).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        handleToolbar();

        FragmentUtil.changeFragment(getSupportFragmentManager(), com.bhaskar.login.R.id.rootContainer, ProfileFragment.newInstance(profileInfo), false, false);
    }


    @Override
    public void setTitle(CharSequence title) {
        toolbar.setTitle(title);
    }

    private void handleToolbar() {
        /*Tool Bar*/
        toolbar = findViewById(com.bhaskar.login.R.id.toolbar);
        toolbar.setBackgroundColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSwitchFragment(Fragment fragment, boolean addToStack, boolean animate) {
        FragmentUtil.changeFragment(getSupportFragmentManager(), com.bhaskar.login.R.id.rootContainer, fragment, addToStack, animate);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if the result is capturing Image
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof EditProfileFragment) {
                fragment.onActivityResult(requestCode, resultCode, data);
                break;
            }
        }
    }
}
