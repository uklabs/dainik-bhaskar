package com.bhaskar.login;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.login.utility.LoginUrls;
import com.bhaskar.login.utility.LoginUtil;
import com.bhaskar.login.utility.Systr;
import com.bhaskar.util.LoginController;
import com.db.util.NetworkStatus;
import com.db.util.VolleyNetworkSingleton;
import com.google.android.gms.auth.api.credentials.Credential;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

import static com.bhaskar.login.LoginManagerActivity.RC_MOBILE_NO_PICKER;

public class SignupFragment extends Fragment implements FragmentLifecycleLogin {

    private boolean isPasswordVisible = false;
    private EditText edtName;
    private EditText edtMobile;
    private EditText edtPassword;
    private ProfileInfo mProfileInfo;
    private SignupInteraction mSignupListener;
    private ProgressDialog progressDialog;

    public static SignupFragment newInstance() {
        return new SignupFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mSignupListener = (SignupInteraction) context;
        } catch (ClassCastException ignored) {
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.bhaskar.login.R.layout.fragment_signup_manager, container, false);

        progressDialog = LoginUtil.getInstance().generateProgressDialog(getContext(), false);
        edtMobile = view.findViewById(com.bhaskar.login.R.id.edtMobile);
        edtPassword = view.findViewById(com.bhaskar.login.R.id.edtPassword);
        edtName = view.findViewById(com.bhaskar.login.R.id.edtName);
        edtMobile.setSelection(edtMobile.getText().toString().length());

        ((TextView) view.findViewById(com.bhaskar.login.R.id.tvSignin)).setText(Html.fromHtml(getString(com.bhaskar.login.R.string.please_login_text)));

        view.findViewById(com.bhaskar.login.R.id.iv_pass_visible).setOnClickListener(v -> {
            isPasswordVisible = !isPasswordVisible;
            if (isPasswordVisible) {
                edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                ((ImageView) view.findViewById(com.bhaskar.login.R.id.iv_pass_visible)).setImageResource(com.bhaskar.login.R.drawable.ic_visibility_on);
            } else {
                edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                ((ImageView) view.findViewById(com.bhaskar.login.R.id.iv_pass_visible)).setImageResource(com.bhaskar.login.R.drawable.ic_visibility_off);
            }
            edtPassword.setTypeface(Typeface.DEFAULT);
        });

        view.findViewById(com.bhaskar.login.R.id.btnSignup).setOnClickListener(view12 -> {
            String name = LoginUtil.getInstance().getProperText(edtName);
            String mobile = LoginUtil.getInstance().getProperText(edtMobile);
            String password = LoginUtil.getInstance().getProperText(edtPassword);
            signupValues(name, mobile, password);
        });

        view.findViewById(com.bhaskar.login.R.id.vGmail).setOnClickListener(view1 -> {
            if (mSignupListener != null) {
                mSignupListener.onGoogle();
            }
        });

        view.findViewById(com.bhaskar.login.R.id.vFacebook).setOnClickListener(view12 -> {
            if (mSignupListener != null) {
                mSignupListener.onFacebook();
            }
        });

        view.findViewById(com.bhaskar.login.R.id.tvSignin).setOnClickListener(v -> {
            if (mSignupListener != null) {
                mSignupListener.onSwitch(1);
            }
        });


        edtMobile.setOnFocusChangeListener((v, hasFocus) -> {
            if (v.isFocused()) {
                if (mSignupListener != null)
                    mSignupListener.onMobileNo(SignupFragment.this);
            }
        });
        hideFacebook(view);
        return view;
    }


    private void hideFacebook(View view) {
        boolean isFacebookDisabled = LoginPreferences.getInstance(getContext()).getBooleanValue(LoginPreferences.Keys.IS_FACEBOOK_LOGIN_DISABLED, false);

        if (isFacebookDisabled) {
            view.findViewById(com.bhaskar.login.R.id.vFacebook).setVisibility(View.GONE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    void showProgress(boolean show) {
        if (isAdded()) {
            if (show) {
                if (progressDialog != null) {
                    progressDialog.show();
                }
            } else {
                if (progressDialog != null) {
                    progressDialog.cancel();
                }
            }
        }
    }

    private void signupValues(String name, String mobile, String password) {
        if (TextUtils.isEmpty(name)) {
            edtName.setError(getResources().getString(com.bhaskar.login.R.string.name_cant_blank));
            return;
        } else if (name.length() < 3) {
            edtName.setError(getResources().getString(com.bhaskar.login.R.string.name_length));
            return;
        } else {
            edtName.setError(null);
        }

        if (TextUtils.isEmpty(mobile) || mobile.length() != 10) {
            edtMobile.setError(getResources().getString(com.bhaskar.login.R.string.enter_valid_mobile_number));
            return;
        } else {
            edtMobile.setError(null);
        }
        if (TextUtils.isEmpty(password)) {
            edtPassword.setError(getResources().getString(com.bhaskar.login.R.string.pass_cant_blank));
            return;
        } else if (password.length() < 4) {
            edtPassword.setError(getResources().getString(com.bhaskar.login.R.string.pass_length));
            return;
        } else {
            edtPassword.setError(null);
        }

        mProfileInfo = new ProfileInfo();
        mProfileInfo.loginType = LoginController.LoginType.MANUAL;
        mProfileInfo.name = LoginUtil.getInstance().getProperText(edtName);
        mProfileInfo.mobile = LoginUtil.getInstance().getProperText(edtMobile);
        mProfileInfo.password = LoginUtil.getInstance().getProperText(edtPassword);

        if (mSignupListener != null) {
            mSignupListener.onSignupServer(mProfileInfo);
        }
//        showOTPDialog(mProfileInfo.mobile);
    }

//    private void showOTPDialog(String mobile) {
//        LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.MOBILE);
//        OTPDailogFragment otpDailogFragment = OTPDailogFragment.getInstance(mobile, true);
//        otpDailogFragment.setCancelable(false);
//        otpDailogFragment.setOTPInteractionListener(new OTPDailogFragment.OTPInteraction() {
//            @Override
//            public void onDone(String mobile, String otp, Dialog dialog) {
//                mProfileInfo.mobile = mobile;
//                signupRequestToServer(mProfileInfo, otp, dialog);
//            }
//
//            @Override
//            public void onCancel(Dialog dialog) {
//                dialog.dismiss();
//                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SKIP, AppFlyerConst.GALabel.OTP);
//                showProgress(false);
//            }
//        });
//        if (isAdded())
//            otpDailogFragment.show(getChildFragmentManager(), OTPDailogFragment.class.getSimpleName());
//    }


    private void signupRequestToServer(ProfileInfo profileInfo, String otp, Dialog dialog) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            showProgress(true);
            JSONObject requestObject = getSignupRequestParams(otp);
            String feedURL = LoginUrls.LOGIN_FEED_BASE_URL + LoginUrls.SIGNUP_URL;
            Systr.println(String.format(" Feed API %s Request params %s", feedURL, requestObject.toString()));
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, feedURL, requestObject, response -> {
                Systr.println(String.format(" Feed API %s Response params %s", feedURL, response.toString()));
                showProgress(false);
                parseSignupJson(response, profileInfo, dialog);
            }, error -> {
                if (isAdded()) {
                    Systr.println(String.format(" Feed API %s Error params %s", feedURL, error.getMessage()));
                    LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.OTP);
                    showProgress(false);
                    LoginUtil.getInstance().showCustomToast(getContext(), getString(com.bhaskar.login.R.string.sorry_error_found_please_try_again_));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return LoginUtil.getInstance().getNetworkHeader(getContext());
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
        } else {
            showProgress(false);
            LoginUtil.getInstance().showCustomToast(getContext(), getString(com.bhaskar.login.R.string.internet_connection_error_));

        }
    }

    private JSONObject getSignupRequestParams(String otp) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", mProfileInfo.name);
            jsonObject.put("email", mProfileInfo.email);
            jsonObject.put("app_id", CommonLoginConstants.BHASKAR_APP_ID);
            jsonObject.put("country_code", CommonLoginConstants.COUNTRY_CODE);
            jsonObject.put("mobile", mProfileInfo.mobile);
            jsonObject.put("pass", mProfileInfo.password);
            jsonObject.put("otp", otp);
            jsonObject.put("login_type", mProfileInfo.loginType);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void parseSignupJson(JSONObject response, ProfileInfo profileInfo, Dialog dialog) {
        String status = response.optString("status");
        String message = response.optString("msg");
        if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("Success")) {
            try {
                JSONObject dataJsonObject = response.getJSONObject("data");
                profileInfo.uID = dataJsonObject.optString("uid");
                profileInfo.uuID = dataJsonObject.optString("uuid");
                profileInfo.name = dataJsonObject.optString("name");
                profileInfo.email = dataJsonObject.optString("email");
                profileInfo.mobile = dataJsonObject.optString("mobile");
                profileInfo.countryCode = dataJsonObject.optString("country_code");
                profileInfo.country = dataJsonObject.optString("country");
                profileInfo.state = dataJsonObject.optString("state");
                profileInfo.city = dataJsonObject.optString("city");
                profileInfo.zipcode = dataJsonObject.optString("zipcode");
                profileInfo.gender = dataJsonObject.optString("gender");
                profileInfo.dob = dataJsonObject.optString("dob");
                profileInfo.profilePic = dataJsonObject.optString("user_pic");
                profileInfo.isUserLoggedIn = true;
                LoginPreferences.getInstance(getContext()).saveDataIntoPreferences(profileInfo, LoginPreferences.Keys.CURRENT_USER);
                LoginUtil.getInstance().showCustomToast(getContext(), message);
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.SUCCESS, AppFlyerConst.GALabel.OTP);
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.REGISTER, LoginController.loginController().getLoginType(profileInfo));

                if (dialog != null && isAdded())
                    dialog.dismiss();

                startActivity(LoginCommonActivity.getIntent(getContext(), LoginCommonActivity.PERSONAL_INFO, profileInfo));
                if (isAdded())
                    Objects.requireNonNull(getActivity()).finish();
            } catch (Exception e) {
                e.printStackTrace();
                LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.OTP);
            }
        } else {
            LoginController.loginController().sendTrackingForLogin(AppFlyerConst.GAAction.FAIL, AppFlyerConst.GALabel.OTP);
            LoginUtil.getInstance().showCustomToast(getContext(), message);
        }
    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {
        LoginController.loginController().sendGAScreen(getContext(), AppFlyerConst.GAScreen.SIGNUP);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_MOBILE_NO_PICKER) {
            if (resultCode == Activity.RESULT_OK) {
                setMobileNumber(data);
            }
        }
        edtMobile.setOnFocusChangeListener(null);

    }

    private void setMobileNumber(Intent data) {
        try {
            Credential cred = data.getParcelableExtra(Credential.EXTRA_KEY);
            String mobileNo = cred.getId();
            if (!TextUtils.isEmpty(mobileNo)) {
                mobileNo = mobileNo.trim().replace(" ", "");
                mobileNo = mobileNo.substring(mobileNo.length() - 10);
                edtMobile.setText(mobileNo);
                edtMobile.setSelection(mobileNo.length() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface SignupInteraction {
        void onFacebook();

        void onGoogle();

        void onSwitch(int type);

        void onSignupServer(ProfileInfo profileInfo);

        void onMobileNo(Fragment fragment);
    }
}
