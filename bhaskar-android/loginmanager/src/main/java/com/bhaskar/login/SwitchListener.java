package com.bhaskar.login;

import androidx.fragment.app.Fragment;

public interface SwitchListener {
    void onSwitchFragment(Fragment fragment, boolean addToStack, boolean animate);
}
