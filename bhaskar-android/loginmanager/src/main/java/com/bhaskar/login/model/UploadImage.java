package com.bhaskar.login.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UploadImage implements Serializable {

    @SerializedName("status")
    @Expose
    private String status="";
    @SerializedName("code")
    @Expose
    private String code="";
    @SerializedName("filename")
    @Expose
    private String filename="";
    @SerializedName("msg")
    @Expose
    private String msg="";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}

