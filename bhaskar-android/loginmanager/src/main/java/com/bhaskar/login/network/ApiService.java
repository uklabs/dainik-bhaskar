package com.bhaskar.login.network;

import com.bhaskar.login.model.UploadImage;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {

    @Multipart
    @POST("UploadUserPic/")
    Call<UploadImage> upload(
            @Part MultipartBody.Part file
    );
}
