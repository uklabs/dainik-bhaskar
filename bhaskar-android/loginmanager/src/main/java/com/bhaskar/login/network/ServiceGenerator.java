package com.bhaskar.login.network;

import android.content.Context;

import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.LoginController;
import com.bhaskar.login.CommonLoginConstants;
import com.bhaskar.login.utility.LoginUrls;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {


    private static Retrofit retrofit;


    public static <S> S createService(
            Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

    public static Retrofit getClient(final Context context) {

        if (retrofit != null) {
            return retrofit;
        }

        String device_id = TrackingData.getDeviceId(context);
        String dbId = TrackingData.getDBId(context);
        String finalDbId = (dbId == null) ? "" : dbId;
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
                        String uuid = (profileInfo == null) ? "" : profileInfo.uuID;
                        Request request = chain.request();
                        Request.Builder builder = request.newBuilder()
                                .addHeader("api-key", CommonLoginConstants.APP_KEY)
                                .addHeader("device-type", CommonLoginConstants.DEVICE_TYPE)
                                .addHeader("content-type", CommonLoginConstants.CONTENT_TYPE_MULTIPART)
                                .addHeader("accept", "application/json")
                                .addHeader("app-id", CommonLoginConstants.BHASKAR_APP_ID)
                                .addHeader("db-id", LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.DB_ID, String.class))
                                .addHeader("device-id", LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.DEVICE_ID, String.class))
                                .addHeader("uuid", (profileInfo == null ? "" : profileInfo.uuID))
                                .addHeader("uid", (profileInfo == null ? "" : profileInfo.uID))
                                .method(request.method(), request.body());
                        return chain.proceed(builder.build());
                    }
                })
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);


        OkHttpClient okHttpClient = okHttpBuilder.build();
        retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(LoginUrls.LOGIN_FEED_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient()
                        .create()))
                .build();
        return retrofit;
    }


}
