package com.bhaskar.login.utility;

import com.db.util.Urls;

public class LoginUrls {
    public static String LOGIN_FEED_BASE_URL = Urls.APP_FEED_BASE_URL;//"https://appfeedlight.bhaskar.com/appFeedV3/";

    public static String LOGIN_URL = "Login/";
    public static String SIGNUP_URL = "Signup/";
    public static String FORGOT_PASSWORD_URL = "ForgetPassword/";
    public static String OTP_REQUEST_URL = "RequestOtp/";
    public static String UPDATE_PROFILE_URL = "ProfileUpdate/";
}
