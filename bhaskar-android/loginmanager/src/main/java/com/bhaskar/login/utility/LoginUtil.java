package com.bhaskar.login.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.login.CommonLoginConstants;
import com.bhaskar.login.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

public class LoginUtil {
    private static LoginUtil _LoginUtil;

    public static LoginUtil getInstance() {
        if (_LoginUtil == null) {
            synchronized (LoginUtil.class) {
                _LoginUtil = new LoginUtil();
            }
        }
        return _LoginUtil;
    }

    public void showCustomToast(Context context, String message) {
        try {
            if (context != null) {
                View layout = LayoutInflater.from(context).inflate(R.layout.layout_custom_login_toast, null);
                TextView text = layout.findViewById(R.id.toast_textview);
                text.setText(message);

                Toast toast = new Toast(context);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        } catch (Exception e) {
        }
    }

    public HashMap<String, String> getNetworkHeader(Context context) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("api-key", CommonLoginConstants.APP_KEY);
        headers.put("device-type", CommonLoginConstants.DEVICE_TYPE);
        headers.put("content-type", CommonLoginConstants.CONTENT_TYPE);
        headers.put("accept", "application/json");
        headers.put("app-id", CommonLoginConstants.BHASKAR_APP_ID);
        headers.put("db-id", LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.DB_ID, String.class));
        headers.put("device-id", LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.DEVICE_ID, String.class));
        ProfileInfo profileInfo = LoginPreferences.getInstance(context).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
        headers.put("uuid", (profileInfo == null ? null : profileInfo.uuID));
        headers.put("uid", (profileInfo == null ? null : profileInfo.uID));
        Systr.println(String.format(" Feed API Header %s", headers.toString()));
        return headers;
    }


    public boolean isEmailValid(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public String getProperText(TextView textView) {
        if (textView == null)
            return "";
        return textView.getText().toString().trim();
    }

    public void setStatusBarColor(Activity activity, int color) {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = activity.getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color);
            }
        } catch (Exception e) {
        }
    }


    /**
     * Hides the already popped up keyboard from the screen.
     *
     * @param context
     */
    public void hideKeyboard(Context context) {
        try {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    public void showConfirmDialog(Context ctx, String message, DialogInterface.OnClickListener yesListener, DialogInterface.OnClickListener noListener) {
        showConfirmDialog(ctx, message, yesListener, noListener, "Yes", "No");
    }

    public ProgressDialog generateProgressDialog(Context context, boolean cancelable) {
        ProgressDialog progressDialog = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            progressDialog = new ProgressDialog(context, R.style.ProgressThemeLogin);
        else
            progressDialog = new ProgressDialog(context, R.style.AlertDialog_Holo_Login);
        //progressDialog.setMessage("Loading..");
        progressDialog.setCancelable(cancelable);
        return progressDialog;
    }

    public File compressImage(File file) {

        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 100;        // x............

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            selectedBitmap = modifyOrientation(file.getAbsolutePath());
            inputStream.close();

            // here i override the original image file
            File outPutFile = File.createTempFile("Image_" + Calendar.getInstance().getTimeInMillis(), "image");
            FileOutputStream outputStream = new FileOutputStream(outPutFile);
            // y.......
            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 30, outputStream);

            return outPutFile;
        } catch (Exception e) {
            return null;
        }
    }


    public static Bitmap modifyOrientation(String image_absolute_path) throws IOException {
        Bitmap bitmap = BitmapFactory.decodeFile(image_absolute_path);
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }


    public static void showConfirmDialog(Context ctx, String message, DialogInterface.OnClickListener yesListener, DialogInterface.OnClickListener noListener, String yesLabel, String noLabel) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);

        if (yesListener == null) {
            yesListener = new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
        }

        if (noListener == null) {
            noListener = new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
        }
        builder.setMessage(message).setPositiveButton(yesLabel, yesListener).setNegativeButton(noLabel, noListener);
        AlertDialog alert = builder.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                setfontAlert(alert.getButton(Dialog.BUTTON_NEGATIVE));
                setfontAlert(alert.getButton(Dialog.BUTTON_POSITIVE));
            }
        });
        alert.show();
    }

    private static void setfontAlert(Button button) {
        button.setTextSize(16);
        button.setTypeface(null, Typeface.NORMAL);
    }


//    public static HashMap<String, Object> getUserDataMap() {
//        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
//
//        HashMap<String, Object> userMap = new HashMap<>();
//        if (profileInfo != null) {
//            userMap.put("email", profileInfo.email);
//            userMap.put("mobile", profileInfo.mobile);
//            userMap.put("isUserLoggedIn", profileInfo.isUserLoggedIn);
//        }
//        return userMap;
//    }

}
