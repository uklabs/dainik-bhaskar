package com.bhaskar.login;

public class CommonLoginConstants {
    public static final String APP_ID = "1463";
    public static final String DEVICE_TYPE = "ANDROID";
    public static final String APP_KEY = "bh@@$k@r-D";
    public static final String CONTENT_TYPE = "application/json charset=utf-8";
    public static final String BHASKAR_APP_ID = "16";
    public static final String EVENT_LABEL = "mb";
    public static final String COUNTRY_CODE = "91";
    public static final String CONTENT_TYPE_MULTIPART = " multipart/form-data";

    /*app widget & sticky api feed url*/
    public static String USER_AGENT = "MoneyBhaskarAndroid";

    public interface AppIds {
        String DAINIK_BHASKAR = "521";
        String DIVYA_BHASKAR = "960";
        String MONEY_BHASKAR = "1463";
        String DIVYA_MARATHI = "5483";
    }
}
