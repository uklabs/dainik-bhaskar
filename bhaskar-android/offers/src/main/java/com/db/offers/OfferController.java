package com.db.offers;

import android.content.Context;

import com.db.offers.common.DBSharedPref;
import com.db.offers.common.activity.OfferTranslucentActivity;
import com.db.offers.common.rxbus.RxBusImpl;
import com.db.offers.megaoffers.sqldb.MegaOfferSqlAdapter;
import com.google.android.gms.analytics.Tracker;

public class OfferController {
    private static final OfferController ourInstance = new OfferController();
    private RxBusImpl mRxBus;
    private MegaOfferSqlAdapter megaOfferSqlAdapter;
    private static Context mContext;
    private Tracker mTracker;
    private DBSharedPref dbSharedPref;

    public static OfferController getInstance() {
        return ourInstance;
    }

    public void initialize(Context context, Tracker tracker) {
        mContext = context;
        mTracker = tracker;
        mRxBus = new RxBusImpl();
        dbSharedPref = new DBSharedPref(mContext);
        initMegaDataBase(mContext);

    }

    public static Context getContext() {
        return mContext;
    }

    public RxBusImpl getRxBus() {
        return mRxBus;
    }


    public MegaOfferSqlAdapter getMegaOfferAdapter() {
        if (megaOfferSqlAdapter == null) {
            initMegaDataBase(mContext);
        }
        return megaOfferSqlAdapter;
    }

    public Tracker getDefaultTracker() {
        return mTracker;
    }

    private void initMegaDataBase(Context context) {
        megaOfferSqlAdapter = new MegaOfferSqlAdapter(context, context.getString(R.string.mega_offer_db_name), context.getResources().getInteger(R.integer.mega_offer_db_version));
        megaOfferSqlAdapter.open();
    }


    private OfferController() {
    }

    public DBSharedPref getDbSharedPref() {
        if (dbSharedPref == null) {
            dbSharedPref = new DBSharedPref(mContext);
        }
        return dbSharedPref;
    }

    public void openOfferWithAction(Context context, String offerAction) {
        context.startActivity(OfferTranslucentActivity.getIntent(context, offerAction));
    }

}
