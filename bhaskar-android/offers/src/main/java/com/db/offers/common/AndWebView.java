package com.db.offers.common;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.WebView;

import androidx.annotation.RequiresApi;

public class AndWebView extends WebView {

    public AndWebView(Context context) {
        super(context);
    }

    public AndWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AndWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AndWebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /*@Override
    public boolean onCheckIsTextEditor() {
        return true;
    }*/
}
