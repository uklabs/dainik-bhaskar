/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common;


import androidx.annotation.IntDef;

public class ApiConstants {

    public static final int API_SUCCESS = 200; // Success code
    public static final int API_INTERNAL_SERVER_ERROR = 500;
    public static final int API_BAD_REQUEST = 400; // Bad request
    public static final int API_UNAUTHORIZED_ACCESS = 401; // Unauthorised access
    public static final int API_FORBIDDEN_ACCESS = 403; // Forbidden access
    public static final int API_NOT_FOUND = 404; // Error not found

    public static final int API_TEST = 2;
    public static final int API_AREAS_OF_INTEREST = 3;
    public static final int API_REGION = 4;
    public static final int API_CONTEST_LIST = 5;
    public static final int API_CONTEST_DETAIL = 6;
    public static final int API_LIVE_TV_LIST = 7;
    public static final int API_USER_PROFILE = 8;
    public static final int API_FORGOT_PASSWORD = 9;
    public static final int API_LOGIN_REQUEST = 10;
    public static final int API_AREAS_OF_INTEREST_UPDATED = 11;
    public static final int API_REGION_UPDATED = 12;
    public static final int API_UPDATE_USER_PROFILE = 13;
    public static final int API_REGISTRATION = 14;
    public static final int API_GUEST_USER = 15;
    public static final int API_GENERATE_CAPTCHA = 16;
    public static final int API_FOR_YOU = 17;
    public static final int API_NORMAL_SEARCH = 18;
    public static final int API_NEWS_BEEP_LIST = 19;
    public static final int API_NEWS_LIST = 20;
    public static final int API_NEWS_DETAILS = 21;
    public static final int API_RELATED_NEWS_LIST = 22;
    public static final int API_NEWS_BOOKMARK = 23;
    public static final int API_IMAGE_LIST = 24;
    public static final int API_VIDEO_LIST = 25;
    public static final int API_IMAGE_DETAIL = 26;
    public static final int API_PANORAMA_LIST = 27;
    public static final int API_NEWS_ROUND_UP = 28;
    public static final int API_EVENTS = 29;
    public static final int API_COMMENT_LIST = 30;
    public static final int API_ADD_COMMENT = 31;
    public static final int API_EDIT_COMMENT = 32;
    public static final int API_DELETE_COMMENT = 33;
    public static final int API_COMMENT_ACTIONS = 34;
    public static final int API_VIDEO_DETAIL = 35;
    public static final int API_ABOUT_US = 36;
    public static final int API_TERMS_AND_CONDITIONS = 37;
    public static final int API_HOME_LISTING = 38;

    // CJ - Citizen Journalism
    public static final int API_CJ_LIST = 39;
    public static final int API_CJ_MYSTORY = 40;
    public static final int API_BOOKMARK_LIST = 41;
    public static final int API_EDIT_HOME_CATEGORIES = 42;
    public static final int API_UPDATE_HOME_CATEGORY_LIST = 43;
    public static final int API_PROFILE_MASTERS = 44;
    public static final int API_CHANGE_PASSWORD = 45;
    public static final int API_UPLOAD_PROFILE_IMAGE = 46;
    public static final int API_WEATHER_DETAIL = 47;
    public static final int API_AUTHOR_LIST = 48;
    public static final int API_UPDATE_AUTHOR_LIST = 49;
    public static final int API_TAG_LIST = 50;
    public static final int API_UPDATE_TAGS = 51;
    public static final int API_PROFILE_SETTINGS = 52;
    public static final int API_RASHIFAL_DAILY = 53;
    public static final int API_RASHIFAL_WEEKLY = 54;
    public static final int API_RASHIFAL_MONTHLY = 55;
    public static final int API_RASHIFAL_YEARLY = 56;
    public static final int API_AR_LIST = 57;
    //Games API
    public static final int API_GET_GAMES_LIST = 58;
    public static final int API_NEWS_TAG_LIST = 59;
    public static final int API_CHIT_CHAT = 60;
    public static final int API_TRENDING_TAG_LIST = 61;
    public static final int API_STATES = 62;
    public static final int API_SOCIAL_LOGIN = 63;
    public static final int API_SEND_FEED_BACK = 64;
    public static final int API_CREATE_CITIZENJORNALISUM = 65;
    public static final int API_UPLOAD_IMAGE = 66;
    public static final int API_UPLOAD_VIDEO = 67;
    //Preferred City API
    public static final int API_REGION_LIST = 68;
    public static final int API_UPDATE_REGION_LIST = 69;
    public static final int API_LIVE_TV_DETAILS = 70;
    // TRAFFIC api
    public static final int API_UPDATE_USER_LOCATION = 71;
    public static final int API_GET_USER_LOCATION = 72;
    public static final int API_NEWS_READ = 73;
    public static final int API_NEWS_SAVED = 74;
    public static final int API_RECENT_READ_NEWS = 75;
    //EVENTS
    public static final int API_EVENT_MOVIES = 76;
    public static final int API_MOVIE_REGION = 77;
    public static final int API_MOVIE_EVENT_DATE = 78;
    public static final int API_LIVE_BLOG_LIST = 79;

    public static final int API_LOYALTY_PROGRAM = 80;
    public static final int API_LOYALTY_LEADERBOARD = 81;
    public static final int API_LIVE_BLOG_DETAILS = 82;
    public static final int API_LOYALTY_SUMMARY = 83;
    public static final int API_SOCIAL_CONNECT = 84;
    public static final int API_SET_PASSWORD = 85;
    public static final int API_FEED_BACK_SUBJECT = 86;
    public static final int API_RASHIFAL_FROM_HOME = 87;
    public static final int API_EVENT_DATE = 88;
    public static final int API_AUTHOR_NEWS_LIST = 89;
    public static final int API_REGION_LIST_HOME = 90;

    public static final int API_RELATED_VIDEO_LIST = 91;
    public static final int API_APP_CONFIG = 92;
    public static final int API_USER_CATEGORY_UPDATE = 93;
    public static final int API_SUBSCRIBE_PLAN = 94;
    public static final int API_SENSEX_INFO = 95;
    public static final int API_SUBSCRIPTION_LIST = 96;
    public static final int API_NOTIFICATION_TOKEN = 97;
    public static final int API_LOGOUT_REQUEST = 98;

    public static final int API_PHOTO_BOOKMARK = 99;
    public static final int API_NOTIFICATION_TYPE = 100;
    public static final int API_NOTIFICATION_LIST = 101;
    public static final int API_SUBSCRIBE_PLAYSTORE_PLAN = 102;
    public static final int API_CJ_DETAIL = 103;
    public static final int API_RESTORE_SUBSCRIPTION = 105;
    public static final int API_BLOCKEDUSER_LOGOUT_REQUEST = 104;
    public static final int API_GET_COUPON_AVAILABLE = 106;
    public static final int API_NOTIFICATION_TYPE_TABS = 107;

    public static final int API_NEWS_BULLETIN = 109;
    public static final int API_UPDATE_USER_GEO_LOCATION = 110;
    public static final int API_NEWS_DETAIL_FROM_BG = 111;
    public static final int API_GET_PROMO_CODES = 112;
    public static final int API_UNLOCK_PROMO_CODE = 113;
    public static final int API_COUPON_OFFER_STATUS = 114;
    public static final int API_REGISTRATION_VALIDATION = 115;
    public static final int API_NEWS_BULLETIN_DETAILS = 116;
    public static final int API_ARTICLE_USER_PROPS = 117;
    public static final int API_FOLLOWING_NOTIFICATION = 118;
    public static final int API_FLICKR_LISTING = 119;
    public static final int API_OVERLAY_VIDEO_LIST = 120;

    public static final int API_MENU_SPLASH_BACK = 1;
    public static final int API_MENU_GUEST_BACK = 121;
    public static final int API_MENU_SPLASH = 122;
    public static final int API_MENU = 123;

    public static final int NORMAL_LOGOUT_SUCCESS = 201;
    public static final int NORMAL_LOGOUT_FAILED = 202;

    public static final String MULTIPART_BODY_TYPE = "multipart/form-data";
    public static String UNKNOWN_ERROR_MESSAGE = "Unknown error";
    public static String COMMENT_ACTION_LIKE = "like";
    public static String COMMENT_ACTION_UNLIKE = "unlike";
    public static String COMMENT_ACTION_FLAG = "flag";
    public static final String NORMAL_LOGOUT = "1";


    @IntDef({API_TEST, API_MENU_SPLASH_BACK, API_AREAS_OF_INTEREST, API_AREAS_OF_INTEREST_UPDATED,
            API_REGION, API_REGION_UPDATED, API_CONTEST_LIST, API_CONTEST_DETAIL,
            API_LIVE_TV_LIST, API_USER_PROFILE, API_FORGOT_PASSWORD, API_LOGIN_REQUEST,
            API_REGISTRATION, API_UPDATE_USER_PROFILE, API_GUEST_USER, API_GENERATE_CAPTCHA,
            API_FOR_YOU, API_NORMAL_SEARCH, API_NEWS_BEEP_LIST, API_NEWS_LIST, API_NEWS_DETAILS,
            API_RELATED_NEWS_LIST, API_NEWS_BOOKMARK, API_IMAGE_LIST, API_VIDEO_LIST,
            API_IMAGE_DETAIL, API_HOME_LISTING, API_PANORAMA_LIST, API_NEWS_ROUND_UP, API_EVENTS,
            API_CJ_LIST, API_PROFILE_MASTERS, API_CJ_MYSTORY, API_BOOKMARK_LIST,
            API_EDIT_HOME_CATEGORIES, API_UPDATE_HOME_CATEGORY_LIST, API_TERMS_AND_CONDITIONS,
            API_ABOUT_US, API_VIDEO_DETAIL, API_COMMENT_ACTIONS, API_DELETE_COMMENT,
            API_EDIT_COMMENT, API_ADD_COMMENT, API_COMMENT_LIST, API_CHANGE_PASSWORD,
            API_UPLOAD_PROFILE_IMAGE, API_WEATHER_DETAIL, API_AUTHOR_LIST, API_UPDATE_AUTHOR_LIST,
            API_TAG_LIST, API_TRENDING_TAG_LIST, API_UPDATE_TAGS, API_PROFILE_SETTINGS,
            API_RASHIFAL_DAILY, API_RASHIFAL_WEEKLY, API_RASHIFAL_MONTHLY, API_RASHIFAL_YEARLY,
            API_AR_LIST, API_GET_GAMES_LIST, API_NEWS_TAG_LIST, API_CHIT_CHAT, API_STATES,
            API_SEND_FEED_BACK, API_SOCIAL_LOGIN, API_UPLOAD_IMAGE, API_UPLOAD_VIDEO, API_REGION_LIST,
            API_CREATE_CITIZENJORNALISUM, API_LIVE_TV_DETAILS, API_UPDATE_REGION_LIST,
            API_UPDATE_USER_LOCATION, API_GET_USER_LOCATION, API_LOYALTY_PROGRAM, API_NEWS_READ,
            API_NEWS_SAVED, API_RECENT_READ_NEWS, API_EVENT_MOVIES, API_MOVIE_REGION, API_MOVIE_EVENT_DATE,
            API_LIVE_BLOG_LIST, API_LOYALTY_LEADERBOARD, API_LOYALTY_SUMMARY, API_LIVE_BLOG_DETAILS,
            API_SOCIAL_CONNECT, API_SET_PASSWORD, API_FEED_BACK_SUBJECT, API_RASHIFAL_FROM_HOME,
            API_EVENT_DATE, API_AUTHOR_NEWS_LIST, API_REGION_LIST_HOME, API_RELATED_VIDEO_LIST,
            API_APP_CONFIG, API_USER_CATEGORY_UPDATE, API_SUBSCRIBE_PLAN, API_SUBSCRIPTION_LIST,
            API_SENSEX_INFO, API_NOTIFICATION_TOKEN, API_LOGOUT_REQUEST, API_PHOTO_BOOKMARK, API_NOTIFICATION_LIST,
            API_NOTIFICATION_TYPE, API_SUBSCRIBE_PLAYSTORE_PLAN, API_CJ_DETAIL, API_BLOCKEDUSER_LOGOUT_REQUEST, API_RESTORE_SUBSCRIPTION,
            API_GET_COUPON_AVAILABLE, API_NOTIFICATION_TYPE_TABS, API_NEWS_BULLETIN, API_UPDATE_USER_GEO_LOCATION,
            API_NEWS_DETAIL_FROM_BG, API_GET_PROMO_CODES, API_UNLOCK_PROMO_CODE, API_COUPON_OFFER_STATUS,
            API_REGISTRATION_VALIDATION, API_ARTICLE_USER_PROPS, API_NEWS_BULLETIN_DETAILS,
            API_FOLLOWING_NOTIFICATION, API_FLICKR_LISTING, API_MENU_GUEST_BACK, API_MENU_SPLASH,API_MENU})


    public @interface API {

    }

    public interface HeaderKeys {
        String KEY_AUTHORIZATION = "Authorization";
        String KEY_USER_AUTHORIZATION = "User_Authorization";
        String KEY_CHANNEL = "Channel";
        String KEY_DEVICE_TYPE = "device-type";
        String KEY_ACCEPT = "Accept";
        String KEY_APP_VERSION = "app-version";
        String KEY_DB_ID = "DB-Id";
        String KEY_APP_ID = "app-id";
        String KEY_CONTENT_TYPE = "Content-Type";
        String KEY_CONNECTION = "Connection";
        String KEY_X_CSRF_TOKEN = "X-CSRF-Token";
        String KEY_ACCESS_TOKEN = "access-token";
        String KEY_ACCESS_CODE = "access-code";
        String KEY_DEVICE_ID = "device-id";
        String KEY_CITY = "city";
        String KEY_RASHI_NAME = "rashi_name";
        String KEY_GENDER = "gender";
        String KEY_UPDATE_DATE = "Update-Date";
        String KEY_INSTALL_DATE = "Install-Date";
    }


    public interface HeaderValues {
        String VALUE_CHANNEL = "MOBILE";
        String VALUE_DEVICE_TYPE = "ANDROID";
        String VALUE_ACCEPT = "application/json";
        String VALUE_CONTENT_TYPE = "application/json";
        String VALUE_CLOSE = "close";
    }

    public interface ApiResponseStatus {
        String SUCCESS = "Success";
    }

    public interface APIEndPoint {
        String GA_CLIENT_ID = "ga-clevertap/apis/ga";
        String CLEVERTAP_ID = "ga-clevertap/apis/clevertap";
    }
}
