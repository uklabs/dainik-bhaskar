package com.db.offers.common;

import androidx.annotation.IntDef;
import androidx.annotation.StringDef;

public interface AppConstants {
    String MEGA_OFFER = "MegaOffer";
    String TAMBOLA_OFFER = "Tambola_V2";

    @StringDef({MEGA_OFFER, TAMBOLA_OFFER})
    public @interface OfferAction {
    }

    int DEFAULT_BOTTOM_MORE_MENU_WIDTH_PERCENTAGE = 22;

    String PROJECT_ID_FCM = "761575745262";
    String PROJECT_FCM = "FCM";

    String NOT_AVAILABLE = "NA";
    int RESEND_OTP = 60000;
    int LOGIN_API_FAILED = 205;


    String IOS_DB_APP_BUNDLE_ID = "com.DainikBhaskarNew";

    String ENGLISH_LABEL_TYPE_24X7_HELP = "24x7 Help";
    String ENGLISH_LABEL_GAMES = "Games";
    String ENGLISH_LABEL_BOARD_GAMES = "BoardGames";

    String DIALOG_TYPE_REGISTRATION = "dialog_registration";
    String DIALOG_TYPE_CHANGE_PASSWORD = "dialog_change_password";
    String DIALOG_TYPE_FEEDBACK_SUCCESS = "dialog_send_feedback";
    String DIALOG_TYPE_PROFILE_UPDATE_SUCCESS = "dialog_type_profile";

    String CATEGORY_ID_SHAHAR_ME_AAJ = "277";

    int REQUEST_LOCATION_SERVICE = 102, LOCATION_SETTINGS_REQUEST = 104;
    long LOCATION_INTERVAL = 10 * 1000,
            SPLASH_SCREEN_TIME = 2500;

    long SPLASH_SCREEN_TIME_APP_UPDATE = 5000;

    //Registration
    String EMAIL_ID = "emailId";
    String MOBILE_NO = "mobileNo";

    //Donot change values

    int RC_MULTI_CONTAINER_LOGIN_REQUIRED = 1021;

    int RC_MULTI_CONTAINER_CHUNAVI_LOGIN_REQUIRED = 1022;

    int SETTINGS_DB = 1;
    int SETTINGS_ID = 2;
    int SETTINGS_VERSION = 3;
    int MENU_GROUP = 1;
    int MENU_SUB_GROUP = 2;
    int MENU_CHILD = 3;
    int MENU_PROFILE = 4;
    int MENU_HEADER = 5;
    String DIALOG_TYPE_FORGOT_PASSWORD = "dialog_forgot_password";
    //Home category
    int HOME_CATEGORY_MAX_COUNT = 7;
    //Extras
    String EXTRA_SHOW_CATEGORY_SELECTION = "show_category_selection";
    String EXTRA_IS_LAUNCHED_FROM_HOME = "is_launched_from_home";
    //String EXTRA_IS_FOR_FIRST_LAUNCH = "extra_is_first_launch";
    String EXTRA_GALLERY_ID = "extra_gallery_id";
    String EXTRA_IS_LIVE = "extra_is_live";
    //String EXTRA_LAUNCH_CATEGORY_SELECTION = "launch_category_selection";
    String CATEGORY_SELECTION_TYPE = "category_selection_type";
    String EXTRA_LAUNCH_LOGIN_TYPE = "launch_login_type";
    String EXTRA_LABEL = "launch_label";
    String EXTRA_IS_FIRST_LAUNCH = "is_first_launch";
    String CONTENT_TYPE_NEWS = "news";
    String CONTENT_TYPE_VIDEOS = "video";
    String CONTENT_TYPE_CJI = "cji";
    String CONTENT_TYPE_LIVE_BLOG = "liveblog";
    String CONTENT_TYPE_IMAGE = "image";
    String TYPE_LOYALTY="type_loyalty";

    int SCREEN_TYPE_PANORAMA = 1;
    int SCREEN_TYPE_AR_LIST = 2;
    int SCREEN_TYPE_NEWS_LIST = 3;
    int SCREEN_TYPE_LIVE_BLOG_LIST = 4;
    int SCREEN_TYPE_CJ_MY_STORIES = 5;
    int SCREEN_TYPE_CJ_OTHER_STORIES = 6;
    int SCREEN_TYPE_SEARCH_RESULTS = 7;

    int NEWS_TAG = 0;
    int BOOKMARKS = 1;
    int RECENTLY_READ_NEWS = 2;
    int SAVED_OFFLINE = 3;
    int AUTHOR_NEWS_LIST = 4;

    int CZ_MEDIA_IMAGE = 0;
    int CZ_MEDIA__VIDEO = 1;
    int CZ_MEDIA_UNKNOWN = 2;

    int CZ_MEDIA_STATUS_WAITING = 0;
    int CZ_MEDIA_STATUS_INPROGRESS = 1;
    int CZ_MEDIA_STATUS_SUCCESS = 2;
    int CZ_MEDIA_STATUS_FAILED = 3;

    String MENU_TYPE_SHARE_AND_WIN = "menuShareAndWin";
    String MENU_TYPE_SHARE_WEBVIEW_CONTEMT = "menuShareWebViewContent";
    String MENU_TYPE_CHECK_FOR_LOGGED_IN = "menuCheckForLoggedIn";
    String MENU_TYPE_APP_UPDATE_REQUIRED = "menuAppUpdateRequired";
    String MENU_TYPE_WHATSAPP_HELPLINE = "whatsapphelpline";
    String MENU_TYPE_KAHANIYAN = "Kahaniya";
    String MENU_TYPE_LIFE_MANAGEMENT = "Life and Management";
    String MENU_TYPE_JOSH_TALKS = "Josh Talk";
    String MENU_TYPE_FLICKR = "flicker";
    String MENU_TYPE_PHOTO_LIST = "photolist";
    String MENU_TYPE_VIDEO_LIST = "videolist";
    String MENU_TYPE_PROFILE = "profile";
    String MENU_TYPE_CITIZEN_JOURNALISM = "citizenjournalism";

    String MENU_TYPE_SETTING = "setting";
    String MENU_TYPE_MULTI_TAB = "home";
    String MENU_TYPE_CHATBOT = "chatbot";
    String MENU_TYPE_AR_LIST = "arlist";
    String MENU_TYPE_NEWS_BEEP = "newsbeep";
    String MENU_TYPE_NEWS_ROUND_UP = "newsroundup";
    String MENU_TYPE_CONTEST = "contestlist";
    String MENU_TYPE_LIVE_TV = "livetv";
    String MENU_TYPE_SONGS = "songs";
    String MENU_TYPE_GAMES = "gameslist";
    String MENU_TYPE_PANORAMA = "panoramaList";
    String MENU_TYPE_MAUSAM = "mausam";
    String MENU_TYPE_TRAFFIC = "traffic";
    String MENU_TYPE_SHEHER_ME_AAJ = "sh";
    String MENU_TYPE_HOME_REORDER = "homereorder";
    String MENU_TYPE_HOME_TAB = "hometab";
    String MENU_TYPE_NEWS_LIST = "newslist";
    String MENU_TYPE_RASHI_PHAL = "rashiphala";
    String MENU_TYPE_BOOK_MARK = "bookmarklist";
    String MENU_TYPE_SAVED_OFFLINE = "offlinelist";
    String MENU_TYPE_EVENTS = "events";
    String MENU_TYPE_MOVIES = "movies";
    String MENU_TYPE_VIDEOS = "videos";
    String MENU_TYPE_IMAGES = "images";
    String MENU_TYPE_NEWS = "news";
    String MENU_TYPE_APKELIYE = "apkeliye";
    String MENU_TYPE_VIDEO_GALLERY = "videogallery";

    String MENU_TYPE_NEWS_PLUS = "newsplus";
    String MENU_TYPE_MORE = "more";
    String MENU_TYPE_SEARCH = "search";
    String MENU_TYPE_MULTIMEDIA = "multimedia";
    String MENU_TYPE_HOME = "home";
    String MENU_TYPE_CJ_DETAIL = "citizenjournalismdetail";
    String MENU_TYPE_TRENDING_TAG = "trendingtag";
    String MENU_TYPE_TRENDING_LIST = "taglist";
    String MENU_TYPE_SHARE_APP = "shareapp";
    String MENU_TYPE_VIDEO_SETTING = "videosettings";
    String MENU_TYPE_NOTIFICATION = "notification";
    String MENU_TYPE_RECENT_NEWS = "recentnews";
    String MENU_TYPE_RECENT_TOPICS = "recenttopics";
    String MENU_TYPE_AUTHOR_HOME = "authorhome";
    String MENU_TYPE_FEEDBACK = "feedback";
    String MENU_TYPE_TUTORIAL = "tutorial";
    String MENU_TYPE_ABOUT_US = "aboutus";
    String MENU_TYPE_TERMS = "terms";

    String MENU_TYPE_INTERESTS = "interests";
    String MENU_TYPE_LOCATIONS = "locations";
    String MENU_TYPE_SUBSCRIPTION = "subscription";

    String TYPE_NEWS_TAG = "news_tag";
    String TYPE_NEWS_AUTHOR = "news_author";
    String TYPE_SECTION = "section";
    String TYPE_MENU_SECTION = "menusection";
    String TYPE_ASTRO_NOTIFICATION = "astroNotification";
    String TYPE_ASTRO_NAME_GENDER = "astroNameAndGender";
    String MENU_TYPE_BUBBLE_MULTITAB = "bublemultitab";
    String MENU_TYPE_LIVE_BLOG = "liveblog";
    String MENU_TYPE_LIVE_BLOG_LISTING = "livebloglist";
    String MENU_TYPE_WEBVIEW = "webview";
    String MENU_TYPE_INTERNAL_WEBVIEW = "internalwebview";
    String MENU_TYPE_MERA_SHEHAR = "merashehar";
    String TYPE_SEARCH_RESULT = "search_result";
    String MENU_TYPE_CONTEST_DETAIL = "contest";
    String MENU_TYPE_NOTIFICATION_LIST = "notificationlist";
    String MENU_TYPE_HAPTIK = "haptik";
    String MENU_TYPE_VIDEO = "video";
    String MENU_TYPE_DB_SHOP = "shop";
    String MENU_TYPE_MERAPAGE = "merapage";
    String MENU_TYPE_ELECTION_MULTI_TAB = "electionmultitab";
    String MENU_TYPE_ELECTION_BUBBLE_MULTITAB = "electionbublemultitab";
    String MENU_TYPE_NEWS_BULLETIN = "newsbulletin";
    String MENU_TYPE_AUDIO_PODCAST = "audiopodcast";
    String MENU_TYPE_MERASHEHAR = "merashehar";

    String MENU_TYPE_TAMBOLA = "tambola";
    String MENU_TYPE_MEGA_OFFER = "megaoffer";
    String MENU_TYPE_TREASURE_HUNT = "treasurehunt";
    String MENU_TYPE_STAYFIT = "stayfit";
    String MENU_TYPE_EPAPER = "epaper";
    String MENU_TYPE_YUPP_TV = "yupptv";
    String MENU_TYPE_RADIO = "radio";
    String MENU_TYPE_OFFER = "coupon";
    String MENU_TYPE_ASTRO = "Astro";
    String MENU_TYPE_PRIME_GAMES = "Games";


    String LAYOUT_HORIZONTAL_1 = "horizontal1";
    String LAYOUT_HORIZONTAL_2 = "horizontal2";
    String LAYOUT_VERTICAL_1 = "vertical1";
    String LAYOUT_VERTICAL_2 = "vertical2";
    String LAYOUT_SENSEX = "sensex";
    String LAYOUT_RASHIPAL = "rashiphala";
    int DAY = 0;
    int NIGHT = 1;
    int AUTHOR_LIST_ALL = 0;
    int AUTHOR_LIST_FOLLOWED = 1;
    String MEDIA_TYPE_VIDEO = "video/mp4";
    String EXTENSION_TYPE_MP4 = "mp4";
    String EXTENSION_TYPE_M3U8 = "m3u8";

    String MEDIA_TYPE_IMAGE_GIF = "image/gif";
    String MEDIA_TYPE_IMAGE = "image/*";
    String MEDIA_TYPE_FILE = "video/*";
    String PROFILE_FILE = "file";
    //Preferred City News Screen
    String PREFERRED_LOCATION = "preferred_location";
    String CATEGORY_ID = "category_id";

    String KEY_TAB_SELECTION_INDEX = "key_tab_selection_index";
    String KEY_TAB_SELECTION_MENU_NAME = "key_tab_selection_menu_name";

    String CITIZEN_JOURANLISUM_FILE_PATH = "czfile";
    String READ_MORE = "read_more";
    String READ_LESS = "read_less";

    String TYPE_TAMBOLA_DASHBOARD = "TambolaDashboard";
    String NEWS_BULLETIN = "news_bulletin";
    String NEWS_BULLETIN_DETAILS = "news_bulletin_details";

    String REPORT_ABUSE_COMMENT_ID = "report_abuse_comment_Id";
    String REPLY_TO_COMMENT = "reply_to_comment";
    String COMMENT_ACTION = "comment_action";
    String NOTIFICATION_UPDATED = "notification_updated";
    //String NOTIFICATION_LISTING = "notification_listing";
    String DIALOG_TYPE_SUBSCRIPTION_SUCCESS = "dialog_subscription_success";
    String ACTION_NOTIFICATION = "action_notification";
    String CATEGORY_NAME = "categoryName";

    //    int COUPON_FLAG = 1000; // This is require for coupon flow
//    int COUPON_FLAG_SKIP = 1001; // This is require for coupon flow
    int DELAY_WEBVIEW_LOAD_TO_TOP = 100;
    String EXTRA_USER_DATA = "user_data";
    String EXTRA_PAGE_TITLE = "extra_page_title";


    // Salutation Messages
    String SALUTATION_GOOD_MORNING = "Good Morning";
    String SALUTATION_GOOD_AFTERNOON = "Good Afternoon";
    String SALUTATION_GOOD_EVENING = "Good Evening";
    String SALUTATION_SWAGAT_HAI = "Swagat Hai";
    String DATE_PATTERN = "dd-MM-yyyy";
    long SALUTATION_DISMISS_INTERVAL = 20000; // 20 seconds

    //multicontainers
    String CONTAINER_TYPE_WEBVIEW = "webview";
    String CONTAINER_TYPE_IMAGE = "image";
    String CONTAINER_TYPE_AUDIO = "audio";
    String CONTAINER_TYPE_ADS = "ads";
    String CONTAINER_OPEN_TYPE_SECTION = "section";
    String CONTAINER_OPEN_TYPE_URL = "webview";

    String CONTAINER_OPEN_TYPE_OVERLAY_AUDIO = "overlayaudio";
    String CONTAINER_OPEN_TYPE_OVERLAY_VIDEO = "overlayvideo";

    String VIDEO_ADS_CAT_POSTFIX = "V";

    interface BundleParams {
        String TAG_REQUEST = "TAG_REQUEST";
        String TAG = "TAG";
    }

    //chitchat constants
    interface chitchatConstants {
        String LOADER_URL = "https://media.giphy.com/media/tJdR1LhKBenR0p3M6y/giphy.gif";
        String CHAT_BOT_ID = "1";
        String USER_ID = "0";
        int LOADER_DELAY = 500;
        int NORMAL_DELAY = 450;
        byte CONTENT_TYPE_TEXT = 1;
        byte CONTENT_TYPE_IMAGE = 2;
        byte CONTENT_TYPE_HTML = 3;
        int MAX_MESSAGES = 100;
        int MAX_REPLY_VIEW_COUNT = 5;
    }

    interface fullScreenImagePagerConstant {
        String LIVE_BLOG = "live_blog";
        String NEWS_DETAIL = "news_detail";
        String MY_UPLOAD = "my_upload";
    }

//    For Ads

    int ADS_REQUEST_NATIVE_IMAGE_LIST = 100;
    int ADS_REQUEST_NATIVE_NEWS_LIST = 101;
    int ADS_REQUEST_NATIVE_VIDEO_LIST = 102;
    int ADS_REQUEST_NATIVE_RELATED_ARTICLE = 107;
    int ADS_HOME_PAGE_BANNER = 0;
    int ADS_ROS_PAGE_BANNER = 1;
    int ADS_DETAILS_BIG_END = 3;

    //Screen Types
    @IntDef()
    public @interface SCREEN_TYPE {
    }

    @IntDef({SCREEN_TYPE_PANORAMA, SCREEN_TYPE_AR_LIST, SCREEN_TYPE_NEWS_LIST,
            SCREEN_TYPE_LIVE_BLOG_LIST, SCREEN_TYPE_CJ_MY_STORIES, SCREEN_TYPE_CJ_OTHER_STORIES})
    public @interface ScreenType {
    }


    public interface EntryPoint {
        int SUBSCRIPTION = 1;
        int COMMENTS = 2;
        int CITIZENJOURNALISM = 3;
    }

    int MESHA = 1;
    int VRASHABA = 2;
    int MITHUNA = 3;
    int KARAK = 4;
    int SIMHA = 5;
    int KANYA = 6;
    int TULA = 7;
    int VARSHCHIKA = 8;
    int DHANU = 9;
    int MAKARA = 10;
    int KUMBA = 11;
    int MEENA = 12;


    @IntDef({TYPE_DEFAULT, TYPE_DB_LOADER})
    public @interface LoaderType {

    }

    int TYPE_DEFAULT = 0;
    int TYPE_DB_LOADER = 1;

    @StringDef({VOICE_TYPE_MALE, VOICE_TYPE_FEMALE})
    public @interface VoiceType {
    }

    String VOICE_TYPE_MALE = "hi-in-x-cfn#male_3-local";
    String VOICE_TYPE_FEMALE = "hi-in-x-cfn#female_1-local";

    String GOOGLE_TTS_ENGINE = "com.google.android.tts";


    String UPLOAD_APPROVED = "1";
    String UPLOAD_UNDER_MODERATION = "2";
    String UPLOAD_UN_APPROVED = "3";


    @StringDef({LOCATION_TYPE_HOME, LOCATION_TYPE_WORK})
    public @interface LocationType {
    }

    String LOCATION_TYPE_HOME = "HOME";
    String LOCATION_TYPE_WORK = "WORK";

    // This will be used as a default text size for news detail slug & heading
    int DEFAULT_NEWS_SLUG_HEADING_TEXT_SIZE = 20;
    // This will be used as a default text size for news detail slug intro with description
    int DEFAULT_NEWS_DESCRIPTION_TEXT_SIZE = 17;

    int MENU_ITEM_ID_HAPTIK = 1;
    int MENU_ITEM_ID_NOTIFICATION = 2;
    int MENU_ITEM_ID_SHOP = 3;
    int MENU_ITEM_ID_WEB_LINKS = 4;
    int MENU_ITEM_ID_TAMBOLA = 5;
    int MENU_ITEM_ID_TREASURE_HUNT = 6;
    int MENU_ITEM_ID_MEGA_OFFER = 7;

    int REQUEST_LOCATION_PERMISSION = 110;

    String[] CHANNEL_IDS = {"channel_id_sound",
            "channel_id_vibration",
            "channel_id_silent",
            "channel_id_sound_and_vibration"};
    String[] CHANNEL_NAMES = {"Sound",
            "Vibration",
            "Silent",
            "Sound and Vibration"};

    int QUALITY_AUTO = 1;
    int QUALITY_BEST = 2;
    int QUALITY_BETTER = 3;
    int QUALITY_STANDARD = 4;

    int NONE = 0;
    int LOGIN_FOR_COMMENTS = 1;
    int LOGIN_FOR_BOOKMARK_LIST = 2;
    int LOGIN_FOR_OFFLINE = 3;
    int LOGIN_FOR_CJ = 4;
    int LOGIN_FOR_BOOKMARK = 5;
    int LOGIN_FOR_MY_INTEREST = 6;
    int LOGIN_FOR_MENU = 7;
    int LOGIN_FOR_SETTINGS = 8;
    int LOGIN_FOR_TRAFFIC = 9;

    int LOGIN_FOR_AUTHOR_FOLLOW_UNFOLLOW = 10;
    int LOGIN_FOR_PHOTO_BOOKMARK = 11;
    int LOGIN_FOR_VIDEO_BOOKMARK = 12;
    int RECENTLY_READ_NEWS_FOLLOW = 13;
    int SCREEN_TYPE_MERA_PAGE = 14;
    int LOGIN_FOR_SELECT_CITY = 15;

    @IntDef({NONE, LOGIN_FOR_COMMENTS, LOGIN_FOR_BOOKMARK_LIST, LOGIN_FOR_OFFLINE,
            LOGIN_FOR_CJ, LOGIN_FOR_BOOKMARK, LOGIN_FOR_MY_INTEREST, LOGIN_FOR_MENU, LOGIN_FOR_SETTINGS,
            LOGIN_FOR_AUTHOR_FOLLOW_UNFOLLOW, LOGIN_FOR_PHOTO_BOOKMARK,
            LOGIN_FOR_TRAFFIC, RECENTLY_READ_NEWS_FOLLOW, SCREEN_TYPE_MERA_PAGE, LOGIN_FOR_SELECT_CITY,
            LOGIN_FOR_VIDEO_BOOKMARK})
    public @interface SCREEN_CONSTANT {

    }

//    interface FCM {
//        String SENDER_ID = "761575745262";
//        String TOKEN_TYPE = "FCM";
//    }

    interface inAppPurchaseConstant {

//        String base64EncodedAppPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq7+3PiZ12nRxta50nz6MJjtVJN73hCSUdePJiR0t3dSObltmprLfdW8hiaV90lwYsjggXggWYkjEsE3pc4/RDLu5q1FLeI54j0WS71hJzofeN3A6/lQZf3SlX5TByg3ip8hAeWbamuS2fNYnlBr2UnsqRS9IxvagXCukN+Qy1AeVx//5FubPQZ//PcpjOrZ4g6rFti/U+rqYkYtvIu1Z7bz8BVIJnblvg4m17vWEY+ddn4gDJDpIOTqTnpcd2qd13htOTORP/whG9PVad1W+Hwv0hwKhaeIhzIOLqHCbulLvynSVqiAQBkYY8F/Lh8n84i3TjEdp+RDcu382AENJgwIDAQAB";
//
//        String KEY_PACKAGE = "packageName";
//        String KEY_SUBSCRIPTION = "subscriptionId";
//        String KEY_TOKEN = "token";
//
//        /*String SKU_DB_MONTHLY = "db_monthly_subscription";
//        String SKU_DB_3_MONTHLY = "db_3_monthly_subscription";
//        String SKU_DB_6_MONTHLY = "db_6_monthly_subscription";
//        String SKU_DB_YEARLY = "db_yearly_subscription";
//        String SKU_DB_FOR_ONE_MONTH = "db_one_month";
//        String SKU_DB_FOR_ONE_YEAR = "db_one_year";
//
//        String TEST_SKU_DB_MONTHLY = "test_db_monthly_subscription";
//        String TEST_SKU_DB_PRODUCT = "test_db_product_1";
//        String KEY_ORDERID = "orderId";
//        String KEY_DEVELOPERPAYLOAD = "developerPayload";*/
    }

    enum LayoutType {
        VIDEO_GALLERY_RVL, VIDEO_GALLERY_RVG, VIDEO_GALLERY_RVH;
    }

//    interface PiPConst {
//        /**
//         * Intent action for media controls from Picture-in-Picture mode.
//         */
//        String ACTION_MEDIA_CONTROL = "media_control";
//
//        /**
//         * Intent extra for media controls from Picture-in-Picture mode.
//         */
//        String EXTRA_CONTROL_TYPE = "control_type";
//
//        /**
//         * The request code for play action PendingIntent.
//         */
//        int REQUEST_PLAY = 1;
//
//        /**
//         * The request code for pause action PendingIntent.
//         */
//        int REQUEST_PAUSE = 2;
//
//        /**
//         * The intent extra value for play action.
//         */
//        int CONTROL_TYPE_PLAY = 1;
//
//        /**
//         * The intent extra value for pause action.
//         */
//        int CONTROL_TYPE_PAUSE = 2;
//    }

    interface RequestCode {
        int REQUEST_CODE_RETURN_TO_HOME_CONTAINER = 1010;
        int RC_LOGIN_FROM_MEGA_OFFER_ACCEPTED = 1011;
        int RC_LOGIN_FROM_MEGA_OFFER_RELOGIN = 1013;
        int RC_LOGIN_FROM_TAMBOLA = 1012;
        int RC_ACCEPT_MEGA_OFFER_TNC = 1021;
    }

//    interface GetPreferenceKeys {
//        String LATTITUDE = "latitude";
//        String LONGITUDE = "longitude";
//        String CITY = "city";
//        String EMAILID = "email_id";
//        String MOBILE_NUMBER = "mobile_number";
//    }
//
//    /*interface CountDownTimerFormat {
//        String COUNTDOWN_FORMAT = "%02d:%02d";
//        int COUNTDOWN_TIMER_INTERVAL = 1000;
//    }*/
//
//    interface CountDownTimerFormat {
//        String COUNTDOWN_FORMAT = "%02d:%02d";
//        String MONTHLY_OFFER_COUNTDOWN_FORMAT = "%02d";
//        int COUNTDOWN_TIMER_INTERVAL = 1000;
//    }
//
//    enum OpenType {
//        SOCIAL_MEDIA, MOBILE_UPDATE;
//    }
//
//    String OPEN_TYPE = "OpenType";


    interface NavigationFlow {
        int NAVIGATE_FROM_SPLASH = 1;
        int NAVIGATE_FROM_HOME = 2;
    }

    enum PointsAllocationType {
        TYPE_NEWS(1), TYPE_VIDEOS(2), TYPE_BULLETIN(2);

        private int pointIncrementer;

        PointsAllocationType(int pointIncrementer){
            this.pointIncrementer = pointIncrementer;
        }

        public int getPointIncrementer() {
            return pointIncrementer;
        }
    }

    /*int LOGIN_FACEBOOK = 1;
    int LOGIN_GOOGLE = 2;
    int LOGIN_MOBILE = 3;*/

    enum UserLoginType {
        NOT_LOGIN("0", "guest"), LOGIN_FACEBOOK("1", "facebook"),
        LOGIN_GOOGLE("2", "google_plus"), LOGIN_MOBILE("3", "phone");

        String loginType, loginTypeName;

        /**
         *
         */
        UserLoginType(String loginType, String loginTypeName) {
            this.loginType = loginType;
            this.loginTypeName = loginTypeName;
        }

        public static UserLoginType getUserLoginType(String loginType) {
            UserLoginType userLoginTypes[] = UserLoginType.values();
            for (UserLoginType userLoginType : userLoginTypes) {
                if (userLoginType.loginType.equals(loginType)) {
                    return userLoginType;
                }
            }
            return UserLoginType.NOT_LOGIN;
        }

        public String getLoginTypeName() {
            return loginTypeName;
        }

        public String getLoginType() {
            return loginType;
        }
    }

    interface CampaignConstants {
        String SUBJECT = "subject";
        String DESCRIPTION = "description";
        String CONTENT_TYPE = "content_type";
        String REFERRER = "referrer";
        String UTM_SOURCE = "utm_source";
        String UTM_MEDIUM = "utm_medium";
        String UTM_CAMPAIGN = "utm_campaign";
        String UTM_TERM = "utm_term";
        String UTM_CONTENT = "utm_content";

        interface BundleConstants {
            String CAMPAIGN_DATA = "campaign_data";
        }
    }
}

//    int LOGIN_TYPE_GUEST = 4;
//    int LOGIN_TYPE_REGISTERED = 5;
//    int LOGIN_TYPE_SOCIAL = 6;
//    int ADS_DETAILS_BIG_FIRST = 2;
//
//    int ADS_INTERSTITIAL = 4;
//    int ADS_HOME_LIST = 5;
//    int ADS_HOME_PAGE_NATIVE = 6;
//    int ADS_SEARCH_PAGE_BANNER_LARGE = 7;
//
//    int ADS_SIZE_350X50 = 10;
//    int ADS_SIZE_350X100 = 11;
//
//    int ADS_REQUEST_NATIVE_NEW_BRIEF = 103;
//    int ADS_REQUEST_NATIVE_NEW_ROUND_UP = 104;
//    int ADS_REQUEST_NATIVE_NEWS_LIVE_BLOG = 105;
//    int ADS_REQUEST_NATIVE_CONTEST = 106;
//
//    int ADS_REQUEST_NATIVE_VIDEO_LISTING = 108;
//    int ADS_REQUEST_NATIVE_TODAY_IN_CITY = 109;
//
//    int ADS_REQUEST_NATIVE_SUB_TYPE_POSITION_1 = 1;
//    int ADS_REQUEST_NATIVE_SUB_TYPE_POSITION_2 = 2;
//    int ADS_REQUEST_NATIVE_SUB_TYPE_POSITION_3 = 3;
//
//
//    int ADS_TYPE_DB_APP_320x50_BTF_HP = 300;
//    int ADS_TYPE_DB_APP_300x250_ATF_HP = 301;
//    int ADS_TYPE_DB_APP_300x250_ATF_ROS = 302;
//    int ADS_TYPE_DB_APP_300x250_BTF_ROS = 303;
//    int ADS_TYPE_DB_APP_300x250_EXIT = 304;
//    int ADS_TYPE_DB_APP_PHOTO_DETAIL_320x50_BTF = 305;
//    int ADS_TYPE_DB_APP_300x250_BTF_ROS_2 = 306;
//
//    int ADS_TYPE_FB_NATIVE_TOP = 307;
//    int ADS_TYPE_FB_NATIVE_MIDDLE = 308;
//    int ADS_TYPE_FB_NATIVE_BOTTOM = 309;
//    int ADS_TYPE_FB_NATIVE_TOP_ROS = 310;
//    int ADS_TYPE_FB_NATIVE_MIDDLE_ROS = 311;
//
//    int ADS_NETWORK_SERVER_NAME_DFP = 0;
//    int ADS_NETWORK_SERVER_NAME_ADMOB = 1;
//    int ADS_NETWORK_SERVER_NAME_ADX = 2;
//    int ADS_NETWORK_SERVER_NAME_FACEBOOK = 3;
//    /*
//    int SWIPE_LEFT = 0;
//    int SWIPE_RIGHT = 1;
//    int SWIPE_UP = 2;
//    int SWIPE_DOWN = 3;*/
//
//    public interface TestModeConfiguration {
//        boolean isTestModeOn = true;
//    }
//
///*    @IntDef({SWIPE_LEFT, SWIPE_RIGHT, SWIPE_UP, SWIPE_DOWN})
//    public @interface SwipeGesture {
//
//    }*/
//
//    //Login Types
//    @IntDef({LOGIN_FACEBOOK, LOGIN_GOOGLE, LOGIN_TYPE_REGISTERED})
//    public @interface LOGIN_TYPE {
//    }
//
//    public interface CategorySelectionType {
//        int MENU_INTRESTS = 3;
//        int MENU_LOCATIONS = 4;
//    }
//
//    public interface SearchType {
//        String SHEHER_ME_AAJ = "51";
//        String CONTEST = "52";
//        String LIVE_TV = "53";
//        String NEWS = "54";
//        String CITIZEN_GERNALISM = "55";
//    }
//
//
//
//    @IntDef({NEWS_TAG, BOOKMARKS, RECENTLY_READ_NEWS, SAVED_OFFLINE})
//    public @interface ListingType {
//    }
//
//    @StringDef({MENU_TYPE_PHOTO_LIST, MENU_TYPE_VIDEO_LIST, MENU_TYPE_PROFILE, MENU_TYPE_CITIZEN_JOURNALISM,
//            MENU_TYPE_INTERESTS, MENU_TYPE_LOCATIONS, MENU_TYPE_SETTING, MENU_TYPE_MULTI_TAB,
//            MENU_TYPE_CHATBOT, MENU_TYPE_AR_LIST, MENU_TYPE_NEWS_BEEP, MENU_TYPE_NEWS_ROUND_UP,
//            MENU_TYPE_CONTEST, MENU_TYPE_LIVE_TV, MENU_TYPE_SONGS,
//            MENU_TYPE_GAMES, MENU_TYPE_PANORAMA, MENU_TYPE_MAUSAM, MENU_TYPE_TRAFFIC,
//            MENU_TYPE_SHEHER_ME_AAJ, MENU_TYPE_HOME_REORDER, MENU_TYPE_HOME_TAB, MENU_TYPE_NEWS_LIST,
//            MENU_TYPE_RASHI_PHAL, MENU_TYPE_BOOK_MARK, MENU_TYPE_SAVED_OFFLINE, MENU_TYPE_EVENTS,
//            MENU_TYPE_MOVIES, MENU_TYPE_NEWS, MENU_TYPE_IMAGES, MENU_TYPE_VIDEOS,
//            MENU_TYPE_SEARCH, MENU_TYPE_APKELIYE, MENU_TYPE_MORE, MENU_TYPE_HOME, MENU_TYPE_CJ_DETAIL,
//            MENU_TYPE_TRENDING_LIST, MENU_TYPE_TRENDING_TAG, MENU_TYPE_SHARE_APP, MENU_TYPE_YUPP_TV,
//            MENU_TYPE_RADIO, MENU_TYPE_STAYFIT, MENU_TYPE_OFFER, TYPE_SEARCH_RESULT, MENU_TYPE_NOTIFICATION_LIST})
//    public @interface MenuType {
//
//    }
//
//    @StringDef({LAYOUT_HORIZONTAL_1, LAYOUT_HORIZONTAL_2, LAYOUT_VERTICAL_1, LAYOUT_VERTICAL_2,
//            LAYOUT_SENSEX, LAYOUT_RASHIPAL})
//    public @interface LayoutType {
//    }
//
//    @IntDef({CZ_MEDIA_IMAGE, CZ_MEDIA__VIDEO, CZ_MEDIA_UNKNOWN})
//    public @interface CZMediaType {
//    }
//
//    @IntDef({CZ_MEDIA_STATUS_WAITING, CZ_MEDIA_STATUS_INPROGRESS, CZ_MEDIA_STATUS_SUCCESS, CZ_MEDIA_STATUS_FAILED})
//    public @interface CZMediaStatus {
//    }
//
//    @IntDef({MESHA, VRASHABA, MITHUNA, KARAK, SIMHA, KANYA, TULA, VARSHCHIKA, DHANU, MAKARA, KUMBA, MEENA})
//    public @interface Rashiphal {
//    }
//
//    @StringDef({UPLOAD_APPROVED, UPLOAD_UNDER_MODERATION, UPLOAD_UN_APPROVED})
//    public @interface UploadStatus {
//    }
