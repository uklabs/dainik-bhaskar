package com.db.offers.common;

public interface AppNavigationFlag {
    int IS_FROM_MENU = 0;
    int IS_FROM_HOME_TAB = 1;
    int IS_FROM_HOME_POPUP = 2;
}