/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

public class AppNavigatorHelper {

    private static AppNavigatorHelper mHelper;
    private static final String TAG = AppNavigatorHelper.class.getSimpleName();

    public static AppNavigatorHelper getInstance() {
        if (mHelper == null) {
            mHelper = new AppNavigatorHelper();
        }
        return mHelper;
    }

    public void launchHomeActivity(@NonNull Activity activity) {
        try {
            Class homeClass = Class.forName("com.bhaskar.view.ui.MainActivity");
            Intent intent = new Intent(activity, homeClass);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void launchHomeActivityFromMegaOfferDashboard(@NonNull Activity activity) {
        try {
            Class homeClass = Class.forName("com.bhaskar.view.ui.MainActivity");
            Intent intent = new Intent(activity, homeClass);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void launchOnBoardActivity(@NonNull Activity activity, boolean showCategorySelection, boolean isLaunchedFromHome, @AppConstants.SCREEN_CONSTANT int type) {

//        Intent intent = new Intent(activity, UserLoginActivity.class);
//        intent.putExtra(AppConstants.EXTRA_SHOW_CATEGORY_SELECTION, showCategorySelection);
//        intent.putExtra(AppConstants.EXTRA_IS_LAUNCHED_FROM_HOME, isLaunchedFromHome);
//        intent.putExtra(BundleConstants.REDIRECT_SCREEN_TYPE, type);
//        intent.putExtra(BundleConstants.KEY_NAVIGATE_FROM, type);
//        activity.startActivity(intent);
    }

    public void launchNewsBulletinAudioPlayerActivityIndividual(Context context, String newsBulletinNId, int audioType) {

//        Intent intent = new Intent(context, NewsBulletinPlayerActivity.class);
//
//        intent.putExtra(BundleConstants.CATEGORY_TYPE, Constants.URL_TYPE_PODCAST);
//        intent.putExtra(BundleConstants.GALLERY_DATA, newsBulletinNId);
//        intent.putExtra(BundleConstants.AUDIO_TYPE, audioType);
//        context.startActivity(intent);
    }

    public void launchLoginActivity(@NonNull Context activity) {

//        Bundle bundle = new Bundle();
//        bundle.putInt(BundleConstants.KEY_NAVIGATE_FROM, AppConstants.NavigationFlow.NAVIGATE_FROM_HOME);
//        UserLoginActivity.startActivity(activity, bundle);
    }

    /**
     * @param activity
     * @param bundle
     */
    public void launchLoginActivity(@NonNull Context activity, @NonNull Bundle bundle) {
        //bundle.putInt(BundleConstants.KEY_NAVIGATE_FROM, AppConstants.NavigationFlow.NAVIGATE_FROM_HOME);

//        UserLoginActivity.startActivity(activity, bundle);
    }

    /**
     * @param context
     * @param bundle
     * @param requestCode
     */


}
