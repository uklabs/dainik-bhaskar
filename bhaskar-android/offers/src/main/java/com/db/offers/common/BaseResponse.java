package com.db.offers.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseResponse implements GsonProguardMarker {
    @ApiConstants.API
    private int mApi;
    @SerializedName("code")
    public int code;
    @SerializedName("message")
    public String message;
    @SerializedName("time")
    @Expose
    public int time;

    public int getApi() {
        return mApi;
    }

    public void setApi(int api) {
        this.mApi = api;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return super.toString() + "\n"
                + code + "\n"
                + time + "\n"
                + message + "\n";
    }
}
