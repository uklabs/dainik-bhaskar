package com.db.offers.common;

import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class CTEvents {

    @SerializedName("eventName")
    @Expose
    private String eventName;
    @SerializedName("eventLabel")
    @Expose
    private String eventLabel;
    @SerializedName("eventValue")
    @Expose
    private String eventValue;
    @SerializedName("ExtraCTEvents")
    @Expose
    private List<ExtraCTEvent> extraCTEvents = null;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventLabel() {
        return eventLabel;
    }

    public void setEventLabel(String eventLabel) {
        this.eventLabel = eventLabel;
    }

    public String getEventValue() {
        return eventValue;
    }

    public void setEventValue(String eventValue) {
        this.eventValue = eventValue;
    }

    public List<ExtraCTEvent> getExtraCTEvents() {
        return extraCTEvents;
    }

    public void setExtraCTEvents(List<ExtraCTEvent> extraCTEvents) {
        this.extraCTEvents = extraCTEvents;
    }


    @Override
    public String toString() {
        return "CTEvents{" +
                "eventName='" + eventName + '\'' +
                ", eventLabel='" + eventLabel + '\'' +
                ", eventValue='" + eventValue + '\'' +
                ", extraCTEvents=" + extraCTEvents +
                '}';
    }
}