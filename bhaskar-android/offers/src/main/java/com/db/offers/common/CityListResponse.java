/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */
package com.db.offers.common;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * This class acts as data provider model class for Preferred City API
 */
public class CityListResponse extends BaseResponse {

    @SerializedName("data")
    State state;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public static class State {
        @SerializedName("rajya")
        List<Location> locations;

        public List<Location> getLocation() {
            return locations;
        }

        public void setLocation(List<Location> location) {
            this.locations = location;
        }

    }

    public static class Location implements Parcelable {

        @SerializedName("id")
        String id;
        @SerializedName("name")
        String name;
        @SerializedName("label")
        String label;
        @SerializedName("nameEnglish")
        String nameEnglish;
        @SerializedName("childrens")
        List<Location> children;

        public String getNameEnglish() {
            return nameEnglish;
        }

        public void setNameEnglish(String nameEnglish) {
            this.nameEnglish = nameEnglish;
        }

        public List<Location> getChildren() {
            return children;
        }

        public void setChildren(List<Location> children) {
            this.children = children;
        }

        public String getFormattedEnglishName() {
            return formattedEnglishName;
        }

        public void setFormattedEnglishName(String formattedEnglishName) {
            this.formattedEnglishName = formattedEnglishName;
        }

        String formattedHindiName;
        String formattedEnglishName;
        String parentName;
        String parentId;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }


        public Location(String id, String name, List<Location> children, String formattedHindiName, String parentName, String parentId) {
            this.id = id;
            this.name = name;
            this.children = children;
            this.formattedHindiName = formattedHindiName;
            this.parentName = parentName;
            this.parentId = parentId;
        }

        protected Location(Parcel in) {
            id = in.readString();
            name = in.readString();
            children = in.createTypedArrayList(Location.CREATOR);
            formattedHindiName = in.readString();
            parentName = in.readString();
            parentId = in.readString();
        }

        public static final Creator<Location> CREATOR = new Creator<Location>() {
            @Override
            public Location createFromParcel(Parcel in) {
                return new Location(in);
            }

            @Override
            public Location[] newArray(int size) {
                return new Location[size];
            }
        };

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            if (TextUtils.isEmpty(name)) {
                name = getLabel();
            }
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Location> getChildrens() {
            return children;
        }

        public void setChildrens(List<Location> children) {
            this.children = children;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
            dest.writeList(children);
            dest.writeString(formattedHindiName);
            dest.writeString(parentName);
            dest.writeString(parentId);
        }

        public String getFormattedHindiName() {
            return formattedHindiName;
        }

        public void setFormattedHindiName(String formattedName) {
            this.formattedHindiName = formattedName;
        }

        public String getParentName() {
            return parentName;
        }

        public void setParentName(String parentName) {
            this.parentName = parentName;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }
    }


}
