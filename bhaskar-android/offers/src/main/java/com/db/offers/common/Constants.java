package com.db.offers.common;

public class Constants {

    /*public static final String UNKNOWN_ERROR = "UNKNOWN_ERROR";

    public static final String AWS_IDENTITY_POOL_ID = "ap-south-1:429e4684-3b57-4b54-a5a7-ac8cff2fbb9d";
    public static final String SNS_PLATFORM_APPLICATION_ARN = "arn:aws:sns:ap-south-1:375242736309:app/GCM/DainikBhaskarPlusAndroid";
    public static final String NORMAL_NOTIFICATION_ARN = "arn:aws:sns:ap-south-1:375242736309:normal_";
    public static final String CRITICAL_NOTIFICATION_ARN = "arn:aws:sns:ap-south-1:375242736309:DBPlusCriticalNotification";
    public static final String AUTHOR_NOTIFICATION_ARN = "arn:aws:sns:ap-south-1:375242736309:author_";

    public static final String NOTIFICATION_ENABLED = "notification_enabled";
    public static final String DND_ENABLED = "dnd_enabled";
    public static final String DND_START = "dnd_start";
    public static final String DND_END = "dnd_end";*/

    public static final String GEO_LOCATION = "geo_location";
    public static final String SUBADMIN_LOCATION = "subadmin_location";
    public static final String ADMIN_LOCATION = "admin_location";

    public static final String URL_TYPE_VIDEO = "videos";
    public static final String URL_TYPE_PHOTO = "photo";
    public static final String URL_TYPE_CITIZEN_JOURNALISM = "citizen-journalism";
    public static final String URL_TYPE_PODCAST = "podcast";
    public static final String URL_TYPE_AUDIO_PODCAST = "audio-podcast";
    public static final String URL_TYPE_AUDIO_PODCAST_LIST = "audiopodcast";

    public static final String URL_TYPE_LIVE_BLOG = "live-blog";
    public static final String URL_TYPE_IMAGE = "image";
    public static final String URL_TYPE_NEWS_CARD = "news";
    public static final int IMAGE = 1;
    public static final int VIDEO = 2;
    public static final float FABLET_MIN_SCALE = (float) 6.00;
    public static final float FABLET_MAX_SCALE = (float) 7.99;
    public static final String TITLE_NEWS = "News";
    public static final String TITLE_LIVE_TV = "Live Tv";
    static final String TITLE_MY_PAGE = "My Page";
    static final String TITLE_RECENTLY_READ_NEWS = "रिसेंटली रेड न्यूज़";
    public static final String TITLE_TOP_NEWS = "टॉप न्यूज़";
    public static final String TITLE = "Title";
    public static final String TITLE_VIDEO_GALLERY = "वीडियो गैलरी";
    public static final String TITLE_PHOTO_GALLERY = "फोटो गैलरी";
    public static final String EN_TITLE_PHOTO_GALLERY = "PhotoGallery";
    public static final String EN_TITLE_VIDEO_GALLERY = "VideoGallery";

    public interface QueryParams {
        String QUERY_PARAM_ID = "id";
        String QUERY_PARAM_NEWS = "News";
        String QUERY_PARAM_TITLE = "title";
        String QUERY_PARAM_PARAMS = "params";
        String QUERY_PARAM_CATEGORY_ID = "categoryId";
    }

    public interface BundleKeys {
        String KEY_APP_TITLE = "key_app_title";
    }

    public interface DeviceType {
        int SMART_PHONE = 1;
        int FABLET = 2;
        int TABLET = 3;
    }

    public interface ActivityRequestCodes {

        int TAKE_PICTURE = 1;
        int RESULT_LOAD_IMAGE = 1021;

        int REQUEST_CAMERA_STORAGE_PERMISSION = 1012;
        int REQUEST_AR_CAMERA_STORAGE_PERMISSION = 1013;

        int REQUEST_CAMERA_PHOTO = 108;
        int REQUEST_CAMERA_VIDEO = 109;
        int REQUEST_GALLERY_SINGLE_PHOTO = 110;
        int REQUEST_GALLERY_MULTIPLE_PHOTO = 111;
        int REQUEST_GALLERY_SINGLE_VIDEO = 112;
        int REQUEST_GALLERY_MULTIPLE_VIDEO = 113;
    }
}
