package com.db.offers.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContainerTypeModel implements GsonProguardMarker {
    @SerializedName("linkType")
    @Expose
    private String linkType;
    @SerializedName("articleIdId")
    @Expose
    private String articleId;
    @SerializedName("catId")
    @Expose
    private String catId;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("sectionName")
    @Expose
    private String sectionName;
    @SerializedName("subSection")
    @Expose
    private String subSectionName;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("redirectUrl")
    @Expose
    private String videoUrl;

    @SerializedName("audioType")
    @Expose
    private int audioType = BundleConstants.AUDIO_TYPE_NEWS_BULLETIN;

    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("contentType")
    @Expose
    private String contentType;
    @SerializedName("appStoreUrl")
    @Expose
    private String appStoreUrl;

    public String getLinkType() {
        return linkType;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public int getAudioType() {
        return audioType;
    }

    public String getArticleId() {
        return articleId;
    }

    public String getCatId() {
        return catId;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public String getSectionName() {
        return sectionName;
    }

    public String getSubSectionName() {
        return subSectionName;
    }

    public String getGender() {
        return gender;
    }

    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }

    public String getContentType() {
        return contentType;
    }

    public String getAppStoreUrl() {
        return appStoreUrl;
    }
}
