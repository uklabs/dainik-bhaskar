package com.db.offers.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.db.offers.OfferController;
import com.db.offers.megaoffers.constants.MegaOffersConstants;
import com.db.offers.tambola.model.SponsorDetail;
import com.bhaskar.appscommon.tracking.TrackingData;

import java.util.Calendar;
import java.util.List;

public final class DBSharedPref {

    private final SharedPreferences sp;
    private final String KEY_DB_SHARED_PREF = "DB_APP_SHARED_PREF";
    private final String KEY_DB_GOOGLE_ADVERTISING_ID = "key_db_google_advertising_id";
    private final String KEY_DEVICE_ID = "key_device_id";
    private final String KEY_APP_VERSION_CODE = "key_app_version_code";
    private final String KEY_DEVICE_DIMENSIONS = "key_device_dimensions";
    private final String KEY_USER_LOCATION = "key_user_location";
    private final String KEY_NEWS_BRIEF_TUTORIAL = "key_news_brief_tutorial";
    private final String KEY_BOTTOM_MORE_MENU_TUTORIAL = "key_bottom_more_tutorial";
    private final String KEY_DRAW_OVER_OTHER_APPS = "key_draw_over_other_apps";
    private final String KEY_GA_NEW_CLINT_ID = "key_ga_new_client_id";
    private final String KEY_GA_OLD_CLINT_ID = "key_ga_old_client_id";
    private final String KEY_CLEVERTAP_NEW_ID = "key_clevertap_new_id";
    private final String KEY_CLEVERTAP_OLD_ID = "key_clevertap_old_id";
    private final String KEY_REMOTE_APP_SETTINGS = "key_remote_app_settings";
    private final String KEY_RASHI_NAME = "key_rashi_name";
    private final String KEY_GENDER = "key_gender";
    private final String KEY_WHITELIST_WEB_URLS = "key_white_list_web_urls";
    private final String KEY_LOGIN_VIA = "key_login_via";
    private final String KEY_TOP_NEWS = "key_top_news";

    /*****User Keys*****/
    private final String KEY_USER_PROFILE = "key_user_profile";
    private final String KEY_DB_ID = "key_db_id";
    private final String KEY_USER_MOBILE = "key_user_mobile";
    private final String KEY_USER_NAME = "key_user_name";

    private final String KEY_IS_USER_LOGGED_IN = "key_user_logged_in";
    private final String KEY_USER_AUTH_TOKEN = "key_user_auth_token";
    private final String KEY_AUTH_TOKEN = "key_auth_token";
    private final String KEY_BASIC_AUTH_TOKEN = "key_basic_auth_token";

    private final String KEY_NOTIFICATION_ENABLED = "key_notification_enabled";
    private final String KEY_DND_ENABLED = "key_dnd_enabled";
    private final String KEY_DND_START = "key_dnd_start";
    private final String KEY_DND_END = "key_dnd_end";
    private final String KEY_LOGIN_TYPE = "key_login_type";
    private final String KEY_USER_UID = "key_user_uid";

    private final String KEY_CREATED_DATE = "key_app_created_date";
    private final String KEY_CHANGED_DATE = "key_app_changed_date";

    private final String KEY_IS_FIRST_LAUNCH = "key_is_first_launch";

    private final String KEY_ASK_LOCTION_POPUP_COUNTER = "key_location_popup_counter";
    private final String KEY_CAMPAIGN_MODEL = "key_campaign_model";

    private final String KEY_LOGIN_T_AND_C_URL = "key_login_tandc_url";
    private final String KEY_SPONSER_DETAIL = "key_sponser_details";

    /*****User Keys*****/

    /**
     * @param context
     */
    public DBSharedPref(Context context) {
        sp = context.getSharedPreferences(KEY_DB_SHARED_PREF, Context.MODE_PRIVATE);
    }


    /***
     *
     * @param userUid
     */
    private void saveUserUid(String userUid) {
        SharedPrefUtil.addString(sp, KEY_USER_UID, userUid);
    }



    /***
     * Call from Update Profile Page Only
     * @param profile
     */
    public void updateUserProfile(@NonNull Profile profile) {
        SharedPrefUtil.addObjectInGsonString(sp, KEY_USER_PROFILE, profile, Profile.class);
        if (profile.getSettings() != null && profile.getSettings().getNotification() != null) {
            saveNotificationStatus(profile.getSettings().getNotification().getEnabled());
            saveDNDStatus(profile.getSettings().getNotification().getDnd().isEnabled());
            saveDNDStartTime(profile.getSettings().getNotification().getDnd().getStart());
            saveDNDEndTime(profile.getSettings().getNotification().getDnd().getEnd());
        }

        saveUserUid(profile.getUid());

        if (!TextUtils.isEmpty(profile.getMobileNo())) {
            loginUser(profile);
        } else {
            logoutUser();
        }
    }




    public String[] getCreatedAndChangedDate() {
        String[] dates = new String[2];

        dates[0] = SharedPrefUtil.getString(sp, KEY_CREATED_DATE, CommonUtils.getDateWithPattern(Calendar.getInstance().getTime(), MegaOffersConstants.CURRENT_DATE_FORMAT));
        dates[1] = SharedPrefUtil.getString(sp, KEY_CHANGED_DATE, CommonUtils.getDateWithPattern(Calendar.getInstance().getTime(), MegaOffersConstants.CURRENT_DATE_FORMAT));
        return dates;
    }

    /***
     *
     * @param profile
     */
    private void loginUser(@NonNull Profile profile) {
        SharedPrefUtil.addBoolean(sp, KEY_IS_USER_LOGGED_IN, true);
        SharedPrefUtil.addString(sp, KEY_USER_MOBILE, profile.getMobileNo());
        SharedPrefUtil.addString(sp, KEY_USER_NAME, profile.getName());

        List<String> connectFrom = profile.getConnectFrom();
        AppConstants.UserLoginType userLoginType;
        if (connectFrom != null && !connectFrom.isEmpty()) {
            userLoginType = AppConstants.UserLoginType.getUserLoginType(profile.getConnectFrom().get(0));
        } else {
            userLoginType = AppConstants.UserLoginType.NOT_LOGIN;
        }
        SharedPrefUtil.addString(sp, KEY_LOGIN_TYPE, userLoginType.getLoginType());
    }

    public void logoutUser() {
        SharedPrefUtil.removeKeyValue(sp, KEY_IS_USER_LOGGED_IN);
        SharedPrefUtil.removeKeyValue(sp, KEY_USER_MOBILE);
        SharedPrefUtil.removeKeyValue(sp, KEY_LOGIN_TYPE);
        SharedPrefUtil.removeKeyValue(sp, KEY_USER_NAME);
    }

    public Profile getUserProfile() {
        return SharedPrefUtil.getObjectFromGsonString(sp, KEY_USER_PROFILE, Profile.class);
    }



    public String getDbId() {
        return TrackingData.getDBId(OfferController.getContext());
    }

    public boolean isUserLoggedIn() {
        return SharedPrefUtil.getBoolean(sp, KEY_IS_USER_LOGGED_IN, false) && isUserMobileNoVerified();
    }

    private boolean isUserMobileNoVerified() {
        return !TextUtils.isEmpty(getUserVerifiedMobileNo());
    }

    public String getUserVerifiedMobileNo() {
        return SharedPrefUtil.getString(sp, KEY_USER_MOBILE, "");
    }


    public void saveNotificationStatus(boolean isEnabled) {
        SharedPrefUtil.addBoolean(sp, KEY_NOTIFICATION_ENABLED, isEnabled);
    }
    public void saveDNDStatus(boolean isEnabled) {
        SharedPrefUtil.addBoolean(sp, KEY_DND_ENABLED, isEnabled);
    }


    public void saveDNDStartTime(String startTime) {
//        int time = DateTimeUtil.getTimeInt(startTime);
//        SharedPrefUtil.addInt(sp, KEY_DND_START, time);
    }


    public void saveDNDEndTime(String endTime) {
//        int time = DateTimeUtil.getTimeInt(endTime);
//        SharedPrefUtil.addInt(sp, KEY_DND_END, time);
    }


    public LocationModel getUserLocation() {
        return SharedPrefUtil.getObjectFromGsonString(sp, KEY_USER_LOCATION, LocationModel.class);
    }



    public WhitListWebUrlModel getWhitListWebUrlModel() {
        return SharedPrefUtil.getObjectFromGsonString(sp, KEY_WHITELIST_WEB_URLS, WhitListWebUrlModel.class);
    }



    public String getDeviceId() {
        return SharedPrefUtil.getString(sp, KEY_DEVICE_ID, "");
    }


    public String getRashiName() {
        return SharedPrefUtil.getString(sp, KEY_RASHI_NAME, "");
    }



    public String getGender() {
        return SharedPrefUtil.getString(sp, KEY_GENDER, "");
    }



    public void saveSponserDetails(SponsorDetail sponsorDetail) {
        SharedPrefUtil.addObjectInGsonString(sp, KEY_SPONSER_DETAIL, sponsorDetail,SponsorDetail.class);
    }

    public SponsorDetail getSponserDetails() {
        return  SharedPrefUtil.getObjectFromGsonString(sp, KEY_SPONSER_DETAIL, SponsorDetail.class);
    }

    public void clear() {
        SharedPrefUtil.clearSharedPreference(sp);
    }
}
