package com.db.offers.common;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nagesh on 6/7/18.
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */
public class DND implements GsonProguardMarker {
    @SerializedName("enabled")
    private boolean enabled;
    @SerializedName("start")
    private String start;
    @SerializedName("end")
    private String end;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
