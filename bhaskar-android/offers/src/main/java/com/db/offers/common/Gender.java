package com.db.offers.common;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class Gender implements GsonProguardMarker {
    @SerializedName("id")
    @Expose
    private int id = 1;
    @SerializedName("name")
    @Expose
    public String name = "Male";
    @SerializedName("selected")
    @Expose
    public boolean selected = false;

    public String getId() {
        return id + "";
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        try {
            if (obj instanceof Gender) {
                return this.name.equalsIgnoreCase(((Gender) obj).name);
            } else if (obj instanceof String) {
                return this.name.equalsIgnoreCase(obj + "");
            }
        } catch (Exception e) {

        }
        return super.equals(obj);
    }
}
