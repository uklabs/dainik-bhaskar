package com.db.offers.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Base64;
import android.widget.ImageView;

import androidx.annotation.NonNull;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class ImageUtil {

    public static void loadImage(String url, ImageView imageView) {
        loadImage(url, imageView, -1, -1, -1, -1, null);
    }

    public static void loadImage(String url, ImageView imageView, int placeholder) {
        loadImage(url, imageView, -1, -1, placeholder, -1, null);
    }

    public static void loadThumbImage(String url, ImageView imageView, int placeholder) {
        loadThumbImage(url, imageView, -1, -1, placeholder, -1, null);
    }

    public static void loadImage(String url, ImageView imageView, int placeholder, int width, int height) {
        loadImage(url, imageView, width, height, placeholder, -1, null);
    }

    public static void loadImage(String url, ImageView imageView, int width, int height) {
        loadImage(url, imageView, width, height, -1, -1, null);
    }

    public static void loadImage(String url, ImageView imageView, int width, int height, RequestOptions options) {
        loadImage(url, imageView, width, height, -1, -1, options);
    }

    public static void loadImage(String url, ImageView imageView, int placeholder, RequestOptions options) {
        loadImage(url, imageView, -1, -1, placeholder, -1, options);
    }

    public static void loadImage(int imageResId, ImageView imageView) {
        loadImage(imageResId, imageView, -1, -1, -1, -1, null);
    }

    public static void loadImage(int imageResId, ImageView imageView, RequestOptions options) {
        loadImage(imageResId, imageView, -1, -1, -1, -1, options);
    }

    public static void loadImage(int imageResId, ImageView imageView, int placeholder) {
        loadImage(imageResId, imageView, -1, -1, placeholder, -1, null);
    }

    private static void loadImage(String url, ImageView imageView, int width, int height,
                                  int placeholder, int error, RequestOptions options) {
        RequestOptions requestOptions = getRequestOptions(width, height, placeholder, error, options);
        Glide.with(imageView)
                .load(url)
                .apply(requestOptions)
                .into(imageView);
    }

    /****
     *
     * @param url
     * @param imageView
     * @param width
     * @param height
     * @param placeholder
     * @param error
     * @param options
     */
    private static void loadThumbImage(String url, ImageView imageView, int width, int height,
                                       int placeholder, int error, RequestOptions options) {
        RequestOptions requestOptions = getRequestOptions(width, height, placeholder, error, options);
        Glide.with(imageView)
                .load(url)
                .thumbnail(1.0f)
                .apply(requestOptions)
                .into(imageView);
    }

    private static void loadImage(int imageResId, ImageView imageView, int width, int height,
                                  int placeholder, int error, RequestOptions options) {

        RequestOptions requestOptions = getRequestOptions(width, height, placeholder, error, options);

        Glide.with(imageView)
                .load(imageResId)
                .apply(requestOptions)
                .into(imageView);
    }

    @SuppressLint("CheckResult")
    @NonNull
    private static RequestOptions getRequestOptions(int width, int height, int placeholder, int error, RequestOptions options) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        requestOptions.skipMemoryCache(false);
        requestOptions.placeholder(placeholder);
        requestOptions.error(error);

        if (options != null) {
            requestOptions.apply(options);
        }

        if (width > 0 && height > 0) {
            requestOptions.override(width, height);
        }
        return requestOptions;
    }


    public static Bitmap getBitmapFromBase64(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        if (decodedByte != null)
            return decodedByte;
        return null;
    }

    public static Bitmap getBitMapFromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void blurBitmapWithRenderscript(
            RenderScript rs, Bitmap bitmap2) {
        // this will blur the bitmapOriginal with a radius of 25
        // and save it in bitmapOriginal
        // use this constructor for best performance, because it uses
        // USAGE_SHARED mode which reuses memory
        final Allocation input =
                Allocation.createFromBitmap(rs, bitmap2);
        final Allocation output = Allocation.createTyped(rs,
                input.getType());
        final ScriptIntrinsicBlur script =
                ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        // must be >0 and <= 25
        script.setRadius(25f);
        script.setInput(input);
        script.forEach(output);
        output.copyTo(bitmap2);
    }

    public static Bitmap getBitmap(Context context, String imageURL) {
        Bitmap bitmap = null;
        try {
            Drawable drawable = Glide.
                    with(context).
                    load(imageURL).
                    into(100, 100).
                    get();
            bitmap = ((BitmapDrawable) drawable).getBitmap();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
