package com.db.offers.common;

public class LocationModel  {

    protected String cityLat, cityLng, cityName;

    public String getCityLat() {
        return cityLat;
    }

    public void setCityLat(String cityLat) {
        this.cityLat = cityLat;
    }

    public String getCityLng() {
        return cityLng;
    }

    public void setCityLng(String cityLng) {
        this.cityLng = cityLng;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }


    @Override
    public String toString() {
        return "WeatherWidgetModel{" +
                "cityLat='" + cityLat + '\'' +
                ", cityLng='" + cityLng + '\'' +
                ", cityName='" + cityName +
                '}';
    }
}
