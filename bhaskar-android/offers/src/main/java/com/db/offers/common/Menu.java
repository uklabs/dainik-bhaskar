package com.db.offers.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Menu implements GsonProguardMarker {

    @SerializedName("params")
    @Expose
    private String params;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("color")
    @Expose
    private String color;

    @SerializedName("iconActive")
    private String iconActive;

    @SerializedName("childrens")
    @Expose
    private List<Menu> children = null;

    @SerializedName("sensexTicker")
    @Expose
    private Boolean sensexTicker;

    @SerializedName("nameInEnglish")
    @Expose
    private String labelInEnglish;

    @SerializedName("extras")
    @Expose
    private List<String> extrasList;

    public List<Menu> getChildren() {
        return children;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getLabelInEnglish() {
        return labelInEnglish;
    }

    public void setLabelInEnglish(String labelInEnglish) {
        this.labelInEnglish = labelInEnglish;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIconActive() {
        return iconActive;
    }

    public void setIconActive(String iconActive) {
        this.iconActive = iconActive;
    }

    public Boolean getSensexTicker() {
        return sensexTicker;
    }

    public void setSensexTicker(Boolean sensexTicker) {
        this.sensexTicker = sensexTicker;
    }

    public List<String> getExtrasList() {
        return extrasList;
    }
}
