/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */
package com.db.offers.common;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class MenuData implements MenuItemData, Parcelable {

    private String params;
    private List<MenuData> childItems;
    private int position = -1;
    private boolean expanded;
    private int level = -1;
    private String label;
    private String type;
    private String imageUrl;
    private String articleId;
    private Boolean sensexTicker;
    private boolean hasDivider;
    private MenuData parent;
    private boolean fromHome;
    private int selectedChildPosition;
    private String id;
    private String color;
    private String alias;
    private int mScreenNavigationFlag = AppNavigationFlag.IS_FROM_MENU;
    private String labelInEnglish;
    private boolean isFromLink = false;
    private String pageTitle;
    private String catId;
    private String sectionName;
    private String subSectionName;
    // Below menu params is added for share content from multi-container
    private String mSubject;
    private String mDescription;
    private String mShareUrl;
    private String mContentType;
    private String mAppStoreUrl;

    public MenuData(int level) {
        this.level = level;
    }

    public MenuData(String type) {
        this.type = type;
    }

    public MenuData(String type, String subject, String description, String shareUrl, String contentType, String appStoreUrl) {
        this.type = type;
        mSubject = subject;
        mDescription = description;
        mShareUrl = shareUrl;
        mContentType = contentType;
        mAppStoreUrl = appStoreUrl;
    }

    private MenuData(Menu menu, MenuData parent, int parentLevel) {
        if (menu != null) {
            this.type = menu.getType();
            this.imageUrl = menu.getIcon();
            this.sensexTicker = menu.getSensexTicker() == null ? false : menu.getSensexTicker();
            this.label = menu.getLabel() == null ? "" : menu.getLabel();
            this.articleId = menu.getId();
            this.level = parentLevel + 1;
            this.parent = parent;
            this.params = menu.getParams() != null ? menu.getParams() : "";
            this.labelInEnglish = menu.getLabelInEnglish();
            addMenuItems(menu.getChildren());
        }
    }

    public MenuData(Menu menu, int parentLevel, boolean hasDivider) {
        if (menu != null) {
            this.hasDivider = hasDivider;
            this.type = menu.getType();
            this.sensexTicker = menu.getSensexTicker() == null ? false : menu.getSensexTicker();
            this.imageUrl = menu.getIcon();
            this.label = menu.getLabel() == null ? "" : menu.getLabel();
            this.articleId = menu.getId();
            this.level = parentLevel + 1;
            this.params = menu.getParams() != null ? menu.getParams() : "";
            this.color = menu.getColor();
            this.pageTitle = this.label;
            this.labelInEnglish = menu.getLabelInEnglish();
            this.pageTitle = this.label;
            addMenuItems(menu.getChildren());
        }
    }

    public MenuData(Menu menu) {
        if (menu != null) {
            this.type = menu.getType();
            this.sensexTicker = menu.getSensexTicker() == null ? false : menu.getSensexTicker();
            this.imageUrl = menu.getIcon();
            this.label = menu.getLabel() == null ? "" : menu.getLabel();
            this.articleId = menu.getId();
            this.params = menu.getParams() != null ? menu.getParams() : "";
            this.color = menu.getColor();
            this.labelInEnglish = menu.getLabelInEnglish();

            addMenuItems(menu.getChildren());
        }
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public MenuData(String type, String articleId, String title) {
        this.type = type;
        this.articleId = articleId;
        this.label = title;
    }

    public MenuData(String type, String title) {
        this.type = type;
        this.label = title;
        this.isFromLink = AppConstants.TYPE_SEARCH_RESULT.equals(type);
    }

//    public MenuData(NewsCardModel data) {
//        //used for search item click from results screen
//        this.type = data.getUrlType();
//        this.label = data.getTitle();
//        if (AppConstants.MENU_TYPE_NEWS_ROUND_UP.equals(data.getType())
//                || AppConstants.MENU_TYPE_LIVE_BLOG.equals(data.getType())
//                || AppConstants.MENU_TYPE_NEWS_ROUND_UP.equals(data.getType())) {
//            this.articleId = data.getCatId();
//            this.id = data.getArticleId();
//        } else {
//            this.id = data.getCatId();
//            this.articleId = data.getArticleId();
//        }
//        isFromLink = true;
//    }
//
//    public MenuData(SearchResult data) {
//        this.type = data.getType();
//        this.label = data.getTitle();
//        if (AppConstants.MENU_TYPE_NEWS_ROUND_UP.equals(data.getType())
//                || AppConstants.MENU_TYPE_LIVE_BLOG.equals(data.getType())
//                || AppConstants.MENU_TYPE_NEWS_ROUND_UP.equals(data.getType())) {
//            this.articleId = data.getCatId();
//            this.id = data.getId();
//        } else {
//            this.id = data.getCatId();
//            this.articleId = data.getId();
//        }
//        this.isFromLink = true;
//    }
//
//    /***
//     *
//     * @param data
//     */
//    public MenuData(NotificationData data) {
//        this.type = data.getUrlType();
//        this.articleId = data.getId();
//        this.label = data.getPageTitle();
//        this.pageTitle = data.getPageTitle();
//        this.catId = data.getCatId();
//        this.params = data.getParam();
//    }

    protected MenuData(Parcel in) {
        params = in.readString();
        childItems = in.createTypedArrayList(MenuData.CREATOR);
        position = in.readInt();
        expanded = in.readByte() != 0;
        level = in.readInt();
        label = in.readString();
        type = in.readString();
        imageUrl = in.readString();
        articleId = in.readString();
        byte tmpSensexTicker = in.readByte();
        sensexTicker = tmpSensexTicker == 0 ? null : tmpSensexTicker == 1;
        hasDivider = in.readByte() != 0;
        fromHome = in.readByte() != 0;
        isFromLink = in.readByte() != 0;
        selectedChildPosition = in.readInt();
        id = in.readString();
        color = in.readString();
        alias = in.readString();
        pageTitle = in.readString();
        setParentToChildMenuData();
//        Logger.d("MenuData", "Child item");
    }

    public MenuData(String type, String id, String title, String colorCode) {
        this.type = type;
        this.articleId = id;
        this.label = title;
        this.articleId = colorCode;
    }

    public void setParentToChildMenuData() {
        if (childItems == null) return;
        for (MenuData childMenuData : childItems) {
            childMenuData.setParent(this);
            childMenuData.setParentToChildMenuData();
        }
    }

    public static final Creator<MenuData> CREATOR = new Creator<MenuData>() {
        @Override
        public MenuData createFromParcel(Parcel in) {
            return new MenuData(in);
        }

        @Override
        public MenuData[] newArray(int size) {
            return new MenuData[size];
        }
    };

    private void addMenuItems(List<Menu> menuItems) {
        if (menuItems != null) {
            childItems = new ArrayList<>();
            for (Menu menu : menuItems) {
                this.childItems.add(new MenuData(menu, this, level));
            }
        }
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getLabelInEnglish() {
        return labelInEnglish;
    }

    public int getLevel() {
        return level;
    }

    public String getType() {
        return type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public int getViewType() {
        return type.equals(AppConstants.MENU_TYPE_PROFILE) ?
                AppConstants.MENU_PROFILE : level;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    @Override
    public boolean hasChildren() {
        return childItems != null && childItems.size() > 0 &&
                !type.equals(AppConstants.MENU_TYPE_SETTING);
    }

    public String getCatId() {
        return catId;
    }

    @Override
    public boolean isExpanded() {
        return expanded;
    }

    @Override
    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public List<MenuData> getChildItems() {
        return childItems;
    }

    @Override
    public boolean hasDivider() {
        return hasDivider;
    }

    public MenuData getParent() {
        return parent;
    }

    public void setParent(MenuData parent) {
        this.parent = parent;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public boolean isFromHome() {
        return fromHome;
    }

    public void setFromHome(boolean fromHome) {
        this.fromHome = fromHome;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public void setSelectedChild(String categoryId) {
        selectedChildPosition = 0;
        for (MenuData child : childItems) {
            if (child.getArticleId().equals(categoryId)) {
                selectedChildPosition = childItems.indexOf(child);
                break;
            }
        }
    }

    public int getSelectedChildPosition() {
        return selectedChildPosition;
    }

    public boolean getSensexTicker() {
        return sensexTicker == null ? false : sensexTicker;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(params);
        parcel.writeTypedList(childItems);
        parcel.writeInt(position);
        parcel.writeByte((byte) (expanded ? 1 : 0));
        parcel.writeInt(level);
        parcel.writeString(label);
        parcel.writeString(type);
        parcel.writeString(imageUrl);
        parcel.writeString(articleId);
        parcel.writeByte((byte) (sensexTicker == null ? 0 : sensexTicker ? 1 : 2));
        parcel.writeByte((byte) (hasDivider ? 1 : 0));
        parcel.writeByte((byte) (fromHome ? 1 : 0));
        parcel.writeByte((byte) (isFromLink ? 1 : 0));
        parcel.writeInt(selectedChildPosition);
        parcel.writeString(id);
        parcel.writeString(color);
        parcel.writeString(alias);
        parcel.writeString(pageTitle);
    }

    public String getAlias() {
        return isFromLink ? "alias" : alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getColor() {
        return color;
    }

    public int getmScreenNavigationFlag() {
        return mScreenNavigationFlag;
    }

    public boolean isFromLink() {
        return isFromLink;
    }

    public String getId() {
        return id;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFromLink(boolean isFromLink) {
        this.isFromLink = isFromLink;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSubSectionName(String subSectionName) {
        this.subSectionName = subSectionName;
    }

    public String getSubSectionName() {
        return subSectionName;
    }

    public String getSubject() {
        return mSubject;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getShareUrl() {
        return mShareUrl;
    }

    public String getContentType() {
        return mContentType;
    }

    public String getAppStoreUrl() {
        return mAppStoreUrl;
    }

    @Override
    public String toString() {
        return "MenuData{" +
                "articleId='" + articleId + '\'' +
                ", id='" + id + '\'' +
                ", label='" + label + '\'' +
                ", type='" + type + '\'' +
                ", childrenSize='" + ((childItems != null) ? childItems.size() + "" : 0) + '\'' +
                ", ParentMenuId='" + ((parent != null) ? parent.getArticleId() : null) + '\'' +
                ", params='" + params + '\'' +
                ", labelInEnglish='" + labelInEnglish + '\'' +
                '}';
    }
}
