/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */
package com.db.offers.common;

import java.util.List;

public interface MenuItemData extends GsonProguardMarker {

    int getViewType();

    boolean hasChildren();

    boolean isExpanded();

    void setPosition(int position);

    int getPosition();

    void setExpanded(boolean expanded);

    List<MenuData> getChildItems();

    boolean hasDivider();


}
