package com.db.offers.common;

import android.os.Parcel;
import android.os.Parcelable;

public interface ParcelableInterface extends Parcelable, GsonProguardMarker {

    void readFromParcel(Parcel source);
}
