/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.db.offers.megaoffers.constants.MegaOffersAPIConstants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Profile implements ParcelableInterface, GsonProguardMarker {

    @SerializedName("dob")
    @Expose
    private String dob;

    @SerializedName("anniversary")
    @Expose
    private String anniversary;

    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mail")
    @Expose
    private String mail;

    @SerializedName("gender")
    @Expose
    private List<Gender> gender = new ArrayList<>();
    @SerializedName("mobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("areasOfInterest")
    @Expose
    private List<String> areasOfInterest = new ArrayList<>();

    @SerializedName("regions")
    @Expose
    private List<String> regions = new ArrayList<>();

    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("settings")
    @Expose
    private Settings settings;

    @SerializedName("dbId")
    @Expose
    private String dbID;

    @SerializedName("guestStatus")
    private boolean isThisGuestUser;

    @SerializedName("connectFrom")
    @Expose
    private List<String> connectFrom;

    @SerializedName("sheherList")
    @Expose
    List<CityListResponse.Location> cityList;

    @SerializedName("myInterest")
    @Expose
    List<Integer> myInterest;

    @SerializedName(MegaOffersAPIConstants.KEY_OFFER_AUTH)
    @Expose
    private String offerAuthKey;

    @SerializedName(MegaOffersAPIConstants.KEY_OFFER_CODE)
    @Expose
    private String offerAuthCode;

    @Override
    public void readFromParcel(Parcel in) {
        uid = in.readString();
        name = in.readString();
        mail = in.readString();
        mobileNo = in.readString();
        areasOfInterest = in.createStringArrayList();
        regions = in.createStringArrayList();
        avatar = in.readString();
        dbID = in.readString();
        offerAuthKey = in.readString();
        offerAuthCode = in.readString();
    }

    public Profile() {
    }

    public Profile(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uid);
        dest.writeString(name);
        dest.writeString(mail);
        dest.writeString(mobileNo);
        dest.writeStringList(areasOfInterest);
        dest.writeStringList(regions);
        dest.writeString(avatar);
        dest.writeString(dbID);
        dest.writeString(offerAuthKey);
        dest.writeString(offerAuthCode);
    }

    public List<String> getConnectFrom() {
        if (connectFrom == null) {
            connectFrom = new ArrayList<>();
        }
        return connectFrom;
    }

    public String getUid() {
        return uid;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getAnniversary() {
        return anniversary;
    }

    public List<Gender> getGender() {
        return gender;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public void setVerifiedMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAvatar() {
        return avatar;
    }

    public List<CityListResponse.Location> getCityList() {
        return cityList;
    }

    public void setCityList(ArrayList<CityListResponse.Location> cityList) {
        this.cityList = cityList;
    }

    public boolean isThisGuestUser() {
        return isThisGuestUser;
    }

    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    public String getDob() {
        return dob;
    }

    public String getDbID() {
        return dbID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<Integer> getMyInterest() {
        return myInterest;
    }

    public void setMyInterest(List<Integer> myInterest) {
        this.myInterest = myInterest;
    }

    public String getOfferAuthKey() {
        return offerAuthKey;
    }

    public String getOfferAuthCode() {
        return offerAuthCode;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setAnniversary(String anniversary) {
        this.anniversary = anniversary;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }



    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public List<String> getAreasOfInterest() {
        return areasOfInterest;
    }

    public void setAreasOfInterest(List<String> areasOfInterest) {
        this.areasOfInterest = areasOfInterest;
    }

    public List<String> getRegions() {
        return regions;
    }

    public void setRegions(List<String> regions) {
        this.regions = regions;
    }

    public void setDbID(String dbID) {
        this.dbID = dbID;
    }

    public void setThisGuestUser(boolean thisGuestUser) {
        isThisGuestUser = thisGuestUser;
    }

    public void setConnectFrom(List<String> connectFrom) {
        this.connectFrom = connectFrom;
    }

    public void setCityList(List<CityListResponse.Location> cityList) {
        this.cityList = cityList;
    }

    public void setOfferAuthKey(String offerAuthKey) {
        this.offerAuthKey = offerAuthKey;
    }

    public void setOfferAuthCode(String offerAuthCode) {
        this.offerAuthCode = offerAuthCode;
    }

    public static Creator<Profile> getCREATOR() {
        return CREATOR;
    }

    public void setGender(List<Gender> genders) {
        this.gender = genders;
    }
}
