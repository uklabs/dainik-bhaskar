/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Settings implements GsonProguardMarker {

    @SerializedName("notification")
    @Expose
    private Notification notification;
    @SerializedName("video")
    @Expose
    private Video video;

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public static class Notification {
        @SerializedName("enabled")
        @Expose
        private Boolean enabled;
        @SerializedName("sound")
        @Expose
        private String sound;
        @SerializedName("unsubscribedTopics")
        @Expose
        private List<String> unscbscribedTopics = null;
        @SerializedName("frequency")
        @Expose
        private String frequency;
        @SerializedName("dnd")
        @Expose
        private DND dnd;

        public DND getDnd() {
            return dnd;
        }

        public void setDnd(DND dnd) {
            this.dnd = dnd;
        }

        public Boolean getEnabled() {
            return enabled != null ? enabled : true;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }

        public String getSound() {
            return sound ;
        }

        public void setSound(String sound) {
            this.sound = sound;
        }

        public List<String> getTopics() {
            return unscbscribedTopics;
        }

        public void setTopics(List<String> topics) {
            this.unscbscribedTopics = topics;
        }

        public String getFrequency() {
            return frequency;
        }

        public void setFrequency(String frequency) {
            this.frequency = frequency;
        }

    }

    public static class Video {
        @SerializedName("quality")
        @Expose
        private String quality;
        @SerializedName("bestOnWIFI")
        @Expose
        private Boolean bestOnWIFI;

        public String getQuality() {
            return quality;
        }

        public void setQuality(String quality) {
            this.quality = quality;
        }

        public Boolean getBestOnWIFI() {
            return bestOnWIFI;
        }

        public void setBestOnWIFI(Boolean bestOnWIFI) {
            this.bestOnWIFI = bestOnWIFI;
        }
    }
}
