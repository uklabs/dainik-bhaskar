package com.db.offers.common;

import com.google.gson.annotations.SerializedName;

public class SyncResponseModel extends BaseResponse implements GsonProguardMarker {

    @SerializedName("data")
    private SyncResponse syncResponse;

    public SyncResponse getSyncResponse() {
        return syncResponse;
    }

    public final class SyncResponse implements GsonProguardMarker {
        @SerializedName("points")
        public int points;

        @SerializedName("offer_month")
        public int offerMonth;

        @SerializedName("visit_count")
        public int visitCount;


        @Override
        public String toString() {
            return "SyncResponse{" +
                    "points=" + points +
                    ", offerMonth=" + offerMonth +
                    ", visitCount=" + visitCount +
                    '}';
        }
    }
}
