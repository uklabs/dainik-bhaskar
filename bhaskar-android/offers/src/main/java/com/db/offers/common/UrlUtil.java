package com.db.offers.common;

import com.db.offers.tambola.constant.TambolaConstants;

public final class UrlUtil {

    //Common
    private static final String BHASKAR_DOMAIN_NAME = "bhaskar.com";
    private static final String BHASKAR_DIVYA_DOMAIN_NAME = "divyabhaskar.co.in";

    //MegaOffer
//    public static final String BASE_URL_MEGAOFFER = "https://node.dbnewshub.com/offer-api/";
    public static final String BASE_URL_MEGAOFFER = "https://offerapi.bhaskar.com/";

    //Tambola
//    private static final String DB_TAMBOLA_BASE_URL = "https://tambola.dbnewshub.com/";

    private static final String DB_TAMBOLA_BASE_URL = "https://tambolaapi.bhaskar.com/";
    private static final String DB_TAMBOLA_WEB_BASE_URL = "https://tambola.bhaskar.com/";


    public static String getDivyaDomainName() {
        return BHASKAR_DIVYA_DOMAIN_NAME;
    }

    public static String getBhaskarDomainName() {
        return BHASKAR_DOMAIN_NAME;
    }

    public static String getDigitalBaseUrl() {
        return BASE_URL_MEGAOFFER;
    }

    public static String getMegaOfferBaseURL() {
        return BASE_URL_MEGAOFFER;
    }

    public static String getTambolaGameRulesUrl() {
        return getTambolaWebBaseUrl() + TambolaConstants.APIEndPoint.GAME_RULE_URL;
    }

    public static String getTambolaTandCUrl() {
        return getTambolaWebBaseUrl() + TambolaConstants.APIEndPoint.TERM_CONDITION_URL;
    }

    public static String getTambolaBaseUrl() {
        return DB_TAMBOLA_BASE_URL;
    }

    public static String getTambolaWebBaseUrl() {
        return DB_TAMBOLA_WEB_BASE_URL;
    }

    public static String getDefaultUrl() {
        return "https://www.bhaskar.com/";
    }


}
