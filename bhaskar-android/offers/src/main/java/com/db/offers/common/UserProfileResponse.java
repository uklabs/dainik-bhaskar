/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfileResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private Data data;

    @SerializedName("authToken")
    @Expose
    private String authToken;

    @SerializedName("refreshToken")
    @Expose
    private String refreshToken;

    public Data getData() {
        return data;
    }

    public class Data {

        @SerializedName("profile")
        @Expose
        private Profile profile;

        public Profile getProfile() {
            return profile;
        }
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
