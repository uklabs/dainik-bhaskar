package com.db.offers.common;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.db.CommonLoginConstants;
import com.bhaskar.util.LoginController;
import com.db.offers.BuildConfig;
import com.db.offers.common.cryptlib.CryptLib;
import com.db.offers.megaoffers.constants.MegaOffersAPIConstants;
import com.bhaskar.appscommon.tracking.TrackingData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_ACCEPT;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_ACCESS_TOKEN;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_APP_ID;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_APP_VERSION;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_CHANNEL;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_CONTENT_TYPE;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_DB_ID;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_DEVICE_ID;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_DEVICE_TYPE;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_X_CSRF_TOKEN;
import static com.db.offers.common.ApiConstants.HeaderValues.VALUE_ACCEPT;
import static com.db.offers.common.ApiConstants.HeaderValues.VALUE_CHANNEL;
import static com.db.offers.common.ApiConstants.HeaderValues.VALUE_CONTENT_TYPE;
import static com.db.offers.common.ApiConstants.HeaderValues.VALUE_DEVICE_TYPE;


public class Utils {
    public static boolean isWebUrlDBInternal(String webUrl, List<String> whiteUrlList) {

        if (!TextUtils.isEmpty(webUrl)) {
            if (whiteUrlList != null && !whiteUrlList.isEmpty()) {
                for (String whiteListURl : whiteUrlList) {
                    if (webUrl.contains(whiteListURl)) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            return false;
        }
    }

    public static void setStatusBarColor(Activity activity, int color) {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = activity.getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color);
            }
        } catch (Exception e) {
        }
    }


    public static Map<String, String> getOfferDashboardWebHeader(@NonNull Context dbApplication) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(KEY_ACCEPT, VALUE_ACCEPT);
        headerMap.put(KEY_CHANNEL, VALUE_CHANNEL);
        headerMap.put(KEY_DEVICE_TYPE, VALUE_DEVICE_TYPE);
        headerMap.put(KEY_CONTENT_TYPE, VALUE_CONTENT_TYPE);
        headerMap.put(KEY_X_CSRF_TOKEN, "");

        headerMap.put(KEY_DB_ID, TrackingData.getDBId(dbApplication));
        headerMap.put(KEY_APP_VERSION, BuildConfig.VERSION_NAME);
        String accessToken = (LoginController.loginController().isUserLoggedIn()) ? LoginController.loginController().getCurrentUser().uuID : "";
        headerMap.put(KEY_ACCESS_TOKEN, (TextUtils.isEmpty(accessToken) ? "" : accessToken));
        headerMap.put(KEY_DEVICE_ID, TrackingData.getDeviceId(dbApplication));
        headerMap.put(KEY_APP_ID, CommonLoginConstants.BHASKAR_APP_ID);


        try {
            CryptLib cryptLib = new CryptLib();
            headerMap.put(MegaOffersAPIConstants.KEY_OFFER_AUTH, cryptLib.encryptPlainTextWithRandomIV(TrackingData.getDeviceId(dbApplication), CryptLib.ENCRYPT_KEY));
            headerMap.put(MegaOffersAPIConstants.KEY_OFFER_CODE, cryptLib.encryptPlainTextWithRandomIV(LoginController.loginController().getCurrentUser().mobile, CryptLib.ENCRYPT_KEY));
        } catch (Exception e) {

        }
        return headerMap;
    }

    public static Map<String, String> getWebHeader(Context context) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(KEY_ACCEPT, VALUE_ACCEPT);
        headerMap.put(KEY_CHANNEL, VALUE_CHANNEL);
        headerMap.put(KEY_DEVICE_TYPE, VALUE_DEVICE_TYPE);
        headerMap.put(KEY_CONTENT_TYPE, VALUE_CONTENT_TYPE);
        headerMap.put(KEY_X_CSRF_TOKEN, "");

        headerMap.put(KEY_DB_ID, TrackingData.getDBId(context));
        headerMap.put(KEY_APP_VERSION, BuildConfig.VERSION_NAME);
        String accessToken = (LoginController.loginController().isUserLoggedIn()) ? LoginController.loginController().getCurrentUser().uuID : "";
        headerMap.put(KEY_ACCESS_TOKEN, (TextUtils.isEmpty(accessToken) ? "" : accessToken));
        headerMap.put(KEY_DEVICE_ID, TrackingData.getDeviceId(context));
        headerMap.put(KEY_APP_ID, CommonLoginConstants.BHASKAR_APP_ID);

        return headerMap;
    }

    public static List<String> getDBWebInternalUrls() {
        List<String> webUrlsList = new ArrayList<>();
        webUrlsList.add(UrlUtil.getBhaskarDomainName());
        webUrlsList.add(UrlUtil.getDivyaDomainName());
        return webUrlsList;
    }


}
