package com.db.offers.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class WhitListWebUrlModel implements GsonProguardMarker {
    @SerializedName("isEnable")
    @Expose
    private boolean isEnable;
    @SerializedName("url_list")
    @Expose
    private List<String> urlList = null;

    public boolean isEnable() {
        return isEnable;
    }

    public List<String> getUrlList() {
        return urlList;
    }

    @Override
    public String toString() {
        return "WhitListWebUrlModel{" +
                "isEnable=" + isEnable +
                ", urlList=" + urlList +
                '}';
    }
}
