package com.db.offers.common.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.db.offers.OfferController;
import com.db.offers.R;
import com.db.offers.common.DBSharedPref;
import com.db.offers.common.Utils;
import com.db.offers.common.WhitListWebUrlModel;
import com.db.offers.common.rxbus.RxBusCallback;
import com.db.offers.common.rxbus.RxBusHelper;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class CoreActivity extends AppCompatActivity implements RxBusCallback {

    private static final String TAG = CoreActivity.class.getSimpleName();

    protected Context dbApplication;
    protected Unbinder mUnBinder;
    protected DBSharedPref dbSharedPref;
    private RxBusHelper mRxBusHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

        Utils.setStatusBarColor(this, ContextCompat.getColor(this, R.color.divya_theme_color));
        registerEvents();
        dbSharedPref = OfferController.getInstance().getDbSharedPref();
        dbApplication = CoreActivity.this;
        // attachSlidr();

    }

    @Override
    protected void onPause() {
        super.onPause();
        //overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void setContentView(@NonNull View view) {
        super.setContentView(view);
        mUnBinder = ButterKnife.bind(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        mUnBinder = ButterKnife.bind(this);
    }

    private void registerEvents() {
        mRxBusHelper = new RxBusHelper();
        mRxBusHelper.registerEvents(OfferController.getInstance().getRxBus(), TAG, this);
    }

    @Override
    public void onEventTrigger(Object event) {
    }

    public List<String> getWhitListWebUrls() {
        WhitListWebUrlModel whitListWebUrlModel = OfferController.getInstance().getDbSharedPref().getWhitListWebUrlModel();
        if (whitListWebUrlModel != null && whitListWebUrlModel.isEnable() && whitListWebUrlModel.getUrlList() != null) {
            return whitListWebUrlModel.getUrlList();
        } else {
            return Utils.getDBWebInternalUrls();
        }
    }

    @Override
    protected void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        if (mRxBusHelper != null) {
            mRxBusHelper.unSubScribe();
            mRxBusHelper = null;
        }
        super.onDestroy();
    }
}
