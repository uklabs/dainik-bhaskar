package com.db.offers.common.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.bhaskar.util.LoginController;
import com.bhaskar.data.model.ProfileInfo;
import com.db.offers.OfferController;
import com.db.offers.R;
import com.db.offers.common.AppConstants;
import com.db.offers.common.BundleConstants;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.DBSharedPref;
import com.db.offers.common.DBToast;
import com.db.offers.common.EasyDialogUtils;
import com.db.offers.common.Gender;
import com.db.offers.common.Profile;
import com.db.offers.common.TransAviLoader;
import com.db.offers.common.network.RetroApiListener;
import com.db.offers.common.utils.DialogResponseInterface;
import com.db.offers.megaoffers.activity.MegaOffersDashboardActivity;
import com.db.offers.megaoffers.activity.MegaOffersListingActivity;
import com.db.offers.megaoffers.constants.MegaOffersConstants;
import com.db.offers.megaoffers.model.response.MegaOffers;
import com.db.offers.megaoffers.model.response.MegaOffersListingModel;
import com.db.offers.megaoffers.providers.MegaOfferApiProvider;
import com.db.offers.tambola.TambolaLauncherProvider;
import com.db.offers.tambola.utils.Utils;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.clevertap.ExtraCTEvent;

import java.util.ArrayList;
import java.util.List;


public class OfferTranslucentActivity extends Activity {

    private static String TAG = "MegaOfferController";
    private String action;
    private DBSharedPref dbSharedPref;

    public static Intent getIntent(Context context, String action) {
        Intent intent = new Intent(context, OfferTranslucentActivity.class);
        intent.putExtra("action", action);
        return intent;
    }

    public DBSharedPref getDbSharedPref() {
        if (dbSharedPref == null) {
            dbSharedPref = new DBSharedPref(OfferTranslucentActivity.this);
        }
        return dbSharedPref;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_trans_offering);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            action = extras.getString("action");
        }
        getDbSharedPref();
        switch (action) {
            case AppConstants.TAMBOLA_OFFER:
                handleTambola();
                break;
            case AppConstants.MEGA_OFFER:
                handleMegaOfferClick();
                break;
        }
    }

    private void handleTambola() {
        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
        if (profileInfo != null && profileInfo.isUserLoggedIn) {
            Profile profile = new Profile();
            profile.setUid(profileInfo.uID);
            profile.setName(profileInfo.name);
            profile.setMail(profileInfo.email);
            profile.setMobileNo(profileInfo.mobile);
            profile.setDbID(TrackingData.getDBId(this));
            String gender = profileInfo.gender;
            List<Gender> genders = new ArrayList<>();
            Gender genderM = new Gender();
            genderM.name = "Male";
            genderM.setId(1);
            Gender genderF = new Gender();
            genderF.name = "Female";
            genderF.setId(2);
            if (gender.equalsIgnoreCase("F"))
                genderF.selected = true;
            else
                genderM.selected = true;
            genders.add(genderM);
            genders.add(genderF);
            profile.setGender(genders);
            OfferController.getInstance().getDbSharedPref().updateUserProfile(profile);
        } else {
            dbSharedPref.logoutUser();
        }

        if (CommonUtils.isNetworkAvailable(OfferTranslucentActivity.this)) {
            if (dbSharedPref.isUserLoggedIn()) {
//                CleverTapDB.getInstance(this).cleverTapTrackEvent(this, CTConstant.EVENT_NAME.HOME_SCREEN_ACTION, CTConstant.PROP_NAME.CLICKON,
//                        CTConstant.PROP_VALUE.ICONS, Utils.getUserDataMap(), new ExtraCTEvent(CTConstant.PROP_NAME.CLICKED_ITEM_NAME, CTConstant.LABEL.TAMBOLA));
//                Tracking.trackGAEvent(OfferTranslucentActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.HOME_SCREEN_ACTION, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.ICONS, "", Utils.getUserDataMap());
                new TambolaLauncherProvider(OfferTranslucentActivity.this).initProvider(OfferTranslucentActivity.this);
            } else {
                Bundle bundle = new Bundle();
                bundle.putInt(BundleConstants.KEY_NAVIGATE_FROM, AppConstants.NavigationFlow.NAVIGATE_FROM_HOME);
                launchLoginActivity(OfferTranslucentActivity.this, bundle, AppConstants.RequestCode.RC_LOGIN_FROM_TAMBOLA);
            }
        } else {
            DBToast.showAlertToast(OfferTranslucentActivity.this, getString(R.string.alert_title_alert), getString(R.string.alert_network_not_exist));
            finish();
        }
    }

    public void handleMegaOfferClick() {
        Activity context = OfferTranslucentActivity.this;
        int navigationType = AppConstants.NavigationFlow.NAVIGATE_FROM_HOME;

        if (CommonUtils.isNetworkAvailable(context)) {
            Log.d(TAG, "handleMegaOfferClick()");
            TransAviLoader transAviLoader = new TransAviLoader(context);
            transAviLoader.show();
            MegaOfferApiProvider megaOfferApiProvider = MegaOfferApiProvider.getInstance(context);
            megaOfferApiProvider.getMegaOffersList(new RetroApiListener<MegaOffersListingModel>() {
                @Override
                public void onApiSuccess(@NonNull MegaOffersListingModel megaOffersListingModel) {
                    try {
                        transAviLoader.dismiss();
                        if (megaOffersListingModel.code == 305 && !TextUtils.isEmpty(megaOffersListingModel.message)) {
                            doLogoutForMegaOffer(megaOffersListingModel.message);
                        } else if (megaOffersListingModel.code == 306 && !TextUtils.isEmpty(megaOffersListingModel.message)) {
                            EasyDialogUtils.showInfoDialog(context, context.getString(R.string.alert_title_error), megaOffersListingModel.message);
                        } else {
                            MegaOffers megaOffers = megaOffersListingModel.getMegaOffers();
                            if (megaOffers.isOfferAccepted()) {
                                if (LoginController.loginController().isUserLoggedIn()) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString(MegaOffersConstants.BundleConstants.KEY_HEADER_TITLE, megaOffers.getAcceptedOffer().offerName);
                                    bundle.putString(MegaOffersConstants.BundleConstants.KEY_DASHBOARD_URL, megaOffers.getDashboardUrl());
                                    bundle.putInt(BundleConstants.KEY_NAVIGATE_FROM, navigationType);
                                    MegaOfferApiProvider.getAndSaveMegaOfferCurrentDateAndMaxPointsCount(OfferTranslucentActivity.this);
                                    launchMegaOfferDashboardActivity(bundle);
                                    finish();
                                } else {
                                    Bundle bundle = new Bundle();
                                    bundle.putString(MegaOffersConstants.BundleConstants.KEY_HEADER_TITLE, megaOffers.getAcceptedOffer().offerName);
                                    bundle.putString(MegaOffersConstants.BundleConstants.KEY_DASHBOARD_URL, megaOffers.getDashboardUrl());
                                    bundle.putInt(BundleConstants.KEY_NAVIGATE_FROM, navigationType);

                                    launchLoginActivity(context, bundle, AppConstants.RequestCode.RC_LOGIN_FROM_MEGA_OFFER_ACCEPTED);
                                }
                            } else if (megaOffers.isOfferAvailable()) {
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(MegaOffersConstants.BundleConstants.KEY_MEGA_OFFER_RESPONSE, megaOffers);
                                bundle.putInt(BundleConstants.KEY_NAVIGATE_FROM, navigationType);
                                MegaOffersListingActivity.startMegaOfferListingActivity(context, bundle);
                                finish();
                            } else {
                                EasyDialogUtils.showInfoDialog(context, context.getString(R.string.alert_title_alert), context.getString(R.string.mo_alert_no_offer_available));
                            }
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "checkForMegaOffersFromServer() onApiSuccess " + e.getMessage());
                        EasyDialogUtils.showInfoDialog(context, context.getString(R.string.alert_title_failed), context.getString(R.string.alert_request_unable_to_process));
                        finish();
                    }
                }

                /***
                 *
                 * @param message
                 */
                private void doLogoutForMegaOffer(@NonNull String message) {

                    EasyDialogUtils.showConfirmationDialog(context, context.getString(R.string.alert_title_error),
                            message, false, R.mipmap.app_icon, "Re-Login", "Cancel", new DialogResponseInterface() {
                                @Override
                                public void doOnPositiveBtnClick(Activity context) {
                                    LoginController.loginController().logout(context);
                                    Bundle bundle = new Bundle();
                                    bundle.putInt(BundleConstants.KEY_NAVIGATE_FROM, navigationType);
                                    launchLoginActivity(context, bundle, AppConstants.RequestCode.RC_LOGIN_FROM_MEGA_OFFER_RELOGIN);
                                }

                                @Override
                                public void doOnNegativeBtnClick(Activity context) {
                                    finish();
                                }
                            });

                }

                @Override
                public void onApiFailed(@NonNull String message) {
                    transAviLoader.dismiss();
                    Log.e(TAG, "checkForMegaOffersFromServer() onApiFailed " + message);
                    EasyDialogUtils.showInfoDialog(context, context.getString(R.string.alert_title_failed), context.getString(R.string.alert_request_unable_to_process));
                }
            });
        } else {
            DBToast.showAlertToast(context, context.getString(R.string.alert_title_alert), context.getString(R.string.alert_network_not_exist));
            finish();
        }
    }

    /***
     *
     * @param bundle
     */
    private void launchMegaOfferDashboardActivity(@NonNull Bundle bundle) {
        MegaOffersDashboardActivity.launchMegaOffersDashboard(OfferTranslucentActivity.this, bundle);
    }

    public void launchLoginActivity(@NonNull Context context, @NonNull Bundle bundle, int requestCode) {
        startActivityForResult(LoginController.loginController().getLoginIntent(context, TrackingData.getDBId(context), TrackingData.getDeviceId(context)), requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (LoginController.loginController().isUserLoggedIn()) {
            switch (requestCode) {
                case AppConstants.RequestCode.RC_LOGIN_FROM_MEGA_OFFER_ACCEPTED:
                case AppConstants.RequestCode.RC_LOGIN_FROM_MEGA_OFFER_RELOGIN:
                    handleMegaOfferClick();
                    break;
                case AppConstants.RequestCode.RC_LOGIN_FROM_TAMBOLA:
                    handleTambola();
                    break;
            }

        } else {
            finish();
        }

    }
}
