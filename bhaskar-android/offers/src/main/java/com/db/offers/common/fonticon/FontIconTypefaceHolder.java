package com.db.offers.common.fonticon;

import android.content.res.AssetManager;
import android.graphics.Typeface;

import java.io.File;

public class FontIconTypefaceHolder {
    private static Typeface sTypeface = Typeface.DEFAULT;

    public static Typeface getTypeface() {
        if (sTypeface == null) {
            throw new IllegalStateException();
        }

        return sTypeface;
    }

    public static void setTypeface(Typeface typeface) {
        sTypeface = typeface;
    }

    public static void init(AssetManager assets, String path) {
        sTypeface = Typeface.createFromAsset(assets, path);
    }

    public static void init(File file) {
        if (file == null)
            sTypeface = Typeface.SANS_SERIF;
        else
            sTypeface = Typeface.createFromFile(file);
    }
}
