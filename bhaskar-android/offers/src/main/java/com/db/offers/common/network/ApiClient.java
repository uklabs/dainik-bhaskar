package com.db.offers.common.network;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.db.CommonLoginConstants;
import com.bhaskar.util.LoginController;
import com.db.offers.BuildConfig;
import com.db.offers.OfferController;
import com.db.offers.common.LocationModel;
import com.db.offers.common.UrlUtil;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.GsonBuilder;

import java.net.URLDecoder;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_ACCEPT;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_ACCESS_TOKEN;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_APP_ID;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_APP_VERSION;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_AUTHORIZATION;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_CHANNEL;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_CITY;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_CONTENT_TYPE;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_DB_ID;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_DEVICE_ID;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_DEVICE_TYPE;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_INSTALL_DATE;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_UPDATE_DATE;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_USER_AUTHORIZATION;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_X_CSRF_TOKEN;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderValues.VALUE_ACCEPT;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderValues.VALUE_CHANNEL;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderValues.VALUE_CONTENT_TYPE;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderValues.VALUE_DEVICE_TYPE;


public final class ApiClient {

    private static final String TAG = ApiClient.class.getSimpleName();

    /**
     * @param dbApplication
     * @return
     */
    private static OkHttpClient getOkHttpClient(@NonNull Context dbApplication) {

        StethoInterceptor stethoInterceptor = null;
        if (BuildConfig.DEBUG) {
            // set your desired log level
            stethoInterceptor = new StethoInterceptor();
        }

        /*long cacheSize = (5L * 1024L * 1024L);
        Cache retrofitCache = new Cache(DBApplication.getApplicationInstance().getCacheDir(), cacheSize);*/

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        okHttpBuilder.addInterceptor(chain -> {
            Request request = chain.request();
            String requestUrl = URLDecoder.decode(request.url().toString(), "UTF-8");

            final String dbId = TrackingData.getDBId(dbApplication);
            final String userPassBasicAuth = "";//dbApplication.getDbSharedPref().getAuthToken();
            final String userAuthToken = "";//dbApplication.getDbSharedPref().getUserAuthToken();

            final String[] createdAndChangedDate = OfferController.getInstance().getDbSharedPref().getCreatedAndChangedDate();

            Request.Builder builder = request.newBuilder()
                    .addHeader(KEY_ACCEPT, VALUE_ACCEPT)
                    .addHeader(KEY_CHANNEL, VALUE_CHANNEL)
                    .addHeader(KEY_DEVICE_TYPE, VALUE_DEVICE_TYPE)
                    .addHeader(KEY_CONTENT_TYPE, VALUE_CONTENT_TYPE);

            builder.addHeader(KEY_AUTHORIZATION, userPassBasicAuth);
            builder.addHeader(KEY_USER_AUTHORIZATION, userAuthToken);

            /*mega offer Headers start*/
            String accessToken = (LoginController.loginController().isUserLoggedIn()) ? LoginController.loginController().getCurrentUser().uuID : "";
            builder.addHeader(KEY_ACCESS_TOKEN, (TextUtils.isEmpty(accessToken) ? "" : accessToken));
            builder.addHeader(KEY_APP_ID, CommonLoginConstants.BHASKAR_APP_ID);
            builder.addHeader(KEY_INSTALL_DATE, TrackingData.getFirstInstallDate(dbApplication)/*"2019-05-13"*/);
            builder.addHeader(KEY_UPDATE_DATE, TrackingData.getAppInstallDate(dbApplication)/*"2019-05-28"*/);
            builder.addHeader(KEY_DEVICE_ID, TrackingData.getDeviceId(dbApplication));
            if (dbId != null) {
                builder.addHeader(KEY_DB_ID, dbId);
            }
            builder.addHeader(KEY_APP_VERSION, BuildConfig.VERSION_NAME);
            /*Mega offers Headers end*/

            builder.addHeader(KEY_X_CSRF_TOKEN, "");

            try {
                LocationModel locationModel = OfferController.getInstance().getDbSharedPref().getUserLocation();
                if (locationModel != null && !TextUtils.isEmpty(locationModel.getCityName())) {
                    builder.addHeader(KEY_CITY, locationModel.getCityName());
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage() + "");
            }
            builder.method(request.method(), request.body());
            builder.url(requestUrl);

            return chain.proceed(builder.build());
        });
        okHttpBuilder.readTimeout(30, TimeUnit.SECONDS);
        okHttpBuilder.connectTimeout(30, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            okHttpBuilder.addNetworkInterceptor(stethoInterceptor);
        }

        OkHttpClient okHttpClient = okHttpBuilder.build();
        okHttpClient.dispatcher().setMaxRequests(8);
        return okHttpClient;
    }

//    public static Retrofit getClient(Context dbApplication) {
//        return new Retrofit.Builder()
//                .client(getOkHttpClient(dbApplication))
//                .baseUrl(UrlUtil.getBaseUrl())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
//                        .setLenient()
//                        .create()))
//                .build();
//    }


    /***
     *
     * @param dbApplication
     * @return
     */
    public static Retrofit getDigitalServerClient(@NonNull Context dbApplication) {


        return new Retrofit.Builder()
                .client(getOkHttpClient(dbApplication))
                .baseUrl(UrlUtil.getDigitalBaseUrl())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient()
                        .create()))
                .build();
    }

    /***
     *
     * @param dbApplication
     * @return
     */
    public static Retrofit getMegaOfferClient(@NonNull Context dbApplication) {


        return new Retrofit.Builder()
                .client(getOkHttpClient(dbApplication))
                .baseUrl(UrlUtil.getMegaOfferBaseURL())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient()
                        .create()))
                .build();
    }


//    public static Retrofit getUserClient(Context context) {
//        return new Retrofit.Builder()
//                .client(getOkHttpClient(context))
//                .baseUrl(UrlUtil.getUserBaseUrl())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
//                        .setLenient()
//                        .create()))
//                .build();
//    }
}
