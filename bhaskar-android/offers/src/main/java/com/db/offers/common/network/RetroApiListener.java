package com.db.offers.common.network;


import androidx.annotation.NonNull;

public interface RetroApiListener<T> {

    void onApiSuccess(@NonNull T response);

    void onApiFailed(@NonNull String message);
}
