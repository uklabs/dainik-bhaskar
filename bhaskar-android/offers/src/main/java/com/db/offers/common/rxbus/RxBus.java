/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common.rxbus;

import io.reactivex.Observable;

public interface RxBus {

    void send(final Object event);

    Observable<Object> toObservable();

    boolean hasObservers();
}