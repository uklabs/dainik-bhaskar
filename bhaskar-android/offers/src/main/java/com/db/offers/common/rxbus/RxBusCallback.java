/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common.rxbus;


public interface RxBusCallback {

    void onEventTrigger(Object event);
}
