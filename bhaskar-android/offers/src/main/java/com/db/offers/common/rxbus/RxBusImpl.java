/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common.rxbus;



import com.db.offers.common.GsonProguardMarker;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxBusImpl implements RxBus , GsonProguardMarker {

    private final PublishSubject<Object> mBus;

    public RxBusImpl() {
        mBus = PublishSubject.create();
    }

    @Override
    public void send(Object event) {
        mBus.onNext(event);
    }

    @Override
    public Observable<Object> toObservable() {
        return mBus;
    }

    @Override
    public boolean hasObservers() {
        return mBus.hasObservers();
    }

}
