package com.db.offers.common.rxbus;


import com.db.offers.common.BaseResponse;
import com.db.offers.common.CityListResponse;
import com.db.offers.common.rxbus.event.ApiResponseEvent;
import com.db.offers.common.rxbus.event.ErrorResponseEvent;
import com.db.offers.common.rxbus.event.NavigationEvent;

import java.util.ArrayList;

public class RxEventUtils {

    public static void sendEventWithFlag(RxBus rxBus, String flag) {
        if (rxBus != null && rxBus.hasObservers()) {
            rxBus.send(new NavigationEvent(flag));
        }
    }

    public static void sendEventWithData(RxBus rxBus, String flag, Object data) {
        if (rxBus != null && rxBus.hasObservers()) {
            NavigationEvent eventWithData = new NavigationEvent(flag);
            eventWithData.setData(data);
            rxBus.send(eventWithData);
        }
    }

    /*public static void sendEventWithDataAndType(RxBus rxBus, String flag, Object data, @AppConstants.SCREEN_TYPE int type) {
        if (rxBus != null && rxBus.hasObservers()) {
            NavigationEvent eventWithData = new NavigationEvent(flag);
            eventWithData.setData(data);
            eventWithData.setScreenType(type);
            rxBus.send(eventWithData);
        }
    }*/

    public static void sendApiResponseEvent(RxBus rxBus, BaseResponse response) {
        if (rxBus != null && rxBus.hasObservers()) {
            ApiResponseEvent event = new ApiResponseEvent(response);
            rxBus.send(event);
        }
    }

    public static void sendApiResponseEventWithFlag(RxBus rxBus, BaseResponse response, int flag) {
        if (rxBus != null && rxBus.hasObservers()) {
            ApiResponseEvent event = new ApiResponseEvent(response, flag);
            rxBus.send(event);
        }
    }

    public static void sendApiResponseEventWithFilter(RxBus rxBus, BaseResponse response, int flag, String filter) {
        if (rxBus != null && rxBus.hasObservers()) {
            ApiResponseEvent event = new ApiResponseEvent(response, flag, filter);
            rxBus.send(event);
        }
    }

    public static void sendErrorResponseEventWithFlag(RxBus rxBus, Error error, int flag) {
        if (rxBus != null && rxBus.hasObservers()) {
            ErrorResponseEvent event = new ErrorResponseEvent(error, flag);
            rxBus.send(event);
        }
    }

    public static void sendEventWithDataAndFilter(RxBus rxBus, String flag, Object data, String filter) {
        if (rxBus != null && rxBus.hasObservers()) {
            NavigationEvent eventWithData = new NavigationEvent(flag);
            eventWithData.setData(data);
            eventWithData.setFilter(filter);
            rxBus.send(eventWithData);
        }
    }

    /**
     * This method is used to trigger event with array list of location objects
     *
     * @param rxBus
     * @param flag
     * @param data
     */
    public static void sendEventWithList(RxBus rxBus, String flag, ArrayList<CityListResponse.Location> data) {
        if (rxBus != null && rxBus.hasObservers()) {
            NavigationEvent eventWithList = new NavigationEvent(flag);
            eventWithList.setItems(data);
            rxBus.send(eventWithList);
        }
    }
}
