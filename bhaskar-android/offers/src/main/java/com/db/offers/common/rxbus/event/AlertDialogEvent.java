/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common.rxbus.event;

import android.os.Parcel;
import android.os.Parcelable;

import com.db.offers.common.GsonProguardMarker;


public class AlertDialogEvent implements Parcelable, GsonProguardMarker {

    public static final String EVENT_REGISTRATION = "1";
    private int flag;

    public AlertDialogEvent(int flag) {
        this.flag = flag;
    }

    protected AlertDialogEvent(Parcel in) {
        flag = in.readInt();
    }

    public static final Creator<AlertDialogEvent> CREATOR = new Creator<AlertDialogEvent>() {
        @Override
        public AlertDialogEvent createFromParcel(Parcel in) {
            return new AlertDialogEvent(in);
        }

        @Override
        public AlertDialogEvent[] newArray(int size) {
            return new AlertDialogEvent[size];
        }
    };

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(flag);
    }
}
