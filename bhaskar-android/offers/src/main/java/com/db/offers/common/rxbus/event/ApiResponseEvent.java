package com.db.offers.common.rxbus.event;


import com.db.offers.common.BaseResponse;
import com.db.offers.common.GsonProguardMarker;

/*
 * Created by akshay on 9/5/18.
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */
public class ApiResponseEvent implements GsonProguardMarker {

    private BaseResponse mResponse;
    private int flag;
    private String filter;

    public ApiResponseEvent(BaseResponse response, int flag) {
        this.mResponse = response;
        this.flag = flag;
    }

    public ApiResponseEvent(BaseResponse response, int flag, String filter) {
        this.mResponse = response;
        this.flag = flag;
        this.filter = filter;
    }

    public ApiResponseEvent(BaseResponse response) {
        this.mResponse = response;
        this.flag = -1;
    }


    public BaseResponse getResponse() {
        return mResponse;
    }

    public int getFlag() {
        return flag;
    }

    public String getFilter() {
        return filter;
    }
}