/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common.rxbus.event;

import android.net.Uri;

import com.db.offers.common.GsonProguardMarker;


public class CameraClickEvent implements GsonProguardMarker {

    private String filePath;
    private Uri uri;

    public CameraClickEvent(Uri uri, String filePath) {
        this.uri = uri;
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public Uri getUri() {
        return uri;
    }
}
