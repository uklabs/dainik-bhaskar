/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common.rxbus.event;


import com.db.offers.common.GsonProguardMarker;

public class ErrorResponseEvent implements GsonProguardMarker {

    private Error mError;
    private int flag;

    public ErrorResponseEvent(Error error, int flag) {
        this.mError = error;
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public Error getError() {
        return mError;
    }

}
