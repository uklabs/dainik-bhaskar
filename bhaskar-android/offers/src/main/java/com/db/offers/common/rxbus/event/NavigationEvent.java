/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */
package com.db.offers.common.rxbus.event;


import com.db.offers.common.AppConstants;
import com.db.offers.common.GsonProguardMarker;

import java.util.ArrayList;

public class NavigationEvent<T> implements GsonProguardMarker {

    //public static  final String EVENT_OPEN_TAB_FROM_VIDEO_ACTIVITY = "event_open_tab_from_back_activity";

    public static final String EVENT_PRIORITY_VIDEO_LIST_SAVED = "event_priority_video_list_saved";

    public static final String EVENT_TOP_NEWS_SELECTED_FROM_TAMBOLA = "event_top_news_selected_from_tambola";

    /*Menu*/
    public static final String EVENT_MENU_GROUP_CLICK = "event_menu_group_click";
    public static final String EVENT_MENU_ITEM_CLICK = "event_menu_item_click";

    public static final String EVENT_BOTTOM_MORE_MENU_ITEM_CLICK = "event_bottom_more_menu_item_click";

    public static final String EVENT_RETRY_CLICK = "event_retry_click";
    //login
    public static final String EVENT_FACEBOOK_EMAIL_REQUEST = "event_facebook_email_request";
    public static final String EVENT_FACEBOOK_EMAIL_CANCELLED = "event_facebook_email_cancelled";
    /*Search*/
    public static final String EVENT_ON_SPEECH_INPUT_RESULTS = "event_on_speech_input_results";
    public static final String EVENT_ON_SPEECH_INPUT_ERROR = "event_on_speech_input_error";
    public static final String EVENT_PROMPT_SPEECH_INPUT = "event_prompt_speech_input";

    /*CONTEST*/
    public static final String EVENT_CONTEST_ITEM_CLICK = "event_contest_item_click";
    public static final String EVENT_MULTIMEDIA_ITEM_CLICK = "event_multimedia_item_click";

    public static final String EVENT_UPDATE_TOOLBAR = "event_update_toolbar";

    public static final String EVENT_LAUNCH_CATEGORY = "event_launch_category";
    public static final String EVENT_NOTIFICATION_TRANSITION = "event_notification_transition";
    public static final String EVENT_NEWS_ROUNDUP_ITEM_CLICK = "news_roundup_item_click";
    public static final String EVENT_NEWS_ROUNDUP_SHARE_CLICK = "news_roundup_share_click";

    /*Rashifal*/
    public static final String EVENT_RASHIFAL_ITEM_CLICKED = "event_rashifal_item_clicked";

    /* Home*/
    public static final String EVENT_BOTTOM_BAR_MENU_CLICK = "event_bottom_bar_menu_clicked";
    public static final String EVENT_LAUNCH_CATEGORY_IN_HOME = "event_launch_category_in_home";
    public static final String EVENT_LAUNCH_CITIZEN_JOURNALISM_IN_HOME = "event_launch_citizen_journalism_in_home";
    public static final String EVENT_LAUNCH_NEWS_BEEP_IN_HOME = "event_launch_news_beep_in_home";
    public static final String EVENT_LAUNCH_LIVE_TV_IN_HOME = "event_launch_live_tv_in_home";
    public static final String EVENT_LAUNCH_RECENT_READ_NEWS = "event_recent_read_news";
    public static final String EVENT_LAUNCH_RECENT_VIEWED_TOPIC = "event_recent_viewed_topic";
    public static final String EVENT_LAUNCH_LOYALTY_PROGRAM = "event_launch_loyalty";
    public static final String EVENT_LAUNCH_CHANGE_PASSWORD = "event_change_password";
    public static final String EVENT_LAUNCH_SHARE_APP = "event_launch";
    public static final String EVENT_LOG_OUT = "event_profile_item_clicked";
    public static final String EVENT_UPDATE_PROFILE_UI = "event_update_profile_ui";
    public static final String EVENT_FORGOT_PSWRD_SUCCESS = "event_forgot_password";
    public static final String EVENT_EDIT_PROFILE = "event_edit_profile";
    public static final String EVENT_LAUNCH_MAUSAM_IN_HOME = "event_launch_mausam_in_home";
    public static final String EVENT_LAUNCH_CATEGORY_SELECTION_IN_HOME = "event_launch_category_selection_in_home";
    public static final String EVENT_OPEN_DRAWER = "event_open_drawer";
    public static final String EVENT_CLOSE_DRAWER = "event_close_drawer";
    public static final String EVENT_LAUNCH_NEWS_ROUND_UP_IN_HOME = "event_launch_news_round_up_in_home";
    public static final String EVENT_MENU_BACK_BUTTON_CLICK = "event_menu_back_button_click";
    public static final String EVENT_LAUNCH_SHEHER_ME_AAJ_IN_HOME = "event_launch_sheher_me_aaj_in_home";
    public static final String EVENT_LOGIN_SUCCESS = "event_login_success";
    public static final String EVENT_SOCIAL_CONNECT_SUCCESS = "event_soial_success";
    public static final String EVENT_REGISTRATION_SUCCESS = "event_registration_success";
    public static final String EVENT_SIGN_UP_CLICKED = "event_sign_up_clicked";
    public static final String EVENT_REGISTRATION_DIALOG = "event_registration_dialog";
    public static final String EVENT_CHANGE_PASSWORD_DIALOG = "event_change_password_dialog";
    public static final String EVENT_USER_DETAILS_SKIP = "event_user_details_skip";
    public static final String EVENT_USER_DETAILS_SUCCESS = "event_user_details_success";
    public static final String EVENT_SET_PASSWORD_SUCCESS = "event_set_password_success";
    public static final String EVENT_DELETE_PROFILE_PICTURE = "event_delete_profile_picture";
    public static final String EVENT_YOUR_INTEREST_TAG_CLICKED = "event_your_interest_tag_clicked";
    public static final String EVENT_AUTHOR_ITEM_CLICKED = "event_author_item_clicked";
    public static final String EVENT_CAPTCHA = "event_captcha_generated";
    public static final String EVENT_LAUNCH_CONTEST_IN_HOME = "event_launch_contest_in_home";
    public static final String EVENT_LAUNCH_CONTEST = "event_launch_contest";
    public static final String EVENT_LAUNCH_BOOKMARK_IN_HOME = "event_launch_bookmark_in_home";
    public static final String EVENT_LAUNCH_SAVED_OFFLINE_IN_HOME = "event_launch_saved_offline_in_home";
    public static final String EVENT_LAUNCH_SUBSCRIPTION = "event_subscription_launch";
    public static final String EVENT_QR_CODE = "event_qr_code";

    public static final String EVENT_HOME_TAB_CARD_CLICKED = "event_home_tab_card_clicked";
    public static final String EVENT_PROFILE_UPDATE_SUCCESS_DIALOG = "event_profile_update_success_dialog";

    public static final String EVENT_FORGOT_PASSWORD_NAVIGATION = "event_forgot_password_navigation";

    //
    public static final String EVENT_SETTING_ITEM_CLICK = "event_setting_item_click";
    public static final String EVENT_FOR_YOU_VIDEO_CLICK = "event_for_you_video_click";
    public static final String EVENT_SEARCH_SUGGESTION_CLICK = "event_search_suggestion_click";
    public static final String EVENT_FEEDBACK_SUBJECT_SELECTED = "event_feedback_subject_selected";

    //Item Clicked
    public static final String EVENT_VIEW_MORE_REPLY_CLICKED = "event_view_more_reply_click";
    public static final String EVENT_ADD_COMMENT_CLICKED = "event_add_comment_click";
    public static final String EVENT_EDIT_COMMENT_CLICKED = "event_edit_comment_click";
    public static final String EVENT_DELETE_COMMENT_CLICKED = "event_delete_comment_click";
    public static final String EVENT_COMMENT_ACTIONS_CLICKED = "event_comment_actions_click";
    public static final String EVENT_FOLLOW_UNFOLLOW_TAG = "event_follow_un_follow_tag";

    public static final String EVENT_KUL_ANK_CLICKED = "EVENT_KUL_ANK_CLICKED";
    public static final String EVENT_LOYALTY_LEADERBOARD_CLICKED = "EVENT_LOYALTY_LEADERBOARD_CLICKED";
    public static final String EVENT_LOYALTY_LUCKY_DRAW_WINNER_CLICKED = "event_lucky_draw_winnner_clicked";

    //Live Blog
    public static final String EVENT_LIVE_BLOG_SHARE_CLICKED = "event_live_blog_share_clicked";
    public static final String EVENT_LIVE_BLOG_TITLE_CLICKED = "event_live_blog_title_clicked";
    public static final String EVENT_LIVE_BLOG_IMAGE_CLICKED = "event_live_blog_image_clicked";
    public static final String EVENT_LIVE_BLOG_VIDEO_CLICKED = "event_live_blog_video_clicked";

    /*PopUp window*/
    public static final String EVENT_POPUP_SHARE_CLICLKED = "event_popup_share_clicked";
    public static final String EVENT_BOOKMARK_LIST_SHARE_CLICKED = "event_bookmark_list_share_clicked";

    public static final String EVENT_POPUP_FLICKR_SHARE_CLICLKED = "event_popup_flickr_share_clicked";
    public static final String EVENT_POPUP_SAVE_CLICLKED = "event_popup_save_clicked";
    public static final String EVENT_POPUP_ITEM_CLICLKED = "event_popup_item_clicked";
    public static final String EVENT_POPUP_REPORT_ABUSE_CLICLKED = "event_popup_report_abuse_clicked";
    public static final String EVENT_POPUP_REMOVE_CLICLKED = "event_popup_remove_clicked";

    public static final String EVENT_FOR_YOU_VIDEO_FETCHED = "event_video_fetched";
    public static final String EVENT_SEND_SEARCH_RESULT = "event_send_search_result";
    public static final String EVENT_CONTEST_LIST_DATA = "event_contest_list_data";
    public static final String EVENT_CATEGORY_UNCHECKED = "event_category_unchecked";
    public static final String EVENT_CATEGORY_CHECKED = "event_category_checked";

    /*Camera and Gallery Selection*/
    public static final String EVENT_CHOOSE_CAMERA_PHOTO = "event_choose_camera_photo";
    public static final String EVENT_CHOOSE_CAMERA_VIDEO = "event_choose_camera_video";
    public static final String EVENT_CHOOSE_SINGLE_PHOTO = "event_choose_single_photo";
    public static final String EVENT_CHOOSE_SINGLE_VIDEO = "event_choose_single_video";
    public static final String EVENT_CHOOSE_MULTIPLE_PHOTO = "event_choose_multiple_photo";
    public static final String EVENT_CHOOSE_MULTIPLE_VIDEO = "event_choose_multiple_video";

    public static final String EVENT_CHAT_BOT_REPLY = "event_chat_bot_reply";
    public static final String EVENT_LAUNCH_SETTINGS_IN_HOME = "event_launch_settings_in_home";
    public static final String EVENT_LAUNCH_HOME_REORDER_IN_HOME = "event_launch_home_reorder_in_home";

    /* Citizen Journalism */
    public static final String EVENT_CITIZEN_JOURNALISM_ITEM_CLICK = "event_cj_item_click";
    public static final String EVENT_NEWS_ITEM_CLICK = "event_news_item_click";
    public static final String EVENT_MORE_TAB_SELECTED = "event_more_tab_selected";
    public static final String LAUNCH_SCREEN_IN_HOME = "event_launch_in_home";
    public static final String EVENT_CITIZEN_JOURNALISM_UPLOAD_SUCCESS = "event_cj_upload_success";
    public static final String EVENT_CITIZEN_JOURNALISM_UPLOAD_ERROR = "event_cj_upload_error";

    //Media Listing
    public static final String EVENT_PHOTO_ITEM_CLICKED = "event_photo_item_clicked";
    public static final String EVENT_VIDEO_ITEM_CLICKED = "event_video_item_clicked";
    public static final String EVENT_OVERLAY_VIDEO_ITEM_CLICKED = "event_overlay_video_item_clicked";
    public static final String EVENT_PANORAMA_ITEM_CLICK = "event_panorama_item_click";

    public static final String EVENT_LIVE_TV_SEARCH_TERM_CHANGED = "event__live_tv_search_term_changed";
    public static final String EVENT_LIVE_TV_ITEM_CLICKED = "event_live_tv_item_clicked";
    public static final String EVENT_CLOSE_SEARCH_BAR = "event_close_search_bar";
    public static final String EVENT_SEE_MORE_URL = "event_see_more_url";
    public static final String EVENT_REMOVE_UPLOADED_IMAGE = "event_remove_uploaded_image";
    public static final String EVENT_ON_LOCATION_DATA_SUCCESS = "event_on_location_data_success";
    public static final String EVENT_AUTHOR_FOLLOWED_CHANGED = "event_author_followed_changed";
    public static final String EVENT_REFRESH_AUTHOR_LIST = "event_refresh_author_list";

    //Notification Screen
    public static final String EVENT_NOTIFICATON_CLICKED = "event_notification_clicked";
    public static final String EVENT_DND_CLICKED = "event_dnd_clicked";
    public static final String EVENT_SOUND_CLICKED = "event_sound_clicked";
    public static final String EVENT_TOPIC_CLICKED = "event_topic_clicked";
    public static final String EVENT_FREQUENCY_CLICKED = "event_frequency_clicked";
    public static final String EVENT_VIDEO_QUALITY_CLICKED = "event_video_quality_clicked";
    public static final String EVENT_BEST_QULAITY_WIFI_CLICKED = "event_best_quality_wifi_clicked";
    public static final String EVENT_UPDATE_HOME_TOOLBAR_COLOR = "event_update_home_toolbar_color";
    public static final String EVENT_NOTIFICATION_ALERT_TYPE_CLICKED = "event_notification_alert_type_clicked";
    public static final String EVENT_BOOKMARK_UPDATED = "event_bookmark_updated";

    public static final String EVENT_NEWS_BOOKMARK_STATUS_CHANGED = "event_news_bookmark_status_changed";

    //NewsDetailEvents
    public static final String EVENT_ON_PAGER_SCROLLED = "event_pager_scrolled";
    public static final String EVENT_TEXT_SIZE_CHANGED = "event_text_size_changed";
    public static final String EVENT_PHOTO_PICKED = "event_photo_picked";
    public static final String EVENT_PHOTO_CLICKED = "EVENT_PHOTO_CLICKED";
    public static final String EVENT_NEWS_DETAIL_PANAROMA_ITEM_CLICKED = "EVENT_NEWS_DETAIL_PANAROMA_ITEM_CLICKED";
    public static final String EVENT_NEWS_AR_ITEM_CLICKED = "EVENT_NEWS_AR_ITEM_CLICKED";
    public static final String EVENT_SEE_MORE_PHOTO_CLICKED = "event_photo_picked";
    public static final String EVENT_LIVE_BLOG_TEXT_SIZE_CHANGE = "event_live_blog_text_resize";

    //ChitChat
    public static final String EVENT_HTML_LINK = "event_html_link";
    public static final String EVENT_AR_PANOROMA_CLICKED = "event_ar_panorama_clicked";
    public static final String EVENT_CHIT_CHAT_VIDEO_CLICKED = "event_chit_chat_video_clicked";
    public static final String EVENT_CHIT_CHAT_ACTION_CLICKED = "event_chit_chat_action_clicked";
    public static final String EVENT_CHIT_CHAT_CONTENT_LOADED = "event_chit_chat_content_loaded";
    public static final String EVENT_CHIT_CHAT_GOODBYE_CONTENT = "event_chit_chat_goodbye_content";

    //ChromeCast
    public static final String EVENT_CAST_SESSION_CONNECTED = "event_cast_session_connected";
    public static final String EVENT_CAST_SESSION_DISCONNECTED = "event_cast_session_disconnected";
    public static final String EVENT_FEEDBACK_SUCCESS_CONTINUE_CLICK = "event_feedback_success_click";
    public static final String EVENT_OPEN_SELECT_LOCATION = "event_select_location";
    public static final String EVENT_OPEN_TRAFFIC_UPDATES = "event_traffic_update";


    public static final String EVENT_CJ_RETRY = "event_cj_action_click";
    //Preferred City
    public static final String EVENT_PREFERRED_CITY = "event_preferred_city";
    public static final String PREFERRED_CITY_INFO = "preferred_city_info";


    public static final String EVENT_CJ_CANCEL = "event_cj_cancel";
    public static final String EVENT_SHEHER_ME_AAJ_CITY_SELECT = "event_sheher_me_aaj_city_select";
    public static final String EVENT_SHEHER_ME_AAJ_DATE_SELECT = "event_sheher_me_aaj_date_select";

    // Citizen Generalism Pop Up events
    public static final String EVENT_POPUP_CJ_SAVE_CLICKED = "event_citzen_generalism_save";
    public static final String EVENT_POPUP_CJ_REMOVE_CLICKED = "event_citzen_generalism_remove";
    public static final String EVENT_POPUP_CJ_SHARE_CLICKED = "event_citzen_generalism_share";
    public static final String EVENT_TAB_SWITCHED = "event_tab_switched";

    public static final String EVENT_LIVE_TV_DETAIL_SUCCESS = "event_live_tv_detail_success";
    public static final String EVENT_GAME_CLICK = "event_game_click";
    public static final String EVENT_YE_BE_PHADE = "event_ye_be_phade";
    public static final String EVENT_MOVIE_SESSION_CLICK = "event_movie_session_click";
    public static final String EVENT_MOVIE_TRAILER_CLICK = "event_movie_trailer_click";
    public static final String EVENT_LIVE_BLOG_CLICK = "live_blog_click";
    public static final String EVENT_ROUNDUP_VIDEO_CLICK = "event_roundup_video_click";
    public static final String EVENT_ROUNDUP_IMAGE_CLICK = "event_roundup_image_click";
    public static final String EVENT_PAGE_NEWS_IMAGE_CLICKED_NEWSDETAIL = "event_news_detil_pager_image_clicked";
    public static final String EVENT_PAGE_NEWS_IMAGE_CLICKED_LIVEBLOG = "event_news_detil_pager_image_clicked";
    public static final String EVENT_PAGE_NEWS_IMAGE_CLICKED_MYUPLOAD = "event_news_detil_pager_image_clicked";
    public static final String EVENT_PAGE_NEWS_IMAGE_CLICKED = "event_news_detil_pager_image_clicked";
    public static final String EVENT_EDIT_HOME_EXCEED_MAX_COUNT = "event_edit_home_exceed_max_count";
    public static final String EVENT_EVENT_EMAIL_CLICK = "event_sma_email_click";
    public static final String EVENT_EVENT_LOCATION_CLICKED = "event_sma_location-click";
    public static final String EVENT_EVENT_CONTACT_CLICKED = "event_sma_contact_click";
    public static final String EVENT_REGISTRATION_DIALOG_NAVIGATE_LOGIN = "event_registartion_dialog_navigate_login";
    public static final String EVENT_NEWS_BEEP_CLICKED = "event_news_beep_item_click";
    public static final String EVENT_PROFILE_PIC_UPLOAD_ICON_CLICK = "event_profile_pic_upload_icon_click";
    public static final String EVENT_LAUNCH_COMMENTS_LIST_FROM_LOGIN = "launch_comments_list_from_login";
    /*public static final String EVENT_PAGE_SWIPE = "event_page_swipe";*/
    public static final String AR_LOADING_FAILS = "event_ar_loading_fails";

    //Bottom Bar Navigation Event
    public static final String BOTTOM_NAVIGATION_EVENT_SHOW = "bottom_navigation_bar_show";
    public static final String BOTTOM_NAVIGATION_EVENT_HIDE = "bottom_navigation_bar_hide";
    public static final String EVENT_RASHI_ITEM_SELECTED = "event_rashi_item_selected";
    public static final String EVENT_SEARCH_RESULT_CLICK = "event_search_result_click";
    public static final String EVENT_PROMPT_LOGIN = "event_prompt_login";
    public static final String EVENT_SUBCATEGORY_TITLE_CLICK = "event_sub_cat_title_click";
    public static final String EVENT_SUBSCRIPTION_CLICK = "event_subscription_click";
    public static final String EVENT_SUBSCRIPTION_REFRESH = "event_subscription_success";

    public static final String EVENT_SUBSCRIPTION_SCREEN_ENDED = "event_subscription_screen_ended";
    public static final String EVENT_SUBSCRIPTION_FAILED = "even_subscription_failed";
    public static final String EVENT_MENU_CLICK = "event_menu_click";
    public static final String EVENT_TOP_NEWS_TITLE_CLICK = "event_top_news_title_click";
    public static final String EVENT_PROFILE_IMAGE_CLICKED = "event_profile_image_clicked";
    public static final String EVENT_RASHIFAL_CARD_CLICKED = "event_rashifal_card_click";
    public static final String EVENT_LOG_OUT_SUCCESS = "event_log_out_";
    public static final String EVENT_CJ_STORY_UPLOADED = "event_cj_story_uploaded";
    public static final String LAUNCH_MULTIMEDIA_ITEM_CLICK = "launch_multi_media_item_click";
    public static final String TYPE_NEWS_TAG = "type_news_tag";
    public static final String TYPE_NEWS_AUTHOR = "type_news_author";
    public static final String EVENT_SETTINGS_SCREEN = "settings_event_screen";
    public static final String EVENT_OTHER_ITEM_CLICK = "event_other_item_click";
    public static final String EVENT_PRIME_ITEM_CLICK = "event_prime_item_click";
    public static final String EVENT_PHOTO_TAB_CHANGED = "event_photo_tab_changed";
    public static final String EVENT_SHOW_RASHIPHAL_ADDS = "event_show_rashiphal_adds";
    public static final String EVENT_NOTIFICATION_TAP = "event_notification_tap";
    public static final String EVENT_UPDATE_DETAILS_TOOLBAR = "event_update_details_toolbar";
    public static final String EVENT_BACK_CLICKED_NEWS_DETAIL = "event_back_clicked_news_detail";
    public static final String EVENT_AD_RELOAD = "EVENT_AD_RELOAD";
    public static final String EVENT_CITY_SELECTED = "event_city_selected";
    public static final String EVENT_UPDATE_MY_INTEREST = "event_update_my_interest";
    public static final String EVENT_API_403_FOUND = "event_api_403_found";
    public static final String EVENT_UPDATE_COMMENT_COUNT = "event_update_comment_count";
    public static final String EVENT_UPDATE_STATUS_BAR = "event_update_status_bar";
    public static final String EVENT_PLACE_SELECTED = "event_place_selected";
    public static final String EVENT_LIVE_BLOG_CARD_CLICK = "event_live_blog_card_click";
    public static final String EVENT_CHIT_CHAT_LINK_CLICK = "event_chit_chat_link_click";
    public static final String EVENT_UPDATE_GA_TAG = "event_update_ga_tag";
    public static final String EVENT_UPDATE_GA_TAG_FOR_UPPER_MENU = "event_update_ga_tag_for_upper_menu";
    public static final String EVENT_CATEGORY_CHECKED_EDIT_HOME = "event_category_checked_edit_home";
    public static final String EVENT_EDIT_HOME_ITEM_MOVED = "edit_home_item_moved";
    public static final String EVENT_PHOTO_OR_VIDEO_GALLERY_SWIPE = "event_photo_or_video_gallery_swipe";
    public static final String EVENT_GA_VIDEO_SCROLL = "event_ga_video_scroll";
    //    public static final String EVENT_GA_VIDEO_PLAY_25_PERCENT = "event_ga_video_play_25_percent";
//    public static final String EVENT_GA_VIDEO_PLAY_50_PERCENT = "event_ga_video_play_50_percent";
//    public static final String EVENT_GA_VIDEO_PLAY_75_PERCENT = "event_ga_video_play_75_percent";
//    public static final String EVENT_GA_VIDEO_PLAY_100_PERCENT = "event_ga_video_play_100_percent";
    public static final String EVENT_UNLOCK_PROMO_CODE = "event_unlock_promo_code";
    public static final String PHOTO_ITEM_CLICKED_GA_EVENT = "photo_item_clicked_ga_event";
    public static final String EVENT_GENDER_SELECTED = "event_gender_selected";
    public static final String EVENT_UPDATE_TOOLBAR_FROM_TAG = "event_update_toolbar_from_tag";
    public static final String EVENT_VIDEO_QUALITY_CHANGED = "event_video_quality_changed";
    public static final String EVENT_LOGIN_SUCCESS_REDIRECT = "event_login_success_redirect";
    public static final String EVENT_PANORAMA_RESUME_RENDERING = "event_panorama_resume_rendering";
    public static final String EVENT_PANORAMA_PAUSE_RENDERING = "event_panorama_pause_rendering";

    //login request events
    public static final String LOGIN_TO_FOLLOW_UNFOLLOW_AUHTOR = "login_to_follow_unfollow_author";
    public static final String EVENT_LAUNCH_CREATE_CJ_ON_LOGIN = "event_launch_create_cj_on_login";

    // Flickr card events
    public static final String EVENT_FLICKR_ITEM_CLICK = "event_flickr_item_click";
    public static final String EVENT_SALUTATION_LINK_CLICK = "event_salutation_click";
    public static final String EVENT_WEATHER_INFO_CLICK = "event_weather_info_click";
    public static final String EVENT_WEATHER_INFO_REMOVED = "event_weather_info_removed";
    public static final String EVENT_ON_FLICKR_PAGER_SCROLLED = "event_flickr_pager_scrolled";

    public static final String EVENT_ON_VIDEO_GALLERY_PAGER_SCROLLED = "event_video_gallery_pager_scrolled";

    public static final String EVENT_ON_NOTI_LIST_PAGER_SCROLLED = "event_noti_list_pager_scrolled";
    public static final String EVENT_ON_OVERLAY_VIDEO_PAGER_SCROLLED = "event_overlay_video_pager_scrolled";
    public static final String EVENT_ON_NEWS_DETAIL_PAGER_DESTROYED = "event_news_detail_pager_destroyed";

    public static final String EVENT_ON_FLICKR_SECTION_SELECTED = "event_flicker_section_selected";
    public static final String EVENT_ON_OPEN_MENU_SUB_SECTION = "event_open_menu_sub_section";

    public static final String EVENT_LOG_IN_FROM_MULTI_CONTAINER = "event_login_from_multi_container";
    public static final String EVENT_LOGGED_IN_SUCCESS_FROM_MULTI_CONTAINER = "event_logged_in_success_from_multi_container";
    public static final String EVENT_APP_UPDATE_FROM_MULTI_CONTAINER = "event_app_update_from_multi_container";

    public static final String EVENT_LOG_IN_FROM_CHUNAVI_PANDIT = "event_login_from_chunavi_pandit";

    private String flag;
    private String tag;
    private T data;
    private int screenType;
    private String filter;

    private ArrayList<T> items;

    public NavigationEvent(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getScreenType() {
        return screenType;
    }

    public void setScreenType(@AppConstants.SCREEN_TYPE int type) {
        screenType = type;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public ArrayList<T> getItems() {
        return items;
    }

    public void setItems(ArrayList<T> items) {
        this.items = items;
    }
}
