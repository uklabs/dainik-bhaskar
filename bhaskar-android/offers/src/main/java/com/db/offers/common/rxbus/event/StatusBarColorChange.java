/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.common.rxbus.event;


import com.db.offers.common.GsonProguardMarker;

public class StatusBarColorChange implements GsonProguardMarker {

    private int type;

    public StatusBarColorChange(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
