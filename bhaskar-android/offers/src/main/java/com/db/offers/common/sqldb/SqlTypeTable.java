package com.db.offers.common.sqldb;


import com.db.offers.common.GsonProguardMarker;

public interface SqlTypeTable extends GsonProguardMarker {
    String getPrimaryKeyColumn();
}
