package com.db.offers.common.utils;

import android.app.Activity;

public interface DialogOkInterface {
	void doOnOkBtnClick(Activity activity);
}
