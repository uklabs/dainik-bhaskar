package com.db.offers.common.utils;

import android.app.Activity;

public interface DialogResponseInterface {
	void doOnPositiveBtnClick(Activity activity);
	void doOnNegativeBtnClick(Activity activity);
}
