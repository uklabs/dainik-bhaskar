package com.db.offers.megaoffers;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.db.offers.common.SharedPrefUtil;
import com.db.offers.megaoffers.constants.MegaOffersConstants;
import com.db.offers.megaoffers.models.MegaOfferDatePointResponse;


public final class MegaOfferSharedPref {

    private final String KEY_MEGA_SHARED_PREF = "MEGA_OFFER_SP";
    private final String KEY_MEGA_TODAY_DATE = "key_mega_today_date";
    private final String KEY_MEGA_MAX_POINTS = "key_mega_max_points";

    private final String KEY_ACCEPTED_OFFER_ID = "key_accepted_offer_id";

    private final SharedPreferences sp;

    private static MegaOfferSharedPref megaOfferSharedPref;

    /***
     *
     * @param context
     * @return
     */
    public static MegaOfferSharedPref getInstance(Context context) {
        if (megaOfferSharedPref == null) {
            megaOfferSharedPref = new MegaOfferSharedPref(context.getApplicationContext());
        }
        return megaOfferSharedPref;
    }

    /**
     * @param context
     */
    private MegaOfferSharedPref(Context context) {
        sp = context.getSharedPreferences(KEY_MEGA_SHARED_PREF, Context.MODE_PRIVATE);
    }

    /***
     *
     * @param megaOfferDateMaxPoint
     */
    public void saveTodayMegaOfferDetail(@NonNull MegaOfferDatePointResponse.MegaOfferDateMaxPoint megaOfferDateMaxPoint) {
        SharedPrefUtil.addString(sp, KEY_MEGA_TODAY_DATE, megaOfferDateMaxPoint.getCurrentDate());
        SharedPrefUtil.addInt(sp, KEY_MEGA_MAX_POINTS, megaOfferDateMaxPoint.getTodayMaxPoints());
    }

    /****
     *
     * @param offerId
     */
    public void saveUserAcceptedOffer(int offerId) {
        SharedPrefUtil.addInt(sp, KEY_ACCEPTED_OFFER_ID, offerId);
        ////SharedPrefUtil.addInt(sp, KEY_ACCEPTED_PLAN_ID, planId);
    }

    public int getUserAcceptedOfferId() {
        return SharedPrefUtil.getInt(sp, KEY_ACCEPTED_OFFER_ID, 0);
    }

    public boolean isUserAcceptedAnyOffer() {
        return SharedPrefUtil.getInt(sp, KEY_ACCEPTED_OFFER_ID, 0) != 0;
    }


    /*public int getUserAcceptedPlanId() {
        return SharedPrefUtil.getInt(sp, KEY_ACCEPTED_PLAN_ID, 0);
    }*/

    public void removeUserAcceptedOffer() {
        SharedPrefUtil.removeKeyValue(sp, KEY_ACCEPTED_OFFER_ID);
        ////haredPrefUtil.removeKeyValue(sp, KEY_ACCEPTED_PLAN_ID);
    }

    public String getMegaOfferTodayDate() {
        return SharedPrefUtil.getString(sp, KEY_MEGA_TODAY_DATE, "");
        //CommonUtils.getDateWithPattern(Calendar.getInstance().getTime(), MegaOffersConstants.CURRENT_DATE_FORMAT));
    }

    public int getMaxPointCount() {
        return SharedPrefUtil.getInt(sp, KEY_MEGA_MAX_POINTS, MegaOffersConstants.MAX_POINTS_COUNT);
    }


}
