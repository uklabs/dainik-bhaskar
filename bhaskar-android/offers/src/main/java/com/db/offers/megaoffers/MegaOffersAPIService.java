package com.db.offers.megaoffers;


import com.db.offers.common.SyncResponseModel;
import com.db.offers.megaoffers.constants.MegaOffersAPIConstants;
import com.db.offers.megaoffers.model.request.AcceptOfferPlanRequest;
import com.db.offers.megaoffers.model.request.PointsRequest;
import com.db.offers.megaoffers.model.response.AcceptMegaOffer;
import com.db.offers.megaoffers.model.response.MegaOffersListingModel;
import com.db.offers.megaoffers.models.MegaOfferDatePointResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface MegaOffersAPIService {

    //new
    @GET(MegaOffersAPIConstants.ApiEndPoint.GET_MEGA_OFFERS_LIST)
    Call<MegaOffersListingModel> getMegaOffersList(@Query("mobile_no") String mobileNo);

    //new
    @POST(MegaOffersAPIConstants.ApiEndPoint.ACCEPT_MEGA_OFFER)
    Call<AcceptMegaOffer> acceptMegaOffer(@Body AcceptOfferPlanRequest acceptOfferPlanRequest);

    @GET
    Call<MegaOfferDatePointResponse> getMegaOfferDateMaxPointsCount(@Url String getDateUrl, @Query("offer_id") int offerId);

    //new
    @POST(MegaOffersAPIConstants.ApiEndPoint.SYNC_MEGA_OFFER_POINTS)
    Call<SyncResponseModel> syncMegaOfferPoints(@Body PointsRequest pointsRequest);
}
