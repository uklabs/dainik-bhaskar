package com.db.offers.megaoffers.activity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.db.offers.R;
import com.db.offers.R2;
import com.db.offers.common.BundleConstants;
import com.db.offers.common.TransAviLoader;
import com.db.offers.common.Utils;
import com.db.offers.common.activity.CoreActivity;
import com.db.offers.megaoffers.callbacks.PointsSyncCallback;
import com.db.offers.megaoffers.constants.MegaOffersConstants;
import com.db.offers.common.AndWebView;
import com.db.offers.common.AppConstants;
import com.db.offers.common.AppNavigatorHelper;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.DBToast;
import com.db.offers.megaoffers.providers.MegaOfferPointsUtils;
import com.db.offers.megaoffers.providers.WebJavaScripInterfaceProvider;
import com.db.offers.megaoffers.providers.WebViewClientProvider;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.OnClick;

public final class MegaOffersDashboardActivity extends CoreActivity {

    @BindView(R2.id.mo_progress_bar_av)
    AVLoadingIndicatorView mWebViewProgressBar;
    @BindView(R2.id.iv_back)
    ImageView mIvBack;
    @BindView(R2.id.tv_header_title)
    TextView mTvHeaderTitle;
    @BindView(R2.id.iv_home)
    ImageView mIvHome;
    @BindView(R2.id.wv_dashboard)
    AndWebView mWvDashboard;
    @BindView(R2.id.tv_unable_to_load_dashboad)
    TextView mTvUnableToLoad;

    private static final String TAG = MegaOffersDashboardActivity.class.getSimpleName();
    private WebJavaScripInterfaceProvider mInterfaceProvider;
    private WebViewClientProvider mWebViewClientProvider;
    private String mWebViewUrl, mHeaderTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mega_offers_dashboard);
        initUI();
    }

    private void initUI() {
        extractBundleData();
        initWebView();
        if (CommonUtils.isNetworkAvailable(dbApplication)) {
            loadUrlOnWebView();
            syncMegaOfferPoints();
        } else {
            noInternetConnectivity();
        }
    }

    private void syncMegaOfferPoints() {
        final TransAviLoader transAviLoader = new TransAviLoader(this);
        transAviLoader.setLoaderColor(ContextCompat.getColor(this, R.color.divya_theme_color));
        transAviLoader.show();
        MegaOfferPointsUtils.getAllPointsAndSyncToServer(MegaOffersDashboardActivity.this, new PointsSyncCallback() {
            @Override
            public void onSyncSuccess() {
                transAviLoader.dismiss();
                mWvDashboard.reload();
                DBToast.showSuccessToast(MegaOffersDashboardActivity.this, getString(R.string.alert_title_success), getString(R.string.alert_mo_sync_success));
            }

            @Override
            public void onPointsNotAvailable() {
                transAviLoader.dismiss();
                DBToast.showSuccessToast(MegaOffersDashboardActivity.this, getString(R.string.alert_title_success), getString(R.string.alert_mo_sync_success));
            }

            @Override
            public void onSyncFailed(String message) {
                transAviLoader.dismiss();
            }
        });
    }

    private void noInternetConnectivity() {
        mWebViewProgressBar.setVisibility(View.GONE);
        mWvDashboard.setVisibility(View.GONE);
        mTvUnableToLoad.setVisibility(View.VISIBLE);
        mTvUnableToLoad.setText(getString(R.string.alert_network_not_exist));
    }

    private void extractBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            int mNavigationType = bundle.getInt(BundleConstants.KEY_NAVIGATE_FROM, AppConstants.NavigationFlow.NAVIGATE_FROM_HOME);
            mHeaderTitle = bundle.getString(MegaOffersConstants.BundleConstants.KEY_HEADER_TITLE);
            mWebViewUrl = bundle.getString(MegaOffersConstants.BundleConstants.KEY_DASHBOARD_URL);

            if (AppConstants.NavigationFlow.NAVIGATE_FROM_SPLASH == mNavigationType) {
                setVisibilities(View.GONE, View.VISIBLE);
            } else {
                setVisibilities(View.VISIBLE, View.GONE);
            }
            setHeaderTitle();
        }
    }

    /**
     * This method is used to set visibilities of back & home icon
     *
     * @param backIconVisibility
     * @param skipIconVisibility
     */
    private void setVisibilities(int backIconVisibility, int skipIconVisibility) {
        mIvBack.setVisibility(backIconVisibility);
        mIvHome.setVisibility(skipIconVisibility);
    }

    private void setHeaderTitle() {
        try {
            if (!TextUtils.isEmpty(mHeaderTitle)) {
                mTvHeaderTitle.setText(mHeaderTitle);
            } else {
                mTvHeaderTitle.setText(getString(R.string.mega_offer_header_title));
            }
        } catch (Exception e) {
            Log.e(TAG, "setHeaderTitle: " + e.getMessage());
        }
    }

    private void initWebView() {
        WebSettings webViewSetting = mWvDashboard.getSettings();
        webViewSetting.setLoadsImagesAutomatically(true);
        webViewSetting.setDomStorageEnabled(true);
        mWvDashboard.clearCache(true);
        mWvDashboard.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        mWvDashboard.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        addJavaScriptInterface();
        setWebViewClient();
    }

    private void addJavaScriptInterface() {
        mInterfaceProvider = new WebJavaScripInterfaceProvider(this);
        mInterfaceProvider.addJsInterface(mWvDashboard);
    }

    private void setWebViewClient() {
        try {
            mWebViewClientProvider = new WebViewClientProvider(this, new WebViewClientProvider.WebPageListener() {
                @Override
                public void onWebPageFinished(WebView view, String url) {
                    try {
                        mWebViewProgressBar.smoothToHide();
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage() + "");
                    }
                }
            });

            mWvDashboard.setWebViewClient(mWebViewClientProvider);
        } catch (Exception e) {
            Log.e(TAG, "setWebViewClient: " + e.getMessage());
        }
    }

    private void loadUrlOnWebView() {
        try {
            if (!TextUtils.isEmpty(mWebViewUrl) && null != mWebViewClientProvider.getWhiteListWebUrls() && !mWebViewClientProvider.getWhiteListWebUrls().isEmpty()) {
                if (Utils.isWebUrlDBInternal(mWebViewUrl, mWebViewClientProvider.getWhiteListWebUrls())) {
                    mWvDashboard.loadUrl(mWebViewUrl, Utils.getOfferDashboardWebHeader(this));
                } else {
                    mWvDashboard.loadUrl(mWebViewUrl);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "loadUrlOnWebView: " + e.getMessage());
        }
    }

    @OnClick({R2.id.iv_back, R2.id.iv_home, R2.id.iv_sync})
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            onBackPressed();
        } else if (id == R.id.iv_home) {
            AppNavigatorHelper.getInstance().launchHomeActivity(this);
        } else if (id == R.id.iv_sync) {
            rotateSync(v);
            syncMegaOfferPoints();
        }
    }

    private void rotateSync(View v) {
        try {
            ObjectAnimator rotate = ObjectAnimator.ofFloat(v, "rotation", 180f, 0f);
            rotate.setRepeatCount(2);
            rotate.setDuration(500);
            rotate.start();
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        if (mWvDashboard.canGoBack()) {
            mWvDashboard.goBack();
        } else {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (mWvDashboard != null && null != mInterfaceProvider) {
            mInterfaceProvider.removeJsInterface(mWvDashboard);
            mWvDashboard.destroy();
        }
        super.onDestroy();
    }

    /**
     * @param coreActivity
     * @param bundle
     */
    public static void launchMegaOffersDashboard(Context coreActivity, Bundle bundle) {
        Intent intent = new Intent(coreActivity, MegaOffersDashboardActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        coreActivity.startActivity(intent);
    }

    /**
     * @param coreActivity
     * @param bundle
     */
    public static void launchMegaOffersDashboardFromSplash(Context coreActivity, Bundle bundle) {
        Intent intent = new Intent(coreActivity, MegaOffersDashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        coreActivity.startActivity(intent);
    }
}
