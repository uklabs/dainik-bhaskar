package com.db.offers.megaoffers.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.db.offers.OfferController;
import com.db.offers.R;
import com.db.offers.R2;
import com.db.offers.common.AppConstants;
import com.db.offers.common.AppNavigatorHelper;
import com.db.offers.common.BundleConstants;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.db.offers.common.ImageUtil;
import com.db.offers.common.activity.CoreActivity;
import com.db.offers.megaoffers.adapter.MegaOffersListingAdapter;
import com.db.offers.megaoffers.callbacks.MegaOfferSelectionCallback;
import com.db.offers.megaoffers.constants.MegaOffersConstants;
import com.db.offers.megaoffers.model.response.MegaOffers;
import com.db.offers.megaoffers.model.response.MegaOffersDetail;
import com.db.offers.megaoffers.model.response.MegaOffersPlanDetail;
import com.bhaskar.appscommon.tracking.Tracking;

import java.util.List;

import butterknife.BindView;


public final class MegaOffersListingActivity extends CoreActivity implements View.OnClickListener {

    @BindView(R2.id.iv_back)
    ImageView mIvBack;
    @BindView(R2.id.tv_header_title)
    TextView mTvHeaderTitle;
    @BindView(R2.id.tv_skip)
    TextView mTvSkip;
    @BindView(R2.id.nsv_offer)
    NestedScrollView mNsvMegaOffers;
    @BindView(R2.id.iv_mega_offer)
    ImageView mIvMegaOffer;
    @BindView(R2.id.tv_choose_offer)
    TextView mTvChooseOfferTitle;
    @BindView(R2.id.tv_choose_offer_description)
    TextView mTvChooseOfferDescription;
    @BindView(R2.id.rv_mega_offers)
    RecyclerView mRvMegaOffers;
    @BindView(R2.id.tv_no_mega_offer_available)
    TextView mTvNoMegaOffers;

    private static final String TAG = MegaOffersListingActivity.class.getSimpleName();
    private String mDashboardUrl;
    private int mNavigationType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mega_offers_listing);
        initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Track CleverTap screen view
//        CTTracker.cleverTapTrackPageView(this, CTConstant.PROP_VALUE.MegaOffer.SCREEN_CHOOSE_YOUR_OFFER, CTConstant.PROP_VALUE.MegaOffer.SCREEN_MEGA_OFFER);
        Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.MegaOffer.SCREEN_MEGA_OFFER + "-" + CTConstant.PROP_VALUE.MegaOffer.SCREEN_CHOOSE_YOUR_OFFER, "", "", "");
    }

    private void initUI() {
        mIvBack.setOnClickListener(this);
        mTvSkip.setOnClickListener(this);

        mNsvMegaOffers.setVisibility(View.GONE);

        initRecyclerView();
        extractBundleData();
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRvMegaOffers.setLayoutManager(layoutManager);
    }

    private void extractBundleData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (null != bundle) {
                mNavigationType = bundle.getInt(BundleConstants.KEY_NAVIGATE_FROM, AppConstants.NavigationFlow.NAVIGATE_FROM_HOME);

                if (mNavigationType == AppConstants.NavigationFlow.NAVIGATE_FROM_SPLASH) {
                    setVisibilities(View.GONE, View.VISIBLE);
                } else {
                    setVisibilities(View.VISIBLE, View.GONE);
                }
                MegaOffers megaOffers = bundle.getParcelable(MegaOffersConstants.BundleConstants.KEY_MEGA_OFFER_RESPONSE);
                if (megaOffers != null) {
                    mNsvMegaOffers.setVisibility(View.VISIBLE);
                    mTvNoMegaOffers.setVisibility(View.GONE);

                    mDashboardUrl = megaOffers.getDashboardUrl();
                    // Get mega offer detail
                    MegaOffersDetail megaOfferDetail = megaOffers.getMegaOffersDetail();
                    if (null != megaOfferDetail) {
                        // Render data on UI
                        renderDataOnUI(megaOfferDetail);
                    } else {
                        // Show error message
                        showErrorMessage(getString(R.string.mo_alert_no_offer_available));
                    }
                } else {
                    showErrorMessage(getString(R.string.mo_alert_no_offer_available));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "extractBundleData: " + e.getMessage());
        }
    }

    /**
     * This method is used to set visibilities of back icon & skip
     *
     * @param backIconVisibility
     * @param skipIconVisibility
     */
    private void setVisibilities(int backIconVisibility, int skipIconVisibility) {
        mIvBack.setVisibility(backIconVisibility);
        mTvSkip.setVisibility(skipIconVisibility);
    }

    /**
     * This method will be used to show message on screen when there is no internet connectivity or no offer detail present
     *
     * @param message
     */
    private void showErrorMessage(String message) {
        mNsvMegaOffers.setVisibility(View.GONE);
        mTvNoMegaOffers.setVisibility(View.VISIBLE);
        mTvHeaderTitle.setText(getString(R.string.mega_offer_header_title));
        mTvNoMegaOffers.setText(message);
    }

    /**
     * This method will render data on ui when data is received from server
     *
     * @param megaOfferDetail
     */
    private void renderDataOnUI(MegaOffersDetail megaOfferDetail) {
        try {
            String offerTitle = Html.fromHtml(megaOfferDetail.getNameHindi()).toString();
            if (!TextUtils.isEmpty(offerTitle)) {
                mTvHeaderTitle.setText(offerTitle);
            }
            String offerImage = megaOfferDetail.getOfferImage();
            if (!TextUtils.isEmpty(offerImage)) {
                mIvMegaOffer.setAnimation(getAnimation(R.id.iv_mega_offer));
                ImageUtil.loadImage(offerImage, mIvMegaOffer, R.drawable.water_mark_news_detail);
            }
            String chooseTitle = Html.fromHtml(megaOfferDetail.getChooseTitle()).toString();
            if (!TextUtils.isEmpty(chooseTitle)) {
                mTvChooseOfferTitle.setAnimation(getAnimation(R.id.tv_choose_offer));
                mTvChooseOfferTitle.setText(chooseTitle);
            }
            String chooseDescription = Html.fromHtml(megaOfferDetail.getChooseDescription()).toString();
            if (!TextUtils.isEmpty(chooseDescription)) {
                mTvChooseOfferDescription.setAnimation(getAnimation(R.id.tv_choose_offer_description));
                mTvChooseOfferDescription.setText(chooseDescription);
            }
            List<MegaOffersPlanDetail> megaOffersPlanDetailList = megaOfferDetail.getMegaOffersPlanDetailList();
            if (null != megaOffersPlanDetailList && !megaOffersPlanDetailList.isEmpty()) {
                // Render offer's plan data
                processAdapter(offerTitle, megaOffersPlanDetailList);
            } else {
                mRvMegaOffers.setVisibility(View.GONE);
                mTvNoMegaOffers.setVisibility(View.VISIBLE);
                mTvNoMegaOffers.setText(getString(R.string.mo_alert_no_plan_available));
            }
        } catch (Exception e) {
            Log.e(TAG, "renderDataOnUI: " + e.getMessage());
        }
    }

    /**
     * @param offerTitle
     * @param megaOffersPlanDetailList
     */
    private void processAdapter(String offerTitle, List<MegaOffersPlanDetail> megaOffersPlanDetailList) {
        try {
            MegaOffersListingAdapter listingAdapter = new MegaOffersListingAdapter(MegaOffersListingActivity.this, megaOffersPlanDetailList, new MegaOfferSelectionCallback() {
                @Override
                public void onOfferCardClicked(MegaOffersPlanDetail megaOffersPlanDetail) {
                    launchMegaOffersTermCondition(offerTitle, megaOffersPlanDetail);
                }
            });
            mRvMegaOffers.setAnimation(getAnimation(R.id.rv_mega_offers));
            mRvMegaOffers.setAdapter(listingAdapter);
        } catch (Exception e) {
            Log.e(TAG, "processAdapter: " + e.getMessage());
        }
    }

    /**
     * This method is used to create animations for UI resources
     *
     * @param resId
     * @return
     */
    private Animation getAnimation(int resId) {
        if (resId == R.id.iv_mega_offer) {
            return AnimationUtils.loadAnimation(this, R.anim.banner_down_bounce_animation);

        } else if (resId == R.id.tv_choose_offer || resId == R.id.tv_choose_offer_description) {
            return AnimationUtils.loadAnimation(this, R.anim.slide_left_animation);

        } else if (resId == R.id.iv_mega_offer) {
            return AnimationUtils.loadAnimation(this, R.anim.slide_right_animation);

        }
        return null;
    }

    /**
     * @param coreActivity
     * @param bundle
     */
    public static void startMegaOfferListingActivity(@NonNull Context coreActivity, @NonNull Bundle bundle) {
        Intent intent = new Intent(coreActivity, MegaOffersListingActivity.class);
        intent.putExtras(bundle);
        coreActivity.startActivity(intent);
    }

    /**
     * @param offerTitle
     */
    private void launchMegaOffersTermCondition(String offerTitle, MegaOffersPlanDetail megaOffersPlanDetail) {
        Bundle bundle = new Bundle();
        bundle.putInt(BundleConstants.KEY_NAVIGATE_FROM, mNavigationType);
        bundle.putString(MegaOffersConstants.BundleConstants.KEY_HEADER_TITLE, offerTitle);
        bundle.putString(MegaOffersConstants.BundleConstants.KEY_DASHBOARD_URL, mDashboardUrl);
        bundle.putParcelable(MegaOffersConstants.BundleConstants.KEY_MEGA_OFFERS_PLAN_DETAIL, megaOffersPlanDetail);
        MegaOffersTermConditionActivity.launchMegaOffersTnC(this, bundle);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.tv_skip) {
            navigaeToNextScreen();

        }

    }

    private void navigaeToNextScreen() {
        AppNavigatorHelper.getInstance().launchHomeActivity(this);
    }

    @Override
    public void onBackPressed() {
        if (mNavigationType != AppConstants.NavigationFlow.NAVIGATE_FROM_SPLASH) {
            super.onBackPressed();
        }
    }
}
