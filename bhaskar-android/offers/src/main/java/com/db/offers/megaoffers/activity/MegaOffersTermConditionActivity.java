package com.db.offers.megaoffers.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bhaskar.util.LoginController;
import com.db.offers.OfferController;
import com.db.offers.R;
import com.db.offers.R2;
import com.db.offers.common.AndWebView;
import com.db.offers.common.AppConstants;
import com.db.offers.common.BundleConstants;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.EasyDialogUtils;
import com.db.offers.common.Profile;
import com.db.offers.common.TransAviLoader;
import com.db.offers.common.Utils;
import com.db.offers.common.activity.CoreActivity;
import com.db.offers.common.cryptlib.CryptLib;
import com.db.offers.common.network.RetroApiListener;
import com.db.offers.common.utils.DialogOkInterface;
import com.db.offers.common.utils.DialogResponseInterface;
import com.db.offers.common.utils.NetworkStatus;
import com.db.offers.megaoffers.MegaOfferSharedPref;
import com.db.offers.megaoffers.callbacks.MegaOfferClaimedCallback;
import com.db.offers.megaoffers.constants.MegaOffersConstants;
import com.db.offers.megaoffers.dialog.MegaOfferClaimedDialog;
import com.db.offers.megaoffers.model.request.AcceptOfferPlanRequest;
import com.db.offers.megaoffers.model.response.AcceptMegaOffer;
import com.db.offers.megaoffers.model.response.MegaOffersPlanDetail;
import com.db.offers.megaoffers.providers.MegaOfferApiProvider;
import com.db.offers.megaoffers.providers.WebJavaScripInterfaceProvider;
import com.db.offers.megaoffers.providers.WebViewClientProvider;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.wang.avi.AVLoadingIndicatorView;

import java.security.NoSuchAlgorithmException;

import javax.crypto.NoSuchPaddingException;

import butterknife.BindView;

public class MegaOffersTermConditionActivity extends CoreActivity implements View.OnClickListener {

    @BindView(R2.id.mo_progress_bar_av)
    AVLoadingIndicatorView mWebViewProgressBar;
    @BindView(R2.id.iv_back)
    ImageView mIvBack;
    @BindView(R2.id.tv_header_title)
    TextView mTvHeaderTitle;
    @BindView(R2.id.wv_term_condition)
    AndWebView mWvTermCondition;
    @BindView(R2.id.lin_confirm)
    LinearLayout mLinConfirm;
    @BindView(R2.id.btn_confirm)
    Button mBtnConfirm;
    @BindView(R2.id.tv_no_mega_offer_term_condition_available)
    TextView mTvNoTermCondition;

    private static final String TAG = MegaOffersTermConditionActivity.class.getSimpleName();
    private MegaOffersPlanDetail mMegaOffersPlanDetail;
    private WebJavaScripInterfaceProvider mInterfaceProvider;
    private WebViewClientProvider mWebViewClientProvider;
    private String mWebViewUrl, mHeaderTitle, dashboardUrl;
    private int mNavigationType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mega_offers_terms_condition);

        initUI();
    }

    private void initUI() {
        mIvBack.setOnClickListener(this);
        mBtnConfirm.setOnClickListener(this);
        mBtnConfirm.setAnimation(AnimationUtils.loadAnimation(this, R.anim.banner_down_bounce_animation));

        extractBundleData();
        initWebView();
        addJavaScriptInterface();
        setWebViewClient();
        if (NetworkStatus.getInstance().isConnected(MegaOffersTermConditionActivity.this)) {
            loadUrlOnWebView();
        } else {
            noInternetConnectivity();
        }
    }

    private void noInternetConnectivity() {
        mWebViewProgressBar.setVisibility(View.GONE);
        mWvTermCondition.setVisibility(View.GONE);
        mLinConfirm.setVisibility(View.GONE);
        mTvNoTermCondition.setVisibility(View.VISIBLE);
        mTvNoTermCondition.setText(getString(R.string.alert_network_not_exist));
    }

    private void extractBundleData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (null != bundle) {
                mNavigationType = bundle.getInt(BundleConstants.KEY_NAVIGATE_FROM, AppConstants.NavigationFlow.NAVIGATE_FROM_HOME);
                mIvBack.setVisibility(View.VISIBLE);
                boolean isUserLoggedIn = LoginController.loginController().isUserLoggedIn();
                if (!isUserLoggedIn) {
                    mBtnConfirm.setText(getString(R.string.btn_login_to_confirm));
                } else {
                    mBtnConfirm.setText(getString(R.string.btn_confirm));
                }
                dashboardUrl = bundle.getString(MegaOffersConstants.BundleConstants.KEY_DASHBOARD_URL);
                mHeaderTitle = bundle.getString(MegaOffersConstants.BundleConstants.KEY_HEADER_TITLE);
                mMegaOffersPlanDetail = bundle.getParcelable(MegaOffersConstants.BundleConstants.KEY_MEGA_OFFERS_PLAN_DETAIL);
                setHeaderTitle();
                if (null != mMegaOffersPlanDetail) {
                    mWebViewUrl = mMegaOffersPlanDetail.getPlanTnCUrl();

                    // Track CleverTap screen view
                    trackCleverTapScreenView(true);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "TnC extractBundleData: " + e.getMessage());
        }
    }

    private void setHeaderTitle() {
        try {
            if (!TextUtils.isEmpty(mHeaderTitle)) {
                mTvHeaderTitle.setText(mHeaderTitle);
            } else {
                mTvHeaderTitle.setText(getString(R.string.mega_offer_header_title));
            }
        } catch (Exception e) {
            Log.e(TAG, "setHeaderTitle: " + e.getMessage());
        }
    }

    private void initWebView() {
        WebSettings webViewSetting = mWvTermCondition.getSettings();
        webViewSetting.setLoadsImagesAutomatically(true);
        webViewSetting.setDomStorageEnabled(true);
        mWvTermCondition.clearCache(true);
        mWvTermCondition.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        mWvTermCondition.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
    }

    private void addJavaScriptInterface() {
        mInterfaceProvider = new WebJavaScripInterfaceProvider(this);
        mInterfaceProvider.addJsInterface(mWvTermCondition);
    }

    private void setWebViewClient() {
        try {
            mWebViewClientProvider = new WebViewClientProvider(this, new WebViewClientProvider.WebPageListener() {
                @Override
                public void onWebPageFinished(WebView view, String url) {
                    try {
                        mWebViewProgressBar.smoothToHide();
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage() + "");
                    }
                }
            });

            mWvTermCondition.setWebViewClient(mWebViewClientProvider);
        } catch (Exception e) {
            Log.e(TAG, "setWebViewClient: " + e.getMessage());
        }
    }

    private void loadUrlOnWebView() {
        try {
            if (!TextUtils.isEmpty(mWebViewUrl) && null != mWebViewClientProvider.getWhiteListWebUrls() && !mWebViewClientProvider.getWhiteListWebUrls().isEmpty()) {
                if (Utils.isWebUrlDBInternal(mWebViewUrl, mWebViewClientProvider.getWhiteListWebUrls())) {
                    mWvTermCondition.loadUrl(mWebViewUrl, Utils.getWebHeader(dbApplication));
                } else {
                    mWvTermCondition.loadUrl(mWebViewUrl);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "loadUrlOnWebView: " + e.getMessage());
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();

        } else if (id == R.id.btn_confirm) {
            if (!LoginController.loginController().isUserLoggedIn()) {
                startActivityForResult(LoginController.loginController().getLoginIntent(MegaOffersTermConditionActivity.this, TrackingData.getDBId(MegaOffersTermConditionActivity.this), TrackingData.getDeviceId(MegaOffersTermConditionActivity.this)), AppConstants.RequestCode.RC_ACCEPT_MEGA_OFFER_TNC);
            } else {
                // Call acccept offer api
                callAcceptOfferApi();
            }
        }
    }

    /***
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.RequestCode.RC_ACCEPT_MEGA_OFFER_TNC) {
            if (LoginController.loginController().isUserLoggedIn()) {
                mBtnConfirm.setText(getString(R.string.btn_confirm));
                // Call acccept offer api
                callAcceptOfferApi();
            }
        }
    }

    /***
     *
     * @param profile
     * @param transAviLoader
     */
    private void acceptOfferApi(@NonNull Profile profile, @NonNull final TransAviLoader transAviLoader) {
        String offerAuthKey = "", offerAuthCode = "";
        try {
            CryptLib cryptLib = new CryptLib();
            offerAuthKey = cryptLib.encryptPlainTextWithRandomIV(TrackingData.getDeviceId(MegaOffersTermConditionActivity.this), CryptLib.ENCRYPT_KEY);
            String mobile = LoginController.loginController().getCurrentUser().mobile;
            offerAuthCode = cryptLib.encryptPlainTextWithRandomIV((TextUtils.isEmpty(mobile) ? "" : mobile), CryptLib.ENCRYPT_KEY);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        AcceptOfferPlanRequest acceptOfferPlanRequest = new AcceptOfferPlanRequest(mMegaOffersPlanDetail.getOfferId(),
                mMegaOffersPlanDetail.getId(), offerAuthKey, offerAuthCode);

        MegaOfferApiProvider.getInstance(dbApplication).acceptMegaOfferPlan(acceptOfferPlanRequest,
                new RetroApiListener<AcceptMegaOffer>() {
                    @Override
                    public void onApiSuccess(@NonNull AcceptMegaOffer acceptMegaOffer) {
                        // Save offerId & planId in local preference
                        transAviLoader.dismiss();
                        try {
                            if (acceptMegaOffer.code == 305 && !TextUtils.isEmpty(acceptMegaOffer.message)) {
                                // Already Taken by another mobile number
                                doLogoutForMegaOffer(acceptMegaOffer.message);
                            } else if (acceptMegaOffer.code == 306 && !TextUtils.isEmpty(acceptMegaOffer.message)) {
                                // Already Taken by another device
                                EasyDialogUtils.showInfoDialog(MegaOffersTermConditionActivity.this, getString(R.string.alert_title_error),
                                        acceptMegaOffer.message);
                            } else if (acceptMegaOffer.code == 307 && !TextUtils.isEmpty(acceptMegaOffer.message)) {
                                MegaOfferSharedPref.getInstance(dbApplication).saveUserAcceptedOffer(mMegaOffersPlanDetail.getOfferId());
                                // Already Accepted
                                EasyDialogUtils.showInfoDialog(MegaOffersTermConditionActivity.this, getString(R.string.alert_title_success),
                                        acceptMegaOffer.message, new DialogOkInterface() {
                                            @Override
                                            public void doOnOkBtnClick(@NonNull Activity activity) {
                                                launchDashboard();
                                                finish();
                                            }
                                        });
                            } else if (acceptMegaOffer.getAcceptMegaOfferDetail().isLogoutRequired()) {
                                EasyDialogUtils.showInfoDialog(MegaOffersTermConditionActivity.this, dbApplication.getString(R.string.alert_title_failed),
                                        acceptMegaOffer.getMessage() + "");
                                LoginController.loginController().logout(dbApplication);
                                mBtnConfirm.setText(getString(R.string.btn_login_to_confirm));
                            } else {
                                MegaOfferSharedPref.getInstance(dbApplication).saveUserAcceptedOffer(acceptOfferPlanRequest.getOfferId());
                                // Show accepted offer dialog
                                showOfferAcceptedDialog(acceptMegaOffer);
                                // Track clever tap screen view
                                trackCleverTapScreenView(false);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage() + "");
                            EasyDialogUtils.showInfoDialog(MegaOffersTermConditionActivity.this, getString(R.string.alert_title_failed),
                                    getString(R.string.alert_api_failed));
                        }
                    }

                    /***
                     *
                     * @param message
                     */
                    private void doLogoutForMegaOffer(@NonNull String message) {

                        EasyDialogUtils.showConfirmationDialog(MegaOffersTermConditionActivity.this, getString(R.string.alert_title_error),
                                message, false, R.mipmap.app_icon, "Re-Login", "Cancel", new DialogResponseInterface() {
                                    @Override
                                    public void doOnPositiveBtnClick(Activity activity) {
                                        LoginController.loginController().logout(dbApplication);
                                        startActivityForResult(LoginController.loginController().getLoginIntent(MegaOffersTermConditionActivity.this, TrackingData.getDBId(MegaOffersTermConditionActivity.this), TrackingData.getDeviceId(MegaOffersTermConditionActivity.this)), AppConstants.RequestCode.RC_ACCEPT_MEGA_OFFER_TNC);
//                                        AppNavigatorHelper.getInstance().launchLoginActivity(MegaOffersTermConditionActivity.this, new Bundle(), AppConstants.RequestCode.RC_ACCEPT_MEGA_OFFER_TNC);
                                    }

                                    @Override
                                    public void doOnNegativeBtnClick(Activity activity) {

                                    }
                                });

                    }

                    @Override
                    public void onApiFailed(@NonNull String message) {
                        transAviLoader.dismiss();
                        EasyDialogUtils.showInfoDialog(MegaOffersTermConditionActivity.this, getString(R.string.alert_title_failed),
                                message);
                    }
                });
    }

    private void callAcceptOfferApi() {
        if (CommonUtils.isNetworkAvailable(dbApplication)) {
            final TransAviLoader transAviLoader = new TransAviLoader(MegaOffersTermConditionActivity.this);
            try {
                Profile profile = OfferController.getInstance().getDbSharedPref().getUserProfile();
                transAviLoader.show();
                acceptOfferApi(profile, transAviLoader);
            } catch (Exception e) {
                transAviLoader.dismiss();
                Log.e(TAG, "callAcceptOfferApi: " + e.getMessage());
                EasyDialogUtils.showInfoDialog(MegaOffersTermConditionActivity.this, getString(R.string.alert_title_failed), getString(R.string.alert_request_unable_to_process));
            }
        } else {
            EasyDialogUtils.showInfoDialog(MegaOffersTermConditionActivity.this, getString(R.string.alert_title_failed), getString(R.string.alert_network_not_exist));
        }
    }

    /**
     * This method is used to show mega offer accepted success dialog
     *
     * @param acceptMegaOffer
     */
    private void showOfferAcceptedDialog(AcceptMegaOffer acceptMegaOffer) {
        new MegaOfferClaimedDialog(MegaOffersTermConditionActivity.this, acceptMegaOffer, new MegaOfferClaimedCallback() {
            @Override
            public void onNavigateToNextScreen() {
                launchDashboard();
            }
        }).show();
    }

    /**
     * This method is used to track clevertap screen view for details & congrats popup
     *
     * @param isOfferDetails
     */
    private void trackCleverTapScreenView(boolean isOfferDetails) {
        int planId = mMegaOffersPlanDetail.getId();
        String planName = mMegaOffersPlanDetail.getPlanName();
        if (planId > 0 && !TextUtils.isEmpty(planName)) {
            StringBuilder builder = new StringBuilder(planId + ": " + planName + " - ");
            if (isOfferDetails) {
                builder.append(CTConstant.PROP_VALUE.MegaOffer.SCREEN_OFFER_DETAILS);
            } else {
                builder.append(CTConstant.PROP_VALUE.MegaOffer.SCREEN_CONGRATS_POPUP);
            }
//            CTTracker.cleverTapTrackPageView(this, builder.toString(), CTConstant.PROP_VALUE.MegaOffer.SCREEN_MEGA_OFFER);
            Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.MegaOffer.SCREEN_MEGA_OFFER + "-" + builder.toString(), "", "", "");

        }
    }

    @Override
    public void onBackPressed() {
        if (mWvTermCondition.canGoBack()) {
            mWvTermCondition.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        if (mWvTermCondition != null && null != mInterfaceProvider) {
            mInterfaceProvider.removeJsInterface(mWvTermCondition);
            mWvTermCondition.destroy();
        }
        super.onDestroy();
    }

    /**
     * @param coreActivity
     * @param bundle
     */
    public static void launchMegaOffersTnC(CoreActivity coreActivity, Bundle bundle) {
        Intent intent = new Intent(coreActivity, MegaOffersTermConditionActivity.class);
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        coreActivity.startActivity(intent);
    }

    private void launchDashboard() {
        Bundle bundle = new Bundle();
        bundle.putInt(BundleConstants.KEY_NAVIGATE_FROM, mNavigationType);
        bundle.putString(MegaOffersConstants.BundleConstants.KEY_HEADER_TITLE, mHeaderTitle);
        bundle.putString(MegaOffersConstants.BundleConstants.KEY_DASHBOARD_URL, dashboardUrl);
        if (mNavigationType == AppConstants.NavigationFlow.NAVIGATE_FROM_SPLASH) {
            MegaOffersDashboardActivity.launchMegaOffersDashboardFromSplash(this, bundle);
        } else {
            MegaOffersDashboardActivity.launchMegaOffersDashboard(this, bundle);
        }
        finish();
    }


}
