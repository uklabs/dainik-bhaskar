package com.db.offers.megaoffers.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.db.offers.R;
import com.db.offers.common.ImageUtil;
import com.db.offers.common.activity.CoreActivity;
import com.db.offers.megaoffers.callbacks.MegaOfferSelectionCallback;
import com.db.offers.megaoffers.model.response.MegaOffersPlanDetail;

import java.util.List;

public class MegaOffersListingAdapter extends RecyclerView.Adapter<MegaOffersListingAdapter.MegaOffersViewHolder> {

    private CoreActivity mCoreActivity;
    private List<MegaOffersPlanDetail> mMegaOffersPlanDetailList;
    private MegaOfferSelectionCallback mSelectionCallback;

    public MegaOffersListingAdapter(CoreActivity coreActivity, List<MegaOffersPlanDetail> megaOffersPlanDetailList, MegaOfferSelectionCallback selectionCallback) {
        mCoreActivity = coreActivity;
        mMegaOffersPlanDetailList = megaOffersPlanDetailList;
        mSelectionCallback = selectionCallback;
    }

    @NonNull
    @Override
    public MegaOffersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCoreActivity).inflate(R.layout.activity_mega_offers_listing_item, parent, false);
        return new MegaOffersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MegaOffersViewHolder holder, int position) {
        MegaOffersPlanDetail megaOffersPlanDetail = mMegaOffersPlanDetailList.get(position);
        try {
            ImageUtil.loadImage(megaOffersPlanDetail.getPlanImage(), holder.ivOfferLogo, R.drawable.water_mark_news_detail);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.cvMegaOfferItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectionCallback.onOfferCardClicked(megaOffersPlanDetail);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMegaOffersPlanDetailList != null ? mMegaOffersPlanDetailList.size() : 0;
    }

    class MegaOffersViewHolder extends RecyclerView.ViewHolder {

        private CardView cvMegaOfferItem;
        private ImageView ivOfferLogo;

        MegaOffersViewHolder(@NonNull View itemView) {
            super(itemView);
            cvMegaOfferItem = itemView.findViewById(R.id.cv_mega_offer);
            ivOfferLogo = itemView.findViewById(R.id.iv_offer_logo);
        }
    }
}
