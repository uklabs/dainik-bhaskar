package com.db.offers.megaoffers.adapter;//package com.bhaskar.darwin.ui.megaoffers.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.widget.BaseExpandableListAdapter;
//import android.widget.ExpandableListView;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.bhaskar.darwin.R;
//import com.bhaskar.darwin.ui.base.CoreActivity;
//
//import java.util.HashMap;
//import java.util.List;
//
//public class MegaOffersTermConditionExpandableAdapter extends BaseExpandableListAdapter {
//
//    private CoreActivity mCoreActivity;
//    private List<String> mListGroup;
//    private HashMap<String, List<String>> mListChild;
//    private LayoutInflater mInflater;
//    private Animation mAnimation;
//    private ExpandableListView mElvTermCondition;
//    private boolean mIsGroupExpanded;
//
//    public MegaOffersTermConditionExpandableAdapter(CoreActivity coreActivity, List<String> listDataGroup,
//                                                    HashMap<String, List<String>> listChildData, ExpandableListView elvTermCondition) {
//        mCoreActivity = coreActivity;
//        mListGroup = listDataGroup;
//        mListChild = listChildData;
//        mElvTermCondition = elvTermCondition;
//        mInflater = (LayoutInflater) mCoreActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////        mAnimation = AnimationUtils.loadAnimation(mCoreActivity, R.anim.rotation_animation);
//    }
//
//    @Override
//    public Object getChild(int groupPosition, int childPosititon) {
//        return mListChild.get(mListGroup.get(groupPosition)).get(childPosititon);
//    }
//
//    @Override
//    public long getChildId(int groupPosition, int childPosition) {
//        return childPosition;
//    }
//
//    @Override
//    public View getChildView(int groupPosition, final int childPosition,
//                             boolean isLastChild, View convertView, ViewGroup parent) {
//
//        final String childText = (String) getChild(groupPosition, childPosition);
//
//        if (convertView == null) {
//            convertView = mInflater.inflate(R.layout.activity_mega_offers_term_condition_child, parent, false);
//        }
//
//        TextView textViewChild = convertView.findViewById(R.id.tv_term_condition_child);
//        textViewChild.setText(childText);
//        return convertView;
//    }
//
//    @Override
//    public int getChildrenCount(int groupPosition) {
//        return mListChild.get(mListGroup.get(groupPosition)).size();
//    }
//
//    @Override
//    public Object getGroup(int groupPosition) {
//        return this.mListGroup.get(groupPosition);
//    }
//
//    @Override
//    public int getGroupCount() {
//        return this.mListGroup.size();
//    }
//
//    @Override
//    public long getGroupId(int groupPosition) {
//        return groupPosition;
//    }
//
//    @Override
//    public View getGroupView(int groupPosition, boolean isExpanded,
//                             View convertView, ViewGroup parent) {
//        String headerTitle = (String) getGroup(groupPosition);
//        if (convertView == null) {
//            convertView = mInflater.inflate(R.layout.activity_mega_offers_term_condition_parent, parent, false);
//        }
//        LinearLayout llParent = convertView.findViewById(R.id.ll_parent);
//        TextView textViewGroup = convertView.findViewById(R.id.tv_term_condition_parent);
//        ImageView ivArrow = convertView.findViewById(R.id.iv_arrow);
//        ivArrow.setTag(groupPosition);
//        textViewGroup.setText(headerTitle);
//        llParent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int tag = (int) ivArrow.getTag();
//                if (mIsGroupExpanded) {
//                    mIsGroupExpanded = false;
//                    mElvTermCondition.collapseGroup(groupPosition);
//                    if (tag == groupPosition) {
//                        ivArrow.setImageResource(R.drawable.ic_mega_offer_arrow_down_black_24dp);
//                    }
//                } else {
//                    mIsGroupExpanded = true;
//                    mElvTermCondition.expandGroup(groupPosition);
//                    if (tag == groupPosition) {
//                        ivArrow.setImageResource(R.drawable.ic_mega_offer_arrow_up_black_24dp);
//                    }
//                }
//            }
//        });
//        return convertView;
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return false;
//    }
//
//    @Override
//    public boolean isChildSelectable(int groupPosition, int childPosition) {
//        return true;
//    }
//}
