package com.db.offers.megaoffers.callbacks;

public interface MegaOfferClaimedCallback {
    void onNavigateToNextScreen();
}
