package com.db.offers.megaoffers.callbacks;


import com.db.offers.megaoffers.model.response.MegaOffersPlanDetail;

public interface MegaOfferSelectionCallback {

    void onOfferCardClicked(MegaOffersPlanDetail megaOffersPlanDetail);
}
