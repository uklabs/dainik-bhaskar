package com.db.offers.megaoffers.callbacks;

public interface PointsSyncCallback {

    void onSyncSuccess();
    void onPointsNotAvailable();
    void onSyncFailed(String message);
}
