package com.db.offers.megaoffers.constants;

import com.db.offers.common.UrlUtil;

public interface MegaOffersAPIConstants {
    String KEY_OFFER_AUTH = "offerAuthKey";
    String KEY_OFFER_CODE = "offerAuthCode";

    interface ApiEndPoint {
        String GET_MEGA_OFFERS_LIST = "v1/get-offer";
        String ACCEPT_MEGA_OFFER = "v1/accept-offer";
        String SYNC_MEGA_OFFER_POINTS = "v1/save-points?_format=json";
        String GET_MEGA_OFFER_DATE_MAX_POINT = UrlUtil.BASE_URL_MEGAOFFER + "v1/";
    }
}
