package com.db.offers.megaoffers.constants;

public interface MegaOffersConstants {

    String CURRENT_DATE_FORMAT = "yyyy-MM-dd";
    int MAX_POINTS_COUNT = 20;

    interface BundleConstants {
        String KEY_MEGA_OFFER_RESPONSE = "mega_offer_response";
        String KEY_MEGA_OFFERS_PLAN_DETAIL = "mega_offer_plan_detail";
        String KEY_HEADER_TITLE = "header_title";
        String KEY_DASHBOARD_URL = "key_dashboard_url";
    }
}
