package com.db.offers.megaoffers.dialog;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.db.offers.R;
import com.db.offers.common.BaseDialog;
import com.db.offers.common.ImageUtil;
import com.db.offers.common.activity.CoreActivity;
import com.db.offers.megaoffers.callbacks.MegaOfferClaimedCallback;
import com.db.offers.megaoffers.model.response.AcceptMegaOffer;


public class MegaOfferClaimedDialog extends BaseDialog implements View.OnClickListener {

    private static final String TAG = MegaOfferClaimedDialog.class.getSimpleName();
    private CoreActivity mCoreActivity;
    private AcceptMegaOffer mAcceptMegaOffer;
    private MegaOfferClaimedCallback mCallback;
    private ImageView mIvBanner, mIvGotOfferMore;
    private TextView mTvCongratsHeader, mTvCongratsMessage;
    private Button mBtnOk;

    public MegaOfferClaimedDialog(@NonNull CoreActivity coreActivity, AcceptMegaOffer acceptMegaOffer, MegaOfferClaimedCallback callback) {
        super(coreActivity, R.style.MegaOfferClaimedDialogTheme);
        mCoreActivity = coreActivity;
        mAcceptMegaOffer = acceptMegaOffer;
        mCallback = callback;
        setCancelable(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_mega_offer_claimed_successfully);

        initUI();
    }

    private void initUI() {
        mIvBanner = findViewById(R.id.iv_banner);
        mIvGotOfferMore = findViewById(R.id.iv_got_offer_more);
        mTvCongratsHeader = findViewById(R.id.tv_congrats_header);
        mTvCongratsMessage = findViewById(R.id.tv_congrats_message);
        mBtnOk = findViewById(R.id.btn_ok);

        mBtnOk.setOnClickListener(this);

        // Render data
        renderDataOnUI();
    }

    private void renderDataOnUI() {
        try {
            AcceptMegaOffer.AcceptMegaOfferDetail acceptMegaOfferDetail = mAcceptMegaOffer.getAcceptMegaOfferDetail();
            if (null != acceptMegaOfferDetail) {
                mIvBanner.setAnimation(getAnimation(R.id.iv_banner));
                String congratsHeader = acceptMegaOfferDetail.getCongratsHeader();
                if (!TextUtils.isEmpty(congratsHeader)) {
                    mTvCongratsHeader.setText(congratsHeader);
                } else {
                    mTvCongratsHeader.setText(mCoreActivity.getString(R.string.mo_congrats_header));
                }
                String congratsMessage = acceptMegaOfferDetail.getCongratsMessage();
                if (!TextUtils.isEmpty(congratsMessage)) {
                    mTvCongratsMessage.setText(congratsMessage);
                } else {
                    mTvCongratsMessage.setText(mCoreActivity.getString(R.string.mo_congrats_message));
                }
                mTvCongratsHeader.setAnimation(getAnimation(R.id.tv_congrats_header));
                mTvCongratsMessage.setAnimation(getAnimation(R.id.tv_congrats_message));
                ImageUtil.loadImage(acceptMegaOfferDetail.getMoreOfferImgUrl(), mIvGotOfferMore, R.drawable.water_mark_news_detail);
                mIvGotOfferMore.startAnimation(getAnimation(R.id.iv_got_offer_more));
                mBtnOk.startAnimation(getAnimation(R.id.btn_ok));
            }
        } catch (Exception e) {
            Log.e(TAG, "renderDataOnUI: " + e.getMessage());
        }
    }

    /**
     * This method is used to create animations for UI resources
     *
     * @param resId
     * @return
     */
    private Animation getAnimation(int resId) {
        if(resId == R.id.iv_banner || resId == R.id.btn_ok){
            return AnimationUtils.loadAnimation(mCoreActivity, R.anim.banner_down_bounce_animation);
        }else if(resId == R.id.tv_congrats_header || resId == R.id.tv_congrats_message){
            return AnimationUtils.loadAnimation(mCoreActivity, R.anim.slide_left_animation);

        }else if(resId == R.id.iv_got_offer_more){
            return AnimationUtils.loadAnimation(mCoreActivity, R.anim.slide_right_animation);

        }

        return null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_ok) {
            dismiss();
            navigateToMegaOfferDashboard();
        }
    }

    private void navigateToMegaOfferDashboard() {
        mCallback.onNavigateToNextScreen();
    }
}
