package com.db.offers.megaoffers.model;

import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class MegaOfferCTModel {
    ////{"MegaOfferPoints":"37","MegaOfferVisits":"9","MegaOfferMonth":"01May_31May"}
    @SerializedName(CTConstant.PROP_NAME.MEGA_OFFER_POINTS)
    @Expose
    private String megaOfferPoints;
    @SerializedName(CTConstant.PROP_NAME.MEGA_OFFER_VISITS)
    @Expose
    private String megaOfferVisits;
    @SerializedName(CTConstant.PROP_NAME.MEGA_OFFER_MONTH)
    @Expose
    private String megaOfferMonth;

    public String getMegaOfferPoints() {
        return megaOfferPoints;
    }

    public String getMegaOfferVisits() {
        return megaOfferVisits;
    }

    public String getMegaOfferMonth() {
        return megaOfferMonth;
    }

    @Override
    public String toString() {
        return "MegaOfferCTModel{" +
                "megaOfferPoints='" + megaOfferPoints + '\'' +
                ", megaOfferVisits='" + megaOfferVisits + '\'' +
                ", megaOfferMonth='" + megaOfferMonth + '\'' +
                '}';
    }
}
