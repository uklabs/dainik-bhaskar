package com.db.offers.megaoffers.model.request;

import androidx.annotation.NonNull;

import com.db.offers.common.GsonProguardMarker;
import com.db.offers.megaoffers.constants.MegaOffersAPIConstants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class AcceptOfferPlanRequest implements GsonProguardMarker {

    @SerializedName("offer_id")
    @Expose
    private int offerId;
    @SerializedName("plan_id")
    @Expose
    private int planId;
    @SerializedName(MegaOffersAPIConstants.KEY_OFFER_AUTH)
    @Expose
    private String offerAuthKey;

    @SerializedName(MegaOffersAPIConstants.KEY_OFFER_CODE)
    @Expose
    private String offerAuthCode;

    /***
     *
     * @param offerId
     * @param planId
     * @param offerAuthKey
     * @param offerAuthCode
     */
    public AcceptOfferPlanRequest(int offerId, int planId, @NonNull String offerAuthKey, @NonNull String offerAuthCode) {
        this.offerId = offerId;
        this.planId = planId;
        this.offerAuthKey = offerAuthKey;
        this.offerAuthCode = offerAuthCode;
    }

    public int getOfferId() {
        return offerId;
    }
}
