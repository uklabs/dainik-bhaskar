package com.db.offers.megaoffers.model.request;

import androidx.annotation.NonNull;

import com.db.offers.common.GsonProguardMarker;
import com.db.offers.megaoffers.constants.MegaOffersAPIConstants;
import com.db.offers.megaoffers.sqldb.tables.PointSyncTable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public final class PointsRequest implements GsonProguardMarker {

    @SerializedName("offer_id")
    @Expose
    private int offerId;

    @SerializedName("points_list")
    @Expose
    private ArrayList<PointSyncTable> pointsList;

    @SerializedName(MegaOffersAPIConstants.KEY_OFFER_AUTH)
    @Expose
    private String offerAuthKey;

    /****
     *
     * @param offerId
     * @param offerAuthKey
     * @param response
     */
    public PointsRequest(int offerId, @NonNull String offerAuthKey, @NonNull ArrayList<PointSyncTable> response) {
        this.offerId = offerId;
        this.pointsList = response;
        this.offerAuthKey = offerAuthKey;
    }

    @Override
    public String toString() {
        return "PointsRequest{" +
                "offerId=" + offerId +
                ", pointsList=" + pointsList +
                ", offerAuthKey='" + offerAuthKey + '\'' +
                '}';
    }
}
