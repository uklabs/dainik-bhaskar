package com.db.offers.megaoffers.model.response;

import com.db.offers.common.BaseResponse;
import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AcceptMegaOffer extends BaseResponse implements GsonProguardMarker {

    @SerializedName("data")
    @Expose
    private AcceptMegaOfferDetail acceptMegaOfferDetail;

    public AcceptMegaOfferDetail getAcceptMegaOfferDetail() {
        return acceptMegaOfferDetail;
    }

    public class AcceptMegaOfferDetail {

        @SerializedName("isLogoutRequired")
        private boolean isLogoutRequired = false;

        @SerializedName("head")
        @Expose
        private String congratsHeader;

        @SerializedName("message")
        @Expose
        private String congratsMessage;

        @SerializedName("image")
        @Expose
        private String moreOfferImgUrl;

        public boolean isLogoutRequired() {
            return isLogoutRequired;
        }

        public String getCongratsHeader() {
            return congratsHeader;
        }

        public String getCongratsMessage() {
            return congratsMessage;
        }

        public String getMoreOfferImgUrl() {
            return moreOfferImgUrl;
        }
    }
}
