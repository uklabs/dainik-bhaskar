package com.db.offers.megaoffers.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.db.offers.common.GsonProguardMarker;
import com.db.offers.common.ParcelableInterface;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class AcceptedOffer implements ParcelableInterface, GsonProguardMarker {

    @SerializedName("offer_id")
    @Expose
    public int offerId;

    @SerializedName("offer_name")
    @Expose
    public String offerName;

    public int getOfferId() {
        return offerId;
    }

    public String getOfferName() {
        return offerName;
    }

    private AcceptedOffer(Parcel parcel) {
        readFromParcel(parcel);
    }

    @Override
    public void readFromParcel(Parcel source) {
        offerId = source.readInt();
        offerName = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(offerId);
        dest.writeString(offerName);
    }

    public static final Parcelable.Creator<AcceptedOffer> CREATOR = new Parcelable.Creator<AcceptedOffer>() {
        @Override
        public AcceptedOffer createFromParcel(Parcel source) {
            return new AcceptedOffer(source);
        }

        @Override
        public AcceptedOffer[] newArray(int size) {
            return new AcceptedOffer[size];
        }
    };
}
