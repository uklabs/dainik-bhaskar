package com.db.offers.megaoffers.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.db.offers.common.GsonProguardMarker;
import com.db.offers.common.ParcelableInterface;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MegaOffers implements ParcelableInterface, GsonProguardMarker {
    @SerializedName("is_offer_available")
    @Expose
    private boolean isOfferAvailable;
    @SerializedName("is_offer_accepted")
    @Expose
    private boolean isOfferAccepted;
    @SerializedName("dashboard_url")
    @Expose
    private String dashboardUrl;
    @SerializedName("offer")
    @Expose
    private MegaOffersDetail megaOffersDetail;

    @SerializedName("accepted_offer")
    @Expose
    private AcceptedOffer acceptedOffer;

    public boolean isOfferAvailable() {
        return isOfferAvailable;
    }

    public boolean isOfferAccepted() {
        return isOfferAccepted;
    }

    public String getDashboardUrl() {
        return dashboardUrl;
    }

    public MegaOffersDetail getMegaOffersDetail() {
        return megaOffersDetail;
    }

    public AcceptedOffer getAcceptedOffer() {
        return acceptedOffer;
    }

    MegaOffers(Parcel source) {
        readFromParcel(source);
    }

    @Override
    public void readFromParcel(Parcel source) {
        isOfferAvailable = source.readByte() != 0;
        isOfferAccepted = source.readByte() != 0;
        dashboardUrl = source.readString();
        megaOffersDetail = source.readParcelable(MegaOffersDetail.class.getClassLoader());
        acceptedOffer = source.readParcelable(AcceptedOffer.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isOfferAvailable ? 1 : 0));
        dest.writeByte((byte) (isOfferAccepted ? 1 : 0));
        dest.writeString(dashboardUrl);
        dest.writeParcelable(megaOffersDetail, flags);
        dest.writeParcelable(acceptedOffer, flags);
    }

    public static final Parcelable.Creator<MegaOffers> CREATOR = new Parcelable.Creator<MegaOffers>() {
        @Override
        public MegaOffers createFromParcel(Parcel source) {
            return new MegaOffers(source);
        }

        @Override
        public MegaOffers[] newArray(int size) {
            return new MegaOffers[size];
        }
    };

    @Override
    public String toString() {
        return "MegaOffers{" +
                "isOfferAvailable=" + isOfferAvailable +
                ", isOfferAccepted=" + isOfferAccepted +
                ", dashboardUrl=" + dashboardUrl +
                ", megaOffersDetail=" + megaOffersDetail +
                '}';
    }
}
