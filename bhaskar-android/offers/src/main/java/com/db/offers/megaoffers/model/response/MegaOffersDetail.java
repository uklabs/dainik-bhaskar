package com.db.offers.megaoffers.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.db.offers.common.GsonProguardMarker;
import com.db.offers.common.ParcelableInterface;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MegaOffersDetail implements ParcelableInterface, GsonProguardMarker {

    @SerializedName("name_en")
    @Expose
    private String nameEnglish;
    @SerializedName("name_hi")
    @Expose
    private String nameHindi;
    @SerializedName("choose_title")
    @Expose
    private String chooseTitle;
    @SerializedName("choose_desc")
    @Expose
    private String chooseDescription;
    @SerializedName("offer_image")
    @Expose
    private String offerImage;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("plans")
    @Expose
    private List<MegaOffersPlanDetail> megaOffersPlanDetailList = new ArrayList<>();

    public String getNameEnglish() {
        return nameEnglish;
    }

    public String getNameHindi() {
        return nameHindi;
    }

    public String getChooseTitle() {
        return chooseTitle;
    }

    public String getChooseDescription() {
        return chooseDescription;
    }

    public String getOfferImage() {
        return offerImage;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<MegaOffersPlanDetail> getMegaOffersPlanDetailList() {
        return megaOffersPlanDetailList;
    }

    MegaOffersDetail(Parcel parcel) {
        readFromParcel(parcel);
    }

    @Override
    public void readFromParcel(Parcel source) {
        nameEnglish = source.readString();
        nameHindi = source.readString();
        chooseTitle = source.readString();
        chooseDescription = source.readString();
        offerImage = source.readString();
        startDate = source.readString();
        endDate = source.readString();
        source.readTypedList(megaOffersPlanDetailList, MegaOffersPlanDetail.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameEnglish);
        dest.writeString(nameHindi);
        dest.writeString(chooseTitle);
        dest.writeString(chooseDescription);
        dest.writeString(offerImage);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeTypedList(megaOffersPlanDetailList);
    }

    public static final Parcelable.Creator<MegaOffersDetail> CREATOR = new Parcelable.Creator<MegaOffersDetail>() {
        @Override
        public MegaOffersDetail createFromParcel(Parcel source) {
            return new MegaOffersDetail(source);
        }

        @Override
        public MegaOffersDetail[] newArray(int size) {
            return new MegaOffersDetail[size];
        }
    };

    @Override
    public String toString() {
        return "MegaOffersDetail{" +
                "nameEnglish='" + nameEnglish + '\'' +
                ", nameHindi='" + nameHindi + '\'' +
                ", chooseTitle='" + chooseTitle + '\'' +
                ", chooseDescription='" + chooseDescription + '\'' +
                ", offerImage='" + offerImage + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", megaOffersPlanDetailList=" + megaOffersPlanDetailList +
                '}';
    }
}
