package com.db.offers.megaoffers.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.db.offers.common.BaseResponse;
import com.db.offers.common.GsonProguardMarker;
import com.db.offers.common.ParcelableInterface;
import com.google.gson.annotations.SerializedName;

public class MegaOffersListingModel extends BaseResponse implements ParcelableInterface, GsonProguardMarker {

    @SerializedName("data")
    private MegaOffers megaOffers;

    public MegaOffers getMegaOffers() {
        return megaOffers;
    }

    private MegaOffersListingModel(Parcel parcel) {
        readFromParcel(parcel);
    }

    @Override
    public void readFromParcel(Parcel source) {
        megaOffers = source.readParcelable(MegaOffers.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(megaOffers, flags);
    }

    public static final Parcelable.Creator<MegaOffersListingModel> CREATOR = new Parcelable.Creator<MegaOffersListingModel>() {
        @Override
        public MegaOffersListingModel createFromParcel(Parcel source) {
            return new MegaOffersListingModel(source);
        }

        @Override
        public MegaOffersListingModel[] newArray(int size) {
            return new MegaOffersListingModel[size];
        }
    };
}
