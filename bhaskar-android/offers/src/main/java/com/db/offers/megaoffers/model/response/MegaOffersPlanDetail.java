package com.db.offers.megaoffers.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.db.offers.common.GsonProguardMarker;
import com.db.offers.common.ParcelableInterface;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MegaOffersPlanDetail implements ParcelableInterface, GsonProguardMarker {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("offer_id")
    @Expose
    private int offerId;
    @SerializedName("plan_name")
    @Expose
    private String planName;
    @SerializedName("plan_image")
    @Expose
    private String planImage;
    @SerializedName("plan_tnc_url")
    @Expose
    private String planTnCUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOfferId() {
        return offerId;
    }

    public String getPlanName() {
        return planName;
    }

    public String getPlanImage() {
        return planImage;
    }

    public String getPlanTnCUrl() {
        return planTnCUrl;
    }

    MegaOffersPlanDetail(Parcel parcel) {
        readFromParcel(parcel);
    }

    @Override
    public void readFromParcel(Parcel source) {
        id = source.readInt();
        offerId = source.readInt();
        planName = source.readString();
        planImage = source.readString();
        planTnCUrl = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(offerId);
        dest.writeString(planName);
        dest.writeString(planImage);
        dest.writeString(planTnCUrl);
    }

    public static final Parcelable.Creator<MegaOffersPlanDetail> CREATOR = new Parcelable.Creator<MegaOffersPlanDetail>() {
        @Override
        public MegaOffersPlanDetail createFromParcel(Parcel source) {
            return new MegaOffersPlanDetail(source);
        }

        @Override
        public MegaOffersPlanDetail[] newArray(int size) {
            return new MegaOffersPlanDetail[size];
        }
    };

    @Override
    public String toString() {
        return "MegaOffersPlanDetail{" +
                "id=" + id +
                ", offerId=" + offerId +
                ", planName='" + planName + '\'' +
                ", planImage='" + planImage + '\'' +
                ", planTnCUrl='" + planTnCUrl + '\'' +
                '}';
    }
}
