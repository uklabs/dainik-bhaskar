package com.db.offers.megaoffers.models;

import com.db.offers.common.BaseResponse;
import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class MegaOfferDatePointResponse extends BaseResponse implements GsonProguardMarker {

    @SerializedName("data")
    @Expose
    private MegaOfferDateMaxPoint megaOfferDateMaxPoint;

    public MegaOfferDateMaxPoint getMegaOfferDateAndMaxPoint() {
        return megaOfferDateMaxPoint;
    }

    public final class MegaOfferDateMaxPoint implements GsonProguardMarker {
        @SerializedName("max_points")
        @Expose
        private int todayMaxPoints;

        @SerializedName("current_date")
        @Expose
        private String currentDate;

        public int getTodayMaxPoints() {
            return todayMaxPoints;
        }

        public String getCurrentDate() {
            return currentDate;
        }

        @Override
        public String toString() {
            return "MegaOfferDateMaxPoint{" +
                    "todayMaxPoints=" + todayMaxPoints +
                    ", currentDate='" + currentDate + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "MegaOfferDatePointResponse{" +
                "megaOfferDateMaxPoint=" + megaOfferDateMaxPoint +
                '}';
    }
}
