package com.db.offers.megaoffers.providers;


import android.content.Context;

public abstract class BaseProvider {

    protected Context context;

    protected BaseProvider(Context context) {
        this.context = context;
    }
}
