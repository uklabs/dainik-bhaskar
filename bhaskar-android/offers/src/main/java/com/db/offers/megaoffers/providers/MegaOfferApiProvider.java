package com.db.offers.megaoffers.providers;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.bhaskar.util.LoginController;
import com.db.offers.R;
import com.db.offers.common.Profile;
import com.db.offers.common.SyncResponseModel;
import com.db.offers.common.network.ApiClient;
import com.db.offers.common.network.RetroApiListener;
import com.db.offers.common.network.RetroCallbackProvider;
import com.db.offers.megaoffers.MegaOfferSharedPref;
import com.db.offers.megaoffers.MegaOffersAPIService;
import com.db.offers.megaoffers.callbacks.PointsSyncCallback;
import com.db.offers.megaoffers.constants.MegaOffersAPIConstants;
import com.db.offers.megaoffers.model.request.AcceptOfferPlanRequest;
import com.db.offers.megaoffers.model.request.PointsRequest;
import com.db.offers.megaoffers.model.response.AcceptMegaOffer;
import com.db.offers.megaoffers.model.response.MegaOffers;
import com.db.offers.megaoffers.model.response.MegaOffersListingModel;
import com.db.offers.megaoffers.models.MegaOfferDatePointResponse;
import com.db.offers.megaoffers.sqldb.tables.PointSyncTable;

import java.util.ArrayList;

import retrofit2.Call;

public class MegaOfferApiProvider extends BaseProvider {

    private static final String TAG = MegaOfferApiProvider.class.getSimpleName();

    private static MegaOfferApiProvider megaOfferApiProvider;
    private Context context;

    /***
     *
     * @param context
     * @return
     */
    public static MegaOfferApiProvider getInstance(@NonNull Context context) {
        if (megaOfferApiProvider == null) {
            megaOfferApiProvider = new MegaOfferApiProvider(context);
        }
        return megaOfferApiProvider;
    }

    /***
     *
     * @param context
     */
    private MegaOfferApiProvider(@NonNull Context context) {
        super(context);
        this.context = context;
    }


    /***
     *
     * @param dbApplication
     */
    public static void getAndSaveMegaOfferCurrentDateAndMaxPointsCount(@NonNull Context dbApplication) {
        MegaOfferSharedPref megaOfferSharedPref = MegaOfferSharedPref.getInstance(dbApplication);

        int offerId = megaOfferSharedPref.getUserAcceptedOfferId();

        if (offerId == 0) {
            return;
        }

        Call<MegaOfferDatePointResponse> call = ApiClient.getDigitalServerClient(dbApplication).create(MegaOffersAPIService.class).
                getMegaOfferDateMaxPointsCount(MegaOffersAPIConstants.ApiEndPoint.GET_MEGA_OFFER_DATE_MAX_POINT, offerId);
        call.enqueue(new RetroCallbackProvider<MegaOfferDatePointResponse>() {
            @Override
            public void onSuccess(Call<MegaOfferDatePointResponse> call, @NonNull MegaOfferDatePointResponse response) {
                try {
                    MegaOfferSharedPref megaOfferSharedPref = MegaOfferSharedPref.getInstance(dbApplication);
                    megaOfferSharedPref.saveTodayMegaOfferDetail(response.getMegaOfferDateAndMaxPoint());
                    MegaOfferPointsUtils.insertAppVisited(dbApplication);
                    Log.d(TAG, "getAndSaveMegaOfferCurrentDateAndMaxPointsCount(): onSuccess()" + response);
                } catch (Exception e) {
                    Log.e(TAG, "getAndSaveMegaOfferCurrentDateAndMaxPointsCount(): onSuccess()" + e.getMessage());
                }
            }

            @Override
            public void onFailed(Call<MegaOfferDatePointResponse> call, Throwable t) {
                Log.e(TAG, "getAndSaveMegaOfferCurrentDateAndMaxPointsCount(): onFailed()" + t.getMessage());
            }
        });

    }

    /***
     *
     * @param retroApiListener
     */
    public void getMegaOffersList(@NonNull RetroApiListener<MegaOffersListingModel> retroApiListener) {

        MegaOffersAPIService megaOffersAPIService = ApiClient.getMegaOfferClient(context).create(MegaOffersAPIService.class);
        String mobileNo = (LoginController.loginController().isUserLoggedIn()) ? LoginController.loginController().getCurrentUser().mobile : "";
        Call<MegaOffersListingModel> call = megaOffersAPIService.getMegaOffersList(mobileNo);

        call.enqueue(new RetroCallbackProvider<MegaOffersListingModel>() {
            @Override
            public void onSuccess(Call<MegaOffersListingModel> call, @NonNull MegaOffersListingModel megaOffersListingModel) {
                try {
                    Log.d(TAG, "getMegaOffersList(): " + megaOffersListingModel);
                    if (null != megaOffersListingModel.getMegaOffers()) {

                        MegaOffers megaOffers = megaOffersListingModel.getMegaOffers();
                        MegaOfferSharedPref megaOfferSharedPref = MegaOfferSharedPref.getInstance(context);
                        if (megaOffers.isOfferAccepted() && megaOffers.getAcceptedOffer() != null) {
                            megaOfferSharedPref.saveUserAcceptedOffer(megaOffers.getAcceptedOffer().offerId);//, megaOffers.getAcceptedOffer().planId);
                        } else {
                            megaOfferSharedPref.removeUserAcceptedOffer();
                        }
                        retroApiListener.onApiSuccess(megaOffersListingModel);
                    } else {
                        retroApiListener.onApiSuccess(megaOffersListingModel);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage() + "");
                    retroApiListener.onApiFailed(context.getString(R.string.alert_request_unable_to_process));
                }
            }

            @Override
            public void onFailed(Call<MegaOffersListingModel> call, Throwable t) {
                retroApiListener.onApiFailed(context.getString(R.string.alert_api_failed));
            }
        });
    }


    /***
     *
     * @param retroApiListener
     */
    public void acceptMegaOfferPlan(@NonNull AcceptOfferPlanRequest offerPlanRequest, @NonNull RetroApiListener<AcceptMegaOffer> retroApiListener) {

        MegaOffersAPIService megaOffersAPIService = ApiClient.getMegaOfferClient(context).create(MegaOffersAPIService.class);

        Call<AcceptMegaOffer> call = megaOffersAPIService.acceptMegaOffer(offerPlanRequest);

        call.enqueue(new RetroCallbackProvider<AcceptMegaOffer>() {
            @Override
            public void onSuccess(Call<AcceptMegaOffer> call, @NonNull AcceptMegaOffer acceptMegaOffer) {
                try {
                    Log.d(TAG, "acceptMegaOfferPlan(): " + acceptMegaOffer);
                    if (null != acceptMegaOffer) {
                        retroApiListener.onApiSuccess(acceptMegaOffer);
                    } else {
                        retroApiListener.onApiFailed(context.getString(R.string.mo_alert_no_offer_available));
                    }
                    MegaOfferApiProvider.getAndSaveMegaOfferCurrentDateAndMaxPointsCount(context);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage() + "");
                    retroApiListener.onApiFailed(context.getString(R.string.alert_request_unable_to_process));
                }
            }

            @Override
            public void onFailed(Call<AcceptMegaOffer> call, Throwable t) {
                retroApiListener.onApiFailed(context.getString(R.string.alert_api_failed));
            }
        });

    }

    /***
     *
     * @param pointsList
     * @param pointsSyncCallback
     */
    void syncPointsToServer(@NonNull ArrayList<PointSyncTable> pointsList,
                            @NonNull PointsSyncCallback pointsSyncCallback) {

        syncPointsToServerWithApi(null, pointsList, pointsSyncCallback);
    }

    /***
     *
     * @param profile
     * @param pointsList
     * @param pointsSyncCallback
     */
    private void syncPointsToServerWithApi(@NonNull Profile profile, @NonNull ArrayList<PointSyncTable> pointsList,
                                           @NonNull PointsSyncCallback pointsSyncCallback) {
        MegaOfferSharedPref megaOfferSharedPref = MegaOfferSharedPref.getInstance(context);
        String accessToken = (LoginController.loginController().isUserLoggedIn()) ? LoginController.loginController().getCurrentUser().uuID : "";
        PointsRequest pointsRequest = new PointsRequest(megaOfferSharedPref.getUserAcceptedOfferId(), (TextUtils.isEmpty(accessToken) ? "" : accessToken), pointsList);

        Call<SyncResponseModel> responseCall = ApiClient.getMegaOfferClient(context).create(MegaOffersAPIService.class).
                syncMegaOfferPoints(pointsRequest);

        responseCall.enqueue(new RetroCallbackProvider<SyncResponseModel>() {
            @Override
            public void onSuccess(Call<SyncResponseModel> call, @NonNull SyncResponseModel response) {
                pointsSyncCallback.onSyncSuccess();
                MegaOfferPointsUtils.deleteAllSyncedPoints(context, megaOfferSharedPref.getMegaOfferTodayDate());
                Log.d(TAG, "syncPointsToServer() onSuccess");
                /*SyncResponseModel.SyncResponse syncResponse = response.getSyncResponse();
                CTTracker.setCleverTapProfileForMegaOffer(context, syncResponse);*/
            }

            @Override
            public void onFailed(Call<SyncResponseModel> call, Throwable t) {
                Log.e(TAG, "syncPointsToServer() onFailed" + t.getMessage());
                pointsSyncCallback.onSyncFailed(t.getMessage() + "");
            }
        });
    }
}
