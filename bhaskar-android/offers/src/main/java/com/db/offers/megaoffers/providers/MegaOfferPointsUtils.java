package com.db.offers.megaoffers.providers;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.db.offers.common.AppConstants;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.sqldb.SqlQueryListener;
import com.db.offers.megaoffers.MegaOfferSharedPref;
import com.db.offers.megaoffers.callbacks.PointsSyncCallback;
import com.db.offers.megaoffers.sqldb.MegaOfferSqlManager;
import com.db.offers.megaoffers.sqldb.tables.PointSyncTable;

import java.util.ArrayList;

public final class MegaOfferPointsUtils {

    private static final String TAG = MegaOfferPointsUtils.class.getSimpleName();

    /***
     *
     * @param context
     * @param pointsAllocationType
     */
    public static void insertOrUpdatePoints(@NonNull Context context, @NonNull AppConstants.PointsAllocationType pointsAllocationType) {
        Log.d(TAG, "insertOrUpdatePoints()");
        MegaOfferSharedPref megaOfferSharedPref = MegaOfferSharedPref.getInstance(context);

        if (!megaOfferSharedPref.isUserAcceptedAnyOffer() || TextUtils.isEmpty(megaOfferSharedPref.getMegaOfferTodayDate())) {
            return;
        }

        MegaOfferSqlManager megaOfferSqlManager = MegaOfferSqlManager.getInstance(context);
        megaOfferSqlManager.insertPointInSyncModel(megaOfferSharedPref.getMegaOfferTodayDate(),
                megaOfferSharedPref.getMaxPointCount(), pointsAllocationType, new SqlQueryListener<Boolean>() {
                    @Override
                    public void onQuerySuccess(@NonNull Boolean response) {
                        Log.d(TAG, "insertOrUpdatePoints onQuerySuccess(): " + response);
                    }

                    @Override
                    public void onQueryFailed(@NonNull String message) {
                        Log.e(TAG, "insertOrUpdatePoints onQueryFailed(): " + message);
                    }
                });
    }

    /**
     * @param context
     */
    public static void insertAppVisited(@NonNull Context context) {
        MegaOfferSharedPref megaOfferSharedPref = MegaOfferSharedPref.getInstance(context);
        if (!megaOfferSharedPref.isUserAcceptedAnyOffer() || TextUtils.isEmpty(megaOfferSharedPref.getMegaOfferTodayDate())) {
            return;
        }

        if (CommonUtils.isNetworkAvailable(context)) {
            Log.d(TAG, "insertAppVisited()");
            MegaOfferSqlManager megaOfferSqlManager = MegaOfferSqlManager.getInstance(context);

            megaOfferSqlManager.insertAppVisitedStatus(megaOfferSharedPref.getMegaOfferTodayDate(), new SqlQueryListener<String>() {
                @Override
                public void onQuerySuccess(@NonNull String currentDate) {
                    Log.d(TAG, "insertAppVisited(): onQuerySuccess: " + currentDate);
                }

                @Override
                public void onQueryFailed(@NonNull String message) {
                    Log.e(TAG, "insertAppVisited(): onQueryFailed: " + message);
                }
            });
        }
    }

    /***
     *
     * @param context
     * @param pointsSyncCallback
     */
    public static void getAllPointsAndSyncToServer(@NonNull Context context, @NonNull PointsSyncCallback pointsSyncCallback) {
        Log.d(TAG, "getAllPointsAndSyncToServer()");
        //MegaOfferSharedPref megaOfferSharedPref = MegaOfferSharedPref.getInstance(dbApplication);
        MegaOfferSqlManager megaOfferSqlManager = MegaOfferSqlManager.getInstance(context);

        megaOfferSqlManager.getPointsFromSyncTable(new SqlQueryListener<ArrayList<PointSyncTable>>() {
            @Override
            public void onQuerySuccess(@NonNull ArrayList<PointSyncTable> response) {
                try {
                    if (response != null && !response.isEmpty()) {

                        MegaOfferApiProvider.getInstance(context).syncPointsToServer(response, pointsSyncCallback);
                    } else {
                        pointsSyncCallback.onPointsNotAvailable();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "getAllPointsAndSyncToServer() " + e.getMessage());
                    pointsSyncCallback.onSyncFailed(e.getMessage() + "");
                }
            }

            @Override
            public void onQueryFailed(@NonNull String message) {
                pointsSyncCallback.onSyncFailed(message);
            }
        });
    }


    /***
     *
     * @param context
     * @param megaOfferTodayDate
     */
    static void deleteAllSyncedPoints(@NonNull Context context, @NonNull String megaOfferTodayDate) {
        try {
            MegaOfferSqlManager megaOfferSqlManager = MegaOfferSqlManager.getInstance(context);
            megaOfferSqlManager.deleteRowsNotHavingCurrentDate(megaOfferTodayDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
