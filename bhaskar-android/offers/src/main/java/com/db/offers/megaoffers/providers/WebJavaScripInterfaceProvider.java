package com.db.offers.megaoffers.providers;

import android.annotation.SuppressLint;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.db.offers.common.activity.CoreActivity;

public final class WebJavaScripInterfaceProvider extends BaseProvider {

    private static final String WEBVIEW_JS_INTERFACE_NAME = "DVBAppWebInterface";

    /**
     * @param coreActivity
     */
    public WebJavaScripInterfaceProvider(@NonNull CoreActivity coreActivity) {
        super(coreActivity);
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void addJsInterface(@NonNull WebView webView) {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.addJavascriptInterface(this, WEBVIEW_JS_INTERFACE_NAME);
    }

    /***
     *
     * @param webView
     */
    public void removeJsInterface(@NonNull WebView webView) {
        webView.removeJavascriptInterface(WEBVIEW_JS_INTERFACE_NAME);
    }

    @JavascriptInterface
    public void alert(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }


}

//{"linkType":"videos","articleIdId":1486696,"catId":951,"url":"https://www.bhaskar.com/videos/bollywood/second-trailer-of-sonchiriya-the-rebels-of-sonchiriya-released-01486696.html"}


