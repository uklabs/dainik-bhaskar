package com.db.offers.megaoffers.providers;

import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import com.db.offers.common.CommonUtils;
import com.db.offers.common.Utils;
import com.db.offers.common.activity.CoreActivity;

import java.util.List;

public class WebViewClientProvider extends WebViewClient {

    private final static String TAG = WebViewClientProvider.class.getSimpleName();
    private CoreActivity coreActivity;
    private WebPageListener webPageListener;
    private List<String> whiteListWebUrls;

    /**
     * @param coreActivity
     * @param webPageListener
     */
    public WebViewClientProvider(CoreActivity coreActivity, WebPageListener webPageListener) {
        this.coreActivity = coreActivity;
        this.webPageListener = webPageListener;
        whiteListWebUrls = coreActivity.getWhitListWebUrls();
    }

    public List<String> getWhiteListWebUrls() {
        return whiteListWebUrls;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        try {
            return isHandleUrl(request.getUrl().toString()) || super.shouldOverrideUrlLoading(view, request);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
        return false;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {

        try {
            return isHandleUrl(url) || super.shouldOverrideUrlLoading(view, url);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
        return false;
    }

    private boolean isHandleUrl(String url) {

        if (Utils.isWebUrlDBInternal(url, whiteListWebUrls)) {
            return false;
        } else {
            CommonUtils.openUrlOnGoogleChromeTab(coreActivity, url);
            return true;
        }
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (webPageListener != null)
            webPageListener.onWebPageFinished(view, url);
    }

    public interface WebPageListener {
        void onWebPageFinished(WebView view, String url);
    }
}
