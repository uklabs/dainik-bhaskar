package com.db.offers.megaoffers.sqldb;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.db.offers.common.sqldb.SQLHelper;
import com.db.offers.megaoffers.sqldb.dao.PointSyncTableDao;


/**
 * MegaOfferSqlAdapter is a child of SQLHelper having all responsibilities of
 * instantiating , opening and closing database connection and keep an
 * SQLDatabase Object
 */
public final class MegaOfferSqlAdapter extends SQLHelper {

    private final String TAG = MegaOfferSqlAdapter.class.getSimpleName();

    /**
     * ***
     * Init Database with params
     *
     * @param context
     * @param dbName
     * @param dbVersion
     */
    public MegaOfferSqlAdapter(Context context, final String dbName, final int dbVersion) {
        super(context, dbName, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqlDB) {
        createInitialTables(sqlDB);
        super.onCreate(sqlDB);
    }

    /**
     * Create individual table needed in database
     *
     * @param sqlDB
     */
    private void createInitialTables(SQLiteDatabase sqlDB) {
        try {
            PointSyncTableDao pointSyncTableDao = new PointSyncTableDao();
            pointSyncTableDao.createSqlTable(sqlDB);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    /**
     * To close Database
     */
    public void close() {
        super.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        super.onUpgrade(arg0, arg1, arg2);
    }

    @Override
    public SQLiteDatabase getSqliteDatabase() {
        if (!super.getSQLiteDatabase().isOpen()) {
            open();
        }
        return super.getSQLiteDatabase();
    }
}

