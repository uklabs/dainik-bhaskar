package com.db.offers.megaoffers.sqldb;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.db.offers.OfferController;
import com.db.offers.common.AppConstants;
import com.db.offers.common.sqldb.SqlQueryListener;
import com.db.offers.megaoffers.sqldb.dao.PointSyncTableDao;
import com.db.offers.megaoffers.sqldb.tables.PointSyncTable;

import java.util.ArrayList;

public class MegaOfferSqlManager {

    private Context context;

    private final String TAG = MegaOfferSqlManager.class.getSimpleName();

    private static MegaOfferSqlManager megaOfferSqlManager;

    /***
     *
     * @param context
     * @return
     */
    public static MegaOfferSqlManager getInstance(@NonNull Context context) {
        if (megaOfferSqlManager == null) {
            megaOfferSqlManager = new MegaOfferSqlManager(context);
        }
        return megaOfferSqlManager;
    }

    /**
     * @param context
     */
    private MegaOfferSqlManager(@NonNull Context context) {
        this.context = context;
    }


    /***
     *
     * @param currentDate
     * @param maxPointsCount
     * @param pointsAllocationType
     * @param sqlQueryListener
     */
    public synchronized void insertPointInSyncModel(@NonNull String currentDate, int maxPointsCount,
                                                    @NonNull AppConstants.PointsAllocationType pointsAllocationType, @Nullable SqlQueryListener<Boolean> sqlQueryListener) {
        MegaOfferSqlAdapter megaOfferSqlAdapter = OfferController.getInstance().getMegaOfferAdapter();
        if (megaOfferSqlAdapter != null) {
            Runnable runnable = () -> {
                try {
                    PointSyncTableDao pointSyncTableDao = new PointSyncTableDao();
                    pointSyncTableDao.insertOrUpdatePoint(megaOfferSqlAdapter.getSqliteDatabase(), currentDate, maxPointsCount, pointsAllocationType);

                    if (sqlQueryListener != null) {
                        sqlQueryListener.onQuerySuccess(true);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "insertPointInSyncModel() " + e.getMessage());
                    if (sqlQueryListener != null) {
                        sqlQueryListener.onQueryFailed(e.getMessage() + "");
                    }
                }
            };
            new Thread(runnable).start();
        }
    }

    /***
     *
     * @param currentDate
     * @param sqlQueryListener
     */
    public synchronized void insertAppVisitedStatus(@NonNull String currentDate, @Nullable SqlQueryListener<String> sqlQueryListener) {
        MegaOfferSqlAdapter megaOfferSqlAdapter = OfferController.getInstance().getMegaOfferAdapter();
        if (megaOfferSqlAdapter != null) {
            Runnable runnable = () -> {
                try {
                    PointSyncTableDao pointSyncTableDao = new PointSyncTableDao();
                    long rowId = pointSyncTableDao.insertAppVisited(megaOfferSqlAdapter.getSqliteDatabase(), currentDate);

                    if (sqlQueryListener != null) {
                        if (rowId != -1) {
                            sqlQueryListener.onQuerySuccess(currentDate);
                        } else {
                            sqlQueryListener.onQueryFailed(rowId + "");
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "insertPointInSyncModel() " + e.getMessage());
                    if (sqlQueryListener != null) {
                        sqlQueryListener.onQueryFailed(e.getMessage() + "");
                    }
                }
            };
            new Thread(runnable).start();
        }
    }


    /***
     *
     * @param sqlQueryListener
     */
    public synchronized void getPointsFromSyncTable(@NonNull SqlQueryListener<ArrayList<PointSyncTable>> sqlQueryListener) {
        MegaOfferSqlAdapter megaOfferSqlAdapter = OfferController.getInstance().getMegaOfferAdapter();
        if (megaOfferSqlAdapter != null) {
            Runnable runnable = () -> {
                try {
                    PointSyncTableDao pointSyncTableDao = new PointSyncTableDao();
                    ArrayList<PointSyncTable> pointsSyncTableArrayList = pointSyncTableDao.getAllPointsFromTable
                            (megaOfferSqlAdapter.getSqliteDatabase());

                    sqlQueryListener.onQuerySuccess(pointsSyncTableArrayList);
                } catch (Exception e) {
                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
                }
            };
            new Thread(runnable).start();
        }
    }

    /***
     *
     * @param currentDate
     */
    public void deleteRowsNotHavingCurrentDate(String currentDate) throws Exception {
        MegaOfferSqlAdapter megaOfferSqlAdapter = OfferController.getInstance().getMegaOfferAdapter();
        PointSyncTableDao pointSyncTableDao = new PointSyncTableDao();
        pointSyncTableDao.deleteRowsNotHavingCurrentDate(megaOfferSqlAdapter.getSqliteDatabase(), currentDate);
    }

    /***
     *
     * @param maxPointCount
     */
    public void deleteAllSyncedPointsRow(int maxPointCount) {
        MegaOfferSqlAdapter megaOfferSqlAdapter = OfferController.getInstance().getMegaOfferAdapter();
        PointSyncTableDao pointSyncTableDao = new PointSyncTableDao();
        pointSyncTableDao.deleteAllSyncedPointsRow(megaOfferSqlAdapter.getSqliteDatabase(), maxPointCount);
    }
}
