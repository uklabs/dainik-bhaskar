package com.db.offers.megaoffers.sqldb;
//
//import android.content.Context;
//import android.os.Handler;
//import android.util.Log;
//
//
//import com.bhaskar.darwin.DBApplication;
//import com.bhaskar.darwin.ui.treasurehunt.sqldb.dao.ArticleVisitTableDao;
//import com.bhaskar.darwin.ui.treasurehunt.sqldb.dao.CoinsSyncTableDao;
//import com.bhaskar.darwin.ui.treasurehunt.sqldb.tables.ArticleVisitTable;
//import com.bhaskar.darwin.ui.treasurehunt.sqldb.tables.CoinsSyncTable;
//import com.bhaskar.sqldb.SqlQueryListener;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public final class THGSqlDBManager {
//
//    private Context context;
//
//    private THGSqlLiteAdapter thSqlLiteAdapter;
//
//    private final String TAG = THGSqlDBManager.class.getSimpleName();
//
//    private boolean isServiceContext;
//
//    private Handler mainHandler;
//
//   /* public THGSqlDBManager(@NonNull Context context) {
//        this.context = context;
//        this.thSqlLiteAdapter = ((DBApplication) context.getApplicationContext()).getThSqlLiteAdapter();
//    }*/
//
//    /***
//     *
//     * @param context
//     * @param isServiceContext
//     */
//    public THGSqlDBManager(@NonNull Context context, boolean isServiceContext) {
//        this.context = context;
//        mainHandler = new Handler(context.getMainLooper());
//        this.isServiceContext = isServiceContext;
//        this.thSqlLiteAdapter = ((DBApplication) context.getApplicationContext()).getThSqlLiteAdapter();
//    }
//
//    public synchronized boolean isCoinsTableEmpty() {
//        boolean isCoinsTableEmpty = true;
//        try {
//            CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//            isCoinsTableEmpty = coinsSyncTableDao.isTableEmpty(thSqlLiteAdapter.getSqliteDatabase());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return isCoinsTableEmpty;
//    }
//
//    public synchronized void emptyArticleVisitTable() {
//        if (thSqlLiteAdapter != null) {
//            Runnable runnable = () -> {
//                try {
//                    ArticleVisitTableDao articleVisitTableDao = new ArticleVisitTableDao();
//                    articleVisitTableDao.deleteTableAllRows(thSqlLiteAdapter.getSqliteDatabase());
//                    Log.i(TAG, "emptyArticleVisitTable");
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /**
//     * @param articleVisitTable
//     * @param sqlQueryListener  set null when result not required
//     */
//    public synchronized void saveArticleInVisitTable(ArticleVisitTable articleVisitTable, SqlQueryListener<ArticleVisitTable> sqlQueryListener) {
//        if (null != context && thSqlLiteAdapter != null) {
//
//            Runnable runnable = () -> {
//                try {
//                    ArticleVisitTableDao articleVisitTableDao = new ArticleVisitTableDao();
//                    articleVisitTableDao.insert(thSqlLiteAdapter.getSqliteDatabase(), articleVisitTable, ArticleVisitTable.class);
//                    if (sqlQueryListener != null) {
//
//                        if (isServiceContext) {
//                            sqlQueryListener.onQuerySuccess(articleVisitTable);
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQuerySuccess(articleVisitTable);
//                                }
//                            });
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /***
//     *
//     * @param articleId
//     * @param sqlQueryListener
//     */
//    public synchronized void isArticleIdExistInVisitTable(String articleId, SqlQueryListener<Boolean> sqlQueryListener) {
//
//        if (null != context && thSqlLiteAdapter != null) {
//
//            Runnable runnable = () -> {
//                try {
//                    ArticleVisitTableDao articleVisitTableDao = new ArticleVisitTableDao();
//                    Boolean isExist = articleVisitTableDao.isArticleIdExistInVisitTable(thSqlLiteAdapter.getSqliteDatabase(), articleId);
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQuerySuccess(isExist);
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQuerySuccess(isExist);
//                                }
//                            });
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /***
//     *
//     * @param articleId
//     * @param sqlQueryListener
//     */
//    public synchronized void isArticleIdExistInSyncTable(String articleId, SqlQueryListener<Boolean> sqlQueryListener) {
//
//        if (null != context && thSqlLiteAdapter != null) {
//
//            Runnable runnable = () -> {
//                try {
//                    CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//                    Boolean isExist = coinsSyncTableDao.isArticleIdExistInSyncTable(thSqlLiteAdapter.getSqliteDatabase(), articleId);
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQuerySuccess(isExist);
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQuerySuccess(isExist);
//                                }
//                            });
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /***
//     *
//     * @param sqlQueryListener
//     */
//    public synchronized void getAllUnsyncedCoins(SqlQueryListener<ArrayList<CoinsSyncTable>> sqlQueryListener) {
//        if (null != context && thSqlLiteAdapter != null) {
//            Runnable runnable = () -> {
//                try {
//                    CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//                    ArrayList<CoinsSyncTable> coinsSyncTableArrayList = coinsSyncTableDao.getAllUnsyncedCoins(
//                            thSqlLiteAdapter.getSqliteDatabase());
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQuerySuccess(coinsSyncTableArrayList);
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQuerySuccess(coinsSyncTableArrayList);
//                                }
//                            });
//                        }
//
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /***
//     *
//     * @param selectedDate
//     * @param sqlQueryListener
//     */
//    public synchronized void getOptedCoinsForSelectedDate(String selectedDate, SqlQueryListener<Integer[]> sqlQueryListener) {
//        if (null != context && thSqlLiteAdapter != null) {
//            Runnable runnable = () -> {
//                try {
//                    CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//                    Integer[] optedCoinsCount = coinsSyncTableDao.getOptedCoinsCount
//                            (thSqlLiteAdapter.getSqliteDatabase(), selectedDate);
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQuerySuccess(optedCoinsCount);
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQuerySuccess(optedCoinsCount);
//                                }
//                            });
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /***
//     *
//     * @param selectedDate
//     */
//    public synchronized void deleteUserCoinsForCurrentDateFromOptedTable(String selectedDate) {
//        if (thSqlLiteAdapter != null) {
//
//            Runnable runnable = () -> {
//                try {
//                    CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//                    int noOfRowsDeleted = coinsSyncTableDao.deleteCoinsForSelectedDate(thSqlLiteAdapter.getSqliteDatabase(), selectedDate);
//                    Log.i(TAG, "noOfRowsDeleted:" + noOfRowsDeleted);
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /***
//     *
//     * @param selectedDate
//     * @param sqlQueryListener
//     */
//    public synchronized void getCoinsForSelectedDate(String selectedDate, SqlQueryListener<ArrayList<CoinsSyncTable>> sqlQueryListener) {
//        if (null != context && thSqlLiteAdapter != null) {
//            Runnable runnable = () -> {
//                try {
//                    CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//                    ArrayList<CoinsSyncTable> coinsSyncTableArrayList = coinsSyncTableDao.getCoinsForSelectedDate
//                            (thSqlLiteAdapter.getSqliteDatabase(), selectedDate);
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQuerySuccess(coinsSyncTableArrayList);
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQuerySuccess(coinsSyncTableArrayList);
//                                }
//                            });
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            new Handler(context.getMainLooper()).post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//
//    /***
//     *
//     * @param sqlQueryListener
//     */
//    public synchronized void getCoinsFromSyncTable(SqlQueryListener<ArrayList<CoinsSyncTable>> sqlQueryListener) {
//
//        if (null != context && thSqlLiteAdapter != null) {
//            Runnable runnable = () -> {
//                try {
//                    CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//                    ArrayList<CoinsSyncTable> coinsSyncTableArrayList = coinsSyncTableDao.getAllCoinsFromTable
//                            (thSqlLiteAdapter.getSqliteDatabase());
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQuerySuccess(coinsSyncTableArrayList);
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQuerySuccess(coinsSyncTableArrayList);
//                                }
//                            });
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /****
//     *
//     * @param currentDate
//     * @param sqlQueryListener
//     */
//    public synchronized void getOptedCoinCountForCurrentDate(String currentDate, SqlQueryListener<Long> sqlQueryListener) {
//        if (null != context && thSqlLiteAdapter != null) {
//            Runnable runnable = () -> {
//                try {
//                    CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//                    long optedCoinsCount = coinsSyncTableDao.getOptedCoinCountForCurrentDate(thSqlLiteAdapter.getSqliteDatabase(), currentDate);
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQuerySuccess(optedCoinsCount);
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQuerySuccess(optedCoinsCount);
//                                }
//                            });
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /**
//     * @param coinsSyncTables
//     * @param sqlQueryListener
//     */
//    public synchronized void updateCoinsServerStatus(ArrayList<CoinsSyncTable> coinsSyncTables, SqlQueryListener<ArrayList<CoinsSyncTable>> sqlQueryListener) {
//
//        if (null != context && thSqlLiteAdapter != null) {
//            Runnable runnable = () -> {
//                try {
//                    CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//                    coinsSyncTableDao.updateCoinsStatus(thSqlLiteAdapter.getSqliteDatabase(), coinsSyncTables);
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQuerySuccess(coinsSyncTables);
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQuerySuccess(coinsSyncTables);
//                                }
//                            });
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /***
//     *
//     * @param coinsSyncTable
//     * @param sqlQueryListener
//     */
//    public synchronized void insertCoinInSyncModel(CoinsSyncTable coinsSyncTable, SqlQueryListener<CoinsSyncTable> sqlQueryListener) {
//
//        if (null != context && thSqlLiteAdapter != null) {
//            Runnable runnable = () -> {
//                try {
//                    CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//                    long rowId = coinsSyncTableDao.insertCoin(thSqlLiteAdapter.getSqliteDatabase(), coinsSyncTable);
//
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            if (rowId != -1)
//                                sqlQueryListener.onQuerySuccess(coinsSyncTable);
//                            else {
//                                sqlQueryListener.onQueryFailed(rowId + "");
//                            }
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    if (rowId != -1)
//                                        sqlQueryListener.onQuerySuccess(coinsSyncTable);
//                                    else {
//                                        sqlQueryListener.onQueryFailed(rowId + "");
//                                    }
//                                }
//                            });
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    /***
//     *
//     * @param coinsSyncTableList
//     * @param sqlQueryListener
//     */
//    public synchronized void insertCoinsInSyncModel(List<CoinsSyncTable> coinsSyncTableList, SqlQueryListener<List<CoinsSyncTable>> sqlQueryListener) {
//
//        if (null != context && thSqlLiteAdapter != null) {
//            Runnable runnable = () -> {
//                try {
//                    CoinsSyncTableDao coinsSyncTableDao = new CoinsSyncTableDao();
//                    coinsSyncTableDao.insertCoins(thSqlLiteAdapter.getSqliteDatabase(), coinsSyncTableList);
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQuerySuccess(coinsSyncTableList);
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQuerySuccess(coinsSyncTableList);
//                                }
//                            });
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.e("getAllCoinsOptedEntries", e.getMessage() + "");
//                    if (sqlQueryListener != null) {
//                        if (isServiceContext) {
//                            sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                        } else {
//                            mainHandler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sqlQueryListener.onQueryFailed(e.getMessage() + "");
//                                }
//                            });
//                        }
//                    }
//                }
//            };
//            new Thread(runnable).start();
//        }
//    }
//
//    public void clearResources() {
//        mainHandler = null;
//    }
//}
//
//
//
