package com.db.offers.tambola;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.db.offers.R;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.DBToast;
import com.db.offers.common.EasyDialogUtils;
import com.db.offers.common.TransAviLoader;
import com.db.offers.megaoffers.providers.BaseProvider;
import com.db.offers.tambola.activity.BingoRegistrationActivity;
import com.db.offers.tambola.activity.TambolaDashboardActivity;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.model.QuestionAttemptStatusModel;
import com.db.offers.tambola.model.UserExistsModel;
import com.db.offers.tambola.network.TambolaAPIClient;
import com.db.offers.tambola.network.TambolaAPIInterface;
import com.db.offers.tambola.utils.TambolaSharedPref;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambolaLauncherProvider extends BaseProvider {

    private static final String TAG = TambolaLauncherProvider.class.getSimpleName();
    private Context context;
    private Activity coreActivity;
    private final long mInterval = 10 * 1000;
    private int mRetryCount = 1;
    private Timer mUserExistTimer;
    private TransAviLoader transAviLoader;
    protected TambolaSharedPref tambolaSharedPref;


    public TambolaLauncherProvider(Context context) {
        super(context);
        this.context = context;
    }

    public void initProvider(Activity coreActivity) {
        this.context = coreActivity;
        this.coreActivity = coreActivity;
        transAviLoader = new TransAviLoader(coreActivity);
        tambolaSharedPref = new TambolaSharedPref(context);
        isUserExistsService();
    }

    private void isUserExistsService() {
        if (coreActivity.isFinishing()) {
            return;
        }
        if (!CommonUtils.isNetworkAvailable(context)) {
            DBToast.showAlertToast(context, context.getString(R.string.alert_network_not_exist));
        } else {
            if (!transAviLoader.isShowing()) {
                transAviLoader.show();
            }
            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(context).create(TambolaAPIInterface.class);
            Call<UserExistsModel> call = apiInterface.is_user_exists();
            call.enqueue(new Callback<UserExistsModel>() {
                @Override
                public void onResponse(@NonNull Call<UserExistsModel> call, @NonNull Response<UserExistsModel> response) {
                    try {
                        if (response.code() == TambolaConstants.API_SUCCESS) {
                            if (mUserExistTimer != null) {
                                mUserExistTimer.cancel();
                            }
                            if (transAviLoader.isShowing()) {
                                transAviLoader.dismiss();
                            }

                            UserExistsModel mUserExistsModel = response.body();
                            Log.d("isUserExistsService", "UserExistsModel: " + mUserExistsModel);
                            if (null != mUserExistsModel) {
                                QuestionAttemptStatusModel attemptStatusModel = mUserExistsModel.getQuestionAttemptStatusModel();
                                if (null != attemptStatusModel) {
                                    int attemptCode = attemptStatusModel.getAttemptCode();
                                    tambolaSharedPref.saveAttemptedQuestionCode(attemptCode);
                                    tambolaSharedPref.saveAttemptedQuestion(attemptStatusModel);
                                    if (attemptCode == 0 || attemptCode == 1) { // 0 = Not attempted || 1 = Attempted with correct answer
                                        checkAndNavigateToTambolaDashboard(mUserExistsModel);
                                    } else { // 2 = Attempted with wrong answer
                                        EasyDialogUtils.showInfoDialog(coreActivity, context.getString(R.string.alert_title_alert), attemptStatusModel.getAttemptMessage());
                                    }
                                } else {
                                    checkAndNavigateToTambolaDashboard(mUserExistsModel);
                                }
                            } else {
                                transAviLoader.dismiss();
                                EasyDialogUtils.showInfoDialog(coreActivity, context.getString(R.string.alert_title_failed), context.getString(R.string.alert_msg_server_failed));
                            }
                        } else {
                            retryApi();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage() + "");
                        transAviLoader.dismiss();
                        EasyDialogUtils.showInfoDialog(coreActivity, context.getString(R.string.alert_title_failed), context.getString(R.string.alert_request_unable_to_process));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UserExistsModel> call, @NonNull Throwable t) {
                    if (mUserExistTimer != null) {
                        mUserExistTimer.cancel();
                    }
                    retryApi();
                }
            });
        }
    }

    private void checkAndNavigateToTambolaDashboard(UserExistsModel userExistsModel) {
        tambolaSharedPref.saveTicketId(userExistsModel.getTicketId());
        //when ticket id not return null
        if (!TextUtils.isEmpty(userExistsModel.getTicketId())) {
            if (userExistsModel.getTambolaStatus().equalsIgnoreCase("1")) {
                TambolaDashboardActivity.startActivity(coreActivity, null);
            }
        } else {
            if (userExistsModel.getTambolaStatus().equalsIgnoreCase("1")) {
                BingoRegistrationActivity.startActivity(coreActivity, null);
            }
        }
        if (coreActivity != null) {
            coreActivity.finish();
        } else {
        }
    }

    private void retryApi() {
        if (mRetryCount <= TambolaConstants.MAX_API_COUNT) {
            mUserExistTimer = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    coreActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            isUserExistsService();
                        }
                    });
                }
            };
            mUserExistTimer.schedule(timerTask, mRetryCount * mInterval);
            Log.d("TambolaRetry", "Retry : " + mRetryCount + " Interval :" + mRetryCount * mInterval);
            mRetryCount++;
        } else {
            transAviLoader.dismiss();
            EasyDialogUtils.showInfoDialog(coreActivity, context.getString(R.string.alert_title_failed), context.getString(R.string.alert_msg_server_failed));
        }
    }
}
