package com.db.offers.tambola.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.db.offers.R;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.Utils;
import com.db.offers.common.activity.CoreActivity;
import com.db.offers.common.utils.NetworkStateReceiver;
import com.db.offers.tambola.utils.TambolaSharedPref;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public abstract class BaseActivity extends CoreActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    protected String TAG = "Bingo";

    protected abstract void initUI();

    protected abstract void processUI();

    protected TambolaSharedPref tambolaSharedPref;

    private final String DATE_FORMAT = "yyyy-MM-dd";
    private final SimpleDateFormat yyyyMMddDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
    protected final void initToolbar(String title) {

        Toolbar toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle(title);
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(view -> onBackPressed());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


        Utils.setStatusBarColor(this, ContextCompat.getColor(this, R.color.divya_theme_color));

        tambolaSharedPref = new TambolaSharedPref(getApplicationContext());
        TAG = getString(R.string.db_tambola_app_name);
    }




    @Override
    protected void onPause() {
        super.onPause();
       // overridePendingTransition( R.anim.enter_from_left,R.anim.exit_to_right);
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tambola_toolbar_menu, menu);
        MenuItem menuItem = menu.getItem(0);
        menuItem.setIcon(R.drawable.ic_home_black_24dp_tambola);
        return true;
    }


    protected String getFormattedCurrentDate() {
        Date currentDate = Calendar.getInstance().getTime();
        return yyyyMMddDateFormat.format(currentDate);
    }

    @Override
    protected void onDestroy() {
        CommonUtils.runGc();
        super.onDestroy();
    }

    protected void showStackedDashboard() {
        Intent intent = new Intent(this, TambolaDashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (this instanceof NumberSuccessActivity) {
            setResult(Activity.RESULT_OK);
            finish();
        } else {
            showStackedDashboard();
        }
        return super.onOptionsItemSelected(item);
    }

    public TambolaSharedPref getTambolaSharedPref() {
        return tambolaSharedPref;
    }

}
