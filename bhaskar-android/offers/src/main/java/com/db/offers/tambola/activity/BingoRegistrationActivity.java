package com.db.offers.tambola.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.db.offers.OfferController;
import com.db.offers.R;
import com.db.offers.common.AppConstants;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.Gender;
import com.db.offers.common.Profile;
import com.db.offers.common.UrlUtil;
import com.db.offers.common.fonticon.FontIconView;
import com.db.offers.megaoffers.providers.WebViewClientProvider;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.dialogs.RegistrationRequestDialog;
import com.db.offers.tambola.model.RegisterUserModel;
import com.db.offers.tambola.utils.DatePickerFragment;
import com.db.offers.tambola.utils.Utils;
import com.bhaskar.appscommon.tracking.Tracking;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.google.android.material.snackbar.Snackbar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public final class BingoRegistrationActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private CircleImageView mCivProfileImage;
    private ImageView mIvUploadPhoto, mIvHelp;
    private EditText mEtName, mEtEmailId, mEtMobileNumber, mEtDob;
    private Spinner mSpGender;
    private CheckBox mCbNiyamShartein;
    private TextView mTvNiyamShartein;
    private Button mBtnRegistration;
    private FontIconView name_icon, email_icon, dob_icon;
    private AlertDialog alertDialog1;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Snackbar alertSnackBar;
    private List<Gender> genderList;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bingo_registration);
        Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.TAMBOLA + "-" + CTConstant.PROP_VALUE.Tambola.SCREEN_REGISTRATION, "", "", "");
        String get_ticket_id = tambolaSharedPref.getTicketId();

        if (!get_ticket_id.equals("false") && !get_ticket_id.equals("")) {
            TambolaDashboardActivity.startActivity(BingoRegistrationActivity.this, null);
            finish();
        }

        initToolbar(getString(R.string.title_db_tambola));
        initUI();
        processUI();
    }

    @Override
    protected void initUI() {
        mCivProfileImage = findViewById(R.id.civ_profile_image);
        mIvUploadPhoto = findViewById(R.id.iv_upload_image);
        mIvHelp = findViewById(R.id.iv_help);
        mEtName = findViewById(R.id.et_name);
        mEtEmailId = findViewById(R.id.et_email_id);
        mEtMobileNumber = findViewById(R.id.et_mobile_number);
        mEtDob = findViewById(R.id.et_dob);
        mSpGender = findViewById(R.id.sp_gender);
        mCbNiyamShartein = findViewById(R.id.cb_niyam_shartein);
        mTvNiyamShartein = findViewById(R.id.tv_niyam_shartein);
        mBtnRegistration = findViewById(R.id.btn_registration);
        name_icon = findViewById(R.id.name_icon);
        email_icon = findViewById(R.id.email_icon);
        dob_icon = findViewById(R.id.dob_icon);
    }

    @Override
    protected void processUI() {

        preFillUserProfile();

        mCbNiyamShartein.setChecked(true);
        mCbNiyamShartein.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_green_dark)));

        mIvUploadPhoto.setOnClickListener(this);
        mIvHelp.setOnClickListener(this);
        mCbNiyamShartein.setOnCheckedChangeListener(this);
        mEtDob.setOnClickListener(this);
        mBtnRegistration.setOnClickListener(this);

        // Set span & mutiple clickable span
        setNiyamSharteinSpan();
        // Enable registration button
        enableRegistrationButton();

        // Remove below line during development
        mCivProfileImage.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.noimage_tambola));


        applyListenersToView();

        mEtDob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mEtDob.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * @param genderList
     */
    private void setGenderSpinnerAdapter(List<Gender> genderList) {
        int position = 0;
        this.genderList = genderList;
        if (genderList != null && !genderList.isEmpty()) {
            for (int index = 0; index < genderList.size(); index++) {
                Gender gender = genderList.get(index);
                if (gender.isSelected()) {
                    position = index;
                }
            }
        }
        ArrayAdapter<Gender> spinnerArrayAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_tambola_dropdown_item,
                genderList);
        mSpGender.setAdapter(spinnerArrayAdapter);
        mSpGender.setSelection(position);
    }

    private void preFillUserProfile() {
        String userVerifiedMobileNo = dbSharedPref.getUserVerifiedMobileNo();
        mEtMobileNumber.setText(userVerifiedMobileNo);

        Profile profile = dbSharedPref.getUserProfile();
        if (profile != null) {
            if (!TextUtils.isEmpty(profile.getName())) {
                mEtName.setText(profile.getName());
            }

            setGenderSpinnerAdapter(profile.getGender());

            String emailAddress = profile.getMail();

            try {
                String connectFrom = profile.getConnectFrom().get(0);
                AppConstants.UserLoginType userLoginType = AppConstants.UserLoginType.getUserLoginType(connectFrom);
                boolean isEditable = com.db.offers.tambola.utils.Utils.isEmailEditable(emailAddress, dbSharedPref.getDbId(), userLoginType);
                mEtEmailId.setEnabled(isEditable);
                mEtEmailId.setClickable(isEditable);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage() + "");
            }

            if (!TextUtils.isEmpty(emailAddress) && emailAddress.contains(dbSharedPref.getDbId())) {
                mEtEmailId.setHint(emailAddress);
                mEtEmailId.setText(Utils.getAccountEmail(dbApplication));
            } else if (!TextUtils.isEmpty(emailAddress)) {
                mEtEmailId.setText(emailAddress);
            } else {
                mEtEmailId.setText(Utils.getAccountEmail(dbApplication));
            }

            if (!TextUtils.isEmpty(profile.getDob())) {
                mEtDob.setText(profile.getDob());
            }
        }
    }

    private void applyListenersToView() {

        mEtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i > 5) {
                    name_icon.setVisibility(View.VISIBLE);
                } else {
                    name_icon.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mEtEmailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (CommonUtils.isEmailValid(mEtEmailId.getText().toString())) {
                    email_icon.setVisibility(View.VISIBLE);
                } else {
                    email_icon.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    /**
     * This method will be used to set span text & multiple clickable span
     */
    private void setNiyamSharteinSpan() {
        SpannableString spannableString;
        String termCondition = getString(R.string.title_terms_condition);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            spannableString = new SpannableString(Html.fromHtml(termCondition, Html.FROM_HTML_MODE_LEGACY));
        } else {
            spannableString = new SpannableString(termCondition);
        }

        // मेरी उम्र 18 साल+ है और मैं
        final ClickableSpan clickableNiyamSharteinCheckBoxSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                if (mCbNiyamShartein.isChecked()) {
                    mCbNiyamShartein.setChecked(false);
                } else {
                    mCbNiyamShartein.setChecked(true);
                }
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        // मेरी उम्र 18 साल+ है और मैं
        spannableString.setSpan(clickableNiyamSharteinCheckBoxSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamSharteinCheckbox,
                TambolaConstants.ClickableSpan.endIndexOfNiyamSharteinCheckbox, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // नियम और शर्तों
        final ClickableSpan clickableNiyamSharteinSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                niyamPopUp();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        // नियम और शर्तों
        spannableString.setSpan(clickableNiyamSharteinSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamShartein,
                TambolaConstants.ClickableSpan.endIndexOfNiyamShartein, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // को स्वीकार करता/करती हूं
        ClickableSpan clickableNiyamSharteinCheckBoxTillEndSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                if (mCbNiyamShartein.isChecked()) {
                    mCbNiyamShartein.setChecked(false);
                } else {
                    mCbNiyamShartein.setChecked(true);
                }
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        // को स्वीकार करता/करती हूं
        spannableString.setSpan(clickableNiyamSharteinCheckBoxTillEndSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamSharteinTillEnd,
                TambolaConstants.ClickableSpan.endIndexOfNiyamSharteinTillEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Set span text
        mTvNiyamShartein.setText(spannableString);
        mTvNiyamShartein.setMovementMethod(LinkMovementMethod.getInstance());
        mTvNiyamShartein.setHighlightColor(Color.TRANSPARENT);

        // Change color of मेरी उम्र 18 साल+ है और मैं
        changeSpanTextColor(clickableNiyamSharteinCheckBoxSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamSharteinCheckbox,
                TambolaConstants.ClickableSpan.endIndexOfNiyamSharteinCheckbox, ContextCompat.getColor(getApplicationContext(), R.color.form_title_color));

        // Change color of नियम और शर्तों
        changeSpanTextColor(clickableNiyamSharteinSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamShartein,
                TambolaConstants.ClickableSpan.endIndexOfNiyamShartein, ContextCompat.getColor(getApplicationContext(), R.color.terms_condition_color));

        // Change color of को स्वीकार करता/करती हूं
        changeSpanTextColor(clickableNiyamSharteinCheckBoxTillEndSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamSharteinTillEnd,
                TambolaConstants.ClickableSpan.endIndexOfNiyamSharteinTillEnd, ContextCompat.getColor(getApplicationContext(), R.color.form_title_color));
    }

    /**
     * This method will be used to change span color
     *
     * @param clickableSpan
     * @param startIndex
     * @param endtIndex
     * @param color
     */
    private void changeSpanTextColor(ClickableSpan clickableSpan, int startIndex, int endtIndex, int color) {
        Spannable text = (Spannable) mTvNiyamShartein.getText();
        // Change color of span
        text.setSpan(clickableSpan, startIndex, endtIndex, Spannable.SPAN_POINT_MARK);
        text.setSpan(new ForegroundColorSpan(color), startIndex, endtIndex, 0);
    }

    /**
     * This method will be used to show tooltip on help icon
     */
    private void showViewToolTip() {
        ViewTooltip.on(mIvHelp).align(ViewTooltip.ALIGN.CENTER).arrowWidth(TambolaConstants.ToolTip.TOOLTIP_WIDTH).arrowHeight
                (TambolaConstants.ToolTip.TOOLTIP_HEIGHT).autoHide(true, TambolaConstants.ToolTip.TOOLTIP_AUTO_DISMISS).clickToHide(true).
                color(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite)).text(getString(R.string.help_mobile_number)).
                textColor(ContextCompat.getColor(getApplicationContext(), R.color.form_title_color)).position(ViewTooltip.Position.TOP).show();
    }


    /**
     * This method will be used to disable registration button if terms & condition is unchecked
     */
    private void disableRegistrationButton() {
        mBtnRegistration.setEnabled(false);
        mBtnRegistration.setAlpha(0.5f);
    }

    /**
     * This method will be used to enable registration button if terms & condition is checked
     */
    private void enableRegistrationButton() {
        mBtnRegistration.setEnabled(true);
        mBtnRegistration.setAlpha(1.0f);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();

        if (id == R.id.cb_niyam_shartein) {
            if (isChecked) {
                buttonView.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_green_dark)));
                // Enable registration button
                enableRegistrationButton();
            } else {
                buttonView.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), android.R.color.darker_gray)));
                // Disable registration button
                disableRegistrationButton();
            }
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_upload_image) {
            profilePopup();
        } else if (id == R.id.iv_help) {
            showViewToolTip();
        } else if (id == R.id.btn_registration) {
            if (TextUtils.isEmpty(mEtName.getText())) {
                mEtName.setError(getString(R.string.err_msg_user_name));
                mEtName.requestFocus();
            } else if (TextUtils.isEmpty(mEtEmailId.getText())) {
                mEtEmailId.setError(getString(R.string.err_msg_email));
                mEtEmailId.requestFocus();
            } else if (!CommonUtils.isEmailValid(mEtEmailId.getText().toString())) {
                mEtEmailId.setError(getString(R.string.err_msg_email));
                mEtEmailId.requestFocus();
            }
            else {
                makeUserRegistration();
            }
        } else if (id == R.id.et_dob) {
            DatePickerFragment dob = new DatePickerFragment();
            String dateOfBirth = mEtDob.getText().toString();
            if (!TextUtils.isEmpty(dateOfBirth)) {
                Bundle bundle = new Bundle();
                bundle.putString(DatePickerFragment.KEY_SELECTED_DATE, dateOfBirth);
                dob.setArguments(bundle);
            }
            dob.show(getSupportFragmentManager(), "DatePicker");
            dob.setOnDatePicked((day, month, year) -> {
                int d, m, y;
                y = Integer.valueOf(year);
                m = Integer.valueOf(month);
                d = Integer.valueOf(day);
                Calendar userAge = new GregorianCalendar(y, m, d);
                Calendar minAdultAge = new GregorianCalendar();
                minAdultAge.add(Calendar.YEAR, -18);
                if (minAdultAge.before(userAge)) {
                    Toast.makeText(BingoRegistrationActivity.this, getString(R.string.you_are_below_18), Toast.LENGTH_LONG).show();
                } else {
                    String dob1 = year + "-" + month + "-" + day;
                    mEtDob.setText(dob1);
                    dob_icon.setVisibility(View.VISIBLE);
                }
            });
        }


    }

    private void profilePopup() {
        if (!isFinishing()) {
            LayoutInflater layoutInflater = getLayoutInflater();
            View customView = layoutInflater.inflate(R.layout.dialog_tambola_profile, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(BingoRegistrationActivity.this);
            builder.setView(customView);

            final LinearLayout camara, gallery;
            camara = customView.findViewById(R.id.camara);
            gallery = customView.findViewById(R.id.gallery);
            final boolean result = CommonUtils.checkPermission(BingoRegistrationActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            camara.setOnClickListener(view -> {
                if (result) {
                    alertDialog1.dismiss();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else {
                    alertDialog1.dismiss();
                    if (ContextCompat.checkSelfPermission(BingoRegistrationActivity.this,
                            Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(BingoRegistrationActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission
                                .WRITE_EXTERNAL_STORAGE}, 0);
                    }
                }
            });
            final boolean resultOne = CommonUtils.checkPermission(BingoRegistrationActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
            gallery.setOnClickListener(view -> {
                if (resultOne) {
                    alertDialog1.dismiss();
                    galleryIntent();
                } else {
                    alertDialog1.dismiss();
                    if (ContextCompat.checkSelfPermission(BingoRegistrationActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager
                            .PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(BingoRegistrationActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                    }
                }
            });

            alertDialog1 = builder.create();
            alertDialog1.show();
        }
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    /**
     * @param alertStringResId
     */
    private void showAlertSnackBar(@StringRes int alertStringResId) {
        if (null == alertSnackBar) {
            View view = findViewById(R.id.cl_parent);
            alertSnackBar = Snackbar
                    .make(view, alertStringResId, Snackbar.LENGTH_LONG)
                    .setAction("OK", view1 -> alertSnackBar.dismiss());
            TextView tv = alertSnackBar.getView().findViewById(R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
        } else {
            alertSnackBar.setText(alertStringResId);
        }
        alertSnackBar.show();
    }


    @Override
    public void networkAvailable() {
        enableRegistrationButton();
    }

    @Override
    public void networkUnavailable() {
        disableRegistrationButton();
    }

    private void makeUserRegistration() {
        if (CommonUtils.isNetworkAvailable(getApplicationContext())) {

            showRegistrationApiDialog();

        } else {
            showAlertSnackBar(R.string.alert_network_not_exist);
        }
    }

    private void showRegistrationApiDialog() {
        String otp = "1";

        Gender gender = new Gender();
        if (genderList != null && !genderList.isEmpty()) {
            gender = genderList.get(mSpGender.getSelectedItemPosition());
        }

        RegisterUserModel userModel = new RegisterUserModel(mEtName.getText().toString(),
                mEtEmailId.getText().toString(), gender.getId(), mEtMobileNumber.getText().toString(), mEtDob.getText().toString(), otp);

        new RegistrationRequestDialog(BingoRegistrationActivity.this, userModel).show();
    }

    /**
     * @param context
     * @param bundle
     */
    public static void startActivity(Activity context, Bundle bundle) {
        Intent intent = new Intent(context, BingoRegistrationActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }

    /**
     * @param data
     */
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mCivProfileImage.setImageBitmap(bm);
    }

    private void onCaptureImageResult(Intent data) {
        try {
            Bundle bundle = data.getExtras();
            if (null != bundle) {
                Bitmap bitmap = (Bitmap) bundle.get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                }
                File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                destination.createNewFile();
                FileOutputStream fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();

                mCivProfileImage.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        alertDialog1.dismiss();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void niyamPopUp() {
        final Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(builder.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        builder.setContentView(R.layout.dialog_tambola_niyam);
        builder.show();

        ImageView cancel = builder.findViewById(R.id.cancel);
        cancel.setOnClickListener(view -> builder.dismiss());

        WebView mWebview = builder.findViewById(R.id.web_niyam);
        ProgressBar progressBar = builder.findViewById(R.id.progressBarWeb);
        mWebview.setWebViewClient(new WebViewClientProvider(this, (view, url) -> {
            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
        }));
        mWebview.getSettings().setLoadsImagesAutomatically(true);
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebview.loadUrl(UrlUtil.getTambolaTandCUrl());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

}
