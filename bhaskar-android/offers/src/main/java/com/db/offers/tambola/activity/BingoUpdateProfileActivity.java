package com.db.offers.tambola.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.db.offers.BuildConfig;
import com.db.offers.OfferController;
import com.db.offers.R;
import com.db.offers.common.AppConstants;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.DBToast;
import com.db.offers.common.Gender;
import com.db.offers.common.Profile;
import com.db.offers.common.TransAviLoader;
import com.db.offers.common.UrlUtil;
import com.db.offers.common.network.RetroCallbackProvider;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.imageprovider.imageCompression.ImageCompressionListener;
import com.db.offers.tambola.imageprovider.imagePicker.ImagePicker;
import com.db.offers.tambola.model.GetProfileModel;
import com.db.offers.tambola.model.UpdateProfileResponseModel;
import com.db.offers.tambola.network.TambolaAPIClient;
import com.db.offers.tambola.network.TambolaAPIInterface;
import com.db.offers.tambola.utils.DatePickerFragment;
import com.db.offers.tambola.utils.Utils;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class BingoUpdateProfileActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private CircleImageView mCivProfileImage;
    private ImageView mIvUploadPhoto, mIvHelp;
    private EditText mEtName, mEtEmailId, mEtMobileNumber, mEtDob;
    private Spinner mSpGender;
    private CheckBox mCbNiyamShartein;
    private TextView mTvNiyamShartein;
    private Button mBtnRegistration;
    private ImageView number_icon, name_icon, email_icon, dob_icon;
    private String selectedPath = "";
    private Snackbar alertSnackBar;
    private List<Gender> genderList;
    private ProgressBar pb;
    private ImagePicker imagePicker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambola_update_profile);

        initToolbar(getString(R.string.title_db_tambola));
        initUI();
        processUI();
//        CTTracker.cleverTapTrackPageView(getApplicationContext(), CTConstant.PROP_VALUE.Tambola.SCREEN_USER_INFO,
//                CTConstant.PROP_VALUE.TAMBOLA);
        Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.TAMBOLA + "-" + CTConstant.PROP_VALUE.Tambola.SCREEN_USER_INFO, "", "", "");

        getProfileUpdate();
    }

    private void getProfileUpdate() {
        if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
            showAlertSnackBar(R.string.alert_network_not_exist);
        } else {
            final TransAviLoader transAviLoader = new TransAviLoader(this);
            transAviLoader.show();
            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(this)
                    .create(TambolaAPIInterface.class);

            final Call<GetProfileModel> call = apiInterface.getProfile();
            call.enqueue(new Callback<GetProfileModel>() {
                @Override
                public void onResponse(Call<GetProfileModel> call, Response<GetProfileModel> response) {
                    transAviLoader.dismiss();
                    try{
                        GetProfileModel profileModel = response.body();
                        Profile profile = new Profile();
                        profile.setName(profileModel.data.name);
                        profile.setMail(profileModel.data.email);
                        profile.setMobileNo(profileModel.data.mobile_no);
                        profile.setAvatar(profileModel.data.photo);
                        profile.setDbID(TrackingData.getDBId(BingoUpdateProfileActivity.this));
                        String gender = profileModel.data.gender;
                        List<Gender> genders = new ArrayList<>();
                        Gender genderM = new Gender();
                        genderM.name = "Male";
                        genderM.setId(1);
                        Gender genderF = new Gender();
                        genderF.name = "Female";
                        genderF.setId(2);
                        if (gender.equalsIgnoreCase("2"))
                            genderF.selected = true;
                        else
                            genderM.selected = true;
                        genders.add(genderM);
                        genders.add(genderF);
                        profile.setGender(genders);
                        OfferController.getInstance().getDbSharedPref().updateUserProfile(profile);
                        preFillUserProfile();
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(Call<GetProfileModel> call, Throwable t) {
                    transAviLoader.dismiss();
                }
            });


        }

    }

    @Override
    protected void initUI() {
        mCivProfileImage = findViewById(R.id.civ_profile_image);
        mIvUploadPhoto = findViewById(R.id.iv_upload_image);
        mIvHelp = findViewById(R.id.iv_help);
        mEtName = findViewById(R.id.et_name);
        mEtEmailId = findViewById(R.id.et_email_id);
        mEtMobileNumber = findViewById(R.id.et_mobile_number);
        mEtDob = findViewById(R.id.et_dob);
        mSpGender = findViewById(R.id.sp_gender);
        mCbNiyamShartein = findViewById(R.id.cb_niyam_shartein);
        mTvNiyamShartein = findViewById(R.id.tv_niyam_shartein);
        mBtnRegistration = findViewById(R.id.btn_registration);
        number_icon = findViewById(R.id.number_icon);
        name_icon = findViewById(R.id.name_icon);
        email_icon = findViewById(R.id.email_icon);
        dob_icon = findViewById(R.id.dob_icon);
    }

    @Override
    protected void processUI() {

        imagePicker = new ImagePicker();

        mCbNiyamShartein.setChecked(true);
        mCbNiyamShartein.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_green_dark)));

        mIvUploadPhoto.setOnClickListener(this);
        mIvHelp.setOnClickListener(this);
        mCbNiyamShartein.setOnCheckedChangeListener(this);
        mEtDob.setOnClickListener(this);
        mBtnRegistration.setOnClickListener(this);

        // changing text of registration button
        mBtnRegistration.setText(getString(R.string.update_btn));

        // Set span & mutiple clickable span
        setNiyamSharteinSpan();
        // Enable registration button
        enableRegistrationButton();

        // Remove below line during development
        mCivProfileImage.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.noimage_tambola));

        applyListenersToView();

        ////getProfileService();

        mEtDob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mEtDob.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        preFillUserProfile();
    }

    private void applyListenersToView() {

        mEtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i > 5) {
                    name_icon.setVisibility(View.VISIBLE);
                } else {
                    name_icon.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mEtEmailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (CommonUtils.isEmailValid(mEtEmailId.getText().toString())) {
                    email_icon.setVisibility(View.VISIBLE);
                } else {
                    email_icon.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mEtMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                number_icon.setVisibility(View.INVISIBLE);
                if (start == 9 && after == 1 && CommonUtils.isValidPhoneNumber(mEtMobileNumber.getText().toString())) {
                    number_icon.setVisibility(View.VISIBLE);
                } else if (start == 9 && after == 0) {
                    number_icon.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                String mobileNumber = mEtMobileNumber.getText().toString().trim();
//                if (!mobileNumber.startsWith(getString(R.string.country_code)) && mobileNumber.length() > 0) {
//                    String concatenatedText = getString(R.string.country_code) + s.toString();
//                    mEtMobileNumber.setText(concatenatedText);
//                    Selection.setSelection(mEtMobileNumber.getText(), mEtMobileNumber.getText().length());
//
//                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * This method will be used to set span text & multiple clickable span
     */
    private void setNiyamSharteinSpan() {
        SpannableString spannableString;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            spannableString = new SpannableString(Html.fromHtml(getString(R.string.title_terms_condition), Html.FROM_HTML_MODE_LEGACY));
        } else {
            spannableString = new SpannableString(getString(R.string.title_terms_condition));
        }

        // मेरी उम्र 18 साल+ है और मैं
        ClickableSpan clickableNiyamSharteinCheckBoxSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                if (mCbNiyamShartein.isChecked()) {
                    mCbNiyamShartein.setChecked(false);
                } else {
                    mCbNiyamShartein.setChecked(true);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        // मेरी उम्र 18 साल+ है और मैं
        spannableString.setSpan(clickableNiyamSharteinCheckBoxSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamSharteinCheckbox,
                TambolaConstants.ClickableSpan.endIndexOfNiyamSharteinCheckbox, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // नियम और शर्तों
        ClickableSpan clickableNiyamSharteinSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                niyamPopUp();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        // नियम और शर्तों
        spannableString.setSpan(clickableNiyamSharteinSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamShartein,
                TambolaConstants.ClickableSpan.endIndexOfNiyamShartein, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // को स्वीकार करता/करती हूं
        ClickableSpan clickableNiyamSharteinCheckBoxTillEndSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                if (mCbNiyamShartein.isChecked()) {
                    mCbNiyamShartein.setChecked(false);
                } else {
                    mCbNiyamShartein.setChecked(true);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        // को स्वीकार करता/करती हूं
        spannableString.setSpan(clickableNiyamSharteinCheckBoxTillEndSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamSharteinTillEnd, TambolaConstants.ClickableSpan.endIndexOfNiyamSharteinTillEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // Set span text
        mTvNiyamShartein.setText(spannableString);
        mTvNiyamShartein.setMovementMethod(LinkMovementMethod.getInstance());
        mTvNiyamShartein.setHighlightColor(Color.TRANSPARENT);

        // Change color of मेरी उम्र 18 साल+ है और मैं
        changeSpanTextColor(clickableNiyamSharteinCheckBoxSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamSharteinCheckbox, TambolaConstants.ClickableSpan.endIndexOfNiyamSharteinCheckbox, ContextCompat.getColor(getApplicationContext(), R.color.form_title_color));

        // Change color of नियम और शर्तों
        changeSpanTextColor(clickableNiyamSharteinSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamShartein, TambolaConstants.ClickableSpan.endIndexOfNiyamShartein, ContextCompat.getColor(getApplicationContext(), R.color.terms_condition_color));

        // Change color of को स्वीकार करता/करती हूं
        changeSpanTextColor(clickableNiyamSharteinCheckBoxTillEndSpan, TambolaConstants.ClickableSpan.startIndexOfNiyamSharteinTillEnd, TambolaConstants.ClickableSpan.endIndexOfNiyamSharteinTillEnd, ContextCompat.getColor(getApplicationContext(), R.color.form_title_color));
    }

    /**
     * This method will be used to change span color
     *
     * @param clickableSpan
     * @param startIndex
     * @param endtIndex
     * @param color
     */
    private void changeSpanTextColor(ClickableSpan clickableSpan, int startIndex, int endtIndex, int color) {
        Spannable text = (Spannable) mTvNiyamShartein.getText();
        // Change color of span
        text.setSpan(clickableSpan, startIndex, endtIndex, Spannable.SPAN_POINT_MARK);
        text.setSpan(new ForegroundColorSpan(color), startIndex, endtIndex, 0);
    }

    /**
     * This method will be used to show tooltip on help icon
     */
    private void showViewToolTip() {
        ViewTooltip.on(mIvHelp).align(ViewTooltip.ALIGN.CENTER).arrowWidth(TambolaConstants.ToolTip.TOOLTIP_WIDTH).arrowHeight
                (TambolaConstants.ToolTip.TOOLTIP_HEIGHT).autoHide(true, TambolaConstants.ToolTip.TOOLTIP_AUTO_DISMISS).clickToHide(true).
                color(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite)).text(getString(R.string.help_mobile_number)).
                textColor(ContextCompat.getColor(getApplicationContext(), R.color.form_title_color)).position(ViewTooltip.Position.TOP).show();
    }


    /**
     * This method will be used to disable registration button if terms & condition is unchecked
     */
    private void disableRegistrationButton() {
        mBtnRegistration.setEnabled(false);
        mBtnRegistration.setAlpha(0.5f);
    }

    /**
     * This method will be used to enable registration button if terms & condition is checked
     */
    private void enableRegistrationButton() {
        mBtnRegistration.setEnabled(true);
        mBtnRegistration.setAlpha(1.0f);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        int id = buttonView.getId();

        if (id == R.id.cb_niyam_shartein) {
            if (isChecked) {
                buttonView.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_green_dark)));
                // Enable registration button
                enableRegistrationButton();
            } else {
                buttonView.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), android.R.color.darker_gray)));
                // Disable registration button
                disableRegistrationButton();
            }
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_upload_image) {
            profilePopup();
            // profilePopup();
        } else if (id == R.id.iv_help) {
            showViewToolTip();
        } else if (id == R.id.btn_registration) {
            if (mEtName.getText().toString().isEmpty()) {
                mEtName.setError(getResources().getString(R.string.err_msg_user_name));
                mEtName.requestFocus();
            } else if (mEtEmailId.getText().toString().isEmpty()) {
                mEtEmailId.setError(getResources().getString(R.string.err_msg_email));
                mEtEmailId.requestFocus();
            } else if (!CommonUtils.isEmailValid(mEtEmailId.getText().toString())) {
                mEtEmailId.setError(getResources().getString(R.string.err_msg_email));
                mEtEmailId.requestFocus();
            }
            /*else if (mEtMobileNumber.getText().toString().isEmpty()) {
                mEtMobileNumber.setError(getResources().getString(R.string.err_msg_phone));
                mEtMobileNumber.requestFocus();
            } else if (mEtMobileNumber.getText().toString().length() < 10 || mEtMobileNumber.getText().toString().length() > 10) {
                mEtMobileNumber.setError(getResources().getString(R.string.err_msg_phone));
                mEtMobileNumber.requestFocus();
            }*/
            /*else if (mEtDob.getText().toString().isEmpty()) {
                mEtDob.setError(getResources().getString(R.string.err_msg_date));
                mEtDob.requestFocus();
            }*/
            else {
                Gender gender = new Gender();
                if (genderList != null && !genderList.isEmpty()) {
                    gender = genderList.get(mSpGender.getSelectedItemPosition());
                }
//                updateDBUserProfile(mEtName.getText().toString(), mEtEmailId.getText().toString(), mEtMobileNumber.getText().toString(),
//                        mEtDob.getText().toString(), gender.getId());
                uploadFile(mEtName.getText().toString(), mEtEmailId.getText().toString(), mEtMobileNumber.getText().toString(),
                        mEtDob.getText().toString(), gender.getId());

                /*if (getMobileNo.equals(mEtMobileNumber.getText().toString())) {
                    uploadFile(mEtName.getText().toString(), mEtEmailId.getText().toString(), mEtMobileNumber.getText().toString(),
                            mEtDob.getText().toString(), gender.getId());
                } else {
                    sendOTPService();
                }*/
            }
        } else if (id == R.id.et_dob) {
            DatePickerFragment dob = new DatePickerFragment();
            String dateOfBirth = mEtDob.getText().toString();
            if (!TextUtils.isEmpty(dateOfBirth)) {
                Bundle bundle = new Bundle();
                bundle.putString(DatePickerFragment.KEY_SELECTED_DATE, dateOfBirth);
                dob.setArguments(bundle);
            }
            dob.show(getSupportFragmentManager(), "DatePicker");
            dob.setOnDatePicked(new DatePickerFragment.OnDatePicked() {
                @Override
                public void onDateProvided(String day, String month, String year) {

                    int d, m, y;
                    y = Integer.valueOf(year);
                    m = Integer.valueOf(month);
                    d = Integer.valueOf(day);
                    Calendar userAge = new GregorianCalendar(y, m, d);
                    Calendar minAdultAge = new GregorianCalendar();
                    minAdultAge.add(Calendar.MONTH, 1);
                    minAdultAge.add(Calendar.YEAR, -18);

                    if (minAdultAge.before(userAge)) {
                        Toast.makeText(BingoUpdateProfileActivity.this, getString(R.string.you_are_below_18), Toast.LENGTH_LONG).show();
                    } else {
                        mEtDob.setText(year + "-" + month + "-" + day);
                        dob_icon.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

//    //send otp service
//    private void sendOTPService() {
//        if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
//            showAlertSnackBar(R.string.alert_network_not_exist);
//        } else {
//            TransAviLoader pd = new TransAviLoader(this);
//            pd.show();
//
//            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(getDbApplication()).create(TambolaAPIInterface.class);
//            SendOtpModel user = new SendOtpModel(mEtMobileNumber.getText().toString());
//            Call<OtpModel> call = apiInterface.otpSend(user);
//
//            call.enqueue(new Callback<OtpModel>() {
//                @Override
//                public void onResponse(@NonNull Call<OtpModel> call, @NonNull Response<OtpModel> response) {
//                    pd.dismiss();
//                    otpModel = response.body();
//                    Log.d("otp response", otpModel + "");
//                    otpPopup();
//                }
//
//                @Override
//                public void onFailure(Call<OtpModel> call, Throwable t) {
//                    pd.dismiss();
//                    Log.d("onFailure", t.getMessage());
//                }
//            });
//        }
//    }

//    private void otpPopup() {
//        otpBuilder = new Dialog(this);
//        otpBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        Objects.requireNonNull(otpBuilder.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        otpBuilder.setContentView(R.layout.dialog_tambola_otp);
//        otpBuilder.setCancelable(false);
//        if (!this.isFinishing()) {
//            otpBuilder.show();
//        }
//        final TextView agin_otp, cancel, ok, time;
//        otpView = otpBuilder.findViewById(R.id.otp_view);
//        agin_otp = otpBuilder.findViewById(R.id.agin_otp);
//        cancel = otpBuilder.findViewById(R.id.cancel);
//        time = otpBuilder.findViewById(R.id.time);
//        ok = otpBuilder.findViewById(R.id.ok);
//
//        timeCountInMilliSeconds = 60000;
//
//        new CountDownTimer(timeCountInMilliSeconds, 1000) {
//            @Override
//            public void onTick(long l) {
//                timeCountInMilliSeconds = l;
//                time.setText(hmsTimeFormatter(l));
//                agin_otp.setEnabled(false);
//                agin_otp.setAlpha(0.5f);
//            }
//
//            @Override
//            public void onFinish() {
//                agin_otp.setEnabled(true);
//                agin_otp.setAlpha(1.0f);
//                time.setText(hmsTimeFormatter(timeCountInMilliSeconds));
//                //countDownTimer.start();
//            }
//
//            private String hmsTimeFormatter(long milliSeconds) {
//                String ms = String.format("%02d %02d",
//                        TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
//                        TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));
//                return ms;
//            }
//        }.start();
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                otpBuilder.dismiss();
//            }
//        });
//
//        ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (TextUtils.isEmpty(otpView.getText())) {
//                    Toast.makeText(BingoUpdateProfileActivity.this, getString(R.string.enter_valid_otp), Toast.LENGTH_LONG).show();
//                } else if (otpView.getText().length() < 6) {
//                    Toast.makeText(BingoUpdateProfileActivity.this, getString(R.string.enter_valid_otp), Toast.LENGTH_LONG).show();
//                } else if (otpModel != null && !otpModel.getOtp().equals(otpView.getText().toString())) {
//                    Toast.makeText(BingoUpdateProfileActivity.this, getString(R.string.invalid_otp), Toast.LENGTH_LONG).show();
//                } else {
//                    getMobileNo = mEtMobileNumber.getText().toString();
//                    Gender gender = new Gender();
//                    if (genderList != null && !genderList.isEmpty()) {
//                        gender = genderList.get(mSpGender.getSelectedItemPosition());
//                    }
//                    uploadFile(mEtName.getText().toString(), mEtEmailId.getText().toString(), mEtMobileNumber.getText().toString(),
//                            mEtDob.getText().toString(), gender.getId());
//                }
//            }
//        });
//        agin_otp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                otpBuilder.dismiss();
//                sendOTPService();
//            }
//        });
//    }

    private void profilePopup() {
        final boolean result = CommonUtils.checkPermission(BingoUpdateProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (result) {
            try {
                imagePicker.withActivity(BingoUpdateProfileActivity.this).start();
            } catch (Exception e) {
                Log.e(TAG, "profilePopup: " + e.getMessage());
            }
        } else {
            ActivityCompat.requestPermissions(BingoUpdateProfileActivity.this, new String[]{Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 0:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
                    try {
                        imagePicker.withActivity(BingoUpdateProfileActivity.this).start();
                    } catch (Exception e) {
                        Log.e(TAG, "onRequestPermissionsResult: " + e.getMessage());
                    }
                }
                break;
            case TambolaConstants.EXTERNAL_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Storage permission is given successfully");
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Storage permission is not given");
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePicker.SELECT_IMAGE && resultCode == Activity.RESULT_OK) {
            //Add compression listener if withCompression is set to true
            imagePicker.addOnCompressListener(new ImageCompressionListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onCompressed(String filePath) {//filePath of the compressed image
                    //convert to bitmap easily
                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                    mCivProfileImage.setImageBitmap(selectedImage);
                    selectedPath = filePath;
                    // putResult(selectedImage);
                }
            });

            //call the method 'getImageFilePath(Intent data)' even if compression is set to false
            if (imagePicker != null) {
                String filePath = imagePicker.getImageFilePath(data);
                if (filePath != null) {//filePath will return null if compression is set to true
                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                    mCivProfileImage.setImageBitmap(selectedImage);
                    selectedPath = filePath;
                }
            }
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void niyamPopUp() {
        final Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        builder.setContentView(R.layout.dialog_tambola_niyam);
        builder.show();

        ImageView cancel = (ImageView) builder.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.dismiss();
            }
        });

        WebView mWebview = builder.findViewById(R.id.web_niyam);
        pb = builder.findViewById(R.id.progressBarWeb);
        mWebview.setWebViewClient(new MyBrowser());
        mWebview.getSettings().setLoadsImagesAutomatically(true);
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebview.loadUrl(UrlUtil.getTambolaTandCUrl());
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {

    }

    private final class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            pb.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pb.setVisibility(View.GONE);
        }
    }

    /**
     * @param context
     * @param bundle
     */
    public static void startActivity(Activity context, Bundle bundle) {
        Intent intent = new Intent(context, BingoUpdateProfileActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }

    /**
     * @param alertStringResId
     */
    private void showAlertSnackBar(@StringRes int alertStringResId) {
        if (null == alertSnackBar) {
            View view = findViewById(R.id.cl_parent);
            alertSnackBar = Snackbar
                    .make(view, alertStringResId, Snackbar.LENGTH_LONG)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertSnackBar.dismiss();
                        }
                    });
            TextView tv = alertSnackBar.getView().findViewById(R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
        } else {
            alertSnackBar.setText(alertStringResId);
        }
        alertSnackBar.show();
    }

    /**
     * @param genderList
     */
    private void setGenderSpinnerAdapter(List<Gender> genderList) {
        int position = 0;
        this.genderList = genderList;
        if (genderList != null && !genderList.isEmpty()) {
            for (int index = 0; index < genderList.size(); index++) {
                Gender gender = genderList.get(index);
                if (gender.isSelected()) {
                    position = index;
                }
            }
        }
        ArrayAdapter<Gender> spinnerArrayAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_tambola_dropdown_item,
                genderList);
        mSpGender.setAdapter(spinnerArrayAdapter);
        mSpGender.setSelection(position);
    }

    private void preFillUserProfile() {
        String userVerifiedMobileNo = dbSharedPref.getUserVerifiedMobileNo();
        mEtMobileNumber.setText(userVerifiedMobileNo);

        Profile profile = dbSharedPref.getUserProfile();
        if (profile != null) {
            if (!TextUtils.isEmpty(profile.getName())) {
                mEtName.setText(profile.getName());
            }

            setGenderSpinnerAdapter(profile.getGender());

            String emailAddress = profile.getMail();

            try {
                String connectFrom = profile.getConnectFrom().get(0);
                AppConstants.UserLoginType userLoginType = AppConstants.UserLoginType.getUserLoginType(connectFrom);
                boolean isEditable = Utils.isEmailEditable(emailAddress, dbSharedPref.getDbId(), userLoginType);
                mEtEmailId.setEnabled(isEditable);
                mEtEmailId.setClickable(isEditable);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage() + "");
            }

            if (!TextUtils.isEmpty(emailAddress) && emailAddress.contains(dbSharedPref.getDbId())) {
                mEtEmailId.setHint(emailAddress);
                mEtEmailId.setText(Utils.getAccountEmail(dbApplication));
            } else if (!TextUtils.isEmpty(emailAddress)) {
                mEtEmailId.setText(emailAddress);
            } else {
                mEtEmailId.setText(Utils.getAccountEmail(dbApplication));
            }

            if (!TextUtils.isEmpty(profile.getDob())) {
                mEtDob.setText(profile.getDob());
            }

            if (!TextUtils.isEmpty(profile.getAvatar())) {
                Glide.with(BingoUpdateProfileActivity.this)
                        .load(profile.getAvatar())
                        .into(mCivProfileImage);
            }
        }
    }


    /***
     *
     * @param name
     * @param email
     * @param mobile_no
     * @param dob
     * @param gender
     */
    private void uploadFile(String name, String email, String mobile_no, String dob, String gender) {
        final TransAviLoader transAviLoader = new TransAviLoader(this);
        transAviLoader.show();

        RequestBody requestFile = null;
        //creating a file
        if (!TextUtils.isEmpty(selectedPath)) {
            File file = new File(selectedPath);
//            updateProfilePictureInBackground(file);
            requestFile = RequestBody.create(MediaType.parse("image/.jpg"), file);
        }

        //creating request body for file
        //RequestBody requestFile = RequestBody.create(MediaType.parse(getContentResolver().getType(fileUri)), file);
        RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody mobile_noBody = RequestBody.create(MediaType.parse("text/plain"), mobile_no);
        RequestBody dobBody = RequestBody.create(MediaType.parse("text/plain"), dob);
        RequestBody genderBody = RequestBody.create(MediaType.parse("text/plain"), gender);

        TambolaAPIInterface api = TambolaAPIClient.getClient(dbApplication).create(TambolaAPIInterface.class);

        //creating a call and calling the upload image method
        Call<UpdateProfileResponseModel> call = api.uploadProfileImage(requestFile, nameBody, emailBody, mobile_noBody, dobBody, genderBody);

        call.enqueue(new RetroCallbackProvider<UpdateProfileResponseModel>() {
            @Override
            public void onSuccess(Call<UpdateProfileResponseModel> call, @NonNull UpdateProfileResponseModel updateProfileResponseModel) {
                try {
                    transAviLoader.dismiss();
                    if (updateProfileResponseModel.message.equalsIgnoreCase("Success")) {
                        Log.d("upload profile data", updateProfileResponseModel.message);
                        DBToast.showSuccessToast(dbApplication, getString(R.string.alert_title_success), getString(R.string.profile_updated));
                        finish();
                    } else {
                        DBToast.showFailToast(dbApplication, getString(R.string.alert_title_failed), getString(R.string.alert_api_failed));
                    }
                } catch (Exception e) {
                    DBToast.showFailToast(dbApplication, getString(R.string.alert_title_failed), getString(R.string.alert_api_failed));
                    Log.e(TAG, e.getMessage() + "");
                }
            }

            @Override
            public void onFailed(Call<UpdateProfileResponseModel> call, Throwable t) {
                transAviLoader.dismiss();
                DBToast.showFailToast(dbApplication, getString(R.string.alert_title_failed), getString(R.string.alert_api_failed));
            }
        });
    }


    /***
     *
     * @param name
     * @param email
     * @param mobile_no
     * @param dob
     * @param gender
     */
//    private void updateDBUserProfile(String name, String email, String mobile_no, String dob, String gender) {
//        try {
//            UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest();
//
//            updateProfileRequest.setName(name);
//            updateProfileRequest.setMail(email);
//            updateProfileRequest.setMobileNo(mobile_no);
//            updateProfileRequest.setDob(dob);
//
//            updateProfileRequest.setGender(gender);
//
//            UserDataApiProvider.getInstance(dbApplication).updateProfile(updateProfileRequest, new RetroApiListener<ProfileSuccessResponse>() {
//                @Override
//                public void onApiSuccess(@NonNull ProfileSuccessResponse response) {
//                    try {
//                        if (response.getData().getProfile() != null) {
//                            DBSharedPref dbSharedPref = OfferController.getInstance().getDbSharedPref();
//                            dbSharedPref.updateUserProfile(response.getData().getProfile());
//                        }
//                    } catch (Exception e) {
//                        Log.e(TAG, e.getMessage() + "");
//                        // DBToast.showFailToast(dbApplication, getString(R.string.alert_title_failed), getString(R.string.alert_api_failed));
//                    }
//                }
//
//                @Override
//                public void onApiFailed(@NonNull String message) {
//                    //DBToast.showFailToast(dbApplication, getString(R.string.alert_title_failed), getString(R.string.alert_api_failed));
//                }
//            });
//        } catch (Exception e) {
//            Log.e(TAG, e.getMessage() + "");
//        }
//    }

    /***
     *
     * @param file
     */
//    private void updateProfilePictureInBackground(@NonNull File file) {
//        try {
//            UserDataApiProvider.getInstance(dbApplication).updateProfilePicture(file, new RetroApiListener<ImageUploadSuccessResponse>() {
//                @Override
//                public void onApiSuccess(@NonNull ImageUploadSuccessResponse response) {
//                    try {
//                        if (response.getData().getUrl() != null) {
//                            DBSharedPref dbSharedPref = OfferController.getInstance().getDbSharedPref();
//                            Profile profile = dbSharedPref.getUserProfile();
//                            profile.setAvatar(response.getData().getUrl());
//                            dbSharedPref.updateUserProfile(profile);
//                        }
//                    } catch (Exception e) {
//                        Log.e(TAG, e.getMessage() + "");
//                    }
//                }
//
//                @Override
//                public void onApiFailed(@NonNull String message) {
//                }
//            });
//        } catch (Exception e) {
//            Log.e(TAG, e.getMessage() + "");
//        }
//    }

    //    // get profile service
//    private void getProfileService() {
//        if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
//            showAlertSnackBar(R.string.alert_network_not_exist);
//        } else {
//            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(getDbApplication()).create(TambolaAPIInterface.class);
//
//            Call<GetProfileModel> call = apiInterface.getProfile();
//
//            TransAviLoader pd = new TransAviLoader(this);
//            pd.show();
//
//            call.enqueue(new RetroCallbackProvider<GetProfileModel>() {
//
//                @Override
//                public void onSuccess(Call<GetProfileModel> call, @NonNull GetProfileModel getProfileModel) {
//                    try {
//                        Log.d("get profile", getProfileModel.message + "\n" + getProfileModel.data.name);
//
//                        getMobileNo = getProfileModel.data.mobile_no;
//                        mEtName.setText(getProfileModel.data.name);
//
//                        mEtMobileNumber.setText(getProfileModel.data.mobile_no);
//                        mEtDob.setText(getProfileModel.data.dob);
//
//                        Profile profile = dbSharedPref.getUserProfile();
//                        if (profile != null) {
//                            setGenderSpinnerAdapter(profile.getGender());
//                        }
//
//                        String emailAddress = getProfileModel.data.email;
//                        try {
//                            String connectFrom = profile.getConnectFrom().get(0);
//                            AppConstants.UserLoginType userLoginType = AppConstants.UserLoginType.getUserLoginType(connectFrom);
//                            if ((AppConstants.UserLoginType.LOGIN_FACEBOOK.equals(userLoginType) ||
//                                    AppConstants.UserLoginType.LOGIN_GOOGLE.equals(userLoginType)) && CommonUtils.isEmailValid(emailAddress)) {
//                                mEtEmailId.setEnabled(false);
//                                mEtEmailId.setClickable(false);
//                            }
//                        } catch (Exception e) {
//                            Log.e(TAG, e.getMessage() + "");
//                        }
//
//                        if (!TextUtils.isEmpty(emailAddress)) {
//                            mEtEmailId.setText(emailAddress);
//                        }
//
//                        mEtEmailId.setText(getProfileModel.data.email);
//
//
//                        if (!getProfileModel.data.photo.equals("")) {
//                            Glide.with(BingoUpdateProfileActivity.this)
//                                    .load(getProfileModel.data.photo)
//                                    .into(mCivProfileImage);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    } finally {
//                        pd.dismiss();
//                    }
//                }
//
//                @Override
//                public void onFailed(Call<GetProfileModel> call, Throwable t) {
//                    pd.dismiss();
//                }
//            });
//        }
//    }
}
