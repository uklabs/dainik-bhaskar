package com.db.offers.tambola.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.db.offers.R;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.TransAviLoader;
import com.db.offers.common.network.RetroCallbackProvider;
import com.db.offers.tambola.adapters.GameAdapter;
import com.db.offers.tambola.model.Game;
import com.db.offers.tambola.model.GameStatusResponseModel;
import com.db.offers.tambola.network.TambolaAPIClient;
import com.db.offers.tambola.network.TambolaAPIInterface;
import com.google.android.material.snackbar.Snackbar;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit2.Call;

public class ClaimStatusActivity extends BaseActivity {

    private RecyclerView recycler_list;
    private ArrayList<Game> statusList = new ArrayList<Game>();
    private GameAdapter gameAdapter;
    private Snackbar alertSnackBar;
    private TextView game_notice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambola_claim_status);
        initToolbar(getString(R.string.title_claim_status));
        initUI();
        processUI();
    }


    @Override
    protected void initUI() {
        recycler_list = findViewById(R.id.recycler_list);
        game_notice = findViewById(R.id.game_notice);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ClaimStatusActivity.this);
        gameAdapter = new GameAdapter(ClaimStatusActivity.this, statusList);
        recycler_list.setLayoutManager(layoutManager);
        recycler_list.setAdapter(gameAdapter);
    }

    @Override
    protected void processUI() {
        game_notice.setText(tambolaSharedPref.getGameStatusNotice());
        getGameStatus();
    }

    private void getGameStatus() {
        if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
            showAlertSnackBar(R.string.alert_network_not_exist);
        } else {
            TransAviLoader dialog = new TransAviLoader(this);
            dialog.show();
            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(this).create(TambolaAPIInterface.class);
            Call<GameStatusResponseModel> call = apiInterface.doGetClaimedList();
            call.enqueue(new RetroCallbackProvider<GameStatusResponseModel>() {

                @Override
                public void onSuccess(Call<GameStatusResponseModel> call, GameStatusResponseModel gameStatusResponseModel) {
                    try {
                        statusList.clear();
                        Log.e("winner respone", gameStatusResponseModel.message);
                        for (int i = 0; i < gameStatusResponseModel.data.Winners.size(); i++) {
                            Game game = new Game();
                            String name = gameStatusResponseModel.data.Winners.get(i).name;
                            String claimed = gameStatusResponseModel.data.Winners.get(i).claimed;
                            String available = gameStatusResponseModel.data.Winners.get(i).available;

                            game.setName(name);
                            game.setClaimed(claimed);
                            game.setAvailiable(available);
                            statusList.add(game);
                            Log.e("name", name);
                            Log.e("claimed", claimed);
                            Log.e("available", available);
                        }
                        gameAdapter.notifyItemRangeInserted(0, statusList.size());
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                        CommonUtils.showToast(getApplicationContext(), getString(R.string.alert_api_failed));
                    } finally {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailed(Call<GameStatusResponseModel> call, Throwable t) {
                    CommonUtils.showToast(getApplicationContext(), getString(R.string.alert_api_failed));
                    dialog.dismiss();
                }
            });
        }
    }


    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {

    }

    /**
     * @param alertStringResId
     */
    private void showAlertSnackBar(@StringRes int alertStringResId) {
        if (null == alertSnackBar) {
            View view = findViewById(R.id.cl_parent);
            alertSnackBar = Snackbar
                    .make(view, alertStringResId, Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.label_okey), view1 -> alertSnackBar.dismiss());
            TextView tv = alertSnackBar.getView().findViewById(R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
        } else {
            alertSnackBar.setText(alertStringResId);
        }
        alertSnackBar.show();
    }


}
