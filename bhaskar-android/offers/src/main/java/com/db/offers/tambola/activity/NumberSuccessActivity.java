package com.db.offers.tambola.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.db.offers.OfferController;
import com.db.offers.R;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.db.offers.common.SharedPrefUtil;
import com.db.offers.tambola.constant.TambolaConstants;
import com.bhaskar.appscommon.tracking.Tracking;

import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

public final class NumberSuccessActivity extends BaseActivity {

    private Toolbar toolbar;
    private Button btnOK;
    private TextView number_one, number_two, number_three;
    private MediaPlayer mPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambola_todays_number);
        //disable swipe to dismiss
        initToolbar(getString(R.string.title_today_number));
        initUI();
        processUI();
//        CTTracker.cleverTapTrackPageView(getApplicationContext(), CTConstant.PROP_VALUE.Tambola.SCREEN_TODAY_NUMBER,
//                CTConstant.PROP_VALUE.TAMBOLA);
        Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.TAMBOLA + "-" + CTConstant.PROP_VALUE.Tambola.SCREEN_TODAY_NUMBER, "", "", "");
    }

    @Override
    protected void initUI() {
        toolbar = findViewById(R.id.toolbar);
        btnOK = findViewById(R.id.btnOK);
        number_one = findViewById(R.id.number_one);
        number_two = findViewById(R.id.number_two);
        number_three = findViewById(R.id.number_three);
    }

    @Override
    protected void processUI() {
        try {

            final KonfettiView konfettiView = findViewById(R.id.konfettiView);
            konfettiView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    konfettiView.build()
                            .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
                            .setDirection(0.0, 359.0)
                            .setSpeed(1f, 5f)
                            .setFadeOutEnabled(true)
                            .setTimeToLive(2000L)
                            .addShapes(Shape.RECT, Shape.CIRCLE)
                            .addSizes(new Size(12, 5f))
                            .setPosition(-50f, 1000 + 50f, -50f, -50f)
                            .streamFor(300, 5000L);
                }
            });

            konfettiView.callOnClick();
            SharedPreferences todays_number_sharedPreferences = getSharedPreferences("todays_number", Context.MODE_PRIVATE);
            //String code = SharedPrefUtil.getString(todays_number_sharedPreferences, "code", "");
            String number = SharedPrefUtil.getString(todays_number_sharedPreferences, "number", "");

            Bundle bundle = getIntent().getExtras();

            if (bundle == null) {
                Toast.makeText(getApplicationContext(), R.string.alert_today_no_not_found, Toast.LENGTH_LONG).show();
                finish();
                return;
            }

            String todayNumber = bundle.getString(TambolaConstants.GetBundleKeys.KEY_TODAY_NUMBER);

            String arr[] = todayNumber.split(",");
            // Toast.makeText(this, String.valueOf(arr.length) + "\n" + number, Toast.LENGTH_SHORT).show();

            for (int index = 0; index < arr.length; index++) {
                if (index == 0) {
                    number_one.setText(arr[index]);
                    number_one.setVisibility(View.VISIBLE);
                } else if (index == 1) {
                    number_two.setText(arr[index]);
                    number_two.setVisibility(View.VISIBLE);
                } else if (index == 2) {
                    number_three.setText(arr[index]);
                    number_three.setVisibility(View.VISIBLE);
                }
            }

            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDashboardAgain();
                }
            });
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDashboardAgain();
                }
            });
            playSound();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }

    public static void startActivity(Activity context, Bundle bundle) {
        Intent intent = new Intent(context, NumberSuccessActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }

    @Override
    public void networkAvailable() {
    }

    @Override
    public void networkUnavailable() {
    }

    void playSound() {
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.reset();
        }
        if (!isFinishing()) {
            mPlayer = MediaPlayer.create(NumberSuccessActivity.this, R.raw.tambola_bubble);
            mPlayer.setVolume(1.0f, 1.0f);
            mPlayer.start();
        }
    }

    @Override
    public void onBackPressed() {
        showDashboardAgain();
    }

    private void showDashboardAgain() {
        /*SharedPreferences sharedPreferences = getSharedPreferences("scan_qr_message", Context.MODE_PRIVATE);
        SharedPrefUtil.addString(sharedPreferences, "code_match", "1");*/

        /*setResult(Activity.RESULT_OK);*/
        try {
            if (mPlayer != null) {
                mPlayer.release();
            }
        } catch (Exception e) {

        }

        setResult(Activity.RESULT_OK);
        finish();
    }

    /**
     * @param context
     * @param bundle
     */
    public static void startActivityForResult(Activity context, Bundle bundle, int requestCode) {
        Intent intent = new Intent(context, NumberSuccessActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivityForResult(intent, requestCode);
    }
}
