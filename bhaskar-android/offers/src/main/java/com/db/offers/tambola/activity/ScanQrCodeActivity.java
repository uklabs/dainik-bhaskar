package com.db.offers.tambola.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;

import com.chaos.view.PinView;
import com.db.offers.OfferController;
import com.db.offers.R;
import com.db.offers.tambola.utils.Utils;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.SharedPrefUtil;
import com.db.offers.tambola.constant.TambolaConstants;
import com.bhaskar.appscommon.tracking.Tracking;
import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import java.util.HashMap;
import java.util.List;

public final class ScanQrCodeActivity extends BaseActivity {
    private Toolbar toolbar;
    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;
//    private EditText mEtCode;
    private PinView pinView;
    private TextView tv_or, todayCodeTv;
    private Button btnProceed;
    private Unregistrar mUnregistrar;
//    private ImageView edt_cancel;
    private Snackbar alertSnackBar;
    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambola_scan_qrcode);
        todayCodeTv = findViewById(R.id.tv_today_code);

        bundle = getIntent().getExtras();

        if (bundle == null) {
            Toast.makeText(getApplicationContext(), "Today Number Not Found", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        initToolbar(getString(R.string.title_qr_scanner));
        initUI();
        processUI();
//        CTTracker.cleverTapTrackPageView(getApplicationContext(), CTConstant.PROP_VALUE.Tambola.SCREEN_SCANNER,
        //              CTConstant.PROP_VALUE.TAMBOLA);
        Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.TAMBOLA + "-" + CTConstant.PROP_VALUE.Tambola.SCREEN_SCANNER, "", "", "");

    }

    @Override
    protected void initUI() {
        toolbar = findViewById(R.id.toolbar);
        barcodeScannerView = findViewById(R.id.zxing_barcode_scanner);
        tv_or = findViewById(R.id.tv_or);
        pinView = findViewById(R.id.pinView);
        btnProceed = findViewById(R.id.btnProceed);
        capture = new CaptureManager(this, barcodeScannerView);
    }

    @Override
    protected void processUI() {
        barcodeScannerView.decodeSingle(barcodeCallback);
        String todayCode = bundle.getString(TambolaConstants.GetBundleKeys.KEY_TODAY_CODE);
        todayCodeTv.setText(todayCode);

          /*
          You can also use {@link KeyboardVisibilityEvent#setEventListener(Activity, KeyboardVisibilityEventListener)}
          if you don't want to unregister the event manually.
         */
        mUnregistrar = KeyboardVisibilityEvent.registerEventListener(this, new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                updateKeyboardStatusText(isOpen);
            }
        });

        updateKeyboardStatusText(KeyboardVisibilityEvent.isKeyboardVisible(this));

        //edit text cancel button listener
       /* edt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIUtil.hideKeyboard(ScanQrCodeActivity.this);
                barcodeScannerView.setVisibility(View.VISIBLE);
                tv_or.setVisibility(View.VISIBLE);
                btnProceed.setVisibility(View.GONE);
                edt_cancel.setVisibility(View.GONE);
                pinView.setText("");
            }
        });*/

        // proceed button listener
        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = getSharedPreferences("todays_number", Context.MODE_PRIVATE);
                String code = SharedPrefUtil.getString(sharedPreferences, "code", "");
                //Code match

                if (pinView.getText().toString().isEmpty()) {
                    pinView.setError(getResources().getString(R.string.err_msg_enter_code));
                    pinView.requestFocus();
                } else {
                    if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
                        showAlertSnackBar(R.string.alert_network_not_exist);
                    } else {
                        showTodayNumberScreenForSuccess(pinView.getText().toString(), false);
                    }
                }
            }
        });

        pinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                pinView.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        pinView.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                Log.i(TAG, "Enter pressed");

                SharedPreferences sharedPreferences = getSharedPreferences("todays_number", Context.MODE_PRIVATE);
                String code = SharedPrefUtil.getString(sharedPreferences, "code", "");
                //Code match

                if (pinView.getText().toString().isEmpty()) {
                    pinView.setError(getResources().getString(R.string.err_msg_enter_code));
                    pinView.requestFocus();
                } else {

                    if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
                        showAlertSnackBar(R.string.alert_network_not_exist);
                    } else {
                        showTodayNumberScreenForSuccess(pinView.getText().toString(), false);
                    }
                }
            }
            return false;
        });

        toolbar.setNavigationOnClickListener(v -> finish());
        //scanQRService();
    }

    private void updateKeyboardStatusText(boolean isOpen) {
        String keyboardValue = String.format((isOpen ? "visible" : "hidden"));

        if (keyboardValue.equals("hidden")) {
            // keyboard close here
            barcodeScannerView.setVisibility(View.VISIBLE);
            tv_or.setVisibility(View.VISIBLE);
            btnProceed.setVisibility(View.GONE);
//            edt_cancel.setVisibility(View.GONE);
        }
        if (keyboardValue.equals("visible")) {
            // keyboard open here
            barcodeScannerView.setVisibility(View.GONE);
            tv_or.setVisibility(View.GONE);
            btnProceed.setVisibility(View.VISIBLE);
//            edt_cancel.setVisibility(View.VISIBLE);
        }
    }

    private BarcodeCallback barcodeCallback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if (result.getText() != null) {

                if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
                    showAlertSnackBar(R.string.alert_network_not_exist);
                } else {
                    pinView.setText(result.getText() + "");
                    SharedPreferences sharedPreferences = getSharedPreferences("todays_number", Context.MODE_PRIVATE);
                    String code = SharedPrefUtil.getString(sharedPreferences, "code", "");
                    //Code match
                    showTodayNumberScreenForSuccess(result.getText(), true);
                }
            } else {
                // ADD GA Event here
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };


    /***
     *
     * @param scannedCode
     */
    private void showTodayNumberScreenForSuccess(String scannedCode, boolean isFromScanning) {
        String todayCode = bundle.getString(TambolaConstants.GetBundleKeys.KEY_TODAY_CODE);
        HashMap<String,Object> userDatamap = Utils.getUserDataMap();
        if (scannedCode.equals(todayCode)) {
            // scanQRService();
            /*//Code does not match
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);
            String formattedDate = simpleDateFormat.format(c);

            SharedPreferences scan_qr_message_sharedPreferences = getSharedPreferences("scan_qr_message", Context.MODE_PRIVATE);
            SharedPrefUtil.addString(scan_qr_message_sharedPreferences, "scan_success", "Success");
            SharedPrefUtil.addString(scan_qr_message_sharedPreferences, "scan_date", formattedDate);*/
            if (isFromScanning) {
//                CTTracker.cleverTapTrackEvent(getDbApplication(),
//                        CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.Tambola.QR_SCAN, CTConstant.PROP_VALUE.SUCCESS);
                Tracking.trackGAEvent(ScanQrCodeActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.Tambola.QR_SCAN, CTConstant.PROP_VALUE.SUCCESS, "",userDatamap);

            } else {
//                CTTracker.cleverTapTrackEvent(getDbApplication(),
//                        CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.Tambola.QR_CODE, CTConstant.PROP_VALUE.SUCCESS);
                Tracking.trackGAEvent(ScanQrCodeActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.Tambola.QR_CODE, CTConstant.PROP_VALUE.SUCCESS, "",userDatamap);
            }
            saveScannedDate();
            NumberSuccessActivity.startActivityForResult(ScanQrCodeActivity.this, bundle, TambolaConstants.RC_SHOW_NUMBER_CODE);
        } else {
            if (isFromScanning) {

//                CTTracker.cleverTapTrackEvent(getDbApplication(),
//                        CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.Tambola.QR_SCAN, CTConstant.PROP_VALUE.FAILED);
                Tracking.trackGAEvent(ScanQrCodeActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.Tambola.QR_SCAN, CTConstant.PROP_VALUE.FAILED, "",userDatamap);

            } else {
//                CTTracker.cleverTapTrackEvent(getDbApplication(),
//                        CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.Tambola.QR_CODE, CTConstant.PROP_VALUE.FAILED);
                Tracking.trackGAEvent(ScanQrCodeActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.Tambola.QR_CODE, CTConstant.PROP_VALUE.FAILED, "",userDatamap);
            }
            alertPopUp();
        }
    }

    private void saveScannedDate() {
        String formattedDate = getFormattedCurrentDate();
        Log.d(TAG, "saveScannedDate: " + formattedDate);
        tambolaSharedPref.saveScannedDate(formattedDate);
    }

    // alert popup when number code does not match
    private void alertPopUp() {
        final Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        builder.setContentView(R.layout.diaog_tambola_scan_qr);
        builder.setCancelable(false);
        if (!isFinishing()) {
            builder.show();
        }
        RelativeLayout allow_action = builder.findViewById(R.id.allow_action);
        TextView tv_msg = builder.findViewById(R.id.tv_msg);
        tv_msg.setText(R.string.alert_not_a_valid_code);
        allow_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.dismiss();
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
        mUnregistrar.unregister();
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {

    }

    /**
     * @param alertStringResId
     */
    private void showAlertSnackBar(@StringRes int alertStringResId) {
        if (null == alertSnackBar) {
            View view = findViewById(R.id.cl_parent);
            alertSnackBar = Snackbar
                    .make(view, alertStringResId, Snackbar.LENGTH_LONG)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertSnackBar.dismiss();
                        }
                    });
            TextView tv = alertSnackBar.getView().findViewById(R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
        } else {
            alertSnackBar.setText(alertStringResId);
        }
        alertSnackBar.show();
    }

    /**
     * @param context
     * @param bundle
     */
    public static void startActivityForResult(Activity context, Bundle bundle, int requestCode) {
        Intent intent = new Intent(context, ScanQrCodeActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TambolaConstants.RC_SHOW_NUMBER_CODE) {
            setResult(resultCode, getIntent());
            finish();
        }
    }
}
