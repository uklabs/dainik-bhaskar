package com.db.offers.tambola.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.db.offers.BuildConfig;
import com.db.offers.OfferController;
import com.db.offers.R;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.EasyDialogUtils;
import com.db.offers.common.UrlUtil;
import com.db.offers.common.network.RetroCallbackProvider;
import com.db.offers.tambola.adapters.DashboardTicketAdapter;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.dialogs.ApiLoadingDialog;
import com.db.offers.tambola.dialogs.ApiLoadingInterface;
import com.db.offers.tambola.model.DataModel;
import com.db.offers.tambola.model.EligibilityStatus;
import com.db.offers.tambola.model.GetTicketModel;
import com.db.offers.tambola.model.ScanQRModel;
import com.db.offers.tambola.model.ScanResultModel;
import com.db.offers.tambola.model.SettingResponseModel;
import com.db.offers.tambola.model.TodayNumberModel;
import com.db.offers.tambola.network.TambolaAPIClient;
import com.db.offers.tambola.network.TambolaAPIInterface;
import com.db.offers.tambola.utils.ApiMessageDialog;
import com.db.offers.tambola.utils.TambolaQuestionCallback;
import com.db.offers.tambola.utils.Utils;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class TambolaDashboardActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "TambolaDashboard";
    public List<String> ticketList = new ArrayList<>();
    private Snackbar alertSnackBar;
    private RecyclerView ticketRecyclerView;
    private TextView ftv_early5, ftv_middle_line, ftv_4_corner, ftv_bottom_line, ftv_top_line, ftv_full_house, winnerBtn;
    private List<String> todayNumberList = new ArrayList<>();
    private Set<String> optedNumberList = new HashSet<>();
    private TextView number_one, number_two, number_three, tv_ticket_id, scanToViewLabelTv;
    private DashboardTicketAdapter ticketAdapter;
    private final RetroCallbackProvider<GetTicketModel> getTicketApiCallback = new RetroCallbackProvider<GetTicketModel>() {
        @Override
        public void onSuccess(Call<GetTicketModel> call, GetTicketModel getTicketModel) {

            if (!isFinishing()) {
                try {
                    int size = ticketList.size();
                    ticketList.clear();
                    ticketAdapter.notifyItemRangeRemoved(0, size);

                    DataModel dataModel = getTicketModel.data;
                    Log.d(TAG, "dataModel: " + dataModel);


                    if (null != dataModel.opted_numbers && !dataModel.opted_numbers.isEmpty()) {
                        // Log.d("opted_numbers arr", getTicketModel.data.opted_numbers.get(i));
                        optedNumberList.addAll(dataModel.opted_numbers);
                    } else {
                        optedNumberList.clear();
                    }
                    setUserEligibilityStatus(dataModel.eligibilityStatus);

                    winnerBtn.setTag(dataModel.winnerList);

                    JSONObject object = new JSONObject(dataModel.ticket_details);
                    for (int index = 0; index < TambolaConstants.TICKET_NO_OF_ROWS; index++) {
                        for (int jIndex = 0; jIndex < TambolaConstants.TICKET_NO_OF_COLUMNS; jIndex++) {
                            JSONObject jsonObject = object.getJSONObject(jIndex + "");
                            String ticket = jsonObject.getString(index + "");
                            ticketList.add(ticket);
                        }
                    }

                    ticketAdapter.notifyItemRangeInserted(0, ticketList.size());
                    ticketAdapter.notifyDataSetChanged();
                    getTicketApiLoadingDialog.dismiss();
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage() + "");
                    doOnApiFailed();
                }
            }
        }

        private void doOnApiFailed() {
            getTicketApiLoadingDialog.handleApiFailure();
        }

        @Override
        public void onFailed(Call<GetTicketModel> call, Throwable t) {
            Log.e(TAG, t.getMessage() + "");
            doOnApiFailed();
        }
    };
    private TodayNumberModel todayNumberModel;
    private boolean isUserLeaved = false;
    private ApiLoadingDialog scanCodeApiLoadingDialog, getTicketApiLoadingDialog;
    private final Callback<ScanResultModel> scanCodeApiCallback = new Callback<ScanResultModel>() {
        @Override
        public void onResponse(@NonNull Call<ScanResultModel> call, @NonNull Response<ScanResultModel> response) {
            try {
                ScanResultModel scanResultModel = response.body();
                showTodayNumbersOnDashboard(true);
                if (scanResultModel.getData().getEligibilityStatus() != null) {
                    setUserEligibilityStatus(scanResultModel.getData().getEligibilityStatus());
                }

                setTicketBoardAgainForScanSuccess();
                congratulationsPopUp();
                scanCodeApiLoadingDialog.dismiss();
            } catch (Exception e) {
                doOnApiFailed();
            }
        }

        @Override
        public void onFailure(Call<ScanResultModel> call, Throwable t) {
            doOnApiFailed();
        }

        private void doOnApiFailed() {
            scanCodeApiLoadingDialog.handleApiFailure();
        }
    };

    /**
     * @param context
     * @param bundle
     */
    public static void startActivity(Activity context, Bundle bundle) {
        Intent intent = new Intent(context, TambolaDashboardActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isUserLeaved = false;
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_tambola_dashboard);
        initToolbar("");
        initUI();

        ticketRecyclerView.setLayoutManager(new GridLayoutManager(TambolaDashboardActivity.this, TambolaConstants.TICKET_NO_OF_COLUMNS));

        ticketAdapter = new DashboardTicketAdapter(TambolaDashboardActivity.this, ticketList, todayNumberList, optedNumberList);
        ticketRecyclerView.setAdapter(ticketAdapter);

        processUI();
    }

    @Override
    protected void initUI() {
        TextView toolbarTitleTv = findViewById(R.id.toolbar_text);
        toolbarTitleTv.setText(R.string.title_db_tambola);

        winnerBtn = findViewById(R.id.card_winner_dashboard);
        winnerBtn.setOnClickListener(this);
        findViewById(R.id.ib_scan_code).setOnClickListener(this);
        findViewById(R.id.card_all_numbers).setOnClickListener(this);
        findViewById(R.id.card_my_info).setOnClickListener(this);
        findViewById(R.id.card_claim_status).setOnClickListener(this);
        findViewById(R.id.card_game_rules).setOnClickListener(this);
        findViewById(R.id.card_help).setOnClickListener(this);
        findViewById(R.id.invite_friends).setOnClickListener(this);
        findViewById(R.id.back_arrow).setOnClickListener(this);
        findViewById(R.id.tvHowToPlay).setOnClickListener(this);

        ticketRecyclerView = findViewById(R.id.rv_ticket);
//        mTopAdview = findViewById(R.id.fl_top_adview);
        ftv_early5 = findViewById(R.id.ftv_early5);
        ftv_middle_line = findViewById(R.id.ftv_middle_line);
        ftv_4_corner = findViewById(R.id.ftv_4_corner);
        ftv_bottom_line = findViewById(R.id.ftv_bottom_line);
        ftv_top_line = findViewById(R.id.ftv_top_line);
        ftv_full_house = findViewById(R.id.ftv_full_house);
        number_one = findViewById(R.id.number_one);
        number_two = findViewById(R.id.number_two);
        number_three = findViewById(R.id.number_three);
        tv_ticket_id = findViewById(R.id.tv_ticket_id);
        scanToViewLabelTv = findViewById(R.id.tv_scan_to_view);
    }

    @Override
    protected void processUI() {
        String ticketId = tambolaSharedPref.getTicketId();//6270
        tv_ticket_id.setText(getString(R.string.label_ticket_id) + ticketId);
        getTicketService();
        getGetTodayNumberService();
        getGetSettingService();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void networkAvailable() {
        //Add Snack Bar
        alertSnackBar.dismiss();
    }

    @Override
    public void networkUnavailable() {
        showAlertSnackBar(R.string.alert_network_not_exist);
    }

    void onHowToPlay() {
        String howToPlayLink = UrlUtil.getTambolaWebBaseUrl() + "terms_faq/how_to_play.html";
        List<String> whiteListWebUrls = getWhitListWebUrls();
        if (Utils.isWebUrlDBInternal(howToPlayLink, whiteListWebUrls)) {
            Bundle bundle = new Bundle();
            bundle.putString(TambolaConstants.GetBundleKeys.KEY_WEB_URL, howToPlayLink);
            bundle.putString(TambolaConstants.GetBundleKeys.KEY_SCREEN_TYPE, getString(R.string.label_how_to_play));
            TambolaWebViewActivity.startActivity(TambolaDashboardActivity.this, bundle);
        } else {
            CommonUtils.openUrlOnGoogleChromeTab(TambolaDashboardActivity.this, howToPlayLink);
        }

    }

    @Override
    public void onClick(View view) {
        isUserLeaved = false;
        HashMap<String, Object> userDataMap = Utils.getUserDataMap();
        if (view.getId() == R.id.card_winner_dashboard && view.getTag() != null) {
            String webUrl = view.getTag().toString();
            Bundle bundle = new Bundle();
            bundle.putString(TambolaConstants.GetBundleKeys.KEY_WEB_URL, webUrl);
            bundle.putString(TambolaConstants.GetBundleKeys.KEY_SCREEN_TYPE, TambolaConstants.WINNER_TYPE_WEB);
            TambolaWebViewActivity.startActivity(this, bundle);

            Tracking.trackGAEvent(this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.WINNERS, "", userDataMap);
            Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.TAMBOLA + "-" + CTConstant.PROP_VALUE.Tambola.SCREEN_WINNERS, "", "", "");

        } else if (view.getId() == R.id.ib_scan_code) {
            // Check tambola launch status
            checkTambolaLaunchStatus();
        } else if (view.getId() == R.id.card_my_info) {
            BingoUpdateProfileActivity.startActivity(TambolaDashboardActivity.this, null);
            Tracking.trackGAEvent(TambolaDashboardActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.MYINFO, "", userDataMap);

        } else if (view.getId() == R.id.card_all_numbers) {
            TicketBoardActivity.startActivity(TambolaDashboardActivity.this, null);
            Tracking.trackGAEvent(TambolaDashboardActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.ALL_NUMBERS, "", userDataMap);

        } else if (view.getId() == R.id.card_claim_status) {
            Intent intent = new Intent(TambolaDashboardActivity.this, ClaimStatusActivity.class);
            startActivity(intent);
            Tracking.trackGAEvent(TambolaDashboardActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.CLAIM_STATUS, "", userDataMap);

        } else if (view.getId() == R.id.card_game_rules) {
            if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
                showAlertSnackBar(R.string.alert_network_not_exist);
            } else {
                String webUrl = UrlUtil.getTambolaGameRulesUrl();
                Bundle bundle = new Bundle();
                bundle.putString(TambolaConstants.GetBundleKeys.KEY_WEB_URL, webUrl);
                bundle.putString(TambolaConstants.GetBundleKeys.KEY_SCREEN_TYPE, TambolaConstants.SCREEN_GAME_RULES);
                TambolaWebViewActivity.startActivity(this, bundle);

//                CTTracker.cleverTapTrackEvent(getDbApplication(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.GAME_RULES);
                Tracking.trackGAEvent(TambolaDashboardActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.GAME_RULES, "", userDataMap);
//
//                CTTracker.cleverTapTrackPageView(getApplicationContext(), CTConstant.PROP_VALUE.Tambola.SCREEN_GAME_RULES,
//                        CTConstant.PROP_VALUE.TAMBOLA);
                Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.TAMBOLA + "-" + CTConstant.PROP_VALUE.Tambola.SCREEN_GAME_RULES, "", "", "");

            }
        } else if (view.getId() == R.id.card_help) {
            onHelp(userDataMap);
        }else if (view.getId() == R.id.back_arrow) {
            finish();
        } else if (view.getId() == R.id.invite_friends) {
            if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
                showAlertSnackBar(R.string.alert_network_not_exist);
            } else {
                try {
                    Spanned text = Html.fromHtml(tambolaSharedPref.getShareText());

                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, text.toString());
                    Intent shareCIntent = Intent.createChooser(shareIntent, getString(R.string.share_via));

                    if (shareCIntent != null && CommonUtils.isDeviceHaveAnySupportedAppForIntent(shareCIntent, getApplicationContext())) {
                        startActivity(shareCIntent);

//                        CTTracker.cleverTapTrackEvent(getDbApplication(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.INVITE_FRIENDS);
                        Tracking.trackGAEvent(TambolaDashboardActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.INVITE_FRIENDS, "", userDataMap);

                    } else {
                        startActivity(shareIntent);

//                        CTTracker.cleverTapTrackEvent(getDbApplication(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.INVITE_FRIENDS);
                        Tracking.trackGAEvent(TambolaDashboardActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.INVITE_FRIENDS, "", userDataMap);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage() + "");
                }
            }
        } else if(R.id.tvHowToPlay == view.getId()){
            onHowToPlay();
        }
    }

    private void onHelp(HashMap<String, Object> userDataMap) {
        String webUrl = "https://www.divyabhaskar.co.in/app-feedback?type=11";
        Bundle bundle = new Bundle();
        bundle.putString(TambolaConstants.GetBundleKeys.KEY_WEB_URL, webUrl);
        bundle.putString(TambolaConstants.GetBundleKeys.KEY_SCREEN_TYPE, TambolaConstants.SCREEN_GAME_RULES);
        TambolaWebViewActivity.startActivity(this, bundle);

        Tracking.trackGAEvent(TambolaDashboardActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.GAME_HELP_LINE, "", userDataMap);
        Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.TAMBOLA + "-" + CTConstant.PROP_VALUE.Tambola.SCREEN_HELP_LINE, "", "", "");

    }

    private void checkTambolaLaunchStatus() {
        if (null != todayNumberModel && !todayNumberModel.tambolaLaunchStatus) {
            EasyDialogUtils.showInfoDialog(TambolaDashboardActivity.this, getString(R.string.db_tambola_app_name), todayNumberModel.message);
        } else if (null != todayNumberModel && null != todayNumberModel.data && null != todayNumberModel.data.code) {
            int attemptCode = tambolaSharedPref.getAttemptedQuestionCode();
            if (attemptCode == 0) { // 0 = Not attempted
                new TambolaQuestionDialog(this, new TambolaQuestionCallback() {
                    @Override
                    public void onAnswerCorrect() {
                        tambolaSharedPref.saveAttemptedQuestionCode(1);
                        launchScanQRCodeScanner(todayNumberModel.data);
                    }

                    @Override
                    public void onAnswerInCorrect() {
                        tambolaSharedPref.saveAttemptedQuestionCode(2);
                    }

                    @Override
                    public void onNotAnswered() {
                        tambolaSharedPref.saveAttemptedQuestionCode(0);
                    }
                }).show();
            } else if (attemptCode == 1) { // 1 = Attempted with correct answer
                launchScanQRCodeScanner(todayNumberModel.data);
            } else {// 2 = Attempted with wrong answer
                finish();
            }
        } else {
            EasyDialogUtils.showInfoDialog(TambolaDashboardActivity.this, getString(R.string.db_tambola_app_name), getString(R.string.alert_today_no_not_avail_to_scan));
        }
    }

    /**
     * @param data
     */
    private void launchScanQRCodeScanner(TodayNumberModel.Data data) {
        Bundle bundle = new Bundle();
        bundle.putString(TambolaConstants.GetBundleKeys.KEY_TODAY_NUMBER, data.numbers);
        bundle.putString(TambolaConstants.GetBundleKeys.KEY_TODAY_CODE, data.code);

        ScanQrCodeActivity.startActivityForResult(TambolaDashboardActivity.this, bundle, TambolaConstants.RC_SCAN_CODE);
        // Track CleverTap event
        Tracking.trackGAEvent(TambolaDashboardActivity.this, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.SCAN_QR, "", Utils.getUserDataMap());

    }

    /**
     * @param status
     */
    private void setUserEligibilityStatus(EligibilityStatus status) {

        try {
            if (status.getEarlyFive().equals("0")) {
                ftv_early5.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_black_24dp_tambola, 0);
            } else {
                ftv_early5.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp_tambola, 0);
            }
            if (status.getCorners().equals("0")) {
                ftv_4_corner.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_black_24dp_tambola, 0);
            } else {
                ftv_4_corner.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp_tambola, 0);
            }
            if (status.getTopLine().equals("0")) {
                ftv_top_line.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_black_24dp_tambola, 0);
            } else {
                ftv_top_line.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp_tambola, 0);
            }
            if (status.getMiddleLine().equals("0")) {
                ftv_middle_line.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_black_24dp_tambola, 0);
            } else {
                ftv_middle_line.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp_tambola, 0);
            }
            if (status.getBottomLine().equals("0")) {
                ftv_bottom_line.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_black_24dp_tambola, 0);
            } else {
                ftv_bottom_line.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp_tambola, 0);
            }
            if (status.getFullHouse().equals("0")) {
                ftv_full_house.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_black_24dp_tambola, 0);
            } else {
                ftv_full_house.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp_tambola, 0);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }

    private void getGetTodayNumberService() {
        if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
            showAlertSnackBar(R.string.alert_network_not_exist);
        } else {
            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(this).
                    create(TambolaAPIInterface.class);

            Call<TodayNumberModel> call = apiInterface.doGetTodayNumber();

            call.enqueue(new Callback<TodayNumberModel>() {
                @Override
                public void onResponse(@NonNull Call<TodayNumberModel> call,
                                       @NonNull Response<TodayNumberModel> response) {
                    if (!isFinishing()) {
                        try {
                            todayNumberModel = response.body();

                            Log.d(TAG, "TodayNumberModel: " + todayNumberModel);

                            if (todayNumberModel != null && todayNumberModel.data != null && todayNumberModel.data.code != null &&
                                    todayNumberModel.data.numbers != null) {

                                String arr[] = todayNumberModel.data.numbers.split(",");
                                todayNumberList.clear();
                                Collections.addAll(todayNumberList, arr);

                                String savedScannedDate = tambolaSharedPref.getSavedScannedDate();

                                if (!TextUtils.isEmpty(savedScannedDate) && savedScannedDate.equals(getFormattedCurrentDate())) {
                                    showTodayNumbersOnDashboard(true);
                                } else {
                                    scanToViewLabelTv.setVisibility(View.VISIBLE);
                                }
                            } else {
                                // Do not show any message here
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(TAG, e.getMessage() + "");
                        }
                    }
                }

                @Override
                public void onFailure(Call<TodayNumberModel> call, Throwable t) {
                    //  progressDialog.dismiss();
                    CommonUtils.showToast(getApplicationContext(), getString(R.string.alert_api_failed));
                }
            });
        }
    }

    /***
     *
     * @param isTodayNoShownOnScreen
     */
    private void showTodayNumbersOnDashboard(boolean isTodayNoShownOnScreen) {
        if (isTodayNoShownOnScreen) {

            if (!todayNumberList.isEmpty()) {
                scanToViewLabelTv.setVisibility(View.GONE);
            }
            if (todayNumberList.size() == 1) {
                number_one.setText(todayNumberList.get(0));
                number_one.setVisibility(View.VISIBLE);
            } else if (todayNumberList.size() == 2) {
                number_one.setText(todayNumberList.get(0));
                number_two.setText(todayNumberList.get(1));
                number_one.setVisibility(View.VISIBLE);
                number_two.setVisibility(View.VISIBLE);
            } else if (todayNumberList.size() == 3) {
                number_one.setText(todayNumberList.get(0));
                number_two.setText(todayNumberList.get(1));
                number_three.setText(todayNumberList.get(2));
                number_one.setVisibility(View.VISIBLE);
                number_two.setVisibility(View.VISIBLE);
                number_three.setVisibility(View.VISIBLE);
            }
        } else {
            number_one.setText("");
            number_two.setText("");
            number_three.setText("");
            number_one.setVisibility(View.GONE);
            number_two.setVisibility(View.GONE);
            number_three.setVisibility(View.GONE);
            scanToViewLabelTv.setVisibility(View.VISIBLE);
        }
    }

    private void getGetSettingService() {

        if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
            showAlertSnackBar(R.string.alert_network_not_exist);
        } else {
            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(this).
                    create(TambolaAPIInterface.class);

            Call<SettingResponseModel> call = apiInterface.doGetSettingData();

            call.enqueue(new Callback<SettingResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<SettingResponseModel> call, @NonNull Response<SettingResponseModel> response) {
                    if (!isFinishing()) {
                        try {
                            SettingResponseModel settingResponseModel = response.body();

                            if (null != settingResponseModel && null != settingResponseModel.data) {
                                tambolaSharedPref.saveShareText(settingResponseModel.data.get(0).value);
                                tambolaSharedPref.saveWinnerNotice(settingResponseModel.data.get(1).value);
                                tambolaSharedPref.saveGameStatusNotice(settingResponseModel.data.get(2).value);
                            } else {
                                CommonUtils.showToast(getApplicationContext(), getString(R.string.alert_api_failed));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            CommonUtils.showToast(getApplicationContext(), getString(R.string.alert_api_failed));
                        }
                    }
                }

                @Override
                public void onFailure(Call<SettingResponseModel> call, Throwable t) {
                    //  progressDialog.dismiss();
                    CommonUtils.showToast(getApplicationContext(), getString(R.string.alert_api_failed));
                }
            });
        }
    }

    private void getTicketService() {
        // Toast.makeText(this, ticket_id, Toast.LENGTH_SHORT).show();

        if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
            showAlertSnackBar(R.string.alert_network_not_exist);
        } else {
            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(this)
                    .create(TambolaAPIInterface.class);

            final Call<GetTicketModel> call = apiInterface.doGetTicketList();

            getTicketApiLoadingDialog = new ApiLoadingDialog(TambolaDashboardActivity.this, new ApiLoadingInterface() {
                @Override
                public void makeApiCall() {
                    call.enqueue(getTicketApiCallback);
                }

                @Override
                public void makeApiCallAgain() {
                    call.clone().enqueue(getTicketApiCallback);
                }

                @Override
                public void onPermanentFailure() {
                    new ApiMessageDialog(TambolaDashboardActivity.this, getString(R.string.alert_title_failed), getString(R.string.alert_request_unable_to_process) + "\n" + getString(R.string.alert_request_try_again)
                            + "\n\n" + getString(R.string.alert_request_scan_code_failed_message) + "\n\n" +
                            getString(R.string.alert_request_sorry_for_failed)).show();
                }
            });
            getTicketApiLoadingDialog.show();
        }
    }

    private void scanQRService() {

        if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
            showAlertSnackBar(R.string.alert_network_not_exist);
        } else {

            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(this).
                    create(TambolaAPIInterface.class);

            /*String number = bundle.getString(TambolaConstants.GetBundleKeys.KEY_TODAY_NUMBER);
            String code = bundle.getString(TambolaConstants.GetBundleKeys.KEY_TODAY_CODE);*/

            ScanQRModel scanQRModel = new ScanQRModel(todayNumberModel.data.code, todayNumberModel.data.numbers);
            final Call<ScanResultModel> call = apiInterface.scanQR(scanQRModel);

            scanCodeApiLoadingDialog = new ApiLoadingDialog(TambolaDashboardActivity.this, new ApiLoadingInterface() {
                @Override
                public void makeApiCall() {
                    call.enqueue(scanCodeApiCallback);
                }

                @Override
                public void makeApiCallAgain() {
                    call.clone().enqueue(scanCodeApiCallback);
                }

                @Override
                public void onPermanentFailure() {
                    new ApiMessageDialog(TambolaDashboardActivity.this, getString(R.string.alert_title_failed), getString(R.string.alert_request_unable_to_process) + "\n" + getString(R.string.alert_request_try_again)
                            + "\n\n" + getString(R.string.alert_request_scan_code_failed_message) + "\n\n" +
                            getString(R.string.alert_request_sorry_for_failed)).show();
                }
            });
            scanCodeApiLoadingDialog.show();
        }
    }

    private void setTicketBoardAgainForScanSuccess() {
        optedNumberList.addAll(todayNumberList);
        ticketAdapter.notifyDataSetChanged();
    }

    /**
     * @param alertStringResId
     */
    private void showAlertSnackBar(@StringRes int alertStringResId) {
        if (null == alertSnackBar) {
            View view = findViewById(R.id.cl_parent);
            alertSnackBar = Snackbar
                    .make(view, alertStringResId, Snackbar.LENGTH_LONG)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertSnackBar.dismiss();
                        }
                    });
            TextView tv = alertSnackBar.getView().findViewById(R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
        } else {
            alertSnackBar.setText(alertStringResId);
        }
        alertSnackBar.show();
    }

    // alert popup when number code does not match
    private void alertPopUp() {
        final Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        builder.setContentView(R.layout.diaog_tambola_scan_qr);
        builder.setCancelable(false);
        builder.show();
        RelativeLayout allow_action = builder.findViewById(R.id.allow_action);
        allow_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.dismiss();
            }
        });
    }

    // alert popup when number code does match
    private void congratulationsPopUp() {
        final Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        builder.setContentView(R.layout.dialog_tambola_congratulation);
        builder.setCancelable(false);
        builder.show();
        RelativeLayout allow_action = builder.findViewById(R.id.allow_action);
        allow_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.dismiss();
                ////getTicketService();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tambola_invite_menu, menu);
        MenuItem menuItem = menu.getItem(0);

        //Drawable icon = FontIconDrawable.inflate(getApplicationContext(), R.drawable.ic_home_black_24dp_tambola);
        menuItem.setIcon(R.drawable.inivite_frnd_bg_tambola);
        menuItem.setVisible(false);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        isUserLeaved = false;
        if (resultCode == Activity.RESULT_OK && requestCode == TambolaConstants.RC_SCAN_CODE) {

            if (BuildConfig.DEBUG) {
                Toast.makeText(getApplicationContext(), "NUMBER SCAN SUCCESS", Toast.LENGTH_LONG).show();
            }

            // Show Today Number on Dashboard since Scan Happened with Success
            showTodayNumbersOnDashboard(true);

            if (data != null) {
                // Scan successful and code matched
                boolean isAnyNumberIsInTicket = false;
                for (String todayNumber : todayNumberList) {
                    if (ticketList.contains(todayNumber)) {
                        isAnyNumberIsInTicket = true;
                        break;
                    }
                }
                if (isAnyNumberIsInTicket) {
                    scanQRService();
                } else {
                    alertPopUp();
                }
            } else {
                // Scan successful but code does not matched
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");

        if (isUserLeaved) {
            String userLeavedDate = tambolaSharedPref.getDateWhenUserTapOnHome();
            if (!TextUtils.isEmpty(userLeavedDate) && userLeavedDate.equals(getFormattedCurrentDate())) {
                getGetTodayNumberService();
            }
            isUserLeaved = false;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");

//        CTTracker.cleverTapTrackPageView(getApplicationContext(), CTConstant.PROP_VALUE.Tambola.SCREEN_DASHBOARD,
//                CTConstant.PROP_VALUE.TAMBOLA);
        Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.TAMBOLA + "-" + CTConstant.PROP_VALUE.Tambola.SCREEN_DASHBOARD, "", "", "");

    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        Log.d(TAG, "onUserLeaveHint");
        isUserLeaved = true;
        tambolaSharedPref.saveDateWhenUserTapOnHome(getFormattedCurrentDate());
    }


//    private final Callback<GetTicketModel> getTicketApiCallback = new Callback<GetTicketModel>() {
//        @Override
//        public void onResponse(@NonNull Call<GetTicketModel> call, @NonNull Response<GetTicketModel> response) {
//            try {
//                GetTicketModel getTicketModel = response.body();
//                int size = ticketList.size();
//                ticketList.clear();
//                ticketAdapter.notifyItemRangeRemoved(0, size);
//                if (!getTicketModel.data.opted_numbers.isEmpty()) {
//                    // Log.d("opted_numbers arr", getTicketModel.data.opted_numbers.get(i));
//                    optedNumberList.addAll(getTicketModel.data.opted_numbers);
//                } else {
//                    optedNumberList.clear();
//                }
//                setUserEligibilityStatus(getTicketModel.data.eligibilityStatus);
//
//                setSponsorDetail(getTicketModel.data.getSponsorDetails());
//
//                JSONObject object = new JSONObject(getTicketModel.data.ticket_details);
//
//                for (int index = 0; index < TambolaConstants.TICKET_NO_OF_ROWS; index++) {
//                    for (int jIndex = 0; jIndex < TambolaConstants.TICKET_NO_OF_COLUMNS; jIndex++) {
//                        JSONObject jsonObject = object.getJSONObject(jIndex + "");
//                        String ticket = jsonObject.getString(index + "");
//                        ticketList.add(ticket);
//                    }
//                }
//
//                try {
//                    mWinnerListUrl = getTicketModel.data.winnerList;
//                } catch (NullPointerException e) {
//
//                }
//
//
//
//                /*Iterator<String> keys = object.length()
//
//                while (keys.hasNext()) {
//                    String keyValue = (String) keys.next();
//                    String valueString = object.getString(keyValue);
//
//                    JSONObject jsonObject = new JSONObject(valueString);
//
//
//                }
//
//
//                Iterator<String> keys = object.keys();
//
//                while (keys.hasNext()) {
//                    String keyValue = (String) keys.next();
//                    String valueString = object.getString(keyValue);
//                    Log.d("keyValue", keyValue + "\n" + valueString);
//
//                    JSONObject jsonObject = new JSONObject(valueString);
//                    Iterator<String> keys1 = jsonObject.keys();
//
//                    while (keys1.hasNext()) {
//                        String keyValue1 = (String) keys1.next();
//                        String valueString1 = jsonObject.getString(keyValue1);
//
//                        //Log.d("filter value", keyValue1 + "\n" + valueString1);
//                        ticketList.add(valueString1);
//                    }
//                }*/
//                ticketAdapter.notifyItemRangeInserted(0, ticketList.size());
//                ticketAdapter.notifyDataSetChanged();
//                getTicketApiLoadingDialog.dismiss();
//            } catch (Exception e) {
//                e.printStackTrace();
//                doOnApiFailed();
//            }
//        }
//
//        @Override
//        public void onFailure(Call<GetTicketModel> call, Throwable t) {
//            Log.e(TAG, t.getMessage() + "");
//            doOnApiFailed();
//        }
//
//        private void doOnApiFailed() {
//            getTicketApiLoadingDialog.handleApiFailure();
//        }
//    };
}
