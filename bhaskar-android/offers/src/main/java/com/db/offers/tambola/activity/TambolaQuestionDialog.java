package com.db.offers.tambola.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;


import com.db.offers.R;
import com.db.offers.common.BaseDialog;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.DBToast;
import com.db.offers.common.EasyDialogUtils;
import com.db.offers.common.TransAviLoader;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.model.AttemptedQuestionRequestModel;
import com.db.offers.tambola.model.AttemptedQuestionResponseModel;
import com.db.offers.tambola.model.QuestionAttemptStatusModel;
import com.db.offers.tambola.network.TambolaAPIClient;
import com.db.offers.tambola.network.TambolaAPIInterface;
import com.db.offers.tambola.utils.JustifiedTextView;
import com.db.offers.tambola.utils.TambolaQuestionCallback;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambolaQuestionDialog extends BaseDialog {

    private static final String TAG = TambolaQuestionDialog.class.getSimpleName();
    private TransAviLoader mTransLoader;
    private JustifiedTextView mTvAttemptQuestionNotice;
    private TextView mTvQuestion, mBtnSubmit, mBtnCancel;
    private RadioGroup mRgOptions;
    private QuestionAttemptStatusModel.Quiz.Option mResponseOption = null;
    private BaseActivity mBaseActivity;
    private TambolaQuestionCallback mCallback;

    TambolaQuestionDialog(@NonNull BaseActivity activityContext, TambolaQuestionCallback callback) {
        super(activityContext, R.style.QuestionDialogTheme);
        mBaseActivity = activityContext;
        mCallback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambola_launcher);
        initUI();
    }

    private void initUI() {
        mTvAttemptQuestionNotice = findViewById(R.id.tv_attempt_question_notice);
        mTvQuestion = findViewById(R.id.tv_question);
        mRgOptions = findViewById(R.id.rg_question_options);
        mBtnSubmit = findViewById(R.id.btn_submit);
        mBtnCancel = findViewById(R.id.btn_cancel);
        FrameLayout mBottomAdview = findViewById(R.id.fl_adv_view_bottom);
        mBottomAdview.setVisibility(View.GONE);
        mTransLoader = new TransAviLoader(mBaseActivity);

        QuestionAttemptStatusModel attemptStatusModel = mBaseActivity.getTambolaSharedPref().getAttemptedQuestion();
        if (null != attemptStatusModel) {
            renderQuestionData(attemptStatusModel);
        } else {
            dismiss();
        }
    }

    /**
     * @param attemptStatusModel
     */
    private void renderQuestionData(QuestionAttemptStatusModel attemptStatusModel) {
        try {
            disableSubmitButton();
            String notice = attemptStatusModel.getNotice();
            if (!TextUtils.isEmpty(notice)) {
                mTvAttemptQuestionNotice.setText(notice);
            }
            String question = attemptStatusModel.getQuiz().getQuestion().getDetail();
            if (!TextUtils.isEmpty(question)) {
                question = mBaseActivity.getString(R.string.prashn) + question;
                mTvQuestion.setText(question);
            }
            // Set question options
            QuestionAttemptStatusModel.Quiz quiz = attemptStatusModel.getQuiz();
            if (null != quiz) {
                List<QuestionAttemptStatusModel.Quiz.Option> optionList = attemptStatusModel.getQuiz().getOptions();
                if (null != optionList && !optionList.isEmpty()) {
                    LayoutInflater inflater = mBaseActivity.getLayoutInflater();
                    for (QuestionAttemptStatusModel.Quiz.Option option : optionList) {
                        RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.layout_quiz_options, null);
                        radioButton.setText(option.getOption());
                        radioButton.setTag(option);
                        // Add view
                        mRgOptions.addView(radioButton);

                        radioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
                            if (isChecked) {
                                enableSubmitButton();
                            }
                        });
                    }
                } else {
                    mCallback.onNotAnswered();
                    dismiss();
                }
            } else {
                mCallback.onNotAnswered();
                dismiss();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
            mCallback.onNotAnswered();
            dismiss();
        }

        mBtnSubmit.setOnClickListener(v -> {
            // Get selected option
            int selectedResId = mRgOptions.getCheckedRadioButtonId();
            if (selectedResId > 0) {
                RadioButton radioButton = findViewById(selectedResId);
                mResponseOption = (QuestionAttemptStatusModel.Quiz.Option) radioButton.getTag();
                getQuestionAttemptedResponse(mResponseOption);
            }
        });

        mBtnCancel.setOnClickListener(v -> {
            mCallback.onNotAnswered();
            dismiss();
        });
    }


    private void enableSubmitButton() {
        mBtnSubmit.setAlpha(1.0f);
        mBtnSubmit.setEnabled(true);
    }

    private void disableSubmitButton() {
        mBtnSubmit.setAlpha(0.3f);
        mBtnSubmit.setEnabled(false);
    }

    /**
     * @param responseOption
     */
    private void getQuestionAttemptedResponse(QuestionAttemptStatusModel.Quiz.Option responseOption) {
        if (!CommonUtils.isNetworkAvailable(mBaseActivity)) {
            DBToast.showAlertToast(mBaseActivity, mBaseActivity.getString(R.string.alert_network_not_exist));
        } else {
            mTransLoader.show();
            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(getContext()).create(TambolaAPIInterface.class);
            AttemptedQuestionRequestModel requestModel = new AttemptedQuestionRequestModel(responseOption.getQid(), responseOption.getOptionId());
            Call<AttemptedQuestionResponseModel> call = apiInterface.doPostAttemptedQuestion(requestModel);

            call.enqueue(new Callback<AttemptedQuestionResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<AttemptedQuestionResponseModel> call, @NonNull Response<AttemptedQuestionResponseModel> response) {
                    try {
                        if (response.code() == TambolaConstants.API_SUCCESS) {
                            AttemptedQuestionResponseModel responseModel = response.body();
                            if (null != responseModel) {
                                AttemptedQuestionResponseModel.AttemptedQuestionStatusModel statusModel = responseModel.getAttemptedQuestionStatusModel();
                                if (null != statusModel) {
                                    if (statusModel.getAttemptCode() == 1) {
                                        mCallback.onAnswerCorrect();
                                    } else {
                                        mCallback.onAnswerInCorrect();
                                        EasyDialogUtils.showFinishDialog(mBaseActivity, mBaseActivity.getString(R.string.alert_title_alert), statusModel.getAttemptMessage());
                                    }
                                }
                            } else {
                                mCallback.onNotAnswered();
                                EasyDialogUtils.showInfoDialog(mBaseActivity, mBaseActivity.getString(R.string.alert_msg_server_failed));
                            }
                        } else {
                            mCallback.onNotAnswered();
                            EasyDialogUtils.showInfoDialog(mBaseActivity, mBaseActivity.getString(R.string.alert_msg_server_failed));
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onResponse: " + e.getMessage());
                        mCallback.onNotAnswered();
                        EasyDialogUtils.showInfoDialog(mBaseActivity, mBaseActivity.getString(R.string.alert_request_unable_to_process));
                    } finally {
                        mTransLoader.dismiss();
                        dismiss();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AttemptedQuestionResponseModel> call, @NonNull Throwable t) {
                    mCallback.onNotAnswered();
                    mTransLoader.dismiss();
                    dismiss();
                    EasyDialogUtils.showInfoDialog(mBaseActivity, mBaseActivity.getString(R.string.alert_msg_server_failed));
                }
            });
        }
    }
}
