package com.db.offers.tambola.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.db.offers.R;
import com.db.offers.R2;
import com.db.offers.megaoffers.providers.WebJavaScripInterfaceProvider;
import com.db.offers.megaoffers.providers.WebViewClientProvider;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.utils.TambolaSharedPref;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import butterknife.BindView;

import static com.db.offers.tambola.constant.TambolaConstants.HeaderKeys.KEY_DEVICE_ID;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderKeys.KEY_TICKET_ID;


public class TambolaWebViewActivity extends BaseActivity  {

    @BindView(R2.id.activity_progress_bar_av)
    AVLoadingIndicatorView mWebViewProgressBar;

    @BindView(R2.id.web_niyam)
    WebView mWebview;

    private String mWebUrl;

    @Nullable
    private String mScreenType;
    private WebJavaScripInterfaceProvider webJavaScripInterfaceProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambola_web_view);
        initUI();
        processUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void initUI() {
        mWebViewProgressBar.smoothToShow();
        extractArguments();
    }

    private void extractArguments() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            mWebUrl = bundle.getString(TambolaConstants.GetBundleKeys.KEY_WEB_URL);
            mScreenType = bundle.getString(TambolaConstants.GetBundleKeys.KEY_SCREEN_TYPE);
        }
    }

    @Override
    protected void processUI() {
        if (!TextUtils.isEmpty(mScreenType)) {
            switch (mScreenType) {
                case TambolaConstants.SCREEN_GAME_RULES:
                    initToolbar(getString(R.string.title_game_rules));
                    break;
                case TambolaConstants.WINNER_TYPE_WEB:
                    initToolbar(getString(R.string.title_tambola_winners));
                    break;
                default:
                    initToolbar(mScreenType);
                    break;
            }
        } else {
            initToolbar(getString(R.string.title_db_tambola));
        }
        mWebview.setWebViewClient(new WebViewClientProvider(this, new WebViewClientProvider.WebPageListener() {
            @Override
            public void onWebPageFinished(WebView view, String url) {
                if (mWebViewProgressBar != null) {
                    mWebViewProgressBar.smoothToHide();
                }
            }
        }));

        mWebview.getSettings().setLoadsImagesAutomatically(true);
        addJavaScriptInterface();
        mWebview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebview.loadUrl(mWebUrl, getWebHeader(this));
    }

    public Map<String, String> getWebHeader(Context context) {
        Map<String, String> headerMap = new HashMap<>();
        final TambolaSharedPref tambolaSharedPref = new TambolaSharedPref(context);
        final String ticketId = tambolaSharedPref.getTicketId();
        String deviceId = TrackingData.getDeviceId(context);
        headerMap.put(KEY_TICKET_ID, ticketId);
        headerMap.put(KEY_DEVICE_ID, deviceId);
        return headerMap;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void addJavaScriptInterface() {
        webJavaScripInterfaceProvider = new WebJavaScripInterfaceProvider(this);
        webJavaScripInterfaceProvider.addJsInterface(mWebview);
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {

    }

    /**
     * @param context
     * @param bundle
     */
    public static void startActivity(Activity context, Bundle bundle) {
        Intent intent = new Intent(context, TambolaWebViewActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWebview != null && null != webJavaScripInterfaceProvider) {
            webJavaScripInterfaceProvider.removeJsInterface(mWebview);
            mWebview.destroy();
        }
    }



    @Override
    public void onBackPressed() {
        if (mWebview.canGoBack()) {
            mWebview.goBack();
        } else{
            super.onBackPressed();
        }
    }
}
