package com.db.offers.tambola.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.db.offers.OfferController;
import com.db.offers.R;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.TransAviLoader;
import com.db.offers.tambola.adapters.NumberBoardAdapter;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.model.AllNumberModel;
import com.db.offers.tambola.model.AllNumbersResponseModel;
import com.db.offers.tambola.network.TambolaAPIClient;
import com.db.offers.tambola.network.TambolaAPIInterface;
import com.bhaskar.appscommon.tracking.Tracking;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class TicketBoardActivity extends BaseActivity {

    private RecyclerView boardRv;
    private Snackbar alertSnackBar;
    private List<AllNumberModel> allNumbersModelList = new ArrayList<>();

    //    private FrameLayout mTopAdview;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambola_ticket_board);
        initToolbar(getString(R.string.title_all_numbers));
        initUI();
        processUI();

        findViewById(R.id.btn_ok).setOnClickListener(v -> finish());

//        CTTracker.cleverTapTrackPageView(getApplicationContext(), CTConstant.PROP_VALUE.Tambola.SCREEN_ALL_NUMBERS,
//                CTConstant.PROP_VALUE.TAMBOLA);
        Tracking.trackGAScreen(this, OfferController.getInstance().getDefaultTracker(), CTConstant.PROP_VALUE.TAMBOLA + "-" + CTConstant.PROP_VALUE.Tambola.SCREEN_ALL_NUMBERS, "", "", "");

    }

    @Override
    protected void initUI() {
        boardRv = findViewById(R.id.rv_number_board);
//        mTopAdview = findViewById(R.id.fl_top_adview);
    }

    @Override
    protected void processUI() {
        getNumberBoardService();
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {

    }


    public static void startActivity(Activity context, Bundle bundle) {
        Intent intent = new Intent(context, TicketBoardActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }


    private void getNumberBoardService() {
        // Toast.makeText(this, ticket_id, Toast.LENGTH_SHORT).show();

        if (!CommonUtils.isNetworkAvailable(getApplicationContext())) {
            showAlertSnackBar(R.string.alert_network_not_exist);
        } else {
            TransAviLoader pd = new TransAviLoader(this);
            pd.show();
            allNumbersModelList.clear();
            TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(this).
                    create(TambolaAPIInterface.class);

            Call<AllNumbersResponseModel> call = apiInterface.doGetNumberBoardList();

            call.enqueue(new Callback<AllNumbersResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<AllNumbersResponseModel> call, @NonNull Response<AllNumbersResponseModel> response) {
                    pd.dismiss();
                    AllNumbersResponseModel allNumbersModel = response.body();
                    if (allNumbersModel != null && allNumbersModel.data != null) {
                        Log.d("Number Board", allNumbersModel.message);

                        for (AllNumbersResponseModel.DataModel dataModel : allNumbersModel.data) {

                            AllNumberModel allNumberModel;

                            if (Integer.parseInt(dataModel.numbers) < TambolaConstants.TICKET_BOARD_ALL_NO_OF_COLUMNS) {
                                allNumberModel = new AllNumberModel("0" + dataModel.numbers);
                            } else {
                                allNumberModel = new AllNumberModel(dataModel.numbers);
                            }

                            allNumberModel.setCode(dataModel.code);
                            allNumberModel.setCreated(dataModel.created);
                            allNumberModel.setValidDate(dataModel.getValiddt());
                            allNumbersModelList.add(allNumberModel);
                        }

                        boardRv.setLayoutManager(new GridLayoutManager(TicketBoardActivity.this, TambolaConstants.TICKET_BOARD_ALL_NO_OF_COLUMNS));
                        boardRv.setAdapter(new NumberBoardAdapter(TicketBoardActivity.this, allNumbersModelList));
                    }
                }

                @Override
                public void onFailure(Call<AllNumbersResponseModel> call, Throwable t) {
                    pd.dismiss();
                }
            });
        }
    }

    /**
     * @param alertStringResId
     */
    private void showAlertSnackBar(@StringRes int alertStringResId) {
        if (null == alertSnackBar) {
            View view = findViewById(R.id.cl_parent);
            alertSnackBar = Snackbar
                    .make(view, alertStringResId, Snackbar.LENGTH_LONG)
                    .setAction("OK", view1 -> alertSnackBar.dismiss());
            TextView tv = alertSnackBar.getView().findViewById(R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
        } else {
            alertSnackBar.setText(alertStringResId);
        }
        alertSnackBar.show();
    }

}
