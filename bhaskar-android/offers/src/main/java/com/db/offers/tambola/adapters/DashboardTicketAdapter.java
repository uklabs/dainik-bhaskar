package com.db.offers.tambola.adapters;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.db.offers.R;
import com.db.offers.common.CommonUtils;
import com.db.offers.tambola.activity.TambolaDashboardActivity;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.utils.MyBounceInterpolator;
import com.db.offers.tambola.utils.SquareCardView;

import java.util.List;
import java.util.Set;

public final class DashboardTicketAdapter extends RecyclerView.Adapter<TicketHolder> {

    private LayoutInflater inflater;
    private List<String> ticketList;
    private Set<String> optedNumberSet;
    private Animation myAnim;
    private List<String> todaysNumberList;
    private int cardBackColorSelect;
    private int cardTextColorWhite, cardTextColorBlack;
    private float cardCornerRadius;

    /***
     *
     * @param dashboardActivity
     * @param ticketList
     * @param todaysNumberList
     * @param optedNumberSet
     */
    public DashboardTicketAdapter(TambolaDashboardActivity dashboardActivity, List<String> ticketList,
                                  List<String> todaysNumberList, Set<String> optedNumberSet) {
        this.ticketList = ticketList;
        this.todaysNumberList = todaysNumberList;
        inflater = LayoutInflater.from(dashboardActivity);
        this.optedNumberSet = optedNumberSet;
        myAnim = AnimationUtils.loadAnimation(dashboardActivity, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        /*DisplayMetrics displayMetrics = new DisplayMetrics();
        dashboardActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int ticketWidth = displayMetrics.widthPixels;
        ticketWidth = (int) ((ticketWidth - (dashboardActivity.getResources().getDimensionPixelSize(R.dimen.ticket_5dp) * 2.35f) -
                (dashboardActivity.getResources().getDimensionPixelSize(R.dimen.ticket_1dp) * 5)) / 9);*/

        cardBackColorSelect = ContextCompat.getColor(dashboardActivity.getApplicationContext(), R.color.tm_btn_grad_l);

        cardTextColorWhite = ContextCompat.getColor(dashboardActivity.getApplicationContext(), R.color.white);

        cardTextColorBlack = ContextCompat.getColor(dashboardActivity.getApplicationContext(), R.color.tambola_black_txt);

        int deviceWidth = CommonUtils.getDeviceWidthAndHeight(dashboardActivity)[0];

        cardCornerRadius = deviceWidth - (TambolaConstants.TICKET_NO_OF_COLUMNS) * dashboardActivity.getResources().getDimensionPixelSize(R.dimen._2sdp);
        cardCornerRadius = cardCornerRadius / TambolaConstants.TICKET_NO_OF_COLUMNS;
        cardCornerRadius = cardCornerRadius / 2;
    }

    @NonNull
    @Override
    public TicketHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SquareCardView squareCardView = (SquareCardView) inflater.inflate(R.layout.item_tambola_number_board, parent, false);

        squareCardView.setRadius(cardCornerRadius);

        /*ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = ticketWidth;
        view.setLayoutParams(params);*/
        return new TicketHolder(squareCardView);
    }

    @Override
    public void onBindViewHolder(@NonNull TicketHolder holder, int position) {
        String ticket = ticketList.get(position);

        if (!TextUtils.isEmpty(ticket) && optedNumberSet.contains(ticket)) {

            ((CardView) holder.itemView).setCardBackgroundColor(cardBackColorSelect);
            holder.numberTv.setTextColor(cardTextColorWhite);

            if (todaysNumberList.contains(ticket)) {
                holder.numberTv.startAnimation(myAnim);
            }
        } else {
            ((CardView) holder.itemView).setCardBackgroundColor(Color.WHITE);
            holder.numberTv.setTextColor(cardTextColorBlack);
        }

        /*if (position % 2 == 0) {
            ((CardView) holder.itemView).setCardBackgroundColor(Color.WHITE);
            holder.numberTv.setTextColor(cardTextColorBlack);
        } else {
            ((CardView) holder.itemView).setCardBackgroundColor(cardBackColorSelect);
            holder.numberTv.setTextColor(cardTextColorWhite);
        }*/

        holder.numberTv.setText(ticket);
    }

    @Override
    public int getItemCount() {
        return ticketList.size();
    }
}
