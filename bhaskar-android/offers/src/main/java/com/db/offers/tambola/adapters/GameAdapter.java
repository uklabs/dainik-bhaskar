package com.db.offers.tambola.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.db.offers.R;
import com.db.offers.tambola.model.Game;

import java.util.ArrayList;
import java.util.Random;

public class GameAdapter extends RecyclerView.Adapter<GameAdapter.ViewHolder> {
    private ArrayList<Game> statusList;
    private LayoutInflater inflater;
    private Random rnd;

    /***
     *
     * @param context
     * @param statusList
     */
    public GameAdapter(Context context, ArrayList<Game> statusList) {
        this.statusList = statusList;
        inflater = LayoutInflater.from(context);
        rnd = new Random();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View contactsView = inflater.inflate(R.layout.tambola_game_list, parent, false);
        return new ViewHolder(contactsView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.claimed_one_text.setText(statusList.get(position).getClaimed());
        holder.prize.setText(statusList.get(position).getName());
        holder.available_one.setText(statusList.get(position).getAvailiable());
        holder.view_color.setBackgroundColor(statusList.get(position).getViewcolor());

        if (!holder.claimed_one_text.getText().toString().equals("0")) {
            holder.claimed_one_layout.setBackgroundColor(Color.parseColor("#CCDFF2"));
        }
        //getting available values and set background color
        if (!holder.available_one.getText().toString().equals("0")) {
            holder.available_layout_one.setBackgroundColor(Color.parseColor("#CCDFF2"));
        }

        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        holder.view_color.setBackgroundColor(color);
    }

    @Override
    public int getItemCount() {
        return statusList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout claimed_one_layout, claimed_two_layout, claimed_three_layout, claimed_four_layout, claimed_five_layout, claimed_six_layout;
        private LinearLayout available_layout_one, available_layout_two, available_layout_three, available_layout_four, available_layout_five, available_layout_six;
        private TextView claimed_one_text, claimed_two_text, claimed_three_text, claimed_four_text, claimed_five_text, claimed_six_text;
        private TextView available_one, available_two, available_three, available_four, available_five, available_six;
        private View view_color;
        private TextView prize;

        ViewHolder(View itemView) {
            super(itemView);

            claimed_one_layout = itemView.findViewById(R.id.claimed_one_layout);

            //availale layout for set backgrond
            available_layout_one = itemView.findViewById(R.id.available_layout_one);
            //claimed text
            claimed_one_text = itemView.findViewById(R.id.claimed_one_text);
            //avilable textview for available
            available_one = itemView.findViewById(R.id.available_one);
            //set view color
            view_color = itemView.findViewById(R.id.view_color);

            prize = itemView.findViewById(R.id.prize);
        }
    }
}
