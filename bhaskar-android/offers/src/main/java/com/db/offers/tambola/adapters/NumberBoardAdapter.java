package com.db.offers.tambola.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.db.offers.R;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.DBToast;
import com.db.offers.common.activity.CoreActivity;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.model.AllNumberModel;
import com.db.offers.tambola.utils.SquareCardView;

import java.util.List;

public final class NumberBoardAdapter extends RecyclerView.Adapter<TicketHolder> {

    private Context activityContext;
    private LayoutInflater inflater;
    private List<AllNumberModel> allNumbersModelList;
    private int cardBackColorSelect;
    private int cardTextColorWhite, cardTextColorBlack;
    private float cardCornerRadius;
    //private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private static final String TAG = NumberBoardAdapter.class.getSimpleName();

    /***
     *
     * @param activityContext
     * @param allNumbersModelList
     */
    public NumberBoardAdapter(CoreActivity activityContext, List<AllNumberModel> allNumbersModelList) {
        this.activityContext = activityContext;
        inflater = LayoutInflater.from(activityContext);
        this.allNumbersModelList = allNumbersModelList;
        Log.d("NumberBoardAdapter", "allNumbersModelList: " + allNumbersModelList);
        cardBackColorSelect = ContextCompat.getColor(activityContext.getApplicationContext(), R.color.tm_btn_grad_l);

        cardTextColorWhite = ContextCompat.getColor(activityContext.getApplicationContext(), R.color.white);
        cardTextColorBlack = ContextCompat.getColor(activityContext.getApplicationContext(), R.color.tambola_black_txt);

        int deviceWidth = CommonUtils.getDeviceWidthAndHeight(activityContext)[0];

        cardCornerRadius = deviceWidth - (TambolaConstants.TICKET_BOARD_ALL_NO_OF_COLUMNS) * activityContext.getResources().getDimensionPixelSize(R.dimen._2sdp);
        cardCornerRadius = cardCornerRadius / TambolaConstants.TICKET_BOARD_ALL_NO_OF_COLUMNS;
        cardCornerRadius = cardCornerRadius / 2;
    }

    @NonNull
    @Override
    public TicketHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SquareCardView squareCardView = (SquareCardView) inflater.inflate(R.layout.item_tambola_number_board, parent, false);
        squareCardView.setRadius(cardCornerRadius);

        return new TicketHolder(squareCardView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TicketHolder holder, int position) {

        position = position + 1;

        String textNumber;

        if (position < 10) {
            textNumber = "0" + position;
        } else {
            textNumber = position + "";
        }
        holder.numberTv.setText(textNumber);

        AllNumberModel numberModelDummy = new AllNumberModel(textNumber);

        int indexOf = allNumbersModelList.indexOf(numberModelDummy);

        /*boolean contains = allNumbersModelList.contains(numberModelDummy);
        Log.d("NumberBoardAdapter", "indexOf: " + indexOf + " textNumber: " + textNumber + " contains: " + contains);*/

        if (indexOf >= 0) {
            AllNumberModel numberModel = allNumbersModelList.get(indexOf);
            holder.itemView.setOnClickListener(clickListener);
            holder.itemView.setTag(numberModel.getValidDate());
            ((CardView) holder.itemView).setCardBackgroundColor(cardBackColorSelect);
            holder.numberTv.setTextColor(cardTextColorWhite);
        } else {
            holder.itemView.setOnClickListener(null);
            ((CardView) holder.itemView).setCardBackgroundColor(Color.WHITE);
            holder.numberTv.setTextColor(cardTextColorBlack);
        }
    }

    private final View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (null != v.getTag()) {
                try {
                    String validDate = (String) v.getTag();
                    /*Date createdDataObj = dateFormat.parse(createdDate);

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(createdDataObj);
                    cal.add(Calendar.DAY_OF_MONTH, 1);

                    createdDataObj = cal.getTime();
                    createdDate = dateFormat.format(createdDataObj);*/

                    DBToast.showSuccessToast(activityContext.getApplicationContext(), "Draw Date :- " + validDate);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage() + "");
                }
            }
        }
    };

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return TambolaConstants.TICKET_BOARD_TOTAL_NUMBER_COUNT;
    }
}
