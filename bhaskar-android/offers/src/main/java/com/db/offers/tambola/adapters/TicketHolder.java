package com.db.offers.tambola.adapters;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.db.offers.R;


public class TicketHolder extends RecyclerView.ViewHolder  {
    TextView numberTv;

    public TicketHolder(View itemView) {
        super(itemView);
        numberTv = itemView.findViewById(R.id.tv_number_board);
    }
}
