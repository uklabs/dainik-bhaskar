package com.db.offers.tambola.constant;

/**
 * Created by hp on 25-06-2018.
 */

public interface TambolaConstants {

//        String DB_TAMBOLA_BASE_URL = "https://tambola.dbnewshub.com/";
    //String DB_TAMBOLA_BASE_URL = "https://stage-digital.dbnewshub.com/";
    //String DB_TAMBOLA_BASE_URL = "http://54.208.139.241/";
//    String DB_TAMBOLA_BASE_URL = "http://13.127.218.105/";

    int MAX_API_COUNT = 3, AUTO_SWAP_INTERVAL = 5000;
    long TIMER_MILLISECONDS_FIRST_RETRY = 60 * 1000;
    long TIMER_MILLISECONDS_SECOND_RETRY = 90 * 1000;
    int EXTERNAL_STORAGE_PERMISSION = 1001;

    int TICKET_NO_OF_ROWS = 3;
    int TICKET_NO_OF_COLUMNS = 9;
    int TICKET_BOARD_ALL_NO_OF_COLUMNS = 10;
    int TICKET_BOARD_TOTAL_NUMBER_COUNT = 90;


    int API_SUCCESS = 200; // Success code
    int API_OTP_FAILED = 401;

    int API_INTERNAL_SERVER_ERROR = 500;
    int API_BAD_REQUEST = 400; // Bad request
    int API_UNAUTHORIZED_ACCESS = 401; // Unauthorised access
    int API_NOT_FOUND = 404; // Error not found

    int RC_SCAN_CODE = 1211;
    int RC_SHOW_NUMBER_CODE = 1212;

    int API_READ_TIMEOUT = 60; // Seconds;
    int API_CONNECT_TIMEOUT = 60; // Seconds;
    String WINNER_TYPE_WEB = "winner_web";
    String SCREEN_GAME_RULES = "Game_Rules";

    interface HeaderKeys {
        String KEY_AUTHORIZATION = "Authorization";
        String KEY_CHANNEL = "Channel";
        String KEY_DEVICE_TYPE = "Device-Type";
        String KEY_ACCEPT = "Accept";
        String KEY_DB_ID = "DB-Id";
        String KEY_CONTENT_TYPE = "Content-Type";
        String KEY_X_CSRF_TOKEN = "X-CSRF-Token";
        String KEY_UUID = "uuid";
        String KEY_APPID = "app-id";
        String KEY_DEVICE_ID = "device-id";
        String KEY_TICKET_ID = "ticket-id";
    }

    interface HeaderValues {
        String VALUE_CHANNEL = "MOBILE";
        String VALUE_DEVICE_TYPE = "ANDROID";
        String VALUE_ACCEPT = "application/json";
        String VALUE_CONTENT_TYPE = "application/json";
        String BHASKAR_APP_ID = "4";
    }

    interface GetBundleKeys {
        // Below keys will be used in coupon flow when we'll navigate from splash -> chatintro -> coupon
        String KEY_TODAY_NUMBER = "key_today_number";
        String KEY_TODAY_CODE = "key_today_code";
        String KEY_REGISTER_MODEL = "key_register_model";
        String KEY_WEB_URL = "key_web_url";
        String KEY_SCREEN_TYPE = "key_screen_type";
    }

    /*interface GenderArray {
       // String[] GENDER = {"Male", "Female", "Rather Not Say"};
    }*/

    interface ClickableSpan {
        int startIndexOfNiyamSharteinCheckbox = 0; // मेरी उम्र 18 साल+ है और मैं
        int endIndexOfNiyamSharteinCheckbox = 27; // मेरी उम्र 18 साल+ है और मैं
        int startIndexOfNiyamShartein = 27; // नियम और शर्तों
        int endIndexOfNiyamShartein = 42; // नियम और शर्तों
        int startIndexOfNiyamSharteinTillEnd = 42; // मेरी उम्र 18 साल+ है और मैं
        int endIndexOfNiyamSharteinTillEnd = 63; // मेरी उम्र 18 साल+ है और मैं
    }

    interface ToolTip {
        int TOOLTIP_AUTO_DISMISS = 3000;
        int TOOLTIP_WIDTH = 20;
        int TOOLTIP_HEIGHT = 20;
    }

    interface APIEndPoint{
        String GAME_RULE_URL = "terms_faq/faq.html";
        String WINNERS_URL = "terms_faq/faq.html";
        String TERM_CONDITION_URL = "terms_faq/terms-condition.html";
    }
}
