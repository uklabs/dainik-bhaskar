package com.db.offers.tambola.dialogs;

public interface ApiLoadingInterface {

    void makeApiCall();
    void makeApiCallAgain();
    void onPermanentFailure();
}
