package com.db.offers.tambola.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import androidx.annotation.NonNull;

import com.db.offers.OfferController;
import com.db.offers.R;
import com.db.offers.common.BaseDialog;
import com.bhaskar.appscommon.tracking.clevertap.CTConstant;
import com.db.offers.tambola.activity.BaseActivity;
import com.db.offers.tambola.activity.TambolaDashboardActivity;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.model.RegisterUserModel;
import com.db.offers.tambola.model.RegisterUserResponseModel;
import com.db.offers.tambola.network.TambolaAPIClient;
import com.db.offers.tambola.network.TambolaAPIInterface;
import com.db.offers.tambola.utils.ApiMessageDialog;
import com.bhaskar.appscommon.tracking.Tracking;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class RegistrationRequestDialog extends BaseDialog implements View.OnClickListener, DialogInterface.OnDismissListener {

    private BaseActivity activity;
    private final String TAG = "RegisterRequestDialog";

    private ViewAnimator viewAnimator;
    private TextView titleTv, timerTv;

    private ProgressBar progressBar;
    private ViewGroup timerContainerView;

    private TextView retryBtn, cancelBtn, showDashboardBtn;
    private Call<RegisterUserResponseModel> requestRetroCall;

    private int apiCount = 0;

    private CountDownTimer countDownTimer, autoSwapTimer;
    private RegisterUserModel userModel;
    private long timerMilliSecond = 0;

    /***
     *
     * @param activity
     * @param userModel
     */
    public RegistrationRequestDialog(@NonNull BaseActivity activity, RegisterUserModel userModel) {
        super(activity, R.style.TransApiRequestLoader);
        this.activity = activity;
        this.userModel = userModel;
        setOnDismissListener(this);
        setCancelable(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_registration_api_loading);
        titleTv = findViewById(R.id.tv_request_title);
        timerTv = findViewById(R.id.tv_request_timer);
        viewAnimator = findViewById(R.id.va_request_prize);
        progressBar = findViewById(R.id.pbar_request);
        progressBar.setVisibility(View.VISIBLE);
        timerContainerView = findViewById(R.id.ll_timer_parent);
        timerContainerView.setVisibility(View.GONE);

        retryBtn = findViewById(R.id.btn_retry);
        cancelBtn = findViewById(R.id.btn_cancel);
        showDashboardBtn = findViewById(R.id.btn_show_dashboard);

        retryBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        showDashboardBtn.setOnClickListener(this);

        retryBtn.setAlpha(0.5f);
        retryBtn.setEnabled(false);
        retryBtn.setVisibility(View.GONE);
        showDashboardBtn.setVisibility(View.GONE);

        addItemInViewAnimator();
        viewAnimator.setInAnimation(activity.getApplicationContext(), R.anim.slide_left);
        viewAnimator.setOutAnimation(activity.getApplicationContext(), R.anim.slide_left_exit);

        makeApiCallToRegisterUser(userModel, getContext());
    }


    /****
     *
     * @param userModel
     */
    private void makeApiCallToRegisterUser(RegisterUserModel userModel, Context context) {
        TambolaAPIInterface apiInterface = TambolaAPIClient.getClient(context).
                create(TambolaAPIInterface.class);

        requestRetroCall = apiInterface.userRegister(userModel);

        apiCount = 1;
        requestRetroCall.enqueue(registerUserModelCallback);
    }

    private void doOnPermanentFailure() {
        dismiss();
        new ApiMessageDialog(activity, activity.getString(R.string.alert_title_failed),
                activity.getString(R.string.alert_request_registration_failed) + "\n" + activity.getString(R.string.alert_request_sorry_for_failed)).show();

        //        CTTracker.cleverTapTrackEvent(activity.getDbApplication(),
//                CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.REGISTRATION, CTConstant.PROP_VALUE.FAILED);
        Tracking.trackGAEvent(activity, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.REGISTRATION, CTConstant.PROP_VALUE.FAILED, "");
    }

    /**
     * @param ticketId
     */
    private void doOnRegistrationSuccess(String ticketId) {
        activity.getTambolaSharedPref().saveTicketId(ticketId);

        titleTv.setText(R.string.alert_request_registration_success);
        retryBtn.setVisibility(View.GONE);
        cancelBtn.setVisibility(View.GONE);
        showDashboardBtn.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        timerContainerView.setVisibility(View.GONE);
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }

//        CTTracker.cleverTapTrackEvent(activity.getDbApplication(),
//                CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.REGISTRATION, CTConstant.PROP_VALUE.SUCCESS);
        Tracking.trackGAEvent(activity, OfferController.getInstance().getDefaultTracker(), CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.REGISTRATION, CTConstant.PROP_VALUE.SUCCESS, "");

    }

    private final Callback<RegisterUserResponseModel> registerUserModelCallback = new Callback<RegisterUserResponseModel>() {

        @Override
        public void onResponse(@NonNull Call<RegisterUserResponseModel> call, @NonNull Response<RegisterUserResponseModel> response) {
            try {
                RegisterUserResponseModel model = response.body();
                if (response.code() == TambolaConstants.API_SUCCESS && model != null) {
                    handleRegistrationApiResponse(model);
                    updateDBUserProfile();
                } else if (response.code() == TambolaConstants.API_OTP_FAILED || (model != null &&
                        model.getStatusCode() == TambolaConstants.API_OTP_FAILED)) {
                    Toast.makeText(activity, activity.getString(R.string.enter_valid_otp), Toast.LENGTH_LONG).show();
                    handleRegistrationApiFailure(this);
                } else {
                    handleRegistrationApiFailure(this);
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage() + "");
                handleRegistrationApiFailure(this);
            }
        }

        @Override
        public void onFailure(@NonNull Call<RegisterUserResponseModel> call, Throwable t) {
            Log.e(TAG, t.getMessage() + "");
            handleRegistrationApiFailure(this);
        }

        /***
         *
         * @param responseUser
         */
        private void handleRegistrationApiResponse(RegisterUserResponseModel responseUser) {
            Log.d(TAG, responseUser + "");
            doOnRegistrationSuccess(responseUser.getTicketId());
        }

        private void handleRegistrationApiFailure(Callback<RegisterUserResponseModel> callback) {
            if (apiCount < TambolaConstants.MAX_API_COUNT) {
                progressBar.setVisibility(View.GONE);
                showCountDownTimer();
                ////titleTv.setText(R.string.alert_request_registration_retry);
            } else {
                doOnPermanentFailure();
            }
        }
    };


    private void showCountDownTimer() {

        if (apiCount == 1) {
            timerMilliSecond = TambolaConstants.TIMER_MILLISECONDS_FIRST_RETRY;
        } else {
            timerMilliSecond = TambolaConstants.TIMER_MILLISECONDS_SECOND_RETRY;
        }

        timerContainerView.setVisibility(View.VISIBLE);

        countDownTimer = new CountDownTimer(timerMilliSecond, 1000) {
            @Override
            public void onTick(long l) {
                timerMilliSecond = l;
                timerTv.setText(hmsTimeFormatter(l));
            }

            @Override
            public void onFinish() {
                if (apiCount < TambolaConstants.MAX_API_COUNT) {
                    retryBtn.setAlpha(1.0f);
                    retryBtn.setEnabled(true);
                    retryBtn.setVisibility(View.VISIBLE);
                    titleTv.setText(R.string.alert_request_registration_retry);
                    timerContainerView.setVisibility(View.GONE);
                } else {
                    retryBtn.setAlpha(0.5f);
                    retryBtn.setEnabled(false);
                    titleTv.setText(R.string.alert_request_registration_failed);
                    timerContainerView.setVisibility(View.GONE);
                }
            }

            private String hmsTimeFormatter(long milliSeconds) {
                return String.format("%02d %02d",
                        TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                        TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));
            }
        }.start();
    }

    private void addItemInViewAnimator() {

        /*GamePrizeModel gamePrizeModel1 = new GamePrizeModel(R.string.label_price_early5, 0);
        GamePrizeModel gamePrizeModel2 = new GamePrizeModel(R.string.label_price_1st_line, 0);
        GamePrizeModel gamePrizeModel3 = new GamePrizeModel(R.string.label_price_2nd_line, 0);
        GamePrizeModel gamePrizeModel4 = new GamePrizeModel(R.string.label_price_3rd_line, 0);
        GamePrizeModel gamePrizeModel5 = new GamePrizeModel(R.string.label_price_full_house, 0);
        GamePrizeModel gamePrizeModel6 = new GamePrizeModel(R.string.label_price_corner, 0);

        List<GamePrizeModel> gamePrizeModelList = new ArrayList<>();
        gamePrizeModelList.add(gamePrizeModel5);
        gamePrizeModelList.add(gamePrizeModel2);
        gamePrizeModelList.add(gamePrizeModel3);
        gamePrizeModelList.add(gamePrizeModel4);
        gamePrizeModelList.add(gamePrizeModel6);
        gamePrizeModelList.add(gamePrizeModel1);

        LayoutInflater inflater = getLayoutInflater();

        for (GamePrizeModel prizeModel : gamePrizeModelList) {
            View view = inflater.inflate(R.layout.item_register_api_view_animator_price, null);
            ImageView priceImageIv = view.findViewById(R.id.iv_price_image);
            TextView priceNameTv = view.findViewById(R.id.tv_price_name);

            priceImageIv.setBackgroundResource(prizeModel.getImageResId());
            priceNameTv.setText(prizeModel.getTxtResId());
            viewAnimator.addView(view);
        }*/

        LayoutInflater inflater = getLayoutInflater();
        String[] gameRules = activity.getResources().getStringArray(R.array.game_niyams);

        for (String gameRule : gameRules) {
            View view = inflater.inflate(R.layout.item_register_api_view_animator_game_rules, null);
            TextView gameRuleTv = view.findViewById(R.id.tv_game_rule);
            gameRuleTv.setText(gameRule);
            viewAnimator.addView(view);
        }
        addAutoSwap();
    }

    private void addAutoSwap() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                autoSwapTimer = new CountDownTimer(Long.MAX_VALUE, TambolaConstants.AUTO_SWAP_INTERVAL) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        viewAnimator.showNext();
                    }

                    @Override
                    public void onFinish() {
                    }
                }.start();
            }
        }, TambolaConstants.AUTO_SWAP_INTERVAL);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_retry) {
            timerContainerView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            requestRetroCall = requestRetroCall.clone();
            requestRetroCall.enqueue(registerUserModelCallback);
            apiCount++;
            retryBtn.setAlpha(0.5f);
            retryBtn.setEnabled(false);
            titleTv.setText(R.string.alert_request_registration_processing);
        } else if (id == R.id.btn_cancel) {
            dismiss();
        } else if (id == R.id.btn_show_dashboard) {
            TambolaDashboardActivity.startActivity(activity, null);
            dismiss();
            activity.finish();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (requestRetroCall != null) {
            requestRetroCall.cancel();
        }
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        if (autoSwapTimer != null) {
            autoSwapTimer.cancel();
        }
    }

    private void updateDBUserProfile() {
        try {
           /* OfferApplication dbApplication = (OfferApplication) activityContext.getApplication();
            UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest();

            updateProfileRequest.setName(userModel.getName());
            updateProfileRequest.setMail(userModel.getEmail());
            updateProfileRequest.setMobileNo(userModel.getMobile_no());
            updateProfileRequest.setDob(userModel.getDob());
            updateProfileRequest.setGender(userModel.getGender());

            UserDataApiProvider.getInstance(dbApplication).updateProfile(updateProfileRequest, new RetroApiListener<ProfileSuccessResponse>() {
                @Override
                public void onApiSuccess(@NonNull ProfileSuccessResponse response) {
                    if (response.getData().getProfile() != null) {
                        DBSharedPref dbSharedPref = dbApplication.getDbSharedPref();
                        dbSharedPref.updateUserProfile(response.getData().getProfile());
                    }
                }

                @Override
                public void onApiFailed(@NonNull String message) {

                }
            });*/
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }
}
