/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.tambola.htmlCompact;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.webkit.WebView;

import androidx.annotation.NonNull;

import com.db.offers.tambola.utils.DbColorThemeHelper;


public class HtmlConverterUtil {
    private static String mCommonJs = "";
    private static String mContent = "";
    private static String mSubHeads = "";



    @NonNull
    public static SpannableStringBuilder getSpannedSlugForTopNews(String slug, String color, float slugProportion) {
        String slugStr = String.format("%s / ", slug);
        SpannableString string = new SpannableString(slugStr);
        string.setSpan(new ForegroundColorSpan(Color.parseColor("#" + (TextUtils.isEmpty(color) ? "F89C1B" : color))), 0, slugStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(string);
        ssBuilder.setSpan(new RelativeSizeSpan(slugProportion), 0, string.length(), 0);
        ssBuilder.setSpan(new StyleSpan(Typeface.BOLD),
                0, string.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        return ssBuilder;
    }



    @NonNull
    public static SpannableStringBuilder getSpannedSlugFromColor(String slug, String color, float slugProportion) {
        String slugStr = String.format("%s/ ", slug);
        SpannableString string = new SpannableString(slugStr);

        string.setSpan(new ForegroundColorSpan(Color.parseColor("#" + (color == null ? "F89C1B" : color))), 0,
                slugStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(string);
        ssBuilder.setSpan(new RelativeSizeSpan(slugProportion), 0, string.length(), 0);
        ssBuilder.setSpan(new StyleSpan(Typeface.BOLD),
                0, string.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        return ssBuilder;
    }

    public static void setHeaderAndLoadDataWithoutSlug(WebView view, String heading) {
        if (view == null) return;
        view.loadDataWithBaseURL("file:///android_asset/", getStyledFont(
                heading)
                , "text/html", "UTF-8", null);
    }

    public static String setSlugIntro(String heading, String fontSize) {
        String font = String.format("%spx", fontSize);
        return "<style> .darwin-article, .darwin-article p, .darwin-article li {" +
                "font-size: " + font + ";}+</style>" +
                "<div class='darwin-article'>" + heading +
                "</div>";
    }



    public static String getStyledFont(String html) {
        if (!TextUtils.isEmpty(html)) {
            boolean addBodyStart = !html.toLowerCase().contains("<body>");
            boolean addBodyEnd = !html.toLowerCase().contains("</body");
            return (addBodyStart ? "<body>" : "") + html + (addBodyEnd ? "</body>" : "");
        }
        return "";
    }


    public static String setAppropriateColorToDescription(String articleIntro, String mCategoryColor, String fontSize) {
        String color = mCategoryColor != null ? mCategoryColor : "FC23DR";
        String font = String.format("%spx", fontSize);
        String headinkStyle = "<style>" + mContent + ".darwin-article li::before{" +
                "        color: #" + color + "; " +
                "    }" +
                ".darwin-color-marker:before{" +
                " color: #" + color + ";" +
                "}" +
                ".darwin-color-marker-bg:before{" +
                " background-color: #" + color + ";" +
                "}" +
                ".darwin-color-text{" +
                " color: #" + color + ";" +
                "}" +
                ".darwin-color-bg{" +
                " background-color: #" + color + ";" +
                "}" + ".darwin-article, .darwin-article p, .darwin-article li {" +
                " font-size: " + font + " ;" +
                "} .darwin-article blockquote::before{" +
                "background-color: #" + color +
                "} </style>" +
                "<script>" + mCommonJs + "</script>" +
                "<script>init('" + color + "')</script>";

        // Below code is added for checking css of news header & pointers to change font size
        String newsHeader = "font-size: 18px;line-height: 1.3;font-family: '';";
        if (articleIntro.contains(newsHeader)) {
            articleIntro = articleIntro.replaceAll(newsHeader, "font-size: " + font + ";line-height: 1.3;font-family: '';");
        }
        String returnText = "<div class='darwin-article darwin-content'>" + articleIntro + "</div>";
        return headinkStyle + returnText;
    }


    /***
     *
     * @param bullet
     * @param mCategoryColor
     * @param fontSize
     * @return
     */
    public static String setBulletPreview(String bullet, String mCategoryColor, String fontSize) {
        String font = String.format("%spx", fontSize);
        String color = mCategoryColor != null ? mCategoryColor : "FC23DR";
        String formatted = "<style>" + mContent + ".darwin-article li::before{" +
                "        color: #" + color + "; " +
                "    }" +
                ".darwin-color-marker:before{" +
                " color: #" + color + ";" +
                "}" +
                ".darwin-color-marker-bg:before{" +
                " background-color: #" + color + ";" +
                "}" +
                ".darwin-color-text{" +
                " color: #" + color + ";" +
                "}" +
                ".darwin-color-bg{" +
                " background-color: #" + color + ";" +
                "} .darwin-article, .darwin-article p, .darwin-article li {" +
                "font-size: " + font + ";" +
                "} .darwin-article blockquote::before{" +
                "background-color: #" + color +
                "} </style>" +
                "<script>" + mCommonJs + "</script>" +
                "<script>init('" + color + "')</script>";
        String htmlWrapper = "<div class='darwin-article darwin-subheads'>" + bullet +
                "</div>";
        return formatted + htmlWrapper;
    }
}