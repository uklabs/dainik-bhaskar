package com.db.offers.tambola.htmlCompact;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.BulletSpan;
import android.util.Log;
import android.util.TypedValue;

import androidx.core.content.ContextCompat;


import com.db.offers.R;
import com.db.offers.tambola.utils.BulletSpanWithRadius;

import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;

//import static com.comscore.android.util.log.AndroidLogger.TAG;

/**
 * This class is used to handle custom html tags which is not handled by default android framework
 * <p>
 * Created on 29/06/2018
 *
 * @author Sathya
 */
public class HtmlHandler {

    private static final String TAG = HtmlHandler.class.getName();
    private Context mContext;
    private int mBulletGapWidth, mColor = Color.parseColor("#F89C1B"); // set by default;
    private boolean mUseNative;
    private String mSource;

    public HtmlHandler(Context context, boolean useNative, String source, String catId) {
        mContext = context;
        mUseNative = useNative;
        mSource = source;
        try {
            if (!TextUtils.isEmpty(catId)) {
//                String categoryColor = DbColorThemeHelper.getInstance().getCategoryColor(catId);
                String categoryColor = "";
                mColor = Color.parseColor(
                        "#" + (TextUtils.isEmpty(categoryColor) ? "F89C1B" : categoryColor));
            } else {
                mColor = Color.parseColor("#F89C1B");
            }
        } catch (Exception e) {
            Log.e(TAG, "HtmlHandler: " + e.getMessage());
        }
    }

    public HtmlHandler(Context context, String source, String topNewsColor) {
        mContext = context;
        mUseNative = false;
        mSource = source;
        mColor = Color.parseColor(
                "#" + (TextUtils.isEmpty(topNewsColor) ? "F89C1B" : topNewsColor));
    }

    public Spanned getHtml() {
        return handleHtmlTag();
    }

    private Spanned handleHtmlTag() {
        mSource = mSource.replaceAll("<h1>", " ");
        mSource = mSource.replaceAll("<h2>", " ");
        mSource = mSource.replaceAll("<h3>", " ");
        mSource = mSource.replaceAll("</h1>", " ");
        mSource = mSource.replaceAll("</h2>", " ");
        mSource = mSource.replaceAll("</h3>", " ");
        Spanned fromHtml = null;
        mBulletGapWidth = mContext.getResources().getDimensionPixelOffset(R.dimen.bullet_gap_width);
        if (mUseNative) {
            Html.ImageGetter imageGetter = new Html.ImageGetter() {
                @Override
                public Drawable getDrawable(String source) {
                    return HtmlHandler.this.getDrawable(source, null);
                }
            };

            Html.TagHandler tagHandler = new Html.TagHandler() {
                @Override
                public void handleTag(boolean opening, String tag, Editable output,
                                      XMLReader xmlReader) {
                    HtmlHandler.this.handleTag(opening, tag, null, output, xmlReader);
                }
            };
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                fromHtml = Html.fromHtml(mSource, 0, null, tagHandler);
            } else {
                fromHtml = Html.fromHtml(mSource, null, tagHandler);
            }
        } else {
            HtmlCompat.ImageGetter imageGetter = new HtmlCompat.ImageGetter() {
                @Override
                public Drawable getDrawable(String source, Attributes attributes) {
                    return HtmlHandler.this.getDrawable(source, attributes);
                }
            };
            HtmlCompat.TagHandler tagHandler = new HtmlCompat.TagHandler() {
                @Override
                public void handleTag(boolean opening, String tag, Attributes attributes,
                                      Editable output, XMLReader xmlReader) {
                    HtmlHandler.this.handleTag(opening, tag, attributes, output, xmlReader);
                }
            };
            HtmlCompat.SpanCallback spanCallback = new HtmlCompat.SpanCallback() {
                @Override
                public Object onSpanCreated(String tag, Object span) {
                    if (span instanceof BulletSpan) {
                        return new BulletSpanWithRadius(10, mBulletGapWidth, mColor);
                    }
                    return span;
                }
            };
            fromHtml = HtmlCompat.fromHtml(mContext, mSource,
                    HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM,
                    null, tagHandler, spanCallback);
            if (fromHtml != null) {
                fromHtml = trim(fromHtml, 0, fromHtml.length());
            }
        }
        return fromHtml;
    }

    private Drawable getDrawable(String source, Attributes attributes) {
        Resources resources = mContext.getResources();
        int drawableId = resources.getIdentifier(source, "drawable", mContext.getPackageName());
        Drawable drawable = ContextCompat.getDrawable(mContext, drawableId);
        if (drawable != null) {
            int width, height;
            if (attributes == null) {
                width = drawable.getIntrinsicWidth();
                height = drawable.getIntrinsicHeight();
            } else {
                width = Integer.parseInt(attributes.getValue("width"));
                width = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, resources.getDisplayMetrics()));
                float ratio = (float) width / (float) drawable.getIntrinsicWidth();
                height = (int) (drawable.getIntrinsicHeight() * ratio);
            }
            drawable.setBounds(0, 0, width, height);
        }
        return drawable;
    }

    private void handleTag(boolean opening, String tag, Attributes attributes, Editable output,
                           XMLReader xmlReader) {
        // Manipulate the output here for otherwise unsupported tags
//        Log.d(TAG, "Unhandled tag <" + tag + ">");
    }

    public static Spanned trim(CharSequence s, int start, int end) {
        while (start < end && Character.isWhitespace(s.charAt(start))) {
            start++;
        }

        while (end > start && Character.isWhitespace(s.charAt(end - 1))) {
            end--;
        }
        return (Spanned) s.subSequence(start, end);
    }
}
