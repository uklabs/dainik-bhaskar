package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

public class AllNumberModel implements GsonProguardMarker {

    @SerializedName("number")
    public String number;
    @SerializedName("code")
    public String code;
    @SerializedName("created")
    public String created;
    @SerializedName("validDate")
    public String validDate;

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public AllNumberModel(String textNumber) {
        this.number = textNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "AllNumberModel{" +
                "number='" + number + '\'' +
                ", code='" + code + '\'' +
                ", created='" + created + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AllNumberModel) {
            return number.equals(((AllNumberModel) obj).number);
        } else {
            return super.equals(obj);
        }
    }
}
