package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllNumbersResponseModel implements GsonProguardMarker {
    @SerializedName("status_code")
    public String status_code;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<DataModel> data;

    public class DataModel implements GsonProguardMarker {
        @SerializedName("numbers")
        public String numbers;
        @SerializedName("code")
        public String code;
        @SerializedName("created")
        public String created;
        @SerializedName("validdt")
        public String validdt;

        public String getValiddt() {
            return validdt;
        }

        public String getNumbers() {
            return numbers;
        }

        public void setNumbers(String numbers) {
            this.numbers = numbers;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }
    }
}
