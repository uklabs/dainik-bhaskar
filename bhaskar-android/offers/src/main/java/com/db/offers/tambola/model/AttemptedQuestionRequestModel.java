package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttemptedQuestionRequestModel implements GsonProguardMarker {
    @SerializedName("qid")
    @Expose
    private int qid;
    @SerializedName("option_id")
    @Expose
    private int optionId;

    public AttemptedQuestionRequestModel(int qid, int optionId) {
        this.qid = qid;
        this.optionId = optionId;
    }

    public int getQid() {
        return qid;
    }

    public void setQid(int qid) {
        this.qid = qid;
    }

    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }
}
