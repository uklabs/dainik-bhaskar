package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttemptedQuestionResponseModel implements GsonProguardMarker {
    @SerializedName("status_code")
    @Expose
    private int statusCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("attempt_status")
    @Expose
    private AttemptedQuestionStatusModel attemptedQuestionStatusModel;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AttemptedQuestionStatusModel getAttemptedQuestionStatusModel() {
        return attemptedQuestionStatusModel;
    }

    public void setAttemptedQuestionStatusModel(AttemptedQuestionStatusModel attemptedQuestionStatusModel) {
        this.attemptedQuestionStatusModel = attemptedQuestionStatusModel;
    }

    public class AttemptedQuestionStatusModel implements GsonProguardMarker{
        @SerializedName("attempt_code")
        @Expose
        private int attemptCode;
        @SerializedName("attempt_message")
        @Expose
        private String attemptMessage;

        public int getAttemptCode() {
            return attemptCode;
        }

        public void setAttemptCode(int attemptCode) {
            this.attemptCode = attemptCode;
        }

        public String getAttemptMessage() {
            return attemptMessage;
        }

        public void setAttemptMessage(String attemptMessage) {
            this.attemptMessage = attemptMessage;
        }
    }
}


