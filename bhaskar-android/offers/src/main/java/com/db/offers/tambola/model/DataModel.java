package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataModel implements GsonProguardMarker {

    @SerializedName("ticket_details")
    public String ticket_details;
    @SerializedName("winner_list")
    public String winnerList;
    @SerializedName("opted_numbers")
    public List<String> opted_numbers;

    @SerializedName("sponsor_details")
    private SponsorDetail sponsorDetail;

    @SerializedName("eligibility_status")
    public EligibilityStatus eligibilityStatus;

    public SponsorDetail getSponsorDetails() {
        return sponsorDetail;
    }

    @Override
    public String toString() {
        return "DataModel{" +
                "ticket_details='" + ticket_details + '\'' +
                ", winnerList='" + winnerList + '\'' +
                ", opted_numbers=" + opted_numbers +
                ", sponsorDetail=" + sponsorDetail +
                ", eligibilityStatus=" + eligibilityStatus +
                '}';
    }
}
