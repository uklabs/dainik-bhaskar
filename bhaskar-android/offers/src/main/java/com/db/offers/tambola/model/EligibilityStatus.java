package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EligibilityStatus implements GsonProguardMarker {

    @SerializedName("early_five")
    @Expose
    private String earlyFive;
    @SerializedName("corners")
    @Expose
    private String corners;
    @SerializedName("top_line")
    @Expose
    private String topLine;
    @SerializedName("middle_line")
    @Expose
    private String middleLine;
    @SerializedName("bottom_line")
    @Expose
    private String bottomLine;
    @SerializedName("full_house")
    @Expose
    private String fullHouse;

    public String getEarlyFive() {
        return earlyFive;
    }


    public String getCorners() {
        return corners;
    }


    public String getTopLine() {
        return topLine;
    }


    public String getMiddleLine() {
        return middleLine;
    }


    public String getBottomLine() {
        return bottomLine;
    }


    public String getFullHouse() {
        return fullHouse;
    }

    @Override
    public String toString() {
        return "EligibilityStatus{" +
                "earlyFive='" + earlyFive + '\'' +
                ", corners='" + corners + '\'' +
                ", topLine='" + topLine + '\'' +
                ", middleLine='" + middleLine + '\'' +
                ", bottomLine='" + bottomLine + '\'' +
                ", fullHouse='" + fullHouse + '\'' +
                '}';
    }
}
