package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

public class Game implements GsonProguardMarker {
    @SerializedName("name")
    private String name;
    @SerializedName("claimed")
    private String claimed;
    @SerializedName("availiable")
    private String availiable;
    @SerializedName("viewcolor")
    private int viewcolor;

    public String getName() {
        return name;
    }

    public void setName(String prize) {
        this.name = prize;
    }

    public String getClaimed() {
        return claimed;
    }

    public void setClaimed(String claimed) {
        this.claimed = claimed;
    }

    public String getAvailiable() {
        return availiable;
    }

    public void setAvailiable(String availiable) {
        this.availiable = availiable;
    }

    public int getViewcolor() {
        return viewcolor;
    }

    public void setViewcolor(int viewcolor) {
        this.viewcolor = viewcolor;
    }
}
