package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GameStatusResponseModel implements GsonProguardMarker {

    @SerializedName("status_code")
    public String status_code;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public Data data;

    public class Data implements GsonProguardMarker {
        @SerializedName("Winners")
        public List<WinnerModel> Winners;
    }

    public class WinnerModel implements GsonProguardMarker{
        @SerializedName("name")
        public String name;
        @SerializedName("claimed")
        public String claimed;
        @SerializedName("available")
        public String available;
    }
}
