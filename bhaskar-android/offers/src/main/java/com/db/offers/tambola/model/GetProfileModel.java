package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

public class GetProfileModel implements GsonProguardMarker {

    @SerializedName("status_code")
    public String status_code;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public DataModel data;

    public class DataModel implements GsonProguardMarker{
        @SerializedName("ticket_id")
        public String ticket_id;
        @SerializedName("name")
        public String name;
        @SerializedName("email")
        public String email;
        @SerializedName("device_id")
        public String device_id;
        @SerializedName("gender")
        public String gender;
        @SerializedName("dob")
        public String dob;
        @SerializedName("mobile_no")
        public String mobile_no;
        @SerializedName("photo")
        public String photo;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
    }
}
