package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuestionAttemptStatusModel implements GsonProguardMarker {
    @SerializedName("attempt_code")
    @Expose
    private int attemptCode;
    @SerializedName("attempt_message")
    @Expose
    private String attemptMessage;
    @SerializedName("notice")
    @Expose
    private String notice;
    @SerializedName("quiz")
    @Expose
    private Quiz quiz;

    public int getAttemptCode() {
        return attemptCode;
    }

    public void setAttemptCode(int attemptCode) {
        this.attemptCode = attemptCode;
    }

    public String getAttemptMessage() {
        return attemptMessage;
    }

    public void setAttemptMessage(String attemptMessage) {
        this.attemptMessage = attemptMessage;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public class Quiz implements GsonProguardMarker {
        @SerializedName("Question")
        @Expose
        private Question question;
        @SerializedName("options")
        @Expose
        private List<Option> options = null;

        public Question getQuestion() {
            return question;
        }

        public void setQuestion(Question question) {
            this.question = question;
        }

        public List<Option> getOptions() {
            return options;
        }

        public void setOptions(List<Option> options) {
            this.options = options;
        }

        public class Question implements GsonProguardMarker {
            @SerializedName("qid")
            @Expose
            private int qid;
            @SerializedName("detail")
            @Expose
            private String detail;

            public int getQid() {
                return qid;
            }

            public void setQid(int qid) {
                this.qid = qid;
            }

            public String getDetail() {
                return detail;
            }

            public void setDetail(String detail) {
                this.detail = detail;
            }
        }

        public class Option implements GsonProguardMarker {
            @SerializedName("qid")
            @Expose
            private int qid;
            @SerializedName("option_id")
            @Expose
            private int optionId;
            @SerializedName("option")
            @Expose
            private String option;

            public int getQid() {
                return qid;
            }

            public void setQid(int qid) {
                this.qid = qid;
            }

            public int getOptionId() {
                return optionId;
            }

            public void setOptionId(int optionId) {
                this.optionId = optionId;
            }

            public String getOption() {
                return option;
            }

            public void setOption(String option) {
                this.option = option;
            }
        }
    }
}
