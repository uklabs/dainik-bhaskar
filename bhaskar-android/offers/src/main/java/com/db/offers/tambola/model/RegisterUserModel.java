package com.db.offers.tambola.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.db.offers.common.GsonProguardMarker;
import com.db.offers.common.ParcelableInterface;
import com.google.gson.annotations.SerializedName;

public class RegisterUserModel implements ParcelableInterface, GsonProguardMarker {
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("gender")
    private String gender;
    @SerializedName("mobile_no")
    private String mobile_no;
    @SerializedName("dob")
    private String dob;
    @SerializedName("otp")
    private String otp;
    @SerializedName("ticket_id")
    private String ticket_id;
    @SerializedName("message")
    private String message;

    /***
     *
     * @param name
     * @param email
     * @param gender
     * @param mobile_no
     * @param dob
     * @param otp
     */
    public RegisterUserModel(String name, String email, String gender, String mobile_no, String dob, String otp) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.mobile_no = mobile_no;
        this.dob = dob;
        this.otp = otp;
    }

    public RegisterUserModel(Parcel parcel) {
        readFromParcel(parcel);
    }

    @Override
    public void readFromParcel(Parcel source) {
        name = source.readString();
        email = source.readString();
        gender = source.readString();
        mobile_no = source.readString();
        dob = source.readString();
        otp = source.readString();
        ticket_id = source.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(gender);
        dest.writeString(mobile_no);
        dest.writeString(dob);
        dest.writeString(otp);
        dest.writeString(ticket_id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(String ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }


    public static final Parcelable.Creator<RegisterUserModel> CREATOR = new Parcelable.Creator<RegisterUserModel>() {
        @Override
        public RegisterUserModel createFromParcel(Parcel in) {
            return new RegisterUserModel(in);
        }

        @Override
        public RegisterUserModel[] newArray(int size) {
            return new RegisterUserModel[size];
        }
    };

    @Override
    public String toString() {
        return "RegisterUserModel{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", mobile_no='" + mobile_no + '\'' +
                ", dob='" + dob + '\'' +
                ", otp='" + otp + '\'' +
                ", ticket_id='" + ticket_id + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
