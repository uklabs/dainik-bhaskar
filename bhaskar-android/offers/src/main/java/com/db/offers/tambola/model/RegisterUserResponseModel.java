package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

public final class RegisterUserResponseModel implements GsonProguardMarker {
    @SerializedName("ticket_id")
    private String ticket_id;
    @SerializedName("status_code")
    private int status_code;
    @SerializedName("message")
    private String message;

    public int getStatusCode() {
        return status_code;
    }

    public String getMessage() {
        return message;
    }

    public String getTicketId() {
        return ticket_id;
    }

    @Override
    public String toString() {
        return "RegisterUserResponseModel{" +
                "ticket_id='" + ticket_id + '\'' +
                ", status_code=" + status_code +
                ", message='" + message + '\'' +
                '}';
    }
}
