package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

public class ScanQRModel implements GsonProguardMarker {
    @SerializedName("code")
    public String code;
    @SerializedName("numbers")
    public String numbers;
    @SerializedName("message")
    public String message;

    public ScanQRModel(String code, String numbers) {
        this.code = code;
        this.numbers = numbers;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
