package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class ScanResultModel implements GsonProguardMarker {

    @SerializedName("status_code")
    @Expose
    private int statusCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ScanResultModel{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public class Data implements GsonProguardMarker {
        @SerializedName("eligibility_status")
        @Expose
        private EligibilityStatus eligibilityStatus;

        public EligibilityStatus getEligibilityStatus() {
            return eligibilityStatus;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "eligibilityStatus=" + eligibilityStatus +
                    '}';
        }
    }
}
