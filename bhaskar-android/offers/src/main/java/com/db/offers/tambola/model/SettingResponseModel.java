package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SettingResponseModel implements GsonProguardMarker {
    @SerializedName("status_code")
    public String status_code;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<DataModel> data;

    public class DataModel {
        @SerializedName("key")
        public String key;
        @SerializedName("value")
        public String value;
    }
}
