package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SponsorDetail implements GsonProguardMarker {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("inlink")
    @Expose
    private boolean inlink;
    @SerializedName("status")
    @Expose
    private boolean status;

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getLink() {
        return link;
    }

    public boolean getInlink() {
        return inlink;
    }

    public boolean getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "SponsorDetail{" +
                "name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", link='" + link + '\'' +
                ", inlink=" + inlink +
                ", status='" + status + '\'' +
                '}';
    }
}
