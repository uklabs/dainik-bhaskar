package com.db.offers.tambola.model;


import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

public class TodayNumberModel implements GsonProguardMarker {
    @SerializedName("status_code")
    public String status_code;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public Data data;
    @SerializedName("today_date")
    private String todayDate;
    @SerializedName("tambola_launch_status")
    public boolean tambolaLaunchStatus;

    public class Data implements GsonProguardMarker {
        @SerializedName("code")
        public String code;
        @SerializedName("numbers")
        public String numbers;

        @Override
        public String toString() {
            return "Data{" +
                    "code='" + code + '\'' +
                    ", numbers='" + numbers + '\'' +
                    '}';
        }
    }

    public String getTodayDate() {
        return todayDate;
    }

    @Override
    public String toString() {
        return "TodayNumberModel{" +
                "status_code='" + status_code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", todayDate='" + todayDate + '\'' +
                ", tambolaLaunchStatus=" + tambolaLaunchStatus +
                '}';
    }
}
