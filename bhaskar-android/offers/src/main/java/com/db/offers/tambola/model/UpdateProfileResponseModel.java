package com.db.offers.tambola.model;


import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.SerializedName;

public class UpdateProfileResponseModel implements GsonProguardMarker {
    @SerializedName("status_code")
    public String status_code;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public String data;
}
