package com.db.offers.tambola.model;

import com.db.offers.common.GsonProguardMarker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserExistsModel implements GsonProguardMarker {
    @SerializedName("status_code")
    @Expose
    private int statusCode;
    @SerializedName("ticket_id")
    @Expose
    private String ticketId;
    @SerializedName("tambola_status")
    @Expose
    private String tambolaStatus;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("attempt_status")
    @Expose
    private QuestionAttemptStatusModel questionAttemptStatusModel;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getTambolaStatus() {
        return tambolaStatus;
    }

    public void setTambolaStatus(String tambolaStatus) {
        this.tambolaStatus = tambolaStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public QuestionAttemptStatusModel getQuestionAttemptStatusModel() {
        return questionAttemptStatusModel;
    }

    public void setQuestionAttemptStatusModel(QuestionAttemptStatusModel questionAttemptStatusModel) {
        this.questionAttemptStatusModel = questionAttemptStatusModel;
    }

    @Override
    public String toString() {
        return "UserExistsModel{" +
                "statusCode=" + statusCode +
                ", ticketId=" + ticketId +
                ", tambolaStatus='" + tambolaStatus + '\'' +
                ", message='" + message + '\'' +
                ", questionAttemptStatusModel=" + questionAttemptStatusModel +
                '}';
    }
}


