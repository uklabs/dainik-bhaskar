package com.db.offers.tambola.network;

import android.content.Context;

import com.bhaskar.util.LoginController;
import com.bhaskar.data.model.ProfileInfo;
import com.db.offers.BuildConfig;
import com.db.offers.common.UrlUtil;
import com.db.offers.tambola.constant.TambolaConstants;
import com.db.offers.tambola.utils.TambolaSharedPref;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hp on 02-06-2018.
 */

public class TambolaAPIClient implements TambolaConstants.HeaderKeys, TambolaConstants.HeaderValues {

    private static Retrofit retrofit;


    /**
     * @param context
     * @return
     */
    public static Retrofit getClient(final Context context) {

        if (retrofit != null) {
            return retrofit;
        }

        final TambolaSharedPref tambolaSharedPref = new TambolaSharedPref(context);
        String device_id = TrackingData.getDeviceId(context);

        String dbId = TrackingData.getDBId(context);

        String finalDbId = (dbId == null) ? "" : dbId;

        StethoInterceptor stethoInterceptor = null;
        if (BuildConfig.DEBUG) {
            // set your desired log level
            stethoInterceptor = new StethoInterceptor();
        }

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        final String ticketId = tambolaSharedPref.getTicketId();
                        ProfileInfo profileInfo = LoginController.loginController().getCurrentUser();
                        String uuid = (profileInfo==null)?"":profileInfo.uuID;
                        Request request = chain.request();
                        Request.Builder builder = request.newBuilder()
                                .addHeader(KEY_ACCEPT, VALUE_ACCEPT)
                                .addHeader(KEY_CHANNEL, VALUE_CHANNEL)
                                .addHeader(KEY_DEVICE_TYPE, VALUE_DEVICE_TYPE)
                                .addHeader(KEY_CONTENT_TYPE, VALUE_CONTENT_TYPE)
                                .addHeader(KEY_AUTHORIZATION, "")
                                .addHeader(KEY_DB_ID, finalDbId)
                                .addHeader(KEY_X_CSRF_TOKEN, "")
                                .addHeader(KEY_UUID, uuid)
                                .addHeader(KEY_APPID, BHASKAR_APP_ID)
                                .addHeader(KEY_DEVICE_ID, device_id)
                                .addHeader(KEY_TICKET_ID, ticketId)
                                .method(request.method(), request.body());
                        return chain.proceed(builder.build());
                    }
                })
                .readTimeout(TambolaConstants.API_READ_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TambolaConstants.API_CONNECT_TIMEOUT, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            okHttpBuilder.addNetworkInterceptor(stethoInterceptor);
        }

        OkHttpClient okHttpClient = okHttpBuilder.build();
        retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(UrlUtil.getTambolaBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient()
                        .create()))
                .build();
        return retrofit;
    }
}
