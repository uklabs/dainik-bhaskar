package com.db.offers.tambola.network;

import com.db.offers.tambola.model.AllNumbersResponseModel;
import com.db.offers.tambola.model.AttemptedQuestionRequestModel;
import com.db.offers.tambola.model.AttemptedQuestionResponseModel;
import com.db.offers.tambola.model.GameStatusResponseModel;
import com.db.offers.tambola.model.GetProfileModel;
import com.db.offers.tambola.model.GetTicketModel;
import com.db.offers.tambola.model.RegisterUserModel;
import com.db.offers.tambola.model.RegisterUserResponseModel;
import com.db.offers.tambola.model.ScanQRModel;
import com.db.offers.tambola.model.ScanResultModel;
import com.db.offers.tambola.model.SettingResponseModel;
import com.db.offers.tambola.model.TodayNumberModel;
import com.db.offers.tambola.model.UpdateProfileResponseModel;
import com.db.offers.tambola.model.UserExistsModel;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface TambolaAPIInterface {

    String DB_TAMBOLA_APPEND_URL = "";

    @GET(DB_TAMBOLA_APPEND_URL + "api/v1/get-ticket")
    Call<GetTicketModel> doGetTicketList();

    @GET(DB_TAMBOLA_APPEND_URL + "api/v2/is-user-exists")
    Call<UserExistsModel> is_user_exists();

    @POST(DB_TAMBOLA_APPEND_URL + "api/v2/quiz")
    Call<AttemptedQuestionResponseModel> doPostAttemptedQuestion(@Body AttemptedQuestionRequestModel requestModel);

    @GET(DB_TAMBOLA_APPEND_URL + "api/v2/todays-number")
    Call<TodayNumberModel> doGetTodayNumber();

    @GET(DB_TAMBOLA_APPEND_URL + "api/v1/profile")
    Call<GetProfileModel> getProfile();


    @POST(DB_TAMBOLA_APPEND_URL + "api/v1/user-register")
    Call<RegisterUserResponseModel> userRegister(@Body RegisterUserModel user);

    @POST(DB_TAMBOLA_APPEND_URL + "api/v1/scan-code")
    Call<ScanResultModel> scanQR(@Body ScanQRModel user);

    @Multipart
    @POST(DB_TAMBOLA_APPEND_URL + "api/v1/user-profile")
    Call<UpdateProfileResponseModel> uploadProfileImage(@Part("photo\"; filename=\"myfile.jpg\" ") RequestBody photo, @Part("name") RequestBody name, @Part("email") RequestBody email, @Part("mobile_no") RequestBody mobile_no, @Part("dob") RequestBody dob, @Part("gender") RequestBody gender);

    @GET(DB_TAMBOLA_APPEND_URL + "api/v2/claim-status")
    Call<GameStatusResponseModel> doGetClaimedList();

    @GET(DB_TAMBOLA_APPEND_URL + "api/v1/number-board")
    Call<AllNumbersResponseModel> doGetNumberBoardList();

    @GET(DB_TAMBOLA_APPEND_URL + "api/v1/settings")
    Call<SettingResponseModel> doGetSettingData();

}
