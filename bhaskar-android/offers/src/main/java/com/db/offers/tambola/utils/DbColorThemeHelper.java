/*
 * Copyright © 2018, DB Corp,
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.db.offers.tambola.utils;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import androidx.core.graphics.drawable.DrawableCompat;


import com.db.offers.common.Menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DbColorThemeHelper {

    private static DbColorThemeHelper mInstance;
    private String defaultCategoryColor = "F89C1B";
    private HashMap<String, String> mCategoryColorMap = new HashMap<>();

    public static DbColorThemeHelper getInstance() {
        if (mInstance == null) {
            mInstance = new DbColorThemeHelper();
        }
        return mInstance;
    }

    public String getDefaultCategoryColor() {
        if (TextUtils.isEmpty(defaultCategoryColor)) {
            defaultCategoryColor = "F89C1B";
        }
        return defaultCategoryColor;
    }



    private boolean isValidColorString(String color) {
        return color != null && color.length() == 6;
    }

    public void setImageColor(Drawable drawable, String color) {
        if (color == null || color.length() < 1) {
            color = "000000";
        }
        DrawableCompat.setTint(drawable, Color.parseColor(String.format("#%s", color)));
    }

    public void setTextColorToTexView(TextView textView, String color) {
        if (color == null || color.length() < 1) {
            color = "000000";
        }
        textView.setTextColor(Color.parseColor(String.format("#%s", color)));
    }



}
