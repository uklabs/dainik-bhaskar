package com.db.offers.tambola.utils;

public interface TambolaQuestionCallback {

    void onAnswerCorrect();

    void onAnswerInCorrect();

    void onNotAnswered();
}
