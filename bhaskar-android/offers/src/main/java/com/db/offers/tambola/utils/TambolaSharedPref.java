package com.db.offers.tambola.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.db.offers.common.SharedPrefUtil;
import com.db.offers.tambola.model.QuestionAttemptStatusModel;


public final class TambolaSharedPref {

    private final String KEY_DB_ID = "key_db_id";
    private final String KEY_TICKET_ID = "key_ticker_id";
    private final String KEY_SCANNED_DATE = "key_scanned_date";
    private final String KEY_USER_LEAVED_DATE = "key_user_leaved_date";
    private final String KEY_BINGO_SHARED_PREF = "BINGO_SHARED_PREF_V2";

    private final String KEY_SHARE_TEXT = "KEY_SHARE_TEXT";
    private final String KEY_WINNER_NOTICE = "KEY_WINNER_NOTICE";
    private final String KEY_GAMESTATUS_NOTICE = "KEY_GAMESTATUS_NOTICE";
    private final String KEY_QUESTION_STATUS = "key_question_status";
    private final String KEY_QUESTION_MODEL = "key_question_model";


    private final SharedPreferences sp;

    /**
     * @param context
     */
    public TambolaSharedPref(Context context) {
        sp = context.getSharedPreferences(KEY_BINGO_SHARED_PREF, Context.MODE_PRIVATE);
    }

    /**
     * @param dbId
     */
    public void saveDbId(String dbId) {
        SharedPrefUtil.addString(sp, KEY_DB_ID, dbId);
    }

    public String getShareText() {
        return SharedPrefUtil.getString(sp, KEY_SHARE_TEXT, "");
    }

    /**
     * @param shareText
     */
    public void saveShareText(String shareText) {
        SharedPrefUtil.addString(sp, KEY_SHARE_TEXT, shareText);
    }


    public String getWinnerNotice() {
        return SharedPrefUtil.getString(sp, KEY_WINNER_NOTICE, "");
    }

    /**
     * @param winnerNotice
     */
    public void saveWinnerNotice(String winnerNotice) {
        SharedPrefUtil.addString(sp, KEY_WINNER_NOTICE, winnerNotice);
    }

    /**
     * @param attemptCode
     */
    public void saveAttemptedQuestionCode(int attemptCode) {
        SharedPrefUtil.addInt(sp, KEY_QUESTION_STATUS, attemptCode);
    }

    public int getAttemptedQuestionCode() {
        return SharedPrefUtil.getInt(sp, KEY_QUESTION_STATUS, 0);
    }

    /**
     * @param attemptStatusModel
     */
    public void saveAttemptedQuestion(QuestionAttemptStatusModel attemptStatusModel) {
        SharedPrefUtil.addObjectInGsonString(sp, KEY_QUESTION_MODEL, attemptStatusModel, QuestionAttemptStatusModel.class);
    }

    /**
     * @return
     */
    public QuestionAttemptStatusModel getAttemptedQuestion() {
        return SharedPrefUtil.getObjectFromGsonString(sp, KEY_QUESTION_MODEL, QuestionAttemptStatusModel.class);
    }

    public String getGameStatusNotice() {
        return SharedPrefUtil.getString(sp, KEY_GAMESTATUS_NOTICE, "");
    }

    /**
     * @param gameStatusNotice
     */
    public void saveGameStatusNotice(String gameStatusNotice) {
        SharedPrefUtil.addString(sp, KEY_GAMESTATUS_NOTICE, gameStatusNotice);
    }

    /**
     * @return
     */
    public String getDbId() {
        return SharedPrefUtil.getString(sp, KEY_DB_ID, "");
    }

    /**
     * @param ticketId
     */
    public void saveTicketId(String ticketId) {
        SharedPrefUtil.addString(sp, KEY_TICKET_ID, ticketId);
    }

    /**
     * @return
     */
    public String getTicketId() {
        return SharedPrefUtil.getString(sp, KEY_TICKET_ID, "");
    }

    public void clear() {
        SharedPrefUtil.clearSharedPreference(sp);
    }

    public String getSavedScannedDate() {
        return SharedPrefUtil.getString(sp, KEY_SCANNED_DATE, "");
    }

    public void saveScannedDate(String scannedDate) {
        SharedPrefUtil.addString(sp, KEY_SCANNED_DATE, scannedDate);
    }


    public String getDateWhenUserTapOnHome() {
        return SharedPrefUtil.getString(sp, KEY_USER_LEAVED_DATE, "");
    }

    public void saveDateWhenUserTapOnHome(String userLeaveDate) {
        SharedPrefUtil.addString(sp, KEY_USER_LEAVED_DATE, userLeaveDate);
    }

    public void clearDateWhenUserTapOnHome() {
        SharedPrefUtil.addString(sp, KEY_USER_LEAVED_DATE, "");
    }
}
