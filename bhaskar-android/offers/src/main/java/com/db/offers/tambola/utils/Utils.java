package com.db.offers.tambola.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.bhaskar.util.LoginController;
import com.bhaskar.data.model.ProfileInfo;
import com.db.offers.BuildConfig;
import com.db.offers.OfferController;
import com.db.offers.common.AppConstants;
import com.db.offers.common.CommonUtils;
import com.db.offers.common.DBSharedPref;
import com.db.offers.common.UrlUtil;
import com.db.offers.common.cryptlib.CryptLib;
import com.db.offers.megaoffers.constants.MegaOffersAPIConstants;
import com.bhaskar.appscommon.tracking.TrackingData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_APP_VERSION;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_GENDER;
import static com.db.offers.common.ApiConstants.HeaderKeys.KEY_RASHI_NAME;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderKeys.KEY_ACCEPT;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderKeys.KEY_CHANNEL;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderKeys.KEY_CONTENT_TYPE;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderKeys.KEY_DB_ID;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderKeys.KEY_DEVICE_ID;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderKeys.KEY_DEVICE_TYPE;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderKeys.KEY_UUID;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderKeys.KEY_X_CSRF_TOKEN;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderValues.VALUE_ACCEPT;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderValues.VALUE_CHANNEL;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderValues.VALUE_CONTENT_TYPE;
import static com.db.offers.tambola.constant.TambolaConstants.HeaderValues.VALUE_DEVICE_TYPE;

public final class Utils {

    private static final String TAG = Utils.class.getName();

    /***
     *
     * @param emailAddress
     * @param dbId
     * @param userLoginType
     * @return
     */
    public static boolean isEmailEditable(String emailAddress, @NonNull String dbId, AppConstants.UserLoginType userLoginType) {
        if (TextUtils.isEmpty(emailAddress) || userLoginType == null) {
            return true;
        } else {
            try {
                if (AppConstants.UserLoginType.LOGIN_GOOGLE.equals(userLoginType) &&
                        (CommonUtils.isEmailValid(emailAddress) && !emailAddress.contains(dbId))) {
                    return false;
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage() + "");
                return true;
            }
            return true;
        }
    }


    /**
     * @param webUrl
     * @return
     */
    public static boolean isWebUrlDBInternal(String webUrl, List<String> whiteUrlList) {

        if (!TextUtils.isEmpty(webUrl)) {
            if (whiteUrlList != null && !whiteUrlList.isEmpty()) {
                for (String whiteListURl : whiteUrlList) {
                    if (webUrl.contains(whiteListURl)) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            return false;
        }
    }

    public static List<String> getDBWebInternalUrls() {
        List<String> webUrlsList = new ArrayList<>();
        webUrlsList.add(UrlUtil.getBhaskarDomainName());
        webUrlsList.add(UrlUtil.getDivyaDomainName());
        return webUrlsList;
    }

    /***
     *
     * @param context
     * @return
     */
    public static Map<String, String> getWebHeader(Context context) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(KEY_ACCEPT, VALUE_ACCEPT);
        headerMap.put(KEY_CHANNEL, VALUE_CHANNEL);
        headerMap.put(KEY_DEVICE_TYPE, VALUE_DEVICE_TYPE);
        headerMap.put(KEY_CONTENT_TYPE, VALUE_CONTENT_TYPE);
        headerMap.put(KEY_APP_VERSION, BuildConfig.VERSION_NAME);
        headerMap.put(KEY_X_CSRF_TOKEN, "");
        headerMap.put(KEY_DB_ID, TrackingData.getDBId(context));
        String deviceId = TrackingData.getDeviceId(context);
        headerMap.put(KEY_UUID, deviceId);
        headerMap.put(KEY_DEVICE_ID, deviceId);
        DBSharedPref dbSharedPref = OfferController.getInstance().getDbSharedPref();
        if (null != dbSharedPref) {
            headerMap.put(KEY_RASHI_NAME, dbSharedPref.getRashiName());
            headerMap.put(KEY_GENDER, dbSharedPref.getGender());
        } else {
            headerMap.put(KEY_RASHI_NAME, "");
            headerMap.put(KEY_GENDER, "");
        }
        return headerMap;
    }

    public static HashMap<String,Object> getUserDataMap(){
        ProfileInfo profileInfo= LoginController.loginController().getCurrentUser();

        HashMap<String,Object> userMap = new HashMap<>();
        if(profileInfo!=null){
            userMap.put("email",profileInfo.email);
            userMap.put("mobile",profileInfo.mobile);
            userMap.put("isUserLoggedIn",profileInfo.isUserLoggedIn);
        }
        return userMap;
    }

    /***
     *
     * @param context
     * @return
     */
    public static Map<String, String> getOfferDashboardWebHeader(@NonNull Context context) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(KEY_ACCEPT, VALUE_ACCEPT);
        headerMap.put(KEY_CHANNEL, VALUE_CHANNEL);
        headerMap.put(KEY_DEVICE_TYPE, VALUE_DEVICE_TYPE);
        headerMap.put(KEY_CONTENT_TYPE, VALUE_CONTENT_TYPE);
        headerMap.put(KEY_APP_VERSION, BuildConfig.VERSION_NAME);
        headerMap.put(KEY_X_CSRF_TOKEN, "");
        headerMap.put(KEY_DB_ID, TrackingData.getDBId(context));
        String deviceId = TrackingData.getDeviceId(context);
        headerMap.put(KEY_UUID, deviceId);
        headerMap.put(KEY_DEVICE_ID, deviceId);

        try {
            CryptLib cryptLib = new CryptLib();

            headerMap.put(MegaOffersAPIConstants.KEY_OFFER_AUTH, cryptLib.encryptPlainTextWithRandomIV(TrackingData.getDeviceId(context), CryptLib.ENCRYPT_KEY));
        } catch (Exception e) {

        }

        return headerMap;
    }

    public static String getAccountEmail(Context dbApplication) {
        return "";
    }

    /***
     *
     * @param dbApplication
     */
//    public static void performLogout(@NonNull OfferApplication dbApplication) {
//        Log.d(TAG, "performLogout()");
//
//        try {
//            ContentProviderClient client = dbApplication.getContentResolver().acquireContentProviderClient("com.bhaskar.darwin.common.CredentialProvider");
//            CredentialProvider provider = null;
//            if (client != null) {
//                provider = (CredentialProvider) client.getLocalContentProvider();
//                if (provider != null) {
//                    provider.removeUserCredential();
//                }
//                client.release();
//            }
//        } catch (Exception e) {
//            Log.d(TAG, e.getMessage() + "");
//        }
//
//        try {
//            dbApplication.getDbSharedPref().logoutUser();
//            ChitChatDataManager.getInstance().deleteChitChatMessages();
//            /*DBAppPreference.setBooleanPreference(PreferenceConstants.IS_USER_LOGGED_IN, false);
//            DBAppPreference.setIntegerPreference(PreferenceConstants.USER_LOGIN_TYPE, 0);*/
//        } catch (Exception e) {
//            Log.e(TAG, e.getMessage() + "");
//        }
//        try {
//            //Facebook logout
//            com.facebook.login.LoginManager.getInstance().logOut();
//        } catch (Exception e) {
//            Log.e(TAG, e.getMessage() + "");
//        }
//        try {
//            //Google logout
//            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(
//                    GoogleSignInOptions.DEFAULT_SIGN_IN).build();
//            GoogleSignIn.getClient(dbApplication, gso).signOut();
//        } catch (Exception e) {
//            Log.e(TAG, e.getMessage() + "");
//        }
//    }

    /**
     * @param catId
     * @return
     */
//    public static String getCategoryName(String catId) {
//        try {
//            List<Menu> menuList = MenuDataManager.getInstance().getMenuListData();
//            for (Menu menu : menuList) {
//                if (menu.getId().equals(catId)) {
//                    return menu.getLabel();
//                }
//            }
//            return "News";
//        } catch (Exception e) {
//            Log.e("Utils", e.getMessage() + "");
//            return null;
//        }
//    }

    /**
     * @param catId
     * @return
     */
//    public static String getCategoryNameInEnglish(String catId) {
//        try {
//            List<Menu> menuList = MenuDataManager.getInstance().getMenuListData();
//            for (Menu menu : menuList) {
//                if (menu.getId().equals(catId)) {
//                    return menu.getLabelInEnglish();
//                }
//            }
//            return "News";
//        } catch (Exception e) {
//            Log.e("Utils", e.getMessage() + "");
//            return null;
//        }
//    }

    /**
     * @param resId
     * @param borderWidth
     * @param swipeSpeed
     * @return
     */
//    public static ParallaxPagerTransformer getParallaxPagerTransform(int resId, int borderWidth, float swipeSpeed) {
//        ParallaxPagerTransformer transformer = new ParallaxPagerTransformer(resId);
//        transformer.setBorderWidth(borderWidth);
//        transformer.setSpeed(swipeSpeed);
//        return transformer;
//    }

    /**
     * @param appUpdateConfigData
     * @return
     */
//    public static AppUpdateResponse checkForShowingAppUpdateCard(String appUpdateConfigData) {
//        String cur_date = CommonUtils.getDateWithPattern(new Date(), AppConstants.DATE_PATTERN);
//        String save_date = DBAppPreference.getStringPreference(PreferenceConstants.CURRENT_DATE, "");
//        if (!cur_date.equals(save_date) && !TextUtils.isEmpty(appUpdateConfigData)) {
//            AppUpdateResponse configResponse = new Gson().fromJson(appUpdateConfigData, AppUpdateResponse.class);
//            if (configResponse != null && configResponse.isEnable() && ForceUpdateHelper.updateRequired(configResponse.getMinVersion())) {
//                return configResponse;
//            }
//        }
//        return null;
//    }

    /**
     * @param coreActivity
     * @param redirectionUrl
     */
//    public static void redirectToStore(CoreActivity coreActivity, String redirectionUrl) {
//        if (!TextUtils.isEmpty(redirectionUrl)) {
//            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(redirectionUrl));
//            coreActivity.startActivity(intent);
//        } else {
//            if (coreActivity != null) {
//                final String appPackageName = coreActivity.getPackageName(); // getPackageName() from Context or Activity object
//                try {
//                    coreActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                } catch (android.content.ActivityNotFoundException anfe) {
//                    coreActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                }
//            }
//        }
//    }
}
