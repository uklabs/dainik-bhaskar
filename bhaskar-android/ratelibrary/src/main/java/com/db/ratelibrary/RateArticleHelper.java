package com.db.ratelibrary;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatDelegate;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.db.ratelibrary.customratingbar.SimpleRatingBar;
import com.db.ratelibrary.db.DatabaseHelper;
import com.db.ratelibrary.db.RateStatusTable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class RateArticleHelper {
    private Activity activity;
    private View rateWidget;
    private RelativeLayout progress_bar_layout;
    private String story_id;
    private int TimeCounter = 0;
    private int themeColor;
    private SimpleRatingBar ratingBar;
    private LinearLayout comment_view;
    private EditText txt_comment;
    private TextView txt_Thanx;
    private TextView txt_rate_heading;
    private String BASE_URL;
    private boolean nightMode;
    private double ratingValue;
    private String dbID;
    private boolean isFeedBack;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private RateUsClickCallback clickCallBack;

    public RateArticleHelper(String baseUrlWebApi) {
        this.BASE_URL = baseUrlWebApi;
    }

    public View getFeedbackView(Activity activity, String storyId, int color, boolean isNightMode, String dbId, String title) {
        isFeedBack = true;
        this.activity = activity;
        this.story_id = storyId;
        this.themeColor = color;
        this.nightMode = isNightMode;
        this.dbID = dbId;
        rateWidget = LayoutInflater.from(activity).inflate(R.layout.layout_rate, null);
        initView(rateWidget, title, true);
        return rateWidget;

    }

    public View getRateView(Activity activity, String storyId, int color, boolean isNightMode, String dbId, String title) {
        this.activity = activity;
        this.story_id = storyId;
        if (!((RateStatusTable) DatabaseHelper.getInstance(activity).getTableObject(RateStatusTable.TABLE_NAME)).isPresent(storyId)) {
            this.themeColor = color;
            this.nightMode = isNightMode;
            this.dbID = dbId;
            rateWidget = LayoutInflater.from(activity).inflate(R.layout.layout_rate, null);
            initView(rateWidget, title, false);
            return rateWidget;
        } else {
            return null;
        }
    }

    private void initView(final View rateWidget, String title, boolean visibleComment) {
        ratingBar = rateWidget.findViewById(R.id.ratingBar);
        comment_view = rateWidget.findViewById(R.id.ll_comment);
        txt_comment = rateWidget.findViewById(R.id.txt_comment);
        progress_bar_layout = rateWidget.findViewById(R.id.progress_bar_layout);
        txt_rate_heading = rateWidget.findViewById(R.id.textRateHeading);
        txt_Thanx = rateWidget.findViewById(R.id.thanxText);
        TextView submitBtn = rateWidget.findViewById(R.id.submit);
        if (visibleComment) {
            comment_view.setVisibility(View.VISIBLE);
            txt_comment.setVisibility(View.GONE);
        } else {
            comment_view.setVisibility(View.GONE);
        }
        txt_rate_heading.setText(title);

        //set dynamic color
        if (nightMode) {
            txt_rate_heading.setTextColor(Color.WHITE);
            txt_Thanx.setTextColor(Color.WHITE);
            txt_comment.setTextColor(Color.WHITE);
            txt_comment.setHintTextColor(Color.WHITE);
            ratingBar.setBorderColor(Color.WHITE);
            ratingBar.setFillColor(Color.WHITE);
            ratingBar.setPressedBorderColor(Color.WHITE);
            ratingBar.setPressedFillColor(Color.WHITE);
        } else {
            int color;
            txt_comment.setTextColor(ContextCompat.getColor(activity, R.color.grey));
            txt_comment.setHintTextColor(ContextCompat.getColor(activity, R.color.grey));
            if (themeColor == 0) {
//                txt_rate_heading.setTextColor(ContextCompat.getColor(activity, R.color.blue));
//                txt_Thanx.setTextColor(ContextCompat.getColor(activity, R.color.blue));

                color = ContextCompat.getColor(activity, R.color.blue);
            } else {
//                txt_rate_heading.setTextColor(themeColor);
//                txt_Thanx.setTextColor(themeColor);
                color = themeColor;
            }
            txt_rate_heading.setTextColor(color);
            txt_Thanx.setTextColor(color);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ((GradientDrawable) submitBtn.getBackground()).setColor(color);
            }

            ratingBar.setBorderColor(color);
            ratingBar.setFillColor(color);
            ratingBar.setPressedBorderColor(color);
            ratingBar.setPressedFillColor(color);
        }

        ratingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float rating = ratingBar.getRating();
                if (rating > 0) {
                    comment_view.setVisibility(View.VISIBLE);
                    txt_comment.setVisibility(View.VISIBLE);
                    ratingBar.setIndicator(true);
                    ratingValue = Double.valueOf(rating);
                    makeRatingRequest(ratingValue, dbID);
                    if (clickCallBack != null)
                        clickCallBack.clickedOnRateUs(rating);
                } else {
                    txt_comment.setVisibility(View.GONE);
                }
            }
        });

//        ratingBar.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
//            @Override
//            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
//
//            }
//        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ratingBar.getRating() > 0) {
                    if (!TextUtils.isEmpty(txt_comment.getText().toString())) {
                        makeCommentRequest(txt_comment.getText().toString(), dbID);
                    } else {
                        clickCallBack.onRateUsSubmitted();
                    }
                } else {
                    Toast.makeText(activity, "Please select Rating Stars.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public static void changeDayNight(Context activity, View rateWidget, boolean nightMode, int themeColor) {
        SimpleRatingBar ratingBar = (SimpleRatingBar) rateWidget.findViewById(R.id.ratingBar);
        EditText txt_comment = (EditText) rateWidget.findViewById(R.id.txt_comment);
        TextView txt_rate_heading = (TextView) rateWidget.findViewById(R.id.textRateHeading);
        TextView txt_Thanx = (TextView) rateWidget.findViewById(R.id.thanxText);

        //set dynamic color
        if (nightMode) {
            txt_rate_heading.setTextColor(Color.WHITE);
            txt_Thanx.setTextColor(Color.WHITE);
            txt_comment.setTextColor(Color.WHITE);
            txt_comment.setTextColor(Color.WHITE);
            ratingBar.setBorderColor(Color.WHITE);
            ratingBar.setFillColor(Color.WHITE);
            ratingBar.setPressedBorderColor(Color.WHITE);
            ratingBar.setPressedFillColor(Color.WHITE);
        } else {
            int color;
            txt_comment.setTextColor(ContextCompat.getColor(activity, R.color.grey));
            txt_comment.setHintTextColor(ContextCompat.getColor(activity, R.color.grey));
            if (themeColor == 0) {
                txt_rate_heading.setTextColor(ContextCompat.getColor(activity, R.color.blue));
                txt_Thanx.setTextColor(ContextCompat.getColor(activity, R.color.blue));
                color = ContextCompat.getColor(activity, R.color.blue);
            } else {
                txt_rate_heading.setTextColor(themeColor);
                txt_Thanx.setTextColor(themeColor);
                color = themeColor;
            }
            ratingBar.setBorderColor(color);
            ratingBar.setFillColor(color);
            ratingBar.setPressedBorderColor(color);
            ratingBar.setPressedFillColor(color);
        }
    }

    private void makeRatingRequest(double ratingValue, String dbID) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("storyid", story_id);
            jsonObject.put("session_id", dbID);
            jsonObject.put("rating", ratingValue);
        } catch (JSONException e) {
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, BASE_URL + Urls.RATE_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response != null) {
                        System.out.println(response);
                        String Message = response.getString("Message");
                        String code = response.getString("code");
                        if (code.equalsIgnoreCase("200")) {
                            Toast.makeText(activity, "rated", Toast.LENGTH_SHORT).show();
                            ((RateStatusTable) DatabaseHelper.getInstance(activity).getTableObject(RateStatusTable.TABLE_NAME)).insertData(story_id, activity);
                        }
                    }
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        Toast.makeText(activity, activity.getResources().getString(R.string.no_network_error), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(activity, activity.getResources().getString(R.string.sorry_error_found_please_try_again_), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("User-Agent", Urls.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(activity.getBaseContext()).addToRequestQueue(jsonObjReq);
    }


    private void makeCommentRequest(String feedback, String dbID) {
        progress_bar_layout.setVisibility(View.VISIBLE);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("storyid", story_id);
            jsonObject.put("session_id", dbID);
            jsonObject.put("feedback", feedback);
        } catch (JSONException e) {
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BASE_URL + Urls.COMMENT_URL, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response != null) {
                                ((RateStatusTable) DatabaseHelper.getInstance(activity).getTableObject(RateStatusTable.TABLE_NAME)).insertData(story_id, activity);
//                                String Message = response.getString("Message");
//                                String code = response.getString("code");
                                progress_bar_layout.setVisibility(View.GONE);
                                rateWidget.findViewById(R.id.rate_layout).setVisibility(View.GONE);

                                if (isFeedBack) {
                                    Toast.makeText(activity, "Thanks Report submitted successfully", Toast.LENGTH_SHORT).show();
                                    clickCallBack.onRateUsSubmitted();
//                                    ((TextView) rateWidget.findViewById(R.id.thanxText)).setText("Thanks Report submitted successfully");
                                } else {
                                    rateWidget.findViewById(R.id.thankyou_ll).setVisibility(View.VISIBLE);
                                    StartTimer();
                                }
                            }
                        } catch (Exception e) {
                            //e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    if (error instanceof NoConnectionError || error instanceof NetworkError) {
                        progress_bar_layout.setVisibility(View.GONE);
                        Toast.makeText(activity, activity.getResources().getString(R.string.no_network_error), Toast.LENGTH_SHORT).show();

                    } else {
                        progress_bar_layout.setVisibility(View.GONE);
                        Toast.makeText(activity, activity.getResources().getString(R.string.sorry_error_found_please_try_again_), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                }
            }
        }) {
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(activity.getBaseContext()).addToRequestQueue(jsonObjReq);
    }

    private void StartTimer() {

        final int maxTime = isFeedBack ? 3 : 60;

        final Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        if (TimeCounter == maxTime) {
                          /*  if (isFeedBack) {
                                ((TextView) rateWidget.findViewById(R.id.thanxText)).setText("Thanks Report submitted successfully");
                            }*/
                            rateWidget.findViewById(R.id.thankyou_ll).setVisibility(View.GONE);
                            t.cancel();
                            clickCallBack.onRateUsSubmitted();
                            return;
                        }
                        TimeCounter++;
                    }
                });
            }
        }, 0, 1000);
    }

    public void setClickCallBack(RateUsClickCallback clickCallBack) {
        this.clickCallBack = clickCallBack;
    }
}
