package com.db.ratelibrary;

/**
 * Created by DB on 11-08-2017.
 */

public interface RateUsClickCallback {
    public void clickedOnRateUs(float rating);

    public void onRateUsSubmitted();
}
