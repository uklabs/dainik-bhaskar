package com.db.ratelibrary.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.concurrent.atomic.AtomicInteger;

public class DatabaseHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "RateModuleManager";
    private static DatabaseHelper instance = null;
    private SQLiteOpenHelper helper;
    //tables
    private RateStatusTable rateStatusTable;
    private AtomicInteger mOpenCounter = new AtomicInteger();
    private SQLiteDatabase mDatabase;

    private DatabaseHelper(Context context) {
        helper = new SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
            @Override
            public void onCreate(SQLiteDatabase db) {
                rateStatusTable.createTable(db);
            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                rateStatusTable.onUpgrade(db, oldVersion, newVersion);
            }

        };
        rateStatusTable = new RateStatusTable(this);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }
        return instance;
    }

    public synchronized SQLiteDatabase getWritableDatabase(String tableName) {
        if (mOpenCounter.incrementAndGet() == 1 && helper != null) {
            mDatabase = helper.getWritableDatabase();
        }
        return mDatabase;
    }

    public void distroyInstance() {
        instance = null;
    }

    public Object getTableObject(String tableName) {
        if (tableName.equalsIgnoreCase(RateStatusTable.TABLE_NAME)) {
            return rateStatusTable;
        }
        return null;
    }

    public void closeDB() {
        if (mOpenCounter.decrementAndGet() == 0 && helper != null && mDatabase != null) {
            mDatabase.close();
        }
    }
}