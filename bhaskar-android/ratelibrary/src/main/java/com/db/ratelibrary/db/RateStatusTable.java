package com.db.ratelibrary.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class RateStatusTable {

    public static final String TABLE_NAME = "mRateStatus";

    public static final String COL_storyId = "storyId";
    Object lock = new Object();

    private DatabaseHelper appDb;

    public RateStatusTable(DatabaseHelper appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + COL_storyId + " TEXT PRIMARY KEY)";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void insertData(String storyId, Context ctx) {
        if (!isPresent(storyId)) {
            insertData1(storyId, ctx);
        }
    }

    public String insertData1(String storyId, Context ctx) {
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                ContentValues values = new ContentValues();
                values.clear();
                if (storyId != null && !storyId.equalsIgnoreCase("null") && storyId.trim().length() > 0)
                    values.put(COL_storyId, storyId);

                returnValue = db.insert(TABLE_NAME, null, values);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }

    public int deleteAllData(Context ctx) {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                rowsAffected = db.delete(TABLE_NAME, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }

    public boolean isPresent(String number) {
        SQLiteDatabase db = null;
        boolean isPresent = false;
        synchronized (lock) {
            try {
                String countQuery = "SELECT * FROM " + TABLE_NAME + " where " + COL_storyId + "=?;";
                db = appDb.getWritableDatabase(TABLE_NAME);
                Cursor cursor = db.rawQuery(countQuery, new String[]{number});
                if (cursor.getCount() > 0) {
                    isPresent = true;
                } else {
                    isPresent = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return isPresent;
    }

}