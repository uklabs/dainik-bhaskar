/*
package com.db.ratelibrary.db;

import android.content.Context;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class SerializeRateData implements Serializable {

    private static final String FILE_NAME = "DainikBhaskarDataFileV2";
    private static SerializeRateData serializeData;
    private static Context mContext;

    // Data Structure
//    private Vector<CityInfo> cityList;


    private List<ArticleRatedInfo> isArticleRatedList;



    private SerializeRateData() {
//        cityList = new Vector<>(6, 6);
        isArticleRatedList = new ArrayList<>();
    }

    // create instance
    public static SerializeRateData getInstance(Context context) {
        SerializeRateData.mContext = context;
        if (serializeData == null) {
            synchronized (SerializeRateData.class) {
                SerializeRateData fileInstance = getFileInstance(context);
                if (fileInstance == null) {
                    if (serializeData == null) {
                        serializeData = new SerializeRateData();

                        save(context);
                    }
                } else {
                    serializeData = fileInstance;
                }
            }
        }
        return serializeData;
    }

    private synchronized static void save(Context context) {
        try {
            if (context != null) {
                FileOutputStream fileOut = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(serializeData);
                out.close();
                fileOut.close();
            }

        } catch (FileNotFoundException e) {
            //e.printStackTrace();
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    private synchronized static SerializeRateData getFileInstance(Context context) {
        SerializeRateData serializeData = null;
        try {
            if (context != null) {
                FileInputStream fileIn = context.openFileInput(FILE_NAME);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                serializeData = (SerializeRateData) in.readObject();
                in.close();
                fileIn.close();
            }
        } catch (IOException i) {
        } catch (ClassNotFoundException c) {
        }
        return serializeData;
    }
    public void saveRateArticle(ArticleRatedInfo rateListInfo) {
        if (!isArticleRatedList.contains(rateListInfo)) {
            isArticleRatedList.add(rateListInfo);
        }
        save(mContext);
    }

    public List<ArticleRatedInfo> getRateList() {
        return isArticleRatedList;
    }

    public boolean isRated(String storyId) {
        return isArticleRatedList.contains(new ArticleRatedInfo(storyId));
    }
    */
/*//*
/set article rated
    public void setRatedArticle(String id, boolean value) {
        int index = isArticleRated.indexOf(new ArticleRatedInfo(id));
        if (index >= 0) {
            ArticleRatedInfo isArticleRatedyInfo = isArticleRated.get(index);
            isArticleRatedyInfo.value = value;
        } else {
            isArticleRated.add(new ArticleRatedInfo(id, value));
        }
        save(mContext);
    }

    public boolean isRatedArticle() {
        for (ArticleRatedInfo isArticleRatedyInfo : isArticleRated) {
            if (isArticleRatedyInfo.value) {
                return true;
            }
        }
        return false;
    }

    public boolean isRatedArticle(String id) {
        int index = isArticleRated.indexOf(new ArticleRatedInfo(id));
        if (index >= 0) {
            ArticleRatedInfo isArticleRatedyInfo = isArticleRated.get(index);
            return isArticleRatedyInfo.value;
        }

        return false;
    }*//*

}
*/
