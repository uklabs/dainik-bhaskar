package com.bhaskar.sharewin;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.db.main.BaseAppCompatActivity;

public class BaseActivity extends BaseAppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
