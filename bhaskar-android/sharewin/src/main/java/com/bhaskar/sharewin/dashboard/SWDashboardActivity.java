package com.bhaskar.sharewin.dashboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.data.local.prefs.SWAppPreferences;
import com.bhaskar.data.model.Offer;
import com.bhaskar.util.LoginController;
import com.bhaskar.util.ShareWinController;
import com.bhaskar.sharewin.BaseActivity;
import com.bhaskar.sharewin.R;
import com.bhaskar.sharewin.utility.FragmentUtil;


public class SWDashboardActivity extends BaseActivity {

    private Offer mOffer;
    private boolean shouldShowCoupon;


    public static Intent getIntent(Context context, Offer offer, boolean shouldShowCoupon) {
        Intent intent = new Intent(context, SWDashboardActivity.class);
        intent.putExtra(SWAppPreferences.Keys.OFFER, offer);
        intent.putExtra(SWAppPreferences.Keys.SHOW_COUPON_ENTRY_POINT, shouldShowCoupon);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sw_dashboard);
        handleExtras(getIntent());
        handleToolbar();
        FragmentUtil.changeFragment(getSupportFragmentManager(), R.id.rootContainer, SWDashboardFragment.getInstance(mOffer, shouldShowCoupon), false, false);
        Tracking.trackGAScreen(this, ShareWinController.shareWinController().getDefaultTracker(), mOffer.gaScreen, "", "", "");

    }

    public void openCouponSystem() {
        setResult(RESULT_OK);
        finish();
    }

    private void handleExtras(Intent intent) {
        if (intent == null)
            return;
        mOffer = (Offer) intent.getSerializableExtra(SWAppPreferences.Keys.OFFER);
        shouldShowCoupon = intent.getBooleanExtra("showCoupon", false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_login) {
            LoginController.loginController().showProfile(SWDashboardActivity.this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            updateFragment();

        }
    }

    private void updateFragment() {
        try {
            SWDashboardFragment fragment = (SWDashboardFragment) getSupportFragmentManager().findFragmentByTag(SWDashboardFragment.class.getName());
            if (fragment != null) {
                fragment.refreshProfileInfo();
            }
        } catch (Exception e) {
        }

    }

    private void handleToolbar() {
        /*Tool Bar*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_sw);
        }
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        TextView tvToolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        tvToolbarTitle.setText(String.valueOf(mOffer.name));
    }

}
