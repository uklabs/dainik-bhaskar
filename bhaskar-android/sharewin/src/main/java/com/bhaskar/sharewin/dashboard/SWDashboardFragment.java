package com.bhaskar.sharewin.dashboard;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.appscommon.tracking.Tracking;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.data.local.prefs.LoginPreferences;
import com.bhaskar.data.local.prefs.SWAppPreferences;
import com.bhaskar.data.model.Offer;
import com.bhaskar.data.model.ProfileInfo;
import com.bhaskar.util.LoginController;
import com.bhaskar.util.ShareWinController;
import com.bhaskar.sharewin.R;
import com.bhaskar.sharewin.models.DashboardData;
import com.bhaskar.sharewin.utility.SWConstants;
import com.bhaskar.sharewin.utility.SWShareUtil;
import com.bhaskar.sharewin.utility.SWUtility;
import com.bhaskar.sharewin.utility.ShareWinURLS;
import com.db.util.AppUtils;
import com.db.util.NetworkStatus;
import com.db.util.Systr;
import com.db.util.VolleyNetworkSingleton;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class SWDashboardFragment extends Fragment {
    private static final String TAG = SWDashboardFragment.class.getSimpleName();
    private LinearLayout ticketListLayout;
    private View vTickets;
    private int STANDARD_COUPON_COUNT = 5;
    private int LOGIN_REQUEST_CODE = 786;
    private int CLAIM_REQUEST_CODE = 786;
    private boolean isOfferEnabled;
    private String offerDisableMsg;
    private String shareMsg;
    private int downloadCount;
    private TextView tvCountMsg;
    private WebView webViewNew;
    private CouponAdapter couponAdapter;
    private List<HashMap<String, String>> couponCodeList;
    private TextView tvTCSubtext;
    private String REF_CODE;
    private ProfileInfo mProfileInfo;
    private Offer mOffer;
    private DashboardData mDashboardData;
    private String mDBId;
    private String mDeviceID;
    private String mAppID;
    private ProgressDialog progressDialog;
    private boolean shouldShowCoupon;

    public static SWDashboardFragment getInstance(Offer mOffer, boolean shouldShowCoupon) {
        SWDashboardFragment fragment = new SWDashboardFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(SWAppPreferences.Keys.OFFER, mOffer);
        bundle.putBoolean(SWAppPreferences.Keys.SHOW_COUPON_ENTRY_POINT, shouldShowCoupon);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProfileInfo = LoginPreferences.getInstance(getContext()).getDataFromPreferences(LoginPreferences.Keys.CURRENT_USER, ProfileInfo.class);
            mOffer = (Offer) getArguments().getSerializable(SWAppPreferences.Keys.OFFER);
            shouldShowCoupon = getArguments().getBoolean(SWAppPreferences.Keys.SHOW_COUPON_ENTRY_POINT);
            mDBId = ShareWinController.shareWinController().getDBId();
            mDeviceID = ShareWinController.shareWinController().getDeviceID();
            mAppID = ShareWinController.shareWinController().getAppId();
            mDashboardData = new DashboardData(mOffer.extraValues);
            isOfferEnabled = mOffer.offerExpire.equalsIgnoreCase("0");
            offerDisableMsg = mDashboardData.offerExpireMsg;
            shareMsg = mDashboardData.shareMsg;
            progressDialog = SWUtility.getInstance().generateProgressDialog(getContext(), false);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sw_dashboard, container, false);
        final WebView webView = view.findViewById(R.id.webView);
        setupWebView(webView);
        ticketListLayout = view.findViewById(R.id.vTicketList);
        vTickets = view.findViewById(R.id.vTickets);
        tvCountMsg = view.findViewById(R.id.tvRefCountMsg);
        tvTCSubtext = view.findViewById(R.id.tvTCSubtext);
        TextView tvNeedHelp = view.findViewById(R.id.tvNeedHelp);
        View viewNeedHelp = view.findViewById(R.id.vNeedHelp);
        if (!TextUtils.isEmpty(mDashboardData.needHelpTxt)) {
            viewNeedHelp.setVisibility(View.VISIBLE);
            tvNeedHelp.setText(String.valueOf(mDashboardData.needHelpTxt));
        } else {
            viewNeedHelp.setVisibility(View.GONE);
        }

        viewNeedHelp.setOnClickListener(v -> startActivity(SWWapV2Activity.getIntent(getContext(), mDashboardData.needHelpTxt, mDashboardData.needHelpUrl)));
        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        webViewNew = view.findViewById(R.id.webViewNew);
        View couponView = view.findViewById(R.id.vCouponScreen);
        if (shouldShowCoupon) {
            couponView.setVisibility(View.VISIBLE);
            couponView.setOnClickListener(v -> {
                ((SWDashboardActivity) Objects.requireNonNull(getActivity())).openCouponSystem();
            });
        } else {
            couponView.setVisibility(View.GONE);
        }

        setupWebView(webViewNew);

        setupTabLayout(tabLayout);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewCoupon);
        couponCodeList = new ArrayList<>();
        couponAdapter = new CouponAdapter(couponCodeList);
        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(getContext());

        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setJustifyContent(JustifyContent.CENTER);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(flexboxLayoutManager);
        recyclerView.setAdapter(couponAdapter);

        webView.loadUrl(mOffer.webUrl);
        webViewNew.loadUrl(mOffer.htp);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        webViewNew.loadUrl(mOffer.htp);
                        return;
                    case 1:
                        webViewNew.loadUrl(mOffer.tnc);
                        return;
                    default:
                        webViewNew.loadUrl(mOffer.faq);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        view.findViewById(R.id.ivFacebook).setOnClickListener(v -> {
            if (NetworkStatus.getInstance().isConnected(getContext())) {
                if (!TextUtils.isEmpty(shareMsg) && !TextUtils.isEmpty(REF_CODE)) {
                    if (isOfferEnabled) {
                        String appendedString = shareMsg.replaceAll("%s", REF_CODE);
                        SWShareUtil.facebookShare(getActivity(),  appendedString);
                        Tracking.trackGAEvent(getContext(), ShareWinController.shareWinController().getDefaultTracker(), "sw_share", "click", "fb", "");
                    } else
                        AppUtils.getInstance().showCustomToast(getContext(), offerDisableMsg);
                } else {
                    AppUtils.getInstance().showCustomToast(getContext(), "Please try again!!");
                }
            } else {
               AppUtils.getInstance().showCustomToast(getContext(), "Network Error");
            }

        });
        view.findViewById(R.id.ivWhatsapp).setOnClickListener(v -> {
            if (NetworkStatus.getInstance().isConnected(getContext())) {
                if (!TextUtils.isEmpty(shareMsg) && !TextUtils.isEmpty(REF_CODE)) {
                    if (isOfferEnabled) {
                        String appendedString = shareMsg.replaceAll("%s", REF_CODE);
                        SWShareUtil.whatsappShare(getActivity(), appendedString/*String.format(shareMsg, REF_CODE)*/);
                        Tracking.trackGAEvent(getContext(), ShareWinController.shareWinController().getDefaultTracker(), "sw_share", "click", "whatsapp", "");
                    } else
                       AppUtils.getInstance().showCustomToast(getContext(), offerDisableMsg);
                } else {
                   AppUtils.getInstance().showCustomToast(getContext(), "Please try again!!");
                }
            } else {
               AppUtils.getInstance().showCustomToast(getContext(), "Network Error");
            }
        });

        setupValues(view);

        if (couponAdapter.getItemCount() == 0) {
            vTickets.setVisibility(View.GONE);
        } else {
            vTickets.setVisibility(View.VISIBLE);
        }
        return view;
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    void refreshProfileInfo() {
        mProfileInfo = LoginController.loginController().getCurrentUser();
        getUserData(false);

    }

    private void setupWebView(WebView webViewNew) {
        WebSettings webSettings = webViewNew.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webViewNew.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webViewNew.setScrollbarFadingEnabled(false);
        webViewNew.setVerticalScrollBarEnabled(false);
        webViewNew.setHorizontalScrollBarEnabled(false);
        webViewNew.setOnLongClickListener(v -> true);
        webViewNew.setLongClickable(false);
        webViewNew.setWebChromeClient(new WebChromeClient());
        webViewNew.setHapticFeedbackEnabled(false);
        webViewNew.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webViewNew.setFocusable(false);
        webViewNew.setFocusableInTouchMode(false);
        webViewNew.setWebViewClient(new MyWebViewClient());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getUserData(true);
    }

    private void getUserData(boolean showProgress) {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            if (showProgress)
                showProgress(true);
            JSONObject requestObject = getUserRequestObject(mProfileInfo);
            String finalFeedURL = ShareWinURLS.SW_FEED_BASE_URL + ShareWinURLS.SW_GET_USER_DATA;
            Systr.println(String.format(" Feed API %s Request params %s", finalFeedURL, requestObject.toString()));
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, finalFeedURL, requestObject,
                    response -> {
                        if (showProgress)
                            showProgress(false);
                        try {
                            Systr.println(String.format(" Feed API %s Response params %s", finalFeedURL, response.toString()));
                            onResponseFromServer(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                Context context = getContext();
                Systr.println(String.format(" Feed API %s Error params %s", finalFeedURL, error.toString()));
                if (showProgress)
                    showProgress(false);
                if (context != null) {
                   AppUtils.getInstance().showCustomToast(getContext(), getString(com.bhaskar.R.string.sorry_error_found_please_try_again_));
                }
                try {
                    Objects.requireNonNull(getActivity()).finish();
                } catch (Exception ignore) {
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", "DivyaBhaskarAndroid");
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
            Objects.requireNonNull(getActivity()).finish();
           AppUtils.getInstance().showCustomToast(getContext(), "Network Error");

        }
    }

    private JSONObject getUserRequestObject(ProfileInfo profileInfo) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty(mAppID))
                jsonObject.put("app_id", mAppID);
            if (!TextUtils.isEmpty(mDBId))
                jsonObject.put("db_id", mDBId);
            if (!TextUtils.isEmpty(mDeviceID))
                jsonObject.put("device_id", mDeviceID);
            if (LoginController.loginController().isUserLoggedIn())
                jsonObject.put("uid", profileInfo.uID);
            if (mOffer != null)
                jsonObject.put("sw_id", mOffer.id);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void claimTicket() {
        mProfileInfo = LoginController.loginController().getCurrentUser();
        if (!LoginController.loginController().isUserLoggedIn()) {
            //Login User First
            LoginController.loginController().getLoginIntent(getActivity(), TrackingData.getDBId(getContext()), TrackingData.getDeviceId(getContext()), LOGIN_REQUEST_CODE);
        } else {
            claimTicketRequest();
        }
    }

    private void claimTicketRequest() {
        if (NetworkStatus.getInstance().isConnected(getContext())) {
            JSONObject requestObject = makeRequestJsonObject();
            showProgress(true);
            String finalFeedURL = ShareWinURLS.SW_FEED_BASE_URL + ShareWinURLS.SW_CLAIM_TICKET;
            Systr.println(String.format(" Feed API %s Request params %s", finalFeedURL, requestObject.toString()));
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, finalFeedURL, requestObject,
                    response -> {
                        try {
                            showProgress(false);
                            Systr.println(String.format(" Feed API %s Request params %s", finalFeedURL, response.toString()));
                            if (response.optString("status").equalsIgnoreCase("Fail")) {
                               AppUtils.getInstance().showCustomToast(getContext(), String.valueOf(response.optString("msg")));

                            } else {
                                parseClaimData(response.optJSONObject("data"));
                               AppUtils.getInstance().showCustomToast(getContext(), "Successfully Claimed!");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }, error -> {
                Context context = getContext();
                Systr.println(String.format(" Feed API %s Error params %s", finalFeedURL, error.getMessage()));
                if (context != null) {
                   AppUtils.getInstance().showCustomToast(getContext(), "Network Error");
                }
                showProgress(false);
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", "bh@@$k@r-D");
                    headers.put("User-Agent", "DivyaBhaskarAndroid");
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(jsonObjReq);

        } else {
           AppUtils.getInstance().showCustomToast(getContext(), "Network Error");
        }
    }

    private void parseClaimData(JSONObject data) throws JSONException {
        if (data == null)
            return;
        int remainingRefCount = data.optInt("remain_ref_cnt");
        int availRefCount = data.optInt("avail_ref_cnt");
        String refCode = data.optString("ref_code");
        couponCodeList.clear();
        JSONArray jsonArray = data.optJSONArray("tickets");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(SWConstants.TICKET_ID, jsonObject.optString(SWConstants.TICKET_ID));
            hashMap.put(SWConstants.CLAIMED, jsonObject.optString(SWConstants.CLAIMED));
            hashMap.put(SWConstants.WAP_TITLE, jsonObject.optString(SWConstants.WAP_TITLE));
            couponCodeList.add(hashMap);
        }
        couponAdapter.notifyDataSetChanged();
        downloadCount = remainingRefCount;

        setTicketList(downloadCount);
        tvCountMsg.setText(String.format(mDashboardData.referralLeftTitle, downloadCount));
        tvTCSubtext.setText(String.format(mDashboardData.ticketLeftTitle, availRefCount));
        if (couponAdapter.getItemCount() == 0) {
            vTickets.setVisibility(View.GONE);
        } else {
            vTickets.setVisibility(View.VISIBLE);
        }
    }

    private JSONObject makeRequestJsonObject() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("db_id", mDBId);
            jsonObject.put("device_id", mDeviceID);
            jsonObject.put("app_id", mAppID);
            if (LoginController.loginController().isUserLoggedIn()) {
                jsonObject.put("uid", mProfileInfo.uID);
                jsonObject.put("name", mProfileInfo.name);
                jsonObject.put("mobile", mProfileInfo.mobile);
                jsonObject.put("email", mProfileInfo.email);
            }
            if (mOffer != null)
                jsonObject.put("sw_id", mOffer.id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void onResponseFromServer(JSONObject response) throws JSONException {
        Log.d(TAG, "onResponseFromServer: ");
        if (response.getString("status").equalsIgnoreCase("Success")) {
            //success
            int availRefCount = response.optJSONObject("data").optInt("avail_ref_cnt");
            int remainRefCount = response.optJSONObject("data").optInt("remain_ref_cnt");
            int totalRefCount = response.optJSONObject("data").optInt("total_ref_cnt");
            REF_CODE = response.optJSONObject("data").optString("ref_code");
            couponCodeList.clear();
            JSONArray jsonArray = response.optJSONObject("data").optJSONArray("tickets");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(SWConstants.TICKET_ID, jsonObject.optString(SWConstants.TICKET_ID));
                hashMap.put(SWConstants.CLAIMED, jsonObject.optString(SWConstants.CLAIMED));
                hashMap.put(SWConstants.WAP_TITLE, jsonObject.optString(SWConstants.WAP_TITLE));
                hashMap.put(SWConstants.TICKET_URL, jsonObject.optString(SWConstants.TICKET_URL));
                couponCodeList.add(hashMap);
            }
            couponAdapter.notifyDataSetChanged();
            downloadCount = remainRefCount;
            setTicketList(downloadCount);
            tvCountMsg.setText(String.format(mDashboardData.referralLeftTitle, downloadCount));
            tvTCSubtext.setText(String.format(mDashboardData.ticketLeftTitle, availRefCount));
            if (couponAdapter.getItemCount() == 0) {
                vTickets.setVisibility(View.GONE);
            } else {
                vTickets.setVisibility(View.VISIBLE);
            }
        } else {

        }
    }

    private void setupTabLayout(TabLayout tabLayout) {
        tabLayout.addTab(tabLayout.newTab().setText("How To Play"), true);
        tabLayout.addTab(tabLayout.newTab().setText("Terms & Conditions"));
        tabLayout.addTab(tabLayout.newTab().setText("FAQ"));
    }

    private void setupValues(View view) {
        TextView tvShareSubTitle = view.findViewById(R.id.tvShareSubTitle);
        TextView tvTCtitle = view.findViewById(R.id.tvTCtitle);
        tvTCSubtext = view.findViewById(R.id.tvTCSubtext);
        TextView tvTicketDesc = view.findViewById(R.id.tvTicketDesc);
        TextView tvTicketDesc2 = view.findViewById(R.id.tvTicketDesc2);
        TextView tvReferralTitle = view.findViewById(R.id.tvReferralTitle);
        TextView tvRefCountMsg = view.findViewById(R.id.tvRefCountMsg);

        tvShareSubTitle.setText(mDashboardData.shareDesc);
        tvTCtitle.setText(mDashboardData.ticketTitle);
        tvTCSubtext.setText(mDashboardData.ticketLeftTitle);
        tvTicketDesc.setText(mDashboardData.ticketDesc);
        tvTicketDesc2.setText(mDashboardData.ticketDesc2);
        tvReferralTitle.setText(mDashboardData.referralTitle);
        tvRefCountMsg.setText(mDashboardData.referralLeftTitle);

    }

    private void setTicketList(int dcount) {
        if (getContext() == null) {
            return;
        }
        double rowCount = (dcount / 5.0 == 0.0) ? 1 : dcount / 5.0;
        int ticketCounter = dcount;
        ticketListLayout.removeAllViews();
        for (int i = 0; i < rowCount; i++) {
            final View view = getLayoutInflater().inflate(R.layout.layout_ticket_list, null);
            RecyclerView recyclerViewTicket = view.findViewById(R.id.recyclerViewTickets);
            Button btnClaimTicket = view.findViewById(R.id.btnClaimTicket);
            btnClaimTicket.setText(mDashboardData.referralClaimBtn);

            boolean filled = (ticketCounter - STANDARD_COUPON_COUNT >= 0);
            if (isOfferEnabled) {
                btnClaimTicket.setEnabled(filled);
                btnClaimTicket.setSelected(filled);
            } else {
                btnClaimTicket.setEnabled(false);
                btnClaimTicket.setSelected(false);
            }

            btnClaimTicket.setOnClickListener(v -> claimTicket());

            TicketAdapter ticketAdapter = new TicketAdapter(getContext(), prepareList(), (filled) ? 5 : downloadCount % STANDARD_COUPON_COUNT);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
            recyclerViewTicket.setLayoutManager(layoutManager);
            recyclerViewTicket.setAdapter(ticketAdapter);
            ticketListLayout.addView(view);
            ticketCounter = ticketCounter - 5;
        }
    }

    private List<String> prepareList() {
        List<String> listItem = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            listItem.add("");
        }
        return listItem;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mProfileInfo = LoginController.loginController().getCurrentUser();
        if (requestCode == LOGIN_REQUEST_CODE && requestCode == -1) {
            getUserData(false);
        } else if (requestCode == CLAIM_REQUEST_CODE) {
            getUserData(false);
        }
    }

    public void showProgress(boolean show) {
        if (isAdded()) {
            if (show) {
                if (progressDialog == null)
                    progressDialog = SWUtility.getInstance().generateProgressDialog(getContext(), false);
                progressDialog.show();
            } else {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // This is my website, so do not override; let my WebView load the page
            view.loadUrl(url);
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

    }

    class CouponAdapter extends RecyclerView.Adapter<CouponAdapter.CouponViewHolder> {
        private List<HashMap<String, String>> couponList;

        CouponAdapter(List<HashMap<String, String>> list) {
            this.couponList = list;
        }

        @NonNull
        @Override
        public CouponViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new CouponViewHolder(inflater.inflate(R.layout.row_coupon_item, parent, false));

        }

        @Override
        public void onBindViewHolder(@NonNull final CouponViewHolder holder, int position) {
            HashMap<String, String> dataMap = couponList.get(position);
            String couponCode = dataMap.get(SWConstants.TICKET_ID);
            String wapTitle = dataMap.get(SWConstants.WAP_TITLE);
            String ticketURL = dataMap.get(SWConstants.TICKET_URL);
            holder.tvCoupon.setText(couponCode);
            holder.tvCoupon.setText(couponCode);
            boolean isClaimed = dataMap.get(SWConstants.CLAIMED).equalsIgnoreCase("1");
            if (isClaimed) {
                holder.tvCoupon.setTextColor(Objects.requireNonNull(getContext()).getResources().getColor(R.color.red_A700));
                holder.tvCoupon.setBackground(getContext().getResources().getDrawable(R.drawable.canvas_coupon_bg_claimed));
            } else {
                holder.tvCoupon.setTextColor(Objects.requireNonNull(getContext()).getResources().getColor(R.color.colorPrimary));
                holder.tvCoupon.setBackground(getContext().getResources().getDrawable(R.drawable.canvas_coupon_bg));
            }
//            String revealCoupon = String.format(ShareWinURLS.SW_REVEAL_COUPON, couponCode);
            holder.tvCoupon.setOnClickListener(v -> {
                startActivityForResult(SWWapV2Activity.getIntent(getContext(), wapTitle, ticketURL), CLAIM_REQUEST_CODE);
            });

            holder.tvCoupon.setOnLongClickListener(v -> {
                copyToClipBoard(ticketURL);
                return false;
            });
        }

        private void copyToClipBoard(String str) {
            ClipboardManager clipboard = (ClipboardManager) Objects.requireNonNull(getContext()).getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("coupon", str);
            if (clipboard != null) {
                clipboard.setPrimaryClip(clip);
               AppUtils.getInstance().showCustomToast(getContext(), "Copied to Clipboard");
            }

        }

        @Override
        public int getItemCount() {
            return couponList.size();
        }

        class CouponViewHolder extends RecyclerView.ViewHolder {
            TextView tvCoupon;

            CouponViewHolder(View itemView) {
                super(itemView);
                tvCoupon = itemView.findViewById(R.id.tvCoupon);
            }
        }
    }

}
