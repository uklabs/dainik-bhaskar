package com.bhaskar.sharewin.dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bhaskar.sharewin.BaseActivity;
import com.bhaskar.sharewin.R;


public class SWWapV2Activity extends BaseActivity {

    private WebView webView;
    private String mTitle;
    private String mURL;
    private ProgressBar progressBar;

    public static Intent getIntent(Context context, String title, String url) {
        Intent intent = new Intent(context, SWWapV2Activity.class);
        intent.putExtra("title", title);
        intent.putExtra("url", url);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sw_wap_v2);
        handleExtras();
        ImageView ivClose = findViewById(R.id.ivClose);
        TextView tvHeader = findViewById(R.id.header_tv);
        webView = findViewById(R.id.webView);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setPadding(0, 0, 0, 0);
        progressBar.setMax(100);

        tvHeader.setText(mTitle);
        ivClose.setOnClickListener(v -> finish());
        setUpWebView();
    }

    private void handleExtras() {
        if (getIntent() != null) {
            mTitle = getIntent().getStringExtra("title");
            mURL = getIntent().getStringExtra("url");
        }
    }

    public void setValue(int progress) {
        progressBar.setProgress(progress);
        if (progress == 100)
            progressBar.setVisibility(View.GONE);
    }

    public void setUpWebView() {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progressBar.setVisibility(View.VISIBLE);
                if (newProgress < 15)
                    setValue(15);
                else
                    setValue(newProgress);
                super.onProgressChanged(view, newProgress);
            }
        });
        webView.loadUrl(mURL);

    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // This is my website, so do not override; let my WebView load the page
            webView.loadUrl(url);
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

    }

}
