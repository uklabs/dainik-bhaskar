package com.bhaskar.sharewin.dashboard;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bhaskar.sharewin.R;

import java.util.List;

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.TicketViewHolder> {

    private final Context context;
    private final List<String> mList;
    private int selectedCount;

    TicketAdapter(Context context, List<String> list, int selectedCount){
        this.context = context;
        this.mList = list;
        this.selectedCount = selectedCount;
    }

    @NonNull
    @Override
    public TicketViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new TicketViewHolder(inflater.inflate(R.layout.row_ticket_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TicketViewHolder holder, int position) {
        holder.tvCounter.setText(String.valueOf(position+1));
        if(position<selectedCount){
            holder.rootContainer.setBackground(context.getResources().getDrawable(R.drawable.canvas_filled_ticket));
            holder.tvCounter.setTextColor(context.getResources().getColor(R.color.color_Black));
            //holder.ivTicket.setBackgroundColor(context.getResources().getColor(R.color.ticket_enabled_color));
        }else{
            //holder.ivTicket.setBackgroundColor(context.getResources().getColor(R.color.ticket_disabled_color));
            holder.rootContainer.setBackground(context.getResources().getDrawable(R.drawable.canvas_empty_ticket));
            holder.tvCounter.setTextColor(context.getResources().getColor(R.color.colorGrey));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class TicketViewHolder extends RecyclerView.ViewHolder{
        private View rootContainer;
        private TextView tvCounter;
        public TicketViewHolder(View itemView) {
            super(itemView);
            tvCounter = itemView.findViewById(R.id.tvCounter);
            rootContainer = itemView.findViewById(R.id.rootContainer);

        }
    }
}
