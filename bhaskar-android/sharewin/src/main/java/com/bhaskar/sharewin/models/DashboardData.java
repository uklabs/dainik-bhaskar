package com.bhaskar.sharewin.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class DashboardData implements Serializable {


    public String shareDesc;
    public String shareMsg;
    public String ticketTitle;
    public String ticketLeftTitle;
    public String ticketDesc;
    public String ticketDesc2;
    public String referralTitle;
    public String referralLeftTitle;
    public String referralClaimBtn;
    public String offerExpire;
    public String offerExpireMsg;
    public String needHelpUrl;
    public String needHelpTxt;
    public DashboardData(String extraValues){
        try {
            JSONObject jsonObject = new JSONObject(extraValues);
            shareDesc  = jsonObject.optString("share_desc");
            shareMsg  = jsonObject.optString("share_msg");
            ticketTitle  = jsonObject.optString("ticket_title");
            ticketLeftTitle  = jsonObject.optString("ticket_left_title");
            ticketDesc  = jsonObject.optString("ticket_desc");
            ticketDesc2  = jsonObject.optString("ticket_desc2");
            referralTitle  = jsonObject.optString("referral_title");
            referralLeftTitle  = jsonObject.optString("referral_left_title");
            referralClaimBtn  = jsonObject.optString("referral_claim_btn");
            offerExpire  = jsonObject.optString("offer_expire");
            offerExpireMsg  = jsonObject.optString("offer_expire_msg");
            needHelpUrl  = jsonObject.optString("need_help_url");
            needHelpTxt  = jsonObject.optString("need_help_txt");
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

}
