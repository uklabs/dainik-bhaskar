package com.bhaskar.sharewin.utility;

public class SWConstants {
    public static final String TICKET_ID = "ticket_id";
    public static final String CLAIMED = "claimed";
    public static final String WAP_TITLE = "wap_title";
    public static final String TICKET_URL = "ticket_url";
}
