package com.bhaskar.sharewin.utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;

import com.bhaskar.sharewin.R;

public class SWUtility {
    private static final SWUtility ourInstance = new SWUtility();

    private SWUtility() {
    }

    public static SWUtility getInstance() {
        return ourInstance;
    }

    public ProgressDialog generateProgressDialog(Context context, boolean cancelable) {
        ProgressDialog progressDialog = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            progressDialog = new ProgressDialog(context, R.style.SWProgressThemeLogin);
        else
            progressDialog = new ProgressDialog(context, R.style.SWAlertDialog_Holo_Login);
        progressDialog.setCancelable(cancelable);
        return progressDialog;
    }


}
