package com.bhaskar.sharewin.utility;

import com.db.util.Urls;

public class ShareWinURLS {

    public static String SW_FEED_BASE_URL = Urls.SW_BASE_URL;
    public static final String SW_GET_USER_DATA = "sharewin/getData/";
    public static final String SW_CLAIM_TICKET = "sharewin/claimticket/";
}
