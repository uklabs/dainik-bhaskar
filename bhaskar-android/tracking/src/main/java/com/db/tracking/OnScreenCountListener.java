package com.bhaskar.appscommon.tracking;

/**
 * Created by DB on 21-04-2017.
 */

public interface OnScreenCountListener {
    void onScreenCount(int count);
}
