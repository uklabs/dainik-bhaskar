package com.bhaskar.appscommon.tracking;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.NotificationInfo;
import com.comscore.Analytics;
import com.comscore.PublisherConfiguration;
import com.bhaskar.appscommon.tracking.appsflyer.AppFlyerConst;
import com.bhaskar.appscommon.tracking.atracker.ATracker;
import com.bhaskar.appscommon.tracking.clevertap.CleverTapDB;
import com.bhaskar.appscommon.tracking.location.Location;
import com.bhaskar.appscommon.tracking.ultima.UltimaTracking;
import com.bhaskar.appscommon.tracking.util.Systr;
import com.bhaskar.appscommon.tracking.util.TrackingPreferences;
import com.bhaskar.appscommon.tracking.util.TrackingQuickPreferences;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.bhaskar.appscommon.tracking.wisdom.RealtimeTracking;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DB on 17-03-2017.
 */

public class Tracking {

    public static final String SESSION_DONE_BROADCAST = "session_complete";
    public static final String PUSH_TAG = "Tracking_push";

    // register count receiver
    public static void registerCountReceiver(Context context, OnScreenCountListener listener) {
        if (context != null) {
            TrackingHost.getInstance(context).registerCountReceiver(listener);
        }
    }

    // intialize all values as location, DBId, sessionId
    public static void intializeTracking(final Context context, final String appId, final String locationUrl, final String toggleNotifyUrl, final String sessionUrl, final String wisdomAppSourceUrl, final @ATracker.ToggleStatus int rashifalStatus, final @ATracker.ToggleStatus int notificationStatus, final String channelId, final String getAppsUrl, final String source, final String medium, final String campaign, final boolean isBlockedCountry) {

        fetchDBIdAndLocation(context, appId, sessionUrl, new OnApiRequestListener() {
            @Override
            public void onComplete() {
                // Toggle Called first time only
                String dbId = TrackingData.getDBId(context);
                if (!TextUtils.isEmpty(dbId)) {
                    notifyToggleFirstTime(context, appId, toggleNotifyUrl, rashifalStatus, notificationStatus, source, medium, campaign);
                }

                // Tracking
                if (!TrackingHost.getInstance(context).getAppSourceHit()) {
                    trackWisdomAppSource(context, wisdomAppSourceUrl, appId, medium, campaign, source, isBlockedCountry);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(SESSION_DONE_BROADCAST));

//                // App List send first time or app update time
//                try {
//                    if (!isBlockedCountry && TrackingPreferences.getInstance(context).getIntValue(TrackingQuickPreferences.APP_LIST_SENT_APP_VERSION, 0) < TrackingUtils.getAppVersionCode(context) && !TextUtils.isEmpty(getAppsUrl)) {
//                        Intent intent = new Intent(context, GetAppsIntentService.class);
//                        intent.putExtra("channelId", channelId);
//                        intent.putExtra("url", getAppsUrl);
//                        context.startService(intent);
//                    }
//                } catch (Exception e) {
//                }
            }
        }, isBlockedCountry);
    }

    private static void fetchDBIdAndLocation(Context context, String appId, String sessionUrl, OnApiRequestListener listener, boolean isBlockedCountry) {
        if (context != null)
            Location.sessionAndLocationTracking(context, sessionUrl, TrackingHost.getInstance(context).getFCMToken(), appId, "", listener, isBlockedCountry);
    }


    private static void notifyToggleFirstTime(Context context, String appId, String toggleUrl, @ATracker.ToggleStatus int rashifalStatus, @ATracker.ToggleStatus int notificationStatus, String source, String medium, String campaign) {
        if (context != null) {
            if (TextUtils.isEmpty(source)) source = "direct";
            if (TextUtils.isEmpty(medium)) medium = "direct";
            if (TextUtils.isEmpty(campaign)) campaign = "direct";

            if ((!TrackingPreferences.getInstance(context).getBooleanValue(TrackingQuickPreferences.FIRST_CALL_NOTIFY_TOGGLE, false)) || (TrackingPreferences.getInstance(context).getIntValue(TrackingQuickPreferences.APP_VERSION_FOR_NOTIFY_TOGGLE, 0) < TrackingUtils.getAppVersionCode(context))) {

//                ATracker.toggleTracking(context, toggleUrl, ATracker.ToggleStatus.ON, ATracker.ToggleType.OTHER, TrackingHost.getInstance(context).getFCMToken(), appId, TrackingUtils.getPrimaryEmailId(context), source, medium, campaign, null);
//                ATracker.toggleTracking(context, toggleUrl, ATracker.ToggleStatus.OFF, ATracker.ToggleType.RASHIFAL, TrackingHost.getInstance(context).getFCMToken(), appId, TrackingUtils.getPrimaryEmailId(context), source, medium, campaign, null);

                if (!TextUtils.isEmpty(TrackingHost.getInstance(context).getFCMToken())) {
                    ATracker.toggleTracking(context, toggleUrl, notificationStatus, ATracker.ToggleType.OTHER, TrackingHost.getInstance(context).getFCMToken(), appId, "", source, medium, campaign, null, "1", "1", "1", "1");
//                    ATracker.toggleTracking(context, toggleUrl, rashifalStatus, ATracker.ToggleType.RASHIFAL, TrackingHost.getInstance(context).getFCMToken(), appId, "", source, medium, campaign, null);

                    TrackingPreferences.getInstance(context).setBooleanValue(TrackingQuickPreferences.FIRST_CALL_NOTIFY_TOGGLE, true);
                    TrackingPreferences.getInstance(context).setIntValue(TrackingQuickPreferences.APP_VERSION_FOR_NOTIFY_TOGGLE, TrackingUtils.getAppVersionCode(context));
                }
            }
        }
    }

    /**********************************************************************
     *  Call in Application
     ***********************/

    public static void initialiseComscore(Context applicationContext, String publisherId, String secretId, boolean isBlockedCountry) {
        if (!isBlockedCountry) {
            // ComScore
            PublisherConfiguration myPublisherConfig = new PublisherConfiguration.Builder()
                    .publisherId(publisherId)
                    .publisherSecret(secretId)
                    .vce(false)
                    .build();
            Analytics.getConfiguration().addClient(myPublisherConfig);
            if (applicationContext != null) {
                Analytics.start(applicationContext);
            }
        }
    }


    /***********************************************************************
     * Call in Activities
     ***********************/

    private static Context mContext;

    public static void onCreate(Context context) {
        mContext = context;
    }

    public static void onStart(Context context) {
    }

    public static void onResume(Context context, boolean isBlockedCountry) {
        if (!isBlockedCountry) {
            // Comscore
            Analytics.notifyEnterForeground();
        }
    }

    public static void onPause(Context context, boolean isBlockedCountry) {
        if (!isBlockedCountry) {
            // Comscore
            Analytics.notifyExitForeground();
        }
    }

    public static void onStop(Context context) {
    }

    public static void onDestroy(Context context) {
    }


    /*************************************************************************
     * Other Tracking calls
     *********************/

    public static void notifyToggle(Context context, String appId, String toggleUrl, @ATracker.ToggleStatus int status, @ATracker.ToggleType String type, String source, String medium, String campaign, OnRequestListener listener) {
        if (context != null) {
            if (TextUtils.isEmpty(source)) source = "direct";
            if (TextUtils.isEmpty(medium)) medium = "direct";
            if (TextUtils.isEmpty(campaign)) campaign = "direct";

            ATracker.toggleTracking(context, toggleUrl, status, type, TrackingHost.getInstance(context).getFCMToken(), appId, "", source, medium, campaign, listener, "1", "1", "1", "1");
        }
    }

    public static void notifyToggle(Context context, String appId, String toggleUrl, @ATracker.ToggleStatus int status, @ATracker.ToggleType String type, String source, String medium, String campaign, OnRequestListener listener, String allType, String breakType, String mrngType, String evenType) {
        if (context != null) {
            if (TextUtils.isEmpty(source)) source = "direct";
            if (TextUtils.isEmpty(medium)) medium = "direct";
            if (TextUtils.isEmpty(campaign)) campaign = "direct";

            ATracker.toggleTracking(context, toggleUrl, status, type, TrackingHost.getInstance(context).getFCMToken(), appId, "", source, medium, campaign, listener, allType, breakType, mrngType, evenType);
        }
    }

    public static void rashifalRegistration(Context context, String rashifalRegisUrl, String name, String dob, String sunshine, String appId, OnRequestListener listener) {
        if (context != null) {
            ATracker.rashifalTracking(context, rashifalRegisUrl, name, dob, sunshine, TrackingHost.getInstance(context).getFCMToken(), appId, "", listener);
        }
    }

    private static void trackFirstScreenOfSession(Context context, Tracker gaTracker, String value, String utm_source, String utm_medium, String utm_campaign) {
        if (context != null) {
            if (!TextUtils.isEmpty(utm_campaign) && !utm_campaign.equalsIgnoreCase("direct")) {
                value = value + "_" + utm_campaign;
                if (value.contains("?")) {
                    value = value + "&utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
                } else {
                    value = value + "?utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
                }
            }

            Systr.println("GA Screen : First : Value : " + value);

            // GTM
            Bundle bundle = new Bundle();
            bundle.putString("ga_screen_name", value);

            bundle.putString("GA_DBID", TrackingData.getDbORDeviceId(context));
            bundle.putString("latest_Install_DT", TrackingData.getAppInstallDate(context));
            bundle.putString("first_Install_DT", TrackingData.getFirstInstallDate(context));
            bundle.putString("campaing_info", "C:" + utm_campaign);

            FirebaseAnalytics instance = FirebaseAnalytics.getInstance(context);
            instance.logEvent("screen_visit", bundle);

            // Google Analytics
//        if (gaTracker != null) {
//            gaTracker.set("&cd4", "");
//            gaTracker.setScreenName(value);
//            gaTracker.send(new HitBuilders.ScreenViewBuilder().build());
//        }


            TrackingHost.getInstance(context).increaseScreenCount();

            // CleverTap Page View
            CleverTapDB.getInstance(context).cleverTapTrackPageView(context, value);

            AppFlyerConst.isFirstGaScreenFired = true;
        }
    }

    public static void trackGAScreen(Context context, Tracker gaTracker, String value, String utm_source, String utm_medium, String utm_campaign) {
        if (context != null) {
            if (!AppFlyerConst.isFirstGaScreenFired) {
                trackFirstScreenOfSession(context, gaTracker, value, utm_source, utm_medium, utm_campaign);
            } else {
                if (!TextUtils.isEmpty(utm_campaign) && !utm_campaign.equalsIgnoreCase("direct")) {
                    value = value + "_" + utm_campaign;
                }

                Systr.println("GA Screen : Value : " + value);

                // GTM
                Bundle bundle = new Bundle();
                bundle.putString("ga_screen_name", value);

                bundle.putString("GA_DBID", TrackingData.getDbORDeviceId(context));
                bundle.putString("latest_Install_DT", TrackingData.getAppInstallDate(context));
                bundle.putString("first_Install_DT", TrackingData.getFirstInstallDate(context));
                bundle.putString("campaing_info", "C:" + utm_campaign);

                FirebaseAnalytics instance = FirebaseAnalytics.getInstance(context);
                instance.logEvent("screen_visit", bundle);

                // Google Analytics
//            if (gaTracker != null) {
//                gaTracker.set("&cd4", "");
//                gaTracker.setScreenName(value);
//                gaTracker.send(new HitBuilders.ScreenViewBuilder().build());
//            }


                TrackingHost.getInstance(context).increaseScreenCount();

                // CleverTap Page View
                CleverTapDB.getInstance(context).cleverTapTrackPageView(context, value);
            }
        }
    }

    public static void trackGAScreen(Context context, Tracker gaTracker, String value, String utm_source, String utm_medium, String utm_campaign, String publisherIdAndName) {

        if (context != null) {
            if (!TextUtils.isEmpty(utm_campaign) && !utm_campaign.equalsIgnoreCase("direct")) {
                value = value + "_" + utm_campaign;
            }

            Systr.println("GA Screen : Value : " + value);

            // GTM
            Bundle bundle = new Bundle();
            bundle.putString("ga_screen_name", value);

            bundle.putString("GA_DBID", TrackingData.getDbORDeviceId(context));
            bundle.putString("latest_Install_DT", TrackingData.getAppInstallDate(context));
            bundle.putString("first_Install_DT", TrackingData.getFirstInstallDate(context));
            bundle.putString("campaing_info", "C:" + utm_campaign);

            FirebaseAnalytics instance = FirebaseAnalytics.getInstance(context);
            instance.logEvent("screen_visit", bundle);

            // Google Analytics
//        if (gaTracker != null) {
//            gaTracker.set("&cd4", publisherIdAndName);
//            gaTracker.setScreenName(value);
//            gaTracker.send(new HitBuilders.ScreenViewBuilder().build());
//        }

            TrackingHost.getInstance(context).increaseScreenCount();

            // CleverTap Page View
            CleverTapDB.getInstance(context).cleverTapTrackPageView(context, value);
        }
    }

    /*public static void trackGAScreen(Context context, Tracker gaTracker, String value, String campaign) {
//        if (!AppFlyerConst.isFirstGaScreenFired)
//            trackFirstScreenOfSession(context, gaTracker, value, campaign);

        if (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct")) {
            value = value + "_" + campaign;
        }

        // Google Analytics
        if (gaTracker != null) {
            Systr.println("GA Screen : Value : " + value);

            gaTracker.setScreenName(value);
            gaTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }

        if (context != null) {
            TrackingHost.getInstance(context).increaseScreenCount();
        }
    }*/

    public static void trackGAEvent(Context mContext, Tracker gaTracker, String category, String action, String label, String campaign) {
        if (mContext != null && TrackingHost.getInstance(mContext).getEventTrackingEnabled()) {
            if (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct")) {
                label = label + "_" + campaign;
            }

            Systr.println("GA Event : Category : " + category + ", Action : " + action + ", label : " + label);

            Bundle bundle = new Bundle();
            bundle.putString("ev_cat", category);
            bundle.putString("ev_action", action);
            bundle.putString("ev_label", label);

            bundle.putString("GA_DBID", TrackingData.getDbORDeviceId(mContext));
            bundle.putString("latest_Install_DT", TrackingData.getAppInstallDate(mContext));
            bundle.putString("first_Install_DT", TrackingData.getFirstInstallDate(mContext));
            bundle.putString("campaing_info", "C:" + campaign);

            FirebaseAnalytics instance = FirebaseAnalytics.getInstance(mContext);
            instance.logEvent("ev_custom", bundle);

            // Google Analytics
//            if (gaTracker != null) {
//                gaTracker.set("&cd4", "");
//                gaTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory(category)
//                        .setAction(action)
//                        .setLabel(label)
//                        .build());
//            }
        }


        // CleverTap Event Tracking
//        CleverTapDB.getInstance(mContext).cleverTapTrackEvent(mContext, CTConstant.EVENT_NAME.EVENT_TAMBOLA, CTConstant.PROP_NAME.CLICKON, CTConstant.PROP_VALUE.Tambola.WINNERS,new HashMap<String, Object>());
    }

    public static void trackGAEvent(Context mContext, Tracker gaTracker, String category, String action, String label, String campaign, HashMap<String, Object> dataMap) {

        if (mContext != null) {
            if (TrackingHost.getInstance(mContext).getEventTrackingEnabled()) {
                if (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct")) {
                    label = label + "_" + campaign;
                }

                Systr.println("GA Event : Category : " + category + ", Action : " + action + ", label : " + label);

                Bundle bundle = new Bundle();
                bundle.putString("ev_cat", category);
                bundle.putString("ev_action", action);
                bundle.putString("ev_label", label);

                bundle.putString("GA_DBID", TrackingData.getDbORDeviceId(mContext));
                bundle.putString("latest_Install_DT", TrackingData.getAppInstallDate(mContext));
                bundle.putString("first_Install_DT", TrackingData.getFirstInstallDate(mContext));
                bundle.putString("campaing_info", "C:" + campaign);

                FirebaseAnalytics instance = FirebaseAnalytics.getInstance(mContext);
                instance.logEvent("ev_custom", bundle);

                // Google Analytics
//            if (gaTracker != null) {
//                gaTracker.set("&cd4", "");
//                gaTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory(category)
//                        .setAction(action)
//                        .setLabel(label)
//                        .build());
//            }
            }

            // CleverTap Event Tracking
            CleverTapDB.getInstance(mContext).cleverTapTrackEvent(mContext, category, action, label, dataMap);
        }
    }

    public static void trackGAEventAlways(Context context, Tracker gaTracker, String category, String action, String label, String campaign) {

        if (context != null) {

            if (!TextUtils.isEmpty(campaign) && !campaign.equalsIgnoreCase("direct")) {
                label = label + "_" + campaign;
            }

            Systr.println("GA Event : Category : " + category + ", Action : " + action + ", label : " + label);

            Bundle bundle = new Bundle();
            bundle.putString("ev_cat", category);
            bundle.putString("ev_action", action);
            bundle.putString("ev_label", label);

            bundle.putString("GA_DBID", TrackingData.getDbORDeviceId(context));
            bundle.putString("latest_Install_DT", TrackingData.getAppInstallDate(context));
            bundle.putString("first_Install_DT", TrackingData.getFirstInstallDate(context));
            bundle.putString("campaing_info", "C:" + campaign);

            FirebaseAnalytics instance = FirebaseAnalytics.getInstance(context);
            instance.logEvent("ev_custom", bundle);

            // Google Analytics
//        if (gaTracker != null) {
//            gaTracker.set("&cd4", "");
//            gaTracker.send(new HitBuilders.EventBuilder()
//                    .setCategory(category)
//                    .setAction(action)
//                    .setLabel(label)
//                    .build());
//        }
        }
    }

    public static void trackWisdomListingPage(Context context, String wisdomUrl, String url, String email, String mobile, String appId) {
//        if (context != null) {
//            RealtimeTracking.listingPageTracking(context, wisdomUrl, url, TrackingHost.getInstance(context).getFCMToken(), email, mobile, appId);
//        }
    }

    public static void trackWisdomArticlePage(Context context, String trackUrl, String url, int pageNumber, String email, String mobile, String appId, int listIndex, int scrollIndex, int time, String prevStoryId) {
        if (context != null) {
            RealtimeTracking.articleAndPhotoGalleryTracking(context, trackUrl, url, pageNumber, TrackingHost.getInstance(context).getFCMToken(), email, mobile, appId, listIndex, scrollIndex, time, prevStoryId);
        }
    }

    public static void trackWisdomNotification(Context context, String wisdomUrl, String catId, String title, String newsType, String eventType, String storyid, String aurl, String url, String appId, String channelNumber, String widgetName) {
        if (context != null) {
            RealtimeTracking.notificationTracking(context, wisdomUrl, catId, title, newsType, eventType, storyid, aurl, url, TrackingHost.getInstance(context).getFCMToken(), appId, channelNumber, widgetName);
        }
    }

    public static void trackWisdomNotification(Context context, String wisdomUrl, String catId, String pCatId, String title, String newsType, String eventType, String storyid, String aurl, String url, String appId, String channelNumber, String widgetName, String sl_id) {
        if (context != null) {
            RealtimeTracking.notificationTracking(context, wisdomUrl, catId, pCatId, title, newsType, eventType, storyid, aurl, url, TrackingHost.getInstance(context).getFCMToken(), appId, channelNumber, widgetName, sl_id);
        }
    }

    public static void trackWisdomAppSource(Context context, String wisdomUrl, String appId, String medium, String campaign, String source, boolean isBlockedCountry) {
        if (context != null) {
//            if (!isBlockedCountry) {
            if (TextUtils.isEmpty(source)) source = "direct";
            if (TextUtils.isEmpty(medium)) medium = "direct";
            if (TextUtils.isEmpty(campaign)) campaign = "direct";

            RealtimeTracking.appSourceTracking(context, wisdomUrl, medium, campaign, source, TrackingHost.getInstance(context).getFCMToken(), appId);
//            }
        }
    }

    public static void trackUltimaVideo(Context context, String ultimaTrackerUrl, String appTrack, boolean isBlockedCountry) {
        if (context != null) {
            if (!isBlockedCountry) {
                UltimaTracking.sendUltimaTrackingToServer(context, ultimaTrackerUrl, appTrack);
            }
        }
    }

    public static void trackUltimaVideoQuartile(Context context, String ultimaTrackerUrl, int quartile, boolean isBlockedCountry) {
        if (context != null) {
            if (!isBlockedCountry) {
                UltimaTracking.sendUltimaTrackingToServer(context, ultimaTrackerUrl, quartile);
            }
        }
    }

    public static void trackUltimaVideoAdRequest(Context context, String ultimaTrackerUrl, String appTrack, String channelId, String videoId, boolean isBlockedCountry) {
        if (context != null) {
            if (!isBlockedCountry) {
                UltimaTracking.sendUltimaAdRequestTrackingToServer(context, ultimaTrackerUrl, appTrack, channelId, videoId);
            }
        }
    }

    public static void trackUltimaVideoAd(Context context, String ultimaTrackerUrl, String appTrack, String channelId, String videoId, int quarType, String errorValue, boolean isBlockedCountry) {
        if (context != null) {
            if (!isBlockedCountry) {
                UltimaTracking.sendUltimaAdTrackingToServer(context, ultimaTrackerUrl, appTrack, channelId, videoId, quarType, errorValue);
            }
        }
    }

    public static void trackVideoImpression(Context context, String impressionTrackUrl, int pageNum, int videoIndex, String eventType, String widgetName, String widgetId, String appTrack) {
        if (context != null) {
            RealtimeTracking.sendVideoImpressionTracking(context, impressionTrackUrl, pageNum, videoIndex, eventType, widgetName, widgetId, appTrack);
        }
    }

    public static void trackVideoAdImpression(Context context, String impressionTrackUrl, int pageNum, int videoIndex, String eventType, String widgetName, String widgetId, String appTrack) {
        if (context != null) {
            RealtimeTracking.sendVideoAdImpressionTracking(context, impressionTrackUrl, pageNum, videoIndex, eventType, widgetName, widgetId, appTrack);
        }
    }

    public static Bundle isCleverTapNotification(Map<String, String> data) {
        Bundle extras = new Bundle();
        try {
            if (!data.isEmpty()) {
                for (Map.Entry<String, String> entry : data.entrySet()) {
                    extras.putString(entry.getKey(), entry.getValue());
                }
                NotificationInfo info = CleverTapAPI.getNotificationInfo(extras);
                if (info.fromCleverTap)
                    return extras;
            }
        } catch (Throwable t) {
            Log.e(PUSH_TAG, t.getMessage() + "");
        }
        return null;
    }

    public static void createCleverTapNotification(Context applicationContext, Bundle bundle) {
        CleverTapAPI.createNotification(applicationContext, bundle);
    }

    public static void setFCMTokenForCleverTap(Context context, String token) {
        try {
            CleverTapAPI cleverTapAPI = CleverTapAPI.getDefaultInstance(context);
            //settingup clever tap notification channel
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                CleverTapAPI.createNotificationChannel(context, "divya_sound_push", "Clever Sound Push", "Your Channel Description", NotificationManager.IMPORTANCE_MAX, true);

                for (int i = 0; i < CHANNEL_IDS.length; i++) {
                    try {
                        CleverTapAPI.createNotificationChannel(context, CHANNEL_IDS[i], CHANNEL_NAMES[i], "Your Channel Description", NotificationManager.IMPORTANCE_MAX, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            if (null != cleverTapAPI) {
                cleverTapAPI.pushFcmRegistrationId(token, true);
            }
        } catch (Exception e) {
            Log.e(PUSH_TAG, e.getMessage() + "");
        }
    }

    static String[] CHANNEL_IDS = {"channel_id_sound",
            "channel_id_vibration",
            "channel_id_silent",
            "channel_id_sound_and_vibration"};

    static String[] CHANNEL_NAMES = {"Sound",
            "Vibration",
            "Silent",
            "Sound and Vibration"};
}
