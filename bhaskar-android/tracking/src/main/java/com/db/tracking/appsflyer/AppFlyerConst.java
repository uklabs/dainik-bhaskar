package com.bhaskar.appscommon.tracking.appsflyer;

/**
 * Created by DB on 17-03-2017.
 */

public class AppFlyerConst {
    public static boolean isFirstGaScreenFired = false;

    public interface Event {
        String TAB = "tab";
    }

    public interface Key {
        String CLICK = "click";
        String BRAND = "prime";
    }

    public interface AlertValue {
        String SCREEN_VALUE = "NOTIFY_HUB";
        String ARTICLE_VALUE = "NOTIFY_HUB-ART";
        String STACK_SCREEN_VALUE = "NOTIFY_HUB_STACK";
        String STACK_ARTICLE_VALUE = "NOTIFY_HUB_STACK-ART";
    }

    public interface RecommendationValue {
        String SCREEN_VALUE = "RECOMMENDATION";
        String ARTICLE_VALUE = "RECOMMENDATION-ART";
    }

    public interface BookmarkValue {
        String SCREEN_VALUE = "BOOKMARK";
        String ARTICLE_VALUE = "BOOKMARK-ART";
    }

    public interface GACategory {
        String RECOMMENDATION = "Recommendation";
        String ACCOUNT = "Account";
        String NOTIFICATION = "Notification";
        String ARTICLE_EVENT = "Article_Event";
        String SCROLL_DEPTH = "Scroll_Depth";
        String SOCIAL = "Social";
        String MENU = "Menu";
        String SPLASH = "Splash";
        String SELFIE = "Selfie";
        String app_update = "Update";
        String GDPR = "gdpr";
        String OFFER = "Offer";
        String PRIME_GALLERY = "Prime_Gallery";
        String TOP_NAV = "Top_Nav";
        String RADIO = "Radio";
        String SCROLL_TEST = "Scroll_Test";
        String VIDEO_GALLERY = "Video_Gallery";
    }

    public interface GAAction {
        String IMPRESSION = "impression";
        String CLICK = "click";
        String DEFAULT = "default";
        String TOGGLE_NOTIFICATION = "toggle_notify";
        String TOGGLE_ALL = "toggle_all";
        String TOGGLE_MORNINIG_BRIEF = "toggle_morning_brief";
        String TOGGLE_EVENING_ROUNDUP = "toggle_evening_roundup";
        String TOGGLE_STICKY = "sticky";
        String TOGGLE_STACK = "stack";
        String SHARE = "share";
        String FONT = "font";
        String COMMENT = "comment";
        String BOOKMARK = "bookmark";
        String DND = "dnd";
        String SOUND = "sound";
        String VIBRATE = "vibrate";
        String RATE_CLICK = "rate_click";

        String VIEW = "view";
        String LIKE = "like";
        String RELIKE = "relike";
        String DISLIKE = "dislike";
        String MUTE = "mute";

        String UPLOAD = "upload";
        String DELETE = "delete";
        String FLAG = "flag";
        String REFLAG = "reflag";
        String SHARE_FB = "share_fb";
        String SHARE_WHATSAPP = "share_whatsapp";

        String ADS = "ads";
        String VERSION_API = "version_api";

        String PLAY_AARTI = "Play_Aarti";
        String PAUSE_AARTI = "Pause_Aarti";
        String DOWNLOAD_AARTI = "Download_Aarti";
        String READ_AARTI = "Read_Aarti";

        String DOWNLOAD = "Download";

        String LOADER_DISPLAY = "Loader_Display";
        String LOADER_CANCEL = "Loader_Cancel";
        String LOADER_ACCEPT = "Loader_Accept";

        String REGISTER = "register";
        String LOGIN = "login";
        String LOGOUT = "logout";

        String SUCCESS = "success";
        String FAIL = "fail";
        String SKIP = "skip";

        String HOME = "Home";
        String ARTICLE = "Article";

        String TOGGLE_VERTICAL_SWIPE = "toggle_vertical_swipe";
    }

    public interface GALabel {
        String ON = "ON";
        String OFF = "OFF";
        String MEDIUM = "medium";
        String SMALL = "small";
        String LARGE = "large";
        String PG = "PG";
        String APP_SHARE = "app_share";
        String RATE_US = "rate_us";
        String HAMBERGER = "hamberger";
        String SWIPE = "swipe";
        String CLICK = "click";
        String FEEDBACK = "feedback";
        String BOOKMARK = "bookmark";

        String HOME = "home";
        String SUCCESS = "success";
        String YES = "yes";
        String NO = "No";

        String WHATSAPP = "whatsapp";

        String FB = "fb";
        String GOOGLE = "google";
        String MOBILE = "mobile";
        String UPDATE = "update";
        String OTP = "otp";


        String DVB_CLICK = "click";
        String NEXT_STORY = "nextstory";
        String COMPLETE = "complete";

        String NOT_RECEIVE_NOTIFICATION = "not_receive_notification";
    }

    public interface GAScreen {
        String HOME = "HOME";
        String BRAND = "PRIME";
        String SETTINGS = "SETTINGS";
        String SETTINGS_NOTIFICATION = "SETTINGS_NOTIFICATION";
        String SETTINGS_VIDEO = "SETTINGS_VIDEO";
        String SETTINGS_QUICKREAD = "SETTINGS_QUICKREAD";
        String SETTINGS_TUTORIAL = "SETTINGS_TUTORIAL";
        String TUTORIAL = "TUTORIAL";
        String BOOKMARK = "BOOKMARK";
        String COMMENT = "COMMENT";
        String PRIVACY_POLICY = "PRIVACY_POLICY";
        String ABOUT_US = "ABOUT US";
        String APPWALL_COUPON_RANI = "APPWALL_COUPONRANI";
        String LOGIN = "LOGIN";
        String FORGOT_PASSWORD = "FORGOT_PASSWORD";
        String SIGNUP = "SIGNUP";
        String WEB_VIEW = "WEBVIEW";
        String SPLASH = "SPLASH";
        String NOTIFY_SPLASH = "NOTIFY-SPLASH";
        String SELFIE = "SELFIE";

        String PROFILE = "PROFILE";
        String EDIT_PROFILE = "EDIT_PROFILE";

        String PERSONAL_INFO_SCREEN = "PERSONAL_INFO";
        String VERIFICATION_SCREEN = "FORGOT_VERIFICATION_SCREEN";

        String NEXT_ARTICLE_GA_ARTICLE = "NEXT-ART";
        String SWIPE_ARTICLE_GA_ARTICLE = "SWIPE-ART";
        String FLICKER_WIDGET_GA_ARTICLE = "FLICKER_WIDGET-ART";
        String FLICKER_WIDGET_HOME_GA_ARTICLE = "FLICKER_WIDGET_HOME-ART";
        String RECOMMENDATION_GA_ARTICLE = "RECOMMENDATION-ART";
        String OPEN_LINK_GA_ARTICLE = "OPEN_LINK-ART";
        String FLICKER_SCREEN = "FlickerScreen";
        String AUTHOR_GA_ARTICLE = "AUTHOR-ART";
        String OPEN_LINK_GA_SCREEN = "OPEN_LINK";

    }

    public interface DBVideosSource {
        String VIDEO_LIST = "VList";
        String VIDEO_RECOMMENDATION = "VRec";
        String VIDEO_NOTIFY = "VNotify";

        String ARTICLE = "Art";
        String ARTICLE_RECOMMENDATION = "ARec";
        String VIDEO_PROVIDER = "VProvider";
        String VIDEO_SEARCH = "VSearch";
        String VIDEO_SHARE = "VShare";
    }

    public interface PrefixSection {
        String HOME = "HOME";
        String HAMBERGER = "HB";
        String PRIME = "PRIME";
    }

    public interface  GAExtra{

        public static final String SEARCH_GA_ARTICLE = "SEARCH-ART";
        public static final String SEARCH_GA_SCREEN = "SEARCH";

        public static final String TAG_GA_ARTICLE = "TAG-ART";
        public static final String TAG_GA_SCREEN = "TAG";

        public static final String SHARE_GA_SCREEN = "SHARE_LINK";
        public static final String SHARE_GA_ARTICLE = "SHARE_LINK-ART";

        public static final String NOTIFICATION_GA_SCREEN = "NOTIFY";
        public static final String NOTIFICATION_GA_ARTICLE = "NOTIFY-ART";
        public static final String NOTIFICATION_GA_ARTICLE_PER = "NOTIFY-ART-PER";

        public static final String NOTIFICATION_WIDGET_NOTIFY_HUB = "notify_hub";
        public static final String NOTIFICATION_WIDGET_NOTIFY = "notify";

        public static final String WIDGET_THUMB = "WIDGET_THUMB";
        public static final String WIDGET_LIST = "WIDGET_LIST";
        public static final String WIDGET_STICKY = "WIDGET_STICKY";

        public static final String WIDGET_THUMB_ART = "WIDGET_THUMB-ART";
        public static final String WIDGET_LIST_ART = "WIDGET_LIST-ART";
        public static final String WIDGET_STICKY_ART = "WIDGET_STICKY-ART";

    }
}
