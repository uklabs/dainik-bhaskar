package com.bhaskar.appscommon.tracking.location;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.appscommon.tracking.OnApiRequestListener;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.util.CellInfo;
import com.bhaskar.appscommon.tracking.util.Systr;
import com.bhaskar.appscommon.tracking.util.TrackingPreferences;
import com.bhaskar.appscommon.tracking.util.TrackingQuickPreferences;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by DB on 21-03-2017.
 */

public class Location {
    private static final String OPERATING_SYSTEM = "android";

    public static void sessionAndLocationTracking(final Context context, String sessionUrl, String token, String appId, String email, final OnApiRequestListener listener, final boolean isBlockedCountry) {
        Systr.println("Session URL  ::" + sessionUrl);
        CellInfo location = TrackingUtils.getLocation(context);

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cn", TrackingUtils.getNetwork(context));
            jsonObject.put("op:", TrackingUtils.getOperatorName(context));
            jsonObject.put("ss", TrackingUtils.getScreenSize(context));
            jsonObject.put("did", TrackingData.getDeviceId(context));
            jsonObject.put("br", Build.BRAND);
            jsonObject.put("md", Build.MODEL);
            jsonObject.put("ov", String.valueOf(Build.VERSION.SDK_INT));
            jsonObject.put("os", OPERATING_SYSTEM);
            jsonObject.put("app_ver", TrackingUtils.getAppVersion(context));
            jsonObject.put("tk", token);
            jsonObject.put("aid", appId);
            jsonObject.put("em", email);

            jsonObject.put("radio", location.radio);
            jsonObject.put("mcc", location.mcc);
            jsonObject.put("mnc", location.mnc);
            jsonObject.put("address", location.address);
            JSONObject cellObject = new JSONObject();
            cellObject.put("lac", location.cells.lad);
            cellObject.put("cid", location.cells.cid);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(cellObject);
            jsonObject.put("cells", jsonArray);

        } catch (Exception e) {
        }

        Systr.println("Session Send Json : " + jsonObject.toString());


        StringRequest stringRequest = new StringRequest(Request.Method.POST, sessionUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                Systr.println("Session Response : " + res);

                try {
                    JSONObject response = new JSONObject(res);
                    parseJsonSession(context, response, isBlockedCountry);
                } catch (Exception e) {
                }

                if (listener != null) {
                    listener.onComplete();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Session Error : " + error);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }) {
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", "1");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                String json = jsonObject.toString();
                String data = TrackingUtils.encryptString(json);
                Systr.println("session data : " + data);
                Map<String, String> params = new HashMap<String, String>();
                params.put("edata", data);
                return params;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(14000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }

    private static void parseJsonSession(Context context, JSONObject data, boolean isBlockedCountry) {
        try {
            if (data.has("location")) {
                try {
                    String value = TrackingUtils.decryptString(data.getString("location"));
                    Systr.println("location str : " + value);

                    if (isBlockedCountry) {
                        TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.CITY, "");
                        TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.STATE, "");
                        TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.COUNTRY, "");
                        TrackingPreferences.getInstance(context).setBooleanValue(TrackingQuickPreferences.HAS_LOCATION, false);

                    } else {
                        JSONObject locationJsonObject = new JSONObject(value);
                        if (locationJsonObject.has("address_detail")) {
                            JSONObject addressJsonObject = locationJsonObject.getJSONObject("address_detail");
                            String city = addressJsonObject.optString("city");
                            String state = addressJsonObject.optString("state");
                            String country = addressJsonObject.optString("country");

                            if (!TextUtils.isEmpty(city))
                                TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.CITY, city);
                            if (!TextUtils.isEmpty(state))
                                TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.STATE, state);
                            if (!TextUtils.isEmpty(country))
                                TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.COUNTRY, country);

                            if (!TextUtils.isEmpty(city)) {
                                TrackingPreferences.getInstance(context).setBooleanValue(TrackingQuickPreferences.HAS_LOCATION, true);
                            }
                        }
                    }
                } catch (JSONException e) {
                }
            }

            if (data.has("session")) {
                JSONObject sessionJsonObject = data.getJSONObject("session");
                String status = sessionJsonObject.optString("status");
                if (status.equals("Success")) {
                    String dbId = sessionJsonObject.optString("dev_id");
                    String sessionId = sessionJsonObject.optString("session_id");
                    String firstInstallDate = sessionJsonObject.optString("first_install_date");

                    TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.DB_ID, dbId);
                    TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.SESSION_ID, sessionId);
                    TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.FIRST_INSTALL_DATE, getDate(firstInstallDate));
                }
            }
        } catch (Exception e) {
        }
    }


    private static String getDate(String startDateString) {
        String currentDateAndTime = startDateString;
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date startDate = formatter.parse(startDateString);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            currentDateAndTime = sdf.format(startDate);
        } catch (Exception e) {
        }
        return currentDateAndTime;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////


}
