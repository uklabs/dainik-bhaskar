package com.bhaskar.appscommon.tracking.util;

/**
 * Created by DB on 31-01-2017.
 */

public class CellInfo {

    public String mcc;
    public String mnc;
    public String radio;
    public Wifi wifiInfo;
    public Cells cells;
    public int address = 2;

    public CellInfo(String radio, String mcc, String mnc, Cells cells, Wifi wifiInfo) {
        this.radio = radio;
        this.mcc = mcc;
        this.mnc = mnc;
        this.cells = cells;
        this.wifiInfo = wifiInfo;
    }

    public static class Cells {
        public int cid;
        public int lad;

        public Cells(int cid, int lac) {
            this.cid = cid;
            this.lad = lac;
        }
    }

    public static class Wifi {
        public String bssid;

        public Wifi(String bssid) {
            this.bssid = bssid;
        }
    }
}
