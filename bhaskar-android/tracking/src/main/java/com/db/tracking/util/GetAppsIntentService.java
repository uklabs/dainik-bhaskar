package com.bhaskar.appscommon.tracking.util;

import android.app.IntentService;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.appscommon.tracking.TrackingData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by frasin1 on 10-03-2018.
 */

public class GetAppsIntentService extends IntentService {

    private String channelId;
    private String url;

    public GetAppsIntentService() {
        super("GetApps");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            channelId = intent.getStringExtra("channelId");
            url = intent.getStringExtra("url");
        }

        final PackageManager pm = getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        JSONArray jsonArray = new JSONArray();
        for (ApplicationInfo packageInfo : packages) {
            if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                try {
                    if (!TextUtils.isEmpty(packageInfo.packageName) && !packageInfo.packageName.equalsIgnoreCase(getPackageName())) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("a_name", packageInfo.loadLabel(pm));
                        jsonObject.put("p_name", packageInfo.packageName);
                        jsonArray.put(jsonObject);
                    }
                } catch (Exception e) {
                }
            }
        }

        Systr.println("GET APPS LIST : " + jsonArray.toString());

        sendListToServer(url, jsonArray);
    }

    private void sendListToServer(String url, JSONArray appsListArray) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("db_id", TrackingData.getDBId(this));
            jsonObject.put("os", "Android");
            jsonObject.put("channel_slno", channelId);
            jsonObject.put("apps", appsListArray);
        } catch (Exception e) {
        }

        Systr.println("Send Get Apps Json : " + jsonObject.toString());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Systr.println("GET APPS Success : " + response);

                        try {
                            if (response.optString("status").equalsIgnoreCase("1")) {
                                TrackingPreferences.getInstance(GetAppsIntentService.this).setIntValue(TrackingQuickPreferences.APP_LIST_SENT_APP_VERSION, TrackingUtils.getAppVersionCode(GetAppsIntentService.this));
                            }
                        } catch (Exception e) {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("GET APPS Error : " + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(this).addToRequestQueue(jsonObjReq);
    }
}
