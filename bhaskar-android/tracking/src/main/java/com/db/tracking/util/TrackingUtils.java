package com.bhaskar.appscommon.tracking.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Display;
import android.view.WindowManager;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by DB on 09-12-2016.
 */

public class TrackingUtils {

    @Nullable
    public static CellInfo getLocation(Context context) {

        String mnc = "";
        String mcc = "";
        try {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager.getCellLocation();

                String networkOperator = telephonyManager.getNetworkOperator();

                String radio = "";
                int phoneType = telephonyManager.getPhoneType();
                switch (phoneType) {
                    case TelephonyManager.PHONE_TYPE_CDMA:
                        radio = "cdma";
                        break;
                    case TelephonyManager.PHONE_TYPE_GSM:
                        radio = "gsm";
                        break;
                    case TelephonyManager.PHONE_TYPE_SIP:
                        radio = "sip";
                        break;
                    case TelephonyManager.PHONE_TYPE_NONE:
                        radio = "none";
                        break;
                }

                if (networkOperator != null && networkOperator.length() > 3) {
                    mcc = networkOperator.substring(0, 3);
                    mnc = networkOperator.substring(3);
                }
                int cid = cellLocation.getCid();
                int lac = cellLocation.getLac();
                CellInfo.Cells cells = new CellInfo.Cells(cid, lac);
                CellInfo.Wifi wifi = new CellInfo.Wifi("");
                return new CellInfo(radio, mcc, mnc, cells, wifi);

//        WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//        WifiInfo wifiInfo = wifiMan.getConnectionInfo();
//        String bssid = wifiInfo.getBSSID();
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

        CellInfo.Cells cells = new CellInfo.Cells(0, 0);
        CellInfo.Wifi wifi = new CellInfo.Wifi("");
        return new CellInfo("", mcc, mnc, cells, wifi);
    }

    public static String getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null)
                return packageInfo.versionName;
        } catch (Exception ignored) {
        }

        return "5.1";
    }

    public static int getAppVersionCode(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null)
                return packageInfo.versionCode;
        } catch (Exception ignored) {
        }

        return 118;
    }

    public static String getScreenSize(Context context) {

        String ss = TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.SCREEN_SIZE, "");

        if (TextUtils.isEmpty(ss)) {

            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;

            ss = width + "*" + height;
            TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.SCREEN_SIZE, ss);
        }

        return ss;
    }

    public static String getOperatorName(Context context) {
        int type = getType(context);
        if (type == 2) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            return manager.getNetworkOperatorName();
        } else {
            return "WiFi";
        }
    }

    public static String getNetwork(Context context) {

        int type = getType(context);
        if (type == 2) {

            TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            int networkType = mTelephonyManager.getNetworkType();

            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "4G";
                default:
                    return "Unknown";
            }

        } else if (type == 1) {
            return "WiFi";

        } else {
            return "Unknown";
        }
    }

    private static int getType(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();

        if (info != null) {
            int netType = info.getType();
            // int netSubtype = info.getSubtype();
            if (netType == ConnectivityManager.TYPE_WIFI) {
                return 1;
            } else if (netType == ConnectivityManager.TYPE_MOBILE) {
                // && netSubtype == TelephonyManager.NETWORK_TYPE_UMTS) {
                return 2;
            }
        }

        return 0;
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {

        String android_id = TrackingPreferences.getInstance(context).getStringValue(TrackingQuickPreferences.DEVICE_ID, "");

        if (TextUtils.isEmpty(android_id)) {
            // check for permission
//            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//                android_id = telephonyManager.getDeviceId();
//
//            } else {
            try {
                android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            } catch (Exception ignored) {

            }
            if (TextUtils.isEmpty(android_id)) {
                android_id = UUID.randomUUID().toString();
            }
//            }

            TrackingPreferences.getInstance(context).setStringValue(TrackingQuickPreferences.DEVICE_ID, android_id);
        }

        return android_id;
    }

    public static String encode(@NonNull String uriString) {
        Pattern allowedUrlCharacters = Pattern.compile("([A-Za-z0-9_.~:/?\\#\\[\\]@!$&'()*+,;" + "=-]|%[0-9a-fA-F]{2})+");
        Matcher matcher = allowedUrlCharacters.matcher(uriString);
        String validUri = null;
        if (matcher.find()) {
            validUri = matcher.group();
        }
        if (TextUtils.isEmpty(validUri) || uriString.length() == validUri.length()) {
            return uriString;
        }

        // The uriString is not encoded. Then recreate the uri and encode it this time
        Uri uri = Uri.parse(uriString);
        Uri.Builder uriBuilder = new Uri.Builder().scheme(uri.getScheme()).authority(uri.getAuthority());

        for (String path : uri.getPathSegments()) {
            uriBuilder.appendPath(path);
        }

        for (String key : uri.getQueryParameterNames()) {
            uriBuilder.appendQueryParameter(key, uri.getQueryParameter(key));
        }

        return uriBuilder.build().toString();
    }

    public static String encryptString(String str, boolean isEncryptOn) {
        if (isEncryptOn) {
            try {
                return Base64.encodeToString(str.getBytes("UTF-8"), Base64.DEFAULT);
            } catch (Exception ignored) {
            }
        } else {
            return str;
        }

        return "";
    }

    public static String encryptString(String str) {
        try {
            return Base64.encodeToString(str.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (Exception ignored) {
        }

        return "";
    }

    public static String decryptString(String str) {
        try {
            return new String(Base64.decode(str, Base64.DEFAULT), "UTF-8");
        } catch (Exception ignored) {
        }

        return "";
    }
}
