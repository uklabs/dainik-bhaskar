package com.bhaskar.appscommon.tracking.wisdom;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.appscommon.tracking.TrackingData;
import com.bhaskar.appscommon.tracking.TrackingHost;
import com.bhaskar.appscommon.tracking.util.Systr;
import com.bhaskar.appscommon.tracking.util.TrackingUtils;
import com.bhaskar.appscommon.tracking.util.VolleyNetworkSingleton;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DB on 23-03-2017.
 */

public class RealtimeTracking {

    private static final String DOMAIN_TYPE = "A";
    private static final String OPERATING_SYSTEM = "android";

    public static void listingPageTracking(Context context, String wisdomUrl, String url, String token, String email, String mobile, String appId) {

        String dbId = TrackingData.getDBId(context);
        if (TextUtils.isEmpty(dbId)) {
            return;
        }

        String fullUrl = "domaintype=" + DOMAIN_TYPE
                + "&url=" + url
                + "&tk=" + token
                + "&e=" + email
                + "&m=" + mobile
                + "&app_ID=" + appId
                + "&p_sessionID=" + TrackingData.getDBId(context)
                + "&session_ID=" + TrackingData.getSessionId(context)
                + "&vno=" + TrackingUtils.getAppVersion(context)
                + "&country=" + TrackingData.getCountry(context)
                + "&state=" + TrackingData.getState(context)
                + "&city=" + TrackingData.getCity(context)
                + "&d_brand=" + Build.BRAND
                + "&d_model=" + Build.MODEL
                + "&d_os=" + OPERATING_SYSTEM
                + "&d_ov=" + Build.VERSION.SDK_INT
                + "&net_type=" + TrackingUtils.getNetwork(context)
                + "&mob_operator=" + TrackingUtils.getOperatorName(context)
                + "&app_vc=" + TrackingUtils.getAppVersionCode(context);

        String finalUrl = wisdomUrl + "?edata=" + TrackingUtils.encryptString(fullUrl);

        finalUrl = TrackingUtils.encode(finalUrl);
        Systr.println("Wisdom listing url : " + finalUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Wisdom listing Response : " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Wisdom listing Error : " + error);
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credential = "admin:admin";
                String auth = "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", "1");
                return headers;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }

    public static void articleAndPhotoGalleryTracking(Context context, String trackUrl, String url, int pageNumber, String token, String email, String mobile, String appId, int listIndex, int scrollIndex, int time, String prevStoryId) {
        String dbId = TrackingData.getDBId(context);
        if (TextUtils.isEmpty(dbId)) {
            return;
        }

        String fullUrl = "domaintype=" + DOMAIN_TYPE
                + "&url=" + url
                + "&pno=" + pageNumber
                + "&tk=" + token
                + "&e=" + email
                + "&m=" + mobile
                + "&app_ID=" + appId
                + "&p_sessionID=" + TrackingData.getDBId(context)
                + "&session_ID=" + TrackingData.getSessionId(context)
                + "&vno=" + TrackingUtils.getAppVersion(context)
                + "&country=" + TrackingData.getCountry(context)
                + "&state=" + TrackingData.getState(context)
                + "&city=" + TrackingData.getCity(context)
                + "&d_brand=" + Build.BRAND
                + "&d_model=" + Build.MODEL
                + "&d_os=" + OPERATING_SYSTEM
                + "&d_ov=" + Build.VERSION.SDK_INT
                + "&net_type=" + TrackingUtils.getNetwork(context)
                + "&app_vc=" + TrackingUtils.getAppVersionCode(context)
                + "&time=" + time
                + "&prevStoryId=" + prevStoryId
                + (listIndex >= 0 ? ("&sno=" + listIndex) : "")
                + (scrollIndex >= 0 ? ("&scroll_no=" + scrollIndex) : "")
                + "&mob_operator=" + TrackingUtils.getOperatorName(context);

        Systr.println("wisdom article url URL woencode: " + fullUrl);

        fullUrl = trackUrl + "&edata=" + TrackingUtils.encryptString(fullUrl);

        String finalUrl = TrackingUtils.encode(fullUrl);
        Systr.println("wisdom article url URL : " + finalUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Wisdom Article Response : " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Wisdom Article Error : " + error);
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credential = "admin:admin";
                String auth = "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", "1");
                return headers;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }

    public static void notificationTracking(Context context, String wisdomUrl, String catId, String title, String newsType, String eventType, String storyid, String aurl, String url, String token, String appId, String channelNumber, String widgetName) {

        String dbId = TrackingData.getDBId(context);
        if (TextUtils.isEmpty(dbId)) {
            return;
        }

        String fullUrl = "?domaintype=" + DOMAIN_TYPE
                + "&channel_slno=" + channelNumber
                + "&cat_id=" + catId
                + "&title=" + title
                + "&news_type=" + newsType
                + "&event_type=" + eventType
                + "&storyid=" + storyid
                + "&aurl=" + aurl
                + "&app_id=" + appId
                + "&url=" + url
                + "&widget_name=" + widgetName;

        String encData = "e=" + ""
                + "&tk=" + token
                + "&p_sessionID=" + TrackingData.getDBId(context)
                + "&device_id=" + TrackingData.getDeviceId(context)
                + "&session_ID=" + TrackingData.getSessionId(context)
                + "&vno=" + TrackingUtils.getAppVersion(context)
                + "&d_brand=" + Build.BRAND
                + "&d_model=" + Build.MODEL
                + "&d_os=" + OPERATING_SYSTEM
                + "&d_ov=" + Build.VERSION.SDK_INT
                + "&net_type=" + TrackingUtils.getNetwork(context)
                + "&mob_operator=" + TrackingUtils.getOperatorName(context)
                + "&app_vc=" + TrackingUtils.getAppVersionCode(context);

        String finalUrl = wisdomUrl + fullUrl + "&edata=" + TrackingUtils.encryptString(encData);

        finalUrl = TrackingUtils.encode(finalUrl);
        Systr.println("wisdom notification url : " + finalUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Wisdom Notification Response : " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Wisdom Notification Error : " + error);
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credential = "admin:admin";
                String auth = "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", "1");
                return headers;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }
    public static void notificationTracking(Context context, String wisdomUrl, String catId,String pCatId, String title, String newsType, String eventType, String storyid, String aurl, String url, String token, String appId, String channelNumber, String widgetName,String sl_id) {

        String dbId = TrackingData.getDBId(context);
        if (TextUtils.isEmpty(dbId)) {
            return;
        }

        String fullUrl = "?domaintype=" + DOMAIN_TYPE
                + "&channel_slno=" + channelNumber
                + "&cat_id=" + catId
                + "&pcat_id=" + pCatId
                + "&title=" + title
                + "&news_type=" + newsType
                + "&event_type=" + eventType
                + "&storyid=" + storyid
                + "&aurl=" + aurl
                + "&app_id=" + appId
                + "&url=" + url
                + "&widget_name=" + widgetName
                + "&sl_id=" + sl_id;

        String encData = "e=" + ""
                + "&tk=" + token
                + "&p_sessionID=" + TrackingData.getDBId(context)
                + "&device_id=" + TrackingData.getDeviceId(context)
                + "&session_ID=" + TrackingData.getSessionId(context)
                + "&vno=" + TrackingUtils.getAppVersion(context)
                + "&d_brand=" + Build.BRAND
                + "&d_model=" + Build.MODEL
                + "&d_os=" + OPERATING_SYSTEM
                + "&d_ov=" + Build.VERSION.SDK_INT
                + "&net_type=" + TrackingUtils.getNetwork(context)
                + "&mob_operator=" + TrackingUtils.getOperatorName(context)
                + "&app_vc=" + TrackingUtils.getAppVersionCode(context);

        String finalUrl = wisdomUrl + fullUrl + "&edata=" + TrackingUtils.encryptString(encData);

        finalUrl = TrackingUtils.encode(finalUrl);
        Systr.println("wisdom notification url : " + finalUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Wisdom Notification Response : " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Wisdom Notification Error : " + error);
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credential = "admin:admin";
                String auth = "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", "1");
                return headers;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }

    public static void bluePiNotificationTracking(Context context, String wisdomUrl, String message, String eventType, String storyid) {
        String dbId = TrackingData.getDBId(context);
        if (TextUtils.isEmpty(dbId)) {
            return;
        }

        String fullUrl = "message=" + message
                + "&event_type=" + eventType
                + "&storyid=" + storyid
                + "&p_sessionID=" + TrackingData.getDBId(context)
                + "&session_ID=" + TrackingData.getSessionId(context)
                + "&vno=" + TrackingUtils.getAppVersion(context)
                + "&d_brand=" + Build.BRAND
                + "&d_model=" + Build.MODEL
                + "&d_os=" + OPERATING_SYSTEM
                + "&d_ov=" + Build.VERSION.SDK_INT
                + "&net_type=" + TrackingUtils.getNetwork(context)
                + "&mob_operator=" + TrackingUtils.getOperatorName(context)
                + "&app_vc=" + TrackingUtils.getAppVersionCode(context);

        String finalUrl = wisdomUrl + "?edata=" + TrackingUtils.encryptString(fullUrl);

        finalUrl = TrackingUtils.encode(finalUrl);
        Systr.println("wisdom notification url : " + finalUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Wisdom Notification Response : " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Wisdom Notification Error : " + error);
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credential = "admin:admin";
                String auth = "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", "1");
                return headers;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }

    public static void appSourceTracking(final Context context, String wisdomUrl, String medium, String campaign, String source, String token, String appId) {
        //TO-DO :: Encrypt
        final boolean isEncryptOn = true;
        String dbId = TrackingData.getDBId(context);
        if (TextUtils.isEmpty(dbId)) {
            return;
        }
        String fullUrl = "app_version=" + TrackingUtils.getAppVersion(context)
                + "&country=" + TrackingData.getCountry(context)
                + "&state=" + TrackingData.getState(context)
                + "&city=" + TrackingData.getCity(context)
                + "&device_model=" + Build.BRAND + "_" + Build.MODEL
                + "&device_os=" + OPERATING_SYSTEM + "_" + Build.VERSION.SDK_INT
                + "&device_id=" + TrackingData.getDBId(context)
                + "&device_token=" + token
                + "&medium=" + medium
                + "&campaign=" + campaign
                + "&source=" + source
                + "&aid=" + appId
                + "&app_vc=" + TrackingUtils.getAppVersionCode(context);

        String finalUrl = wisdomUrl + "?edata=" + TrackingUtils.encryptString(fullUrl, isEncryptOn);

        finalUrl = TrackingUtils.encode(finalUrl);
        Systr.println("wisdom app source url : " + finalUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Wisdom app source Response : " + response);

                TrackingHost.getInstance(context).setAppSourceHit(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Wisdom app source Error : " + error);
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credential = "admin:admin";
                String auth = "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", (isEncryptOn) ? "1" : "0");
                return headers;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }

    public static void sendVideoImpressionTracking(final Context context, String impressionTrackUrl, int pageNum, int videoIndex, String eventType, String widgetName, String widgetId, String appTrack) {

        String fullUrl = "page_no=" + pageNum
                + "&widget_rec_pos=" + videoIndex
                + "&session_id=" + TrackingData.getDBId(context)
                + "&device_id=" + TrackingData.getDeviceId(context)
                + "&event_type=" + eventType
                + "&widget_name=" + widgetName
                + "&widget_id=" + widgetId
                + "&app_track=" + appTrack
                + "&country=" + TrackingData.getCountry(context)
                + "&state=" + TrackingData.getState(context)
                + "&city=" + TrackingData.getCity(context)
                + "&device_type=2";

        fullUrl = impressionTrackUrl + "&edata=" + TrackingUtils.encryptString(fullUrl);

        fullUrl = TrackingUtils.encode(fullUrl);
        Systr.println("wisdom video impression url : " + fullUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, fullUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Wisdom Video Impression Response : " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Wisdom Video Impression Error : " + error);
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credential = "admin:admin";
                String auth = "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", "1");
                return headers;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }

    public static void sendVideoAdImpressionTracking(final Context context, String impressionTrackUrl, int pageNum, int videoIndex, String eventType, String widgetName, String widgetId, String appTrack) {

        String fullUrl = "page_no=" + pageNum
                + "&widget_rec_pos=" + videoIndex
                + "&session_id=" + TrackingData.getDBId(context)
                + "&device_id=" + TrackingData.getDeviceId(context)
                + "&event_type=" + eventType
                + "&widget_name=" + widgetName
                + "&widget_id=" + widgetId
                + "&app_track=" + appTrack
                + "&country=" + TrackingData.getCountry(context)
                + "&state=" + TrackingData.getState(context)
                + "&city=" + TrackingData.getCity(context)
                + "&device_type=2";

        fullUrl = impressionTrackUrl + "?edata=" + TrackingUtils.encryptString(fullUrl);

        fullUrl = TrackingUtils.encode(fullUrl);
        Systr.println("wisdom video impression ad url : " + fullUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, fullUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Systr.println("Wisdom Video Impression ad Response : " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Wisdom Video Impression ad Error : " + error);
            }
        }) {
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                String credential = "admin:admin";
                String auth = "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                headers.put("api-key", "bh@@$k@r-D");
                headers.put("encrypt", "1");
                return headers;
            }
        };

        VolleyNetworkSingleton volleyNetworkSingleton = VolleyNetworkSingleton.getInstance(context);
        volleyNetworkSingleton.addToRequestQueue(stringRequest);
    }
}
